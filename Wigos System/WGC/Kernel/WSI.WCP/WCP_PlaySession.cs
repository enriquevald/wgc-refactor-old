//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlaySession.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 14-JUL-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-JUL-2010 RCI    First release.
// 11-MAY-2012 DDM    Added property total_cash_in in the class PlaySession.
// 11-NOV-2013 NMR    Properly name for param in GetOpenSessionIdForTerminal
// 03-DIC-2013 NMR    Improvement to load Play Session by given status
// 17-APR-2014 ICS&LEM  Fixed Bug WIGOSTITO-1194: Plays don't update player account 
// 20-SEP-2017 JMM    Fixed Bug WIGOS-5309 [Ticket #8698] Sala Golden Lion - 048 - Lentitud en Recargas 
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;

namespace WSI.WCP
{
  public class PlaySession
  {
    #region Properties

    private Int32 m_terminal_id;
    private Int64 m_play_session_id;

    private Int64 m_transaction_id;

    private PlaySessionType m_play_session_type;
    private Boolean m_stand_alone_session;
    private Decimal m_initial_balance;
    private Decimal m_balance;

    private String m_terminal_name;
    private Int64 m_account_id;
    private String m_card_track_data;
    private PlaySessionStatus m_status;
    private Boolean m_has_cashin_amount;
    private Decimal m_cancellable_amount;
    private Decimal m_total_cash_in;

    public Int32 TerminalId
    {
      get { return m_terminal_id; }
    }
    public Int64 PlaySessionId
    {
      get { return m_play_session_id; }
    }
    public Int64 TransactionId
    {
      get { return m_transaction_id; }
    }
    public PlaySessionType PlaySessionType
    {
      get { return m_play_session_type; }
    }
    public Boolean StandAloneSession
    {
      get { return m_stand_alone_session; }
    }
    public Decimal InitialBalance
    {
      get { return m_initial_balance; }
    }
    public Decimal Balance
    {
      get { return m_balance; }
    }
    public String TerminalName
    {
      get { return m_terminal_name; }
    }
    public Int64 AccountId
    {
      get { return m_account_id; }
    }
    public String CardTrackData
    {
      get { return m_card_track_data; }
    }
    public PlaySessionStatus Status
    {
      get { return m_status; }
    }
    public Boolean HasCashInAmount
    {
      get { return m_has_cashin_amount; }
    }
    public Decimal CancellableAmount
    {
      get { return m_cancellable_amount; }
    }
    public Decimal TotalCashIn
    {
      get { return m_total_cash_in; }
    }


    #endregion // Properties

    public PlaySession(Int32 TerminalId, Int64 PlaySessionId)
    {
      m_terminal_id = TerminalId;
      m_play_session_id = PlaySessionId;

      m_transaction_id = 0;

      m_play_session_type = PlaySessionType.NONE;
      m_stand_alone_session = true;
      m_initial_balance = 0;
      m_balance = 0;

      m_terminal_name = "";
      m_account_id = 0;
      m_card_track_data = "";

      m_has_cashin_amount = false;
      m_cancellable_amount = 0;
    }

    #region Constants

    private const Int16 COLUMN_TERMINAL_ID = 0;
    private const Int16 COLUMN_PLAY_SESSION_ID = 1;
    private const Int16 COLUMN_TRANSACTION_ID = 2;
    private const Int16 COLUMN_PLAY_SESSION_TYPE = 3;
    private const Int16 COLUMN_STAND_ALONE_SESSION = 4;
    private const Int16 COLUMN_INITIAL_BALANCE = 5;
    private const Int16 COLUMN_CURRENT_BALANCE = 6;
    private const Int16 COLUMN_TERMINAL_NAME = 7;
    private const Int16 COLUMN_ACCOUNT_ID = 8;
    private const Int16 COLUMN_TRACK_DATA = 9;
    private const Int16 COLUMN_HAS_CASHIN_AMOUNT = 10;
    private const Int16 COLUMN_CANCELLABLE_AMOUNT = 11;

    #endregion // Constants

    #region Attributes

    private static ReaderWriterLock m_rw_lock = new ReaderWriterLock();
    private static DataTable m_dt_play_sessions = null;

    #endregion // Attributes

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init attributes of the static class:
    //           - DataTable for the PlaySession Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

      try
      {
        if (m_dt_play_sessions != null)
        {
          // already init... return!
          Log.Warning("Init: PlaySession already initialized.");

          return;
        }

        // Create DataTable Columns

        m_dt_play_sessions = new DataTable("PLAY_SESSIONS");

        m_dt_play_sessions.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32")).Unique = true;
        m_dt_play_sessions.Columns.Add("PLAY_SESSION_ID", Type.GetType("System.Int64")).Unique = true;
        m_dt_play_sessions.Columns.Add("TRANSACTION_ID", Type.GetType("System.Int64"));
        m_dt_play_sessions.Columns.Add("PLAY_SESSION_TYPE", Type.GetType("System.Int32"));
        m_dt_play_sessions.Columns.Add("STAND_ALONE_SESSION", Type.GetType("System.Boolean"));
        m_dt_play_sessions.Columns.Add("INITIAL_BALANCE", Type.GetType("System.Decimal"));
        m_dt_play_sessions.Columns.Add("CURRENT_BALANCE", Type.GetType("System.Decimal"));
        m_dt_play_sessions.Columns.Add("TERMINAL_NAME", Type.GetType("System.String"));
        m_dt_play_sessions.Columns.Add("ACCOUNT_ID", Type.GetType("System.Int64"));
        m_dt_play_sessions.Columns.Add("TRACKDATA", Type.GetType("System.String"));
        m_dt_play_sessions.Columns.Add("HAS_CASHIN_AMOUNT", Type.GetType("System.Int32"));
        m_dt_play_sessions.Columns.Add("CANCELLABLE_AMOUNT", Type.GetType("System.Decimal"));
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Add a PlaySession in the DataTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32   TerminalId
    //          - Int64   PlaySessionId
    //          - Int64   TransactionId
    //          - Int32   PlaySessionType
    //          - Boolean StandAloneSession
    //          - Boolean IsPromotion
    //          - Decimal CurrentBalance
    //          - String  TerminalName
    //          - Int64   AccountId
    //          - String  TrackData
    //          - String  HasCashInAmount
    //          - String  CancellableAmount
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If play session has been added correctly.
    //
    public static Boolean Add(Int32 TerminalId,
                              Int64 PlaySessionId,
                              Int64 TransactionId,
                              PlaySessionType PlaySessionType,
                              Boolean StandAloneSession,
                              Decimal InitialBalance,
                              Decimal CurrentBalance,
                              String TerminalName,
                              Int64 AccountId,
                              String TrackData,
                              Boolean HasCashInAmount,
                              Decimal CancellableAmount)
    {
      String _filter;
      DataRow[] _dr_play_sessions;
      DataRow _play_session;

      m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

      try
      {
        if (m_dt_play_sessions == null)
        {
          Log.Error("Not initialized. PlaySession.Add Terminal: " + TerminalId + ", PlaySession: " + PlaySessionId + ".");

          return false;
        }

        // Search Terminal.
        _filter = "TERMINAL_ID = " + TerminalId;
        _dr_play_sessions = m_dt_play_sessions.Select(_filter);

        // Delete all previous PlaySessions for the TerminalId.
        foreach (DataRow _dr_play_session in _dr_play_sessions)
        {
          m_dt_play_sessions.Rows.Remove(_dr_play_session);
        }

        // Insert NEW TerminalId/PlaySessionId
        _play_session = m_dt_play_sessions.NewRow();
        _play_session[COLUMN_TERMINAL_ID] = TerminalId;
        _play_session[COLUMN_PLAY_SESSION_ID] = PlaySessionId;
        _play_session[COLUMN_TRANSACTION_ID] = TransactionId;
        _play_session[COLUMN_PLAY_SESSION_TYPE] = (Int32)PlaySessionType;
        _play_session[COLUMN_STAND_ALONE_SESSION] = StandAloneSession;
        _play_session[COLUMN_INITIAL_BALANCE] = InitialBalance;
        _play_session[COLUMN_CURRENT_BALANCE] = CurrentBalance;
        _play_session[COLUMN_TERMINAL_NAME] = TerminalName;
        _play_session[COLUMN_ACCOUNT_ID] = AccountId;
        _play_session[COLUMN_TRACK_DATA] = TrackData;
        _play_session[COLUMN_HAS_CASHIN_AMOUNT] = (HasCashInAmount ? 1 : 0);
        _play_session[COLUMN_CANCELLABLE_AMOUNT] = CancellableAmount;
        m_dt_play_sessions.Rows.Add(_play_session);

        _play_session.AcceptChanges();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: PlaySession.Add Terminal: " + TerminalId + ", PlaySession: " + PlaySessionId + ".");

        Log.Exception(_ex);

        return false;
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }
    } // Add

    //------------------------------------------------------------------------------
    // PURPOSE : Reload a PlaySession from the DB to the Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 TerminalId
    //          - Int64 PlaySessionId
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - PlaySession
    //
    public static PlaySession Reload(Int32 TerminalId, Int64 PlaySessionId, SqlTransaction SqlTrx)
    {
      PlaySession _ps;

      _ps = new PlaySession(TerminalId, PlaySessionId);

      if (!ReadPlaySessionFromDatabase(ref _ps, SqlTrx))
      {
        return null;
      }

      if (Add(TerminalId, PlaySessionId,
              _ps.TransactionId, _ps.PlaySessionType, _ps.StandAloneSession,
              _ps.InitialBalance, _ps.Balance, _ps.TerminalName, _ps.AccountId, _ps.CardTrackData, _ps.m_has_cashin_amount, _ps.m_cancellable_amount))
      {
        return _ps;
      }

      return null;
    } // Reload

    //------------------------------------------------------------------------------
    // PURPOSE : Update a PlaySession from the DataTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Decimal InitialBalance
    //          - Decimal PlayedAmount
    //          - Decimal WonAmount
    //          - Decimal FinalBalance
    //
    //      - OUTPUT :
    //          - WCP_ResponseCodes ResponseCode
    //          - String ResponseCodeText
    //
    // RETURNS :
    //      - Boolean: If play session has been updated correctly.
    //
    public Boolean Update(Decimal InitialBalance,
                          Decimal PlayedAmount,
                          Decimal WonAmount,
                          Decimal FinalBalance,
                          out WCP_ResponseCodes ResponseCode,
                          out String ResponseCodeText)
    {
      String _filter;
      DataRow[] _dr_play_sessions;
      DataRow _play_session;

      ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
      ResponseCodeText = "";

      if (m_stand_alone_session)
      {
        return true;
      }

      try
      {
        // Check Final Balance
        if (InitialBalance - PlayedAmount + WonAmount != FinalBalance)
        {
          ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_BALANCE_MISMATCH;
          ResponseCodeText = "Balance mismatch";

          return false;
        }

        if (PlayedAmount < 0)
        {
          ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
          ResponseCodeText = "PlayedAmount less than 0";

          return false;
        }

        if (WonAmount < 0)
        {
          ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
          ResponseCodeText = "WonAmount less than 0";

          return false;
        }

        if (InitialBalance < 0)
        {
          ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
          ResponseCodeText = "InitialBalance less than 0";

          return false;
        }

        if (FinalBalance < 0)
        {
          ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
          ResponseCodeText = "FinalBalance less than 0";

          return false;
        }

        // Check Initial Balance
        if (m_balance != InitialBalance)
        {
          ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_BALANCE_MISMATCH;
          ResponseCodeText = "InitialBalance mismatch";

          return false;
        }

        m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

        try
        {
          if (m_dt_play_sessions == null)
          {
            ResponseCode = WCP_ResponseCodes.WCP_RC_ERROR;
            ResponseCodeText = "Cache not initialized";

            return false;
          }

          _filter = "TERMINAL_ID = " + m_terminal_id + " AND PLAY_SESSION_ID = " + m_play_session_id;
          _dr_play_sessions = m_dt_play_sessions.Select(_filter);

          if (_dr_play_sessions.Length == 0)
          {
            ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_VALID;
            ResponseCodeText = "Invalid Card Session";

            return false;
          }
          if (_dr_play_sessions.Length > 1)
          {
            Log.Warning("PlaySession.Update: Too many PlaySessions for TerminalId: " + m_terminal_id +
                        ", PlaySessionId: " + m_play_session_id + " . Count: " + _dr_play_sessions.Length + ".");
          }

          _play_session = _dr_play_sessions[0];
          _play_session[COLUMN_CURRENT_BALANCE] = FinalBalance;

          return true;
        }
        finally
        {
          m_rw_lock.ReleaseWriterLock();
        }
      }
      finally
      {
        if (ResponseCode != WCP_ResponseCodes.WCP_RC_OK)
        {
          Log.Error("PlaySession.Update: " + ResponseCode + " " + ResponseCodeText +
          " Terminal/PlaySession/m_balance/Initial/Played/Won/Final = " + m_terminal_id + "/" + m_play_session_id + "/" +
          m_balance + "/" + InitialBalance + "/" + PlayedAmount + "/" + WonAmount + "/" + FinalBalance + ".");
        }
      }
    } // Update

    public static Boolean UpdateAccount(Int64 PlaySessionId, Int64 AccountId, String TrackData)
    {
      String _filter;
      DataRow[] _dr_play_sessions;

      m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

      try
      {
        if (m_dt_play_sessions == null)
        {
          Log.Error("Not initialized. PlaySession.UpdateAccount PlaySession: " + PlaySessionId + ", AccountId: " + AccountId + ".");

          return false;
        }

        // Search PlaySession.
        _filter = "PLAY_SESSION_ID = " + PlaySessionId;
        _dr_play_sessions = m_dt_play_sessions.Select(_filter);

        if(_dr_play_sessions.Length > 0)
        {
          _dr_play_sessions[0][COLUMN_ACCOUNT_ID] = AccountId;
          _dr_play_sessions[0][COLUMN_TRACK_DATA] = TrackData;
          _dr_play_sessions[0].AcceptChanges();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: PlaySession.UpdateAccount PlaySession: " + PlaySessionId + ", AccountId: " + AccountId + ".");
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Rollback Updated PlaySession.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Decimal InitialBalance
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void Rollback(Decimal InitialBalance)
    {
      String _filter;
      DataRow[] _dr_play_sessions;
      DataRow _play_session;

      if (m_stand_alone_session)
      {
        return;
      }

      m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

      try
      {
        if (m_dt_play_sessions == null)
        {
          Log.Warning("PlaySession.Rollback: DataTable not initialized.");

          return;
        }

        _filter = "TERMINAL_ID = " + m_terminal_id + " AND PLAY_SESSION_ID = " + m_play_session_id;
        _dr_play_sessions = m_dt_play_sessions.Select(_filter);

        if (_dr_play_sessions.Length == 0)
        {
          Log.Warning("PlaySession.Rollback: Terminal not found. TerminalId: " + m_terminal_id + ", PlaySessionId: " + m_play_session_id);

          return;
        }

        if (_dr_play_sessions.Length > 1)
        {
          Log.Warning("PlaySession.Rollback: Too many PlaySessions for TerminalId: " + m_terminal_id +
                      ", PlaySessionId: " + m_play_session_id + " . Count: " + _dr_play_sessions.Length + ".");
        }

        _play_session = _dr_play_sessions[0];
        _play_session[COLUMN_CURRENT_BALANCE] = InitialBalance;

      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }

    } // Rollback

    //------------------------------------------------------------------------------
    // PURPOSE : Get a PlaySession from the DataTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32   TerminalId
    //          - Int64   PlaySessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - PlaySession
    //
    public static PlaySession Get(Int32 TerminalId, Int64 PlaySessionId, SqlTransaction Trx)
    {
      PlaySession _ps;
      String _filter;
      DataRow[] _dr_play_sessions;
      DataRow _play_session;

      _ps = new PlaySession(TerminalId, PlaySessionId);

      // Special Case: PlaySessionId == 0, indicates Terminal reports without a card. This case is not cached, so
      // return an initialized object.
      if (PlaySessionId == 0)
      {
        return _ps;
      }

      m_rw_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);

      try
      {
        if (m_dt_play_sessions == null)
        {
          Log.Error("Not initialized. PlaySession.Get Terminal: " + TerminalId + ", PlaySession: " + PlaySessionId + ".");

          return null;
        }

        _filter = "TERMINAL_ID = " + TerminalId + " AND PLAY_SESSION_ID = " + PlaySessionId;
        _dr_play_sessions = m_dt_play_sessions.Select(_filter);

        if (_dr_play_sessions.Length > 1)
        {
          Log.Warning("PlaySession.Get: Too many PlaySessions for TerminalId: " + TerminalId +
                      ", PlaySessionId: " + PlaySessionId + " . Count: " + _dr_play_sessions.Length + ".");
        }

        if (_dr_play_sessions.Length >= 1)
        {
          _play_session = _dr_play_sessions[0];

          _ps.m_transaction_id = (Int64)_play_session[COLUMN_TRANSACTION_ID];

          _ps.m_play_session_type = (PlaySessionType)_play_session[COLUMN_PLAY_SESSION_TYPE];
          _ps.m_stand_alone_session = (Boolean)_play_session[COLUMN_STAND_ALONE_SESSION];
          _ps.m_initial_balance = (Decimal)_play_session[COLUMN_INITIAL_BALANCE];
          _ps.m_balance = (Decimal)_play_session[COLUMN_CURRENT_BALANCE];

          _ps.m_terminal_name = (String)_play_session[COLUMN_TERMINAL_NAME];
          _ps.m_account_id = (Int64)_play_session[COLUMN_ACCOUNT_ID];
          _ps.m_card_track_data = (String)_play_session[COLUMN_TRACK_DATA];
          _ps.m_status = PlaySessionStatus.Opened;
          _ps.m_has_cashin_amount = (((Int32)_play_session[COLUMN_HAS_CASHIN_AMOUNT]) == 1);
          _ps.m_cancellable_amount = ((Decimal)_play_session[COLUMN_CANCELLABLE_AMOUNT]);

          return _ps;
        }
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }

      //
      // ACC 19-JUL-2010 Get PlaySession from DataBase
      //
      ReadPlaySessionFromDatabase(ref _ps, Trx);

      return _ps;

    } // Get

    //------------------------------------------------------------------------------
    // PURPOSE : PlaySession is active ?
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64   PlaySessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - TRUE: is active
    //      - FALSE: otherwise
    //
    public static Boolean IsActive(Int64 PlaySessionId)
    {
      String _filter;
      DataRow[] _dr_play_sessions;

      if (PlaySessionId == 0)
      {
        return false;
      }

      m_rw_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);

      try
      {
        if (m_dt_play_sessions == null)
        {
          Log.Error("Not initialized. PlaySession.IsActive (PlaySession=" + PlaySessionId + ").");

          return false;
        }

        _filter = "PLAY_SESSION_ID = " + PlaySessionId;
        _dr_play_sessions = m_dt_play_sessions.Select(_filter);



        if (_dr_play_sessions.Length >= 1)
        {
          if (_dr_play_sessions.Length > 1)
          {
            Log.Warning("Too many PlaySessions, Count: " + _dr_play_sessions.Length + ". PlaySession.IsActive (PlaySessionId=" + PlaySessionId + ").");
          }

          return true;
        }
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }

      return false;

    } // IsActive

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows related to the TerminalId from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean UnloadByTerminalId(Int32 TerminalId)
    {
      String _filter;

      _filter = "TERMINAL_ID = " + TerminalId;

      return Unload(_filter);
    } // UnloadByTerminalId

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows related to the PlaySessionId from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 PlaySessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean Unload(Int64 PlaySessionId)
    {
      String _filter;

      _filter = "PLAY_SESSION_ID = " + PlaySessionId;

      return Unload(_filter);
    } // Unload

    //------------------------------------------------------------------------------
    // PURPOSE : Load last Active PlaySession for the TerminalId to PlaySession Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //         - TerminalId
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void LoadLastPlaySession(int TerminalId, Boolean ActiveSession, out Int64 PlaySessionId, SqlTransaction SqlTrx)
    {
      String _sql_query;
      SqlCommand _sql_command;
      SqlDataReader _reader;

      Int64 _play_session_id;
      Int64 _transaction_id;
      PlaySessionType _play_session_type;
      Boolean _stand_alone_session;
      Decimal _initial_balance;
      Decimal _balance;
      String _terminal_name;
      Int64 _account_id;
      String _card_track_data;
      Boolean _has_cashin_amount;
      Decimal _cancellable_amount;

      PlaySessionId = 0;

      //20-SEP-2017 JMM    Fixed Bug WIGOS-5309 [Ticket #8698] Sala Golden Lion - 048 - Lentitud en Recargas 
      _sql_query = "EXEC SP_LastPlaySession @pTerminalId, @pActive";

      _sql_command = new SqlCommand(_sql_query);
      _sql_command.Connection = SqlTrx.Connection;
      _sql_command.Transaction = SqlTrx;
      _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
      _sql_command.Parameters.Add("@pActive", SqlDbType.Bit).Value = ActiveSession;

      _reader = null;

      try
      {
        _reader = _sql_command.ExecuteReader();
        if (_reader.Read())
        {
          _play_session_id = _reader.GetInt64(0);
          _transaction_id = _reader.IsDBNull(1) ? 0 : _reader.GetInt64(1);
          _play_session_type = (PlaySessionType)_reader.GetInt32(2);
          _stand_alone_session = _reader.GetBoolean(3);
          _initial_balance = _reader.IsDBNull(4) ? 0 : _reader.GetDecimal(4);
          _balance = _reader.IsDBNull(5) ? 0 : _reader.GetDecimal(5);
          _terminal_name = _reader.IsDBNull(6) ? "" : _reader.GetString(6);
          _account_id = _reader.IsDBNull(7) ? 0 : _reader.GetInt64(7);
          _card_track_data = _reader.IsDBNull(8) ? "" : _reader.GetString(8);
          _has_cashin_amount = _reader.IsDBNull(9) ? false : ((_reader.GetInt32(9)) == 1);
          _cancellable_amount = _reader.IsDBNull(10) ? 0 : _reader.GetDecimal(10);
          if (!_stand_alone_session)
          {
            _balance = _reader.IsDBNull(11) ? 0 : _reader.GetDecimal(11);
            _balance = Math.Max(_balance, 0);
          }

          PlaySession.Add(TerminalId,
                          _play_session_id,
                          _transaction_id,
                          _play_session_type,
                          _stand_alone_session,
                          _initial_balance,
                          _balance,
                          _terminal_name,
                          _account_id,
                          _card_track_data,
                          _has_cashin_amount,
                          _cancellable_amount);

          PlaySessionId = _play_session_id;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
          _reader.Dispose();
          _reader = null;
        }
      }
    } // LoadLastActivePlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : Load last Active PlaySession for the TerminalId to PlaySession Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //         - TerminalId
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void LoadLastActivePlaySession(int TerminalId, SqlTransaction SqlTrx)
    {
      Int64 _play_session_id;

      LoadLastPlaySession(TerminalId, true, out _play_session_id, SqlTrx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Read PlaySession from database from terminal string name
    //
    //  PARAMS :
    //      - INPUT :
    //         - TerminalId string 
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If PlaySession has been read correctly. False: Otherwise.
    //
    //   NOTES :
    //
    private static Boolean ReadPlaySessionFromDatabase(ref PlaySession PlaySession, SqlTransaction SqlTrx)
    {
      String _sql_txt;

      try
      {
        // DDM 10-MAY-2012 : Add field total cash 
        _sql_txt = "  SELECT   PS_WCP_TRANSACTION_ID " +
                           " , PS_TYPE " +
                           " , PS_STAND_ALONE " +
                           " , PS_INITIAL_BALANCE " +
                           " , PS_FINAL_BALANCE " +
                           " , TE_NAME " +
                           " , PS_ACCOUNT_ID " +
                           " , AC_TRACK_DATA " +
                           " , PS_STATUS " +
                           " , CASE WHEN ( PS_CASH_IN > 0 ) THEN 1 ELSE 0 END AS HAS_CASHIN_AMOUNT" +
                           " , ISNULL (PS_CANCELLABLE_AMOUNT, 0) PS_CANCELLABLE_AMOUNT " +
                           " , (PS_INITIAL_BALANCE + PS_CASH_IN) PS_TOTAL_CASH_IN " +
                      " FROM   ACCOUNTS " +
                           " , PLAY_SESSIONS " +
                           " , TERMINALS " +
                     " WHERE   PS_PLAY_SESSION_ID  = @PlaySessionId " +
                       " AND   PS_TERMINAL_ID      = @TerminalId " +
                       " AND   PS_TERMINAL_ID      = TE_TERMINAL_ID " +
                       " AND   PS_ACCOUNT_ID       = AC_ACCOUNT_ID " +
                       " ORDER BY PS_PLAY_SESSION_ID DESC ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@PlaySessionId", SqlDbType.BigInt).Value = PlaySession.PlaySessionId;
          _sql_cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = PlaySession.TerminalId;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              PlaySession.m_transaction_id = _sql_reader.IsDBNull(0) ? 0 : _sql_reader.GetInt64(0);
              PlaySession.m_play_session_type = (PlaySessionType)_sql_reader.GetInt32(1);
              PlaySession.m_stand_alone_session = _sql_reader.GetBoolean(2);
              PlaySession.m_initial_balance = _sql_reader.IsDBNull(3) ? 0 : _sql_reader.GetDecimal(3);
              PlaySession.m_balance = _sql_reader.IsDBNull(4) ? 0 : _sql_reader.GetDecimal(4);
              PlaySession.m_terminal_name = _sql_reader.IsDBNull(5) ? "" : _sql_reader.GetString(5);
              PlaySession.m_account_id = _sql_reader.IsDBNull(6) ? 0 : _sql_reader.GetInt64(6);
              PlaySession.m_card_track_data = _sql_reader.IsDBNull(7) ? "" : _sql_reader.GetString(7);
              PlaySession.m_status = (PlaySessionStatus)_sql_reader.GetInt32(8);
              PlaySession.m_has_cashin_amount = ((_sql_reader.GetInt32(9)) == 1);
              PlaySession.m_cancellable_amount = _sql_reader.IsDBNull(10) ? 0 : _sql_reader.GetDecimal(10);
              PlaySession.m_total_cash_in = _sql_reader.IsDBNull(11) ? 0 : _sql_reader.GetDecimal(11);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadPlaySessionFromDatabase

    //------------------------------------------------------------------------------
    // PURPOSE : Check if there is an open play session for a particular terminal
    //
    //  PARAMS :
    //      - INPUT :
    //         - TerminalId
    //         - SqlTrx
    //
    //      - OUTPUT :
    //        - AccountId (from play-session)
    //
    // RETURNS :
    //      - True: If PlaySession has been read correctly. False: Otherwise.
    //
    //   NOTES :
    //
    public static Int64 GetOpenSessionIdForTerminal(Int32 TerminalId, out Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _string_builder;
      Int64 _play_session_id;

      AccountId = 0;
      _string_builder = new StringBuilder();

      try
      {
        _string_builder.AppendLine(" SELECT   PS_PLAY_SESSION_ID ");
        _string_builder.AppendLine("        , PS_ACCOUNT_ID ");
        _string_builder.AppendLine("   FROM   PLAY_SESSIONS ");
        _string_builder.AppendLine("        , TERMINALS ");
        _string_builder.AppendLine("  WHERE   PS_TERMINAL_ID  = TE_TERMINAL_ID ");
        _string_builder.AppendLine("    AND   TE_TERMINAL_ID  = @pTerminalId ");
        _string_builder.AppendLine("    AND   PS_STATUS       = @pPlaySessionStatus ");

        using (SqlCommand _sql_cmd = new SqlCommand(_string_builder.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pPlaySessionStatus", SqlDbType.Int).Value = PlaySessionStatus.Opened;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              _play_session_id = _sql_reader.IsDBNull(0) ? 0 : _sql_reader.GetInt64(0);
              AccountId = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetInt64(1);

              return _play_session_id;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return 0;
    } // GetOpenSessionIdForTerminal



    //------------------------------------------------------------------------------
    // PURPOSE :   Is Cancellable PlaySession.
    //
    //  PARAMS :
    //      - INPUT :
    //         - PlaySessionId
    //         - RequestTransactionId
    //         - CancellableBalance
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If PlaySession Ok. False: Otherwise.
    //
    //   NOTES :
    //
    public static Boolean SetCancellableAmount(Int64 PlaySessionId,
                                               Int64 RequestTransactionId,
                                               Decimal CancellableBalance,
                                               SqlTransaction SqlTrx)
    {
      String _sql;
      SqlCommand _cmd;

      _sql = "";
      _sql += "UPDATE   PLAY_SESSIONS";
      _sql += "   SET   PS_CANCELLABLE_AMOUNT = @pAmount ";
      _sql += "     ,   PS_WCP_TRANSACTION_ID = @pTransactionId ";
      _sql += " WHERE   PS_PLAY_SESSION_ID = @pPlaySessionID";

      _cmd = new SqlCommand(_sql);
      _cmd.Connection = SqlTrx.Connection;
      _cmd.Transaction = SqlTrx;

      _cmd.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;
      _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = RequestTransactionId;
      _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = CancellableBalance;

      if (_cmd.ExecuteNonQuery() != 1)
      {
        return false;
      }

      return true;

    } // SetCancellableAmount

    //------------------------------------------------------------------------------
    // PURPOSE : Is Cancellable PlaySession.
    //
    //  PARAMS :
    //      - INPUT :
    //         - TerminalId
    //         - RequestTransactionId
    //         - PlaySessionId
    //         - SqlTrx
    //
    //      - OUTPUT :
    //         - AccountId
    //         - CancellableAmount
    //         - HasCashInAmount
    //
    // RETURNS :
    //      - True: If PlaySession Is Cancellable. False: Otherwise.
    //
    //   NOTES :
    //
    public static Boolean IsCancellable(Int32 TerminalId,
                                        Int64 RequestTransactionId,
                                        Int64 PlaySessionId,
                                        out Int64 AccountId,
                                        out Decimal CancellableAmount,
                                        out Boolean HasCashInAmount,
                                        SqlTransaction SqlTrx)
    {
      PlaySession _ps;

      AccountId = 0;
      CancellableAmount = 0;
      HasCashInAmount = false;

      // Check Session Data
      if (PlaySessionId == 0)
      {
        return false;
      }


      // We are sure the PlaySessionId is in Cache and Active (previously checked).
      // Reload the PlaySession from DB and save it in the Cache.
      _ps = PlaySession.Reload(TerminalId, PlaySessionId, SqlTrx);

      if (_ps == null)
      {
        return false;
      }

      if (_ps.Status != PlaySessionStatus.Opened)
      {
        return false;
      }


      if (_ps.TransactionId != RequestTransactionId)
      {
        return false;
      }

      AccountId = _ps.AccountId;
      CancellableAmount = _ps.CancellableAmount;
      HasCashInAmount = _ps.HasCashInAmount;

      return true;
    } // IsCancellable

    //------------------------------------------------------------------------------
    // PURPOSE: Closes a Play Session
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId
    //          - AccountID
    //          - CardBalance
    //          - CancellableAmount
    //          - HasCashInAmount
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Nothing
    //        
    //   NOTES :
    //
    public static Boolean CancelPlaySession(Int64 PlaySessionId,
                                           Int64 AccountID,
                                           Decimal CardBalance,
                                           Decimal CancellableAmount,
                                           Boolean HasCashInAmount,
                                           SqlTransaction SqlTrx)
    {
      SqlCommand sql_command;
      String sql_str;

      //
      // In Stand Alone mode the CancellableAmount is equal to CardBalance.
      //

      // If contains CashIn amount, the session is left open and the amount is subtracted from cashin.
      // The Cancellable amount always is reset.
      sql_str = "";
      sql_str += "UPDATE PLAY_SESSIONS SET   PS_CASH_IN             = CASE WHEN PS_CASH_IN > 0 THEN PS_CASH_IN - @pCancellableAmount ELSE PS_CASH_IN END ";
      sql_str += "                       ,   PS_CANCELLABLE_AMOUNT  = 0 ";
      sql_str += "                   WHERE   PS_PLAY_SESSION_ID     = @pPlaySessionID";
      sql_str += "                     AND   PS_STATUS              = @pOldStatus ";
      sql_str += "                     AND   PS_CANCELLABLE_AMOUNT  = @pCancellableAmount";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = SqlTrx.Connection;
      sql_command.Transaction = SqlTrx;

      sql_command.Parameters.Add("@pCancellableAmount", SqlDbType.Money).Value = CancellableAmount;
      sql_command.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;
      sql_command.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Opened;

      if (sql_command.ExecuteNonQuery() != 1)
      {
        return false;
      }

      // If contains cash-in amount (cash added in "Bola Extra") the session is left open.
      if (HasCashInAmount)
      {
        // Add the received amount to the current balance
        WCP_BusinessLogic.DB_AddToAccountBalance(AccountID, CancellableAmount, SqlTrx);

        return true;
      }

      //
      // Close Session
      //
      WCP_BusinessLogic.DB_PlaySessionClose(PlaySessionId,
                                            AccountID,
                                            CardBalance,
                                            SqlTrx);

      return true;

    } // CancelPlaySession

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows based on Filter from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Filter
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static Boolean Unload(String Filter)
    {
      DataRow[] _play_session_rows;

      m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

      try
      {

        if (m_dt_play_sessions == null)
        {
          Log.Error("Not initialized. PlaySession.Unload Filter " + Filter + ".");
          return false;
        }

        _play_session_rows = m_dt_play_sessions.Select(Filter);
        foreach (DataRow _row in _play_session_rows)
        {
          m_dt_play_sessions.Rows.Remove(_row);
        }

        return true;
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }
    } // Unload

    #endregion // Private Methods

  } // PlaySession

} // WSI.WCP
