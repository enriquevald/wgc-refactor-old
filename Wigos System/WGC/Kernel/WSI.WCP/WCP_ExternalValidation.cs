﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ExternalValidation.cs
// 
//   DESCRIPTION: Thread that registers the External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 01-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
//------------------------------------------------------------------------------
//
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.Common.ExternalPaymentAndSaleValidation;

namespace WSI.WCP
{
  public class WCP_ExternalValidation
  {
    #region Members
    private static Thread m_thread;
    #endregion

    #region "Public Methods"
    /// <summary>
    /// Creates and starts the thread 
    /// </summary>
    public static void Init()
    {
      m_thread = new Thread(ExternalValidationProcessThread);
      m_thread.Name = "ExternalValidationProcessThread";

      m_thread.Start();
    } // Init
    #endregion

    #region "Private Methods"
    /// <summary>
    /// Thread that processes the External Validation records
    /// </summary>
    private static void ExternalValidationProcessThread()
    {
      Int32 _wait_time;
      Int32 _number_of_records_to_process;

      _wait_time = (1000 * GetWaitSecondsDefault());

      while (true)
      {
        Thread.Sleep(_wait_time);

        // Set default wait time
        _wait_time = (1000 * GetWaitSeconds());

        if (!Services.IsPrincipal("WCP"))
        {
          continue;
        }

        if (!ExternalPaymentAndSaleValidationCommon.ExternalPaymentValidationEnabled())
        {
          // If the evo functionality is not enabled we don't continue
          continue;
        }

        if (!ExternalPaymentAndSaleValidationCommon.ExpedThreadEnabled)
        {
          // If the patterns search thread is not enabled we don't continue
          continue;
        }

        _number_of_records_to_process = ExternalPaymentAndSaleValidationCommon.ExpedThreadNumberOfRecordsToProcess;

        try
        {
          ExternalPaymentAndSaleValidationCommon.ExternalPaymentValidationProcessPendingTransactions(_number_of_records_to_process);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        } // try
      } // while
    } // ExternalValidationProcessThread

    /// <summary>
    /// Get default wait seconds for the Thread
    /// </summary>
    /// <returns></returns>
    private static int GetWaitSecondsDefault()
    {
      return ExternalPaymentAndSaleValidationCommon.ExpedThreadWaitSecondsDefault;
    }

    /// <summary>
    /// Get wait seconds for the Thread
    /// </summary>
    /// <returns></returns>
    private static int GetWaitSeconds()
    {
      return ExternalPaymentAndSaleValidationCommon.ExpedThreadWaitSeconds;
    }
    #endregion
  }
}
