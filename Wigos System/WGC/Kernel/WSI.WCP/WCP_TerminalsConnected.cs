//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TerminalsConnected.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 04-NOV-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-NOV-2010 RCI    First release.
// 25-MAR-2015 FOS    TASK727:Add master_id in table Terminals_connected
// 05-NOV-2015 MPO    Bug 6186: Sol: Added Terminal Offline in terminal connected
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Threading;

namespace WSI.WCP
{
  public static class WCP_TerminalsConnected
  {
    static Thread m_thread;

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_TerminalsConnected class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      m_thread = new Thread(TerminalsConnectedThread);
      m_thread.Name = "TerminalsConnected";
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for thread starts
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Start()
    {
      // Thread starts
      m_thread.Start();
    }

    #endregion // Public Functions

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static private void TerminalsConnectedThread()
    {
      SqlConnection _sql_conn;
      Int32 _wait_hint;

      _wait_hint = 0;

      while (true)
      {
        System.Threading.Thread.Sleep(_wait_hint);
        // In case of error, wait 1 minute.
        _wait_hint = 1 * 60 * 1000;

        _sql_conn = null;

        try
        {
          //
          // Connect to DB
          //
          _sql_conn = WGDB.Connection();

          if (_sql_conn == null)
          {
            continue;
          }

          // Check connection states
          if (_sql_conn.State != ConnectionState.Open)
          {
            if (_sql_conn.State != ConnectionState.Broken)
            {
              _sql_conn.Close();
            }

            _sql_conn.Dispose();
            _sql_conn = null;

            continue;
          }

          //
          // Generate Connected Terminals: WIN, 3GS and SAS-HOST.
          //
          if (GenerateTerminalsConnected(_sql_conn))
          {
            // Success, wait 15 minutes.
            _wait_hint = 15 * 60 * 1000;
          }
          else
          {
            // In case of error, wait 1 minute.
            _wait_hint = 1 * 60 * 1000;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          if (_sql_conn != null)
          {
            _sql_conn.Close();
            _sql_conn = null;
          }
        }

      } // while

    } // TerminalsConnectedThread

    //------------------------------------------------------------------------------
    // PURPOSE : Generate Connected Terminals
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Generated ok. False: Otherwise.
    //
    private static Boolean GenerateTerminalsConnected(SqlConnection SqlConn)
    {
      DataTable _sql_dt_terminals_in_session;
      DataTable _sql_dt_active_terminals;
      DataRow[] _dr_active_and_in_session;
      DateTime _current_date;
      SqlDataAdapter _sql_da;
      SqlCommand _sql_cmd;
      StringBuilder _sql_sb;
      String _str_in;
      TerminalTypes _type;

      _current_date = Misc.TodayOpening().Date;

      _sql_dt_terminals_in_session = Common.BatchUpdate.TerminalSession.GetOpenedTerminalSessions();

      //
      // Select actual active terminals.
      //
      _sql_dt_active_terminals = new DataTable("ACTIVE_TERMINALS");
      _sql_da = new SqlDataAdapter();
      _sql_cmd = new SqlCommand();
      _sql_cmd.Connection = SqlConn;
      _sql_da.SelectCommand = _sql_cmd;

      _sql_sb = new StringBuilder();
      _sql_sb.AppendLine("SELECT   TE_TERMINAL_ID ");
      _sql_sb.AppendLine("       , TE_TERMINAL_TYPE ");
      _sql_sb.AppendLine("       , TE_MASTER_ID ");
      _sql_sb.AppendLine("  FROM   TERMINALS ");
      _sql_sb.AppendLine(" WHERE   TE_STATUS      = @pStatusActive ");  // Terminal is Active
      _sql_sb.AppendLine("   AND   TE_TYPE        = @pTypeGamingTerminal ");
      _sql_sb.AppendLine("   AND   TE_MASTER_ID NOT IN ( ");     // Terminal already NOT connected
      _sql_sb.AppendLine("                                 SELECT   TC_MASTER_ID ");
      _sql_sb.AppendLine("                                   FROM   TERMINALS_CONNECTED ");
      _sql_sb.AppendLine("                                  WHERE   TC_DATE        = @pCurrentDate ");
      _sql_sb.AppendLine("                                    AND   TC_STATUS      = @pStatusActive ");
      _sql_sb.AppendLine("                                    AND   TC_CONNECTED   = @pConnected ");
      _sql_sb.AppendLine("                               ) ");

      // Include GamingTerminalTypes in the SELECT.
      _str_in = Misc.GamingTerminalTypeListToString();
      if (_str_in.Length > 0)
      {
        _str_in = "   AND   TE_TERMINAL_TYPE IN ( " + _str_in + " )";
        _sql_sb.AppendLine(_str_in);
      }

      _sql_cmd.CommandText = _sql_sb.ToString();
      _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = TerminalStatus.ACTIVE;
      _sql_cmd.Parameters.Add("@pTypeGamingTerminal", SqlDbType.Int).Value = WCP_TerminalType.GamingTerminal;
      _sql_cmd.Parameters.Add("@pCurrentDate", SqlDbType.DateTime).Value = _current_date;
      _sql_cmd.Parameters.Add("@pConnected", SqlDbType.Bit).Value = 1;  // Connected

      _sql_da.Fill(_sql_dt_active_terminals);

      _sql_cmd.Dispose();
      _sql_da.Dispose();

      foreach (DataRow _dr_active in _sql_dt_active_terminals.Rows)
      {
        _type = (TerminalTypes)((Int16)_dr_active["TE_TERMINAL_TYPE"]);
        if (_type != TerminalTypes.OFFLINE && _type != TerminalTypes.T3GS && _type != TerminalTypes.CASHDESK_DRAW && _type != TerminalTypes.GAMEGATEWAY && _type != TerminalTypes.PARIPLAY)
        {
          _dr_active_and_in_session = _sql_dt_terminals_in_session.Select("TERMINAL_ID = " + _dr_active["TE_TERMINAL_ID"]);
          if (_dr_active_and_in_session.Length == 0)
          {
            _dr_active.Delete();
          }
        }
      }

      _sql_dt_active_terminals.AcceptChanges();

      return InsertTerminalsConnected(SqlConn, _sql_dt_active_terminals, _current_date);
    } // GenerateTerminalsConnected

    //------------------------------------------------------------------------------
    // PURPOSE : Insert connected terminals to table TERMINALS_CONNECTED
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConnection SqlConn
    //          - DataTable Terminals
    //          - DateTime CurrentDate
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Inserted ok. False: Otherwise.
    //
    private static Boolean InsertTerminalsConnected(SqlConnection SqlConn,
                                                    DataTable Terminals,
                                                    DateTime CurrentDate)
    {
      SqlTransaction _sql_trx;
      SqlCommand _sql_cmd;
      SqlDataAdapter _sql_da;
      StringBuilder _sql_sb;

      if (Terminals == null)
      {
        // Nothing to insert.
        return true;
      }
      if (Terminals.Rows.Count == 0)
      {
        // Nothing to insert.
        return true;
      }

      // Mark rows as added.
      foreach (DataRow _dr_terminal in Terminals.Rows)
      {
        try { _dr_terminal.SetAdded(); }
        catch { }
      }

      _sql_da = new SqlDataAdapter();
      _sql_cmd = new SqlCommand();

      _sql_da.InsertCommand = _sql_cmd;
      _sql_da.ContinueUpdateOnError = true;
      _sql_da.UpdateBatchSize = 500;
      _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

      _sql_sb = new StringBuilder();
      _sql_sb.AppendLine("IF NOT EXISTS ");
      _sql_sb.AppendLine("( ");
      _sql_sb.AppendLine("   SELECT   1 ");
      _sql_sb.AppendLine("     FROM   TERMINALS_CONNECTED ");
      _sql_sb.AppendLine("    WHERE   TC_DATE        = @pWorkingDay ");
      _sql_sb.AppendLine("      AND   TC_MASTER_ID   = @pMasterId ");
      _sql_sb.AppendLine(") ");
      _sql_sb.AppendLine("   INSERT INTO TERMINALS_CONNECTED ( TC_MASTER_ID");
      _sql_sb.AppendLine("                                   , TC_DATE ");
      _sql_sb.AppendLine("                                   , TC_TERMINAL_ID ");
      _sql_sb.AppendLine("                                   , TC_STATUS ");
      _sql_sb.AppendLine("                                   , TC_CONNECTED ");
      _sql_sb.AppendLine("                                    ) ");
      _sql_sb.AppendLine("                            VALUES ( @pMasterId");
      _sql_sb.AppendLine("                                   , @pWorkingDay");
      _sql_sb.AppendLine("                                   , @pTerminalId");
      _sql_sb.AppendLine("                                   , @pStatusActive");
      _sql_sb.AppendLine("                                   , @pConnected");
      _sql_sb.AppendLine("                                   ) ");
      _sql_sb.AppendLine("ELSE ");
      _sql_sb.AppendLine("   UPDATE   TERMINALS_CONNECTED ");
      _sql_sb.AppendLine("      SET   TC_STATUS      = @pStatusActive ");
      _sql_sb.AppendLine("          , TC_CONNECTED   = @pConnected ");
      _sql_sb.AppendLine("    WHERE   TC_DATE        = @pWorkingDay ");
      _sql_sb.AppendLine("      AND   TC_MASTER_ID   = @pMasterId ");

      _sql_cmd.CommandText = _sql_sb.ToString();
      _sql_cmd.Parameters.Add("@pMasterId", SqlDbType.Int).SourceColumn = "TE_MASTER_ID";
      _sql_cmd.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = CurrentDate;
      _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TE_TERMINAL_ID";
      _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = TerminalStatus.ACTIVE;
      _sql_cmd.Parameters.Add("@pConnected", SqlDbType.Bit).Value = 1;  // Connected

      _sql_trx = SqlConn.BeginTransaction();

      _sql_cmd.Connection = SqlConn;
      _sql_cmd.Transaction = _sql_trx;

      _sql_da.Update(Terminals);

      _sql_trx.Commit();

      _sql_cmd.Dispose();
      _sql_da.Dispose();

      if (Terminals.HasErrors)
      {
        Log.Error("TerminalsConnected: " + Terminals.GetErrors()[0].RowError);

        return false;
      }

      return true;
    } // InsertTerminalsConnected

    #endregion // Private Functions

  } // WCP_TerminalsConnected

} // WSI.WCP
