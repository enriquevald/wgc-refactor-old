//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_BU_MachineMultiDenomInfo.cs
// 
//   DESCRIPTION: WCP_BU_MachineMultiDenomInfo class
// 
//        AUTHOR: Marcos Piedra
// 
// CREATION DATE: 29-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-JUL-2015 MPO    First release (based on machine meters).
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;
using WSI.WCP;

namespace WSI.WCP
{
  public static class WCP_BU_MachineMultiDenomInfo
  {

    #region Constants

    private const Int16 COLUMN_TERMINAL_ID = 0;
    private const Int16 COLUMN_DENOMINATION = 1;
    private const Int16 COLUMN_WCP_SEQUENCE_ID = 2;
    private const Int16 COLUMN_LAST_REPORTED = 3;
    private const Int16 COLUMN_PLAYED_AMOUNT = 4;
    private const Int16 COLUMN_WON_AMOUNT = 5;
    private const Int16 COLUMN_JACKPOT_AMOUNT = 6;
    private const Int16 COLUMN_PLAYED_COUNT = 7;
    private const Int16 COLUMN_WON_COUNT = 8;
    private const Int16 COLUMN_DELTA_PLAYED_AMOUNT = 9;
    private const Int16 COLUMN_DELTA_WON_AMOUNT = 10;
    private const Int16 COLUMN_DELTA_JACKPOT_AMOUNT = 11;
    private const Int16 COLUMN_DELTA_PLAYED_COUNT = 12;
    private const Int16 COLUMN_DELTA_WON_COUNT = 13;
    private const Int16 COLUMN_MAX_PLAYED_AMOUNT = 14;
    private const Int16 COLUMN_MAX_WON_AMOUNT = 15;
    private const Int16 COLUMN_MAX_JACKPOT_AMOUNT = 16;
    private const Int16 COLUMN_MAX_PLAYED_COUNT = 17;
    private const Int16 COLUMN_MAX_WON_COUNT = 18;
    private const Int16 COLUMN_OLD_PLAYED_COUNT = 19;
    private const Int16 COLUMN_OLD_PLAYED_AMOUNT = 20;
    private const Int16 COLUMN_OLD_WON_COUNT = 21;
    private const Int16 COLUMN_OLD_WON_AMOUNT = 22;
    private const Int16 COLUMN_OLD_JACKPOT_AMOUNT = 23;
    private const Int16 COLUMN_RESPONSE_CODE = 24;

    #endregion // Constants

    #region Attributes

    private static DataTable m_dt_machine_denom_meters = null;

    private static SqlDataAdapter m_adap_machine_denom_meters = null;
    private static SqlDataAdapter m_adap_mdm_select = null;

    private static ReaderWriterLock m_rw_lock_queue = new ReaderWriterLock();
    private static ArrayList m_unload_queue = new ArrayList(10);

    public static Int32 m_time_num_all = 0;
    public static Int32 m_time_num_loads = 0;

    public static Int64 m_time_all = 0;
    public static Int64 m_time_update_session = 0;
    public static Int64 m_time_gets = 0;
    public static Int64 m_time_updates = 0;
    public static Int64 m_time_updateDB = 0;
    public static Int64 m_time_commit = 0;
    public static Int64 m_time_response = 0;
    public static Int64 m_time_task_process = 0;
    public static Int64 m_time_begin_trx = 0;
    public static Int64 m_time_unload = 0;
    public static Int64 m_time_loads = 0;
    public static Int64 m_time_inserts = 0;

    #endregion // Attributes

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init attributes of the static class.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      SqlCommand _sql_cmd_select;
      SqlCommand _sql_cmd_insert;
      SqlCommand _sql_cmd_update;
      StringBuilder _sb_query;

      _sql_cmd_insert = new SqlCommand();
      _sql_cmd_update = new SqlCommand();

      // Already init.
      if (m_dt_machine_denom_meters != null)
      {
        return;
      }

      // Create DataTable Columns
      m_dt_machine_denom_meters = new DataTable("MACHINE_DENOM_METERS");

      m_dt_machine_denom_meters.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      m_dt_machine_denom_meters.Columns.Add("DENOMINATION", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("WCP_SEQUENCE_ID", Type.GetType("System.Int64"));
      m_dt_machine_denom_meters.Columns.Add("LAST_REPORTED", Type.GetType("System.DateTime"));
      m_dt_machine_denom_meters.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_denom_meters.Columns.Add("WON_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_denom_meters.Columns.Add("DELTA_PLAYED_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_machine_denom_meters.Columns.Add("DELTA_WON_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_machine_denom_meters.Columns.Add("DELTA_JACKPOT_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_machine_denom_meters.Columns.Add("DELTA_PLAYED_COUNT", Type.GetType("System.Int64")).DefaultValue = 0;
      m_dt_machine_denom_meters.Columns.Add("DELTA_WON_COUNT", Type.GetType("System.Int64")).DefaultValue = 0;
      m_dt_machine_denom_meters.Columns.Add("MAX_PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("MAX_WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("MAX_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("MAX_PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_denom_meters.Columns.Add("MAX_WON_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_denom_meters.Columns.Add("OLD_PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_denom_meters.Columns.Add("OLD_PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("OLD_WON_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_denom_meters.Columns.Add("OLD_WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("OLD_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_denom_meters.Columns.Add("RESPONSE_CODE", Type.GetType("System.Object"));

      // Primary Key for DataTable MachineDenomMeters.
      m_dt_machine_denom_meters.PrimaryKey = new DataColumn[] { m_dt_machine_denom_meters.Columns["TERMINAL_ID"] , 
                                                                m_dt_machine_denom_meters.Columns["DENOMINATION"] };

      // Create SelectCommand for MACHINE_METERS, and create SqlDataAdapter ONLY for Select.
      _sb_query = new StringBuilder();
      _sb_query.AppendLine(" SELECT   MDM_TERMINAL_ID ");
      _sb_query.AppendLine("        , MDM_DENOMINATION ");
      _sb_query.AppendLine("        , MDM_WCP_SEQUENCE_ID ");
      _sb_query.AppendLine("        , MDM_LAST_REPORTED ");
      _sb_query.AppendLine("        , MDM_PLAYED_AMOUNT ");
      _sb_query.AppendLine("        , MDM_WON_AMOUNT ");
      _sb_query.AppendLine("        , MDM_JACKPOT_AMOUNT ");
      _sb_query.AppendLine("        , MDM_PLAYED_COUNT ");
      _sb_query.AppendLine("        , MDM_WON_COUNT ");
      _sb_query.AppendLine("        , MDM_DELTA_PLAYED_AMOUNT ");
      _sb_query.AppendLine("        , MDM_DELTA_WON_AMOUNT ");
      _sb_query.AppendLine("        , MDM_DELTA_JACKPOT_AMOUNT ");
      _sb_query.AppendLine("        , MDM_DELTA_PLAYED_COUNT ");
      _sb_query.AppendLine("        , MDM_DELTA_WON_COUNT ");
      _sb_query.AppendLine("        , MDM_MAX_PLAYED_AMOUNT ");
      _sb_query.AppendLine("        , MDM_MAX_WON_AMOUNT ");
      _sb_query.AppendLine("        , MDM_MAX_JACKPOT_AMOUNT ");
      _sb_query.AppendLine("        , MDM_MAX_PLAYED_COUNT ");
      _sb_query.AppendLine("        , MDM_MAX_WON_COUNT ");
      _sb_query.AppendLine("   FROM   MACHINE_DENOM_METERS ");
      _sb_query.AppendLine("  WHERE   MDM_TERMINAL_ID = @pTerminalId ");
      _sb_query.AppendLine("    AND   MDM_DENOMINATION = @pDenomination ");

      _sql_cmd_select = new SqlCommand(_sb_query.ToString());
      _sql_cmd_select.Parameters.Add("@pTerminalId", SqlDbType.Int);
      _sql_cmd_select.Parameters.Add("@pDenomination", SqlDbType.Money);

      m_adap_mdm_select = new SqlDataAdapter();
      m_adap_mdm_select.SelectCommand = _sql_cmd_select;
      m_adap_mdm_select.InsertCommand = null;
      m_adap_mdm_select.UpdateCommand = null;
      m_adap_mdm_select.DeleteCommand = null;

      // Create Insert and Update Command for MACHINE_DENOM_METERS, and create SqlDataAdapter.

      // INSERT MACHINE_DENOM_METERS
      _sb_query = new StringBuilder();
      _sb_query.AppendLine(" SET NOCOUNT OFF ");
      _sb_query.AppendLine(" INSERT INTO   MACHINE_DENOM_METERS ");
      _sb_query.AppendLine("             ( MDM_TERMINAL_ID ");
      _sb_query.AppendLine("             , MDM_DENOMINATION ");
      _sb_query.AppendLine("             , MDM_WCP_SEQUENCE_ID ");
      _sb_query.AppendLine("             , MDM_LAST_REPORTED ");
      _sb_query.AppendLine("             , MDM_PLAYED_AMOUNT ");
      _sb_query.AppendLine("             , MDM_WON_AMOUNT ");
      _sb_query.AppendLine("             , MDM_JACKPOT_AMOUNT ");
      _sb_query.AppendLine("             , MDM_PLAYED_COUNT ");
      _sb_query.AppendLine("             , MDM_WON_COUNT ");
      _sb_query.AppendLine("             , MDM_MAX_PLAYED_AMOUNT ");
      _sb_query.AppendLine("             , MDM_MAX_WON_AMOUNT ");
      _sb_query.AppendLine("             , MDM_MAX_JACKPOT_AMOUNT ");
      _sb_query.AppendLine("             , MDM_MAX_PLAYED_COUNT ");
      _sb_query.AppendLine("             , MDM_MAX_WON_COUNT) ");
      _sb_query.AppendLine("      VALUES ");
      _sb_query.AppendLine("             ( @pTerminalId ");
      _sb_query.AppendLine("             , @pDenomination ");
      _sb_query.AppendLine("             , @pWcpSequenceId ");
      _sb_query.AppendLine("             , GETDATE() ");
      _sb_query.AppendLine("             , @pPlayedAmount ");
      _sb_query.AppendLine("             , @pWonAmount ");
      _sb_query.AppendLine("             , @pJackpotAmount ");
      _sb_query.AppendLine("             , @pPlayedCount ");
      _sb_query.AppendLine("             , @pWonCount ");
      _sb_query.AppendLine("             , @pMaxPlayedAmount ");
      _sb_query.AppendLine("             , @pMaxWonAmount ");
      _sb_query.AppendLine("             , @pMaxJackpotAmount ");
      _sb_query.AppendLine("             , @pMaxPlayedCount ");
      _sb_query.AppendLine("             , @pMaxWonCount) ");
      _sb_query.AppendLine(" SET NOCOUNT ON ");

      _sql_cmd_insert.CommandText = _sb_query.ToString();
      _sql_cmd_insert.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _sql_cmd_insert.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "MDM_DENOMINATION";
      _sql_cmd_insert.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "MDM_WCP_SEQUENCE_ID";
      _sql_cmd_insert.Parameters.Add("@pPlayedAmount", SqlDbType.Decimal).SourceColumn = "MDM_PLAYED_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pWonAmount", SqlDbType.Decimal).SourceColumn = "MDM_WON_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pJackpotAmount", SqlDbType.Decimal).SourceColumn = "MDM_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).SourceColumn = "MDM_PLAYED_COUNT";
      _sql_cmd_insert.Parameters.Add("@pWonCount", SqlDbType.BigInt).SourceColumn = "MDM_WON_COUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxPlayedAmount", SqlDbType.BigInt).SourceColumn = "MDM_MAX_PLAYED_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxWonAmount", SqlDbType.Decimal).SourceColumn = "MDM_MAX_WON_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxJackpotAmount", SqlDbType.Decimal).SourceColumn = "MDM_MAX_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxPlayedCount", SqlDbType.BigInt).SourceColumn = "MDM_MAX_PLAYED_COUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxWonCount", SqlDbType.BigInt).SourceColumn = "MDM_MAX_WON_COUNT";

      // UPDATE MACHINE_DENOM_METERS
      _sb_query = new StringBuilder();
      _sb_query.AppendLine("  SET NOCOUNT OFF ");
      _sb_query.AppendLine("  UPDATE   MACHINE_METERS ");
      _sb_query.AppendLine("     SET   MDM_WCP_SEQUENCE_ID      = @pWcpSequenceId ");
      _sb_query.AppendLine("         , MDM_LAST_REPORTED        = GETDATE() ");
      _sb_query.AppendLine("         , MDM_PLAYED_AMOUNT        = @pNewPlayedAmount ");
      _sb_query.AppendLine("         , MDM_WON_AMOUNT           = @pNewWonAmount ");
      _sb_query.AppendLine("         , MDM_JACKPOT_AMOUNT       = @pNewJackpotAmount ");
      _sb_query.AppendLine("         , MDM_PLAYED_COUNT         = @pNewPlayedCount ");
      _sb_query.AppendLine("         , MDM_WON_COUNT            = @pNewWonCount ");
      _sb_query.AppendLine("         , MDM_DELTA_PLAYED_AMOUNT  = MDM_DELTA_PLAYED_AMOUNT  + @pDeltaPlayedAmount ");
      _sb_query.AppendLine("         , MDM_DELTA_WON_AMOUNT     = MDM_DELTA_WON_AMOUNT     + @pDeltaWonAmount + @pDeltaJackpotAmount ");
      _sb_query.AppendLine("         , MDM_DELTA_JACKPOT_AMOUNT = MDM_DELTA_JACKPOT_AMOUNT + @pDeltaJackpotAmount ");
      _sb_query.AppendLine("         , MDM_DELTA_PLAYED_COUNT   = MDM_DELTA_PLAYED_COUNT   + @pDeltaPlayedCount ");
      _sb_query.AppendLine("         , MDM_DELTA_WON_COUNT      = MDM_DELTA_WON_COUNT      + @pDeltaWonCount ");
      _sb_query.AppendLine("         , MDM_MAX_PLAYED_AMOUNT    = @pMaxPlayedAmount ");
      _sb_query.AppendLine("         , MDM_MAX_WON_AMOUNT       = @pMaxWonAmount ");
      _sb_query.AppendLine("         , MDM_MAX_JACKPOT_AMOUNT   = @pMaxJackpotAmount ");
      _sb_query.AppendLine("         , MDM_MAX_PLAYED_COUNT     = @pMaxPlayedCount ");
      _sb_query.AppendLine("         , MDM_MAX_WON_COUNT        = @pMaxWonCount ");
      _sb_query.AppendLine("   WHERE   MDM_TERMINAL_ID  = @pTerminalId ");
      _sb_query.AppendLine("     AND   MDM_DENOMINATION = @pDenomination ");
      _sb_query.AppendLine("     AND   (MM_PLAYED_COUNT   = @pOldPlayedCount   OR (MM_PLAYED_COUNT IS NULL   AND @pOldPlayedCount IS NULL)) ");
      _sb_query.AppendLine("     AND   (MM_PLAYED_AMOUNT  = @pOldPlayedAmount  OR (MM_PLAYED_AMOUNT IS NULL  AND @pOldPlayedAmount IS NULL)) ");
      _sb_query.AppendLine("     AND   (MM_WON_COUNT      = @pOldWonCount      OR (MM_WON_COUNT IS NULL      AND @pOldWonCount IS NULL)) ");
      _sb_query.AppendLine("     AND   (MM_WON_AMOUNT     = @pOldWonAmount     OR (MM_WON_AMOUNT IS NULL     AND @pOldWonAmount IS NULL)) ");
      _sb_query.AppendLine("     AND   (MM_JACKPOT_AMOUNT = @pOldJackpotAmount OR (MM_JACKPOT_AMOUNT IS NULL AND @pOldJackpotAmount IS NULL)) ");
      _sb_query.AppendLine("  SET NOCOUNT ON ");

      _sql_cmd_update.CommandText = _sb_query.ToString();
      _sql_cmd_update.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _sql_cmd_update.Parameters.Add("@pDenomination", SqlDbType.Int).SourceColumn = "DENOMINATION";
      _sql_cmd_update.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
      _sql_cmd_update.Parameters.Add("@pNewPlayedCount", SqlDbType.BigInt).SourceColumn = "PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewPlayedAmount", SqlDbType.Money).SourceColumn = "PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewWonCount", SqlDbType.BigInt).SourceColumn = "WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewWonAmount", SqlDbType.Money).SourceColumn = "WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewJackpotAmount", SqlDbType.Money).SourceColumn = "JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxPlayedCount", SqlDbType.BigInt).SourceColumn = "MAX_PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pMaxPlayedAmount", SqlDbType.Money).SourceColumn = "MAX_PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxWonCount", SqlDbType.BigInt).SourceColumn = "MAX_WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pMaxWonAmount", SqlDbType.Money).SourceColumn = "MAX_WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxJackpotAmount", SqlDbType.Money).SourceColumn = "MAX_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).SourceColumn = "DELTA_PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).SourceColumn = "DELTA_PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).SourceColumn = "DELTA_WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).SourceColumn = "DELTA_WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).SourceColumn = "DELTA_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldPlayedCount", SqlDbType.BigInt).SourceColumn = "OLD_PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldPlayedAmount", SqlDbType.Money).SourceColumn = "OLD_PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldWonCount", SqlDbType.BigInt).SourceColumn = "OLD_WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldWonAmount", SqlDbType.Money).SourceColumn = "OLD_WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldJackpotAmount", SqlDbType.Money).SourceColumn = "OLD_JACKPOT_AMOUNT";

      m_adap_machine_denom_meters = new SqlDataAdapter();
      m_adap_machine_denom_meters.SelectCommand = null;
      m_adap_machine_denom_meters.InsertCommand = WGDB.DownsizeCommandText(_sql_cmd_insert);
      m_adap_machine_denom_meters.UpdateCommand = WGDB.DownsizeCommandText(_sql_cmd_update);
      m_adap_machine_denom_meters.DeleteCommand = null;
      m_adap_machine_denom_meters.ContinueUpdateOnError = false;

      m_adap_machine_denom_meters.UpdateBatchSize = 500;
      m_adap_machine_denom_meters.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
      m_adap_machine_denom_meters.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Load MachineDenomMeters data from DB to DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Load(Int32 TerminalId, Currency Denomination, SqlTransaction Trx)
    {
      int _ticks;

      _ticks = Environment.TickCount;

      m_adap_mdm_select.SelectCommand.Connection = Trx.Connection;
      m_adap_mdm_select.SelectCommand.Transaction = Trx;
      m_adap_mdm_select.SelectCommand.Parameters["@pTerminalId"].Value = TerminalId;
      m_adap_mdm_select.SelectCommand.Parameters["@pDenomination"].Value = Denomination;

      m_adap_mdm_select.Fill(m_dt_machine_denom_meters);

      m_time_loads += Misc.GetElapsedTicks(_ticks);
      m_time_num_loads++;


    } // Load

    //------------------------------------------------------------------------------
    // PURPOSE : Unqueue a Unload petition for TerminalId.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void EnqueueUnload(Int32 TerminalId)
    {
      try
      {
        m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);

        m_unload_queue.Add(TerminalId);
      }
      finally
      {
        m_rw_lock_queue.ReleaseWriterLock();
      }
    } // EnqueueUnload

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows related to the TerminalId from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean Unload(Int32 TerminalId)
    {
      String _filter;

      _filter = "TERMINAL_ID = " + TerminalId.ToString();

      return Unload(_filter);
    } // Unload

    //------------------------------------------------------------------------------
    // PURPOSE : Get Machine Meters from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //          - DbMachineMultiDenomMeters
    //          - DbWcpSequenceId
    //          - RowMachineMultiDenomMeters
    //
    // RETURNS :
    //      - true: successful.
    //      - false: not found or error.
    //
    public static Boolean Get(Int32 TerminalId,
                              Int64 Denomination,
                              out MachineMultiDenomMeters DbMachineMultiDenomMeters,
                              out Int64 DbWcpSequenceId,
                              out DataRow RowMachineMultiDenomMeters,
                              SqlTransaction Trx)
    {
      String _filter;
      DataRow[] _term_rows;
      DataRow _term_row;
      int _ticks;

      _ticks = Environment.TickCount;

      DbMachineMultiDenomMeters = new MachineMultiDenomMeters();
      DbWcpSequenceId = 0;
      RowMachineMultiDenomMeters = null;

      try
      {
        if (m_dt_machine_denom_meters == null)
        {
          throw new Exception("Get: BU_MachineMultiDenomMeters not initialized. Call Init first!");
        }

        _term_rows = null;

        Currency _denomination;
        _denomination = ((Decimal)Denomination) / 100;
        _filter = "TERMINAL_ID = " + TerminalId.ToString() + " AND DENOMINATION = " + _denomination.ToString();

        for (int _idx_tries = 0; _idx_tries < 2; _idx_tries++)
        {
          _term_rows = m_dt_machine_denom_meters.Select(_filter);

          if (_term_rows.Length == 0)
          {
            if (_idx_tries == 0)
            {
              Load(TerminalId, Denomination, Trx);
            }
            else
            {
              return false;
            }
          }
          else
          {
            break;
          }
        }

        if (_term_rows.Length > 1)
        {
          Log.Warning("BU_MachineMultiDenomMeters.Get: Too many Machine Meters for TerminalId: " + TerminalId + ". Count: " + _term_rows.Length + ".");
        }

        _term_row = _term_rows[0];

        DbWcpSequenceId = (Int64)_term_row[COLUMN_WCP_SEQUENCE_ID];

        DbMachineMultiDenomMeters.Denomination = (Int64)((Decimal)_term_row[COLUMN_DENOMINATION] * 100);
        DbMachineMultiDenomMeters.PlayedQuantity = (Int64)_term_row[COLUMN_PLAYED_COUNT];
        DbMachineMultiDenomMeters.PlayedCents = (Int64)((Decimal)_term_row[COLUMN_PLAYED_AMOUNT] * 100);
        DbMachineMultiDenomMeters.WonQuantity = (Int64)_term_row[COLUMN_WON_COUNT];
        DbMachineMultiDenomMeters.WonCents = (Int64)((Decimal)_term_row[COLUMN_WON_AMOUNT] * 100);

        if (!_term_row.IsNull(COLUMN_JACKPOT_AMOUNT))
        {
          DbMachineMultiDenomMeters.JackpotCents = (Int64)((Decimal)_term_row[COLUMN_JACKPOT_AMOUNT] * 100);
        }
        else
        {
          DbMachineMultiDenomMeters.JackpotCents = -1;
        }

        if (!_term_row.IsNull(COLUMN_MAX_PLAYED_COUNT))
        {
          DbMachineMultiDenomMeters.PlayedMaxValueQuantity = (Int64)_term_row[COLUMN_MAX_PLAYED_COUNT];
          DbMachineMultiDenomMeters.PlayedMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_PLAYED_AMOUNT] * 100);
          DbMachineMultiDenomMeters.WonMaxValueQuantity = (Int64)_term_row[COLUMN_MAX_WON_COUNT];
          DbMachineMultiDenomMeters.WonMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_WON_AMOUNT] * 100);
          if (!_term_row.IsNull(COLUMN_MAX_JACKPOT_AMOUNT))
          {
            DbMachineMultiDenomMeters.JackpotMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_JACKPOT_AMOUNT] * 100);
          }
          else
          {
            DbMachineMultiDenomMeters.JackpotMaxValueCents = -1;
          }

        }
        else
        {
          DbMachineMultiDenomMeters.PlayedMaxValueQuantity = -1;
          DbMachineMultiDenomMeters.PlayedMaxValueCents = -1;
          DbMachineMultiDenomMeters.WonMaxValueQuantity = -1;
          DbMachineMultiDenomMeters.WonMaxValueCents = -1;
          DbMachineMultiDenomMeters.JackpotMaxValueCents = -1;
        }

        RowMachineMultiDenomMeters = _term_row;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_time_gets += Misc.GetElapsedTicks(_ticks);
      }
    } // Get

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Machine Meters received to the DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - ProviderId
    //          - TerminalName
    //          - WcpSequenceId
    //          - NewMachineMultiDenomMeters: Wcp message machine meters
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: machine meters inserted.
    //      - false: error.
    //
    public static Boolean Insert(Int32 TerminalId,
                                 Int64 WcpSequenceId,
                                 MachineMultiDenomMeters NewMachineMultiDenomMeters,
                                 IWcpTaskStatus WcpTaskStatus)
    {
      DataRow _row;
      int _ticks;

      if (m_dt_machine_denom_meters == null)
      {
        throw new Exception("Insert: BU_MachineDenomMeters not initialized. Call Init first!");
      }

      _ticks = Environment.TickCount;

      try
      {
        _row = m_dt_machine_denom_meters.NewRow();

        _row[COLUMN_TERMINAL_ID] = TerminalId;
        _row[COLUMN_DENOMINATION] = ((Decimal)NewMachineMultiDenomMeters.Denomination) / 100;
        _row[COLUMN_WCP_SEQUENCE_ID] = WcpSequenceId;

        _row[COLUMN_PLAYED_COUNT] = NewMachineMultiDenomMeters.PlayedQuantity;
        _row[COLUMN_PLAYED_AMOUNT] = ((Decimal)NewMachineMultiDenomMeters.PlayedCents) / 100;
        _row[COLUMN_WON_COUNT] = NewMachineMultiDenomMeters.WonQuantity;
        _row[COLUMN_WON_AMOUNT] = ((Decimal)NewMachineMultiDenomMeters.WonCents) / 100;
        if (NewMachineMultiDenomMeters.JackpotCents == -1)
        {
          _row[COLUMN_JACKPOT_AMOUNT] = DBNull.Value;
        }
        else
        {
          _row[COLUMN_JACKPOT_AMOUNT] = ((Decimal)NewMachineMultiDenomMeters.JackpotCents) / 100;
        }

        // Response Code
        _row[COLUMN_RESPONSE_CODE] = WcpTaskStatus;

        m_dt_machine_denom_meters.Rows.Add(_row);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_time_inserts += Misc.GetElapsedTicks(_ticks);
      }
    } // Insert

    //------------------------------------------------------------------------------
    // PURPOSE : Update Machine Meters from the DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - NewMachineMultiDenomMeters: Wcp message machine meters
    //          - OldMachineMultiDenomMeters
    //          - DeltaMachineMultiDenomMeters
    //          - RowMachineMultiDenomMeters
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: machine meters updated.
    //      - false: not found or error.
    //
    public static Boolean Update(Int32 TerminalId,
                                 Int64 WcpSequenceId,
                                 MachineMultiDenomMeters NewMachineMultiDenomMeters,
                                 MachineMultiDenomMeters OldMachineMultiDenomMeters,
                                 MachineMultiDenomMeters DeltaMachineMultiDenomMeters,
                                 DataRow RowMachineMultiDenomMeters,
                                 IWcpTaskStatus WcpTaskStatus)
    {
      int _ticks;

      if (m_dt_machine_denom_meters == null)
      {
        throw new Exception("Update: BU_MachineDenomMeters not initialized. Call Init first!");
      }

      _ticks = Environment.TickCount;

      try
      {
        RowMachineMultiDenomMeters.BeginEdit();

        RowMachineMultiDenomMeters[COLUMN_WCP_SEQUENCE_ID] = WcpSequenceId;
        RowMachineMultiDenomMeters[COLUMN_LAST_REPORTED] = WGDB.Now;

        RowMachineMultiDenomMeters[COLUMN_PLAYED_COUNT] = NewMachineMultiDenomMeters.PlayedQuantity;
        RowMachineMultiDenomMeters[COLUMN_PLAYED_AMOUNT] = ((Decimal)NewMachineMultiDenomMeters.PlayedCents) / 100;
        RowMachineMultiDenomMeters[COLUMN_WON_COUNT] = NewMachineMultiDenomMeters.WonQuantity;
        RowMachineMultiDenomMeters[COLUMN_WON_AMOUNT] = ((Decimal)NewMachineMultiDenomMeters.WonCents) / 100;
        if (NewMachineMultiDenomMeters.JackpotCents == -1)
        {
          RowMachineMultiDenomMeters[COLUMN_JACKPOT_AMOUNT] = DBNull.Value;
        }
        else
        {
          RowMachineMultiDenomMeters[COLUMN_JACKPOT_AMOUNT] = ((Decimal)NewMachineMultiDenomMeters.JackpotCents) / 100;
        }

        RowMachineMultiDenomMeters[COLUMN_MAX_PLAYED_COUNT] = NewMachineMultiDenomMeters.PlayedMaxValueQuantity;
        RowMachineMultiDenomMeters[COLUMN_MAX_PLAYED_AMOUNT] = ((Decimal)NewMachineMultiDenomMeters.PlayedMaxValueCents) / 100;
        RowMachineMultiDenomMeters[COLUMN_MAX_WON_COUNT] = NewMachineMultiDenomMeters.WonMaxValueQuantity;
        RowMachineMultiDenomMeters[COLUMN_MAX_WON_AMOUNT] = ((Decimal)NewMachineMultiDenomMeters.WonMaxValueCents) / 100;
        if (NewMachineMultiDenomMeters.JackpotMaxValueCents == -1)
        {
          RowMachineMultiDenomMeters[COLUMN_MAX_JACKPOT_AMOUNT] = DBNull.Value;
        }
        else
        {
          RowMachineMultiDenomMeters[COLUMN_MAX_JACKPOT_AMOUNT] = ((Decimal)NewMachineMultiDenomMeters.JackpotMaxValueCents) / 100;
        }

        RowMachineMultiDenomMeters[COLUMN_OLD_PLAYED_COUNT] = OldMachineMultiDenomMeters.PlayedQuantity;
        RowMachineMultiDenomMeters[COLUMN_OLD_PLAYED_AMOUNT] = ((Decimal)OldMachineMultiDenomMeters.PlayedCents) / 100;
        RowMachineMultiDenomMeters[COLUMN_OLD_WON_COUNT] = OldMachineMultiDenomMeters.WonQuantity;
        RowMachineMultiDenomMeters[COLUMN_OLD_WON_AMOUNT] = ((Decimal)OldMachineMultiDenomMeters.WonCents) / 100;

        if (OldMachineMultiDenomMeters.JackpotCents == -1)
        {
          RowMachineMultiDenomMeters[COLUMN_OLD_JACKPOT_AMOUNT] = DBNull.Value;
        }
        else
        {
          RowMachineMultiDenomMeters[COLUMN_OLD_JACKPOT_AMOUNT] = ((Decimal)OldMachineMultiDenomMeters.JackpotCents) / 100;
        }

        RowMachineMultiDenomMeters[COLUMN_DELTA_PLAYED_COUNT] = DeltaMachineMultiDenomMeters.PlayedQuantity;
        RowMachineMultiDenomMeters[COLUMN_DELTA_PLAYED_AMOUNT] = ((Decimal)DeltaMachineMultiDenomMeters.PlayedCents) / 100;
        RowMachineMultiDenomMeters[COLUMN_DELTA_WON_COUNT] = DeltaMachineMultiDenomMeters.WonQuantity;
        RowMachineMultiDenomMeters[COLUMN_DELTA_WON_AMOUNT] = ((Decimal)DeltaMachineMultiDenomMeters.WonCents) / 100;
        RowMachineMultiDenomMeters[COLUMN_DELTA_JACKPOT_AMOUNT] = ((Decimal)DeltaMachineMultiDenomMeters.JackpotCents) / 100;

        // Response Code
        RowMachineMultiDenomMeters[COLUMN_RESPONSE_CODE] = WcpTaskStatus;

        RowMachineMultiDenomMeters.EndEdit();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_time_updates += Misc.GetElapsedTicks(_ticks);
      }
    } // Update

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update DataTable Rows Machine Meters to DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data is inserted/updated correctly or not.
    //
    public static Boolean UpdateDB(SqlTransaction Trx)
    {
      String _rollback_point;
      Boolean _error;
      DataRow[] _rows_to_db;
      DataRow[] _rows_to_update;
      DataRow[] _rows_to_insert;
      Int32 _num_rows_deleted;
      IWcpTaskStatus _wcp_task_status;
      Int32 _step;

      if (m_dt_machine_denom_meters == null)
      {
        Log.Error("UpdateDB: BU_MachineDenomMeters not initialized. Call Init first!");
        return false;
      }

      _rollback_point = "MACHINE_DENOM_METERS_BEFORE_INSERT";
      _error = false;
      _step = 0;

      try
      {
        while (true)
        {
          _rows_to_db = m_dt_machine_denom_meters.Select("", "TERMINAL_ID, DENOMINATION", DataViewRowState.Added | DataViewRowState.ModifiedCurrent);

          // If no rows to insert/update, return depending of _error variable.
          if (_rows_to_db.Length == 0)
          {
            return !_error;
          }

          // Need to to this here, before adapter.Update().
          // In case of error, rows correctly updated/inserted must be marked again with its RowState.
          _rows_to_update = m_dt_machine_denom_meters.Select("", "TERMINAL_ID, DENOMINATION", DataViewRowState.ModifiedCurrent);
          _rows_to_insert = m_dt_machine_denom_meters.Select("", "TERMINAL_ID, DENOMINATION", DataViewRowState.Added);

          try
          {
            Trx.Save(_rollback_point);

            m_adap_machine_denom_meters.InsertCommand.Connection = Trx.Connection;
            m_adap_machine_denom_meters.InsertCommand.Transaction = Trx;
            m_adap_machine_denom_meters.UpdateCommand.Connection = Trx.Connection;
            m_adap_machine_denom_meters.UpdateCommand.Transaction = Trx;

            _error = true;

            _step = 1;

            try
            {
              // Insert / Update rows to DB -- Time consuming
              //int ROWS = m_adap_machine_meters.Update(_rows_to_db);
              if (m_adap_machine_denom_meters.Update(_rows_to_db) == _rows_to_db.Length)
              {
                _error = false;
              }
            }
            catch (Exception ex)
            {
              Log.Warning("Exception 1: WCP_BU_MachineDenomMeters.UpdateDB.");
              Log.Exception(ex);
            }

            _step = 2;

            if (!_error)
            {
              // Everything is ok.

              _step = 4;

              foreach (DataRow _row in _rows_to_db)
              {
                _wcp_task_status = (IWcpTaskStatus)_row[COLUMN_RESPONSE_CODE];
                _wcp_task_status.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
              }

              return true;
            } // if (!error)

            _step = 5;

            if (_error)
            {
              Log.Warning("------------- BEGIN MACHINE DENOM METERS NOT UPDATED -------------");

              _step = 6;

              _num_rows_deleted = 0;

              foreach (DataRow _row in _rows_to_update)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row[COLUMN_TERMINAL_ID].ToString() +
                              ",Denom: " + _row[COLUMN_DENOMINATION].ToString() +
                              ". Details: " + _row.RowError);

                  _row.Delete();
                  _row.AcceptChanges();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetModified();
                }
              }

              _step = 9;

              foreach (DataRow _row in _rows_to_insert)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row[COLUMN_TERMINAL_ID].ToString() +
                              ",Denom: " + _row[COLUMN_DENOMINATION].ToString() +
                              ". Details: " + _row.RowError);

                  _row.Delete();
                  _row.AcceptChanges();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetAdded();
                }
              }

              _step = 10;

              if (_num_rows_deleted == 0)
              {
                Log.Error("!!!!! Error found but not in data rows. Deleted all cache !!!!!");
                m_dt_machine_denom_meters.Clear();
              }

              Log.Warning("------------- END MACHINE DENOM METERS NOT UPDATED -------------");

              Trx.Rollback(_rollback_point);

            } // if (_error)

            _step = 11;

          } // try
          catch (Exception _ex)
          {
            Log.Warning("Exception 2: WCP_BU_MachineDenomMeters.UpdateDB. Step: " + _step.ToString());
            Log.Exception(_ex);

            try
            {
              Trx.Rollback(_rollback_point);
            }
            catch (Exception _ex2)
            {
              Log.Exception(_ex2);
            }

            return false;
          }
        } // while (true)
      } // try
      catch (Exception _ex)
      {
        Log.Warning("Exception 3: WCP_BU_MachineDenomMeters.UpdateDB.");
        Log.Exception(_ex);
        return false;
      }
      finally
      {
      }
    } // UpdateDB

    //------------------------------------------------------------------------------
    // PURPOSE : Process all MachineMultiDenomMeters from Tasks in the array.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Tasks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Process(ArrayList Tasks, SqlConnection SqlConn)
    {
      SqlTransaction _sql_trx;
      WCP_Message _wcp_request;
      WCP_MsgMachineMultiDenomInfo _request;
      SecureTcpClient _tcp_client;
      Int32 _tick0;
      Int32 _ticks;

      _tick0 = Environment.TickCount;

      _sql_trx = null;

      try
      {
        try
        {
          Init();

          _ticks = Environment.TickCount;

          if (m_unload_queue.Count > 0)
          {
            try
            {
              m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);
              if (m_unload_queue.Count > 0)
              {
                foreach (Int32 _term in m_unload_queue)
                {
                  Unload(_term);
                }
                m_unload_queue.Clear();
              }
            }
            finally
            {
              m_rw_lock_queue.ReleaseWriterLock();
            }
          }

          m_time_unload += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          _sql_trx = SqlConn.BeginTransaction();

          m_time_begin_trx += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _wcp_request = _wcp_task.Request;
            _tcp_client = (SecureTcpClient)_wcp_task.WcpClient;

            _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR, "No information available.");

            _request = (WCP_MsgMachineMultiDenomInfo)_wcp_request.MsgContent;

            if (!BU_ProcessMachineDenomMeters(_tcp_client.InternalId,
                                              _wcp_request.MsgHeader.TerminalSessionId,
                                              _wcp_request.MsgHeader.SequenceId,
                                              _request,
                                              _wcp_task,
                                              _sql_trx))
            {
              Log.Error("WCP_BU_MachineMultiDenomInfo.Process: Not updated. Terminal: " + _tcp_client.InternalId + ".");
            }
          }

          m_time_task_process += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          if (!UpdateDB(_sql_trx))
          {
            Log.Error("WCP_BU_MachineMultiDenomInfo.Process: No machine meters have been modified!");
          }

          m_time_updateDB += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          _sql_trx.Commit();

          m_time_commit += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _wcp_task.SendResponse();
          }
          m_time_response += Misc.GetElapsedTicks(_ticks);
        }
        catch (Exception ex)
        {
          Log.Warning("Exception: WCP_BU_MachineMultiDenomInfo.Process.");
          Log.Exception(ex);
        }
        finally
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              _sql_trx.Rollback();
            }
          }
        }

        m_time_all += Misc.GetElapsedTicks(_tick0);
        m_time_num_all += Tasks.Count;

        if (m_time_num_all >= 10000)
        //if (m_time_all >= 100)
        {
          // Only for test
          ////Log.Message("MM times:[#Tasks/#Load]=[" + m_time_num_all + "/" + m_time_num_loads + "] " +
          ////            "ALL:" + m_time_all + "," +
          ////            "Session:" + m_time_update_session + "," +
          ////            "Get:" + m_time_gets + "," +
          ////            "Update:" + m_time_updates + "," +
          ////            "UpdateDB:" + m_time_updateDB + "," +
          ////            "Commit:" + m_time_commit + "," +
          ////            "Response:" + m_time_response + "," +
          ////            "ProcessTaskMM:" + m_time_task_process + "," +
          ////            "BeginTrx:" + m_time_begin_trx + "," +
          ////            "Unload:" + m_time_unload + "," +
          ////            "Load:" + m_time_loads + "," +
          ////            "Insert:" + m_time_inserts + ".");

          m_time_num_all = 0;
          m_time_num_loads = 0;

          m_time_all = 0;
          m_time_update_session = 0;
          m_time_gets = 0;
          m_time_updates = 0;
          m_time_updateDB = 0;
          m_time_commit = 0;
          m_time_response = 0;
          m_time_task_process = 0;
          m_time_begin_trx = 0;
          m_time_unload = 0;
          m_time_loads = 0;
          m_time_inserts = 0;
        }
      }
      catch
      {
      }
    } // Process

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows based on Filter from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Filter
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static Boolean Unload(String Filter)
    {
      DataRow[] _term_rows;

      if (m_dt_machine_denom_meters == null)
      {
        Log.Error("Unload: WCP_BU_MachineMultiDenomInfo not initialized. Call Init first!");
        return false;
      }

      _term_rows = m_dt_machine_denom_meters.Select(Filter);
      foreach (DataRow _row in _term_rows)
      {
        m_dt_machine_denom_meters.Rows.Remove(_row);
      }

      return true;
    } // Unload

    //------------------------------------------------------------------------------
    // PURPOSE : Process new wcp MachineMultiDenomInfo message
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SessionId
    //          - WcpSequenceId
    //          - WCP_MsgMachineMultiDenomInfo: Wcp message
    //          - WcpTaskStatus
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: update machine meters in memory correctly.
    //      - false: otherwise.
    //
    //   NOTES :
    //
    private static Boolean BU_ProcessMachineDenomMeters(Int32 TerminalId,
                                                        Int64 SessionId,
                                                        Int64 WcpSequenceId,
                                                        WCP_MsgMachineMultiDenomInfo NewMachineMultiDenomInfo,
                                                        WSI.WCP.IWcpTaskStatus WcpTaskStatus,
                                                        SqlTransaction Trx)
    {
      Int64 _db_wcp_sequence_id;

      MachineMultiDenomMeters _new_machine_denom_meters;
      MachineMultiDenomMeters _old_machine_denom_meters;
      MachineMultiDenomMeters _delta_machine_denom_meters;
      MachineMultiDenomMeters _zero_machine_denom_meters;
      DataRow _de_machine_denom_meters;
      String _terminal_name;
      String _provider_id;
      Boolean _big_increment;
      Int64 _decrement;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_select;
      long _interval_update;
      long _interval_all_process;

      _interval_select = 0;
      _interval_update = 0;
      _interval_all_process = 0;
      _tick0 = Environment.TickCount;

      _zero_machine_denom_meters = new MachineMultiDenomMeters();

      try
      {
        // Get wcp message machine meters
        GetMsgMachineMultiDenomMeters(NewMachineMultiDenomInfo, out _new_machine_denom_meters);

        _tick1 = Environment.TickCount;

        // Get database Machine Meters
        if (!Get(TerminalId, _new_machine_denom_meters.Denomination, out _old_machine_denom_meters, out _db_wcp_sequence_id, out _de_machine_denom_meters, Trx))
        {
          if (!Common.BatchUpdate.TerminalSession.GetData(TerminalId, SessionId, out _provider_id, out _terminal_name))
          {
            Log.Warning("BU MachineDenomMeters, TerminalSession Not Active. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

            return false;
          }

          // Insert Machine Meters
          if (!Insert(TerminalId, WcpSequenceId, _new_machine_denom_meters, WcpTaskStatus))
          {
            Log.Warning("BU MachineDenomMeters, Insert failed. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

            return false;
          }

          return true;
        }

        _tick2 = Environment.TickCount;
        _interval_select = Misc.GetElapsedTicks(_tick1, _tick2);

        if (_db_wcp_sequence_id >= WcpSequenceId)
        {
          // Message arrived is previous than message in DB.
          // Don't treat it, but mark Task as OK and return.
          WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

          return true;
        }

        if (_new_machine_denom_meters.IsEqual(_old_machine_denom_meters))
        {
          // No changed in MachineDenomMeters counters. Mark Task as OK and return.
          WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

          return true;
        }

        _delta_machine_denom_meters = new MachineMultiDenomMeters();
        _big_increment = false;

        GetDeltaMachineDenomMetersPlayedWon(TerminalId, _old_machine_denom_meters, _new_machine_denom_meters, ref _delta_machine_denom_meters, ref _big_increment, Trx);

        if (_big_increment)
        {
          if (_old_machine_denom_meters.PlayedQuantity > _new_machine_denom_meters.PlayedQuantity)
          {
            // No update machine denomination meters if played counter back 5 or less units
            if (_old_machine_denom_meters.PlayedQuantity - _new_machine_denom_meters.PlayedQuantity <= 5)
            {
              _decrement = _old_machine_denom_meters.PlayedQuantity - _new_machine_denom_meters.PlayedQuantity;

              Log.Message("*** Machine Meters: PlayedQuantity went back: " + _decrement.ToString() + " units. Machine Meters NOT updated for Terminal: " + TerminalId.ToString() + ".");

              // No update MachineDenomMeters. Mark Task as OK and return.
              WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

              return true;
            }
          }
        }

        _tick1 = Environment.TickCount;

        // Update Machine Meters
        if (!Update(TerminalId, WcpSequenceId, _new_machine_denom_meters, _old_machine_denom_meters, _delta_machine_denom_meters, _de_machine_denom_meters, WcpTaskStatus))
        {
          Log.Warning("BU Updating Machine Meters. TerminalId: " + TerminalId.ToString() + ", WcpSequenceId: " + WcpSequenceId.ToString());

          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update = Misc.GetElapsedTicks(_tick1, _tick2);
        _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);

        // Don't mark Task ResponseCode yet.
        // There are modifications in MachineDenomMeters. Wait for UpdateDB() to do it.

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_interval_select > 300
            || _interval_update > 300
            || _interval_all_process > 600)
        {
          Log.Warning("BU Terminal: " + TerminalId.ToString() + ". Message MachineDenomMeters times: Select/Update/Total = " + _interval_select.ToString() + "/" + _interval_update.ToString() + "/" + _interval_all_process.ToString());
        }
      }
    } // BU_ProcessMachineDenomMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Copy Wcp Msg Machine Meters in MachineMultiDenomMeters class
    //
    //  PARAMS :
    //      - INPUT :
    //          - WCP_MsgMachineMultiDenomInfo: Wcp report message 
    //
    //      - OUTPUT :
    //          - NewMachineMultiDenomMeters
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GetMsgMachineMultiDenomMeters(WCP_MsgMachineMultiDenomInfo WcpMsgMachineDenomMeters, out MachineMultiDenomMeters NewMachineMultiDenomMeters)
    {
      NewMachineMultiDenomMeters = new MachineMultiDenomMeters();

      //////NewMachineMultiDenomMeters.Denomination = WcpMsgMachineDenomMeters.Denomination;
      //////NewMachineMultiDenomMeters.PlayedQuantity = WcpMsgMachineDenomMeters.PlayedQuantity;
      //////NewMachineMultiDenomMeters.PlayedMaxValueQuantity = WcpMsgMachineDenomMeters.PlayedMaxValueQuantity;
      //////NewMachineMultiDenomMeters.PlayedCents = WcpMsgMachineDenomMeters.PlayedCents;
      //////NewMachineMultiDenomMeters.PlayedMaxValueCents = WcpMsgMachineDenomMeters.PlayedMaxValueCents;
      //////NewMachineMultiDenomMeters.WonQuantity = WcpMsgMachineDenomMeters.WonQuantity;
      //////NewMachineMultiDenomMeters.WonMaxValueQuantity = WcpMsgMachineDenomMeters.WonMaxValueQuantity;
      //////NewMachineMultiDenomMeters.WonCents = WcpMsgMachineDenomMeters.WonCents;
      //////NewMachineMultiDenomMeters.WonMaxValueCents = WcpMsgMachineDenomMeters.WonMaxValueCents;
      //////NewMachineMultiDenomMeters.JackpotCents = WcpMsgMachineDenomMeters.JackpotCents;
      //////NewMachineMultiDenomMeters.JackpotMaxValueCents = WcpMsgMachineDenomMeters.JackpotMaxValueCents;

    } // GetMsgMachineMultiDenomMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Get Delta all PlayedWon Machine Meters 
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - OldMachineMultiDenomMeters
    //          - NewMachineMultiDenomMeters
    //
    //      - OUTPUT :
    //          - DeltaMachineMultiDenomMeters
    //          - BigIncrement
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GetDeltaMachineDenomMetersPlayedWon(Int32 TerminalId, MachineMultiDenomMeters OldMachineMultiDenomMeters, MachineMultiDenomMeters NewMachineMultiDenomMeters, ref MachineMultiDenomMeters DeltaMachineMultiDenomMeters, ref Boolean BigIncrement, SqlTransaction Trx)
    {
      Boolean _big;
      Boolean _big_played_quantity;
      Boolean _big_played_cents;
      Boolean _big_won_quantity;
      Boolean _big_won_cents;
      Boolean _big_jackpot_cents;

      //String _terminal_name;
      //String _provider_id;
      //long _sesion_id;
      //Boolean _new_is_zero;
      //Boolean _old_is_zero;
      //DateTime _now = WGDB.Now;

      _big_played_quantity = false;
      _big_played_cents = false;
      _big_won_quantity = false;
      _big_won_cents = false;
      _big_jackpot_cents = false;

      //
      // Value -1 indicates not existing meter
      //
      ////_new_is_zero = (NewMachineDenomMeters.PlayedCents == 0
      ////                && NewMachineDenomMeters.PlayedQuantity == 0
      ////                && NewMachineDenomMeters.WonCents == 0
      ////                && NewMachineDenomMeters.WonQuantity == 0
      ////                && (NewMachineDenomMeters.JackpotCents == 0 || NewMachineDenomMeters.JackpotCents == -1)); // Value -1 indicates not existing meter

      ////_old_is_zero = (OldMachineDenomMeters.PlayedCents == 0
      ////                && OldMachineDenomMeters.PlayedQuantity == 0
      ////                && OldMachineDenomMeters.WonCents == 0
      ////                && OldMachineDenomMeters.WonQuantity == 0
      ////                && (OldMachineDenomMeters.JackpotCents == 0 || OldMachineDenomMeters.JackpotCents == -1)); // Value -1 indicates not existing meter

      ////if (_new_is_zero && !_old_is_zero)
      ////{
      ////  Log.Warning("*** PlayedWon Machine Denomination Meters Reset *** TerminalId: " + TerminalId.ToString() +
      ////              ". Old meter values:" +
      ////              "  Denomination: " + OldMachineDenomMeters.Denomination.ToString() +
      ////              ", Played Count/Cents: " + OldMachineDenomMeters.PlayedQuantity.ToString() + " / " + OldMachineDenomMeters.PlayedCents.ToString() +
      ////              ", Won Count/Cents: " + OldMachineDenomMeters.WonQuantity.ToString() + " / " + OldMachineDenomMeters.WonCents.ToString() +
      ////              ", Jackpot Cents: " + OldMachineDenomMeters.JackpotCents.ToString());

      ////  WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
      ////  WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

      ////  StringBuilder _sb;
      ////  _sb = new StringBuilder();
      ////  _sb.AppendLine("*** PlayedWon Machine Denomination Meters Reset ***");
      ////  _sb.AppendLine(" Old meter values:");
      ////  _sb.AppendLine("     Denomination: " + OldMachineDenomMeters.Denomination.ToString());
      ////  _sb.AppendLine("     Played Count/Cents: " + OldMachineDenomMeters.PlayedQuantity.ToString() + " / " + OldMachineDenomMeters.PlayedCents.ToString());
      ////  _sb.AppendLine("     Won Count/Cents: " + OldMachineDenomMeters.WonQuantity.ToString() + " / " + OldMachineDenomMeters.WonCents.ToString());
      ////  _sb.AppendLine("     Jackpot Cents: " + OldMachineDenomMeters.JackpotCents.ToString());

      ////  Alarm.Register(AlarmSourceCode.TerminalSystem,
      ////                  TerminalId,
      ////                  Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
      ////                  AlarmCode.TerminalSystem_MachineMeterReset,
      ////                 _sb.ToString());
      ////  return;
      ////}

      _big_played_quantity = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachinePlayedQuantity", OldMachineMultiDenomMeters.PlayedQuantity, NewMachineMultiDenomMeters.PlayedQuantity, OldMachineMultiDenomMeters.PlayedMaxValueQuantity, NewMachineMultiDenomMeters.PlayedMaxValueQuantity, BigIncrementsData.QuantityGamesPlayed(TerminalId), out DeltaMachineMultiDenomMeters.PlayedQuantity);
      _big_played_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachinePlayedCents", OldMachineMultiDenomMeters.PlayedCents, NewMachineMultiDenomMeters.PlayedCents, OldMachineMultiDenomMeters.PlayedMaxValueCents, NewMachineMultiDenomMeters.PlayedMaxValueCents, BigIncrementsData.CentsPlayed(TerminalId), out DeltaMachineMultiDenomMeters.PlayedCents);
      _big_won_quantity = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachineWonQuantity", OldMachineMultiDenomMeters.WonQuantity, NewMachineMultiDenomMeters.WonQuantity, OldMachineMultiDenomMeters.WonMaxValueQuantity, NewMachineMultiDenomMeters.WonMaxValueQuantity, BigIncrementsData.QuantityGamesWon(TerminalId), out DeltaMachineMultiDenomMeters.WonQuantity);
      _big_won_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachineWonCents", OldMachineMultiDenomMeters.WonCents, NewMachineMultiDenomMeters.WonCents, OldMachineMultiDenomMeters.WonMaxValueCents, NewMachineMultiDenomMeters.WonMaxValueCents, BigIncrementsData.CentsWon(TerminalId), out DeltaMachineMultiDenomMeters.WonCents);

      _big = false;
      _big = _big || _big_played_quantity;
      _big = _big || _big_played_cents;
      _big = _big || _big_won_quantity;
      _big = _big || _big_won_cents;

      // ACC 15-JUN-2012 Add Jackpot Cents into WonCents      
      if (OldMachineMultiDenomMeters.JackpotCents != -1)
      {
        _big_jackpot_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachineJackpotCents", OldMachineMultiDenomMeters.JackpotCents, NewMachineMultiDenomMeters.JackpotCents, OldMachineMultiDenomMeters.JackpotMaxValueCents, NewMachineMultiDenomMeters.JackpotMaxValueCents, BigIncrementsData.CentsJackpot(TerminalId), out DeltaMachineMultiDenomMeters.JackpotCents);
        _big = _big || _big_jackpot_cents;

        // ACC 09-OCT-2014 
        //  --> This increment is done in machine meters query (MM_DELTA_WON_AMOUNT      + @pDeltaWonAmount + @pDeltaJackpotAmount)
        // DeltaMachineDenomMeters.WonCents += DeltaMachineDenomMeters.JackpotCents;
      }

      ////if (_big)
      ////{
      ////  // Return all "delta" as zero
      ////  DeltaMachineDenomMeters = new MachineDenomMeters();

      ////  Log.Warning("*** PlayedWon Machine Denomination Meters BigIncrement *** TerminalId: " + TerminalId.ToString() +
      ////              ". Old meter values:" +
      ////              "  Denomination: " + OldMachineDenomMeters.Denomination.ToString() +
      ////              "  Played Count/Cents: " + OldMachineDenomMeters.PlayedQuantity.ToString() + " / " + OldMachineDenomMeters.PlayedCents.ToString() +
      ////              ", Won Count/Cents: " + OldMachineDenomMeters.WonQuantity.ToString() + " / " + OldMachineDenomMeters.WonCents.ToString());

      ////  Log.Warning("*** PlayedWon Machine Denomination Meters BigIncrement *** TerminalId: " + TerminalId.ToString() +
      ////              ". New meter values:" +
      ////              "  Denomination: " + NewMachineDenomMeters.Denomination.ToString() +
      ////              "  Played Count/Cents: " + NewMachineDenomMeters.PlayedQuantity.ToString() + " / " + NewMachineDenomMeters.PlayedCents.ToString() +
      ////              ", Won Count/Cents: " + NewMachineDenomMeters.WonQuantity.ToString() + " / " + NewMachineDenomMeters.WonCents.ToString());

      ////  BigIncrement = true;

      ////  WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
      ////  WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

      ////  StringBuilder _sb;
      ////  _sb = new StringBuilder();
      ////  _sb.AppendLine("*** PlayedWon Machine Denomination Meters BigIncrement ***");
      ////  _sb.AppendLine(" Old meter values:");
      ////  _sb.AppendLine("     Denomination: " + OldMachineDenomMeters.Denomination.ToString());
      ////  _sb.AppendLine("     Played Count/Cents: " + OldMachineDenomMeters.PlayedQuantity.ToString() + " / " + OldMachineDenomMeters.PlayedCents.ToString());
      ////  _sb.AppendLine("     Won Count/Cents: " + OldMachineDenomMeters.WonQuantity.ToString() + " / " + OldMachineDenomMeters.WonCents.ToString());
      ////  _sb.AppendLine(" New meter values:");
      ////  _sb.AppendLine("     Denomination: " + NewMachineDenomMeters.Denomination.ToString());
      ////  _sb.AppendLine("     Played Count/Cents: " + NewMachineDenomMeters.PlayedQuantity.ToString() + " / " + NewMachineDenomMeters.PlayedCents.ToString());
      ////  _sb.AppendLine("     Won Count/Cents: " + NewMachineDenomMeters.WonQuantity.ToString() + " / " + NewMachineDenomMeters.WonCents.ToString());

      ////  Alarm.Register(AlarmSourceCode.TerminalSystem,
      ////                 TerminalId,
      ////                 Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
      ////                 AlarmCode.TerminalSystem_MachineMeterBigIncrement,
      ////                 _sb.ToString());
      ////}

      ////if (NewMachineDenomMeters.PlayedQuantity < OldMachineDenomMeters.PlayedQuantity ||
      ////    NewMachineDenomMeters.WonQuantity < OldMachineDenomMeters.WonQuantity ||
      ////    NewMachineDenomMeters.PlayedCents < OldMachineDenomMeters.PlayedCents ||
      ////    NewMachineDenomMeters.WonCents < OldMachineDenomMeters.WonCents ||
      ////    NewMachineDenomMeters.JackpotCents < OldMachineDenomMeters.JackpotCents)
      ////{
      ////  StringBuilder _sb;

      ////  WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
      ////  WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

      ////  _sb = new StringBuilder();
      ////  _sb.AppendLine("*** PlayedWon Machine Denomination Meters Rollover ***");
      ////  _sb.AppendLine(" Old meter values:");
      ////  _sb.AppendLine("     Denomination: " + OldMachineDenomMeters.Denomination.ToString());
      ////  _sb.AppendLine("     Played Count/Cents: " + OldMachineDenomMeters.PlayedQuantity.ToString() + " / " + OldMachineDenomMeters.PlayedCents.ToString());
      ////  _sb.AppendLine("     Won Count/Cents: " + OldMachineDenomMeters.WonQuantity.ToString() + " / " + OldMachineDenomMeters.WonCents.ToString());
      ////  if (OldMachineDenomMeters.JackpotCents != -1)
      ////  {
      ////    _sb.AppendLine("     Jackpot Cents: " + OldMachineDenomMeters.JackpotCents.ToString());
      ////  }
      ////  _sb.AppendLine(" New meter values:");
      ////  _sb.AppendLine("     Denomination: " + NewMachineDenomMeters.Denomination.ToString());
      ////  _sb.AppendLine("     Played Count/Cents: " + NewMachineDenomMeters.PlayedQuantity.ToString() + " / " + NewMachineDenomMeters.PlayedCents.ToString());
      ////  _sb.AppendLine("     Won Count/Cents: " + NewMachineDenomMeters.WonQuantity.ToString() + " / " + NewMachineDenomMeters.WonCents.ToString());
      ////  if (OldMachineDenomMeters.JackpotCents != -1)
      ////  {
      ////    _sb.AppendLine("     Jackpot Cents: " + NewMachineDenomMeters.JackpotCents.ToString());
      ////  }

      ////  Alarm.Register(AlarmSourceCode.TerminalSystem,
      ////                 TerminalId,
      ////                 Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
      ////                 (UInt32)AlarmCode.TerminalSystem_MeterRollover,
      ////                 _sb.ToString(),
      ////                 AlarmSeverity.Info,
      ////                 _now,
      ////                 Trx);
      ////}
    } // GetDeltaMachineDenomMetersPlayedWon

    #endregion // Private Methods

    #region Statistics

    //------------------------------------------------------------------------------
    // PURPOSE : Prepares delta machine meters for Update machine meters statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: delta machine meters inserted in Sales per Hour.
    //      - false: error.
    //
    static public Boolean ProcessMachineDenomMetersStatistics(SqlTransaction Trx)
    {
      DataTable _dt;
      Boolean _rc;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_select;
      long _interval_update_MDSH;
      long _interval_update_MDM;
      long _interval_all_process;

      _interval_select = 0;
      _interval_update_MDSH = 0;
      _interval_update_MDM = 0;
      _interval_all_process = 0;
      _tick0 = Environment.TickCount;

      _dt = null;

      try
      {
        _tick1 = Environment.TickCount;

        // Select Delta Machine Meters
        _rc = GetDeltaMachineDenomMeters(out _dt, Trx);
        if (!_rc)
        {
          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_select = Misc.GetElapsedTicks(_tick1, _tick2);

        _tick1 = Environment.TickCount;

        // Update MACHINE_DENOM_STATS_PER_HOUR
        _rc = UpdateMachineDenomStatsPerHour(_dt, Trx);
        if (!_rc)
        {
          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update_MDSH = Misc.GetElapsedTicks(_tick1, _tick2);

        _tick1 = Environment.TickCount;

        // Update Delta Machine Meters
        _rc = UpdateDeltaMachineDenomMeters(_dt, Trx);
        if (!_rc)
        {
          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update_MDM = Misc.GetElapsedTicks(_tick1, _tick2);

        _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);

        if (_interval_select > 4000
            || _interval_update_MDSH > 4000
            || _interval_update_MDM > 4000
            || _interval_all_process > 8000)
        {
          Log.Warning("Thread Statistics MachineDenomMeters times: Select/UpdateMDSH/UpdateMDM/Total = " + _interval_select.ToString() + "/" + _interval_update_MDSH.ToString() + "/" + _interval_update_MDM.ToString() + "/" + _interval_all_process.ToString());
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
      }
    } // ProcessMachineDenomMetersStatistics

    //------------------------------------------------------------------------------
    // PURPOSE : Update Machine statistics per hour.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DeltaMachineMeter: Datatable with delta Machine meters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: delta Machine meters inserted in Sales per Hour.
    //      - false: error.
    //
    static private Boolean UpdateMachineDenomStatsPerHour(DataTable DeltaMachineDenomMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      int _update_batch_size = 500;
      int _nr;

      if (DeltaMachineDenomMeters.Rows.Count == 0)
      {
        return true;
      }

      try
      {
        //    - Set SqlDataAdapter commands

        _da = new SqlDataAdapter();

        // When batch updates are enabled (UpdateBatchSize != 1), the UpdatedRowSource property value 
        // of the DataAdapter's UpdateCommand, InsertCommand, and DeleteCommand should be set to None 
        // or OutputParameters. 
        _da.UpdateBatchSize = _update_batch_size;

        //    - Set SqlDataAdapter commands
        //        - SelectCommand 
        //        - DeleteCommand 
        //        - InsertCommand 
        //        - UpdateCommand 

        //        - UpdateCommand 
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE MACHINE_DENOM_STATS_PER_HOUR");
        _sql_txt.AppendLine("   SET   MDSH_PLAYED_COUNT    = MDSH_PLAYED_COUNT    + @pPlayedCount");
        _sql_txt.AppendLine("       , MDSH_PLAYED_AMOUNT   = MDSH_PLAYED_AMOUNT   + @pPlayedAmount");
        _sql_txt.AppendLine("       , MDSH_WON_COUNT       = MDSH_WON_COUNT       + @pWonCount");
        _sql_txt.AppendLine("       , MDSH_WON_AMOUNT      = MDSH_WON_AMOUNT      + @pWonAmount");
        _sql_txt.AppendLine("       , MDSH_JACKPOT_AMOUNT  = ISNULL(MDSH_JACKPOT_AMOUNT, 0) + ISNULL(@pDeltaJackpot, 0) ");
        _sql_txt.AppendLine(" WHERE   MDSH_TERMINAL_ID  = @pTerminalId");
        _sql_txt.AppendLine("   AND   MDSH_DENOMINATION = @pDenomination");
        _sql_txt.AppendLine("   AND   MDSH_BASE_HOUR    = @pBaseHour");

        _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
        _da.UpdateCommand.Connection = Trx.Connection;
        _da.UpdateCommand.Transaction = Trx;
        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        _da.UpdateCommand.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).SourceColumn = "MDM_DELTA_PLAYED_COUNT";
        _da.UpdateCommand.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "MDM_DELTA_PLAYED_AMOUNT";
        _da.UpdateCommand.Parameters.Add("@pWonCount", SqlDbType.BigInt).SourceColumn = "MDM_DELTA_WON_COUNT";
        _da.UpdateCommand.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "MDM_DELTA_WON_AMOUNT";
        _da.UpdateCommand.Parameters.Add("@pDeltaJackpot", SqlDbType.Money).SourceColumn = "MDM_DELTA_JACKPOT_AMOUNT";
        _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "MDM_TERMINAL_ID";
        _da.UpdateCommand.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "MDM_DENOMINATION";
        _da.UpdateCommand.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "BASE_HOUR";        

        //        - DeleteCommand & SelectCommand   
        _da.DeleteCommand = null;
        _da.SelectCommand = null;

        //    - Execute Update command
        _da.InsertCommand = null;
        _da.UpdateBatchSize = _update_batch_size;
        _da.ContinueUpdateOnError = true;

        _nr = _da.Update(DeltaMachineDenomMeters);

        if (DeltaMachineDenomMeters.Rows.Count == _nr)
        {
          return true;
        }

        foreach (DataRow _dr in DeltaMachineDenomMeters.Rows)
        {
          if (_dr.HasErrors)
          {
            _dr.ClearErrors();
            _dr.AcceptChanges();
            _dr.SetAdded();
          }
          else
          {
            _dr.AcceptChanges();
            if (_dr.RowState != DataRowState.Unchanged)
            {
              return false;
            }
          }
        }

        _da.UpdateCommand = null;

        // Insert
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("INSERT INTO  MACHINE_DENOM_STATS_PER_HOUR ");
        _sql_txt.AppendLine("           ( MDSH_BASE_HOUR ");
        _sql_txt.AppendLine("           , MDSH_TERMINAL_ID ");
        _sql_txt.AppendLine("           , MDSH_DENOMINATION ");
        _sql_txt.AppendLine("           , MDSH_PLAYED_COUNT ");
        _sql_txt.AppendLine("           , MDSH_PLAYED_AMOUNT ");
        _sql_txt.AppendLine("           , MDSH_WON_COUNT ");
        _sql_txt.AppendLine("           , MDSH_WON_AMOUNT ");
        _sql_txt.AppendLine("           , MDSH_JACKPOT_AMOUNT ) ");
        _sql_txt.AppendLine("VALUES     ( @pBaseHour, @pTerminalId, @pDenomination ,@pPlayedCount, @pPlayedAmount, @pWonCount, @pWonAmount) ");

        _da.InsertCommand = new SqlCommand(_sql_txt.ToString());
        _da.InsertCommand.Connection = Trx.Connection;
        _da.InsertCommand.Transaction = Trx;

        _da.InsertCommand.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "BASE_HOUR";
        _da.InsertCommand.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "MDM_TERMINAL_ID";
        _da.InsertCommand.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "MDM_DENOMINATION";
        _da.InsertCommand.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).SourceColumn = "MDM_DELTA_PLAYED_COUNT";
        _da.InsertCommand.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "MDM_DELTA_PLAYED_AMOUNT";
        _da.InsertCommand.Parameters.Add("@pWonCount", SqlDbType.BigInt).SourceColumn = "MDM_DELTA_WON_COUNT";
        _da.InsertCommand.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "MDM_DELTA_WON_AMOUNT";        

        _da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
        _nr = _nr + _da.Update(DeltaMachineDenomMeters);

        if (DeltaMachineDenomMeters.Rows.Count != _nr)
        {
          // Some inserts failed!
          // Set the 'delta' to zero and continue with the transaction
          foreach (DataRow _dr in DeltaMachineDenomMeters.Rows)
          {
            if (_dr.HasErrors)
            {
              _dr.ClearErrors();
              _dr["MDM_DELTA_PLAYED_COUNT"] = 0;
              _dr["MDM_DELTA_PLAYED_AMOUNT"] = 0;
              _dr["MDM_DELTA_WON_COUNT"] = 0;
              _dr["MDM_DELTA_WON_AMOUNT"] = 0;
              _dr["MDM_DELTA_JACKPOT_AMOUNT"] = 0;
              _nr = _nr + 1;
            }
            _dr.AcceptChanges();
          }

          if (DeltaMachineDenomMeters.Rows.Count != _nr)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
      }
    } // UpdateMachineStatsPerHour

    //------------------------------------------------------------------------------
    // PURPOSE : Prepares delta Machine meters for Update Machine meters statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //          - DeltaMachineDenomMeters
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: delta Machine meters Selected from Machine Meters.
    //      - false: error.
    //
    static public Boolean GetDeltaMachineDenomMeters(out DataTable DeltaMachineDenomMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      DateTime _base_hour;
      int _idx_row;
      DataTable _rows_for_update;
      int _nr;

      _base_hour = DateTime.Now; // To avoid compiler warning
      DeltaMachineDenomMeters = new DataTable("MACHINE_DENOM_METERS");

      //
      // Lock by rows and with PK order --> Select MachineDenomMeters for UPDATE .. WHERE [PK]
      //
      _rows_for_update = new DataTable("MACHINE_DENOM_METERS");

      // Set SqlDataAdapter commands
      _da = new SqlDataAdapter();

      // Accept negative deltas

      //        - SelectCommand 
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("  SELECT   MDM_TERMINAL_ID ");
      _sql_txt.AppendLine("         , MDM_DENOMINATION ");      
      _sql_txt.AppendLine("    FROM   MACHINE_DENOM_METERS ");
      _sql_txt.AppendLine("   WHERE   MDM_DELTA_UPDATING  = 0 ");
      _sql_txt.AppendLine("     AND   (   MDM_DELTA_PLAYED_COUNT <> 0 ");
      _sql_txt.AppendLine("            OR MDM_DELTA_PLAYED_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR MDM_DELTA_WON_COUNT <> 0 ");
      _sql_txt.AppendLine("            OR MDM_DELTA_WON_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR MDM_DELTA_JACKPOT_AMOUNT <> 0 ) ");
      _sql_txt.AppendLine("ORDER BY   MDM_TERMINAL_ID ");
      _sql_txt.AppendLine("         , MDM_DENOMINATION ");

      _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
      _da.SelectCommand.Connection = Trx.Connection;
      _da.SelectCommand.Transaction = Trx;

      //        - UpdateCommand, DeleteCommand & InsertCommand
      _da.DeleteCommand = null;
      _da.InsertCommand = null;
      _da.UpdateCommand = null;

      //    - Execute Select command
      _da.Fill(_rows_for_update);

      if (_rows_for_update.Rows.Count == 0)
      {
        return false;
      }

      //
      // UPDATE GM_DELTA_UPDATING
      //
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("UPDATE   MACHINE_DENOM_METERS");
      _sql_txt.AppendLine("   SET   MDM_DELTA_UPDATING = 1 ");
      _sql_txt.AppendLine(" WHERE   MDM_TERMINAL_ID    = @pTerminalId ");
      _sql_txt.AppendLine("   AND   MDM_DENOMINATION   = @pDenomination ");
      _sql_txt.AppendLine("   AND   MDM_DELTA_UPDATING = 0 ");

      _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
      _da.UpdateCommand.Connection = Trx.Connection;
      _da.UpdateCommand.Transaction = Trx;
      _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "MDM_TERMINAL_ID";
      _da.UpdateCommand.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "MDM_DENOMINATION";

      //    - SetModified
      for (_idx_row = 0; _idx_row < _rows_for_update.Rows.Count; _idx_row++)
      {
        _rows_for_update.Rows[_idx_row].SetModified();
      }

      _da.UpdateBatchSize = 500;
      _da.ContinueUpdateOnError = true;

      _nr = _da.Update(_rows_for_update);
      if (_nr == 0)
      {
        return false;
      }

      //    - Set SqlDataAdapter commands
      //        - SelectCommand 
      //        - DeleteCommand 
      //        - InsertCommand 
      //        - UpdateCommand 
      _da = new SqlDataAdapter();

      //        - SelectCommand 
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("  SELECT   MDM_TERMINAL_ID ");
      _sql_txt.AppendLine("         , MDM_DENOMINATION ");
      _sql_txt.AppendLine("         , MDM_DELTA_PLAYED_COUNT ");
      _sql_txt.AppendLine("         , MDM_DELTA_PLAYED_AMOUNT ");
      _sql_txt.AppendLine("         , MDM_DELTA_WON_COUNT ");
      _sql_txt.AppendLine("         , MDM_DELTA_WON_AMOUNT ");
      _sql_txt.AppendLine("         , MDM_DELTA_JACKPOT_AMOUNT ");
      _sql_txt.AppendLine("         , GETDATE() AS 'BASE_HOUR' ");
      _sql_txt.AppendLine("    FROM   MACHINE_DENOM_METERS ");
      _sql_txt.AppendLine("   WHERE   MDM_DELTA_UPDATING = 1 ");
      _sql_txt.AppendLine("ORDER BY   MDM_TERMINAL_ID  ");

      _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
      _da.SelectCommand.Connection = Trx.Connection;
      _da.SelectCommand.Transaction = Trx;

      //        - UpdateCommand, DeleteCommand & InsertCommand
      _da.DeleteCommand = null;
      _da.InsertCommand = null;
      _da.UpdateCommand = null;

      //    - Execute Select command
      _da.Fill(DeltaMachineDenomMeters);

      if (DeltaMachineDenomMeters.Rows.Count == 0)
      {
        return false;
      }

      for (_idx_row = 0; _idx_row < DeltaMachineDenomMeters.Rows.Count; _idx_row++)
      {
        if (_idx_row == 0)
        {
          _base_hour = (DateTime)DeltaMachineDenomMeters.Rows[_idx_row]["BASE_HOUR"];
          _base_hour = _base_hour.AddMinutes(-_base_hour.Minute);
          _base_hour = _base_hour.AddSeconds(-_base_hour.Second);
          _base_hour = _base_hour.AddMilliseconds(-_base_hour.Millisecond);
        }

        // Truncate current datetime and set the row as mofidied
        DeltaMachineDenomMeters.Rows[_idx_row]["BASE_HOUR"] = _base_hour;
        DeltaMachineDenomMeters.Rows[_idx_row].AcceptChanges();
        DeltaMachineDenomMeters.Rows[_idx_row].SetModified();
      }

      return true;

    } // GetDeltaMachineDenomMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update delta Machine meters.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //          - DeltaMachineDenomMeters
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: delta Machine meters Selected from Machine Meters.
    //      - false: error.
    //
    static public Boolean UpdateDeltaMachineDenomMeters(DataTable DeltaMachineDenomMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      int _update_batch_size = 500;
      int _idx_row;
      int _nr;

      //    - Set SqlDataAdapter commands
      //        - SelectCommand 
      //        - DeleteCommand 
      //        - InsertCommand 
      //        - UpdateCommand 
      _da = new SqlDataAdapter();

      //        - UpdateCommand 
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("UPDATE MACHINE_DENOM_METERS");
      _sql_txt.AppendLine("   SET MDM_DELTA_PLAYED_COUNT     = MDM_DELTA_PLAYED_COUNT   - @pDeltaPlayedCount");
      _sql_txt.AppendLine("     , MDM_DELTA_PLAYED_AMOUNT    = MDM_DELTA_PLAYED_AMOUNT  - @pDeltaPlayedAmount");
      _sql_txt.AppendLine("     , MDM_DELTA_WON_COUNT        = MDM_DELTA_WON_COUNT      - @pDeltaWonCount");
      _sql_txt.AppendLine("     , MDM_DELTA_WON_AMOUNT       = MDM_DELTA_WON_AMOUNT     - @pDeltaWonAmount");
      _sql_txt.AppendLine("     , MDM_DELTA_JACKPOT_AMOUNT   = MDM_DELTA_JACKPOT_AMOUNT - @pDeltaJackpotAmount");
      _sql_txt.AppendLine("     , MDM_DELTA_UPDATING         = 0");
      _sql_txt.AppendLine(" WHERE MDM_TERMINAL_ID            = @pTerminalId");
      _sql_txt.AppendLine("   AND MDM_DENOMINATION           = @pDenomination");
      _sql_txt.AppendLine("   AND MDM_DELTA_UPDATING         = 1");

      _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
      _da.UpdateCommand.Connection = Trx.Connection;
      _da.UpdateCommand.Transaction = Trx;
      _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      _da.UpdateCommand.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).SourceColumn = "MM_DELTA_PLAYED_COUNT";
      _da.UpdateCommand.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).SourceColumn = "MM_DELTA_PLAYED_AMOUNT";
      _da.UpdateCommand.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).SourceColumn = "MM_DELTA_WON_COUNT";
      _da.UpdateCommand.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).SourceColumn = "MM_DELTA_WON_AMOUNT";
      _da.UpdateCommand.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).SourceColumn = "MM_DELTA_JACKPOT_AMOUNT";
      _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "MM_TERMINAL_ID";
      _da.UpdateCommand.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "MM_TERMINAL_ID";

      //        - DeleteCommand & InsertCommand
      _da.DeleteCommand = null;
      _da.InsertCommand = null;

      //    - SetModified
      for (_idx_row = 0; _idx_row < DeltaMachineDenomMeters.Rows.Count; _idx_row++)
      {
        DeltaMachineDenomMeters.Rows[_idx_row].SetModified();
      }

      //    - Execute Update command
      _da.UpdateBatchSize = _update_batch_size;
      _da.ContinueUpdateOnError = true;

      _nr = _da.Update(DeltaMachineDenomMeters);
      if (_nr != DeltaMachineDenomMeters.Rows.Count)
      {
        return false;
      }

      return true;

    } // UpdateDeltaMachineDenomMeters

    #endregion // Statistics

  } // WCP_BU_MachineDenomMeters

  //------------------------------------------------------------------------------
  // PURPOSE : MachineDenomMeters class 
  //
  //   NOTES :
  //
  public class MachineMultiDenomMeters
  {
    // PLAYED WON METERS
    public Int64 Denomination = 0;
    public Int64 PlayedCents = 0;
    public Int64 PlayedMaxValueCents = 0;
    public Int64 PlayedQuantity = 0;
    public Int64 PlayedMaxValueQuantity = 0;
    public Int64 WonCents = 0;
    public Int64 WonMaxValueCents = 0;
    public Int64 WonQuantity = 0;
    public Int64 WonMaxValueQuantity = 0;
    public Int64 JackpotCents = 0;
    public Int64 JackpotMaxValueCents = 0;

    #region Operators Overloading

    public Boolean IsEqual(MachineMultiDenomMeters MM2)
    {
      if (this.PlayedCents != MM2.PlayedCents ||
          this.PlayedQuantity != MM2.PlayedQuantity ||
          this.WonCents != MM2.WonCents ||
          this.WonQuantity != MM2.WonQuantity ||
          this.JackpotCents != MM2.JackpotCents ||
          this.PlayedMaxValueCents != MM2.PlayedMaxValueCents ||
          this.PlayedMaxValueQuantity != MM2.PlayedMaxValueQuantity ||
          this.WonMaxValueCents != MM2.WonMaxValueCents ||
          this.WonMaxValueQuantity != MM2.WonMaxValueQuantity ||
          this.JackpotMaxValueCents != MM2.JackpotMaxValueCents)
      {
        return false;
      }

      return true;
    }

    #endregion
  }

} // WSI.WCP
