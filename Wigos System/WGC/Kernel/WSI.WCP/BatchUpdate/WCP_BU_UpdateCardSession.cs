//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_BU_UpdateCardSession.cs
// 
//   DESCRIPTION: WCP_BU_UpdateCardSession class
// 
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 12-AUG-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2010 RCI    First release.
// 11-JUL-2014 XCD    Calculate estimated points awarded
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;
using WSI.WCP;

namespace WSI.WCP
{
  public static class WCP_BU_UpdateCardSession
  {

    #region Constants

    private const Int16 COLUMN_CARD_SESSION_ID = 0;
    private const Int16 COLUMN_PLAYED_COUNT = 1;
    private const Int16 COLUMN_PLAYED_AMOUNT = 2;
    private const Int16 COLUMN_WON_COUNT = 3;
    private const Int16 COLUMN_WON_AMOUNT = 4;
    private const Int16 COLUMN_RESPONSE_CODE = 5;

    #endregion // Constants

    #region Attributes
    
    private static DataTable m_dt_card_sessions = null;
    private static SqlDataAdapter m_adap_card_sessions = null;
    private static StringBuilder m_sql_update_txt;

    #endregion // Attributes

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init attributes of the static class.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      SqlCommand _sql_cmd_update;

      // Already init.
      if (m_dt_card_sessions != null)
      {
        return;
      }

      // Create DataTable Columns

      m_dt_card_sessions = new DataTable("CARD_SESSIONS");

      m_dt_card_sessions.Columns.Add("CARD_SESSION_ID", Type.GetType("System.Int64"));
      m_dt_card_sessions.Columns.Add("PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_card_sessions.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_card_sessions.Columns.Add("WON_COUNT", Type.GetType("System.Int64"));
      m_dt_card_sessions.Columns.Add("WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_card_sessions.Columns.Add("RESPONSE_CODE", Type.GetType("System.Object"));

      // Primary Key for DataTable CardSessions (useful for Select() as it creates an index).
      m_dt_card_sessions.PrimaryKey = new DataColumn[] {m_dt_card_sessions.Columns["CARD_SESSION_ID"]};

      // Create Update Command for CARD_SESSIONS, and create SqlDataAdapter.

      // UPDATE PLAY_SESSIONS

      m_sql_update_txt = new StringBuilder();
      m_sql_update_txt.AppendLine("UPDATE PLAY_SESSIONS ");
      m_sql_update_txt.AppendLine("   SET PS_PLAYED_COUNT    = CASE WHEN (@pPlayedCount > PS_PLAYED_COUNT) THEN @pPlayedCount ELSE PS_PLAYED_COUNT END ");
      m_sql_update_txt.AppendLine("     , PS_PLAYED_AMOUNT   = CASE WHEN (@pPlayedAmount > PS_PLAYED_AMOUNT) THEN @pPlayedAmount ELSE PS_PLAYED_AMOUNT END ");
      m_sql_update_txt.AppendLine("     , PS_WON_COUNT       = CASE WHEN (@pWonCount > PS_WON_COUNT) THEN @pWonCount ELSE PS_WON_COUNT END ");
      m_sql_update_txt.AppendLine("     , PS_WON_AMOUNT      = CASE WHEN (@pWonAmount > PS_WON_AMOUNT) THEN @pWonAmount ELSE PS_WON_AMOUNT END ");
      m_sql_update_txt.AppendLine("     , PS_LOCKED          = NULL ");
      m_sql_update_txt.AppendLine("     , PS_FINISHED        = CASE WHEN (PS_STATUS = @pStatusOpened) THEN GETDATE()   ELSE PS_FINISHED END ");
      m_sql_update_txt.AppendLine(" WHERE PS_PLAY_SESSION_ID = @pCardSessionID ");
      m_sql_update_txt.AppendLine("   AND ( PS_STAND_ALONE = 1 ) ");

      _sql_cmd_update = new SqlCommand(m_sql_update_txt.ToString());
      _sql_cmd_update.Parameters.Add("@pCardSessionID", SqlDbType.BigInt).SourceColumn = "CARD_SESSION_ID";
      _sql_cmd_update.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).SourceColumn = "PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pWonCount", SqlDbType.BigInt).SourceColumn = "WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;

      m_adap_card_sessions = new SqlDataAdapter();
      m_adap_card_sessions.SelectCommand = null;
      m_adap_card_sessions.InsertCommand = null;
      m_adap_card_sessions.UpdateCommand = _sql_cmd_update;
      m_adap_card_sessions.DeleteCommand = null;
      m_adap_card_sessions.ContinueUpdateOnError = true;

      m_adap_card_sessions.UpdateBatchSize = 500;
      m_adap_card_sessions.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Process all UpdateCardSessions from Tasks in the array.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Tasks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Process(ArrayList Tasks, SqlConnection SqlConn)
    {
      SqlTransaction _sql_trx;
      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      WCP_MsgUpdateCardSession _request;
      WCP_MsgUpdateCardSessionReply _response;

      _sql_trx = null;

      try
      {
        try
        {
          Init();

          _sql_trx = SqlConn.BeginTransaction();

          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _wcp_request = _wcp_task.Request;
            _wcp_response = _wcp_task.Response;

            _request = (WCP_MsgUpdateCardSession)_wcp_request.MsgContent;
            _response = (WCP_MsgUpdateCardSessionReply) _wcp_task.Response.MsgContent;

            _response.EstimatedPointsCents = 0;
            if (!Update(_request.CardSessionId, _request.PlayedCount, _request.PlayedCents,
                        _request.WonCount, _request.WonCents, _request.JackpotCents, _wcp_task))
            {
              
              Log.Error("WCP_BU_UpdateCardSession.Process: Not updated. CardSessionId: " + _request.CardSessionId + ".");
            }
            else
            {
              
              //// XCD 11-JUL-2014 Calculate estimated won points
              //MultiPromos.InSessionParameters _params;
              //StringBuilder _sb;
              //Decimal _points = 0;
              //Boolean _awarded = true;
              //_sb = new StringBuilder();

              //_sql_trx.Save("CalculatePlayedAndWon");

              //if (!MultiPromos.Trx_GetDeltaPlayedWon(_request.CardSessionId, out _params, _sql_trx))
              //{
              //  //TODO log error message
              //}

              
              //if (!MultiPromos.Trx_UpdatePlayedAndWon(_params, _sb, _sql_trx))
              //{
              //  //TODO log error message
              //}                            
              //WSI.Common.Terminal.TerminalInfo _terminal_info;

              //_terminal_info = new WSI.Common.Terminal.TerminalInfo();

              //_sb.Length = 0;
              //_sb.AppendLine("SELECT PS_TERMINAL_ID FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @pPlaySessionId");

              //using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx))
              //{
              //  _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = _request.CardSessionId;

              //  _terminal_info.TerminalId = (Int32)_cmd.ExecuteScalar();                
              //}
              
              //WSI.Common.Terminal.Trx_GetTerminalInfo(_terminal_info.TerminalId, out _terminal_info, _sql_trx);              

              //MultiPromos.Trx_ComputeWonPoints(_params.AccountId, 
              //                                 _terminal_info, 
              //                                 _params.Played.TotalRedeemable - _params.Won.Redeemable,
              //                                 _params.Played.TotalRedeemable, 
              //                                 _params.Played.TotalBalance, 
              //                                 out _points, out _awarded, _sql_trx);

              //if (_awarded)
              //{
              //  _response.EstimatedPointsCents = (Int64) (_points * 100);
              //}
              //else
              //{
              //  _response.EstimatedPointsCents = 0;
              //}

              //_sql_trx.Rollback("CalculatePlayedAndWon");            
            }
          }

          if (!UpdateDB(_sql_trx))
          {
            Log.Error("WCP_BU_UpdateCardSession.Process: No CardSessions have been modified!");
          }

          _sql_trx.Commit();
          
          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _wcp_task.SendResponse();
          }
        }
        catch (Exception ex)
        {
          Log.Warning("Exception: WCP_BU_UpdateCardSession.Process.");
          Log.Exception(ex);
        }
        finally
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              _sql_trx.Rollback();
            }
          }
        }
      }
      catch
      {
      }
    } // Process



    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Update Card Session Counters received to the DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CardSessionId
    //          - PlayedCount
    //          - PlayedAmount
    //          - WonCount
    //          - WonAmount
    //          - JackpotAmount
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: Card session counters updated.
    //      - false: error.
    //
    private static Boolean Update(Int64 CardSessionId,
                                  Int64 PlayedCount,
                                  Decimal PlayedAmount,
                                  Int64 WonCount,
                                  Decimal WonAmount,
                                  Decimal JackpotAmount,
                                  IWcpTaskStatus WcpTaskStatus)
    {
      DataRow[] _card_sessions;
      DataRow _card_session;

      if (m_dt_card_sessions == null)
      {
        throw new Exception("Update: BU_UpdateCardSession not initialized. Call Init first!");
      }

      try
      {
        _card_sessions = m_dt_card_sessions.Select("CARD_SESSION_ID = " + CardSessionId);

        if (_card_sessions.Length > 1)
        {
          Log.Warning("BU_UpdateCardSession.Update: Too many Card Sessions for CardSession: " + CardSessionId +
              ". Count: " + _card_sessions.Length + ".");
        }

        if (_card_sessions.Length == 0)
        {
          // New Row
          _card_session = m_dt_card_sessions.NewRow();
        }
        else
        {
          // Update Row
          _card_session = _card_sessions[0];
          _card_session.BeginEdit();
        }

        _card_session[COLUMN_CARD_SESSION_ID] = CardSessionId;
        _card_session[COLUMN_PLAYED_COUNT] = PlayedCount;
        _card_session[COLUMN_PLAYED_AMOUNT] = ((Decimal)PlayedAmount) / 100;
        _card_session[COLUMN_WON_COUNT] = WonCount;
        // AJQ & ACC & XI 25-AUG-2010
        // JackpotAmount will be added when the handpay is paid.
        _card_session[COLUMN_WON_AMOUNT] = ((Decimal)WonAmount) / 100;

        // Response Code
        WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR, "No information available.");
        _card_session[COLUMN_RESPONSE_CODE] = WcpTaskStatus;

        if (_card_sessions.Length == 0)
        {
          // New Row
          m_dt_card_sessions.Rows.Add(_card_session);

          // AcceptChanges clear RowState, then set row as Modified.
          _card_session.AcceptChanges();
          _card_session.SetModified();
        }
        else
        {
          // Update Row
          _card_session.EndEdit();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    } // Update

    //------------------------------------------------------------------------------
    // PURPOSE : Update DataTable Rows Card Sessions to DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data is updated correctly or not.
    //
    private static Boolean UpdateDB(SqlTransaction Trx)
    {
      String _rollback_point;
      Boolean _error;
      DataRow[] _errors;
      Int32 _num_rows_deleted;
      IWcpTaskStatus _wcp_task_status;

      if (m_dt_card_sessions == null)
      {
        Log.Error("UpdateDB: BU_UpdateCardSession not initialized. Call Init first!");
        return false;
      }

      _rollback_point = "CARD_SESSION_BEFORE_INSERT";
      _error = false;

      try
      {
        while (true)
        {
          // If no rows to update, return depending of _error variable.
          if (m_dt_card_sessions.Rows.Count == 0)
          {
            return !_error;
          }

          try
          {
            Trx.Save(_rollback_point);

            m_adap_card_sessions.UpdateCommand.Connection = Trx.Connection;
            m_adap_card_sessions.UpdateCommand.Transaction = Trx;

            _error = true;
            try
            {
              // Update rows to DB -- Time consuming
              if (m_adap_card_sessions.Update(m_dt_card_sessions) == m_dt_card_sessions.Rows.Count)
              {
                _error = false;
              }
            }
            catch (Exception ex)
            {
              Log.Warning("Exception 1: WCP_BU_UpdateCardSession.UpdateDB.");
              Log.Exception(ex);
            }

            if (!_error)
            {
              // Everything is ok.
              foreach (DataRow _row in m_dt_card_sessions.Rows)
              {
                _wcp_task_status = (IWcpTaskStatus)_row[COLUMN_RESPONSE_CODE];
                _wcp_task_status.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
              }
              return true;
            } // if (!_error)

            Log.Warning("------------- BEGIN UPDATE_CARD_SESSION NOT UPDATED -------------");

            _num_rows_deleted = 0;
            _errors = m_dt_card_sessions.GetErrors();

            foreach (DataRow _row_error in _errors)
            {
              Log.Warning("CardSessionId: " + _row_error[COLUMN_CARD_SESSION_ID] +
                          ", PlayedCount: " + _row_error[COLUMN_PLAYED_COUNT] +
                          ", PlayedAmount: " + _row_error[COLUMN_PLAYED_AMOUNT] +
                          ", WonCount: " + _row_error[COLUMN_WON_COUNT] +
                          ", WonAmount: " + _row_error[COLUMN_WON_AMOUNT] +
                          ". Details: " + _row_error.RowError);

              _row_error.Delete();
              _num_rows_deleted++;
            }

            if (_num_rows_deleted == 0)
            {
              Log.Error("!!!!! Error found but not in data rows. Deleted all cache !!!!!");
              m_dt_card_sessions.Clear();
            }

            m_dt_card_sessions.AcceptChanges();
            foreach (DataRow _row in m_dt_card_sessions.Rows)
            {
              _row.SetModified();
            }

            Log.Warning("------------- END   UPDATE_CARD_SESSION NOT UPDATED -------------");

            Trx.Rollback(_rollback_point);

          } // try
          catch (Exception _ex)
          {
            Log.Warning("Exception 2: WCP_BU_UpdateCardSession.UpdateDB.");
            Log.Exception(_ex);
            return false;
          }
        } // while (true)
      } // try
      catch (Exception _ex)
      {
        Log.Warning("Exception 3: WCP_BU_UpdateCardSession.UpdateDB.");
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_dt_card_sessions.Clear();
      }
    } // UpdateDB

    #endregion // Private Methods

  } // WCP_BU_UpdateCardSession

} // WSI.WCP
