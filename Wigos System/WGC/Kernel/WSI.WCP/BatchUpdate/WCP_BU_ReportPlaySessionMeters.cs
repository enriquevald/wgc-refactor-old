//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//  
//   MODULE NAME: WCP_BU_ReportPlaySessionMeters.cs
// 
//   DESCRIPTION: WCP_BU_ReportPlaySessionMeters class; batch-update process of machine
//                and TITO play session meters
// 
//        AUTHOR: Nelson Madrigal Reyes
//  
// CREATION DATE: 12-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-NOV-2013 NMR    First release.
// 26-NOV-2013 NMR    Changed data types in destine columns.
// 04-DIC-2013 NMR    Renamed some columns of the report
// 08-JAN-2014 AMF    Fixed Bug WIGOSTITO-952
// 07-MAR-2014 JMM    Fixed Bug WIGOSTITO-1128
// 25-MAR-2014 DDM & RCI & MPO    Fixed Bug WIGOSTITO-1169: Calculate PLAY_SESSIONS meters correctly (Redeemable and Non-Redeemable parts)
// 17-APR-2014 ICS & LEM          Fixed Bug WIGOSTITO-1194: Plays don't update player account 
// 28-APR-2014 DDM & DHA          Fixed Bug WIGOSTITO-1211: Plays don't update ps_cash_in
// 11-JUL-2014 XCD    Calculate estimated points awarded
// 06-AUG-2014 MPO    WIGOSTITO-1249: When Auto Close Session (abandoned) It isn't necessary close the sessi�n again.
// 30-SEP-2014 ACC    Check datetime to avoid sql overflow
// 06-AUG-2015 MPO    Bug 3584: Reduce database queries
// 14-SEP-2015 DHA    Product Backlog Item 3705: Added coins from terminals
// 19-JAN-2016 JRC    Product Backlog Item 7909:Multiple buckets Waking up the thread
// 13-JUL-2016 SMN    Fixed Bug 14837:Mesa de juego (Cashless,TITO): No se pueden sacar a los jugadores cuando est�n jugando
// 02-AGO-2016 JRC    PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
// 01-SEP-2017 JML    PBI 29479: WIGOS-4700 Promotional credit reported as cashable after card out/in
// 29-NOV-2017 JML    Bug 30948:WIGOS-6888 [Ticket #10804] Error en la acumulaci�n de puntos
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Collections;
using WSI.Common.BatchUpdate;
using WSI.WCP.WCP_Messages.Meters;

namespace WSI.WCP.BatchUpdate
{
  class WCP_BU_ReportPlaySessionMeters
  {
    // Design Pattern: singleton
    public static WCP_BU_ReportPlaySessionMeters Instance = new WCP_BU_ReportPlaySessionMeters();

    #region Constants

    protected const String ROLLBACK_POINT = "PLAY_SESSION_BEFORE_INSERT";

    protected const Int32 IDX_PLAY_SESSION_ID = 0;
    protected const Int32 IDX_TERMINAL_ID = 1;
    protected const Int32 IDX_STATUS = 2;
    protected const Int32 IDX_STARTED_TIME = 3;
    protected const Int32 IDX_FINISHED_TIME = 4;
    protected const Int32 IDX_MACHINE_PLAYED_COUNT = 5;
    protected const Int32 IDX_MACHINE_PLAYED_AMOUNT = 6;
    protected const Int32 IDX_MACHINE_WON_COUNT = 7;
    protected const Int32 IDX_MACHINE_WON_AMOUNT = 8;
    protected const Int32 IDX_REDEEMABLE_TICKET_IN = 9;
    protected const Int32 IDX_PROMO_NR_TICKET_IN = 10;
    protected const Int32 IDX_PROMO_RE_TICKET_IN = 11;
    protected const Int32 IDX_REEDEMABLE_TICKET_OUT = 12;
    protected const Int32 IDX_PROMO_NR_TICKET_OUT = 13;
    protected const Int32 IDX_PS_CASH_IN = 14;           // Bills In
    protected const Int32 IDX_RESPONSE_CODE = 15;
    protected const Int32 IDX_ACCOUNT_ID = 16;

    protected const Int32 IDX_PS_NON_REDEEMABLE_CASH_IN = 17;
    protected const Int32 IDX_PS_REDEEMABLE_CASH_IN = 18;
    protected const Int32 IDX_PS_RE_CASH_IN = 19;
    protected const Int32 IDX_PS_PROMO_RE_CASH_IN = 20;
    protected const Int32 IDX_PS_NON_REDEEMABLE_CASH_OUT = 21;
    protected const Int32 IDX_PS_REDEEMABLE_CASH_OUT = 22;
    protected const Int32 IDX_PS_RE_CASH_OUT = 23;
    protected const Int32 IDX_PS_PROMO_RE_CASH_OUT = 24;
    protected const Int32 IDX_PS_NON_REDEEMABLE_PLAYED = 25;
    protected const Int32 IDX_PS_REDEEMABLE_PLAYED = 26;
    protected const Int32 IDX_PS_NON_REDEEMABLE_WON = 27;
    protected const Int32 IDX_PS_REDEEMABLE_WON = 28;

    protected const Int32 IDX_TICK_COUNT = 29;

    protected const Int32 TIME_TO_CHECK_IN_MS = 1 * 60 * 1000;

    protected Int32 m_last_check = Misc.GetTickCount();

    #endregion // Constants

    #region Attributes

    private DataTable m_dt_play_sessions = null;
    private SqlDataAdapter m_adap_play_sessions_modify;
    private SqlDataAdapter m_adap_play_sessions_select;

    #endregion // Attributes

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init attributes of the class.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    protected void Init()
    {
      StringBuilder _sb;
      SqlCommand _sql_cmd;

      if (m_dt_play_sessions == null)
      {
        // Create DataTable Columns
        m_dt_play_sessions = new DataTable("PLAY_SESSIONS");
        m_dt_play_sessions.Columns.Add("PLAY_SESSION_ID", Type.GetType("System.Int64")); // 0
        m_dt_play_sessions.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32")); // 1
        m_dt_play_sessions.Columns.Add("STATUS", Type.GetType("System.Int32")); // 2
        m_dt_play_sessions.Columns.Add("STARTED_TIME", Type.GetType("System.DateTime")); // 3
        m_dt_play_sessions.Columns.Add("FINISHED_TIME", Type.GetType("System.DateTime")); // 4
        m_dt_play_sessions.Columns.Add("MACHINE_PLAYED_COUNT", Type.GetType("System.Int64")); // 5
        m_dt_play_sessions.Columns.Add("MACHINE_PLAYED_AMOUNT", Type.GetType("System.Decimal")); // 6
        m_dt_play_sessions.Columns.Add("MACHINE_WON_COUNT", Type.GetType("System.Int64")); // 7
        m_dt_play_sessions.Columns.Add("MACHINE_WON_AMOUNT", Type.GetType("System.Decimal")); // 8
        m_dt_play_sessions.Columns.Add("REDEEMABLE_TICKET_IN", Type.GetType("System.Decimal")); // 9
        m_dt_play_sessions.Columns.Add("PROMO_NR_TICKET_IN", Type.GetType("System.Decimal")); // 10
        m_dt_play_sessions.Columns.Add("PROMO_RE_TICKET_IN", Type.GetType("System.Decimal")); // 11
        m_dt_play_sessions.Columns.Add("REDEEMABLE_TICKET_OUT", Type.GetType("System.Decimal")); // 12
        m_dt_play_sessions.Columns.Add("PROMO_NR_TICKET_OUT", Type.GetType("System.Decimal")); // 13
        m_dt_play_sessions.Columns.Add("PS_CASH_IN", Type.GetType("System.Decimal")); // 14
        m_dt_play_sessions.Columns.Add("RESPONSE_CODE", Type.GetType("System.Object")); // 15
        m_dt_play_sessions.Columns.Add("ACCOUNT_ID", Type.GetType("System.Int64")); // 16

        m_dt_play_sessions.Columns.Add("PS_NON_REDEEMABLE_CASH_IN", Type.GetType("System.Decimal"));  // 17
        m_dt_play_sessions.Columns.Add("PS_REDEEMABLE_CASH_IN", Type.GetType("System.Decimal")); // 18
        m_dt_play_sessions.Columns.Add("PS_RE_CASH_IN", Type.GetType("System.Decimal")); // 19
        m_dt_play_sessions.Columns.Add("PS_PROMO_RE_CASH_IN", Type.GetType("System.Decimal")); // 20

        m_dt_play_sessions.Columns.Add("PS_NON_REDEEMABLE_CASH_OUT", Type.GetType("System.Decimal")); // 21
        m_dt_play_sessions.Columns.Add("PS_REDEEMABLE_CASH_OUT", Type.GetType("System.Decimal")); // 22
        m_dt_play_sessions.Columns.Add("PS_RE_CASH_OUT", Type.GetType("System.Decimal")); // 23
        m_dt_play_sessions.Columns.Add("PS_PROMO_RE_CASH_OUT", Type.GetType("System.Decimal")); // 24

        m_dt_play_sessions.Columns.Add("PS_NON_REDEEMABLE_PLAYED", Type.GetType("System.Decimal")); // 25
        m_dt_play_sessions.Columns.Add("PS_REDEEMABLE_PLAYED", Type.GetType("System.Decimal")); // 26

        m_dt_play_sessions.Columns.Add("PS_NON_REDEEMABLE_WON", Type.GetType("System.Decimal")); // 27
        m_dt_play_sessions.Columns.Add("PS_REDEEMABLE_WON", Type.GetType("System.Decimal")); // 28

        m_dt_play_sessions.Columns.Add("TICK_COUNT", Type.GetType("System.Int32")); // 29

        // Primary Key for DataTable PlaySessions (useful for Select() as it creates an index).
        m_dt_play_sessions.PrimaryKey = new DataColumn[] { m_dt_play_sessions.Columns["PLAY_SESSION_ID"] };

        //
        // create select command for PLAY_SESSIONS
        //
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   PS_PLAY_SESSION_ID     AS PLAY_SESSION_ID ");
        _sb.AppendLine("       , PS_TERMINAL_ID         AS TERMINAL_ID ");
        _sb.AppendLine("       , PS_STATUS              AS STATUS ");
        _sb.AppendLine("       , PS_STARTED             AS STARTED_TIME ");
        _sb.AppendLine("       , PS_FINISHED            AS FINISHED_TIME ");
        _sb.AppendLine("       , PS_PLAYED_COUNT        AS MACHINE_PLAYED_COUNT ");
        _sb.AppendLine("       , PS_PLAYED_AMOUNT       AS MACHINE_PLAYED_AMOUNT ");
        _sb.AppendLine("       , PS_WON_COUNT           AS MACHINE_WON_COUNT ");
        _sb.AppendLine("       , PS_WON_AMOUNT          AS MACHINE_WON_AMOUNT ");
        _sb.AppendLine("       , PS_RE_TICKET_IN        AS REDEEMABLE_TICKET_IN ");
        _sb.AppendLine("       , PS_PROMO_NR_TICKET_IN  AS PROMO_NR_TICKET_IN ");
        _sb.AppendLine("       , PS_PROMO_RE_TICKET_IN  AS PROMO_RE_TICKET_IN ");
        _sb.AppendLine("       , PS_RE_TICKET_OUT       AS REDEEMABLE_TICKET_OUT ");
        _sb.AppendLine("       , PS_PROMO_NR_TICKET_OUT AS PROMO_NR_TICKET_OUT ");
        _sb.AppendLine("       , PS_CASH_IN             ");
        _sb.AppendLine("       , PS_ACCOUNT_ID          AS ACCOUNT_ID ");

        _sb.AppendLine("       , PS_NON_REDEEMABLE_CASH_IN ");
        _sb.AppendLine("       , PS_REDEEMABLE_CASH_IN ");
        _sb.AppendLine("       , PS_RE_CASH_IN ");
        _sb.AppendLine("       , PS_PROMO_RE_CASH_IN ");
        _sb.AppendLine("       , PS_NON_REDEEMABLE_CASH_OUT ");
        _sb.AppendLine("       , PS_REDEEMABLE_CASH_OUT ");
        _sb.AppendLine("       , PS_RE_CASH_OUT ");
        _sb.AppendLine("       , PS_PROMO_RE_CASH_OUT ");
        _sb.AppendLine("       , PS_NON_REDEEMABLE_PLAYED ");
        _sb.AppendLine("       , PS_REDEEMABLE_PLAYED ");
        _sb.AppendLine("       , PS_NON_REDEEMABLE_WON ");
        _sb.AppendLine("       , PS_REDEEMABLE_WON ");

        _sb.AppendLine("  FROM   PLAY_SESSIONS ");
        _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");

        _sql_cmd = new SqlCommand(_sb.ToString());
        _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt);

        m_adap_play_sessions_select = new SqlDataAdapter();
        m_adap_play_sessions_select.SelectCommand = _sql_cmd;

        //
        // Create Update Command for PLAY_SESSIONS, and create SqlDataAdapter.
        //

        // UPDATE PLAY_SESSIONS
        _sb.Length = 0;
        _sb.AppendLine("UPDATE   PLAY_SESSIONS ");
        _sb.AppendLine("   SET   PS_LOCKED        = NULL ");
        _sb.AppendLine("       , PS_STARTED       = CASE WHEN (PS_STARTED IS NULL) THEN @pStartedTime ELSE PS_STARTED END ");
        _sb.AppendLine("       , PS_FINISHED      = CASE WHEN (@pStatus = 1) THEN @pFinishedTime ELSE GETDATE() END ");
        _sb.AppendLine("       , PS_PLAYED_COUNT  = CASE WHEN (@pPlayedCount > ISNULL(PS_PLAYED_COUNT, 0)) THEN @pPlayedCount ELSE PS_PLAYED_COUNT END ");
        _sb.AppendLine("       , PS_PLAYED_AMOUNT = CASE WHEN (@pPlayedAmount > ISNULL(PS_PLAYED_AMOUNT, 0)) THEN @pPlayedAmount ELSE PS_PLAYED_AMOUNT END ");
        _sb.AppendLine("       , PS_WON_COUNT     = CASE WHEN (@pWonCount > ISNULL(PS_WON_COUNT, 0)) THEN @pWonCount ELSE PS_WON_COUNT END ");
        _sb.AppendLine("       , PS_WON_AMOUNT    = CASE WHEN (@pWonAmount > ISNULL(PS_WON_AMOUNT, 0)) THEN @pWonAmount ELSE PS_WON_AMOUNT END ");
        _sb.AppendLine("       , PS_RE_TICKET_IN  = CASE WHEN (@pRedeemableTicketIn > ISNULL(PS_RE_TICKET_IN, 0)) THEN @pRedeemableTicketIn ELSE PS_RE_TICKET_IN END ");
        _sb.AppendLine("       , PS_PROMO_NR_TICKET_IN  = CASE WHEN (@pPromoNrTicketIn > ISNULL(PS_PROMO_NR_TICKET_IN, 0)) THEN @pPromoNrTicketIn ELSE PS_PROMO_NR_TICKET_IN END ");
        _sb.AppendLine("       , PS_PROMO_RE_TICKET_IN  = CASE WHEN (@pPromoReTicketIn > ISNULL(PS_PROMO_RE_TICKET_IN, 0)) THEN @pPromoReTicketIn ELSE PS_PROMO_RE_TICKET_IN END ");
        _sb.AppendLine("       , PS_RE_TICKET_OUT       = CASE WHEN (@pRedeemableTicketOut > ISNULL(PS_RE_TICKET_OUT, 0)) THEN @pRedeemableTicketOut ELSE PS_RE_TICKET_OUT END ");
        _sb.AppendLine("       , PS_PROMO_NR_TICKET_OUT = CASE WHEN (@pPromoNrTicketOut > ISNULL(PS_PROMO_NR_TICKET_OUT, 0)) THEN @pPromoNrTicketOut ELSE PS_PROMO_NR_TICKET_OUT END ");
        _sb.AppendLine("       , PS_CASH_IN             = CASE WHEN (@pCashIn > ISNULL(PS_CASH_IN, 0)) THEN @pCashIn ELSE PS_CASH_IN END ");

        _sb.AppendLine("       , PS_NON_REDEEMABLE_CASH_IN  = CASE WHEN (@pNonRedeemableCashIn > ISNULL(PS_NON_REDEEMABLE_CASH_IN, 0)) THEN @pNonRedeemableCashIn ELSE PS_NON_REDEEMABLE_CASH_IN END ");
        _sb.AppendLine("       , PS_REDEEMABLE_CASH_IN      = CASE WHEN (@pRedeemableCashIn > ISNULL(PS_REDEEMABLE_CASH_IN, 0)) THEN @pRedeemableCashIn ELSE PS_REDEEMABLE_CASH_IN END ");
        _sb.AppendLine("       , PS_RE_CASH_IN              = CASE WHEN (@pReCashIn > ISNULL(PS_RE_CASH_IN, 0)) THEN @pReCashIn ELSE PS_RE_CASH_IN END ");
        _sb.AppendLine("       , PS_PROMO_RE_CASH_IN        = CASE WHEN (@pPromoReCashIn > ISNULL(PS_PROMO_RE_CASH_IN, 0)) THEN @pPromoReCashIn ELSE PS_PROMO_RE_CASH_IN END ");
        _sb.AppendLine("       , PS_NON_REDEEMABLE_CASH_OUT = CASE WHEN (@pNonRedeemableCashOut > ISNULL(PS_NON_REDEEMABLE_CASH_OUT, 0)) THEN @pNonRedeemableCashOut ELSE PS_NON_REDEEMABLE_CASH_OUT END ");
        _sb.AppendLine("       , PS_REDEEMABLE_CASH_OUT     = CASE WHEN (@pRedeemableCashOut > ISNULL(PS_REDEEMABLE_CASH_OUT, 0)) THEN @pRedeemableCashOut ELSE PS_REDEEMABLE_CASH_OUT END ");
        _sb.AppendLine("       , PS_RE_CASH_OUT             = CASE WHEN (@pReCashOut > ISNULL(PS_RE_CASH_OUT, 0)) THEN @pReCashOut ELSE PS_RE_CASH_OUT END ");
        _sb.AppendLine("       , PS_PROMO_RE_CASH_OUT       = CASE WHEN (@pPromoReCashOut > ISNULL(PS_PROMO_RE_CASH_OUT, 0)) THEN @pPromoReCashOut ELSE PS_PROMO_RE_CASH_OUT END ");
        _sb.AppendLine("       , PS_NON_REDEEMABLE_PLAYED   = CASE WHEN (@pNonRedeemablePlayed > ISNULL(PS_NON_REDEEMABLE_PLAYED, 0)) THEN @pNonRedeemablePlayed ELSE PS_NON_REDEEMABLE_PLAYED END ");
        _sb.AppendLine("       , PS_REDEEMABLE_PLAYED       = CASE WHEN (@pRedeemablePlayed > ISNULL(PS_REDEEMABLE_PLAYED, 0)) THEN @pRedeemablePlayed ELSE PS_REDEEMABLE_PLAYED END ");
        _sb.AppendLine("       , PS_NON_REDEEMABLE_WON      = CASE WHEN (@pNonRedeemableWon > ISNULL(PS_NON_REDEEMABLE_WON, 0)) THEN @pNonRedeemableWon ELSE PS_NON_REDEEMABLE_WON END ");
        _sb.AppendLine("       , PS_REDEEMABLE_WON          = CASE WHEN (@pRedeemableWon > ISNULL(PS_REDEEMABLE_WON, 0)) THEN @pRedeemableWon ELSE PS_REDEEMABLE_WON END ");

        _sb.AppendLine("       , PS_ACCOUNT_ID          = @pAccountId ");
        _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");

        _sql_cmd = new SqlCommand(_sb.ToString());
        _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).SourceColumn = "PLAY_SESSION_ID";
        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
        _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Bit).SourceColumn = "STATUS";
        _sql_cmd.Parameters.Add("@pStartedTime", SqlDbType.DateTime).SourceColumn = "STARTED_TIME";
        _sql_cmd.Parameters.Add("@pFinishedTime", SqlDbType.DateTime).SourceColumn = "FINISHED_TIME";
        _sql_cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).SourceColumn = "MACHINE_PLAYED_COUNT";
        _sql_cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Decimal).SourceColumn = "MACHINE_PLAYED_AMOUNT";
        _sql_cmd.Parameters.Add("@pWonCount", SqlDbType.BigInt).SourceColumn = "MACHINE_WON_COUNT";
        _sql_cmd.Parameters.Add("@pWonAmount", SqlDbType.Decimal).SourceColumn = "MACHINE_WON_AMOUNT";
        _sql_cmd.Parameters.Add("@pRedeemableTicketIn", SqlDbType.Decimal).SourceColumn = "REDEEMABLE_TICKET_IN";
        _sql_cmd.Parameters.Add("@pPromoNrTicketIn", SqlDbType.Decimal).SourceColumn = "PROMO_NR_TICKET_IN";
        _sql_cmd.Parameters.Add("@pPromoReTicketIn", SqlDbType.Decimal).SourceColumn = "PROMO_RE_TICKET_IN";
        _sql_cmd.Parameters.Add("@pRedeemableTicketOut", SqlDbType.Decimal).SourceColumn = "REDEEMABLE_TICKET_OUT";
        _sql_cmd.Parameters.Add("@pPromoNrTicketOut", SqlDbType.Decimal).SourceColumn = "PROMO_NR_TICKET_OUT";
        _sql_cmd.Parameters.Add("@pCashIn", SqlDbType.Decimal).SourceColumn = "PS_CASH_IN";
        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.Decimal).SourceColumn = "ACCOUNT_ID";

        _sql_cmd.Parameters.Add("@pNonRedeemableCashIn", SqlDbType.Decimal).SourceColumn = "PS_NON_REDEEMABLE_CASH_IN";
        _sql_cmd.Parameters.Add("@pRedeemableCashIn", SqlDbType.Decimal).SourceColumn = "PS_REDEEMABLE_CASH_IN";
        _sql_cmd.Parameters.Add("@pReCashIn", SqlDbType.Decimal).SourceColumn = "PS_RE_CASH_IN";
        _sql_cmd.Parameters.Add("@pPromoReCashIn", SqlDbType.Decimal).SourceColumn = "PS_PROMO_RE_CASH_IN";
        _sql_cmd.Parameters.Add("@pNonRedeemableCashOut", SqlDbType.Decimal).SourceColumn = "PS_NON_REDEEMABLE_CASH_OUT";
        _sql_cmd.Parameters.Add("@pRedeemableCashOut", SqlDbType.Decimal).SourceColumn = "PS_REDEEMABLE_CASH_OUT";
        _sql_cmd.Parameters.Add("@pReCashOut", SqlDbType.Decimal).SourceColumn = "PS_RE_CASH_OUT";
        _sql_cmd.Parameters.Add("@pPromoReCashOut", SqlDbType.Decimal).SourceColumn = "PS_PROMO_RE_CASH_OUT";
        _sql_cmd.Parameters.Add("@pNonRedeemablePlayed", SqlDbType.Decimal).SourceColumn = "PS_NON_REDEEMABLE_PLAYED";
        _sql_cmd.Parameters.Add("@pRedeemablePlayed", SqlDbType.Decimal).SourceColumn = "PS_REDEEMABLE_PLAYED";
        _sql_cmd.Parameters.Add("@pNonRedeemableWon", SqlDbType.Decimal).SourceColumn = "PS_NON_REDEEMABLE_WON";
        _sql_cmd.Parameters.Add("@pRedeemableWon", SqlDbType.Decimal).SourceColumn = "PS_REDEEMABLE_WON";

        m_adap_play_sessions_modify = new SqlDataAdapter();
        m_adap_play_sessions_modify.SelectCommand = null;
        m_adap_play_sessions_modify.InsertCommand = null;
        m_adap_play_sessions_modify.UpdateCommand = _sql_cmd;
        m_adap_play_sessions_modify.DeleteCommand = null;
        m_adap_play_sessions_modify.ContinueUpdateOnError = true;

        m_adap_play_sessions_modify.UpdateBatchSize = 500;
        m_adap_play_sessions_modify.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
      }
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Process all Update Play Sessions from Tasks in the array.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Tasks
    //          - SqlConnection
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void Process(ArrayList Tasks, SqlConnection SqlConn)
    {
      SqlTransaction _sql_trx;
      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      WCP_MsgPlaySessionMeters _request;
      WCP_MsgPlaySessionMetersReply _response;
      SessionTerminal _terminal;
      Int64 _account_id;
      Boolean _update_play_session;
      MultiPromos.EndSessionInput _end_input;
      MultiPromos.EndSessionOutput _end_output;
      MultiPromos.InSessionParameters _in_session_params;
      String _trackdata;
      Boolean _executed_OnEndCardEession;
      
      _sql_trx = null;
      _trackdata = "";
      _executed_OnEndCardEession = false;
      try
      {
        Init();
        _sql_trx = SqlConn.BeginTransaction();

        foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
        {
          _wcp_request = _wcp_task.Request;
          _wcp_response = _wcp_task.Response;

          _request = (WCP_MsgPlaySessionMeters)_wcp_request.MsgContent;
          _response = (WCP_MsgPlaySessionMetersReply)_wcp_response.MsgContent;

////////#if DEBUG
////////          Log.Message(String.Format("*** PlaySession FT Meters: Terminal: {0}", _wcp_request.MsgHeader.TerminalId));
////////          Log.Message(String.Format("   - To   GM [RE/NR/PR]: {0, 10}/{1, 10}/{2, 10}", 
////////                                    _request.ft_meters.to_gm_cashable_cents, 
////////                                    _request.ft_meters.to_gm_restricted_cents, 
////////                                    _request.ft_meters.to_gm_non_restricted_cents));
////////          Log.Message(String.Format("   - From GM [RE/NR/PR]: {0, 10}/{1, 10}/{2, 10}", 
////////                                    _request.ft_meters.from_gm_cashable_cents,
////////                                    _request.ft_meters.from_gm_restricted_cents,
////////                                    _request.ft_meters.from_gm_non_restricted_cents));
////////#endif

          _update_play_session = true;

          if (!this.GetTerminalData(_wcp_request.MsgHeader.TerminalId, out _terminal, _sql_trx))
          {
            _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR, "Terminal not found.");
            Log.Error("BU_ReportPlaySessionMeters.Process" +
                      ", Terminal not found. TerminalId/SequenceId=" +
                      _wcp_request.MsgHeader.TerminalId + "/" +
                      _wcp_request.MsgHeader.SequenceId);
            continue;
          }

          // No Play Session --> Return OK for no more report this session... 
          if (_request.play_session_id == 0)
          {
            _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
            Log.Error("BU_ReportPlaySessionMeters.Process" +
                      ", No PlaySession. TerminalId=" +
                      _wcp_request.MsgHeader.TerminalId);
            continue;
          }

          if (_terminal.AccountId == 0)
          {
            _terminal.AccountId = Accounts.GetOrCreateVirtualAccount(_terminal.terminal_id, _sql_trx);
          }

          _account_id = 0;
          if (!String.IsNullOrEmpty(_request.track_data1))
          {
            this.GetAccountId(_request.track_data1, out _account_id, _sql_trx);
            _trackdata = _request.track_data1;
          }
          else if (!String.IsNullOrEmpty(_request.track_data2))
          {
            this.GetAccountId(_request.track_data2, out _account_id, _sql_trx);
            _trackdata = _request.track_data2;
          }

          if (_account_id > 0)
          {
            PlaySession.UpdateAccount(_request.play_session_id, _account_id, _trackdata);
          }

          _end_input = new MultiPromos.EndSessionInput();
          _end_input = _request.EndSessionInput();
          _end_input.TransactionId = _wcp_request.TransactionId;
          _end_input.AccountId = _account_id;
          _end_input.VirtualAccountId = _terminal.AccountId;
          _end_input.IsTITO = true;
          
          _in_session_params = MultiPromos.TITO_GetDeltaPlayedWon(_end_input);

          if (_request.closed)
          {
            PlaySessionStatus _ps_status;
            DB_GetPlaySessionStatus(_request.play_session_id, out _ps_status, _sql_trx);

            _update_play_session = false;

            if (_ps_status == PlaySessionStatus.Closed) //------- || _ps_status == PlaySessionStatus.Abandoned)
            {
              _update_play_session = true;
            }
            else
            {
              _sql_trx.Save("Trx_OnEndCardSession");
              if (!WSI.Common.MultiPromos.Trx_OnEndCardSession(_terminal.terminal_id, (TerminalTypes)_terminal.terminal_type, _terminal.name, _end_input, out _end_output, _sql_trx))
              {
                Log.Error(_end_output.Message);
                _sql_trx.Rollback("Trx_OnEndCardSession");
              }
              else 
              {
                _executed_OnEndCardEession = true;
              }
            }
          }

          if (_update_play_session)
          {
            if (_account_id == 0)
            {
              _account_id = _terminal.AccountId;
            }

            this.GetFromDBIfNotExists(_request.play_session_id, _sql_trx);
            if (!this.UpdateOrInsert(_terminal.terminal_id, _account_id, _request, _end_input.TitoSessionMeters, _in_session_params, _wcp_task))
            {
              Log.Error("BU_ReportPlaySessionMeters.Process: Not updated. TerminalId: " + _terminal.terminal_id);
            }
          }

          if (_end_input.FoundInEgm.TotalBalance > 0 || _end_input.RemainingInEgm.TotalBalance > 0)
          {
            if (!Trx_UpdateFoundAndRemainingInEgm(_end_input.PlaySessionId, _end_input.FoundInEgm, _end_input.RemainingInEgm, _sql_trx))
            {
              Log.Error("BU_ReportPlaySessionMeters.Process.Trx_UpdateFoundAndRemainingInEgm failed.");
            }
          }

          //XCD 11-JUL-2014 Calculate estimated points awarded
          WSI.Common.Terminal.TerminalInfo _terminal_info;
          Decimal _points = 0;
          Boolean _awarded = false;

          _terminal_info = new WSI.Common.Terminal.TerminalInfo();

          WSI.Common.Terminal.Trx_GetTerminalInfo(_terminal.terminal_id, out _terminal_info, _sql_trx);

          //calculations only . doesn't create any records . need the _points for the next calculation
          BucketsUpdate.ComputeWonBuckets(_account_id,
                                          _terminal_info,
                                          _in_session_params.Played.TotalRedeemable,
                                          Buckets.BucketId.RedemptionPoints,
                                          _in_session_params.PlaySessionId,
                                          out _points,
                                          out _awarded,
                                          _sql_trx);
          if (_awarded)
          {
            _response.EstimatedPointsCents = (Int64)(_points * 100);
          }
          else
          {
            _response.EstimatedPointsCents = 0;
          }

        } // end-foreach

        DB_Update(_sql_trx);

        _sql_trx.Commit();

        #if !SQL_BUSINESS_LOGIC
        if (_executed_OnEndCardEession)
        {
          BucketsUpdate.SetDataAvailable(); /* wakes up the thread*/
        }
        #endif

        foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
        {
          _wcp_task.SendResponse();
        }
      }
      catch (Exception ex)
      {
        Log.Warning("Exception: BU_ReportPlaySessionMeters.Process.");
        Log.Exception(ex);
      }
      finally
      {
        if (_sql_trx != null && _sql_trx.Connection != null)
        {
          _sql_trx.Rollback();
        }
      }
    } // Process

    #endregion // Public Methods

    #region Private Methods

    private static Boolean Trx_UpdateFoundAndRemainingInEgm(Int64 PlaySessionId, MultiPromos.AccountBalance Found, MultiPromos.AccountBalance Remaining, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("UPDATE   PLAY_SESSIONS");
      _sb.AppendLine("   SET   PS_RE_FOUND_IN_EGM     = @pREFoundInEgm");
      _sb.AppendLine("       , PS_NR_FOUND_IN_EGM     = @pNRFoundInEgm ");
      _sb.AppendLine("       , PS_RE_REMAINING_IN_EGM = @pRERemainingInEgm ");
      _sb.AppendLine("       , PS_NR_REMAINING_IN_EGM = @pNRRemainingInEgm ");
      _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID     = @pPlaySessionID ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pREFoundInEgm", SqlDbType.Money).Value = Math.Max(Found.Redeemable, 0);
          _cmd.Parameters.Add("@pNRFoundInEgm", SqlDbType.Money).Value = Math.Max(Found.TotalNotRedeemable, 0);
          _cmd.Parameters.Add("@pRERemainingInEgm", SqlDbType.Money).Value = Math.Max(Remaining.Redeemable, 0);
          _cmd.Parameters.Add("@pNRRemainingInEgm", SqlDbType.Money).Value = Math.Max(Remaining.TotalNotRedeemable, 0);

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_OnEndCardSession

    private Boolean GetFromDBIfNotExists(Int64 PlaySession, SqlTransaction Trx)
    {
      try
      {
        if (m_dt_play_sessions.Rows.Find(PlaySession) == null)
        {
          DB_Load(PlaySession, Trx);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetFromDBIfNotExists


    //------------------------------------------------------------------------------
    // PURPOSE : Load Terminal meters data from DB to DataTable Cache
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void DB_Load(Int64 PlaySession, SqlTransaction Trx)
    {
      m_adap_play_sessions_select.SelectCommand.Connection = Trx.Connection;
      m_adap_play_sessions_select.SelectCommand.Transaction = Trx;
      m_adap_play_sessions_select.SelectCommand.Parameters["@pPlaySessionId"].Value = PlaySession;
      m_adap_play_sessions_select.Fill(m_dt_play_sessions);
    } // Load

    //------------------------------------------------------------------------------
    // PURPOSE : Update/Insert received Play Session Counters to the DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WCP_MsgPlaySession
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: play session counters updated.
    //      - false: error.
    //
    private Boolean UpdateOrInsert(Int32 TerminalId, Int64 AccountId, WCP_MsgPlaySessionMeters PlaySession, MultiPromos.TITOSessionMeters SessionMeters,
                                   MultiPromos.InSessionParameters InSessionParams, IWcpTaskStatus WcpTask)
    {
      Boolean _result;
      DataRow _play_session_row;
      Boolean _is_new_row;

      _result = false;
      _is_new_row = false;

      try
      {
        _play_session_row = m_dt_play_sessions.Rows.Find(PlaySession.play_session_id);

        if (_play_session_row == null)
        {
          _play_session_row = m_dt_play_sessions.NewRow();
          _is_new_row = true;
        }
        else
        {
          _play_session_row.BeginEdit();
        }

        _play_session_row[IDX_TICK_COUNT] = Misc.GetTickCount();
        _play_session_row[IDX_PLAY_SESSION_ID] = PlaySession.play_session_id;
        _play_session_row[IDX_TERMINAL_ID] = TerminalId;

        if (PlaySession.closed)
        {
          _play_session_row[IDX_STATUS] = PlaySessionStatus.Closed;
        }
        else
        {
          _play_session_row[IDX_STATUS] = PlaySessionStatus.Opened;
        }

        _play_session_row[IDX_STARTED_TIME] = PlaySession.started_datetime;
        // ACC 30-SEP-2014 To avoid datetime overflow
        if (PlaySession.started_datetime.Year < 2000 || PlaySession.started_datetime.Year > 9990)
        {
          _play_session_row[IDX_STARTED_TIME] = WGDB.Now;
        }

        if (PlaySession.closed_datetime.Ticks > 0)
        {
          _play_session_row[IDX_FINISHED_TIME] = PlaySession.closed_datetime;
          // ACC 30-SEP-2014 To avoid datetime overflow
          if (PlaySession.closed_datetime.Year < 2000 || PlaySession.closed_datetime.Year > 9990)
          {
            _play_session_row[IDX_FINISHED_TIME] = WGDB.Now;
          }
        }
        else
        {
          _play_session_row[IDX_FINISHED_TIME] = DBNull.Value;
        }
        _play_session_row[IDX_MACHINE_PLAYED_COUNT] = PlaySession.machine_meters.num_played;
        _play_session_row[IDX_MACHINE_PLAYED_AMOUNT] = PlaySession.machine_meters.played_cents / 100.0;
        _play_session_row[IDX_MACHINE_WON_COUNT] = PlaySession.machine_meters.num_won;
        _play_session_row[IDX_MACHINE_WON_AMOUNT] = PlaySession.machine_meters.won_cents / 100.0;

        _play_session_row[IDX_REDEEMABLE_TICKET_IN] = PlaySession.tito_session_meters.ticket_in_cashable_cents / 100.0;
        _play_session_row[IDX_PROMO_NR_TICKET_IN] = PlaySession.tito_session_meters.ticket_in_promo_nr_cents / 100.0;
        _play_session_row[IDX_PROMO_RE_TICKET_IN] = PlaySession.tito_session_meters.ticket_in_promo_re_cents / 100.0;
        _play_session_row[IDX_REEDEMABLE_TICKET_OUT] = PlaySession.tito_session_meters.ticket_out_cashable_cents / 100.0;
        _play_session_row[IDX_PROMO_NR_TICKET_OUT] = PlaySession.tito_session_meters.ticket_out_promo_nr_cents / 100.0;

        _play_session_row[IDX_PS_CASH_IN] = PlaySession.tito_session_meters.cash_in_total_in_cents / 100.0;

        _play_session_row[IDX_PS_RE_CASH_IN] = SessionMeters.RedeemableCashIn;
        _play_session_row[IDX_PS_PROMO_RE_CASH_IN] = SessionMeters.PromoReCashIn;
        _play_session_row[IDX_PS_NON_REDEEMABLE_CASH_IN] = SessionMeters.PromoNrCashIn;
        _play_session_row[IDX_PS_REDEEMABLE_CASH_IN] = SessionMeters.RedeemableCashIn + SessionMeters.PromoReCashIn + (Decimal)(PlaySession.ft_meters.to_gm_cashable_cents / 100.0);

        _play_session_row[IDX_PS_RE_CASH_OUT] = SessionMeters.RedeemableCashOut;
        _play_session_row[IDX_PS_PROMO_RE_CASH_OUT] = 0;
        _play_session_row[IDX_PS_NON_REDEEMABLE_CASH_OUT] = SessionMeters.PromoNrCashOut;
        _play_session_row[IDX_PS_REDEEMABLE_CASH_OUT] = SessionMeters.RedeemableCashOut;

        _play_session_row[IDX_PS_NON_REDEEMABLE_PLAYED] = InSessionParams.Played.TotalNotRedeemable;
        _play_session_row[IDX_PS_REDEEMABLE_PLAYED] = InSessionParams.Played.TotalRedeemable;

        _play_session_row[IDX_PS_NON_REDEEMABLE_WON] = InSessionParams.Won.TotalNotRedeemable;
        _play_session_row[IDX_PS_REDEEMABLE_WON] = InSessionParams.Won.TotalRedeemable;

        _play_session_row[IDX_ACCOUNT_ID] = AccountId;

        // Response Code
        _play_session_row[IDX_RESPONSE_CODE] = WcpTask;

        if (_is_new_row)
        {
          m_dt_play_sessions.Rows.Add(_play_session_row);
          _play_session_row.AcceptChanges();
          _play_session_row.SetModified();
        }
        else
        {
          _play_session_row.EndEdit();
        }

        _result = true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _result;
    } // Update

    //------------------------------------------------------------------------------
    // PURPOSE : Update DataTable Rows Pla Sessions to DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data is updated correctly or not.
    //
    private Boolean DB_Update(SqlTransaction Trx)
    {
      Boolean _error;
      DataRow[] _errors;
      DataRow[] _rows_to_update;
      Int32 _num_rows_deleted;
      IWcpTaskStatus _wcp_task_status;
      Int64 _elapsed;

      _error = false;

      try
      {
        while (true)
        {
          // If no rows to update, return depending of _error variable.
          if (m_dt_play_sessions.Rows.Count == 0)
          {
            return !_error;
          }

          _rows_to_update = m_dt_play_sessions.Select("", "", DataViewRowState.ModifiedCurrent);

          try
          {
            Trx.Save(ROLLBACK_POINT);

            m_adap_play_sessions_modify.UpdateCommand.Connection = Trx.Connection;
            m_adap_play_sessions_modify.UpdateCommand.Transaction = Trx;

            _error = true;
            try
            {
              // Update rows to DB -- Time consuming
              if (m_adap_play_sessions_modify.Update(m_dt_play_sessions) == _rows_to_update.Length)
              {
                _error = false;
              }
            }
            catch (Exception ex)
            {
              Log.Warning("Exception 1: BU_ReportPlaySessionMeters.DB_Update");
              Log.Exception(ex);
            }

            if (!_error)
            {
              // Everything is ok.
              foreach (DataRow _row in m_dt_play_sessions.Rows)
              {
                _wcp_task_status = (IWcpTaskStatus)_row[IDX_RESPONSE_CODE];
                _wcp_task_status.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
              }

              _elapsed = Misc.GetElapsedTicks(m_last_check);
              if (_elapsed >= TIME_TO_CHECK_IN_MS)
              {
                foreach (DataRow _row in m_dt_play_sessions.Rows)
                {
                  _elapsed = Misc.GetElapsedTicks((Int32)_row[IDX_TICK_COUNT]);
                  if (_elapsed >= TIME_TO_CHECK_IN_MS)
                  {
                    _row.Delete();
                  }
                }
                m_dt_play_sessions.AcceptChanges();

                m_last_check = Misc.GetTickCount();
              }

              return true;
            } // if (!_error)

            Log.Warning("------------- BEGIN PLAY_SESSION NOT UPDATED -------------");

            _num_rows_deleted = 0;
            _errors = m_dt_play_sessions.GetErrors();

            foreach (DataRow _row_error in _errors)
            {
              Log.Warning("PlaySessionId: " + _row_error[IDX_PLAY_SESSION_ID] +
                          ", TerminalId: " + _row_error[IDX_TERMINAL_ID] +
                          ". Details: " + _row_error.RowError);
              _row_error.Delete();
              _num_rows_deleted++;
            }

            if (_num_rows_deleted == 0)
            {
              m_dt_play_sessions.Clear();
            }

            m_dt_play_sessions.AcceptChanges();
            foreach (DataRow _row in m_dt_play_sessions.Rows)
            {
              _row.SetModified();
            }

            Log.Warning("------------- END   PLAY_SESSION NOT UPDATED -------------");

            Trx.Rollback(ROLLBACK_POINT);

          } // try
          catch (Exception _ex)
          {
            Log.Warning("Exception 2: BU_ReportPlaySessionMeters.DB_Update");
            Log.Exception(_ex);
            return false;
          }
        } // while (true)
      } // try
      catch (Exception _ex)
      {
        Log.Warning("Exception 3: BU_ReportPlaySessionMeters.DB_Update");
        Log.Exception(_ex);
        return false;
      }
    } // DB_Update

    private Boolean GetAccountId(String ExternalTrackData, out Int64 AccountId, SqlTransaction Trx)
    {
      Object _obj;
      String _internal_track_data;
      Int32 _card_type;

      AccountId = 0;
      try
      {

        if (!WSI.Common.CardNumber.TrackDataToInternal(ExternalTrackData, out _internal_track_data, out _card_type))
        {

          return false;
        }

        using (SqlCommand _cmd = new SqlCommand("SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData", Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).Value = _internal_track_data;
          _obj = _cmd.ExecuteScalar();
          if (_obj == null)
          {
            return false;
          }

          AccountId = (Int64)_obj;
        }

        return true;
      }
      catch
      { }

      return false;
    }

    private Boolean DB_GetPlaySessionStatus(Int64 PlaySessionId, out PlaySessionStatus Status, SqlTransaction Trx)
    {
      Object _obj;

      Status = PlaySessionStatus.Closed;
      try
      {

        using (SqlCommand _cmd = new SqlCommand("SELECT PS_STATUS FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @pPlaySessionId", Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _obj = _cmd.ExecuteScalar();
          if (_obj == null)
          {
            return false;
          }

          Status = (PlaySessionStatus)_obj;
        }

        return true;
      }
      catch
      { }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get terminal data.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String ExternalTerminalId
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //          - SessionTerminal OutTerminal
    //
    // RETURNS :
    //      - Boolean: If correctly or not.
    //
    private Boolean GetTerminalData(String ExternalTerminalId, out SessionTerminal OutTerminal, SqlTransaction Trx)
    {
      StringBuilder _sb;

      OutTerminal = null;

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0) ");
      _sb.AppendLine("       , TE_TERMINAL_ID ");
      _sb.AppendLine("       , TE_TERMINAL_TYPE ");
      _sb.AppendLine("       , TE_NAME ");
      _sb.AppendLine("  FROM   TERMINALS ");
      _sb.AppendLine(" WHERE   TE_EXTERNAL_ID = @pExternalTerminalId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pExternalTerminalId", SqlDbType.NVarChar).Value = ExternalTerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              OutTerminal = new SessionTerminal();

              OutTerminal.AccountId = _reader.GetInt64(0);
              OutTerminal.terminal_id = _reader.GetInt32(1);
              OutTerminal.terminal_type = _reader.GetInt16(2);
              OutTerminal.name = _reader.GetString(3);

              return true;
            }
          }
        }
      }
      catch
      { }

      return false;
    } // GetTerminalData

    #endregion // Private Methods
  }
}
