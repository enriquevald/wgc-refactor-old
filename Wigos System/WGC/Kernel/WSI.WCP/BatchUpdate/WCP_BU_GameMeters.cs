//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_BU_GameMeters.cs
// 
//   DESCRIPTION: WCP_BU_GameMeters class
// 
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 09-JUL-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-JUL-2010 RCI    First release.
// 14-DEC-2011 ACC    Only One Game per Sas Id in memory
// 24-JUL-2012 ACC    Extended Meters: Add maximum meter values. Delta meters is zero if any maximum changed.
// 07-AUG-2014 ACC    Add Progressive Jackpot Cents into Meters
// 07-SEP-2014 ACC    Check Progressive special machines
// 30-MAR-2015 MPO    WIG-2183 - Expression filter for datatables need a parse
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;

namespace WSI.WCP
{
  public static class WCP_BU_GameMeters
  {

    #region Constants

    private const Int16 COLUMN_TERMINAL_ID = 0;
    private const Int16 COLUMN_GAME_BASE_NAME = 1;
    private const Int16 COLUMN_SEQUENCE_ID = 2;
    private const Int16 COLUMN_DENOMINATION = 3;
    private const Int16 COLUMN_PLAYED_COUNT = 4;
    private const Int16 COLUMN_PLAYED_AMOUNT = 5;
    private const Int16 COLUMN_WON_COUNT = 6;
    private const Int16 COLUMN_WON_AMOUNT = 7;
    private const Int16 COLUMN_JACKPOT_AMOUNT = 8;
    private const Int16 COLUMN_LAST_REPORTED = 9;
    private const Int16 COLUMN_DELTA_GAME_NAME = 10;
    private const Int16 COLUMN_DELTA_PLAYED_COUNT = 11;
    private const Int16 COLUMN_DELTA_PLAYED_AMOUNT = 12;
    private const Int16 COLUMN_DELTA_WON_COUNT = 13;
    private const Int16 COLUMN_DELTA_WON_AMOUNT = 14;
    private const Int16 COLUMN_DELTA_JACKPOT_AMOUNT = 15;
    private const Int16 COLUMN_OLD_PLAYED_COUNT = 16;
    private const Int16 COLUMN_OLD_PLAYED_AMOUNT = 17;
    private const Int16 COLUMN_OLD_WON_COUNT = 18;
    private const Int16 COLUMN_OLD_WON_AMOUNT = 19;
    private const Int16 COLUMN_OLD_JACKPOT_AMOUNT = 20;
    private const Int16 COLUMN_RESPONSE_CODE = 21;
    private const Int16 COLUMN_PROVIDER_ID = 22;
    private const Int16 COLUMN_TERMINAL_NAME = 23;
    private const Int16 COLUMN_MAX_PLAYED_COUNT = 24;
    private const Int16 COLUMN_MAX_PLAYED_AMOUNT = 25;
    private const Int16 COLUMN_MAX_WON_COUNT = 26;
    private const Int16 COLUMN_MAX_WON_AMOUNT = 27;
    private const Int16 COLUMN_MAX_JACKPOT_AMOUNT = 28;
    private const Int16 COLUMN_PROGRESSIVE_JACKPOT_AMOUNT = 29;
    private const Int16 COLUMN_DELTA_PROGRESSIVE_JACKPOT_AMOUNT = 30;
    private const Int16 COLUMN_OLD_PROGRESSIVE_JACKPOT_AMOUNT = 31;
    private const Int16 COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT = 32;

    #endregion // Constants

    #region Attributes

    private static DataTable m_dt_game_meters = null;
    private static DataTable m_dt_handpays = null;

    private static SqlDataAdapter m_adap_game_meters = null;
    private static SqlDataAdapter m_adap_gm_select = null;
    private static SqlDataAdapter m_adap_gm_select_last_with_this_sas_id = null;
    private static SqlDataAdapter m_adap_handpays = null;

    private static ReaderWriterLock m_rw_lock_queue = new ReaderWriterLock();
    private static ArrayList m_unload_queue = new ArrayList(10);

    public static Int32 m_time_num_all = 0;
    public static Int32 m_time_num_loads = 0;

    public static Int64 m_time_all = 0;
    public static Int64 m_time_update_session = 0;
    public static Int64 m_time_gets = 0;
    public static Int64 m_time_updates = 0;
    public static Int64 m_time_register_term_game = 0;
    public static Int64 m_time_updateDB = 0;
    public static Int64 m_time_commit = 0;
    public static Int64 m_time_response = 0;
    public static Int64 m_time_task_process = 0;
    public static Int64 m_time_begin_trx = 0;
    public static Int64 m_time_unload = 0;
    public static Int64 m_time_loads = 0;
    public static Int64 m_time_inserts = 0;

    #endregion // Attributes

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init attributes of the static class.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      SqlCommand _sql_cmd_select;
      SqlCommand _sql_cmd_insert;
      SqlCommand _sql_cmd_update;
      String _sql_query;

      // Already init.
      if (m_dt_game_meters != null)
      {
        return;
      }

      // Create DataTable Columns

      m_dt_game_meters = new DataTable("GAME_METERS");

      m_dt_game_meters.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      m_dt_game_meters.Columns.Add("GAME_BASE_NAME", Type.GetType("System.String"));
      m_dt_game_meters.Columns.Add("SEQUENCE_ID", Type.GetType("System.Int64"));
      m_dt_game_meters.Columns.Add("DENOMINATION", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_game_meters.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("WON_COUNT", Type.GetType("System.Int64"));
      m_dt_game_meters.Columns.Add("WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("LAST_REPORTED", Type.GetType("System.DateTime"));
      m_dt_game_meters.Columns.Add("DELTA_GAME_NAME", Type.GetType("System.String"));
      m_dt_game_meters.Columns.Add("DELTA_PLAYED_COUNT", Type.GetType("System.Int64")).DefaultValue = 0;
      m_dt_game_meters.Columns.Add("DELTA_PLAYED_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_game_meters.Columns.Add("DELTA_WON_COUNT", Type.GetType("System.Int64")).DefaultValue = 0;
      m_dt_game_meters.Columns.Add("DELTA_WON_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_game_meters.Columns.Add("DELTA_JACKPOT_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_game_meters.Columns.Add("OLD_PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_game_meters.Columns.Add("OLD_PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("OLD_WON_COUNT", Type.GetType("System.Int64"));
      m_dt_game_meters.Columns.Add("OLD_WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("OLD_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("RESPONSE_CODE", Type.GetType("System.Object"));

      m_dt_game_meters.Columns.Add("PROVIDER_ID", Type.GetType("System.String"));
      m_dt_game_meters.Columns.Add("TERMINAL_NAME", Type.GetType("System.String"));

      m_dt_game_meters.Columns.Add("MAX_PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_game_meters.Columns.Add("MAX_PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("MAX_WON_COUNT", Type.GetType("System.Int64"));
      m_dt_game_meters.Columns.Add("MAX_WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("MAX_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("PROGRESSIVE_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("DELTA_PROGRESSIVE_JACKPOT_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_game_meters.Columns.Add("OLD_PROGRESSIVE_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("MAX_PROGRESSIVE_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_game_meters.Columns.Add("TE_ISO_CODE", Type.GetType("System.String"));

      // Primary Key for DataTable GameMeters.
      m_dt_game_meters.PrimaryKey = new DataColumn[] {m_dt_game_meters.Columns["TERMINAL_ID"], 
                                                      m_dt_game_meters.Columns["GAME_BASE_NAME"]};

      m_dt_handpays = m_dt_game_meters.Clone();


      // Create SelectCommand for GAME_METERS, and create SqlDataAdapter ONLY for Select.

      _sql_query = "SELECT   GM_TERMINAL_ID         AS TERMINAL_ID " +
                         " , GM_GAME_BASE_NAME      AS GAME_BASE_NAME " +
                         " , GM_WCP_SEQUENCE_ID     AS SEQUENCE_ID " +
                         " , GM_DENOMINATION        AS DENOMINATION " +
                         " , GM_PLAYED_COUNT        AS PLAYED_COUNT " +
                         " , GM_PLAYED_AMOUNT       AS PLAYED_AMOUNT " +
                         " , GM_WON_COUNT           AS WON_COUNT " +
                         " , GM_WON_AMOUNT          AS WON_AMOUNT " +
                         " , GM_JACKPOT_AMOUNT      AS JACKPOT_AMOUNT " +
                         " , GM_LAST_REPORTED       AS LAST_REPORTED " +
                         " , TE_NAME                AS TERMINAL_NAME " +
                         " , TE_PROVIDER_ID         AS PROVIDER_ID " +
                         " , GM_MAX_PLAYED_COUNT    AS MAX_PLAYED_COUNT " +
                         " , GM_MAX_PLAYED_AMOUNT   AS MAX_PLAYED_AMOUNT " +
                         " , GM_MAX_WON_COUNT       AS MAX_WON_COUNT " +
                         " , GM_MAX_WON_AMOUNT      AS MAX_WON_AMOUNT " +
                         " , GM_MAX_JACKPOT_AMOUNT  AS MAX_JACKPOT_AMOUNT " +
                         " , GM_PROGRESSIVE_JACKPOT_AMOUNT      AS PROGRESSIVE_JACKPOT_AMOUNT " +
                         " , GM_MAX_PROGRESSIVE_JACKPOT_AMOUNT  AS MAX_PROGRESSIVE_JACKPOT_AMOUNT " +
                         " , TE_ISO_CODE            AS TE_ISO_CODE " +
                    " FROM   GAME_METERS, TERMINALS " +
                   " WHERE   GM_TERMINAL_ID     = @pTerminalId " +
                   "   AND   GM_GAME_BASE_NAME  = @pGameBaseName " +
                   "   AND   GM_TERMINAL_ID     = TE_TERMINAL_ID ";

      _sql_cmd_select = new SqlCommand(_sql_query);
      _sql_cmd_select.Parameters.Add("@pTerminalId", SqlDbType.Int);
      _sql_cmd_select.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar);

      m_adap_gm_select = new SqlDataAdapter();
      m_adap_gm_select.SelectCommand = _sql_cmd_select;
      m_adap_gm_select.InsertCommand = null;
      m_adap_gm_select.UpdateCommand = null;
      m_adap_gm_select.DeleteCommand = null;

      //
      // ACC 14-DEC-2011 Only One Game per Sas Id in memory
      //
      _sql_query = "SELECT TOP 1 GM_TERMINAL_ID         AS TERMINAL_ID " +
                             " , GM_GAME_BASE_NAME      AS GAME_BASE_NAME " +
                             " , GM_WCP_SEQUENCE_ID     AS SEQUENCE_ID " +
                             " , GM_DENOMINATION        AS DENOMINATION " +
                             " , GM_PLAYED_COUNT        AS PLAYED_COUNT " +
                             " , GM_PLAYED_AMOUNT       AS PLAYED_AMOUNT " +
                             " , GM_WON_COUNT           AS WON_COUNT " +
                             " , GM_WON_AMOUNT          AS WON_AMOUNT " +
                             " , GM_JACKPOT_AMOUNT      AS JACKPOT_AMOUNT " +
                             " , GM_LAST_REPORTED       AS LAST_REPORTED " +
                             " , TE_NAME                AS TERMINAL_NAME " +
                             " , TE_PROVIDER_ID         AS PROVIDER_ID " +
                             " , GM_MAX_PLAYED_COUNT    AS MAX_PLAYED_COUNT " +
                             " , GM_MAX_PLAYED_AMOUNT   AS MAX_PLAYED_AMOUNT " +
                             " , GM_MAX_WON_COUNT       AS MAX_WON_COUNT " +
                             " , GM_MAX_WON_AMOUNT      AS MAX_WON_AMOUNT " +
                             " , GM_MAX_JACKPOT_AMOUNT  AS MAX_JACKPOT_AMOUNT " +
                             " , GM_PROGRESSIVE_JACKPOT_AMOUNT      AS PROGRESSIVE_JACKPOT_AMOUNT " +
                             " , GM_MAX_PROGRESSIVE_JACKPOT_AMOUNT  AS MAX_PROGRESSIVE_JACKPOT_AMOUNT " +
                             " , TE_ISO_CODE            AS TE_ISO_CODE " +
                    " FROM   GAME_METERS, TERMINALS " +
                   " WHERE   GM_TERMINAL_ID     = @pTerminalId " +
                   "   AND   GM_GAME_BASE_NAME  like @pSasId " +
                   "   AND   GM_TERMINAL_ID     = TE_TERMINAL_ID " +
                " ORDER BY   GM_LAST_REPORTED DESC ";

      _sql_cmd_select = new SqlCommand(_sql_query);
      _sql_cmd_select.Parameters.Add("@pTerminalId", SqlDbType.Int);
      _sql_cmd_select.Parameters.Add("@pSasId", SqlDbType.NVarChar);

      m_adap_gm_select_last_with_this_sas_id = new SqlDataAdapter();
      m_adap_gm_select_last_with_this_sas_id.SelectCommand = _sql_cmd_select;
      m_adap_gm_select_last_with_this_sas_id.InsertCommand = null;
      m_adap_gm_select_last_with_this_sas_id.UpdateCommand = null;
      m_adap_gm_select_last_with_this_sas_id.DeleteCommand = null;


      // Create Insert and Update Command for GAME_METERS, and create SqlDataAdapter.

      // INSERT GAME_METERS
      _sql_query = "INSERT INTO GAME_METERS ( GM_TERMINAL_ID " +
                                          " , GM_GAME_BASE_NAME " +
                                          " , GM_WCP_SEQUENCE_ID " +
                                          " , GM_DENOMINATION " +
                                          " , GM_PLAYED_COUNT " +
                                          " , GM_PLAYED_AMOUNT " +
                                          " , GM_WON_COUNT " +
                                          " , GM_WON_AMOUNT " +
                                          " , GM_JACKPOT_AMOUNT " +
                                          " , GM_PROGRESSIVE_JACKPOT_AMOUNT " +
                                          " , GM_MAX_PLAYED_COUNT " +
                                          " , GM_MAX_PLAYED_AMOUNT " +
                                          " , GM_MAX_WON_COUNT " +
                                          " , GM_MAX_WON_AMOUNT " +
                                          " , GM_MAX_JACKPOT_AMOUNT " +
                                          " , GM_MAX_PROGRESSIVE_JACKPOT_AMOUNT " +
                                          " , GM_LAST_REPORTED ) " +
                                  " VALUES  ( @pTerminalId " +
                                          " , @pGameBaseName " +
                                          " , @pWcpSequenceId " +
                                          " , @pDenomination " +
                                          " , @pNewPlayedCount " +
                                          " , @pNewPlayedAmount " +
                                          " , @pNewWonCount " +
                                          " , @pNewWonAmount " +
                                          " , @pNewJackpotAmount " +
                                          " , @pNewProgressiveJackpotAmount " +
                                          " , @pMaxPlayedCount " +
                                          " , @pMaxPlayedAmount " +
                                          " , @pMaxWonCount " +
                                          " , @pMaxWonAmount " +
                                          " , @pMaxJackpotAmount " +
                                          " , @pMaxProgressiveJackpotAmount " +
                                          " , GETDATE() ) ";

      _sql_cmd_insert = new SqlCommand(_sql_query);
      _sql_cmd_insert.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _sql_cmd_insert.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar).SourceColumn = "GAME_BASE_NAME";
      _sql_cmd_insert.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
      _sql_cmd_insert.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "DENOMINATION";
      _sql_cmd_insert.Parameters.Add("@pNewPlayedCount", SqlDbType.BigInt).SourceColumn = "PLAYED_COUNT";
      _sql_cmd_insert.Parameters.Add("@pNewPlayedAmount", SqlDbType.Money).SourceColumn = "PLAYED_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pNewWonCount", SqlDbType.BigInt).SourceColumn = "WON_COUNT";
      _sql_cmd_insert.Parameters.Add("@pNewWonAmount", SqlDbType.Money).SourceColumn = "WON_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pNewJackpotAmount", SqlDbType.Money).SourceColumn = "JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pNewProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxPlayedCount", SqlDbType.BigInt).SourceColumn = "MAX_PLAYED_COUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxPlayedAmount", SqlDbType.Money).SourceColumn = "MAX_PLAYED_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxWonCount", SqlDbType.BigInt).SourceColumn = "MAX_WON_COUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxWonAmount", SqlDbType.Money).SourceColumn = "MAX_WON_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxJackpotAmount", SqlDbType.Money).SourceColumn = "MAX_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "MAX_PROGRESSIVE_JACKPOT_AMOUNT";

      // UPDATE GAME_METERS
      _sql_query = "UPDATE GAME_METERS SET GM_WCP_SEQUENCE_ID        = @pWcpSequenceId " +
                                       " , GM_DENOMINATION           = @pDenomination " +
                                       " , GM_PLAYED_COUNT           = @pNewPlayedCount " +
                                       " , GM_PLAYED_AMOUNT          = @pNewPlayedAmount " +
                                       " , GM_WON_COUNT              = @pNewWonCount " +
                                       " , GM_WON_AMOUNT             = @pNewWonAmount " +
                                       " , GM_JACKPOT_AMOUNT         = @pNewJackpotAmount " +
                                       " , GM_PROGRESSIVE_JACKPOT_AMOUNT         = @pNewProgressiveJackpotAmount " +
                                       " , GM_MAX_PLAYED_COUNT       = @pMaxPlayedCount " +
                                       " , GM_MAX_PLAYED_AMOUNT      = @pMaxPlayedAmount " +
                                       " , GM_MAX_WON_COUNT          = @pMaxWonCount " +
                                       " , GM_MAX_WON_AMOUNT         = @pMaxWonAmount " +
                                       " , GM_MAX_JACKPOT_AMOUNT     = @pMaxJackpotAmount " +
                                       " , GM_MAX_PROGRESSIVE_JACKPOT_AMOUNT     = @pMaxProgressiveJackpotAmount " +
                                       " , GM_LAST_REPORTED          = GETDATE() " +
                                       " , GM_DELTA_GAME_NAME        = @pDeltaGameName " +
                                       " , GM_DELTA_PLAYED_COUNT     = GM_DELTA_PLAYED_COUNT    + @pDeltaPlayedCount " +
                                       " , GM_DELTA_PLAYED_AMOUNT    = GM_DELTA_PLAYED_AMOUNT   + @pDeltaPlayedAmount " +
                                       " , GM_DELTA_WON_COUNT        = GM_DELTA_WON_COUNT       + @pDeltaWonCount " +
                                       " , GM_DELTA_WON_AMOUNT       = GM_DELTA_WON_AMOUNT      + @pDeltaWonAmount " +
                                       " , GM_DELTA_JACKPOT_AMOUNT   = GM_DELTA_JACKPOT_AMOUNT  + @pDeltaJackpotAmount " +
                                       " , GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT  = ISNULL(GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT, 0) + @pDeltaProgressiveJackpotAmount " +
                   " WHERE   GM_TERMINAL_ID     = @pTerminalId   " +
                     " AND   GM_GAME_BASE_NAME  = @pGameBaseName " +
                     " AND   GM_PLAYED_COUNT    = @pOldPlayedCount " +
                     " AND   GM_PLAYED_AMOUNT   = @pOldPlayedAmount " +
                     " AND   GM_WON_COUNT       = @pOldWonCount " +
                     " AND   GM_WON_AMOUNT      = @pOldWonAmount " +
                     " AND   GM_JACKPOT_AMOUNT  = @pOldJackpotAmount " +
                     " AND   ((GM_PROGRESSIVE_JACKPOT_AMOUNT      = @pOldProgressiveJackpotAmount) OR (GM_PROGRESSIVE_JACKPOT_AMOUNT IS NULL AND @pOldProgressiveJackpotAmount IS NULL))  ";

      _sql_cmd_update = new SqlCommand(_sql_query);
      _sql_cmd_update.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
      _sql_cmd_update.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "DENOMINATION";
      _sql_cmd_update.Parameters.Add("@pNewPlayedCount", SqlDbType.BigInt).SourceColumn = "PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewPlayedAmount", SqlDbType.Money).SourceColumn = "PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewWonCount", SqlDbType.BigInt).SourceColumn = "WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewWonAmount", SqlDbType.Money).SourceColumn = "WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewJackpotAmount", SqlDbType.Money).SourceColumn = "JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _sql_cmd_update.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar).SourceColumn = "GAME_BASE_NAME";
      _sql_cmd_update.Parameters.Add("@pDeltaGameName", SqlDbType.NVarChar).SourceColumn = "DELTA_GAME_NAME";
      _sql_cmd_update.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).SourceColumn = "DELTA_PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).SourceColumn = "DELTA_PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).SourceColumn = "DELTA_WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).SourceColumn = "DELTA_WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).SourceColumn = "DELTA_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "DELTA_PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldPlayedCount", SqlDbType.BigInt).SourceColumn = "OLD_PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldPlayedAmount", SqlDbType.Money).SourceColumn = "OLD_PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldWonCount", SqlDbType.BigInt).SourceColumn = "OLD_WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldWonAmount", SqlDbType.Money).SourceColumn = "OLD_WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldJackpotAmount", SqlDbType.Money).SourceColumn = "OLD_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "OLD_PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxPlayedCount", SqlDbType.BigInt).SourceColumn = "MAX_PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pMaxPlayedAmount", SqlDbType.Money).SourceColumn = "MAX_PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxWonCount", SqlDbType.BigInt).SourceColumn = "MAX_WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pMaxWonAmount", SqlDbType.Money).SourceColumn = "MAX_WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxJackpotAmount", SqlDbType.Money).SourceColumn = "MAX_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "MAX_PROGRESSIVE_JACKPOT_AMOUNT";

      m_adap_game_meters = new SqlDataAdapter();
      m_adap_game_meters.SelectCommand = null;
      m_adap_game_meters.InsertCommand = _sql_cmd_insert;
      m_adap_game_meters.UpdateCommand = _sql_cmd_update;
      m_adap_game_meters.DeleteCommand = null;
      m_adap_game_meters.ContinueUpdateOnError = true;

      m_adap_game_meters.UpdateBatchSize = 500;
      m_adap_game_meters.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
      m_adap_game_meters.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      //INSERT HANDPAY with HP_TYPE MachineReported = 0.

      _sql_query = "INSERT INTO HANDPAYS (HP_TERMINAL_ID " +
                                      " , HP_DATETIME " +
                                      " , HP_PREVIOUS_METERS " +
                                      " , HP_AMOUNT " +
                                      " , HP_TE_NAME " +
                                      " , HP_TE_PROVIDER_ID " +
                                      " , HP_TYPE " +
                                      " , HP_AMT0 " +
                                      " , HP_CUR0 ) " +
                                " VALUES (@TerminalId " +
                                      " , GETDATE() " +
                                      " , @PreviousMeters " +
                                      " , dbo.ApplyExchange2(@pDeltaJackpotAmount, @pTeISOCode, @pNatCur) " + // HP_AMOUNT
                                      " , @TermName " +
                                      " , @ProviderId " +
                                      " , " + ((int)HANDPAY_TYPE.JACKPOT) + " " +
                                      " , @pDeltaJackpotAmount " + // HP_AMT0
                                      " , @pTeISOCode ) "; // HP_CUR0

      _sql_cmd_insert = new SqlCommand(_sql_query);
      _sql_cmd_insert.Parameters.Add("@TerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _sql_cmd_insert.Parameters.Add("@PreviousMeters", SqlDbType.DateTime).SourceColumn = "LAST_REPORTED";
      
      _sql_cmd_insert.Parameters.Add("@TermName", SqlDbType.NVarChar).SourceColumn = "TERMINAL_NAME";
      _sql_cmd_insert.Parameters.Add("@ProviderId", SqlDbType.NVarChar).SourceColumn = "PROVIDER_ID";

      _sql_cmd_insert.Parameters.Add("@pNatCur", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency();
      _sql_cmd_insert.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).SourceColumn = "DELTA_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pTeISOCode", SqlDbType.NVarChar,3).SourceColumn = "TE_ISO_CODE"; 

      m_adap_handpays = new SqlDataAdapter();
      m_adap_handpays.SelectCommand = null;
      m_adap_handpays.InsertCommand = _sql_cmd_insert;
      m_adap_handpays.UpdateCommand = null;
      m_adap_handpays.DeleteCommand = null;
      m_adap_handpays.ContinueUpdateOnError = true;

      m_adap_handpays.UpdateBatchSize = 500;
      m_adap_handpays.InsertCommand.UpdatedRowSource = UpdateRowSource.None;



    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Load GameMeters data from DB to DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Load(Int32 TerminalId, String GameBaseName, SqlTransaction Trx)
    {
      int _ticks;

      _ticks = Environment.TickCount;

      m_adap_gm_select.SelectCommand.Connection = Trx.Connection;
      m_adap_gm_select.SelectCommand.Transaction = Trx;
      m_adap_gm_select.SelectCommand.Parameters["@pTerminalId"].Value = TerminalId;
      m_adap_gm_select.SelectCommand.Parameters["@pGameBaseName"].Value = GameBaseName;

      m_adap_gm_select.Fill(m_dt_game_meters);

      m_time_loads += Misc.GetElapsedTicks(_ticks);
      m_time_num_loads++;


    } // Load

    //------------------------------------------------------------------------------
    // PURPOSE : Unqueue a Unload petition for TerminalId.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void EnqueueUnload(Int32 TerminalId)
    {
      try
      {
        m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);

        m_unload_queue.Add(TerminalId);
      }
      finally
      {
        m_rw_lock_queue.ReleaseWriterLock();
      }
    } // EnqueueUnload

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows related to the TerminalId from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean Unload(Int32 TerminalId)
    {
      String _filter;

      _filter = "TERMINAL_ID = " + TerminalId;

      return Unload(_filter);
    } // Unload

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows related to the TerminalId-GameBaseName from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - GameBaseName
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean Unload(Int32 TerminalId, String GameBaseName)
    {
      String _filter;

      _filter = "TERMINAL_ID = " + TerminalId + " AND GAME_BASE_NAME = '" + ParseValueForDT(GameBaseName) + "'";

      return Unload(_filter);
    } // Unload

    //------------------------------------------------------------------------------
    // PURPOSE : Get Game Meters from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - GameBaseName
    //          - Trx
    //
    //      - OUTPUT :
    //          - DbGameMeters
    //          - DbWcpSequenceId
    //          - RowGameMeters
    //
    // RETURNS :
    //      - true: successful.
    //      - false: not found or error.
    //
    public static Boolean Get(Int32 TerminalId, String GameBaseName, out GameMeters DbGameMeters,
                              out Int64 DbWcpSequenceId, out DataRow RowGameMeters, SqlTransaction Trx)
    {
      String _filter;
      DataRow[] _term_rows;
      DataRow _term_row;
      int _ticks;

      _ticks = Environment.TickCount;

      DbGameMeters = new GameMeters();
      DbWcpSequenceId = 0;
      RowGameMeters = null;

      try
      {
        if (m_dt_game_meters == null)
        {
          throw new Exception("Get: BU_GameMeters not initialized. Call Init first!");
        }

        _term_rows = null;
        _filter = "TERMINAL_ID = " + TerminalId + " AND GAME_BASE_NAME = '" + ParseValueForDT(GameBaseName) + "'";

        for (int _idx_tries = 0; _idx_tries < 2; _idx_tries++)
        {
          _term_rows = m_dt_game_meters.Select(_filter);

          if (_term_rows.Length == 0)
          {
            if (_idx_tries == 0)
            {
              Load(TerminalId, GameBaseName, Trx);
            }
            else
            {
              //Log.Error("BU_GameMeters.Get: NOT Found Game Meters for TerminalId: " + TerminalId +
              //          ", GameBaseName: " + GameBaseName + " .");
              return false;
            }
          }
          else
          {
            break;
          }
        }
        if (_term_rows.Length > 1)
        {
          Log.Warning("BU_GameMeters.Get: Too many Game Meters for TerminalId: " + TerminalId +
                      ", GameBaseName: " + GameBaseName + " . Count: " + _term_rows.Length + ".");
        }

        _term_row = _term_rows[0];

        DbWcpSequenceId = (Int64)_term_row[COLUMN_SEQUENCE_ID];
        DbGameMeters.GameBaseName = (String)_term_row[COLUMN_GAME_BASE_NAME];
        DbGameMeters.DenominationCents = (Int32)((Decimal)_term_row[COLUMN_DENOMINATION] * 100);
        DbGameMeters.PlayedCount = (Int64)_term_row[COLUMN_PLAYED_COUNT];
        DbGameMeters.PlayedCents = (Int64)((Decimal)_term_row[COLUMN_PLAYED_AMOUNT] * 100);
        DbGameMeters.WonCount = (Int64)_term_row[COLUMN_WON_COUNT];
        DbGameMeters.WonCents = (Int64)((Decimal)_term_row[COLUMN_WON_AMOUNT] * 100);
        DbGameMeters.JackpotCents = (Int64)((Decimal)_term_row[COLUMN_JACKPOT_AMOUNT] * 100);

        if (!_term_row.IsNull(COLUMN_PROGRESSIVE_JACKPOT_AMOUNT))
        {
          DbGameMeters.ProgressiveJackpotCents = (Int64)((Decimal)_term_row[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] * 100);
        }
        else
        {
          DbGameMeters.ProgressiveJackpotCents = -1;
        }

        if (!_term_row.IsNull(COLUMN_MAX_PLAYED_COUNT))
        {
          DbGameMeters.PlayedCountMaxValue = (Int64)_term_row[COLUMN_MAX_PLAYED_COUNT];
          DbGameMeters.PlayedMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_PLAYED_AMOUNT] * 100);
          DbGameMeters.WonCountMaxValue = (Int64)_term_row[COLUMN_MAX_WON_COUNT];
          DbGameMeters.WonMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_WON_AMOUNT] * 100);
          DbGameMeters.JackpotMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_JACKPOT_AMOUNT] * 100);

          if (!_term_row.IsNull(COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT))
          {
            DbGameMeters.ProgressiveJackpotMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] * 100);
          }
          else
          {
            DbGameMeters.ProgressiveJackpotMaxValueCents = 0;
          }
        }
        else
        {
          DbGameMeters.PlayedCountMaxValue = -1;
          DbGameMeters.PlayedMaxValueCents = -1;
          DbGameMeters.WonCountMaxValue = -1;
          DbGameMeters.WonMaxValueCents = -1;
          DbGameMeters.JackpotMaxValueCents = -1;
          DbGameMeters.ProgressiveJackpotMaxValueCents = -1;
        }

        RowGameMeters = _term_row;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_time_gets += Misc.GetElapsedTicks(_ticks);
      }
    } // Get

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Game Meters received to the DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - NewGameMeter: Wcp message game meters
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: game meters inserted.
    //      - false: error.
    //
    public static Boolean Insert(Int32 TerminalId,
                                 String ProviderId,
                                 String Name,
                                 Int64 WcpSequenceId,
                                 GameMeters NewGameMeters,
                                 IWcpTaskStatus WcpTaskStatus)
    {
      DataRow _row;
      int _ticks;

      if (m_dt_game_meters == null)
      {
        throw new Exception("Insert: BU_GameMeters not initialized. Call Init first!");
      }

      _ticks = Environment.TickCount;

      try
      {
        _row = m_dt_game_meters.NewRow();

        _row[COLUMN_TERMINAL_ID] = TerminalId;
        _row[COLUMN_GAME_BASE_NAME] = NewGameMeters.GameBaseName;

        _row[COLUMN_PROVIDER_ID] = ProviderId;
        _row[COLUMN_TERMINAL_NAME] = Name;

        _row[COLUMN_SEQUENCE_ID] = WcpSequenceId;
        _row[COLUMN_DENOMINATION] = ((Decimal)NewGameMeters.DenominationCents) / 100;

        _row[COLUMN_PLAYED_COUNT] = NewGameMeters.PlayedCount;
        _row[COLUMN_PLAYED_AMOUNT] = ((Decimal)NewGameMeters.PlayedCents) / 100;
        _row[COLUMN_WON_COUNT] = NewGameMeters.WonCount;
        _row[COLUMN_WON_AMOUNT] = ((Decimal)NewGameMeters.WonCents) / 100;
        _row[COLUMN_JACKPOT_AMOUNT] = ((Decimal)NewGameMeters.JackpotCents) / 100;

        if (  NewGameMeters.ProgressiveJackpotCents == 0
           || NewGameMeters.ProgressiveJackpotCents == -1)
        {
          _row[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
        }
        else
        {
          _row[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)NewGameMeters.ProgressiveJackpotCents) / 100;
        }

        _row[COLUMN_MAX_PLAYED_COUNT] = NewGameMeters.PlayedCountMaxValue;
        _row[COLUMN_MAX_PLAYED_AMOUNT] = ((Decimal)NewGameMeters.PlayedMaxValueCents) / 100;
        _row[COLUMN_MAX_WON_COUNT] = NewGameMeters.WonCountMaxValue;
        _row[COLUMN_MAX_WON_AMOUNT] = ((Decimal)NewGameMeters.WonMaxValueCents) / 100;
        _row[COLUMN_MAX_JACKPOT_AMOUNT] = ((Decimal)NewGameMeters.JackpotMaxValueCents) / 100;

        if (  NewGameMeters.ProgressiveJackpotMaxValueCents == 0
           || NewGameMeters.ProgressiveJackpotMaxValueCents == -1)
        {
          _row[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
        }
        else
        {
          _row[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)NewGameMeters.ProgressiveJackpotMaxValueCents) / 100;
        }

        // Response Code
        _row[COLUMN_RESPONSE_CODE] = WcpTaskStatus;

        m_dt_game_meters.Rows.Add(_row);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_time_inserts += Misc.GetElapsedTicks(_ticks);
      }
    } // Insert

    //------------------------------------------------------------------------------
    // PURPOSE : Update Game Meters from the DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - NewGameMeters: Wcp message game meters
    //          - OldGameMeters
    //          - DeltaGameMeters
    //          - RowGameMeters
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: game meters updated.
    //      - false: not found or error.
    //
    public static Boolean Update(Int32 TerminalId, Int64 WcpSequenceId, GameMeters NewGameMeters,
                                 GameMeters OldGameMeters, GameMeters DeltaGameMeters, DataRow RowGameMeters,
                                 IWcpTaskStatus WcpTaskStatus)
    {
      int _ticks;

      if (m_dt_game_meters == null)
      {
        throw new Exception("Update: BU_GameMeters not initialized. Call Init first!");
      }

      _ticks = Environment.TickCount;

      try
      {
        RowGameMeters.BeginEdit();

        RowGameMeters[COLUMN_SEQUENCE_ID] = WcpSequenceId;
        RowGameMeters[COLUMN_DENOMINATION] = ((Decimal)NewGameMeters.DenominationCents) / 100;

        RowGameMeters[COLUMN_LAST_REPORTED] = WGDB.Now;

        RowGameMeters[COLUMN_PLAYED_COUNT] = NewGameMeters.PlayedCount;
        RowGameMeters[COLUMN_PLAYED_AMOUNT] = ((Decimal)NewGameMeters.PlayedCents) / 100;
        RowGameMeters[COLUMN_WON_COUNT] = NewGameMeters.WonCount;
        RowGameMeters[COLUMN_WON_AMOUNT] = ((Decimal)NewGameMeters.WonCents) / 100;
        RowGameMeters[COLUMN_JACKPOT_AMOUNT] = ((Decimal)NewGameMeters.JackpotCents) / 100;

        if (   NewGameMeters.ProgressiveJackpotCents == 0
            || NewGameMeters.ProgressiveJackpotCents == -1)
        {
          RowGameMeters[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
        }
        else
        {
          RowGameMeters[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)NewGameMeters.ProgressiveJackpotCents) / 100;
        }

        RowGameMeters[COLUMN_MAX_PLAYED_COUNT] = NewGameMeters.PlayedCountMaxValue;
        RowGameMeters[COLUMN_MAX_PLAYED_AMOUNT] = ((Decimal)NewGameMeters.PlayedMaxValueCents) / 100;
        RowGameMeters[COLUMN_MAX_WON_COUNT] = NewGameMeters.WonCountMaxValue;
        RowGameMeters[COLUMN_MAX_WON_AMOUNT] = ((Decimal)NewGameMeters.WonMaxValueCents) / 100;
        RowGameMeters[COLUMN_MAX_JACKPOT_AMOUNT] = ((Decimal)NewGameMeters.JackpotMaxValueCents) / 100;

        if (   NewGameMeters.ProgressiveJackpotMaxValueCents == 0
            || NewGameMeters.ProgressiveJackpotMaxValueCents == -1)
        {
          RowGameMeters[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
        }
        else
        {
          RowGameMeters[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)NewGameMeters.ProgressiveJackpotMaxValueCents) / 100;
        }

        RowGameMeters[COLUMN_OLD_PLAYED_COUNT] = OldGameMeters.PlayedCount;
        RowGameMeters[COLUMN_OLD_PLAYED_AMOUNT] = ((Decimal)OldGameMeters.PlayedCents) / 100;
        RowGameMeters[COLUMN_OLD_WON_COUNT] = OldGameMeters.WonCount;
        RowGameMeters[COLUMN_OLD_WON_AMOUNT] = ((Decimal)OldGameMeters.WonCents) / 100;
        RowGameMeters[COLUMN_OLD_JACKPOT_AMOUNT] = ((Decimal)OldGameMeters.JackpotCents) / 100;
        if (OldGameMeters.ProgressiveJackpotCents == -1)
        {
          RowGameMeters[COLUMN_OLD_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
        }
        else
        {
          RowGameMeters[COLUMN_OLD_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)OldGameMeters.ProgressiveJackpotCents) / 100;
        }

        RowGameMeters[COLUMN_DELTA_GAME_NAME] = DeltaGameMeters.GameId;
        RowGameMeters[COLUMN_DELTA_PLAYED_COUNT] = DeltaGameMeters.PlayedCount;
        RowGameMeters[COLUMN_DELTA_PLAYED_AMOUNT] = ((Decimal)DeltaGameMeters.PlayedCents) / 100;
        RowGameMeters[COLUMN_DELTA_WON_COUNT] = DeltaGameMeters.WonCount;
        RowGameMeters[COLUMN_DELTA_WON_AMOUNT] = ((Decimal)DeltaGameMeters.WonCents) / 100;
        RowGameMeters[COLUMN_DELTA_JACKPOT_AMOUNT] = ((Decimal)DeltaGameMeters.JackpotCents) / 100;
        RowGameMeters[COLUMN_DELTA_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)DeltaGameMeters.ProgressiveJackpotCents) / 100;

        // Response Code
        RowGameMeters[COLUMN_RESPONSE_CODE] = WcpTaskStatus;

        RowGameMeters.EndEdit();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_time_updates += Misc.GetElapsedTicks(_ticks);
      }
    } // Update

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update DataTable Rows Game Meters to DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data is inserted/updated correctly or not.
    //
    public static Boolean UpdateDB(SqlTransaction Trx)
    {
      String _rollback_point;
      String _filter;
      Boolean _error;
      Boolean _error_handpays;
      DataRow[] _rows_to_db;
      DataRow[] _rows_to_update;
      DataRow[] _rows_to_insert;
      DataRow[] _errors;
      DataRow[] _rows_cache;
      Int32 _num_rows_deleted;
      IWcpTaskStatus _wcp_task_status;
      Int32 _step;
      Currency _alarm_threshold;
      String _source_name;
      String _alarm;

      if (m_dt_game_meters == null)
      {
        Log.Error("UpdateDB: BU_GameMeters not initialized. Call Init first!");
        return false;
      }

      _rollback_point = "GAME_METERS_BEFORE_INSERT";
      _error = false;
      _step = 0;

      try
      {
        while (true)
        {
          _rows_to_db = m_dt_game_meters.Select("", "TERMINAL_ID, GAME_BASE_NAME", DataViewRowState.Added | DataViewRowState.ModifiedCurrent);

          // If no rows to insert/update, return depending of _error variable.
          if (_rows_to_db.Length == 0)
          {
            return !_error;
          }

          // Need to to this here, before adapter.Update().
          // In case of error, rows correctly updated/inserted must be marked again with its RowState.
          _rows_to_update = m_dt_game_meters.Select("", "TERMINAL_ID, GAME_BASE_NAME", DataViewRowState.ModifiedCurrent);
          _rows_to_insert = m_dt_game_meters.Select("", "TERMINAL_ID, GAME_BASE_NAME", DataViewRowState.Added);

          try
          {
            Trx.Save(_rollback_point);

            m_adap_game_meters.InsertCommand.Connection = Trx.Connection;
            m_adap_game_meters.InsertCommand.Transaction = Trx;
            m_adap_game_meters.UpdateCommand.Connection = Trx.Connection;
            m_adap_game_meters.UpdateCommand.Transaction = Trx;

            _error = true;
            _error_handpays = false;

            _step = 1;

            try
            {
              // Insert / Update rows to DB -- Time consuming
              if (m_adap_game_meters.Update(_rows_to_db) == _rows_to_db.Length)
              {
                _error = false;
              }
            }
            catch (Exception ex)
            {
              Log.Warning("Exception 1: WCP_BU_GameMeters.UpdateDB.");
              Log.Exception(ex);
            }

            if (!_error)
            {
              // Everything is ok.

              _step = 2;

              // Insert Handpays for correct GameMeters.

              // Get rows, in another DataTable, with pending Handpays.
              m_dt_handpays.Clear();

              _step = 21;

              foreach (DataRow _r in _rows_to_db)
              {
                // Handpays arrive in JACKPOT game meter counter.
                if ((Decimal)_r[COLUMN_DELTA_JACKPOT_AMOUNT] > 0)
                {
                  Boolean _insert_hp;
                  WSI.Common.Terminal.TerminalInfo _terminal_info;

                  _insert_hp = true;
                  if (WSI.Common.Terminal.Trx_GetTerminalInfo(((Int32)_r[COLUMN_TERMINAL_ID]), out _terminal_info, Trx))
                  {
                    if (_terminal_info.SASFlagActivated(SAS_FLAGS.USE_HANDPAY_INFORMATION))
                    {
                      _insert_hp = false;
                    } // if
                  }

                  if (_insert_hp)
                  {
                    // AcceptChanges(), SetAdded() is needed to ImportRow as Added. After that, AcceptChanges() again.
                    _r.AcceptChanges();
                    _r.SetAdded();
                    m_dt_handpays.ImportRow(_r);
                    _r.AcceptChanges();
                  }
                }

                //
                // Check Progressive special machines
                //
                if ((Decimal)_r[COLUMN_DELTA_PROGRESSIVE_JACKPOT_AMOUNT] > 0)
                {
                  WSI.Common.Terminal.TerminalInfo _terminal_info;

                  if (WSI.Common.Terminal.Trx_GetTerminalInfo(((Int32)_r[COLUMN_TERMINAL_ID]), out _terminal_info, Trx))
                  {
                    if (_terminal_info.SASFlagActivated(SAS_FLAGS.SPECIAL_PROGRESSIVE_METER))
                    {
                      if (GeneralParam.GetBoolean("Progressives", "Enabled", false)
                          && GeneralParam.GetBoolean("Progressives", "AutoRegister.OnSpecialMeterIncrement", false))
                      {
                        Int64 _progressive_id;

                        if (Progressives.GetProgressiveFromTerminal(((Int32)_r[COLUMN_TERMINAL_ID]), out _progressive_id, Trx))
                        {
                          if (!WSI.Common.Progressives.RegisterProgressiveJackpotAwarded(_progressive_id, 1, (Decimal)_r[COLUMN_DELTA_PROGRESSIVE_JACKPOT_AMOUNT], ((Int32)_r[COLUMN_TERMINAL_ID]), Trx))
                          {
                            Log.Warning("Progressives: RegisterProgressiveJackpotAwarded failed. TerminalId: " + ((Int32)_r[COLUMN_TERMINAL_ID]).ToString());
                          }
                        }
                        else
                        {
                          Log.Warning("Progressives: Special Terminal increment progressive and NOT progressive assigned. TerminalId: " + ((Int32)_r[COLUMN_TERMINAL_ID]).ToString());
                        }
                      } // if "Progressives"."AutoRegister.OnSpecialMeterIncrement"
                    } // if SAS_FLAGS.SPECIAL_PROGRESSIVE_METER
                  } // Trx_GetTerminalInfo
                } // COLUMN_DELTA_PROGRESSIVE_JACKPOT_AMOUNT > 0

              } // foreach

              _step = 3;

              if (m_dt_handpays.Rows.Count > 0)
              {
                m_adap_handpays.InsertCommand.Connection = Trx.Connection;
                m_adap_handpays.InsertCommand.Transaction = Trx;

                _error_handpays = true;
                _error = true;
                try
                {
                  // Insert Handpays rows to DB
                  if (m_adap_handpays.Update(m_dt_handpays) == m_dt_handpays.Rows.Count)
                  {
                    _error_handpays = false;
                    _error = false;

                    _alarm_threshold = GeneralParam.GetCurrency("Alarms", "HandpaysAmountThreshold", 0);

                    foreach (DataRow _row in m_dt_handpays.Rows)
                    {
                      // Check if handpay amount exceeds the amount defined on the GP that signals the alarm point
                      if ((Decimal)_row[COLUMN_DELTA_JACKPOT_AMOUNT] > _alarm_threshold && _alarm_threshold > 0)
                      {
                        _source_name = WSI.Common.Misc.ConcatProviderAndTerminal((String)_row[COLUMN_PROVIDER_ID], (String)_row[COLUMN_TERMINAL_NAME]);
                        _alarm = Resource.String("STR_ALARM_JACKPOT_AMOUNT_THRESHOLD", (((Currency)(Decimal)_row[COLUMN_DELTA_JACKPOT_AMOUNT])).ToString());

                        // Handpay amount exceedds GP amount. Insert alarm 
                        Alarm.Register(AlarmSourceCode.TerminalSystem,
                                       (Int32)_row[COLUMN_TERMINAL_ID],
                                       _source_name,
                                       (UInt32)AlarmCode.TerminalSystem_HandpayWarningAmount,
                                       _alarm,
                                       AlarmSeverity.Warning,
                                       WGDB.Now,
                                       Trx);
                      }
                    }
                  }
                }
                catch (Exception ex)
                {
                  Log.Warning("Exception Handpays: WCP_BU_GameMeters.UpdateDB.");
                  Log.Exception(ex);
                }
              }

              _step = 4;

              if (!_error_handpays)
              {
                foreach (DataRow _row in _rows_to_db)
                {
                  _wcp_task_status = (IWcpTaskStatus)_row[COLUMN_RESPONSE_CODE];
                  _wcp_task_status.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
                }
                return true;
              }
            } // if (!error)

            _step = 5;

            if (_error)
            {
              Log.Warning("------------- BEGIN GAME METERS NOT UPDATED -------------");

              _step = 6;

              if (_error_handpays)
              {
                _errors = m_dt_handpays.GetErrors();

                _step = 7;

                foreach (DataRow _row_error in _errors)
                {
                  _filter = "TERMINAL_ID = " + _row_error[COLUMN_TERMINAL_ID].ToString() +
                       " AND GAME_BASE_NAME = '" + ParseValueForDT(_row_error[COLUMN_GAME_BASE_NAME].ToString()) + "'";
                  _rows_cache = m_dt_game_meters.Select(_filter);
                  if (_rows_cache.Length > 0)
                  {
                    _rows_cache[0].RowError = "Handpay Error. Amount: " +
                                              _row_error[COLUMN_DELTA_JACKPOT_AMOUNT].ToString() +
                                              ", RowError: " + _row_error.RowError;
                    if (_rows_cache.Length > 1)
                    {
                      Log.Warning("    Handpay Error. Too many GameMeters for " +
                                  "TerminalId: " + _row_error[COLUMN_TERMINAL_ID].ToString() +
                                  ", GameBaseName: " + _row_error[COLUMN_GAME_BASE_NAME].ToString() +
                                  ", AmountToHandpay: " + _row_error[COLUMN_DELTA_JACKPOT_AMOUNT].ToString() +
                                  ". Count: " + _rows_cache.Length);
                    }
                  }
                  else
                  {
                    Log.Warning("    Handpay Error. Data not found in cache. " +
                                "TerminalId: " + _row_error[COLUMN_TERMINAL_ID].ToString() +
                                ", GameBaseName: " + _row_error[COLUMN_GAME_BASE_NAME].ToString() +
                                ", AmountToHandpay: " + _row_error[COLUMN_DELTA_JACKPOT_AMOUNT].ToString() +
                                ". Details: " + _row_error.RowError);
                  }
                }
              }

              _step = 8;

              _num_rows_deleted = 0;

              foreach (DataRow _row in _rows_to_update)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row[COLUMN_TERMINAL_ID].ToString() +
                              ", GameBaseName: " + _row[COLUMN_GAME_BASE_NAME].ToString() +
                              ". Details: " + _row.RowError);

                  _row.Delete();
                  _row.AcceptChanges();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetModified();
                }
              }

              _step = 9;

              foreach (DataRow _row in _rows_to_insert)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row[COLUMN_TERMINAL_ID].ToString() +
                              ", GameBaseName: " + _row[COLUMN_GAME_BASE_NAME].ToString() +
                              ". Details: " + _row.RowError);

                  _row.Delete();
                  _row.AcceptChanges();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetAdded();
                }
              }

              _step = 10;

              if (_num_rows_deleted == 0)
              {
                Log.Error("!!!!! Error found but not in data rows. Deleted all cache !!!!!");
                m_dt_game_meters.Clear();
              }

              Log.Warning("------------- END   GAME METERS NOT UPDATED -------------");

              Trx.Rollback(_rollback_point);

            } // if (_error || _error_handpays)

            _step = 11;

          } // try
          catch (Exception _ex)
          {
            Log.Warning("Exception 2: WCP_BU_GameMeters.UpdateDB. Step: " + _step.ToString());
            Log.Exception(_ex);

            try
            {
              Trx.Rollback(_rollback_point);
            }
            catch (Exception _ex2)
            {
              Log.Exception(_ex2);
            }

            return false;
          }
        } // while (true)
      } // try
      catch (Exception _ex)
      {
        Log.Warning("Exception 3: WCP_BU_GameMeters.UpdateDB.");
        Log.Exception(_ex);
        return false;
      }
      finally
      {
      }
    } // UpdateDB

    //------------------------------------------------------------------------------
    // PURPOSE : Process all GameMeters from Tasks in the array.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Tasks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Process(ArrayList Tasks, SqlConnection SqlConn)
    {
      SqlTransaction _sql_trx;
      WCP_Message _wcp_request;
      WCP_MsgReportGameMeters _request;
      SecureTcpClient _tcp_client;
      Int32 _tick0;
      Int32 _ticks;

      _tick0 = Environment.TickCount;

      _sql_trx = null;

      try
      {
        try
        {
          Init();

          _ticks = Environment.TickCount;

          if (m_unload_queue.Count > 0)
          {
            try
            {
              m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);
              if (m_unload_queue.Count > 0)
              {
                foreach (Int32 _term in m_unload_queue)
                {
                  Unload(_term);
                }
                m_unload_queue.Clear();
              }
            }
            finally
            {
              m_rw_lock_queue.ReleaseWriterLock();
            }
          }

          m_time_unload += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          _sql_trx = SqlConn.BeginTransaction();

          m_time_begin_trx += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _wcp_request = _wcp_task.Request;
            _tcp_client = (SecureTcpClient)_wcp_task.WcpClient;

            _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR, "No information available.");
            _request = (WCP_MsgReportGameMeters)_wcp_request.MsgContent;

            if (!WCP_GameMeters.BU_ProcessGameMeters(_tcp_client.InternalId, _wcp_request.MsgHeader.TerminalSessionId, _wcp_request.MsgHeader.SequenceId, _request, _wcp_task, _sql_trx))
            {
              Log.Error("WCP_BU_GameMeters.Process: Not updated. Terminal: " + _tcp_client.InternalId + ".");
            }
          }

          m_time_task_process += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          if (!UpdateDB(_sql_trx))
          {
            Log.Error("WCP_BU_GameMeters.Process: No game meters have been modified!");
          }

          m_time_updateDB += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          _sql_trx.Commit();

          m_time_commit += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _wcp_task.SendResponse();
          }
          m_time_response += Misc.GetElapsedTicks(_ticks);
        }
        catch (Exception ex)
        {
          Log.Warning("Exception: WCP_BU_GameMeters.Process.");
          Log.Exception(ex);
        }
        finally
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              _sql_trx.Rollback();
            }
          }
        }

        m_time_all += Misc.GetElapsedTicks(_tick0);
        m_time_num_all += Tasks.Count;

        if (m_time_num_all >= 10000)
        {
          // Only for test
          ////Log.Message("GM times:[#Tasks/#Load]=[" + m_time_num_all + "/" + m_time_num_loads + "] " +
          ////            "ALL:" + m_time_all + "," +
          ////            "Session:" + m_time_update_session + "," +
          ////            "Get:" + m_time_gets + "," +
          ////            "Update:" + m_time_updates + "," +
          ////            "TermGame:" + m_time_register_term_game + "," +
          ////            "UpdateDB:" + m_time_updateDB + "," +
          ////            "Commit:" + m_time_commit + "," +
          ////            "Response:" + m_time_response + "," +
          ////            "ProcessTaskGM:" + m_time_task_process + "," +
          ////            "BeginTrx:" + m_time_begin_trx + "," +
          ////            "Unload:" + m_time_unload + "," +
          ////            "Load:" + m_time_loads + "," +
          ////            "Insert:" + m_time_inserts + ".");

          m_time_num_all = 0;
          m_time_num_loads = 0;

          m_time_all = 0;
          m_time_update_session = 0;
          m_time_gets = 0;
          m_time_updates = 0;
          m_time_register_term_game = 0;
          m_time_updateDB = 0;
          m_time_commit = 0;
          m_time_response = 0;
          m_time_task_process = 0;
          m_time_begin_trx = 0;
          m_time_unload = 0;
          m_time_loads = 0;
          m_time_inserts = 0;
        }
      }
      catch
      {
      }
    } // Process

    //------------------------------------------------------------------------------
    // PURPOSE : Get Game Meters (by Sas Id) from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - NewGameMeters
    //          - Trx
    //
    //      - OUTPUT :
    //          - DbGameMeters
    //          - DbWcpSequenceId
    //          - RowGameMeters
    //
    // RETURNS :
    //      - true: successful.
    //      - false: not found or error.
    //
    public static Boolean GetGameMetersBySasId(Int32 TerminalId, GameMeters NewGameMeters, out GameMeters DbGameMeters,
                                               out Int64 DbWcpSequenceId, out DataRow RowGameMeters, SqlTransaction Trx)
    {
      String _filter;
      Int32 _sas_id;
      String _sas_id_filter;
      DataRow[] _term_rows;
      DataRow _term_row;
      int _ticks;

      _ticks = Environment.TickCount;

      DbGameMeters = new GameMeters();
      DbWcpSequenceId = 0;
      RowGameMeters = null;

      try
      {
        if (m_dt_game_meters == null)
        {
          throw new Exception("Get: BU_GameMeters not initialized. Call Init first!");
        }

        _term_rows = null;

        _sas_id = NewGameMeters.SasIdFromBaseName();
        if (_sas_id >= 0)
        {
          _sas_id_filter = "SAS - " + _sas_id.ToString("00") + " %";
        }
        else
        {
          _sas_id_filter = NewGameMeters.GameBaseName;
        }

        _filter = "TERMINAL_ID = " + TerminalId + " AND GAME_BASE_NAME like '" + _sas_id_filter + "'";

        _term_rows = m_dt_game_meters.Select(_filter);

        if (_term_rows.Length == 0)
        {
          return false;
        }

        if (_term_rows.Length > 1)
        {
          Log.Warning("BU_GameMeters.Get: Too many Game Meters for TerminalId: " + TerminalId +
                      ", GameBaseName: " + NewGameMeters.GameBaseName + " . Count: " + _term_rows.Length + ".");
        }

        _term_row = _term_rows[0];

        DbWcpSequenceId = (Int64)_term_row[COLUMN_SEQUENCE_ID];
        DbGameMeters.GameBaseName = (String)_term_row[COLUMN_GAME_BASE_NAME];
        DbGameMeters.DenominationCents = (Int32)((Decimal)_term_row[COLUMN_DENOMINATION] * 100);
        DbGameMeters.PlayedCount = (Int64)_term_row[COLUMN_PLAYED_COUNT];
        DbGameMeters.PlayedCents = (Int64)((Decimal)_term_row[COLUMN_PLAYED_AMOUNT] * 100);
        DbGameMeters.WonCount = (Int64)_term_row[COLUMN_WON_COUNT];
        DbGameMeters.WonCents = (Int64)((Decimal)_term_row[COLUMN_WON_AMOUNT] * 100);
        DbGameMeters.JackpotCents = (Int64)((Decimal)_term_row[COLUMN_JACKPOT_AMOUNT] * 100);

        if (!_term_row.IsNull(COLUMN_PROGRESSIVE_JACKPOT_AMOUNT))
        {
          DbGameMeters.ProgressiveJackpotCents = (Int64)((Decimal)_term_row[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] * 100);
        }
        else
        {
          DbGameMeters.ProgressiveJackpotCents = -1;
        }


        if (!_term_row.IsNull(COLUMN_MAX_PLAYED_COUNT))
        {
          DbGameMeters.PlayedCountMaxValue = (Int64)_term_row[COLUMN_MAX_PLAYED_COUNT];
          DbGameMeters.PlayedMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_PLAYED_AMOUNT] * 100);
          DbGameMeters.WonCountMaxValue = (Int64)_term_row[COLUMN_MAX_WON_COUNT];
          DbGameMeters.WonMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_WON_AMOUNT] * 100);
          DbGameMeters.JackpotMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_JACKPOT_AMOUNT] * 100);

          if (!_term_row.IsNull(COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT))
          {
            DbGameMeters.ProgressiveJackpotMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] * 100);
          }
          else
          {
            DbGameMeters.ProgressiveJackpotMaxValueCents = 0;
          }
        }
        else
        {
          DbGameMeters.PlayedCountMaxValue = -1;
          DbGameMeters.PlayedMaxValueCents = -1;
          DbGameMeters.WonCountMaxValue = -1;
          DbGameMeters.WonMaxValueCents = -1;
          DbGameMeters.JackpotMaxValueCents = -1;
          DbGameMeters.ProgressiveJackpotMaxValueCents = -1;
        }

        RowGameMeters = _term_row;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        m_time_gets += Misc.GetElapsedTicks(_ticks);
      }
    } // GetGameMetersBySasId

    //------------------------------------------------------------------------------
    // PURPOSE : Get Game Meters (by Sas Id) from Database.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - NewGameMeters
    //          - Trx
    //
    //      - OUTPUT :
    //          - DbGameMeters
    //          - DbWcpSequenceId
    //          - RowGameMeters
    //
    // RETURNS :
    //      - true: successful.
    //      - false: not found or error.
    //
    public static Boolean ReadLastMetersFromDatabaseBySasId(Int32 TerminalId, GameMeters NewGameMeters, out GameMeters DbGameMeters,
                                                            out Int64 DbWcpSequenceId, out DataRow RowGameMeters, SqlTransaction Trx)
    {
      int _ticks;

      _ticks = Environment.TickCount;

      DbGameMeters = new GameMeters();
      DbWcpSequenceId = 0;
      RowGameMeters = null;

      try
      {
        if (m_dt_game_meters == null)
        {
          throw new Exception("Get: BU_GameMeters not initialized. Call Init first!");
        }

        // Load from database last game with same Sas Id reported
        LoadLastMetersWithSameSasIdFromDatabase(TerminalId, NewGameMeters, Trx);

        if (!GetGameMetersBySasId(TerminalId, NewGameMeters, out DbGameMeters, out DbWcpSequenceId, out RowGameMeters, Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        m_time_gets += Misc.GetElapsedTicks(_ticks);
      }

    } // ReadLastMetersFromDatabaseBySasId

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows based on Filter from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Filter
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static Boolean Unload(String Filter)
    {
      DataRow[] _term_rows;

      if (m_dt_game_meters == null)
      {
        Log.Error("Unload: BU_GameMeters not initialized. Call Init first!");
        return false;
      }

      _term_rows = m_dt_game_meters.Select(Filter);
      foreach (DataRow _row in _term_rows)
      {
        m_dt_game_meters.Rows.Remove(_row);
      }

      return true;
    } // Unload

    //------------------------------------------------------------------------------
    // PURPOSE : Load GameMeters data from DB to DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - NewGameMeters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void LoadLastMetersWithSameSasIdFromDatabase(Int32 TerminalId, GameMeters NewGameMeters, SqlTransaction Trx)
    {
      Int32 _sas_id;
      String _sas_id_filter;
      int _ticks;

      _ticks = Environment.TickCount;

      _sas_id = NewGameMeters.SasIdFromBaseName();
      if (_sas_id >= 0)
      {
        _sas_id_filter = "SAS - " + _sas_id.ToString("00") + " %";
      }
      else
      {
        _sas_id_filter = NewGameMeters.GameBaseName;
      }

      m_adap_gm_select_last_with_this_sas_id.SelectCommand.Connection = Trx.Connection;
      m_adap_gm_select_last_with_this_sas_id.SelectCommand.Transaction = Trx;
      m_adap_gm_select_last_with_this_sas_id.SelectCommand.Parameters["@pTerminalId"].Value = TerminalId;
      m_adap_gm_select_last_with_this_sas_id.SelectCommand.Parameters["@pSasId"].Value = _sas_id_filter;

      m_adap_gm_select_last_with_this_sas_id.Fill(m_dt_game_meters);

      m_time_loads += Misc.GetElapsedTicks(_ticks);
      m_time_num_loads++;

    } // LoadLastMetersWithSameSasIdFromDatabase

    //------------------------------------------------------------------------------
    // PURPOSE : Format the string for DataTable's Select method filter
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static String ParseValueForDT(String Value)
    {
      StringBuilder _sb;

      _sb = new StringBuilder(Value.Length);

      for (Int32 _idx = 0; _idx < Value.Length; _idx++)
      {
        switch (Value[_idx])
        {
          case ']':
          case '[':
          case '%':
          case '*':
            _sb.Append("[").Append(Value[_idx]).Append("]");
            break;

          case '\'':
            _sb.Append("''");
            break;

          default:
            _sb.Append(Value[_idx]);
            break;
        }
      }

      return _sb.ToString();      
    }

    #endregion // Private Methods

  } // WCP_BU_GameMeters

} // WSI.WCP
