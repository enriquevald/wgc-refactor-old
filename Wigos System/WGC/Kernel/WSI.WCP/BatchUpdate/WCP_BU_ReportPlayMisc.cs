//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_BU_ReportPlayMisc.cs
// 
//   DESCRIPTION: Partial WCP_BU_ReportPlay class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 09-JUL-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-JUL-2010 ACC    First release.
// 05-APR-2012 RCI    Added the process to insert/update GAME_METERS from LKT-WIN PLAYS
// 04-JUL-2012 RCI & ACC    In routine CheckCardSessionIdAndReportedBalance, added control TerminalType is SAS-HOST
// 09-JUL-2012 ACC    Change wheres for process LKT plays: "BU_COLUMN_TERMINAL_TYPE = 1" instead "BU_COLUMN_STAND_ALONE_SESSION = 0"
//
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Xml;
using System.Collections;

namespace WSI.WCP
{

  public static partial class WCP_BU_ReportPlay
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create and fill initial data for datatable plays_to_process
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportPlays
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void InitializePlaysDataTable(ReportPlayDataList ReportPlays)
    {
      DataRow _dr;
      Int32 _idx_play;
      ReportPlayData _report_play_data;

      // Create DataTable
      ReportPlays.plays_to_process = new DataTable("PLAYS");

      // Initial columns
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_INDEX", Type.GetType("System.Int32"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_CARD_SESSION_ID", Type.GetType("System.Int64"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_TRANSACTION_ID", Type.GetType("System.Int64"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_ACCOUNT_ID", Type.GetType("System.Int64"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_TERMINAL_ID", Type.GetType("System.Int32"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_WCP_SEQUENCE_ID", Type.GetType("System.Int64"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_WCP_TRANSACTION_ID", Type.GetType("System.Int64"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_INITIAL_BALANCE", Type.GetType("System.Decimal"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_WON_AMOUNT", Type.GetType("System.Decimal"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_FINAL_BALANCE", Type.GetType("System.Decimal"));
      // Calculate columns in process
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_GAME_ID", Type.GetType("System.Int32"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_STAND_ALONE_SESSION", Type.GetType("System.Int32"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_AC_BALANCE", Type.GetType("System.Decimal"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_TERMINAL_NAME", Type.GetType("System.String"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_WON_COUNT", Type.GetType("System.Int32"));

      // RCI 05-APR-2012: Needed to insert/update GAME_METERS
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_DENOMINATION", Type.GetType("System.Decimal"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_GAME_NAME", Type.GetType("System.String"));
      ReportPlays.plays_to_process.Columns.Add("BU_COLUMN_TERMINAL_TYPE", Type.GetType("System.Int32"));

      // RCI 13-AUG-2010: {Begin,End}LoadData() is needed to speed up adding rows. No checks while adding.
      ReportPlays.plays_to_process.BeginLoadData();

      // Insert rows and fill data
      for (_idx_play = 0; _idx_play < ReportPlays.num_plays; _idx_play++)
      {
        // Get play data
        _report_play_data = ReportPlays.GetPlay(_idx_play);

        if (_report_play_data.finished_process)
        {
          continue;
        }

        // Create new row
        _dr = ReportPlays.plays_to_process.NewRow();

        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_INDEX] = _idx_play;
        if (_report_play_data.request.CardSessionId == 0)
        {
          _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_CARD_SESSION_ID] = DBNull.Value;
        }
        else
        {
          _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_CARD_SESSION_ID] = _report_play_data.request.CardSessionId;
        }

        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_TRANSACTION_ID] = _report_play_data.play_session.TransactionId;

        if (_report_play_data.play_session.AccountId == 0)
        {
          _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_ACCOUNT_ID] = DBNull.Value;
        }
        else
        {
          _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_ACCOUNT_ID] = _report_play_data.play_session.AccountId;
        }
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_TERMINAL_ID] = _report_play_data.play_session.TerminalId;
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_WCP_SEQUENCE_ID] = _report_play_data.common_data.wcp_request.MsgHeader.SequenceId;
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_WCP_TRANSACTION_ID] = _report_play_data.request.TransactionId;
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_INITIAL_BALANCE] = _report_play_data.balance_before_play;
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_PLAYED_AMOUNT] = _report_play_data.played_amount;
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_WON_AMOUNT] = _report_play_data.won_amount;
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_FINAL_BALANCE] = _report_play_data.balance_after_play;

        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_DENOMINATION] = _report_play_data.denomination;
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_GAME_NAME] = _report_play_data.game_name;

        // Calculate columns in process

        // Cache: Get game id 
        _report_play_data.game_id = WCP_Games.GameID(_report_play_data.request.GameId);
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_GAME_ID] = _report_play_data.game_id;

        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_STAND_ALONE_SESSION] = 0;
        if (_report_play_data.stand_alone_session)
        {
          _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_STAND_ALONE_SESSION] = 1;
        }
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_AC_BALANCE] = 0;
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_TERMINAL_NAME] = _report_play_data.play_session.TerminalName;
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_WON_COUNT] = 0;
        if (_report_play_data.won_amount > 0)
        {
          _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_WON_COUNT] = 1;
        }
        _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_TERMINAL_TYPE] = _report_play_data.common_data.tcp_client.TerminalType;

        ReportPlays.plays_to_process.Rows.Add(_dr);
      }

      ReportPlays.plays_to_process.EndLoadData();

    } // InitializePlaysDataTable

    //------------------------------------------------------------------------------
    // PURPOSE : Insert all plays
    //
    //  PARAMS :
    //      - INPUT :
    //          - (I/O) ReportPlays
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void BU_InsertPlays(ReportPlayDataList ReportPlays, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      int _nr;
      ReportPlayData _report_play_data;
      DataRow[] _errors;

      try
      {
        //    - Set SqlDataAdapter commands
        _da = new SqlDataAdapter();

        _da.UpdateBatchSize = 500;
        _da.ContinueUpdateOnError = true;

        // Set SqlDataAdapter commands
        _da.SelectCommand = null;
        _da.DeleteCommand = null;
        _da.UpdateCommand = null;

        // Insert
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("INSERT INTO PLAYS ");
        _sql_txt.AppendLine("           ( PL_PLAY_SESSION_ID ");
        _sql_txt.AppendLine("           , PL_ACCOUNT_ID ");
        _sql_txt.AppendLine("           , PL_TERMINAL_ID ");
        _sql_txt.AppendLine("           , PL_WCP_SEQUENCE_ID ");
        _sql_txt.AppendLine("           , PL_WCP_TRANSACTION_ID ");
        _sql_txt.AppendLine("           , PL_GAME_ID ");
        _sql_txt.AppendLine("           , PL_INITIAL_BALANCE ");
        _sql_txt.AppendLine("           , PL_PLAYED_AMOUNT ");
        _sql_txt.AppendLine("           , PL_WON_AMOUNT ");
        _sql_txt.AppendLine("           , PL_FINAL_BALANCE ");
        _sql_txt.AppendLine("           , PL_DATETIME) ");
        _sql_txt.AppendLine("VALUES     ( @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, GETDATE()) ");

        _da.InsertCommand = new SqlCommand(_sql_txt.ToString());
        _da.InsertCommand.Connection = Trx.Connection;
        _da.InsertCommand.Transaction = Trx;
        _da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

        _da.InsertCommand.Parameters.Add("@p1", SqlDbType.BigInt, 8, "BU_COLUMN_CARD_SESSION_ID");
        _da.InsertCommand.Parameters.Add("@p2", SqlDbType.BigInt, 8, "BU_COLUMN_ACCOUNT_ID");
        _da.InsertCommand.Parameters.Add("@p3", SqlDbType.Int, 4, "BU_COLUMN_TERMINAL_ID");
        _da.InsertCommand.Parameters.Add("@p4", SqlDbType.BigInt, 8, "BU_COLUMN_WCP_SEQUENCE_ID");
        _da.InsertCommand.Parameters.Add("@p5", SqlDbType.BigInt, 8, "BU_COLUMN_WCP_TRANSACTION_ID");
        _da.InsertCommand.Parameters.Add("@p6", SqlDbType.Int, 4, "BU_COLUMN_GAME_ID");
        _da.InsertCommand.Parameters.Add("@p7", SqlDbType.Money, 8, "BU_COLUMN_INITIAL_BALANCE");
        _da.InsertCommand.Parameters.Add("@p8", SqlDbType.Money, 8, "BU_COLUMN_PLAYED_AMOUNT");
        _da.InsertCommand.Parameters.Add("@p9", SqlDbType.Money, 8, "BU_COLUMN_WON_AMOUNT");
        _da.InsertCommand.Parameters.Add("@p10", SqlDbType.Money, 8, "BU_COLUMN_FINAL_BALANCE");

        _nr = _da.Update(ReportPlays.plays_to_process);

        if (ReportPlays.plays_to_process.Rows.Count != _nr)
        {
          _errors = ReportPlays.plays_to_process.GetErrors();
          foreach (DataRow _dr in _errors)
          {
            _report_play_data = ReportPlays.GetPlay((Int32)_dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_INDEX]);

            if (ExistPlay(_report_play_data, Trx))
            {
              _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
            }
            else
            {
              ReportPlays.rollback_work = true;
              _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_ERROR;
              Log.Warning("Inserting plays. PlaySessionId: " + _report_play_data.request.CardSessionId.ToString() + ". Details: " + _dr.RowError);
            }

            _report_play_data.finished_process = true;
            _dr.Delete();
          } // foreach

          ReportPlays.plays_to_process.AcceptChanges();
        } // if ( Count != _nr )

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }

    } // BU_InsertPlays

    //------------------------------------------------------------------------------
    // PURPOSE : Insert or update GAME_METERS for every play
    //
    //  PARAMS :
    //      - INPUT :
    //          - (I/O) ReportPlays
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void BU_InsertOrUpdateGameMeters(ReportPlayDataList ReportPlays, SqlTransaction SqlTrx)
    {
      Int32 _nr;
      ReportPlayData _report_play_data;

      try
      {
        foreach (DataRow _dr in ReportPlays.rows_to_update)
        {
          _dr.SetModified();
        }

        using (SqlCommand _sql_cmd = WSI.Common.Terminal.GetGameMetersUpdateCommand(SqlTrx))
        {
          //_sql_cmd.UpdatedRowSource = UpdateRowSource.None;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "BU_COLUMN_TERMINAL_ID";
          _sql_cmd.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar, 50).SourceColumn = "BU_COLUMN_GAME_NAME";
          _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "BU_COLUMN_DENOMINATION";
          _sql_cmd.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "BU_COLUMN_WCP_SEQUENCE_ID";
          _sql_cmd.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).Value = 1;
          _sql_cmd.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).SourceColumn = "BU_COLUMN_PLAYED_AMOUNT";
          _sql_cmd.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).SourceColumn = "BU_COLUMN_WON_COUNT";
          _sql_cmd.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).SourceColumn = "BU_COLUMN_WON_AMOUNT";
          _sql_cmd.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).Value = 0;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.UpdateBatchSize = 500;
            _sql_da.ContinueUpdateOnError = true;
            _sql_da.UpdateCommand = _sql_cmd;

            _nr = _sql_da.Update(ReportPlays.rows_to_update);
          }
        }

        if (ReportPlays.rows_to_update.Length != _nr)
        {
          foreach (DataRow _dr in ReportPlays.rows_to_update)
          {
            if (_dr.HasErrors)
            {
              _dr.ClearErrors();
              _report_play_data = ReportPlays.GetPlay((Int32)_dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_INDEX]);

              _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_ERROR;
              _report_play_data.common_data.wcp_response.MsgHeader.ResponseCodeText = "GAME_METERS Not Updated.";
              Log.Warning("Updating GAME_METERS. TerminalId: " + _report_play_data.play_session.TerminalId.ToString() +
                          ", GameName: " + _report_play_data.game_name + ". Details: " + _dr.RowError);

              _report_play_data.finished_process = true;
              ReportPlays.rollback_work = true;
            }
            _dr.AcceptChanges();

          } // foreach

        } // if ( Count != _nr )
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }
    } // BU_InsertOrUpdateGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update account balance
    //
    //  PARAMS :
    //      - INPUT :
    //          - (I/O) ReportPlays
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void BU_UpdatePlaySessions(ReportPlayDataList ReportPlays, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      int _nr;
      ReportPlayData _report_play_data;

      try
      {
        // AJQ & JVV 26-SEP-2013, Avoid processing when there is nothing to do.
        if (ReportPlays.rows_to_update.Length == 0)
        {
          return;
        }

        //    - Set SqlDataAdapter commands
        _da = new SqlDataAdapter();

        //
        // Calculate amounts.
        //
        foreach (DataRow _dr in ReportPlays.rows_to_update)
        {
          _dr.SetModified();
        } // foreach

        _da.UpdateBatchSize = 500;
        _da.ContinueUpdateOnError = true;

        // Set SqlDataAdapter commands
        _da.SelectCommand = null;
        _da.DeleteCommand = null;
        _da.InsertCommand = null;
        _da.UpdateCommand = null;

        // CashIn and CashOut always is 0 for play
        //
        // PS_CASH_IN not updated
        // PS_CASH_OUT not updated
        //

        // Update command
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE PLAY_SESSIONS ");
        _sql_txt.AppendLine("   SET PS_PLAYED_COUNT    = PS_PLAYED_COUNT  + 1 ");
        _sql_txt.AppendLine("     , PS_PLAYED_AMOUNT   = PS_PLAYED_AMOUNT + @pPlayedAmount ");
        _sql_txt.AppendLine("     , PS_WON_COUNT       = PS_WON_COUNT     + @pWonCount ");
        _sql_txt.AppendLine("     , PS_WON_AMOUNT      = PS_WON_AMOUNT    + @pWonAmount ");
        _sql_txt.AppendLine("     , PS_LOCKED          = NULL ");
        // AJQ & JVV 26-SEP-2013, Avoid touching the finished date when session is not opened
        _sql_txt.AppendLine("     , PS_FINISHED        = CASE WHEN (PS_STATUS = @pStatusOpened) THEN GETDATE() ELSE PS_FINISHED END ");
        _sql_txt.AppendLine(" WHERE PS_PLAY_SESSION_ID = @pPlaySessionID ");
        _sql_txt.AppendLine("   AND (   ( PS_STAND_ALONE = 1 ) ");
        _sql_txt.AppendLine("        OR ( PS_STAND_ALONE = 0 AND PS_STATUS = @pStatusOpened ) ) ");

        _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
        _da.UpdateCommand.Connection = Trx.Connection;
        _da.UpdateCommand.Transaction = Trx;
        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

        _da.UpdateCommand.Parameters.Add("@pPlayedAmount", SqlDbType.Money, 8, "BU_COLUMN_PLAYED_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@pWonCount", SqlDbType.Int, 4, "BU_COLUMN_WON_COUNT");
        _da.UpdateCommand.Parameters.Add("@pWonAmount", SqlDbType.Money, 8, "BU_COLUMN_WON_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@pFinalBalance", SqlDbType.Money, 8, "BU_COLUMN_FINAL_BALANCE");
        _da.UpdateCommand.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt, 8, "BU_COLUMN_CARD_SESSION_ID");
        _da.UpdateCommand.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Opened;

        _nr = _da.Update(ReportPlays.rows_to_update);

        if (ReportPlays.rows_to_update.Length != _nr)
        {
          foreach (DataRow _dr in ReportPlays.rows_to_update)
          {
            if (_dr.HasErrors)
            {
              _dr.ClearErrors();
              _report_play_data = ReportPlays.GetPlay((Int32)_dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_INDEX]);

              _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_ERROR;
              _report_play_data.common_data.wcp_response.MsgHeader.ResponseCodeText = "PLAY_SESSION Not Updated.";
              Log.Warning("Updating PlaySession. PlaySessionId: " + _report_play_data.request.CardSessionId.ToString() + ". Details: " + _dr.RowError);

              _report_play_data.finished_process = true;
              ReportPlays.rollback_work = true;
            }
            _dr.AcceptChanges();

          } // foreach

        } // if ( Count != _nr )

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }

    } // BU_UpdatePlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : Check if exist this play
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportPlay
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: play exist.
    //      - false: otherwise.
    //
    //   NOTES :
    //
    private static Boolean ExistPlay(ReportPlayData ReportPlay, SqlTransaction Trx)
    {
      SqlCommand sql_sel_command;
      String sql_sel_str;
      SqlDataReader reader;
      Int32 num_plays;

      // Check if play already exist
      num_plays = 0;
      sql_sel_str = "SELECT COUNT(*) " +
                     " FROM PLAYS WITH( INDEX(IX_plays_transaction))" +
                     "WHERE PL_TERMINAL_ID          = @p1 " +
                     "  AND PL_WCP_TRANSACTION_ID   = @p2 " +
                     "  AND PL_GAME_ID              = @p3 " +
                     "  AND PL_INITIAL_BALANCE      = @p4 " +
                     "  AND PL_PLAYED_AMOUNT        = @p5 " +
                     "  AND PL_WON_AMOUNT           = @p6 " +
                     "  AND PL_FINAL_BALANCE        = @p7 ";

      sql_sel_command = new SqlCommand(sql_sel_str);
      sql_sel_command.Connection = Trx.Connection;
      sql_sel_command.Transaction = Trx;

      sql_sel_command.Parameters.Add("@p1", SqlDbType.Int, 4, "PL_TERMINAL_ID").Value = ReportPlay.play_session.TerminalId;
      sql_sel_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "PL_WCP_TRANSACTION_ID").Value = ReportPlay.request.TransactionId;
      sql_sel_command.Parameters.Add("@p3", SqlDbType.Int, 4, "PL_GAME_ID").Value = ReportPlay.game_id;
      sql_sel_command.Parameters.Add("@p4", SqlDbType.Money, 8, "PL_INITIAL_BALANCE").Value = ReportPlay.balance_before_play;
      sql_sel_command.Parameters.Add("@p5", SqlDbType.Money, 8, "PL_PLAYED_AMOUNT").Value = ReportPlay.played_amount;
      sql_sel_command.Parameters.Add("@p6", SqlDbType.Money, 8, "PL_WON_AMOUNT").Value = ReportPlay.won_amount;
      sql_sel_command.Parameters.Add("@p7", SqlDbType.Money, 8, "PL_FINAL_BALANCE").Value = ReportPlay.balance_after_play;

      reader = null;

      try
      {
        reader = sql_sel_command.ExecuteReader();
        if (reader.Read() && !reader.IsDBNull(0))
        {
          num_plays = (Int32)reader[0];
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Reading play. TransactionId: " + ReportPlay.request.TransactionId.ToString());

        return false;
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      if (num_plays > 0)
      {
        // Play already was inserted
        return true;
      }

      return false;

    } // ExistPlay

    //------------------------------------------------------------------------------
    // PURPOSE : Check Server and Terminal sessions
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportPlays
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void CheckServerAndTerminalSessions(ReportPlayDataList ReportPlays)
    {
      ReportPlayData _report_play_data;
      Int32 _idx_play;

      for (_idx_play = 0; _idx_play < ReportPlays.num_plays; _idx_play++)
      {
        // Get play data
        _report_play_data = ReportPlays.GetPlay(_idx_play);

        // Check SessionId
        if (_report_play_data.common_data.wcp_request.MsgHeader.TerminalSessionId < 0)
        {
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_TERMINAL_SESSION_ID;
          _report_play_data.finished_process = true;
        }

        // Check TerminalId
        if (_report_play_data.common_data.tcp_client.InternalId == 0)
        {
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED;
          _report_play_data.finished_process = true;
        }

        // Check session active
        if (!Common.BatchUpdate.TerminalSession.IsActive(_report_play_data.common_data.wcp_request.MsgHeader.TerminalSessionId))
        {
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TERMINAL_SESSION_NOT_VALID;
          _report_play_data.finished_process = true;
        }

        // Check server id
        if (_report_play_data.common_data.wcp_request.MsgHeader.ServerId.Length > 0)
        {
          if (_report_play_data.common_data.wcp_request.MsgHeader.ServerSessionId == 0)
          {
            _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_SERVER_SESSION_ID;
            _report_play_data.finished_process = true;
          }
          if (!Common.BatchUpdate.TerminalSession.IsActive(_report_play_data.common_data.wcp_request.MsgHeader.ServerSessionId))
          {
            _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_SERVER_SESSION_NOT_VALID;
            _report_play_data.finished_process = true;
          }
        }

        if (_report_play_data.common_data.wcp_request.MsgHeader.TerminalSessionId == 0)
        {
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_TERMINAL_SESSION_ID;
          _report_play_data.finished_process = true;
        }

      } // for

    } // CheckServerAndTerminalSessions

    //------------------------------------------------------------------------------
    // PURPOSE : Check Card SessionId and reported balance (not stand alone sessions)
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportPlays
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void CheckCardSessionIdAndReportedBalance(ReportPlayDataList ReportPlays)
    {
      ReportPlayData _report_play_data;

      foreach (DataRow _dr in ReportPlays.plays_to_process.Rows)
      {
        _report_play_data = ReportPlays.GetPlay((Int32)_dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_INDEX]);

        // Check reported play with NoCardSession & StandAloneMode
        // Some machines could report 'old' plays.
        if (_report_play_data.request.CardSessionId == 0)
        {
          // Force Stand Alone when CardSessionId is 0 AND TerminalType is SAS_HOST
          // RCI & ACC 09-JUL-2012: Added TerminalType is SAS-HOST
          if (_report_play_data.common_data.tcp_client.TerminalType == TerminalTypes.SAS_HOST)
          {
            _report_play_data.stand_alone_session = true;

            _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_STAND_ALONE_SESSION] = 1;
            _dr.AcceptChanges();
          }
          else
          {
            _report_play_data.stand_alone_session = false;

            _dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_STAND_ALONE_SESSION] = 0;
            _dr.AcceptChanges();
          }
        }

        if (_report_play_data.stand_alone_session)
        {
          continue;
        }

        // ACC 10-JUL-2012 Only check balance if CardSessionId > 0 and LKT WIN Terminals
        if (   _report_play_data.request.CardSessionId == 0
            || _report_play_data.common_data.tcp_client.TerminalType != TerminalTypes.WIN)
        {
          continue;
        }

        // [!stand_alone_session] continue to process

        // Check Play Session Id
        if (!PlaySession.IsActive(_report_play_data.request.CardSessionId))
        {
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED;
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCodeText = "CardSessionId isn't active.";
          _report_play_data.finished_process = true;
          ReportPlays.rollback_work = true;

          continue;
        }
        // Check Session Data
        if (_report_play_data.play_session.PlaySessionId != _report_play_data.request.CardSessionId)
        {
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_VALID;
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCodeText = "Card session not valid.";
          _report_play_data.finished_process = true;
          ReportPlays.rollback_work = true;

          continue;
        }
        if (_report_play_data.request.SessionBalanceBeforePlay < _report_play_data.request.PlayedAmount)
        {
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCodeText = "Play Not Possible: BalanceBeforePlay lower than PlayedAmount";
          _report_play_data.finished_process = true;
          ReportPlays.rollback_work = true;

          continue;
        }

        if (!_report_play_data.play_session.Update(_report_play_data.balance_before_play,
                                                   _report_play_data.played_amount,
                                                   _report_play_data.won_amount,
                                                   _report_play_data.balance_after_play,
                                                   out _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode,
                                                   out _report_play_data.common_data.wcp_response.MsgHeader.ResponseCodeText))
        {
          _report_play_data.finished_process = true;
          ReportPlays.rollback_work = true;

          continue;
        }

        _report_play_data.play_session_updated = true;

      } // foreach

    } // CheckCardSessionIdAndReportedBalance

    //------------------------------------------------------------------------------
    // PURPOSE : Alesis process
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportPlays
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void AlesisProcess(ReportPlayDataList ReportPlays, SqlTransaction Trx)
    {
      ReportPlayData _report_play_data;

      foreach (DataRow _dr in ReportPlays.rows_to_update)
      {
        _dr.SetModified();

        _report_play_data = ReportPlays.GetPlay((Int32)_dr[(Int32)BU_PLAY_COLUMNS.BU_COLUMN_INDEX]);

        try
        {
          switch (_report_play_data.play_session.PlaySessionType)
          {
            case PlaySessionType.ALESIS:
              {
                AleTerminal ale_terminal;
                Alesis.StatusCode status_code;

                _report_play_data.force_end_session = true;
                
                ale_terminal = (AleTerminal)AleTerminal.GetTerminal(_report_play_data.common_data.tcp_client.InternalId, Trx);
                ale_terminal.balance = _report_play_data.balance_after_play;

                status_code = Alesis.ReportPlay(ale_terminal, Trx);

                switch (status_code)
                {
                  case Alesis.StatusCode.Success:
                    _report_play_data.force_end_session = false;
                    break;

                  case Alesis.StatusCode.AccountNumberNotValid:
                  case Alesis.StatusCode.MachineNumberNotValid:
                  case Alesis.StatusCode.SessionNumberNotValid:
                  case Alesis.StatusCode.AccessDenied:
                    Log.Error("ReportPlay SessionID=" + ale_terminal.ale_session_id + " CardTrackData=" + ale_terminal.card_track_data + " Error:" + status_code.ToString());
                    break;

                  case Alesis.StatusCode.NotConnectedToDatabase:
                    Log.Error("ReportPlay SessionID=" + ale_terminal.ale_session_id + " CardTrackData=" + ale_terminal.card_track_data + " Error:" + status_code.ToString());
                    break;

                  default:
                    Log.Error("ReportPlay SessionID=" + ale_terminal.ale_session_id + " CardTrackData=" + ale_terminal.card_track_data + " Error:" + status_code.ToString());
                    break;
                } // switch (status_code)
              }
              break;

            case PlaySessionType.WIN:
              // Do nothing
              break;

            default:
              {
                Log.Warning("Unexpected PlaySessionType: " + _report_play_data.play_session.PlaySessionType.ToString() + " PlaySessionID: " + _report_play_data.play_session.PlaySessionId.ToString());
                _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED;
                _report_play_data.finished_process = true;
              }
              break;
          } // switch (terminal.play_session_type)

          if (_report_play_data.force_end_session)
          {
            WCP_BusinessLogic.DB_PlaySessionClose(_report_play_data.request.CardSessionId,
                                                  _report_play_data.play_session.AccountId,
                                                  _report_play_data.play_session.Balance,
                                                  Trx);

            // Insert movement into Db
            WCP_BusinessLogic.DB_InsertMovement(_report_play_data.request.CardSessionId,
                                                _report_play_data.play_session.AccountId,
                                                _report_play_data.common_data.tcp_client.InternalId,
                                                _report_play_data.common_data.wcp_request.MsgHeader.SequenceId,
                                                _report_play_data.common_data.wcp_request.TransactionId,
                                                MovementType.EndCardSession,
                                                _report_play_data.play_session.Balance,
                                                0,
                                                0,
                                                _report_play_data.play_session.Balance,
                                                out _report_play_data.account_movement_id,
                                                Trx);

            _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_VALID;
            _report_play_data.finished_process = true;

          } // force_end_session

        }
        catch (WCP_Exception wcp_ex)
        {
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = wcp_ex.ResponseCode;
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCodeText = wcp_ex.ResponseCodeText;
          _report_play_data.finished_process = true;
          ReportPlays.rollback_work = true;
        }
        catch (Exception ex)
        {
          _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_ERROR;
          _report_play_data.finished_process = true;
          ReportPlays.rollback_work = true;

          Log.Exception(ex);
        }

        _dr.AcceptChanges();

      } // foreach

    } // AlesisProcess

  }
}
