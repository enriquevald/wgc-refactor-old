//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_BU_ReportPlay.cs
// 
//   DESCRIPTION: WCP_BU_ReportPlay class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 05-JUL-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUL-2010 ACC    First release.
// 05-APR-2012 RCI    Added the process to insert/update GAME_METERS from LKT-WIN PLAYS
// 09-JUL-2012 ACC    Change wheres for process LKT plays: "BU_COLUMN_TERMINAL_TYPE = 1" instead "BU_COLUMN_STAND_ALONE_SESSION = 0"
//
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Xml;
using System.Collections;

namespace WSI.WCP
{
  #region Definitions

  //------------------------------------------------------------------------------
  // PURPOSE : Common data class 
  //
  //   NOTES :
  //
  public class WCP_CommonMsgData
  {
    // Initial data
    public String xml_message;
    public WCP_Message wcp_request;
    public SecureTcpClient tcp_client;
    public SecureTcpServer tcp_server;

    // Calculate data
    public WCP_Message wcp_response;
    public Int64 wcp_message_id;
    public String str_response;

  } // WCP_CommonMsgData

  //------------------------------------------------------------------------------
  // PURPOSE : Report play data class 
  //
  //   NOTES :
  //
  public class ReportPlayData
  {
    public WCP_CommonMsgData common_data;

    public WCP_MsgReportPlay request;
    public WCP_MsgReportPlayReply response;
    public PlaySession play_session;
    public Boolean play_session_updated;
    public Decimal denomination;
    public Decimal played_amount;
    public Decimal won_amount;
    public Decimal balance_before_play;
    public Decimal balance_after_play;
    public Boolean force_end_session;
    public Int64 account_movement_id;
    public Int32 game_id;
    public String game_name;
    public Boolean stand_alone_session;

    public Boolean finished_process;

    public ReportPlayData(String XmlMessage, WCP_Message WcpRequest, SecureTcpClient TcpClient, SecureTcpServer TcpServer)
    {
      common_data = new WCP_CommonMsgData();

      common_data.xml_message = XmlMessage;
      common_data.wcp_request = WcpRequest;
      common_data.tcp_client  = TcpClient;
      common_data.tcp_server  = TcpServer;

      common_data.wcp_response = WCP_Message.CreateMessage(common_data.wcp_request.MsgHeader.MsgType + 1);
      common_data.wcp_response.MsgHeader.PrepareReply(common_data.wcp_request.MsgHeader);

      finished_process = false;
      play_session_updated = false;
    }

  } // class ReportPlayData

  //------------------------------------------------------------------------------
  // PURPOSE : Report play data list class 
  //
  //   NOTES :
  //
  public class ReportPlayDataList
  {
    public DataTable plays_to_process;
    public DataRow[] rows_to_update;

    public Boolean rollback_work;

    private ArrayList plays = new ArrayList();  // ReportPlayData

    public void InsertPlay(ReportPlayData Play)
    {
      plays.Add(Play);
    }

    public void DeletePlay(Int32 IdxPlay)
    {
      plays.RemoveAt(IdxPlay);
    }

    public ReportPlayData GetPlay(Int32 IdxPlay)
    {
      if (plays.Count <= IdxPlay)
      {
        return null;
      }

      return (ReportPlayData)plays[IdxPlay];
    }

    public Int32 num_plays
    {
      get
      {
        return plays.Count;
      }
    }

  } // class ReportPlayDataList

  #endregion // Definitions

  public static partial class WCP_BU_ReportPlay
  {

    #region Enums

    private enum BU_PLAY_COLUMNS
    {
      BU_COLUMN_INDEX = 0
    , BU_COLUMN_CARD_SESSION_ID = 1
    , BU_COLUMN_TRANSACTION_ID = 2
    , BU_COLUMN_ACCOUNT_ID = 3
    , BU_COLUMN_TERMINAL_ID = 4
    , BU_COLUMN_WCP_SEQUENCE_ID = 5
    , BU_COLUMN_WCP_TRANSACTION_ID = 6
    , BU_COLUMN_INITIAL_BALANCE = 7
    , BU_COLUMN_PLAYED_AMOUNT = 8
    , BU_COLUMN_WON_AMOUNT = 9
    , BU_COLUMN_FINAL_BALANCE = 10
    , BU_COLUMN_GAME_ID = 11
    , BU_COLUMN_STAND_ALONE_SESSION = 12
    , BU_COLUMN_AC_BALANCE = 13
    , BU_COLUMN_TERMINAL_NAME = 14
    , BU_COLUMN_WON_COUNT = 15
    , BU_COLUMN_DENOMINATION = 16
    , BU_COLUMN_GAME_NAME = 17
    , BU_COLUMN_TERMINAL_TYPE = 18

    }

    #endregion // Enums

  //------------------------------------------------------------------------------
    // PURPOSE : Main function for process plays
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportPlays
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Process(ReportPlayDataList ReportPlays, SqlConnection SqlConn)
    {
      SqlTransaction  _sql_trx;
      Boolean         _error;
      Int32           _idx_play;
      ReportPlayData  _report_play_data;
      ArrayList       _rows_to_delete;
      Boolean         _sent;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_init_data;
      long _interval_insert_play;
      long _interval_GAME_METERS;
      long _interval_update_PS;
      long _interval_update_CARD;
      long _interval_commit;
      long _interval_sent;
      long _interval_all_process;
      long _num_initial_trx;
      long _num_rollback_work;

      _interval_init_data = 0;
      _interval_insert_play = 0;
      _interval_GAME_METERS = 0;
      _interval_update_CARD = 0;
      _interval_update_PS = 0;
      _interval_commit = 0;
      _interval_sent = 0;
      _interval_all_process = 0;
      _num_rollback_work = 0;
      _num_initial_trx = ReportPlays.num_plays;
      _tick0 = Environment.TickCount;

      _rows_to_delete = new ArrayList();
      _sql_trx = null;
      _error = true;

      try
      {

        //
        // Common start process transaction
        //

        // Check server and terminal sessions
        CheckServerAndTerminalSessions(ReportPlays);

        for (_idx_play = 0; _idx_play < ReportPlays.num_plays; _idx_play++)
        {
          // Get play data
          _report_play_data = ReportPlays.GetPlay(_idx_play);

          _report_play_data.request = (WCP_MsgReportPlay)_report_play_data.common_data.wcp_request.MsgContent;
          _report_play_data.response = (WCP_MsgReportPlayReply)_report_play_data.common_data.wcp_response.MsgContent;

          // Set Response Balance
          _report_play_data.response.SessionBalanceAfterPlay = _report_play_data.request.SessionBalanceAfterPlay;
        } // for

        // Begin Transaction
        _sql_trx = SqlConn.BeginTransaction();

        //
        // Process all plays
        //
        ReportPlays.rollback_work = false;
        while (true)
        {
          if (ReportPlays.rollback_work)
          {
            _sql_trx.Rollback();

            // For show in logger
            _num_rollback_work++;

            //
            // ROLLBACK (Don't insert DB_InsertWCPMessage, DB_InsertWCPTransaction)
            //

            //
            // Send all prcessed messages
            //
            for (_idx_play = ReportPlays.num_plays - 1; _idx_play >= 0; _idx_play--)
            {
              // Get play data
              _report_play_data = ReportPlays.GetPlay(_idx_play);

              // Rollback PlaySession.Update
              if (_report_play_data.play_session_updated)
              {
                _report_play_data.play_session.Rollback(_report_play_data.balance_before_play);
                _report_play_data.play_session_updated = false;
              }

              // Send and Delete all processed transactions
              if (_report_play_data.finished_process)
              {
                _report_play_data.common_data.str_response = _report_play_data.common_data.wcp_response.ToXml();

                _report_play_data.common_data.tcp_client.Send(_report_play_data.common_data.str_response);
                NetLog.Add(NetEvent.MessageSent, _report_play_data.common_data.tcp_client.Identity, _report_play_data.common_data.str_response);

                ReportPlays.DeletePlay(_idx_play);
              }

            } // for _idx_play

            // Begin Transaction
            _sql_trx = SqlConn.BeginTransaction();

            ReportPlays.rollback_work = false;

          } // if

          if (ReportPlays.rollback_work)
          {
            continue;
          }

          //
          // Specific process transaction
          //

          _tick1 = Environment.TickCount;

          for (_idx_play = 0; _idx_play < ReportPlays.num_plays; _idx_play++)
          {
            // Get play data
            _report_play_data = ReportPlays.GetPlay(_idx_play);

            _report_play_data.force_end_session = false;

            _report_play_data.balance_before_play = _report_play_data.request.SessionBalanceBeforePlay;
            _report_play_data.balance_before_play /= 100;
            _report_play_data.played_amount = _report_play_data.request.PlayedAmount;
            _report_play_data.played_amount /= 100;
            _report_play_data.won_amount = _report_play_data.request.WonAmount;
            _report_play_data.won_amount /= 100;
            _report_play_data.balance_after_play = _report_play_data.request.SessionBalanceAfterPlay;
            _report_play_data.balance_after_play /= 100;
            _report_play_data.denomination = _report_play_data.request.Denomination;
            _report_play_data.denomination /= 100;

            _report_play_data.game_name = _report_play_data.request.GameId;

            // Register Terminal/Game
            DbCache.RegisterTerminalGame(_report_play_data.common_data.tcp_client.InternalId, _report_play_data.request.GameId, _sql_trx);

            // Get PlaySession
            _report_play_data.play_session = PlaySession.Get(_report_play_data.common_data.tcp_client.InternalId, 
                                                             _report_play_data.request.CardSessionId,
                                                             _sql_trx);

            if (_report_play_data.play_session == null)
            {
              _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_VALID;
              _report_play_data.finished_process = true;
              ReportPlays.rollback_work = true;
            }

            _report_play_data.stand_alone_session = _report_play_data.play_session.StandAloneSession;

            // Check Card Session Id
            if (_report_play_data.request.CardSessionId < 0)
            {
              _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_VALID;
              _report_play_data.finished_process = true;
              ReportPlays.rollback_work = true;
            }
          } // for

          if (ReportPlays.rollback_work)
          {
            continue;
          }

          // Initialize plays DataTable
          InitializePlaysDataTable(ReportPlays);

          _tick2 = Environment.TickCount;
          _interval_init_data = Misc.GetElapsedTicks(_tick1, _tick2);

          _tick1 = Environment.TickCount;

          // Insert Plays
          BU_InsertPlays(ReportPlays, _sql_trx);

          _tick2 = Environment.TickCount;
          _interval_insert_play = Misc.GetElapsedTicks(_tick1, _tick2);

          if (ReportPlays.rollback_work)
          {
            continue;
          }

          //
          // [!play_already_exists] continue to process, otherwise rows removed of DataTable.
          //

          // Check Card SessionId and reported balance (not stand alone sessions)
          CheckCardSessionIdAndReportedBalance(ReportPlays);
          
          if (ReportPlays.rollback_work)
          {
            continue;
          }

          //
          // ACC 10-JUL-2012 No change account balance
          //
          //
          // Get from ReportPlays.plays_to_process contains all plays with !stand_alone_session
          // for do Batch update in DB_UpdateAccountBalance and DB_InsertMovement functions
          //
          //////_tick1 = Environment.TickCount;
          // ACC 10-JUL-2012 No change account balance
          //////ReportPlays.rows_to_update = ReportPlays.plays_to_process.Select("BU_COLUMN_TERMINAL_TYPE = 1");
          //////BU_UpdateAccountBalance(ReportPlays, _sql_trx);
          //////if (ReportPlays.rollback_work)
          //////{
          //////  continue;
          //////}
          // ACC 10-JUL-2012 No change account balance. No insert movement
          //////// ACC 09-JUL-2012 Change where: "BU_COLUMN_TERMINAL_TYPE = 1" instead "BU_COLUMN_STAND_ALONE_SESSION = 0"
          //////ReportPlays.rows_to_update = ReportPlays.plays_to_process.Select("BU_COLUMN_TERMINAL_TYPE = 1");
          //////BU_InsertAccountMovements(ReportPlays, MovementType.Play, _sql_trx);
          //////if (ReportPlays.rollback_work)
          //////{
          //////  continue;
          //////}
          //////_tick2 = Environment.TickCount;
          //////_interval_update_CARD = Misc.GetElapsedTicks(_tick1, _tick2);

          //
          // RCI 05-APR-2012: Process GAME_METERS for !stand_alone_session
          //

          _tick1 = Environment.TickCount;

          // ACC 09-JUL-2012 Change where: "BU_COLUMN_TERMINAL_TYPE = 1" instead "BU_COLUMN_STAND_ALONE_SESSION = 0"
          ReportPlays.rows_to_update = ReportPlays.plays_to_process.Select("BU_COLUMN_TERMINAL_TYPE = 1");
          BU_InsertOrUpdateGameMeters(ReportPlays, _sql_trx);

          if (ReportPlays.rollback_work)
          {
            continue;
          }

          _tick2 = Environment.TickCount;
          _interval_GAME_METERS = Misc.GetElapsedTicks(_tick1, _tick2);

          //
          // Process all plays (stand_alone_session && !stand_alone_session)
          //

          _tick1 = Environment.TickCount;

          // RCI 13-AUG-2010: Only UpdatePlaySessions for plays where TransactionId = 0.
          //                  If Transaction > 0, PlaySession will be updated on EndCardSession.
          // ACC 09-JUL-2012 Add into where: "BU_COLUMN_TERMINAL_TYPE = 1" 
          //                 And remove "AND BU_COLUMN_TRANSACTION_ID = 0"
          ReportPlays.rows_to_update = ReportPlays.plays_to_process.Select("BU_COLUMN_CARD_SESSION_ID > 0 " +
                                                                           "AND BU_COLUMN_TERMINAL_TYPE = 1 ");
          BU_UpdatePlaySessions(ReportPlays, _sql_trx);

          _tick2 = Environment.TickCount;
          _interval_update_PS = Misc.GetElapsedTicks(_tick1, _tick2);

          if (ReportPlays.rollback_work)
          {
            continue;
          }

          //
          // ALESIS PROCESS
          //
          ReportPlays.rows_to_update = ReportPlays.plays_to_process.Select("BU_COLUMN_CARD_SESSION_ID > 0");
          AlesisProcess(ReportPlays, _sql_trx);

          if (ReportPlays.rollback_work)
          {
            continue;
          }

          //
          // Common end process transaction
          //

          //
          // Set response code
          //
          for (_idx_play = 0; _idx_play < ReportPlays.num_plays; _idx_play++)
          {
            // Get play data
            _report_play_data = ReportPlays.GetPlay(_idx_play);

            if (!_report_play_data.finished_process)
            {
              _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
            }

            _report_play_data.common_data.str_response = _report_play_data.common_data.wcp_response.ToXml();

          } // for _idx_play

          //
          // ACC 10-JUL-2012 No wcp_message, wcp trx.
          //
          ////////for (_idx_play = 0; _idx_play < ReportPlays.num_plays; _idx_play++)
          ////////{
          ////////  // Get play data
          ////////  _report_play_data = ReportPlays.GetPlay(_idx_play);
          ////////  try
          ////////  {
          ////////    //
          ////////    // TODO: Batch Update: DB_InsertWCPMessage
          ////////    //
          ////////    //////////WCP_BusinessLogic.DB_InsertWCPMessage(_report_play_data.common_data.tcp_client.InternalId,
          ////////    //////////                                      _report_play_data.common_data.wcp_request.MsgHeader.TerminalSessionId,
          ////////    //////////                                      true,
          ////////    //////////                                      _report_play_data.common_data.str_response,
          ////////    //////////                                      _report_play_data.common_data.wcp_request.MsgHeader.SequenceId,
          ////////    //////////                                      out _report_play_data.common_data.wcp_message_id,
          ////////    //////////                                      _sql_trx);
          ////////    //
          ////////    // TODO: Batch Update: Update wcp trx
          ////////    //
          ////////    //////////WCP_BusinessLogic.DB_UpdateWCPTransaction(_report_play_data.common_data.tcp_client.InternalId,
          ////////    //////////                                          _report_play_data.common_data.wcp_request.MsgHeader.SequenceId,
          ////////    //////////                                          true,
          ////////    //////////                                          _report_play_data.common_data.wcp_message_id,
          ////////    //////////                                          _sql_trx);
          ////////  }
          ////////  catch (WCP_Exception wcp_ex)
          ////////  {
          ////////    _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = wcp_ex.ResponseCode;
          ////////    _report_play_data.common_data.wcp_response.MsgHeader.ResponseCodeText = wcp_ex.ResponseCodeText;
          ////////    _report_play_data.finished_process = true;
          ////////    ReportPlays.rollback_work = true;
          ////////  }
          ////////  catch (Exception ex)
          ////////  {
          ////////    _report_play_data.common_data.wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_ERROR;
          ////////    _report_play_data.finished_process = true;
          ////////    ReportPlays.rollback_work = true;
          ////////    Log.Exception(ex);
          ////////  }
          ////////} // for _idx_play

          if (ReportPlays.rollback_work)
          {
            continue;
          }

          //
          // Finished all transactions
          //
          for (_idx_play = 0; _idx_play < ReportPlays.num_plays; _idx_play++)
          {
            // Get play data
            _report_play_data = ReportPlays.GetPlay(_idx_play);

            if (!_report_play_data.finished_process)
            {
              _report_play_data.finished_process = true;
            }
          } // for _idx_play

          _tick1 = Environment.TickCount;

          // End Transaction
          _sql_trx.Commit();

          _tick2 = Environment.TickCount;
          _interval_commit = Misc.GetElapsedTicks(_tick1, _tick2);

          // End process, all trx processed.
          break;

        } // while (true)

        _tick1 = Environment.TickCount;

        //
        // Send all messages
        //
        for (_idx_play = 0; _idx_play < ReportPlays.num_plays; _idx_play++)
        {
          // Get play data
          _report_play_data = ReportPlays.GetPlay(_idx_play);

          _sent = _report_play_data.common_data.tcp_client.Send(_report_play_data.common_data.str_response);

          if (!_sent)
          {
            // Disconnected and work commited!!!
            Log.Warning("Work commited: disconnected. " + "\n - - - - - ---> " + _report_play_data.common_data.xml_message + "\n - - - - - <--- " + _report_play_data.common_data.str_response);
          }
          else
          {
            NetLog.Add(NetEvent.MessageSent, _report_play_data.common_data.tcp_client.Identity, _report_play_data.common_data.str_response);
          }

        } // for _idx_play

        _tick2 = Environment.TickCount;
        _interval_sent = Misc.GetElapsedTicks(_tick1, _tick2);

        _tick2 = Environment.TickCount;
        _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);

        if (_interval_all_process > 3000)
        {
          Log.Warning("Play times: [Trx/TrxOK/RBacks] Ini/Insert/UpdCard/GM/UpdPS/Commit/Sent/Total = [" +
                                                      _num_initial_trx.ToString() + "/" +
                                                      ReportPlays.num_plays.ToString() + "/" +
                                                      _num_rollback_work.ToString() + "] " +
                                                      _interval_init_data.ToString() + "/" +
                                                      _interval_insert_play.ToString() + "/" +
                                                      _interval_update_CARD.ToString() + "/" +
                                                      _interval_GAME_METERS.ToString() + "/" +
                                                      _interval_update_PS.ToString() + "/" +
                                                      _interval_commit.ToString() + "/" +
                                                      _interval_sent.ToString() + "/" +
                                                      _interval_all_process.ToString());
        }

        _error = false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return;
      }
      finally
      {
        if ( _sql_trx != null )
        {
          if ( _sql_trx.Connection != null )
          {
            try 
            {
              _sql_trx.Rollback();
            }
            catch (Exception _ex)
            { 
              Log.Exception (_ex);
            }
            if ( !_error )
            {
              Log.Message("*** ERROR DB TRX ROLLBACK on Play Messages.");
            }
          }
          _sql_trx = null;
        }
      } // finally

    } // Process

  }
}
