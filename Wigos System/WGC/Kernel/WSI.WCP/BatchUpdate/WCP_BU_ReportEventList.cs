//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_BU_ReportEventList.cs
// 
//   DESCRIPTION: WCP_BU_ReportEventList class
// 
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 06-JUL-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-JUL-2010 RCI    First release.
// 23-APR-2010 JAB    SAS CRC ERROR
// 02-JUL-2012 ACC    Added Peru External Events.
// 21-MAY-2013 RRB    Fixed Bug #782: Client 3GS send jackpots with data bad formatted.
// 24-DEC-2013 ICS    Added functionality to set status of a terminal.
// 23-JAN-2014 ICS    Fixed Bug WIGOSTITO-1003: Bad alarm description
// 06-FEB-2014 ICS    Fixed Bug WIGOSTITO-1051: Unknown events in the WCP
// 05-MAR-2014 AMF    Fixed Bug WIGOSTITO-1106: Increase delta
// 05-MAR-2014 JCOR   Added PENDING_TRANSFER.
// 03-JUL-2014 JMM    Added Technician card events
// 19-JUN-2015 XCD    Automatic Handpays payment
// 29-JUN-2015 FJC    Product Backlog Item 282.
// 24-FEB-2016 JCA    PBI 9311: Intrusi�n de m�quinas: Inserci�n de Alarma
// 10-AUG-2016 ETP    Fixed bug 16475: Add description to alarm.
// 27-SEP-2016 JMM    PBI 18229:WCP - Proceso de la alarma Ticket Amount Not Multiple of Machine Denomination
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;

namespace WSI.WCP
{
  public static class WCP_BU_ReportEventList
  {
    private const Int32 DB_EVENT_HISTORY_DEVICE_STATUS = 1;
    private const Int32 DB_EVENT_HISTORY_OPERATION = 2;

    #region Constants

    private const Int16 COLUMN_SEQUENCE_ID = 0;
    private const Int16 COLUMN_TERMINAL_ID = 1;
    private const Int16 COLUMN_SESSION_ID = 2;
    private const Int16 COLUMN_TERMINAL_OBJECT = 3;
    private const Int16 COLUMN_DATETIME = 4;
    private const Int16 COLUMN_EVENT_TYPE = 5;
    private const Int16 COLUMN_DEVICE_CODE = 6;
    private const Int16 COLUMN_DEVICE_STATUS = 7;
    private const Int16 COLUMN_DEVICE_PRIORITY = 8;
    private const Int16 COLUMN_OPERATION_CODE = 9;
    private const Int16 COLUMN_OPERATION_DATA = 10;
    private const Int16 COLUMN_RESPONSE_CODE = 11;
    private const Int16 COLUMN_TECHNICAL_ACCOUNT_ID = 12;
    private const Int16 COLUMN_TECHNICAL_MODE = 13;

    #endregion // Constants

    #region Attributes

    
    private static DataTable m_dt_events = null;
    private static SqlDataAdapter m_sql_adap = null;
    private static DateTime m_min_datetime = new DateTime(1970, 1, 1);

    #endregion // Attributes

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init attributes of the static class.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      SqlCommand _sql_command;
      String _sql_query;

      // Already init.
      
      if (m_dt_events != null)
      {
        return;
      }

      // Create DataTable Columns

      m_dt_events = new DataTable("EVENTS");

      m_dt_events.Columns.Add("SEQUENCE_ID", Type.GetType("System.Int64"));
      m_dt_events.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      m_dt_events.Columns.Add("SESSION_ID", Type.GetType("System.Int64"));
      m_dt_events.Columns.Add("TERMINAL_OBJECT", Type.GetType("System.Object"));
      m_dt_events.Columns.Add("DATETIME", Type.GetType("System.DateTime"));
      m_dt_events.Columns.Add("EVENT_TYPE", Type.GetType("System.Int32"));
      m_dt_events.Columns.Add("DEVICE_CODE", Type.GetType("System.Int32"));
      m_dt_events.Columns.Add("DEVICE_STATUS", Type.GetType("System.Int32"));
      m_dt_events.Columns.Add("DEVICE_PRIORITY", Type.GetType("System.Int32"));
      m_dt_events.Columns.Add("OPERATION_CODE", Type.GetType("System.Int32"));
      m_dt_events.Columns.Add("OPERATION_DATA", Type.GetType("System.Decimal"));
      m_dt_events.Columns.Add("RESPONSE_CODE", Type.GetType("System.Object"));
      m_dt_events.Columns.Add("TECHNICAL_ACCOUNT_ID", Type.GetType("System.Int64"));
      m_dt_events.Columns.Add("TECHNICAL_MODE", Type.GetType("System.Int16"));

      // Create Insert Command and SqlDataAdapter.

      _sql_query = "INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID " +
                                            ", EH_SESSION_ID " +
                                            ", EH_DATETIME " +
                                            ", EH_EVENT_TYPE " +
                                            ", EH_DEVICE_CODE " +
                                            ", EH_DEVICE_STATUS " +
                                            ", EH_DEVICE_PRIORITY " +
                                            ", EH_OPERATION_CODE " +
                                            ", EH_OPERATION_DATA) " +
                                      "VALUES (@TerminalId " +
                                            ", @SessionId " +
                                            ", @DateTime " +
                                            ", @EventType " +
                                            ", @DeviceCode " +
                                            ", @DeviceStatus " +
                                            ", @DevicePriority " +
                                            ", @OperationCode " +
                                            ", @OperationData) ";

      _sql_command = new SqlCommand(_sql_query);
      _sql_command.Parameters.Add("@TerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _sql_command.Parameters.Add("@SessionId", SqlDbType.BigInt).SourceColumn = "SESSION_ID";
      _sql_command.Parameters.Add("@DateTime", SqlDbType.DateTime).SourceColumn = "DATETIME";
      _sql_command.Parameters.Add("@EventType", SqlDbType.Int).SourceColumn = "EVENT_TYPE";
      _sql_command.Parameters.Add("@DeviceCode", SqlDbType.Int).SourceColumn = "DEVICE_CODE";
      _sql_command.Parameters.Add("@DeviceStatus", SqlDbType.Int).SourceColumn = "DEVICE_STATUS";
      _sql_command.Parameters.Add("@DevicePriority", SqlDbType.Int).SourceColumn = "DEVICE_PRIORITY";
      _sql_command.Parameters.Add("@OperationCode", SqlDbType.Int).SourceColumn = "OPERATION_CODE";
      _sql_command.Parameters.Add("@OperationData", SqlDbType.Money).SourceColumn = "OPERATION_DATA";

      m_sql_adap = new SqlDataAdapter();
      m_sql_adap.SelectCommand = null;
      m_sql_adap.InsertCommand = _sql_command;
      m_sql_adap.UpdateCommand = null;
      m_sql_adap.DeleteCommand = null;
      m_sql_adap.ContinueUpdateOnError = true;

      m_sql_adap.UpdateBatchSize = 500;
      m_sql_adap.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Add an event of a terminal in the DataTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SequenceId
    //          - Terminal
    //          - SessionId
    //          - EventType
    //          - DeviceStatus
    //          - Operation
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void AddEvent(Int64 SequenceId, Terminal Terminal, Int64 SessionId,
                                Int32 EventType, WCP_DeviceStatus DeviceStatus, WCP_Operation Operation,
                                IWcpTaskStatus WcpTaskStatus)
    {
      DataRow _row;
      Decimal _op_data;
      DateTime _event_datetime;

      if (m_dt_events == null)
      {
        WCP_BU_ReportEventList.Init();
      }

      WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR, "No information available.");

      _row = m_dt_events.NewRow();

      _row[COLUMN_SEQUENCE_ID] = SequenceId;
      _row[COLUMN_TERMINAL_ID] = Terminal.terminal_id;
      _row[COLUMN_SESSION_ID] = SessionId;
      _row[COLUMN_TERMINAL_OBJECT] = Terminal;
      _row[COLUMN_EVENT_TYPE] = (Int32)EventType;
      // Device Status
      _row[COLUMN_DEVICE_CODE] = DBNull.Value;
      _row[COLUMN_DEVICE_STATUS] = DBNull.Value;
      _row[COLUMN_DEVICE_PRIORITY] = DBNull.Value;
      // Operation
      _row[COLUMN_OPERATION_CODE] = DBNull.Value;
      _row[COLUMN_OPERATION_DATA] = DBNull.Value;
      _row[COLUMN_TECHNICAL_ACCOUNT_ID] = DBNull.Value;
      _row[COLUMN_TECHNICAL_MODE] = DBNull.Value;
      // Response Code
      _row[COLUMN_RESPONSE_CODE] = WcpTaskStatus;

      if (EventType == DB_EVENT_HISTORY_DEVICE_STATUS)
      {
        _event_datetime = DeviceStatus.LocalTime;
        _row[COLUMN_DEVICE_CODE] = DeviceStatus.Code;
        _row[COLUMN_DEVICE_STATUS] = DeviceStatus.Status;
        _row[COLUMN_DEVICE_PRIORITY] = DeviceStatus.Priority;
      }
      else if (EventType == DB_EVENT_HISTORY_OPERATION)
      {
        _op_data = (Decimal)Operation.Data;
        if (Operation.Code == OperationCodes.WCP_OPERATION_CODE_JACKPOT_WON
            || Operation.Code == OperationCodes.WCP_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE)
        {
          _op_data = _op_data / 100;
        }

        _event_datetime = Operation.LocalTime;
        _row[COLUMN_OPERATION_CODE] = Operation.Code;
        _row[COLUMN_OPERATION_DATA] = _op_data;
        _row[COLUMN_TECHNICAL_ACCOUNT_ID] = Operation.TechnicalAccountId;
        _row[COLUMN_TECHNICAL_MODE] = Operation.TechMode;
      }
      else
      {
        Log.Warning("Exception throw: Unexpected EVENT Type. TerminalId: " + Terminal.terminal_id.ToString() +
                    " SessionId: " + SessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Unexpected EVENT Type.");
      }

      // Check Event DateTime value
      if (DateTime.Compare(_event_datetime, m_min_datetime) < 0)
      {
        Log.Warning("EVENT NOT INSERTED: Terminal: " + Terminal.terminal_id.ToString() +
                    ", Session: " + SessionId.ToString() +
                    ", Sequence: " + SequenceId.ToString() +
                    ", EventType: " + EventType +
                    ", DateTime: " + _event_datetime.ToString() +
                    ". DateTime is earlier than minimum defined (" + m_min_datetime.ToString() + ")");
      }
      else
      {
        _row[COLUMN_DATETIME] = _event_datetime;
        m_dt_events.Rows.Add(_row);
      }
    } // AddEvent

    //------------------------------------------------------------------------------
    // PURPOSE : Add all events of a terminal in the DataTable. The events are in the Request.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Terminal
    //          - Request
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void AddEvents(Terminal Terminal, WCP_Message Request, IWcpTaskStatus WcpTaskStatus)
    {
      WCP_MsgReportEventList _request;

      _request = (WCP_MsgReportEventList)Request.MsgContent;

      // Device List
      foreach (WCP_DeviceStatus _device_status in _request.DeviceStatusList)
      {
        WCP_BU_ReportEventList.AddEvent(Request.MsgHeader.SequenceId,
                                        Terminal,
                                        Request.MsgHeader.TerminalSessionId,
                                        DB_EVENT_HISTORY_DEVICE_STATUS,
                                        _device_status,
                                        null,
                                        WcpTaskStatus);
      } // foreach

      // Operations
      foreach (WCP_Operation _operation in _request.OperationList)
      {
        WCP_BU_ReportEventList.AddEvent(Request.MsgHeader.SequenceId,
                                        Terminal,
                                        Request.MsgHeader.TerminalSessionId,
                                        DB_EVENT_HISTORY_OPERATION,
                                        null,
                                        _operation,
                                        WcpTaskStatus);

      } // foreach
    } // AddEvents

    //------------------------------------------------------------------------------
    // PURPOSE : Add all events of terminals from the Task ArrayList.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Tasks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Process(ArrayList Tasks, SqlConnection SqlConn)
    {
      SqlTransaction _sql_trx;
      Terminal _terminal;
      Int32 _terminal_id;

      _sql_trx = null;

      try
      {
        try
        {
          _sql_trx = SqlConn.BeginTransaction();

          WCP_BU_ReportEventList.Init();

          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _terminal_id = _wcp_task.WcpClient.InternalId;
            _terminal = Terminal.GetTerminal(_terminal_id, _sql_trx);
            WCP_BU_ReportEventList.AddEvents(_terminal, _wcp_task.Request, _wcp_task);
          }

          if (!WCP_BU_ReportEventList.InsertEvents(_sql_trx))
          {
            Log.Error("BU_ReportEventList.Process: No events have been inserted!");
          }
          _sql_trx.Commit();

          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _wcp_task.SendResponse();
          }
        }
        catch
        {
        }
        finally
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              _sql_trx.Rollback();
            }
          }
        }
      }
      catch
      {
      }
    } // Process

    //------------------------------------------------------------------------------
    // PURPOSE : Insert DataTable Rows Events to DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data is inserted correctly or not.
    //
    public static Boolean InsertEvents(SqlTransaction Trx)
    {
      String _rollback_point;
      Boolean _error;

      if (m_dt_events == null)
      {
        Log.Error("WCP_BU_ReportEventList.InsertEvents: Event DataTable is null.");
        return false;
      }

      _rollback_point = "REPORT_EVENT_LIST_BEFORE_INSERT";
      _error = false;

      try
      {
        while (true)
        {
          // If no rows to insert, return depending of _error variable.
          if (m_dt_events.Rows.Count == 0)
          {
            return !_error;
          }

          try
          {
            Trx.Save(_rollback_point);

            m_sql_adap.InsertCommand.Connection = Trx.Connection;
            m_sql_adap.InsertCommand.Transaction = Trx;

            _error = true;
            try
            {
              // Insert Added rows to DB -- Time consuming
              if (m_sql_adap.Update(m_dt_events) == m_dt_events.Rows.Count)
              {
                _error = false;
              }
            }
            catch (Exception ex)
            {
              Log.Warning("Exception 1: BU_SessionMsgReceived.InsertEvents.");
              Log.Exception(ex);
            }

            if (!_error)
            {
              // Everything is ok, send Alesis Events for the inserted rows and return.

              DataRow[] _events;

              _events = m_dt_events.Select("", "TERMINAL_ID, DATETIME");

              foreach (DataRow _row in _events)
              {
                IWcpTaskStatus _wcp_task_status;

                SendAlesisEvent(_row, Trx);
                SaveHandpay(_row, Trx);
                // SSC 19-ABR-2012
                ChangeStacker(_row, Trx);
                // AJQ 26-OCT-2011, Generate Alarms on received events
                GenerateAlarm(_row, Trx);
                // XCD 18-MAY-2015 Set terminal funds transfer status
                TerminalFundsTransferStatus(_row, Trx);

                _wcp_task_status = (IWcpTaskStatus)_row[COLUMN_RESPONSE_CODE];
                _wcp_task_status.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
              }
              return true;
            }
            else
            {
              // Delete rows with errors from DataTable and rows with same Terminal-SequenceId than the deleted rows.
              DeleteRowsWithErrors();
              Trx.Rollback(_rollback_point);
            }
          } // try
          catch (Exception _ex)
          {
            Log.Warning("Exception 2: BU_SessionMsgReceived.InsertEvents.");
            Log.Exception(_ex);
            return false;
          }
        } // while (true)
      } // try
      finally
      {
        m_dt_events.Clear();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update Events as Meters to DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data is updated correctly or not.
    //
    public static Boolean UpdateEventMeters(Int32 TerminalId, UInt32 EventCode, DateTime EventDateTime, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" DECLARE @v_SasAccountDenom  AS  MONEY ");
      _sb.AppendLine(" SET @v_SasAccountDenom = (SELECT   TE_SAS_ACCOUNTING_DENOM ");
      _sb.AppendLine("                             FROM   TERMINALS ");
      _sb.AppendLine("                            WHERE   TE_TERMINAL_ID  =  @pTerminalId) ");
      _sb.AppendLine(" IF EXISTS ( SELECT   1 ");
      _sb.AppendLine("               FROM   TERMINAL_SAS_METERS ");
      _sb.AppendLine("              WHERE   TSM_TERMINAL_ID   =  @pTerminalId ");
      _sb.AppendLine("                AND   TSM_METER_CODE    =  @pMeterCode ");
      _sb.AppendLine("                AND   TSM_GAME_ID       =  0 ");
      _sb.AppendLine("                AND   TSM_DENOMINATION  =  0 ) ");
      _sb.AppendLine(" BEGIN ");
      _sb.AppendLine("     UPDATE   TERMINAL_SAS_METERS ");
      _sb.AppendLine("        SET   TSM_WCP_SEQUENCE_ID       =  TSM_WCP_SEQUENCE_ID + 1 ");
      _sb.AppendLine("            , TSM_LAST_REPORTED         =  @pEventDateTime ");
      _sb.AppendLine("            , TSM_LAST_MODIFIED         =  GetDate() ");
      _sb.AppendLine("            , TSM_METER_VALUE           =  (CASE WHEN (TSM_METER_VALUE + 1 > TSM_METER_MAX_VALUE) ");
      _sb.AppendLine("                                                   THEN 0 ");
      _sb.AppendLine("                                                   ELSE (TSM_METER_VALUE + 1) ");
      _sb.AppendLine("                                            END) ");
      _sb.AppendLine("            , TSM_DELTA_VALUE           =  TSM_DELTA_VALUE + 1 ");
      _sb.AppendLine("            , TSM_SAS_ACCOUNTING_DENOM  =  ISNULL(@v_SasAccountDenom, 0.01) ");
      _sb.AppendLine("      WHERE   TSM_TERMINAL_ID   =  @pTerminalId ");
      _sb.AppendLine("        AND   TSM_METER_CODE    =  @pMeterCode ");
      _sb.AppendLine("        AND   TSM_GAME_ID       =  0 ");
      _sb.AppendLine("        AND   TSM_DENOMINATION  =  0 ");
      _sb.AppendLine(" END ");
      _sb.AppendLine(" ELSE ");
      _sb.AppendLine(" BEGIN ");
      _sb.AppendLine("     INSERT INTO TERMINAL_SAS_METERS ");
      _sb.AppendLine("                ( ");
      _sb.AppendLine("                  TSM_TERMINAL_ID ");
      _sb.AppendLine("                , TSM_METER_CODE ");
      _sb.AppendLine("                , TSM_GAME_ID ");
      _sb.AppendLine("                , TSM_DENOMINATION ");
      _sb.AppendLine("                , TSM_WCP_SEQUENCE_ID ");
      _sb.AppendLine("                , TSM_LAST_REPORTED ");
      _sb.AppendLine("                , TSM_LAST_MODIFIED ");
      _sb.AppendLine("                , TSM_METER_VALUE ");
      _sb.AppendLine("                , TSM_METER_MAX_VALUE ");
      _sb.AppendLine("                , TSM_DELTA_VALUE ");
      _sb.AppendLine("                , TSM_RAW_DELTA_VALUE ");
      _sb.AppendLine("                , TSM_DELTA_UPDATING ");
      _sb.AppendLine("                , TSM_SAS_ACCOUNTING_DENOM ");
      _sb.AppendLine("                ) ");
      _sb.AppendLine("          VALUES ");
      _sb.AppendLine("                ( @pTerminalId ");
      _sb.AppendLine("                , @pMeterCode ");
      _sb.AppendLine("                , 0 ");
      _sb.AppendLine("                , 0 ");
      _sb.AppendLine("                , 1 ");
      _sb.AppendLine("                , @pEventDateTime ");
      _sb.AppendLine("                , GetDate() ");
      _sb.AppendLine("                , 1 ");
      _sb.AppendLine("                , 10000000000 ");
      _sb.AppendLine("                , 1 ");
      _sb.AppendLine("                , 0 ");
      _sb.AppendLine("                , 0 ");
      _sb.AppendLine("                , ISNULL(@v_SasAccountDenom, 0.01) ");
      _sb.AppendLine("                ) ");
      _sb.AppendLine(" END ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pMeterCode", SqlDbType.Int).Value = EventCode;
          _sql_cmd.Parameters.Add("@pEventDateTime", SqlDbType.DateTime).Value = EventDateTime;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
          else
          {
            Log.Warning("UpdateEventMeters: Updating EVENT METER [Not updated]: TerminalId: " + TerminalId.ToString() + ", EventCode: " + EventCode.ToString());
          }
        } // using SqlCommand
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("UpdateEventMeters: Updating EVENT METER [Exception]: TerminalId: " + TerminalId.ToString() + ", EventCode: " + EventCode.ToString());
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Note Acceptor --> Update notes with StackerId older than the current StackerId.
    //
    //  PARAMS :
    //      - INPUT :
    //          - EventRow
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void ChangeStacker(DataRow Row, SqlTransaction Trx)
    {
      Int64 _stacker_id;
      String _sql_str;
      //Int32 _num_rows_updated;
      Terminal _terminal;

      if ((Int32)Row[COLUMN_EVENT_TYPE] != DB_EVENT_HISTORY_OPERATION)
      {
        return;
      }

      if ((OperationCodes)Row[COLUMN_OPERATION_CODE] != OperationCodes.WCP_OPERATION_CODE_CHANGE_STACKER)
      {
        return;
      }

      try
      {
        _terminal = (Terminal)Row[COLUMN_TERMINAL_OBJECT];
        _stacker_id = (Int64)(Decimal)Row[COLUMN_OPERATION_DATA];

        _sql_str = "UPDATE   TERMINAL_MONEY                       " +
                   "   SET   TM_INTO_ACCEPTOR  = @pIntoAcceptor   " +
                   " WHERE   TM_TERMINAL_ID    = @pTerminalId      " +
                   "   AND   TM_STACKER_ID     < @pStackerId      ";

        //Update notes with StackerId older than the current StackerId --> into_acceptor = false
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pIntoAcceptor", SqlDbType.Bit).Value = 0;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal.terminal_id;
          _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = _stacker_id;

          _sql_cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("ChangeStacker: Updating TERMINAL_MONEY [Exception]: into_acceptor.");

        return;
      }
    }

    private static void GenerateAlarm(DataRow EventRow, SqlTransaction Trx)
    {
      Boolean _alarm;
      AlarmSeverity _severity;
      UInt32 _alarm_code;
      WSI.Common.Cashier.EnumEventType _event_type;
      WSI.Common.Cashier.EnumDeviceCode _device_code;
      WSI.Common.Cashier.EnumDeviceStatus _device_status;
      WSI.Common.Cashier.EnumEventSeverity _device_severity;
      WSI.Common.Cashier.EnumOperationCode _operation_code;
      Decimal _operation_data;
      Int64 _technical_accound_id;
      Int16 _technical_mode;

      _event_type = Cashier.EnumEventType.WCP_EVENT_TYPE_OPERATION;
      _device_code = Cashier.EnumDeviceCode.COMMON_DEVICE_CODE_NO_DEVICE;
      _device_status = Cashier.EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN;
      _device_severity = Cashier.EnumEventSeverity.COMMON_SEVERITY_OK;
      _operation_code = Cashier.EnumOperationCode.COMMON_OPERATION_CODE_NO_OPERATION;
      _operation_data = 0;
      _alarm_code = 0;
      _technical_accound_id = -1;
      _technical_mode = 0;

      _alarm = true;
      _severity = AlarmSeverity.Error;

      switch ((Int32)EventRow[COLUMN_EVENT_TYPE])
      {
        case DB_EVENT_HISTORY_DEVICE_STATUS:
          {
            _device_severity = ((Cashier.EnumEventSeverity)((Int32)EventRow[COLUMN_DEVICE_PRIORITY]));

            switch (_device_severity)
            {
              case Cashier.EnumEventSeverity.COMMON_SEVERITY_OK:
                _alarm = false;

                break;

              case Cashier.EnumEventSeverity.COMMON_SEVERITY_WARNING:
                _severity = AlarmSeverity.Warning;

                break;

              case Cashier.EnumEventSeverity.COMMON_SEVERITY_ERROR:
                _severity = AlarmSeverity.Error;

                break;
            }

            if (_alarm)
            {
              _event_type = Cashier.EnumEventType.WCP_EVENT_TYPE_DEVICE_STATUS;
              _device_code = ((Cashier.EnumDeviceCode)((Int32)EventRow[COLUMN_DEVICE_CODE]));
              _device_status = ((Cashier.EnumDeviceStatus)((Int32)EventRow[COLUMN_DEVICE_STATUS])); ;

              _alarm_code = 0x00070000 + ((((UInt32)_device_code) << 8) & 0x0000FF00) + (((UInt32)_device_status) & 0x000000FF);
            }

            //<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>
            // //TJG 22-FEB-2012 Test Code to force SAS METERS BACK + SAS METERS JUMP
            //{
            //  UInt32 [] _event_data;
            //  UInt32    _idx;
            //  UInt32    _count;  

            //  _event_data = new UInt32 [100];

            //  _count = 0;
            //  _event_data[_count] = 0x00021010; _count++;
            //  _event_data[_count] = 0x00022000; _count++;
            //  _event_data[_count] = 0x00023000; _count++;
            //  _event_data[_count] = 0x00024000; _count++;
            //  _event_data[_count] = 0x00025000; _count++;
            //  _event_data[_count] = 0x00025010; _count++;
            //  _event_data[_count] = 0x00025030; _count++;
            //  _event_data[_count] = 0x00025510; _count++;
            //  _event_data[_count] = 0x00025FFF; _count++;
            //  _event_data[_count] = 0x00026000; _count++;
            //  _event_data[_count] = 0x00026789; _count++;
            //  _event_data[_count] = 0x00031111; _count++;
            //  _event_data[_count] = 0x00031010; _count++;
            //  _event_data[_count] = 0x00032000; _count++;
            //  _event_data[_count] = 0x00033000; _count++;
            //  _event_data[_count] = 0x00034000; _count++;
            //  _event_data[_count] = 0x00035000; _count++;
            //  _event_data[_count] = 0x00031111; _count++;

            //  for ( _idx = 0; _idx < _count; _idx++ )
            //  {
            //    GenerateAlarmOperationEvent (_event_data[_idx],
            //                                (DateTime) EventRow[COLUMN_DATETIME],
            //                                (Terminal) EventRow[COLUMN_TERMINAL_OBJECT],
            //                                (Int64) EventRow[COLUMN_SESSION_ID],
            //                                Trx);
            //  }
            //}
            //<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>

          }
          break;

        case DB_EVENT_HISTORY_OPERATION:
          {
            switch ((OperationCodes)EventRow[COLUMN_OPERATION_CODE])
            {

              case OperationCodes.WCP_OPERATION_CODE_EVENT:

                if ((TechModeEnter)(Int16)EventRow[COLUMN_TECHNICAL_MODE] == TechModeEnter.WCP_TECH_MODE_PIN_PAD ||
                    (TechModeEnter)(Int16)EventRow[COLUMN_TECHNICAL_MODE] == TechModeEnter.WCP_TECH_MODE_CARD_WITH_VALIDATION ||
                    (TechModeEnter)(Int16)EventRow[COLUMN_TECHNICAL_MODE] == TechModeEnter.WCP_TECH_MODE_CARD_WITHOUT_VALIDATION)
                {

                  _technical_accound_id = (Int64)EventRow[COLUMN_TECHNICAL_ACCOUNT_ID];
                }

                GenerateAlarmOperationEvent((UInt32)(Decimal)EventRow[COLUMN_OPERATION_DATA],
                                            (DateTime)EventRow[COLUMN_DATETIME],
                                            (Terminal)EventRow[COLUMN_TERMINAL_OBJECT],
                                            (Int64)EventRow[COLUMN_SESSION_ID],
                                            _technical_accound_id,
                                            Trx);

                _alarm = false;

                break;

              case OperationCodes.WCP_OPERATION_CODE_START:
              case OperationCodes.WCP_OPERATION_CODE_SHUTDOWN:
              case OperationCodes.WCP_OPERATION_CODE_RESTART:
              case OperationCodes.WCP_OPERATION_CODE_TUNE_SCREEN:
              case OperationCodes.WCP_OPERATION_CODE_LAUNCH_EXPLORER:
              case OperationCodes.WCP_OPERATION_CODE_ASSIGN_KIOSK:
              case OperationCodes.WCP_OPERATION_CODE_UNLOCK_DOOR:
              case OperationCodes.WCP_OPERATION_CODE_LOGIN:
              case OperationCodes.WCP_OPERATION_CODE_LOGOUT:
              case OperationCodes.WCP_OPERATION_CODE_DISPLAY_SETTINGS:
              case OperationCodes.WCP_OPERATION_CODE_DOOR_CLOSED:
              case OperationCodes.WCP_OPERATION_CODE_UNBLOCK_KIOSK:
              case OperationCodes.WCP_OPERATION_CODE_TEST_MODE_LEAVE:
              case OperationCodes.WCP_OPERATION_CODE_ATTENDANT_MENU_EXIT:
              case OperationCodes.WCP_OPERATION_CODE_OPERATOR_MENU_EXIT:
              case OperationCodes.WCP_OPERATION_CODE_HANDPAY_REQUESTED:
              case OperationCodes.WCP_OPERATION_CODE_HANDPAY_RESET:
              case OperationCodes.WCP_OPERATION_CODE_TERMINAL_STATUS_CHANGE:
              case OperationCodes.WCP_OPERATION_CODE_CHANGE_STACKER:
              case OperationCodes.WCP_OPERATION_CODE_TECH_OFFLINE_INSERTED:
              case OperationCodes.WCP_OPERATION_CODE_TECH_OFFLINE_REMOVED:
                _severity = AlarmSeverity.Info;

                break;

              case OperationCodes.WCP_OPERATION_CODE_JACKPOT_WON:
              case OperationCodes.WCP_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE:
              case OperationCodes.WCP_OPERATION_CODE_DEASSIGN_KIOSK:
              case OperationCodes.WCP_OPERATION_CODE_DOOR_OPENED:
              case OperationCodes.WCP_OPERATION_CODE_CALL_ATTENDANT:
              case OperationCodes.WCP_OPERATION_CODE_TEST_MODE_ENTER:
              case OperationCodes.WCP_OPERATION_CODE_ATTENDANT_MENU_ENTER:
              case OperationCodes.WCP_OPERATION_CODE_OPERATOR_MENU_ENTER:
              case OperationCodes.WCP_OPERATION_CODE_SECURITY_CASHOUT_CENTS:
              case OperationCodes.WCP_OPERATION_CODE_SECURITY_CASHOUT_UNITS:
              case OperationCodes.WCP_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED:
              case OperationCodes.WCP_OPERATION_CODE_BLOCK_KIOSK:
              case OperationCodes.WCP_OPERATION_CODE_CARD_ABANDONED:
              case OperationCodes.WCP_OPERATION_CODE_PENDING_TRANSFER_IN:
              case OperationCodes.WCP_OPERATION_CODE_PENDING_TRANSFER_OUT:
              case OperationCodes.WCP_OPERATION_CODE_TICKET_NOT_EVEN_MULTIPLE_MACHINE_DENOM:
                _severity = AlarmSeverity.Warning;

                break;

              case OperationCodes.WCP_OPERATION_CODE_LOGIN_ERROR:
              case OperationCodes.WCP_OPERATION_CODE_GAME_FATAL_ERROR:
              case OperationCodes.WCP_OPERATION_CODE_TITO_WRONG_PRINTED_TICKET_ID:
              case OperationCodes.WCP_OPERATION_CODE_TITO_CANT_READ_TICKET_INFO:
              case OperationCodes.WCP_OPERATION_CODE_AFT_NOT_AUTHORIZED:
              case OperationCodes.WCP_OPERATION_CODE_LOCK_AND_AFT_AMOUNT_MISMATCH:
                _severity = AlarmSeverity.Error;

                break;

              case OperationCodes.WCP_OPERATION_CODE_DATABASE_ALL_FAIL:
              case OperationCodes.WCP_OPERATION_CODE_DATABASE_COPY_FAIL:
                _severity = (OperationCodes)EventRow[COLUMN_OPERATION_CODE] == OperationCodes.WCP_OPERATION_CODE_DATABASE_COPY_FAIL ? AlarmSeverity.Warning : AlarmSeverity.Error;

                if (!PeruEvents.InsertTechnicalEvent(((Terminal)EventRow[COLUMN_TERMINAL_OBJECT]).terminal_id, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_HOST_MEMORY_FAIL, WGDB.Now))
                {
                  Log.Message("InsertTechnicalEvent: return false. ");
                }
                if (!ColJuegos.InsertColJuegosEvent(ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_HOST_MEMORY_FAIL, ((Terminal)EventRow[COLUMN_TERMINAL_OBJECT]).terminal_id))
                {
                  Log.Message("InsertColJuegosEvent: return false. ");
                }
                break;

              case OperationCodes.WCP_OPERATION_CODE_BILL_IN_LIMIT_NOTE_ACCEPTOR_LOCK:
              case OperationCodes.WCP_OPERATION_CODE_BILL_IN_LIMIT_EGM_LOCK:
              case OperationCodes.WCP_OPERATION_CODE_BILL_IN_LIMIT_HANDPAY_LOCK:
                _severity = AlarmSeverity.Warning;
                break;

              default:
                _alarm = false;
                break;
            }

            if (_alarm)
            {
              _event_type = Cashier.EnumEventType.WCP_EVENT_TYPE_OPERATION;
              _operation_code = (WSI.Common.Cashier.EnumOperationCode)EventRow[COLUMN_OPERATION_CODE];
              _operation_data = (Decimal)EventRow[COLUMN_OPERATION_DATA];
              _alarm_code = 0x00060000 + ((UInt32)(((Int32)EventRow[COLUMN_OPERATION_CODE]) & 0x0000FFFF));
              _technical_accound_id = (Int64)EventRow[COLUMN_TECHNICAL_ACCOUNT_ID];
              _technical_mode = (Int16)EventRow[COLUMN_TECHNICAL_MODE];
            }
          }
          break;

        default:
          _alarm = false;

          break;
      }

      if (_alarm)
      {
        AlarmSourceCode _source_code;
        Int32 _source_id;
        String _source_name;
        String _description;
        Terminal _terminal;
        String _terminal_name;
        String _provider_id;
        TerminalTypes _terminal_type;

        _terminal = (Terminal)EventRow[COLUMN_TERMINAL_OBJECT];

        _description = WSI.Common.Cashier.EvOpGetDescription(_event_type, _device_code, _device_status,
                                                             _device_severity, _operation_code, _operation_data, _technical_accound_id, _technical_mode);

        if (_operation_code == Cashier.EnumOperationCode.COMMON_OPERATION_CODE_TITO_CANT_READ_TICKET_INFO
          || _operation_code == Cashier.EnumOperationCode.COMMON_OPERATION_CODE_TITO_WRONG_PRINTED_TICKET_ID)
        {
          _technical_accound_id = 0;
          _technical_mode = 0;
        }

        WSI.Common.BatchUpdate.TerminalSession.GetData(_terminal.terminal_id, (Int64)EventRow[COLUMN_SESSION_ID], out _provider_id, out _terminal_name, out _terminal_type);
        _source_name = Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);
        _source_id = _terminal.terminal_id;
        _source_code = _terminal_type == TerminalTypes.SAS_HOST ? AlarmSourceCode.TerminalSASMachine : AlarmSourceCode.TerminalWCP;

        if (_operation_code == Cashier.EnumOperationCode.COMMON_OPERATION_CODE_AFT_NOT_AUTHORIZED)
        {
          Int64 _play_session_id;

          _play_session_id = _technical_accound_id;
          _technical_accound_id = 0;
          _technical_mode = 0;
        
          //get accountid by play_session
          if (GeneralParam.GetBoolean("Intrusion", "BlockMachine"))
          {
            WSI.Common.TerminalStatusFlags.DB_SetFlag(TerminalStatusFlags.BITMASK_TYPE.Machine_status, _source_id, (Int32)TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_INTRUSION, Trx);
          }

          if (_play_session_id != 0)
          {
            SqlCommand _sql_command;
            Int64 _account_id;
            String _account_name;
            DateTime _ps_started;
            StringBuilder _sb_sql_str;

            _account_id = 0;
            _account_name = "";
            _ps_started = WGDB.Now;
            _sb_sql_str = new StringBuilder();

            // Get account ID
            _sb_sql_str.AppendLine("   SELECT   ISNULL (AC_ACCOUNT_ID, 0) ");
            _sb_sql_str.AppendLine("          , ISNULL (AC_HOLDER_NAME, '') ");
            _sb_sql_str.AppendLine("          , PS_STARTED ");
            _sb_sql_str.AppendLine("     FROM   PLAY_SESSIONS ");
            _sb_sql_str.AppendLine("LEFT JOIN   ACCOUNTS ON AC_ACCOUNT_ID = PS_ACCOUNT_ID ");
            _sb_sql_str.AppendLine("    WHERE   PS_PLAY_SESSION_ID = @p1");
            _sb_sql_str.AppendLine("      AND   AC_TYPE NOT IN ( @p2, @p3) ");

            _sql_command = new SqlCommand(_sb_sql_str.ToString());
            _sql_command.Connection = Trx.Connection;
            _sql_command.Transaction = Trx;
            _sql_command.Parameters.Add("@p1", SqlDbType.BigInt).Value = _play_session_id;
            _sql_command.Parameters.Add("@p2", SqlDbType.BigInt).Value = AccountType.ACCOUNT_VIRTUAL_CASHIER;
            _sql_command.Parameters.Add("@p3", SqlDbType.BigInt).Value = AccountType.ACCOUNT_VIRTUAL_TERMINAL;

            try
            {
              using (SqlDataReader _reader = _sql_command.ExecuteReader())
              {
                if (_reader.Read())
              {
                  _account_id = _reader.GetInt64(0);
                  _account_name = _reader.GetString(1);
                  _ps_started = _reader.GetDateTime(2);
                }
              }

              _description = WSI.Common.Cashier.EvOpGetDescription(_event_type, _device_code, _device_status,
                                                             _device_severity, _operation_code, _operation_data, _play_session_id, _technical_mode, 
                                                             _account_id, _account_name, _ps_started);

              if (_account_id > 0 && GeneralParam.GetBoolean("Intrusion", "BlockAccount"))
              {
                CardData _card_data;
                CashierSessionInfo _session_info;

                // Block the account
                _card_data = new CardData();
                CardData.DB_CardGetAllData(_account_id, _card_data);
                _card_data.Blocked = true;
              
                _card_data.BlockReason = AccountBlockReason.INTRUSION;
                _card_data.BlockDescription = "Intrusion";

                _session_info = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
                if (_session_info == null)
                {
                  _session_info = new CashierSessionInfo();
                  _session_info.AuthorizedByUserName = "";
                  _session_info.TerminalId = 0;
                  _session_info.TerminalName = "";
                  _session_info.CashierSessionId = 0;
                }

                // Blocks the account if the requirements are meet
                if (!CardData.DB_BlockUnblockCard(_card_data, _session_info, Trx))
                {
                  Log.Error("GenerateAlarm: Error blocking account AccountID: " + _account_id.ToString());
                }
              }
            }
            catch (Exception _ex)
            {
              Log.Error("GenerateAlarm: Error getting AccountID. PlaySessionId: " + _play_session_id.ToString());
              Log.Exception(_ex);
            }
          }
        }

        if ((_source_code == AlarmSourceCode.TerminalSASMachine) &&
             ((TechModeEnter)_technical_mode == TechModeEnter.WCP_TECH_MODE_PIN_PAD ||
              (TechModeEnter)_technical_mode == TechModeEnter.WCP_TECH_MODE_CARD_WITH_VALIDATION ||
              (TechModeEnter)_technical_mode == TechModeEnter.WCP_TECH_MODE_CARD_WITHOUT_VALIDATION))
        {
          // Technician Card Insert / Remove
          Alarm.Register(_source_code, _source_id, _source_name,
                         _alarm_code, _description, _severity, (DateTime)EventRow[COLUMN_DATETIME], (Int64)_technical_accound_id, Trx);

          switch (_operation_code)
          {
            case Cashier.EnumOperationCode.COMMON_OPERATION_CODE_TECH_OFFLINE_INSERTED:
              if (_technical_accound_id > 0)
              {

                WSI.Common.TerminalStatusFlags.DB_SetFlag(TerminalStatusFlags.BITMASK_TYPE.EGM, _source_id, (Int32)TerminalStatusFlags.EGM_FLAG.WITH_TECHNICIAN, Trx);
              }
              break;

            case Cashier.EnumOperationCode.COMMON_OPERATION_CODE_TECH_OFFLINE_REMOVED:

              WSI.Common.TerminalStatusFlags.DB_UnsetFlag(TerminalStatusFlags.BITMASK_TYPE.EGM, _source_id, (Int32)TerminalStatusFlags.EGM_FLAG.WITH_TECHNICIAN, Trx);
              break;
          }
        }
        else
        {
          Alarm.Register(_source_code, _source_id, _source_name,
                       _alarm_code, _description, _severity, (DateTime)EventRow[COLUMN_DATETIME], Trx);
      }
      }
    } // GenerateAlarm

    //------------------------------------------------------------------------------
    // PURPOSE :               
    //           Si operation = �EVENT� entonces
    //              Si Alarm (EVENT) entonces
    //                 Insertar Alarma
    //  PARAMS :
    //      - INPUT :
    //          - Event
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void GenerateAlarmOperationEvent(UInt32 EventData,
                                                  DateTime EventDateTime,
                                                  Terminal Terminal,
                                                  Int64 TerminalSessionId,
                                                  Int64 TechnicalAccountId,
                                                  SqlTransaction Trx)
    {
      AlarmSourceCode _source_code;
      Int32 _source_id;
      String _source_name;
      UInt32 _code;
      String _str_alarm;
      AlarmSeverity _severity;
      Boolean _issue_alarm;

      String _terminal_name;
      String _provider_id;

      _source_name = "";
      _source_id = 0;
      _source_code = AlarmSourceCode.NotSet;
      _code = EventData;
      _str_alarm = "";
      _severity = AlarmSeverity.Error;

      _issue_alarm = false;

      switch (EventData & 0xFFFF0000)
      {
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        case 0x00010000:  // SAS related events
          {
            UInt16 _sas_event_code;
            Boolean _set_status;

            _issue_alarm = true;
            _set_status = false;

            WSI.Common.BatchUpdate.TerminalSession.GetData(Terminal.terminal_id, TerminalSessionId, out _provider_id, out _terminal_name);
            _source_name = Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);
            _source_id = Terminal.terminal_id;
            _source_code = AlarmSourceCode.TerminalSASMachine;
            _code = EventData;

            _sas_event_code = (UInt16)(EventData & 0x0000FFFF);
            _str_alarm = Resource.String("STR_EA_0x" + EventData.ToString("X").PadLeft(8, '0'));

            switch (_sas_event_code)
            {

              case 0x0017:  // AC power was applied to gaming machine
                _severity = AlarmSeverity.Info;

                if (!PeruEvents.InsertTechnicalEvent(Terminal.terminal_id, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_RESTORING_ON_POWER_UP, EventDateTime, Trx))
                {
                  Log.Error("InsertTechnicalEvent: return false. ");
                }
                if (!ColJuegos.InsertColJuegosEvent(ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_RESTORING_ON_POWER_UP, Terminal.terminal_id))
                {
                  Log.Error("InsertColJuegosEvent: return false. ");
                }

                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }

                break;
              case 0x0018: _severity = AlarmSeverity.Info; break;            // AC power was lost from gaming machine

              case 0x0011: // Slot door was opened
              case 0x0013: // Drop door was opened
              case 0x0015: // Card cage was opened
              case 0x0019: // Cashbox door was opened
              case 0x001D: // Belly door was opened
                _severity = AlarmSeverity.Warning;

                if (!PeruEvents.InsertTechnicalEvent(Terminal.terminal_id, true, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_CASHBOX_DOOR_OPENED, EventDateTime, Trx))
                {
                  Log.Error("InsertTechnicalEvent: return false. ");
                }

                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }

                _set_status = true;
                break;

              case 0x0012: // Slot door was closed
              case 0x0014: // Drop door was closed
              case 0x0016: // Card cage was closed
              case 0x001A: // Cashbox door was closed
              case 0x001E: // Belly door was closed
                _severity = AlarmSeverity.Info;

                if (!PeruEvents.InsertTechnicalEvent(Terminal.terminal_id, false, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_CASHBOX_DOOR_OPENED, EventDateTime, Trx))
                {
                  Log.Error("InsertTechnicalEvent: return false. ");
                }
                _set_status = true;

                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }

                break;

              case 0x001B: _severity = AlarmSeverity.Warning; break;         // Cashbox was removed
              case 0x001C: _severity = AlarmSeverity.Info; break;            // Cashobox was installed
              case 0x0020: _severity = AlarmSeverity.Error; break;           // General tilt

              case 0x0031:   // CMOS RAM error (data recovered from EEPROM)
              case 0x0032:   // CMOS RAM error (no data recovered from EEPROM)
              case 0x0033:   // CMOS RAM error (bad device)
              case 0x0034:   // EEPROM error (data error)
              case 0x0035:   // EEPROM error (bad device)
              case 0x0036:   // EPROM error (different checksum - version changed)
              case 0x0037:   // EPROM error (bad checksum compare)
              case 0x0038:   // Partitioned EPROM error (checksum - versoin changed)
              case 0x0039:   // Partitioned EPROM error (bad checksum compare)
                _severity = AlarmSeverity.Error;

                if (!PeruEvents.InsertTechnicalEvent(Terminal.terminal_id, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_MEMORY_FAIL, EventDateTime, Trx))
                {
                  Log.Error("InsertTechnicalEvent: return false. ");
                }

                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }
                _set_status = true;

                break;

              case 0x003A: _severity = AlarmSeverity.Info; break;            // Memory error reset (operator used self test switch)

              case 0x003B: // Low backup battery detected
                _severity = AlarmSeverity.Warning;

                if (!PeruEvents.InsertTechnicalEvent(Terminal.terminal_id, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_LOW_BATTERY, EventDateTime, Trx))
                {
                  Log.Error("InsertTechnicalEvent: return false. ");
                }

                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }
                _set_status = true;
                break;

              case 0x003C:
                _severity = AlarmSeverity.Warning;          // Operator changed options

                if (!ColJuegos.InsertColJuegosEvent(ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHINCAL_EVENTS_MACHINE_GAME_CHANGE, Terminal.terminal_id, Trx))
                {
                  Log.Error("InsertColJuegosEvent: ENUM_COLJUEGOS_TECHINCAL_EVENTS_MACHINE_GAME_CHANGE return false");
                }

                break;

              case 0x0051: _severity = AlarmSeverity.Info; break;            // Handpay is pending
              case 0x0052: _severity = AlarmSeverity.Info; break;            // Handpay was reset
              case 0x0054: _severity = AlarmSeverity.Info; break;            // Progressive win (cashout device/credit paid)
              case 0x0055: _severity = AlarmSeverity.Info; break;            // Player has cancelled the handpay request
              case 0x0056: _severity = AlarmSeverity.Info; break;            // SAS Progressive level hit
              case 0x0070: _severity = AlarmSeverity.Warning; break;         // Exception buffer overflow

              case 0x0071:                                                   // Change Lamp On  - Call Attendant
                _severity = AlarmSeverity.Warning;
                _set_status = true;
                break;

              case 0x0072:                                                   // Change Lamp Off - Call Attendant
                _severity = AlarmSeverity.Info;
                _set_status = true;
                break;

              case 0x007A:   // Gaming machine soft meters reset to zero
                _severity = AlarmSeverity.Error;

                // AJQ, 22-AUG-2013, Report "Meter Reset" event only when the meter are zero, ignore the SAS-Exception
                ////////////if (!PeruEvents.InsertTechnicalEvent(Terminal.terminal_id, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_RESET_MEMORY, EventDateTime, Trx))
                ////////////{
                ////////////  Log.Error("InsertTechnicalEvent: return false. ");
                ////////////}
                break;

              case 0x0082: _severity = AlarmSeverity.Warning; break;         // Display meters or attendant menu has been entered
              case 0x0083: _severity = AlarmSeverity.Info; break;            // Display meters or attendant menu has been exited
              case 0x0084: _severity = AlarmSeverity.Warning; break;         // Self test or operator menu has been entered
              case 0x0085: _severity = AlarmSeverity.Info; break;            // Self test or operator menu has been exited
              case 0x0086: _severity = AlarmSeverity.Error; break;           // Gaming machine is out of service (by attendant)
              case 0x008A: _severity = AlarmSeverity.Warning; break;         // Game recall entry has been displayed
              case 0x0098: _severity = AlarmSeverity.Warning; break;         // Power off card cage access
              case 0x0099: _severity = AlarmSeverity.Warning; break;         // Power off slot door access
              case 0x009A: _severity = AlarmSeverity.Warning; break;         // Power off cashbox door access
              case 0x009B: _severity = AlarmSeverity.Warning; break;         // Power off drop door access

              // SAS TITO related events
              case 0x0028:                                                   // Bill jam
              case 0x0029:                                                   // Bill acceptor hardware failure
              case 0x0060:                                                   // Printer communication error
              case 0x0061:                                                   // Printer paper out error
              case 0x0075:                                                   // Printer power off
              case 0x0078:                                                   // Printer carriage jammed
              case 0x0027:                                                   // Stacker full
                _severity = AlarmSeverity.Error;
                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }
                _set_status = true;
                break;

              case 0x002A:                                                   // Reverse bill detected
              case 0x002B:                                                   // Bill rejected
              case 0x002C:                                                   // Counterfeit bill detected
                _severity = AlarmSeverity.Info;
                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }
                break;

              case 0x0021:                                                   // Coin in tilt
              case 0x002D:                                                   // Reverse coin in detected
              case 0x0079:                                                   // Coin in lockout malfunction
                _severity = AlarmSeverity.Error;
                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }
                _set_status = true;
                break;

              case 0x0076:                                                   // Printer power on
                _severity = AlarmSeverity.Info;
                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }
                _set_status = true;
                break;

              case 0x0074:                                                   // Printer paper low
              case 0x002E:                                                   // Stacker near full
                _severity = AlarmSeverity.Warning;
                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }
                _set_status = true;
                break;

              case 0x0077:                                                   // Replace printer ribbon
                _severity = AlarmSeverity.Warning;
                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }
                _set_status = true;
                break;

              case 0x0101: // SAS disconnected
                _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost;

                if (!PeruEvents.InsertTechnicalEvent(Terminal.terminal_id, true, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_DISCONNECTED, EventDateTime, Trx))
                {
                  Log.Error("InsertTechnicalEvent: return false. ");
                }
                if (!ColJuegos.InsertColJuegosEvent(ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_DISCONNECTED, Terminal.terminal_id))
                {
                  Log.Error("InsertColJuegosEvent: return false. ");
                }

                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }

                _set_status = true;
                break;

              case 0x0102: // SAS connected
                _severity = AlarmSeverity.Info; _source_code = AlarmSourceCode.TerminalSASHost;

                if (!PeruEvents.InsertTechnicalEvent(Terminal.terminal_id, false, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_DISCONNECTED, EventDateTime, Trx))
                {
                  Log.Error("InsertTechnicalEvent: return false. ");
                }
                if (!ColJuegos.InsertColJuegosEvent(ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_DISCONNECTED, Terminal.terminal_id))
                {
                  Log.Error("InsertColJuegosEvent: return false. ");
                }

                if (!UpdateEventMeters(Terminal.terminal_id, EventData, EventDateTime, Trx))
                {
                  Log.Error("UpdateEventMeters: return false. ");
                }

                if (Misc.SystemMode() == SYSTEM_MODE.WASS)
                {
                  if (!SoftwareValidations.InsertSoftwareValidation(Terminal.terminal_id, Trx))
                  {
                    Log.Error("InsertSoftwareValidation: return false. ");
                  }
                }

                _set_status = true;
                break;

              // Meter Errors
              case 0x0110: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Could not get any of the counters 
              case 0x0111: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Error getting counters 
              case 0x0112: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Error getting Hand Pays counter (HP-Meter)
              case 0x0113: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Error getting counters game (Coin-in/Coin-out) 
              case 0x0114: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Error getting counters extended 
              case 0x0115: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Error getting extended counter of Payments Manual (HP-Meter) 
              case 0x0116: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Error getting counters extended play (Coin-in/Coin-out)
              // Recurrent Meter Errors
              case 0x0121: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Recurrent error getting counters  
              case 0x0122: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Recurrent error getting Hand Pays counter (HP-Meter)
              case 0x0123: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Recurrent error getting counters game (Coin-in/Coin-out)
              case 0x0124: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Recurrent error getting counters extended 
              case 0x0125: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Recurrent error getting extended counter of Payments Manual (HP-Meter)
              case 0x0126: _severity = AlarmSeverity.Error; _source_code = AlarmSourceCode.TerminalSASHost; break;  //Recurrent error getting counters extended play (Coin-in/Coin-out)

              default:
                {
                  if (_sas_event_code >= 0x0000 && _sas_event_code <= 0x00FF)
                  {
                    // SAS Exception
                    _source_code = AlarmSourceCode.TerminalSASMachine;
                    _severity = AlarmSeverity.Info;
                    _str_alarm = Resource.String("STR_EA_SAS_MACHINE_ANY", "0x" + _sas_event_code.ToString("X").PadLeft(2, '0'));
                  }
                  else
                  {
                    _source_code = AlarmSourceCode.TerminalSASHost;
                    _severity = AlarmSeverity.Info;
                    _str_alarm = Resource.String("STR_EA_SAS_HOST_ANY", "0x" + _sas_event_code.ToString("X").PadLeft(4, '0'));
                  }
                }
                break;

            } // switch (EventData & 0x0000FFFF) 

            // Set Terminal Status if it's needed
            if (_set_status)
            {
              TerminalStatusFlags.SetTerminalStatus(Terminal.terminal_id, _sas_event_code, Trx);
            }

          } // case 0x00010000 // SAS related events
          break;

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        case 0x00020000:  // SAS Meters Back
          {
            UInt16 _sas_meter_event_code;
            UInt32 _string_code;

            _issue_alarm = true;

            WSI.Common.BatchUpdate.TerminalSession.GetData(Terminal.terminal_id, TerminalSessionId, out _provider_id, out _terminal_name);
            _source_name = Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);
            _source_id = Terminal.terminal_id;
            _source_code = AlarmSourceCode.TerminalSASHost;
            _severity = AlarmSeverity.Warning;
            _code = EventData;

            _sas_meter_event_code = (UInt16)((EventData & 0x0000FF00) >> 8);
            _string_code = (UInt32)(EventData & 0xFFFFFF00);

            // Alarm text depends on the source
            //    - Game                    0x000210**    STR_EA_0x00021000
            //    - Machine                 0x00022000    STR_EA_0x00022000
            //    - Play Session            0x00023000    STR_EA_0x00023000
            //    - Fund Transfer           0x00024000    STR_EA_0x00024000
            //    - Handpay cancellation    0x00025***    STR_EA_0x00025000

            switch (_sas_meter_event_code)
            {
              case 0x10:   // Game
                {
                  UInt16 _game_id;

                  _game_id = (UInt16)(EventData & 0x000000FF);
                  _str_alarm = Resource.String("STR_EA_0x" + _string_code.ToString("X").PadLeft(8, '0'),
                                                "SAS - " + _game_id.ToString("00"));
                }
                break;

              case 0x20:   // Machine        
              case 0x30:   // Play Session   
              case 0x40:   // Fund Transfer     
                _str_alarm = Resource.String("STR_EA_0x" + _string_code.ToString("X").PadLeft(8, '0'));
                break;

              default:
                {
                  UInt16 _aux_event_code;

                  // 0x00025***
                  _aux_event_code = (UInt16)((EventData & 0x0000F000) >> 8);
                  _string_code = (UInt32)(EventData & 0xFFFFF000);

                  if (_aux_event_code == 0x50)     // Handpay cancellation
                  {
                    double _minutes;
                    String _aux_text;
                    TimeSpan _ts;
                    int _split_minutes;
                    int _split_hours;

                    _minutes = (double)(EventData & 0x00000FFF);
                    _ts = TimeSpan.FromMinutes(_minutes);

                    // Split time into hours-minutes
                    _split_minutes = _ts.Minutes;

                    _ts = TimeSpan.FromMinutes(_minutes - _split_minutes);
                    _split_hours = (int)_ts.TotalHours;

                    _aux_text = _split_hours.ToString("00") + "h" + _split_minutes.ToString("00") + "m";
                    _str_alarm = Resource.String("STR_EA_0x" + _string_code.ToString("X").PadLeft(8, '0'), _aux_text);
                  }
                  else
                  {
                    UInt16 _aux_code;

                    _aux_code = (UInt16)(EventData & 0x0000FFFF);
                    _str_alarm = Resource.String("STR_EA_SAS_METER_BACK_ANY", "0x" + _aux_code.ToString("X").PadLeft(4, '0'));
                  }
                }
                break;
            } // switch (EventData & 0x0000FFFF) 
          }
          break;    // 0x00020000 - SAS Meters Back

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        case 0x00030000:  // SAS Meters Jump
          {
            UInt16 _sas_meter_event_code;
            UInt32 _string_code;

            _issue_alarm = true;

            WSI.Common.BatchUpdate.TerminalSession.GetData(Terminal.terminal_id, TerminalSessionId, out _provider_id, out _terminal_name);
            _source_name = Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);
            _source_id = Terminal.terminal_id;
            _source_code = AlarmSourceCode.TerminalSASHost;
            _severity = AlarmSeverity.Warning;
            _code = EventData;

            _sas_meter_event_code = (UInt16)((EventData & 0x0000FF00) >> 8);
            _string_code = (UInt32)(EventData & 0xFFFFFF00);

            // Alarm text depends on the source
            //    - Game                    0x000310**    STR_EA_0x00031000
            //    - Machine                 0x00032000    STR_EA_0x00032000
            //    - Play Session            0x00033000    STR_EA_0x00033000
            //    - Fund Transfer           0x00034000    STR_EA_0x00034000

            switch (_sas_meter_event_code)
            {
              case 0x10:   // Game
                {
                  UInt16 _game_id;

                  _game_id = (UInt16)(EventData & 0x000000FF);
                  _str_alarm = Resource.String("STR_EA_0x" + _string_code.ToString("X").PadLeft(8, '0'),
                                                "SAS - " + _game_id.ToString("00"));
                }
                break;

              case 0x20:   // Machine        
              case 0x30:   // Play Session   
              case 0x40:   // Fund Transfer     
                _str_alarm = Resource.String("STR_EA_0x" + _string_code.ToString("X").PadLeft(8, '0'));
                break;

              default:
                {
                  UInt16 _aux_code;

                  _aux_code = (UInt16)(EventData & 0x0000FFFF);
                  _str_alarm = Resource.String("STR_EA_SAS_METER_JUMP_ANY", "0x" + _aux_code.ToString("X").PadLeft(4, '0'));
                }
                break;
            } // switch (EventData & 0x0000FFFF) 
          }
          break;    // 0x00030000 - SAS Meters Jump

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        case 0x00040000:  // SAS CRC ERROR
          {
            UInt32 _string_code;

            _issue_alarm = true;

            WSI.Common.BatchUpdate.TerminalSession.GetData(Terminal.terminal_id, TerminalSessionId, out _provider_id, out _terminal_name);
            _source_name = Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);
            _source_id = Terminal.terminal_id;
            _source_code = AlarmSourceCode.TerminalSASHost;
            _severity = AlarmSeverity.Warning;
            _code = EventData;

            _string_code = (UInt32)(EventData & 0xFFFFFF00);
            _str_alarm = Resource.String("STR_EA_0x" + _string_code.ToString("X").PadLeft(8, '0'));

          }
          break; // 0x00040000:  // SAS CRC ERROR

        default:
          break;
      }

      if (_issue_alarm)
      {
        Alarm.Register(_source_code, _source_id, _source_name, _code, _str_alarm, _severity, EventDateTime, TechnicalAccountId, Trx);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Set terminal funds transfer status
    //
    //  PARAMS :
    //      - INPUT :
    //          - EventRow
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void TerminalFundsTransferStatus(DataRow Row, SqlTransaction Trx)
    {
      Int32 _transfer_status;
      Terminal _terminal;

      if ((Int32)Row[COLUMN_EVENT_TYPE] != DB_EVENT_HISTORY_OPERATION)
      {
        return;
      }

      if ((OperationCodes)Row[COLUMN_OPERATION_CODE] != OperationCodes.WCP_OPERATION_CODE_TERMINAL_TRANSFER_STATUS)
      {
        return;
      }

      _terminal = (Terminal)Row[COLUMN_TERMINAL_OBJECT];
      _transfer_status = Convert.ToInt32(Row[COLUMN_OPERATION_DATA]);

      if (_transfer_status > 0)
      {
        // Error
        WSI.Common.TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(_terminal.terminal_id, WSI.Common.TerminalFundsTransfer.TRANSFER_STATUS.CANCELED_BY_TERMINAL, Trx);
      }
      else
      {
        // Transfer OK
        WSI.Common.TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(_terminal.terminal_id, WSI.Common.TerminalFundsTransfer.TRANSFER_STATUS.TRANSFERRED, Trx);
      }
    }

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows with errors from DataTable and rows with same Terminal-SequenceId than the deleted rows.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void DeleteRowsWithErrors()
    {
      DataRow[] _errors;
      DataRow[] _events_same_request;
      DataRow[] _rows;
      String _filter;
      Int32 _previous_terminal_id;
      Int64 _previous_sequence_id;
      Int32 _terminal_id;
      Int64 _sequence_id;
      Int32 _num_rows_deleted;

      _previous_terminal_id = -1;
      _previous_sequence_id = -1;
      _num_rows_deleted = 0;

      Log.Warning("------------- BEGIN EVENTS NOT INSERTED (ReportEventList) -------------");

      _errors = m_dt_events.GetErrors();
      foreach (DataRow _row_error in _errors)
      {
        _terminal_id = (Int32)_row_error[COLUMN_TERMINAL_ID];
        _sequence_id = (Int64)_row_error[COLUMN_SEQUENCE_ID];

        if (_terminal_id != _previous_terminal_id || _sequence_id != _previous_sequence_id)
        {
          _previous_terminal_id = _terminal_id;
          _previous_sequence_id = _sequence_id;

          _filter = "TERMINAL_ID = " + _terminal_id.ToString() + " AND SEQUENCE_ID = " + _sequence_id.ToString();

          _events_same_request = m_dt_events.Select(_filter);
          foreach (DataRow _row_event in _events_same_request)
          {
            Log.Warning("Terminal: " + _row_event[COLUMN_TERMINAL_ID].ToString() +
                        ", Session: " + _row_event[COLUMN_SESSION_ID].ToString() +
                        ", Sequence: " + _row_event[COLUMN_SEQUENCE_ID].ToString() +
                        ", EventType: " + ((Int32)_row_event[COLUMN_EVENT_TYPE]).ToString() +
                        ". Details: " + _row_event.RowError);

            // Use better _row_event.Delete() than m_dt_events.Rows.Remove(_row_event).
            // _row_event.Delete() only marks row for deletion.
            // m_dt_events.Rows.Remove(_row) really removes the row and can produce error in foreach.
            _row_event.Delete();
            _num_rows_deleted++;
          }
        }
      }

      if (_num_rows_deleted == 0)
      {
        Log.Error("!!!!! Error found but not in data rows. Deleted all cache !!!!!");
        m_dt_events.Clear();
      }

      Log.Warning("------------- END   EVENTS NOT INSERTED (ReportEventList) -------------");

      // AcceptChanges delete the rows, and mark Unchanged the remaining Added rows.
      m_dt_events.AcceptChanges();

      // After delete, mark again the remaining rows as Added.
      _rows = m_dt_events.Select();
      foreach (DataRow _row in _rows)
      {
        _row.SetAdded();
      }

    } // DeleteRowsWithErrors

    //------------------------------------------------------------------------------
    // PURPOSE : Send Alesis Event.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataRow Row
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void SendAlesisEvent(DataRow Row, SqlTransaction Trx)
    {
      Terminal _terminal;
      AleTerminal _ale_terminal;
      Int32 _event_id;
      Decimal _prize_amount;
      Boolean _call_to_alesis;
      Alesis.StatusCode _status_code;

      _terminal = (Terminal)Row[COLUMN_TERMINAL_OBJECT];

      if (_terminal.play_session_type == PlaySessionType.ALESIS
          && (Int32)Row[COLUMN_EVENT_TYPE] == DB_EVENT_HISTORY_OPERATION)
      {
        _ale_terminal = (AleTerminal)_terminal;

        _call_to_alesis = false;
        _event_id = 0;
        _prize_amount = 0;

        switch ((OperationCodes)Row[COLUMN_OPERATION_CODE])
        {
          case OperationCodes.WCP_OPERATION_CODE_JACKPOT_WON:
            _call_to_alesis = true;
            _event_id = 1;
            _prize_amount = (Decimal)Row[COLUMN_OPERATION_DATA];
            break;

          case OperationCodes.WCP_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE:
            _call_to_alesis = true;
            _event_id = 2;
            _prize_amount = (Decimal)Row[COLUMN_OPERATION_DATA];
            break;

          case OperationCodes.WCP_OPERATION_CODE_CALL_ATTENDANT:
            _call_to_alesis = true;
            _event_id = 3;
            break;

          default:
            break;
        }

        if (_terminal.play_session_id == 0)
        {
          _call_to_alesis = false;
        }

        if (_call_to_alesis)
        {
          // Call to alesis
          _status_code = Alesis.ReportEvent((AleTerminal)_terminal, _event_id, _prize_amount, Trx);

          switch (_status_code)
          {
            case Alesis.StatusCode.Success:
              // Nothing
              break;

            case Alesis.StatusCode.AccountNumberNotValid:
            case Alesis.StatusCode.MachineNumberNotValid:
            case Alesis.StatusCode.SessionNumberNotValid:
            case Alesis.StatusCode.AccessDenied:
              Log.Error("ReportEvent SessionID=" + _ale_terminal.ale_session_id + " Error:" + _status_code.ToString());
              break;

            case Alesis.StatusCode.NotConnectedToDatabase:
              Log.Error("ReportEvent SessionID=" + _ale_terminal.ale_session_id + " Error:" + _status_code.ToString());
              break;

            default:
              Log.Error("ReportEvent SessionID=" + _ale_terminal.ale_session_id + " Error:" + _status_code.ToString());
              break;

          } // switch

        } // if (call_to_alesis)
      } // if (SessionType.ALESIS)

    }// SendAlesisEvent

    //------------------------------------------------------------------------------
    // PURPOSE : Save Handpay.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataRow Row
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void SaveHandpay(DataRow Row, SqlTransaction Trx)
    {
      Terminal _terminal;
      SqlCommand _sql_command;
      SqlDataReader _reader;
      String _sql_query;
      String _terminal_name;
      String _provider_id;
      Int32 _num_rows_inserted;
      DateTime _event_datetime;
      Int32 _count;
      Handpays.HandpayRegisterOrCancelParameters _input_handpay;
      Int64 _account_id;
      SMIB_COMMUNICATION_TYPE _smib_comm_type;
      DateTime _date_last_ps_finished;
      Object _obj;

      Decimal _amount;
      String _egm_iso;
      Decimal _egm_amt;
      Decimal _sys_amt;


      _terminal = (Terminal)Row[COLUMN_TERMINAL_OBJECT];

      if ((Int32)Row[COLUMN_EVENT_TYPE] != DB_EVENT_HISTORY_OPERATION)
      {
        return;
      }

      if ((OperationCodes)Row[COLUMN_OPERATION_CODE] != OperationCodes.WCP_OPERATION_CODE_SECURITY_CASHOUT_CENTS
          && (OperationCodes)Row[COLUMN_OPERATION_CODE] != OperationCodes.WCP_OPERATION_CODE_SECURITY_CASHOUT_UNITS)
      {
        return;
      }

      //
      // Insert Handpay
      //

      _sql_query = "SELECT TE_NAME " +
                   "     , TE_PROVIDER_ID " +
                   "FROM   TERMINALS " +
                   "WHERE  TE_TERMINAL_ID    = @TerminalId ";

      _sql_command = new SqlCommand(_sql_query);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@TerminalId", SqlDbType.Int).Value = _terminal.terminal_id;

      _reader = null;
      _terminal_name = null;
      _provider_id = null;

      try
      {
        _reader = _sql_command.ExecuteReader();
        if (_reader.Read())
        {
          _terminal_name = _reader[0].ToString();
          _provider_id = _reader[1].ToString();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("SaveHandpay: Error reading Handpay information " + _terminal.terminal_id.ToString());

        return;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
          _reader.Dispose();
          _reader = null;
        }
      }

      _event_datetime = ((DateTime)Row[COLUMN_DATETIME]);

      _sql_query = "SELECT COUNT(*) " +
                   "FROM   HANDPAYS " +
                   "WHERE  HP_TERMINAL_ID  = @TerminalId " +
                   "  AND  HP_DATETIME     = @DateTime " +
                   "  AND  HP_AMOUNT       = @Amount ";

      _sql_command = new SqlCommand(_sql_query);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@TerminalId", SqlDbType.Int).Value = _terminal.terminal_id;
      _sql_command.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = _event_datetime;

      if ((OperationCodes)Row[COLUMN_OPERATION_CODE] == OperationCodes.WCP_OPERATION_CODE_SECURITY_CASHOUT_UNITS)
      {
        _sql_command.Parameters.Add("@Amount", SqlDbType.Money).Value = ((Decimal)Row[COLUMN_OPERATION_DATA]);
      }
      else
      {
        _sql_command.Parameters.Add("@Amount", SqlDbType.Money).Value = ((Decimal)Row[COLUMN_OPERATION_DATA]) / 100;
      }

      try
      {
        _count = (int)_sql_command.ExecuteScalar();

        if (_count > 0)
        {
          // Already inserted !
          return;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      //
      // Insert handpay
      //
      if ((OperationCodes)Row[COLUMN_OPERATION_CODE] == OperationCodes.WCP_OPERATION_CODE_SECURITY_CASHOUT_UNITS)
      {
       _amount = ((Decimal)Row[COLUMN_OPERATION_DATA]);
      }
      else
      {
        _amount = ((Decimal)Row[COLUMN_OPERATION_DATA]) / 100;
      }


      WSI.Common.TITO.HandPay.GetFloorDualCurrency(_amount, _terminal.terminal_id, out _sys_amt, out _egm_amt, out _egm_iso, Trx);

      _sql_query = "INSERT INTO HANDPAYS (HP_TERMINAL_ID " +
                                      " , HP_DATETIME " +
                                      " , HP_PREVIOUS_METERS " +
                                      " , HP_AMOUNT " +
                                      " , HP_TE_NAME " +
                                      " , HP_TE_PROVIDER_ID " +
                                      " , HP_TYPE " +
                                      " , HP_AMT0  " +
                                      " , HP_CUR0 ) " +
                                " VALUES (@TerminalId " +
                                      " , @DateTime " +
                                      " , NULL " +
                                      " , @Amount " +
                                      " , @TermName " +
                                      " , @ProviderId " +
                                      " , @HandpayType " +
                                      " , @pAmount0 " +
                                      " , @pCurren0 ) ";

      _sql_command = new SqlCommand(_sql_query);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@TerminalId", SqlDbType.Int).Value = _terminal.terminal_id;
      _sql_command.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = _event_datetime;

      _sql_command.Parameters.Add("@Amount", SqlDbType.Money).Value = _sys_amt;

      _sql_command.Parameters.Add("@TermName", SqlDbType.NVarChar).Value = _terminal_name;
      _sql_command.Parameters.Add("@ProviderId", SqlDbType.NVarChar).Value = _provider_id;
      _sql_command.Parameters.Add("@HandpayType", SqlDbType.Int).Value = ((int)HANDPAY_TYPE.ORPHAN_CREDITS);

      _sql_command.Parameters.Add("@pAmount0", SqlDbType.Money).Value = (Decimal)_egm_amt;
      _sql_command.Parameters.Add("@pCurren0", SqlDbType.NVarChar, 3).Value = _egm_iso;

      try
      {
        _num_rows_inserted = _sql_command.ExecuteNonQuery();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("SaveHandpay: Inserting HANDPAY [Exception]: TerminalId: " + _terminal.terminal_id.ToString());

        return;
      }

      if (_num_rows_inserted != 1)
      {
        Log.Warning("SaveHandpay: Inserting HANDPAY [Not updated]: TerminalId: " + _terminal.terminal_id.ToString());

        return;
      }

      // XCD 19-JUN-2015 Automatic Handpays payment
      if (Common.GeneralParam.GetBoolean("OrphanCredits", "AutomaticPayment", true) == true)
      {
        try
        {
          _smib_comm_type = SMIB_COMMUNICATION_TYPE.NONE;
          _account_id = 0;
          _date_last_ps_finished = DateTime.MinValue;

          _sql_query = " SELECT TE_SMIB2EGM_COMM_TYPE FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId ";

          _sql_command = new SqlCommand(_sql_query);
          _sql_command.Connection = Trx.Connection;
          _sql_command.Transaction = Trx;

          _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal.terminal_id;

          _obj = _sql_command.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            _smib_comm_type = (SMIB_COMMUNICATION_TYPE)Convert.ToInt32(_obj);
          }

          if (_smib_comm_type != SMIB_COMMUNICATION_TYPE.PULSES)
          {
            return;
          }

          _sql_query = " SELECT  TOP 1 PS_ACCOUNT_ID " +
                        "       , PS_FINISHED " +
                        "  FROM   PLAY_SESSIONS WITH (INDEX([IX_PS_FINISHED])) " +
                        " WHERE   PS_TERMINAL_ID = @pTerminalId " +
                        "   AND   PS_FINISHED > DATEADD (MINUTE, -60, GETDATE()) " +
                        "   AND   PS_STATUS NOT IN (@pSesionStatus1, @pSesionStatus2)  " +
                        " ORDER   BY PS_FINISHED DESC ";

          _sql_command = new SqlCommand(_sql_query);
          _sql_command.Connection = Trx.Connection;
          _sql_command.Transaction = Trx;

          _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal.terminal_id;
          _sql_command.Parameters.Add("@pSesionStatus1", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _sql_command.Parameters.Add("@pSesionStatus2", SqlDbType.Int).Value = PlaySessionStatus.HandpayPayment;          

          using (SqlDataReader _sql_reader = _sql_command.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return;
            }

            _account_id = _sql_reader.GetInt64(0);
            _date_last_ps_finished = _sql_reader.GetDateTime(1);
          }

          _sql_query = " SELECT   COUNT (1) " +
                       "   FROM   ACCOUNT_MOVEMENTS " +
                       "  WHERE   AM_ACCOUNT_ID = @pAccountId " +
                       "    AND   AM_TYPE IN (@pMov1, @pMov2) " +
                       "	  AND   AM_DATETIME >= @pDatePsFinished ";

          _sql_command = new SqlCommand(_sql_query);
          _sql_command.Connection = Trx.Connection;
          _sql_command.Transaction = Trx;

          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _account_id;
          _sql_command.Parameters.Add("@pDatePsFinished", SqlDbType.DateTime).Value = _date_last_ps_finished;
          _sql_command.Parameters.Add("@pMov1", SqlDbType.Int).Value = MovementType.Handpay;
          _sql_command.Parameters.Add("@pMov2", SqlDbType.Int).Value = MovementType.ManualHandpay;

          _obj = _sql_command.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            if ((Int32)_obj > 0)
            {
              // There are paid handpays after playsession finished
              return;
            }
          }

          // Prepare to PAY
          _input_handpay = new Handpays.HandpayRegisterOrCancelParameters();
          _input_handpay.TerminalId = _terminal.terminal_id;
          _input_handpay.TerminalProvider = _provider_id;
          _input_handpay.TerminalName = _terminal_name;
          _input_handpay.HandpayType = HANDPAY_TYPE.ORPHAN_CREDITS;

          if ((OperationCodes)Row[COLUMN_OPERATION_CODE] == OperationCodes.WCP_OPERATION_CODE_SECURITY_CASHOUT_UNITS)
          {
            _input_handpay.HandpayAmount = ((Decimal)Row[COLUMN_OPERATION_DATA]);
          }
          else
          {
            _input_handpay.HandpayAmount = ((Decimal)Row[COLUMN_OPERATION_DATA]) / 100;
          }

          _input_handpay.HandpayDate = _event_datetime;
          _input_handpay.CancelHandpay = false;
          _input_handpay.SiteJackpotAwardedOnTerminalId = _terminal.terminal_id;

          Trx.Save("AUTOPAY_ORPHAN");

          if (!Common.Handpays.AutomaticallyPayHandPay(_account_id, _input_handpay, Trx))
          {
            Trx.Rollback("AUTOPAY_ORPHAN");

            Log.Error("Unable to AutomaticallyPayHandPay. AccountId: " + _account_id.ToString());
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

    } // SaveHandpay

    #endregion // Private Methods

  } // WCP_BU_ReportEventList

} // WSI.WCP
