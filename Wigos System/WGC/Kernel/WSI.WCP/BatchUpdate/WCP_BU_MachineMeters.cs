//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_BU_MachineMeters.cs
// 
//   DESCRIPTION: WCP_BU_MachineMeters class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 18-NOV-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-NOV-2011 ACC    First release.
// 15-JUN-2012 ACC    Add Jackpot Cents into Machine Meters
// 02-JUL-2012 ACC    Added Peru External Event.
// 24-JUL-2012 ACC    Extended Meters: Add maximum meter values. Delta meters is zero if any maximum changed.
// 05-SEP-2013 RCI & AJQ    Even the meters are equals, generate events for Peru
// 27-JAN-2104 DRV    Added New Meters: Played(Count and amount), Won(Count and amount) and Jackpot. 
// 12-FEB-2014 DRV    Fixed Bug: WIG-589 - Errors in the log of the WCP while trying to update the meters
// 22-APR-2014 DRV    Added New Meters: AFT to GM (cents and quantity), AFT from GM (cents and quantity)
// 28-APR-2014 MPO    WIG-861: Added ISNULL in tsmh (first_time)
// 07-AUG-2014 ACC    Add Progressive Jackpot Cents into Meters
// 09-OCT-2014 ACC    WIG-1434: Hist�rico de contadores: el contador de ganado est� incluyendo los jackpots
// 09-OCT-2014 ACC    WIG-1828: Hist�rico de contadores: las transferencias de entrada/salida se muestran valor 0 para los terminales EFT
// 16-JAN-2015 ANM    Added informative alarm when First Time in SAS Host
// 28-JAN-2015 SGB    Fixed Bug WIG-1951: Added informative alarm when is Rollover
// 16-JUN-2016 FGB    When there is a Service RAM Clear, Rollover or SAS Denom Change it creates group records in TSMH with the values of the meters 
// 20-JUN-2016 FGB    Now the creation of the SAS Denom Accounting change record it is done here, instead of WCP_BU_MachineMeters
// 06-SEP-2016 JMM    Bug 17162:WCP: Se resetea el valor del contador de jackpot progresivo si el SAS Host no puede leerlo de la m�quina EGM
// 21-OCT-2016 JPJ    Bug 19537: High numbers generate an exception when saving into the table
// 19-APR-2017 FGB    WIGOS-1387: Machine Meters SAS Accounting Denom change returns true when new value is NULL
// 25-JAN-2018 FGB    WIGOS-7569: AGG SITE-Terminal analyse's screen: error identifiying the the anomaly "change of denomination"
// 26-APR-2018 XGJ    WIGOS-10478 AGG-Lotery: Machine is disconnected don't generate periods
// 26-APR-2018 FJC    WIGOS-10409 AGG - SITE - MULTISITE: The denomination change is not applied to all meters (Total drop, etc).
//                    WIGOS-10405 AGG - SITE -MULTISITE: Rollover event generated with accounting denomination "0,00"
// 07-MAY-2018 FJC    WIGOS-10718 AGG - SITE - MULTISITE: Hourly registers are missing between terminal disconnection and the connection it.
// 06-JUN-2018 JML    Fixed Bug 32902:WIGOS-12599 AFIP Client: Error sending meters with rollover when the accounting denomination is different to 0,01.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.WCP.MeterTSMHGroup;

namespace WSI.WCP
{
  public static class WCP_BU_MachineMeters
  {
    public static Int32[] MetersReportedByMM = new Int32[] { 0x0000, 0x0001, 0x0002, 0x0005, 0x0006, 0x00A0, 0x00A1, 0x00B8, 0x00B9, 0x1000 };

    #region Constants
    //Meter code
    public const Int32 METER_CODE_GAMES_PLAYED = 5;
    public const Int32 METER_CODE_GAMES_WON = 6;
    public const Int32 METER_CODE_TOTAL_COIN_IN_CREDITS = 0;
    public const Int32 METER_CODE_TOTAL_COIN_OUT_CREDITS = 1;
    public const Int32 METER_CODE_TOTAL_JACKPOT_CREDITS = 2;
    public const Int32 METER_CODE_TOTAL_PROGRESSIVE_JACKPOT_CREDITS = 4096;
    public const Int32 METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_CENTS = 160;
    public const Int32 METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_QUANTITY = 161;
    public const Int32 METER_CODE_INHOUSE_TRANSFERS_TO_HOST_CENTS = 184;
    public const Int32 METER_CODE_INHOUSE_TRANSFERS_TO_HOST_QUANTITY = 185;

    //Columns index
    private const Int16 COLUMN_TERMINAL_ID = 0;
    private const Int16 COLUMN_SEQUENCE_ID = 1;
    private const Int16 COLUMN_PLAYED_WON_AVAILABLE = 2;
    private const Int16 COLUMN_PLAYED_COUNT = 3;
    private const Int16 COLUMN_PLAYED_AMOUNT = 4;
    private const Int16 COLUMN_WON_COUNT = 5;
    private const Int16 COLUMN_WON_AMOUNT = 6;
    private const Int16 COLUMN_JACKPOT_AMOUNT = 7;
    private const Int16 COLUMN_AFT_AVAILABLE = 8;
    private const Int16 COLUMN_AFT_TO_GM_COUNT = 9;
    private const Int16 COLUMN_AFT_TO_GM_AMOUNT = 10;
    private const Int16 COLUMN_AFT_FROM_GM_COUNT = 11;
    private const Int16 COLUMN_AFT_FROM_GM_AMOUNT = 12;
    private const Int16 COLUMN_EFT_TO_GM_COUNT = 13;
    private const Int16 COLUMN_EFT_TO_GM_AMOUNT = 14;
    private const Int16 COLUMN_EFT_FROM_GM_COUNT = 15;
    private const Int16 COLUMN_EFT_FROM_GM_AMOUNT = 16;
    private const Int16 COLUMN_LAST_REPORTED = 17;
    private const Int16 COLUMN_DELTA_PLAYED_COUNT = 18;
    private const Int16 COLUMN_DELTA_PLAYED_AMOUNT = 19;
    private const Int16 COLUMN_DELTA_WON_COUNT = 20;
    private const Int16 COLUMN_DELTA_WON_AMOUNT = 21;
    private const Int16 COLUMN_DELTA_JACKPOT_AMOUNT = 22;
    private const Int16 COLUMN_DELTA_TO_GM_COUNT = 23;
    private const Int16 COLUMN_DELTA_TO_GM_AMOUNT = 24;
    private const Int16 COLUMN_DELTA_FROM_GM_COUNT = 25;
    private const Int16 COLUMN_DELTA_FROM_GM_AMOUNT = 26;
    private const Int16 COLUMN_OLD_PLAYED_COUNT = 27;
    private const Int16 COLUMN_OLD_PLAYED_AMOUNT = 28;
    private const Int16 COLUMN_OLD_WON_COUNT = 29;
    private const Int16 COLUMN_OLD_WON_AMOUNT = 30;
    private const Int16 COLUMN_OLD_JACKPOT_AMOUNT = 31;
    private const Int16 COLUMN_OLD_AFT_TO_GM_COUNT = 32;
    private const Int16 COLUMN_OLD_AFT_TO_GM_AMOUNT = 33;
    private const Int16 COLUMN_OLD_AFT_FROM_GM_COUNT = 34;
    private const Int16 COLUMN_OLD_AFT_FROM_GM_AMOUNT = 35;
    private const Int16 COLUMN_OLD_EFT_TO_GM_COUNT = 36;
    private const Int16 COLUMN_OLD_EFT_TO_GM_AMOUNT = 37;
    private const Int16 COLUMN_OLD_EFT_FROM_GM_COUNT = 38;
    private const Int16 COLUMN_OLD_EFT_FROM_GM_AMOUNT = 39;
    private const Int16 COLUMN_RESPONSE_CODE = 40;
    private const Int16 COLUMN_PROVIDER_ID = 41;
    private const Int16 COLUMN_TERMINAL_NAME = 42;
    private const Int16 COLUMN_MAX_PLAYED_COUNT = 43;
    private const Int16 COLUMN_MAX_PLAYED_AMOUNT = 44;
    private const Int16 COLUMN_MAX_WON_COUNT = 45;
    private const Int16 COLUMN_MAX_WON_AMOUNT = 46;
    private const Int16 COLUMN_MAX_JACKPOT_AMOUNT = 47;
    private const Int16 COLUMN_PROGRESSIVE_JACKPOT_AMOUNT = 48;
    private const Int16 COLUMN_DELTA_PROGRESSIVE_JACKPOT_AMOUNT = 49;
    private const Int16 COLUMN_OLD_PROGRESSIVE_JACKPOT_AMOUNT = 50;
    private const Int16 COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT = 51;
    private const Int16 COLUMN_TERMINAL_SAS_ACCOUNTING_DENOM = 52;

    public const Decimal EMPTY_SAS_ACCOUNTING_DENOM = -1m;
    public const Decimal DEFAULT_SAS_ACCOUNTING_DENOM = 0.01m;

    #endregion // Constants

    #region Attributes

    private static DataTable m_dt_machine_meters = null;

    public static Boolean m_has_to_reload_dt = false; //Has to reload datatable, because a machine SAS account denom has changed

    private static SqlDataAdapter m_adap_machine_meters = null;
    private static SqlDataAdapter m_adap_mm_select = null;

    private static ReaderWriterLock m_rw_lock_queue = new ReaderWriterLock();
    private static ArrayList m_unload_queue = new ArrayList(10);

    public static Int32 m_time_num_all = 0;
    public static Int32 m_time_num_loads = 0;

    public static Int64 m_time_all = 0;
    public static Int64 m_time_update_session = 0;
    public static Int64 m_time_gets = 0;
    public static Int64 m_time_updates = 0;
    public static Int64 m_time_updateDB = 0;
    public static Int64 m_time_commit = 0;
    public static Int64 m_time_response = 0;
    public static Int64 m_time_task_process = 0;
    public static Int64 m_time_begin_trx = 0;
    public static Int64 m_time_unload = 0;
    public static Int64 m_time_loads = 0;
    public static Int64 m_time_inserts = 0;

    #endregion // Attributes

    #region Structures

    private struct SqlMeterCodeInfo
    {
      public Int32 MeterCodeId;
      public String MeterCodeParameter;
      public String MeterValueParameter;
      public String MeterDeltaParameter;
      public String MeterMaxValueParameter;
      public String MeterAvailable;
    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init attributes of the static class.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      SqlCommand _sql_cmd_select;
      SqlCommand _sql_cmd_insert;
      SqlCommand _sql_cmd_update;
      SqlMeterCodeInfo[] _tsm_to_update_from_manchine_meters;
      StringBuilder _sb_query;
      StringBuilder _insert_or_update_tsm;
      StringBuilder _sb_terminal_sas_meters_query;
      SqlMeterCodeInfo _meter_info;

      // Already init.
      if (m_dt_machine_meters != null)
      {
        return;
      }

      _sql_cmd_insert = new SqlCommand();
      _sql_cmd_update = new SqlCommand();

      // Create DataTable Columns
      m_dt_machine_meters = new DataTable("MACHINE_METERS");

      m_dt_machine_meters.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      m_dt_machine_meters.Columns.Add("SEQUENCE_ID", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("PLAYED_WON_AVAILABLE", Type.GetType("System.Int32"));
      m_dt_machine_meters.Columns.Add("PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("WON_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("AFT_AVAILABLE", Type.GetType("System.Int32"));
      m_dt_machine_meters.Columns.Add("AFT_TO_GM_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("AFT_TO_GM_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("AFT_FROM_GM_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("AFT_FROM_GM_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("EFT_TO_GM_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("EFT_TO_GM_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("EFT_FROM_GM_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("EFT_FROM_GM_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("LAST_REPORTED", Type.GetType("System.DateTime"));
      m_dt_machine_meters.Columns.Add("DELTA_PLAYED_COUNT", Type.GetType("System.Int64")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("DELTA_PLAYED_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("DELTA_WON_COUNT", Type.GetType("System.Int64")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("DELTA_WON_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("DELTA_JACKPOT_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("DELTA_TO_GM_COUNT", Type.GetType("System.Int64")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("DELTA_TO_GM_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("DELTA_FROM_GM_COUNT", Type.GetType("System.Int64")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("DELTA_FROM_GM_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("OLD_PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("OLD_PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("OLD_WON_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("OLD_WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("OLD_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("OLD_AFT_TO_GM_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("OLD_AFT_TO_GM_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("OLD_AFT_FROM_GM_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("OLD_AFT_FROM_GM_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("OLD_EFT_TO_GM_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("OLD_EFT_TO_GM_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("OLD_EFT_FROM_GM_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("OLD_EFT_FROM_GM_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("RESPONSE_CODE", Type.GetType("System.Object"));
      m_dt_machine_meters.Columns.Add("PROVIDER_ID", Type.GetType("System.String"));
      m_dt_machine_meters.Columns.Add("TERMINAL_NAME", Type.GetType("System.String"));
      m_dt_machine_meters.Columns.Add("MAX_PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("MAX_PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("MAX_WON_COUNT", Type.GetType("System.Int64"));
      m_dt_machine_meters.Columns.Add("MAX_WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("MAX_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("PROGRESSIVE_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("DELTA_PROGRESSIVE_JACKPOT_AMOUNT", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_dt_machine_meters.Columns.Add("OLD_PROGRESSIVE_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("MAX_PROGRESSIVE_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_machine_meters.Columns.Add("TERMINAL_SAS_ACCOUNTING_DENOM", Type.GetType("System.Decimal")).DefaultValue = EMPTY_SAS_ACCOUNTING_DENOM; //Terminal SAS Accounting Denom

      // Primary Key for DataTable MachineMeters.
      m_dt_machine_meters.PrimaryKey = new DataColumn[] { m_dt_machine_meters.Columns["TERMINAL_ID"] };

      _tsm_to_update_from_manchine_meters = GetTerminalSASMeters();

      // Create SelectCommand for MACHINE_METERS, and create SqlDataAdapter ONLY for Select.
      _sb_query = new StringBuilder();
      _sb_query.AppendLine("     SELECT   MM_TERMINAL_ID                          AS  TERMINAL_ID                       ");
      _sb_query.AppendLine("           ,  MM_WCP_SEQUENCE_ID                      AS  SEQUENCE_ID                       ");
      _sb_query.AppendLine("           ,  (CASE WHEN (MM_PLAYED_COUNT > 0)                                              ");
      _sb_query.AppendLine("                      THEN 1                                                                ");
      _sb_query.AppendLine("                      ELSE 0                                                                ");
      _sb_query.AppendLine("               END)                                   AS  PLAYED_WON_AVAILABLE              ");
      _sb_query.AppendLine("           ,  MM_PLAYED_COUNT                         AS  PLAYED_COUNT                      ");
      _sb_query.AppendLine("           ,  MM_PLAYED_AMOUNT                        AS  PLAYED_AMOUNT                     ");
      _sb_query.AppendLine("           ,  MM_WON_COUNT                            AS  WON_COUNT                         ");
      _sb_query.AppendLine("           ,  MM_WON_AMOUNT                           AS  WON_AMOUNT                        ");
      _sb_query.AppendLine("           ,  MM_JACKPOT_AMOUNT                       AS  JACKPOT_AMOUNT                    ");
      _sb_query.AppendLine("           ,  (CASE WHEN (MM_AFT_TO_GM_COUNT > 0)                                           ");
      _sb_query.AppendLine("                      THEN 1                                                                ");
      _sb_query.AppendLine("                      ELSE 0                                                                ");
      _sb_query.AppendLine("               END)                                   AS  AFT_AVAILABLE                     ");
      _sb_query.AppendLine("           ,  MM_AFT_TO_GM_COUNT                      AS  AFT_TO_GM_COUNT                   ");
      _sb_query.AppendLine("           ,  MM_AFT_TO_GM_AMOUNT                     AS  AFT_TO_GM_AMOUNT                  ");
      _sb_query.AppendLine("           ,  MM_AFT_FROM_GM_COUNT                    AS  AFT_FROM_GM_COUNT                 ");
      _sb_query.AppendLine("           ,  MM_AFT_FROM_GM_AMOUNT                   AS  AFT_FROM_GM_AMOUNT                ");
      _sb_query.AppendLine("           ,  MM_EFT_TO_GM_COUNT                      AS  EFT_TO_GM_COUNT                   ");
      _sb_query.AppendLine("           ,  MM_EFT_TO_GM_AMOUNT                     AS  EFT_TO_GM_AMOUNT                  ");
      _sb_query.AppendLine("           ,  MM_EFT_FROM_GM_COUNT                    AS  EFT_FROM_GM_COUNT                 ");
      _sb_query.AppendLine("           ,  MM_EFT_FROM_GM_AMOUNT                   AS  EFT_FROM_GM_AMOUNT                ");
      _sb_query.AppendLine("           ,  MM_LAST_REPORTED                        AS  LAST_REPORTED                     ");
      _sb_query.AppendLine("           ,  TE_NAME                                 AS  TERMINAL_NAME                     ");
      _sb_query.AppendLine("           ,  TE_PROVIDER_ID                          AS  PROVIDER_ID                       ");
      _sb_query.AppendLine("           ,  MM_MAX_PLAYED_COUNT                     AS  MAX_PLAYED_COUNT                  ");
      _sb_query.AppendLine("           ,  MM_MAX_PLAYED_AMOUNT                    AS  MAX_PLAYED_AMOUNT                 ");
      _sb_query.AppendLine("           ,  MM_MAX_WON_COUNT                        AS  MAX_WON_COUNT                     ");
      _sb_query.AppendLine("           ,  MM_MAX_WON_AMOUNT                       AS  MAX_WON_AMOUNT                    ");
      _sb_query.AppendLine("           ,  MM_MAX_JACKPOT_AMOUNT                   AS  MAX_JACKPOT_AMOUNT                ");
      _sb_query.AppendLine("           ,  MM_PROGRESSIVE_JACKPOT_AMOUNT           AS  PROGRESSIVE_JACKPOT_AMOUNT        ");
      _sb_query.AppendLine("           ,  MM_MAX_PROGRESSIVE_JACKPOT_AMOUNT       AS  MAX_PROGRESSIVE_JACKPOT_AMOUNT    ");
      //Get SAS_ACCOUNTING_DENOM defined in the terminal (it gets updated)
      _sb_query.AppendLine("           ,  ISNULL(TE_SAS_ACCOUNTING_DENOM, -1)     AS  TERMINAL_SAS_ACCOUNTING_DENOM     ");
      _sb_query.AppendLine("       FROM   MACHINE_METERS                                                                ");
      _sb_query.AppendLine(" INNER JOIN   TERMINALS ON MM_TERMINAL_ID = TE_TERMINAL_ID                                  ");
      _sb_query.AppendLine("      WHERE   MM_TERMINAL_ID  =  @pTerminalId                                               ");

      _sql_cmd_select = new SqlCommand(_sb_query.ToString());
      _sql_cmd_select.Parameters.Add("@pTerminalId", SqlDbType.Int);

      m_adap_mm_select = new SqlDataAdapter();
      m_adap_mm_select.SelectCommand = _sql_cmd_select;
      m_adap_mm_select.InsertCommand = null;
      m_adap_mm_select.UpdateCommand = null;
      m_adap_mm_select.DeleteCommand = null;

      _insert_or_update_tsm = new StringBuilder();

      _insert_or_update_tsm.AppendLine(" IF (#IF_AVAILABLE# = 1) ");
      _insert_or_update_tsm.AppendLine(" BEGIN                   ");
      _insert_or_update_tsm.AppendLine("   IF EXISTS ( SELECT   1                                                                 ");
      _insert_or_update_tsm.AppendLine("                 FROM   TERMINAL_SAS_METERS                                               ");
      _insert_or_update_tsm.AppendLine("                WHERE   TSM_TERMINAL_ID  = @pTerminalId                                   ");
      _insert_or_update_tsm.AppendLine("                  AND   TSM_METER_CODE   = #METER_CODE#                                   ");
      _insert_or_update_tsm.AppendLine("                  AND   TSM_GAME_ID      = 0                                              ");
      _insert_or_update_tsm.AppendLine("                  AND   TSM_DENOMINATION = 0 )                                            ");
      _insert_or_update_tsm.AppendLine("   BEGIN                                                                                  ");
      _insert_or_update_tsm.AppendLine("     UPDATE   TERMINAL_SAS_METERS                                                         ");
      _insert_or_update_tsm.AppendLine("        SET   TSM_WCP_SEQUENCE_ID       = @pWcpSequenceId                                 ");
      _insert_or_update_tsm.AppendLine("            , TSM_LAST_REPORTED         = GETDATE()                                       ");
      _insert_or_update_tsm.AppendLine("            , TSM_LAST_MODIFIED         = GETDATE()                                       ");
      _insert_or_update_tsm.AppendLine("            , TSM_METER_VALUE           = ISNULL(#METER_VALUE#, 0)                        ");
      _insert_or_update_tsm.AppendLine("            , TSM_DELTA_VALUE           = TSM_DELTA_VALUE + ISNULL(#DELTA_VALUE#, 0)      ");
      _insert_or_update_tsm.AppendLine("            , TSM_RAW_DELTA_VALUE       = TSM_RAW_DELTA_VALUE + ISNULL(#DELTA_VALUE#, 0)  ");
      _insert_or_update_tsm.AppendLine("            , TSM_SAS_ACCOUNTING_DENOM  = @pSasAccountDenom                               ");
      _insert_or_update_tsm.AppendLine("      WHERE   TSM_TERMINAL_ID           = @pTerminalId                                    ");
      _insert_or_update_tsm.AppendLine("        AND   TSM_METER_CODE            = #METER_CODE#                                    ");
      _insert_or_update_tsm.AppendLine("        AND   TSM_GAME_ID               = 0                                               ");
      _insert_or_update_tsm.AppendLine("        AND   TSM_DENOMINATION          = 0                                               ");
      _insert_or_update_tsm.AppendLine("   END                                                                      ");
      _insert_or_update_tsm.AppendLine("   ELSE                                                                     ");
      _insert_or_update_tsm.AppendLine("   BEGIN                                                                    ");
      _insert_or_update_tsm.AppendLine("     INSERT INTO TERMINAL_SAS_METERS_HISTORY (                              ");
      _insert_or_update_tsm.AppendLine("                                               TSMH_TERMINAL_ID             ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_METER_CODE              ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_GAME_ID                 ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_DENOMINATION            ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_TYPE                    ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_DATETIME                ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_METER_INI_VALUE         ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_METER_FIN_VALUE         ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_METER_INCREMENT         ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_RAW_METER_INCREMENT     ");
      _insert_or_update_tsm.AppendLine("                                             , TSMH_SAS_ACCOUNTING_DENOM    ");
      _insert_or_update_tsm.AppendLine("                                             )                              ");
      _insert_or_update_tsm.AppendLine("                                      VALUES (                              ");
      _insert_or_update_tsm.AppendLine("                                               @pTerminalId                 ");
      _insert_or_update_tsm.AppendLine("                                             , #METER_CODE#                 ");
      _insert_or_update_tsm.AppendLine("                                             , 0                            ");
      _insert_or_update_tsm.AppendLine("                                             , 0                            ");
      _insert_or_update_tsm.AppendLine("                                             , " + (Int16)ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME);
      _insert_or_update_tsm.AppendLine("                                             , GETDATE()                    ");
      _insert_or_update_tsm.AppendLine("                                             , 0                            ");
      _insert_or_update_tsm.AppendLine("                                             , ISNULL ( #METER_VALUE#, 0)   ");
      _insert_or_update_tsm.AppendLine("                                             , 0                            ");
      _insert_or_update_tsm.AppendLine("                                             , 0                            ");
      _insert_or_update_tsm.AppendLine("                                             , @pSasAccountDenom            ");
      _insert_or_update_tsm.AppendLine("                                             )                              ");

      _insert_or_update_tsm.AppendLine("                                               ");
      _insert_or_update_tsm.AppendLine("     INSERT INTO ALARMS (                      ");
      _insert_or_update_tsm.AppendLine("                          AL_SOURCE_CODE       ");
      _insert_or_update_tsm.AppendLine("                        , AL_SOURCE_ID         ");
      _insert_or_update_tsm.AppendLine("                        , AL_SOURCE_NAME       ");
      _insert_or_update_tsm.AppendLine("                        , AL_ALARM_CODE        ");
      _insert_or_update_tsm.AppendLine("                        , AL_ALARM_NAME        ");
      _insert_or_update_tsm.AppendLine("                        , AL_ALARM_DESCRIPTION ");
      _insert_or_update_tsm.AppendLine("                        , AL_SEVERITY          ");
      _insert_or_update_tsm.AppendLine("                        , AL_REPORTED          ");
      _insert_or_update_tsm.AppendLine("                        , AL_DATETIME          ");
      _insert_or_update_tsm.AppendLine("                        )                      ");
      _insert_or_update_tsm.AppendLine("                 VALUES (                      ");
      _insert_or_update_tsm.AppendLine("                          @pAlarmSourceCode    ");
      _insert_or_update_tsm.AppendLine("                        , @pTerminalId         ");
      _insert_or_update_tsm.AppendLine("                        , @pAlarmProviderID + ' - ' + @pAlarmTerminalName ");
      _insert_or_update_tsm.AppendLine("                        , @pAlarmCode          ");
      _insert_or_update_tsm.AppendLine("                        , @pAlarmName          ");
      _insert_or_update_tsm.AppendLine("                        , @pAlarmDescription   ");
      _insert_or_update_tsm.AppendLine("                        , @pAlarmSeverity      ");
      _insert_or_update_tsm.AppendLine("                        , GETDATE ()           ");
      _insert_or_update_tsm.AppendLine("                        , GETDATE ()           ");
      _insert_or_update_tsm.AppendLine("                        )                      ");

      _insert_or_update_tsm.AppendLine("     INSERT INTO TERMINAL_SAS_METERS (                                ");
      _insert_or_update_tsm.AppendLine("                                       TSM_TERMINAL_ID                ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_METER_CODE                 ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_GAME_ID                    ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_DENOMINATION               ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_WCP_SEQUENCE_ID            ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_LAST_REPORTED              ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_LAST_MODIFIED              ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_METER_VALUE                ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_METER_MAX_VALUE            ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_DELTA_VALUE                ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_RAW_DELTA_VALUE            ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_DELTA_UPDATING             ");
      _insert_or_update_tsm.AppendLine("                                     , TSM_SAS_ACCOUNTING_DENOM       ");
      _insert_or_update_tsm.AppendLine("                                     )                                ");
      _insert_or_update_tsm.AppendLine("                           (SELECT     @pTerminalId                   ");
      _insert_or_update_tsm.AppendLine("                                     , #METER_CODE#                   ");
      _insert_or_update_tsm.AppendLine("                                     , 0                              ");
      _insert_or_update_tsm.AppendLine("                                     , 0                              ");
      _insert_or_update_tsm.AppendLine("                                     , @pWcpSequenceId                ");
      _insert_or_update_tsm.AppendLine("                                     , GETDATE()                      ");
      _insert_or_update_tsm.AppendLine("                                     , GETDATE()                      ");
      _insert_or_update_tsm.AppendLine("                                     , ISNULL ( #METER_VALUE#, 0)     ");
      _insert_or_update_tsm.AppendLine("                                     , ISNULL ( #METER_MAX_VALUE#, 0) ");
      _insert_or_update_tsm.AppendLine("                                     , 0                              ");
      _insert_or_update_tsm.AppendLine("                                     , 0                              ");    // RAW_DELTA_VALUE
      _insert_or_update_tsm.AppendLine("                                     , 0                              ");
      _insert_or_update_tsm.AppendLine("                                     , @pSasAccountDenom              ");
      _insert_or_update_tsm.AppendLine("                                     )                                ");
      _insert_or_update_tsm.AppendLine("   END                                                                ");
      _insert_or_update_tsm.AppendLine(" END ");

      _sb_terminal_sas_meters_query = new StringBuilder();

      for (int i = 0; i < _tsm_to_update_from_manchine_meters.Length; i++)
      {
        _meter_info = _tsm_to_update_from_manchine_meters[i];

        _sb_terminal_sas_meters_query.AppendLine(_insert_or_update_tsm.ToString());
        _sb_terminal_sas_meters_query = _sb_terminal_sas_meters_query.Replace("#METER_CODE#", _meter_info.MeterCodeParameter);
        _sb_terminal_sas_meters_query = _sb_terminal_sas_meters_query.Replace("#METER_VALUE#", _meter_info.MeterValueParameter);
        _sb_terminal_sas_meters_query = _sb_terminal_sas_meters_query.Replace("#DELTA_VALUE#", _meter_info.MeterDeltaParameter);
        _sb_terminal_sas_meters_query = _sb_terminal_sas_meters_query.Replace("#METER_MAX_VALUE#", _meter_info.MeterMaxValueParameter);
        _sb_terminal_sas_meters_query = _sb_terminal_sas_meters_query.Replace("#IF_AVAILABLE#", _meter_info.MeterAvailable);

        _sql_cmd_insert.Parameters.Add(_meter_info.MeterCodeParameter, SqlDbType.Int).Value = _meter_info.MeterCodeId;
        _sql_cmd_update.Parameters.Add(_meter_info.MeterCodeParameter, SqlDbType.Int).Value = _meter_info.MeterCodeId;
      }

      // Create Insert and Update Command for MACHINE_METERS, and create SqlDataAdapter.

      // Insert MACHINE_METERS
      _sb_query = new StringBuilder();
      _sb_query.AppendLine(" SET NOCOUNT OFF                                       ");
      _sb_query.AppendLine("INSERT INTO MACHINE_METERS (                           ");
      _sb_query.AppendLine("                             MM_TERMINAL_ID            ");
      _sb_query.AppendLine("                           , MM_WCP_SEQUENCE_ID        ");
      _sb_query.AppendLine("                           , MM_PLAYED_COUNT           ");
      _sb_query.AppendLine("                           , MM_PLAYED_AMOUNT          ");
      _sb_query.AppendLine("                           , MM_WON_COUNT              ");
      _sb_query.AppendLine("                           , MM_WON_AMOUNT             ");
      _sb_query.AppendLine("                           , MM_JACKPOT_AMOUNT         ");
      _sb_query.AppendLine("                           , MM_PROGRESSIVE_JACKPOT_AMOUNT         ");
      _sb_query.AppendLine("                           , MM_MAX_PLAYED_COUNT       ");
      _sb_query.AppendLine("                           , MM_MAX_PLAYED_AMOUNT      ");
      _sb_query.AppendLine("                           , MM_MAX_WON_COUNT          ");
      _sb_query.AppendLine("                           , MM_MAX_WON_AMOUNT         ");
      _sb_query.AppendLine("                           , MM_MAX_JACKPOT_AMOUNT     ");
      _sb_query.AppendLine("                           , MM_MAX_PROGRESSIVE_JACKPOT_AMOUNT     ");
      _sb_query.AppendLine("                           , MM_AFT_TO_GM_COUNT        ");
      _sb_query.AppendLine("                           , MM_AFT_TO_GM_AMOUNT       ");
      _sb_query.AppendLine("                           , MM_AFT_FROM_GM_COUNT      ");
      _sb_query.AppendLine("                           , MM_AFT_FROM_GM_AMOUNT     ");
      _sb_query.AppendLine("                           , MM_EFT_TO_GM_COUNT        ");
      _sb_query.AppendLine("                           , MM_EFT_TO_GM_AMOUNT       ");
      _sb_query.AppendLine("                           , MM_EFT_FROM_GM_COUNT      ");
      _sb_query.AppendLine("                           , MM_EFT_FROM_GM_AMOUNT     ");
      _sb_query.AppendLine("                           , MM_LAST_REPORTED          ");
      _sb_query.AppendLine("                           )                           ");
      _sb_query.AppendLine("                   VALUES  (                           ");
      _sb_query.AppendLine("                             @pTerminalId              ");
      _sb_query.AppendLine("                           , @pWcpSequenceId           ");
      _sb_query.AppendLine("                           , @pNewPlayedCount          ");
      _sb_query.AppendLine("                           , @pNewPlayedAmount         ");
      _sb_query.AppendLine("                           , @pNewWonCount             ");
      _sb_query.AppendLine("                           , @pNewWonAmount            ");
      _sb_query.AppendLine("                           , @pNewJackpotAmount        ");
      _sb_query.AppendLine("                           , @pNewProgressiveJackpotAmount        ");
      _sb_query.AppendLine("                           , @pMaxPlayedCount          ");
      _sb_query.AppendLine("                           , @pMaxPlayedAmount         ");
      _sb_query.AppendLine("                           , @pMaxWonCount             ");
      _sb_query.AppendLine("                           , @pMaxWonAmount            ");
      _sb_query.AppendLine("                           , @pMaxJackpotAmount        ");
      _sb_query.AppendLine("                           , @pMaxProgressiveJackpotAmount        ");
      _sb_query.AppendLine("                           , @pNewAftToGmCount         ");
      _sb_query.AppendLine("                           , @pNewAftToGmAmount        ");
      _sb_query.AppendLine("                           , @pNewAftFromGmCount       ");
      _sb_query.AppendLine("                           , @pNewAftFromGmAmount      ");
      _sb_query.AppendLine("                           , @pNewEftToGmCount         ");
      _sb_query.AppendLine("                           , @pNewEftToGmAmount        ");
      _sb_query.AppendLine("                           , @pNewEftFromGmCount       ");
      _sb_query.AppendLine("                           , @pNewEftFromGmAmount      ");
      _sb_query.AppendLine("                           , GETDATE()                 ");
      _sb_query.AppendLine("                           )                           ");
      _sb_query.AppendLine(" SET NOCOUNT ON                                        ");

      _sb_query.AppendLine(_sb_terminal_sas_meters_query.ToString());

      _sb_query.AppendLine(" SET NOCOUNT OFF ");

      _sql_cmd_insert.CommandText = _sb_query.ToString();

      _sql_cmd_insert.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _sql_cmd_insert.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
      _sql_cmd_insert.Parameters.Add("@pPlayedWonAvailable", SqlDbType.Int).SourceColumn = "PLAYED_WON_AVAILABLE";
      _sql_cmd_insert.Parameters.Add("@pNewPlayedCount", SqlDbType.BigInt).SourceColumn = "PLAYED_COUNT";
      _sql_cmd_insert.Parameters.Add("@pNewPlayedAmount", SqlDbType.Money).SourceColumn = "PLAYED_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pNewWonCount", SqlDbType.BigInt).SourceColumn = "WON_COUNT";
      _sql_cmd_insert.Parameters.Add("@pNewWonAmount", SqlDbType.Money).SourceColumn = "WON_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pNewJackpotAmount", SqlDbType.Money).SourceColumn = "JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pNewProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxPlayedCount", SqlDbType.BigInt).SourceColumn = "MAX_PLAYED_COUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxPlayedAmount", SqlDbType.Money).SourceColumn = "MAX_PLAYED_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxWonCount", SqlDbType.BigInt).SourceColumn = "MAX_WON_COUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxWonAmount", SqlDbType.Money).SourceColumn = "MAX_WON_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxJackpotAmount", SqlDbType.Money).SourceColumn = "MAX_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pMaxProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "MAX_PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pAftAvailable", SqlDbType.Int).SourceColumn = "AFT_AVAILABLE";
      _sql_cmd_insert.Parameters.Add("@pNewAftToGmCount", SqlDbType.BigInt).SourceColumn = "AFT_TO_GM_COUNT";
      _sql_cmd_insert.Parameters.Add("@pNewAftToGmAmount", SqlDbType.Money).SourceColumn = "AFT_TO_GM_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pNewAftFromGmCount", SqlDbType.BigInt).SourceColumn = "AFT_FROM_GM_COUNT";
      _sql_cmd_insert.Parameters.Add("@pNewAftFromGmAmount", SqlDbType.Money).SourceColumn = "AFT_FROM_GM_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pNewEftToGmCount", SqlDbType.BigInt).SourceColumn = "EFT_TO_GM_COUNT";
      _sql_cmd_insert.Parameters.Add("@pNewEftToGmAmount", SqlDbType.Money).SourceColumn = "EFT_TO_GM_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pNewEftFromGmCount", SqlDbType.BigInt).SourceColumn = "EFT_FROM_GM_COUNT";
      _sql_cmd_insert.Parameters.Add("@pNewEftFromGmAmount", SqlDbType.Money).SourceColumn = "EFT_FROM_GM_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).SourceColumn = "DELTA_PLAYED_COUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).SourceColumn = "DELTA_PLAYED_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).SourceColumn = "DELTA_WON_COUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).SourceColumn = "DELTA_WON_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).SourceColumn = "DELTA_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "DELTA_PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaToGmCount", SqlDbType.BigInt).SourceColumn = "DELTA_TO_GM_COUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaToGmAmount", SqlDbType.Money).SourceColumn = "DELTA_TO_GM_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaFromGmCount", SqlDbType.BigInt).SourceColumn = "DELTA_FROM_GM_COUNT";
      _sql_cmd_insert.Parameters.Add("@pDeltaFromGmAmount", SqlDbType.Money).SourceColumn = "DELTA_FROM_GM_AMOUNT";
      _sql_cmd_insert.Parameters.Add("@pSasAccountDenom", SqlDbType.Money).SourceColumn = "TERMINAL_SAS_ACCOUNTING_DENOM";

      //Code added for insert alarm
      _sql_cmd_insert.Parameters.Add("@pAlarmTerminalName", SqlDbType.NVarChar).SourceColumn = "TERMINAL_NAME";
      _sql_cmd_insert.Parameters.Add("@pAlarmProviderID", SqlDbType.NVarChar).SourceColumn = "PROVIDER_ID";
      _sql_cmd_insert.Parameters.Add("@pAlarmSourceCode", SqlDbType.Int).Value = AlarmSourceCode.TerminalSASHost;
      _sql_cmd_insert.Parameters.Add("@pAlarmName", SqlDbType.NVarChar).Value = AlarmSourceCode.TerminalSASHost.ToString();
      _sql_cmd_insert.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = AlarmCode.TerminalSystem_SasHostFirstTime;
      _sql_cmd_insert.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar).Value = "*** SAS Meters First Time ***";
      _sql_cmd_insert.Parameters.Add("@pAlarmSeverity", SqlDbType.Int).Value = AlarmSeverity.Info;

      // Update MACHINE_METERS
      _sb_query = new StringBuilder();
      _sb_query.AppendLine(" SET NOCOUNT OFF                                                                                                                        ");
      _sb_query.AppendLine(" UPDATE MACHINE_METERS                                                                                                                  ");
      _sb_query.AppendLine("    SET MM_WCP_SEQUENCE_ID                    = @pWcpSequenceId                                                                         ");
      _sb_query.AppendLine("      , MM_PLAYED_COUNT                       = @pNewPlayedCount                                                                        ");
      _sb_query.AppendLine("      , MM_PLAYED_AMOUNT                      = @pNewPlayedAmount                                                                       ");
      _sb_query.AppendLine("      , MM_WON_COUNT                          = @pNewWonCount                                                                           ");
      _sb_query.AppendLine("      , MM_WON_AMOUNT                         = @pNewWonAmount                                                                          ");
      _sb_query.AppendLine("      , MM_JACKPOT_AMOUNT                     = @pNewJackpotAmount                                                                      ");
      _sb_query.AppendLine("      , MM_PROGRESSIVE_JACKPOT_AMOUNT         = @pNewProgressiveJackpotAmount                                                           ");
      _sb_query.AppendLine("      , MM_MAX_PLAYED_COUNT                   = @pMaxPlayedCount                                                                        ");
      _sb_query.AppendLine("      , MM_MAX_PLAYED_AMOUNT                  = @pMaxPlayedAmount                                                                       ");
      _sb_query.AppendLine("      , MM_MAX_WON_COUNT                      = @pMaxWonCount                                                                           ");
      _sb_query.AppendLine("      , MM_MAX_WON_AMOUNT                     = @pMaxWonAmount                                                                          ");
      _sb_query.AppendLine("      , MM_MAX_JACKPOT_AMOUNT                 = @pMaxJackpotAmount                                                                      ");
      _sb_query.AppendLine("      , MM_MAX_PROGRESSIVE_JACKPOT_AMOUNT     = @pMaxProgressiveJackpotAmount                                                           ");
      _sb_query.AppendLine("      , MM_AFT_TO_GM_COUNT                    = @pNewAftToGmCount                                                                       ");
      _sb_query.AppendLine("      , MM_AFT_TO_GM_AMOUNT                   = @pNewAftToGmAmount                                                                      ");
      _sb_query.AppendLine("      , MM_AFT_FROM_GM_COUNT                  = @pNewAftFromGmCount                                                                     ");
      _sb_query.AppendLine("      , MM_AFT_FROM_GM_AMOUNT                 = @pNewAftFromGmAmount                                                                    ");
      _sb_query.AppendLine("      , MM_EFT_TO_GM_COUNT                    = @pNewEftToGmCount                                                                       ");
      _sb_query.AppendLine("      , MM_EFT_TO_GM_AMOUNT                   = @pNewEftToGmAmount                                                                      ");
      _sb_query.AppendLine("      , MM_EFT_FROM_GM_COUNT                  = @pNewEftFromGmCount                                                                     ");
      _sb_query.AppendLine("      , MM_EFT_FROM_GM_AMOUNT                 = @pNewEftFromGmAmount                                                                    ");
      _sb_query.AppendLine("      , MM_LAST_REPORTED                      = GETDATE()                                                                               ");
      _sb_query.AppendLine("      , MM_DELTA_PLAYED_COUNT                 = MM_DELTA_PLAYED_COUNT    + @pDeltaPlayedCount                                           ");
      _sb_query.AppendLine("      , MM_DELTA_PLAYED_AMOUNT                = MM_DELTA_PLAYED_AMOUNT   + @pDeltaPlayedAmount                                          ");
      _sb_query.AppendLine("      , MM_DELTA_WON_COUNT                    = MM_DELTA_WON_COUNT       + @pDeltaWonCount                                              ");
      _sb_query.AppendLine("      , MM_DELTA_WON_AMOUNT                   = MM_DELTA_WON_AMOUNT      + @pDeltaWonAmount + @pDeltaJackpotAmount                      ");
      _sb_query.AppendLine("      , MM_DELTA_JACKPOT_AMOUNT               = MM_DELTA_JACKPOT_AMOUNT  + @pDeltaJackpotAmount                                         ");
      _sb_query.AppendLine("      , MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT   = ISNULL(MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT, 0)  + @pDeltaProgressiveJackpotAmount       ");
      _sb_query.AppendLine("      , MM_DELTA_TO_GM_COUNT                  = MM_DELTA_TO_GM_COUNT     + @pDeltaToGmCount                                             ");
      _sb_query.AppendLine("      , MM_DELTA_TO_GM_AMOUNT                 = MM_DELTA_TO_GM_AMOUNT    + @pDeltaToGmAmount                                            ");
      _sb_query.AppendLine("      , MM_DELTA_FROM_GM_COUNT                = MM_DELTA_FROM_GM_COUNT   + @pDeltaFromGmCount                                           ");
      _sb_query.AppendLine("      , MM_DELTA_FROM_GM_AMOUNT               = MM_DELTA_FROM_GM_AMOUNT  + @pDeltaFromGmAmount                                          ");
      _sb_query.AppendLine("  WHERE   MM_TERMINAL_ID                   = @pTerminalId                                                                               ");
      _sb_query.AppendLine("    AND   ((MM_PLAYED_COUNT                = @pOldPlayedCount) OR (MM_PLAYED_COUNT IS NULL AND @pOldPlayedCount IS NULL))               ");
      _sb_query.AppendLine("    AND   ((MM_PLAYED_AMOUNT               = @pOldPlayedAmount) OR (MM_PLAYED_AMOUNT IS NULL AND @pOldPlayedAmount IS NULL))            ");
      _sb_query.AppendLine("    AND   ((MM_WON_COUNT                   = @pOldWonCount) OR (MM_WON_COUNT IS NULL AND @pOldWonCount IS NULL))                        ");
      _sb_query.AppendLine("    AND   ((MM_WON_AMOUNT                  = @pOldWonAmount) OR (MM_WON_AMOUNT IS NULL AND @pOldWonAmount IS NULL))                     ");
      _sb_query.AppendLine("    AND   ((MM_JACKPOT_AMOUNT              = @pOldJackpotAmount) OR (MM_JACKPOT_AMOUNT IS NULL AND @pOldJackpotAmount IS NULL))         ");
      _sb_query.AppendLine("    AND   ((MM_PROGRESSIVE_JACKPOT_AMOUNT  = @pOldProgressiveJackpotAmount) OR (MM_PROGRESSIVE_JACKPOT_AMOUNT IS NULL AND @pOldProgressiveJackpotAmount IS NULL))  ");
      _sb_query.AppendLine("    AND   ((MM_AFT_TO_GM_COUNT             = @pOldAftToGmCount) OR (MM_AFT_TO_GM_COUNT IS NULL AND @pOldAftToGmCount IS NULL))          ");
      _sb_query.AppendLine("    AND   ((MM_AFT_TO_GM_AMOUNT            = @pOldAftToGmAmount) OR (MM_AFT_TO_GM_AMOUNT IS NULL AND @pOldAftToGmAmount IS NULL))       ");
      _sb_query.AppendLine("    AND   ((MM_AFT_FROM_GM_COUNT           = @pOldAftFromGmCount) OR (MM_AFT_FROM_GM_COUNT IS NULL AND @pOldAftFromGmCount IS NULL))    ");
      _sb_query.AppendLine("    AND   ((MM_AFT_FROM_GM_AMOUNT          = @pOldAftFromGmAmount) OR (MM_AFT_FROM_GM_AMOUNT IS NULL AND @pOldAftFromGmAmount IS NULL)) ");
      _sb_query.AppendLine("    AND   ((MM_EFT_TO_GM_COUNT             = @pOldEftToGmCount) OR (MM_EFT_TO_GM_COUNT IS NULL AND @pOldEftToGmCount IS NULL))          ");
      _sb_query.AppendLine("    AND   ((MM_EFT_TO_GM_AMOUNT            = @pOldEftToGmAmount) OR (MM_EFT_TO_GM_AMOUNT IS NULL AND @pOldEftToGmAmount IS NULL))       ");
      _sb_query.AppendLine("    AND   ((MM_EFT_FROM_GM_COUNT           = @pOldEftFromGmCount) OR (MM_EFT_FROM_GM_COUNT IS NULL AND @pOldEftFromGmCount IS NULL))    ");
      _sb_query.AppendLine("    AND   ((MM_EFT_FROM_GM_AMOUNT          = @pOldEftFromGmAmount) OR (MM_EFT_FROM_GM_AMOUNT IS NULL AND @pOldEftFromGmAmount IS NULL)) ");
      _sb_query.AppendLine("                                                                                                                                        ");
      _sb_query.AppendLine(" SET NOCOUNT ON                                       ");

      _sb_query.AppendLine(_sb_terminal_sas_meters_query.ToString());

      _sb_query.AppendLine(" SET NOCOUNT OFF ");

      _sql_cmd_update.CommandText = _sb_query.ToString();
      _sql_cmd_update.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
      _sql_cmd_update.Parameters.Add("@pPlayedWonAvailable", SqlDbType.Int).SourceColumn = "PLAYED_WON_AVAILABLE";
      _sql_cmd_update.Parameters.Add("@pNewPlayedCount", SqlDbType.BigInt).SourceColumn = "PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewPlayedAmount", SqlDbType.Money).SourceColumn = "PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewWonCount", SqlDbType.BigInt).SourceColumn = "WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewWonAmount", SqlDbType.Money).SourceColumn = "WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewJackpotAmount", SqlDbType.Money).SourceColumn = "JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxPlayedCount", SqlDbType.BigInt).SourceColumn = "MAX_PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pMaxPlayedAmount", SqlDbType.Money).SourceColumn = "MAX_PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxWonCount", SqlDbType.BigInt).SourceColumn = "MAX_WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pMaxWonAmount", SqlDbType.Money).SourceColumn = "MAX_WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxJackpotAmount", SqlDbType.Money).SourceColumn = "MAX_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pMaxProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "MAX_PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pAftAvailable", SqlDbType.Int).SourceColumn = "AFT_AVAILABLE";
      _sql_cmd_update.Parameters.Add("@pNewAftToGmCount", SqlDbType.BigInt).SourceColumn = "AFT_TO_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewAftToGmAmount", SqlDbType.Money).SourceColumn = "AFT_TO_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewAftFromGmCount", SqlDbType.BigInt).SourceColumn = "AFT_FROM_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewAftFromGmAmount", SqlDbType.Money).SourceColumn = "AFT_FROM_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewEftToGmCount", SqlDbType.BigInt).SourceColumn = "EFT_TO_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewEftToGmAmount", SqlDbType.Money).SourceColumn = "EFT_TO_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pNewEftFromGmCount", SqlDbType.BigInt).SourceColumn = "EFT_FROM_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pNewEftFromGmAmount", SqlDbType.Money).SourceColumn = "EFT_FROM_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).SourceColumn = "DELTA_PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).SourceColumn = "DELTA_PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).SourceColumn = "DELTA_WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).SourceColumn = "DELTA_WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).SourceColumn = "DELTA_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "DELTA_PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaToGmCount", SqlDbType.BigInt).SourceColumn = "DELTA_TO_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaToGmAmount", SqlDbType.Money).SourceColumn = "DELTA_TO_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaFromGmCount", SqlDbType.BigInt).SourceColumn = "DELTA_FROM_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pDeltaFromGmAmount", SqlDbType.Money).SourceColumn = "DELTA_FROM_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _sql_cmd_update.Parameters.Add("@pOldPlayedCount", SqlDbType.BigInt).SourceColumn = "OLD_PLAYED_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldPlayedAmount", SqlDbType.Money).SourceColumn = "OLD_PLAYED_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldWonCount", SqlDbType.BigInt).SourceColumn = "OLD_WON_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldWonAmount", SqlDbType.Money).SourceColumn = "OLD_WON_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldJackpotAmount", SqlDbType.Money).SourceColumn = "OLD_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "OLD_PROGRESSIVE_JACKPOT_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldAftToGmCount", SqlDbType.BigInt).SourceColumn = "OLD_AFT_TO_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldAftToGmAmount", SqlDbType.Money).SourceColumn = "OLD_AFT_TO_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldAftFromGmCount", SqlDbType.BigInt).SourceColumn = "OLD_AFT_FROM_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldAftFromGmAmount", SqlDbType.Money).SourceColumn = "OLD_AFT_FROM_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldEftToGmCount", SqlDbType.BigInt).SourceColumn = "OLD_EFT_TO_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldEftToGmAmount", SqlDbType.Money).SourceColumn = "OLD_EFT_TO_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pOldEftFromGmCount", SqlDbType.BigInt).SourceColumn = "OLD_EFT_FROM_GM_COUNT";
      _sql_cmd_update.Parameters.Add("@pOldEftFromGmAmount", SqlDbType.Money).SourceColumn = "OLD_EFT_FROM_GM_AMOUNT";
      _sql_cmd_update.Parameters.Add("@pSasAccountDenom", SqlDbType.Money).SourceColumn = "TERMINAL_SAS_ACCOUNTING_DENOM";

      //Code added for insert alarm
      _sql_cmd_update.Parameters.Add("@pAlarmTerminalName", SqlDbType.NVarChar).SourceColumn = "TERMINAL_NAME";
      _sql_cmd_update.Parameters.Add("@pAlarmProviderID", SqlDbType.NVarChar).SourceColumn = "PROVIDER_ID";
      _sql_cmd_update.Parameters.Add("@pAlarmSourceCode", SqlDbType.Int).Value = AlarmSourceCode.TerminalSASHost;
      _sql_cmd_update.Parameters.Add("@pAlarmName", SqlDbType.NVarChar).Value = AlarmSourceCode.TerminalSASHost.ToString();
      _sql_cmd_update.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = AlarmCode.TerminalSystem_SasHostFirstTime;
      _sql_cmd_update.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar).Value = "*** SAS Meters First Time ***";
      _sql_cmd_update.Parameters.Add("@pAlarmSeverity", SqlDbType.Int).Value = AlarmSeverity.Info;

      m_adap_machine_meters = new SqlDataAdapter();
      m_adap_machine_meters.SelectCommand = null;
      m_adap_machine_meters.InsertCommand = WGDB.DownsizeCommandText(_sql_cmd_insert);
      m_adap_machine_meters.UpdateCommand = WGDB.DownsizeCommandText(_sql_cmd_update);
      m_adap_machine_meters.DeleteCommand = null;
      m_adap_machine_meters.ContinueUpdateOnError = false;

      m_adap_machine_meters.UpdateBatchSize = 500;
      m_adap_machine_meters.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
      m_adap_machine_meters.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
    }

    private static SqlMeterCodeInfo[] GetTerminalSASMeters()
    {
      Int32[] _meter_codes;
      SqlMeterCodeInfo[] _meters;
      //Meters that would be updated in tsm from machine meters message - To add a new one, must be added in this list and in GetMeterData function
      _meter_codes = WCP_BU_MachineMeters.MetersReportedByMM;
      _meters = new SqlMeterCodeInfo[_meter_codes.Length];

      for (Int32 _i = 0; _i < _meter_codes.Length; _i++)
      {
        _meters[_i] = GetMeterData(_meter_codes[_i]);
      }

      return _meters;
    }

    private static SqlMeterCodeInfo GetMeterData(int MeterCodeId)
    {
      SqlMeterCodeInfo _meter_info;

      _meter_info = new SqlMeterCodeInfo();

      switch (MeterCodeId)
      {
        case 0x0000:
          _meter_info.MeterCodeId = 0x0000;
          _meter_info.MeterCodeParameter = "@pGamesPlayedAmount";
          _meter_info.MeterValueParameter = "@pNewPlayedAmount * 100";
          _meter_info.MeterDeltaParameter = "@pDeltaPlayedAmount * 100";
          _meter_info.MeterMaxValueParameter = "@pMaxPlayedAmount * 100";
          _meter_info.MeterAvailable = "@pPlayedWonAvailable";
          break;

        case 0x0001:
          _meter_info.MeterCodeId = 0x0001;
          _meter_info.MeterCodeParameter = "@pGamesWonAmount";
          _meter_info.MeterValueParameter = "@pNewWonAmount * 100";
          _meter_info.MeterDeltaParameter = "@pDeltaWonAmount * 100";
          _meter_info.MeterMaxValueParameter = "@pMaxWonAmount * 100";
          _meter_info.MeterAvailable = "@pPlayedWonAvailable";
          break;

        case 0x0002:
          _meter_info.MeterCodeId = 0x0002;
          _meter_info.MeterCodeParameter = "@pJackpotCode";
          _meter_info.MeterValueParameter = "@pNewJackpotAmount * 100";
          _meter_info.MeterDeltaParameter = "@pDeltaJackpotAmount * 100";
          _meter_info.MeterMaxValueParameter = "@pMaxJackpotAmount * 100";
          _meter_info.MeterAvailable = "@pPlayedWonAvailable";
          break;

        case 0x0005:
          _meter_info.MeterCodeId = 0x0005;
          _meter_info.MeterCodeParameter = "@pGamesPlayedCount";
          _meter_info.MeterValueParameter = "@pNewPlayedCount";
          _meter_info.MeterDeltaParameter = "@pDeltaPlayedCount";
          _meter_info.MeterMaxValueParameter = "@pMaxPlayedCount";
          _meter_info.MeterAvailable = "@pPlayedWonAvailable";
          break;

        case 0x0006:
          _meter_info.MeterCodeId = 0x0006;
          _meter_info.MeterCodeParameter = "@pGamesWonCount";
          _meter_info.MeterValueParameter = "@pNewWonCount";
          _meter_info.MeterDeltaParameter = "@pDeltaWonCount";
          _meter_info.MeterMaxValueParameter = "@pMaxWonCount";
          _meter_info.MeterAvailable = "@pPlayedWonAvailable";
          break;

        case 0x00A0:
          _meter_info.MeterCodeId = 0x00A0;
          _meter_info.MeterCodeParameter = "@pAFTToGmAmount";
          _meter_info.MeterValueParameter = "isnull (@pNewAftToGmAmount * 100, 0) + isnull (@pNewEftToGmAmount * 100, 0) ";
          _meter_info.MeterDeltaParameter = "@pDeltaToGmAmount * 100";
          _meter_info.MeterMaxValueParameter = "0";
          _meter_info.MeterAvailable = "@pAftAvailable";
          break;

        case 0x00A1:
          _meter_info.MeterCodeId = 0x00A1;
          _meter_info.MeterCodeParameter = "@pAFTToGmCount";
          _meter_info.MeterValueParameter = "isnull (@pNewAftToGmCount, 0) + isnull (@pNewEftToGmCount, 0) ";
          _meter_info.MeterDeltaParameter = "@pDeltaToGmCount";
          _meter_info.MeterMaxValueParameter = "0";
          _meter_info.MeterAvailable = "@pAftAvailable";
          break;

        case 0x00B8:
          _meter_info.MeterCodeId = 0x00B8;
          _meter_info.MeterCodeParameter = "@pAFTFromGmAmount";
          _meter_info.MeterValueParameter = "isnull (@pNewAftFromGmAmount * 100, 0) + isnull (@pNewEftFromGmAmount * 100, 0) ";
          _meter_info.MeterDeltaParameter = "@pDeltaFromGmAmount * 100";
          _meter_info.MeterMaxValueParameter = "0";
          _meter_info.MeterAvailable = "@pAftAvailable";
          break;

        case 0x00B9:
          _meter_info.MeterCodeId = 0x00B9;
          _meter_info.MeterCodeParameter = "@pAFTFromGmCount";
          _meter_info.MeterValueParameter = "isnull (@pNewAftFromGmCount, 0) + isnull (@pNewEftFromGmCount, 0) ";
          _meter_info.MeterDeltaParameter = "@pDeltaFromGmCount";
          _meter_info.MeterMaxValueParameter = "0";
          _meter_info.MeterAvailable = "@pAftAvailable";
          break;

        case 0x1000:
          _meter_info.MeterCodeId = 0x1000;
          _meter_info.MeterCodeParameter = "@pProgressiveJackpotCode";
          _meter_info.MeterValueParameter = "@pNewProgressiveJackpotAmount * 100";
          _meter_info.MeterDeltaParameter = "@pDeltaProgressiveJackpotAmount * 100";
          _meter_info.MeterMaxValueParameter = "@pMaxProgressiveJackpotAmount * 100";
          _meter_info.MeterAvailable = "@pPlayedWonAvailable";
          break;

        default:
          _meter_info.MeterCodeId = 0;
          break;
      }

      return _meter_info;
    } // GetMeterData

    //------------------------------------------------------------------------------
    // PURPOSE : Load MachineMeters data from DB to DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Load(Int32 TerminalId, SqlTransaction Trx)
    {
      int _ticks;

      _ticks = Environment.TickCount;

      m_adap_mm_select.SelectCommand.Connection = Trx.Connection;
      m_adap_mm_select.SelectCommand.Transaction = Trx;
      m_adap_mm_select.SelectCommand.Parameters["@pTerminalId"].Value = TerminalId;

      m_adap_mm_select.Fill(m_dt_machine_meters);

      m_has_to_reload_dt = false;
      m_time_loads += Misc.GetElapsedTicks(_ticks);
      m_time_num_loads++;
    } // Load

    //------------------------------------------------------------------------------
    // PURPOSE : Unqueue a Unload petition for TerminalId.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void EnqueueUnload(Int32 TerminalId)
    {
      try
      {
        m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);

        m_unload_queue.Add(TerminalId);
      }
      finally
      {
        m_rw_lock_queue.ReleaseWriterLock();
      }
    } // EnqueueUnload

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows related to the TerminalId from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean Unload(Int32 TerminalId)
    {
      String _filter;

      _filter = "TERMINAL_ID = " + TerminalId;

      return Unload(_filter);
    } // Unload

    //------------------------------------------------------------------------------
    // PURPOSE : Get Machine Meters from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //          - DbMachineMeters
    //          - DbWcpSequenceId
    //          - RowMachineMeters
    //
    // RETURNS :
    //      - true: successful.
    //      - false: not found or error.
    //
    public static Boolean Get(Int32 TerminalId, out MachineMeters DbMachineMeters,
                              out Int64 DbWcpSequenceId, out DataRow RowMachineMeters, SqlTransaction Trx)
    {
      String _filter;
      DataRow[] _term_rows;
      DataRow _term_row;
      int _ticks;

      _ticks = Environment.TickCount;

      DbMachineMeters = new MachineMeters();
      DbWcpSequenceId = 0;
      RowMachineMeters = null;

      try
      {
        if (m_dt_machine_meters == null)
        {
          throw new Exception("Get: BU_MachineMeters not initialized. Call Init first!");
        }

        //Has to reload datatable. The terminal value of TE_SAS_ACCOUNTING_DENOM has changed
        if (m_has_to_reload_dt)
        {
          Load(TerminalId, Trx);
        }

        _term_rows = null;
        _filter = "TERMINAL_ID = " + TerminalId;

        for (int _idx_tries = 0; _idx_tries < 2; _idx_tries++)
        {
          _term_rows = m_dt_machine_meters.Select(_filter);

          if (_term_rows.Length != 0)
          {
            break;
          }

          if (_idx_tries != 0)
          {
            return false;
          }

          Load(TerminalId, Trx);
        }

        if (_term_rows.Length > 1)
        {
          Log.Warning("BU_MachineMeters.Get: Too many Machine Meters for TerminalId: " + TerminalId +
                      ". Count: " + _term_rows.Length + ".");
        }

        _term_row = _term_rows[0];

        DbWcpSequenceId = (Int64)_term_row[COLUMN_SEQUENCE_ID];

        DbMachineMeters.PlayWonAvalaible = false;

        if (!_term_row.IsNull(COLUMN_PLAYED_COUNT))
        {
          DbMachineMeters.PlayWonAvalaible = true;
          DbMachineMeters.PlayedQuantity = (Int64)_term_row[COLUMN_PLAYED_COUNT];
          DbMachineMeters.PlayedCents = (Int64)((Decimal)_term_row[COLUMN_PLAYED_AMOUNT] * 100);
          DbMachineMeters.WonQuantity = (Int64)_term_row[COLUMN_WON_COUNT];
          DbMachineMeters.WonCents = (Int64)((Decimal)_term_row[COLUMN_WON_AMOUNT] * 100);

          if (!_term_row.IsNull(COLUMN_JACKPOT_AMOUNT))
          {
            DbMachineMeters.JackpotCents = (Int64)((Decimal)_term_row[COLUMN_JACKPOT_AMOUNT] * 100);
          }
          else
          {
            DbMachineMeters.JackpotCents = -1;
          }

          if (!_term_row.IsNull(COLUMN_PROGRESSIVE_JACKPOT_AMOUNT))
          {
            DbMachineMeters.ProgressiveJackpotCents = (Int64)((Decimal)_term_row[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] * 100);
          }
          else
          {
            DbMachineMeters.ProgressiveJackpotCents = -1;
          }

          if (!_term_row.IsNull(COLUMN_MAX_PLAYED_COUNT))
          {
            DbMachineMeters.PlayedMaxValueQuantity = (Int64)_term_row[COLUMN_MAX_PLAYED_COUNT];
            DbMachineMeters.PlayedMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_PLAYED_AMOUNT] * 100);
            DbMachineMeters.WonMaxValueQuantity = (Int64)_term_row[COLUMN_MAX_WON_COUNT];
            DbMachineMeters.WonMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_WON_AMOUNT] * 100);

            if (!_term_row.IsNull(COLUMN_MAX_JACKPOT_AMOUNT))
            {
              DbMachineMeters.JackpotMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_JACKPOT_AMOUNT] * 100);
            }
            else
            {
              DbMachineMeters.JackpotMaxValueCents = -1;
            }

            if (!_term_row.IsNull(COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT))
            {
              DbMachineMeters.ProgressiveJackpotMaxValueCents = (Int64)((Decimal)_term_row[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] * 100);
            }
            else
            {
              DbMachineMeters.ProgressiveJackpotMaxValueCents = -1;
            }
          }
          else
          {
            DbMachineMeters.PlayedMaxValueQuantity = -1;
            DbMachineMeters.PlayedMaxValueCents = -1;
            DbMachineMeters.WonMaxValueQuantity = -1;
            DbMachineMeters.WonMaxValueCents = -1;
            DbMachineMeters.JackpotMaxValueCents = -1;
            DbMachineMeters.ProgressiveJackpotMaxValueCents = -1;
          }
        }

        DbMachineMeters.AftFundTransferAvalaible = false;
        if (!_term_row.IsNull(COLUMN_AFT_TO_GM_COUNT))
        {
          DbMachineMeters.AftFundTransferAvalaible = true;
          DbMachineMeters.AftToGmQuantity = (Int64)_term_row[COLUMN_AFT_TO_GM_COUNT];
          DbMachineMeters.AftToGmCents = (Int64)((Decimal)_term_row[COLUMN_AFT_TO_GM_AMOUNT] * 100);
          DbMachineMeters.AftFromGmQuantity = (Int64)_term_row[COLUMN_AFT_FROM_GM_COUNT];
          DbMachineMeters.AftFromGmCents = (Int64)((Decimal)_term_row[COLUMN_AFT_FROM_GM_AMOUNT] * 100);
        }

        DbMachineMeters.EftFundTransferAvalaible = false;
        if (!_term_row.IsNull(COLUMN_EFT_TO_GM_COUNT))
        {
          DbMachineMeters.EftFundTransferAvalaible = true;
          DbMachineMeters.EftToGmQuantity = (Int64)_term_row[COLUMN_EFT_TO_GM_COUNT];
          DbMachineMeters.EftToGmCents = (Int64)((Decimal)_term_row[COLUMN_EFT_TO_GM_AMOUNT] * 100);
          DbMachineMeters.EftFromGmQuantity = (Int64)_term_row[COLUMN_EFT_FROM_GM_COUNT];
          DbMachineMeters.EftFromGmCents = (Int64)((Decimal)_term_row[COLUMN_EFT_FROM_GM_AMOUNT] * 100);
        }

        RowMachineMeters = _term_row;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_time_gets += Misc.GetElapsedTicks(_ticks);
      }
    } // Get

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Machine Meters received to the DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - ProviderId
    //          - TerminalName
    //          - WcpSequenceId
    //          - NewMachineMeters: Wcp message machine meters
    //          - TerminalSASAccountDenom
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: machine meters inserted.
    //      - false: error.
    //
    public static Boolean Insert(Int32 TerminalId,
                                   String ProviderId,
                                   String TerminalName,
                                   Int64 WcpSequenceId,
                                   MachineMeters NewMachineMeters,
                                 Decimal TerminalSASAccountDenom,
                                   IWcpTaskStatus WcpTaskStatus)
    {
      DataRow _row;
      int _ticks;

      if (m_dt_machine_meters == null)
      {
        throw new Exception("Insert: BU_MachineMeters not initialized. Call Init first!");
      }

      _ticks = Environment.TickCount;

      try
      {
        _row = m_dt_machine_meters.NewRow();

        _row[COLUMN_TERMINAL_ID] = TerminalId;
        _row[COLUMN_PROVIDER_ID] = ProviderId;
        _row[COLUMN_TERMINAL_NAME] = TerminalName;
        _row[COLUMN_SEQUENCE_ID] = WcpSequenceId;

        _row[COLUMN_PLAYED_WON_AVAILABLE] = NewMachineMeters.PlayWonAvalaible;

        if (NewMachineMeters.PlayWonAvalaible)
        {
          _row[COLUMN_PLAYED_COUNT] = NewMachineMeters.PlayedQuantity;
          _row[COLUMN_PLAYED_AMOUNT] = ((Decimal)NewMachineMeters.PlayedCents) / 100;
          _row[COLUMN_WON_COUNT] = NewMachineMeters.WonQuantity;
          _row[COLUMN_WON_AMOUNT] = ((Decimal)NewMachineMeters.WonCents) / 100;

          if (NewMachineMeters.JackpotCents == -1)
          {
            _row[COLUMN_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            _row[COLUMN_JACKPOT_AMOUNT] = ((Decimal)NewMachineMeters.JackpotCents) / 100;
          }

          if (NewMachineMeters.ProgressiveJackpotCents == -1)
          {
            _row[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            _row[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)NewMachineMeters.ProgressiveJackpotCents) / 100;
          }

          _row[COLUMN_MAX_PLAYED_COUNT] = NewMachineMeters.PlayedMaxValueQuantity;
          _row[COLUMN_MAX_PLAYED_AMOUNT] = ((Decimal)NewMachineMeters.PlayedMaxValueCents) / 100;
          _row[COLUMN_MAX_WON_COUNT] = NewMachineMeters.WonMaxValueQuantity;
          _row[COLUMN_MAX_WON_AMOUNT] = ((Decimal)NewMachineMeters.WonMaxValueCents) / 100;

          if (NewMachineMeters.JackpotMaxValueCents == -1)
          {
            _row[COLUMN_MAX_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            _row[COLUMN_MAX_JACKPOT_AMOUNT] = ((Decimal)NewMachineMeters.JackpotMaxValueCents) / 100;
          }

          if (NewMachineMeters.ProgressiveJackpotMaxValueCents == -1)
          {
            _row[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            _row[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)NewMachineMeters.ProgressiveJackpotMaxValueCents) / 100;
          }
        }
        else
        {
          _row[COLUMN_PLAYED_COUNT] = DBNull.Value;
          _row[COLUMN_PLAYED_AMOUNT] = DBNull.Value;
          _row[COLUMN_WON_COUNT] = DBNull.Value;
          _row[COLUMN_WON_AMOUNT] = DBNull.Value;
          _row[COLUMN_JACKPOT_AMOUNT] = DBNull.Value;
          _row[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
        }

        _row[COLUMN_AFT_AVAILABLE] = NewMachineMeters.AftFundTransferAvalaible;
        if (NewMachineMeters.AftFundTransferAvalaible)
        {
          _row[COLUMN_AFT_TO_GM_COUNT] = NewMachineMeters.AftToGmQuantity;
          _row[COLUMN_AFT_TO_GM_AMOUNT] = ((Decimal)NewMachineMeters.AftToGmCents) / 100;
          _row[COLUMN_AFT_FROM_GM_COUNT] = NewMachineMeters.AftFromGmQuantity;
          _row[COLUMN_AFT_FROM_GM_AMOUNT] = ((Decimal)NewMachineMeters.AftFromGmCents) / 100;
        }
        else
        {
          _row[COLUMN_AFT_TO_GM_COUNT] = DBNull.Value;
          _row[COLUMN_AFT_TO_GM_AMOUNT] = DBNull.Value;
          _row[COLUMN_AFT_FROM_GM_COUNT] = DBNull.Value;
          _row[COLUMN_AFT_FROM_GM_AMOUNT] = DBNull.Value;
        }

        if (NewMachineMeters.EftFundTransferAvalaible)
        {
          _row[COLUMN_EFT_TO_GM_COUNT] = NewMachineMeters.EftToGmQuantity;
          _row[COLUMN_EFT_TO_GM_AMOUNT] = ((Decimal)NewMachineMeters.EftToGmCents) / 100;
          _row[COLUMN_EFT_FROM_GM_COUNT] = NewMachineMeters.EftFromGmQuantity;
          _row[COLUMN_EFT_FROM_GM_AMOUNT] = ((Decimal)NewMachineMeters.EftFromGmCents) / 100;
        }
        else
        {
          _row[COLUMN_EFT_TO_GM_COUNT] = DBNull.Value;
          _row[COLUMN_EFT_TO_GM_AMOUNT] = DBNull.Value;
          _row[COLUMN_EFT_FROM_GM_COUNT] = DBNull.Value;
          _row[COLUMN_EFT_FROM_GM_AMOUNT] = DBNull.Value;
        }

        // Terminal SAS account denom
        _row[COLUMN_TERMINAL_SAS_ACCOUNTING_DENOM] = TerminalSASAccountDenom;

        // Response Code
        _row[COLUMN_RESPONSE_CODE] = WcpTaskStatus;

        m_dt_machine_meters.Rows.Add(_row);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_time_inserts += Misc.GetElapsedTicks(_ticks);
      }
    } // Insert

    //------------------------------------------------------------------------------
    // PURPOSE : Update Machine Meters from the DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - NewMachineMeters: Wcp message machine meters
    //          - OldMachineMeters
    //          - DeltaMachineMeters
    //          - RowMachineMeters
    //          - WcpTaskStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: machine meters updated.
    //      - false: not found or error.
    //
    public static Boolean Update(Int32 TerminalId, Int64 WcpSequenceId, MachineMeters NewMachineMeters,
                                 MachineMeters OldMachineMeters, MachineMeters DeltaMachineMeters, DataRow RowMachineMeters,
                                 IWcpTaskStatus WcpTaskStatus)
    {
      int _ticks;

      if (m_dt_machine_meters == null)
      {
        throw new Exception("Update: BU_MachineMeters not initialized. Call Init first!");
      }

      _ticks = Environment.TickCount;

      try
      {
        RowMachineMeters.BeginEdit();

        RowMachineMeters[COLUMN_SEQUENCE_ID] = WcpSequenceId;
        RowMachineMeters[COLUMN_LAST_REPORTED] = WGDB.Now;

        RowMachineMeters[COLUMN_PLAYED_WON_AVAILABLE] = NewMachineMeters.PlayWonAvalaible;

        if (NewMachineMeters.PlayWonAvalaible)
        {
          RowMachineMeters[COLUMN_PLAYED_COUNT] = NewMachineMeters.PlayedQuantity;
          RowMachineMeters[COLUMN_PLAYED_AMOUNT] = ((Decimal)NewMachineMeters.PlayedCents) / 100;
          RowMachineMeters[COLUMN_WON_COUNT] = NewMachineMeters.WonQuantity;
          RowMachineMeters[COLUMN_WON_AMOUNT] = ((Decimal)NewMachineMeters.WonCents) / 100;

          if (NewMachineMeters.JackpotCents == -1)
          {
            RowMachineMeters[COLUMN_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            RowMachineMeters[COLUMN_JACKPOT_AMOUNT] = ((Decimal)NewMachineMeters.JackpotCents) / 100;
          }

          if (NewMachineMeters.ProgressiveJackpotCents == -1)
          {
            RowMachineMeters[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            RowMachineMeters[COLUMN_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)NewMachineMeters.ProgressiveJackpotCents) / 100;
          }

          RowMachineMeters[COLUMN_MAX_PLAYED_COUNT] = NewMachineMeters.PlayedMaxValueQuantity;
          RowMachineMeters[COLUMN_MAX_PLAYED_AMOUNT] = ((Decimal)NewMachineMeters.PlayedMaxValueCents) / 100;
          RowMachineMeters[COLUMN_MAX_WON_COUNT] = NewMachineMeters.WonMaxValueQuantity;
          RowMachineMeters[COLUMN_MAX_WON_AMOUNT] = ((Decimal)NewMachineMeters.WonMaxValueCents) / 100;

          if (NewMachineMeters.JackpotMaxValueCents == -1)
          {
            RowMachineMeters[COLUMN_MAX_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            RowMachineMeters[COLUMN_MAX_JACKPOT_AMOUNT] = ((Decimal)NewMachineMeters.JackpotMaxValueCents) / 100;
          }

          if (NewMachineMeters.ProgressiveJackpotMaxValueCents == -1)
          {
            RowMachineMeters[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            RowMachineMeters[COLUMN_MAX_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)NewMachineMeters.ProgressiveJackpotMaxValueCents) / 100;
          }
        }

        RowMachineMeters[COLUMN_AFT_AVAILABLE] = NewMachineMeters.AftFundTransferAvalaible;

        if (NewMachineMeters.AftFundTransferAvalaible)
        {
          RowMachineMeters[COLUMN_AFT_TO_GM_COUNT] = NewMachineMeters.AftToGmQuantity;
          RowMachineMeters[COLUMN_AFT_TO_GM_AMOUNT] = ((Decimal)NewMachineMeters.AftToGmCents) / 100;
          RowMachineMeters[COLUMN_AFT_FROM_GM_COUNT] = NewMachineMeters.AftFromGmQuantity;
          RowMachineMeters[COLUMN_AFT_FROM_GM_AMOUNT] = ((Decimal)NewMachineMeters.AftFromGmCents) / 100;
        }

        if (NewMachineMeters.EftFundTransferAvalaible)
        {
          RowMachineMeters[COLUMN_EFT_TO_GM_COUNT] = NewMachineMeters.EftToGmQuantity;
          RowMachineMeters[COLUMN_EFT_TO_GM_AMOUNT] = ((Decimal)NewMachineMeters.EftToGmCents) / 100;
          RowMachineMeters[COLUMN_EFT_FROM_GM_COUNT] = NewMachineMeters.EftFromGmQuantity;
          RowMachineMeters[COLUMN_EFT_FROM_GM_AMOUNT] = ((Decimal)NewMachineMeters.EftFromGmCents) / 100;
        }

        if (OldMachineMeters.PlayWonAvalaible)
        {
          RowMachineMeters[COLUMN_OLD_PLAYED_COUNT] = OldMachineMeters.PlayedQuantity;
          RowMachineMeters[COLUMN_OLD_PLAYED_AMOUNT] = ((Decimal)OldMachineMeters.PlayedCents) / 100;
          RowMachineMeters[COLUMN_OLD_WON_COUNT] = OldMachineMeters.WonQuantity;
          RowMachineMeters[COLUMN_OLD_WON_AMOUNT] = ((Decimal)OldMachineMeters.WonCents) / 100;

          if (OldMachineMeters.JackpotCents == -1)
          {
            RowMachineMeters[COLUMN_OLD_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            RowMachineMeters[COLUMN_OLD_JACKPOT_AMOUNT] = ((Decimal)OldMachineMeters.JackpotCents) / 100;
          }

          if (OldMachineMeters.ProgressiveJackpotCents == -1)
          {
            RowMachineMeters[COLUMN_OLD_PROGRESSIVE_JACKPOT_AMOUNT] = DBNull.Value;
          }
          else
          {
            RowMachineMeters[COLUMN_OLD_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)OldMachineMeters.ProgressiveJackpotCents) / 100;
          }
        }

        if (OldMachineMeters.AftFundTransferAvalaible)
        {
          RowMachineMeters[COLUMN_OLD_AFT_TO_GM_COUNT] = OldMachineMeters.AftToGmQuantity;
          RowMachineMeters[COLUMN_OLD_AFT_TO_GM_AMOUNT] = ((Decimal)OldMachineMeters.AftToGmCents) / 100;
          RowMachineMeters[COLUMN_OLD_AFT_FROM_GM_COUNT] = OldMachineMeters.AftFromGmQuantity;
          RowMachineMeters[COLUMN_OLD_AFT_FROM_GM_AMOUNT] = ((Decimal)OldMachineMeters.AftFromGmCents) / 100;
        }

        if (OldMachineMeters.EftFundTransferAvalaible)
        {
          RowMachineMeters[COLUMN_OLD_EFT_TO_GM_COUNT] = OldMachineMeters.EftToGmQuantity;
          RowMachineMeters[COLUMN_OLD_EFT_TO_GM_AMOUNT] = ((Decimal)OldMachineMeters.EftToGmCents) / 100;
          RowMachineMeters[COLUMN_OLD_EFT_FROM_GM_COUNT] = OldMachineMeters.EftFromGmQuantity;
          RowMachineMeters[COLUMN_OLD_EFT_FROM_GM_AMOUNT] = ((Decimal)OldMachineMeters.EftFromGmCents) / 100;
        }

        RowMachineMeters[COLUMN_DELTA_PLAYED_COUNT] = DeltaMachineMeters.PlayedQuantity;
        RowMachineMeters[COLUMN_DELTA_PLAYED_AMOUNT] = ((Decimal)DeltaMachineMeters.PlayedCents) / 100;
        RowMachineMeters[COLUMN_DELTA_WON_COUNT] = DeltaMachineMeters.WonQuantity;
        RowMachineMeters[COLUMN_DELTA_WON_AMOUNT] = ((Decimal)DeltaMachineMeters.WonCents) / 100;
        RowMachineMeters[COLUMN_DELTA_JACKPOT_AMOUNT] = ((Decimal)DeltaMachineMeters.JackpotCents) / 100;
        RowMachineMeters[COLUMN_DELTA_PROGRESSIVE_JACKPOT_AMOUNT] = ((Decimal)DeltaMachineMeters.ProgressiveJackpotCents) / 100;
        RowMachineMeters[COLUMN_DELTA_TO_GM_COUNT] = DeltaMachineMeters.AftToGmQuantity + DeltaMachineMeters.EftToGmQuantity;
        RowMachineMeters[COLUMN_DELTA_TO_GM_AMOUNT] = ((Decimal)(DeltaMachineMeters.AftToGmCents + DeltaMachineMeters.EftToGmCents)) / 100;
        RowMachineMeters[COLUMN_DELTA_FROM_GM_COUNT] = DeltaMachineMeters.AftFromGmQuantity + DeltaMachineMeters.EftFromGmQuantity;
        RowMachineMeters[COLUMN_DELTA_FROM_GM_AMOUNT] = ((Decimal)(DeltaMachineMeters.AftFromGmCents + DeltaMachineMeters.EftFromGmCents)) / 100;

        // Response Code
        RowMachineMeters[COLUMN_RESPONSE_CODE] = WcpTaskStatus;

        RowMachineMeters.EndEdit();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_time_updates += Misc.GetElapsedTicks(_ticks);
      }
    } // Update

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update DataTable Rows Machine Meters to DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data is inserted/updated correctly or not.
    //
    public static Boolean UpdateDB(SqlTransaction Trx)
    {
      String _rollback_point;
      Boolean _error;
      DataRow[] _rows_to_db;
      DataRow[] _rows_to_update;
      DataRow[] _rows_to_insert;
      Int32 _num_rows_deleted;
      IWcpTaskStatus _wcp_task_status;
      Int32 _step;

      if (m_dt_machine_meters == null)
      {
        Log.Error("UpdateDB: BU_MachineMeters not initialized. Call Init first!");
        return false;
      }

      _rollback_point = "MACHINE_METERS_BEFORE_INSERT";
      _error = false;
      _step = 0;

      try
      {
        while (true)
        {
          _rows_to_db = m_dt_machine_meters.Select("", "TERMINAL_ID", DataViewRowState.Added | DataViewRowState.ModifiedCurrent);

          // If no rows to insert/update, return depending of _error variable.
          if (_rows_to_db.Length == 0)
          {
            return !_error;
          }

          // Need to to this here, before adapter.Update().
          // In case of error, rows correctly updated/inserted must be marked again with its RowState.
          _rows_to_update = m_dt_machine_meters.Select("", "TERMINAL_ID", DataViewRowState.ModifiedCurrent);
          _rows_to_insert = m_dt_machine_meters.Select("", "TERMINAL_ID", DataViewRowState.Added);

          try
          {
            Trx.Save(_rollback_point);

            m_adap_machine_meters.InsertCommand.Connection = Trx.Connection;
            m_adap_machine_meters.InsertCommand.Transaction = Trx;
            m_adap_machine_meters.UpdateCommand.Connection = Trx.Connection;
            m_adap_machine_meters.UpdateCommand.Transaction = Trx;

            _error = true;

            _step = 1;

            try
            {
              // Insert / Update rows to DB -- Time consuming
              //int ROWS = m_adap_machine_meters.Update(_rows_to_db);
              if (m_adap_machine_meters.Update(_rows_to_db) == _rows_to_db.Length)
              {
                _error = false;
              }
            }
            catch (Exception ex)
            {
              Log.Warning("Exception 1: WCP_BU_MachineMeters.UpdateDB.");
              Log.Exception(ex);
            }

            _step = 2;

            if (!_error)
            {
              // Everything is ok.

              _step = 4;

              foreach (DataRow _row in _rows_to_db)
              {
                _wcp_task_status = (IWcpTaskStatus)_row[COLUMN_RESPONSE_CODE];
                _wcp_task_status.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
              }

              return true;
            } // if (!error)

            _step = 5;

            if (_error)
            {
              Log.Warning("------------- BEGIN MACHINE METERS NOT UPDATED -------------");

              _step = 6;

              _num_rows_deleted = 0;

              foreach (DataRow _row in _rows_to_update)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row[COLUMN_TERMINAL_ID].ToString() +
                              ". Details: " + _row.RowError);

                  _row.Delete();
                  _row.AcceptChanges();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetModified();
                }
              }

              _step = 9;

              foreach (DataRow _row in _rows_to_insert)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row[COLUMN_TERMINAL_ID].ToString() +
                              ". Details: " + _row.RowError);

                  _row.Delete();
                  _row.AcceptChanges();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetAdded();
                }
              }

              _step = 10;

              if (_num_rows_deleted == 0)
              {
                Log.Error("!!!!! Error found but not in data rows. Deleted all cache !!!!!");
                m_dt_machine_meters.Clear();
              }

              Log.Warning("------------- END   MACHINE METERS NOT UPDATED -------------");

              Trx.Rollback(_rollback_point);

            } // if (_error)

            _step = 11;

          } // try
          catch (Exception _ex)
          {
            Log.Warning("Exception 2: WCP_BU_MachineMeters.UpdateDB. Step: " + _step.ToString());
            Log.Exception(_ex);

            try
            {
              Trx.Rollback(_rollback_point);
            }
            catch (Exception _ex2)
            {
              Log.Exception(_ex2);
            }

            return false;
          }
        } // while (true)
      } // try
      catch (Exception _ex)
      {
        Log.Warning("Exception 3: WCP_BU_MachineMeters.UpdateDB.");
        Log.Exception(_ex);
        return false;
      }
      finally
      {
      }
    } // UpdateDB

    //------------------------------------------------------------------------------
    // PURPOSE : Process all MachineMeters from Tasks in the array.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Tasks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Process(ArrayList Tasks, SqlConnection SqlConn)
    {
      SqlTransaction _sql_trx;
      WCP_Message _wcp_request;
      WCP_MsgReportMachineMeters _request;
      SecureTcpClient _tcp_client;
      Int32 _tick0;
      Int32 _ticks;

      _tick0 = Environment.TickCount;

      _sql_trx = null;

      try
      {
        try
        {
          Init();

          _ticks = Environment.TickCount;

          if (m_unload_queue.Count > 0)
          {
            try
            {
              m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);

              if (m_unload_queue.Count > 0)
              {
                foreach (Int32 _term in m_unload_queue)
                {
                  Unload(_term);
                }

                m_unload_queue.Clear();
              }
            }
            finally
            {
              m_rw_lock_queue.ReleaseWriterLock();
            }
          }

          m_time_unload += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          _sql_trx = SqlConn.BeginTransaction();

          m_time_begin_trx += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _wcp_request = _wcp_task.Request;
            _tcp_client = (SecureTcpClient)_wcp_task.WcpClient;

            _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR, "No information available.");

            _request = (WCP_MsgReportMachineMeters)_wcp_request.MsgContent;

            if (!BU_ProcessMachineMeters(_tcp_client.InternalId, _wcp_request.MsgHeader.TerminalSessionId, _wcp_request.MsgHeader.SequenceId, _request, _wcp_task, _sql_trx))
            {
              Log.Error("WCP_BU_MachineMeters.Process: Not updated. Terminal: " + _tcp_client.InternalId + ".");
            }
          }

          m_time_task_process += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          if (!UpdateDB(_sql_trx))
          {
            Log.Error("WCP_BU_MachineMeters.Process: No machine meters have been modified!");
          }

          m_time_updateDB += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          _sql_trx.Commit();

          m_time_commit += Misc.GetElapsedTicks(_ticks);
          _ticks = Environment.TickCount;

          foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
          {
            _wcp_task.SendResponse();
          }

          m_time_response += Misc.GetElapsedTicks(_ticks);
        }
        catch (Exception ex)
        {
          Log.Warning("Exception: WCP_BU_MachineMeters.Process.");
          Log.Exception(ex);
        }
        finally
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              _sql_trx.Rollback();
            }
          }
        }

        m_time_all += Misc.GetElapsedTicks(_tick0);
        m_time_num_all += Tasks.Count;

        if (m_time_num_all >= 10000)
        //if (m_time_all >= 100)
        {
          // Only for test
          ////Log.Message("MM times:[#Tasks/#Load]=[" + m_time_num_all + "/" + m_time_num_loads + "] " +
          ////            "ALL:" + m_time_all + "," +
          ////            "Session:" + m_time_update_session + "," +
          ////            "Get:" + m_time_gets + "," +
          ////            "Update:" + m_time_updates + "," +
          ////            "UpdateDB:" + m_time_updateDB + "," +
          ////            "Commit:" + m_time_commit + "," +
          ////            "Response:" + m_time_response + "," +
          ////            "ProcessTaskMM:" + m_time_task_process + "," +
          ////            "BeginTrx:" + m_time_begin_trx + "," +
          ////            "Unload:" + m_time_unload + "," +
          ////            "Load:" + m_time_loads + "," +
          ////            "Insert:" + m_time_inserts + ".");

          m_time_num_all = 0;
          m_time_num_loads = 0;

          m_time_all = 0;
          m_time_update_session = 0;
          m_time_gets = 0;
          m_time_updates = 0;
          m_time_updateDB = 0;
          m_time_commit = 0;
          m_time_response = 0;
          m_time_task_process = 0;
          m_time_begin_trx = 0;
          m_time_unload = 0;
          m_time_loads = 0;
          m_time_inserts = 0;
        }
      }
      catch
      {
      }
    } // Process

    /// <summary>
    /// Creates SAS Accounting Denom change records
    ///   We get the current meters of machine
    ///   Create all the 14 and 140 meter types for principal meters
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="OldSASAccountDenom"></param>
    /// <param name="NewSASAccountDenom"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean GenerateDenomChange(Int32 TerminalId, Decimal OldSASAccountDenom, Decimal NewSASAccountDenom, SqlTransaction Trx)
    {
      Int64 DbWcpSequenceId;
      DataRow RowMachineMeters;

      MachineMeters _old_machine_meters;
      _old_machine_meters = new MachineMeters();

      //Get current machine meters
      if (Get(TerminalId, out _old_machine_meters, out DbWcpSequenceId, out RowMachineMeters, Trx))
      {
        MachineMeters _new_machine_meters;
        //Get copy of current meters
        _new_machine_meters = MachineMeters.Copy(_old_machine_meters);

        MachineMeters _delta_machine_meters;
        _delta_machine_meters = new MachineMeters();

        //Denom change
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine("*** Machine Meters SAS Accounting Denom change ***");
        _sb.AppendLine("     TerminalId: " + TerminalId.ToString());
        _sb.AppendLine("     Old SAS Accounting Denom: " + OldSASAccountDenom.ToString("0.00"));
        _sb.AppendLine("     New SAS Accounting Denom: " + NewSASAccountDenom.ToString("0.00"));
        Log.Warning(_sb.ToString());

        //Insert machine meters for SAS Accounting Denom change
        InsertIntoTSMHDenomChange(_old_machine_meters, _new_machine_meters, _delta_machine_meters, TerminalId, OldSASAccountDenom, Trx);

        //Reload datatable because ocurred a SAS Accounting Denom change 
        m_has_to_reload_dt = true;
      }

      return true;
    }
    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows based on Filter from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Filter
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static Boolean Unload(String Filter)
    {
      DataRow[] _term_rows;

      if (m_dt_machine_meters == null)
      {
        Log.Error("Unload: BU_MachineMeters not initialized. Call Init first!");
        return false;
      }

      _term_rows = m_dt_machine_meters.Select(Filter);
      foreach (DataRow _row in _term_rows)
      {
        m_dt_machine_meters.Rows.Remove(_row);
      }

      return true;
    } // Unload

    /// <summary>
    /// Get EGM SAS accounting denom
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SasAccountingDenom"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean GetEgmSasAccountingDenom(Int32 TerminalId,
                                                   out Decimal SasAccountingDenom,
                                                   SqlTransaction SqlTrx)
    {
      WSI.WCP.Terminal _terminal;
      _terminal = new WSI.WCP.Terminal();

      //Get terminal info
      if (!_terminal.GetEgmSasAcountDenom(TerminalId,
                                          out SasAccountingDenom,
                                          SqlTrx))
      {
        //If termminal is not found
        SasAccountingDenom = DEFAULT_SAS_ACCOUNTING_DENOM;  //Default value
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Process new wcp MachineMeters message
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SessionId
    //          - WcpSequenceId
    //          - NewMachineMeters: Wcp message
    //          - WcpTaskStatus
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: update machine meters in memory correctly.
    //      - false: otherwise.
    //
    //   NOTES :
    //
    private static Boolean BU_ProcessMachineMeters(Int32 TerminalId,
                                                   Int64 SessionId,
                                                   Int64 WcpSequenceId,
                                                   WCP_MsgReportMachineMeters NewMachineMeters,
                                                   WSI.WCP.IWcpTaskStatus WcpTaskStatus,
                                                   SqlTransaction Trx)
    {
      Int64 _db_wcp_sequence_id;

      MachineMeters _new_machine_meters;
      MachineMeters _old_machine_meters;
      MachineMeters _delta_machine_meters;
      MachineMeters _zero_machine_meters;
      DataRow _row_machine_meters;
      String _terminal_name;
      String _provider_id;
      Boolean _big_increment;
      Int64 _decrement;
      Decimal _terminal_SAS_account_denom;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_select;
      long _interval_update;
      long _interval_all_process;

      _interval_select = 0;
      _interval_update = 0;
      _interval_all_process = 0;
      _tick0 = Environment.TickCount;

      _zero_machine_meters = new MachineMeters();

      try
      {
        // Get wcp message machine meters
        GetMsgReportMachineMeters(NewMachineMeters, out _new_machine_meters);

        _tick1 = Environment.TickCount;

        // Get database Machine Meters
        if (!Get(TerminalId, out _old_machine_meters, out _db_wcp_sequence_id, out _row_machine_meters, Trx))
        {
          if (!Common.BatchUpdate.TerminalSession.GetData(TerminalId, SessionId, out _provider_id, out _terminal_name))
          {
            Log.Warning("BU MachineMeters, TerminalSession Not Active. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

            return false;
          }

          //Get terminal sas account denom
          if (!GetTerminalSASAccountingDenom(TerminalId, _row_machine_meters, out _terminal_SAS_account_denom, Trx))
          {
            Log.Warning("BU MachineMeters, GetTerminalSASAccountingDenom terminal not found. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

            return false;
          }

          // Insert Machine Meters
          if (!Insert(TerminalId, _provider_id, _terminal_name, WcpSequenceId, _new_machine_meters, _terminal_SAS_account_denom, WcpTaskStatus))
          {
            Log.Warning("BU MachineMeters, Insert failed. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

            return false;
          }

          return true;
        }

        _tick2 = Environment.TickCount;
        _interval_select = Misc.GetElapsedTicks(_tick1, _tick2);

        if (_db_wcp_sequence_id >= WcpSequenceId)
        {
          // Message arrived is previous than message in DB.
          // Don't treat it, but mark Task as OK and return.
          WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

          return true;
        }

        //
        // Generate Events for External protocol
        //
        if (!PeruEvents.InsertMachineMetersEvent(TerminalId, _old_machine_meters, _new_machine_meters))
        {
          Log.Error("InsertMachineMetersEvent: failed.");
        }

        if (!ColJuegos.InsertColJuegosMachineMetersEvent(TerminalId, _old_machine_meters, _new_machine_meters))
        {
          Log.Error("InsertColJuegosMachineMetersEvent: failed.");
        }

        if (!GeneralParam.GetBoolean("Alarms", "Terminal.DailyUnreportedMeters"))
        {
          if (_new_machine_meters.IsEqual(_old_machine_meters))
          {
            // No changed in MachineMeters counters. Mark Task as OK and return.
            WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

            return true;
          }
        }

        _delta_machine_meters = new MachineMeters();
        _big_increment = false;

        if (!NewMachineMeters.IgnoreDeltas)
        {
          GetDeltaMachineMetersPlayedWon(TerminalId, _old_machine_meters, _new_machine_meters, ref _delta_machine_meters, ref _big_increment, Trx);
        }

        GetDeltaMachineMetersAftFundTransfer(TerminalId, _old_machine_meters, _new_machine_meters, ref _delta_machine_meters, ref _big_increment, Trx);
        GetDeltaMachineMetersEftFundTransfer(TerminalId, _old_machine_meters, _new_machine_meters, ref _delta_machine_meters, ref _big_increment, Trx);

        if (_big_increment)
        {
          // Went PlayedQuantity back?
          if (_new_machine_meters.PlayWonAvalaible)
          {
            if (_old_machine_meters.PlayedQuantity > _new_machine_meters.PlayedQuantity)
            {
              // No update machine meters if played counter back 5 or less units
              if (_old_machine_meters.PlayedQuantity - _new_machine_meters.PlayedQuantity <= 5)
              {
                _decrement = _old_machine_meters.PlayedQuantity - _new_machine_meters.PlayedQuantity;

                Log.Message("*** Machine Meters: PlayedQuantity went back: " + _decrement.ToString() + " units. Machine Meters NOT updated for Terminal: " + TerminalId.ToString() + ".");

                // No update MachineMeters. Mark Task as OK and return.
                WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

                return true;
              }
            }
          }

          // Went AftToGmQuantity back?
          if (_new_machine_meters.AftFundTransferAvalaible)
          {
            if (_old_machine_meters.AftToGmQuantity > _new_machine_meters.AftToGmQuantity)
            {
              // No update machine meters if played counter back 5 or less units
              if (_old_machine_meters.AftToGmQuantity - _new_machine_meters.AftToGmQuantity <= 5)
              {
                _decrement = _old_machine_meters.AftToGmQuantity - _new_machine_meters.AftToGmQuantity;

                Log.Message("*** Machine Meters: AftToGmQuantity went back: " + _decrement.ToString() + " units. Machine Meters NOT updated for Terminal: " + TerminalId.ToString() + ".");

                // No update MachineMeters. Mark Task as OK and return.
                WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

                return true;
              }
            }
          }

          // Went EftToGmQuantity back?
          if (_new_machine_meters.EftFundTransferAvalaible)
          {
            if (_old_machine_meters.EftToGmQuantity > _new_machine_meters.EftToGmQuantity)
            {
              // No update machine meters if played counter back 5 or less units
              if (_old_machine_meters.EftToGmQuantity - _new_machine_meters.EftToGmQuantity <= 5)
              {
                _decrement = _old_machine_meters.EftToGmQuantity - _new_machine_meters.EftToGmQuantity;

                Log.Message("*** Machine Meters: EftToGmQuantity went back: " + _decrement.ToString() + " units. Machine Meters NOT updated for Terminal: " + TerminalId.ToString() + ".");

                // No update MachineMeters. Mark Task as OK and return.
                WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

                return true;
              }
            }
          }
        }

        _tick1 = Environment.TickCount;

        // Update Machine Meters
        if (!Update(TerminalId, WcpSequenceId, _new_machine_meters, _old_machine_meters, _delta_machine_meters, _row_machine_meters, WcpTaskStatus))
        {
          Log.Warning("BU Updating Machine Meters. TerminalId: " + TerminalId.ToString() + ", WcpSequenceId: " + WcpSequenceId.ToString());

          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update = Misc.GetElapsedTicks(_tick1, _tick2);
        _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);

        // Don't mark Task ResponseCode yet.
        // There are modifications in MachineMeters. Wait for UpdateDB() to do it.

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_interval_select > 300
            || _interval_update > 300
            || _interval_all_process > 600)
        {
          Log.Warning("BU Terminal: " + TerminalId.ToString() + ". Message MachineMeters times: Select/Update/Total = " + _interval_select.ToString() + "/" + _interval_update.ToString() + "/" + _interval_all_process.ToString());
        }
      }
    } // BU_ProcessMachineMeters

    /// <summary>
    /// Get terminal SAS accounting dneom
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="RowMachineMeters"></param>
    /// <param name="TerminalSASAccountDenom"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean GetTerminalSASAccountingDenom(Int32 TerminalId, DataRow RowMachineMeters, out Decimal TerminalSASAccountDenom, SqlTransaction Trx)
    {
      if (RowMachineMeters != null)
      {
        if (!RowMachineMeters.IsNull(COLUMN_TERMINAL_SAS_ACCOUNTING_DENOM))
        {
          TerminalSASAccountDenom = (decimal)RowMachineMeters[COLUMN_TERMINAL_SAS_ACCOUNTING_DENOM];

          return true;
        }
      }

      return (GetEgmSasAccountingDenom(TerminalId, out TerminalSASAccountDenom, Trx));
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Copy Wcp Msg Machine Meters in MachineMeters class
    //
    //  PARAMS :
    //      - INPUT :
    //          - WcpMsgMachineMeters: Wcp report message 
    //
    //      - OUTPUT :
    //          - NewMachineMeters
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GetMsgReportMachineMeters(WCP_MsgReportMachineMeters WcpMsgMachineMeters, out MachineMeters NewMachineMeters)
    {
      NewMachineMeters = new MachineMeters();

      NewMachineMeters.PlayWonAvalaible = WcpMsgMachineMeters.PlayWonAvalaible;

      if (WcpMsgMachineMeters.PlayWonAvalaible)
      {
        NewMachineMeters.PlayedQuantity = WcpMsgMachineMeters.PlayedQuantity;
        NewMachineMeters.PlayedMaxValueQuantity = WcpMsgMachineMeters.PlayedMaxValueQuantity;
        NewMachineMeters.PlayedCents = WcpMsgMachineMeters.PlayedCents;
        NewMachineMeters.PlayedMaxValueCents = WcpMsgMachineMeters.PlayedMaxValueCents;
        NewMachineMeters.WonQuantity = WcpMsgMachineMeters.WonQuantity;
        NewMachineMeters.WonMaxValueQuantity = WcpMsgMachineMeters.WonMaxValueQuantity;
        NewMachineMeters.WonCents = WcpMsgMachineMeters.WonCents;
        NewMachineMeters.WonMaxValueCents = WcpMsgMachineMeters.WonMaxValueCents;
        // ACC 15-JUN-2012 Add Jackpot Cents into Machine Meters
        NewMachineMeters.JackpotCents = WcpMsgMachineMeters.JackpotCents;
        NewMachineMeters.JackpotMaxValueCents = WcpMsgMachineMeters.JackpotMaxValueCents;
        // ACC 05-AUG-2014 Add ProgressiveJackpot Cents into Machine Meters
        NewMachineMeters.ProgressiveJackpotCents = WcpMsgMachineMeters.ProgressiveJackpotCents;
        NewMachineMeters.ProgressiveJackpotMaxValueCents = WcpMsgMachineMeters.ProgressiveJackpotMaxValueCents;
      }

      NewMachineMeters.AftFundTransferAvalaible = false;
      if (WcpMsgMachineMeters.FundTransferType == WCP_FundTransferType.FUND_TRANSFER_AFT)
      {
        NewMachineMeters.AftFundTransferAvalaible = true;

        NewMachineMeters.AftToGmQuantity = WcpMsgMachineMeters.ToGmQuantity;
        NewMachineMeters.AftToGmMaxValueQuantity = WcpMsgMachineMeters.ToGmMaxValueQuantity;
        NewMachineMeters.AftToGmCents = WcpMsgMachineMeters.ToGmCents;
        NewMachineMeters.AftToGmMaxValueCents = WcpMsgMachineMeters.ToGmMaxValueCents;
        NewMachineMeters.AftFromGmQuantity = WcpMsgMachineMeters.FromGmQuantity;
        NewMachineMeters.AftFromGmMaxValueQuantity = WcpMsgMachineMeters.FromGmMaxValueQuantity;
        NewMachineMeters.AftFromGmCents = WcpMsgMachineMeters.FromGmCents;
        NewMachineMeters.AftFromGmMaxValueCents = WcpMsgMachineMeters.FromGmMaxValueCents;
      }

      NewMachineMeters.EftFundTransferAvalaible = false;
      if (WcpMsgMachineMeters.FundTransferType == WCP_FundTransferType.FUND_TRANSFER_EFT)
      {
        NewMachineMeters.EftFundTransferAvalaible = true;

        NewMachineMeters.EftToGmQuantity = WcpMsgMachineMeters.ToGmQuantity;
        NewMachineMeters.EftToGmMaxValueQuantity = WcpMsgMachineMeters.ToGmMaxValueQuantity;
        NewMachineMeters.EftToGmCents = WcpMsgMachineMeters.ToGmCents;
        NewMachineMeters.EftToGmMaxValueCents = WcpMsgMachineMeters.ToGmMaxValueCents;
        NewMachineMeters.EftFromGmQuantity = WcpMsgMachineMeters.FromGmQuantity;
        NewMachineMeters.EftFromGmMaxValueQuantity = WcpMsgMachineMeters.FromGmMaxValueQuantity;
        NewMachineMeters.EftFromGmCents = WcpMsgMachineMeters.FromGmCents;
        NewMachineMeters.EftFromGmMaxValueCents = WcpMsgMachineMeters.FromGmMaxValueCents;
      }

    } // GetMsgReportMachineMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Get Delta all PlayedWon Machine Meters 
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - OldMachineMeters
    //          - NewMachineMeters
    //
    //      - OUTPUT :
    //          - DeltaMachineMeters
    //          - BigIncrement
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GetDeltaMachineMetersPlayedWon(Int32 TerminalId, MachineMeters OldMachineMeters, MachineMeters NewMachineMeters,
                                                       ref MachineMeters DeltaMachineMeters, ref Boolean BigIncrement,
                                                       SqlTransaction Trx)
    {
      Boolean _big;
      Boolean _big_played_quantity;
      Boolean _big_played_cents;
      Boolean _big_won_quantity;
      Boolean _big_won_cents;
      Boolean _big_jackpot_cents;
      Boolean _big_progressive_jackpot_cents;

      String _terminal_name;
      String _provider_id;
      long _sesion_id;
      Boolean _new_is_zero;
      Boolean _old_is_zero;
      DateTime _now = WGDB.Now;

      MachineMeters _delta_machine_meters_ajustments;

      _big_played_quantity = false;
      _big_played_cents = false;
      _big_won_quantity = false;
      _big_won_cents = false;
      _big_jackpot_cents = false;
      _big_progressive_jackpot_cents = false;

      if (!NewMachineMeters.PlayWonAvalaible
          || !OldMachineMeters.PlayWonAvalaible)
      {
        return;
      }

      //
      // Value -1 indicates not existing meter
      //
      _new_is_zero = (NewMachineMeters.PlayedCents == 0
                      && NewMachineMeters.PlayedQuantity == 0
                      && NewMachineMeters.WonCents == 0
                      && NewMachineMeters.WonQuantity == 0
                      && (NewMachineMeters.JackpotCents == 0 || NewMachineMeters.JackpotCents == -1)); // Value -1 indicates not existing meter

      _old_is_zero = (OldMachineMeters.PlayedCents == 0
                      && OldMachineMeters.PlayedQuantity == 0
                      && OldMachineMeters.WonCents == 0
                      && OldMachineMeters.WonQuantity == 0
                      && (OldMachineMeters.JackpotCents == 0 || OldMachineMeters.JackpotCents == -1)); // Value -1 indicates not existing meter

      if (_new_is_zero && !_old_is_zero)
      {
        //Machine Meters Reset
        Log.Warning("*** PlayedWon Machine Meters Reset *** TerminalId: " + TerminalId.ToString() +
                    ". Old meter values:" +
                    "  Played Count/Cents: " + OldMachineMeters.PlayedQuantity.ToString() + " / " + OldMachineMeters.PlayedCents.ToString() +
                    ", Won Count/Cents: " + OldMachineMeters.WonQuantity.ToString() + " / " + OldMachineMeters.WonCents.ToString() +
                    ", Jackpot Cents: " + OldMachineMeters.JackpotCents.ToString());

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.AppendLine("*** PlayedWon Machine Meters Reset ***");
        _sb.AppendLine(" Old meter values:");
        _sb.AppendLine("     Played Count/Cents: " + OldMachineMeters.PlayedQuantity.ToString() + " / " + OldMachineMeters.PlayedCents.ToString());
        _sb.AppendLine("     Won Count/Cents: " + OldMachineMeters.WonQuantity.ToString() + " / " + OldMachineMeters.WonCents.ToString());
        _sb.AppendLine("     Jackpot Cents: " + OldMachineMeters.JackpotCents.ToString());

        Alarm.Register(AlarmSourceCode.TerminalSystem,
                        TerminalId,
                        Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                        AlarmCode.TerminalSystem_MachineMeterReset,
                       _sb.ToString());

        InsertSasMetersAdjustmentsReset(TerminalId, OldMachineMeters, NewMachineMeters, DeltaMachineMeters);

        //FGB 16-JUN-2016 
        InsertIntoTSMHServiceRAMClear(OldMachineMeters, NewMachineMeters, DeltaMachineMeters, TerminalId, Trx);

        return;
      }

      _big_played_quantity = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachinePlayedQuantity", OldMachineMeters.PlayedQuantity, NewMachineMeters.PlayedQuantity, OldMachineMeters.PlayedMaxValueQuantity, NewMachineMeters.PlayedMaxValueQuantity, BigIncrementsData.QuantityGamesPlayed(TerminalId), out DeltaMachineMeters.PlayedQuantity);
      _big_played_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachinePlayedCents", OldMachineMeters.PlayedCents, NewMachineMeters.PlayedCents, OldMachineMeters.PlayedMaxValueCents, NewMachineMeters.PlayedMaxValueCents, BigIncrementsData.CentsPlayed(TerminalId), out DeltaMachineMeters.PlayedCents);
      _big_won_quantity = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachineWonQuantity", OldMachineMeters.WonQuantity, NewMachineMeters.WonQuantity, OldMachineMeters.WonMaxValueQuantity, NewMachineMeters.WonMaxValueQuantity, BigIncrementsData.QuantityGamesWon(TerminalId), out DeltaMachineMeters.WonQuantity);
      _big_won_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachineWonCents", OldMachineMeters.WonCents, NewMachineMeters.WonCents, OldMachineMeters.WonMaxValueCents, NewMachineMeters.WonMaxValueCents, BigIncrementsData.CentsWon(TerminalId), out DeltaMachineMeters.WonCents);

      // ACC 15-JUN-2012 Add Jackpot Cents into WonCents      
      if (OldMachineMeters.JackpotCents != -1)
      {
        _big_jackpot_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachineJackpotCents", OldMachineMeters.JackpotCents, NewMachineMeters.JackpotCents, OldMachineMeters.JackpotMaxValueCents, NewMachineMeters.JackpotMaxValueCents, BigIncrementsData.CentsJackpot(TerminalId), out DeltaMachineMeters.JackpotCents);

        // ACC 09-OCT-2014 
        //  --> This increment is done in machine meters query (MM_DELTA_WON_AMOUNT + @pDeltaWonAmount + @pDeltaJackpotAmount)
        // DeltaMachineMeters.WonCents += DeltaMachineMeters.JackpotCents;
      }

      // ACC 05-AUG-2014 Progressive Jackpot Cents 
      if (OldMachineMeters.ProgressiveJackpotCents != -1)
      {
        _big_progressive_jackpot_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "MachineProgressiveJackpotCents", OldMachineMeters.ProgressiveJackpotCents, NewMachineMeters.ProgressiveJackpotCents, OldMachineMeters.ProgressiveJackpotMaxValueCents, NewMachineMeters.ProgressiveJackpotMaxValueCents, BigIncrementsData.CentsJackpot(TerminalId), out DeltaMachineMeters.ProgressiveJackpotCents);
      }

      _big = false;
      _big = _big || _big_played_quantity;
      _big = _big || _big_played_cents;
      _big = _big || _big_won_quantity;
      _big = _big || _big_won_cents;
      _big = _big || _big_jackpot_cents;
      _big = _big || _big_progressive_jackpot_cents;

      // Get "delta" for register adjusments
      _delta_machine_meters_ajustments = new MachineMeters();
      _delta_machine_meters_ajustments.PlayedQuantity = DeltaMachineMeters.PlayedQuantity;
      _delta_machine_meters_ajustments.PlayedCents = DeltaMachineMeters.PlayedCents;
      _delta_machine_meters_ajustments.WonQuantity = DeltaMachineMeters.WonQuantity;
      _delta_machine_meters_ajustments.WonCents = DeltaMachineMeters.WonCents;
      _delta_machine_meters_ajustments.JackpotCents = DeltaMachineMeters.JackpotCents;
      _delta_machine_meters_ajustments.ProgressiveJackpotCents = DeltaMachineMeters.ProgressiveJackpotCents;

      if (_big)
      {
        //Machine Meters BigIncrement
        BigIncrement = true;

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

        String _alarm_name;
        _alarm_name = "*** Machine Meters BigIncrement ***";

        if (_big_played_quantity)
        {
          DeltaMachineMeters.PlayedQuantity = 0;
          Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_MachineMeterBigIncrement_ByMeter, 0x0005, TerminalId, _provider_id, _terminal_name, OldMachineMeters.PlayedQuantity, NewMachineMeters.PlayedQuantity);
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                            , TerminalId
                                            , 0x0005
                                            , 0
                                            , 0
                                            , WGDB.Now
                                            , TerminalMeterGroup.MetersAdjustmentsType.System
                                            , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_BigIncrement
                                            , OldMachineMeters.PlayedQuantity
                                            , NewMachineMeters.PlayedQuantity
                                            , _delta_machine_meters_ajustments.PlayedQuantity
                                            , "Big Increment: Machine Played Quantity");
        }

        if (_big_played_cents)
        {
          DeltaMachineMeters.PlayedCents = 0;
          Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_MachineMeterBigIncrement_ByMeter, 0x0000, TerminalId, _provider_id, _terminal_name, OldMachineMeters.PlayedCents, NewMachineMeters.PlayedCents);
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                            , TerminalId
                                            , 0x0000
                                            , 0
                                            , 0
                                            , WGDB.Now
                                            , TerminalMeterGroup.MetersAdjustmentsType.System
                                            , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_BigIncrement
                                            , OldMachineMeters.PlayedCents
                                            , NewMachineMeters.PlayedCents
                                            , _delta_machine_meters_ajustments.PlayedCents
                                            , "Big Increment: Machine Played Cents");
        }

        if (_big_won_quantity)
        {
          DeltaMachineMeters.WonQuantity = 0;
          Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_MachineMeterBigIncrement_ByMeter, 0x0006, TerminalId, _provider_id, _terminal_name, OldMachineMeters.WonQuantity, NewMachineMeters.WonQuantity);
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                            , TerminalId
                                            , 0x0006
                                            , 0
                                            , 0
                                            , WGDB.Now
                                            , TerminalMeterGroup.MetersAdjustmentsType.System
                                            , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_BigIncrement
                                            , OldMachineMeters.WonQuantity
                                            , NewMachineMeters.WonQuantity
                                            , _delta_machine_meters_ajustments.WonQuantity
                                            , "Big Increment: Machine Won Quantity");
        }

        if (_big_won_cents)
        {
          DeltaMachineMeters.WonCents = 0;
          Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_MachineMeterBigIncrement_ByMeter, 0x0001, TerminalId, _provider_id, _terminal_name, OldMachineMeters.WonCents, NewMachineMeters.WonCents);
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                            , TerminalId
                                            , 0x0001
                                            , 0
                                            , 0
                                            , WGDB.Now
                                            , TerminalMeterGroup.MetersAdjustmentsType.System
                                            , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_BigIncrement
                                            , OldMachineMeters.WonCents
                                            , NewMachineMeters.WonCents
                                            , _delta_machine_meters_ajustments.WonCents
                                            , "Big Increment: Machine Won Cents");
        }

        if (_big_jackpot_cents)
        {
          DeltaMachineMeters.JackpotCents = 0;
          Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_MachineMeterBigIncrement_ByMeter, 0x0002, TerminalId, _provider_id, _terminal_name, OldMachineMeters.JackpotCents, NewMachineMeters.JackpotCents);
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                            , TerminalId
                                            , 0x0002
                                            , 0
                                            , 0
                                            , WGDB.Now
                                            , TerminalMeterGroup.MetersAdjustmentsType.System
                                            , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_BigIncrement
                                            , OldMachineMeters.JackpotCents
                                            , NewMachineMeters.JackpotCents
                                            , _delta_machine_meters_ajustments.JackpotCents
                                            , "Big Increment: Machine Jackpot Cents");
        }

        if (_big_progressive_jackpot_cents)
        {
          DeltaMachineMeters.ProgressiveJackpotCents = 0;
          Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_MachineMeterBigIncrement_ByMeter, 0x1000, TerminalId, _provider_id, _terminal_name, OldMachineMeters.ProgressiveJackpotCents, NewMachineMeters.ProgressiveJackpotCents);
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                            , TerminalId
                                            , 0x1000
                                            , 0
                                            , 0
                                            , WGDB.Now
                                            , TerminalMeterGroup.MetersAdjustmentsType.System
                                            , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_BigIncrement
                                            , OldMachineMeters.ProgressiveJackpotCents
                                            , NewMachineMeters.ProgressiveJackpotCents
                                            , _delta_machine_meters_ajustments.ProgressiveJackpotCents
                                            , "Big Increment: Machine Progressive Jackpot Cents");
        }

        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.AppendLine("*** PlayedWon Machine Meters BigIncrement ***");
        _sb.AppendLine(" Old meter values:");
        _sb.AppendLine("     Played Count/Cents: " + OldMachineMeters.PlayedQuantity.ToString() + " / " + OldMachineMeters.PlayedCents.ToString());
        _sb.AppendLine("     Won Count/Cents: " + OldMachineMeters.WonQuantity.ToString() + " / " + OldMachineMeters.WonCents.ToString());
        _sb.AppendLine(" New meter values:");
        _sb.AppendLine("     Played Count/Cents: " + NewMachineMeters.PlayedQuantity.ToString() + " / " + NewMachineMeters.PlayedCents.ToString());
        _sb.AppendLine("     Won Count/Cents: " + NewMachineMeters.WonQuantity.ToString() + " / " + NewMachineMeters.WonCents.ToString());

        Alarm.Register(AlarmSourceCode.TerminalSystem,
                        TerminalId,
                        Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                        AlarmCode.TerminalSystem_MachineMeterBigIncrement,
                        _sb.ToString());
      }

      //28-01-2015 SGB Alarm Rollover
      if (NewMachineMeters.PlayedQuantity < OldMachineMeters.PlayedQuantity ||
          NewMachineMeters.WonQuantity < OldMachineMeters.WonQuantity ||
          NewMachineMeters.PlayedCents < OldMachineMeters.PlayedCents ||
          NewMachineMeters.WonCents < OldMachineMeters.WonCents ||
          NewMachineMeters.JackpotCents < OldMachineMeters.JackpotCents ||
          (NewMachineMeters.ProgressiveJackpotCents < OldMachineMeters.ProgressiveJackpotCents
            && NewMachineMeters.ProgressiveJackpotMaxValueCents != -1
            && NewMachineMeters.ProgressiveJackpotMaxValueCents != 0))
      {
        //Machine Meters Rollover
        StringBuilder _sb;

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

        _sb = new StringBuilder();
        _sb.AppendLine("*** PlayedWon Machine Meters Rollover ***");
        _sb.AppendLine(" Old meter values:");
        _sb.AppendLine("     Played Count/Cents: " + OldMachineMeters.PlayedQuantity.ToString() + " / " + OldMachineMeters.PlayedCents.ToString());
        _sb.AppendLine("     Won Count/Cents: " + OldMachineMeters.WonQuantity.ToString() + " / " + OldMachineMeters.WonCents.ToString());
        if (OldMachineMeters.JackpotCents != -1)
        {
          _sb.AppendLine("     Jackpot Cents: " + OldMachineMeters.JackpotCents.ToString());
        }
        if (OldMachineMeters.ProgressiveJackpotCents != -1)
        {
          _sb.AppendLine("     Progressive Jackpot: " + OldMachineMeters.ProgressiveJackpotCents.ToString());
        }

        _sb.AppendLine(" New meter values:");
        _sb.AppendLine("     Played Count/Cents: " + NewMachineMeters.PlayedQuantity.ToString() + " / " + NewMachineMeters.PlayedCents.ToString());
        _sb.AppendLine("     Won Count/Cents: " + NewMachineMeters.WonQuantity.ToString() + " / " + NewMachineMeters.WonCents.ToString());

        if (NewMachineMeters.JackpotCents != -1)
        {
          _sb.AppendLine("     Jackpot Cents: " + NewMachineMeters.JackpotCents.ToString());
        }
        if (NewMachineMeters.ProgressiveJackpotCents != -1)
        {
          _sb.AppendLine("     Progressive Jackpot: " + NewMachineMeters.ProgressiveJackpotCents.ToString());
        }

        Alarm.Register(AlarmSourceCode.TerminalSystem,
                       TerminalId,
                       Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                       (UInt32)AlarmCode.TerminalSystem_MeterRollover,
                       _sb.ToString(),
                       AlarmSeverity.Info,
                       _now,
                       Trx);

        InsertIntoTSMHRollover(OldMachineMeters, NewMachineMeters, DeltaMachineMeters, TerminalId, Trx);

        InsertSasMetersAdjustmentsRollover(TerminalId, OldMachineMeters, NewMachineMeters, _delta_machine_meters_ajustments);
      }
    } // GetDeltaMachineMetersPlayedWon

    /// <summary>
    /// Insert SAS Meters Adjustments 
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="OldMachineMeters"></param>
    /// <param name="NewMachineMeters"></param>
    /// <param name="DeltaMachineMeters"></param>
    private static void InsertSasMetersAdjustmentsReset(Int32 TerminalId, MachineMeters OldMachineMeters, MachineMeters NewMachineMeters, MachineMeters DeltaMachineMeters)
    {
      if (NewMachineMeters.PlayedQuantity == 0 && NewMachineMeters.PlayedQuantity != OldMachineMeters.PlayedQuantity)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0005
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                  , OldMachineMeters.PlayedQuantity
                                                  , NewMachineMeters.PlayedQuantity
                                                  , DeltaMachineMeters.PlayedQuantity
                                                  , "Reset: Machine Played Quantity");
      }

      if (NewMachineMeters.PlayedCents == 0 && NewMachineMeters.PlayedCents != OldMachineMeters.PlayedCents)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0000
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                  , OldMachineMeters.PlayedCents
                                                  , NewMachineMeters.PlayedCents
                                                  , DeltaMachineMeters.PlayedCents
                                                  , "Reset: Machine Played Cents");
      }

      if (NewMachineMeters.WonQuantity == 0 && NewMachineMeters.WonQuantity != OldMachineMeters.WonQuantity)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0006
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                  , OldMachineMeters.WonQuantity
                                                  , NewMachineMeters.WonQuantity
                                                  , DeltaMachineMeters.WonQuantity
                                                  , "Reset: Machine Won Quantity");
      }

      if (NewMachineMeters.WonCents == 0 && NewMachineMeters.WonCents != OldMachineMeters.WonCents)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0001
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                  , OldMachineMeters.WonCents
                                                  , NewMachineMeters.WonCents
                                                  , DeltaMachineMeters.WonCents
                                                  , "Reset: Machine Won Cents");
      }

      if ((NewMachineMeters.JackpotCents == 0 || NewMachineMeters.JackpotCents == -1) && NewMachineMeters.JackpotCents != OldMachineMeters.JackpotCents)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0002
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                  , OldMachineMeters.JackpotCents
                                                  , NewMachineMeters.JackpotCents
                                                  , DeltaMachineMeters.JackpotCents
                                                  , "Reset: Machine Jackpot Cents");
      }
    }

    /// <summary>
    /// Insert SAS Meters Adjustments for a Rollover    
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="OldMachineMeters"></param>
    /// <param name="NewMachineMeters"></param>
    /// <param name="DeltaMachineMetersAdjustments"></param>
    private static void InsertSasMetersAdjustmentsRollover(Int32 TerminalId, MachineMeters OldMachineMeters, MachineMeters NewMachineMeters, MachineMeters DeltaMachineMetersAdjustments)
    {
      if (NewMachineMeters.PlayedQuantity < OldMachineMeters.PlayedQuantity)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0005
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Rollover
                                                  , OldMachineMeters.PlayedQuantity
                                                  , NewMachineMeters.PlayedQuantity
                                                  , DeltaMachineMetersAdjustments.PlayedQuantity
                                                  , "Rollover: Machine Played Quantity");
      }

      if (NewMachineMeters.WonQuantity < OldMachineMeters.WonQuantity)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0006
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Rollover
                                                  , OldMachineMeters.WonQuantity
                                                  , NewMachineMeters.WonQuantity
                                                  , DeltaMachineMetersAdjustments.WonQuantity
                                                  , "Rollover: Machine Won Quantity");
      }

      if (NewMachineMeters.PlayedCents < OldMachineMeters.PlayedCents)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0000
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Rollover
                                                  , OldMachineMeters.PlayedCents
                                                  , NewMachineMeters.PlayedCents
                                                  , DeltaMachineMetersAdjustments.PlayedCents
                                                  , "Rollover: Machine Played Cents");
      }

      if (NewMachineMeters.WonCents < OldMachineMeters.WonCents)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0001
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Rollover
                                                  , OldMachineMeters.WonCents
                                                  , NewMachineMeters.WonCents
                                                  , DeltaMachineMetersAdjustments.WonCents
                                                  , "Rollover: Machine Won Cents");
      }

      if (NewMachineMeters.JackpotCents < OldMachineMeters.JackpotCents)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0002
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Rollover
                                                  , OldMachineMeters.JackpotCents
                                                  , NewMachineMeters.JackpotCents
                                                  , DeltaMachineMetersAdjustments.JackpotCents
                                                  , "Rollover: Machine Jackpot Cents");
      }

      if (NewMachineMeters.ProgressiveJackpotCents < OldMachineMeters.ProgressiveJackpotCents)
      {
        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x1000
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Rollover
                                                  , OldMachineMeters.ProgressiveJackpotCents
                                                  , NewMachineMeters.ProgressiveJackpotCents
                                                  , DeltaMachineMetersAdjustments.ProgressiveJackpotCents
                                                  , "Rollover: Machine Progressive Jackpot Cents");
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get Delta all AFT FundTransfer Machine Meters 
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - OldMachineMeters
    //          - NewMachineMeters
    //
    //      - OUTPUT :
    //          - DeltaMachineMeters
    //          - BigIncrement
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GetDeltaMachineMetersAftFundTransfer(Int32 TerminalId, MachineMeters OldMachineMeters, MachineMeters NewMachineMeters, ref MachineMeters DeltaMachineMeters, ref Boolean BigIncrement, SqlTransaction Trx)
    {
      Boolean _big;
      String _terminal_name;
      String _provider_id;
      long _sesion_id;
      Boolean _new_is_zero;
      Boolean _old_is_zero;
      DateTime _now = WGDB.Now;

      if (!NewMachineMeters.AftFundTransferAvalaible
          || !OldMachineMeters.AftFundTransferAvalaible)
      {
        return;
      }

      _new_is_zero = (NewMachineMeters.AftToGmCents == 0
                      && NewMachineMeters.AftToGmQuantity == 0
                      && NewMachineMeters.AftFromGmCents == 0
                      && NewMachineMeters.AftFromGmQuantity == 0);

      _old_is_zero = (OldMachineMeters.AftToGmCents == 0
                      && OldMachineMeters.AftToGmQuantity == 0
                      && OldMachineMeters.AftFromGmCents == 0
                      && OldMachineMeters.AftFromGmQuantity == 0);

      if (_new_is_zero && !_old_is_zero)
      {
        Log.Warning("*** FundTransfer AFT Machine Meters Reset *** TerminalId: " + TerminalId.ToString() +
                    ". Old meter values:" +
                    "  ToGm Quantity/Cents: " + OldMachineMeters.AftToGmQuantity.ToString() + " / " + OldMachineMeters.AftToGmCents.ToString() +
                    ", FromGm Quantity/Cents: " + OldMachineMeters.AftFromGmQuantity.ToString() + " / " + OldMachineMeters.AftFromGmCents.ToString());

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.AppendLine("*** FundTransfer AFT Machine Meters Reset ***");
        _sb.AppendLine(" Old meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + OldMachineMeters.AftToGmQuantity.ToString() + " / " + OldMachineMeters.AftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + OldMachineMeters.AftFromGmQuantity.ToString() + " / " + OldMachineMeters.AftFromGmCents.ToString());

        Alarm.Register(AlarmSourceCode.TerminalSystem,
                        TerminalId,
                        Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                        AlarmCode.TerminalSystem_MachineMeterReset,
                        _sb.ToString());

        if (NewMachineMeters.AftToGmCents == 0 && NewMachineMeters.AftToGmCents != OldMachineMeters.AftToGmCents)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00A0
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.AftToGmCents
                                                    , NewMachineMeters.AftToGmCents
                                                    , DeltaMachineMeters.AftToGmCents
                                                    , "Reset: Machine AftToGm Cents");
        }

        if (NewMachineMeters.AftToGmQuantity == 0 && NewMachineMeters.AftToGmQuantity != OldMachineMeters.AftToGmQuantity)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00A1
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.AftToGmQuantity
                                                    , NewMachineMeters.AftToGmQuantity
                                                    , DeltaMachineMeters.AftToGmQuantity
                                                    , "Reset: Machine AftToGm Quantity");
        }

        if (NewMachineMeters.AftFromGmCents == 0 && NewMachineMeters.AftFromGmCents != OldMachineMeters.AftFromGmCents)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00B8
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.AftFromGmCents
                                                    , NewMachineMeters.AftFromGmCents
                                                    , DeltaMachineMeters.AftFromGmCents
                                                    , "Reset: Machine AftFromToGm Cents");
        }

        if (NewMachineMeters.AftFromGmQuantity == 0 && NewMachineMeters.AftFromGmQuantity != OldMachineMeters.AftFromGmQuantity)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00B9
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.AftFromGmQuantity
                                                    , NewMachineMeters.AftFromGmQuantity
                                                    , DeltaMachineMeters.AftFromGmQuantity
                                                    , "Reset: Machine AftFromToGm Quantity");
        }

        return;
      }

      _big = false;

      _big = _big || TerminalMeterGroup.GetDeltaMeter(TerminalId, "AftToGmQuantity", OldMachineMeters.AftToGmQuantity, NewMachineMeters.AftToGmQuantity, NewMachineMeters.AftToGmMaxValueQuantity, BigIncrementsData.QuantityFTToEgm(TerminalId), out DeltaMachineMeters.AftToGmQuantity);
      _big = _big || TerminalMeterGroup.GetDeltaMeter(TerminalId, "AftToGmCents", OldMachineMeters.AftToGmCents, NewMachineMeters.AftToGmCents, NewMachineMeters.AftToGmMaxValueCents, BigIncrementsData.CentsFTToEgm(TerminalId), out DeltaMachineMeters.AftToGmCents);
      _big = _big || TerminalMeterGroup.GetDeltaMeter(TerminalId, "AftFromGmQuantity", OldMachineMeters.AftFromGmQuantity, NewMachineMeters.AftFromGmQuantity, NewMachineMeters.AftFromGmMaxValueQuantity, BigIncrementsData.QuantityFTFromEgm(TerminalId), out DeltaMachineMeters.AftFromGmQuantity);
      _big = _big || TerminalMeterGroup.GetDeltaMeter(TerminalId, "AftFromGmCents", OldMachineMeters.AftFromGmCents, NewMachineMeters.AftFromGmCents, NewMachineMeters.AftFromGmMaxValueCents, BigIncrementsData.CentsFTFromEgm(TerminalId), out DeltaMachineMeters.AftFromGmCents);

      if (_big)
      {
        // Return all "delta" as zero
        DeltaMachineMeters = new MachineMeters();

        Log.Warning("*** FundTransfer AFT Machine Meters BigIncrement *** TerminalId: " + TerminalId.ToString() +
                    ". Old meter values:" +
                    "  ToGm Quantity/Cents: " + OldMachineMeters.AftToGmQuantity.ToString() + " / " + OldMachineMeters.AftToGmCents.ToString() +
                    ", FromGm Quantity/Cents: " + OldMachineMeters.AftFromGmQuantity.ToString() + " / " + OldMachineMeters.AftFromGmCents.ToString());

        Log.Warning("*** FundTransfer AFT Machine Meters BigIncrement *** TerminalId: " + TerminalId.ToString() +
                    ". New meter values:" +
                    "  ToGm Quantity/Cents: " + NewMachineMeters.AftToGmQuantity.ToString() + " / " + NewMachineMeters.AftToGmCents.ToString() +
                    ", FromGm Quantity/Cents: " + NewMachineMeters.AftFromGmQuantity.ToString() + " / " + NewMachineMeters.AftFromGmCents.ToString());

        BigIncrement = true;

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.AppendLine("*** FundTransfer AFT Machine Meters BigIncrement ***");
        _sb.AppendLine(" Old meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + OldMachineMeters.AftToGmQuantity.ToString() + " / " + OldMachineMeters.AftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + OldMachineMeters.AftFromGmQuantity.ToString() + " / " + OldMachineMeters.AftFromGmCents.ToString());
        _sb.AppendLine(" New meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + NewMachineMeters.AftToGmQuantity.ToString() + " / " + NewMachineMeters.AftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + NewMachineMeters.AftFromGmQuantity.ToString() + " / " + NewMachineMeters.AftFromGmCents.ToString());

        Alarm.Register(AlarmSourceCode.TerminalSystem,
                        TerminalId,
                        Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                        AlarmCode.TerminalSystem_MachineMeterBigIncrement,
                        _sb.ToString());
      }

      //28-01-2015 SGB Alarm Rollover
      if (NewMachineMeters.AftToGmQuantity < OldMachineMeters.AftToGmQuantity ||
          NewMachineMeters.AftFromGmQuantity < OldMachineMeters.AftFromGmQuantity ||
          NewMachineMeters.AftToGmCents < OldMachineMeters.AftToGmCents ||
          NewMachineMeters.AftFromGmCents < OldMachineMeters.AftFromGmCents)
      {
        StringBuilder _sb;

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

        _sb = new StringBuilder();
        _sb.AppendLine("*** FundTransfer AFT Machine Meters Rollover ***");
        _sb.AppendLine(" Old meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + OldMachineMeters.AftToGmQuantity.ToString() + " / " + OldMachineMeters.AftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + OldMachineMeters.AftFromGmQuantity.ToString() + " / " + OldMachineMeters.AftFromGmCents.ToString());
        _sb.AppendLine(" New meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + NewMachineMeters.AftToGmQuantity.ToString() + " / " + NewMachineMeters.AftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + NewMachineMeters.AftFromGmQuantity.ToString() + " / " + NewMachineMeters.AftFromGmCents.ToString());

        Alarm.Register(AlarmSourceCode.TerminalSystem,
                       TerminalId,
                       Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                       (UInt32)AlarmCode.TerminalSystem_MeterRollover,
                       _sb.ToString(),
                       AlarmSeverity.Info,
                       _now,
                       Trx);

        InsertIntoTSMHRollover(OldMachineMeters, NewMachineMeters, DeltaMachineMeters, TerminalId, Trx);

        if (NewMachineMeters.AftToGmQuantity < OldMachineMeters.AftToGmQuantity)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00A1
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.AftToGmQuantity
                                                    , NewMachineMeters.AftToGmQuantity
                                                    , DeltaMachineMeters.AftToGmQuantity
                                                    , "Rollover: Machine AftToGm Quantity");
        }

        if (NewMachineMeters.AftFromGmQuantity < OldMachineMeters.AftFromGmQuantity)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00B9
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.AftFromGmQuantity
                                                    , NewMachineMeters.AftFromGmQuantity
                                                    , DeltaMachineMeters.AftFromGmQuantity
                                                    , "Rollover: Machine AftFromGm Quantity");
        }

        if (NewMachineMeters.AftToGmCents < OldMachineMeters.AftToGmCents)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00A0
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.AftToGmCents
                                                    , NewMachineMeters.AftToGmCents
                                                    , DeltaMachineMeters.AftToGmCents
                                                    , "Rollover: Machine AftToGm Cents");
        }

        if (NewMachineMeters.AftFromGmCents < OldMachineMeters.AftFromGmCents)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00B8
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.AftFromGmCents
                                                    , NewMachineMeters.AftFromGmCents
                                                    , DeltaMachineMeters.AftFromGmCents
                                                    , "Rollover: Machine AftFromGm Cents");
        }
      }
    } // GetDeltaMachineMetersAftFundTransfer

    //------------------------------------------------------------------------------
    // PURPOSE : Get Delta all EFT FundTransfer Machine Meters 
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - OldMachineMeters
    //          - NewMachineMeters
    //
    //      - OUTPUT :
    //          - DeltaMachineMeters
    //          - BigIncrement
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GetDeltaMachineMetersEftFundTransfer(Int32 TerminalId, MachineMeters OldMachineMeters, MachineMeters NewMachineMeters, ref MachineMeters DeltaMachineMeters, ref Boolean BigIncrement, SqlTransaction Trx)
    {
      Boolean _big;
      String _terminal_name;
      String _provider_id;
      long _sesion_id;
      Boolean _new_is_zero;
      Boolean _old_is_zero;
      DateTime _now = WGDB.Now;

      if (!NewMachineMeters.EftFundTransferAvalaible
          || !OldMachineMeters.EftFundTransferAvalaible)
      {
        return;
      }

      _new_is_zero = (NewMachineMeters.EftToGmCents == 0
                      && NewMachineMeters.EftToGmQuantity == 0
                      && NewMachineMeters.EftFromGmCents == 0
                      && NewMachineMeters.EftFromGmQuantity == 0);

      _old_is_zero = (OldMachineMeters.EftToGmCents == 0
                      && OldMachineMeters.EftToGmQuantity == 0
                      && OldMachineMeters.EftFromGmCents == 0
                      && OldMachineMeters.EftFromGmQuantity == 0);

      if (_new_is_zero && !_old_is_zero)
      {
        Log.Warning("*** FundTransfer EFT Machine Meters Reset *** TerminalId: " + TerminalId.ToString() +
                    ". Old meter values:" +
                    "  ToGm Quantity/Cents: " + OldMachineMeters.EftToGmQuantity.ToString() + " / " + OldMachineMeters.EftToGmCents.ToString() +
                    ", FromGm Quantity/Cents: " + OldMachineMeters.EftFromGmQuantity.ToString() + " / " + OldMachineMeters.EftFromGmCents.ToString());

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.AppendLine("*** FundTransfer EFT Machine Meters Reset ***");
        _sb.AppendLine(" Old meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + OldMachineMeters.EftToGmQuantity.ToString() + " / " + OldMachineMeters.EftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + OldMachineMeters.EftFromGmQuantity.ToString() + " / " + OldMachineMeters.EftFromGmCents.ToString());

        Alarm.Register(AlarmSourceCode.TerminalSystem,
                        TerminalId,
                        Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                        AlarmCode.TerminalSystem_MachineMeterReset,
                        _sb.ToString());

        if (NewMachineMeters.EftToGmCents == 0 && NewMachineMeters.EftToGmCents != OldMachineMeters.EftToGmCents)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00A0
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.EftToGmCents
                                                    , NewMachineMeters.EftToGmCents
                                                    , DeltaMachineMeters.EftToGmCents
                                                    , "Reset: Machine EftToGm Cents");
        }

        if (NewMachineMeters.EftToGmQuantity == 0 && NewMachineMeters.EftToGmQuantity != OldMachineMeters.EftToGmQuantity)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00A1
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.EftToGmQuantity
                                                    , NewMachineMeters.EftToGmQuantity
                                                    , DeltaMachineMeters.EftToGmQuantity
                                                    , "Reset: Machine EftToGm Quantity");
        }

        if (NewMachineMeters.EftFromGmCents == 0 && NewMachineMeters.EftFromGmCents != OldMachineMeters.EftFromGmCents)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00B8
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.EftFromGmCents
                                                    , NewMachineMeters.EftFromGmCents
                                                    , DeltaMachineMeters.EftFromGmCents
                                                    , "Reset: Machine EftFromGm Cents");
        }

        if (NewMachineMeters.EftFromGmQuantity == 0 && NewMachineMeters.EftFromGmQuantity != OldMachineMeters.EftFromGmQuantity)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00B9
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.EftFromGmQuantity
                                                    , NewMachineMeters.EftFromGmQuantity
                                                    , DeltaMachineMeters.EftFromGmQuantity
                                                    , "Reset: Machine EftFromGm Quantity");
        }

        return;
      }

      _big = false;

      _big = _big || TerminalMeterGroup.GetDeltaMeter(TerminalId, "EftToGmQuantity", OldMachineMeters.EftToGmQuantity, NewMachineMeters.EftToGmQuantity, NewMachineMeters.EftToGmMaxValueQuantity, BigIncrementsData.QuantityFTToEgm(TerminalId), out DeltaMachineMeters.EftToGmQuantity);
      _big = _big || TerminalMeterGroup.GetDeltaMeter(TerminalId, "EftToGmCents", OldMachineMeters.EftToGmCents, NewMachineMeters.EftToGmCents, NewMachineMeters.EftToGmMaxValueCents, BigIncrementsData.CentsFTToEgm(TerminalId), out DeltaMachineMeters.EftToGmCents);
      _big = _big || TerminalMeterGroup.GetDeltaMeter(TerminalId, "EftFromGmQuantity", OldMachineMeters.EftFromGmQuantity, NewMachineMeters.EftFromGmQuantity, NewMachineMeters.EftFromGmMaxValueQuantity, BigIncrementsData.QuantityFTFromEgm(TerminalId), out DeltaMachineMeters.EftFromGmQuantity);
      _big = _big || TerminalMeterGroup.GetDeltaMeter(TerminalId, "EftFromGmCents", OldMachineMeters.EftFromGmCents, NewMachineMeters.EftFromGmCents, NewMachineMeters.EftFromGmMaxValueCents, BigIncrementsData.CentsFTFromEgm(TerminalId), out DeltaMachineMeters.EftFromGmCents);

      if (_big)
      {
        // Return all "delta" as zero
        DeltaMachineMeters = new MachineMeters();

        Log.Warning("*** FundTransfer EFT Machine Meters BigIncrement *** TerminalId: " + TerminalId.ToString() +
                    ". Old meter values:" +
                    "  ToGm Quantity/Cents: " + OldMachineMeters.EftToGmQuantity.ToString() + " / " + OldMachineMeters.EftToGmCents.ToString() +
                    ", FromGm Quantity/Cents: " + OldMachineMeters.EftFromGmQuantity.ToString() + " / " + OldMachineMeters.EftFromGmCents.ToString());

        Log.Warning("*** FundTransfer EFT Machine Meters BigIncrement *** TerminalId: " + TerminalId.ToString() +
                    ". New meter values:" +
                    "  ToGm Quantity/Cents: " + NewMachineMeters.EftToGmQuantity.ToString() + " / " + NewMachineMeters.EftToGmCents.ToString() +
                    ", FromGm Quantity/Cents: " + NewMachineMeters.EftFromGmQuantity.ToString() + " / " + NewMachineMeters.EftFromGmCents.ToString());

        BigIncrement = true;

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.AppendLine("*** FundTransfer EFT Machine Meters BigIncrement ***");
        _sb.AppendLine(" Old meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + OldMachineMeters.EftToGmQuantity.ToString() + " / " + OldMachineMeters.EftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + OldMachineMeters.EftFromGmQuantity.ToString() + " / " + OldMachineMeters.EftFromGmCents.ToString());
        _sb.AppendLine(" New meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + NewMachineMeters.EftToGmQuantity.ToString() + " / " + NewMachineMeters.EftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + NewMachineMeters.EftFromGmQuantity.ToString() + " / " + NewMachineMeters.EftFromGmCents.ToString());

        Alarm.Register(AlarmSourceCode.TerminalSystem,
                        TerminalId,
                        Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                        AlarmCode.TerminalSystem_MachineMeterBigIncrement,
                        _sb.ToString());
      }

      //28-01-2015 SGB Alarm Rollover
      if (NewMachineMeters.EftToGmQuantity < OldMachineMeters.EftToGmQuantity ||
          NewMachineMeters.EftFromGmQuantity < OldMachineMeters.EftFromGmQuantity ||
          NewMachineMeters.EftToGmCents < OldMachineMeters.EftToGmCents ||
          NewMachineMeters.EftFromGmCents < OldMachineMeters.EftFromGmCents)
      {
        StringBuilder _sb;

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

        _sb = new StringBuilder();
        _sb.AppendLine("*** FundTransfer EFT Machine Meters Rollover ***");
        _sb.AppendLine(" Old meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + OldMachineMeters.EftToGmQuantity.ToString() + " / " + OldMachineMeters.EftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + OldMachineMeters.EftFromGmQuantity.ToString() + " / " + OldMachineMeters.EftFromGmCents.ToString());
        _sb.AppendLine(" New meter values:");
        _sb.AppendLine("     ToGm Quantity/Cents: " + NewMachineMeters.EftToGmQuantity.ToString() + " / " + NewMachineMeters.EftToGmCents.ToString());
        _sb.AppendLine("     FromGm Quantity/Cents: " + NewMachineMeters.EftFromGmQuantity.ToString() + " / " + NewMachineMeters.EftFromGmCents.ToString());

        Alarm.Register(AlarmSourceCode.TerminalSystem,
                       TerminalId,
                       Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                       (UInt32)AlarmCode.TerminalSystem_MeterRollover,
                       _sb.ToString(),
                       AlarmSeverity.Info,
                       _now,
                       Trx);

        InsertIntoTSMHRollover(OldMachineMeters, NewMachineMeters, DeltaMachineMeters, TerminalId, Trx);

        if (NewMachineMeters.EftToGmQuantity < OldMachineMeters.EftToGmQuantity)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00A1
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.EftToGmQuantity
                                                    , NewMachineMeters.EftToGmQuantity
                                                    , DeltaMachineMeters.EftToGmQuantity
                                                    , "Rollover: Machine EftToGm Quantity");
        }

        if (NewMachineMeters.EftFromGmQuantity < OldMachineMeters.EftFromGmQuantity)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00B9
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.EftFromGmQuantity
                                                    , NewMachineMeters.EftFromGmQuantity
                                                    , DeltaMachineMeters.EftFromGmQuantity
                                                    , "Rollover: Machine EftFromGm Quantity");
        }

        if (NewMachineMeters.EftToGmCents < OldMachineMeters.EftToGmCents)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00A0
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.EftToGmCents
                                                    , NewMachineMeters.EftToGmCents
                                                    , DeltaMachineMeters.EftToGmCents
                                                    , "Rollover: Machine EftToGm Cents");
        }

        if (NewMachineMeters.EftFromGmCents < OldMachineMeters.EftFromGmCents)
        {
          TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                    , TerminalId
                                                    , 0x00B8
                                                    , 0
                                                    , 0
                                                    , WGDB.Now
                                                    , TerminalMeterGroup.MetersAdjustmentsType.System
                                                    , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_Reset
                                                    , OldMachineMeters.EftFromGmCents
                                                    , NewMachineMeters.EftFromGmCents
                                                    , DeltaMachineMeters.EftFromGmCents
                                                    , "Rollover: Machine EftFromGm Cents");
        }
      }
    } // GetDeltaMachineMetersEftFundTransfer

    /// <summary>
    /// Returns the old and new values by MeterCode 
    /// </summary>
    /// <param name="MeterCode"></param>
    /// <param name="IsAFTMeter"></param>
    /// <param name="OldValues"></param>
    /// <param name="NewValues"></param>
    /// <param name="DeltaValues"></param>
    /// <param name="OldValue"></param>
    /// <param name="NewValue"></param>
    /// <param name="DeltaValue"></param>
    /// <param name="RawValue"></param>
    /// <param name="MaxMeterValue"></param>
    private static void GetValuesMeterByMeterCode(Int32 MeterCode, Boolean IsAFTMeter, MachineMeters OldValues, MachineMeters NewValues, MachineMeters DeltaValues, out Int64 OldValue, out Int64 NewValue, out Int64 DeltaValue, out Int64 RawValue, out Int64? MaxMeterValue)
    {
      OldValue = 0;
      NewValue = 0;
      DeltaValue = 0;
      RawValue = 0; // non calculated
      MaxMeterValue = null; //max value of the meter

      switch (MeterCode)
      {
        case 5: // Games played
          OldValue = OldValues.PlayedQuantity;
          NewValue = NewValues.PlayedQuantity;
          DeltaValue = DeltaValues.PlayedQuantity;
          MaxMeterValue = OldValues.PlayedMaxValueQuantity;
          break;

        case 6: // Games won
          OldValue = OldValues.WonQuantity;
          NewValue = NewValues.WonQuantity;
          DeltaValue = DeltaValues.WonQuantity;
          MaxMeterValue = OldValues.WonMaxValueQuantity;
          break;

        case 0: // Total coin in credits
          OldValue = OldValues.PlayedCents;
          NewValue = NewValues.PlayedCents;
          DeltaValue = DeltaValues.PlayedCents;
          MaxMeterValue = OldValues.PlayedMaxValueCents;
          break;

        case 1: // Total coin out credits
          OldValue = OldValues.WonCents;
          NewValue = NewValues.WonCents;
          DeltaValue = DeltaValues.WonCents;
          MaxMeterValue = OldValues.WonMaxValueCents;
          break;

        case 2: // Total jackpot credits
          OldValue = OldValues.JackpotCents;
          NewValue = NewValues.JackpotCents;
          DeltaValue = DeltaValues.JackpotCents;
          if (OldValues.JackpotMaxValueCents != -1)
          {
            MaxMeterValue = OldValues.JackpotMaxValueCents;
          }
          break;

        case 4096: // Total progressive jackpot credits
          OldValue = OldValues.ProgressiveJackpotCents;
          NewValue = NewValues.ProgressiveJackpotCents;
          DeltaValue = DeltaValues.ProgressiveJackpotCents;
          if (OldValues.ProgressiveJackpotMaxValueCents != 1)
          {
            MaxMeterValue = OldValues.ProgressiveJackpotMaxValueCents;
          }
          break;

        case 161: // In-House transfers to gaming machine that included cashable amounts (quantity)
          if (IsAFTMeter)
          {
            OldValue = OldValues.AftToGmQuantity;
            NewValue = NewValues.AftToGmQuantity;
            DeltaValue = DeltaValues.AftToGmQuantity;
          }
          else
          {
            OldValue = OldValues.EftToGmQuantity;
            NewValue = NewValues.EftToGmQuantity;
            DeltaValue = DeltaValues.EftToGmQuantity;
          }

          break;

        case 185: // In-house transfers to host that included cashable amounts (quantity)
          if (IsAFTMeter)
          {
            OldValue = OldValues.AftFromGmQuantity;
            NewValue = NewValues.AftFromGmQuantity;
            DeltaValue = DeltaValues.AftFromGmQuantity;
          }
          else
          {
            OldValue = OldValues.EftFromGmQuantity;
            NewValue = NewValues.EftFromGmQuantity;
            DeltaValue = DeltaValues.EftFromGmQuantity;
          }

          break;

        case 160: // In-house cashable transfers to gaming machine (cents)
          if (IsAFTMeter)
          {
            OldValue = OldValues.AftToGmCents;
            NewValue = NewValues.AftToGmCents;
            DeltaValue = DeltaValues.AftToGmCents;
          }
          else
          {
            OldValue = OldValues.EftToGmCents;
            NewValue = NewValues.EftToGmCents;
            DeltaValue = DeltaValues.EftToGmCents;
          }

          break;

        case 184: // In-house cashable transfers to host (cents)
          if (IsAFTMeter)
          {
            OldValue = OldValues.AftFromGmCents;
            NewValue = NewValues.AftFromGmCents;
            DeltaValue = DeltaValues.AftFromGmCents;
          }
          else
          {
            OldValue = OldValues.EftFromGmCents;
            NewValue = NewValues.EftFromGmCents;
            DeltaValue = DeltaValues.EftFromGmCents;
          }

          break;
      }
        }

    /// <summary>
    /// Returns the meter code (and if it is an AFT meter) that caused the Rollover
    /// </summary>
    /// <param name="OldValues"></param>
    /// <param name="NewValues"></param>
    /// <param name="MeterCodeRollover"></param>
    /// <param name="IsAFTMeter"></param>
    private static void GetMeterThatCausedRollover(MachineMeters OldValues, MachineMeters NewValues, out Int32 MeterCodeRollover, out Boolean IsAFTMeter)
    {
      MeterCodeRollover = -1;
      IsAFTMeter = false;

      if (NewValues.PlayedCents < OldValues.PlayedCents)
      {
        MeterCodeRollover = METER_CODE_TOTAL_COIN_IN_CREDITS; // Total coin in credits
      }
      else if (NewValues.WonCents < OldValues.WonCents)
      {
        MeterCodeRollover = METER_CODE_TOTAL_COIN_OUT_CREDITS; // Total coin out credits
      }
      else if (NewValues.JackpotCents < OldValues.JackpotCents)
      {
        MeterCodeRollover = METER_CODE_TOTAL_JACKPOT_CREDITS; // Total jackpot credits
      }
      else if (NewValues.PlayedQuantity < OldValues.PlayedQuantity)
      {
        MeterCodeRollover = METER_CODE_GAMES_PLAYED; // Games played
      }
      else if (NewValues.WonQuantity < OldValues.WonQuantity)
      {
        MeterCodeRollover = METER_CODE_GAMES_WON; // Games won
      }
      else if (NewValues.ProgressiveJackpotCents < OldValues.ProgressiveJackpotCents)
      {
        MeterCodeRollover = METER_CODE_TOTAL_PROGRESSIVE_JACKPOT_CREDITS; // Total progressive jackpot credits
      }
      else if (NewValues.AftToGmQuantity < OldValues.AftToGmQuantity)
      {
        MeterCodeRollover = METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_QUANTITY; // In-house transfers to gaming machine that included cashable amounts (quantity)
        IsAFTMeter = true;
      }
      else if (NewValues.AftFromGmQuantity < OldValues.AftFromGmQuantity)
      {
        MeterCodeRollover = METER_CODE_INHOUSE_TRANSFERS_TO_HOST_QUANTITY; // In-house transfers to host that included cashable amounts (quantity)
        IsAFTMeter = true;
      }
      else if (NewValues.AftToGmCents < OldValues.AftToGmCents)
      {
        MeterCodeRollover = METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_CENTS; // In-house cashable transfers to gaming machine (cents)
        IsAFTMeter = true;
      }
      else if (NewValues.AftFromGmCents < OldValues.AftFromGmCents)
      {
        MeterCodeRollover = METER_CODE_INHOUSE_TRANSFERS_TO_HOST_CENTS; // In-house cashable transfers to host (cents)
        IsAFTMeter = true;
      }
      else if (NewValues.EftToGmQuantity < OldValues.EftToGmQuantity)
      {
        MeterCodeRollover = METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_QUANTITY; // In-House transfers to gaming machine that included cashable amounts (quantity)
      }
      else if (NewValues.EftFromGmQuantity < OldValues.EftFromGmQuantity)
      {
        MeterCodeRollover = METER_CODE_INHOUSE_TRANSFERS_TO_HOST_QUANTITY; // In-house transfers to host that included cashable amounts (quantity)
      }
      else if (NewValues.EftToGmCents < OldValues.EftToGmCents)
      {
        MeterCodeRollover = METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_CENTS; // In-house cashable transfers to gaming machine (cents)
      }
      else if (NewValues.EftFromGmCents < OldValues.EftFromGmCents)
      {
        MeterCodeRollover = METER_CODE_INHOUSE_TRANSFERS_TO_HOST_CENTS; // In-house cashable transfers to host (cents)
      }
    }

    /// <summary>
    /// Creates TSMH record fro a RollOver. Also it creates the group records for it.
    /// </summary>
    /// <param name="OldValues"></param>
    /// <param name="NewValues"></param>
    /// <param name="DeltaValues"></param>
    /// <param name="TerminalId"></param>
    /// <param name="Trx"></param>
    private static void InsertIntoTSMHRollover(MachineMeters OldValues, MachineMeters NewValues, MachineMeters DeltaValues, Int32 TerminalId, SqlTransaction Trx)
    {
      Int32 _meter_code;
      DateTime _created_time;
      Int64 _group_id;
      Int32 _meter_origin_code;
      Boolean _is_AFT_Meter;

      try
      {
        GetMeterThatCausedRollover(OldValues, NewValues, out _meter_code, out _is_AFT_Meter);

        if (_meter_code == -1)
        {
          Log.Warning("Rollover not expected");

          return;
        }

        //Get values for CreatedDatetime, GroupId and MeterOrigin
        _created_time = WGDB.Now;
        GetTerminalMetersHistoryEventsSequenceValue(Trx, out _group_id); //Get sequence value for Group Id
        _meter_origin_code = _meter_code; //Get the origin meter code

        //We create a RollOver record in TSMH with CreatedDatetime, GroupId and MeterOrigin values
        InsertIntoTSMH(TerminalId, _meter_code, ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER, _is_AFT_Meter, OldValues, NewValues, DeltaValues, _created_time, _group_id, _meter_origin_code, null, Trx);

        //We create records for the group in TSMH, with the same CreatedDatetime, GroupId and MeterOrigin values
        InsertIntoTSMHGroup(TerminalId, ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER, OldValues, NewValues, DeltaValues, _created_time, _group_id, _meter_origin_code, null, Trx);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    /// <summary>
    /// Creates TSMH record for a Service RAM Clear. Also it creates the group records for it.
    /// </summary>
    /// <param name="OldValues"></param>
    /// <param name="NewValues"></param>
    /// <param name="DeltaValues"></param>
    /// <param name="TerminalId"></param>
    /// <param name="Trx"></param>
    private static void InsertIntoTSMHServiceRAMClear(MachineMeters OldValues, MachineMeters NewValues, MachineMeters DeltaValues, Int32 TerminalId, SqlTransaction Trx)
    {
      Int32 _meter_code;
      DateTime _created_time;
      Int64 _group_id;
      Int32? _meter_origin_code;
      Boolean _is_AFT_Meter;

      try
      {
        //We create the record for the meter 0
        _meter_code = 0; // Total coin in credits
        _is_AFT_Meter = false;

        //Get values for CreatedDatetime, GroupId and MeterOrigin
        _created_time = WGDB.Now;
        GetTerminalMetersHistoryEventsSequenceValue(Trx, out _group_id); //Get sequence value for Group Id
        _meter_origin_code = null; //It is a Service RAM Clear and we don't know the origin meter

        //We create a Service RAM Clear record in TSMH with CreatedDatetime, GroupId and MeterOrigin values
        InsertIntoTSMH(TerminalId, _meter_code, ENUM_SAS_METER_HISTORY.TSMH_METER_SERVICE_RAM_CLEAR, _is_AFT_Meter, OldValues, NewValues, DeltaValues, _created_time, _group_id, _meter_origin_code, null, Trx);

        //We create records for the group in TSMH, with the same CreatedDatetime, GroupId and MeterOrigin values
        InsertIntoTSMHGroup(TerminalId, ENUM_SAS_METER_HISTORY.TSMH_METER_SERVICE_RAM_CLEAR, OldValues, NewValues, DeltaValues, _created_time, _group_id, _meter_origin_code, null, Trx);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    /// <summary>
    /// Creates TSMH record for a SAS Accounting Denom Change. Also it creates the group records for it.
    /// </summary>
    /// <param name="OldValues"></param>
    /// <param name="NewValues"></param>
    /// <param name="DeltaValues"></param>
    /// <param name="TerminalId"></param>
    /// <param name="OldSASAccountDenom"></param>
    /// <param name="Trx"></param>
    private static void InsertIntoTSMHDenomChange(MachineMeters OldValues, MachineMeters NewValues, MachineMeters DeltaValues, Int32 TerminalId, Decimal OldSASAccountDenom, SqlTransaction Trx)
    {
      Int32 _meter_code;
      DateTime _created_time;
      Int64 _group_id;
      Int32? _meter_origin_code;
      Boolean _is_AFT_Meter;

      try
      {
        //We create the record for the meter 0
        _meter_code = 0; // Total coin in credits
        _is_AFT_Meter = false;

        //Get values for CreatedDatetime, GroupId and MeterOrigin
        _created_time = WGDB.Now;
        GetTerminalMetersHistoryEventsSequenceValue(Trx, out _group_id); //Get sequence value for Group Id
        _meter_origin_code = null; //It is a Denom Change and we don't know the origin meter

        //We create a Denom Change record in TSMH with CreatedDatetime, GroupId and MeterOrigin values
        InsertIntoTSMH(TerminalId, _meter_code, ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE, _is_AFT_Meter, OldValues, NewValues, DeltaValues, _created_time, _group_id, _meter_origin_code, OldSASAccountDenom, Trx);

        //We create records for the group in TSMH, with the same CreatedDatetime, GroupId and MeterOrigin values
        InsertIntoTSMHGroup(TerminalId, ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE, OldValues, NewValues, DeltaValues, _created_time, _group_id, _meter_origin_code, OldSASAccountDenom, Trx);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    /// <summary>
    /// Gets the value of the sequence used when there is a TerminalMetersHistoryEvents
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="SequenceValue"></param>
    /// <returns></returns>
    private static Boolean GetTerminalMetersHistoryEventsSequenceValue(SqlTransaction SqlTrx, out Int64 SequenceValue)
    {
      SequenceValue = 0;

      try
      {
        return Sequences.GetValue(SqlTrx, SequenceId.TerminalMetersHistoryEvents, out SequenceValue);
      }
      catch (Exception _ex)
      {
        Log.Warning("BU_SasMeters.GetTerminalMetersHistoryEventsSequenceValue");
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Get list of meters for group
    /// </summary>
    /// <param name="OldValues"></param>
    /// <param name="NewValues"></param>
    /// <returns></returns>
    public static List<KeyValuePair<Int32, Boolean>> GetListMetersForGroup(MachineMeters OldValues, MachineMeters NewValues)
    {
      MeterTSMHGroupList _meter_tsmh_group = new MeterTSMHGroupList();

      return _meter_tsmh_group.GetListMetersForGroup(OldValues, NewValues);
    }

    /// <summary>
    /// Get meter history type for group
    /// </summary>
    /// <param name="MeterType"></param>
    /// <param name="MeterTypeForGroup"></param>
    /// <returns></returns>
    private static Boolean GetMeterHistoryForGroup(ENUM_SAS_METER_HISTORY MeterType, out ENUM_SAS_METER_HISTORY MeterTypeForGroup)
    {
      MeterTypeForGroup = ENUM_SAS_METER_HISTORY.NONE;

      switch (MeterType)
      {
        //case ENUM_SAS_METER_HISTORY.TSMH_METER_RESET:
        //  MeterTypeForGroup = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_RESET;
        //  break;

        case ENUM_SAS_METER_HISTORY.TSMH_METER_SERVICE_RAM_CLEAR:
          MeterTypeForGroup = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SERVICE_RAM_CLEAR;
          break;

        case ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER:
          MeterTypeForGroup = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_ROLLOVER;
          break;

        case ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE:
          MeterTypeForGroup = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SAS_ACCOUNT_DENOM_CHANGE;
          break;

        default:
          Log.Error(String.Format("GetMeterHistoryForGroup: MeterType {0} not suported", MeterType.ToString()));
          return false;
      }

      return true;
    }

    /// <summary>
    /// We create records for the group in TSMH, with the values of the Meters (old & new) and the values of parameters CreatedDatetime, GroupId and MeterOrigin 
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="MeterType"></param>
    /// <param name="OldValues"></param>
    /// <param name="NewValues"></param>
    /// <param name="DeltaValues"></param>
    /// <param name="CreatedTime"></param>
    /// <param name="GroupId"></param>
    /// <param name="MeterCodeOrigin">It is nullable, only informed when is a Rollover</param>
    /// <param name="OldSASAccountDenom">It is nullable, only informed when is a SAS Denom Change</param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean InsertIntoTSMHGroup(Int32 TerminalId, ENUM_SAS_METER_HISTORY MeterType, MachineMeters OldValues, MachineMeters NewValues, MachineMeters DeltaValues, DateTime CreatedTime, Int64 GroupId, Int32? MeterCodeOrigin, Decimal? OldSASAccountDenom, SqlTransaction Trx)
    {
      ENUM_SAS_METER_HISTORY _meter_type_group;

      try
      {
        if (!GetMeterHistoryForGroup(MeterType, out _meter_type_group))
        {
            return false;
        }

        //Get list of meters for the group in TSMH
        List<KeyValuePair<Int32, Boolean>> _list_meters_for_group;
        _list_meters_for_group = GetListMetersForGroup(OldValues, NewValues);

        Int32 _meter_code;
        Boolean _is_AFT_meter;

        foreach (KeyValuePair<Int32, Boolean> _meter_item in _list_meters_for_group)
        {
          _meter_code = _meter_item.Key;
          _is_AFT_meter = _meter_item.Value;

          InsertIntoTSMH(TerminalId, _meter_code, _meter_type_group, _is_AFT_meter, OldValues, NewValues, DeltaValues, CreatedTime, GroupId, MeterCodeOrigin, OldSASAccountDenom, Trx);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //private static List<Int32, Boolean> GetMeterCodeListForGroup(Boolean GetAftMeters, Boolean GetEftMeters)
    //{
    //  List<Int32> _group_meter_list;
    //  _group_meter_list = new List<Int32>();

    //  return _group_meter_list;
    //}

    /// <summary>
    /// Inserts a record into TSMH
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="MeterCode"></param>
    /// <param name="MeterType"></param>
    /// <param name="OldValue"></param>
    /// <param name="NewValue"></param>
    /// <param name="DeltaValue"></param>
    /// <param name="RawValue"></param>
    /// <param name="CreatedTime"></param>
    /// <param name="GroupId"></param>
    /// <param name="MeterOrigin"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean InsertIntoTSMH(Int32 TerminalId, Int32 MeterCode, ENUM_SAS_METER_HISTORY MeterType, Boolean IsAFTMeter, MachineMeters OldValues, MachineMeters NewValues, MachineMeters DeltaValues, DateTime CreatedTime, Int64 GroupId, Int32? MeterOrigin, Decimal? SASAccountDenom, SqlTransaction Trx)
    {
      StringBuilder _sb;

      Int64 _old_value;
      Int64 _new_value;
      Int64 _delta_value;
      Int64 _raw_value;
      Int64? _max_meter_value;

      try
      {
        //Get values for the meter
        GetValuesMeterByMeterCode(MeterCode, IsAFTMeter, OldValues, NewValues, DeltaValues, out _old_value, out _new_value, out _delta_value, out _raw_value, out _max_meter_value);

        _sb = new StringBuilder();
        _sb.AppendLine(" DECLARE @v_SasAccountDenom   AS  MONEY                               ");

        _sb.AppendLine(" SET @v_SasAccountDenom = @pSasAccountDenom                           ");

        //If SAS Accounting Denom not informed, then take machine's SAS Accounting Denom
        _sb.AppendLine(" IF (@v_SasAccountDenom IS NULL)                                      ");
        _sb.AppendLine(" BEGIN  ");
        _sb.AppendLine("    SET @v_SasAccountDenom = (SELECT   TE_SAS_ACCOUNTING_DENOM        ");
        _sb.AppendLine("                                FROM   TERMINALS                      ");
        _sb.AppendLine("                               WHERE   TE_TERMINAL_ID = @pTerminalId) ");
        _sb.AppendLine(" END                                                                  ");

        _sb.AppendLine(" INSERT INTO  TERMINAL_SAS_METERS_HISTORY                             ");
        _sb.AppendLine("            (                                                         ");
        _sb.AppendLine("              TSMH_TERMINAL_ID                                        ");
        _sb.AppendLine("            , TSMH_METER_CODE                                         ");
        _sb.AppendLine("            , TSMH_GAME_ID                                            ");
        _sb.AppendLine("            , TSMH_DENOMINATION                                       ");
        _sb.AppendLine("            , TSMH_TYPE                                               ");
        _sb.AppendLine("            , TSMH_DATETIME                                           ");
        _sb.AppendLine("            , TSMH_LAST_REPORTED                                      ");
        _sb.AppendLine("            , TSMH_CREATED_DATETIME                                   ");
        _sb.AppendLine("            , TSMH_METER_INI_VALUE                                    ");
        _sb.AppendLine("            , TSMH_METER_FIN_VALUE                                    ");
        _sb.AppendLine("            , TSMH_METER_INCREMENT                                    ");
        _sb.AppendLine("            , TSMH_RAW_METER_INCREMENT                                ");
        _sb.AppendLine("            , TSMH_SAS_ACCOUNTING_DENOM                               ");
        _sb.AppendLine("            , TSMH_GROUP_ID                                           ");
        _sb.AppendLine("            , TSMH_METER_ORIGIN                                       ");
        _sb.AppendLine("            , TSMH_METER_MAX_VALUE                                    ");
        _sb.AppendLine("            )                                                         ");
        _sb.AppendLine("    VALUES                                                            ");
        _sb.AppendLine("            (                                                         ");
        _sb.AppendLine("              @pTerminalId                                            ");
        _sb.AppendLine("            , @pMeterCode                                             ");
        _sb.AppendLine("            , @pGameId                                                ");
        _sb.AppendLine("            , @pDenomination                                          ");
        _sb.AppendLine("            , @pType                                                  ");
        _sb.AppendLine("            , @pDatetime                                              ");
        _sb.AppendLine("            , @pLastReported                                          ");
        _sb.AppendLine("            , @pCreatedDatetime                                       ");
        _sb.AppendLine("            , @pMeterIniValue                                         ");
        _sb.AppendLine("            , @pMeterFinValue                                         ");
        _sb.AppendLine("            , @pMeterIncrement                                        ");
        _sb.AppendLine("            , @pRawMeterIncrement                                     ");
        _sb.AppendLine("            , ISNULL(@v_SasAccountDenom, 0.01)                        ");
        _sb.AppendLine("            , @pGroupId                                               ");
        _sb.AppendLine("            , @pMeterOrigin                                           ");
        _sb.AppendLine("            , @pMeterMaxValue                                         ");
        _sb.AppendLine("            )                                                         ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pMeterCode", SqlDbType.Int).Value = MeterCode;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Value = 0;
          _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Decimal).Value = 0;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = MeterType;

              _sql_cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = CreatedTime;
          _sql_cmd.Parameters.Add("@pLastReported", SqlDbType.DateTime).Value = CreatedTime;
          _sql_cmd.Parameters.Add("@pCreatedDatetime", SqlDbType.DateTime).Value = CreatedTime;

          _sql_cmd.Parameters.Add("@pMeterIniValue", SqlDbType.BigInt).Value = _old_value;
          _sql_cmd.Parameters.Add("@pMeterFinValue", SqlDbType.BigInt).Value = _new_value;
          _sql_cmd.Parameters.Add("@pMeterIncrement", SqlDbType.BigInt).Value = _delta_value;
          _sql_cmd.Parameters.Add("@pRawMeterIncrement", SqlDbType.BigInt).Value = _raw_value;
          //FGB: 16-JUN-2016
          _sql_cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = GroupId;

          //Only when it's a Rollover the origin meter is known
          if (MeterOrigin != null)
          {
            _sql_cmd.Parameters.Add("@pMeterOrigin", SqlDbType.Int).Value = MeterOrigin;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pMeterOrigin", SqlDbType.Int).Value = DBNull.Value;
          }

          //Only when it's a Rollover the max value is known
          if (_max_meter_value != null)
          {
            _sql_cmd.Parameters.Add("@pMeterMaxValue", SqlDbType.BigInt).Value = _max_meter_value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pMeterMaxValue", SqlDbType.BigInt).Value = DBNull.Value;
          }

          //SAS Accounting Denom (only informed on SAS Accounting Denom Change)
          if (SASAccountDenom != null)
          {
            _sql_cmd.Parameters.Add("@pSasAccountDenom", SqlDbType.Decimal).Value = SASAccountDenom;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pSasAccountDenom", SqlDbType.Decimal).Value = DBNull.Value;
          }

          _sql_cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    private static void SetToModified(DataRow[] Rows)
    {
      foreach (DataRow _row in Rows)
      {
        if (_row.RowState != DataRowState.Unchanged)
        {
          _row.AcceptChanges();
        }

        _row.SetModified();
      }
      }

    #endregion // Private Methods

    #region Statistics

    //------------------------------------------------------------------------------
    // PURPOSE : Prepares delta machine meters for Update machine meters statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: delta machine meters inserted in Sales per Hour.
    //      - false: error.
    //
    static public Boolean ProcessMachineMetersStatistics(SqlTransaction Trx)
    {
      DataTable _dt;
      Boolean _rc;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_select;
      long _interval_update_MSH;
      long _interval_update_MM;
      long _interval_all_process;

      _interval_select = 0;
      _interval_update_MSH = 0;
      _interval_update_MM = 0;
      _interval_all_process = 0;
      _tick0 = Environment.TickCount;

      _dt = null;

      try
      {
        _tick1 = Environment.TickCount;

        // Select Delta Machine Meters
        _rc = GetDeltaMachineMeters(out _dt, Trx);
        if (!_rc)
        {
          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_select = Misc.GetElapsedTicks(_tick1, _tick2);

        _tick1 = Environment.TickCount;

        // Update MACHINE_STATS_PER_HOUR
        _rc = UpdateMachineStatsPerHour(_dt, Trx);
        if (!_rc)
        {
          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update_MSH = Misc.GetElapsedTicks(_tick1, _tick2);

        _tick1 = Environment.TickCount;

        // Update Delta Machine Meters
        _rc = UpdateDeltaMachineMeters(_dt, Trx);
        if (!_rc)
        {
          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update_MM = Misc.GetElapsedTicks(_tick1, _tick2);

        _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);

        if (_interval_select > 4000
            || _interval_update_MSH > 4000
            || _interval_update_MM > 4000
            || _interval_all_process > 8000)
        {
          Log.Warning("Thread Statistics MachineMeters times: Select/UpdateMSH/UpdateMM/Total = " + _interval_select.ToString() + "/" + _interval_update_MSH.ToString() + "/" + _interval_update_MM.ToString() + "/" + _interval_all_process.ToString());
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
      }
    } // ProcessMachineMetersStatistics

    //------------------------------------------------------------------------------
    // PURPOSE : Update Machine statistics per hour.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DeltaMachineMeter: Datatable with delta Machine meters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: delta Machine meters inserted in Sales per Hour.
    //      - false: error.
    //
    static private Boolean UpdateMachineStatsPerHour(DataTable DeltaMachineMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      int _update_batch_size = 500;
      int _nr;
      Int32 _progressives_enabled;

      if (DeltaMachineMeters.Rows.Count == 0)
      {
        return true;
      }

      try
      {
        _progressives_enabled = GeneralParam.GetInt32("Progressives", "Enabled", 0);

        //    - Set SqlDataAdapter commands

        _da = new SqlDataAdapter();

        // When batch updates are enabled (UpdateBatchSize != 1), the UpdatedRowSource property value 
        // of the DataAdapter's UpdateCommand, InsertCommand, and DeleteCommand should be set to None 
        // or OutputParameters. 
        _da.UpdateBatchSize = _update_batch_size;

        //    - Set SqlDataAdapter commands
        //        - SelectCommand 
        //        - DeleteCommand 
        //        - InsertCommand 
        //        - UpdateCommand 

        //        - UpdateCommand 
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE MACHINE_STATS_PER_HOUR");
        _sql_txt.AppendLine("   SET MSH_PLAYED_COUNT                  =  MSH_PLAYED_COUNT                            + @pPlayedCount");
        _sql_txt.AppendLine("     , MSH_PLAYED_AMOUNT                 =  MSH_PLAYED_AMOUNT                           + @pPlayedAmount");
        _sql_txt.AppendLine("     , MSH_WON_COUNT                     =  MSH_WON_COUNT                               + @pWonCount");
        _sql_txt.AppendLine("     , MSH_WON_AMOUNT                    =  MSH_WON_AMOUNT                              + @pWonAmount");
        _sql_txt.AppendLine("     , MSH_JACKPOT_AMOUNT                =  ISNULL(MSH_JACKPOT_AMOUNT, 0)               + ISNULL(@pDeltaJackpot, 0) ");
        _sql_txt.AppendLine("     , MSH_PROGRESSIVE_JACKPOT_AMOUNT    =  ISNULL(MSH_PROGRESSIVE_JACKPOT_AMOUNT, 0)   + 0 "); // ACC 17-OCT-2014  ISNULL(@pDeltaProgressive, 0) ");
        _sql_txt.AppendLine("     , MSH_PROGRESSIVE_JACKPOT_AMOUNT_0  =  ISNULL(MSH_PROGRESSIVE_JACKPOT_AMOUNT_0, 0) + 0 "); // ACC 17-OCT-2014  (CASE WHEN @pProgressivesEnabled = 1 THEN ISNULL(@pDeltaProgressive, 0) ELSE 0 END) ");
        _sql_txt.AppendLine("     , MSH_TO_GM_COUNT                   =  MSH_TO_GM_COUNT                             + @pToGmCount");
        _sql_txt.AppendLine("     , MSH_TO_GM_AMOUNT                  =  MSH_TO_GM_AMOUNT                            + @pToGmAmount");
        _sql_txt.AppendLine("     , MSH_FROM_GM_COUNT                 =  MSH_FROM_GM_COUNT                           + @pFromGmCount");
        _sql_txt.AppendLine("     , MSH_FROM_GM_AMOUNT                =  MSH_FROM_GM_AMOUNT                          + @pFromGmAmount");
        _sql_txt.AppendLine(" WHERE MSH_TERMINAL_ID  = @pTerminalId");
        _sql_txt.AppendLine("   AND MSH_BASE_HOUR    = @pBaseHour");

        _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
        _da.UpdateCommand.Connection = Trx.Connection;
        _da.UpdateCommand.Transaction = Trx;
        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        _da.UpdateCommand.Parameters.Add("@pPlayedCount", SqlDbType.BigInt, 8, "MM_DELTA_PLAYED_COUNT");
        _da.UpdateCommand.Parameters.Add("@pPlayedAmount", SqlDbType.Money, 8, "MM_DELTA_PLAYED_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@pWonCount", SqlDbType.BigInt, 8, "MM_DELTA_WON_COUNT");
        _da.UpdateCommand.Parameters.Add("@pWonAmount", SqlDbType.Money, 8, "MM_DELTA_WON_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@pProgressivesEnabled", SqlDbType.Int).Value = _progressives_enabled;
        _da.UpdateCommand.Parameters.Add("@pDeltaJackpot", SqlDbType.Money, 8, "MM_DELTA_JACKPOT_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@pDeltaProgressive", SqlDbType.Money, 8, "MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@pToGmCount", SqlDbType.BigInt, 8, "MM_DELTA_TO_GM_COUNT");
        _da.UpdateCommand.Parameters.Add("@pToGmAmount", SqlDbType.Money, 8, "MM_DELTA_TO_GM_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@pFromGmCount", SqlDbType.BigInt, 8, "MM_DELTA_FROM_GM_COUNT");
        _da.UpdateCommand.Parameters.Add("@pFromGmAmount", SqlDbType.Money, 8, "MM_DELTA_FROM_GM_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int, 4, "MM_TERMINAL_ID");
        _da.UpdateCommand.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "BASE_HOUR";

        //        - DeleteCommand & SelectCommand   
        _da.DeleteCommand = null;
        _da.SelectCommand = null;

        //    - Execute Update command
        _da.InsertCommand = null;
        _da.UpdateBatchSize = _update_batch_size;
        _da.ContinueUpdateOnError = true;

        _nr = _da.Update(DeltaMachineMeters);

        if (DeltaMachineMeters.Rows.Count == _nr)
        {
          return true;
        }

        foreach (DataRow _dr in DeltaMachineMeters.Rows)
        {
          if (_dr.HasErrors)
          {
            _dr.ClearErrors();
            _dr.AcceptChanges();
            _dr.SetAdded();
          }
          else
          {
            _dr.AcceptChanges();
            if (_dr.RowState != DataRowState.Unchanged)
            {
              return false;
            }
          }
        }

        _da.UpdateCommand = null;

        // Insert
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("INSERT INTO  MACHINE_STATS_PER_HOUR                ");
        _sql_txt.AppendLine("           (                                       ");
        _sql_txt.AppendLine("             MSH_BASE_HOUR                         ");
        _sql_txt.AppendLine("           , MSH_TERMINAL_ID                       ");
        _sql_txt.AppendLine("           , MSH_PLAYED_COUNT                      ");
        _sql_txt.AppendLine("           , MSH_PLAYED_AMOUNT                     ");
        _sql_txt.AppendLine("           , MSH_WON_COUNT                         ");
        _sql_txt.AppendLine("           , MSH_WON_AMOUNT                        ");
        _sql_txt.AppendLine("           , MSH_JACKPOT_AMOUNT                    ");
        _sql_txt.AppendLine("           , MSH_PROGRESSIVE_JACKPOT_AMOUNT        ");
        _sql_txt.AppendLine("           , MSH_PROGRESSIVE_JACKPOT_AMOUNT_0      ");
        _sql_txt.AppendLine("           , MSH_TO_GM_COUNT                       ");
        _sql_txt.AppendLine("           , MSH_TO_GM_AMOUNT                      ");
        _sql_txt.AppendLine("           , MSH_FROM_GM_COUNT                     ");
        _sql_txt.AppendLine("           , MSH_FROM_GM_AMOUNT                    ");
        _sql_txt.AppendLine("          )                                        ");
        _sql_txt.AppendLine("VALUES    (                                        ");
        _sql_txt.AppendLine("             @pBaseHour, @pTerminalId, @pPlayedCount, @pPlayedAmount, @pWonCount, @pWonAmount, ");
        //_sql_txt.AppendLine("             ISNULL(@pDeltaJackpot, 0), ISNULL(@pDeltaProgressive, 0), (CASE WHEN @pProgressivesEnabled = 1 THEN ISNULL(@pDeltaProgressive, 0) ELSE 0 END), ");
        // ACC 17-OCT-2014 Progressives are inserted when it paid by cashier.
        _sql_txt.AppendLine("             ISNULL(@pDeltaJackpot, 0), 0, 0, ");
        _sql_txt.AppendLine("             @pToGmCount, @pToGmAmount, @pFromGmCount, @pFromGmAmount ");
        _sql_txt.AppendLine("           ) ");

        _da.InsertCommand = new SqlCommand(_sql_txt.ToString());
        _da.InsertCommand.Connection = Trx.Connection;
        _da.InsertCommand.Transaction = Trx;

        _da.InsertCommand.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "BASE_HOUR";
        _da.InsertCommand.Parameters.Add("@pTerminalId", SqlDbType.Int, 4, "MM_TERMINAL_ID");
        _da.InsertCommand.Parameters.Add("@pPlayedCount", SqlDbType.BigInt, 8, "MM_DELTA_PLAYED_COUNT");
        _da.InsertCommand.Parameters.Add("@pPlayedAmount", SqlDbType.Money, 8, "MM_DELTA_PLAYED_AMOUNT");
        _da.InsertCommand.Parameters.Add("@pWonCount", SqlDbType.BigInt, 8, "MM_DELTA_WON_COUNT");
        _da.InsertCommand.Parameters.Add("@pWonAmount", SqlDbType.Money, 8, "MM_DELTA_WON_AMOUNT");
        _da.InsertCommand.Parameters.Add("@pProgressivesEnabled", SqlDbType.Int).Value = _progressives_enabled;
        _da.InsertCommand.Parameters.Add("@pDeltaJackpot", SqlDbType.Money, 8, "MM_DELTA_JACKPOT_AMOUNT");
        _da.InsertCommand.Parameters.Add("@pDeltaProgressive", SqlDbType.Money, 8, "MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT");
        _da.InsertCommand.Parameters.Add("@pToGmCount", SqlDbType.BigInt, 8, "MM_DELTA_TO_GM_COUNT");
        _da.InsertCommand.Parameters.Add("@pToGmAmount", SqlDbType.Money, 8, "MM_DELTA_TO_GM_AMOUNT");
        _da.InsertCommand.Parameters.Add("@pFromGmCount", SqlDbType.BigInt, 8, "MM_DELTA_FROM_GM_COUNT");
        _da.InsertCommand.Parameters.Add("@pFromGmAmount", SqlDbType.Money, 8, "MM_DELTA_FROM_GM_AMOUNT");

        _da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
        _nr = _nr + _da.Update(DeltaMachineMeters);

        if (DeltaMachineMeters.Rows.Count != _nr)
        {
          // Some inserts failed!
          // Set the 'delta' to zero and continue with the transaction
          foreach (DataRow _dr in DeltaMachineMeters.Rows)
          {
            if (_dr.HasErrors)
            {
              _dr.ClearErrors();
              _dr["MM_DELTA_PLAYED_COUNT"] = 0;
              _dr["MM_DELTA_PLAYED_AMOUNT"] = 0;
              _dr["MM_DELTA_WON_COUNT"] = 0;
              _dr["MM_DELTA_WON_AMOUNT"] = 0;
              _dr["MM_DELTA_JACKPOT_AMOUNT"] = 0;
              _dr["MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT"] = 0;
              _dr["MM_DELTA_TO_GM_COUNT"] = 0;
              _dr["MM_DELTA_TO_GM_AMOUNT"] = 0;
              _dr["MM_DELTA_FROM_GM_COUNT"] = 0;
              _dr["MM_DELTA_FROM_GM_AMOUNT"] = 0;
              _nr = _nr + 1;
            }
            _dr.AcceptChanges();
          }

          if (DeltaMachineMeters.Rows.Count != _nr)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

        return false;
    } // UpdateMachineStatsPerHour

    //------------------------------------------------------------------------------
    // PURPOSE : Prepares delta Machine meters for Update Machine meters statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //          - DeltaMachineMeters
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: delta Machine meters Selected from Machine Meters.
    //      - false: error.
    //
    static public Boolean GetDeltaMachineMeters(out DataTable DeltaMachineMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      DateTime _base_hour;
      int _idx_row;
      DataTable _rows_for_update;
      int _nr;

      _base_hour = DateTime.Now; // To avoid compiler warning
      DeltaMachineMeters = new DataTable("MACHINE_METERS");

      //
      // Lock by rows and with PK order --> Select MachineMeters for UPDATE .. WHERE [PK]
      //
      _rows_for_update = new DataTable("MACHINE_METERS");

      // Set SqlDataAdapter commands
      _da = new SqlDataAdapter();

      // Accept negative deltas

      //        - SelectCommand 
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("  SELECT   MM_TERMINAL_ID ");
      _sql_txt.AppendLine("    FROM   MACHINE_METERS ");
      _sql_txt.AppendLine("   WHERE   MM_DELTA_UPDATING  = 0 ");
      _sql_txt.AppendLine("     AND   (   MM_DELTA_PLAYED_COUNT <> 0 ");
      _sql_txt.AppendLine("            OR MM_DELTA_PLAYED_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR MM_DELTA_WON_COUNT <> 0 ");
      _sql_txt.AppendLine("            OR MM_DELTA_WON_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR MM_DELTA_JACKPOT_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR MM_DELTA_TO_GM_COUNT <> 0 ");
      _sql_txt.AppendLine("            OR MM_DELTA_TO_GM_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR MM_DELTA_FROM_GM_COUNT <> 0 ");
      _sql_txt.AppendLine("            OR MM_DELTA_FROM_GM_AMOUNT <> 0 ) ");
      _sql_txt.AppendLine("ORDER BY   MM_TERMINAL_ID  ");

      _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
      _da.SelectCommand.Connection = Trx.Connection;
      _da.SelectCommand.Transaction = Trx;

      //        - UpdateCommand, DeleteCommand & InsertCommand
      _da.DeleteCommand = null;
      _da.InsertCommand = null;
      _da.UpdateCommand = null;

      //    - Execute Select command
      _da.Fill(_rows_for_update);

      if (_rows_for_update.Rows.Count == 0)
      {
        return false;
      }

      //
      // UPDATE GM_DELTA_UPDATING
      //
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("UPDATE MACHINE_METERS");
      _sql_txt.AppendLine("   SET MM_DELTA_UPDATING       = 1 ");
      _sql_txt.AppendLine(" WHERE MM_TERMINAL_ID          = @pTerminalId ");
      _sql_txt.AppendLine("   AND MM_DELTA_UPDATING       = 0 ");

      _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
      _da.UpdateCommand.Connection = Trx.Connection;
      _da.UpdateCommand.Transaction = Trx;
      _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int, 4, "MM_TERMINAL_ID");

      //    - SetModified
      for (_idx_row = 0; _idx_row < _rows_for_update.Rows.Count; _idx_row++)
      {
        _rows_for_update.Rows[_idx_row].SetModified();
      }

      _da.UpdateBatchSize = 500;
      _da.ContinueUpdateOnError = true;

      _nr = _da.Update(_rows_for_update);
      if (_nr == 0)
      {
        return false;
      }

      //    - Set SqlDataAdapter commands
      //        - SelectCommand 
      //        - DeleteCommand 
      //        - InsertCommand 
      //        - UpdateCommand 
      _da = new SqlDataAdapter();

      //        - SelectCommand 
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("  SELECT   MM_TERMINAL_ID ");
      _sql_txt.AppendLine("         , MM_DELTA_PLAYED_COUNT ");
      _sql_txt.AppendLine("         , MM_DELTA_PLAYED_AMOUNT ");
      _sql_txt.AppendLine("         , MM_DELTA_WON_COUNT ");
      _sql_txt.AppendLine("         , MM_DELTA_WON_AMOUNT ");
      _sql_txt.AppendLine("         , MM_DELTA_JACKPOT_AMOUNT ");
      _sql_txt.AppendLine("         , MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT ");
      _sql_txt.AppendLine("         , MM_DELTA_TO_GM_COUNT ");
      _sql_txt.AppendLine("         , MM_DELTA_TO_GM_AMOUNT ");
      _sql_txt.AppendLine("         , MM_DELTA_FROM_GM_COUNT ");
      _sql_txt.AppendLine("         , MM_DELTA_FROM_GM_AMOUNT ");
      _sql_txt.AppendLine("         , GETDATE() AS 'BASE_HOUR' ");
      _sql_txt.AppendLine("    FROM   MACHINE_METERS ");
      _sql_txt.AppendLine("   WHERE   MM_DELTA_UPDATING = 1 ");
      _sql_txt.AppendLine("ORDER BY   MM_TERMINAL_ID  ");

      _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
      _da.SelectCommand.Connection = Trx.Connection;
      _da.SelectCommand.Transaction = Trx;

      //        - UpdateCommand, DeleteCommand & InsertCommand
      _da.DeleteCommand = null;
      _da.InsertCommand = null;
      _da.UpdateCommand = null;

      //    - Execute Select command
      _da.Fill(DeltaMachineMeters);

      if (DeltaMachineMeters.Rows.Count == 0)
      {
        return false;
      }

      for (_idx_row = 0; _idx_row < DeltaMachineMeters.Rows.Count; _idx_row++)
      {
        if (_idx_row == 0)
        {
          _base_hour = (DateTime)DeltaMachineMeters.Rows[_idx_row]["BASE_HOUR"];
          _base_hour = _base_hour.AddMinutes(-_base_hour.Minute);
          _base_hour = _base_hour.AddSeconds(-_base_hour.Second);
          _base_hour = _base_hour.AddMilliseconds(-_base_hour.Millisecond);
        }

        // Truncate current datetime and set the row as mofidied
        DeltaMachineMeters.Rows[_idx_row]["BASE_HOUR"] = _base_hour;
        DeltaMachineMeters.Rows[_idx_row].AcceptChanges();
        DeltaMachineMeters.Rows[_idx_row].SetModified();
      }

      return true;
    } // GetDeltaMachineMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update delta Machine meters.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //          - DeltaMachineMeters
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: delta Machine meters Selected from Machine Meters.
    //      - false: error.
    //
    static public Boolean UpdateDeltaMachineMeters(DataTable DeltaMachineMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      int _update_batch_size = 500;
      int _idx_row;
      int _nr;

      //    - Set SqlDataAdapter commands
      //        - SelectCommand 
      //        - DeleteCommand 
      //        - InsertCommand 
      //        - UpdateCommand 
      _da = new SqlDataAdapter();

      //        - UpdateCommand 
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("UPDATE   MACHINE_METERS                                                                                                     ");
      _sql_txt.AppendLine("   SET   MM_DELTA_PLAYED_COUNT                 =  (MM_DELTA_PLAYED_COUNT                - @pDeltaPlayedCount)               ");
      _sql_txt.AppendLine("     ,   MM_DELTA_PLAYED_AMOUNT                =  (MM_DELTA_PLAYED_AMOUNT               - @pDeltaPlayedAmount)              ");
      _sql_txt.AppendLine("     ,   MM_DELTA_WON_COUNT                    =  (MM_DELTA_WON_COUNT                   - @pDeltaWonCount)                  ");
      _sql_txt.AppendLine("     ,   MM_DELTA_WON_AMOUNT                   =  (MM_DELTA_WON_AMOUNT                  - @pDeltaWonAmount)                 ");
      _sql_txt.AppendLine("     ,   MM_DELTA_JACKPOT_AMOUNT               =  (MM_DELTA_JACKPOT_AMOUNT              - @pDeltaJackpotAmount)             ");
      _sql_txt.AppendLine("     ,   MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT   =  (MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT  - @pDeltaProgressiveJackpotAmount)  ");
      _sql_txt.AppendLine("     ,   MM_DELTA_TO_GM_COUNT                  =  (MM_DELTA_TO_GM_COUNT                 - @pDeltaToGmCount)                 ");
      _sql_txt.AppendLine("     ,   MM_DELTA_TO_GM_AMOUNT                 =  (MM_DELTA_TO_GM_AMOUNT                - @pDeltaToGmAmount)                ");
      _sql_txt.AppendLine("     ,   MM_DELTA_FROM_GM_COUNT                =  (MM_DELTA_FROM_GM_COUNT               - @pDeltaFromGmCount)               ");
      _sql_txt.AppendLine("     ,   MM_DELTA_FROM_GM_AMOUNT               =  (MM_DELTA_FROM_GM_AMOUNT              - @pDeltaFromGmAmount)              ");
      _sql_txt.AppendLine("     ,   MM_DELTA_UPDATING                     =  0                                                                         ");
      _sql_txt.AppendLine(" WHERE   MM_TERMINAL_ID     =  @pTerminalId                                                                                 ");
      _sql_txt.AppendLine("   AND   MM_DELTA_UPDATING  =  1                                                                                            ");

      _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
      _da.UpdateCommand.Connection = Trx.Connection;
      _da.UpdateCommand.Transaction = Trx;
      _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      _da.UpdateCommand.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt, 8, "MM_DELTA_PLAYED_COUNT");
      _da.UpdateCommand.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money, 8, "MM_DELTA_PLAYED_AMOUNT");
      _da.UpdateCommand.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt, 8, "MM_DELTA_WON_COUNT");
      _da.UpdateCommand.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money, 8, "MM_DELTA_WON_AMOUNT");
      _da.UpdateCommand.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money, 8, "MM_DELTA_JACKPOT_AMOUNT");
      _da.UpdateCommand.Parameters.Add("@pDeltaProgressiveJackpotAmount", SqlDbType.Money, 8, "MM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT");
      _da.UpdateCommand.Parameters.Add("@pDeltaToGmCount", SqlDbType.BigInt, 8, "MM_DELTA_TO_GM_COUNT");
      _da.UpdateCommand.Parameters.Add("@pDeltaToGmAmount", SqlDbType.Money, 8, "MM_DELTA_TO_GM_AMOUNT");
      _da.UpdateCommand.Parameters.Add("@pDeltaFromGmCount", SqlDbType.BigInt, 8, "MM_DELTA_FROM_GM_COUNT");
      _da.UpdateCommand.Parameters.Add("@pDeltaFromGmAmount", SqlDbType.Money, 8, "MM_DELTA_FROM_GM_AMOUNT");
      _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int, 4, "MM_TERMINAL_ID");

      //        - DeleteCommand & InsertCommand
      _da.DeleteCommand = null;
      _da.InsertCommand = null;

      //    - SetModified
      for (_idx_row = 0; _idx_row < DeltaMachineMeters.Rows.Count; _idx_row++)
      {
        DeltaMachineMeters.Rows[_idx_row].SetModified();
      }

      //    - Execute Update command
      _da.UpdateBatchSize = _update_batch_size;
      _da.ContinueUpdateOnError = true;

      _nr = _da.Update(DeltaMachineMeters);
      if (_nr != DeltaMachineMeters.Rows.Count)
      {
        return false;
      }

      return true;
    } // UpdateDeltaMachineMeters

    #endregion // Statistics

  } // WCP_BU_MachineMeters

} // WSI.WCP