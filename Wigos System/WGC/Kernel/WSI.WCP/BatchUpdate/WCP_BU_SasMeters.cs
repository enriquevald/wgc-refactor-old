//------------------------------------------------------------------------------
// Copyright © 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_BU_SasMeters.cs
// 
//   DESCRIPTION: WCP_BU_SasMeters class; processing of terminal meters
// 
//        AUTHOR: Nelson Madrigal Reyes
// 
// CREATION DATE: 22-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-OCT-2013 NMR    First release.
// 06-NOV-2013 NMR    Messages reorganization
// 08-NOV-2013 NMR    Use indexes for data row access
// 14-NOV-2013 NMR    Loaded SAS meters catalog into memory
// 15-NOV-2013 NMR    Fixed error: passing wrong parameter while updating meters
// 19-NOV-2013 NMR    Improvement: common alarm text and conditions
// 29-NOV-2013 NMR    Don't process meter when property MaxValue is zero
// 04-DIC-2013 NMR    Fixed error when updating SAS meters
// 20-DIC-2013 NMR    All dates in sas_meters + sas_meters_history, are the same in single process
// 21-DIC-2013 NMR    Added exclusive access to db-table sas_meters
// 26-FEB-2014 XIT    Deleted SmartParams uses
// 14-JAN-2015 DRV    Fixed Bug WIG-1905: Error in meter increment
// 16-JAN-2015 ANM    Added informative alarm when First Time in SAS Host
// 26-MAY-2015 MPO    WIG-2256: Incorrect values for SAS Meters
// 07-SEP-2015 MPO    TFS ITEM 2194: SAS16: Estadísticas multi-denominación: WCP
// 20-JUN-2016 FGB    The creation of the SAS Denom Accounting change record, now it is done in WCP_BU_MachineMeters
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.Common.TITO;
using WSI.WCP.WCP_Messages.Meters;

namespace WSI.WCP
{
  class WCP_BU_SasMeters
  {
    // Design Pattern: singleton
    public static WCP_BU_SasMeters Instance = new WCP_BU_SasMeters();

    #region Internal Attributes

    private DataTable m_dt_sas_meters;
    private DataTable m_dt_sas_meters_catalog;    // to preserve meters catalog in memory
    private SqlDataAdapter m_adap_meters_select;
    private SqlDataAdapter m_adap_meters_modify;
    private SqlCommand m_sql_cmd_history;

    private ReaderWriterLock m_rw_lock_queue; // locker for multithreading
    private ReaderWriterLock m_rw_lock_process; // locker for multi-terminal-reports
    private ArrayList m_unload_queue;         // list for unregistered terminals control

    #endregion

    #region Constants

    const String ROLLBACK_POINT = "SAS_METERS_BEFORE_INSERT";
    const Int32 MAX_WAIT_TIME_FOR_LOCK = 3000;  // max wait time for exclusive acces acquisition to procedure 'Process'

    private const Int16 IDX_TERMINAL_ID = 0;
    private const Int16 IDX_SEQUENCE_ID = 1;
    private const Int16 IDX_METER_CODE = 2;
    private const Int16 IDX_GAME_ID = 3;
    private const Int16 IDX_DENOMINATION = 4;
    private const Int16 IDX_LAST_REPORTED = 5;
    private const Int16 IDX_LAST_MODIFIED = 6;
    private const Int16 IDX_METER_VALUE = 7;
    private const Int16 IDX_METER_MAX_VALUE = 8;
    private const Int16 IDX_DELTA_VALUE = 9;
    private const Int16 IDX_RAW_DELTA_VALUE = 10;
    private const Int16 IDX_RESPONSE_CODE = 11;
    private const Int16 IDX_DELTA_UPDATING = 12;
    private const Int16 IDX_SAS_ACCOUNTING_DENOM = 13;
    private const Int16 IDX_METER_SAS_ACCOUNTING_DENOM = 14;


    private const Int16 IDX_CATALOG_METER_CODE = 0;
    private const Int16 IDX_CATALOG_DESCRIPTION = 1;

    #endregion

    #region Constructor + Public Methods

    public WCP_BU_SasMeters()
    {
      m_dt_sas_meters = null;

      m_unload_queue = new ArrayList(10);
      m_rw_lock_queue = new ReaderWriterLock();
      m_rw_lock_process = new ReaderWriterLock();


    }

    public void LockProcess()
    {
      m_rw_lock_process.AcquireWriterLock(Timeout.Infinite);
    }

    public void UnlockProcess()
    {
      m_rw_lock_process.ReleaseWriterLock();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enqueue a petition for unload Terminal data.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //

    // JRM TODO: This method is never called. Is this queue ever populated?
    public void EnqueueUnload(Int32 TerminalId)
    {
      try
      {
        m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);

        m_unload_queue.Add(TerminalId);
      }
      finally
      {
        m_rw_lock_queue.ReleaseWriterLock();
      }

    } // EnqueueUnload

    //------------------------------------------------------------------------------
    // PURPOSE : Process all Meters from Tasks in the array.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Tasks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void Process(ArrayList Tasks, SqlConnection SqlConn)
    {
      SqlTransaction _trx;

      try
      {
        _trx = SqlConn.BeginTransaction();
        if (this.Save(Tasks, _trx))
        {
          _trx.Commit();
        }

        foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
        {
          _wcp_task.SendResponse();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    } // Process

    //------------------------------------------------------------------------------
    // PURPOSE : Process all Meters from Tasks in the array. 
    //
    //  PARAMS :
    //      - INPUT :
    //          - Tasks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public Boolean Save(ArrayList Tasks, SqlTransaction Trx)
    {
      Int32 _tick0;
      Int64 _time_all;
      Int32 _count_processed;
      int _terminal_id;
      string _provider_id;
      string _terminal_name;
      TerminalStatus _status;
      string _server_external_id;
      Currency _sas_account_denom;

      _tick0 = Environment.TickCount;
      _time_all = 0;

      // acquire exclusive access to process, to prevent multi-thread update
      this.LockProcess();

      try
      {
        // checking internal structures; finish if something was wrong
        if (!this.Init())
        {

          return false;
        }

        // load sas meters catalog
        this.DB_LoadSasMetersCatalog(Trx);

        //
        // processing disconnected terminals
        this.UnloadDisconnectedTerminals();

        //
        // save meters in memory
        _count_processed = 0;
        foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
        {
          if (_wcp_task.Request.MsgHeader.MsgType == WSI.WCP.WCP_MsgTypes.WCP_MsgMachineMultiDenomInfo)
          {
            // For all multidenom info msg, store the XML into terminal_denom_game
            // Get terminal id
            if (!Misc.GetTerminalInfo(_wcp_task.Request.MsgHeader.TerminalId,
                                      1,
                                      out _terminal_id,
                                      out _provider_id,
                                      out _terminal_name,
                                      out _status,
                                      out _server_external_id,
                                      out _sas_account_denom,
                                      Trx))
            {
              return false;
            }

            //_wcp_task.Request.MsgHeader.GetSystemTime
            if (!DB_StoreGameDenomInfo(_terminal_id, _wcp_task.Request.MsgHeader.GetSystemTime, _wcp_task.XmlRequest, Trx))
            {
              Log.Warning(String.Format("An error occurred calling DB_StoreGameDenomInfo.  TerminalId: {0}", _wcp_task.Request.MsgHeader.TerminalId));
            }
          }

          _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR, "No information available.");
          if (this.MetersToInMemoryTable(_wcp_task, Trx))
          {
            _count_processed++;
          }
        }

        // save all changes into db 
        if (_count_processed > 0)
        {
          if (!this.DB_Update(Trx))
          {

            return false;
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Warning("Exception: BU_SasMeters.Process");
        Log.Exception(ex);
      }
      finally
      {
        this.UnlockProcess();

        _time_all = Misc.GetElapsedTicks(_tick0);
        if (_time_all > 15000)
        {
          Log.Warning("WCP_BU_SasMeters Save: " + _time_all.ToString());
        }
      }

      return false;
    }

    #endregion

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Initializes internal attributes of the class.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private Boolean Init()
    {
      Boolean _result;
      StringBuilder _sb;
      SqlCommand _sql_cmd_select;
      SqlCommand _sql_cmd_insert;
      SqlCommand _sql_cmd_update;

      if (m_dt_sas_meters != null)
      {
        return true;
      }

      _result = false;

      try
      {
        //
        // Datatable for memory data processing
        m_dt_sas_meters = new DataTable("TERMINAL_SAS_METERS");

        m_dt_sas_meters.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
        m_dt_sas_meters.Columns.Add("SEQUENCE_ID", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("METER_CODE", Type.GetType("System.Int32"));
        m_dt_sas_meters.Columns.Add("GAME_ID", Type.GetType("System.Int32"));
        m_dt_sas_meters.Columns.Add("DENOMINATION", Type.GetType("System.Decimal"));
        m_dt_sas_meters.Columns.Add("LAST_REPORTED", Type.GetType("System.DateTime"));  // nullable
        m_dt_sas_meters.Columns.Add("LAST_MODIFIED", Type.GetType("System.DateTime"));  // nullable
        m_dt_sas_meters.Columns.Add("METER_VALUE", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("METER_MAX_VALUE", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("DELTA_VALUE", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("RAW_DELTA_VALUE", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("RESPONSE_CODE", Type.GetType("System.Object"));
        m_dt_sas_meters.Columns.Add("DELTA_UPDATING", Type.GetType("System.Int32"));
        m_dt_sas_meters.Columns.Add("SAS_ACCOUNTING_DENOM", Type.GetType("System.Decimal"));

        // Primary Key for DataTable SAS meters.
        m_dt_sas_meters.PrimaryKey = new DataColumn[] { m_dt_sas_meters.Columns["TERMINAL_ID"],
                                                        m_dt_sas_meters.Columns["METER_CODE"],
                                                        m_dt_sas_meters.Columns["GAME_ID"],
                                                        m_dt_sas_meters.Columns["DENOMINATION"]
                                                      };
        //
        // SelectCommand for TERMINAL_SAS_METERS, and create SqlDataAdapter ONLY for Select.
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   TSM_TERMINAL_ID                  AS TERMINAL_ID ");
        _sb.AppendLine("       , TSM_WCP_SEQUENCE_ID              AS SEQUENCE_ID ");
        _sb.AppendLine("       , TSM_METER_CODE                   AS METER_CODE ");
        _sb.AppendLine("       , TSM_GAME_ID                      AS GAME_ID ");
        _sb.AppendLine("       , TSM_DENOMINATION                 AS DENOMINATION ");
        _sb.AppendLine("       , TSM_LAST_REPORTED                AS LAST_REPORTED ");
        _sb.AppendLine("       , TSM_LAST_MODIFIED                AS LAST_MODIFIED ");
        _sb.AppendLine("       , TSM_METER_VALUE                  AS METER_VALUE ");
        _sb.AppendLine("       , TSM_METER_MAX_VALUE              AS METER_MAX_VALUE ");
        _sb.AppendLine("       , 0                                AS DELTA_VALUE ");
        _sb.AppendLine("       , 0                                AS RAW_DELTA_VALUE ");
        _sb.AppendLine("       , TE_SAS_ACCOUNTING_DENOM          AS SAS_ACCOUNTING_DENOM ");
        _sb.AppendLine("       , TSM_SAS_ACCOUNTING_DENOM         AS METER_SAS_ACCOUNTING_DENOM ");
        _sb.AppendLine("  FROM   TERMINAL_SAS_METERS ");
        _sb.AppendLine(" INNER   JOIN TERMINALS ON TSM_TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine(" WHERE   TSM_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("   AND   TSM_METER_CODE = CASE WHEN @pMeterCode = 0 THEN TSM_METER_CODE ELSE @pMeterCode END ");
        _sb.AppendLine("ORDER BY TSM_TERMINAL_ID, TSM_METER_CODE, TSM_GAME_ID, TSM_DENOMINATION ");

        _sql_cmd_select = new SqlCommand(_sb.ToString());
        _sql_cmd_select.Parameters.Add("@pTerminalId", SqlDbType.Int);
        _sql_cmd_select.Parameters.Add("@pMeterCode", SqlDbType.Int);

        m_adap_meters_select = new SqlDataAdapter();
        m_adap_meters_select.SelectCommand = _sql_cmd_select;
        m_adap_meters_select.InsertCommand = null;
        m_adap_meters_select.UpdateCommand = null;
        m_adap_meters_select.DeleteCommand = null;

        //
        // insert query to create row in ticket_meters table
        _sb.Length = 0;
        _sb.AppendLine("INSERT INTO   TERMINAL_SAS_METERS       ");
        _sb.AppendLine("            (                           ");
        _sb.AppendLine("              TSM_TERMINAL_ID           ");
        _sb.AppendLine("            , TSM_WCP_SEQUENCE_ID       ");
        _sb.AppendLine("            , TSM_METER_CODE            ");
        _sb.AppendLine("            , TSM_GAME_ID               ");
        _sb.AppendLine("            , TSM_DENOMINATION          ");
        _sb.AppendLine("            , TSM_LAST_REPORTED         ");
        _sb.AppendLine("            , TSM_METER_VALUE           ");
        _sb.AppendLine("            , TSM_METER_MAX_VALUE       ");
        _sb.AppendLine("            , TSM_DELTA_VALUE           ");
        _sb.AppendLine("            , TSM_RAW_DELTA_VALUE       ");
        _sb.AppendLine("            , TSM_DELTA_UPDATING        ");
        _sb.AppendLine("            , TSM_SAS_ACCOUNTING_DENOM  ");
        _sb.AppendLine("            )                           ");
        _sb.AppendLine("    VALUES  (                           ");
        _sb.AppendLine("              @pTerminalId              ");
        _sb.AppendLine("            , @pWcpSequenceId           ");
        _sb.AppendLine("            , @pMeterCode               ");
        _sb.AppendLine("            , @pGameId                  ");
        _sb.AppendLine("            , @pDenomination            ");
        _sb.AppendLine("            , @pLastReported            ");
        _sb.AppendLine("            , @pMeterValue              ");
        _sb.AppendLine("            , @pMeterMaxValue           ");
        _sb.AppendLine("            , @pDeltaValue              ");
        _sb.AppendLine("            , @pRawDeltaValue           ");
        _sb.AppendLine("            , @pDeltaUpdating           ");
        _sb.AppendLine("            , @pSasAccounDenom          ");
        _sb.AppendLine("            )                           ");

        _sql_cmd_insert = new SqlCommand(_sb.ToString());
        _sql_cmd_insert.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
        _sql_cmd_insert.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
        _sql_cmd_insert.Parameters.Add("@pMeterCode", SqlDbType.Int).SourceColumn = "METER_CODE";
        _sql_cmd_insert.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GAME_ID";
        _sql_cmd_insert.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "DENOMINATION";
        _sql_cmd_insert.Parameters.Add("@pLastReported", SqlDbType.DateTime).SourceColumn = "LAST_REPORTED";
        _sql_cmd_insert.Parameters.Add("@pMeterValue", SqlDbType.BigInt).SourceColumn = "METER_VALUE";
        _sql_cmd_insert.Parameters.Add("@pMeterMaxValue", SqlDbType.BigInt).SourceColumn = "METER_MAX_VALUE";
        _sql_cmd_insert.Parameters.Add("@pDeltaValue", SqlDbType.BigInt).SourceColumn = "DELTA_VALUE";
        _sql_cmd_insert.Parameters.Add("@pRawDeltaValue", SqlDbType.BigInt).SourceColumn = "RAW_DELTA_VALUE";
        _sql_cmd_insert.Parameters.Add("@pDeltaUpdating", SqlDbType.Int).SourceColumn = "DELTA_UPDATING";
        _sql_cmd_insert.Parameters.Add("@pSasAccounDenom", SqlDbType.Decimal).SourceColumn = "SAS_ACCOUNTING_DENOM";

        //
        // query to update row in ticket_meters table
        _sb.Length = 0;
        _sb.AppendLine("UPDATE   TERMINAL_SAS_METERS ");
        _sb.AppendLine("   SET   TSM_WCP_SEQUENCE_ID = @pWcpSequenceId ");
        _sb.AppendLine("       , TSM_LAST_REPORTED   = @pLastReported ");
        _sb.AppendLine("       , TSM_LAST_MODIFIED   = @pLastModified ");
        _sb.AppendLine("       , TSM_METER_VALUE     = @pMeterValue ");
        _sb.AppendLine("       , TSM_METER_MAX_VALUE = @pMeterMaxValue ");
        _sb.AppendLine("       , TSM_DELTA_VALUE     = TSM_DELTA_VALUE + @pDeltaValue ");
        _sb.AppendLine("       , TSM_RAW_DELTA_VALUE = TSM_RAW_DELTA_VALUE + @pRawDeltaValue ");
        _sb.AppendLine("       , TSM_SAS_ACCOUNTING_DENOM = @pSasAccounDenom ");
        _sb.AppendLine(" WHERE   TSM_TERMINAL_ID     = @pTerminalId ");
        _sb.AppendLine("   AND   TSM_METER_CODE      = @pMeterCode ");
        _sb.AppendLine("   AND   TSM_GAME_ID         = @pGameId ");
        _sb.AppendLine("   AND   TSM_DENOMINATION    = @pDenomination ");

        _sql_cmd_update = new SqlCommand(_sb.ToString());
        _sql_cmd_update.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
        _sql_cmd_update.Parameters.Add("@pMeterCode", SqlDbType.Int).SourceColumn = "METER_CODE";
        _sql_cmd_update.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GAME_ID";
        _sql_cmd_update.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "DENOMINATION";
        _sql_cmd_update.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
        _sql_cmd_update.Parameters.Add("@pLastReported", SqlDbType.DateTime).SourceColumn = "LAST_REPORTED";
        _sql_cmd_update.Parameters.Add("@pLastModified", SqlDbType.DateTime).SourceColumn = "LAST_MODIFIED";
        _sql_cmd_update.Parameters.Add("@pMeterValue", SqlDbType.BigInt).SourceColumn = "METER_VALUE";
        _sql_cmd_update.Parameters.Add("@pMeterMaxValue", SqlDbType.BigInt).SourceColumn = "METER_MAX_VALUE";
        _sql_cmd_update.Parameters.Add("@pDeltaValue", SqlDbType.BigInt).SourceColumn = "DELTA_VALUE";
        _sql_cmd_update.Parameters.Add("@pRawDeltaValue", SqlDbType.BigInt).SourceColumn = "RAW_DELTA_VALUE";
        _sql_cmd_update.Parameters.Add("@pSasAccounDenom", SqlDbType.Decimal).SourceColumn = "SAS_ACCOUNTING_DENOM";

        m_adap_meters_modify = new SqlDataAdapter();
        m_adap_meters_modify.SelectCommand = null;
        m_adap_meters_modify.InsertCommand = _sql_cmd_insert;
        m_adap_meters_modify.UpdateCommand = _sql_cmd_update;
        m_adap_meters_modify.DeleteCommand = null;
        m_adap_meters_modify.ContinueUpdateOnError = true;
        m_adap_meters_modify.UpdateBatchSize = 500;
        m_adap_meters_modify.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
        m_adap_meters_modify.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        //
        // string builder for history
        //
        _sb.Length = 0;

        _sb.AppendLine(" DECLARE @pSasAccounDenom  MONEY ");
        _sb.AppendLine("  SELECT @pSasAccounDenom = TE_SAS_ACCOUNTING_DENOM ");
        _sb.AppendLine("    FROM TERMINALS ");
        _sb.AppendLine("   WHERE TE_TERMINAL_ID = @pTerminalId ");

        _sb.AppendLine("INSERT INTO   TERMINAL_SAS_METERS_HISTORY ");
        _sb.AppendLine("            ( TSMH_TERMINAL_ID            ");
        _sb.AppendLine("            , TSMH_METER_CODE             ");
        _sb.AppendLine("            , TSMH_GAME_ID                ");
        _sb.AppendLine("            , TSMH_DENOMINATION           ");
        _sb.AppendLine("            , TSMH_TYPE                   ");
        _sb.AppendLine("            , TSMH_DATETIME               ");
        _sb.AppendLine("            , TSMH_METER_INI_VALUE        ");
        _sb.AppendLine("            , TSMH_METER_FIN_VALUE        ");
        _sb.AppendLine("            , TSMH_METER_INCREMENT        ");
        _sb.AppendLine("            , TSMH_RAW_METER_INCREMENT    ");
        _sb.AppendLine("            , TSMH_SAS_ACCOUNTING_DENOM ) ");
        _sb.AppendLine("    VALUES  ( @pTerminalId                ");
        _sb.AppendLine("            , @pMeterCode                 ");
        _sb.AppendLine("            , @pGameId                    ");
        _sb.AppendLine("            , @pDenomination              ");
        _sb.AppendLine("            , @pType                      ");
        _sb.AppendLine("            , @pDateTime                  ");
        _sb.AppendLine("            , @pMeterIniValue             ");
        _sb.AppendLine("            , @pMeterFinValue             ");
        _sb.AppendLine("            , @pMeterIncrement            ");
        _sb.AppendLine("            , @pRawMeterIncrement         ");
        _sb.AppendLine("            , @pSasAccounDenom )          ");

        m_sql_cmd_history = new SqlCommand(_sb.ToString());
        m_sql_cmd_history.Parameters.Add("@pTerminalId", SqlDbType.Int);
        m_sql_cmd_history.Parameters.Add("@pMeterCode", SqlDbType.Int);
        m_sql_cmd_history.Parameters.Add("@pGameId", SqlDbType.Int);
        m_sql_cmd_history.Parameters.Add("@pDenomination", SqlDbType.Decimal);
        m_sql_cmd_history.Parameters.Add("@pType", SqlDbType.Int);
        m_sql_cmd_history.Parameters.Add("@pDateTime", SqlDbType.DateTime);
        m_sql_cmd_history.Parameters.Add("@pMeterIniValue", SqlDbType.BigInt);
        m_sql_cmd_history.Parameters.Add("@pMeterFinValue", SqlDbType.BigInt);
        m_sql_cmd_history.Parameters.Add("@pMeterIncrement", SqlDbType.BigInt);
        m_sql_cmd_history.Parameters.Add("@pRawMeterIncrement", SqlDbType.BigInt);

        _result = true;
      }
      catch (Exception _ex)
      {
        m_dt_sas_meters = null;
        Log.Warning("Exception: BU_SasMeters.Init");
        Log.Exception(_ex);
      }

      return _result;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Load SAS meters catalog from DB to DataTable Cache
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void DB_LoadSasMetersCatalog(SqlTransaction Trx)
    {
      StringBuilder _sb;

      if (m_dt_sas_meters_catalog == null)
      {
        _sb = new StringBuilder();
        try
        {
          m_dt_sas_meters_catalog = new DataTable("SAS_METERS_CATALOG");

          // load data from BD
          _sb.AppendLine("SELECT   SMC_METER_CODE ");
          _sb.AppendLine("       , SMC_DESCRIPTION ");
          _sb.AppendLine("  FROM   SAS_METERS_CATALOG ");
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            using (SqlDataAdapter _sql_adap = new SqlDataAdapter(_sql_cmd))
            {
              _sql_adap.Fill(m_dt_sas_meters_catalog);
            }
          }
        }
        catch (Exception _ex)
        {
          m_dt_sas_meters_catalog.Clear();
          m_dt_sas_meters_catalog = null;
          Log.Exception(_ex);
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get and compose meter description for Logging purpouses
    //
    //  PARAMS :
    //      - INPUT :
    //          - MeterCode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private String ComposeMeterDescription(Int32 MeterCode)
    {
      String _result;
      DataRow[] _term_rows;
      DataRow _row;

      _term_rows = m_dt_sas_meters_catalog.Select(" SMC_METER_CODE = " + MeterCode);
      if (_term_rows.Length > 0)
      {
        _row = _term_rows[0];
        _result = String.Format("0x{0:X4} - {1}", (Int32)_row[IDX_CATALOG_METER_CODE], _row[IDX_CATALOG_DESCRIPTION]);
      }
      else
      {
        _result = String.Format("0x{0:X4}", MeterCode);
      }

      return _result;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Load Terminal meters data from DB to DataTable Cache
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void DB_Load(Int32 TerminalId, SqlTransaction Trx)
    {
      Int32 _now_ticks;
      Int64 _elapsed_ticks;

      _now_ticks = Environment.TickCount;

      m_adap_meters_select.SelectCommand.Connection = Trx.Connection;
      m_adap_meters_select.SelectCommand.Transaction = Trx;
      m_adap_meters_select.SelectCommand.Parameters["@pTerminalId"].Value = TerminalId;
      m_adap_meters_select.SelectCommand.Parameters["@pMeterCode"].Value = 0;
      m_adap_meters_select.Fill(m_dt_sas_meters);

      _elapsed_ticks = Misc.GetElapsedTicks(_now_ticks);

    } // Load

    //------------------------------------------------------------------------------
    // PURPOSE : Check if is a pulse counting device meter, and translate it to a 
    //           SAS meter
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void TranslatePCDMeters(Int32 TerminalId, ref ArrayList Meters, SqlTransaction Trx)
    {
      const Int32 PCD_METER_MASK = 0x2000;
      SAS_Meter _translated_meter;
      ArrayList _new_meters;
      StringBuilder _sb;
      DataTable _dt_meters_translate;
      DataRow[] _dr_meter_result;

      _sb = new StringBuilder();
      _new_meters = new ArrayList();
      _dt_meters_translate = null;

      foreach (SAS_Meter _meter in Meters)
      {
        if ((_meter.Code & PCD_METER_MASK) == PCD_METER_MASK)
        {
          // Is PCD reported counter
          _translated_meter = new SAS_Meter(_meter.Code, _meter.GameId, _meter.DenomCents, _meter.Value, _meter.MaxValue);

          // Get meter from table pcd_translated meters
          if (_dt_meters_translate == null)
          {
            try
            {
              _dt_meters_translate = new DataTable();
              // load data from BD
              _sb.AppendLine("SELECT   PMT_TERMINAL_ID "); // 0
              _sb.AppendLine("       , PMT_METER_CODE ");  // 1            
              _sb.AppendLine("       , PMT_METER_DENOMINATION "); // 2
              _sb.AppendLine("       , PMT_SAS_METER "); // 3
              _sb.AppendLine("  FROM   PCD_METERS_TRANSLATION ");
              _sb.AppendLine(" WHERE   PMT_TERMINAL_ID IN (0, @p1) ");
              _sb.AppendLine("   AND   PMT_METER_STATUS = 1 ");

              using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
              {
                _sql_cmd.Parameters.Add("@p1", SqlDbType.Int).Value = TerminalId;

                using (SqlDataAdapter _sql_adap = new SqlDataAdapter(_sql_cmd))
                {

                  _sql_adap.Fill(_dt_meters_translate);
                }
              }
            }
            catch (Exception _ex)
            {
              _dt_meters_translate.Dispose();
              _dt_meters_translate = null;
              Log.Exception(_ex);
              continue;
            }
          }

          _sb.Length = 0;

          _dr_meter_result = _dt_meters_translate.Select("PMT_METER_CODE = " + _meter.Code, "PMT_TERMINAL_ID DESC");

          if (_dr_meter_result.Length <= 0)
          {
            // No need to be translated
            continue;
          }

          _translated_meter.Code = (Int32)_dr_meter_result[0][3];
          _translated_meter.DenomCents = (Decimal)_dr_meter_result[0][2];

          _new_meters.Add(_translated_meter);
        }
      }

      foreach (SAS_Meter _meter in _new_meters)
      {
        Meters.Add(_meter);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Process task with SAS meters list
    //           The steps here are:
    //            - Translate WCP_MsgXXX to local structure
    //            - Check / load existing metrics for terminal/code
    //              - Insert if row not exists
    //                - get terminal identifier from TerminalSession
    //                - insert row in meters table for terminal/code
    //            - check if sequence is correct; return if not
    //            - check if are changes in received data; return if not
    //            - check if for increment for each pair VALUE+MAX is too big
    //              - update without DELTA, if is too big
    //              - calculate DELTA columns and save, if not
    //            - Save all changes
    //
    //  PARAMS :
    //      - INPUT :
    //          - WcpTask: message with list of meters to be updated
    //          - Trx: common transaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - TRUE if some meter was stored to global DataTable
    //
    private Boolean MetersToInMemoryTable(WcpInputMsgProcessTask WcpTask, SqlTransaction Trx)
    {
      Boolean _result;
      Boolean _is_big_increment;
      Boolean _is_first_time;
      Int32 _meters_processed;
      Int32 _terminal_id;
      Int64 _delta_value;
      Int64 _raw_delta_value;
      Int64 _max_units_increment;
      DateTime _time_of_process;
      ENUM_SAS_METER_HISTORY _sas_meter_history_type;
      String _provider_id;
      String _terminal_name;
      StringBuilder _sb;
      DataRow _meter_db_row;
      WCP_Message _wcp_request;
      ArrayList _meters_list;
      SAS_Meter _new_meter;
      SAS_Meter _old_meter;
      Boolean _change_stacker;
      StringBuilder _sb_stacker;
      Boolean _process_denom;
      Decimal _sas_account_denom;
      Decimal _meter_sas_account_denom;

      _result = false;
      _meters_processed = 0;
      _meters_list = null;
      _time_of_process = WGDB.Now;
      _wcp_request = WcpTask.Request;
      _change_stacker = false;
      _sb_stacker = null;
      _process_denom = false;
      _is_first_time = false;
      _sas_account_denom = 0;
      _meter_sas_account_denom = 0;

      try
      {
        _terminal_id = ((SecureTcpClient)WcpTask.WcpClient).InternalId;

        switch (_wcp_request.MsgHeader.MsgType)
        {
          case WCP_MsgTypes.WCP_MsgSasMeters:
            _meters_list = ((WCP_MsgSasMeters)_wcp_request.MsgContent).Meters;
            //Log.Warning("Reported meters before:" + _meters_list.Count);
            //TranslatePCDMeters(_terminal_id, ref _meters_list, Trx);
            //Log.Warning("Reported meters after:" + _meters_list.Count);
            break;

          case WCP_MsgTypes.WCP_MsgCashSessionPendingClosing:
            _meters_list = ((WCP_MsgCashSessionPendingClosing)_wcp_request.MsgContent).LastSasMeters;
            _change_stacker = true;
            _sb_stacker = new StringBuilder();
            _sb_stacker.Append("CHANGE_STACKER(T: " + _terminal_id.ToString("D5") + ") <METERS> ");
            break;

          case WCP_MsgTypes.WCP_MsgReportHPCMeter:
            _meters_list = ((WCP_MsgReportHPCMeter)_wcp_request.MsgContent).SasMeters;
            break;

          case WCP_MsgTypes.WCP_MsgMachineMultiDenomInfo:
            ArrayList _denom_list = ((WCP_MsgMachineMultiDenomInfo)_wcp_request.MsgContent).DenominationList;

            _meters_list = new ArrayList();

            foreach (WCP_MsgMachineMultiDenomInfo.Denomination _denom in _denom_list)
            {
              _meters_list.AddRange(_denom.MeterList);
            }

            _process_denom = true;

            break;

          default:
            return false;
        }

        _old_meter = new SAS_Meter();
        _sb = new StringBuilder();

        //
        // inner cycle for single meter treatment
        //
        foreach (SAS_Meter _meter in _meters_list)
        {
          _sas_meter_history_type = ENUM_SAS_METER_HISTORY.NONE;

          //int _ticks;
          //_ticks = Environment.TickCount;

          // don't process meter when MaxValue is zero
          if (_meter.MaxValue == 0)
          {
            _sb.Length = 0;
            _sb.AppendLine("*** This meter is discarded because MaxValue == 0 ***");
            _sb.AppendLine("   Meter: " + this.ComposeMeterDescription(_meter.Code));
            _sb.AppendLine("   Meter value: " + _meter.Value);
            Log.Warning(" TerminalId: " + _terminal_id.ToString() + " " + _sb.ToString());

            _result = true;
            continue;
          }

          if (_meter.GameId != 0)
          {
            _sb.Length = 0;
            _sb.AppendLine("*** This meter is discarded because GameId <> 0 ***");
            _sb.AppendLine("   Meter: " + this.ComposeMeterDescription(_meter.Code));
            _sb.AppendLine("   Meter value: " + _meter.Value);
            Log.Warning(" TerminalId: " + _terminal_id.ToString() + " " + _sb.ToString());

            _result = true;
            continue;
          }

          if (!_process_denom)
          {
            if (_meter.DenomCents != 0)
            {
              _sb.Length = 0;
              _sb.AppendLine("*** This meter is discarded because DenomCents <> 0 ***");
              _sb.AppendLine("   Meter: " + this.ComposeMeterDescription(_meter.Code));
              _sb.AppendLine("   Meter value: " + _meter.Value);
              Log.Warning(" TerminalId: " + _terminal_id.ToString() + " " + _sb.ToString());

              _result = true;
              continue;
            }

            Boolean _next_meter;

            _next_meter = false;
            for (Int32 _i = 0; _i < WCP_BU_MachineMeters.MetersReportedByMM.Length; _i++)
            {
              if (WCP_BU_MachineMeters.MetersReportedByMM[_i] == _meter.Code)
              {
                _sb.Length = 0;
                _sb.AppendLine("*** This meter is discarded because is reported through MM ***");
                _sb.AppendLine("   Meter: " + this.ComposeMeterDescription(_meter.Code));
                _sb.AppendLine("   Meter value: " + _meter.Value);
                Log.Warning(" TerminalId: " + _terminal_id.ToString() + " " + _sb.ToString());

                _next_meter = true;
                _result = true;
                break;
              }
            }

            if (_next_meter)
            {
              continue;
            }
          }

          //System.Diagnostics.Debug.Print("Time elapsed: " + Misc.GetElapsedTicks(_ticks));

          if (_change_stacker)
          {
            _sb_stacker.Append(String.Format("0x{0} = {1}; ", _meter.Code.ToString("X2"), _meter.Value.ToString("D8")));
          }

          // is necessary the copy of Ptr, for properties modification
          _new_meter = _meter;
          _new_meter.TimeOfProcess = _time_of_process;

          // load row from DataTable cache. If it does not exist, then insert
          _is_first_time = this.MemoryTableLoadMeterFromDatabase(_terminal_id, _new_meter, ref _old_meter, out _meter_db_row, Trx);
          if (_is_first_time)
          {
            if (this.MemoryTableAddMeter(_terminal_id, _wcp_request.MsgHeader.SequenceId, _new_meter, WcpTask))
            {
              _meters_processed++;
              _result = true;
            }
            else
            {
              Log.Warning("BU_SasMeters.ProcessTask" +
                          ", Insert failed. TerminalId/SessionId/SequenceId/Meter=" +
                          _terminal_id + "/" +
                          _wcp_request.MsgHeader.TerminalSessionId + "/" +
                          _wcp_request.MsgHeader.SequenceId + "/" +
                          this.ComposeMeterDescription(_new_meter.Code));
            }

            //TODO BATCH UPDATE en el "BatchUpdate" del Final
            this.DB_InsertIntoHistory(_terminal_id,
                                      ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME,
                                      0, //OldValue
                                      0, //DeltaValue
                                      0, //RawDeltaValue
                                      _new_meter,
                                      Trx);

            //Add alarm when first time
            _sb.Length = 0;
            _sb.AppendLine("*** SAS Meters First Time ***");
            Common.BatchUpdate.TerminalSession.GetData(_terminal_id, _wcp_request.MsgHeader.TerminalSessionId,
                                                         out _provider_id, out _terminal_name);
            Alarm.Register(AlarmSourceCode.TerminalSASHost,
                     _terminal_id,
                     Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                     AlarmCode.TerminalSystem_SasHostFirstTime,
                     _sb.ToString());
            //

            continue;
          }

          //
          // row for terminal/meterCode was found, then update it
          //

          //
          // If message arrived is previous than meter in DB, don't treat it,
          // but mark Task as OK and continue with list
          if (_old_meter.SequenceId >= _wcp_request.MsgHeader.SequenceId)
          {
            _result = true;

            continue;
          }

          if (_new_meter.Code == 0x3E || _new_meter.Code == 0x3F || _new_meter.Code == 0x0C)
          {
            // filtered meters: only save them !!!
            // 0x3E = 62: MeterCode: Number of bills currently in the stacker (Issue exception 7B when this meter is reset)
            // 0x3F = 63: MeterCode: Total value of bills currently in the stacker (credits) (Issue exception 7B when this meter is reset)
            // 0x0C = 12: MeterCode: Current credits
            this.MemoryTableMeterToMeterRow(_terminal_id,
                                        _wcp_request.MsgHeader.SequenceId,
                                        _new_meter,
                                        0, //delta_value
                                        0, //raw_delta_value
                                        _meter_db_row,
                                        WcpTask);
            _meters_processed++;
            _result = true;

            continue;
          }

          // compare old data vs new data
          if (_new_meter.Value == _old_meter.Value &&
              _new_meter.MaxValue == _old_meter.MaxValue &&
              _sas_account_denom == _meter_sas_account_denom &&
              !GeneralParam.GetBoolean("Alarms", "Terminal.DailyUnreportedMeters"))
          {
            // No changes in Meters. Mark Task as OK and continue with list
            _result = true;
            continue;
          }

          _delta_value = 0;
          _raw_delta_value = 0;

          _is_big_increment = false;

          if (!_meter.IgnoreDeltas)
          {
            //
            // Calculate increments (delta values); method return boolean flag BigIncrement
            // if for some pair increment is too big. In this case, data aren't saved and
            // alarm is stored
            SAS_Meter.GetBigIncrement(_new_meter.Code, out _max_units_increment, _terminal_id);

            // calculate delta without rollover
            _raw_delta_value = this.GetRawDeltaMeter(_old_meter.Value, _new_meter.Value, _new_meter.MaxValue);

            // calculate delta with rollover
            _is_big_increment = TerminalMeterGroup.GetDeltaMeter(_terminal_id,
                                                                   this.ComposeMeterDescription(_new_meter.Code),
                                                                   _old_meter.Value,
                                                                   _new_meter.Value,
                                                                   _old_meter.MaxValue,
                                                                   _new_meter.MaxValue,
                                                                   _max_units_increment,
                                                                   out _delta_value);
          }

          if (_is_big_increment)
          {
            _sb.Length = 0;
            _sb.AppendLine("*** SAS Meters BigIncrement ***");
            _sb.AppendLine("   Meter: " + this.ComposeMeterDescription(_new_meter.Code));
            _sb.AppendLine("   Old meter value: " + _old_meter.Value);
            _sb.AppendLine("   New meter value: " + _new_meter.Value);

            Common.BatchUpdate.TerminalSession.GetData(_terminal_id, _wcp_request.MsgHeader.TerminalSessionId,
                                                       out _provider_id, out _terminal_name);
            Alarm.Register(AlarmSourceCode.TerminalSystem,
                           _terminal_id,
                           Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                           AlarmCode.TerminalSystem_SasMeterBigIncrement,
                           _sb.ToString());

            TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                              , _terminal_id
                                              , _new_meter.Code
                                              , _new_meter.GameId
                                              , (Int32)(_new_meter.DenomCents)
                                              , WGDB.Now
                                              , TerminalMeterGroup.MetersAdjustmentsType.System
                                              , TerminalMeterGroup.MetersAdjustmentsSubType.System_SAS_BigIncrement
                                              , _old_meter.Value
                                              , _new_meter.Value
                                              , _delta_value
                                              , "BigIncrement: SAS Meter BigIncrement");

            _delta_value = 0;

            _sas_meter_history_type = ENUM_SAS_METER_HISTORY.TSMH_METER_DISCARDED;
          }

          if (_old_meter.MaxValue != _new_meter.MaxValue)
          {
            String _meter_name;

            _meter_name = this.ComposeMeterDescription(_new_meter.Code);

            _sb.Length = 0;
            _sb.AppendLine("Max Value Modified. TerminalId: " + _terminal_id.ToString());
            _sb.Append(", MeterName: " + _meter_name);
            _sb.Append(", New: " + _new_meter.MaxValue.ToString());
            _sb.Append(", Old: " + _old_meter.MaxValue.ToString());
            _sb.Append(", New Meter: " + _new_meter.Value.ToString());
            _sb.Append(", Old Meter: " + _old_meter.Value.ToString());

            Log.Warning(_sb.ToString());

            _sb.Length = 0;
            _sb.AppendLine("*** SAS Meters Max Value Modified ***");
            _sb.AppendLine("   Meter: " + _meter_name);
            _sb.AppendLine("   Old max value: " + _old_meter.MaxValue.ToString());
            _sb.AppendLine("   New max value: " + _new_meter.MaxValue.ToString());
            _sb.AppendLine("   New Meter: " + _new_meter.Value.ToString());
            _sb.AppendLine("   Old Meter: " + _old_meter.Value.ToString());

            Common.BatchUpdate.TerminalSession.GetData(_terminal_id, _wcp_request.MsgHeader.TerminalSessionId,
                                                       out _provider_id, out _terminal_name);

            Alarm.Register(AlarmSourceCode.TerminalSystem,
                           _terminal_id,
                           Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                           AlarmCode.TerminalSystem_MaxValueModified,
                           _sb.ToString());

          }

          // Insert alarm for each bill accepted on the terminal
          if (_meter.Code >= 0x40 && _meter.Code <= 0x57)
          {
            long _idx_bill;
            String _source_name;
            String _alarm;

            Common.BatchUpdate.TerminalSession.GetData(_terminal_id, _wcp_request.MsgHeader.TerminalSessionId,
                                                       out _provider_id, out _terminal_name);

            _source_name = WSI.Common.Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);
            _alarm = Resource.String("STR_ALARM_BILL_ACCEPTED", ((Currency)SAS_Meter.GetBillDenomination(_meter.Code)).ToString());

            for (_idx_bill = 0; _idx_bill < _delta_value; _idx_bill++)
            {
              Alarm.Register(AlarmSourceCode.TerminalSystem,
                             _terminal_id,
                             _source_name,
                             (UInt32)AlarmCode.TerminalSystem_BillInserted,
                             _alarm,
                             AlarmSeverity.Info,
                             _wcp_request.MsgHeader.GetSystemTime,
                             Trx);
            }
          }

          //
          // save the changes to temporary DataTable
          //
          if (this.MemoryTableMeterToMeterRow(_terminal_id,
                                  _wcp_request.MsgHeader.SequenceId,
                                  _new_meter,
                                  _delta_value,
                                  _raw_delta_value,
                                  _meter_db_row,
                                  WcpTask))
          {
            _meters_processed++;
            _result = true;
          }
          else
          {
            Log.Warning("BU Updating Meters Fails" +
                        ". TerminalId: " + _terminal_id +
                        ", WcpSequenceId: " + _wcp_request.MsgHeader.SequenceId +
                        ", Meter: " + this.ComposeMeterDescription(_new_meter.Code));
          }

          //
          // now, data will stored in history table. Conditions to store are:
          // IF Value == 0 AND ( (OldValue <> 0) || (!OldValue) )
          //   INSERT TERMINAL_SAS_METERS_HISTORY   "Event-MeterReset"
          // IF Value < OldValue 
          //   INSERT TERMINAL_SAS_METERS_HISTORY   "Event-MeterRollover"
          if (_sas_meter_history_type == ENUM_SAS_METER_HISTORY.NONE)
          {
            if (_new_meter.Value == 0 && _old_meter.Value != 0)
            {
              _sas_meter_history_type = ENUM_SAS_METER_HISTORY.TSMH_METER_RESET;
            }
            else if (_new_meter.Value < _old_meter.Value)
            {
              _sas_meter_history_type = ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER;
              String _meter_name;

              _meter_name = this.ComposeMeterDescription(_new_meter.Code);

              _sb.Length = 0;
              _sb.AppendLine("*** SAS Meters Rollover ***");
              _sb.AppendLine("   Meter: " + _meter_name);
              _sb.AppendLine("   New Meter: " + _new_meter.Value.ToString());
              _sb.AppendLine("   Old Meter: " + _old_meter.Value.ToString());

              Common.BatchUpdate.TerminalSession.GetData(_terminal_id, _wcp_request.MsgHeader.TerminalSessionId,
                                                         out _provider_id, out _terminal_name);

              Alarm.Register(AlarmSourceCode.TerminalSystem,
                             _terminal_id,
                             Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                             (UInt32)AlarmCode.TerminalSystem_MeterRollover,
                             _sb.ToString(),
                             AlarmSeverity.Info);

              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , _terminal_id
                                                        , _new_meter.Code
                                                        , _new_meter.GameId
                                                        , (Int32)_new_meter.DenomCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_SAS_Rollover
                                                        , _old_meter.Value
                                                        , _new_meter.Value
                                                        , 0
                                                        , "Rollover: SAS Meter Rollover");
            }
          }

          if (_sas_meter_history_type != ENUM_SAS_METER_HISTORY.NONE)
          {
            this.DB_InsertIntoHistory(_terminal_id,
                                      _sas_meter_history_type,
                                      _old_meter.Value,
                                      _delta_value,
                                      _raw_delta_value,
                                      _new_meter,
                                      Trx);
          }

          // end history
        }
      }
      catch (Exception ex)
      {
        Log.Warning("Exception: BU_SasMeters.ProcessTasks");
        Log.Exception(ex);
      }

      //
      // finally, if nothing was processed and someting was in the meters list, OK result is returned
      if ((_meters_list.Count > 0 && _meters_processed == 0) || WcpTask.Request.MsgHeader.MsgType == WSI.WCP.WCP_MsgTypes.WCP_MsgMachineMultiDenomInfo)
      {
        WcpTask.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
      }
      //

      if (_change_stacker)
      {
        Log.Message(_sb_stacker.ToString());
      }

      return _result;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get SAS Meters from DataTable Cache, if exists
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId: ID of terminal
    //          - Code: meter code (hexa?)
    //          - Trx
    //
    //      - OUTPUT :
    //          - OldSasMeter: meter for this code
    //          - Row: row from data where read
    //          - Trx: common transaction
    //
    // RETURNS :
    //      - true: successful.
    //      - false: not found or error.
    //
    private Boolean MemoryTableLoadMeterFromDatabase(Int32 TerminalId, SAS_Meter NewMeter, ref SAS_Meter OldMeter, out DataRow Row, SqlTransaction Trx)
    {
      String _filter;
      DataRow[] _term_rows;

      Row = null;

      try
      {
        _filter = "      TERMINAL_ID = " + TerminalId;
        _filter += " AND METER_CODE = " + NewMeter.Code;
        _filter += " AND GAME_ID = " + NewMeter.GameId;
        _filter += " AND DENOMINATION = " + Format.LocalNumberToDBNumber(NewMeter.DenomCents.ToString());
        _term_rows = null;
        //
        // two times tries read data from BD
        for (Int32 _idx_tries = 0; _idx_tries < 2; _idx_tries++)
        {
          _term_rows = m_dt_sas_meters.Select(_filter);
          if (_term_rows.Length == 0)
          {
            if (_idx_tries == 0)
            {
              DB_Load(TerminalId, Trx);
            }
            else
            {
              // stop process after second try-time
              break;
            }
          }
          else
          {
            Row = _term_rows[0];
            break;
          }
        }

        if (_term_rows != null && _term_rows.Length > 1)
        {
          Log.Warning("BU_SasMeters.DB_Load: Too many meters" +
                      ". TerminalId: " + TerminalId +
                      ", Code: " + this.ComposeMeterDescription(NewMeter.Code) +
                      ". Count: " + _term_rows.Length + ".");
        }

        if (Row != null)
        {
          OldMeter.Code = (Int32)Row[IDX_METER_CODE];
          OldMeter.SequenceId = (Int64)Row[IDX_SEQUENCE_ID];
          OldMeter.GameId = (Int32)Row[IDX_GAME_ID];
          OldMeter.DenomCents = (Decimal)Row[IDX_DENOMINATION];
          OldMeter.Value = (Int64)Row[IDX_METER_VALUE];
          OldMeter.MaxValue = (Int64)Row[IDX_METER_MAX_VALUE];
        }
      }
      catch (Exception _ex)
      {
        Row = null;
        Log.Exception(_ex);
      }

      return Row == null;

    } // DB_Load

    //------------------------------------------------------------------------------
    // PURPOSE : store received Meters into DataTable Cache.
    //           OJO: there is no try-catch in the code; caller method is responsible
    //                of capture
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - Meter: Wcp item with SAS meter (specific meter)
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: ticket meters inserted.
    //      - false: error.
    //
    private Boolean MemoryTableAddMeter(Int32 TerminalId,
                              Int64 WcpSequenceId,
                              SAS_Meter Meter,
                              IWcpTaskStatus WcpTask)
    {
      Boolean _result;
      DataRow _new_row;
      Int32 _ticks;
      Int64 _elapsed_ticks;

      _ticks = Environment.TickCount;
      _result = false;

      _new_row = m_dt_sas_meters.NewRow();
      _new_row[IDX_TERMINAL_ID] = TerminalId;
      _new_row[IDX_SEQUENCE_ID] = WcpSequenceId;
      _new_row[IDX_METER_CODE] = Meter.Code;
      _new_row[IDX_GAME_ID] = Meter.GameId;
      _new_row[IDX_DENOMINATION] = Meter.DenomCents;
      _new_row[IDX_LAST_REPORTED] = Meter.TimeOfProcess;
      _new_row[IDX_METER_VALUE] = Meter.Value;
      _new_row[IDX_METER_MAX_VALUE] = Meter.MaxValue;
      _new_row[IDX_DELTA_VALUE] = 0;
      _new_row[IDX_RAW_DELTA_VALUE] = 0;
      _new_row[IDX_RESPONSE_CODE] = WcpTask;
      _new_row[IDX_DELTA_UPDATING] = 0;

      m_dt_sas_meters.Rows.Add(_new_row);

      _result = true;
      _elapsed_ticks = Misc.GetElapsedTicks(_ticks);

      return _result;
    } // DB_Insert

    //------------------------------------------------------------------------------
    // PURPOSE : Update into DataTable Rows the given Meter.
    //           TODO: there is no try-catch in the code; caller method is responsible
    //                of capture
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data was updated correctly or not.
    //
    private Boolean MemoryTableMeterToMeterRow(Int32 TerminalId,
                                        Int64 WcpSequenceId,
                                        SAS_Meter NewMeter,
                                        Int64 DeltaValue,
                                        Int64 RawDeltaValue,
                                        DataRow SasMeterRow,
                                        IWcpTaskStatus WcpTask)
    {
      try
      {
        SasMeterRow.BeginEdit();
        SasMeterRow[IDX_TERMINAL_ID] = TerminalId;
        SasMeterRow[IDX_SEQUENCE_ID] = WcpSequenceId;
        SasMeterRow[IDX_METER_CODE] = NewMeter.Code;
        SasMeterRow[IDX_GAME_ID] = NewMeter.GameId;
        SasMeterRow[IDX_DENOMINATION] = NewMeter.DenomCents;
        SasMeterRow[IDX_LAST_REPORTED] = NewMeter.TimeOfProcess;
        if (DeltaValue > 0)
        {
          SasMeterRow[IDX_LAST_MODIFIED] = NewMeter.TimeOfProcess;
        }
        SasMeterRow[IDX_METER_VALUE] = NewMeter.Value;
        SasMeterRow[IDX_METER_MAX_VALUE] = NewMeter.MaxValue;

        SasMeterRow[IDX_DELTA_VALUE] = (Int64)SasMeterRow[IDX_DELTA_VALUE] + DeltaValue;
        SasMeterRow[IDX_RAW_DELTA_VALUE] = (Int64)SasMeterRow[IDX_RAW_DELTA_VALUE] + RawDeltaValue;

        SasMeterRow[IDX_RESPONSE_CODE] = WcpTask;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        try
        {
          SasMeterRow.EndEdit();
        }
        catch { }
      }

      return false;
    } // DB_UpdateMeter

    //------------------------------------------------------------------------------
    // PURPOSE : store received Meters into DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - Meter: Wcp item with SAS meter (specific meter)
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: ticket meters inserted.
    //      - false: error.
    //
    private Boolean DB_InsertIntoHistory(Int32 TerminalId,
                                         ENUM_SAS_METER_HISTORY HistoryType,
                                         Int64 OldValue,
                                         Int64 DeltaValue,
                                         Int64 RawDeltaValue,
                                         SAS_Meter Meter,
                                         SqlTransaction Trx)
    {
      Boolean _result;
      Int32 _ticks;
      Int64 _elapsed_ticks;

      _ticks = Environment.TickCount;
      _result = false;

      try
      {
        m_sql_cmd_history.Connection = Trx.Connection;
        m_sql_cmd_history.Transaction = Trx;

        m_sql_cmd_history.Parameters["@pTerminalId"].Value = TerminalId;
        m_sql_cmd_history.Parameters["@pMeterCode"].Value = Meter.Code;
        m_sql_cmd_history.Parameters["@pGameId"].Value = Meter.GameId;
        m_sql_cmd_history.Parameters["@pDenomination"].Value = Meter.DenomCents;
        m_sql_cmd_history.Parameters["@pType"].Value = (Int16)HistoryType;
        m_sql_cmd_history.Parameters["@pDateTime"].Value = Meter.TimeOfProcess;
        m_sql_cmd_history.Parameters["@pMeterIniValue"].Value = OldValue;
        m_sql_cmd_history.Parameters["@pMeterFinValue"].Value = Meter.Value;
        m_sql_cmd_history.Parameters["@pMeterIncrement"].Value = DeltaValue;
        m_sql_cmd_history.Parameters["@pRawMeterIncrement"].Value = RawDeltaValue;

        _result = m_sql_cmd_history.ExecuteNonQuery() == 1;
      }
      catch (Exception _ex)
      {
        Log.Warning("BU_SasMeters.DB_InsertIntoHistory. Inserting history");
        Log.Exception(_ex);
      }
      finally
      {
        _elapsed_ticks = Misc.GetElapsedTicks(_ticks);

      }

      return _result;
    } // DB_Insert


    //------------------------------------------------------------------------------
    // PURPOSE : Get RAW delta of one SAS Meter 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OldMeter
    //          - NewMeter
    //          - NewMeterMaxValue
    //
    //      - OUTPUT :
    //          - DeltaMeterValue
    //
    // RETURNS :
    //      - Int64: A RAW delta value
    //
    //   NOTES :
    //
    private Int64 GetRawDeltaMeter(Int64 OldMeterValue,
                                   Int64 NewMeterValue,
                                   Int64 NewMeterMaxValue)
    {
      Int64 _result;

      _result = 0;

      if (NewMeterValue >= OldMeterValue)
      {
        _result = NewMeterValue - OldMeterValue;
      }
      else
      {
        _result = NewMeterValue + (NewMeterMaxValue - OldMeterValue);
      }

      return _result;
    } // GetDeltaMeter
    //------------------------------------------------------------------------------
    // PURPOSE : Process the queue with terminals checked as disconnected
    //           (unload its data from DataTable)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - INT32: the number of processed/unloaded terminals
    //
    private Int32 UnloadDisconnectedTerminals()
    {
      Int32 _result;
      String _filter;

      _result = 0;

      try
      {
        m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);

        foreach (Int32 _terminal_id in m_unload_queue)
        {
          _filter = "TERMINAL_ID = " + _terminal_id;
          if (this.DB_Unload(_filter))
            _result++;
        }
        m_unload_queue.Clear();
      }
      catch (Exception ex)
      {
        Log.Warning("Exception: BU_SasMeters.UnloadDisconnectedTerminals");
        Log.Exception(ex);
      }
      finally
      {
        m_rw_lock_queue.ReleaseWriterLock();
      }

      return _result;

    } // UnloadDisconnectedTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows based on Filter from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Filter: selection of rows to be removed
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private Boolean DB_Unload(String Filter)
    {
      Boolean _result;
      DataRow[] _term_rows;

      _result = false;

      _term_rows = m_dt_sas_meters.Select(Filter);
      foreach (DataRow _row in _term_rows)
      {
        m_dt_sas_meters.Rows.Remove(_row);
        _result = true;
      }

      return _result;

    } // DB_Unload

    //------------------------------------------------------------------------------
    // PURPOSE : Save DataTable Rows Meters to DB.
    //           If something went wrong, whole task is setted with flag WCP_RC_ERROR
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data was stored correctly or not.
    //
    private Boolean DB_Update(SqlTransaction Trx)
    {
      Boolean _error_occured;
      Int32 _num_rows_deleted;
      DataRow[] _rows_to_db;
      DataRow[] _rows_to_update;
      DataRow[] _rows_to_insert;
      WcpInputMsgProcessTask _wcp_task;

      _error_occured = false;

      try
      {
        while (true)
        {
          // If no rows to insert/update, return depending of _error variable.
          _rows_to_db = m_dt_sas_meters.Select("", "TERMINAL_ID", DataViewRowState.Added | DataViewRowState.ModifiedCurrent);

          if (_rows_to_db.Length == 0)
          {

            return true;
          }

          // Need to do this here, before adapter.Update().
          // In case of error, rows correctly updated/inserted must be marked again with its RowState.
          _rows_to_update = m_dt_sas_meters.Select("", "TERMINAL_ID", DataViewRowState.ModifiedCurrent);
          _rows_to_insert = m_dt_sas_meters.Select("", "TERMINAL_ID", DataViewRowState.Added);

          try
          {
            Trx.Save(ROLLBACK_POINT);

            m_adap_meters_modify.InsertCommand.Connection = Trx.Connection;
            m_adap_meters_modify.InsertCommand.Transaction = Trx;
            m_adap_meters_modify.UpdateCommand.Connection = Trx.Connection;
            m_adap_meters_modify.UpdateCommand.Transaction = Trx;

            _error_occured = true;
            try
            {
              // Insert / Update rows to DB -- Time consuming
              if (m_adap_meters_modify.Update(_rows_to_db) == _rows_to_db.Length)
              {
                _error_occured = false;
              }
            }
            catch (Exception ex)
            {
              Log.Warning("Exception 1: BU_SasMeters.DB_Update.");
              Log.Exception(ex);
            }

            if (!_error_occured)
            {
              foreach (DataRow _row in m_dt_sas_meters.Rows)
              {
                if (_row[IDX_RESPONSE_CODE] != System.DBNull.Value)
                {
                  _wcp_task = (WcpInputMsgProcessTask)_row[IDX_RESPONSE_CODE];
                  _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
                }
              }

              foreach (DataRow _row in _rows_to_db)
              {
                _row[IDX_DELTA_VALUE] = 0;
                _row[IDX_RAW_DELTA_VALUE] = 0;

                _row.AcceptChanges();
              }

              return true;
            }
            else
            {
              Log.Warning("------------- BEGIN SAS METERS NOT UPDATED -------------");
              _num_rows_deleted = 0;

              foreach (DataRow _row in _rows_to_update)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row[IDX_TERMINAL_ID].ToString() +
                              ". Meter: " + this.ComposeMeterDescription((Int32)_row[IDX_METER_CODE]) +
                              ". Details: " + _row.RowError);
                  // whole task is setted with error flag
                  _wcp_task = (WcpInputMsgProcessTask)_row[IDX_RESPONSE_CODE];
                  _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR);
                  _row.Delete();
                  _row.AcceptChanges();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetModified();
                }
              }

              foreach (DataRow _row in _rows_to_insert)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row[IDX_TERMINAL_ID].ToString() +
                              ". Meter: " + this.ComposeMeterDescription((Int32)_row[IDX_METER_CODE]) +
                              ". Details: " + _row.RowError);
                  // whole task is setted with error flag
                  _wcp_task = (WcpInputMsgProcessTask)_row[IDX_RESPONSE_CODE];
                  _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR);
                  _row.Delete();
                  _row.AcceptChanges();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetAdded();
                }
              }

              if (_num_rows_deleted == 0)
              {
                Log.Error("!!! Error found but not in data rows. Cache deleted !!!");
                m_dt_sas_meters.Clear();
              }

              Log.Warning("------------- END SAS METERS NOT UPDATED -------------");

              Trx.Rollback(ROLLBACK_POINT);

            } // if (_error)

          } // try
          catch (Exception _ex)
          {
            Log.Warning("Exception 2: BU_SasMeters.DB_Update");
            Log.Exception(_ex);
            try
            {
              Trx.Rollback(ROLLBACK_POINT);
            }
            catch (Exception _ex2)
            {
              Log.Exception(_ex2);
            }

            return false;
          }
        } // while (true)

      } // try
      catch (Exception _ex)
      {
        Log.Warning("Exception 3: BU_SasMeters.DB_Update.");
        Log.Exception(_ex);

        return false;
      }

    } // DB_Update

    //------------------------------------------------------------------------------
    // PURPOSE : Save Game-denom relationship info to DB.
    //           If something went wrong, whole task is setted with flag WCP_RC_ERROR
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data was stored correctly or not.
    //
    private Boolean DB_StoreGameDenomInfo(Int32 TerminalId, DateTime MsgDate, String XML, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" IF NOT EXISTS(SELECT TOP 1 * FROM   TERMINAL_DENOM_GAME            ");
      _sb.AppendLine("                             WHERE  TDG_TERMINAL_ID = @pTerminalId )");
      _sb.AppendLine(" INSERT INTO  TERMINAL_DENOM_GAME                                   ");
      _sb.AppendLine("            (   TDG_TERMINAL_ID                                     ");
      _sb.AppendLine("              , TDG_RECEIVED                                        ");
      _sb.AppendLine("              , TDG_DATA_XML)                                       ");
      _sb.AppendLine(" VALUES     ( @pTerminalId, @pReceived, @pRequestXML)               ");
      _sb.AppendLine(" ELSE                                                               ");
      _sb.AppendLine(" UPDATE   TERMINAL_DENOM_GAME                                       ");
      _sb.AppendLine("    SET   TDG_RECEIVED              = @pReceived                    ");
      _sb.AppendLine("      ,   TDG_DATA_XML              = @pRequestXML                  ");
      _sb.AppendLine("   WHERE  TDG_TERMINAL_ID = @pTerminalId                            ");

      using (SqlDataAdapter _da = new SqlDataAdapter())
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pReceived", SqlDbType.DateTime).Value = MsgDate;
          _cmd.Parameters.Add("@pRequestXML", SqlDbType.Xml).Value = XML;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;
        }
      }
    } // DB_StoreGameDenomInfo

    #endregion
  }
}
