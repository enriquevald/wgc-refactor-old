//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_BU_TITO_SAS_Meters.cs
// 
//   DESCRIPTION: WCP_BU_TITO_SAS_Meters class; processing of terminal meters
// 
//        AUTHOR: Nelson Madrigal Reyes
// 
// CREATION DATE: 22-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-OCT-2013 NMR    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.WCP.WCP_Messages.TITO;
using WSI.WCP.CadillacJack;
using WSI.Common.TITO;

namespace WSI.WCP
{
  class WCP_BU_TITO_SAS_Meters
  {
    // singleton
    public static WCP_BU_TITO_SAS_Meters Instance = new WCP_BU_TITO_SAS_Meters();

    #region Internal Attributes

    private DataTable m_dt_sas_meters;
    private SqlDataAdapter m_adap_meters_select;
    private SqlDataAdapter m_adap_meters_modify;
    private SqlCommand m_sql_cmd_history;

    private ReaderWriterLock m_rw_lock_queue; // locker for multithreading
    private ArrayList m_unload_queue;         // list for unregistered terminals control

    private SmartParams m_counters;           // list of counters and elapsed times of some operations

    #endregion

    #region Constants
    static protected String ROLLBACK_POINT = "SAS_METERS_BEFORE_INSERT";
    #endregion

    #region Constructor + Public Methods

    public WCP_BU_TITO_SAS_Meters()
    {
      m_dt_sas_meters = null;

      m_unload_queue = new ArrayList(10);
      m_rw_lock_queue = new ReaderWriterLock();

      m_counters = new SmartParams();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enqueue a petition for unload Terminal data.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void EnqueueUnload(Int32 TerminalId)
    {
      try
      {
        m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);

        m_unload_queue.Add(TerminalId);
      }
      finally
      {
        m_rw_lock_queue.ReleaseWriterLock();
      }

    } // EnqueueUnload

    //------------------------------------------------------------------------------
    // PURPOSE : Process all Meters from Tasks in the array.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Tasks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void Process(ArrayList Tasks, SqlConnection SqlConn)
    {
      Int32 _now_ticks;
      Int32 _start_ticks;
      Int64 _elapsed_ticks;
      Int32 _count_processed;
      SqlTransaction _sql_trx;
      //WCP_Message _wcp_request;
      //WCP_TITO_MsgSASMeters _request;
      //SecureTcpClient _tcp_client;

      _start_ticks = Environment.TickCount;
      _sql_trx = null;

      //
      // a. checking internal structures; finish if something was wrong
      if (!this.Init())
      {
        return;
      }

      //
      // b. processing disconnected terminals
      _now_ticks = Environment.TickCount;
      _count_processed = this.UnloadDisconnectedTerminals();
      _elapsed_ticks = Misc.GetElapsedTicks(_now_ticks);
      m_counters.Add("UnloadsCount", _count_processed);
      m_counters.Add("UnloadsTime", _elapsed_ticks);

      //
      // c. updating terminals sessions
      _now_ticks = Environment.TickCount;
      _count_processed = this.UpdateTerminalsSession(Tasks);
      _elapsed_ticks = Misc.GetElapsedTicks(_now_ticks);
      m_counters.Add("UpdateSessionsCount", _count_processed);
      m_counters.Add("UpdateSessionsTime", _elapsed_ticks);

      try
      {
        //
        //.d start DB transaction
        _now_ticks = Environment.TickCount;
        _sql_trx = SqlConn.BeginTransaction();
        _elapsed_ticks = Misc.GetElapsedTicks(_now_ticks);
        m_counters.Add("BeginTransactionTime", _elapsed_ticks);

        //
        // e. The process
        _now_ticks = Environment.TickCount;

        foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
        {
          _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR, "No information available.");
          NetLog.Add(NetEvent.MessageReceived, ((SecureTcpClient)_wcp_task.WcpClient).Identity, _wcp_task.XmlRequest);

          if (this.ProcessTask(_wcp_task, _sql_trx))
          {
            _count_processed++;
          }
        }
        _elapsed_ticks = Misc.GetElapsedTicks(_now_ticks);
        m_counters.Add("TasksProcessCount", _count_processed);
        m_counters.Add("TasksProcessTime", _elapsed_ticks);

        //
        // f. save all changes to memory DataTable
        if (_count_processed > 0)
        {
          _now_ticks = Environment.TickCount;

          if (!this.DB_Update(_sql_trx))
          {
            Log.Error("BU_TITO_SAS_Meters.Process: No tickets meters have been modified!");
          }
          _elapsed_ticks = Misc.GetElapsedTicks(_now_ticks);
          m_counters.Add("UpdateDBTime", _elapsed_ticks);
        }

        //
        // g. save all changes to DB
        _now_ticks = Environment.TickCount;
        _sql_trx.Commit();
        _sql_trx = null;
        _elapsed_ticks = Misc.GetElapsedTicks(_now_ticks);
        m_counters.Add("CommitTransactionTime", _elapsed_ticks);

        //
        // h. finally, send all responses
        _now_ticks = Environment.TickCount;

        foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
        {
          _wcp_task.SendResponse();
        }

        _elapsed_ticks += Misc.GetElapsedTicks(_now_ticks);
        m_counters.Add("SendResponsesTime", _elapsed_ticks);
      }
      catch (Exception ex)
      {
        Log.Warning("Exception: BU_TITO_SAS_Meters.Process");
        Log.Exception(ex);
      }
      finally
      {
        if (_sql_trx != null && _sql_trx.Connection != null)
        {
          _sql_trx.Rollback();
        }
      }

      _elapsed_ticks += Misc.GetElapsedTicks(_start_ticks);
      m_counters.Add("EntireProcessTime", _elapsed_ticks);
      m_counters.Add("EntireProcessCount", Tasks.Count);

      if (m_counters.AsLong("EntireProcessCount") >= 10000)
      {
        //
        // NMR-TODO
        // for testing purpouses: print values

        // reset / clear values
        m_counters.Clear();
      }
    } // Process

    #endregion

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Initializes internal attributes of the class.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private Boolean Init()
    {
      Boolean _result;
      StringBuilder _sb;
      SqlCommand _sql_cmd_select;
      SqlCommand _sql_cmd_insert;
      SqlCommand _sql_cmd_update;

      if (m_dt_sas_meters != null)
      {
        return true;
      }

      _result = false;
      try
      {
        //
        // Datatable for memory data processing
        m_dt_sas_meters = new DataTable("TERMINAL_SAS_METERS");

        m_dt_sas_meters.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
        m_dt_sas_meters.Columns.Add("SEQUENCE_ID", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("METER_CODE", Type.GetType("System.Int32"));
        m_dt_sas_meters.Columns.Add("GAME_ID", Type.GetType("System.Int32"));
        m_dt_sas_meters.Columns.Add("DENOMINATION", Type.GetType("System.Decimal"));
        m_dt_sas_meters.Columns.Add("LAST_REPORTED", Type.GetType("System.DateTime"));  // nullable
        m_dt_sas_meters.Columns.Add("LAST_MODIFIED", Type.GetType("System.DateTime"));  // nullable
        m_dt_sas_meters.Columns.Add("METER_VALUE", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("METER_MAX_VALUE", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("DELTA_VALUE", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("RAW_DELTA_VALUE", Type.GetType("System.Int64"));
        m_dt_sas_meters.Columns.Add("RESPONSE_CODE", Type.GetType("System.Object"));

        // Primary Key for DataTable tickets_meters.
        m_dt_sas_meters.PrimaryKey = new DataColumn[] { m_dt_sas_meters.Columns["TERMINAL_ID"],
                                                        m_dt_sas_meters.Columns["METER_CODE"]
                                                        //m_dt_sas_meters.Columns["GAME_ID"],     // NMR-TODO: review if are necesaries
                                                        //m_dt_sas_meters.Columns["DENOMINATION"]
                                                      };
        //
        // SelectCommand for TERMINAL_SAS_METERS, and create SqlDataAdapter ONLY for Select.
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   TSM_TERMINAL_ID                  AS TERMINAL_ID ");
        _sb.AppendLine("       , TSM_WCP_SEQUENCE_ID              AS SEQUENCE_ID ");
        _sb.AppendLine("       , TSM_METER_CODE                   AS METER_CODE ");
        _sb.AppendLine("       , TSM_GAME_ID                      AS GAME_ID ");
        _sb.AppendLine("       , TSM_DENOMINATION                 AS DENOMINATION ");
        _sb.AppendLine("       , TSM_LAST_REPORTED                AS LAST_REPORTED ");
        _sb.AppendLine("       , TSM_LAST_MODIFIED                AS LAST_MODIFIED ");
        _sb.AppendLine("       , TSM_METER_VALUE                  AS METER_VALUE ");
        _sb.AppendLine("       , TSM_METER_MAX_VALUE              AS METER_MAX_VALUE ");
        _sb.AppendLine("       , TSM_DELTA_VALUE                  AS DELTA_VALUE ");
        _sb.AppendLine("       , TSM_RAW_DELTA_VALUE              AS RAW_DELTA_VALUE ");
        _sb.AppendLine("  FROM   TERMINAL_SAS_METERS ");
        _sb.AppendLine(" WHERE   TSM_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("   AND   TSM_METER_CODE = CASE WHEN @pMeterCode = 0 THEN TSM_METER_CODE ELSE @pMeterCode END ");

        _sql_cmd_select = new SqlCommand(_sb.ToString());
        _sql_cmd_select.Parameters.Add("@pTerminalId", SqlDbType.Int);
        _sql_cmd_select.Parameters.Add("@pMeterCode", SqlDbType.Int);

        m_adap_meters_select = new SqlDataAdapter();
        m_adap_meters_select.SelectCommand = _sql_cmd_select;
        m_adap_meters_select.InsertCommand = null;
        m_adap_meters_select.UpdateCommand = null;
        m_adap_meters_select.DeleteCommand = null;

        //
        // insert query to create row in ticket_meters table
        _sb.Length = 0;
        _sb.AppendLine("INSERT INTO   TERMINAL_SAS_METERS     ");
        _sb.AppendLine("            ( TSM_TERMINAL_ID         ");
        _sb.AppendLine("            , TSM_WCP_SEQUENCE_ID     ");
        _sb.AppendLine("            , TSM_METER_CODE          ");
        _sb.AppendLine("            , TSM_GAME_ID             ");
        _sb.AppendLine("            , TSM_DENOMINATION        ");
        _sb.AppendLine("            , TSM_LAST_REPORTED       ");
        _sb.AppendLine("            , TSM_METER_VALUE         ");
        _sb.AppendLine("            , TSM_METER_MAX_VALUE     ");
        _sb.AppendLine("            , TSM_DELTA_VALUE         ");
        _sb.AppendLine("            , TSM_RAW_DELTA_VALUE     ");
        _sb.AppendLine("            )                         ");
        _sb.AppendLine("    VALUES  ( @pTerminalId            ");
        _sb.AppendLine("            , @pWcpSequenceId         ");
        _sb.AppendLine("            , @pMeterCode             ");
        _sb.AppendLine("            , @pGameId                ");
        _sb.AppendLine("            , @pDenomination          ");
        _sb.AppendLine("            , GETDATE()               ");
        _sb.AppendLine("            , @pMeterValue            ");
        _sb.AppendLine("            , @pMeterMaxValue         ");
        _sb.AppendLine("            , @pDeltaValue            ");
        _sb.AppendLine("            , @pRawDeltaValue         ");
        _sb.AppendLine("            )                         ");

        _sql_cmd_insert = new SqlCommand(_sb.ToString());
        _sql_cmd_insert.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
        _sql_cmd_insert.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
        _sql_cmd_insert.Parameters.Add("@pMeterCode", SqlDbType.Int).SourceColumn = "METER_CODE";
        _sql_cmd_insert.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GAME_ID";
        _sql_cmd_insert.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "DENOMINATION";
        _sql_cmd_insert.Parameters.Add("@pMeterValue", SqlDbType.BigInt).SourceColumn = "METER_VALUE";
        _sql_cmd_insert.Parameters.Add("@pMeterMaxValue", SqlDbType.BigInt).SourceColumn = "METER_MAX_VALUE";
        _sql_cmd_insert.Parameters.Add("@pDeltaValue", SqlDbType.BigInt).SourceColumn = "DELTA_VALUE";
        _sql_cmd_insert.Parameters.Add("@pRawDeltaValue", SqlDbType.BigInt).SourceColumn = "RAW_DELTA_VALUE";

        //
        // query to update row in ticket_meters table
        _sb.Length = 0;
        _sb.AppendLine("UPDATE   TERMINAL_SAS_METERS ");
        _sb.AppendLine("   SET   TSM_WCP_SEQUENCE_ID = @pWcpSequenceId ");
        _sb.AppendLine("       , TSM_GAME_ID = @pGameId ");
        _sb.AppendLine("       , TSM_DENOMINATION = @pDenomination ");
        _sb.AppendLine("       , TSM_LAST_MODIFIED = GETDATE() ");
        _sb.AppendLine("       , TSM_METER_VALUE = @pMeterValue ");
        _sb.AppendLine("       , TSM_METER_MAX_VALUE = @pMeterMaxValue ");
        _sb.AppendLine("       , TSM_DELTA_VALUE = @pDeltaValue ");
        _sb.AppendLine("       , TSM_RAW_DELTA_VALUE = @pRawDeltaValue ");
        _sb.AppendLine(" WHERE   TSM_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("   AND   TSM_METER_CODE = @pMeterCode ");

        _sql_cmd_update = new SqlCommand(_sb.ToString());
        _sql_cmd_update.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
        _sql_cmd_update.Parameters.Add("@pMeterCode", SqlDbType.Int).SourceColumn = "METER_CODE";
        _sql_cmd_update.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
        _sql_cmd_update.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GAME_ID";
        _sql_cmd_update.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "DENOMINATION";
        _sql_cmd_update.Parameters.Add("@pMeterValue", SqlDbType.BigInt).SourceColumn = "METER_VALUE";
        _sql_cmd_update.Parameters.Add("@pMeterMaxValue", SqlDbType.BigInt).SourceColumn = "METER_MAX_VALUE";
        _sql_cmd_update.Parameters.Add("@pDeltaValue", SqlDbType.BigInt).SourceColumn = "DELTA_VALUE";
        _sql_cmd_update.Parameters.Add("@pRawDeltaValue", SqlDbType.BigInt).SourceColumn = "RAW_DELTA_VALUE";

        m_adap_meters_modify = new SqlDataAdapter();
        m_adap_meters_modify.SelectCommand = null;
        m_adap_meters_modify.InsertCommand = _sql_cmd_insert;
        m_adap_meters_modify.UpdateCommand = _sql_cmd_update;
        m_adap_meters_modify.DeleteCommand = null;
        m_adap_meters_modify.ContinueUpdateOnError = true;
        m_adap_meters_modify.UpdateBatchSize = 500;
        m_adap_meters_modify.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
        m_adap_meters_modify.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        //
        // string builder for history
        //
        _sb.Length = 0;
        _sb.AppendLine("INSERT INTO   TERMINAL_SAS_METERS_HISTORY ");
        _sb.AppendLine("            ( TSMH_TERMINAL_ID            ");
        _sb.AppendLine("            , TSMH_METER_CODE             ");
        _sb.AppendLine("            , TSMH_GAME_ID                ");
        _sb.AppendLine("            , TSMH_DENOMINATION           ");
        _sb.AppendLine("            , TSMH_TYPE                   ");
        _sb.AppendLine("            , TSMH_DATETIME               ");
        _sb.AppendLine("            , TSMH_METER_INI_VALUE        ");
        _sb.AppendLine("            , TSMH_METER_FIN_VALUE        ");
        _sb.AppendLine("            , TSMH_METER_INCREMENT        ");
        _sb.AppendLine("            , TSMH_RAW_METER_INCREMENT    ");
        _sb.AppendLine("            )                             ");
        _sb.AppendLine("    VALUES  ( @pTerminalId                ");
        _sb.AppendLine("            , @pMeterCode                 ");
        _sb.AppendLine("            , @pGameId                    ");
        _sb.AppendLine("            , @pDenomination              ");
        _sb.AppendLine("            , @pType                      ");
        _sb.AppendLine("            , @pDateTime                  ");
        _sb.AppendLine("            , @pMeterIniValue             ");
        _sb.AppendLine("            , @pMeterFinValue             ");
        _sb.AppendLine("            , @pMeterIncrement            ");
        _sb.AppendLine("            , @pRawMeterIncrement         ");
        _sb.AppendLine("            )                             ");

        m_sql_cmd_history = new SqlCommand(_sb.ToString());
        m_sql_cmd_history.Parameters.Add("@pTerminalId", SqlDbType.Int);
        m_sql_cmd_history.Parameters.Add("@pMeterCode", SqlDbType.Int);
        m_sql_cmd_history.Parameters.Add("@pGameId", SqlDbType.Int);
        m_sql_cmd_history.Parameters.Add("@pDenomination", SqlDbType.Decimal);
        m_sql_cmd_history.Parameters.Add("@pType", SqlDbType.Int);
        m_sql_cmd_history.Parameters.Add("@pDateTime", SqlDbType.DateTime);
        m_sql_cmd_history.Parameters.Add("@pMeterIniValue", SqlDbType.BigInt);
        m_sql_cmd_history.Parameters.Add("@pMeterFinValue", SqlDbType.BigInt);
        m_sql_cmd_history.Parameters.Add("@pMeterIncrement", SqlDbType.BigInt);
        m_sql_cmd_history.Parameters.Add("@pRawMeterIncrement", SqlDbType.BigInt);

        _result = true;
      }
      catch (Exception _ex)
      {
        m_dt_sas_meters = null;
        Log.Warning("Exception: BU_TITO_SAS_Meters.Init");
        Log.Exception(_ex);
      }

      return _result;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Load Terminal meters data from DB to DataTable Cache
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void DB_Load(Int32 TerminalId, SqlTransaction Trx)
    {
      Int32 _now_ticks;
      Int64 _elapsed_ticks;

      _now_ticks = Environment.TickCount;

      m_adap_meters_select.SelectCommand.Connection = Trx.Connection;
      m_adap_meters_select.SelectCommand.Transaction = Trx;
      m_adap_meters_select.SelectCommand.Parameters["@pTerminalId"].Value = TerminalId;
      m_adap_meters_select.SelectCommand.Parameters["@pMeterCode"].Value = 0;
      m_adap_meters_select.Fill(m_dt_sas_meters);

      _elapsed_ticks = Misc.GetElapsedTicks(_now_ticks);

      m_counters.Add("LoadTerminalsCount", 1);
      m_counters.Add("LoadTerminalsTime", _elapsed_ticks);

    } // Load

    //------------------------------------------------------------------------------
    // PURPOSE : Process task with SAS meters list
    //           The steps here are:
    //            - Translate WCP_MsgXXX to local structure
    //            - Check / load existing metrics for terminal/code
    //              - Insert if row not exists
    //                - get terminal identifier from TerminalSession
    //                - insert row in meters table for terminal/code
    //            - check if sequence is correct; return if not
    //            - check if are changes in received data; return if not
    //            - check if for increment for each pair VALUE+MAX is too big
    //              - update without DELTA, if is too big
    //              - calculate DELTA columns and save, if not
    //            - Save all changes
    //
    //  PARAMS :
    //      - INPUT :
    //          - WcpTask: message with list of meters to be updated
    //          - Trx: common transaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - TRUE if some meter was stored to global DataTable
    //
    private Boolean ProcessTask(WcpInputMsgProcessTask WcpTask, SqlTransaction Trx)
    {
      Boolean _result;
      Boolean _is_big_increment;
      Int64 _delta_value;
      Int64 _raw_delta_value;
      Int64 _max_units_increment;
      ENUM_SAS_METER_HISTORY _sas_meter_history;
      String _provider_id;
      String _terminal_name;
      StringBuilder _sb;
      DataRow _meter_db_row;
      WCP_Message _wcp_request;
      WCP_TITO_MsgSasMeters _request;
      SAS_Meter _old_meter;

      _result = false;

      try
      {
        _wcp_request = WcpTask.Request;
        _request = (WCP_TITO_MsgSasMeters)_wcp_request.MsgContent;
        _old_meter = new SAS_Meter();
        _sb = new StringBuilder();

        //
        // inner cycle for single meter treatment
        //
        foreach (SAS_Meter _new_meter in _request.Meters)
        {
          // load row from DataTable cach�. If not exists, then insert
          if (!this.DB_Load(_request.TerminalId, _new_meter.Code, ref _old_meter, out _meter_db_row, Trx))
          {
            if (this.DB_Insert(_request.TerminalId, _wcp_request.MsgHeader.SequenceId, _new_meter, WcpTask))
            {
              _result = true;
            }
            else
            {
              Log.Warning("BU_TITO_SAS_Meters" +
                          ", Insert failed. TerminalId/SessionId/SequenceId/MeterCode=" +
                          _request.TerminalId + "/" +
                          _wcp_request.MsgHeader.TerminalSessionId + "/" +
                          _wcp_request.MsgHeader.SequenceId + "/" +
                          _new_meter.Code);
            }

            this.DB_InsertIntoHistory(_request.TerminalId,
                                      ENUM_SAS_METER_HISTORY.TSMH_METER_RESET,
                                      0/*OldValue*/,
                                      0/*DeltaValue*/,
                                      0/*RawDeltaValue*/,
                                      _new_meter,
                                      Trx);
            continue;
          }

          //
          // row for terminal/meterCode was found, then update it
          //

          //
          // If message arrived is previous than message in DB, don't treat it,
          // but mark Task as OK and continue with list
          if (_old_meter.SequenceId >= _wcp_request.MsgHeader.SequenceId)
          {
            _result = true;
            continue;
          }

          //
          // compare old data vs new data
          if (_new_meter.Value == _old_meter.Value &&
              _new_meter.MaxValue == _old_meter.MaxValue)
          {
            // No changes in Meters. Mark Task as OK and continue with list
            _result = true;
            continue;
          }

          _delta_value = 0;
          _raw_delta_value = 0;
          //
          // check if all input data are zeros and old not; in TRUE case, DELTA is not calculated
          //
          if ((_new_meter.Value == 0 && _new_meter.MaxValue == 0) &&
              (_old_meter.Value != 0 || _old_meter.MaxValue != 0))
          {
            _sb.Length = 0;
            _sb.AppendLine("*** SAS Meters Reset ***" +
                           "  TerminalId: " + _request.TerminalId +
                           ". MeterCode: " + _new_meter.Code +
                           ". Old meter values: " + _old_meter.Value + "/" + _old_meter.MaxValue);
            Log.Warning(_sb.ToString());

            Common.BatchUpdate.TerminalSession.GetData(_request.TerminalId, _wcp_request.MsgHeader.TerminalSessionId,
                                                       out _provider_id, out _terminal_name);
            Alarm.Register(AlarmSourceCode.TerminalSystem,
                           _request.TerminalId,
                           Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                           AlarmCode.TerminalSystem_SasMeterReset,
                           _sb.ToString());
          }
          else
          {
            //
            // Calculate increments (delta values); method return boolean flag BigIncrement
            // if for some pair increment is too big. In this case, data aren't saved and
            // alarm is stored
            //
            // NMR-TODO: review this switch; real values are in study
            switch (_new_meter.Code)
            {
              case 0:
                _max_units_increment = 50;
                break;

              case 1:
                _max_units_increment = 5000;
                break;

              default:
                _max_units_increment = 500;
                break;
            }

            // calculate delta without rollover
            _raw_delta_value = this.GetRawDeltaMeter(_old_meter.Value, _new_meter.Value, _new_meter.MaxValue);

            // calculate delta with rollover
            _is_big_increment = WCP_BU_MachineMeters.GetDeltaMeter(_request.TerminalId,
                                                                   _new_meter.Code.ToString(),
                                                                   _old_meter.Value,
                                                                   _new_meter.Value,
                                                                   _old_meter.MaxValue,
                                                                   _new_meter.MaxValue,
                                                                   _max_units_increment,
                                                                   out _delta_value);
            if (_is_big_increment)
            {
              _sb.Length = 0;
              _sb.AppendLine("SAS Meters BigIncrement");
              _sb.AppendLine(". TerminalId: " + _request.TerminalId);
              _sb.AppendLine(". Meter values: " + _old_meter.Value + "/" + _old_meter.MaxValue);
              _sb.AppendLine(". Old meter values: " + _new_meter.Value + "/" + _new_meter.MaxValue);
              _sb.AppendLine(". Increment: " + _delta_value);

              Common.BatchUpdate.TerminalSession.GetData(_request.TerminalId, _wcp_request.MsgHeader.TerminalSessionId,
                                                         out _provider_id, out _terminal_name);
              Alarm.Register(AlarmSourceCode.TerminalSystem,
                             _request.TerminalId,
                             Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                             AlarmCode.TerminalSystem_SasMeterBigIncrement,
                             _sb.ToString());

              _delta_value = 0;
              //
              // NMR-TODO: review this code:
              // here is check to decide if with BigIncrement operation can continue
              //
              ////if (_old_meter.Value > _new_meter.Value)
              ////{
              ////  // No update meters if value went back 5 or less units
              ////  if (_old_meter.Value - _new_meter.Value <= 5)
              ////  {
              ////    _decrement = _old_meter.Value - _new_meter.Value;
              ////    Log.Message("*** SAS Meters: Meter "+ _new_meter.Code +" went back " + _decrement +" units."+
              ////                " SAS Meter NOT updated for Terminal: " + _request.TerminalId + ".");
              ////    WcpTask.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
              ////    continue;
              ////  }
              ////}
            }
          }

          //
          // save the changes to temporary DataTable
          //
          if (this.DB_UpdateMeter(_request.TerminalId, _wcp_request.MsgHeader.TerminalSessionId,
                                  _new_meter, _delta_value, _raw_delta_value,
                                  _meter_db_row, WcpTask, Trx))
          {
            _result = true;
          }
          else
          {
            Log.Warning("BU Updating Meters Fails" +
                        ". TerminalId: " + _request.TerminalId +
                        ", WcpSequenceId: " + _wcp_request.MsgHeader.SequenceId +
                        ", MeterCode: " + _new_meter.Code);
          }

          //
          // now, data will stored in history table. Conditions to store are:
          //IF Value == 0 AND ( (OldValue <> 0) || (!OldValue) )
          //   INSERT TERMINAL_SAS_METERS_HISTORY   "Event-MeterReset"
          //IF Value < OldValue 
          //   INSERT TERMINAL_SAS_METERS_HISTORY   "Event-MeterRollover"
          _sas_meter_history = ENUM_SAS_METER_HISTORY.NONE;

          if (_new_meter.Value == 0 && _old_meter.Value != 0)
          {
            _sas_meter_history = ENUM_SAS_METER_HISTORY.TSMH_METER_RESET;
          }
          else if (_new_meter.Value < _old_meter.Value)
          {
            _sas_meter_history = ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER;
          }

          if (_sas_meter_history != ENUM_SAS_METER_HISTORY.NONE)
          {
            this.DB_InsertIntoHistory(_request.TerminalId,
                                      _sas_meter_history,
                                      _old_meter.Value,
                                      _delta_value,
                                      _raw_delta_value,
                                      _new_meter,
                                      Trx);
          }
          // end history
        }
      }
      catch (Exception ex)
      {
        Log.Warning("Exception: BU_TITO_SAS_Meters.ProcessTasks");
        Log.Exception(ex);
      }

      return _result;
    } // ProcessTask

    //------------------------------------------------------------------------------
    // PURPOSE : Get SAS Meters from DataTable Cache, if exists
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId: ID of terminal
    //          - Code: meter code (hexa?)
    //          - Trx
    //
    //      - OUTPUT :
    //          - OldSasMeter: meter for this code
    //          - Row: row from data where read
    //          - Trx: common transaction
    //
    // RETURNS :
    //      - true: successful.
    //      - false: not found or error.
    //
    private Boolean DB_Load(Int32 TerminalId, Int32 Code, ref SAS_Meter SasMeter, out DataRow Row, SqlTransaction Trx)
    {
      String _filter;
      DataRow[] _term_rows;
      Int32 _ticks;
      Int64 _elapsed_ticks;

      _ticks = Environment.TickCount;
      Row = null;

      try
      {
        _filter  = "     TERMINAL_ID = " + TerminalId;
        _filter += " AND METER_CODE = " + Code;
        _term_rows = null;
        //
        // two times tries read data from BD
        for (Int32 _idx_tries = 0; _idx_tries < 2; _idx_tries++)
        {
          _term_rows = m_dt_sas_meters.Select(_filter);
          if (_term_rows.Length == 0)
          {
            if (_idx_tries == 0)
            {
              DB_Load(TerminalId, Trx);
            }
            else
            {
              // stop process after second try-time
              break;
            }
          }
          else
          {
            Row = _term_rows[0];
            break;
          }
        }

        if (_term_rows != null && _term_rows.Length > 1)
        {
          Log.Warning("BU_TITO_SAS_Meters.DB_Load: Too many meters"+
                      ". TerminalId: " + TerminalId +
                      ", Code: " + Code +
                      ". Count: " + _term_rows.Length + ".");
        }

        if (Row != null)
        {
          SasMeter.Code = (Int32)Row["METER_CODE"];
          SasMeter.SequenceId = (Int64)Row["SEQUENCE_ID"];
          SasMeter.GameId = (Int32)Row["GAME_ID"];
          SasMeter.DenomCents = (Decimal)Row["DENOMINATION"];
          SasMeter.Value = (Int64)Row["METER_VALUE"];
          SasMeter.MaxValue = (Int64)Row["METER_MAX_VALUE"];
        }
      }
      catch (Exception _ex)
      {
        Row = null;
        Log.Exception(_ex);
      }
      finally
      {
        _elapsed_ticks = Misc.GetElapsedTicks(_ticks);
        m_counters.Add("ProcessGetTime", _elapsed_ticks);
      }

      return Row != null;

    } // DB_Load

    //------------------------------------------------------------------------------
    // PURPOSE : store received Meters into DataTable Cache.
    //           OJO: there is no try-catch in the code; caller method is responsible
    //                of capture
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - Meter: Wcp item with SAS meter (specific meter)
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: ticket meters inserted.
    //      - false: error.
    //
    private Boolean DB_Insert(Int32 TerminalId,
                              Int64 WcpSequenceId,
                              SAS_Meter Meter,
                              IWcpTaskStatus WcpTask)
    {
      Boolean _result;
      DataRow _new_row;
      Int32 _ticks;
      Int64 _elapsed_ticks;

      _ticks = Environment.TickCount;
      _result = false;

      _new_row = m_dt_sas_meters.NewRow();
      _new_row["TERMINAL_ID"] = TerminalId;
      _new_row["SEQUENCE_ID"] = WcpSequenceId;
      _new_row["METER_CODE"] = Meter.Code;
      _new_row["GAME_ID"] = Meter.GameId;
      _new_row["DENOMINATION"] = Meter.DenomCents;
      _new_row["METER_VALUE"] = Meter.Value;
      _new_row["METER_MAX_VALUE"] = Meter.MaxValue;
      _new_row["DELTA_VALUE"] = 0;
      _new_row["RAW_DELTA_VALUE"] = 0;
      _new_row["RESPONSE_CODE"] = WcpTask;
      m_dt_sas_meters.Rows.Add(_new_row);

      _result = true;
      _elapsed_ticks = Misc.GetElapsedTicks(_ticks);
      m_counters.Add("ProcessInsertTime", _elapsed_ticks);

      return _result;
    } // DB_Insert

    //------------------------------------------------------------------------------
    // PURPOSE : Update into DataTable Rows the given Meter.
    //           OJO: there is no try-catch in the code; caller method is responsible
    //                of capture
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data was updated correctly or not.
    //
    private Boolean DB_UpdateMeter(Int32 TerminalId,
                                   Int64 WcpSequenceId,
                                   SAS_Meter NewMeter,
                                   Int64 DeltaValue,
                                   Int64 RawDeltaValue,
                                   DataRow SasMeterRow,
                                   IWcpTaskStatus WcpTask,
                                   SqlTransaction Trx)
    {
      Boolean _result;

      _result = false;

      SasMeterRow.BeginEdit();
      SasMeterRow["TERMINAL_ID"] = TerminalId;
      SasMeterRow["SEQUENCE_ID"] = WcpSequenceId;
      SasMeterRow["METER_CODE"] = NewMeter.Code;
      SasMeterRow["GAME_ID"] = NewMeter.GameId;
      SasMeterRow["DENOMINATION"] = NewMeter.DenomCents;
      SasMeterRow["METER_VALUE"] = NewMeter.Value;
      SasMeterRow["METER_MAX_VALUE"] = NewMeter.MaxValue;
      SasMeterRow["DELTA_VALUE"] = DeltaValue;
      SasMeterRow["RAW_DELTA_VALUE"] = RawDeltaValue;
      SasMeterRow["RESPONSE_CODE"] = WcpTask;
      SasMeterRow.EndEdit();
      _result = true;

      return _result;
    } // DB_UpdateMeter

    //------------------------------------------------------------------------------
    // PURPOSE : store received Meters into DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - Meter: Wcp item with SAS meter (specific meter)
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: ticket meters inserted.
    //      - false: error.
    //
    private Boolean DB_InsertIntoHistory(Int32 TerminalId,
                                         ENUM_SAS_METER_HISTORY HistoryType,
                                         Int64 OldValue,
                                         Int64 DeltaValue,
                                         Int64 RawDeltaValue,
                                         SAS_Meter Meter,
                                         SqlTransaction Trx)
    {
      Boolean _result;
      Int32 _ticks;
      Int64 _elapsed_ticks;

      _ticks = Environment.TickCount;
      _result = false;

      try
      {
        m_sql_cmd_history.Connection = Trx.Connection;
        m_sql_cmd_history.Transaction = Trx;

        m_sql_cmd_history.Parameters["@pTerminalId"].Value = TerminalId;
        m_sql_cmd_history.Parameters["@pMeterCode"].Value = Meter.Code;
        m_sql_cmd_history.Parameters["@pGameId"].Value = Meter.GameId;
        m_sql_cmd_history.Parameters["@pDenomination"].Value = Meter.DenomCents;
        m_sql_cmd_history.Parameters["@pType"].Value = (Int16)HistoryType;
        m_sql_cmd_history.Parameters["@pDateTime"].Value = WGDB.Now;
        m_sql_cmd_history.Parameters["@pMeterIniValue"].Value = OldValue;
        m_sql_cmd_history.Parameters["@pMeterFinValue"].Value = Meter.Value;
        m_sql_cmd_history.Parameters["@pMeterIncrement"].Value = DeltaValue;
        m_sql_cmd_history.Parameters["@pRawMeterIncrement"].Value = RawDeltaValue;

        _result = m_sql_cmd_history.ExecuteNonQuery() == 1;
      }
      catch (Exception _ex)
      {
        Log.Warning("WCP_BU_TITO_SAS_Meters.DB_InsertIntoHistory. Inserting history");
        Log.Exception(_ex);
      }
      finally
      {
        _elapsed_ticks = Misc.GetElapsedTicks(_ticks);
        m_counters.Add("ProcessHistoryInsertTime", _elapsed_ticks);
      }

      return _result;
    } // DB_Insert

    //------------------------------------------------------------------------------
    // PURPOSE : Get RAW delta of one SAS Meter 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OldMeter
    //          - NewMeter
    //          - NewMeterMaxValue
    //
    //      - OUTPUT :
    //          - DeltaMeterValue
    //
    // RETURNS :
    //      - Int64: A RAW delta value
    //
    //   NOTES :
    //
    private Int64 GetRawDeltaMeter(Int64 OldMeterValue,
                                   Int64 NewMeterValue,
                                   Int64 NewMeterMaxValue)
    {
      Int64 _result;

      _result = 0;

      if (NewMeterValue >= OldMeterValue)
      {
        _result = NewMeterValue - OldMeterValue;
      }
      else
      {
        _result = NewMeterValue + (NewMeterMaxValue - OldMeterValue);
      }

      return _result;
    } // GetDeltaMeter

    //------------------------------------------------------------------------------
    // PURPOSE : Update status of terminals in the message list
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - INT32: the number of processed/updated terminals
    //
    private Int32 UpdateTerminalsSession(ArrayList Tasks)
    {
      Int32 _result;
      WCP_Message _wcp_request;
      SecureTcpClient _tcp_client;

      _result = 0;

      Common.BatchUpdate.TerminalSession.Lock();
      try
      {
        foreach (WcpInputMsgProcessTask _wcp_task in Tasks)
        {
          _wcp_request = _wcp_task.Request;
          _tcp_client = (SecureTcpClient)_wcp_task.WcpClient;

          if (Common.BatchUpdate.TerminalSession.SessionMessageReceived(_tcp_client.InternalId,
                                                                        _wcp_request.MsgHeader.TerminalSessionId,
                                                                        _wcp_request.MsgHeader.SequenceId,
                                                                        _wcp_request.TransactionId))
          {
            _result++;
          }
          else
          {
            Log.Error("BU_TITO_SAS_Meters.UpdateTerminalsSession: SessionMessageReceived: Session not found. Terminal: " + _tcp_client.InternalId + ".");
          }
        }
      }
      catch (Exception ex)
      {
        Log.Warning("Exception: BU_TITO_SAS_Meters.UpdateTerminalsSession");
        Log.Exception(ex);
      }
      finally
      {
        Common.BatchUpdate.TerminalSession.Unlock();
      }

      return _result;
    } // UpdateTerminalsSession

    //------------------------------------------------------------------------------
    // PURPOSE : Process the queue with terminals checked as disconnected
    //           (unload its data from DataTable)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - INT32: the number of processed/unloaded terminals
    //
    private Int32 UnloadDisconnectedTerminals()
    {
      Int32 _result;
      String _filter;

      _result = 0;

      try
      {
        m_rw_lock_queue.AcquireWriterLock(Timeout.Infinite);

        foreach (Int32 _terminal_id in m_unload_queue)
        {
          _filter = "TERMINAL_ID = " + _terminal_id;
          if (this.DB_Unload(_filter))
            _result++;
        }
        m_unload_queue.Clear();
      }
      catch (Exception ex)
      {
        Log.Warning("Exception: BU_TITO_SAS_Meters.UnloadDisconnectedTerminals");
        Log.Exception(ex);
      }
      finally
      {
        m_rw_lock_queue.ReleaseWriterLock();
      }

      return _result;

    } // UnloadDisconnectedTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows based on Filter from DataTable Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Filter: selection of rows to be removed
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private Boolean DB_Unload(String Filter)
    {
      Boolean _result;
      DataRow[] _term_rows;

      _result = false;

      if (m_dt_sas_meters != null)
      {
        _term_rows = m_dt_sas_meters.Select(Filter);

        foreach (DataRow _row in _term_rows)
        {
          m_dt_sas_meters.Rows.Remove(_row);
          _result = true;
        }
      }
      else
      {
        Log.Error("DB_Unload: WCP_BU_TITO_SAS_Meters not initialized. Call Init first!");
      }

      return _result;

    } // DB_Unload

    //------------------------------------------------------------------------------
    // PURPOSE : Save DataTable Rows Meters to DB.
    //           If something went wrong, whole task is setted with flag WCP_RC_ERROR
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If data was stored correctly or not.
    //
    private Boolean DB_Update(SqlTransaction Trx)
    {
      Boolean _was_error;
      Int32 _num_rows_deleted;
      DataRow[] _rows_to_db;
      DataRow[] _rows_to_update;
      DataRow[] _rows_to_insert;
      WcpInputMsgProcessTask _wcp_task;

      _was_error = false;

      try
      {
        while (true)
        {
          // If no rows to insert/update, return depending of _error variable.
          _rows_to_db = m_dt_sas_meters.Select("", "TERMINAL_ID", DataViewRowState.Added | DataViewRowState.ModifiedCurrent);

          if (_rows_to_db.Length == 0)
          {
            return true;
          }

          // Need to do this here, before adapter.Update().
          // In case of error, rows correctly updated/inserted must be marked again with its RowState.
          _rows_to_update = m_dt_sas_meters.Select("", "TERMINAL_ID", DataViewRowState.ModifiedCurrent);
          _rows_to_insert = m_dt_sas_meters.Select("", "TERMINAL_ID", DataViewRowState.Added);

          try
          {
            Trx.Save(ROLLBACK_POINT);

            m_adap_meters_modify.InsertCommand.Connection = Trx.Connection;
            m_adap_meters_modify.InsertCommand.Transaction = Trx;
            m_adap_meters_modify.UpdateCommand.Connection = Trx.Connection;
            m_adap_meters_modify.UpdateCommand.Transaction = Trx;

            _was_error = true;
            try
            {
              // Insert / Update rows to DB -- Time consuming
              if (m_adap_meters_modify.Update(_rows_to_db) == _rows_to_db.Length)
              {
                // NMR-TODO: TBD
                Log.Message("Meters. Inserted: " + _rows_to_insert.Length + ", Updated: " + _rows_to_update.Length);
                _was_error = false;
              }
            }
            catch (Exception ex)
            {
              Log.Warning("Exception 1: WCP_BU_TITO_SAS_Meters.UpdateDB.");
              Log.Exception(ex);
            }

            if (!_was_error)
            {
              // Everything is ok.
              DataRow _row = _rows_to_db[0];
              _wcp_task = (WcpInputMsgProcessTask)_row["RESPONSE_CODE"];
              _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);
              // task is the same for all rows; then break;
              return true;
            }
            else
            {
              Log.Warning("------------- BEGIN SAS METERS NOT UPDATED -------------");
              _num_rows_deleted = 0;

              foreach (DataRow _row in _rows_to_update)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row["TERMINAL_ID"].ToString() +
                              ". MeterCode: " + _row["METER_CODE"].ToString() +
                              ". Details: " + _row.RowError);
                  // whole task is setted with error flag
                  _wcp_task = (WcpInputMsgProcessTask)_row["RESPONSE_CODE"];
                  _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR);
                  _row.Delete();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetModified();
                }
              }

              foreach (DataRow _row in _rows_to_insert)
              {
                if (_row.HasErrors)
                {
                  Log.Warning("TerminalId: " + _row["TERMINAL_ID"].ToString() +
                              ". MeterCode: " + _row["METER_CODE"].ToString() +
                              ". Details: " + _row.RowError);
                  // whole task is setted with error flag
                  _wcp_task = (WcpInputMsgProcessTask)_row["RESPONSE_CODE"];
                  _wcp_task.SetResponseCode(WCP_ResponseCodes.WCP_RC_ERROR);
                  _row.Delete();
                  _num_rows_deleted++;
                }
                else
                {
                  _row.AcceptChanges();
                  _row.SetAdded();
                }
              }

              if (_num_rows_deleted == 0)
              {
                Log.Error("!!! Error found but not in data rows. Cache deleted !!!");
                m_dt_sas_meters.Clear();
              }

              Log.Warning("------------- END SAS METERS NOT UPDATED -------------");

              Trx.Rollback(ROLLBACK_POINT);

            } // if (_error)

          } // try
          catch (Exception _ex)
          {
            Log.Warning("Exception 2: WCP_BU_TITO_SAS_Meters.UpdateDB");
            Log.Exception(_ex);
            try
            {
              Trx.Rollback(ROLLBACK_POINT);
            }
            catch (Exception _ex2)
            {
              Log.Exception(_ex2);
            }

            return false;
          }
        } // while (true)

      } // try
      catch (Exception _ex)
      {
        Log.Warning("Exception 3: WCP_BU_TITO_SAS_Meters.UpdateDB.");
        Log.Exception(_ex);
        return false;
      }

    } // DB_Update

    #endregion
  }
}
