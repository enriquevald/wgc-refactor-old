﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: WCP_BucketsExpirationDayMonth.cs
//
//   DESCRIPTION: WCP_BucketsExpirationDayMonth class
//
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 26-JAN-2016

// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JAN-2016 FGB    First release.
// 10-MAR-2016 FGB    PBI 10130: Multiple Buckets: Sincronización MultiSite: Kernel - Validar Expiración y Cambio Nivel
//------------------------------------------------------------------------------

using System;
using System.Threading;
using WSI.Common;

namespace WSI.WCP
{
  public static class WCP_BucketsExpirationDayMonth
  {
    #region Attributes
    private static Thread m_thread;
    #endregion Attributes

    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_BucketsExpirationDayMonth class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void Init()
    {
      m_thread = new Thread(BucketsExpirationDayMonthThread);
      m_thread.Name = "BucketsExpirationDayMonthThread";
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for thread starts
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Start()
    {
      // Thread starts
      m_thread.Start();
    }
    #endregion Public Methods

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static void BucketsExpirationDayMonthThread()
    {
      int _wait_hint;
      DateTime _now;

      while (true)
      {
        _now = WGDB.Now;
        _wait_hint = 60000 - (_now.Second * 1000) - _now.Millisecond;

        Thread.Sleep(_wait_hint);

        // Only the service running as 'Principal' will excute the tasks.
        if (!Services.IsPrincipal("WCP"))
        {
          continue;
        }

        if (CommonMultiSite.GetPlayerTrackingMode() != PlayerTracking_Mode.Site)
        {
          continue;
        }

        try
        {
          if (!WSI.Common.BucketsExpirationDayMonth.ProcessBucketsExpirationDayMonth())
          {
            Log.Warning("WSI.Common.BucketsExpirationDayMonth.ProcessBucketsExpirationDayMonth");
          }
        }
        catch (Exception _ex)
        {
          Log.Warning("WCP_BucketsExpirationDayMonth.BucketsExpirationDayMonthThread. Exception in function: ProcessBucketsExpirationDayMonth");
          Log.Exception(_ex);
        }
      }
    } // BucketsExpirationDayMonthThread
    #endregion Private Methods
  } // WCP_BucketsExpirationDayMonth
} // WSI.WCP
