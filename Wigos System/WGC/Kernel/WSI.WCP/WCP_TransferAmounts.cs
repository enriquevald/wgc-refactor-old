//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TransferAmounts.cs
// 
//   DESCRIPTION: Class to transfer amounts
// 
//        AUTHOR: Susana Sams�
// 
// CREATION DATE: 01-MAR-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-MAR-2012 SSC    First release.
// 25-MAY-2012 RCI    In routine CanTransferToAccount(), can't add credit if balance mismatch and is NOT playing in a 3GS terminal.
// 13-FEB-2013 HBB & MPO    Added functionality of one cashier session for each PromoBOX.
// 26-APR-2013 HBB & ICS    Added functionality of one cashier session for each SYS-Acceptor.
// 24-MAY-2013 LEM    Modified DB_TransferAmountToPlayerAccount to use the AccountMovementsTable and CashierMovementsTable constructor with TerminalName and TerminalId.
//                    DB_TransferAmountToPlayerAccount calls SetMobileBankInformation only when a REAL MobileBank.
// 12-AUG-2013 RCI & LEM    Own code WCP_RC for recharges failed due to AntiMoneyLaundering control.
// 15-NOV-2013 JBP    Adapted DB_TransferAmountToPlayerAccount and ReadPlayerAccount for SYS_TITO.
// 20-NOV-2013 JBP    Reset account balance & Add substract movement. (TITO bill CashIn)
// 19-SEP-2014 SGB & RCI    Fixed Bug #WIG-1246 Don't allow a note acceptor recharge when cage is disabled.
// 19-SEP-2014 SGB & RCI    Fixed Bug #WIG-1231 Check if exceeds the allowable limit amount.
// 15-APR-2015 JBC & RMS    Cannot recharge when cashier session is out of gaming day
// 05-AUG-2015 YNM    TFS-3109: Annonymous account min allowed cash in amount recharge
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Collections;
using WSI.Common.TITO;

namespace WSI.WCP
{
  public class WCP_TransferAmounts
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Transfer credit in to player account by Cashier or Mobile Bank, or by Terminal Money (Money events)
    //          Also, obtain the Initial and Final balance.
    //
    //  PARAMS:
    //      - INPUT:
    //          - MobileBankTrackData
    //          - PlayerTrackData
    //          - TerminalId
    //          - TerminalName
    //          - AmountToAdd
    //          - WcpSequenceId
    //          - WcpTransactionId
    //          - Trx
    //          - Account
    //          - MBUserType
    //
    //      - OUTPUT:
    //          - InitialBalance
    //          - FinalBalance
    //
    // RETURNS:
    //      - Boolean: TRUE If has been transferred correctly.
    //
    // NOTES :
    //  Steps:
    //        1. Read Player Account 
    //        2. Check if can transfer
    //        3. Get win_mobile_bank_track_data and win_player_track_data
    //        4. Check for session Transfer operation canot be performed if:
    //            4.1. Play session is not zero and mb operation terminal is not the same that current terminal.
    //        5. Get Best Promotion for this account & Amount
    //        6. Calculate cash in splits
    //        7. Add credit to player account.
    //        8. Read mobile bank account.
    //        9. Create mobile bank or note acceptor transfer operation
    //        10. Account movements
    //        11. Create voucher in DB
    //        12. Update Play Session with a Cash-In on the middle of a play. 
    //
    public static Boolean DB_TransferAmountToPlayerAccount(String MobileBankTrackData,
                                                             String PlayerTrackData,
                                                             Int32 TerminalId,
                                                             String TerminalName,
                                                             Int64 MBAccountId,
                                                             Decimal RedeemableAmountToAdd,
                                                             Int64 WcpSequenceId,
                                                             Int64 WcpTransactionId,
                                                             out Decimal PreviousAccountBalance,
                                                             out Decimal CurrentAccountBalance,
                                                             out ArrayList VoucherList,
                                                             SqlTransaction Trx,
                                                             WCP_AccountManager.WCP_Account Account,
                                                             MB_USER_TYPE MBUserType)
    {

      SqlCommand _sql_cmd;
      String _sql_txt;

      // Player variables
      WCP_AccountManager.WCP_Account _account;

      // Mobile bank variables
      Int64 _mb_account_id;
      Int64 _mb_cashier_session_id;
      Decimal _mb_ini_balance;
      String _mb_holder_name;
      MB_USER_TYPE _mb_account_type;
      String _win_mobile_bank_track_data;
      Int32 _automatically_assign_promotion;

      DataTable _promos;

      OperationCode _operation_code;
      Int64 _operation_id;
      Currency _nr2;
      Decimal _dec_nr2;
      Decimal _nr2_pct;
      CashierSessionInfo _cashier_session_info;

      Int32 _user_id;
      String _user_name;
      Int32 _cashier_id;
      String _cashier_name;
      Currency _final_movement_balance;
      Int64 _promotion_id;
      Currency _promotion_amount;
      Currency _spent_used;
      String _win_player_track_data;
      GU_USER_TYPE _gui_user_type;
      Boolean _is_acceptor;

      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;

      String _str_message;
      RechargeInputParameters _input_params;
      RechargeOutputParameters _output_params;

      MultiPromos.AccountBalance _to_add;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _fin_balance;

      DateTime _gaming_day;

      //Initialization
      _account = Account;
      _operation_id = 0;
      _promotion_id = 0;
      _promotion_amount = 0;
      _spent_used = 0;

      // Output parameters initialization
      PreviousAccountBalance = 0;
      CurrentAccountBalance = 0;
      VoucherList = new ArrayList();

      // ACC & FJC Clear NR2 for get the same random value in this operation
      WSI.Common.Cashier.ClearNR2();

      _gaming_day = DateTime.MinValue;

      // 1. Read Player Account 
      // 2. Check if can transfer
      // 3. Get win_mobile_bank_track_data and win_player_track_data
      // 4. Check for session Transfer operation canot be performed if:
      //      4.1. Play session is not zero and mb operation terminal is not the same that current terminal.
      // 5. Get Best Promotion for this account & Amount
      // 6. Calculate cash in splits
      // 7. Read mobile bank account.
      // 8. Create mobile bank or note acceptor transfer operation
      // 9. Add credit to player account.
      // 10. Account movements
      // 11. Save accounts and cashier movements
      // 12. Create voucher in DB
      // 13. Update Play Session with a Cash-In on the middle of a play. 

      // TODO: Take in account in all related the new MobileBank user for Auto-Service Kiosk: MB_USER_TYPE.SYS_PROMOBOX.

      // 1. Read Player Account 
      ReadPlayerAccount(Account, PlayerTrackData, MBUserType, Trx, out _account);

      if (!Cashier.GetNR2(_account.id, RedeemableAmountToAdd, out _dec_nr2, out _nr2_pct))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "WCP.DB_TransferAmountToPlayerAccount: GetNR2 failed.");
      }
      _nr2 = _dec_nr2;

      // 2. Check if can transfer
      if (!CanTransferToAccount(_account, MBUserType, Trx))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_BLOCKED);
      }

      _gui_user_type = Cashier.GetGUIUserType(MBUserType, out _is_acceptor);

      if (_gui_user_type == GU_USER_TYPE.NOT_ASSIGNED)
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, String.Format("MB UserType unknown: {0}.", MBUserType));
      }

      // 3. Get win_mobile_bank_track_data and win_player_track_data
      if (!_is_acceptor)
      {
        _win_mobile_bank_track_data = "NOT SET";
        if (!CardNumber.ConvertTrackData(WSI.Common.CardTrackData.CriptMode.WCP, MobileBankTrackData, out _win_mobile_bank_track_data))
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Invalid Mobile bank Track Number");
        }
      }
      else
      {
        _win_mobile_bank_track_data = MobileBankTrackData;
      }

      _win_player_track_data = "NOT SET";
      if (!CardNumber.ConvertTrackData(WSI.Common.CardTrackData.CriptMode.WCP, PlayerTrackData, out _win_player_track_data))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Invalid Player Track Number");
      }

      // 4. Check for session Transfer operation canot be performed if:
      //    4.1. Play session is not zero and mb operation terminal is not the same that current terminal.
      ////if (_account.play_session_id != 0 && TerminalId != _account.terminal_id)
      ////{
      ////  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_ALREADY_IN_USE, "Card already in use.");
      ////}

      // 5. Get Best Promotion for this account & Amount
      Int32.TryParse(Misc.ReadGeneralParams("MobileBank", "AutomaticallyAssignPromotion", Trx), out _automatically_assign_promotion);

      if (_automatically_assign_promotion == 1)
      {
        Promotion.GetBestApplicablePromotion(_account.id, RedeemableAmountToAdd, out _promotion_id, out _promotion_amount, out _spent_used, Trx);
      }

      // 7. Read mobile bank account.
      _mb_account_id = MBAccountId;
      _mb_cashier_session_id = 0;
      _mb_ini_balance = 0;
      _mb_holder_name = "";
      _mb_account_type = MB_USER_TYPE.NOT_ASSIGNED;

      if (_is_acceptor)
      {
        if (!ReadMBAccountNoteAcceptor(MBAccountId, Trx, out _mb_cashier_session_id, out _mb_ini_balance, out _mb_holder_name, out _mb_account_type))
        {
          Log.Warning("Exception throw: Error reading Mobile Bank Card. CardTrackData: " + MobileBankTrackData);

          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Mobile Bank Account Does Not Exist.");
        }
      }
      else
      {
        if (!ReadMBAccount(_win_mobile_bank_track_data, Trx, out _mb_account_id, out _mb_cashier_session_id, out _mb_ini_balance, out _mb_holder_name, out _mb_account_type))
        {
          Log.Warning("Exception throw: Error reading Mobile Bank Card. CardTrackData: " + MobileBankTrackData);

          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Mobile Bank Account Does Not Exist.");
        }
      }

      if (_mb_account_id == 0)
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Mobile Bank Account Does Not Exist.");
      }

      if (_mb_cashier_session_id == 0)
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Mobile Bank Account NOT in Session.");
      }

      // Check if mobile bank account type is correct
      if ((_mb_account_type == MB_USER_TYPE.SYS_ACCEPTOR && MBUserType != MB_USER_TYPE.SYS_ACCEPTOR) ||
          (_mb_account_type != MB_USER_TYPE.SYS_ACCEPTOR && MBUserType == MB_USER_TYPE.SYS_ACCEPTOR))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Mobile Bank Account Type NOT Valid.");
      }

      // Check if mobile bank account type is correct
      if ((_mb_account_type == MB_USER_TYPE.SYS_PROMOBOX && MBUserType != MB_USER_TYPE.SYS_PROMOBOX) ||
          (_mb_account_type != MB_USER_TYPE.SYS_PROMOBOX && MBUserType == MB_USER_TYPE.SYS_PROMOBOX))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Mobile Bank Account Type NOT Valid.");
      }

      // Check if mobile bank account type is correct
      if ((_mb_account_type == MB_USER_TYPE.SYS_TITO && MBUserType != MB_USER_TYPE.SYS_TITO) ||
          (_mb_account_type != MB_USER_TYPE.SYS_TITO && MBUserType == MB_USER_TYPE.SYS_TITO))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Mobile Bank Account Type NOT Valid.");
      }

      if (!MultiPromos.Trx_UpdateAccountBalance(_account.id, MultiPromos.AccountBalance.Zero, out _ini_balance, Trx))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Operation not available.");
      }

      // 8. Create mobile bank or note acceptor transfer operation
      if (_promotion_amount > 0)
      {
        _operation_code = _is_acceptor ? OperationCode.NA_PROMOTION : OperationCode.MB_PROMOTION;
      }
      else
      {
        _operation_code = _is_acceptor ? OperationCode.NA_CASH_IN : OperationCode.MB_CASH_IN;
      }

      if (!Operations.DB_InsertOperation(_operation_code, _account.id, _mb_cashier_session_id, _mb_account_id, RedeemableAmountToAdd,
                                         _promotion_id, _promotion_amount, 0, _nr2, 0, 0, string.Empty, out _operation_id, Trx))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Operation not available.");
      }

      _promos = AccountPromotion.CreateAccountPromotionTable();

      if (_promotion_id > 0 && _promotion_amount > 0)
      {
        AccountPromotion _account_promo;

        _account_promo = new AccountPromotion();

        if (!Promotion.ReadPromotionData(Trx, _promotion_id, _account_promo))
        {
          Log.Error("DB_TransferAmountToPlayerAccount. Promotion not found: " + _promotion_id + ".");

          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Promotion not found.");
        }

        if (!AccountPromotion.AddPromotionToAccount(_operation_id, _account.id, _account.level, RedeemableAmountToAdd, 0, true, _promotion_amount, _account_promo, _promos))
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "AddAccountPromotion error.");
        }

        if (_spent_used > 0)
        {
          if (!Promotion.UpdateSpentUsedInPromotion(_spent_used, _account.id, _account_promo, Trx, out _str_message))
          {
            Log.Error("DB_TransferAmountToPlayerAccount. " + _str_message);

            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, _str_message);
          }
        }
      }

      _user_id = 0;
      _user_name = "";

      // Set MobileBank Information only when a REAL MobileBank. If it's PromoBOX, NoteAcceptor or SYS_TITO, the CashierSessionInfo is set previously in DB_AddCredit().
      if (!_is_acceptor)
      {
        //Update Cashier Session Info with Mobile Bank cashier information
        if (!Cashier.DB_ReadOpenedCashierSession(_mb_cashier_session_id, Trx, out _user_id, out _user_name, out _cashier_id, out _cashier_name, out _final_movement_balance, out _gaming_day))
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Operation not available.");
        }

        // Need to do this for the vouchers.
        CommonCashierInformation.SetMobileBankInformation(_mb_cashier_session_id, _user_id, _user_name, _cashier_id, _cashier_name, _mb_account_id, _mb_holder_name, _gaming_day);
      }

      _cashier_session_info = CommonCashierInformation.CashierSessionInfo();
      _cashier_session_info.IsMobileBank = true;
      _cashier_session_info.UserType = _gui_user_type;

      if (_gui_user_type == GU_USER_TYPE.USER)
      {
        _cashier_session_info.AuthorizedByUserId = (Int32)_mb_account_id;
        _cashier_session_info.AuthorizedByUserName = _mb_holder_name;
        _cashier_session_info.CashierSessionId = _mb_cashier_session_id;
        _cashier_session_info.UserId = _user_id;
        _cashier_session_info.UserName = _user_name;
      }

      // TODO: Revisar play_session_id = 0.
      //_account_movements = new AccountMovementsTable(TerminalId, _terminal_name, _account.play_session_id);
      _account_movements = new AccountMovementsTable(_cashier_session_info, TerminalId, TerminalName);
      _cashier_movements = new CashierMovementsTable(_cashier_session_info, TerminalName);

      // ICS 30-APR-2013: Only update the mobile bank if it's not SYS_ACCEPTOR or SYS_PROMOBOX or SYS_TITO
      if (!_is_acceptor)
      {

        //    10.4. Substract amount transfered from mobile bank account.
        _sql_txt = "UPDATE   MOBILE_BANKS           " +
                   "   SET   MB_BALANCE             = MB_BALANCE      - @pAmount  " +
                   "       , MB_CASH_IN             = MB_CASH_IN      + @pAmount  " +
                   "       , MB_PENDING_CASH        = MB_PENDING_CASH + @pAmount  " +
                   "       , MB_LAST_ACTIVITY       = GETDATE()                   " +
                   "       , MB_ACTUAL_NUMBER_OF_RECHARGES = isNull(MB_ACTUAL_NUMBER_OF_RECHARGES, 0) + 1  " +
                   " OUTPUT  DELETED.MB_BALANCE     " +
                   " WHERE   MB_ACCOUNT_ID          = @pAccountId                 " +
                   "   AND   MB_CASHIER_SESSION_ID  = @pCashierSessionId          " +
                   "   AND ( MB_BALANCE            >= @pAmount OR MB_ACCOUNT_TYPE IN (@pMBAccountTypeSysAcceptor, @pMBAccountTypeSysPromoBOX) ) ";

        _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx);
        _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = RedeemableAmountToAdd;
        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _mb_account_id;
        _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = _mb_cashier_session_id;
        _sql_cmd.Parameters.Add("@pMBAccountTypeSysAcceptor", SqlDbType.Int).Value = MB_USER_TYPE.SYS_ACCEPTOR;
        _sql_cmd.Parameters.Add("@pMBAccountTypeSysPromoBOX", SqlDbType.Int).Value = MB_USER_TYPE.SYS_PROMOBOX;

        // ACC 12-JUL-2012 Change update with output
        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (!_reader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Mobile bank card Not valid.");
          }

          _mb_ini_balance = _reader.GetDecimal(0);
        }

      }

      //    10.5. Insert a mobile bank account movement.
      //
      WCP_BusinessLogic.DB_MobileBankInsertMovement(_mb_cashier_session_id, _mb_account_id, TerminalId, WcpSequenceId, WcpTransactionId,
                                                    _account.id, _win_player_track_data, 0,
                                                    MBMovementType.TransferCredit,       // Type
                                                    _mb_ini_balance,                     // InitialBalance
                                                    RedeemableAmountToAdd,               // SubAmount
                                                    0,                                   // AddAmount
                                                    _mb_ini_balance - RedeemableAmountToAdd, // FinalBalance
                                                    _operation_id, Trx);


      _input_params = new RechargeInputParameters();
      _input_params.AccountId = _account.id;
      _input_params.AccountLevel = _account.level;
      _input_params.CardPaid = true;
      _input_params.CardPrice = 0;
      _input_params.ExternalTrackData = PlayerTrackData;
      _input_params.VoucherAccountInfo = _account.VoucherAccountInfo(PlayerTrackData);
      
      if (!Accounts.DB_Recharge(_operation_id, _input_params, RedeemableAmountToAdd, _promos, _account_movements, _cashier_movements, Trx,
                                       out _output_params))
      {
        switch (_output_params.ErrorCode)
        {
          case RECHARGE_ERROR_CODE.ERROR_GENERIC:
          default:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB_TransferAmountToPlayerAccount: DB_Recharge failed. Account: " + _account.id.ToString() +
                                    ", Amount: " + RedeemableAmountToAdd.ToString() + ".");

          case RECHARGE_ERROR_CODE.ERROR_AML_RECHARGE_NOT_AUTHORIZED:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ML_RECHARGE_NOT_AUTHORIZED, "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to AntiMoneyLaundering control. Account: " + _account.id.ToString() +
                                    ", Amount: " + RedeemableAmountToAdd.ToString() + ".");

          case RECHARGE_ERROR_CODE.ERROR_RECHARGE_LIMIT_EXCEEDED:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_EXCEEDED_MAX_ALLOWED_CASH_IN, "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to max allowed recharge limit exceeded. Account: " + _account.id.ToString() +
                                    ", Amount: " + RedeemableAmountToAdd.ToString() + ".");

          case RECHARGE_ERROR_CODE.ERROR_DAILY_RECHARGE_LIMIT_EXCEEDED:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_EXCEEDED_MAX_ALLOWED_DAILY_CASH_IN, "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to max allowed daily recharge limit exceeded. Account: " + _account.id.ToString() +
                                    ", Amount: " + RedeemableAmountToAdd.ToString() + ".");

          case RECHARGE_ERROR_CODE.ERROR_NOTE_ACCEPTOR_WITH_CAGE_DISABLED:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_RECHARGE_NOT_AUTHORIZED, "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to a note acceptor and cage is disabled. Account: " + _account.id.ToString() +
                                    ", Amount: " + RedeemableAmountToAdd.ToString() + ".");

          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED,
                                    "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to account global max allowed recharges limit exceeded. Account: : " + _account.id.ToString() + ", Amount: " + RedeemableAmountToAdd.ToString() + ".");

          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED,
                                    "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to account global max allowed recharges amount limit exceeded. Account: : " + _account.id.ToString() + ", Amount: " + RedeemableAmountToAdd.ToString() + ".");

          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED,
                                    "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to account daily max allowed recharges limit exceeded. Account: : " + _account.id.ToString() + ", Amount: " + RedeemableAmountToAdd.ToString() + ".");

          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED,
                                    "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to account daily max allowed recharges amount limit exceeded. Account: : " + _account.id.ToString() + ", Amount: " + RedeemableAmountToAdd.ToString() + ".");
          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT,
                                    "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to account min allowed recharge amount is not reached. Account: : " + _account.id.ToString() + ", Amount: " + RedeemableAmountToAdd.ToString() + ".");

          case RECHARGE_ERROR_CODE.ERROR_RECHARGE_OUT_GAMING_DAY:
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_GAMING_DAY_EXPIRED,
                                    "DB_TransferAmountToPlayerAccount: DB_Recharge failed due to Gaming Day. Account: : " + _account.id.ToString() + ", Amount: " + RedeemableAmountToAdd.ToString() + ".");
        }
      }

      //JBP 20-NOV-2013: Reset account balance & Add substract movement (TITO bill CashIn).
      if (MBUserType == MB_USER_TYPE.SYS_TITO && !WSI.Common.Misc.IsMico2Mode())
      {
        // Reset account balance. 
        _to_add = new MultiPromos.AccountBalance(RedeemableAmountToAdd, 0, 0);

        Utils.AccountBalanceOut(_input_params.AccountId
                                , _to_add
                                , 0
                                , out _ini_balance
                                , out _fin_balance
                                , Trx);

        // Add substract movement. 
        _account_movements.Add(_operation_id
                                , _input_params.AccountId
                                , MovementType.TITO_AccountToTerminalCredit
                                , _ini_balance.TotalBalance // Must be 0 + RedeemableAmountToAdd
                                , RedeemableAmountToAdd
                                , 0
                                , _fin_balance.TotalBalance);  // Must be 0
      }

      // 11. Save accounts and cashier movements
      if (!_account_movements.Save(Trx))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB_TransferAmountToPlayerAccount: AccountMovements.Save failed. Account: " + _account.id.ToString() + ".");
      }

      if (!_cashier_movements.Save(Trx))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB_TransferAmountToPlayerAccount: CashierMovements.Save failed. Account: " + _account.id.ToString() + ".");
      }

      PreviousAccountBalance = _output_params.InitialBalance.TotalBalance;
      CurrentAccountBalance = _output_params.FinalBalance.TotalBalance;
      VoucherList = _output_params.VoucherList;

      return true;

    } // DB_TransferAmountToPlayerAccount

    //------------------------------------------------------------------------------
    // PURPOSE : Checks if amount and balance is ok and adds credit to the account.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Account
    //          - MBUserType
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: TRUE If amount can be transferred to account.
    //      
    //
    private static Boolean CanTransferToAccount(WCP_AccountManager.WCP_Account Account, MB_USER_TYPE MBUserType, SqlTransaction Trx)
    {
      return true;
    } // CanTransferToAccount


    //------------------------------------------------------------------------------
    // PURPOSE : Read player account.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Account: It has value for MoneyEvent (MB_USER_TYPE.SYSTEM)
    //          - PlayerTrackData: Account trackdata
    //          - MBUserType: Mobile Bank user type
    //          - Trx: Transaction
    //
    //      - OUTPUT :
    //          - CurrentAccount
    //
    // RETURNS :
    //      
    //
    private static void ReadPlayerAccount(WCP_AccountManager.WCP_Account Account, String PlayerTrackData, MB_USER_TYPE MBUserType,
                                          SqlTransaction Trx, out WCP_AccountManager.WCP_Account CurrentAccount)
    {

      CurrentAccount = null;

      if (MBUserType == MB_USER_TYPE.NOT_ASSIGNED)
      {
        return;
      }

      if (!Cashier.MobileBankIsAcceptor(MBUserType))
      {
        CurrentAccount = WCP_AccountManager.Account(PlayerTrackData, Trx);
        if (CurrentAccount == null)
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
        }
        if (CurrentAccount.type == AccountType.ACCOUNT_UNKNOWN)
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
        }
        if (CurrentAccount.blocked)
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_BLOCKED);
        }
      }
      else
      {
        if (Account.type == AccountType.ACCOUNT_UNKNOWN)
        {
          Log.Message("*** Money Events: Card Id " + Account.id + " not valid. Account type unknown");
        }

        if (Account.blocked)
        {
          Log.Message("*** Money Events: Card Id " + Account.id + " not valid. Account blocked");
        }
        CurrentAccount = Account;
      }

    } //ReadPlayerAccount

    //------------------------------------------------------------------------------
    // PURPOSE : Read mobile account
    //
    //  PARAMS :
    //      - INPUT :
    //          - MBTrackData: Mobile bank trackdata
    //          - Trx: Transaction
    //
    //      - OUTPUT :
    //          - AccountIdMobileBank
    //          - CashierSessionId
    //          - MBBalance
    //          - MBHolderName
    //          - MBAccountType
    //
    // RETURNS :
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved      
    //
    private static Boolean ReadMBAccount(String MBTrackData, SqlTransaction Trx, out Int64 AccountIdMobileBank, out Int64 CashierSessionId,
                                      out Decimal MBBalance, out String MBHolderName, out MB_USER_TYPE MBAccountType)
    {

      String _sql_txt;

      //Inicialization
      AccountIdMobileBank = 0;
      CashierSessionId = 0;
      MBBalance = 0;
      MBHolderName = "";
      MBAccountType = MB_USER_TYPE.NOT_ASSIGNED;


      _sql_txt = "SELECT   MB_ACCOUNT_ID                 " +
                 "       , MB_CASHIER_SESSION_ID         " +
                 "       , MB_BALANCE                    " +
                 "       , MB_HOLDER_NAME                " +
                 "       , MB_ACCOUNT_TYPE               " +
                 "  FROM   MOBILE_BANKS                  " +
                 " WHERE   MB_TRACK_DATA = @pMBTrackdata ";

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pMBTrackdata", SqlDbType.NVarChar).Value = MBTrackData;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            if (!_reader.IsDBNull(0)) { AccountIdMobileBank = _reader.GetInt64(0); }
            if (!_reader.IsDBNull(1)) { CashierSessionId = _reader.GetInt64(1); }
            if (!_reader.IsDBNull(2)) { MBBalance = _reader.GetDecimal(2); }
            if (!_reader.IsDBNull(3)) { MBHolderName = _reader.GetString(3); }
            if (!_reader.IsDBNull(4)) { MBAccountType = (MB_USER_TYPE)_reader.GetInt16(4); }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } //ReadMBAccount

    //------------------------------------------------------------------------------
    // PURPOSE : Read mobile account for note acceptor
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountIdMobileBank: Mobile bank account id
    //          - Trx: Transaction
    //
    //      - OUTPUT :
    //          - MBCashierSessionId
    //          - MBBalance
    //          - MBHolderName
    //          - MBAccountType
    //
    // RETURNS :
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    //
    private static Boolean ReadMBAccountNoteAcceptor(Int64 AccountIdMobileBank, SqlTransaction Trx, out Int64 MBCashierSessionId,
                                      out Decimal MBBalance, out String MBHolderName, out MB_USER_TYPE MBAccountType)
    {

      String _sql_txt;

      //Inicialization
      MBCashierSessionId = 0;
      MBBalance = 0;
      MBHolderName = "";
      MBAccountType = MB_USER_TYPE.NOT_ASSIGNED;


      _sql_txt = "SELECT   MB_CASHIER_SESSION_ID                        " +
                 "       , MB_BALANCE                                   " +
                 "       , MB_HOLDER_NAME                               " +
                 "       , MB_ACCOUNT_TYPE                              " +
                 "  FROM   MOBILE_BANKS                                 " +
                 " WHERE   MB_ACCOUNT_ID = @pAccountIdMobileBank        ";

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pAccountIdMobileBank", SqlDbType.BigInt).Value = AccountIdMobileBank;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            if (!_reader.IsDBNull(0)) { MBCashierSessionId = _reader.GetInt64(0); }
            if (!_reader.IsDBNull(1)) { MBBalance = _reader.GetDecimal(1); }
            if (!_reader.IsDBNull(2)) { MBHolderName = _reader.GetString(2); }
            if (!_reader.IsDBNull(3)) { MBAccountType = (MB_USER_TYPE)_reader.GetInt16(3); }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } //ReadMBAccountNoteAcceptor

  }
}
