﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TerminalsTimeDisconnected.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 13-DEC-2017
// 
// REVISION HISTORY:
// 
// Date         Author  Description
// -----------  ------  ---------------------------------------------------------
// 13-DEC-2017  AMF     First release.
//-------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using WSI.Common;

namespace WSI.WCP
{
  public static class WCP_TerminalsTimeDisconnected
  {
    /// <summary>
    /// Initializes Terminals time disconnected thread
    /// </summary>
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(GenerateTerminalsTimeDisconnected);
      _thread.Name = "GenerateTerminalsTimeDisconnected";
      _thread.Start();

    } // Init

    /// <summary>
    /// Generate terminals time disconnected data
    /// </summary>
    static private void GenerateTerminalsTimeDisconnected()
    {
      Int32 _time_seconds;
      Boolean _operations_allowed;
      Boolean _updated_terminals_enabled;

      _time_seconds = 300;

      while (true)
      {
        try
        {
          if (!GeneralParam.GetBoolean("TerminalsTimeDisconnected", "Thread.Enabled", false))
          {
            Thread.Sleep(300 * 1000); // Disabled wait: 5 min

            continue;
          }

          if (!OperationsSchedule.OperationsAllowed(WGDB.Now, out _operations_allowed, out _updated_terminals_enabled))
          {
            Thread.Sleep(300 * 1000); // Disabled wait: 5 min

            continue;
          }

          if (!_operations_allowed)
          {
            Thread.Sleep(300 * 1000); // Disabled wait: 5 min

            continue;
          }

          // min => 30 seconds. max => 24 h. - Default 300 seconds
          _time_seconds = GeneralParam.GetInt32("TerminalsTimeDisconnected", "Thread.WaitSeconds", 300, 30, 86400);

          if (Services.IsPrincipal("WCP"))
          {
            if (!TerminalsTimeDisconnected(_time_seconds))
            {
              Log.Warning("WCP_TerminalsTimeDisconnected. Exception in function: GenerateTerminalsTimeDisconnected");
            }
          }

          Thread.Sleep(_time_seconds * 1000);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    } // GenerateTerminalsTimeDisconnected

    /// <summary>
    /// Terminals time disconnected
    /// </summary>
    /// <param name="TimeSeconds"></param>
    /// <returns></returns>
    static private Boolean TerminalsTimeDisconnected(Int32 TimeSeconds)
    {
      DataTable _dt_terminals;
      StringBuilder _sb;

      try
      {
        _dt_terminals = new DataTable();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand("SP_TerminalsTimeDisconnected", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandType = CommandType.StoredProcedure;
            _sql_cmd.Parameters.Add("@pTimeSeconds", SqlDbType.Int).Value = TimeSeconds;

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
            {
              _da.Fill(_dt_terminals);
            }
          }

          if (_dt_terminals.Rows.Count > 0)
          {
            foreach (DataRow _dr in _dt_terminals.Rows)
            {
              _dr.SetModified();
            }

            _sb = new StringBuilder();
            _sb.AppendLine("IF NOT EXISTS (SELECT   1                                                                       ");
            _sb.AppendLine("                 FROM   TERMINALS_TIME_DISCONNECTED                                             ");
            _sb.AppendLine("                WHERE   TTD_SITE_ID     = @pSiteId                                              ");
            _sb.AppendLine("                  AND   TTD_TERMINAL_ID = @pTerminalId                                          ");
            _sb.AppendLine("                  AND   TTD_WORKING_DAY = @pWorkingDay)                                         ");
            _sb.AppendLine("BEGIN                                                                                           ");
            _sb.AppendLine("INSERT INTO TERMINALS_TIME_DISCONNECTED                                                         ");
            _sb.AppendLine("            (                                                                                   ");
            _sb.AppendLine("              TTD_SITE_ID                                                                       ");
            _sb.AppendLine("            , TTD_TERMINAL_ID                                                                   ");
            _sb.AppendLine("            , TTD_WORKING_DAY                                                                   ");
            _sb.AppendLine("            , TTD_EGM_COLLECTOR_SECONDS                                                         ");
            _sb.AppendLine("            , TTD_COLLECTOR_CENTER_SECONDS                                                      ");
            _sb.AppendLine("            )                                                                                   ");
            _sb.AppendLine("    VALUES  (                                                                                   ");
            _sb.AppendLine("              @pSiteId                                                                          ");
            _sb.AppendLine("            , @pTerminalId                                                                      ");
            _sb.AppendLine("            , @pWorkingDay                                                                      ");
            _sb.AppendLine("            , @pEGMCollectorSeconds                                                             ");
            _sb.AppendLine("            , @pCollectorCenterSeconds                                                          ");
            _sb.AppendLine("            )                                                                                   ");
            _sb.AppendLine("END                                                                                             ");
            _sb.AppendLine("ELSE                                                                                            ");
            _sb.AppendLine("BEGIN                                                                                           ");
            _sb.AppendLine("UPDATE   TERMINALS_TIME_DISCONNECTED                                                            ");
            _sb.AppendLine("   SET   TTD_EGM_COLLECTOR_SECONDS    = TTD_EGM_COLLECTOR_SECONDS    + @pEGMCollectorSeconds    ");
            _sb.AppendLine("       , TTD_COLLECTOR_CENTER_SECONDS = TTD_COLLECTOR_CENTER_SECONDS + @pCollectorCenterSeconds ");
            _sb.AppendLine(" WHERE   TTD_SITE_ID     = @pSiteId                                                             ");
            _sb.AppendLine("   AND   TTD_TERMINAL_ID = @pTerminalId                                                         ");
            _sb.AppendLine("   AND   TTD_WORKING_DAY = @pWorkingDay                                                         ");
            _sb.AppendLine("END                                                                                             ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.BigInt).Value = GeneralParam.GetInt32("Site", "Identifier", 0); ;
              _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TE_TERMINAL_ID";
              _sql_cmd.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = Misc.TodayOpening();
              _sql_cmd.Parameters.Add("@pEGMCollectorSeconds", SqlDbType.Int).SourceColumn = "EGM_COLLECTOR_SECONDS";
              _sql_cmd.Parameters.Add("@pCollectorCenterSeconds", SqlDbType.Int).SourceColumn = "COLLECTOR_CENTER_SECONDS";

              using (SqlDataAdapter _da = new SqlDataAdapter())
              {
                _da.InsertCommand = _sql_cmd;
                _da.UpdateCommand = _sql_cmd.Clone();
                _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

                if (_da.Update(_dt_terminals) != _dt_terminals.Rows.Count)
                {
                  Log.Warning("WCP_TerminalsTimeDisconnected.TerminalsTimeDisconnected: Not all data has been updated");

                  return false;
                }
                else
                {
                  _db_trx.Commit();
                }
              }
            }
          }

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // TerminalsTimeDisconnected
  }
}
