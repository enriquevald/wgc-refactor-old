using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.TITO;
using System.Data.SqlClient;
using WSI.Common;

public static partial class WCP_TITO
{
  public static Boolean TITO_ProcessChangeStacker(Int32 TerminalContainerId,
                                                Stacker NewStacker,
                                         SqlTransaction Trx)
  {
    String _description;
    String _stacker_id;
    String _stacker_status;
    Boolean _is_ok;
    _is_ok = WCP_TITO.ChangeStackerInTitoMode(TerminalContainerId,
                                              NewStacker,
                                              Trx);

    if (!_is_ok)
    {
      _stacker_id = NewStacker == null ? "-" : NewStacker.StackerId.ToString();
      _stacker_status = NewStacker == null ? "-" : NewStacker.Status.ToString();
      _description = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_INSERT_STACKER_ERROR",
                                     _stacker_id,
                                     _stacker_status);

      Alarm.Register(AlarmSourceCode.TerminalWCP,
                     TerminalContainerId,
                     "TITO_WCP",
                     (UInt32)AlarmCode.WCP_TITO_StackerChangeFail,
                     _description,
                     AlarmSeverity.Error,
                     WGDB.Now,
                     Trx);
    }

    return _is_ok;
  }

  //------------------------------------------------------------------------------
  // PURPOSE : Stacker change in TITO mode. 
  //           Updates stackers and money_collections tables.
  //
  //  PARAMS :
  //      - INPUT :
  //          - Terminal container
  //          - Stacker id
  //          - DataRow object
  //          - SqlTransaction Trx
  //
  //      - OUTPUT : Populated Stacker object with stacker id
  //
  // RETURNS : true if all ok, otherwise false.
  //
  private static Boolean ChangeStackerInTitoMode(Int32 TerminalContainerId,
                                               Stacker NewStacker,
                                        SqlTransaction Trx)
  {
    String _description;
    Stacker _old_stacker;
    MoneyCollection _money_collection;
    Boolean _stacker_status_error;

    if (TerminalContainerId <= 0)
    {
      return false;
    }

    try
    {

      // Extract old stacker
      if (!MoneyCollection.DB_GetUnextractedStacker(TerminalContainerId,
                                                    out _old_stacker,
                                                    out _money_collection,
                                                    Trx))
      {
        if (_old_stacker == null)
        {
          _description = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TERMINAL_EMPTY");

          Alarm.Register(AlarmSourceCode.TerminalWCP,
                         TerminalContainerId,
                         "TITO_WCP",
                         (UInt32)AlarmCode.WCP_TITO_EmptyTerminal,
                         _description,
                         AlarmSeverity.Error,
                         WGDB.Now,
                         Trx);
        }

        if (_money_collection == null)
        {
          _description = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TERMINAL_NONE_EXTRACT_PENDING_COLLECTION"); 

          Alarm.Register(AlarmSourceCode.TerminalWCP,
                         TerminalContainerId,
                         "TITO_WCP",
                         (UInt32)AlarmCode.WCP_TITO_NoneExtractPendingCollection,
                         _description,
                         AlarmSeverity.Error,
                         WGDB.Now,
                         Trx);
        }

        return false;
      }

      if (!_money_collection.DB_ExtractStacker(Trx))
      {
        return false;
      }

      if (!_old_stacker.DB_RemoveFromTerminal(Trx))
      {
        return false;
      }

      // Insert new stacker
      if (NewStacker != null)
      {
        _stacker_status_error = false;

        switch (NewStacker.Status)
        {
          case TITO_STACKER_STATUS.DISABLED:
          case TITO_STACKER_STATUS.INSERTED_VALIDATED:
          case TITO_STACKER_STATUS.COLLECTION_PENDING:
            _stacker_status_error = true;
            break;

          case TITO_STACKER_STATUS.DEACTIVATED:
            _description = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_INSERT_STATE_STACKER_ERROR",
                                           NewStacker.StackerId,
                                           NewStacker.Status,
                                           TITO_STACKER_STATUS.INSERTED_PENDING.ToString());

            Alarm.Register(AlarmSourceCode.TerminalWCP,
                           TerminalContainerId,
                           "TITO_WCP",
                           (UInt32)AlarmCode.WCP_TITO_WrongStackerState,
                           _description,
                           AlarmSeverity.Error,
                           WGDB.Now,
                           Trx);
            break;

          case TITO_STACKER_STATUS.INSERTED_PENDING:
            if (NewStacker.Inserted != TerminalContainerId)
            {
              _description = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_INSERT_STACKER_WITH_WRONG_TERMINAL_ASSIGNED",
                                             NewStacker.StackerId,
                                             NewStacker.Inserted);

              Alarm.Register(AlarmSourceCode.TerminalWCP,
                             TerminalContainerId,
                             "TITO_WCP",
                             (UInt32)AlarmCode.WCP_TITO_WrongStackerTerminal,
                             _description,
                             AlarmSeverity.Error,
                             WGDB.Now,
                             Trx);
            }
            break;

          case TITO_STACKER_STATUS.AVAILABLE:
          default:
            break;
        }

        if (_stacker_status_error)
        {
          return false;
        }

        if (!NewStacker.DB_AssignToTerminal(TerminalContainerId, Trx))
        {
          return false;
        }

        if (!MoneyCollection.DB_CreateMoneyCollection(TerminalContainerId, NewStacker.StackerId, Trx))
        {
          return false;
        }
      }
      else
      {
        if (!MoneyCollection.DB_CreateMoneyCollection(TerminalContainerId, 0, Trx))
        {
          return false;
        }

        _description = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TERMINAL_NEW_COLLECTION_WITHOUT_STACKER");

        Alarm.Register(AlarmSourceCode.TerminalWCP,
                       TerminalContainerId,
                       "TITO_WCP",
                       (UInt32)AlarmCode.WCP_TITO_NewCollectionWithoutStacker,
                       _description,
                       AlarmSeverity.Error,
                       WGDB.Now,
                       Trx);
      }

      return true;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      Log.Warning("ChangeStacker error (TITO mode)");
      return false;
    }
  }
}
