//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Commands.cs
// 
//   DESCRIPTION: WCP_Commands class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 21-MAY-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2010 ACC    First release.
// 02-APR-2014 RCI    Fixed Bug WIG-788: Deadlock in DB_TimeoutWcpCommands
// 12-NOV-2014 FJC    Added SiteJackPotAwarded message code for showing in display LCD
// 06-OCT-2015 FJC    Product Backlog 4704
// 16-MAR-2016 FJC    PBI 9105: BonoPlay: LCD: Cambios varios
// 22-JUN-2017 FJC    WIGOS-3142 WS2S - Set / Retrieve Reserved Credit on the Cashier
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Threading;
using System.Xml;

namespace WSI.WCP
{

  #region Enums

  public enum WCP_CommandStatus
  {
    Pending = 0,
    Sent = 1,
    RepliedOk = 2,
    RepliedError = 3,
    TimeoutDisconnected = 4,
    TimeoutNotReply = 5,
    Processed = 6
  }

  #endregion

  #region ShowMessageParameters

  public class ShowMessageParameters
  {
    public String Line1 = "";
    public String Line2 = "";
    public Int32 TimeShowMessage = 0;
    public Int32 NumTimes = 0;
    public Int32 TimeBetweenMessages = 0;

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<ShowMessageParameters>");
      _sb.AppendLine("<Line1>" + Line1 + "</Line1>");
      _sb.AppendLine("<Line2>" + Line2 + "</Line2>");
      _sb.AppendLine("<TimeShowMessage>" + TimeShowMessage.ToString() + "</TimeShowMessage>");
      _sb.AppendLine("<NumTimes>" + NumTimes.ToString() + "</NumTimes>");
      _sb.AppendLine("<TimeBetweenMessages>" + TimeBetweenMessages.ToString() + "</TimeBetweenMessages>");
      _sb.AppendLine("</ShowMessageParameters>");

      return _sb.ToString();
    }

    public void LoadXml(String XmlData)
    {
      XmlDocument _xml;
      XmlNode _node;

      _xml = new XmlDocument();

      try
      {
        _xml.LoadXml(XmlData);
        _node = _xml.SelectSingleNode("ShowMessageParameters");
        Line1 = XML.GetValue(_node, "Line1");
        Line2 = XML.GetValue(_node, "Line2");
        TimeShowMessage = Int32.Parse(XML.GetValue(_node, "TimeShowMessage"));
        NumTimes = Int32.Parse(XML.GetValue(_node, "NumTimes"));
        TimeBetweenMessages = Int32.Parse(XML.GetValue(_node, "TimeBetweenMessages"));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
  }

  #endregion // ShowMessageParameters

  public static class WCP_Commands
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Init wcp commands thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(WcpCommandsThread);
      _thread.Name = "WcpCommands";
      _thread.Start();

      _thread = new Thread(WSI.WCP.MassiveBonus.BonusingThread);
      _thread.Name = "Bonusing";
      _thread.Start();

      if (Misc.SystemMode() == SYSTEM_MODE.WASS)
      {
        _thread = new Thread(WSI.WCP.SwValidation.SWValidationThread);
        _thread.Name = "SoftwareValidation";
        _thread.Start();
      }
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Process pending wcp commands
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private void WcpCommandsThread()
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Random _rnd;
      DataTable _wcp_commands;
      Int64 _command_id;
      Int32 _terminal_id;
      WCP_CommandCode _command_code;
      String _command_parameter;

      // Wait 1 minute

      System.Threading.Thread.Sleep(60000);

      _rnd = new Random(Environment.TickCount);

      while (true)
      {
        // Wait 1 second
        System.Threading.Thread.Sleep(1000 + _rnd.Next(500));

        _sql_conn = null;
        _sql_trx = null;

        try
        {
          //
          // Connect to DB
          //
          _sql_conn = WGDB.Connection();

          if (_sql_conn == null)
          {
            continue;
          }

          // Check connection states
          if (_sql_conn.State != ConnectionState.Open)
          {
            if (_sql_conn.State != ConnectionState.Broken)
            {
              _sql_conn.Close();
            }

            _sql_conn.Dispose();
            _sql_conn = null;

            continue;
          }

          // RCI 02-APR-2014: Only update Timeouts if is principal. Avoid Deadlocks.
          if (Services.IsPrincipal("WCP"))
          {
            try
            {
              _sql_trx = _sql_conn.BeginTransaction();

              // Update Timeout commands
              if (DB_TimeoutWcpCommands(_sql_trx))
              {
                _sql_trx.Commit();
              }
            }
            catch (Exception _ex)
            {
              Log.Exception(_ex);

              // Don't exit. Continue work...
            }
            finally
            {
              if (_sql_trx != null)
              {
                if (_sql_trx.Connection != null)
                {
                  _sql_trx.Rollback();
                }
                _sql_trx = null;
              }
            }
          }

          _sql_trx = _sql_conn.BeginTransaction();

          // Select Pending Commands
          if (!GetPendingCommands(out _wcp_commands, _sql_trx))
          {
            continue;
          }

          foreach (DataRow _dr in _wcp_commands.Rows)
          {
            _command_id = (Int64)_dr["CMD_ID"];
            _terminal_id = (Int32)_dr["CMD_TERMINAL_ID"];
            _command_code = (WCP_CommandCode)_dr["CMD_CODE"];

            _command_parameter = "";
            if (!_dr.IsNull("CMD_PARAMETER"))
            {
              _command_parameter = (String)_dr["CMD_PARAMETER"];
            }

            // Sent Command
            if (!SentWcpCommand(_command_id, _terminal_id, _command_code, _command_parameter))
            {
              continue;
            }

            _dr["CMD_STATUS"] = (Int32)WCP_CommandStatus.Sent;

          } // foreach

          // Update sent commands
          if (!DB_UpdateWcpCommands(_wcp_commands, _sql_trx))
          {
            continue;
          }

          _sql_trx.Commit();

          //Process WCP_Commands Gamegateway 
          if (Misc.IsGameGatewayEnabled())
          {
            if (!WCP_Commands_GameGateway.ProcessCommandsGameGateway())
            {
              //Write Logger
            }
          }

        }

        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              _sql_trx.Rollback();
            }
            _sql_trx = null;
          }

          if (_sql_conn != null)
          {
            _sql_conn.Close();
            _sql_conn = null;
          }
        }

      } // while

    } // WcpCommandsThread


    //------------------------------------------------------------------------------
    // PURPOSE : Get pending commands for send to kiosks
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //          - PendingCommands
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Found pending commands.
    //      - false: error or not found.
    //
    public static Boolean GetPendingCommands(out DataTable PendingCommands, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;

      PendingCommands = null;

      try
      {
        PendingCommands = new DataTable("WCP_COMMANDS");

        //    - Set SqlDataAdapter commands
        //        - SelectCommand 
        _da = new SqlDataAdapter();

        //        - SelectCommand 
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT CMD_ID");
        _sql_txt.AppendLine("     , CMD_TERMINAL_ID");
        _sql_txt.AppendLine("     , CMD_CODE");
        _sql_txt.AppendLine("     , CMD_PARAMETER");
        _sql_txt.AppendLine("     , CMD_STATUS");
        _sql_txt.AppendLine("  FROM WCP_COMMANDS");
        _sql_txt.AppendLine(" WHERE CMD_STATUS = " + ((Int32)WCP_CommandStatus.Pending).ToString());

        _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
        _da.SelectCommand.Connection = Trx.Connection;
        _da.SelectCommand.Transaction = Trx;

        //    - Execute Select command
        _da.Fill(PendingCommands);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    } // GetPendingCommands

    //------------------------------------------------------------------------------
    // PURPOSE : Update Wcp Commands 
    //
    //  PARAMS :
    //      - INPUT :
    //          - CommandId
    //          - TerminalId
    //          - CommandCode
    //          - Parameter
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: wcp command sent.
    //      - false: not sent: no connected or error.
    //
    public static Boolean SentWcpCommand(Int64 CommandId, Int32 TerminalId, WCP_CommandCode CommandCode, String Parameter)
    {
      WCP_Message _wcp_request;
      DataRow _terminal;

      _wcp_request = null;

      try
      {
        switch (CommandCode)
        {
          case WCP_CommandCode.GetLogger:
            {
              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgGetLogger);
            }
            break;

          case WCP_CommandCode.Restart:
            {
              WCP_MsgExecuteCommand _request;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_RESTART;
              _request.CommandParameters = "";
            }
            break;

          case WCP_CommandCode.SiteJackpotAwarded: //FJC 12-NOV-2014
            {
              WCP_MsgExecuteCommand _request;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_SITE_JACKPOT_AWARDED;
              _request.CommandParameters = Parameter;
            }
            break;

          case WCP_CommandCode.RequestTransfer:
            {
              if (!WCP.Server.IsConnected(TerminalId))
              {
                return false;
              }

              WCP_MsgExecuteCommand _request;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_REQUEST_TRANSFER;
              _request.CommandParameters = "";

              //Set the terminal transfer status to NOTIFIED
              using (DB_TRX _db_trx = new DB_TRX())
              {
                if (!TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(TerminalId,
                                                                                 TerminalFundsTransfer.TRANSFER_STATUS.NOTIFIED,
                                                                                 _db_trx.SqlTransaction))
                {
                  Log.Warning("RequestTransfer -> WcpCommand Not Sent [DB_UpdateTerminalFundsTransferStatus failed]: TerminalId: " + TerminalId.ToString() + ", CommandId: " + CommandId.ToString() + ", CommandCode: " + CommandCode.ToString());

                  return false;
                }

                _db_trx.Commit();
              }

            }
            break;

          case WCP_CommandCode.SubsMachinePartialCredit: // NOT USED FOR NOW
            {
              WCP_MsgExecuteCommand _request;
              WCP_MsgSubsMachinePartialCredit _test_xml;

              _test_xml = new WCP_MsgSubsMachinePartialCredit();

              //Harcoded temporally
              //_test_xml.RequestAmount = 0;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);
              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_SUBS_MACHINE_PARTIAL_CREDIT;
              _request.CommandParameters = _test_xml.ToXml();
            }
            break;

          case WCP_CommandCode.GameGateway:
            {
              WCP_MsgExecuteCommand _request;
              WCP_MsgGameGateway _test_xml;

              _test_xml = new WCP_MsgGameGateway();
              _test_xml.MsgText = Parameter;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_GAMEGATEWAY;
              _request.CommandParameters = _test_xml.ToXml();
            }
            break;

          case WCP_CommandCode.GameGatewayGetCredit:
            {
              WCP_MsgExecuteCommand _request;
              WCP_MsgGameGatewayGetCredit _test_xml;

              _test_xml = new WCP_MsgGameGatewayGetCredit();
              _test_xml.MsgText = Parameter;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);
              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_GAMEGATEWAY_GET_CREDIT;
              _request.CommandParameters = _test_xml.ToXml();
            }
            break;

          case WCP_CommandCode.RequestMeters: //JMM 15-OCT-2015
            {
              WCP_MsgExecuteCommand _request;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_REQUEST_METERS;
              _request.CommandParameters = Parameter;
            }
            break;

          case WCP_CommandCode.RequestChangeStacker:
            {
              WCP_MsgExecuteCommand _request;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_REQUEST_CHANGE_STACKER;
              _request.CommandParameters = Parameter;
            }
            break;

          case WCP_CommandCode.RequestUpdatePCDMeters:
            {
              WCP_MsgExecuteCommand _request;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_REQUEST_UPDATE_PCD_METERS;
              _request.CommandParameters = Parameter;
            }
            break;
          case WCP_CommandCode.MobiBankRechargeNotification: // Recharge Notification (into Account or EGM)
            {
              WCP_MsgExecuteCommand _request;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_MOBIBANK_RECHARGE_NOTIFICATION;
              _request.CommandParameters = Parameter;
            }

            break;

          case WCP_CommandCode.MobiBankRequestTransFer: // Notifiction to recharge Account and follow EGM 
            {
              WCP_MsgExecuteCommand _request;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_MOBIBANK_REQUEST_TRANSFER;
              _request.CommandParameters = Parameter;
            }

            break;

          case WCP_CommandCode.MobiBankCancelRequest: // Notification to cancel recharge from App Movil
            {
              WCP_MsgExecuteCommand _request;

              _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

              _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
              _request.Command = WCP_CommandTypes.WCP_CMD_MOBIBANK_CANCEL_REQUEST;
              _request.CommandParameters = Parameter;
            }

            break;


          default:
            {
              return false;
            }
          // Unreachable
          //break;
        }

        if (!WCP.Server.IsConnected(TerminalId))
        {
          return false;
        }

        _terminal = DbCache.Terminal(TerminalId);

        // RCI & AJQ 22-MAR-2011: Can't send commands to these terminals (p.ex: RETIRED).
        if (_terminal.IsNull("TE_EXTERNAL_ID"))
        {
          return false;
        }

        _wcp_request.MsgHeader.TerminalId = (String)_terminal["TE_EXTERNAL_ID"];
        _wcp_request.MsgHeader.TerminalSessionId = 0;
        _wcp_request.MsgHeader.SequenceId = CommandId;

        if (!WCP.Server.SendTo(TerminalId, 0, _wcp_request.ToXml()))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Not Sent WcpCommand [Exception]: TerminalId: " + TerminalId.ToString() + ", CommandId: " + CommandId.ToString() + ", CommandCode: " + CommandCode.ToString());

        return false;
      }

    } // SentWcpCommand

    //------------------------------------------------------------------------------
    // PURPOSE : Update statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - WcpCommands: Datatable with wcp commands
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: wcp commands updated.
    //      - false: error.
    //
    static private Boolean DB_UpdateWcpCommands(DataTable WcpCommands, SqlTransaction Trx)
    {
      String _sql_str;
      SqlDataAdapter _da;
      int _nr;
      int _num_modified;

      _num_modified = WcpCommands.Select("", "", DataViewRowState.ModifiedCurrent).Length;

      if (_num_modified == 0)
      {
        return true;
      }

      try
      {
        //    - Set SqlDataAdapter commands

        _da = new SqlDataAdapter();

        //        - UpdateCommand 
        _sql_str = "";
        _sql_str += "UPDATE WCP_COMMANDS SET CMD_STATUS             = @pCommandStatus ";
        _sql_str += "                      , CMD_STATUS_CHANGED     = GETDATE() ";
        _sql_str += " WHERE   CMD_ID           = @pCommandId   ";
        _sql_str += "   AND   CMD_TERMINAL_ID  = @pTerminalID ";

        _da.UpdateCommand = new SqlCommand(_sql_str);
        _da.UpdateCommand.Connection = Trx.Connection;
        _da.UpdateCommand.Transaction = Trx;
        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        _da.UpdateCommand.Parameters.Add("@pCommandStatus", SqlDbType.Int, 4, "CMD_STATUS");
        _da.UpdateCommand.Parameters.Add("@pCommandId", SqlDbType.BigInt, 8, "CMD_ID");
        _da.UpdateCommand.Parameters.Add("@pTerminalID", SqlDbType.Int, 4, "CMD_TERMINAL_ID");

        //    - Execute Update command
        _da.UpdateBatchSize = 500;
        _da.ContinueUpdateOnError = true;
        _nr = _da.Update(WcpCommands);

        if (_num_modified != _nr)
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
      }

    } // DB_UpdateWcpCommands

    //------------------------------------------------------------------------------
    // PURPOSE : Update Wcp Commands 
    //
    //  PARAMS :
    //      - INPUT :
    //          - CommandId
    //          - TerminalId
    //          - CommandStatus
    //          - ResponseData
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: wcp command updated.
    //      - false: not found or error.
    //
    public static Boolean DB_UpdateWcpCommand(Int64 CommandId, Int32 TerminalId, WCP_CommandStatus CommandStatus, String ResponseData, SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      String _sql_str;
      Int32 _num_rows_updated;

      _sql_str = "";
      _sql_str += "UPDATE WCP_COMMANDS SET CMD_STATUS             = @pCommandStatus ";
      _sql_str += "                      , CMD_STATUS_CHANGED     = GETDATE() ";
      _sql_str += "                      , CMD_RESPONSE           = @pResponse ";
      _sql_str += " WHERE   CMD_ID           = @pCommandId   ";
      _sql_str += "   AND   CMD_TERMINAL_ID  = @pTerminalID ";

      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;
      _sql_command.Parameters.Add("@pCommandStatus", SqlDbType.Int).Value = (Int32)CommandStatus;
      _sql_command.Parameters.Add("@pCommandId", SqlDbType.BigInt).Value = CommandId;
      _sql_command.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
      if (ResponseData == "")
      {
        _sql_command.Parameters.Add("@pResponse", SqlDbType.NVarChar).Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@pResponse", SqlDbType.NVarChar).Value = ResponseData;
      }

      try
      {
        _num_rows_updated = _sql_command.ExecuteNonQuery();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Updating WCP_COMMANDS [Exception]: TerminalId: " + TerminalId.ToString() + ", pCommandId: " + CommandId.ToString() + ", CommandStatus: " + CommandStatus.ToString());

        return false;
      }

      if (_num_rows_updated != 1)
      {
        Log.Warning("Updating WCP_COMMANDS [Not updated]: TerminalId: " + TerminalId.ToString() + ", pCommandId: " + CommandId.ToString() + ", CommandStatus: " + CommandStatus.ToString());

        return false;
      }

      return true;

    } // DB_UpdateWcpCommand

    //------------------------------------------------------------------------------
    // PURPOSE : Timeout Wcp Commands 
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static Boolean DB_TimeoutWcpCommands(SqlTransaction Trx)
    {
      List<long> _list_ids;
      int _count = 0;

      StringBuilder _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   CMD_ID ");
      _sb.AppendLine("   FROM   WCP_COMMANDS  ");
      _sb.AppendLine("  WHERE ( CMD_STATUS = @pStatusPending AND DATEDIFF(SECOND , CMD_STATUS_CHANGED, GETDATE()) >= 10 ) ");
      _sb.AppendLine("    OR ( CMD_STATUS = @pStatusSent AND DATEDIFF(SECOND , CMD_STATUS_CHANGED, GETDATE()) >= 60 )");

      // Fill id list
      _list_ids = new List<long>();
      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = (Int32)WCP_CommandStatus.Pending;
        _sql_cmd.Parameters.Add("@pStatusSent", SqlDbType.Int).Value = (Int32)WCP_CommandStatus.Sent;

        using (SqlDataReader _sql_read = _sql_cmd.ExecuteReader())
        {
          while (_sql_read.Read())
          {
            _list_ids.Add(_sql_read.GetInt64(0));
          }
        }
      }

      try
      {
        // Update ids       
        foreach (Int64 _id in _list_ids)
        {
          _count++;
          if (_count >= 10)
          {
            if (!Services.IsPrincipal("WCP"))
            {
              return true;
            }
            _count = 0;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sb = new StringBuilder();
            _sb.AppendLine(" UPDATE WCP_COMMANDS SET CMD_STATUS             = CASE WHEN (CMD_STATUS = @pStatusPending) THEN @pStatusTimeoutDisconnected ELSE @pStatusTimeoutNotReply END ");
            _sb.AppendLine("                       , CMD_STATUS_CHANGED     = GETDATE() ");
            _sb.AppendLine(" WHERE (CMD_ID = @pCmdId)");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = (Int32)WCP_CommandStatus.Pending;
              _sql_cmd.Parameters.Add("@pStatusTimeoutDisconnected", SqlDbType.Int).Value = (Int32)WCP_CommandStatus.TimeoutDisconnected;
              _sql_cmd.Parameters.Add("@pStatusTimeoutNotReply", SqlDbType.Int).Value = (Int32)WCP_CommandStatus.TimeoutNotReply;
              _sql_cmd.Parameters.Add("@pCmdId", SqlDbType.BigInt).Value = _id;
              _sql_cmd.ExecuteNonQuery();
              _db_trx.Commit();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Updating Timeout WCP_COMMANDS [Exception]");

        return false;
      }

      return true;
    }
  }

  #region WcpCommand GAMEGATEWAY
  
  public static class WCP_Commands_GameGateway
  {
    /// <summary>
    /// 
    /// </summary>
    public static Boolean ProcessCommandsGameGateway()
    {
      DataTable _dt_pending_commands_gamegateway;
      Int64 _command_id;
      Int32 _terminal_id;
      GameGateway.TYPE_WCP _command_type;
      WCP_CommandCode _command_code;
      String _command_parameter;
      Boolean _has_registrers;
      Boolean _has_changes;
      Dictionary<Int32, GameGateway.TYPE_WCP> _terminals_msg;

      _dt_pending_commands_gamegateway = null;
      _has_registrers = false;
      _has_changes = false;
      _terminals_msg = new Dictionary<Int32, GameGateway.TYPE_WCP>();

      try
      {
        //1. Get Pending Gamegateway commands 
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _has_registrers = GetPendingCommandsGameGateway(out _dt_pending_commands_gamegateway, _db_trx.SqlTransaction);
        }

        if (_has_registrers && _dt_pending_commands_gamegateway.Rows.Count > 0)
        {
          foreach (DataRow _dr in _dt_pending_commands_gamegateway.Rows)
          {
            _command_id = (Int64)_dr["GCM_ID"];
            _terminal_id = (Int32)_dr["GCM_TERMINAL_ID"];
            _command_code = (WCP_CommandCode)_dr["GCM_CODE"];
            _command_parameter = !_dr.IsNull("GCM_PARAMETER") ? (String)_dr["GCM_PARAMETER"] : String.Empty;
            _command_type = (GameGateway.TYPE_WCP)_dr["GCM_TYPE"];
            
            // Don't sent 2 message to same Terminal
            if (!_terminals_msg.ContainsKey(_terminal_id))
            {
              if (_command_type != GameGateway.TYPE_WCP.DEBIT)
              {
                _terminals_msg.Add(_terminal_id, _command_type);
              }
              //2. Send Command
              // FJC Execute commands with 0 --> Not inserted into WCP_COMMANDS table (reply not update status).
              if (!WCP_Commands.SentWcpCommand(0, _terminal_id, _command_code, _command_parameter))
              {
                continue;
              }

              if (_command_type == GameGateway.TYPE_WCP.DEBIT)
              {
                _dr["GCM_STATUS"] = (Int32)WCP_CommandStatus.Processed;
              }
              else
              {
                _dr["GCM_STATUS"] = (Int32)WCP_CommandStatus.Sent;
              }
            }

            if (_command_type == GameGateway.TYPE_WCP.DRAW)
            {
              _dr.Delete();
            }
            _has_changes = true;
          }

          if (_has_changes)
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              //3. Update Status (Sent)
              if (DB_UpdateWcpCommandsGameGateway(_dt_pending_commands_gamegateway, _db_trx.SqlTransaction))
              {
                _db_trx.Commit();
              }
            }
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    private static Boolean GetPendingCommandsGameGateway(out DataTable PendingCommands, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;

      PendingCommands = null;

      try
      {
        //TODO: Change for new table

        PendingCommands = new DataTable("GAMEGATEWAY_COMMAND_MESSAGES");

        //    - Set SqlDataAdapter commands
        //        - SelectCommand 
        _da = new SqlDataAdapter();

        //        - SelectCommand 
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("  SELECT   GCM_ID                                                                   ");
        _sql_txt.AppendLine("         , GCM_TERMINAL_ID                                                          ");
        _sql_txt.AppendLine("         , GCM_CODE                                                                 ");
        _sql_txt.AppendLine("         , GCM_TYPE                                                                 ");
        _sql_txt.AppendLine("         , GCM_PARAMETER                                                            ");
        _sql_txt.AppendLine("         , GCM_STATUS                                                               ");
        _sql_txt.AppendLine("    FROM   GAMEGATEWAY_COMMAND_MESSAGES                                             ");
        _sql_txt.AppendLine("   WHERE   GCM_TYPE   IN ( @pType1, @pType2, @pType3 )                               ");
        _sql_txt.AppendLine("     AND   GCM_STATUS IN ( @pStatus1, @pStatus2 )                                   ");
        _sql_txt.AppendLine("     AND   (GCM_STATUS_CHANGED IS NULL                                              ");
        _sql_txt.AppendLine("      OR   DATEDIFF(second, GCM_STATUS_CHANGED, GETDATE()) > 2 * 60)                "); //TODO ASK difference minutes 
        _sql_txt.AppendLine("ORDER BY   GCM_CODE DESC, GCM_TYPE DESC, GCM_TERMINAL_ID                            ");
        _sql_txt.AppendLine("         , ISNULL (GCM_STATUS_CHANGED, GCM_CREATED)                                 ");

        _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
        _da.SelectCommand.Parameters.Add("@pType1", SqlDbType.Int).Value = GameGateway.TYPE_WCP.DRAW;
        _da.SelectCommand.Parameters.Add("@pType2", SqlDbType.Int).Value = GameGateway.TYPE_WCP.PRIZE;
        _da.SelectCommand.Parameters.Add("@pType3", SqlDbType.Int).Value = GameGateway.TYPE_WCP.DEBIT;

        _da.SelectCommand.Parameters.Add("@pStatus1", SqlDbType.Int).Value = WCP_CommandStatus.Pending;
        _da.SelectCommand.Parameters.Add("@pStatus2", SqlDbType.Int).Value = WCP_CommandStatus.Sent;
        
        _da.SelectCommand.Connection = Trx.Connection;
        _da.SelectCommand.Transaction = Trx;

        //    - Execute Select command
        _da.Fill(PendingCommands);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    private static Boolean DB_UpdateWcpCommandsGameGateway(DataTable WcpCommands, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        //    - Set SqlDataAdapter commands
        
        using (SqlDataAdapter _da_command_messages = new SqlDataAdapter())
        {
          //- UpdateCommand 
          _sb = new StringBuilder();
          _sb.AppendLine("UPDATE  GAMEGATEWAY_COMMAND_MESSAGES         ");
          _sb.AppendLine("   SET  GCM_STATUS         = @pCommandStatus ");
          _sb.AppendLine("      , GCM_STATUS_CHANGED = GETDATE()       ");
          _sb.AppendLine(" WHERE  GCM_ID             = @pCommandId     ");

          using (SqlCommand _sql_cmd_update = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd_update.Parameters.Add("@pCommandStatus", SqlDbType.Int, 4, "GCM_STATUS");
            _sql_cmd_update.Parameters.Add("@pCommandId", SqlDbType.BigInt, 8, "GCM_ID");
            _sql_cmd_update.Parameters.Add("@pTerminalID", SqlDbType.Int, 4, "GCM_TERMINAL_ID");
            _sql_cmd_update.UpdatedRowSource = UpdateRowSource.None;
            _da_command_messages.UpdateCommand = _sql_cmd_update;
          }
          
          //- DeleteCommand 
          _sb = new StringBuilder();
          _sb.AppendLine(" DELETE FROM GAMEGATEWAY_COMMAND_MESSAGES WHERE GCM_ID = @pGcmId");

          using (SqlCommand _sql_cmd_delete = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd_delete.Parameters.Add("@pGcmId", SqlDbType.BigInt, System.Int32.MaxValue, "GCM_ID");
            _sql_cmd_delete.UpdatedRowSource = UpdateRowSource.None;
            _da_command_messages.DeleteCommand = _sql_cmd_delete;
          }

          //- BatchUpdate
          _da_command_messages.UpdateBatchSize = 500;
          _da_command_messages.Update(WcpCommands);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    /// <summary>
    /// Deletes a "gamegateway wcp_command
    /// </summary>
    public static Boolean DB_DeleteWcpCommandsGameGateway(long CmdId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("DELETE                                ");
        _sb.AppendLine("  FROM  GAMEGATEWAY_COMMAND_MESSAGES  ");
        _sb.AppendLine(" WHERE  GCM_ID = @pCmdId              ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pCmdId", SqlDbType.BigInt).Value = CmdId;
          _sql_cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Deleting GAMEGATEWAY_COMMAND_MESSAGES [Not Deleted]: CmdId: " + CmdId.ToString());

        return false;
      }
      return true;
    }
  }

  #endregion


}
