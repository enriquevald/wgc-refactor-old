using System;
using System.Collections.Generic;
using System.Text;
using WSI.WCP;
using WSI.Common;
using System.Data.SqlClient;

namespace WSI.WCP
{
  public static partial class WCP_Processor
  {

    private static void WCP_ProcessProtocolParameters(WCP_Message Request, WCP_Message Response, Int32 TerminalId, SqlTransaction _sql_trx)
    {
      WCP_MsgGetProtocolParameters _request;
      WCP_MsgGetProtocolParametersReply _response;
      StringBuilder _sb;
      Int32 _idx_input;
      Int32 _idx_output;
      Int64 _port_type;
      SMIB_COMMUNICATION_TYPE _protocol;

      try
      {
        _request = (WCP_MsgGetProtocolParameters)Request.MsgContent;
        _response = (WCP_MsgGetProtocolParametersReply)Response.MsgContent;

        _protocol = (SMIB_COMMUNICATION_TYPE)_request.Protocol;

        _response.SmibConfig.Protocol = _protocol;
        _sb = new StringBuilder();

        switch (_protocol)
        {
          case SMIB_COMMUNICATION_TYPE.PULSES:
            {
              _sb.AppendLine("SELECT   PMT_PCD_IO_NUMBER "); // 0
              _sb.AppendLine("       , PMT_PCD_IO_TYPE ");// 1
              _sb.AppendLine("       , PMT_EGM_NUMBER "); // 2
              _sb.AppendLine("       , PMT_EGM_NUMBER_MULTIPLIER "); // 3
              _sb.AppendLine("       , PMT_OUPUT_TIME_PULSE_DOWN "); // 4
              _sb.AppendLine("       , PMT_OUPUT_TIME_PULSE_UP "); // 5
              _sb.AppendLine("  FROM   PCD_METERS_TRANSLATION ");
              _sb.AppendLine(" WHERE   PMT_CONFIGURATION_ID = (SELECT TE_SMIB2EGM_CONF_ID FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId) ");

              using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(),_sql_trx.Connection,_sql_trx))
              {
                _sql_cmd.Parameters.Add("@pTerminalId", System.Data.SqlDbType.Int).Value = TerminalId;

                using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
                {
                  _idx_input = 0;
                  _idx_output = 0;

                  while (_reader.Read())
                  {
                    _port_type = _reader.GetInt32(1); // PMT_PCD_IO_TYPE

                    if ((_port_type & 0xF) == (Int32) SmibProtocols.PCDProtocolConfiguration.IoType.INPUT)
                    {
                      _response.SmibConfig.PcdParams.InputPort(_idx_input).PcdIoNumber = _reader.GetInt32(0); // PMT_PCD_IO_NUMBER
                      _response.SmibConfig.PcdParams.InputPort(_idx_input).CodeNumber = _reader.GetInt64(2); // PMT_EGM_NUMBER

                      if (_response.SmibConfig.PcdParams.InputPort(_idx_input).CodeNumber > SmibProtocols.PCDProtocolConfiguration.PCD_DOOR_MASK)
                      {
                        _response.SmibConfig.PcdParams.InputPort(_idx_input).PcdIoType = SmibProtocols.PCDProtocolConfiguration.PortType.DOOR;
                        _response.SmibConfig.PcdParams.InputPort(_idx_input).PulseEdgeType = (SmibProtocols.PCDProtocolConfiguration.EdgeType)(_port_type & SmibProtocols.PCDProtocolConfiguration.PCD_PULSES_TYPE_MASK);                        

                      }
                      else
                      {
                        _response.SmibConfig.PcdParams.InputPort(_idx_input).PcdIoType = SmibProtocols.PCDProtocolConfiguration.PortType.METER;
                        _response.SmibConfig.PcdParams.InputPort(_idx_input).CodeNumberMultiplier = _reader.GetDecimal(3); // PMT_EGM_NUMBER_MULTIPLIER
                      }
                      
                      _idx_input++;
                    }
                    else if ( (_port_type & 0xF) == (Int32) SmibProtocols.PCDProtocolConfiguration.IoType.OUTPUT)
                    {                      
                      _response.SmibConfig.PcdParams.OutputPort(_idx_output).PcdIoNumber = _reader.GetInt32(0); // PMT_PCD_IO_NUMBER
                      _response.SmibConfig.PcdParams.OutputPort(_idx_output).CodeNumber = _reader.GetInt64(2); // PMT_EGM_NUMBER
                      _response.SmibConfig.PcdParams.OutputPort(_idx_output).PulseEdgeType = (SmibProtocols.PCDProtocolConfiguration.EdgeType)(_port_type & SmibProtocols.PCDProtocolConfiguration.PCD_PULSES_TYPE_MASK);

                      if (_response.SmibConfig.PcdParams.OutputPort(_idx_output).CodeNumber == (Int32)SmibProtocols.PCDProtocolConfiguration.OutputCodes.SHUTDOWN)
                      {
                        _response.SmibConfig.PcdParams.OutputPort(_idx_output).PcdIoType = SmibProtocols.PCDProtocolConfiguration.PortType.SHUTDOWN;
                      }
                      else if (_response.SmibConfig.PcdParams.OutputPort(_idx_output).CodeNumber == (Int32)SmibProtocols.PCDProtocolConfiguration.OutputCodes.CASH_IN)
                      {
                        _response.SmibConfig.PcdParams.OutputPort(_idx_output).PcdIoType = SmibProtocols.PCDProtocolConfiguration.PortType.CASH_IN;
                        _response.SmibConfig.PcdParams.OutputPort(_idx_output).CodeNumberMultiplier = _reader.GetDecimal(3); // PMT_EGM_NUMBER_MULTIPLIER
                        _response.SmibConfig.PcdParams.OutputPort(_idx_output).TimePulseDownMs = (UInt32) _reader.GetInt32(4); // PMT_OUPUT_TIME_PULSE_DOWN
                        _response.SmibConfig.PcdParams.OutputPort(_idx_output).TimePulseUpMs = (UInt32) _reader.GetInt32(5); // PMT_OUPUT_TIME_PULSE_UP
                      }

                      _idx_output++;
                    }
                  } // while
                }               
              }              
            }
            break;
          case SMIB_COMMUNICATION_TYPE.WCP:
          case SMIB_COMMUNICATION_TYPE.SAS:
          case SMIB_COMMUNICATION_TYPE.NONE:
          default:
            Response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_PROTOCOL_VERSION;
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_ERROR;
      }
    }
  
  }
}
