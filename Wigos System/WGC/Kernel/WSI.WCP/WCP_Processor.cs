//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : WCP_Procesor.cs
// 
//   DESCRIPTION : 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007  AJQ    Initial version
// ...
// 17-MAR-2014  JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 25-MAR-2014  JCOR   Added call to DB_UpdateHandPayCandidatePlaySession.
// 02-APR-2014  JCOR   Added case WCP_MsgVersionSignature.
// 04-APR-2014  AMF    Added new information from Long Poll 1B
// 23-MAY-2014  JCOR   
// 02-JUN-2014  RRR    Fixed Bug WIG-959: PIN required in anonymous accounts when general param disables it
// 11-JUL-2014  AMF    Regionalization
// 04-AUG-2014  ACC    Added TitoCloseSessionWhenRemoveCard parameter.
// 30-SEP-2014  LEM    Fixed Bug WIG-1246: Allowed cash in without cage activated
// 03-DEC-2014  JMV    Fixed Bug WIG-1798 
// 09-DEC-2014  JMM    Added by recharges promotions to the promotion list sent to the WKT
// 11-DIC-2014  JML    Add configurable lines messages for anonymous account and card blocked from ELP
// 11-DIC-2014  JML & DDM    Added configurable lines messages for Welcome messages
// 18-DIC-2014  JML    Fixed Bug WIG-1866: Message for anonymous accounts only for not blocked accounts
// 27-JAN-2015  JMV    Added DynamicFormatDecimals
// 05-FEB-2015  XCD    TITO: Added functionality to transfer NR credit from player account to terminal (using LCD Touch)
// 30-MAR-2015  DHA    Set display name by general param mask for terminals
// 28-APR-2015  YNM    Fixed Bug WIG-2252: System allows recharged with closed session MB
// 20-JUL-2015  MPO    WIG-2607: Wrong decimal configuration for SAS Host
// 29-JUN-2015  FJC    Product Backlog Item 282.
// 07-SEP-2015  MPO    TFS ITEM 2194: SAS16: Estad�sticas multi-denominaci�n: WCP
// 06-OCT-2015  RCI    Fixed Bug TFS-5074: Wrong decimal configuration for SAS Host
// 06-OCT-2015  FJC    Product Backlog 4704
// 17-NOV-2015  ETP    Product Backlog Item 5835: Added transfer amounts.
// 24-NOV-2015  MPO    Fixed Bug TFS-6972: Error when receive MultiDenom messages
// 19-JAN-2016  JRC    Product Backlog Item 7909: Multiple buckets. Waking up the thread
// 20-JAN-2016  ACC    Product Backlog Item 8580: iMB: Rechazar recargas retrasadas
// 19-FEB-2016  ETP    Product Backlog Item 9752: Dual Currency: Configurar montos transferencia cr�dito NR
// 19-FEB-2016  ETP    Product Backlog Item 9748: Multiple buckets: Canje de buckets RE
// 16-MAR-2016  FJC    PBI 9105: BonoPlay: LCD: Cambios varios
// 29-MAR-2016  DHA    Bug 10986: error on ticket offline (handpay)
// 04-APR-2016  FJC    BUG 7585: No se insertan Movimientos de cuenta al reservar cr�ditos desde el LCD
// 29-SEP-2016  JMM    10660:SAS HOST: Control del aceptador de billetes (MICO2)
// 22-NOV-2016  JMM    PBI 19744:Pago autom�tico de handpays
// 23-JAN-2017  FJC    PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
// 13-MAR-2016  ATB    PBI 25262: Exped - Payment authorisation
// 21-MAR-2017  JMM    Fixed Bug 25819: MICO2: No se puede iniciar sesi�n mediante tarjeta.
// 21-MAR-2017  FJC    Fixed Bug 25947:Sorteo de M�quina. Error al ejecutar sorteo con transferencia a otra cuenta.
// 29-MAR-2017  FAV    PBI 25956:Third TAX - Cash desk draw and UNR TAX Configuration
// 31-MAR-2017  FJC    PBI 26015:ZitroTrack
// 06-APR-2017  FJC    PBI 26615:Sorteo de m�quina - Protecci�n para cerrar sesiones abiertas
// 30-MAY-2017  FJC    PBI 27761:Sorteo de m�quina. Modificiones en la visualizaci�n de las pantallas (LCD y InTouch)). 
// 15-JUN-2017  FAV    Bug 28169:WCP - Exception in Log about EvOpGetDescription
// 13-JUL-2017  ETP    Bug 28678: System bypasses controls to recharge on anonymous accounts when using MB's
// 25-JUL-2017  DHA    Bug 28929:WIGOS-3103 Some witholding documents are not being generated
// 03-AUG-2017  ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI (WIGOS-4000)
// 02-AUG-2017  JMM    PBI 28983: EGM Reserve - Phase 1 - Reserve on EGM by Carded Player
// 02-AUG-2017  JMM    PBI 28983:EGM Reserve - Phase 1 - Reserve on EGM by Carded Player
// 22-AUG-2017  FJC    Fixed Defect: WIGOS-4649 Terminal Draw: The Draw does not appear in the terminal and continue with start card session
// 29-AUG-2017  FJC    WIGOS-4771 Terminal Draw. Include GameId for each recharge
// 25-AUG-2017  ATB    PBI 28710: EGM Reserve � Settings (WIGOS-4697)
// 29-NOV-2017  FJC    WIGOS-3592 MobiBank: InTouch - Recharge request
// 19-JUN-2018  FJC    Bug: WIGOS-13044 Mobile Bank - To make a recharge, CARD UNREGISTERED is accepted and it continue the normal process
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using System.Security.Cryptography;
using System.Globalization;
using WSI.Common.TITO;
using WSI.WCP.BatchUpdate;
using WSI.Common.BatchUpdate;
using WSI.Common.TerminalDraw;
using WSI.Common.ExternalPaymentAndSaleValidation;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.WCP
{

  public class WCP_XmlSink : IXmlSink
  {

    public virtual void Process(Object Server, Object Client, String XmlMessage)
    {
      WcpInputMsgProcessTask msg_input_task;

      msg_input_task = new WcpInputMsgProcessTask(Server, Client, XmlMessage);

      if (msg_input_task.HasToBeExecuted)
      {
        WCP_WorkerPool.Enqueue(msg_input_task);
      }
    }
  }



  public interface IWcpTaskStatus
  {
    void SetResponseCode(WCP_ResponseCodes ResponseCode);
    void SetResponseCode(WCP_ResponseCodes ResponseCode, String ResponseCodeText);
  }

  public class WcpInputMsgProcessTask : Task, IWcpTaskStatus
  {
    SecureTcpServer m_wcp_server;
    SecureTcpClient m_wcp_client;

    int m_tick_arrived;
    int m_tick_dequeued;
    int m_tick_processed;

    Boolean m_ready_to_execute = false;

    //---------------------------
    protected Int64 m_client_id = 0;    // Connection ID
    protected int m_terminal_id = 0;    // Terminal ID
    protected Int64 m_wcp_session_id = 0;    // WCP Session ID
    protected Int64 m_card_session_id = 0;    // Card Session ID

    protected WCP_Message m_wcp_request = null;
    protected WCP_Message m_wcp_response = null;
    //-----------------------------------------------------------------

    #region Properties

    public Boolean HasToBeExecuted
    {
      get
      {
        if (!m_ready_to_execute) return false;
        if (m_wcp_client == null) return false;
        if (!m_wcp_client.IsConnected) return false;

        return true;
      }
    }
    protected String XmlResponse
    {
      get
      {
        return m_wcp_response.ToXml();
      }
    }
    protected internal String XmlRequest
    {
      get
      {
        return m_wcp_request.ToXml();
      }
    }

    public SecureTcpClient WcpClient
    {
      get
      {
        return m_wcp_client;
      }
    }

    public SecureTcpServer WcpServer
    {
      get
      {
        return m_wcp_server;
      }
    }

    protected internal WCP_Message Request
    {
      get
      {
        return m_wcp_request;
      }
    }

    protected internal WCP_Message Response
    {
      get
      {
        return m_wcp_response;
      }
    }

    public Int64 ElapsedTicks
    {
      get
      {
        return Misc.GetElapsedTicks(m_tick_arrived);
      }
    }

    #endregion // Properties


    public WcpInputMsgProcessTask(Object Server, Object Client, String XmlMessage)
    {
      String _msg_type;
      Int64 _session_id;

      m_tick_arrived = Environment.TickCount;
      m_tick_dequeued = m_tick_arrived;
      m_tick_processed = m_tick_arrived;


      m_wcp_server = (SecureTcpServer)Server;
      m_wcp_client = (SecureTcpClient)Client;

      m_terminal_id = m_wcp_client.InternalId;

      m_wcp_request = null;
      m_wcp_response = null;

      try
      {
        m_wcp_request = WCP_Message.CreateMessage(XmlMessage);
      }
      catch (Exception _ex)
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine("XML Message Exception");
        _sb.AppendLine("- Exception: " + _ex.ToString());
        _sb.AppendLine("- Message: " + XmlMessage);
        Log.Message(_sb.ToString());

        m_wcp_request = null;

        return;
      }

      try
      {
        _msg_type = m_wcp_request.MsgHeader.MsgType.ToString();
        if (_msg_type.IndexOf("Reply") > 0)
        {
          m_wcp_response = null;

          m_ready_to_execute = true;

          return;
        }

        if (_msg_type.StartsWith("WCP_TITO_Msg"))
        {
          if (!Common.TITO.Utils.IsTitoMode())
          {
            if (!_msg_type.StartsWith("WCP_TITO_MsgEgmParameters"))
            {
              // Ignore message
              return;
            }
          }
        }

        m_wcp_response = WCP_Message.CreateMessage(m_wcp_request.MsgHeader.MsgType + 1);
        m_wcp_response.MsgHeader.PrepareReply(m_wcp_request.MsgHeader);

        _session_id = (m_wcp_request.MsgHeader.TerminalId.Length > 0) ? m_wcp_request.MsgHeader.TerminalSessionId : m_wcp_request.MsgHeader.ServerSessionId;
        if (m_wcp_client.InternalId > 0 && _session_id > 0)
        {
          if (!TerminalSession.SessionMessageReceived(m_wcp_client.InternalId, _session_id, m_wcp_request.MsgHeader.SequenceId, m_wcp_request.TransactionId))
          {
            m_wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TERMINAL_SESSION_NOT_VALID;
            m_wcp_client.Send(m_wcp_response.ToXml());

            return;
          }
        }

        m_ready_to_execute = true;
      }
      catch (Exception _ex)
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine("XML Message Exception (Reply)");
        _sb.AppendLine("- Exception: " + _ex.ToString());
        _sb.AppendLine("- Message: " + XmlMessage);
        Log.Message(_sb.ToString());

        m_wcp_request = null;
        m_wcp_response = null;

        return;
      }

    } // WcpInputMsgProcessTask

    public override void Execute()
    {
      if (!HasToBeExecuted)
      {
        return;
      }

      if (!AddToPending())
      {
        return;
      }

      try
      {
        WCP_Processor.ProcessWcpInputMsg(this);
      }
      catch
      {
      }
      finally
      {
        long _tq;
        long _tx;
        long _tt;

        m_tick_processed = Environment.TickCount;

        _tq = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
        _tx = Misc.GetElapsedTicks(m_tick_dequeued, m_tick_processed);
        _tt = Misc.GetElapsedTicks(m_tick_arrived, m_tick_processed);

        if (_tq > 10000
            || _tx > 10000
            || _tt > 20000)
        {
          Log.Warning("Message times: Queued/Process/Total = " + _tq.ToString() + "/" + _tx.ToString() + "/" + _tt.ToString() + " ----> " + this.m_wcp_request.MsgHeader.MsgType.ToString());
        }
      }
    } // Execute

    public override void Execute(Task[] Tasks)
    {
      ArrayList _tasks0;
      ArrayList _tasks1;
      WcpInputMsgProcessTask _wcp_task;
      WCP_MsgTypes _wcp_msg_type;
      SqlConnection _sql_conn;

      if (Tasks == null)
      {
        return;
      }
      if (Tasks.Length <= 0)
      {
        return;
      }

      _tasks0 = new ArrayList(Tasks);
      _tasks1 = new ArrayList(Tasks.Length);

      while (_tasks0.Count > 0)
      {
        // Get MsgType of the 1st task
        _wcp_task = (WcpInputMsgProcessTask)_tasks0[0];
        _wcp_msg_type = _wcp_task.Request.MsgHeader.MsgType;

        // Group all tasks of the same MsgType
        foreach (WcpInputMsgProcessTask _task in _tasks0)
        {
          if (_task.Request.MsgHeader.MsgType == _wcp_msg_type)
          {
            _tasks1.Add(_task);
          }
        } // foreach

        // Remove all the tasks from _task0
        if (_tasks1.Count == _tasks0.Count)
        {
          // _tasks1 contains all the tasks from _task0
          _tasks0.Clear();
        }
        else
        {
          foreach (WcpInputMsgProcessTask _task in _tasks1)
          {
            _tasks0.Remove(_task);
          } // foreach        
        }

        try
        {
          // Execute
          _sql_conn = WGDB.Connection();
          try
          {
            WcpInputMsgProcessTask.ExecuteGroup(_wcp_msg_type, _tasks1, _sql_conn);
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
          if (_sql_conn != null)
          {
            try { _sql_conn.Close(); }
            catch { ; }
            _sql_conn.Dispose();
            _sql_conn = null;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // while (_tasks0.Count > 0)
    } // Execute 

    private static void ExecuteGroup(WCP_MsgTypes MsgType, System.Collections.ArrayList Tasks, SqlConnection SqlConn)
    {
      ArrayList _pending;
      int _tick_arrived;
      int _tick_dequeued;
      int _tick_processed;

      _tick_dequeued = Environment.TickCount;
      _tick_processed = _tick_dequeued;
      _tick_arrived = _tick_dequeued;

      _pending = new ArrayList(Tasks.Count);

      try
      {
        // Check Timeout and/or Disconnected tasks
        foreach (WcpInputMsgProcessTask _task in Tasks)
        {
          if (!_task.HasToBeExecuted)
          {
            continue;
          }

          _tick_arrived = Math.Min(_task.m_tick_arrived, _tick_arrived);

          if (_task.AddToPending())
          {
            _pending.Add(_task);
          }
          else
          {
            // Timeout/Disconnected: do nothing
          }
        }

        switch (MsgType)
        {
          case WCP_MsgTypes.WCP_MsgReportPlay:
            {
              ReportPlayData _play_data;
              ReportPlayDataList _play_data_list;

              _play_data_list = new ReportPlayDataList();

              foreach (WcpInputMsgProcessTask _wcp_task in _pending)
              {
                _play_data = new ReportPlayData(_wcp_task.XmlRequest, _wcp_task.Request, _wcp_task.WcpClient, _wcp_task.WcpServer);
                _play_data_list.InsertPlay(_play_data);
              }

              WCP_BU_ReportPlay.Process(_play_data_list, SqlConn);
            }
            break;

          case WCP_MsgTypes.WCP_MsgReportEventList:
            // ACC & RCI 16-AUG-2010: TODO & TEST: Not now!
            ////////_pending_ok = WcpInputMsgProcessTask.SessionMessageReceived(_pending);
            WCP_BU_ReportEventList.Process(_pending, SqlConn);
            break;

          case WCP_MsgTypes.WCP_MsgReportGameMeters:
            WCP_BU_GameMeters.Process(_pending, SqlConn);
            break;

          case WCP_MsgTypes.WCP_MsgUpdateCardSession:
            // ACC & RCI 16-AUG-2010: TODO & TEST: Not now!
            ////////_pending_ok = WcpInputMsgProcessTask.SessionMessageReceived(_pending);
            WCP_BU_UpdateCardSession.Process(_pending, SqlConn);
            break;

          case WCP_MsgTypes.WCP_MsgPlaySessionMeters:
            WCP_BU_ReportPlaySessionMeters.Instance.Process(_pending, SqlConn);
            break;

          case WCP_MsgTypes.WCP_MsgReportMachineMeters:
            WCP_BU_MachineMeters.Process(_pending, SqlConn);
            break;

          case WCP_MsgTypes.WCP_MsgSasMeters:
            WCP_BU_SasMeters.Instance.Process(_pending, SqlConn);
            break;

          case WCP_MsgTypes.WCP_MsgMachineMultiDenomInfo:
            WCP_BU_SasMeters.Instance.Process(_pending, SqlConn);
            break;

          case WCP_MsgTypes.WCP_MsgCashSessionPendingClosing:
          default:
            {
              foreach (WcpInputMsgProcessTask _wcp_task in _pending)
              {
                _wcp_task.Execute();
              }
            }
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Message("EXCEPTION processiong: " + MsgType.ToString() + ", Details: " + _ex.Message);
      }
      finally
      {
        long _tq;
        long _tx;
        long _tt;

        _tick_processed = Environment.TickCount;

        _tq = Misc.GetElapsedTicks(_tick_arrived, _tick_dequeued);
        _tx = Misc.GetElapsedTicks(_tick_dequeued, _tick_processed);
        _tt = Misc.GetElapsedTicks(_tick_arrived, _tick_processed);

        if (_tx >= 10000
            || _tt >= 20000)
        {
          Log.Warning("MessageType " + MsgType.ToString() + " Count:" + _pending.Count + " Time Queued/Process/Total = " + _tq.ToString() + "/" + _tx.ToString() + "/" + _tt.ToString());
        }
      }
    } // ExecuteGroup

    #region IXXX Members

    public void SetResponseCode(WCP_ResponseCodes ResponseCode)
    {
      SetResponseCode(ResponseCode, "");
    }

    public void SetResponseCode(WCP_ResponseCodes ResponseCode, String ResponseCodeText)
    {
      if (m_wcp_response == null)
      {
        throw new Exception("WcpResponse should be created first!");
      }
      this.m_wcp_response.MsgHeader.ResponseCode = ResponseCode;
      this.m_wcp_response.MsgHeader.ResponseCodeText = ResponseCodeText;
    }

    public WCP_ResponseCodes GetResponseCode()
    {
      return this.m_wcp_response.MsgHeader.ResponseCode;
    }

    public String GetResponseCodeText()
    {
      return this.m_wcp_response.MsgHeader.ResponseCodeText;
    }

    #endregion

    public void SendResponse()
    {
      String _xml;

      if (this.m_wcp_response != null)
      {
        _xml = this.m_wcp_response.ToXml();
        this.WcpClient.Send(_xml);
        NetLog.Add(NetEvent.MessageSent, this.WcpClient.Identity, _xml);
      }
    } // SendResponse

    public Boolean AddToPending()
    {
      String _msg_type;
      long _interval;
      SecureTcpClient _tcp_client;
      SecureTcpServer _tcp_server;

      if (m_wcp_request == null)
      {
        return false;
      }

      _msg_type = m_wcp_request.MsgHeader.MsgType.ToString();
      if (_msg_type.IndexOf("Reply") > 0)
      {
        // It is a reply  (Command, Logger, ...)
        // Never timeout
        return true;
      }

      m_tick_dequeued = Environment.TickCount;
      m_tick_processed = m_tick_dequeued;
      _interval = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
      if (_interval >= 10000)
      {
        // Ignore the task     
        Log.Warning("Message Ignored: timeout. Queued: " + _interval.ToString() + " ms. " + this.Request.MsgHeader.MsgType.ToString());

        return false;
      }

      _tcp_client = (SecureTcpClient)m_wcp_client;
      _tcp_server = (SecureTcpServer)m_wcp_server;

      if (!_tcp_server.IsConnected(_tcp_client))
      {
        // Ignore the task     
        Log.Warning("Message Ignored: disconnected. Queued: " + _interval.ToString() + " ms. " + this.Request.MsgHeader.MsgType.ToString());

        return false;
      }

      return true;
    } // AddToPending

  } // WcpInputMsgProcessTask


  public enum WCP_Pool
  {
    Default = 0,  //  1 Worker
    Enroll = 1,  //  1 Worker
    PlaySession = 2,  // 20 Workers
    Report = 3,  //  5 Workers

    MsgReportEventList = 1001, // Report Event List 
    MsgReadonly = 1002, // Get Parameters
    MsgReportPlaySas = 1003, // Report Play
    MsgReportPlayWin = 1004, // Report Play
    MsgReportGameMeters = 1005, // Game Meters
    MsgUpdateCardSession = 1006, // Update Card Session
    MsgReportMachineMeters = 1007, // Machine Meters
    MsgReportSasMeters = 1008, // Machine SAS Meters
    MsgReportPlaySessionMeters = 1009,  // machine meters + tito play session meters
    MsgReportMultiDenomInfo = 1010,

    MsgTito_ValidationNumber = 2000,
    MsgTito_IsValid = 2001,
    MsgTito_Cancel = 2002,
    MsgTito_CreateTicket = 2003,
  };

  public static class WCP_WorkerPool
  {
    private static WorkerPool m_pool_default = null;
    private static WorkerPool m_pool_enroll = null;
    private static WorkerPool m_pool_play_session = null;
    private static WorkerPool m_pool_report = null;
    private static WorkerPool m_pool_readonly = null;

    private static WorkerPool m_pool_tito_validation_number = null;
    private static WorkerPool m_pool_tito_create_ticket = null;
    private static WorkerPool m_pool_tito_is_valid = null;
    private static WorkerPool m_pool_tito_cancel = null;

    // Per message pools
    private static WorkerPool m_pool_msg_report_event_list = null;
    private static WorkerPool m_pool_msg_report_play_sas = null;
    private static WorkerPool m_pool_msg_report_play_win = null;
    private static WorkerPool m_pool_msg_report_game_meters = null;
    private static WorkerPool m_pool_msg_update_card_session = null;
    private static WorkerPool m_pool_msg_report_machine_meters = null;
    private static WorkerPool m_pool_msg_report_multidenominfo = null;

    private static WorkerPool m_pool_msg_report_sas_meters = null;
    private static WorkerPool m_pool_msg_report_play_session_meters = null;

    private static WCP_Pool Pool(WcpInputMsgProcessTask Task)
    {
      WCP_MsgTypes _msg_type;

      _msg_type = WCP_MsgTypes.WCP_MsgUnknown;
      if (Task.Request != null)
      {
        _msg_type = Task.Request.MsgHeader.MsgType;
      }

      switch (_msg_type)
      {
        case WCP_MsgTypes.WCP_MsgAddCredit:
        case WCP_MsgTypes.WCP_MsgEndSession:
        case WCP_MsgTypes.WCP_MsgGetCardType:
        case WCP_MsgTypes.WCP_MsgLockCard:
        case WCP_MsgTypes.WCP_MsgMobileBankCardCheck:
        case WCP_MsgTypes.WCP_MsgMobileBankCardTransfer:
        case WCP_MsgTypes.WCP_MsgCardInformation:
        case WCP_MsgTypes.WCP_MsgMBInformation:
        case WCP_MsgTypes.WCP_MsgStartCardSession:
        case WCP_MsgTypes.WCP_MsgCancelStartSession:
        case WCP_MsgTypes.WCP_MsgCheckTicket:
          return WCP_Pool.PlaySession;

        case WCP_MsgTypes.WCP_MsgEnrollServer:
        case WCP_MsgTypes.WCP_MsgEnrollTerminal:
        case WCP_MsgTypes.WCP_MsgUnenrollServer:
        case WCP_MsgTypes.WCP_MsgUnenrollTerminal:
          return WCP_Pool.Enroll;

        // AJQ 06-JUL-2010, BatchUpdate for ReportEventList
        case WCP_MsgTypes.WCP_MsgGetParameters:
        case WCP_MsgTypes.WCP_MsgKeepAlive:
        case WCP_MsgTypes.WCP_MsgPlayConditions:
          return WCP_Pool.MsgReadonly;

        case WCP_MsgTypes.WCP_MsgReportEvent:
        case WCP_MsgTypes.WCP_MsgReportMeters:
          return WCP_Pool.Report;

        // AJQ 06-JUL-2010, BatchUpdate for ReportEventList
        case WCP_MsgTypes.WCP_MsgReportEventList:
          return WCP_Pool.MsgReportEventList;

        // RCI 12-JUL-2010, BatchUpdate for ReportGameMeters
        case WCP_MsgTypes.WCP_MsgReportGameMeters:
          return WCP_Pool.MsgReportGameMeters;

        // RCI 13-AUG-2010, BatchUpdate for UpdateCardSession
        case WCP_MsgTypes.WCP_MsgUpdateCardSession:
          return WCP_Pool.MsgUpdateCardSession;

        case WCP_MsgTypes.WCP_MsgPlaySessionMeters:
          return WCP_Pool.MsgReportPlaySessionMeters;

        case WCP_MsgTypes.WCP_MsgReportPlay:
          if (Task.WcpClient.TerminalType == TerminalTypes.SAS_HOST) // SAS Host
          {
            return WCP_Pool.MsgReportPlaySas; //  WCP_Pool.Report;
          }
          return WCP_Pool.MsgReportPlayWin; //  WCP_Pool.PlaySession;

        case WCP_MsgTypes.WCP_MsgReportMachineMeters:
          return WCP_Pool.MsgReportMachineMeters;

        case WCP_MsgTypes.WCP_MsgMachineMultiDenomInfo:
          return WCP_Pool.MsgReportMultiDenomInfo;

        case WCP_MsgTypes.WCP_MsgSasMeters:
        case WCP_MsgTypes.WCP_MsgCashSessionPendingClosing:
          return WCP_Pool.MsgReportSasMeters;

        case WCP_MsgTypes.WCP_TITO_MsgIsValidTicket:
          return WCP_Pool.MsgTito_IsValid;

        case WCP_MsgTypes.WCP_TITO_MsgCancelTicket:
          return WCP_Pool.MsgTito_Cancel;

        case WCP_MsgTypes.WCP_TITO_MsgValidationNumber:
          return WCP_Pool.MsgTito_ValidationNumber;

        case WCP_MsgTypes.WCP_TITO_MsgCreateTicket:
          return WCP_Pool.MsgTito_CreateTicket;

        case WCP_MsgTypes.WCP_MsgUnknown:
        default:
          return WCP_Pool.Default;
      } // switch
    }

    public static void Init()
    {
      int _pool_workers;

      _pool_workers = 1;
      if (Environment.GetEnvironmentVariable("WCP_POOL_TITO_VALIDATION_NUMBER") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_TITO_VALIDATION_NUMBER"));
      }
      m_pool_tito_validation_number = new WorkerPool("WCP Pool TITO Validation Number", _pool_workers);

      _pool_workers = 1;
      if (Environment.GetEnvironmentVariable("WCP_POOL_TITO_IS_VALID") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_TITO_IS_VALID"));
      }
      m_pool_tito_is_valid = new WorkerPool("WCP Pool TITO IsValid", _pool_workers);

      _pool_workers = 1;
      if (Environment.GetEnvironmentVariable("WCP_POOL_TITO_CANCEL") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_TITO_CANCEL"));
      }
      m_pool_tito_cancel = new WorkerPool("WCP Pool TITO Cancel", _pool_workers);

      _pool_workers = 1;
      if (Environment.GetEnvironmentVariable("WCP_POOL_TITO_CREATE_TICKET") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_TITO_CREATE_TICKET"));
      }
      m_pool_tito_create_ticket = new WorkerPool("WCP Pool TITO CreateTicket", _pool_workers);

      _pool_workers = 20;
      if (Environment.GetEnvironmentVariable("WCP_POOL_DEFAULT") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_DEFAULT"));
      }
      m_pool_default = new WorkerPool("WCP Pool Default", _pool_workers);

      _pool_workers = 20;
      if (Environment.GetEnvironmentVariable("WCP_POOL_ENROLL") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_ENROLL"));
      }
      m_pool_enroll = new WorkerPool("WCP Pool Enroll", _pool_workers);

      _pool_workers = 20;
      if (Environment.GetEnvironmentVariable("WCP_POOL_PLAY_SESSION") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_PLAY_SESSION"));
      }
      m_pool_play_session = new WorkerPool("WCP Pool PlaySession", _pool_workers);

      _pool_workers = 20;
      if (Environment.GetEnvironmentVariable("WCP_POOL_REPORT") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_REPORT"));
      }
      m_pool_report = new WorkerPool("WCP Pool Report", _pool_workers);

      m_pool_msg_report_event_list = new WorkerPool("WCP MsgReportEventList", 1, true);

      _pool_workers = 10;
      if (Environment.GetEnvironmentVariable("WCP_POOL_REPORTPLAY_SAS") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_REPORTPLAY_SAS"));
      }
      m_pool_msg_report_play_sas = new WorkerPool("WCP MsgReportPlay-SAS", _pool_workers, true);

      _pool_workers = 10;
      if (Environment.GetEnvironmentVariable("WCP_POOL_REPORTPLAY_WIN") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_REPORTPLAY_WIN"));
      }
      m_pool_msg_report_play_win = new WorkerPool("WCP MsgReportPlay-WIN", _pool_workers, true);

      m_pool_msg_report_game_meters = new WorkerPool("WCP MsgReportGameMeters", 1, true);
      m_pool_msg_update_card_session = new WorkerPool("WCP MsgUpdateCardSession", 1, true);

      m_pool_msg_report_machine_meters = new WorkerPool("WCP MsgReportMachineMeters", 1, true);
      m_pool_msg_report_multidenominfo = new WorkerPool("WCP MsgMachineMultiDenomInfo", 1, true);
      m_pool_msg_report_sas_meters = new WorkerPool("WCP MsgReportSasMeters", 1, true);
      m_pool_msg_report_play_session_meters = new WorkerPool("WCP MsgPlaySessionMeters", 1, true);

      _pool_workers = 5;
      if (Environment.GetEnvironmentVariable("WCP_POOL_READONLY") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WCP_POOL_READONLY"));
      }
      m_pool_readonly = new WorkerPool("WCP MsgReadonly", _pool_workers, true);
    }

    public static void Enqueue(WcpInputMsgProcessTask Task)
    {
      WCP_Pool _pool;

      _pool = Pool(Task);

      switch (_pool)
      {
        case WCP_Pool.Default:
          m_pool_default.EnqueueTask(Task);
          break;

        case WCP_Pool.Enroll:
          m_pool_enroll.EnqueueTask(Task);
          break;

        case WCP_Pool.PlaySession:
          m_pool_play_session.EnqueueTask(Task);
          break;

        case WCP_Pool.Report:
          m_pool_report.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgReadonly:
          m_pool_readonly.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgReportEventList:
          m_pool_msg_report_event_list.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgReportPlaySas:
          m_pool_msg_report_play_sas.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgReportPlayWin:
          m_pool_msg_report_play_win.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgReportGameMeters:
          m_pool_msg_report_game_meters.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgUpdateCardSession:
          m_pool_msg_update_card_session.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgReportMachineMeters:
          m_pool_msg_report_machine_meters.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgReportMultiDenomInfo:
          m_pool_msg_report_multidenominfo.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgReportSasMeters:
          m_pool_msg_report_sas_meters.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgReportPlaySessionMeters:
          m_pool_msg_report_play_session_meters.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgTito_CreateTicket:
          m_pool_tito_create_ticket.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgTito_IsValid:
          m_pool_tito_is_valid.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgTito_Cancel:
          m_pool_tito_cancel.EnqueueTask(Task);
          break;

        case WCP_Pool.MsgTito_ValidationNumber:
          m_pool_tito_validation_number.EnqueueTask(Task);
          break;

        default:
          Log.Error("Unknown WCP Pool: " + _pool.ToString());
          break;
      }
    }
  }

  public static partial class WCP_Processor
  {
    public static void ProcessWcpInputMsg(WcpInputMsgProcessTask Task)
    {
      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      SecureTcpClient _tcp_client;
      SecureTcpServer _tcp_server;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Boolean _error;
      Int64 _response_session_id;
      Int32 _response_terminal_id;
      String _msg_type;
      Boolean _is_tito_mode;
      //////Int64 _dummy_int64;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_commit;
      long _interval_sent;
      long _interval_all_process;

      _is_tito_mode = Common.TITO.Utils.IsTitoMode();

      _sql_conn = null;
      _sql_trx = null;
      _error = true;

      _tcp_client = Task.WcpClient;
      _tcp_server = Task.WcpServer;
      _wcp_request = Task.Request;
      _wcp_response = Task.Response;


      try
      {
        _response_terminal_id = _tcp_client.InternalId;
        _response_session_id = (_wcp_request.MsgHeader.TerminalId.Length > 0) ? _wcp_request.MsgHeader.TerminalSessionId : _wcp_request.MsgHeader.ServerSessionId;

        _msg_type = _wcp_request.MsgHeader.MsgType.ToString();
        if (_msg_type.IndexOf("Reply") > 0)
        {
          WCP_ReplyMessages.Process(_tcp_client.InternalId, _wcp_request);

          return;
        }

        if (_response_session_id < 0)
        {
          if (_wcp_request.MsgHeader.TerminalId.Length > 0)
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_TERMINAL_SESSION_ID, "SessionId less than 0");
          }
          else
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_SERVER_SESSION_ID, "SessionId less than 0");
          }
        }

        // AJQ 18-MAY-2012, WKT Messages
        if (_msg_type.StartsWith("WCP_WKT_Msg"))
        {
          String _str_response;

          try
          {
            WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

            if (_tcp_client.IsConnected)
            {
              _str_response = _wcp_response.ToXml();
              NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, _str_response);
              _tcp_client.Send(_str_response);
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          return;
        }

        // JCA & XCD 25-JUL-2014, LCD Messages
        if (_msg_type.StartsWith("WCP_LCD_Msg"))
        {
          String _str_response;

          try
          {
            WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

            if (_tcp_client.IsConnected)
            {
              _str_response = _wcp_response.ToXml();
              NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, _str_response);
              _tcp_client.Send(_str_response);
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          return;
        }

        // JRM 27-SEP-2013, TITO Messages
        if (_msg_type.StartsWith("WCP_TITO_Msg"))
        {
          String _str_response;

          try
          {
            if (WCP_TITO.TITO_ProcessRequest(_tcp_client, _wcp_request, _wcp_response))
            {
              if (_tcp_client.IsConnected)
              {
                _str_response = _wcp_response.ToXml();
                NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, _str_response);
                _tcp_client.Send(_str_response);
              }
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          return;
        }

        // AJQ 24-JUL-2007, Do NOT log/store the KeepAlive messages
        // ACC 25-JUL-2007, Do NOT log/store the GetCardType messages
        switch (_wcp_request.MsgHeader.MsgType)
        {
          case WCP_MsgTypes.WCP_MsgKeepAlive:
            {
              _tcp_client.Send(_wcp_response.ToXml());
              _error = false;
            }
            return;


          //
          // AJQ 16-FEB-2009, Don't save "GetParameters" (wcp_messages/transactions)
          // XIT 21-JUN-2012, Added access to BD to get Currencies
          //
          case WCP_MsgTypes.WCP_MsgGetParameters:
            {
              WCP_MsgGetParametersReply _response;
              Boolean _terminal_loaded;
              String msg_response_str;
              String _provider_id;
              ENUM_AUTHENTICATION_STATUS _authentication_status;
              Int32 _machine_status;
              String _message_line1;
              String _message_line2;
              CurrencyExchange _national_currency;
              List<CurrencyExchange> _currencies;
              String _terminal_name;
              String _mask_name;
              Int64 _dummy_int64;
              Int32 technician_activity_timeout;

              // ACC 20-JAN-2016 Set Diff datetime (Server-Terminal)
              TimeSpan _ts;
              try
              {
                _ts = WGDB.Now - _wcp_request.MsgHeader.GetSystemTime;
                _tcp_client.DiffDatetimeMilliSeconds = (int)_ts.TotalMilliseconds;
              }
              catch { };

              _response = (WCP_MsgGetParametersReply)_wcp_response.MsgContent;
              _response.AssetNumber = (UInt32)_response_terminal_id;
              _response.TitoMode = _is_tito_mode;
              _terminal_name = String.Empty;
              _mask_name = GeneralParam.GetString("SasHost", "DisplayName.Format");

              if (!Common.BatchUpdate.TerminalSession.GetData(_response_terminal_id, _tcp_client.SessionId, out _provider_id, out _response.MachineName))
              {
                _response.MachineName = "";
              }
              _response.NoteAcceptorParameters = WCP_BusinessLogic.DB_GetCurrencies();

              // Read Sas Flags
              using (DB_TRX _db_trx = new DB_TRX())
              {
                WSI.Common.Terminal.TerminalInfo _terminal_info;

                _response.SasFlags = 0;

                if (_is_tito_mode)
                {
                  _terminal_loaded = WCP_PlaySessionLogic.Instance.LoadTerminalInfoWithParams(_response_terminal_id,
                                                                                              out _terminal_info,
                                                                                              _db_trx.SqlTransaction);
                }
                else
                {
                  _terminal_loaded = WSI.Common.Terminal.Trx_GetTerminalInfo(_response_terminal_id,
                                                                             out _terminal_info,
                                                                             _db_trx.SqlTransaction);
                }

                // DHA 30-MAR-2015: set display name by general param mask
                // DHA & JML & DDM: in future refactor previous functions:
                //  - LoadTerminalInfoWithParams(...)
                //  - Trx_GetTerminalInfo(...)
                // NOTE: should be implemented into TerminalSession.GetData(...)
                if (WSI.Common.Terminal.GetTerminalDisplayName(_response_terminal_id, _mask_name, out _terminal_name, _db_trx.SqlTransaction))
                {
                  _response.MachineName = _terminal_name;
                }

                if (_terminal_loaded)
                {
                  if (WSI.Common.Terminal.SASFlagActived(_terminal_info.SASFlags, SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL))
                  {
                    // Check if the reservation has expired.
                    if (TerminalReservation.CheckExpiration(_terminal_info.TerminalId, _db_trx.SqlTransaction))
                    {
                      using (DB_TRX _trx_reserve = new DB_TRX())
                      { 
                        // The reservation has expired.  Clean it
                        if (TerminalReservation.Remove(_terminal_info.TerminalId, _trx_reserve.SqlTransaction))
                        {
                          _trx_reserve.Commit();
                        }
                      }

                      _terminal_info.SASFlags &= (Int32)~SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL;
                    }                    
                  }

                  _response.SasFlags = (UInt32)_terminal_info.SASFlags;

                  if (GeneralParam.GetBoolean("Intrusion", "DisableAFT"))
                  {
                    _response.SasFlags |= (Int32)SAS_FLAGS.SAS_FLAG_DISABLED_AFT_ON_INTRUSION;
                  }

                  // Tito vars 
                  if (_is_tito_mode)
                  {
                    _response.SequenceId = _terminal_info.SequenceId;
                    _response.SystemId = _terminal_info.SystemId;
                    _response.AllowCashableTicketOut = _terminal_info.AllowCashableTicketOut;
                    _response.AllowPromotionalTicketOut = _terminal_info.AllowPromotionalTicketOut;
                    _response.AllowTicketIn = _terminal_info.AllowTicketIn;
                    _response.ExpirationTicketsCashable = _terminal_info.ExpirationTicketsCashable;
                    _response.ExpirationTicketsPromotional = _terminal_info.ExpirationTicketsPromotional;
                    _response.TitoTicketAddress_1 = _terminal_info.TitoTicketAddress_1;
                    _response.TitoTicketAddress_2 = _terminal_info.TitoTicketAddress_2;
                    _response.TitoTicketLocalization = _terminal_info.TitoTicketLocalization;
                    _response.TitoTicketTitleDebit = _terminal_info.TitoTicketTitleDebit;
                    _response.TitoTicketTitleRestricted = _terminal_info.TitoTicketTitleRestricted;
                    _response.MachineId = _terminal_info.MachineId;
                    _response.TicketsMaxCreationAmount = _terminal_info.TicketsMaxCreationAmount;
                    _response.BillsMetersRequired = WCP_PlaySessionLogic.Instance.LoadFlagsBillsMetersRequired(SAS_METERS_GROUPS.IDX_SMG_BILLS, _db_trx.SqlTransaction);
                    _response.HostId = _terminal_info.HostId;
                  }
                }

                // Add Wass Machine Lock field
                _response.WassMachineLocked = 0;
                if (SoftwareValidations.ReadAuthenticationMachineStatus(_response_terminal_id, out _authentication_status, out _machine_status, _db_trx.SqlTransaction))
                {
                  if (_machine_status != (Int32)TerminalStatusFlags.MACHINE_FLAGS.NON_BLOCKED && !TerminalStatusFlags.IsFlagActived(_machine_status, (Int32)TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_UNBLOCKED))
                  {
                    _response.WassMachineLocked = 1;
                  }
                }

                // XCD 22-APR-2015 Add protocol that terminal must connect
                if (WSI.Common.Terminal.Trx_GetTerminalSmibProtocol(_response_terminal_id, _tcp_client.TerminalType, _db_trx.SqlTransaction, out _response.Protocol, out _dummy_int64))
                {
                  switch (_response.Protocol)
                  {
                    case SMIB_COMMUNICATION_TYPE.PULSES:
                      _response.RequestProtocolParameters = true;
                      break;

                    case SMIB_COMMUNICATION_TYPE.SAS:
                    case SMIB_COMMUNICATION_TYPE.WCP:
                    case SMIB_COMMUNICATION_TYPE.NONE:
                    default:
                      // Not implemented
                      _response.RequestProtocolParameters = false;
                      break;
                  }
                }

                // ACC 27-APR-2015 Add change stacker mode (0: Machine, 1: StackerId)
                _response.ChangeStackerMode = 0; // Machine
                _response.CurrentStackerId = 0;
                if (GeneralParam.GetBoolean("WCP", "Terminal.UseStackerId"))
                {
                  Stacker _new_stacker;

                  _response.ChangeStackerMode = 1; // StackerId

                  _new_stacker = Stacker.DB_ReadStackerInsertedInTerminal(_tcp_client.InternalId, _db_trx.SqlTransaction);
                  if (_new_stacker != null)
                  {
                    _response.CurrentStackerId = _new_stacker.StackerId;
                  }
                }

              } // _db_trx

              // 11-DIC-2014 JML & DDM   Added LCDMessages
              LCDMessages.GetLcdMessage(_response_terminal_id, LCDMessages.ENUM_TYPE_MESSAGE.WITHOUT_CARD, out _message_line1, out _message_line2);
              if (String.IsNullOrEmpty(_message_line1) && String.IsNullOrEmpty(_message_line2))
              {
                _message_line1 = WSI.Common.Misc.ReadGeneralParams("WelcomeMessage", "Line1");
                _message_line2 = WSI.Common.Misc.ReadGeneralParams("WelcomeMessage", "Line2");
              }
              _response.WelcomeMessageLine1 = _message_line1;
              _response.WelcomeMessageLine2 = _message_line2;

              if (!Misc.IsFloorDualCurrencyEnabled())
              {
                _response.CurrencySymbol = WSI.Common.Misc.ReadGeneralParams("SasHost", "CurrencySymbol");
              }
              else
              {
                _response.CurrencySymbol = "";
              }
              _response.Language = WSI.Common.GeneralParam.GetString("SasHost", "Language", "es");  //Spanish as default value
              _response.SystemMode = (Int32)Misc.SystemMode();

              _response.TitoCloseSessionWhenRemoveCard = WSI.Common.GeneralParam.GetInt32("TITO", "CloseSessionWhenRemoveCard", 0);

              // JMV
              WSI.Common.CurrencyExchange.GetAllowedCurrencies(true, out _national_currency, out _currencies);

              // RCI & MPO & ACC 06-OCT-2015: TFS-5074: If currency_exchange.formatted_decimals is negative, the format must be dynamic.
              if (_national_currency.FormattedDecimals < 0)
              {
                _response.FormatDecimalsFlags = 2;
              }
              else
              {
                _response.FormatDecimalsFlags = (Byte)WSI.Common.GeneralParam.GetInt32("RegionalOptions", "DynamicFormatDecimals", 0);
              }

              _response.NumDecimals = (Byte)Math.Abs(_national_currency.FormattedDecimals);

              // FJC 29-JUN-2015
              technician_activity_timeout = GeneralParam.GetInt32("SasHost", "TechnicianTimeout", 1);
              _response.TechnicianActivityTimeout = technician_activity_timeout;

              // JMM 21-MAR-2016: Mico2 mode
              _response.Mico2 = Misc.IsMico2Mode();
              _response.EnableDisableNoteAcceptor = ((_response.SasFlags & (Int32)SAS_FLAGS.SAS_FLAGS_ENABLE_DISABLE_NOTE_ACCEPTOR) == (Int32)SAS_FLAGS.SAS_FLAGS_ENABLE_DISABLE_NOTE_ACCEPTOR);

              // FJC 30-JAN-2017 PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
              _response.EnableDisableTerminalDraw = CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws((Int16)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02);

              // FJC 24-MAR-2017 PBI 26015:ZitroTrack
              _response.EnableDisableZitroTrack = WSI.Common.GeneralParam.GetBoolean("Zitro.Track", "Enabled", false);

              _response.BillInLimitLockThreshold = (Int64)WSI.Common.GeneralParam.GetCurrency("TITO", "Threshold.EGM.BillIn", 0);
              _response.BillInLimitLockMode = WSI.Common.GeneralParam.GetInt32("TITO", "Threshold.EGM.BillIn.BlockMode", 0);

              // FJC 30-NOV-2017 WIGOS-3998 MobiBank: InTouch - Card out
              _response.EnableDisableMobiBank = WSI.Common.GeneralParam.GetBoolean("Movibank", "Device", false);

              // Send Message
              msg_response_str = _wcp_response.ToXml();
              _tcp_client.Send(msg_response_str);
              NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, msg_response_str);

              _error = false;

              return;
            }

          case WCP_MsgTypes.WCP_MsgPlayConditions:
            {
              WCP_MsgPlayConditions request;
              WCP_MsgPlayConditionsReply response;
              String msg_response_str;

              request = (WCP_MsgPlayConditions)_wcp_request.MsgContent;
              response = (WCP_MsgPlayConditionsReply)_wcp_response.MsgContent;

              // Check Play Session Id
              if (!PlaySession.IsActive(request.CardSessionId))
              {
                _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED;
                _wcp_response.MsgHeader.ResponseCodeText = "CardSessionId isn't active.";
              }
              else
              {
                // Check played amount
                if (request.SessionBalanceBeforePlay < request.BetAmount)
                {
                  _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
                  _wcp_response.MsgHeader.ResponseCodeText = "Play Not Possible: BalanceBeforePlay lower than BetAmount";
                }
                else
                {
                  // TODO: Check balance from the active play session
                  response.SessionBalanceBeforePlay = request.SessionBalanceBeforePlay;

                  if (!WCP_BusinessLogic.DB_EnoughActivePlaySessions(request.CardSessionId))
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_NOT_ENOUGH_PLAYERS;
                    _wcp_response.MsgHeader.ResponseCodeText = "Opened Play Sessions less than 2.";
                  }
                }
              }

              msg_response_str = _wcp_response.ToXml();
              _tcp_client.Send(msg_response_str);
              NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, msg_response_str);

              _error = false;

              return;
            }
          // Unreached code. 
          // break;

          case WCP_MsgTypes.WCP_MsgReportGameMeters:
            {
              WCP_MsgReportGameMeters request;
              WCP_MsgReportGameMetersReply response;
              String msg_response_str;

              request = (WCP_MsgReportGameMeters)_wcp_request.MsgContent;
              response = (WCP_MsgReportGameMetersReply)_wcp_response.MsgContent;

              if (!WCP_GameMeters.ProcessGameMeters(_tcp_client.InternalId, _wcp_request.MsgHeader.SequenceId, request))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "GameMeters not updated.");
              }

              msg_response_str = _wcp_response.ToXml();
              _tcp_client.Send(msg_response_str);
              NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, msg_response_str);

              _error = false;

              return;
            }
          // Unreached code. 
          // break;

          case WCP_MsgTypes.WCP_MsgReportHPCMeter:
            {
              String msg_response_str;

              if (!WCP_HPCMeter.ProcessHPCMeter(_tcp_client.InternalId, _wcp_request.MsgHeader.SequenceId, Task))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "HPCMeter not updated.");
              }

              msg_response_str = _wcp_response.ToXml();
              _tcp_client.Send(msg_response_str);
              NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, msg_response_str);

              _error = false;

              return;
            }
          // Unreached code. 
          // break;

          case WCP_MsgTypes.WCP_MsgReportMachineMeters:
            {
              WCP_MsgReportMachineMeters request;
              WCP_MsgReportMachineMetersReply response;
              String msg_response_str;

              request = (WCP_MsgReportMachineMeters)_wcp_request.MsgContent;
              response = (WCP_MsgReportMachineMetersReply)_wcp_response.MsgContent;

              //
              // This trx is processed in Batch Update. It should not go here !!
              // 

              msg_response_str = _wcp_response.ToXml();
              _tcp_client.Send(msg_response_str);
              NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, msg_response_str);

              _error = false;

              return;
            }
          case WCP_MsgTypes.WCP_MsgMachineMultiDenomInfo:
            {
              WCP_MsgMachineMultiDenomInfo request;
              WCP_MsgMachineMultiDenomInfoReply response;
              String msg_response_str;

              request = (WCP_MsgMachineMultiDenomInfo)_wcp_request.MsgContent;
              response = (WCP_MsgMachineMultiDenomInfoReply)_wcp_response.MsgContent;

              //
              // This trx is processed in Batch Update. It should not go here !!
              // 

              msg_response_str = _wcp_response.ToXml();
              _tcp_client.Send(msg_response_str);
              NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, msg_response_str);

              _error = false;

              return;
            }

          default:
            break;
        }

        // Get Sql Connection 
        _sql_conn = WGDB.Connection();


        //
        // Request from Gaming processing:
        //

        // Begin Transaction
        _sql_trx = _sql_conn.BeginTransaction();

        if (_wcp_request.MsgHeader.MsgType != WCP_MsgTypes.WCP_MsgEnrollServer
            && _wcp_request.MsgHeader.MsgType != WCP_MsgTypes.WCP_MsgEnrollTerminal)
        {
          if (_tcp_client.InternalId == 0)
          {
            if (_wcp_request.MsgHeader.TerminalId.Length > 0)
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "Terminal not enrolled");
            }
            else
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_SERVER_NOT_AUTHORIZED, "Server not enrolled");
            }
          }

          // AJQ 27-JUL-2010, Not a retry, the wcp messages are not inserted.

          if (!Common.BatchUpdate.TerminalSession.IsActive(_response_session_id))
          {
            if (_wcp_request.MsgHeader.TerminalId.Length > 0)
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_SESSION_NOT_VALID, "a.TerminalSessionId is not active");
            }
            else
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_SERVER_SESSION_NOT_VALID, "a.ServerSessionId is not active");
            }
          }
        }

        if (_wcp_request.MsgHeader.MsgType.ToString().IndexOf("Reply") >= 0)
        {
          return;
        }

        if (_wcp_request.MsgHeader.MsgType == WCP_MsgTypes.WCP_MsgEnrollServer)
        {
          if (_wcp_request.MsgHeader.ServerId.Length == 0)
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_SERVER_ID, "ServerId cannot be empty.");
          }
          if (_wcp_request.MsgHeader.ServerSessionId != 0)
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_SERVER_SESSION_ID, "ServerSessionId should be 0");
          }
        }
        else
        {
          if (_wcp_request.MsgHeader.ServerId.Length > 0)
          {
            if (_wcp_request.MsgHeader.ServerSessionId == 0)
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_SERVER_SESSION_ID, "ServerSessionId cannot be 0");
            }
            if (!Common.BatchUpdate.TerminalSession.IsActive(_wcp_request.MsgHeader.ServerSessionId))
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_SERVER_SESSION_NOT_VALID, "ServerSessionId is not active");
            }
          }
        }

        if (_wcp_request.MsgHeader.MsgType == WCP_MsgTypes.WCP_MsgEnrollTerminal)
        {
          if (_wcp_request.MsgHeader.TerminalId.Length == 0)
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_TERMINAL_ID, "TerminalId cannot be empty");
          }
          if (_wcp_request.MsgHeader.TerminalSessionId != 0)
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_TERMINAL_SESSION_ID, "TerminalSessionId should be 0");
          }
        }
        else
        {
          if (_wcp_request.MsgHeader.TerminalId.Length > 0)
          {
            if (_wcp_request.MsgHeader.TerminalSessionId == 0)
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_TERMINAL_SESSION_ID, "TerminalSessionId cannot be 0");
            }

            if (!Common.BatchUpdate.TerminalSession.IsActive(_wcp_request.MsgHeader.TerminalSessionId))
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_SESSION_NOT_VALID, "TerminalSessionId is not active");
            }
          }
        }

        if (_wcp_request.MsgHeader.TerminalId.Length > 0)
        {
          if (_wcp_request.MsgHeader.TerminalSessionId > 0)
          {
            if (!Common.BatchUpdate.TerminalSession.IsActive(_wcp_request.MsgHeader.TerminalSessionId))
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "TerminalSession is not active");
            }
          }
        }

        if (_wcp_request.MsgHeader.ServerId.Length > 0)
        {
          if (_wcp_request.MsgHeader.ServerSessionId > 0)
          {
            if (!Common.BatchUpdate.TerminalSession.IsActive(_wcp_request.MsgHeader.ServerSessionId))
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_SERVER_NOT_AUTHORIZED, "ServerSession is not active");
            }
          }
        }

        Boolean _end_session_ok;
        _end_session_ok = false;

        switch (_wcp_request.MsgHeader.MsgType)
        {
          case WCP_MsgTypes.WCP_MsgStartCardSession:
            {
              // ACC 25-JUL-2007
              // Check Server or Terminal is blocked.
              if (DbCache.IsTerminalLocked(_wcp_request.MsgHeader.TerminalId))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "Server or Terminal not authorized. Not found or blocked");
              }

              // RCI 17-AUG-2012: The DisableNewSessions check is performed now in the common StartSession routine: Trx_CommonStartCardSession().
            }
            break;

          case WCP_MsgTypes.WCP_MsgEndSession:
            {
              WCP_MsgEndSession request;

              request = (WCP_MsgEndSession)_wcp_request.MsgContent;

              // Check Play Session Id
              if (!PlaySession.IsActive(request.CardSessionId) && !request.OnlyAddBalances)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED, "CardSessionId isn't active.");
              }
            }
            break;

          case WCP_MsgTypes.WCP_MsgLockCard:
            {
              WCP_MsgLockCard request;
              request = (WCP_MsgLockCard)_wcp_request.MsgContent;

              // Check Play Session Id
              if (!PlaySession.IsActive(request.CardSessionId))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED, "CardSessionId isn't active.");
              }
            }
            break;

          case WCP_MsgTypes.WCP_MsgReportPlay:
            break;

          case WCP_MsgTypes.WCP_MsgAddCredit:
            {
              // MBF 26-JAN-2012 Implemented Add Credit Msg
              WCP_MsgAddCredit _request;
              WCP_MsgAddCreditReply _response;
              String _terminal_name;
              ArrayList _voucher_list;
              Int32 _last_hours_to_look_up_for_recharges;
              Int64 _promos_account_id;

              decimal _balance_after_add;

              _request = (WCP_MsgAddCredit)_wcp_request.MsgContent;
              _response = (WCP_MsgAddCreditReply)_wcp_response.MsgContent;

              _response.VoucherList = new ArrayList();

              Misc.GetTerminalName(_tcp_client.InternalId, out _terminal_name, _sql_trx);

              _last_hours_to_look_up_for_recharges = GeneralParam.GetInt32("ManualPromotions", "LastHoursToGetRecharges", 24);


              if (!Accounts.GetAccountIdFromExternalTrackData(_request.GetTrackData(0), _sql_trx, out _promos_account_id))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MoneyEvent - GetAccountIdFromExternalTrackData");
              }

              _response.AccountAlreadyHasPromo = Promotion.GetAccountCanApplyRechargesPromotion(_tcp_client.InternalId, _promos_account_id, _last_hours_to_look_up_for_recharges, _sql_trx);

              WCP_MoneyEvents.DB_AddCredit(_request.GetTrackData(0),
                                           _request.CardSessionId,
                                           ((decimal)_request.AmountToAdd) / 100,
                                           _request.TransactionId,
                                           _tcp_client.TerminalType,
                                           _tcp_client.InternalId,
                                           _terminal_name,
                                           _request.AcceptedTime,
                                           _request.StackerId,
                                           _wcp_request.MsgHeader.SequenceId,
                                           _wcp_request.TransactionId,
                                           true,
                                           out _balance_after_add,
                                           out _voucher_list,
                                           _sql_trx);

              _response.SessionBalanceAfterAdd = (Int64)(_balance_after_add * 100);

              _response.BillInGivesPromo = _response.AccountAlreadyHasPromo;
              if (!_response.BillInGivesPromo)
              {
                _response.BillInGivesPromo = Promotion.GetAccountCanApplyRechargesPromotion(_tcp_client.InternalId, _promos_account_id, _last_hours_to_look_up_for_recharges, _sql_trx);
              }

              if (!WSI.Common.TITO.Utils.IsTitoMode())
              {
                if (!WCP_MoneyEvents.DB_InsertOrUpdateCashierTerminalMoney(_request.AmountToAdd / 100, _tcp_client.InternalId, _sql_trx))
                {
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR);
                }
              }
              // TJG 01-OCT-2012
              // TELEVISA don't want vouchers to be printed when adding credits to the PromoBox
              // Temporary solution until a General Parameter is defined
              //if (_tcp_client.TerminalType == TerminalTypes.PROMOBOX)
              //{
              //  _response.VoucherList = _voucher_list;
              //}
            }
            break;

          case WCP_MsgTypes.WCP_MsgMoneyEvent:
            {

              if (!_is_tito_mode)
              {
                // MBF 01-FEB-2012 Implemented Money Event
                WCP_MsgMoneyEvent _request;
                WCP_MsgMoneyEventReply _response;
                PlaySession _playsession;
                Boolean _already_transferred = false;
                Int64 _account_id;
                Int64 _cashier_session_id;
                Int64 _play_session_id;
                Decimal _amount;

                _request = (WCP_MsgMoneyEvent)_wcp_request.MsgContent;
                _response = (WCP_MsgMoneyEventReply)_wcp_response.MsgContent;

                _amount = ((decimal)_request.Amount) / 100;

                _playsession = WSI.WCP.PlaySession.Get(_tcp_client.InternalId, _request.CardSessionId, _sql_trx);

                if (_playsession == null)
                {
                  Log.Message("*** Money Events: Play Session not found. TerminalId / TransactionId / TrackData = " + _tcp_client.InternalId.ToString() +
                              "/ " + _request.TransactionId.ToString() + "/ " + _request.GetTrackData(0) + ".");
                }

                _account_id = 0;
                _cashier_session_id = 0;
                _play_session_id = _request.CardSessionId;

                if (!WCP_MoneyEvents.DB_MoneyEvent(_tcp_client.InternalId
                                                   , _request.TransactionId
                                                   , _amount
                                                   , _request.LocalDatetime
                                                   , _request.GetTrackData(0)
                                                   , _play_session_id
                                                   , _account_id
                                                   , _request.LocalDatetime
                                                   , DateTime.MinValue
                                                   , _request.StackerId
                                                   , _cashier_session_id
                                                   , out _already_transferred
                                                   , _sql_trx))
                {
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR);  // TODO: Add a specific WCP_ResponseCode for the MoneyEvent.
                }
              }
            }
            break;

          case WCP_MsgTypes.WCP_MsgCashSessionPendingClosing:

            WCP_ChangeStacker.MsgProcess(Task, _is_tito_mode, _sql_trx);

            break;

          case WCP_MsgTypes.WCP_MsgReportEvent:
            {
              WCP_MsgReportEvent request;

              request = (WCP_MsgReportEvent)_wcp_request.MsgContent;

              WCP_BusinessLogic.DB_InsertEvent(_tcp_client.InternalId, _response_session_id, request.EventType, request.EventData, request.EventDateTime, _sql_trx);
            }
            break;

          case WCP_MsgTypes.WCP_MsgReportMeters:
            {
              WCP_MsgReportMeters request;

              request = (WCP_MsgReportMeters)_wcp_request.MsgContent;

              WCP_BusinessLogic.DB_UpdateMeters(_tcp_client.InternalId, request, _sql_trx);
            }
            break;

          // Mobile Bank
          case WCP_MsgTypes.WCP_MsgMobileBankCardCheck:
            {
              WCP_MsgMobileBankCardCheck request;
              WCP_MsgMobileBankCardCheckReply response;
              WCP_ResponseCodes _rc;
              TimeSpan _diff;
              Int32 _diff_seconds;
              Int32 _time_gap_seconds;

              // ACC 20-JAN-2016 Check Diff request
              {
                _diff = WGDB.Now - _wcp_request.MsgHeader.GetSystemTime.AddMilliseconds(_tcp_client.DiffDatetimeMilliSeconds);
                _diff_seconds = (int)Math.Abs(_diff.TotalSeconds);

                // ACC 20-JAN-2016 if received request delayed return ERROR (RECHARGE_NOT_AUTHORIZED)
                _time_gap_seconds = GeneralParam.GetInt32("MobileBank", "TimeGapSeconds", 0);

                if (_time_gap_seconds > 0 && _diff_seconds > _time_gap_seconds)
                {
                  Log.Error("MobileBank: Gap = " + _diff_seconds.ToString() + " s., RECHARGE_NOT_AUTHORIZED." + " ClientTimeOffset: " + _tcp_client.DiffDatetimeMilliSeconds.ToString() + " ms.");
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_RECHARGE_NOT_AUTHORIZED, "Not Synch.");
                }
                else if (_time_gap_seconds == 0 && _diff_seconds > 10)
                {
                  Log.Warning("MobileBank: Gap = " + _diff_seconds.ToString() + " s." + " ClientTimeOffset: " + _tcp_client.DiffDatetimeMilliSeconds.ToString() + " ms.");
                }
              }

              request = (WCP_MsgMobileBankCardCheck)_wcp_request.MsgContent;
              response = (WCP_MsgMobileBankCardCheckReply)_wcp_response.MsgContent;

              // Check Server or Terminal is blocked.
              if (DbCache.IsTerminalLocked(_wcp_request.MsgHeader.TerminalId))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "Server or Terminal not authorized. Not found or blocked");
              }

              // Mobile Bank Set last terminal
              WCP_BusinessLogic.DB_MobileBankSetTerminal(request.MobileBankGetTrackData(0), _tcp_client.InternalId, _tcp_client.Identity, _sql_trx);

              // Check Mobile account
              _rc = WCP_BusinessLogic.DB_MobileBankAccountCheck(request.MobileBankGetTrackData(0), request.Pin, 0, out response.MobileBankAuthorizedAmountx100, _sql_trx);
              switch (_rc)
              {
                case WCP_ResponseCodes.WCP_RC_CARD_WRONG_PIN:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_WRONG_PIN;
                    _wcp_response.MsgHeader.ResponseCodeText = "Wrong Pin.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
                    _wcp_response.MsgHeader.ResponseCodeText = "AuthorizedAmount lower than AmountToTransfer.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT;
                    _wcp_response.MsgHeader.ResponseCodeText = "Total recharges exceeded total recharges limit for MB.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGE_LIMIT:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGE_LIMIT;
                    _wcp_response.MsgHeader.ResponseCodeText = "Recharge exceeded total recharge limit for MB.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT;
                    _wcp_response.MsgHeader.ResponseCodeText = "AuthorizedAmount lower than AmountToTransfer.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_MB_SESSION_CLOSED:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_MB_SESSION_CLOSED;
                    _wcp_response.MsgHeader.ResponseCodeText = "Session closed.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_OK:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
                  }
                  break;

                default:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_ERROR;
                    _wcp_response.MsgHeader.ResponseCodeText = "DB_MobileBankAccountCheck. Unknown response code: " + _rc.ToString();
                  }
                  break;
              }
            }
            break;

          case WCP_MsgTypes.WCP_MsgMobileBankCardTransfer:
            {
              WCP_MsgMobileBankCardTransfer request;
              WCP_MsgMobileBankCardTransferReply response;
              WCP_ResponseCodes _rc;
              Int64 authorized_amount;
              Decimal previous_account_balance;
              Decimal current_account_balance;
              String _terminal_name;
              TimeSpan _diff;
              Int32 _diff_seconds;
              Int32 _time_gap_seconds;
              WCP_AccountManager.WCP_Account _account;
              
              // ACC 20-JAN-2016 Check Diff request
              {
                _diff = WGDB.Now - _wcp_request.MsgHeader.GetSystemTime.AddMilliseconds(_tcp_client.DiffDatetimeMilliSeconds);
                _diff_seconds = (int)Math.Abs(_diff.TotalSeconds);

                // ACC 20-JAN-2016 if received request delayed return ERROR (RECHARGE_NOT_AUTHORIZED)
                _time_gap_seconds = GeneralParam.GetInt32("MobileBank", "TimeGapSeconds", 0);

                if (_time_gap_seconds > 0 && _diff_seconds > _time_gap_seconds)
                {
                  Log.Error("MobileBank: Gap = " + _diff_seconds.ToString() + " s., RECHARGE_NOT_AUTHORIZED." + " ClientTimeOffset: " + _tcp_client.DiffDatetimeMilliSeconds.ToString() + " ms.");
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_RECHARGE_NOT_AUTHORIZED, "Not Synch.");
                }
                else if (_time_gap_seconds == 0 && _diff_seconds > 10)
                {
                  Log.Warning("MobileBank: Gap = " + _diff_seconds.ToString() + " s." + " ClientTimeOffset: " + _tcp_client.DiffDatetimeMilliSeconds.ToString() + " ms.");
                }
              }

              previous_account_balance = 0;
              current_account_balance = 0;

              request = (WCP_MsgMobileBankCardTransfer)_wcp_request.MsgContent;
              response = (WCP_MsgMobileBankCardTransferReply)_wcp_response.MsgContent;              
              
              // Read Account
              _account = WCP_AccountManager.Account(request.PlayerGetTrackData(0), _sql_trx);

              // Check if account doesn't exist on the system
              if (_account == null)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Account doesn't exist on the system");
              }

              // Check if account is anonymous in MB recharge
              if (_account.level == 0 && !GeneralParam.GetBoolean("Cashier", "AllowedAnonymous", true))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "[Anonymous] Card not authorized");
              }

              // In case of mode Exped activated, an authorization must be requested
              if (ExternalPaymentAndSaleValidationCommon.ExpedMode == EnumExternalPaymentAndSaleValidationMode.MODE_EXPED
              && !Misc.ExternalWS.ExpedRequestAuthorization(_account.id, 0, 0))
              {
                if (ExternalPaymentAndSaleValidationCommon.ExpedCashierPaymentInCashLimit != 0
                    && request.AmountToTransferx100 >= ExternalPaymentAndSaleValidationCommon.ExpedCashierPaymentInCashLimit * 100)
                {

                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED, "[Exped] Card not authorized");
                }
              }
              
              // Check Server or Terminal is blocked.
              if (DbCache.IsTerminalLocked(_wcp_request.MsgHeader.TerminalId))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "Server or Terminal not authorized. Not found or blocked");
              }

              // Mobile Bank Set last terminal
              WCP_BusinessLogic.DB_MobileBankSetTerminal(request.MobileBankGetTrackData(0), _tcp_client.InternalId, _tcp_client.Identity, _sql_trx);

              // Check Mobile account
              _rc = WCP_BusinessLogic.DB_MobileBankAccountCheck(request.MobileBankGetTrackData(0), request.Pin, request.AmountToTransferx100, out authorized_amount, _sql_trx);
              switch (_rc)
              {
                case WCP_ResponseCodes.WCP_RC_CARD_WRONG_PIN:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_WRONG_PIN;
                    _wcp_response.MsgHeader.ResponseCodeText = "Wrong Pin.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
                    _wcp_response.MsgHeader.ResponseCodeText = "AuthorizedAmount lower than AmountToTransfer.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT;
                    _wcp_response.MsgHeader.ResponseCodeText = "Total recharges exceeded total recharges limit for MB.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGE_LIMIT:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGE_LIMIT;
                    _wcp_response.MsgHeader.ResponseCodeText = "Recharge exceeded total recharge limit for MB.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT;
                    _wcp_response.MsgHeader.ResponseCodeText = "AuthorizedAmount lower than AmountToTransfer.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_MB_SESSION_CLOSED:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_MB_SESSION_CLOSED;
                    _wcp_response.MsgHeader.ResponseCodeText = "Session closed.";
                  }
                  break;

                case WCP_ResponseCodes.WCP_RC_OK:
                  {
                    Misc.GetTerminalName(_tcp_client.InternalId, out _terminal_name, _sql_trx);
                    // Mobile account transfer credit
                    WCP_BusinessLogic.DB_MobileBankAccountTransfer(request.MobileBankGetTrackData(0),
                                                                   request.PlayerGetTrackData(0),
                                                                   _tcp_client.InternalId,
                                                                   _terminal_name,
                                                                   request.AmountToTransferx100,
                                                                   _wcp_request.MsgHeader.SequenceId,
                                                                   _wcp_request.TransactionId,
                                                                   out previous_account_balance,
                                                                   out current_account_balance,
                                                                   _sql_trx);
                  }
                  break;

                default:
                  {
                    _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_ERROR;
                    _wcp_response.MsgHeader.ResponseCodeText = "DB_MobileBankAccountCheck. Unknown response code: " + _rc.ToString();
                  }
                  break;
              }

              // Prepare response
              response.PlayerPreviousCardBalancex100 = (Int64)(previous_account_balance * 100);
              response.PlayerCurrentCardBalancex100 = (Int64)(current_account_balance * 100);

            }
            break;

          case WCP_MsgTypes.WCP_MsgCheckTicket:
            {
              //WCP_PayTicket.ProcessCheckTicket(_tcp_client, _wcp_request, _wcp_response, _sql_trx);
            }
            break;

          case WCP_MsgTypes.WCP_MsgPayTicket:
            {
              //WCP_PayTicket.ProcessPayTicket(_tcp_client, _wcp_request, _wcp_response, _sql_trx);
            }
            break;

          case WCP_MsgTypes.WCP_MsgEGMHandpays:
            {
              WCP_MsgEGMHandpays _request;
              WCP_MsgEGMHandpaysReply _response;
              Decimal _handpay_amount;
              Decimal _handpay_amount0;
              Boolean _does_hand_pay_exists;
              String _source_name;
              String _alarm = "";
              String _provider_id;
              String _terminal_name;
              long _sesion_id;
              Int64 _account_id;
              Decimal _redeem_cash_in;
              Decimal _redeem_cash_in0;
              Decimal _non_redeem_cash_in;
              Int64 _account_promotion_id;
              Currency _alarm_threshold;
              Boolean _automatically_pay_handpay;

              String _egm_iso;
              Decimal _egm_amt;
              Decimal _sys_amt;

              _request = (WCP_MsgEGMHandpays)_wcp_request.MsgContent;
              _response = (WCP_MsgEGMHandpaysReply)_wcp_response.MsgContent;

              // DHA 29-MAR-2016: set default values
              _egm_iso = CurrencyExchange.GetNationalCurrency();
              _sys_amt = _request.HandpayCents;
              _egm_amt = _request.HandpayCents;
              _redeem_cash_in0 = 0;

              _account_id = Accounts.GetOrCreateVirtualAccount(_response_terminal_id, _sql_trx);

              if (Misc.IsFloorDualCurrencyEnabled())
              {
                // DHA 29-MAR-2016: get conversion for Floor Dual Currency
                // Multiply to truncate decimal part because amount is in cents
                WSI.Common.TITO.HandPay.GetFloorDualCurrency(_request.HandpayCents / (Decimal)100.0, _response_terminal_id, out _sys_amt, out _egm_amt, out _egm_iso, _sql_trx);
                _sys_amt = _sys_amt * 100;
                _egm_amt = _egm_amt * 100;
              }

              // Block the virtual account
              MultiPromos.AccountBalance _dummy;
              if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, MultiPromos.AccountBalance.Zero, out _dummy, _sql_trx))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Error updating account");
              }

              if (!HandPay.DB_DoesHandPayExists(_response_terminal_id,
                                                _request.TransactionID,
                                                (Int64)_sys_amt,
                                                out _does_hand_pay_exists,
                                                _sql_trx))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Error checking if handpay exists");
              }

              // Insert handpay if doesn't exist.
              if (!_does_hand_pay_exists)
              {
                if (WCP_BusinessLogic.DB_InsertHandpay(_response_terminal_id, _request, _sql_trx))
                {
                  bool _hp_rollback_pending;

                  _hp_rollback_pending = false;

                  try
                  {
                    // JMM 11-OCT-2016: If automatic handpay paying is enabled, look up for the account id related to the play session & automatically pay the handpay
                    _automatically_pay_handpay = false;

                    if (Utils.Handpays_AutomaticallyPay(_response_terminal_id, _request.HandpayCents, _sql_trx))
                    {
                      // First, select the candidate play session id
                      Int64? _play_session_id_to_update;

                      _play_session_id_to_update = null;

                      if (_request.PlaySessionID != 0)
                      {
                        _play_session_id_to_update = _request.PlaySessionID;
                      }
                      else if (_request.PreviousPlaySessionID != 0)
                      {
                        _play_session_id_to_update = _request.PreviousPlaySessionID;
                      }

                      if (_play_session_id_to_update != null)
                      {
                        // Get the account id
                        StringBuilder _sb;
                        Object _obj;

                        _sb = new StringBuilder();

                        _sb.AppendLine("SELECT   PS_ACCOUNT_ID                        ");
                        _sb.AppendLine("  FROM   PLAY_SESSIONS                        ");
                        _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");

                        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx))
                        {
                          _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = _play_session_id_to_update;

                          try
                          {
                            _obj = _sql_cmd.ExecuteScalar();
                            if (_obj != null)
                            {
                              _account_id = (Int64)_obj;
                              _automatically_pay_handpay = true;
                            }
                          }
                          catch (Exception _ex)
                          {
                            Log.Error("WCP_MsgEGMHandpays: Error getting AccountID. PlaySessionId: " + _play_session_id_to_update.ToString());
                            Log.Exception(_ex);
                          }
                        }

                        if (_automatically_pay_handpay)
                        {
                          CardData _card_data = new CardData();
                          int _terminal_id;
                          TerminalStatus _status;
                          string _server_external_id;
                          Currency _sas_account_denom;
                          Currency _played_amount;
                          string _denomination_str;
                          string _denomination_value;
                          CashierSessionInfo _session_info;

                          _card_data = new CardData();

                          if (CardData.DB_CardGetAllData(_account_id, _card_data))
                          {
                            // Don't pay the handpay if the session is related to a virtual account
                            if (!_card_data.IsVirtualCard)
                            {
                              Handpays.HandpayRegisterOrCancelParameters _input;

                              if (Misc.GetTerminalInfo(_wcp_request.MsgHeader.TerminalId,
                                                       1,
                                                       out _terminal_id,
                                                       out _provider_id,
                                                       out _terminal_name,
                                                       out _status,
                                                       out _server_external_id,
                                                       out _sas_account_denom,
                                                       _sql_trx))
                              {
                                HandPay.GetFloorDualCurrency(((Decimal)_request.HandpayCents / 100),
                                                             _terminal_id,
                                                             out _sys_amt,
                                                             out _egm_amt,
                                                             out _egm_iso,
                                                             _sql_trx);

                                if (!Handpays.GetHandpaySessionPlayedAmount(_account_id,
                                                                            _terminal_id,
                                                                            _request.Datetime,
                                                                            out _played_amount,
                                                                            out _denomination_str,
                                                                            out _denomination_value))
                                {
                                  _played_amount = -1;
                                }

                                _input = new Handpays.HandpayRegisterOrCancelParameters();
                                _input.CancelHandpay = false;

                                _input.HandpayType = _request.HandpayType;
                                _input.HandpayDate = _request.Datetime;

                                _input.HandpayAmount = _sys_amt;

                                _input.CardData = _card_data;
                                _input.TerminalId = _terminal_id;
                                _input.TerminalProvider = _provider_id;
                                _input.TerminalName = _terminal_name;
                                _input.SessionPlayedAmount = _played_amount;
                                _input.DenominationStr = _denomination_str;
                                _input.DenominationValue = _denomination_value;
                                _input.ProgressiveId = 0;
                                _input.HandpayId = 0;
                                _input.Level = 0;

                                _input.HandpayAmt0 = _egm_amt;
                                _input.HandpayCur0 = _egm_iso;
                                _input.HandpayAmt1 = 0;
                                _input.HandpayCur1 = "";

                                _input.AccountOperationCommentHandPay = "";

                                _session_info = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);

                                _hp_rollback_pending = true;
                                _sql_trx.Save("HANDPAY AUTOMATIC PAYMENT");

                                if (Handpays.RegisterOrCancel(_input, _session_info, OperationCode.HANDPAY_AUTOMATICALLY_PAYMENT, _play_session_id_to_update, _sql_trx))
                                {
                                  _hp_rollback_pending = false;
                                }
                                else
                                {
                                  Log.Message("*** WCP_MsgEGMHandpays: Error occurred registering the Handpay!");
                                }
                              }
                            }
                          }
                        }  //if ( _automatically_pay_handpay )
                      }
                    }
                  }
                  catch (Exception _ex)
                  {
                    Log.Error("WCP_MsgEGMHandpays: Error on automatic payment.  TerminalID: [" + _response_terminal_id + "] HandpayCents: [" + _request.HandpayCents + "]");
                    Log.Exception(_ex);
                  }
                  finally
                  {
                    if (_hp_rollback_pending)
                    {
                      _sql_trx.Rollback("HANDPAY AUTOMATIC PAYMENT");
                    }
                  }

                  // DHA 29-MAR-2016: set values for national currency and exchange currency
                  _handpay_amount0 = _request.HandpayCents / (Decimal)100.0;
                  _handpay_amount = _sys_amt / (Decimal)100.0;

                  //calculate the promotion NR cost 
                  // DHA 29-MAR-2016: pass issued amounts to play session
                  if (WCP_BusinessLogic.DB_GetPlaySessionCashIn(_request.PlaySessionID, _request.PreviousPlaySessionID, out _redeem_cash_in0, out _non_redeem_cash_in, out _account_promotion_id, _sql_trx)
                                                             && _non_redeem_cash_in > 0
                                                             && _handpay_amount0 - _redeem_cash_in0 > 0
                                                             && _account_promotion_id > 0)
                  {
                    // DHA 29-MAR-2016: calculate redeem cash in for national currency and exchange
                    _redeem_cash_in = _redeem_cash_in0;
                    if (Misc.IsFloorDualCurrencyEnabled())
                    {
                      WSI.Common.TITO.HandPay.GetFloorDualCurrency(_redeem_cash_in0, _response_terminal_id, out _redeem_cash_in, out _redeem_cash_in0, out _egm_iso, _sql_trx);
                    }
                    MultiPromos.Trx_UpdateAccountPromotionCost(_handpay_amount - _redeem_cash_in, _account_promotion_id, _sql_trx);
                  }

                  // If there is a handpay less than 5 minutes ago.
                  if (HandPay.ExistsSuspectedHandPay(_response_terminal_id,
                                                      _handpay_amount,
                                                      _request.HandpayType,
                                                      _request.TransactionID,
                                                      _sql_trx))
                  {
                    WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(_response_terminal_id, out _sesion_id);
                    WSI.Common.BatchUpdate.TerminalSession.GetData(_response_terminal_id, _sesion_id, out _provider_id, out _terminal_name);
                    _source_name = WSI.Common.Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);

                    switch (_request.HandpayType)
                    {
                      case HANDPAY_TYPE.CANCELLED_CREDITS:
                        _alarm = Resource.String("STR_EA_0x00110010C", ((Currency)_handpay_amount).ToString());
                        break;
                      case HANDPAY_TYPE.JACKPOT:
                        _alarm = Resource.String("STR_EA_0x00110010J", ((Currency)_handpay_amount).ToString());
                        break;
                      default:
                        Log.Message("WCP_MsgTypes.WCP_MsgEGMHandpays --> No specified Handpay.");
                        break;
                    }

                    // Launch alarm.
                    Alarm.Register(AlarmSourceCode.TerminalWCP,
                                   _response_terminal_id,
                                   _source_name,
                                   (UInt32)AlarmCode.WCP_SuspectedHandPay,
                                   _alarm,
                                   AlarmSeverity.Warning);
                  }


                  if (!WCP_HPCMeter.DB_UpdateHandpayTicketId(_response_terminal_id,
                                                             _request.TransactionID,
                                                             _request.HandpayType,
                                                             _handpay_amount,
                                                             0,
                                                             _sql_trx))
                  {
                    Log.Message("WCP_MsgTypes.WCP_MsgEGMHandpays -->  DB_UpdateHandpayTicketID failed!!! ");
                  }

                  _alarm_threshold = GeneralParam.GetCurrency("Alarms", "HandpaysAmountThreshold", 0);

                  // Check if handpay amount exceeds the amount defined on the GP that signals the alarm point
                  if (_handpay_amount > _alarm_threshold && _alarm_threshold > 0)
                  {
                    WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(_response_terminal_id, out _sesion_id);
                    WSI.Common.BatchUpdate.TerminalSession.GetData(_response_terminal_id, _sesion_id, out _provider_id, out _terminal_name);
                    _source_name = WSI.Common.Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);

                    switch (_request.HandpayType)
                    {
                      case HANDPAY_TYPE.CANCELLED_CREDITS:
                        _alarm = Resource.String("STR_ALARM_CANCELED_CREDITS_AMOUNT_THRESHOLD", ((Currency)_handpay_amount).ToString());
                        break;
                      case HANDPAY_TYPE.JACKPOT:
                        _alarm = Resource.String("STR_ALARM_JACKPOT_AMOUNT_THRESHOLD", ((Currency)_handpay_amount).ToString());
                        break;
                      default:
                        Log.Message("WCP_MsgTypes.WCP_MsgEGMHandpays --> No specified Handpay.");
                        _alarm = String.Format("Handpay {0}", ((Currency)_handpay_amount));
                        break;
                    }

                    // Handpay amount exceedds GP amount. Insert alarm 
                    Alarm.Register(AlarmSourceCode.TerminalSystem,
                                   _response_terminal_id,
                                   _source_name,
                                   (UInt32)AlarmCode.TerminalSystem_HandpayWarningAmount,
                                   _alarm,
                                   AlarmSeverity.Warning,
                                   _request.Datetime,
                                   _sql_trx);
                  }
                }
              }
              else
              {
                _handpay_amount = _request.HandpayCents / (Decimal)100.0;

                // DHA 29-MAR-2016: calculate amount in national currency and exchange
                if (Misc.IsFloorDualCurrencyEnabled())
                {
                  WSI.Common.TITO.HandPay.GetFloorDualCurrency(_handpay_amount, _response_terminal_id, out _handpay_amount, out _handpay_amount0, out _egm_iso, _sql_trx);
                }

                if (!WCP_BusinessLogic.DB_UpdateHandPayCandidatePlaySession(_response_terminal_id,
                                                                            _handpay_amount,
                                                                            _request,
                                                                            _sql_trx))
                {
                  Log.Error("WCP_MsgTypes.WCP_MsgEGMHandpays -->  DB_UpdateHandPayCandidatePlaySession failed!!! ");
                }
              }
            }
            break;

          case WCP_MsgTypes.WCP_MsgBonusTransferStatus:
            {
              WCP_MsgBonusTransferStatus _request;
              WCP_MsgBonusTransferStatusReply _response;

              _request = (WCP_MsgBonusTransferStatus)_wcp_request.MsgContent;
              _response = (WCP_MsgBonusTransferStatusReply)_wcp_response.MsgContent;

              MassiveBonus.DB_UpdateBonus(_request);
            }
            break;
          case WCP_MsgTypes.WCP_MsgSubsMachinePartialCredit:
            {
              WCP_MsgSubsMachinePartialCredit _request;
              WCP_MsgSubsMachinePartialCreditReply _response;

              _request = (WCP_MsgSubsMachinePartialCredit)_wcp_request.MsgContent;
              _response = (WCP_MsgSubsMachinePartialCreditReply)_wcp_response.MsgContent;
            }
            break;

          case WCP_MsgTypes.WCP_MsgGameGatewayGetCredit: //GameGateway==> Cancel Modifiy or Reserve Credit || Get Credit
            {
              WCP_MsgGameGatewayGetCredit _request;
              WCP_MsgGameGatewayGetCreditReply _response;
              MultiPromos.REQUEST_TYPE _request_type;
              Decimal _request_amount;
              MultiPromos.REQUEST_TYPE_RESPONSE Status;

              //Account Movements
              Decimal _initial_reserved_credit;

              //Initialize
              Status = MultiPromos.REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_ERROR; //Error
              _request_amount = 0;
              _request_type = MultiPromos.REQUEST_TYPE.REPORT_RESERVED_CREDIT;

              // Initialize Account Movement
              _initial_reserved_credit = 0;

              //Get Request
              _request = (WCP_MsgGameGatewayGetCredit)_wcp_request.MsgContent;
              _response = (WCP_MsgGameGatewayGetCreditReply)_wcp_response.MsgContent;

              //Convert
              _request_type = (MultiPromos.REQUEST_TYPE)_request.TypeRequest;
              _request_amount = Convert.ToDecimal(_request.RequestAmount);

              switch (_request_type)
              {
                case MultiPromos.REQUEST_TYPE.REPORT_GET_CREDIT:          //Get Credit (WithOut Buckets)

                  if (WSI.Common.WcpCommands.GetGetCreditWcpCommandResponse(_request.CmdId,
                                                                            GeneralParam.GetInt32("GameGateway", "Provider.001.CreditTimeOut", 20, 10, 30)))
                  {
                    Status = (Int16)MultiPromos.REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_OK;
                  }

                  break;

                case MultiPromos.REQUEST_TYPE.REPORT_RESERVED_CREDIT:
                case MultiPromos.REQUEST_TYPE.CANCEL_RESERVED_CREDIT:
                case MultiPromos.REQUEST_TYPE.MODIFY_RESERVED_CREDIT:     //Reserve Credit (With Buckets)

                  using (DB_TRX _trx = new DB_TRX())
                  {
                    //1. Calling Process for reserve credit
                    if (MultiPromos.Trx_GameGateway_Reserve_Credit(_request.AccountId, _request_amount, _request_type, true, out Status, out _initial_reserved_credit, _trx.SqlTransaction))
                    {
                      // 2. Calling Process for create an account movement
                      if (WSI.Common.GameGateway.InsertAccountMovementGameGateway(_request.AccountId, _wcp_request.MsgHeader.TerminalId, _initial_reserved_credit, _request_amount, _trx.SqlTransaction))
                      {
                        _trx.Commit();

                        Status = (Int16)MultiPromos.REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_OK;
                      }
                      else
                      {
                        Log.Error(String.Format("WCP_MsgGameGatewayGetCredit -->  InsertAccountMovementGameGateway failed!!! -TerminalId: {0}; AccountId: {1}", _wcp_request.MsgHeader.TerminalId,
                                                                                                                                                                _request.AccountId));
                      }
                    }
                    else
                    {
                      Log.Error(String.Format("WCP_MsgGameGatewayGetCredit --> Trx_GameGateway_Reserve_Credit failed!!! -TerminalId: {0}; AccountId: {1}", _wcp_request.MsgHeader.TerminalId,
                                                                                                                                                           _request.AccountId));
                    }
                  }

                  break;

                default:

                  Log.Error("WCP_MsgGameGatewayGetCredit -->  _request_type not identified!!! ");

                  break;
              }

              _response.Status = (Int16)Status;

            } //WCP_MsgTypes.WCP_MsgGameGatewayGetCredit
            break;

          case WCP_MsgTypes.WCP_MsgGameGatewayProcessMsg:
            {
              WCP_MsgGameGatewayProcessMsg _request;
              WCP_MsgGameGatewayProcessMsgReply _response;

              _request = (WCP_MsgGameGatewayProcessMsg)_wcp_request.MsgContent;
              _response = (WCP_MsgGameGatewayProcessMsgReply)_wcp_response.MsgContent;

              // Delete Command (Prizes) (FROM LCD==>SAS==>WCP)
              WCP_Commands_GameGateway.DB_DeleteWcpCommandsGameGateway(_request.CmdId, _sql_trx);
              _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;

            }
            break;

          case WCP_MsgTypes.WCP_MsgVersionSignature:
            {
              WCP_MsgVersionSignature _request;

              _request = (WCP_MsgVersionSignature)_wcp_request.MsgContent;

              SwValidation.SWValidationProcessMsg(_request, _request.ValitadionID, _sql_trx);
            }
            break;

          default:
            break;
        }

        switch (_wcp_request.MsgHeader.MsgType)
        {
          case WCP_MsgTypes.WCP_MsgEnrollServer:
            WCP_ProcessEnroll.EnrollServer(_tcp_client, _wcp_request, _wcp_response, _sql_trx);
            break;

          case WCP_MsgTypes.WCP_MsgEnrollTerminal:
            WCP_ProcessEnroll.EnrollTerminal(_tcp_client, _wcp_request, _wcp_response, _sql_trx);
            break;

          case WCP_MsgTypes.WCP_MsgUnenrollServer:
            WCP_ProcessEnroll.UnenrollServer(_tcp_client, _wcp_request, _wcp_response, _sql_trx);
            break;

          case WCP_MsgTypes.WCP_MsgUnenrollTerminal:
            WCP_ProcessEnroll.UnenrollTerminal(_tcp_client, _wcp_request, _wcp_response, _sql_trx);
            break;

          case WCP_MsgTypes.WCP_MsgGetCardType:
            {
              WCP_MsgGetCardType request;
              WCP_MsgGetCardTypeReply response;
              String _track_data;
              Int32 _unused_int;
              Boolean _is_multisite_member;
              Int32 _user_type;
              Int32 _block_reason;
              String _terminal_name;

              request = (WCP_MsgGetCardType)_wcp_request.MsgContent;
              response = (WCP_MsgGetCardTypeReply)_wcp_response.MsgContent;
              // 11-DIC-2014 JML   Added UserType & BlockReason output in CardType
              response.CardType = WCP_AccountManager.CardType(request.GetTrackData(0), _sql_trx, out response.AccountId, out response.HolderName, out _user_type, out _block_reason);
              _is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember", false);

              // 11-DIC-2014  JML    Add message for anonymous accounts
              // 18-DIC-2014  JML    Fixed Bug WIG-1866: Message for anonymous accounts only for not blocked accounts
              if (_user_type == 0 && _block_reason == (Int32)AccountBlockReason.NONE)
              {
                LCDMessages.GetLcdMessage(_response_terminal_id, LCDMessages.ENUM_TYPE_MESSAGE.ANONYMOUS_ACCOUNT, out response.MessageLine1, out response.MessageLine2);
              }

              if (_is_multisite_member)
              {
                switch (response.CardType)
                {
                  case WCP_CardTypes.CARD_TYPE_PLAYER:
                  case WCP_CardTypes.CARD_TYPE_PLAYER_PIN:

                    _track_data = request.GetTrackData(0);

                    // Get internal track data from external trackdata
                    if (WSI.Common.CardNumber.TrackDataToInternal(_track_data, out _track_data, out _unused_int))
                    {
                      if (!CommonMultiSite.ForceSynchronization_Accounts(_track_data))
                      {
                        Log.Warning("Can't trigger to sincronize account....");
                      }
                    }

                    break;

                  default:
                    break;
                }
                // 11-DIC-2014 JML   Add message for block account by ELP (External system cancel)
                if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") == 1)
                {
                  if (Accounts.ExistsBlockReasonInMask(AccountBlockReason.EXTERNAL_SYSTEM_CANCELED, _block_reason))
                  {
                    LCDMessages.GetLcdMessage(_response_terminal_id, LCDMessages.ENUM_TYPE_MESSAGE.CANCELED_ACCOUNT, out response.MessageLine1, out response.MessageLine2);
                    //_wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_CARD_BLOCKED;
                    throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_BLOCKED);
                  }
                }
              }

              //JBC 30-01-2015 BirthDay alarm feature
              if (_user_type != 0 && (response.CardType == WCP_CardTypes.CARD_TYPE_PLAYER || response.CardType == WCP_CardTypes.CARD_TYPE_PLAYER_PIN))
              {
                Misc.GetTerminalName(_tcp_client.InternalId, out _terminal_name, _sql_trx);

                if (!MultiPromos.CheckBirthdayAlarm(response.AccountId, response.HolderName, _response_terminal_id.ToString() + " - " + _terminal_name, TerminalTypes.SAS_HOST, _sql_trx))
                {
                  Log.Error("WCP_Processor: CheckBirthdayAlarm error");
                }
              }

            }
            break;


          case WCP_MsgTypes.WCP_MsgCardInformation:
            {
              WCP_MsgCardInformation request;
              WCP_MsgCardInformationReply response;
              WCP_AccountManager.WCP_Account _account;
              Terminal _terminal;
              WSI.Common.Terminal.TerminalInfo _terminal_info;
              WSI.Common.TerminalReservation.TerminalReservationStatus _reservation_status;
              Int32 _reserved_terminal_id;

              String[] _level_name = new String[5];

              request = (WCP_MsgCardInformation)_wcp_request.MsgContent;
              response = (WCP_MsgCardInformationReply)_wcp_response.MsgContent;

              _terminal = null;
              _terminal_info = null;

              // Read Account
              _account = WCP_AccountManager.Account(request.GetTrackData(0), _sql_trx);
              if (_account == null)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
              }
              if (_account.type == AccountType.ACCOUNT_UNKNOWN)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
              }
              if ((_is_tito_mode) && (_account.blocked))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_BLOCKED);
              }

              // Check the terminal is not reserved by another account              

              _terminal = Terminal.GetTerminal(_tcp_client.InternalId, _sql_trx);

              _reservation_status = TerminalReservation.CheckStatus(_terminal.terminal_id, _account.id, out _reserved_terminal_id, _sql_trx);

              if (_reservation_status == TerminalReservation.TerminalReservationStatus.ReservedByAccount)
              {
                //The terminal was previously reserved by this account.  Clean it!
                TerminalReservation.Remove(_terminal.terminal_id, _sql_trx);
              }
              else if (_reservation_status == TerminalReservation.TerminalReservationStatus.ReservedByAnotherAccount)
              {
                //The terminal was previously reserved by another account.  Avoid to start card session
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_RESERVED);
              }
              else if (_reservation_status == TerminalReservation.TerminalReservationStatus.AccountHasAnotherTerminalReserved)
              {
                if (!GeneralParam.GetBoolean("Terminal", "AllowStartCardSessionWithAnotherReserve", false))
                {
                  //The terminal was previously reserved by another account.  Avoid to start card session
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ANOTHER_TERMINAL_RESERVED);
                }

                // The account has another terminal previously reserved.  Clean this reserve and allow to start card session
                TerminalReservation.Remove(_reserved_terminal_id, _sql_trx);
              }

              //On MICO2 mode, ensure the terminal is not linked to any other account
              if (WSI.Common.Misc.IsMico2Mode())
              {
                if (!MultiPromos.Trx_CheckTerminalLink(_account.id, _tcp_client.InternalId, _sql_trx))
                {
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_CLOSED);
                }
              }


              WCP_MultiSiteRequests.StartCardSession(_tcp_client.InternalId, _account.id, _account.level, _sql_trx);

              response.Account = _account.id;
              response.HolderName = _account.display_holder_name;
              response.Birthday = _account.birth_date;
              response.CardBalance = (Int64)(_account.display_balance * 100);

              response.SessionActive = false;

              if (_account.terminal_id != 0)
              {
                _terminal = Terminal.GetTerminal(_account.terminal_id, _sql_trx);
                response.LastTerminal = _terminal.name;
                response.SessionActive = true;
              }
              else if (_account.last_terminal_id != 0)
              {
                _terminal = Terminal.GetTerminal(_account.last_terminal_id, _sql_trx);
                response.LastTerminal = _terminal.name;
              }
              else
              {
                WSI.Common.Terminal.Trx_GetTerminalInfoByExternalId(_wcp_request.MsgHeader.TerminalId, out _terminal_info, _sql_trx);
                response.LastTerminal = "";
              }

              _level_name[0] = "---";
              _level_name[1] = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level01.Name");
              _level_name[2] = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level02.Name");
              _level_name[3] = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level03.Name");
              _level_name[4] = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level04.Name");

              if (_account.level < _level_name.Length)
              {
                response.Level = (String)_level_name[_account.level];
              }
              else
              {
                response.Level = _level_name[0];
              }

              response.Points = (Int32)_account.points;

              //ETP: PBI 5835 - Added new GP for NR Transfer amount - Default = MXN;5;25;50;75;100
              String _response = GeneralParam.GetString("DisplayTouch", "CustomButtons.TransferAmount.NR", "MXN;5;25;50;75;100");
              if (_terminal == null)
              {
                response.NRTransferAmount = WSI.Common.Misc.GetTerminalTransferAmount(_response, _terminal_info.CurrencyCode);
              }
              else
              {
                response.NRTransferAmount = WSI.Common.Misc.GetTerminalTransferAmount(_response, _terminal.terminal_ISO_code);
              }

            }
            break; // case WCP_MsgTypes.WCP_MsgCardInformation:

          case WCP_MsgTypes.WCP_MsgMBInformation:
            {
              WCP_MsgMBInformation _request;
              WCP_MsgMBInformationReply _response;
              String _internal_card_id;
              MBCardData _card_info = new MBCardData();
              Int32 _card_type;
              Int64 _sequence_id;
              String _sql_txt;

              _request = (WCP_MsgMBInformation)_wcp_request.MsgContent;
              _response = (WCP_MsgMBInformationReply)_wcp_response.MsgContent;

              // Get Mb Card Id
              _card_info.CardId = 0;

              // Obtain internal card identifier from track data
              if (!CardNumber.TrackDataToInternal(_request.GetTrackData(0), out _internal_card_id, out _card_type))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
              }

              // Obtain card site id.
              WSI.Common.CardNumber.SplitInternalTrackData(_internal_card_id, out _card_info.SiteId, out _sequence_id);

              // Check the card belong to the site.
              if (!CardData.CheckTrackdataSiteId( new ExternalTrackData( _request.GetTrackData(0))))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
              }

              using (DB_TRX _db_trx = new DB_TRX())
              {
                try
                {
                  // Get card data
                  _sql_txt = "SELECT MB_ACCOUNT_ID " +
                               "FROM MOBILE_BANKS " +
                              "WHERE MB_TRACK_DATA = @pMbTrackData ";

                  using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt))
                  {
                    _sql_cmd.Parameters.Add("@pMbTrackData", SqlDbType.NVarChar, 50, "MB_TRACK_DATA").Value = _internal_card_id;

                    _card_info.CardId = ((Int64)_db_trx.ExecuteScalar(_sql_cmd));
                  }
                }
                catch (Exception _ex)
                {
                  Log.Exception(_ex);
                }
              }

              if (_card_info.CardId == 0)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
              }

              // Read Mb Account Info
              if (!MobileBank.DB_MBCardGetAllData(_card_info.CardId, _card_info, null))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
              }

              _response.MbAccount = _card_info.CardId;
              _response.MbName = _card_info.HolderName;
              _response.MbLimitx100 = (Int64)(_card_info.SalesLimit * 100);

              //
              // Get Last Movements
              //
              using (DB_TRX _trx = new DB_TRX())
              {
                try
                {
                  _sql_txt = "  SELECT TOP 10    " +
                             "          MBM_DATETIME   " +
                             "        , CASE WHEN (AC_HOLDER_NAME IS NULL) THEN ' --- ' ELSE AC_HOLDER_NAME END  " +
                             "        , ISNULL(MBM_TERMINAL_NAME, '') TERMINAL_NAME " +
                             "        , MBM_SUB_AMOUNT   " +
                             "    FROM  MB_MOVEMENTS    " +
                             "          LEFT OUTER JOIN  ACCOUNTS ON MB_MOVEMENTS.MBM_PLAYER_TRACKDATA = ACCOUNTS.AC_TRACK_DATA     " +
                             "   WHERE  MBM_MB_ID = @pCardId   " +
                             "     AND  MBM_TYPE = 1   " +
                             " ORDER BY MBM_DATETIME DESC, MBM_MOVEMENT_ID DESC   ";

                  using (SqlCommand _cmd = new SqlCommand(_sql_txt, _trx.SqlTransaction.Connection, _trx.SqlTransaction))
                  {
                    _cmd.Parameters.Add("@pCardId", SqlDbType.BigInt).Value = _card_info.CardId;

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                      while (_reader.Read() && !_reader.IsDBNull(0))
                      {
                        WCP_TransferHistoryItem _item = new WCP_TransferHistoryItem();

                        _item.Datetime = (DateTime)_reader[0];
                        _item.PlayerName = (String)_reader[1];
                        _item.TerminalName = (String)_reader[2];
                        _item.Amountx100 = (Int64)(((Decimal)_reader[3]) * 100);

                        _response.TransferHistoryItems.Add(_item);
                      }
                    } // using SQLDataReader
                  } // using SqlCommand
                } // try
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
              } // using DB_TRX
            }
            break; // case WCP_MsgTypes.WCP_MsgMBInformation:

          case WCP_MsgTypes.WCP_MsgStartCardSession:
            {
              WCP_MsgStartCardSession _request;
              WCP_MsgStartCardSessionReply _response;
              Decimal account_balance;
              Terminal terminal;
              Int64 play_session_id;
              String _s2s_holder_name_to_display;
              MultiPromos.AccountBalance _account_balance_to_transfer;
              MultiPromos.StartSessionTransferMode _transfer_mode;
              Boolean _transfer_with_buckets;

              _s2s_holder_name_to_display = "";
              _request = (WCP_MsgStartCardSession)_wcp_request.MsgContent;

              _response = (WCP_MsgStartCardSessionReply)_wcp_response.MsgContent;

              if (WCP_BusinessLogic.CashlessMode == CashlessMode.ALESIS)
              {
                {
                  Alesis.StatusCode status_code;
                  AleTerminal ale_terminal;

                  terminal = Terminal.GetTerminal(_tcp_client.InternalId, _sql_trx);

                  _request = (WCP_MsgStartCardSession)_wcp_request.MsgContent;
                  _response = (WCP_MsgStartCardSessionReply)_wcp_response.MsgContent;

                  status_code = Alesis.StartCardSession(_tcp_client.InternalId, 0, _request.GetTrackData(0), 0, out account_balance, out _s2s_holder_name_to_display, _sql_trx);

                  switch (status_code)
                  {
                    case Alesis.StatusCode.Success:
                      {
                        //ale_terminal = (AleTerminal)terminal;

                        //if (ale_terminal.ale_status != AleTerminal.ConfigurationStatus.OK)
                        //{
                        //  ale_terminal.SetTerminalConfigurationStatus(AleTerminal.ConfigurationStatus.OK, sql_trx);
                        //}

                        //_account.display_balance = account_balance;
                      }
                      break;

                    case Alesis.StatusCode.AccountNumberNotValid:
                      throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Alesis player account: not valid.");
                    //break;

                    case Alesis.StatusCode.AccountAlreadyInUse:
                      throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_ALREADY_IN_USE, "Alesis player account: card already in use.");
                    //break;

                    case Alesis.StatusCode.AccessDenied:
                      throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_BLOCKED, "Alesis player account: access denied.");
                    //break;

                    case Alesis.StatusCode.NotConnectedToDatabase:
                      throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Alesis player account: not connected to database.");
                    //break;

                    case Alesis.StatusCode.MachineNumberNotValid:
                      {
                        ale_terminal = (AleTerminal)terminal;

                        if (ale_terminal.ale_status != AleTerminal.ConfigurationStatus.ERROR)
                        {
                          ale_terminal.SetTerminalConfigurationStatus(AleTerminal.ConfigurationStatus.ERROR, _sql_trx);
                        }

                        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "Alesis terminal: Not authorized.");
                      }
                    //break;

                    default:
                      throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Alesis player account: Unexpected alesis return code.");
                    //break;

                  } // switch
                }

              }

              Boolean stand_alone_session;
              Decimal mov_promo_ini_fin_balance;
              Boolean _dummy;
              PlaySessionType _ps_type;
              WCP_AccountManager.WCP_Account _account;
              Int64 _transaction_id;
              Int64 _account_id;
              TerminalReservation.TerminalReservationStatus _reservation_status;
              Int32 _reserved_terminal_id;

              // TODO: Check Version.
              ////String _protocol_major_version_str;
              ////Int32 _protocol_major_version;

              stand_alone_session = WCP_BusinessLogic.DB_PlayStandAlone(_tcp_client.InternalId, out _dummy, _sql_trx);

              //
              // TODO: Check Version.
              //
              ////_protocol_major_version_str = wcp_request.MsgHeader.GetProtocolVersion.Substring(0, wcp_request.MsgHeader.GetProtocolVersion.IndexOf("."));
              ////if (!Int32.TryParse(_protocol_major_version_str, out _protocol_major_version))
              ////{
              ////  _protocol_major_version = 0;
              ////}

              ////if (_protocol_major_version < 2)
              ////{
              ////  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "Protocol major version is incorrect.");
              ////}

              terminal = Terminal.GetTerminal(_tcp_client.InternalId, _sql_trx);
              if (terminal.blocked)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "Terminal Is Blocked.");
              }

              // XCD 21-JAN-2015 Do partial transfer, if 0 transfer all credit
              _account_balance_to_transfer = MultiPromos.AccountBalance.Zero;
              _transfer_mode = (MultiPromos.StartSessionTransferMode)_request.GetTranferMode();

              if (_transfer_mode == MultiPromos.StartSessionTransferMode.Partial
               || _transfer_mode == MultiPromos.StartSessionTransferMode.All)
              {
                _account_balance_to_transfer = CurrencyExchange.GetExchange(_request.GetBalanceToTransfer(), terminal.terminal_ISO_code,
                                                                            CurrencyExchange.GetNationalCurrency(), _sql_trx);

                if (_is_tito_mode &&
                  !terminal.terminal_ISO_code.Equals(CurrencyExchange.GetNationalCurrency())
                  && _request.GetBalanceToTransfer().PromoNotRedeemable != 0)
                {
                  String _str_transfer_amounts = GeneralParam.GetString("DisplayTouch", "CustomButtons.TransferAmount.NR", "MXN;5;25;50;75;100");
                  _str_transfer_amounts = WSI.Common.Misc.GetTerminalTransferAmount(_str_transfer_amounts, terminal.terminal_ISO_code);
                  String[] _transfer_amount;

                  _transfer_amount = _str_transfer_amounts.Split(';');
                  Decimal _button_amount = -1;

                  foreach (String _amount in _transfer_amount)
                  {
                    if (!Decimal.TryParse(_amount, out _button_amount))
                    {
                      continue;
                    }
                    if (_button_amount == _request.GetBalanceToTransfer().PromoNotRedeemable)
                    {
                      break;
                    }

                  }
                  if (_button_amount != _request.GetBalanceToTransfer().PromoNotRedeemable)
                  {
                    _transfer_mode = MultiPromos.StartSessionTransferMode.All;
                  }

                }

              }

              _account = null;

              //On Mico2, the play session could be started with no card.  So, is there is no a valid account, get the virtual one
              if (WSI.Common.Misc.IsMico2Mode())
              {
                String _external_track_data;

                _external_track_data = _request.GetTrackData(0);

                if (_external_track_data != "00000000000000000000")
                {
                  _account = WCP_AccountManager.Account(_external_track_data, _sql_trx);
                }

                if (WSI.Common.Misc.IsMico2Mode() && _account == null)
                {
                  _account_id = Accounts.GetOrCreateVirtualAccount(terminal.terminal_id, _sql_trx);
                  _account = WCP_AccountManager.Account(_account_id, _sql_trx);
                }
              }
              else
              {
                // Read Account
                if (_is_tito_mode)
                {
                  _account_id = Accounts.GetOrCreateVirtualAccount(terminal.terminal_id, _sql_trx);
                  _account = WCP_AccountManager.Account(_account_id, _sql_trx);
                }
                else
                {
                  _account = WCP_AccountManager.Account(_request.GetTrackData(0), _sql_trx);
                }
              }

              if (_account == null)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
              }
              if (_account.type == AccountType.ACCOUNT_UNKNOWN)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
              }
              if (_account.blocked)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_BLOCKED);
              }

              // Check the terminal is not reserved by another account
              _reservation_status = TerminalReservation.CheckStatus(terminal.terminal_id, _account.id, out _reserved_terminal_id, _sql_trx);

              if (_reservation_status == TerminalReservation.TerminalReservationStatus.ReservedByAccount)
              {
                //The terminal was previously reserved by this account.  Clean it!
                TerminalReservation.Remove(terminal.terminal_id, _sql_trx);
              }
              else if (_reservation_status == TerminalReservation.TerminalReservationStatus.ReservedByAnotherAccount)
              {
                //The terminal was previously reserved by another account.  Avoid to start card session
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_RESERVED);
              }
              else if (_reservation_status == TerminalReservation.TerminalReservationStatus.AccountHasAnotherTerminalReserved)
              {
                if (!GeneralParam.GetBoolean("Terminal", "AllowStartCardSessionWithAnotherReserve", false))
                {
                  //The terminal was previously reserved by another account.  Avoid to start card session
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ANOTHER_TERMINAL_RESERVED);
                }

                // The account has another terminal previously reserved.  Clean this reserve and allow to start card session                
                TerminalReservation.Remove(_reserved_terminal_id, _sql_trx);
              }
              
              WCP_ResponseCodes _rc;
              _rc = WCP_ResponseCodes.WCP_RC_OK;

              if (Accounts.DoesActionRequirePlayerPinRequest(Accounts.PinRequestSource.EBOX
                                                            , Accounts.PinRequestOperationType.PLAY
                                                            , String.IsNullOrEmpty(_account.holder_name)))
              {
                // RCI 24-NOV-2011: Modified call to method DB_CheckAccountPIN().
                _rc = WCP_BusinessLogic.DB_CheckAccountPIN(_account.id, _request.Pin);
              }

              if (_rc != WCP_ResponseCodes.WCP_RC_OK)
              {
                throw new WCP_Exception(_rc);
              }

              // RCI 09-AUG-2010: Get TransactionId.
              _transaction_id = _request.GetTransactionId();

              // save the "real" balance
              mov_promo_ini_fin_balance = _account.display_balance;


              // No current Play Session 
              play_session_id = 0;
              if (_account.type == AccountType.ACCOUNT_WIN)
              {
                //
                // Check Account Status
                // AJQ 21-JAN-2010, If current terminal OR play session is not 0 means card in use

                if (_account.play_session_id != 0 || _account.terminal_id != 0)
                {
                  if (_account.terminal_id != _tcp_client.InternalId)
                  {
                    throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_ALREADY_IN_USE, "Duplicated Card In.");
                  }
                  if (PlaySession.IsActive(_account.play_session_id))
                  {
                    // Unlocks the session
                    if (!WCP_BusinessLogic.DB_UnlockPlaySession(_account.play_session_id, _sql_trx))
                    {
                      Log.Warning("Card Session cannot be unlocked. CardSessionId: " + _account.play_session_id.ToString());
                      throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_VALID, "Card Session cannot be unlocked");
                    }
                    play_session_id = _account.play_session_id;
                  }
                }
              }
              else if (_account.type == AccountType.ACCOUNT_ALESIS)
              {
                if (!MultiPromos.Trx_UnlinkAccountTerminal(_account.id, _account.terminal_id, _account.play_session_id, _sql_trx))
                {
                  Log.Warning("Alesis Card Session cannot be unlinked. CardSessionId: " + _account.play_session_id.ToString());
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_VALID, "Alesis Card Session cannot be unlinked");
                }
              }
              //ETP: Only used in cashless transfer. 
              _transfer_with_buckets = _request.IsTransferWithBuckets();

              MultiPromos.StartSessionInput _input;
              MultiPromos.StartSessionOutput _output;

              _input = new MultiPromos.StartSessionInput();
              _input.TransactionId = _transaction_id;
              _input.TerminalId = _tcp_client.InternalId;
              _input.TrackData = _request.GetTrackData(0);
              _input.IsTitoMode = _is_tito_mode;
              _input.TransferMode = _transfer_mode;
              _input.AccountBalanceToTransfer = _account_balance_to_transfer;
              _input.TransferWithBuckets = _transfer_with_buckets;

              // ACC 22-MAY-2018 Pulses protocol with MICO2 mode forced CASHLESS StartCardSession
              SMIB_COMMUNICATION_TYPE _protocol;
              Int64 _dummy_int64;

              if (Misc.IsMico2Mode())
              {
                if (WSI.Common.Terminal.Trx_GetTerminalSmibProtocol(_response_terminal_id, _tcp_client.TerminalType, _sql_trx, out _protocol, out _dummy_int64))
                {
                  if (_protocol == SMIB_COMMUNICATION_TYPE.PULSES)
                  {
                    _input.IsTitoMode = false;
                  }
                }
              }
              
              _output = MultiPromos.Trx_WcpStartCardSession(_input, _sql_trx);

              _output.PlayableBalance.ModeReserved = _account.reserved_mode;

              switch (_output.StatusCode)
              {
                case MultiPromos.StartSessionStatus.Ok:
                  break;

                case MultiPromos.StartSessionStatus.AccountUnknown:
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "");

                case MultiPromos.StartSessionStatus.AccountBlocked:
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_BLOCKED, "");

                case MultiPromos.StartSessionStatus.AccountInSession:
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_ALREADY_IN_USE, "");

                case MultiPromos.StartSessionStatus.Error:
                  Log.Error("Trx_WcpStartCardSession. " + _output.ErrorMsg);
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "");

                case MultiPromos.StartSessionStatus.TerminalUnknown:
                case MultiPromos.StartSessionStatus.TerminalBlocked:
                default:
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "");
              }


              switch (_account.type)
              {
                case AccountType.ACCOUNT_WIN:
                case AccountType.ACCOUNT_VIRTUAL_TERMINAL:
                  // Do nothing
                  break;

                case AccountType.ACCOUNT_ALESIS:
                  _account.display_holder_name = _s2s_holder_name_to_display;
                  break;

                default:
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED, "Invalid card type.");
                //break;
              }

              _response.CardBalance = (Int64)(_output.PlayableBalance.TotalBalance * 100);
              _response.CardSessionId = _output.PlaySessionId;
              _response.HolderName = _account.display_holder_name;
              _response.Points = (Int32)_output.Points;

              // ACC 06-JUN-2012 Allow CashIn while not promotions and enabled Acceptor.
              if (Misc.IsNoteAcceptorEnabled())
              {
                _response.AllowCashIn = true;
              }

              // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
              _response.Balance = _output.PlayableBalance;

              // RCI 15-JUL-2010: Add PlaySession in Cache.
              switch (_account.type)
              {
                case AccountType.ACCOUNT_WIN:
                case AccountType.ACCOUNT_VIRTUAL_TERMINAL:
                  _ps_type = PlaySessionType.WIN;
                  break;

                case AccountType.ACCOUNT_ALESIS:
                  _ps_type = PlaySessionType.ALESIS;
                  break;

                default:
                  _ps_type = PlaySessionType.NONE;
                  break;
              }

              PlaySession.Add(_tcp_client.InternalId,
                              _output.PlaySessionId,
                              _transaction_id,
                              _ps_type,
                              stand_alone_session,
                              _output.TotalToGMBalance.TotalBalance,
                              _output.TotalToGMBalance.TotalBalance,
                              terminal.name,
                              _account.id,
                              _account.int_track_data,
                              (_output.TotalToGMBalance.TotalBalance > _output.PlayableBalance.TotalBalance),
                              _output.PlayableBalance.TotalBalance);

              // ACC 02-MAY-2013 ELP & Requests to MultiSite in Start Card Session trx.
              WCP_MultiSiteRequests.StartCardSession(_tcp_client.InternalId, _account.id, _account.level, _sql_trx);
              if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
              {
                _response.Points = 0;
              }
            }
            break;
          case WCP_MsgTypes.WCP_MsgCancelStartSession:
            {
              WCP_MsgCancelStartSession _request;
              WCP_MsgCancelStartSessionReply _response;
              Int64 _closed_play_session_id;
              Boolean _cancelled;

              _request = (WCP_MsgCancelStartSession)_wcp_request.MsgContent;
              _response = (WCP_MsgCancelStartSessionReply)_wcp_response.MsgContent;

              if (Utils.IsTitoMode() && !WSI.Common.Misc.IsMico2Mode())
              {
                if (!MultiPromos.Trx_TITO_TransferFromAccount_CancelStartCardSession(_tcp_client.InternalId, _request.TransactionId, _request.GetCancelledBalance(), out _cancelled, out _closed_play_session_id, _sql_trx))
                {
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Unknown error.");
                }
              }
              else
              {
                if (!MultiPromos.Trx_CancelStartCardSession(_tcp_client.InternalId, _request.TransactionId, out _cancelled, out _closed_play_session_id, _sql_trx))
                {
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Unknown error.");
                }

                if (_cancelled)
                {
                  Log.Warning(String.Format("WCP_MsgCancelStartSession -> TerminalId={0}, PlaySessionId={1}, TransactionId={2}  *** Cancelled ***",
                              _tcp_client.InternalId,
                              _closed_play_session_id,
                              _request.TransactionId));
                  // TODO: Andreu  alarma y event
                  //Alarm.Register(AlarmSourceCode.TerminalWCP, _tcp_client.InternalId, 
                }

                if (_closed_play_session_id != 0)
                {
                  PlaySession.Unload(_closed_play_session_id);
                }
              }

              _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
            }
            break;

          case WCP_MsgTypes.WCP_MsgEndSession:
            {
              WCP_MsgEndSession request;
              WCP_MsgEndSessionReply response;
              Int64 card_id;
              Decimal received_card_balance;
              Decimal card_balance;
              Terminal terminal;
              PlaySession _ps;
              WCP_AccountManager.WCP_Account _account;
              Boolean _big_mismatch;
              MultiPromos.EndSessionInput _end_input;
              MultiPromos.EndSessionOutput _end_output;
              StringBuilder _msg;
              String _source_name;
              WSI.Common.Terminal.TerminalInfo _terminal_info;
              CardData.PlaySession _ps_info;
              Currency _closed_initial;
              Currency _reported_final;
              Currency _reported_played;
              Currency _reported_won;
              Currency _reported_initial;
              Currency _diference_initial;
              Currency _diference_played;
              Currency _diference_won;
              Int64 _diference_played_count;
              Int64 _diference_won_count;
              Currency _diference_final;
              String _message;
              Boolean _register;

              // TODO: Check Version.
              ////String _protocol_major_version_str;
              ////Int32 _protocol_major_version;

              _big_mismatch = false;

              request = (WCP_MsgEndSession)_wcp_request.MsgContent;
              response = (WCP_MsgEndSessionReply)_wcp_response.MsgContent;

              //
              // TODO: Check Version.
              //
              ////_protocol_major_version_str = wcp_request.MsgHeader.GetProtocolVersion.Substring(0, wcp_request.MsgHeader.GetProtocolVersion.IndexOf("."));
              ////if (!Int32.TryParse(_protocol_major_version_str, out _protocol_major_version))
              ////{
              ////  _protocol_major_version = 0;
              ////}

              ////if (_protocol_major_version < 2)
              ////{
              ////  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "Protocol major version is incorrect.");
              ////}

              terminal = Terminal.GetTerminal(_tcp_client.InternalId, _sql_trx);

              // RCI 09-JUL-2013: If terminal_id == 0, a DB error has occurred.
              if (terminal.terminal_id == 0)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE,
                                        "Internal error reading Terminal. TerminalId " + _tcp_client.InternalId + ".");
              }

              // Check Session Data
              if (request.CardSessionId == 0
                  || terminal.play_session_id != request.CardSessionId)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_VALID, "Player Account Does Not Exist.");
              }

              // RCI & AJQ 19-MAY-2011: We are sure the PlaySessionId is in Cache and Active (previously checked).
              //                        Reload the PlaySession from DB and save it in the Cache.
              _ps = PlaySession.Reload(_tcp_client.InternalId, request.CardSessionId, _sql_trx);

              if (_ps == null)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE,
                  "Internal Error reloading the PlaySession " + request.CardSessionId +
                                        " for the TerminalId " + _tcp_client.InternalId + ".");
              }

              // RCI 30-JUL-2012: Check if the play_session is still opened. if not, finish the play session now.
              if (_ps.Status != PlaySessionStatus.Opened)
              {
                // RBG 20-MAR-2013: Check if the play_session was manually closed. if yes, register an alarm.

                // Get Terminal ang play session information
                if (!WSI.Common.Terminal.Trx_GetTerminalInfo(terminal.terminal_id, out _terminal_info, _sql_trx))
                {
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE,
                    "Internal Error reading information about terminal with id " + terminal.terminal_id +
                                                      " with PlaySession " + request.CardSessionId + ".");
                }

                _ps_info = CardData.PlaySession.Read(request.CardSessionId, _sql_trx);
                _source_name = Misc.ConcatProviderAndTerminal(_terminal_info.ProviderName, _terminal_info.Name);

                // Read Account
                _account = WCP_AccountManager.Account(_ps.AccountId, _sql_trx);
                if (_account == null)
                {
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID,
                    "Internal Error reading information about account with id " + _ps.AccountId +
                                                 " in the PlaySession " + request.CardSessionId +
                                        " in the terminal with id " + terminal.terminal_id + ".");
                }
                // RCI 09-JUL-2013: If _account.id == 0, a DB error has occurred.
                if (_account.id == 0)
                {
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE, "Internal Error reading information about account with id " + _ps.AccountId + ".");
                }

                // Calculate closed values
                _closed_initial = _ps_info.InitialBalance + _ps_info.CashIn;

                // Calculate reported values
                _reported_final = (Decimal)request.CardBalance / 100m;
                _reported_played = (Decimal)request.PlayedCents / 100m;
                _reported_won = (Decimal)request.WonCents / 100m;
                _reported_initial = _reported_final + _reported_played - _reported_won;

                // Calculate diference between Reported data and Closing data: diference = reported - closing.
                _diference_initial = _reported_initial - _closed_initial;
                _diference_played = _reported_played - _ps_info.PlayedAmount;
                _diference_won = _reported_won - _ps_info.WonAmount;
                _diference_played_count = request.PlayedCount - (Int64)_ps_info.PlayedCount;
                _diference_won_count = request.WonCount - (Int64)_ps_info.WonCount;
                _diference_final = _reported_final - _ps_info.FinalBalance;

                // Register alarm info:
                /*
                         Terminal: {0}
                         Account: {1}
                         Session start: {2}
                         Session end: {3}
                         Reported data: Initial {10}, Played {11}, Won {12}, Final {13}, No. Plays {14}, No. Wins {15}
                         Closing data: Initial {4}, Played {5}, Won {6}, Final {7}, No.Plays {8}, No.Wins {9}
                         Diference: Initial {16}, Played {17}, Won {18}, Final {19}, No.Plays {20}, No.Wins {21}
                 */

                _message = String.Format(Resource.String("STR_EA_GAMING_SESSION_DATA"),
                                       _terminal_info.TerminalId.ToString() + " - " + _terminal_info.Name,
                                       _account.id.ToString() + " - " + _account.display_holder_name,
                                       Format.CustomFormatDateTime(_ps_info.Started, true),
                                       Format.CustomFormatDateTime(_ps_info.LastActivity, true),
                                       _closed_initial.ToString(),
                                       _ps_info.PlayedAmount.ToString(),
                                       _ps_info.WonAmount.ToString(),
                                       _ps_info.FinalBalance.ToString(),
                                       _ps_info.PlayedCount.ToString(),
                                       _ps_info.WonCount.ToString(),
                                       _reported_initial.ToString(),
                                       _reported_played.ToString(),
                                       _reported_won.ToString(),
                                       _reported_final.ToString(),
                                       request.PlayedCount.ToString(),
                                       request.WonCount.ToString(),
                                       _diference_initial.ToString(),
                                       _diference_played.ToString(),
                                       _diference_won.ToString(),
                                       _diference_final.ToString(),
                                       _diference_played_count.ToString(),
                                       _diference_won_count.ToString()
                                      );

                // Register alarm if session has been manually closed
                if (_ps.Status == PlaySessionStatus.ManualClosed)
                {
                  // Concatenate de message title 
                  _message = String.Format(Resource.String("STR_EA_0x00020007"), _diference_final.ToString()) + Environment.NewLine + _message;

                  using (DB_TRX _db_trx = new DB_TRX())
                  {
                    _register = Alarm.Register(AlarmSourceCode.TerminalSystem,
                                 terminal.terminal_id,
                                 _source_name,
                                 (UInt32)AlarmCode.TerminalSystem_PlaySessionWasManuallyClosed,
                                 _message,
                                 (_diference_final == 0) ? AlarmSeverity.Info : AlarmSeverity.Warning,
                                 DateTime.MinValue,
                                 _db_trx.SqlTransaction
                                 );
                    if (_register)
                    {
                      _db_trx.Commit();
                    }
                  }
                }
                else
                {
                  if (_diference_final != 0)
                  {
                    // Concatenate de message title 
                    _message = String.Format(Resource.String("STR_EA_GAMING_SESSION_ALREADY_CLOSED"), _diference_final.ToString()) + Environment.NewLine + _message;

                    Log.Warning(_message);
                  }
                }

                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_SESSION_NOT_VALID,
                                        String.Format("PlaySessionId: {0} is not opened. Status: {1}.", request.CardSessionId.ToString(), _ps.Status.ToString()));
              }

              // Set values
              card_id = _ps.AccountId;
              card_balance = terminal.balance;
              received_card_balance = ((Decimal)request.CardBalance) / 100;

              // TODO ANDREU
              _end_input = request.EndSessionInput();
              _end_input.AccountId = _ps.AccountId;

              _sql_trx.Save("Trx_OnEndCardSession");

              _msg = new StringBuilder();
              _msg.AppendLine(String.Format("WCP_MsgEndSession -> TeminalId={0}, Type={1}, Name={2}, PlaySessionId={3}, AccountId={4}",
                              _tcp_client.InternalId, _tcp_client.TerminalType.ToString(), terminal.name, _end_input.PlaySessionId.ToString(), _end_input.AccountId.ToString()));

              _end_session_ok = MultiPromos.Trx_OnEndCardSession(_tcp_client.InternalId, _tcp_client.TerminalType, terminal.name,
                                                                 _end_input, out _end_output, _sql_trx);

              if (!String.IsNullOrEmpty(_end_output.Message))
              {
                _msg.AppendLine(_end_output.Message);
              }

              if (!_end_session_ok)
              {
                switch (_end_output.StatusCode)
                {
                  case MultiPromos.EndSessionStatus.Ok:
                  case MultiPromos.EndSessionStatus.BalanceMismatch:
                    _msg.AppendLine("Unexpected StatusCode: " + _end_output.StatusCode.ToString());
                    Log.Error(_msg.ToString());
                    throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, _end_output.ErrorMsg.ToString());

                  case MultiPromos.EndSessionStatus.ErrorRetry:
                    _sql_trx.Rollback("Trx_OnEndCardSession");
                    Log.Error(_msg.ToString());
                    throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE, _end_output.ErrorMsg.ToString());

                  case MultiPromos.EndSessionStatus.FatalError:
                  case MultiPromos.EndSessionStatus.NotOpened:
                  default:
                    _sql_trx.Rollback("Trx_OnEndCardSession");
                    Log.Error(_msg.ToString());
                    throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, _end_output.ErrorMsg.ToString());
                }
              }

              switch (_end_output.StatusCode)
              {
                case MultiPromos.EndSessionStatus.Ok:
                  if (!String.IsNullOrEmpty(_end_output.Message))
                  {
                    Log.Warning(_msg.ToString());
                  }
                  break;

                case MultiPromos.EndSessionStatus.BalanceMismatch:
                  Log.Warning(_msg.ToString());
                  break;

                default:
                  _msg.AppendLine("Unexpected StatusCode: " + _end_output.StatusCode.ToString());
                  Log.Error(_msg.ToString());
                  throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, _end_output.ErrorMsg.ToString());
              }

              response.CardBalance = (Int64)(_end_output.PlaySessionFinalBalance.TotalBalance * 100);

              // Read Account
              _account = WCP_AccountManager.Account(card_id, _sql_trx);
              if (_account == null)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
              }

              // RCI 09-JUL-2013: If _account.id == 0, a DB error has occurred.
              if (_account.id == 0)
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE);
              }

              response.HolderName = _account.display_holder_name;
              response.Points = (Int32)_account.points;

              // ACC 02-MAY-2013 ELP & Requests to MultiSite in Start Card Session trx.
              if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
              {
                response.Points = 0;
              }

              // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
              response.Balance = _end_output.PlaySessionFinalBalance;

              if (!_big_mismatch)
              {
                switch (terminal.play_session_type)
                {
                  case PlaySessionType.WIN:
                    break;

                  case PlaySessionType.ALESIS:
                    {
                      AleTerminal ale_terminal;
                      Alesis.StatusCode status_code;

                      terminal.balance = received_card_balance;
                      ale_terminal = (AleTerminal)terminal;
                      status_code = Alesis.EndCardSession(ale_terminal, _sql_trx);

                      switch (status_code)
                      {
                        case Alesis.StatusCode.Success:
                          break;

                        case Alesis.StatusCode.AccountNumberNotValid:
                        case Alesis.StatusCode.MachineNumberNotValid:
                        case Alesis.StatusCode.SessionNumberNotValid:
                        case Alesis.StatusCode.AccessDenied:
                          Log.Error("EndCardSession SessionID=" + ale_terminal.ale_session_id + " CardTrackData=" + ale_terminal.card_track_data + " Error:" + status_code.ToString());
                          break;

                        case Alesis.StatusCode.NotConnectedToDatabase:
                          Log.Error("EndCardSession SessionID=" + ale_terminal.ale_session_id + " CardTrackData=" + ale_terminal.card_track_data + " Error:" + status_code.ToString());
                          break;

                        default:
                          Log.Error("EndCardSession SessionID=" + ale_terminal.ale_session_id + " CardTrackData=" + ale_terminal.card_track_data + " Error: Unexpected status=" + status_code.ToString());
                          break;
                      } // switch (status_code)
                    }
                    break;

                  default:
                    throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED, "Invalid card type.");
                  //break;
                }

                if (_wcp_request.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_SESSION_TIMEOUT)
                {
                  WCP_Operation _operation;
                  UInt32 _alarm_code;
                  String _description;

                  //
                  // Insert Event "Card Abandoned"
                  //
                  _operation = new WCP_Operation();
                  _operation.Code = OperationCodes.WCP_OPERATION_CODE_CARD_ABANDONED;
                  _operation.Data = (UInt32)card_id;
                  _operation.LocalTime = WGDB.Now;

                  // Insert into database
                  WCP_BusinessLogic.DB_NewInsertEvent(_tcp_client.InternalId,
                                                      _response_session_id,
                                                      WCP_EventTypesCodes.WCP_EV_OPERATION,
                                                      null,
                                                      _operation,
                                                      _sql_trx);

                  // Insert Alarm
                  // Get Terminal ang play session information
                  if (!WSI.Common.Terminal.Trx_GetTerminalInfo(terminal.terminal_id, out _terminal_info, _sql_trx))
                  {
                    throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE,
                      "Internal Error reading information about terminal with id " + terminal.terminal_id +
                                                        " with PlaySession " + request.CardSessionId + ".");
                  }

                  _source_name = Misc.ConcatProviderAndTerminal(_terminal_info.ProviderName, _terminal_info.Name);
                  _alarm_code = 0x00060000 + ((UInt32)(((Int32)OperationCodes.WCP_OPERATION_CODE_CARD_ABANDONED) & 0x0000FFFF));

                  _description = Cashier.EvOpGetDescription(Cashier.EnumEventType.WCP_EVENT_TYPE_OPERATION,
                                                            Cashier.EnumDeviceCode.COMMON_DEVICE_CODE_NO_DEVICE,
                                                            Cashier.EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN,
                                                            Cashier.EnumEventSeverity.COMMON_SEVERITY_OK,
                                                            Cashier.EnumOperationCode.COMMON_OPERATION_CODE_CARD_ABANDONED,
                                                            card_id,
                                                            0,
                                                            0);

                  Alarm.Register(AlarmSourceCode.TerminalSASMachine, terminal.terminal_id, _source_name, _alarm_code, _description,
                                 AlarmSeverity.Warning, _operation.LocalTime, _sql_trx);

                  //
                  // Block Account
                  //
                  WCP_BusinessLogic.DB_SetAccountBlocked(card_id, AccountBlockReason.ABANDONED_CARD, _sql_trx);
                }
              } // if (!_big_mismatch)
            }
            break;

          case WCP_MsgTypes.WCP_MsgReportEventList:
            {
              Terminal terminal;
              WCP_MsgReportEventList request;
              WCP_DeviceStatus device_status;
              WCP_Operation operation;


              terminal = Terminal.GetTerminal(_tcp_client.InternalId, _sql_trx);

              request = (WCP_MsgReportEventList)_wcp_request.MsgContent;

              // Device List
              for (int idx_device_status = 0; idx_device_status < request.NumDeviceStatus; idx_device_status++)
              {
                device_status = (WCP_DeviceStatus)request.DeviceStatusList[idx_device_status];

                // Insert into database
                WCP_BusinessLogic.DB_NewInsertEvent(_tcp_client.InternalId,
                                                    _response_session_id,
                                                    WCP_EventTypesCodes.WCP_EV_DEVICE_STATUS,
                                                    device_status,
                                                    null,
                                                    _sql_trx);
              }

              // Operations
              for (int idx_operation = 0; idx_operation < request.NumOperations; idx_operation++)
              {
                operation = (WCP_Operation)request.OperationList[idx_operation];

                // Insert into database
                WCP_BusinessLogic.DB_NewInsertEvent(_tcp_client.InternalId,
                                                    _response_session_id,
                                                    WCP_EventTypesCodes.WCP_EV_OPERATION,
                                                    null,
                                                    operation,
                                                    _sql_trx);

                if (terminal == null)
                {
                  continue;
                }

                if (terminal.play_session_type == PlaySessionType.ALESIS)
                {
                  AleTerminal ale_terminal;
                  Int32 event_id;
                  Decimal prize_amount;
                  Boolean call_to_alesis;
                  Alesis.StatusCode status_code;

                  ale_terminal = (AleTerminal)terminal;

                  call_to_alesis = false;
                  event_id = 0;
                  prize_amount = 0;

                  switch ((OperationCodes)operation.Code)
                  {
                    case OperationCodes.WCP_OPERATION_CODE_JACKPOT_WON:
                      call_to_alesis = true;
                      event_id = 1;
                      prize_amount = ((Decimal)operation.Data) / 100;
                      break;

                    case OperationCodes.WCP_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE:
                      call_to_alesis = true;
                      event_id = 2;
                      prize_amount = ((Decimal)operation.Data) / 100;
                      break;

                    case OperationCodes.WCP_OPERATION_CODE_CALL_ATTENDANT:
                      call_to_alesis = true;
                      event_id = 3;
                      break;

                    default:
                      break;
                  }

                  if (terminal.play_session_id == 0)
                  {
                    call_to_alesis = false;
                  }

                  if (call_to_alesis)
                  {
                    // Call to alesis
                    status_code = Alesis.ReportEvent((AleTerminal)terminal, event_id, prize_amount, _sql_trx);

                    switch (status_code)
                    {
                      case Alesis.StatusCode.Success:
                        // Nothing
                        break;

                      case Alesis.StatusCode.AccountNumberNotValid:
                      case Alesis.StatusCode.MachineNumberNotValid:
                      case Alesis.StatusCode.SessionNumberNotValid:
                      case Alesis.StatusCode.AccessDenied:
                        Log.Error("ReportEvent SessionID=" + ale_terminal.ale_session_id + " Error:" + status_code.ToString());
                        break;

                      case Alesis.StatusCode.NotConnectedToDatabase:
                        Log.Error("ReportEvent SessionID=" + ale_terminal.ale_session_id + " Error:" + status_code.ToString());
                        break;

                      default:
                        Log.Error("ReportEvent SessionID=" + ale_terminal.ale_session_id + " Error:" + status_code.ToString());
                        break;

                    } // switch

                  } // if (call_to_alesis)
                }

              } // for idx_operation
            }
            break;

          case WCP_MsgTypes.WCP_MsgLockCard:
            {
              WCP_MsgLockCard request;
              WCP_MsgLockCardReply response;
              int _lock_timeout;
              DateTime lock_date_time;

              lock_date_time = new DateTime();
              lock_date_time = DateTime.Now;

              request = (WCP_MsgLockCard)_wcp_request.MsgContent;
              response = (WCP_MsgLockCardReply)_wcp_response.MsgContent;


              _lock_timeout = 0;
              response.LockCardTimeout = _lock_timeout;

              if (!WCP_BusinessLogic.DB_LockPlaySession(request.CardSessionId, _sql_trx))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "The session can not be locked.");
              }


              if (!WCP_BusinessLogic.Db_GetLockTimeOut(out _lock_timeout, _sql_trx))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Error getting general params ");
              }

              response.LockCardTimeout = _lock_timeout;

            }
            break;

          case WCP_MsgTypes.WCP_MsgPlayerCheckPin:
            {
              WCP_ProcessPlayerCheckPin(_wcp_request, _wcp_response);
            }
            break;

          case WCP_MsgTypes.WCP_MsgGetProtocolParameters:
            {
              WCP_ProcessProtocolParameters(_wcp_request, _wcp_response, _tcp_client.InternalId, _sql_trx);
            }
            break;

          case WCP_MsgTypes.WCP_LCD_MsgGetParameters:
            {
              WCP_LCD_MsgGetParameters _request;
              WCP_LCD_MsgGetParametersReply _response;

              _request = (WCP_LCD_MsgGetParameters)_wcp_request.MsgContent;
              _response = (WCP_LCD_MsgGetParametersReply)_wcp_response.MsgContent;
            }
            break;

          case WCP_MsgTypes.WCP_MsgTerminalDrawGetPendingDraw:
            {
              WCP_MsgTerminalDrawGetPendingDraw _request;
              WCP_MsgTerminalDrawGetPendingDrawReply _response;

              _request = (WCP_MsgTerminalDrawGetPendingDraw)_wcp_request.MsgContent;
              _response = (WCP_MsgTerminalDrawGetPendingDrawReply)_wcp_response.MsgContent;

              _response.PendingDraw = ProcessTerminalDrawGetPendingDraw(_request, ref _response, _tcp_client.InternalId, _sql_trx);
            }
            break;

          case WCP_MsgTypes.WCP_MsgTerminalDrawProcessDraw:
            {
              WCP_MsgTerminalDrawProcessDraw _request;
              WCP_MsgTerminalDrawProcessDrawReply _response;

              _request = (WCP_MsgTerminalDrawProcessDraw)_wcp_request.MsgContent;
              _response = (WCP_MsgTerminalDrawProcessDrawReply)_wcp_response.MsgContent;

              _response.PrizePlanGames = ProcessTerminalDrawProcessDraw(_request, ref _response, _tcp_client.InternalId, _sql_trx);              
            }
            break;



          case WCP_MsgTypes.WCP_MsgTerminalDrawGameTimeElapsed:
            {
              WCP_MsgTerminalDrawGameTimeElapsed _request;
              WCP_MsgTerminalDrawGameTimeElapsedReply _response;

              _request = (WCP_MsgTerminalDrawGameTimeElapsed)_wcp_request.MsgContent;
              _response = (WCP_MsgTerminalDrawGameTimeElapsedReply)_wcp_response.MsgContent;

              if (!TerminalDraw.ProcessTerminalDrawEnd(_request.PlaySessionId, _request.GameTimeElapsed, _request.ClosePlaySession, _sql_trx))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, " ");
              }
            }
            break;

          case WCP_MsgTypes.WCP_MsgReserveTerminal:
            {
              WCP_MsgReserveTerminal _request;
              WCP_MsgReserveTerminalReply _response;
              WSI.Common.Terminal.TerminalInfo _terminal_info;
              TerminalReservation.ReserveTerminalResultData _reserve_error;
              WCP_AccountManager.WCP_Account _account;
              Int32 _expiration_minutes;

              _request = (WCP_MsgReserveTerminal)_wcp_request.MsgContent;
              _response = (WCP_MsgReserveTerminalReply)_wcp_response.MsgContent;

              _response.ReserveError = (Int32)TerminalReservation.ReserveTerminalResult.UnknownError;
              _response.ReserveErrorMsg = "";
              _response.ExpirationMinutes = 0;

              WSI.Common.Terminal.Trx_GetTerminalInfoByExternalId(_wcp_request.MsgHeader.TerminalId, out _terminal_info, _sql_trx);

              // Read Account
              _account = WCP_AccountManager.Account(_request.TrackData, _sql_trx);

              if (_account == null)
              {
                Log.Error(String.Format("Terminal.Reserve() [TerminalId: {0}]: No account related to the track data {1}", _terminal_info.TerminalId, _request.TrackData));

                _response.ReserveError = (Int32)TerminalReservation.ReserveTerminalResult.NoTrackDataAccount;
                _response.ReserveErrorMsg = TerminalReservation.GetErrorMessage(TerminalReservation.ReserveTerminalResult.NoTrackDataAccount);

                break;
              }

              if (!TerminalReservation.Reserve(_terminal_info.TerminalId, _account.id,_account.level, out _reserve_error, out _expiration_minutes, _sql_trx))
              {
                Log.Error(String.Format("Terminal.Reserve() failed: TerminalId: {0}; AccountId: {1} ", _terminal_info.TerminalId, _account.id));

                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, " ");
              }

              _response.ReserveError = (Int32)_reserve_error.ErrorCode;
              _response.ReserveErrorMsg = TerminalReservation.GetErrorMessage(_reserve_error);
              _response.ExpirationMinutes = _expiration_minutes;
            }
            break;

          case WCP_MsgTypes.WCP_MsgMobiBankRecharge:
            {
              WCP_MsgMobiBankRecharge _request;
              WCP_MsgMobiBankRechargeReply _response;
              WSI.Common.MobiBank.MobiBankRecharge _mobibank_recharge;

              _mobibank_recharge = new WSI.Common.MobiBank.MobiBankRecharge();

              _request = (WCP_MsgMobiBankRecharge)_wcp_request.MsgContent;
              _response = (WCP_MsgMobiBankRechargeReply)_wcp_response.MsgContent;

              if (!_mobibank_recharge.DB_MobiBankInsertRecharge(_tcp_client.InternalId, _request.AccountId, _sql_trx))
              {
                Log.Error(String.Format("WCP_MsgMobiBankRecharge: MobiBank Recharge() failed: TerminalId: {0}; AccountId: {1} ", _wcp_request.MsgHeader.TerminalId, _request.AccountId));

                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, " ");
              }
            }
            break;

          case WCP_MsgTypes.WCP_MsgMobiBankCancelRequest:
            {
              WCP_MsgMobiBankCancelRequest _request;
              WCP_MsgMobiBankCancelRequestReply _response;
              WSI.Common.MobiBank.MobiBankRecharge _mobibank_recharge;

              _mobibank_recharge = new WSI.Common.MobiBank.MobiBankRecharge();

              _request = (WCP_MsgMobiBankCancelRequest)_wcp_request.MsgContent;
              _response = (WCP_MsgMobiBankCancelRequestReply)_wcp_response.MsgContent;

              if (!_mobibank_recharge.DB_MobiBankCancelRecharges(_request.AccountId, (MobileBankStatusCancelReason)_request.CancelReason, _sql_trx))
              {
                Log.Error(String.Format("WCP_MsgMobiBankCancelRequest: MobiBank Recharge() failed: TerminalId: {0}; AccountId: {1} ", _tcp_client.InternalId, _request.AccountId));

                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, " ");
              }
            }
            break;

          case WCP_MsgTypes.WCP_MsgMobiBankNotifyRechargeEGM:
            {
              WCP_MsgMobiBankNotifyRechargeEGM _request;
              WCP_MsgMobiBankNotifyRechargeEGMReply _response;

              _request = (WCP_MsgMobiBankNotifyRechargeEGM)_wcp_request.MsgContent;
              _response = (WCP_MsgMobiBankNotifyRechargeEGMReply)_wcp_response.MsgContent;

              WSI.Common.MobiBank.MobiBankRecharge _mobibank_recharge;

              _mobibank_recharge = new WSI.Common.MobiBank.MobiBankRecharge();

              if (!_mobibank_recharge.NotifyRechargeAndUpdateRequestStatus(_request.RequestId, _request.Status, _sql_trx))
              {
                Log.Error(String.Format("NotifyRechargeAndUpdateRequestStatus: MobiBank Notification EGM failed: TerminalId: {0}; RequestId: {1} ", _tcp_client.InternalId, _request.RequestId));

                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, " ");
              }
              
            }
            break;

          default:
            break;

        }

        String str_response;

        str_response = _wcp_response.ToXml();

        _interval_commit = 0;
        _interval_sent = 0;
        _interval_all_process = 0;
        _tick0 = Environment.TickCount;

        try
        {
          bool _rollback;
          long _interval;

          _rollback = false;
          _interval = Task.ElapsedTicks;
          if (_interval >= 25000
              || !_tcp_server.IsConnected(_tcp_client))
          {
            _rollback = true;
          }

          if (_rollback)
          {
            // Disconnected: Rollback and don't send the response
            _sql_trx.Rollback();
            // Ignore the task     
            Log.Warning("Work rolled-back: disconnected. Process time: " + _interval.ToString() + " ms. " + "\n - - - - - ---> " + Task.XmlRequest + "\n - - - - - <--- " + str_response);
          }
          else
          {
            bool _sent;

            _tick1 = Environment.TickCount;
            _sql_trx.Commit();
            _tick2 = Environment.TickCount;
            _interval_commit = Misc.GetElapsedTicks(_tick1, _tick2);

#if !SQL_BUSINESS_LOGIC
            if ((_wcp_request.MsgHeader.MsgType == WCP_MsgTypes.WCP_MsgEndSession) && (_end_session_ok))
            {
              BucketsUpdate.SetDataAvailable(); /* wakes up the thread*/
            }
#endif

            // After Commit, update changes in memory.
            switch (_wcp_request.MsgHeader.MsgType)
            {
              case WCP_MsgTypes.WCP_MsgEndSession:
                {
                  WCP_MsgEndSession request;

                  request = (WCP_MsgEndSession)_wcp_request.MsgContent;

                  // RCI & AJQ 19-MAY-2011: Unload play_session from Cache after Commit.
                  PlaySession.Unload(request.CardSessionId);
                }
                break;

              default:
                break;
            }



            _tick1 = Environment.TickCount;
            _sent = _tcp_client.Send(str_response);
            _tick2 = Environment.TickCount;
            _interval_sent = Misc.GetElapsedTicks(_tick1, _tick2);

            if (!_sent)
            {
              // Disconnected and work commited!!!
              Log.Warning("Work commited: disconnected. " + "\n - - - - - ---> " + Task.XmlRequest + "\n - - - - - <--- " + str_response);
            }
            else
            {
              NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, str_response);
            }

            _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);

            if (_interval_commit > 3000
                || _interval_sent > 3000
                || _interval_all_process > 10000)
            {
              Log.Warning("End Process time: Commit/Sent/Total = " + _interval_commit.ToString() + "/" +
                                                                     _interval_sent.ToString() + "/" +
                                                                     _interval_all_process.ToString() + Task.XmlRequest);
            }

          }
          _error = false;
        }
        catch
        {
        }
      }
      catch (WCP_Exception wcp_ex)
      {
        String wcp_error_response;

        wcp_error_response = null;
        try
        {
          if (_wcp_response == null)
          {
            _wcp_response = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgError);
          }
          _wcp_response.MsgHeader.ResponseCode = wcp_ex.ResponseCode;
          _wcp_response.MsgHeader.ResponseCodeText = wcp_ex.ResponseCodeText;

          // Create error response
          wcp_error_response = _wcp_response.ToXml();

          _tcp_client.Send(wcp_error_response);
          NetLog.Add(NetEvent.MessageSent, _tcp_client.Identity, wcp_error_response);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch (Exception ex)
      {
        NetLog.Add(NetEvent.MessageFormatError, _tcp_client.Identity + " a.1)", ex.Message);
        NetLog.Add(NetEvent.MessageFormatError, _tcp_client.Identity + " b.1)", Task.XmlRequest);

        Log.Exception(ex);

        return;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try
            {
              _sql_trx.Rollback();
            }
            catch (Exception _ex)
            {
              Log.Exception(_ex);
            }
            if (!_error)
            {
              Log.Message("*** ERROR DB TRX ROLLBACK on MessageType: " + Task.Request.MsgHeader.MsgType.ToString());
              Log.Message("*** Received Message: " + Task.XmlRequest);
            }
          }
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try
          {
            _sql_conn.Close();
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
          _sql_conn = null;
        }
      }
    }

    /// <summary>
    /// Process terminal draw get pending draw
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="InternalId"></param>
    /// <param name="SqlTrx"></param>
    private static TerminalDrawAccountEntity ProcessTerminalDrawGetPendingDraw(WCP_MsgTerminalDrawGetPendingDraw Request, ref WCP_MsgTerminalDrawGetPendingDrawReply Response, int InternalId, SqlTransaction SqlTrx)
    {
      TerminalDrawAccountEntity _account_entity;

      if (!TerminalDraw.TerminalDrawGetPendingDraw(Request.AccountId, InternalId, out _account_entity, SqlTrx))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, " ");
      }

      return _account_entity;
    } // ProcessTerminalDrawGetPendingDraw

    /// <summary>
    /// Process terminal draw get pending draw
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="InternalId"></param>
    /// <param name="SqlTrx"></param>
    private static TerminalDrawPrizePlanGames ProcessTerminalDrawProcessDraw(WCP_MsgTerminalDrawProcessDraw Request, ref WCP_MsgTerminalDrawProcessDrawReply Response, int InternalId, SqlTransaction SqlTrx)
    {
      TerminalDrawPrizePlanGames _prize_plan_games;

      if (!TerminalDraw.TerminalDrawProcessDraw(InternalId, Request.ProcessPendingDraw, out _prize_plan_games, SqlTrx))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, " ");
      }

      return _prize_plan_games;
    } // ProcessTerminalDrawGetPendingDraw
  }
}
