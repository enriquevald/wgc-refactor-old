//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//   MODULE NAME: WCP_PlaySessionLogic.cs
//   DESCRIPTION: WCP_PlaySessionLogic class; process of play session meters
// 
//        AUTHOR: Nelson Madrigal Reyes
// 
// CREATION DATE: 19-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-NOV-2013 NMR    First release.
// 28-NOV-2013 NMR    Don't actualize play-session totals
// 03-DIC-2013 NMR    Improvement to load more optional values
// 04-DIC-2013 NMR    Controlling overflow in TE_SEQUENCE_ID
// 18-DIC-2013 JRM    Re-made entire sessions objects acquirement process because old one was too slow
// 07-JAN-2014 DHA    Fixed Bug WIGOSTITO-948: In case that there was not Stacker, the cashier session was created with the 'Super User' user.
// 07-JAN-2014 JRM    Added "Provider_id" to tito session objects method
// 25-APR-2014 DHA & JML Rename/Added TITO Cashier/Account Movements
// 04-SEP-2015 YNM    TFS-4096: Cashier: screen last inserted bills and tickets
// 30-SEP-2015 MPO    Bug 4836 - Bug 4800 - TITO - WCP: It needed the gaming day in cashier session
// 19-JAN-2016 DHA    Task 8521:Floor Dual Currency: convert Max ticket out GP to terminal ISO Code
// 28-JAN-2016 DHA    Product Backlog Item 8351:Floor Dual Currency: Movimientos de tickets TITO en M.N.
// 30-NOV-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
// 21-JUL-2017 FJC    Fixed Bug WIGOS-3888 Accounting - TITO tickets associated with anonymous accounts when using players card.
//------------------------------------------------------------------------------

using System;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using WSI.Common.TITO;
using System.Data;
using System.Collections;

namespace WSI.WCP
{
  /// NMR-TODO: temporary extension of class; will be integrated
  public class SessionTerminal : Terminal
  {
    public Int32 machine_id;
    public Int64 sequence_id;

    // external properties; internall uses
    public Int64 AccountId;
    public Int64 PlaySessionId;     // external property
    public Int64 CashierSessionId;  // external property
  }

  public class WCP_PlaySessionLogic
  {
    #region Global Vars

    // design pattern singleton
    static public WCP_PlaySessionLogic Instance = new WCP_PlaySessionLogic();

    // temporary vars to accelerate some process
    protected WCP_AccountManager.WCP_Account m_account_tmp;
    protected Ticket m_tito_ticket_tmp;

    #endregion

    public WCP_PlaySessionLogic()
    {
      m_account_tmp = new WCP_AccountManager.WCP_Account();
      m_tito_ticket_tmp = new Ticket();
    }

    #region Private/Protected Methods

    ////------------------------------------------------------------------------------
    //// PURPOSE : Update/Reset Sequence ID in given terminal
    ////           OJO: method doesn't capture BD exceptions
    ////
    ////  PARAMS:
    ////      - INPUT:
    ////        - TerminalId
    ////        - SqlTransaction
    ////
    ////      - OUTPUT: 
    ////
    //// RETURNS:
    ////
    ////------------------------------------------------------------------------------

    //protected Boolean DB_UpdateTerminalSequenceId(Int32 TerminalId, Int64 NewSequenceId, SqlTransaction Trx)
    //{
    //  Boolean _result;
    //  StringBuilder _sb;

    //  _result = false;

    //  _sb = new StringBuilder();
    //  _sb.AppendLine(" UPDATE   TERMINALS ");
    //  _sb.AppendLine("    SET   TE_SEQUENCE_ID = @pNewSequenceId ");
    //  _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId ");

    //  using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
    //  {
    //    _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
    //    _cmd.Parameters.Add("@pNewSequenceId", SqlDbType.BigInt).Value = NewSequenceId;

    //    _result = _cmd.ExecuteNonQuery() == 1;
    //  }

    //  return _result;
    //} // DB_ResetTerminalSequenceId

    //------------------------------------------------------------------------------
    // PURPOSE : Get or create critical objects TITO Messages processing
    //         : Objects fetched or created are:
    //           - Play Session id
    //           - Virtual account id
    //           - terminal id
    //           - cashier_session_id
    //           - money_collection_id
    //           - cashier_terminal_id 
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId
    //        - CreatePlaySessionIfMissing
    //        - MBUserType
    //        - SqlTransaction
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //
    //------------------------------------------------------------------------------
    public static Boolean GetTitoSessionObjects(Int32 TerminalId,
                                                Int64 TransactionId,
                                                Boolean CreatePlaySessionIfMissing,
                                                MB_USER_TYPE MBUserType,
                                                out String TerminalName,
                                                out Int64 PlaySessionId,
                                                out Int64 CashierSessionId,
                                                out Int64 MoneyCollectionId,
                                                out Int64 VirtualAccountId,
                                                out String CardTrackData,
                                                out Int32 CashierTerminalId,
                                                out Int64 MobileBankId,
                                                out String MobileBankTrackData,
                                                out Int64 MobileBankSessionId,
                                                out String UserName,
                                                out Int32 UserId,
                                                out String ProviderId,
                                                out String CashierName,
                                                SqlTransaction Trx)
    {
      Int64 DummyPlayerAccountId; 

      return GetTitoSessionObjects(TerminalId, TransactionId, CreatePlaySessionIfMissing, MBUserType, 
                                   out TerminalName,
                                   out PlaySessionId,
                                   out CashierSessionId,
                                   out MoneyCollectionId,
                                   out VirtualAccountId,
                                   out CardTrackData,
                                   out CashierTerminalId,
                                   out MobileBankId,
                                   out MobileBankTrackData,
                                   out MobileBankSessionId,
                                   out UserName,
                                   out UserId,
                                   out ProviderId,
                                   out CashierName,
                                   out DummyPlayerAccountId,
                                   Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get or create critical objects TITO Messages processing
    //         : Objects fetched or created are:
    //           - Play Session id
    //           - Virtual account id
    //           - terminal id
    //           - cashier_session_id
    //           - money_collection_id
    //           - cashier_terminal_id 
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId
    //        - CreatePlaySessionIfMissing
    //        - MBUserType
    //        - SqlTransaction
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //
    //------------------------------------------------------------------------------
    public static Boolean GetTitoSessionObjects(Int32 TerminalId,
                                                Int64 TransactionId,
                                                Boolean CreatePlaySessionIfMissing,
                                                MB_USER_TYPE MBUserType,
                                                out String TerminalName,
                                                out Int64 PlaySessionId,
                                                out Int64 CashierSessionId,
                                                out Int64 MoneyCollectionId,
                                                out Int64 VirtualAccountId,
                                                out String CardTrackData,
                                                out Int32 CashierTerminalId,
                                                out Int64 MobileBankId,
                                                out String MobileBankTrackData,
                                                out Int64 MobileBankSessionId,
                                                out String UserName,
                                                out Int32 UserId,
                                                out String ProviderId,
                                                out String CashierName,
                                                out Int64 PlayerAccountId,
                                                SqlTransaction Trx)
    {
      StringBuilder _sb;
      MultiPromos.InSessionAction _action;
      TerminalTypes _terminal_type;
      Boolean _created_virtual_account;

      PlaySessionId = 0;
      CashierSessionId = 0;
      MoneyCollectionId = 0;
      TerminalName = String.Empty;
      VirtualAccountId = 0;
      CardTrackData = String.Empty;
      MobileBankId = 0;
      MobileBankTrackData = String.Empty;
      MobileBankSessionId = 0;
      CashierTerminalId = 0;
      UserName = String.Empty;
      UserId = 0;
      ProviderId = String.Empty;
      CashierName = String.Empty;
      PlayerAccountId = 0;

      _terminal_type = TerminalTypes.UNKNOWN;

      _sb = new StringBuilder();

      // JRM TODO: If there are more than one play session open for X or Y reason (debug mode issues) , should we pick top 1??
      // Select every and all Ids needed to perform Tito messages. If some are missing, they will be 0.
      _sb.AppendLine("  SELECT   TE_TERMINAL_ID                                                                                          ");
      _sb.AppendLine("         , ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)                                                                        ");
      _sb.AppendLine("         , ISNULL(PS_PLAY_SESSION_ID, 0)                                                                           ");
      _sb.AppendLine("         , ISNULL(MB_CASHIER_SESSION_ID, 0)                                                                        ");
      _sb.AppendLine("         , ISNULL(CT_CASHIER_ID, 0)                                                                                ");
      _sb.AppendLine("         , ISNULL(TE_NAME, '')                                                                                     ");
      _sb.AppendLine("         , ISNULL(MB_ACCOUNT_ID, 0)                                                                                ");
      _sb.AppendLine("         , ISNULL(MB_TRACK_DATA, '')                                                                               ");
      _sb.AppendLine("         , TE_TYPE                                                                                                 ");
      _sb.AppendLine("         , TE_PROVIDER_ID                                                                                          ");
      _sb.AppendLine("         , ISNULL(CT_NAME, '')                                                                                     ");
      _sb.AppendLine("         , AC_TRACK_DATA                                                                                           ");
      _sb.AppendLine("         , ISNULL(PS_ACCOUNT_ID, 0)                                                                                ");
      _sb.AppendLine("    FROM   TERMINALS                                                                                               ");
      _sb.AppendLine("    LEFT   JOIN PLAY_SESSIONS      ON PS_TERMINAL_ID = TE_TERMINAL_ID AND PS_STATUS             = @pStatusOpen     ");
      _sb.AppendLine("    LEFT   JOIN MOBILE_BANKS       ON MB_TERMINAL_ID = TE_TERMINAL_ID AND MB_ACCOUNT_TYPE       = @pMbAccountType  ");
      _sb.AppendLine("    LEFT   JOIN CASHIER_TERMINALS  ON CT_TERMINAL_ID = TE_TERMINAL_ID                                              ");
      _sb.AppendLine("    LEFT   JOIN ACCOUNTS           ON AC_ACCOUNT_ID  = TE_VIRTUAL_ACCOUNT_ID                                       ");
      _sb.AppendLine("   WHERE   TE_TERMINAL_ID                            = @pTerminalId                                                ");
      _sb.AppendLine("ORDER BY   PS_PLAY_SESSION_ID DESC                                                                                 ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pMbAccountType", SqlDbType.Int).Value = (Int32)MBUserType;
          _cmd.Parameters.Add("@pStatusOpen", SqlDbType.BigInt).Value = (Int32)PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pCashiserSessionOpen", SqlDbType.Int).Value = (Int32)CASHIER_SESSION_STATUS.OPEN;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            TerminalId = _reader.GetInt32(0);
            VirtualAccountId = _reader.GetInt64(1);
            PlaySessionId = _reader.GetInt64(2);
            MobileBankSessionId = _reader.GetInt64(3);
            CashierTerminalId = _reader.GetInt32(4);
            TerminalName = _reader.GetString(5);
            MobileBankId = _reader.GetInt64(6);
            MobileBankTrackData = _reader.GetString(7);
            _terminal_type = (TerminalTypes)_reader.GetInt32(8);
            ProviderId = _reader.GetString(9);
            CashierName = _reader.GetString(10);
            CardTrackData = _reader.GetString(11);
            PlayerAccountId = _reader.GetInt64(12);
          }
        }

        // If there is no accountId, create virtual TITO account
        if (VirtualAccountId == 0)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // AJQ 08-MAR-2014, Use a different TRX to create the account
            if (!WCP_TITO_CreateVirtualAccount(TerminalId, out VirtualAccountId, out _created_virtual_account, _db_trx.SqlTransaction))
            {
              return false;
            }

            _db_trx.Commit();
          }
        }
        if (VirtualAccountId == 0)
        {
          return false;
        }

        // Block the virtual account
        MultiPromos.AccountBalance _dummy;
        if (!MultiPromos.Trx_UpdateAccountBalance(VirtualAccountId, MultiPromos.AccountBalance.Zero, out _dummy, Trx))
        {
          return false;
        }

        if (CashierTerminalId == 0 || MobileBankId == 0)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // If there is no CashierTerminal for this terminal, create one
            if (CashierTerminalId == 0)
            {
              if (!WCP_TITO_CreateCashierTerminal(TerminalName, TerminalId, out CashierTerminalId, _db_trx.SqlTransaction))
              {
                return false;
              }
            }

            // If there is no MobileBank, create one
            if (MobileBankId == 0)
            {
              if (!WCP_TITO_CreateMobileBank(TerminalId, out MobileBankId, out MobileBankSessionId, out MobileBankTrackData, _db_trx.SqlTransaction))
              {
                return false;
              }
            }

            _db_trx.Commit();
          }
        }

        if (CashierTerminalId == 0 || MobileBankId == 0)
        {
          return false;
        }

        //If User System doesn't exist, error
        if (!Cashier.GetSystemUser(Cashier.GetGUIUserType(MBUserType), Trx, out UserId, out UserName))
        {
          Log.Error("GetOrOpenSystemCashierSession. Error getting the System User.");

          return false;
        }

        // If there is no CashierSession, create one
        if (!WCP_TITO_GetOrCreateCashierSessionAndMoneyCollection(CashierTerminalId, TerminalId, TerminalName, MobileBankId, UserId, UserName,
                                                                  out CashierSessionId, out MoneyCollectionId, Trx))
        {
          return false;
        }

        // if the MobileBank Session id is different from that of the CashierSessionId then we need to update MobileBank with the new one
        if (CashierSessionId != MobileBankSessionId)
        {
          if (!Cashier.UpdateMBOnOpenCashierSession(MobileBankId, MBUserType, MobileBankTrackData, CashierSessionId, Trx))
          {
            return false;
          }
        }

        // If there is no PlaySession, create one
        if (PlaySessionId == 0 && CreatePlaySessionIfMissing)
        {
          if (!MultiPromos.Trx_GetPlaySessionId(VirtualAccountId,
                                                TerminalId,
                                                _terminal_type,
                                                true,
                                                MultiPromos.StartSessionTransferMode.Default,
                                                MultiPromos.AccountBalance.Zero,
                                                out PlaySessionId,
                                                out _action,
                                                Trx))
          {
            Log.Warning("MultiPromos.Trx_GetPlaySessionId. Error or TimeOut.");

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetTitoSessionObjectIds


    //------------------------------------------------------------------------------
    // PURPOSE: Create Mobile Bank entry
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId Int32
    //          - Trx
    //
    //      - OUTPUT:
    //          - CashierSessionId
    //
    // RETURNS: 
    //        
    //   NOTES :
    public static Boolean WCP_TITO_CreateMobileBank(Int32 TerminalId,
                                                    out Int64 MobileAccountId,
                                                    out Int64 MobileBankSessionId,
                                                    out String MbTrackData,
                                                    SqlTransaction Trx)
    {
      MobileAccountId = 0;
      MobileBankSessionId = 0;
      MbTrackData = String.Empty;

      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine(" DECLARE @mb_account_id          AS BIGINT ");
        _sb.AppendLine(" DECLARE @track_data             AS NVARCHAR(20) ");
        _sb.AppendLine(" DECLARE @mb_cashier_session_id  AS BIGINT ");
        _sb.AppendLine();
        _sb.AppendLine(" SET @mb_account_id       = NULL");
        _sb.AppendLine(" SELECT   @mb_account_id  = MB_ACCOUNT_ID");
        _sb.AppendLine("        , @track_data     = MB_TRACK_DATA");
        _sb.AppendLine("   FROM   MOBILE_BANKS");
        _sb.AppendLine("  WHERE   MB_TERMINAL_ID  = @pTerminalId");
        _sb.AppendLine("    AND   MB_ACCOUNT_TYPE = @pMBAccountType");
        _sb.AppendLine();
        _sb.AppendLine(" IF (@mb_account_id IS NULL )");
        _sb.AppendLine(" BEGIN ");
        _sb.AppendLine("   INSERT INTO   MOBILE_BANKS ");
        _sb.AppendLine("               ( MB_BLOCKED ");
        _sb.AppendLine("               , MB_BALANCE ");
        _sb.AppendLine("               , MB_ACCOUNT_TYPE ");
        _sb.AppendLine("               , MB_PIN ");
        _sb.AppendLine("               , MB_HOLDER_NAME ");
        _sb.AppendLine("               , MB_TERMINAL_ID ");
        _sb.AppendLine("               ) ");
        _sb.AppendLine("        VALUES ( 0 ");
        _sb.AppendLine("               , 0 ");
        _sb.AppendLine("               , @pMBAccountType ");
        _sb.AppendLine("               , '0000'");
        _sb.AppendLine("               , 'SYS-TITO'");
        _sb.AppendLine("               , @pTerminalId ");
        _sb.AppendLine("               ) ");
        _sb.AppendLine();
        _sb.AppendLine("   SET @mb_account_id         = SCOPE_IDENTITY() ");
        _sb.AppendLine("   SET @track_data            = RIGHT('0000000000000' + CAST(@mb_account_id AS NVARCHAR), 13) ");
        _sb.AppendLine();
        _sb.AppendLine("   UPDATE   MOBILE_BANKS ");
        _sb.AppendLine("      SET   MB_TRACK_DATA = @track_data ");
        _sb.AppendLine("    WHERE   MB_ACCOUNT_ID = @mb_account_id ");
        _sb.AppendLine(" END ");

        _sb.AppendLine(" SELECT @mb_account_id, @track_data");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pMBAccountType", SqlDbType.Int).Value = (Int32)MB_USER_TYPE.SYS_TITO;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              MobileAccountId = _reader.GetInt64(0);
              MbTrackData = _reader.GetString(1);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // WCP_TITO_CreateMobileBank


    //------------------------------------------------------------------------------
    // PURPOSE: Get the current System cashier session if exists.
    //          If not exists, open a new system cashier session.
    //
    //  PARAMS:
    //      - INPUT:
    //          - GU_USER_TYPE UserType
    //          - String TerminalName
    //          - SqlTransaction Trx
    //
    //      - OUTPUT:
    //          - Int64 NewCashierSessionId 
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    //
    public static Boolean WCP_TITO_GetOrCreateCashierSessionAndMoneyCollection(Int32 CashierTerminalId,
                                                                               Int32 TerminalId,
                                                                               String TerminalName,
                                                                               Int64 MbAccountId,
                                                                               Int32 UserId,
                                                                               String UserName,
                                                                               out Int64 CashierSessionId,
                                                                               out Int64 MoneyCollectionId,
                                                                               SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _param_session_id;
      SqlParameter _param_money_collection_id;
      SqlParameter _param_session_created;
      String _name;
      Decimal _dec_value;
      CashierMovementsTable _cashier_mov_table;
      CashierSessionInfo _csi;
      Boolean _session_created;
      DateTime _gaming_day;
      DateTime _now, _next_opening_time;
      Int32 _gp_min_before_closing;

      CashierSessionId = 0;
      MoneyCollectionId = 0;

      try
      {
        _session_created = false;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          //GamingDay session open
          _now = WGDB.Now;
          _next_opening_time = Misc.TodayOpening().AddDays(1);
          _gp_min_before_closing = GeneralParam.GetInt32("GamingDay", "CashOpening.Select.MinutesBeforeTodayOpening");

          if (_now >= _next_opening_time.AddMinutes(-_gp_min_before_closing))
          {
            _gaming_day = _next_opening_time;
          }
          else
          {
            _gaming_day = Misc.TodayOpening();
          }

          _sb = new StringBuilder();
          _sb.AppendLine("SET @pSessionId         = NULL ");
          _sb.AppendLine("SET @pMoneyCollectionId = NULL ");
          _sb.AppendLine("SET @pSessionCreated    = 0 ");

          _sb.AppendLine("SELECT   @pSessionId         = CS_SESSION_ID ");
          _sb.AppendLine("       , @pMoneyCollectionId = MC_COLLECTION_ID ");
          _sb.AppendLine("  FROM   CASHIER_SESSIONS ");
          _sb.AppendLine("  LEFT   JOIN MONEY_COLLECTIONS  ON MC_TERMINAL_ID = @pTerminalId AND MC_CASHIER_SESSION_ID = CS_SESSION_ID ");
          _sb.AppendLine(" WHERE   CS_CASHIER_ID = @pCashierTerminalId ");
          _sb.AppendLine("   AND   CS_STATUS     = @pCashierSessionOpen ");

          _sb.AppendLine("IF (@pSessionId IS NULL) ");
          _sb.AppendLine("BEGIN ");
          _sb.AppendLine("  INSERT INTO    CASHIER_SESSIONS ");
          _sb.AppendLine("               ( CS_NAME ");
          _sb.AppendLine("               , CS_USER_ID ");
          _sb.AppendLine("               , CS_CASHIER_ID ");
          _sb.AppendLine("               , CS_TAX_A_PCT ");
          _sb.AppendLine("               , CS_TAX_B_PCT ");
          _sb.AppendLine("               , CS_SESSION_BY_TERMINAL ");
          _sb.AppendLine("               , CS_GAMING_DAY ");
          _sb.AppendLine("               ) ");
          _sb.AppendLine("        VALUES ");
          _sb.AppendLine("               ( @pName ");               // session name
          _sb.AppendLine("               , @pUserId ");             // session_user_id
          _sb.AppendLine("               , @pCashierTerminalId ");  // session_cashier_terminal_id
          _sb.AppendLine("               , @pTax_a_pct ");
          _sb.AppendLine("               , @pTax_b_pct ");
          _sb.AppendLine("               , @pSessionByTerminal ");
          _sb.AppendLine("               , @pGamingDay ");
          _sb.AppendLine("               ) ");
          _sb.AppendLine("  SET   @pSessionId         = SCOPE_IDENTITY() ");
          _sb.AppendLine("  SET   @pMoneyCollectionId = NULL ");
          _sb.AppendLine("  SET   @pSessionCreated    = 1 ");
          _sb.AppendLine("END ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            // Format Session Name
            _name = UserName + " " + WGDB.Now.ToString("dd/MMM/yyyy HH:mm");
            _name = _name.ToUpper();

            _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = _name;
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
            _sql_cmd.Parameters.Add("@pCashierTerminalId", SqlDbType.Int).Value = CashierTerminalId;
            _sql_cmd.Parameters.Add("@pCashierSessionOpen", SqlDbType.Int).Value = (Int32)CASHIER_SESSION_STATUS.OPEN;

            _dec_value = GeneralParam.GetDecimal("Cashier", "Split.A.Tax.Pct", -1);
            if (_dec_value >= 0)
            {
              _sql_cmd.Parameters.Add("@pTax_a_pct", SqlDbType.Decimal).Value = _dec_value;
            }
            _dec_value = GeneralParam.GetDecimal("Cashier", "Split.B.Tax.Pct", -1);
            if (_dec_value >= 0)
            {
              _sql_cmd.Parameters.Add("@pTax_b_pct", SqlDbType.Decimal).Value = _dec_value;
            }

            // In TITO mode, the cashier sessions are always by terminal.
            _sql_cmd.Parameters.Add("@pSessionByTerminal", SqlDbType.Bit).Value = true;

            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
            _sql_cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = _gaming_day;

            _param_session_id = _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt);
            _param_session_id.Direction = ParameterDirection.Output;

            _param_money_collection_id = _sql_cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt);
            _param_money_collection_id.Direction = ParameterDirection.Output;

            _param_session_created = _sql_cmd.Parameters.Add("@pSessionCreated", SqlDbType.Bit);
            _param_session_created.Direction = ParameterDirection.Output;

            // Can't check the number of rows: INSERT only if not exists.
            _db_trx.ExecuteNonQuery(_sql_cmd);
          }

          _db_trx.Commit();
        }

        CashierSessionId = (Int64)_param_session_id.Value;
        if (_param_money_collection_id.Value != null && _param_money_collection_id.Value != DBNull.Value)
        {
          MoneyCollectionId = (Int64)_param_money_collection_id.Value;
        }
        _session_created = (Boolean)_param_session_created.Value;

        CommonCashierInformation.SetCashierInformation(CashierSessionId, UserId, UserName, CashierTerminalId, TerminalName);

        // Lock the cashier session
        try
        {
          Cashier.UpdateSessionBalance(Trx, CashierSessionId, 0);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          return false;
        }

        if (_session_created)
        {
          _csi = CommonCashierInformation.CashierSessionInfo();
          _cashier_mov_table = new CashierMovementsTable(_csi);
          _cashier_mov_table.Add(0, CASHIER_MOVEMENT.OPEN_SESSION, 0, 0, "");
          if (!_cashier_mov_table.Save(Trx))
          {
            Log.Error("GetOrOpenSystemCashierSession. Error saving cashier movements");

            return false;
          }

          //// AJQ 07-MAR-2014, Always try to link the cashier session with cage
          //if (!Cage.LinkTerminalWithCageSession(_csi, Trx))
          //{
          //  Log.Error("LinkTerminalWithCageSession failed. Session id: " + CashierSessionId.ToString());

          //  return false;
          //}
        }

        if (MoneyCollectionId == 0)
        {
          if (!MoneyCollection.DB_CreateTerminalMoneyCollection(TerminalId, 0, 0, CashierSessionId, out MoneyCollectionId, Trx))
          {
            return false;
          }
          if (MoneyCollectionId == 0)
          {
            return false;
          }

          if (!MoneyCollectionMeter.InitializeMoneyCollectionMeter(CashierSessionId, MoneyCollectionId, TerminalId, Trx))
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // WCP_TITO_GetOrCreateCashierSessionAndMoneyCollection

    //------------------------------------------------------------------------------
    // PURPOSE : Create virtual account for play session
    //
    //  PARAMS :
    //      - INPUT :
    //         - TerminalId
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  
    //
    //   NOTES :
    //
    public static Boolean WCP_TITO_CreateVirtualAccount(Int32 TerminalId, out Int64 VirtualAccountId, out Boolean CreatedVirtualAccount, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _param;
      String _track_data;
      Int64 _current_site_id;

      VirtualAccountId = 0;
      CreatedVirtualAccount = false;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   @AccountId = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0) ");
        _sb.AppendLine("   FROM   TERMINALS ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId; ");
        _sb.AppendLine();
        _sb.AppendLine(" IF ( @AccountId = 0 ) ");
        _sb.AppendLine("   UPDATE   TERMINALS                                       ");
        _sb.AppendLine("      SET   TE_VIRTUAL_ACCOUNT_ID  = TE_VIRTUAL_ACCOUNT_ID  ");
        _sb.AppendLine("    WHERE   TE_TERMINAL_ID         = @pTerminalId;          ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _param = _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          // Can't check the number of rows: UPDATE only if Virtual account_id not exists.
          _sql_cmd.ExecuteNonQuery();

          VirtualAccountId = (Int64)_param.Value;
          if (VirtualAccountId != 0)
          {
            return true;
          }
        }

        if (!Int64.TryParse(GeneralParam.GetString("Site", "Identifier", "0"), out _current_site_id))
        {
          return false;
        }

        _track_data = "00000000000000000000" + CardData.VIRTUAL_FLAG_TRACK_DATA;

        _sb = new StringBuilder();
        _sb.AppendLine("DECLARE @pAccountId AS BIGINT ");
        _sb.AppendLine();
        _sb.AppendLine("   EXECUTE   CreateAccount @pAccountId OUTPUT   ");
        _sb.AppendLine("   SET @AccountId = @pAccountId                 ");
        _sb.AppendLine("   UPDATE   ACCOUNTS                            ");
        _sb.AppendLine("      SET   AC_TYPE             = @pAccountType ");
        _sb.AppendLine("          , AC_BLOCKED          = 0             ");
        _sb.AppendLine("          , AC_BALANCE          = 0             ");
        _sb.AppendLine("          , AC_RE_BALANCE       = 0             ");
        _sb.AppendLine("          , AC_CARD_PAID        = 1             ");
        _sb.AppendLine("          , AC_TRACK_DATA       = @pTrackData + CONVERT(VARCHAR(19), @AccountId)  ");
        _sb.AppendLine("   WHERE    AC_ACCOUNT_ID       = @pAccountId;  ");
        _sb.AppendLine();
        _sb.AppendLine("   UPDATE   TERMINALS                               ");
        _sb.AppendLine("      SET   TE_VIRTUAL_ACCOUNT_ID  = @pAccountId    ");
        _sb.AppendLine("    WHERE   TE_TERMINAL_ID         = @pTerminalId;  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pVirtualId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pAccountType", SqlDbType.Int).Value = (Int32)AccountType.ACCOUNT_VIRTUAL_TERMINAL;
          _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50).Value = _track_data;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          _param = _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_sql_cmd.ExecuteNonQuery() >= 1)
          {
            VirtualAccountId = (Int64)_param.Value;
            CreatedVirtualAccount = true;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // WCP_TITO_CreateVirtualAccount

    //------------------------------------------------------------------------------
    // PURPOSE: Get the cashier terminal id from a specific terminal name.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SqlTrx
    //          - TerminalName
    //          - IsAcceptor
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public static Boolean WCP_TITO_CreateCashierTerminal(String TerminalName, Int32 TerminalId, out Int32 CashierTerminalId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _sql_param;

      CashierTerminalId = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SET @pCashierTerminalId = NULL");

        _sb.AppendLine("SELECT   @pCashierTerminalId = CT_CASHIER_ID FROM CASHIER_TERMINALS");
        _sb.AppendLine(" WHERE   CT_TERMINAL_ID = @pTerminalId");

        _sb.AppendLine("IF (@pCashierTerminalId IS NULL)");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("  INSERT INTO   CASHIER_TERMINALS ( CT_NAME, CT_TERMINAL_ID ) ");
        _sb.AppendLine("       VALUES   ( @pTerminalName, @pTerminalId ) ");
        _sb.AppendLine("          SET   @pCashierTerminalId = SCOPE_IDENTITY() ");
        _sb.AppendLine("END");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = TerminalName;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          _sql_param = _cmd.Parameters.Add("@pCashierTerminalId", SqlDbType.Int);
          _sql_param.Direction = ParameterDirection.Output;

          _cmd.ExecuteNonQuery();
        }

        CashierTerminalId = (Int32)_sql_param.Value;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // WCP_TITO_CreateCashierTerminal

    //------------------------------------------------------------------------------
    // PURPOSE : External caller for method update account, with int/out money
    //           (ticket cancel) movements in used account
    //
    //  PARAMS :
    //      - INPUT :
    //        - TerminalId
    //        - PlaySessionId
    //        - Amount
    //        - AccountId
    //        - SequenceId
    //        - TransactionId
    //        - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean GenerateTicketInAccountMovements(Int32 TerminalId,
                                                           String TerminalName,
                                                           Int64 PlaySessionId,
                                                           Int64 CashierSessionId,
                                                           Decimal Amount,
                                                           Int64 AccountId,
                                                           Int64 SequenceId,
                                                           Int64 TransactionId,
                                                           TITO_TICKET_TYPE TicketType,
                                                           SqlTransaction Trx)
    {
      Int64 _account_movement_id;
      // DHA 25-APR-2014 modified/added TITO tickets movements
      MovementType _account_mov_type;

      if (TicketType == TITO_TICKET_TYPE.CASHABLE)
      {
        _account_mov_type = MovementType.TITO_TicketMachinePlayedCashable;
      }
      else if (TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
      {
        _account_mov_type = MovementType.TITO_TicketMachinePlayedPromoRedeemable;
      }
      else
      {
        _account_mov_type = MovementType.TITO_ticketMachinePlayedPromoNotRedeemable;
      }

      //RCI No need to change to AccountsMovementTable object (new insert movement method)
      // Add cash movement
      WCP_BusinessLogic.DB_InsertMovement(0,
                                          PlaySessionId,
                                          AccountId,
                                          TerminalId,
                                          SequenceId,
                                          TransactionId,
                                          _account_mov_type,
                                          0,                                            // InitialBalance
                                          0,                                            // SubAmount
                                          Amount,                                       // AddAmount
                                          Amount,                                       // FinalBalance
                                          out _account_movement_id,
                                          Trx);
      //RCI No need to change to AccountsMovementTable object (new insert movement method)
      // Add opposite cash movement
      WCP_BusinessLogic.DB_InsertMovement(0,
                                          PlaySessionId,
                                          AccountId,
                                          TerminalId,
                                          SequenceId,
                                          TransactionId,
                                          MovementType.TITO_AccountToTerminalCredit,
                                          Amount,                                           // InitialBalance
                                          Amount,                                           // SubAmount
                                          0,                                                // AddAmount
                                          0,                                                // FinalBalance
                                          out _account_movement_id,
                                          Trx);

      return true;
    } // GenerateTicketInAccountMovements

    //------------------------------------------------------------------------------
    // PURPOSE : External caller for method update cashier session, with in money
    //           (ticket cancel) 
    //
    //  PARAMS :
    //      - INPUT :
    //        - TerminalId
    //        - PlaySessionId
    //        - Amount
    //        - AccountId
    //        - SequenceId
    //        - TransactionId
    //        - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean GenerateTicketInCashierMovements(Int32 TerminalId,
                                                           String TerminalName,
                                                           Decimal Amount,
                                                           Int64 AccountId,
                                                           String Trackdata,
                                                           TITO_TICKET_TYPE TicketType,
                                                           Int64 TicketId,
                                                           Int64 ValidationNumber,
                                                           SqlTransaction Trx)
    {
      CASHIER_MOVEMENT _cashier_mov_type;

      CashierMovementsTable _cashier_movemets = new CashierMovementsTable(CommonCashierInformation.CashierSessionInfo(), TerminalName);
      // DHA 25-APR-2014 modified/added TITO tickets movements
      if (TicketType == TITO_TICKET_TYPE.CASHABLE)
      {
        _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_CASHABLE;
      }
      else if (TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
      {
        _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE;
      }
      else
      {
        _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE;
      }

      _cashier_movemets.Add(0, _cashier_mov_type, Amount, AccountId, Trackdata, ValidationNumber.ToString(), "", Amount);
      _cashier_movemets.AddRelatedIdInLastMov(TicketId);

      if (Misc.IsFloorDualCurrencyEnabled())
      {
        if (!DualCurrencyAddTicketPlayedExchangeMovement(TicketId, TerminalId, AccountId, Trackdata, ValidationNumber, ref _cashier_movemets, Trx))
        {
          Log.Error("Error in DualCurrencyInsertTicketPlayedExchangeMovement");

          return false;
        }
      }

      if (!_cashier_movemets.Save(Trx))
      {
        Log.Message("GenerateTicketInCashierMovements: Error saving cashier movement");

        return false;
      }

      return true;
    } // GenerateTicketInCashierMovements

    public static Boolean DualCurrencyAddTicketPlayedExchangeMovement(Int64 TicketId, Int32 TerminalId, Int64 AccountId, String Trackdata, Int64 ValidationNumber, ref CashierMovementsTable CashierMovements, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Decimal _amount_national_cur;
      Decimal _amount_played;
      String _iso_code_played;

      _amount_national_cur = 0;
      _amount_played = 0;
      _iso_code_played = "";

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("DECLARE   @TERMINAL_ISO_CODE AS NVARCHAR(3)");

        _sb.AppendLine("SELECT   @TERMINAL_ISO_CODE = TE_ISO_CODE");
        _sb.AppendLine("  FROM   TERMINALS");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId");

        _sb.AppendLine("IF @TERMINAL_ISO_CODE <> @pNationalCurrency");
        _sb.AppendLine("  BEGIN");
        _sb.AppendLine("  SELECT   TI_AMOUNT");
        _sb.AppendLine("         , TI_AMT1");
        _sb.AppendLine("         , TI_CUR1");
        _sb.AppendLine("    FROM   TICKETS");
        _sb.AppendLine("   WHERE   TI_TICKET_ID = @pTicketId");
        _sb.AppendLine("  END");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
          _sql_cmd.Parameters.Add("@pNationalCurrency", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency();

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _amount_national_cur = _reader.GetDecimal(0);
              _amount_played = _reader.GetDecimal(1);
              _iso_code_played = _reader.GetString(2);
            }
            else
            {
              return true;
            }
          }
        }

        // Insert movement
        CashierMovements.Add(0, CASHIER_MOVEMENT.TITO_TICKETS_PLAYED_EXCHANGE_DUAL_CURRENCY, 0, AccountId, Trackdata, ValidationNumber.ToString(), _iso_code_played);
        CashierMovements.SetInitialBalanceInLastMov(_amount_played);
        CashierMovements.AddSubAmountInLastMov(_amount_national_cur);
      }
      catch (Exception)
      {
        return false;
      }

      return true;
    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Load configured SAS bills meter
    //
    //  PARAMS:
    //      - INPUT:
    //        - SAS_METERS_GROUPS
    //        - SqlTransaction
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //        - Int32, with seted flags of required bill meters
    //
    public Int32 LoadFlagsBillsMetersRequired(SAS_METERS_GROUPS GroupId, SqlTransaction Trx)
    {
      Int32 _result;
      Int32 _idx_bit;
      Int32 _first_group_code;
      Int32 _max_array_len;
      BitArray _bits;
      Int32[] _integers;
      StringBuilder _sb;

      _result = 0;
      _idx_bit = 0;
      _max_array_len = 32;

      switch (GroupId)
      {
        case SAS_METERS_GROUPS.IDX_SMG_BILLS: _first_group_code = 0x0040; break;
        case SAS_METERS_GROUPS.IDX_SMG_TICKETS: _first_group_code = 0x0080; break;
        default: _first_group_code = 0x0000; break;
      }

      _sb = new StringBuilder();
      _sb.AppendLine("   SELECT   SMCG_METER_CODE ");
      _sb.AppendLine("     FROM   SAS_METERS_CATALOG_PER_GROUP ");
      _sb.AppendLine("LEFT JOIN   SAS_METERS_GROUPS AS METERS_GROUPS ");
      _sb.AppendLine("            ON SMCG_GROUP_ID = METERS_GROUPS.SMG_GROUP_ID ");
      _sb.AppendLine("    WHERE   SMCG_GROUP_ID = @pGroupId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pGroupId", SqlDbType.Int).Value = (Int32)GroupId;
        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          // set the bits on array
          _bits = new BitArray(_max_array_len);
          while (_reader.Read())
          {
            _idx_bit = _reader.GetInt32(0) - _first_group_code;
            if (_idx_bit >= 0 && _idx_bit < _max_array_len)
            {
              _bits[_idx_bit] = true;
            }
            else
            {
              Log.Error(String.Format("Index/Bit for SAS meters out of range: {0}", _reader.GetInt32(0)));
            }
          }

          // convert bits to integer value
          _integers = new Int32[1];
          _bits.CopyTo(_integers, 0);
          _result = (Int32)_integers[0];
        }
      }

      return _result;
    } // LoadFlagsBillsMetersRequired

    //------------------------------------------------------------------------------
    // PURPOSE: Load terminal data + config, based in its TerminalId
    //
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId
    //        - SqlTransaction
    //
    //      - OUTPUT: 
    //        - WSI.Common.Terminal.TerminalInfo
    //
    // RETURNS:
    //        - True, if terminal-data was updated
    //
    public Boolean LoadTerminalInfoWithParams(Int32 TerminalId, out WSI.Common.Terminal.TerminalInfo OutTerminal, SqlTransaction Trx)
    {
      Boolean _result;
      StringBuilder _sb;
      Int32 _sas_flags_terminal;
      Int32 _sas_flags_terminal_use_site_default;
      Int32 _sas_flags_site_default;
      String _terminal_iso_code;
      Decimal? _max_allowed_ticket_out;
      WSI.Common.Terminal.TerminalInfo _terminal_full_info;


      OutTerminal = null;
      _result = false;
      _max_allowed_ticket_out = 0;
      _sb = new StringBuilder();

      try
      {
        _terminal_full_info = new WSI.Common.Terminal.TerminalInfo();
        WSI.Common.Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_full_info, Trx);

        _sb.AppendLine(" SELECT   TE_TERMINAL_ID                "); // 0
        _sb.AppendLine("        , ISNULL(TE_PROV_ID, 0)         "); // 1
        _sb.AppendLine("        , TE_TERMINAL_TYPE              "); // 2
        _sb.AppendLine("        , TE_NAME                       "); // 3
        _sb.AppendLine("        , TE_PROVIDER_ID                "); // 4
        _sb.AppendLine("        , TE_BLOCKED                    "); // 5
        _sb.AppendLine("        , TE_STATUS                     "); // 6
        _sb.AppendLine("        , TE_CURRENT_PLAY_SESSION_ID    "); // 7
        _sb.AppendLine("        , TE_CURRENT_ACCOUNT_ID         "); // 8
        _sb.AppendLine("        , PV_POINTS_MULTIPLIER          "); // 9
        _sb.AppendLine("        , CAST(ISNULL ( ( SELECT   1                           ");
        _sb.AppendLine("                       FROM   TERMINAL_GROUPS                  ");
        _sb.AppendLine("                      WHERE   TG_ELEMENT_TYPE = @pElementType  ");
        _sb.AppendLine("                        AND   TG_TERMINAL_ID = @pTerminalId    ");
        _sb.AppendLine("                        AND   TG_ELEMENT_ID = 1 ), 0 ) AS BIT) "); // Element Id 1 is JackPot Generic
        _sb.AppendLine("          AS SITE_JACKPOT               "); // 10
        _sb.AppendLine("        , PV_ONLY_REDEEMABLE            "); // 11
        _sb.AppendLine("        , TE_SAS_FLAGS                  "); // 12
        _sb.AppendLine("        , TE_SAS_FLAGS_USE_SITE_DEFAULT "); // 13
        _sb.AppendLine("        , TE_SEQUENCE_ID                "); // 14
        _sb.AppendLine("        , ISNULL(TE_ALLOWED_CASHABLE_EMISSION, @pAllowCashableEmission) AS ALLOWED_EMISSION "); // 15
        _sb.AppendLine("        , ISNULL(TE_ALLOWED_PROMO_EMISSION, @pAllowPromotionalEmission) AS PROMO_EMISSION ");   // 16
        _sb.AppendLine("        , ISNULL(TE_ALLOWED_REDEMPTION, @pAllowRedemption) AS ALLOWED_REDEMPTION ");            // 17
        _sb.AppendLine("        , TE_MAX_ALLOWED_TO             "); // 18
        _sb.AppendLine("        , TE_ISO_CODE                   "); // 19
        _sb.AppendLine("        , TE_TITO_HOST_ID               "); // 20
        _sb.AppendLine("   FROM   TERMINALS ");
        _sb.AppendLine("LEFT JOIN PROVIDERS ON PV_ID = TE_PROV_ID");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.JACKPOT;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          _cmd.Parameters.Add("@pAllowCashableEmission", SqlDbType.Int).Value = GeneralParam.GetInt32("TITO", "CashableTickets.AllowEmission", 0);
          _cmd.Parameters.Add("@pAllowPromotionalEmission", SqlDbType.Int).Value = GeneralParam.GetInt32("TITO", "PromotionalTickets.AllowEmission", 0);
          _cmd.Parameters.Add("@pAllowRedemption", SqlDbType.Int).Value = GeneralParam.GetInt32("TITO", "AllowRedemption", 0);
          
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return _result;
            }

            OutTerminal = new WSI.Common.Terminal.TerminalInfo();
            OutTerminal.TerminalId = _reader.GetInt32(0);
            OutTerminal.ProvId = _reader.GetInt32(1);

            OutTerminal.TerminalType = (TerminalTypes)_reader.GetInt16(2);
            OutTerminal.Name = _reader.GetString(3);
            OutTerminal.ProviderName = _reader.GetString(4);
            OutTerminal.Blocked = _reader.GetBoolean(5);
            OutTerminal.Status = (TerminalStatus)_reader.GetInt32(6);
            OutTerminal.CurrentPlaySessionId = _reader.IsDBNull(7) ? 0 : _reader.GetInt64(7);
            OutTerminal.CurrentAccountId = _reader.IsDBNull(8) ? 0 : _reader.GetInt64(8);
            OutTerminal.PvPointsMultiplier = Math.Max(0, _reader.GetDecimal(9));
            OutTerminal.PvSiteJackpot = _reader.GetBoolean(10);
            OutTerminal.PvOnlyRedeemable = _reader.GetBoolean(11);

            _sas_flags_terminal = _reader.GetInt32(12);
            _sas_flags_terminal_use_site_default = _reader.GetInt32(13);
            _sas_flags_site_default = GeneralParam.GetInt32("SasHost", "SystemDefaultSasFlags");

            OutTerminal.SASFlags = _sas_flags_site_default & _sas_flags_terminal_use_site_default
                                   |
                                   _sas_flags_terminal & ~_sas_flags_terminal_use_site_default;

            // Maintain value for SAS_FLAGS_RESERVE_TERMINAL, ignore the site default value
            if (WSI.Common.Terminal.SASFlagActived(_sas_flags_terminal, SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL))
            {
              OutTerminal.SASFlags |= (Int32)SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL;
            }

            OutTerminal.PlayMode = (WSI.Common.Terminal.CreditsPlayMode)GeneralParam.GetInt32("Credits", "PlayMode", 0);

            switch (OutTerminal.PlayMode)
            {
              case WSI.Common.Terminal.CreditsPlayMode.NotRedeemableLast:
              case WSI.Common.Terminal.CreditsPlayMode.PromotionalsFirst:
                break;

              default:
                OutTerminal.PlayMode = WSI.Common.Terminal.CreditsPlayMode.NotRedeemableLast;
                break;
            }

            if (OutTerminal.PlayMode == WSI.Common.Terminal.CreditsPlayMode.PromotionalsFirst)
            {
              if (OutTerminal.TerminalType == TerminalTypes.SAS_HOST)
              {
                if (OutTerminal.SASFlagActivated(SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE))
                {
                  OutTerminal.PlayMode = WSI.Common.Terminal.CreditsPlayMode.SasMachine;
                }
              }
            }

            OutTerminal.SequenceId = _reader.IsDBNull(14) ? 0 : _reader.GetInt64(14);
            OutTerminal.AllowCashableTicketOut = _reader.GetBoolean(15);
            OutTerminal.AllowPromotionalTicketOut = _reader.GetBoolean(16);
            OutTerminal.AllowTicketIn = _reader.GetBoolean(17);
            OutTerminal.MachineId = _reader.GetInt32(0);

            // DHA 13-JAN-2016: set terminal max allowed amount ticket out
            if (_reader.IsDBNull(18))
            {
              _max_allowed_ticket_out = null;
            }
            else
            {
              _max_allowed_ticket_out = _reader.GetDecimal(18);
            }

            if (_reader.IsDBNull(19))
            {
              _terminal_iso_code = CurrencyExchange.GetNationalCurrency();
            }
            else
            {
              _terminal_iso_code = _reader.GetString(19);
            }

            OutTerminal.HostId = _reader.GetInt32(20);            

            _reader.Close();

            OutTerminal.ExpirationTicketsPromotional = GeneralParam.GetInt32("TITO", "PromotionalTickets.NonRedeemable.ExpirationDays", 0);
            OutTerminal.ExpirationTicketsCashable = GeneralParam.GetInt32("TITO", "CashableTickets.ExpirationDays", 0);
            OutTerminal.SystemId = GeneralParam.GetInt32("TITO", "TicketsSystemId", 0);

            OutTerminal.TitoTicketLocalization = GeneralParam.GetString("TITO", "Tickets.Location", "");
            OutTerminal.TitoTicketAddress_1 = Utils.GetValueAddres(_terminal_full_info, 1);
            OutTerminal.TitoTicketAddress_2 = Utils.GetValueAddres(_terminal_full_info, 2);
            OutTerminal.TitoTicketTitleDebit = GeneralParam.GetString("TITO", "DebitTickets.Title", "");
            OutTerminal.TitoTicketTitleRestricted = GeneralParam.GetString("TITO", "PromotionalTickets.NonRedeemable.Title", "");

            _result = true;
          }

          if (!_max_allowed_ticket_out.HasValue)
          {
            _max_allowed_ticket_out = GeneralParam.GetDecimal("TITO", "Tickets.MaxAllowedTicketOut", 0);
            // DHA 19-JAN-2016: when dual currency is enabled, parameter MaxAllowedTicketOut must be translate to terminal iso code
            if (Misc.IsFloorDualCurrencyEnabled() && CurrencyExchange.GetNationalCurrency() != _terminal_iso_code)
            {
              Decimal _amount_converted;
              CurrencyExchange _target_currency;

              _amount_converted = 0;
              _target_currency = new CurrencyExchange();

              // Read target exchange config
              CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, _terminal_iso_code, out _target_currency, Trx);
              _target_currency.ChangeRate = 1 / _target_currency.ChangeRate;
              _target_currency.SimpleApplyExchange(_max_allowed_ticket_out.Value, out _amount_converted);
              _max_allowed_ticket_out = _amount_converted;

            }
          }

          OutTerminal.TicketsMaxCreationAmount = (Int64)(_max_allowed_ticket_out * 100);
        }
      }
      catch (Exception _ex)
      {
        OutTerminal = null;
        Log.Exception(_ex);
      }

      return _result;
    } // LoadTerminalInfoWithParams

    /// <summary>
    /// WCP_GetTerminalData
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="CashierTerminalId"></param>
    /// <param name="TerminalId"></param>
    /// <param name="TerminalName"></param>
    /// <param name="MobileBankId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean WCP_GetTerminalData(String VendorId, String SerialNumber, int MachineNumber, out Int32 CashierTerminalId, out Int32 TerminalId, out String TerminalName, out Int64 MobileBankId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      TerminalId = 0;
      CashierTerminalId = 0;
      TerminalName = "";
      MobileBankId = 0;

      try
      {

        _sb.AppendLine("     SELECT   TE_TERMINAL_ID AS Terminal_ID                                                             ");
        _sb.AppendLine("            , ISNULL(CT_CASHIER_ID, 0) AS CASHIER_TERMINAL_ID                                           ");
        _sb.AppendLine("  	        , ISNULL(TE_EXTERNAL_ID, '') AS TERMINAL_NAME                                               ");
        _sb.AppendLine("  	        , ISNULL(MB_ACCOUNT_ID, 0) AS MOBILE_BANK_ID                                                ");
        _sb.AppendLine("       FROM   TERMINALS                                                                                 ");
        _sb.AppendLine("  LEFT JOIN   TERMINALS_3GS     ON TE_TERMINAL_ID = T3GS_TERMINAL_ID                                    ");
        _sb.AppendLine("  LEFT JOIN   MOBILE_BANKS      ON TE_TERMINAL_ID = MB_TERMINAL_ID AND MB_ACCOUNT_TYPE = @pAccountType  ");
        _sb.AppendLine("  LEFT JOIN   CASHIER_TERMINALS ON TE_TERMINAL_ID = CT_TERMINAL_ID                                      ");
        _sb.AppendLine("      WHERE   T3GS_VENDOR_ID        = @pVendorID                                                        ");
        _sb.AppendLine("        AND   T3GS_SERIAL_NUMBER    = @pSerialNumber                                                    ");
        _sb.AppendLine("        AND   T3GS_MACHINE_NUMBER   = @pMachineNumber                                                   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pVendorID", SqlDbType.NVarChar).Value = VendorId;
          _cmd.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).Value = SerialNumber;
          _cmd.Parameters.Add("@pMachineNumber", SqlDbType.Int).Value = MachineNumber;
          _cmd.Parameters.Add("@pAccountType", SqlDbType.Int).Value = (Int32)MB_USER_TYPE.SYS_ACCEPTOR;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            TerminalId = _reader.GetInt32(0);
            CashierTerminalId = _reader.GetInt32(1);
            TerminalName = _reader.GetString(2);
            MobileBankId = _reader.GetInt64(3);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }


    ////------------------------------------------------------------------------------
    //// PURPOSE: Get Session Terminal
    ////
    ////  PARAMS:
    ////      - INPUT:
    ////        - Terminal Id
    ////        - SqlTransaction
    ////
    ////      - OUTPUT: 
    ////        - Filled OutTerminalInfo
    ////
    //// RETURNS:
    ////
    //public Boolean GetSequenceTerminal(Int32 TerminalId, out Int64 SequenceTerminal, SqlTransaction Trx)
    //{
    //  Boolean _result;
    //  StringBuilder _sb;

    //  _result = false;
    //  SequenceTerminal = -1;

    //  try
    //  {
    //    _sb = new StringBuilder();

    //    _sb.AppendLine("DECLARE @_last_seq as BigInt");
    //    _sb.AppendLine("");
    //    _sb.AppendLine("UPDATE   TERMINALS ");
    //    _sb.AppendLine("   SET   @_last_seq = ISNULL(TE_SEQUENCE_ID, 1)");
    //    _sb.AppendLine("       , TE_SEQUENCE_ID = CASE WHEN (ISNULL(TE_SEQUENCE_ID, 1) + 1) > @pMaxSequence");
    //    _sb.AppendLine("							   THEN 1  ELSE  ISNULL(TE_SEQUENCE_ID, 0) + 1 END");
    //    _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId");
    //    _sb.AppendLine("");
    //    _sb.AppendLine("SELECT   @_last_seq");

    //    using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
    //    {
    //      _cmd.Parameters.Add("@pMaxSequence", SqlDbType.BigInt).Value = Ticket.MAX_TERMINAL_SEQUENCE_ID;
    //      _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

    //      using (SqlDataReader _reader = _cmd.ExecuteReader())
    //      {
    //        if (_reader.Read())
    //        {
    //          SequenceTerminal = _reader.GetInt64(0);

    //          _result = true;
    //        }
    //      }
    //    }
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);
    //    _result = false;
    //  }

    //  return _result;
    //} // GetSessionTerminal


    #endregion


  }
}
