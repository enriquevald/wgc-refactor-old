//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Statistics.cs
// 
//   DESCRIPTION: GameMeters and MachineMeters updates
// 
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 12-APR-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-APR-2012 RCI    First release.
//------------------------------------------------------------------------------

using System;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.WCP
{
  public static class StatisticsPerHour
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes Statistics thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(UpdateGameMetersAndMachineMetersThread);
      _thread.Name = "UpdateGameMetersAndMachineMetersThread";
      _thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Reloads the statistics per hour from game meters (Sas Host Terminals)
    //           Reloads the statistics per hour from machine meters (Sas Host Terminals)
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static private void UpdateGameMetersAndMachineMetersThread()
    {
      Random _rnd;

      _rnd = new Random(Environment.TickCount);

      while (true)
      {
        // Wait 1 minute
        Thread.Sleep(60000);

        try
        {
          Thread.Sleep(_rnd.Next(10000));

          using (DB_TRX _db_trx = new DB_TRX())
          {
            //
            // Check Game Meters and Accumulate to SalesPerHour
            //
            if (WCP_GameMeters.UpdateStatisticsFromGameMeters(_db_trx.SqlTransaction))
            {
              _db_trx.Commit();
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        try
        {
          Thread.Sleep(_rnd.Next(10000));

          using (DB_TRX _db_trx = new DB_TRX())
          {
            //
            // Check Machine Meters and Accumulate to MachinePerHour
            //
            if (WCP_BU_MachineMeters.ProcessMachineMetersStatistics(_db_trx.SqlTransaction))
            {
              _db_trx.Commit();
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        // TERMINAL SAS METERS HISTORY
        
      } // while

    } // UpdateGameMetersAndMachineMetersThread

  } // class Statistics
}
