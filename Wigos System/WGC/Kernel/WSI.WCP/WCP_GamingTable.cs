﻿//---------------------------------------------------------------------------------------------------------------------------------
// Copyright © 2010 Win Systems Ltd.
//---------------------------------------------------------------------------------------------------------------------------------
//
//   MODULE NAME : WCP_GamingTable.cs
//
//   DESCRIPTION : WCP_GamingTable class
//
// REVISION HISTORY :
//
// Date        Author      Description
// ----------- ---------- ----------------------------------------------------------
// 09-MAY-2017 JML        First release.
// 10-MAY-2017 JML        PBI 27288: Win / loss - Create automatically a null value for unreported hours
//---------------------------------------------------------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WCP;
using System.Collections;
using System.Threading;

namespace WSI.WCP
{
  public static class WCP_GamingTable
  {
    static Thread m_thread;

    private const Int32 WAIT_FOR_NEXT_TRY = (1 * 60 * 1000);      // 1 minutes
    private const Int32 WAIT_FOR_FIRST_TRY = (10 * 1000);         // 10 seconds
    private const Int32 NUM_JOBS = 1;                             // Number of Jobs to perform on thread

    //---------------------------------------------------------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_GamingTable class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //---------------------------------------------------------------------------------------------------------------------------------
    public static void Init()
    {
      m_thread = new Thread(GamingTables_PeriodicJobThread);
      m_thread.Name = "GamingTables_PeriodicJobThread";

    } // Init

    //---------------------------------------------------------------------------------------------------------------------------------
    // PURPOSE : Public function for thread starts
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //---------------------------------------------------------------------------------------------------------------------------------
    public static void Start()
    {
      // Thread starts
      m_thread.Start();

    } // Start

    #region Private Functions

    //---------------------------------------------------------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //---------------------------------------------------------------------------------------------------------------------------------
    static private void GamingTables_PeriodicJobThread()
    {
      Int32 _wait_hint;

      Int32 _step;
      Int32 _tick0;
      Int64[] _ellapsed;

      _ellapsed = new Int64[NUM_JOBS + 1];

      _wait_hint = WAIT_FOR_FIRST_TRY;

      while (true)
      {
        System.Threading.Thread.Sleep(_wait_hint);

        if (!GamingTableBusinessLogic.IsEnabledGTWinLoss())
        {
          _wait_hint = WAIT_FOR_NEXT_TRY * 30;     // 30 times more slow
          continue;
        }

        if (!Services.IsPrincipal("WCP"))
        {
          _wait_hint = WAIT_FOR_NEXT_TRY * 30;     // 30 times more slow
          continue;
        }

        _wait_hint = WAIT_FOR_NEXT_TRY;

        _step = 0;

        try
        {
          // Initializate the _ellapsed array
          for (int _idx_step = 0; _idx_step < NUM_JOBS; _idx_step++)
          {
            _ellapsed[_idx_step] = 0;
          }

          //
          // Create Unreported Hours for Win/Loss.
          //       
          _step = 0;
          _tick0 = Environment.TickCount;
          WinLoss_CreateUnreportedHours();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // CODE
          _step++;
          _ellapsed[_step] = 0;
          for (int _idx_step = 0; _idx_step < _step; _idx_step++)
          {
            if (_ellapsed[_idx_step] > 1000)
            {
              Log.Warning("GamingTables_periodicJob.Step[" + _idx_step.ToString() + "], Duration: " + _ellapsed[_idx_step].ToString() + " ms.");
            }
          }

        }
        catch (Exception _ex)
        {
          String _function = "NONE";

          switch (_step)
          {
            case 0:
              _function = "WinLoss_CreateUnreportedHours";
              break;
            default:
              _function = "NONE";
              break;
          }

          Log.Warning("WCP_GamingTable.GamingTables_periodicJobTread. Exception in function: " + _function);
          Log.Exception(_ex);
        }

      } // while

    } // PeriodicJobThread

    //---------------------------------------------------------------------------------------------------------------------------------
    // PURPOSE : Create Unreported Hours for win / loss
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //---------------------------------------------------------------------------------------------------------------------------------
    private static Boolean WinLoss_CreateUnreportedHours()
    {
      StringBuilder _sb_read;
      StringBuilder _sb_insert;
      DataTable _gaming_table_sessions_open;
      Int64 _play_session_id;
      DateTime _datetime_hour;
      String _iso_code;
      Int32 _cage_currency_type;

      Int32 _margin_minutes;
      DateTime _now;
      DateTime _date_to_add;
      DateTime _date_hour_to_add;

      Int32 _user_id;
      String _user_name;

      _margin_minutes = GamingTableBusinessLogic.GetGTWinLossMinutes();
      _now = WGDB.Now;
      _date_to_add = _now.AddMinutes(-1);
      _date_hour_to_add = new DateTime(_date_to_add.Year, _date_to_add.Month, _date_to_add.Day, _date_to_add.Hour, 0, 0);

      _iso_code = CurrencyExchange.GetNationalCurrency();
      _cage_currency_type = (Int32)CageCurrencyType.ChipsRedimible;

      _sb_read = new StringBuilder();
      _sb_read.AppendLine("     SELECT   GTS_GAMING_TABLE_SESSION_ID ");
      _sb_read.AppendLine("            , @pDateHourToAdd ");
      _sb_read.AppendLine("            , CS_STATUS ");
      _sb_read.AppendLine("            , CS_OPENING_DATE ");
      _sb_read.AppendLine("            , CS_CLOSING_DATE  ");
      _sb_read.AppendLine("       FROM   GAMING_TABLES_SESSIONS  ");
      _sb_read.AppendLine(" INNER JOIN   CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID  ");
      _sb_read.AppendLine("      WHERE   CS_OPENING_DATE > DATEADD(MINUTE, -60, @pNow) AND CS_OPENING_DATE < @pNow AND ISNULL(CS_CLOSING_DATE, GETDATE()) >= @pDateHourToAdd ");
      _sb_read.AppendLine("         OR   CS_CLOSING_DATE > DATEADD(MINUTE, -60, @pNow) AND CS_CLOSING_DATE < @pNow AND CS_CLOSING_DATE >= @pDateHourToAdd ");
      _sb_read.AppendLine("         OR   CS_STATUS = 0 ");

      _sb_insert = new StringBuilder();
      _sb_insert.AppendLine(" IF NOT EXISTS(SELECT 1 FROM   gaming_tables_win_loss");
      _sb_insert.AppendLine("                       WHERE   gtwl_gaming_table_session_id = @pGamingTableSessionId ");
      _sb_insert.AppendLine("                         AND   gtwl_datetime_hour           = @pDatetimeHour         ");
      _sb_insert.AppendLine("                         AND   gtwl_iso_code                = @pIsoCode              ");
      _sb_insert.AppendLine("                         AND   gtwl_cage_currency_type      = @pCageCurrencyType )   ");
      _sb_insert.AppendLine(" INSERT INTO  gaming_tables_win_loss       ");
      _sb_insert.AppendLine("            ( gtwl_gaming_table_session_id ");
      _sb_insert.AppendLine("            , gtwl_datetime_hour           ");
      _sb_insert.AppendLine("            , gtwl_iso_code                ");
      _sb_insert.AppendLine("            , gtwl_cage_currency_type      ");
      _sb_insert.AppendLine("            , gtwl_user_id                 ");
      _sb_insert.AppendLine("            , gtwl_last_update )           ");
      _sb_insert.AppendLine(" VALUES     ( @pGamingTableSessionId ");
      _sb_insert.AppendLine("            , @pDatetimeHour         ");
      _sb_insert.AppendLine("            , @pIsoCode              ");
      _sb_insert.AppendLine("            , @pCageCurrencyType     ");
      _sb_insert.AppendLine("            , @pUserId               ");
      _sb_insert.AppendLine("            , @pLastUpdate )         ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb_read.ToString()))
          {
            _cmd.Parameters.Add("@pDateHourToAdd", SqlDbType.DateTime).Value = _date_hour_to_add;
            _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = _now;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _gaming_table_sessions_open = new DataTable();
              _db_trx.Fill(_da, _gaming_table_sessions_open);
            }
          }

          if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_SYSTEM, _db_trx.SqlTransaction, out _user_id, out _user_name))
          {
            Log.Error("WinLoss_CreateUnreportedHours: Error in GetSystemUser.");

            return false;
          }

          foreach (DataRow _gt_session in _gaming_table_sessions_open.Rows)
          {
            _play_session_id = (Int64)_gt_session[0];
            _datetime_hour = (DateTime)_gt_session[1];

            using (SqlCommand _sql_cmd = new SqlCommand(_sb_insert.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = _play_session_id;
              _sql_cmd.Parameters.Add("@pDatetimeHour", SqlDbType.DateTime).Value = _date_hour_to_add;
              _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = _iso_code;
              _sql_cmd.Parameters.Add("@pCageCurrencyType", SqlDbType.Int).Value = _cage_currency_type;
              _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;
              _sql_cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = _now;

              _sql_cmd.ExecuteNonQuery();
            }
          }

          _db_trx.Commit();
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // WinLoss_CreateUnreportedHours

    #endregion  //  Private Functions

  } // class WCP_GamingTable

} // WSI.WCP
