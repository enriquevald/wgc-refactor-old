//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Bonusing.cs
// 
//   DESCRIPTION: WCP_Bonusing class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 22-JAN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-JAN-2014 JMM    First release.
// 05-JUN-2014 JMM    Fixed Bug WIG-1010: Bonus balance report doesn't show not confirmed bonuses
// 20-ABR-2016 JML    Fixed Bug: 12154: Dual Currency - "Site Jackpot": Don't exchanged prize to machine currency
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Threading;
using System.Xml;

namespace WSI.WCP
{
  /// <summary>  
  /// OJO: Class name MUST MATCH with definition in enum WCP_MsgTypes
  /// </summary>
  public class WCP_MsgBonusTransferStatus : IXml
  {
    public Int64 TransactionId;
    public Int32 Status; // 0: Unknown, 10: Pending, 20: Timeout, 30: Transferred, 11: NotifiedToEgm, 12: NotifiedToEgmAck, 13: NotifiedToEgmNack
    public Int64 CentsTransferredRe;
    public Int64 CentsTransferredPromoRe;
    public Int64 CentsTransferredPromoNr;
    public Int64 TransferredToPlaySessionId;

    public string ToXml()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgBonusTransferStatus>");
      _sb.AppendLine("  <TransactionId>" + TransactionId.ToString() + "</TransactionId>");
      _sb.AppendLine("  <Status>" + Status.ToString() + "</Status>");
      _sb.AppendLine("  <CentsTransferred>");
      _sb.AppendLine("    <Re>" + CentsTransferredRe.ToString() + "</Re>");
      _sb.AppendLine("    <PromoRe>" + CentsTransferredPromoRe.ToString() + "</PromoRe>");
      _sb.AppendLine("    <PromoNr>" + CentsTransferredPromoNr.ToString() + "</PromoNr>");
      _sb.AppendLine("  </CentsTransferred>");
      _sb.AppendLine("  <TransferredToPlaySessionId>" + TransferredToPlaySessionId.ToString() + "</TransferredToPlaySessionId>");
      _sb.AppendLine("</WCP_MsgBonusTransferStatus>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      TransactionId = XML.GetAsInt64(Xml, "TransactionId");
      Status = XML.GetAsInt32(Xml, "Status");
      CentsTransferredRe = XML.GetAsInt64(Xml, "Re");
      CentsTransferredPromoRe = XML.GetAsInt64(Xml, "PromoRe");
      CentsTransferredPromoNr = XML.GetAsInt64(Xml, "PromoNr");
      TransferredToPlaySessionId = XML.GetAsInt64(Xml, "TransferredToPlaySessionId");
    }
  }

  public class WCP_MsgBonusTransferStatusReply : IXml
  {
    public string ToXml()
    {
      return "<WCP_MsgBonusTransferStatusReply></WCP_MsgBonusTransferStatusReply>";
    }
    public void LoadXml(XmlReader Xml)
    {
      return;
    }
  }

  public class WCP_CmdBonusAward : IXml
  {
    public Int64 TransactionId;

    // CentsToTransfer
    public Int64 CentsToTransferRe;
    public Int64 CentsToTransferPromoRe;
    public Int64 CentsToTransferPromoNr;

    // Conditions
    public Int32 Timeout;
    public Int64 MinBetCents; // Award when bet amount is greater or equal to MinBetCents
    public Boolean NotRedimibleMustBeZero;
    public Byte SasTaxStatus;
    public Int32 TransferFlags;

    public Int32 Reserved1;
    public Int32 Reserved2;

    public string ToXml()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_CmdBonusAward>");
      _sb.AppendLine("  <TransactionId>" + TransactionId.ToString() + "</TransactionId>");
      _sb.AppendLine("  <CentsToTransfer>");
      _sb.AppendLine("    <Re>" + CentsToTransferRe.ToString() + "</Re>");
      _sb.AppendLine("    <PromoRe>" + CentsToTransferPromoRe.ToString() + "</PromoRe>");
      _sb.AppendLine("    <PromoNr>" + CentsToTransferPromoNr.ToString() + "</PromoNr>");
      _sb.AppendLine("  <CentsToTransfer>");
      _sb.AppendLine("  <Conditions>");
      _sb.AppendLine("    <Timeout>" + Timeout.ToString() + "</Timeout>");
      _sb.AppendLine("    <MinBetCents>" + MinBetCents.ToString() + "</MinBetCents>");
      _sb.AppendLine("    <NotRedimibleMustBeZero>" + (NotRedimibleMustBeZero ? "1" : "0") + "</NotRedimibleMustBeZero>");
      _sb.AppendLine("    <TransferFlags>" + TransferFlags.ToString() + "</TransferFlags>");
      _sb.AppendLine("    <Reserved1>" + Reserved1.ToString() + "</Reserved1>");
      _sb.AppendLine("    <Reserved2>" + Reserved2.ToString() + "</Reserved2>");
      _sb.AppendLine("    <SasTaxStaus>" + SasTaxStatus.ToString() + "</SasTaxStaus>");
      _sb.AppendLine("  </Conditions>");
      _sb.AppendLine("</WCP_CmdBonusAward>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      TransactionId = XML.GetAsInt64(Xml, "TransactionId");

      CentsToTransferRe = XML.GetAsInt64(Xml, "Re");
      CentsToTransferPromoRe = XML.GetAsInt64(Xml, "PromoRe");
      CentsToTransferPromoNr = XML.GetAsInt64(Xml, "PromoNr");

      Timeout = XML.GetAsInt32(Xml, "Timeout");
      MinBetCents = XML.GetAsInt64(Xml, "MinBetCents");
      NotRedimibleMustBeZero = (XML.GetAsInt32(Xml, "NotRedimibleMustBeZero") != 0);
      TransferFlags = XML.GetAsInt32(Xml, "TransferFlags");
      Reserved1 = XML.GetAsInt32(Xml, "Reserved1");
      Reserved2 = XML.GetAsInt32(Xml, "Reserved2");
      SasTaxStatus = (Byte)XML.GetAsInt32(Xml, "SasTaxStaus");
    }

  }

  public class WCP_CmdBonusStatus : IXml
  {
    public Int64 TransactionId;

    public string ToXml()
    {
      return "<WCP_CmdBonusStatus><TransactionId>" + TransactionId.ToString() + "</TransactionId></WCP_CmdBonusStatus>";
    }
    public void LoadXml(XmlReader Xml)
    {
      TransactionId = XML.GetAsInt64(Xml, "TransactionId");
    }
  }

  public static class MassiveBonus
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Updates the BONUSES table
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 SASBonusStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - BonusTransferStatus
    //
    public static Boolean SaveBonusStatus(DataRow[] Bonus)
    {
      try
      {
        int _num_updated;
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   BONUSES ");
        _sb.AppendLine("   SET   BNS_TRANSFER_STATUS              = @pTransferStatus1 ");
        _sb.AppendLine("       , BNS_SAS_HOST_STATUS              = @pSASHostStatus ");
        _sb.AppendLine("       , BNS_TRANSFERRED_PLAY_SESSION_ID  = @pPlaySessionID");
        _sb.AppendLine("       , BNS_TRANSFERRED_RE               = @pTransferredRE");
        _sb.AppendLine("       , BNS_TRANSFERRED_PROMO_RE         = @pTransferredPromoRE");
        _sb.AppendLine("       , BNS_TRANSFERRED_PROMO_NR         = @pTransferredPromoNR");
        _sb.AppendLine("       , BNS_TRANSFER_STATUS_CHANGED      = CASE WHEN (BNS_TRANSFER_STATUS = @pTransferStatus1) THEN BNS_TRANSFER_STATUS_CHANGED ELSE GETDATE() END ");
        _sb.AppendLine("       , BNS_WCP_CMD_DATETIME             = GETDATE()         ");
        _sb.AppendLine("       , BNS_TRANSFERRED_DATETIME         = @pTransferredDateTime ");
        _sb.AppendLine(" WHERE   BNS_BONUS_ID                     = @pBonusId         ");
        _sb.AppendLine("   AND   BNS_TRANSFER_STATUS              = @pTransferStatus0 ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            using (SqlCommand _cmd_update = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              SqlParameter _param;

              _param = new SqlParameter("@pTransferStatus0", SqlDbType.Int);
              _param.SourceColumn = "BNS_TRANSFER_STATUS";
              _param.SourceVersion = DataRowVersion.Original;
              _cmd_update.Parameters.Add(_param);

              _param = new SqlParameter("@pTransferStatus1", SqlDbType.Int);
              _param.SourceColumn = "BNS_TRANSFER_STATUS";
              _param.SourceVersion = DataRowVersion.Current;
              _cmd_update.Parameters.Add(_param);

              _param = new SqlParameter("@pSASHostStatus", SqlDbType.Int);
              _param.SourceColumn = "BNS_SAS_HOST_STATUS";
              _param.SourceVersion = DataRowVersion.Current;
              _cmd_update.Parameters.Add(_param);

              _param = new SqlParameter("@pPlaySessionID", SqlDbType.Int);
              _param.SourceColumn = "BNS_TRANSFERRED_PLAY_SESSION_ID";
              _param.SourceVersion = DataRowVersion.Current;
              _cmd_update.Parameters.Add(_param);

              _param = new SqlParameter("@pTransferredRE", SqlDbType.Money);
              _param.SourceColumn = "BNS_TRANSFERRED_RE";
              _param.SourceVersion = DataRowVersion.Current;
              _cmd_update.Parameters.Add(_param);

              _param = new SqlParameter("@pTransferredPromoRE", SqlDbType.Money);
              _param.SourceColumn = "BNS_TRANSFERRED_PROMO_RE";
              _param.SourceVersion = DataRowVersion.Current;
              _cmd_update.Parameters.Add(_param);

              _param = new SqlParameter("@pTransferredPromoNR", SqlDbType.Money);
              _param.SourceColumn = "BNS_TRANSFERRED_PROMO_NR";
              _param.SourceVersion = DataRowVersion.Current;
              _cmd_update.Parameters.Add(_param);

              _param = new SqlParameter("@pTransferredDateTime", SqlDbType.DateTime);
              _param.SourceColumn = "BNS_TRANSFERRED_DATETIME";
              _param.SourceVersion = DataRowVersion.Current;
              _cmd_update.Parameters.Add(_param);

              _param = new SqlParameter("@pBonusId", SqlDbType.BigInt);
              _param.SourceColumn = "BNS_BONUS_ID";
              _param.SourceVersion = DataRowVersion.Original;
              _cmd_update.Parameters.Add(_param);

              _da.UpdateCommand = _cmd_update;
              _num_updated = _da.Update(Bonus);

              if (_num_updated == Bonus.Length)
              {
                _db_trx.Commit();

                return true;
              }

              Log.Warning(String.Format("SaveBonusStatus ROLLBACK, NumModified: {0}, NumUpdated: {1}", Bonus.Length.ToString(), _num_updated.ToString()));

              _db_trx.Rollback();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //SaveBonusStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Translates the SAS Host enum ENUM_BONUS_STATUS to BonusTransferStatus
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 SASBonusStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - BonusTransferStatus
    //
    public static BonusTransferStatus SASBonusStatus2BonusTransferStatus(Int32 SASBonusStatus)
    {
      BonusTransferStatus _transfer_status;

      _transfer_status = BonusTransferStatus.UnknownTransactionId;

      switch (SASBonusStatus)
      {
        case 0:   //BONUS_STATUS_UNKNOWN
          _transfer_status = BonusTransferStatus.UnknownTransactionId;
          break;
        case 5:   //BONUS_STATUS_PENDING
        case 20:  //BONUS_STATUS_NOTIFIED_TO_EGM
        case 21:  //BONUS_STATUS_NOTIFIED_TO_EGM_NACK
        case 22:  //BONUS_STATUS_NOTIFIED_TO_EGM_ACK
        case 99:  //BONUS_STATUS_AWARDED_ON_EGM
          _transfer_status = BonusTransferStatus.Notified;
          break;
        case 30:  //BONUS_STATUS_NOTIFIED_TO_EGM_IN_DOUBT
        case 31:  //BONUS_STATUS_NOTIFIED_TO_EGM_NACK_IN_DOUBT 
        case 32:  //BONUS_STATUS_NOTIFIED_TO_EGM_ACK_IN_DOUBT
          _transfer_status = BonusTransferStatus.NotifiedInDoubt;
          break;
        case 10:  //BONUS_STATUS_TIMEOUT
        case 12:  //BONUS_STATUS_SUPPORTED
        case 13:  //BONUS_STATUS_AWARD_IN_PROGRESS
        case 14:  //BONUS_STATUS_INVALID_BONUS_INFO
        case 15:  //BONUS_STATUS_NO_FOUND
          _transfer_status = BonusTransferStatus.CanceledTimeout;
          break;
        case 11:  //BONUS_STATUS_NO_CREDIT
          _transfer_status = BonusTransferStatus.NoCredits;
          break;
        case 100: //BONUS_STATUS_TRANSFERRED
          _transfer_status = BonusTransferStatus.Confirmed;
          break;
        default:
          Log.Error("SASBonusStatus2BonusTransferStatus Error: Unexpected value for SASBonusStatus = " + SASBonusStatus);
          break;
      }

      return _transfer_status;
    } // SASBonusStatus2BonusTransferStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Reads the corresponding bonus reltaed to the given bonus id
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 BonusId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True: No error. False: Error.
    //
    public static Boolean ReadBonuses(Int64 BonusId, out DataTable Bonuses)
    {
      Bonuses = new DataTable("BONUSES");

      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   BNS_BONUS_ID                     ");
        _sb.AppendLine("       , BNS_TO_TRANSFER_RE               ");
        _sb.AppendLine("       , BNS_TO_TRANSFER_PROMO_RE         ");
        _sb.AppendLine("       , BNS_TO_TRANSFER_PROMO_NR         ");
        _sb.AppendLine("       , BNS_SOURCE_TYPE                  ");
        _sb.AppendLine("       , BNS_SOURCE_BIGINT1               ");
        _sb.AppendLine("       , BNS_SOURCE_BIGINT2               ");
        _sb.AppendLine("       , BNS_SOURCE_INT1                  ");
        _sb.AppendLine("       , BNS_SOURCE_INT2                  ");
        _sb.AppendLine("       , BNS_TARGET_TYPE                  ");
        _sb.AppendLine("       , BNS_TARGET_TERMINAL_ID           ");
        _sb.AppendLine("       , BNS_TARGET_ACCOUNT_ID            ");
        _sb.AppendLine("       , BNS_TRANSFER_STATUS              ");
        _sb.AppendLine("       , BNS_SAS_HOST_STATUS              ");
        _sb.AppendLine("       , DATEDIFF (SECOND, isnull (BNS_TRANSFER_STATUS_CHANGED, BNS_INSERTED),  GETDATE()) BNS_ELAPSED ");
        _sb.AppendLine("       , DATEDIFF (SECOND, BNS_WCP_CMD_DATETIME,        GETDATE()) BNS_WCP_CMD_ELAPSED ");
        _sb.AppendLine("       , BNS_TRANSFERRED_RE               ");
        _sb.AppendLine("       , BNS_TRANSFERRED_PROMO_RE         ");
        _sb.AppendLine("       , BNS_TRANSFERRED_PROMO_NR         ");
        _sb.AppendLine("       , BNS_TRANSFERRED_PLAY_SESSION_ID  ");
        _sb.AppendLine("       , BNS_TRANSFERRED_DATETIME         ");
        _sb.AppendLine("  FROM   BONUSES                  ");
        _sb.AppendLine(" WHERE   BNS_BONUS_ID = @pBonusId");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pBonusId", SqlDbType.BigInt).Value = BonusId;
            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _da.Fill(Bonuses);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadBonuses

    //------------------------------------------------------------------------------
    // PURPOSE : Reads the bonuses which their current status matches with the given BonusStatus
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 BonusId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True: No error. False: Error.
    //
    private static Boolean ReadBonuses(BonusTransferStatus BonusStatus, out DataTable Bonuses)
    {
      Bonuses = new DataTable("BONUSES");

      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   BNS_BONUS_ID                     ");
        _sb.AppendLine("       , BNS_TO_TRANSFER_RE               ");
        _sb.AppendLine("       , BNS_TO_TRANSFER_PROMO_RE         ");
        _sb.AppendLine("       , BNS_TO_TRANSFER_PROMO_NR         ");
        _sb.AppendLine("       , BNS_SOURCE_TYPE                  ");
        _sb.AppendLine("       , BNS_SOURCE_BIGINT1               ");
        _sb.AppendLine("       , BNS_SOURCE_BIGINT2               ");
        _sb.AppendLine("       , BNS_SOURCE_INT1                  ");
        _sb.AppendLine("       , BNS_SOURCE_INT2                  ");
        _sb.AppendLine("       , BNS_TARGET_TYPE                  ");
        _sb.AppendLine("       , BNS_TARGET_TERMINAL_ID           ");
        _sb.AppendLine("       , BNS_TARGET_ACCOUNT_ID            ");
        _sb.AppendLine("       , BNS_TRANSFER_STATUS              ");
        _sb.AppendLine("       , BNS_SAS_HOST_STATUS              ");
        _sb.AppendLine("       , DATEDIFF (SECOND, isnull (BNS_TRANSFER_STATUS_CHANGED, BNS_INSERTED),  GETDATE()) BNS_ELAPSED ");
        _sb.AppendLine("       , DATEDIFF (SECOND, BNS_WCP_CMD_DATETIME,        GETDATE()) BNS_WCP_CMD_ELAPSED ");
        _sb.AppendLine("       , BNS_TRANSFERRED_RE               ");
        _sb.AppendLine("       , BNS_TRANSFERRED_PROMO_RE         ");
        _sb.AppendLine("       , BNS_TRANSFERRED_PROMO_NR         ");
        _sb.AppendLine("       , BNS_TRANSFERRED_PLAY_SESSION_ID  ");
        _sb.AppendLine("       , BNS_TRANSFERRED_DATETIME         ");
        _sb.AppendLine("  FROM   BONUSES                          ");
        _sb.AppendLine(" WHERE   BNS_TRANSFER_STATUS = @pTransferStatus0");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pTransferStatus0", SqlDbType.Int).Value = (int)BonusStatus;
            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _da.Fill(Bonuses);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadBonuses


    public static void BonusingThread()
    {
      int _iter;
      int _num_notified;
      int _num_pending;

      _num_pending = 0;
      _num_notified = 0;
      _iter = 0;

      while (true)
      {
        try
        {
          Misc.WaitSeconds(20, true);
          _iter++;

          BonusAwardNotification(out _num_pending);

          if (_iter % 3 == 0)
          {
            BonusRequestStatus(out _num_notified);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Aks the terminal for the pending bonuses status
    //
    //  PARAMS :
    //      - INPUT :

    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static void BonusRequestStatus(out int NumNotified)
    {
      DataTable _notified_bonus;
      DataRow[] _modified_bonus;
      int _terminal_id;
      Int64 _bonus_id;

      NumNotified = -1;

      if (WCP.Server == null)
      {
        // WCP.Server not initialized yet, try it next time;
        return;
      }

      // Read all pending bonuses
      if (!ReadBonuses(BonusTransferStatus.Notified, out _notified_bonus))
      {
        Log.Error("ReadBonuses(Notified) failed!");
        return;
      }

      NumNotified = _notified_bonus.Rows.Count;

      if (_notified_bonus.Rows.Count == 0)
      {
        return;
      }

      // Loop on all pending bonuses
      // -> Determine which ones have to be notified
      foreach (DataRow _bonus in _notified_bonus.Rows)
      {
        _bonus_id = (Int64)_bonus["BNS_BONUS_ID"];
        _terminal_id = (int)_bonus["BNS_TARGET_TERMINAL_ID"];

        if (!WCP.Server.IsConnected(_terminal_id))
        {
          // Perhaps connected to the other server, Skip
          continue;
        }

        // Force register update
        _bonus["BNS_TRANSFER_STATUS"] = (int)BonusTransferStatus.Notified;

        // Send WCP CMD (WCP_CMD_BONUS_STATUS)
        WCP_Message _wcp_request;
        WCP_MsgExecuteCommand _wcp_command;
        WCP_CmdBonusStatus _cmd_bonus_status;
        DataRow _terminal;

        _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);

        _terminal = DbCache.Terminal(_terminal_id);

        // RCI & AJQ 22-MAR-2011: Can't send commands to these terminals (p.ex: RETIRED).
        if (_terminal.IsNull("TE_EXTERNAL_ID"))
        {
          continue;
        }

        _wcp_request.MsgHeader.TerminalId = (String)_terminal["TE_EXTERNAL_ID"];
        _wcp_request.MsgHeader.TerminalSessionId = 0;
        // Execute commands with 0 --> Not inserted into WCP_COMMANDS table (reply not update status).
        _wcp_request.MsgHeader.SequenceId = 0;

        _wcp_command = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
        _wcp_command.Command = WCP_CommandTypes.WCP_CMD_BONUS_STATUS;
        _cmd_bonus_status = new WCP_CmdBonusStatus();
        _cmd_bonus_status.TransactionId = _bonus_id;
        _wcp_command.CommandParameters = _cmd_bonus_status.ToXml();
        WCP.Server.SendTo(_terminal_id, 0, _wcp_request.ToXml());

      } // foreach (_notified_bonus)

      _modified_bonus = _notified_bonus.Select("", "", DataViewRowState.ModifiedCurrent);
      if (_modified_bonus.Length == 0)
      {
        return;
      }

      // Update the status 
      if (!SaveBonusStatus(_modified_bonus))
      {
        Log.Error("SaveBonusStatus(_status_requested) failed!)");
        return;
      }
    } // BonusRequestStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Requests the terminal to award the pending bonuses
    //
    //  PARAMS :
    //      - INPUT :

    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static void BonusAwardNotification(out int NumPending)
    {
      DataTable _pending_bonus;
      DataRow[] _modified_bonus;

      int _target_type;
      int _terminal_id;
      int _elapsed;
      Int64 _bonus_id;

      NumPending = -1;

      if (WCP.Server == null)
      {
        // WCP.Server not initialized yet, try it next time;
        return;
      }

      try
      {
        // Read all pending bonuses
        if (!ReadBonuses(BonusTransferStatus.Pending, out _pending_bonus))
        {
          Log.Error("ReadBonuses(Pending) failed!");
          return;
        }

        NumPending = _pending_bonus.Rows.Count;

        if (_pending_bonus.Rows.Count == 0)
        {
          return;
        }

        // Loop on all pending bonuses
        // -> Determine which ones have to be notified
        foreach (DataRow _bonus in _pending_bonus.Rows)
        {
          _bonus_id = (Int64)_bonus["BNS_BONUS_ID"];

          if (Services.IsPrincipal("WCP"))
          {
            _elapsed = Math.Abs((int)_bonus["BNS_ELAPSED"]);
            if (_elapsed >= 30)
            {
              // Only the "Principal" marks the bonuses as timeout
              _bonus["BNS_TRANSFER_STATUS"] = (int)BonusTransferStatus.CanceledTimeout;
              Log.Warning(String.Format("BonusId: {0}, Timeout", _bonus_id.ToString()));
              continue;
            }

            _target_type = (int)_bonus["BNS_TARGET_TYPE"];
            if (_target_type != 1)
            {
              _bonus["BNS_TRANSFER_STATUS"] = (int)BonusTransferStatus.Error;
              Log.Error(String.Format("BonusId: {0}, Not supported TargetType: {1}", _bonus_id.ToString(), _target_type.ToString()));
              continue;
            }
          }

          _terminal_id = (int)_bonus["BNS_TARGET_TERMINAL_ID"];
          if (!WCP.Server.IsConnected(_terminal_id))
          {
            // Perhaps connected to the other server, Skip
            continue;
          }

          _bonus["BNS_TRANSFER_STATUS"] = BonusTransferStatus.Notified;

        } // foreach (_pending_bonus)

        _modified_bonus = _pending_bonus.Select("", "", DataViewRowState.ModifiedCurrent);
        if (_modified_bonus.Length == 0)
        {
          return;
        }

        // Update the status 
        if (!SaveBonusStatus(_modified_bonus))
        {
          Log.Error("SaveBonusStatus(_modified_bonus) failed!)");
          return;
        }

        // Notify the bonus to the terminals
        foreach (DataRow _bonus in _modified_bonus)
        {
          BonusTransferStatus _bns_status;

          _bns_status = (BonusTransferStatus)_bonus["BNS_TRANSFER_STATUS"];
          if (_bns_status != BonusTransferStatus.Notified)
          {
            continue;
          }
          _terminal_id = (int)_bonus["BNS_TARGET_TERMINAL_ID"];
          if (!WCP.Server.IsConnected(_terminal_id))
          {
            // Before was connected, now is not connected
            _bonus["BNS_TRANSFER_STATUS"] = (int)BonusTransferStatus.Pending;

            continue;
          }

          _bonus_id = (Int64)_bonus["BNS_BONUS_ID"];

          // Send WCP CMD (WCP_CMD_BONUS_STATUS)
          WCP_Message _wcp_request;
          WCP_MsgExecuteCommand _wcp_command;
          WCP_CmdBonusAward _cmd_bonus_award;

          MultiPromos.AccountBalance _bonus_amt;
          // JML 20-Abr-2016 : Convert prize bonus to machine currency
          _bonus_amt = new MultiPromos.AccountBalance();
          _bonus_amt.Redeemable = (Decimal)_bonus["BNS_TO_TRANSFER_RE"];
          _bonus_amt.PromoRedeemable = (Decimal)_bonus["BNS_TO_TRANSFER_PROMO_RE"];
          _bonus_amt.PromoNotRedeemable = (Decimal)_bonus["BNS_TO_TRANSFER_PROMO_NR"];
          if (GeneralParam.GetBoolean("FloorDualCurrency", "Enabled", false))
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              WSI.Common.TITO.HandPay.GetAmountToMachine(_bonus_amt, _terminal_id, out _bonus_amt, _db_trx.SqlTransaction);
            }
          }

          _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);
          _wcp_command = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
          _wcp_command.Command = WCP_CommandTypes.WCP_CMD_BONUS_AWARD;
          _cmd_bonus_award = new WCP_CmdBonusAward();
          _cmd_bonus_award.TransactionId = _bonus_id;
          _cmd_bonus_award.CentsToTransferRe = (Int64)(_bonus_amt.Redeemable * 100);
          _cmd_bonus_award.CentsToTransferPromoRe = (Int64)(_bonus_amt.PromoRedeemable * 100);
          _cmd_bonus_award.CentsToTransferPromoNr = (Int64)(_bonus_amt.PromoNotRedeemable * 100);
          _cmd_bonus_award.SasTaxStatus = 2;  //TODO JMM 29-JAN-2014: Send proper value
          _cmd_bonus_award.TransferFlags = 0;
          _cmd_bonus_award.Timeout = 30000;
          _cmd_bonus_award.Reserved1 = 0;
          _cmd_bonus_award.Reserved2 = 0;
          _wcp_command.CommandParameters = _cmd_bonus_award.ToXml();
          WCP.Server.SendTo(_terminal_id, 0, _wcp_request.ToXml());
        }

        _modified_bonus = _pending_bonus.Select("", "", DataViewRowState.ModifiedCurrent);
        if (_modified_bonus.Length == 0)
        {
          return;
        }

        // Update the status 
        if (!SaveBonusStatus(_modified_bonus))
        {
          Log.Error("SaveBonusStatus(_already_pending) failed!)");
          return;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // BonusAwardNotification

    //------------------------------------------------------------------------------
    // PURPOSE : Processes the WCP_MsgBonusTransferStatus and updates the bonus on the DB
    //
    //  PARAMS :
    //      - INPUT :

    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void DB_UpdateBonus(WCP_MsgBonusTransferStatus MsgBonusTransferInfo)
    {
      DataTable _dr_bonuses;
      BonusTransferStatus _bonus_status;
      MultiPromos.AccountBalance _local_amount;
      MultiPromos.AccountBalance _machine_amount;

      if (MassiveBonus.ReadBonuses(MsgBonusTransferInfo.TransactionId, out _dr_bonuses))
      {
        if (_dr_bonuses.Rows.Count == 1)
        {
          _bonus_status = MassiveBonus.SASBonusStatus2BonusTransferStatus(MsgBonusTransferInfo.Status);

          _local_amount = new MultiPromos.AccountBalance();

          _local_amount.Redeemable = (Decimal)MsgBonusTransferInfo.CentsTransferredRe / 100;
          _local_amount.PromoRedeemable = (Decimal)MsgBonusTransferInfo.CentsTransferredPromoRe / 100;
          _local_amount.PromoNotRedeemable = (Decimal)MsgBonusTransferInfo.CentsTransferredPromoNr / 100;

          if (GeneralParam.GetBoolean("FloorDualCurrency", "Enabled", false))
          {
            _machine_amount = new MultiPromos.AccountBalance();

            _local_amount.Redeemable = (Decimal)_dr_bonuses.Rows[0]["BNS_TO_TRANSFER_RE"];
            _local_amount.PromoRedeemable = (Decimal)_dr_bonuses.Rows[0]["BNS_TO_TRANSFER_PROMO_RE"];
            _local_amount.PromoNotRedeemable = (Decimal)_dr_bonuses.Rows[0]["BNS_TO_TRANSFER_PROMO_NR"];

            using (DB_TRX _db_trx = new DB_TRX())
            {
              WSI.Common.TITO.HandPay.GetAmountToMachine(_local_amount, ((Int32)(_dr_bonuses.Rows[0]["bns_target_terminal_id"])), out _machine_amount, _db_trx.SqlTransaction);
            }

            // 20-Abr-2016 JML   If amount acepted by machine is equal to original amount send to machine then we don't do conversions and save the original national amount
            //                      else the acepted amount is converted to national currency for save 
            if (!(_machine_amount.Redeemable == ((Decimal)MsgBonusTransferInfo.CentsTransferredRe / 100)
              && _machine_amount.PromoRedeemable == ((Decimal)MsgBonusTransferInfo.CentsTransferredPromoRe / 100)
              && _machine_amount.PromoNotRedeemable == ((Decimal)MsgBonusTransferInfo.CentsTransferredPromoNr / 100)))
            {
              _machine_amount.Redeemable = (Decimal)MsgBonusTransferInfo.CentsTransferredRe / 100;
              _machine_amount.PromoRedeemable = (Decimal)MsgBonusTransferInfo.CentsTransferredPromoRe / 100;
              _machine_amount.PromoNotRedeemable = (Decimal)MsgBonusTransferInfo.CentsTransferredPromoNr / 100;
              using (DB_TRX _db_trx = new DB_TRX())
              {
                WSI.Common.TITO.HandPay.GetAmountFromMachine(_machine_amount, ((Int32)(_dr_bonuses.Rows[0]["bns_target_terminal_id"])), out _local_amount, _db_trx.SqlTransaction);
              }
            }
          }
      

          _dr_bonuses.Rows[0]["BNS_TRANSFER_STATUS"] = _bonus_status;
          _dr_bonuses.Rows[0]["BNS_SAS_HOST_STATUS"] = MsgBonusTransferInfo.Status;
          _dr_bonuses.Rows[0]["BNS_TRANSFERRED_RE"] = _local_amount.Redeemable;
          _dr_bonuses.Rows[0]["BNS_TRANSFERRED_PROMO_RE"] = _local_amount.PromoRedeemable;
          _dr_bonuses.Rows[0]["BNS_TRANSFERRED_PROMO_NR"] = _local_amount.PromoNotRedeemable;
          _dr_bonuses.Rows[0]["BNS_TRANSFERRED_PLAY_SESSION_ID"] = MsgBonusTransferInfo.TransferredToPlaySessionId;
          
          if (MsgBonusTransferInfo.CentsTransferredRe + MsgBonusTransferInfo.CentsTransferredPromoRe + MsgBonusTransferInfo.CentsTransferredPromoNr != 0)
          {
            _dr_bonuses.Rows[0]["BNS_TRANSFERRED_DATETIME"] = WGDB.Now;
          }

          MassiveBonus.SaveBonusStatus(_dr_bonuses.Select());
        }
        else
        {
          Log.Error(String.Format(" DB_UpdateBonus -> {0} bonuses found for TransactionId = {1} ", _dr_bonuses.Rows.Count, MsgBonusTransferInfo.TransactionId));
        }
      }
      else
      {
        Log.Error(String.Format(" DB_UpdateBonus -> An error occurred calling ReadBonuses for TransactionId = {0} ", MsgBonusTransferInfo.TransactionId));
      }
    } // DB_UpdateBonus
  }
}
