//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgVersionSignature.cs
// 
//   DESCRIPTION: WCP_MsgVersionSignature class
// 
//        AUTHOR: Jordi C�rdoba
// 
// CREATION DATE: 01-APR-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-APR-2014 JCOR   First release.
// 08-APR-2014 JCOR   Added SWValidationProcessMsg.
// 15-APR-2014 JCOR   Added GetTerminalName.
// 24-APR-2014 JCOR   Added ThrowAlarm.
//------------------------------------------------------------------------------

using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Data;

namespace WSI.WCP
{
  public class WCP_MsgVersionSignature : IXml
  {
    public Int64 ValitadionID;
    public Int32 ReceivedStatus;
    public Int32 Method;
    public String Seed;
    public String Signature;

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgVersionSignature>");
      _sb.AppendLine("  <Method>" + Method + "</Method>");
      _sb.AppendLine("  <Seed>" + Seed + "</Seed>");
      _sb.AppendLine("  <Signature>" + Signature + "</Signature>");
      _sb.AppendLine("  <ValitadionID>" + ValitadionID + "</ValitadionID>");
      _sb.AppendLine("  <ReceivedStatus>" + ReceivedStatus + "</ReceivedStatus>");
      _sb.AppendLine("</WCP_MsgVersionSignature>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_MsgVersionSignature")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgVersionSignature not found in content area");
      }

      Method = XML.GetAsInt32(Xml, "Method");
      Seed = XML.GetValue(Xml, "Seed");
      Signature = XML.GetValue(Xml, "Signature");
      ValitadionID = XML.GetAsInt64(Xml, "ValitadionID");
      ReceivedStatus = XML.GetAsInt32(Xml, "ReceivedStatus");
    }
  }

  public class WCP_MsgVersionSignatureReply : IXml
  {
    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgVersionSignatureReply>");
      _sb.AppendLine("</WCP_MsgVersionSignatureReply>");
      return _sb.ToString();
    }


    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_MsgVersionSignatureReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgVersionSignature not found in content area");
      }
    }
  }

  public class WCP_CmdSwValidation : IXml
  {
    public Int64 ValitadionID;
    public Int32 Method;
    public String Seed;


    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_CmdSwValidation>");
      _sb.AppendLine("  <ValitadionID>" + ValitadionID + "</ValitadionID>");
      _sb.AppendLine("  <Method>" + Method + "</Method>");
      _sb.AppendLine("  <Seed>" + Seed + "</Seed>");
      _sb.AppendLine("</WCP_CmdSwValidation>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      ValitadionID = XML.GetAsInt64(Xml, "ValitadionID");
      Method = XML.GetAsInt32(Xml, "Method");
      Seed = XML.GetValue(Xml, "Seed");
    }
  }

  public static class SwValidation
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Send the WCP Command to the terminal requesting the Version Signature
    // 
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId
    //        - ValidatdionID
    //        - Method
    //        - Seed
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    //
    public static void WCP_CmdVersionSignature(Int32 TerminalId, Int64 ValidatdionID, ENUM_AUTHENTICATION_METHOD Method, String Seed)
    {
      // Send WCP CMD (WCP_CMD_REQUEST_SOFTWARE_VALIDATION)
      WCP_Message _wcp_request;
      WCP_MsgExecuteCommand _wcp_command;
      WCP_CmdSwValidation _cmd_sw_valitation;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);
      _wcp_command = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
      _wcp_command.Command = WCP_CommandTypes.WCP_CMD_REQUEST_SOFTWARE_VALIDATION;
      _cmd_sw_valitation = new WCP_CmdSwValidation();

      _cmd_sw_valitation.ValitadionID = ValidatdionID;
      _cmd_sw_valitation.Method = (Int32)Method;
      _cmd_sw_valitation.Seed = Seed;

      _wcp_command.CommandParameters = _cmd_sw_valitation.ToXml();

      WCP.Server.SendTo(TerminalId, 0, _wcp_request.ToXml());
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Requests the terminal ROM Signature
    //
    //  PARAMS :
    //      - INPUT :

    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static void SWValidationNotification(out int NumPending)
    {
      DataTable _pending_sw_validation;
      DataRow[] _modified_sw_validation;      
      int _terminal_id;
      int _elapsed;
      Int64 _validation_id;
      String _seed;
      SoftwareValidationStatus _sw_validation_status;

      NumPending = -1;

      if (WCP.Server == null)
      {
        // WCP.Server not initialized yet, try it next time;
        return;
      }

      try
      {
        // Read all pending software validations
        if (!SoftwareValidations.ReadSoftwareValidationByStatus(SoftwareValidationStatus.Pending, out _pending_sw_validation))
        {
          Log.Error("GetSoftwareValidationByStatus(Pending) failed!");
          return;
        }

        NumPending = _pending_sw_validation.Rows.Count;

        if (_pending_sw_validation.Rows.Count == 0)
        {
          return;
        }

        // Loop on all pending software validation requests
        // -> Determine which ones have to be notified
        foreach (DataRow _sw_validation in _pending_sw_validation.Rows)
        {
          _validation_id = (Int64)_sw_validation["SVAL_VALIDATION_ID"];

          if (Services.IsPrincipal("WCP"))
          {
            _elapsed = Math.Abs((int)_sw_validation["SVAL_ELAPSED"]);
            if (_elapsed >= 30)
            {
              // Only the "Principal" marks the bonuses as timeout
              _sw_validation["SVAL_STATUS"] = (int)SoftwareValidationStatus.CanceledTimeout;
              Log.Warning(String.Format("ValidationId: {0}, Timeout", _validation_id.ToString()));
              continue;
            }
          }

          _terminal_id = (int)_sw_validation["SVAL_TERMINAL_ID"];
          if (!WCP.Server.IsConnected(_terminal_id))
          {
            // Perhaps connected to the other server, Skip
            continue;
          }

          _sw_validation["SVAL_STATUS"] = SoftwareValidationStatus.Notified;

        } // foreach (_sw_validation)

        _modified_sw_validation = _pending_sw_validation.Select("", "", DataViewRowState.ModifiedCurrent);
        if (_modified_sw_validation.Length == 0)
        {
          return;
        }

        // Update the status 
        if (!SoftwareValidations.SaveSoftwareValidation(_modified_sw_validation))
        {
          Log.Error("UpdateSoftwareValidation(_modified_sw_validation) failed!)");
          return;
        }

        // Notify the software validation request to the terminals
        foreach (DataRow _sw_validation in _modified_sw_validation)
        {
          _sw_validation_status = (SoftwareValidationStatus)_sw_validation["SVAL_STATUS"];

          if (_sw_validation_status != SoftwareValidationStatus.Notified)
          {
            continue;
          }

          _terminal_id = (int)_sw_validation["SVAL_TERMINAL_ID"];
          if (!WCP.Server.IsConnected(_terminal_id))
          {
            // Before was connected, now is not connected
            _sw_validation["SVAL_STATUS"] = (int)SoftwareValidationStatus.Pending;

            continue;
          }

          _validation_id = (Int64)_sw_validation["SVAL_VALIDATION_ID"];
          _seed = (String)_sw_validation["SVAL_SEED"];

          // Send WCP CMD (WCP_CMD_REQUEST_SOFTWARE_VALIDATION)
          WCP_CmdVersionSignature(_terminal_id, _validation_id, ENUM_AUTHENTICATION_METHOD.ROMCRC16, _seed);
        }

        _modified_sw_validation = _pending_sw_validation.Select("", "", DataViewRowState.ModifiedCurrent);
        if (_modified_sw_validation.Length == 0)
        {
          return;
        }

        // Update the status 
        if (!SoftwareValidations.SaveSoftwareValidation(_modified_sw_validation))
        {
          Log.Error("UpdateSoftwareValidation(_modified_sw_validation) failed!)");
          return;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // SWValidationNotification

    //------------------------------------------------------------------------------
    // PURPOSE : Aks the terminal for the pending software validation request status
    //
    //  PARAMS :
    //      - INPUT :

    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static void SWValidationStatus(out int NumNotified)
    {
      DataTable _notified_sw_validations;
      DataRow[] _modified_sw_validation;
      int _terminal_id;
      Int64 _validation_id;
      String _seed;

      NumNotified = -1;

      if (WCP.Server == null)
      {
        // WCP.Server not initialized yet, try it next time;
        return;
      }

      // Read all pending software validation requests
      if (!SoftwareValidations.ReadSoftwareValidationByStatus(SoftwareValidationStatus.Notified, out _notified_sw_validations))
      {
        Log.Error("GetSoftwareValidationByStatus(Notified) failed!");
        return;
      }

      NumNotified = _notified_sw_validations.Rows.Count;

      if (_notified_sw_validations.Rows.Count == 0)
      {
        return;
      }

      // Loop on all pending software validation requests
      // -> Determine which ones have to be notified
      foreach (DataRow _sw_validation in _notified_sw_validations.Rows)
      {
        _validation_id = (Int64)_sw_validation["SVAL_VALIDATION_ID"];
        _terminal_id = (int)_sw_validation["SVAL_TERMINAL_ID"];

        if (!WCP.Server.IsConnected(_terminal_id))
        {
          // Perhaps connected to the other server, Skip
          continue;
        }

        // Force register update
        _sw_validation["SVAL_STATUS"] = (int)SoftwareValidationStatus.Notified;
        _seed = (String)_sw_validation["SVAL_SEED"];

        // To ask for the software validation status querying the same validation id again
        // Send WCP CMD (WCP_CMD_REQUEST_SOFTWARE_VALIDATION)
        WCP_CmdVersionSignature(_terminal_id, _validation_id, ENUM_AUTHENTICATION_METHOD.ROMCRC16, _seed);

      } // foreach (_sw_validation)

      _modified_sw_validation = _notified_sw_validations.Select("", "", DataViewRowState.ModifiedCurrent);
      if (_modified_sw_validation.Length == 0)
      {
        return;
      }

      // Update the status 
      if (!SoftwareValidations.SaveSoftwareValidation(_modified_sw_validation))
      {
        Log.Error("UpdateSoftwareValidation(_modified_sw_validation) failed!)");
        return;
      }
    } // SWValidationStatus

    public static void SWValidationThread()
    {
      int _iter;
      int _num_notified;
      int _num_pending;

      _num_pending = 0;
      _num_notified = 0;
      _iter = 0;

      while (true)
      {
        try
        {
          Misc.WaitSeconds(5, true);
          _iter++;

          SWValidationNotification(out _num_pending);

          if (_iter % 10 == 0)
          {
            SWValidationStatus(out _num_notified);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      }
    }


    static SoftwareValidationStatus ConvertStatus(SoftwareValidationStatusLKT Status)
    {
      switch (Status)
      {
        case SoftwareValidationStatusLKT.UNKNOWN:
          return SoftwareValidationStatus.Unknown;
        case SoftwareValidationStatusLKT.PENDING:
        case SoftwareValidationStatusLKT.REQUESTED_TO_EGM:
        case SoftwareValidationStatusLKT.REQUESTED_TO_EGM_ACK:
        case SoftwareValidationStatusLKT.REQUESTED_TO_EGM_NACK:
        case SoftwareValidationStatusLKT.ANSWERED_FROM_EGM:
          return SoftwareValidationStatus.Notified;
        case SoftwareValidationStatusLKT.ANSWERED:
          return SoftwareValidationStatus.ConfirmedNonValidated;
        case SoftwareValidationStatusLKT.TIMEOUT:
          return SoftwareValidationStatus.CanceledTimeout;
        default:
          return SoftwareValidationStatus.Unknown;
      }
    } // SoftwareValidationStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the terminal name from an ID.
    //
    //  PARAMS :
    //      - INPUT :
    //        - TerminalID.
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String : Terminal's name.
    //
    private static String GetTerminalName (Int32 TerminalID)
    {      
      String _provider_id;
      String _terminal_name;
      long _sesion_id;

      Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalID, out _sesion_id);
      Common.BatchUpdate.TerminalSession.GetData(TerminalID, _sesion_id, out _provider_id, out _terminal_name);
      return Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);
    } // GetTerminalName

    //------------------------------------------------------------------------------
    // PURPOSE : Throws alarms of version signature.
    //
    //  PARAMS :
    //      - INPUT :
    //        - IsStatusError.
    //        - Source_name.
    //        - Terminal_id.
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void ThrowAlarm(Boolean IsStatusError, String Source_name, Int32 Terminal_id, SqlTransaction SqlTrx)
    {
      String _alarm_msg; 
      UInt32 _alarm_code;
      AlarmSeverity _alarm_severity;    

      if ( IsStatusError )
      {
        _alarm_code = (UInt32)AlarmCode.WCP_SwValidationError;
        _alarm_msg = Resource.String("STR_EA_0x00110011", Source_name);
        _alarm_severity = AlarmSeverity.Error;
      }
      else
      {
        _alarm_code = (UInt32) AlarmCode.WCP_SwValidationOK;
        _alarm_msg = Resource.String("STR_EA_0x00110012", Source_name);
        _alarm_severity = AlarmSeverity.Info;
      }

      Alarm.Register(AlarmSourceCode.TerminalWCP,
                     Terminal_id,
                     Source_name,
                     _alarm_code,
                     _alarm_msg,
                     _alarm_severity,
                     DateTime.MinValue,
                     SqlTrx);

    } // ThrowAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Process the incoming message.
    //
    //  PARAMS :
    //      - INPUT :
    //        - WCP_MsgVersionSignature: Request.
    //        - Int64: ValidationID.
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: All went OK; False: otherwise
    //
    public static void SWValidationProcessMsg(WCP_MsgVersionSignature Request, Int64 ValidationID, SqlTransaction SqlTrx)
    {
      SoftwareValidationStatus _sw_validation_status;
      DataTable _dt_signature_validation;
      DataRow[] _signature;
      DataTable _sw_validation;
      DataRow _sw_validation_row;
      Int32 _terminal_id;
      String  _source_name;
      Boolean _is_status_error;

      
      if ( !SoftwareValidations.ReadSoftwareValidationByID(ValidationID, out _sw_validation, SqlTrx) )
      {
        return;
      }

      _sw_validation_status = ConvertStatus((SoftwareValidationStatusLKT)Request.ReceivedStatus);       

      if (_sw_validation.Rows.Count == 0)
      {
        Log.Error(" WCP_MsgVersionSignature. The software validation ID doesn't exist.");
        return;
      }

      _sw_validation_row = _sw_validation.Rows[0];
      _sw_validation_row["SVAL_STATUS"] = _sw_validation_status;
      _sw_validation_row["SVAL_RECEIVED_STATUS"] = Request.ReceivedStatus;
      _sw_validation_row["SVAL_RECEIVED_SIGNATURE"] = Request.Signature;
      _terminal_id = (Int32)_sw_validation_row["SVAL_TERMINAL_ID"];

      if ( !SoftwareValidations.SaveSoftwareValidation(_sw_validation.Select(), SqlTrx) )
      {
        return;
      }

      switch (_sw_validation_status)
      {
        case SoftwareValidationStatus.ConfirmedNonValidated:

          _source_name = GetTerminalName(_terminal_id);

          Log.Message("*** Software validation received: Terminal: " + _source_name + " (ID: " + _terminal_id.ToString() + ")" + ", ValidationID: " + Request.ValitadionID.ToString() + ", Seed: "
                      + Request.Seed + ", Signature: " + Request.Signature);

          // Reads what signature we have in DB.
          if ( !SoftwareValidations.ReadSignatureValidation(_terminal_id, out _dt_signature_validation, SqlTrx) )
          {
            return;
          }

          _signature = _dt_signature_validation.Select("TE_AUTHENTICATION_SIGNATURE = '" + Request.Signature + "'");

          if (_signature.Length == 1)
          {
            // Status = OK.
            if ( !SoftwareValidations.SaveAuthenticationStatus(_terminal_id, ENUM_AUTHENTICATION_STATUS.OK, SqlTrx) )
            {
              return;
            }
            
            _sw_validation_row["SVAL_STATUS"] = SoftwareValidationStatus.ConfirmedOK;

            _is_status_error = false;

          }
          else
          {
            // Status = Error. 
            if ( !SoftwareValidations.SaveAuthenticationStatus(_terminal_id, ENUM_AUTHENTICATION_STATUS.ERROR, SqlTrx) ) // TERMINAL
            {
              return;
            }

            _sw_validation_row["SVAL_STATUS"] = SoftwareValidationStatus.ConfirmedError;

            _is_status_error = true;

          } // if

          if (!SoftwareValidations.SaveSoftwareValidation(_sw_validation.Select(), SqlTrx))
          {
            return;
          }

          ThrowAlarm(_is_status_error, _source_name, _terminal_id, SqlTrx);

          break; // Confirmed

        case SoftwareValidationStatus.Unknown:
          Log.Error("Validation status is unknown!");
          return; // Unknown

        case SoftwareValidationStatus.CanceledTimeout:
        case SoftwareValidationStatus.Error:
        case SoftwareValidationStatus.Notified:
        case SoftwareValidationStatus.Pending:
          return;
        default:
          return;
      }
    }
  }
}
