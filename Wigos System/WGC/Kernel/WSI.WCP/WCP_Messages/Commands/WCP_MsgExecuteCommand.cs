//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgExecuteCommand.cs
// 
//   DESCRIPTION: WCP_MsgExecuteCommand class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgExecuteCommand : IXml
  {
    public WCP_CommandTypes Command;
    public String CommandParameters;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgExecuteCommand", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "Command", Command.ToString ());
      XML.AppendChild(node, "CommandParameters", CommandParameters);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgExecuteCommand")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgExecuteCommand not found in content area");
      }
      else
      {
        Command = (WCP_CommandTypes)Enum.Parse(typeof(WCP_CommandTypes), XML.GetValue(reader, "Command"), true);
        CommandParameters = XML.GetValue(reader, "CommandParameters");
      }
    }
  }

  public class WCP_MsgExecuteCommandReply : IXml
  {
    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgExecuteCommandReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgExecuteCommandReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgExecuteCommandReply not found in content area");
      }
    }

  }
}
