//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgKeepAlive.cs
// 
//   DESCRIPTION: WCP_MsgKeepAlive class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 15-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgError : IXml
  {
    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgError", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    }


    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgError")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgError not found in content area");
      }
    }
  }
}