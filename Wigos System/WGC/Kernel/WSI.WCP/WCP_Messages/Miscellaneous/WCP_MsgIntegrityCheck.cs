//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgIntegrityCheck.cs
// 
//   DESCRIPTION: WCP_MsgIntegrityCheck class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 15-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;


namespace WSI.WCP
{
  public class WCP_MsgIntegrityCheck : IXml
  {
    public UInt32 Crc32;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgIntegrityCheck", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "CRC32", Crc32.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgIntegrityCheck")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgIntegrityCheck not found in content area");
      }
      else
      {
        Crc32 = UInt32.Parse(XML.GetValue(reader, "CRC32"));
      }
    }
  }

  public class WCP_MsgIntegrityCheckReply : IXml
  {
    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgIntegrityCheckReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      
      if (reader.Name != "WCP_MsgIntegrityCheckReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgIntegrityCheckReply not found in content area");
      }
    }
  }
}
