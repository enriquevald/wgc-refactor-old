//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgReserveTerminal.cs
// 
//   DESCRIPTION: WCP_MsgReserveTerminal class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 01-FEB-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-FEB-2017 JMM    First release.
// 02-AUG-2017 JMM    PBI 28983: EGM Reserve - Phase 1 - Reserve on EGM by Carded Player
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgReserveTerminal : IXml
  {
    private String m_trackdata;

    public String TrackData
    {
      get
      {
        return m_trackdata;
      }
      set
      {
        m_trackdata = value;
      }
    }

    public String ToXml()
    {
      XmlDocument _xml;
      XmlNode _node;
      XmlAttribute _attribute;

      _xml = new XmlDocument();

      _node = _xml.CreateNode(XmlNodeType.Element, "WCP_MsgReserveTerminal", "");
      _xml.AppendChild(_node);

      _attribute = _node.OwnerDocument.CreateAttribute("TrackData");
      _attribute.Value = TrackData;
      _node.Attributes.Append(_attribute);

      return _xml.OuterXml;
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgReserveTerminal")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReserveTerminal not found in content area");
      }
      else
      {
        while (Reader.Name != "WCP_MsgReserveTerminal" || !Reader.NodeType.Equals(XmlNodeType.Element))
        {
          Reader.Read();  // Read until End Element 'WCP_MsgReserveTerminal'
        }
        
        TrackData = Reader.GetAttribute("TrackData");
      }
    }

  }

  public class WCP_MsgReserveTerminalReply : IXml
  {
    private Int32 m_reserve_error;
    private String m_reserve_error_msg;
    private Int32 m_expiration_minutes;

    public Int32 ReserveError
    {
      get
      {
        return m_reserve_error;        
      }
      set
      {
        m_reserve_error = value;
      }
    }

    public String ReserveErrorMsg
    {
      get
      {
        return m_reserve_error_msg;
      }
      set
      {
        m_reserve_error_msg = value;
      }
    }

    public Int32 ExpirationMinutes
    {
      get
      {
        return m_expiration_minutes;
      }
      set
      {
        m_expiration_minutes = value;
      }
    }


    public String ToXml()
    {
      XmlDocument _xml;
      XmlNode _node;
      XmlAttribute _attribute;

      _xml = new XmlDocument();

      _node = _xml.CreateNode(XmlNodeType.Element, "WCP_MsgReserveTerminalReply", "");
      _xml.AppendChild(_node);

      _attribute = _node.OwnerDocument.CreateAttribute("ReserveError");
      _attribute.Value = ReserveError.ToString();
      _node.Attributes.Append(_attribute);

      _attribute = _node.OwnerDocument.CreateAttribute("ReserveErrorMsg");
      _attribute.Value = ReserveErrorMsg.ToString();
      _node.Attributes.Append(_attribute);

      _attribute = _node.OwnerDocument.CreateAttribute("ExpirationMinutes");
      _attribute.Value = ExpirationMinutes.ToString();
      _node.Attributes.Append(_attribute);      

      return _xml.OuterXml;
    }

    public void LoadXml(XmlReader Reader)
    {
      ExpirationMinutes = 0;

      if (Reader.Name != "WCP_MsgReserveTerminalReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReserveTerminalReply not found in content area");
      }
      else
      {        
        while (Reader.Name != "WCP_MsgReserveTerminalReply" || !Reader.NodeType.Equals(XmlNodeType.Element))
        {
          Reader.Read();  // Read until End Element 'WCP_MsgReserveTerminalReply'
        }

        ReserveError = Int32.Parse(Reader.GetAttribute("ReserveError"));
        ReserveErrorMsg = Reader.GetAttribute("ReserveErrorMsg");

        //Read ExpirationMinutes.  If server is running an old version, there is no a ExpirationMinutes tag
        if (null != Reader.GetAttribute("ExpirationMinutes"))
        {
          ExpirationMinutes = Int32.Parse(Reader.GetAttribute("ExpirationMinutes"));
        }
      }
    }
  }
}
