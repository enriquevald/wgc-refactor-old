//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgGetLogger.cs
// 
//   DESCRIPTION: WCP_MsgGetLogger class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 20-MAY-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-MAY-2010 ACC    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;
using System.IO;
using System.IO.Compression;

namespace WSI.WCP
{
  public class WCP_MsgGetLogger : IXml
  {
    #region IXml Members

    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgGetLogger", "");
      xml.AppendChild(node);

      return xml.OuterXml; 
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgGetLogger")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetLogger not found in content area");
      }
    }

    #endregion
  }

  public class WCP_MsgGetLoggerReply : IXml
  {
    private String m_deflated_messages;

    public String LoggerMessages 
    {
      get
      {
        return m_deflated_messages;
      }
      set
      {
        Misc.DeflateString(value, out m_deflated_messages);
      }
    }

    #region IXml Members

    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_MsgGetLoggerReply></WCP_MsgGetLoggerReply>");
      node = xml.SelectSingleNode("WCP_MsgGetLoggerReply");

      XML.AppendChild(node, "CompressedLoggerMessages", m_deflated_messages);

      return xml.OuterXml;
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgGetLoggerReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetLoggerReply not found in content area");
      }
      else
      {
        m_deflated_messages = XML.GetValue(Reader, "CompressedLoggerMessages");

        Reader.Close();
      }
    }

    #endregion
  }
}
