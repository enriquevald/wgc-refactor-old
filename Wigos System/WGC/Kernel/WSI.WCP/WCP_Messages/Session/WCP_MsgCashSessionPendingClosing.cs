//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgCashSessionPendingClosing.cs
// 
//   DESCRIPTION: WCP_MsgCashSessionPendingClosing class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 24-MAY-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-MAY-2013 JMM    First version.
// 28-NOV-2013 NMR    Added last SAS meters process (when stacker is extracted)
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgCashSessionPendingClosing : IXml
  {
    protected static String MESSAGE_ENVELOPE = "WCP_MsgCashSessionPendingClosing";
    protected static String METERS_COMMON_NODE = "SAS_Meters";
    protected static String METERS_NODE = "SAS_Meter";

    // ACC 13-04-2015 Add TransactionId
    public Int64 TransactionId = 0;

    public Int64 NewStackerId;
    public ArrayList LastSasMeters;   // last meters reported by SAS HOST
    public DateTime ChangeDatetime;
    public Boolean ReadMeters;

    public WCP_MsgCashSessionPendingClosing()
    {
      NewStackerId = 0;
      LastSasMeters = new ArrayList();
      ChangeDatetime = DateTime.Now;
    } // WCP_MsgCashSessionPendingClosing

    public void SetTransactionId(Int64 Value)
    {
      TransactionId = Value;
    }

    public Int64 GetTransactionId()
    {
      return TransactionId;
    }

    public String ToXml()
    {
      StringBuilder _sb;
      String _attributes;
      Int32 _counter;
      
      _sb = new StringBuilder();
      _sb.AppendLine("<" + MESSAGE_ENVELOPE + ">");
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "NewStackerId", NewStackerId));

      //
      // add SAS Meter items from array to common Node 'SAS_Meters'
      // each element will under tag 'SAS_Meter'; valid data will stored as attributes
      // format of this part will as follows:
      // <Meters>
      //    <Meter Code="15" GameId="18" DenomCents="2000" Value="198" MaxValue="999999" >1</Meter>
      //    <Meter Code="15" GameId="18" DenomCents="2000" Value="198" MaxValue="999999" >2</Meter>
      // </Meters>
      //
      _counter = 0;
      _sb.AppendLine("<" + METERS_COMMON_NODE + ">");
      foreach (SAS_Meter _meter in LastSasMeters)
      {
        // concatenate values in ATTRIBUTE format
        _attributes = " Code=\"" + _meter.Code.ToString() + "\" ";
        _attributes += "GameId=\"" + _meter.GameId.ToString() + "\" ";
        _attributes += "DenomCents=\"" + _meter.DenomCents.ToString() + "\" ";
        _attributes += "Value=\"" + _meter.Value.ToString() + "\" ";
        _attributes += "MaxValue=\"" + _meter.MaxValue.ToString() + "\" ";

        // add the METER node
        _sb.AppendLine("<" + METERS_NODE + _attributes + ">" + (++_counter) + "</" + METERS_NODE + ">");
      }
      _sb.AppendLine("</" + METERS_COMMON_NODE + ">");

      // add datetime
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "ChangeDatetime", XML.XmlDateTimeString(ChangeDatetime)));

      // ACC 13-04-2015 Add TransactionId
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "TransactionId", TransactionId));

      // JMM 17-11-2015 Add ReadMeters
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "ReadMeters", ReadMeters ? "1" : "0"));

      _sb.AppendLine("</" + MESSAGE_ENVELOPE + ">");

      return _sb.ToString();

    } // ToXml

    public void LoadXml(XmlReader Reader)
    {
      //String _stacker_id;
      SAS_Meter _meter;
      XmlDocument _doc;
      XmlNodeList _nodes_list;
      Int64 _transaction_id;
      String _value;

      if (!Reader.Name.Equals(MESSAGE_ENVELOPE))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCashSessionPendingClosing not found in content area");
      }

      _doc = new XmlDocument();
      _doc.Load(Reader);

      _value = XML.GetValue(_doc.FirstChild, "NewStackerId");
      if (_value == String.Empty)
      {
        return;
      }
      NewStackerId = Int64.Parse(_value);

      LastSasMeters.Clear();
      _nodes_list = _doc.GetElementsByTagName(METERS_NODE);
      foreach (XmlNode _node in _nodes_list)
      {
        _meter = new SAS_Meter();
        _meter.Code = Int32.Parse(XML.GetAttributeValue(_node, "Code"));
        _meter.GameId = Int32.Parse(XML.GetAttributeValue(_node, "GameId"));
        _meter.DenomCents = Decimal.Parse(XML.GetAttributeValue(_node, "DenomCents"));
        _meter.Value = Int64.Parse(XML.GetAttributeValue(_node, "Value"));
        _meter.MaxValue = Int64.Parse(XML.GetAttributeValue(_node, "MaxValue"));

        LastSasMeters.Add(_meter);
      }

      DateTime.TryParse(XML.GetValue(_doc.FirstChild, "ChangeDatetime"), out ChangeDatetime);

      // ACC 13-04-2015 Add TransactionId
      _value = XML.GetValue(_doc.FirstChild, "TransactionId");
      if (_value != String.Empty)
      {
        Int64.TryParse(XML.GetValue(_doc.FirstChild, "TransactionId"), out _transaction_id);
        SetTransactionId(_transaction_id);
      }

      // JMM 17-11-2015 Add ReadMeters
      ReadMeters = (XML.GetValue(_doc.FirstChild, "ReadMeters") == "1");

    } // LoadXml
  } // public class WCP_MsgCashSessionPendingClosing

  public class WCP_MsgCashSessionPendingClosingReply : IXml
  {
    public Int32 Result;
    public String AlreadyInsertedTerminalName = "";

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgCashSessionPendingClosingReply", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "PendingClosingResult", Result.ToString());
      XML.AppendChild(node, "AlreadyInsertedTerminalName", AlreadyInsertedTerminalName);      

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgCashSessionPendingClosingReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCashSessionPendingClosingReply not found in content area");
      }

      Result = Int32.Parse(XML.GetValue(reader, "PendingClosingResult"));
      
      // ACC 27-APR-2015 Add already inserted Terminal name.
      AlreadyInsertedTerminalName = XML.GetValue(reader, "AlreadyInsertedTerminalName", true);

    } // LoadXml

  } // public class WCP_MsgCashSessionPendingClosingReply
} // namespace WSI.WCP
