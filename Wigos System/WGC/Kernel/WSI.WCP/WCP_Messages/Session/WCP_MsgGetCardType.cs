//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgGetCardType.cs
// 
//   DESCRIPTION: WCP_MsgGetCardType class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgGetCardType : IXml
  {
    ArrayList Track;

    public WCP_MsgGetCardType ()
    {
      Track = new ArrayList ();
    }

    public void SetTrackData (int TrackIndex, String TrackData)
    {
      while ( Track.Count <= TrackIndex )
      {
        Track.Add ("");
      }
      Track[TrackIndex] = TrackData;
    }

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode track;

      xml = new XmlDocument();
      xml.LoadXml ("<WCP_MsgGetCardType><Card /></WCP_MsgGetCardType>");


      node = xml.SelectSingleNode ("WCP_MsgGetCardType/Card");

      for (int track_idx = 0; track_idx < Track.Count; track_idx++)
      {
        track = xml.CreateNode (XmlNodeType.Element, "Track", "");
        
        node.AppendChild (track);

        XML.AppendChild (track, "TrackIndex", track_idx.ToString ());
        XML.AppendChild (track, "TrackData", Track[track_idx].ToString ());

      }

      return xml.OuterXml;
    }


    public void LoadXml(XmlReader reader)
    {
      bool endCard;
      if (reader.Name == "WCP_MsgGetCardType")
      {
        endCard = false;
        while (reader.Name != "Card" && (reader.NodeType.Equals(XmlNodeType.Element)))
        {
          if (!reader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetCardType Card element missing");
          }
        }
        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!endCard)
        {
          if (!reader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetCardType Track element missing");
          }
          else
          {
            int track_index;
            String track_data;

            track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
            track_data = XML.GetValue(reader, "TrackData");
            SetTrackData(track_index, track_data);
            //advance until </track_data> element
            reader.Read();
            //advance until </track> element 
            reader.Read();
            if ((reader.Name == "Track") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              reader.Read();
              if ((reader.Name == "Card") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                endCard = true;
              }
            }
          }
        }
      }
      else
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetCardType not found in content area");
      }
    }
  }

  public class WCP_MsgGetCardTypeReply : IXml
  {
    public WCP_CardTypes CardType;
    public Int64 AccountId = 0;
    public String HolderName = "";
    public String MessageLine1 = "";
    public String MessageLine2 = "";
    
    public WCP_MsgGetCardTypeReply ()
    {
      // Default value
      CardType = WCP_CardTypes.CARD_TYPE_PLAYER;
    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgGetCardTypeReply>");
      _sb.AppendLine("<CardType>" + CardType.ToString() + "</CardType>");
      _sb.AppendLine("<AccountId>" + AccountId.ToString() + "</AccountId>");
      _sb.AppendLine("<HolderName>" + XML.StringToXmlValue(HolderName) + "</HolderName>");
      _sb.AppendLine("<MessageLine1>" + XML.StringToXmlValue(MessageLine1) + "</MessageLine1>");
      _sb.AppendLine("<MessageLine2>" + XML.StringToXmlValue(MessageLine2) + "</MessageLine2>");
      _sb.AppendLine("</WCP_MsgGetCardTypeReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      String card_type;
      if (reader.Name != "WCP_MsgGetCardTypeReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetCardTypeReply not found in content area");
      }
      else
      {
        card_type = XML.GetValue(reader, "CardType");
        CardType = (WCP_CardTypes)Enum.Parse(typeof(WCP_CardTypes), card_type, true);
        Int64.TryParse(XML.GetValue(reader, "AccountId", true), out AccountId);
        HolderName = XML.GetValue(reader, "HolderName", true);
        MessageLine1 = XML.ReadTagValue(reader, "MessageLine1", true);
        MessageLine2 = XML.ReadTagValue(reader, "MessageLine2", true);
      }
    }
  }
}
