//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgStartCardSession.cs
// 
//   DESCRIPTION: WCP_MsgStartCardSession class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
// 06-JUN-2012 ACC    Add Session Allow Cashin flag.
// 19-FEB-2016 ETP    Product Backlog Item 9748: Multiple buckets: Canje de buckets RE
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgStartCardSession:IXml
  {
    // RCI 09-AUG-2010: Value 0 indicates TransactionId is NOT present
    Int64 TransactionId = 0;
    ArrayList Track;
    public String Pin = "";
    Int32 TransferMode = 0;
    MultiPromos.AccountBalance AccountBalanceToTranfer = MultiPromos.AccountBalance.Zero;
    Boolean TransferWithBuckets = false;

    public WCP_MsgStartCardSession ()
    {
      Track = new ArrayList ();
    }

    public void SetTransactionId (Int64 Value)
    {
      TransactionId = Value;
    }

    public void SetPin(String Value)
    {
      Pin = Value;
    }

    public void SetTrackData (int TrackIndex, String TrackData)
    {
      while ( Track.Count <= TrackIndex )
      {
        Track.Add ("");
      }
      Track[TrackIndex] = TrackData;
    }

    public Int64 GetTransactionId ()
    {
      return TransactionId;
    }

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    }

    public Int32 GetTranferMode()
    {
      return TransferMode;
    }

    public void SetTranferMode(Int32 Value)
    {
      TransferMode = Value;
    }

    public MultiPromos.AccountBalance GetBalanceToTransfer()
    {
      return AccountBalanceToTranfer;
    }

    public void SetBalanceToTransfer(MultiPromos.AccountBalance ValueAccountBalanceToTranfer)
    {
      AccountBalanceToTranfer = ValueAccountBalanceToTranfer;
    }
   
    public Boolean IsTransferWithBuckets()
    {
      return TransferWithBuckets;
    }

    public void SetTransferWithBuckets(Boolean ValueTransferWithBuckets)
    {
      TransferWithBuckets = ValueTransferWithBuckets;
    }

    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode parent_node;
      XmlNode track;

      xml = new XmlDocument ();
      xml.LoadXml ("<WCP_MsgStartCardSession><Card></Card></WCP_MsgStartCardSession>");

      node = xml.SelectSingleNode("WCP_MsgStartCardSession/Card");

      // RCI 09-AUG-2010: Added optional field TransactionId.
      if (TransactionId > 0)
      {
        parent_node = xml.SelectSingleNode("WCP_MsgStartCardSession");
        XML.InsertBefore(parent_node, node, "TransactionId", TransactionId.ToString());
      }

      for ( int track_idx = 0; track_idx < Track.Count; track_idx++ )
      {
        track = xml.CreateNode (XmlNodeType.Element, "Track", "");

        node.AppendChild (track);

        XML.AppendChild (track, "TrackIndex", track_idx.ToString ());
        XML.AppendChild (track, "TrackData", Track[track_idx].ToString ());
      }
      node = xml.SelectSingleNode ("WCP_MsgStartCardSession");
      XML.AppendChild(node, "Pin", Pin);

      XML.AppendChild(node, "TransferMode", TransferMode.ToString());
      if (TransferMode != 0)
      {
        parent_node = XML.AppendChild(node, "BalanceToTransfer", "");        
        XML.AppendChild(parent_node, "Redeemable", ((Int64)(AccountBalanceToTranfer.Redeemable * 100)).ToString());
        XML.AppendChild(parent_node, "PromoRedeemable", ((Int64)(AccountBalanceToTranfer.PromoRedeemable * 100)).ToString());
        XML.AppendChild(parent_node, "PromoNotRedeemable", ((Int64)(AccountBalanceToTranfer.PromoNotRedeemable * 100)).ToString());
      }

      XML.AppendChild(node, "TransferWithBuckets", TransferWithBuckets.ToString());
      
      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Xmlreader)
    {
      bool endCard;
      Int64 _transaction_id;
      Int32 _aux_value;

      if (Xmlreader.Name == "WCP_MsgStartCardSession")
      {
        // RCI 09-AUG-2010: Added optional field TransactionId.
        if (!Xmlreader.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession Card element missing");
        }
        if (Xmlreader.Name == "TransactionId" && Xmlreader.NodeType.Equals(XmlNodeType.Element))
        {
          if (!Int64.TryParse(XML.GetValue(Xmlreader, "TransactionId"), out _transaction_id))
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession TransactionId: Can't parse value");
          }
          SetTransactionId(_transaction_id);
        }

        endCard = false;
        while (Xmlreader.Name != "Card" || (!Xmlreader.NodeType.Equals(XmlNodeType.Element)))
        {
          if (!Xmlreader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession Card element missing");
          }
        }
        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!endCard)
        {
          if (!Xmlreader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession Track element missing");
          }
          else
          {
            int track_index;
            String track_data;

            track_index = int.Parse(XML.GetValue(Xmlreader, "TrackIndex"));
            track_data = XML.GetValue(Xmlreader, "TrackData");
            SetTrackData(track_index, track_data);
            //advance until </track_data> element
            Xmlreader.Read();
            //advance until </track> element 
            Xmlreader.Read();
            if ((Xmlreader.Name == "Track") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              Xmlreader.Read();
              if ((Xmlreader.Name == "Card") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                endCard = true;
              }
            }
          }
        }

        Pin = XML.GetValue(Xmlreader, "Pin", true);

        if (Int32.TryParse(XML.GetValue(Xmlreader, "TransferMode", true), out _aux_value))
        {
          TransferMode = _aux_value;
        }        

        if (TransferMode != 0)
        {
          AccountBalanceToTranfer.LoadXml(Xmlreader);
        }

        if (!Boolean.TryParse(XML.GetValue(Xmlreader, "TransferWithBuckets", true), out TransferWithBuckets))
        {
          TransferWithBuckets = false;
        }
      }
      else
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession not found in content area");
      }
    }
  }

  public class WCP_MsgStartCardSessionReply:IXml
  {
    public Int64 CardSessionId;
    public Int64 CardBalance;
    public String HolderName;
    public Int32 Points;
    public Boolean AllowCashIn = false;
    public MultiPromos.AccountBalance Balance = MultiPromos.AccountBalance.Zero;

    public String ToXml ()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgStartCardSessionReply>");
      _sb.AppendLine("<CardSessionId>" + CardSessionId.ToString() + "</CardSessionId>");
      _sb.AppendLine("<CardBalance>" + CardBalance.ToString() + "</CardBalance>");
      _sb.AppendLine("<HolderName>" + XML.StringToXmlValue(HolderName) + "</HolderName>");
      // RCI 05-NOV-2010: Added optional field for points balance.
      _sb.AppendLine("<Points>" + Points.ToString() + "</Points>");
      _sb.AppendLine("<AllowCashIn>" + AllowCashIn.ToString() + "</AllowCashIn>");
      // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
      _sb.AppendLine("<Balance>");
      _sb.AppendLine(Balance.ToXml());
      _sb.AppendLine("</Balance>");
      _sb.AppendLine("</WCP_MsgStartCardSessionReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgStartCardSessionReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSessionReply not found in content area");
      }
      else
      {
        CardSessionId = Int64.Parse(XML.GetValue(reader, "CardSessionId"));
        CardBalance = Int64.Parse(XML.GetValue(reader, "CardBalance"));
        HolderName = XML.GetValue(reader, "HolderName");

        // RCI 05-NOV-2010: Added optional field for points balance.
        Int32.TryParse(XML.GetValue(reader, "Points", true), out Points);
        if (!Boolean.TryParse(XML.GetValue(reader, "AllowCashIn", true), out AllowCashIn))
        {
          AllowCashIn = false;
        }

        // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
        Balance.LoadXml(reader);
      }
    }
  }
}
