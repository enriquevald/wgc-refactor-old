//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgEndSession.cs
// 
//   DESCRIPTION: WCP_MsgEndSession class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 RRT    First release.
// 22-NOV-2016 JMM    PBI 19744:Pago automático de handpays
// 12-SEP-2017 JML    PBI 29479: WIGOS-4700 Promotional credit reported as cashable after card out/in
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgEndSession : IXml
  {
    public Int64 TransactionId = 0;
    public Int64 CardSessionId;
    public Int64 CardBalance;

    public Int64 PlayedCount = 0;
    public Int64 PlayedCents = 0;
    public Int64 WonCount = 0;
    public Int64 WonCents = 0;
    public Int64 JackpotCents = 0;

    public Int64 PlayedReedemable = 0;
    public Int64 PlayedNonRestrictedAmount = 0;
    public Int64 PlayedRestrictedAmount = 0;
    
    public Int64 HandpaysAmountCents = 0;

    public Boolean HasCounters = false;
    public MultiPromos.AccountBalance Balance = MultiPromos.AccountBalance.Zero;

    public Boolean OnlyAddBalances = false;

    public MultiPromos.EndSessionInput EndSessionInput()
    {
      MultiPromos.EndSessionInput _input;

      _input = new MultiPromos.EndSessionInput();

      _input.TransactionId = this.TransactionId;
      _input.PlaySessionId = this.CardSessionId;
      _input.BalanceFromGM = ((Decimal)CardBalance) / 100m;

      _input.HasMeters = this.HasCounters;
      if (this.HasCounters)
      {
        _input.Meters.PlayedCount = this.PlayedCount;
        _input.Meters.PlayedAmount = ((Decimal)this.PlayedCents) / 100m;
        _input.Meters.WonCount = this.WonCount;
        _input.Meters.WonAmount = ((Decimal)this.WonCents) / 100m;
        _input.Meters.JackpotAmount = ((Decimal)this.JackpotCents) / 100m;
        _input.Meters.PlayedReedemable = ((Decimal)this.PlayedReedemable) / 100.00m;
        _input.Meters.PlayedNonRestrictedAmount = ((Decimal)this.PlayedNonRestrictedAmount) / 100.00m;
        _input.Meters.PlayedRestrictedAmount = ((Decimal)this.PlayedRestrictedAmount) / 100.00m;


        _input.Meters.HandpaysAmount = ((Decimal) this.HandpaysAmountCents) / 100m;
      }

      return _input;
    } // EndSessionInput


    #region IXml Members

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgEndSession>");

      // RCI 09-AUG-2010: Added optional field TransactionId.
      if (TransactionId > 0)
      {
        _sb.AppendLine("<TransactionId>" + TransactionId.ToString() + "</TransactionId>");
      }

      _sb.AppendLine("<CardSessionId>" + CardSessionId.ToString() + "</CardSessionId>");
      _sb.AppendLine("<CardBalance>" + CardBalance.ToString() + "</CardBalance>");

      // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
      _sb.AppendLine("<Balance>");
      _sb.AppendLine(Balance.ToXml());
      _sb.AppendLine("</Balance>");

      _sb.AppendLine("<OnlyAddBalances>" + (OnlyAddBalances ? "1" : "0") + "</OnlyAddBalances>");      

      // RCI 10-AUG-2010: Added optional fields for counters.
      if (HasCounters)
      {
        _sb.AppendLine("<PlayedCount>" + PlayedCount.ToString() + "</PlayedCount>");
        _sb.AppendLine("<PlayedCents>" + PlayedCents.ToString() + "</PlayedCents>");
        _sb.AppendLine("<WonCount>" + WonCount.ToString() + "</WonCount>");
        _sb.AppendLine("<WonCents>" + WonCents.ToString() + "</WonCents>");
        _sb.AppendLine("<JackpotCents>" + JackpotCents.ToString() + "</JackpotCents>");
        _sb.AppendLine("<HandpaysAmountCents>" + HandpaysAmountCents.ToString() + "</HandpaysAmountCents>");
      }

      _sb.AppendLine("</WCP_MsgEndSession>");

      return _sb.ToString();

    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      Int64 _transaction_id;
      Int64 _played_count;
      Int64 _played_cents;
      Int64 _won_count;
      Int64 _won_cents;
      Int64 _jackpot_cents;
      Int32 _only_add_balances;
      Int64 _handpays_amount_cents;

      if (Reader.Name != "WCP_MsgEndSession")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession not found in content area");
      }
      else
      {
        // RCI 09-AUG-2010: Added optional field TransactionId.
        if (!Reader.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession CardSessionId element missing");
        }

        //
        // AJQ ACC 19-SEP-2012 The message created with the StringBuilder contains an empty item, skip it!
        //
        if (Reader.Name == "")
        {
          if (!Reader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession CardSessionId element missing");
          }
        }

        if (Reader.Name == "TransactionId" && Reader.NodeType.Equals(XmlNodeType.Element))
        {
          if (!Int64.TryParse(XML.GetValue(Reader, "TransactionId"), out _transaction_id))
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession TransactionId: Can't parse value");
          }
          TransactionId = _transaction_id;
        }

        CardSessionId = Int64.Parse(XML.GetValue(Reader, "CardSessionId"));
        CardBalance = Int64.Parse(XML.GetValue(Reader, "CardBalance"));

        // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
        while (Reader.Name != "Balance" && Reader.Name != "PlayedCount" && Reader.Name != "WCP_MsgEndSession")
        {
          if (!Reader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession Balance or PlayedCount element missing");
          }
        }

        if (Reader.Name == "Balance")
        {
          Balance.LoadXml(Reader);
        }

        Int32.TryParse(XML.ReadTagValue(Reader, "OnlyAddBalances", true), out _only_add_balances);
        OnlyAddBalances = _only_add_balances > 0 ? true : false;

        // RCI 10-AUG-2010: Added optional fields for counters.
        HasCounters = false;
        PlayedCount = 0;
        PlayedCents = 0;
        WonCount = 0;
        WonCents = 0;
        JackpotCents = 0;
        HandpaysAmountCents = 0;

        _handpays_amount_cents = 0;

        if (!Int64.TryParse(XML.GetValue(Reader, "PlayedCount", true), out _played_count))
        {
          return;
        }
        if (!Int64.TryParse(XML.GetValue(Reader, "PlayedCents", true), out _played_cents))
        {
          return;
        }
        if (!Int64.TryParse(XML.GetValue(Reader, "WonCount", true), out _won_count))
        {
          return;
        }
        if (!Int64.TryParse(XML.GetValue(Reader, "WonCents", true), out _won_cents))
        {
          return;
        }
        if (!Int64.TryParse(XML.GetValue(Reader, "JackpotCents", true), out _jackpot_cents))
        {
          return;
        }

        //Read HandpaysAmountCents.  If terminal has old version, there is no a HandpaysAmountCents tag
        while (Reader.Name != "HandpaysAmountCents" && !Reader.NodeType.Equals(XmlNodeType.Element) && !Reader.EOF)
        {
          Reader.Read();
        }

        if (Reader.Name == "HandpaysAmountCents" && Reader.NodeType.Equals(XmlNodeType.Element))
        {
          if (!Int64.TryParse(XML.GetValue(Reader, "HandpaysAmountCents", true), out _handpays_amount_cents))
          {
            return;
          }
        }

        HasCounters = true;
        PlayedCount = _played_count;
        PlayedCents = _played_cents;
        WonCount = _won_count;
        WonCents = _won_cents;
        JackpotCents = _jackpot_cents;
        HandpaysAmountCents = _handpays_amount_cents;
      }
    } // LoadXml

    #endregion // IXml Members

  } // WCP_MsgEndSession

  public class WCP_MsgEndSessionReply : IXml
  {
    public Int64 CardBalance;
    public String HolderName;
    public Int32 Points;

    public MultiPromos.AccountBalance Balance = MultiPromos.AccountBalance.Zero;

    #region IXml Members

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgEndSessionReply>");
      _sb.AppendLine("<CardBalance>" + CardBalance.ToString() + "</CardBalance>");
      _sb.AppendLine("<HolderName>" + XML.StringToXmlValue(HolderName) + "</HolderName>");
      _sb.AppendLine("<Points>" + Points.ToString() + "</Points>");

      // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
      _sb.AppendLine("<Balance>");
      _sb.AppendLine(Balance.ToXml());
      _sb.AppendLine("</Balance>");

      _sb.AppendLine("</WCP_MsgEndSessionReply>");

      return _sb.ToString();

    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgEndSessionReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSessionReply not found in content area");
      }
      else
      {
        CardBalance = Int64.Parse(XML.GetValue(Reader, "CardBalance"));

        // RCI 05-NOV-2010: Added optional fields for holder name and points balance.
        HolderName = XML.GetValue(Reader, "HolderName", true);
        Int32.TryParse(XML.GetValue(Reader, "Points", true), out Points);

        // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
        Balance.LoadXml(Reader);
      }
    } // LoadXml

    #endregion // IXml Members

  } // WCP_MsgEndSessionReply

} // WSI.WCP
