//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgCardInformation.cs
// 
//   DESCRIPTION: WCP_MsgCardInformation class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
// 05-MAY-2011 MBF    Copied from WCP_MsgStartCardSession.cs
// 17-NOV-2015 ETP    Product Backlog Item 5835: Added transfer amounts.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgCardInformation : IXml
  {
    // RCI 09-AUG-2010: Value 0 indicates TransactionId is NOT present
    Int64 TransactionId = 0;
    ArrayList Track;
    String Pin = "";

    public WCP_MsgCardInformation()
    {
      Track = new ArrayList();
    }


    public void SetTransactionId(Int64 Value)
    {
      TransactionId = Value;
    }

    public void SetPin(String Value)
    {
      Pin = Value;
    }

    public void SetTrackData(int TrackIndex, String TrackData)
    {
      while (Track.Count <= TrackIndex)
      {
        Track.Add("");
      }
      Track[TrackIndex] = TrackData;
    }

    public Int64 GetTransactionId()
    {
      return TransactionId;
    }

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode parent_node;
      XmlNode track;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_MsgCardInformation><Card></Card></WCP_MsgCardInformation>");

      node = xml.SelectSingleNode("WCP_MsgCardInformation/Card");

      // RCI 09-AUG-2010: Added optional field TransactionId.
      if (TransactionId > 0)
      {
        parent_node = xml.SelectSingleNode("WCP_MsgCardInformation");
        XML.InsertBefore(parent_node, node, "TransactionId", TransactionId.ToString());
      }

      for (int track_idx = 0; track_idx < Track.Count; track_idx++)
      {
        track = xml.CreateNode(XmlNodeType.Element, "Track", "");

        node.AppendChild(track);

        XML.AppendChild(track, "TrackIndex", track_idx.ToString());
        XML.AppendChild(track, "TrackData", Track[track_idx].ToString());
      }
      node = xml.SelectSingleNode("WCP_MsgCardInformation");
      XML.AppendChild(node, "Pin", Pin);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Xmlreader)
    {
      bool endCard;
      Int64 _transaction_id;

      if (Xmlreader.Name == "WCP_MsgCardInformation")
      {
        // RCI 09-AUG-2010: Added optional field TransactionId.
        if (!Xmlreader.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCardInformation Card element missing");
        }
        if (Xmlreader.Name == "TransactionId" && Xmlreader.NodeType.Equals(XmlNodeType.Element))
        {
          if (!Int64.TryParse(XML.GetValue(Xmlreader, "TransactionId"), out _transaction_id))
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCardInformation TransactionId: Can't parse value");
          }
          SetTransactionId(_transaction_id);
        }

        endCard = false;
        while (Xmlreader.Name != "Card" || (!Xmlreader.NodeType.Equals(XmlNodeType.Element)))
        {
          if (!Xmlreader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCardInformation Card element missing");
          }
        }
        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!endCard)
        {
          if (!Xmlreader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCardInformation Track element missing");
          }
          else
          {
            int track_index;
            String track_data;

            track_index = int.Parse(XML.GetValue(Xmlreader, "TrackIndex"));
            track_data = XML.GetValue(Xmlreader, "TrackData");
            SetTrackData(track_index, track_data);
            //advance until </track_data> element
            Xmlreader.Read();
            //advance until </track> element 
            Xmlreader.Read();
            if ((Xmlreader.Name == "Track") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              Xmlreader.Read();
              if ((Xmlreader.Name == "Card") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                endCard = true;
              }
            }
          }
        }

        Pin = XML.GetValue(Xmlreader, "Pin", true);
      }
      else
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCardInformation not found in content area");
      }
    }
  }

  public class WCP_MsgCardInformationReply : IXml
  {
    public Int64    Account;
    public String   HolderName;
    public DateTime Birthday;
    public Int64    CardBalance;
    public String   LastTerminal;
    public String   Level;
    public Int32    Points;
    public Boolean  SessionActive;
    public String   NRTransferAmount;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode preferences;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgCardInformationReply", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "Account", Account.ToString());
      XML.AppendChild(node, "HolderName", HolderName);
      XML.AppendChild(node, "Birthday", XML.XmlDateTimeString(Birthday));
      XML.AppendChild(node, "CardBalance", CardBalance.ToString());
      XML.AppendChild(node, "LastTerminal", LastTerminal);
      XML.AppendChild(node, "Level", Level);
      XML.AppendChild(node, "Points", Points.ToString());
      XML.AppendChild(node, "SessionActive", SessionActive.ToString());
      
      // ETP 18-NOV-2015 NR Transfer amounts for LCD Display.
      preferences = xml.CreateNode(XmlNodeType.Element, "Preferences", "");
      node.AppendChild(preferences);
      XML.AppendChild(preferences, "NRTransferAmount", NRTransferAmount);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgCardInformationReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCardInformationReply not found in content area");
      }
      else
      {
        Account = Int64.Parse(XML.GetValue(reader, "Account"));
        HolderName = XML.GetValue(reader, "HolderName");
        DateTime.TryParse(XML.GetValue(reader, "Birthday"), out Birthday);

        CardBalance = Int64.Parse(XML.GetValue(reader, "CardBalance"));
        LastTerminal = XML.GetValue(reader, "LastTerminal");
        Level = XML.GetValue(reader, "Level");

        Int32.TryParse(XML.GetValue(reader, "Points", true), out Points);
        Boolean.TryParse(XML.GetValue(reader, "SessionActive", true), out SessionActive);
        NRTransferAmount = XML.GetValue(reader, "NRTransferAmount",true);
      }
    }
  }
}
