//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMoneyEvents.cs
// 
//   DESCRIPTION: WCP_MsgMoneyEvents class
// 
//        AUTHOR: Miquel Beltran
// 
// CREATION DATE: 30-JAN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JAN-2012 MBF    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgMoneyEvent : IXml
  {
    public Int32 LktMoneyEventId;
    public DateTime LocalDatetime;
    public ENUM_MONEY_SOURCE Source;
    public ENUM_MONEY_TARGET Target;
    public Int64 Amount;

    public ENUM_ACCEPTED_MONEY_STATUS Status;
    public Int64 TransactionId;
    public Int64 CardSessionId;
    public Int64 StackerId; 
    
    private ArrayList Track;


    public void SetLocalDatetime(Int64 FileTime)
    {
      LocalDatetime = DateTime.FromFileTime(FileTime);
    }

    public WCP_MsgMoneyEvent()
    {
      Track = new ArrayList ();
      LocalDatetime = DateTime.Now;
    } // WCP_MsgMoneyEvent
    
    public void SetTrackData (int TrackIndex, String TrackData)
    {
      while ( Track.Count <= TrackIndex )
      {
        Track.Add ("");
      }
      Track[TrackIndex] = TrackData;
    } // SetTrackData

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    } // GetTrackData

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode _card_node;
      XmlNode _track;
      int _track_idx;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgMoneyEvent", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "LktMoneyEventId", LktMoneyEventId.ToString());
      XML.AppendChild(node, "LocalDatetime", XML.XmlDateTimeString(LocalDatetime));
      XML.AppendChild(node, "Source", Source.ToString());
      XML.AppendChild(node, "Target", Target.ToString());
      XML.AppendChild(node, "Amount", Amount.ToString());
      XML.AppendChild(node, "Status", Status.ToString());
      XML.AppendChild(node, "TransactionId", TransactionId.ToString());
      XML.AppendChild(node, "CardSessionId", CardSessionId.ToString());
      XML.AppendChild(node, "StackerId", StackerId.ToString());

      // Create Card node and append.
      _card_node = xml.CreateNode(XmlNodeType.Element, "Card", "");
      node.AppendChild(_card_node);

      // Load card trackdata tracks on node
      for (_track_idx = 0; _track_idx < Track.Count; _track_idx++)
      {
        _track = xml.CreateNode(XmlNodeType.Element, "Track", "");
        _card_node.AppendChild(_track);

        XML.AppendChild(_track, "TrackIndex", _track_idx.ToString());
        XML.AppendChild(_track, "TrackData", Track[_track_idx].ToString());
      }

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      int _track_index;
      String _track_data;
      bool _end_card;
      String time;
      String _str;

      if (reader.Name != "WCP_MsgMoneyEvent")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMoneyEvent not found in content area");
      }


      LktMoneyEventId = Int32.Parse(XML.GetValue(reader, "LktMoneyEventId"));

      time = XML.GetValue(reader, "LocalDatetime");
      DateTime.TryParse(time, out LocalDatetime);


      _str = XML.GetValue(reader, "Source");
      Source = (ENUM_MONEY_SOURCE)Enum.Parse(typeof(ENUM_MONEY_SOURCE), _str, true);

      _str = XML.GetValue(reader, "Target");
      Target = (ENUM_MONEY_TARGET)Enum.Parse(typeof(ENUM_MONEY_TARGET), _str, true);

      Amount = Int64.Parse(XML.GetValue(reader, "Amount"));

      _str = XML.GetValue(reader, "Status");
      Status = (ENUM_ACCEPTED_MONEY_STATUS)Enum.Parse(typeof(ENUM_ACCEPTED_MONEY_STATUS), _str, true);

      TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));
      CardSessionId = Int64.Parse(XML.GetValue(reader, "CardSessionId"));
      StackerId = Int64.Parse(XML.GetValue(reader, "StackerId"));
      

      _end_card = false;

      while (reader.Name != "Card")
      {
        if (!reader.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMoneyEvent Card element missing");
        }
      }

      while (!_end_card)
      {
        _track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
        _track_data = XML.GetValue(reader, "TrackData");
        SetTrackData(_track_index, _track_data);

        //advance until </track> element 
        while (reader.Name != "Track")
        {
          if (!reader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMoneyEvent Track element missing");
          }
        }

        if ((reader.NodeType.Equals(XmlNodeType.EndElement)))
        {
          reader.Read();
          if ((reader.Name == "Card") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
          {
            _end_card = true;
          } // if
        } // if
      }// while

    } // LoadXml
  } // public class WCP_MsgMoneyEvent

  public class WCP_MsgMoneyEventReply : IXml
  {

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgMoneyEventReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgMoneyEventReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMoneyEventReply not found in content area");
      }
    } // LoadXml

  } // public class WCP_MsgMoneyEventReply
} // namespace WSI.WCP
