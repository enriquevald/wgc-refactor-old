//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgReportPlay.cs
// 
//   DESCRIPTION: WCP_MsgReportPlay class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgReportPlay : IXml
  {
    public Int64 TransactionId;
    public Int64 CardSessionId;
    public Int64 SessionBalanceBeforePlay;
    public Int64 SessionBalanceAfterPlay;
    public String GameId;
    public int Denomination;
    public Int64 PlayedAmount;
    public Int64 WonAmount;
    public double PayoutPercentage;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportPlay", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "TransactionId", TransactionId.ToString());
      XML.AppendChild(node, "CardSessionId", CardSessionId.ToString());
      XML.AppendChild(node, "SessionBalanceBeforePlay", SessionBalanceBeforePlay.ToString());
      XML.AppendChild(node, "SessionBalanceAfterPlay", SessionBalanceAfterPlay.ToString());
      XML.AppendChild(node, "GameId", GameId);
      XML.AppendChild(node, "Denomination", Denomination.ToString());
      XML.AppendChild(node, "PlayedAmount", PlayedAmount.ToString());
      XML.AppendChild(node, "WonAmount", WonAmount.ToString());

      
      XML.AppendChild(node, "PayoutPercentage", PayoutPercentage.ToString());

      // AJQ - 04-DES-2014, New format without decimal.

      //// New Format
      //int _x10000;
      //_x10000 = (int)(PayoutPercentage * 100);
      //_x10000 = Math.Max(0, _x10000);
      //_x10000 = Math.Min(10000, _x10000);
      //XML.AppendChild(node, "PayoutPercentage", _x10000.ToString("0000"));

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgReportPlay")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportPlay not found in content area");
      }
      else
      {
        TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));
        CardSessionId = Int64.Parse(XML.GetValue(reader, "CardSessionId"));
        SessionBalanceBeforePlay = Int64.Parse(XML.GetValue(reader, "SessionBalanceBeforePlay"));
        SessionBalanceAfterPlay = Int64.Parse(XML.GetValue(reader, "SessionBalanceAfterPlay"));
        GameId = XML.GetValue(reader, "GameId");
        Denomination = Int32.Parse(XML.GetValue(reader, "Denomination"));
        PlayedAmount = Int64.Parse(XML.GetValue(reader, "PlayedAmount"));
        WonAmount = Int64.Parse(XML.GetValue(reader, "WonAmount"));


        String _payout;
        Boolean _ok;

        PayoutPercentage = 0;
        _payout = XML.GetValue(reader, "PayoutPercentage");
        if (!String.IsNullOrEmpty(_payout))
        {
          if (_payout.Length == 4 && _payout.IndexOf('.') < 0)
          {
            // New format
            if (double.TryParse(_payout, out PayoutPercentage))
            {
              PayoutPercentage /= 100;
            }
            else
            {
              PayoutPercentage = 0;
            }
          }
          else
          {
            // Old format
            _ok = double.TryParse(_payout, out PayoutPercentage);
            if (PayoutPercentage > 100 || PayoutPercentage < 0)
            {
              _ok = false;
              PayoutPercentage = 0;
            }

            if (!_ok)
            {
              if (_payout.IndexOf('.') > 0)
              {
                _payout = _payout + "0000"; // Ensure at least 4 decimals 
                _payout = _payout.Substring(0, _payout.IndexOf('.') + 3);
                _payout = _payout.Replace(".", "");
                _ok = double.TryParse(_payout, out PayoutPercentage);
                if (_ok)
                {
                  PayoutPercentage = PayoutPercentage / 100;
                }
              }
            }

            PayoutPercentage = Math.Max(0, PayoutPercentage);
            PayoutPercentage = Math.Min(100, PayoutPercentage);
          }
        }
      }
    }
  }

  public class WCP_MsgReportPlayReply : IXml
  {
    public Int64 SessionBalanceAfterPlay;

    public String ToXml()
    {
      return "<WCP_MsgReportPlayReply><SessionBalanceAfterPlay>" + SessionBalanceAfterPlay.ToString() + "</SessionBalanceAfterPlay></WCP_MsgReportPlayReply>";

      ////XmlDocument xml;
      ////XmlNode node;

      ////xml = new XmlDocument();

      ////node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportPlayReply", "");
      ////xml.AppendChild(node);

      ////XML.AppendChild(node, "SessionBalanceAfterPlay", SessionBalanceAfterPlay.ToString());

      ////return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgReportPlayReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportPlayReply not found in content area");
      }
      else
      {
        SessionBalanceAfterPlay = Int64.Parse(XML.GetValue(reader, "SessionBalanceAfterPlay"));
      }
    }
  }
}
