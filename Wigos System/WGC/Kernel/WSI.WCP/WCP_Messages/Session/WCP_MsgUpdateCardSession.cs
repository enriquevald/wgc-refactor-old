//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgUpdateCardSession.cs
// 
//   DESCRIPTION: WCP_MsgUpdateCardSession class
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 09-AUG-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-AUG-2010 RCI    First release.
// 11-JUL-2014 XCD    Calculate estimated points awarded
//------------------------------------------------------------------------------

  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Text;
  using System.Xml;
  using WSI.Common;

  namespace WSI.WCP
  {
    public class WCP_MsgUpdateCardSession : IXml
    {
      public Int64 CardSessionId;
      public Int64 PlayedCount;
      public Int64 PlayedCents;
      public Int64 WonCount;
      public Int64 WonCents;
      public Int64 JackpotCents;

      #region IXml Members

      //------------------------------------------------------------------------------
      // PURPOSE : Convert the properties of this class to a XML String representation.
      //
      //  PARAMS :
      //      - INPUT :
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //      - String
      //
      public String ToXml()
      {
        XmlDocument _xml;
        XmlNode _node;

        _xml = new XmlDocument();

        _node = _xml.CreateNode(XmlNodeType.Element, "WCP_MsgUpdateCardSession", "");
        _xml.AppendChild(_node);

        XML.AppendChild(_node, "CardSessionId", CardSessionId.ToString());
        XML.AppendChild(_node, "PlayedCount", PlayedCount.ToString());
        XML.AppendChild(_node, "PlayedCents", PlayedCents.ToString());
        XML.AppendChild(_node, "WonCount", WonCount.ToString());
        XML.AppendChild(_node, "WonCents", WonCents.ToString());
        XML.AppendChild(_node, "JackpotCents", JackpotCents.ToString());

        return _xml.OuterXml;
      } // ToXml

      //------------------------------------------------------------------------------
      // PURPOSE : Parse a XML Document and save values into the properties of this class.
      //
      //  PARAMS :
      //      - INPUT :
      //          - XmlReader Reader
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      public void LoadXml(XmlReader Reader)
      {
        if (Reader.Name != "WCP_MsgUpdateCardSession")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgUpdateCardSession not found in content area");
        }
        else
        {
          CardSessionId = Int64.Parse(XML.GetValue(Reader, "CardSessionId"));
          PlayedCount = Int64.Parse(XML.GetValue(Reader, "PlayedCount"));
          PlayedCents = Int64.Parse(XML.GetValue(Reader, "PlayedCents"));
          WonCount = Int64.Parse(XML.GetValue(Reader, "WonCount"));
          WonCents = Int64.Parse(XML.GetValue(Reader, "WonCents"));
          JackpotCents = Int64.Parse(XML.GetValue(Reader, "JackpotCents"));
        }
      } // LoadXml

      #endregion // IXml Members

    } // WCP_MsgUpdateCardSession

    public class WCP_MsgUpdateCardSessionReply : IXml
    {
      public Int64 EstimatedPointsCents;

      #region IXml Members

      //------------------------------------------------------------------------------
      // PURPOSE : Convert the properties of this class to a XML String representation.
      //
      //  PARAMS :
      //      - INPUT :
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //      - String
      //
      public String ToXml()
      {
        XmlDocument _xml;
        XmlNode _node;

        _xml = new XmlDocument();

        _node = _xml.CreateNode(XmlNodeType.Element, "WCP_MsgUpdateCardSessionReply", "");
        _xml.AppendChild(_node);

        XML.AppendChild(_node, "EstimatedPointsCents", EstimatedPointsCents.ToString());

        return _xml.OuterXml;
        
      } // ToXml

      //------------------------------------------------------------------------------
      // PURPOSE : Parse a XML Document and save values into the properties of this class.
      //
      //  PARAMS :
      //      - INPUT :
      //          - XmlReader Reader
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      public void LoadXml(XmlReader Reader)
      {
        if (Reader.Name != "WCP_MsgUpdateCardSessionReply")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgUpdateCardSessionReply not found in content area");
        }
        else
        {
          EstimatedPointsCents = Int64.Parse(XML.GetValue(Reader, "EstimatedPointsCents", true));
        }
      } // LoadXml

      #endregion // IXml Members

    } // WCP_MsgUpdateCardSessionReply

  } // WSI.WCP
