//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgPlayerCheckPin.cs
// 
//   DESCRIPTION: WCP_MsgPlayerCheckPin  class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 03-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-JUL-2014 JMM    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgPlayerCheckPin : IXml, WCP_WKT.IExtendedPlayer
  {
    public WCP_WKT.WKT_Player Player;

    public WCP_MsgPlayerCheckPin()
    {
      this.Player = new WCP_WKT.WKT_Player();
    }

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.Append("<WCP_MsgPlayerCheckPin>");
      _sb.Append(Player.ToXml());
      _sb.Append("</WCP_MsgPlayerCheckPin>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_MsgPlayerCheckPin")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgPlayerCheckPin not found in content area");
      }
      Player.Pin = XML.ReadTagValue(Xml, "Pin");
      Player.Trackdata = XML.ReadTagValue(Xml, "TrackData");

    }
    #endregion

    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get { return this.Player; }
      set { this.Player = value; }
    }

    #endregion
  }

  public class WCP_MsgPlayerCheckPinReply : IXml
  {
    public WCP_MsgPlayerCheckPinReply()
    {
    }

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgPlayerCheckPinReply>");
      _sb.AppendLine("</WCP_MsgPlayerCheckPinReply>");      

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_MsgPlayerCheckPinReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgPlayerCheckPinReply not found in content area");
      }
    }

    #endregion
  }
}
