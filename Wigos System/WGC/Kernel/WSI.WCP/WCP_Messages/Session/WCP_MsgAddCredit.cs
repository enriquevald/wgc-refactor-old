//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgAddCredit.cs
// 
//   DESCRIPTION: WCP_MsgAddCredit class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 RRT    First release.
// 26-JAN-2012 MBF    Added trackdata info.
// 09-DEC-2014 JMM    Added by recharges promotions to the promotion list sent to the WKT
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgAddCredit : IXml
  {
    public Int64 TransactionId;
    public Int64 CardSessionId;
    public Int64 AmountToAdd;
    public String Currency;
    public DateTime AcceptedTime;
    public Int64 StackerId; 

    ArrayList Track;

    public WCP_MsgAddCredit()
    {
      Track = new ArrayList ();
      Currency = "";
    } // WCP_MsgAddCredit

    public void SetTrackData (int TrackIndex, String TrackData)
    {
      while ( Track.Count <= TrackIndex )
      {
        Track.Add ("");
      }
      Track[TrackIndex] = TrackData;
    } // SetTrackData

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    } // GetTrackData

    public void SetAcceptedTime(Int64 FileTime)
    {
      AcceptedTime = DateTime.FromFileTime(FileTime);
    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode _card_node;
      XmlNode _track;
      int     _track_idx;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgAddCredit", "");
      xml.AppendChild(node);

      XML.AppendChild (node, "TransactionId", TransactionId.ToString ());

      // MBF 26-JAN-2012 Trackdata information

      // Create Card node and append.
      _card_node = xml.CreateNode(XmlNodeType.Element, "Card", "");
      node.AppendChild(_card_node);

      // Load card trackdata tracks on node
      for (_track_idx = 0; _track_idx < Track.Count; _track_idx++)
      {
        _track = xml.CreateNode(XmlNodeType.Element, "Track", "");
        _card_node.AppendChild(_track);

        XML.AppendChild(_track, "TrackIndex", _track_idx.ToString());
        XML.AppendChild(_track, "TrackData", Track[_track_idx].ToString());
      }

      XML.AppendChild (node, "CardSessionId", CardSessionId.ToString ());
      XML.AppendChild(node, "AmountToAdd", AmountToAdd.ToString());
      XML.AppendChild(node, "Currency", Currency);
      XML.AppendChild(node, "AcceptedTime", XML.XmlDateTimeString(AcceptedTime));
      XML.AppendChild(node, "StackerId", StackerId.ToString());

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      int _track_index;
      String _track_data;
      bool _end_card;
      String _time;

      if (reader.Name != "WCP_MsgAddCredit")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgAddCredit not found in content area");
      }
      else
      {
        TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));

        // MBF 26-JAN-2012 Trackdata information
        _end_card = false;

        while (reader.Name != "Card")
        {
          if (!reader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgAddCredit Card element missing");
          }
        }
        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!_end_card)
        {
          _track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
          _track_data = XML.GetValue(reader, "TrackData");
          SetTrackData(_track_index, _track_data);

          //advance until </track> element 
          while (reader.Name != "Track")
          {
            if (!reader.Read())
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgAddCredit Track element missing");
            }
          }

          if ((reader.NodeType.Equals(XmlNodeType.EndElement)))
          {
            reader.Read();
            if ((reader.Name == "Card") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              _end_card = true;
            } // if
          } // if
        }// while

        CardSessionId = Int64.Parse(XML.GetValue(reader, "CardSessionId"));
        AmountToAdd = Int64.Parse(XML.GetValue(reader, "AmountToAdd"));
        Currency = XML.GetValue(reader, "Currency");

        _time = XML.GetValue(reader, "AcceptedTime");
        DateTime.TryParse(_time, out AcceptedTime);

        StackerId = Int64.Parse(XML.GetValue(reader, "StackerId"));

      } // else
    } // LoadXml
  }

  public class WCP_MsgAddCreditReply : IXml, WCP_WKT.IVouchers
  {
    public Int64 SessionBalanceAfterAdd;
    public Boolean AccountAlreadyHasPromo;
    public Boolean BillInGivesPromo;

    private ArrayList _voucherList;

    private String _deflated_voucher;
    private String _inflated_voucher;

    #region IVoucher Members

    public ArrayList VoucherList
    {
      get
      {
        return _voucherList;
      }
      set
      {
        this._voucherList = value;
      }
    }

    #endregion

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgAddCreditReply>");
      _sb.Append("<SessionBalanceAfterAdd>" + SessionBalanceAfterAdd.ToString() + "</SessionBalanceAfterAdd>");
      _sb.Append("<AccountAlreadyHasPromo>" + (AccountAlreadyHasPromo ? "1" : "0") + "</AccountAlreadyHasPromo>");
      _sb.Append("<BillInGivesPromo>" + (BillInGivesPromo ? "1" : "0") + "</BillInGivesPromo>");      

      _sb.AppendLine("<Vouchers>");
      foreach (Voucher _voucher in VoucherList)
      {
        Misc.DeflateString(_voucher.VoucherHTML.ToString(), out _deflated_voucher);
        _sb.Append("<Voucher>" + XML.StringToXmlValue(_deflated_voucher) + "</Voucher>");
      }
      _sb.AppendLine("</Vouchers>");
      _sb.AppendLine("</WCP_MsgAddCreditReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      VoucherList = new ArrayList();

      if (reader.Name != "WCP_MsgAddCreditReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgAddCreditReply not found in content area");
      }
      else
      {
        SessionBalanceAfterAdd = Int64.Parse(XML.GetValue(reader, "SessionBalanceAfterAdd"));

        reader.Read();
        reader.Read();
        if (reader.Name == "AccountAlreadyHasPromo")
        {
          AccountAlreadyHasPromo = XML.ReadTagValue(reader, "AccountAlreadyHasPromo") == "1";
        }

        reader.Read();
        if (reader.Name == "BillInGivesPromo")
        {
          BillInGivesPromo = XML.ReadTagValue(reader, "BillInGivesPromo") == "1";
        }        

        while ((!(reader.Name == "Vouchers" && reader.NodeType == XmlNodeType.EndElement)) && (!(reader.Name == "WCP_MsgAddCreditReply" && reader.NodeType == XmlNodeType.EndElement)))
        {
          reader.Read();
          if ((reader.Name == "Voucher") && (reader.NodeType != XmlNodeType.EndElement))
          {
            _deflated_voucher = XML.GetValue(reader, "Voucher");
            Misc.InflateString(_deflated_voucher, out _inflated_voucher);
            VoucherList.Add(_inflated_voucher);
          }
        }
      }
    }
  }
}
