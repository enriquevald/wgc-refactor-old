//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgPlayConditions.cs
// 
//   DESCRIPTION: WCP_MsgPlayConditions class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Text;
  using System.Xml;
  using WSI.Common;

  namespace WSI.WCP
  {
    public class WCP_MsgPlayConditions : IXml
    {
      public Int64 CardSessionId;
      public Int64 SessionBalanceBeforePlay;
      public String GameId;
      public Int64 BetAmount;

      public String ToXml()
      {
        XmlDocument xml;
        XmlNode node;

        xml = new XmlDocument();

        node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgPlayConditions", "");
        xml.AppendChild(node);

        XML.AppendChild (node, "CardSessionId", CardSessionId.ToString ());
        XML.AppendChild (node, "SessionBalanceBeforePlay", SessionBalanceBeforePlay.ToString ());
        XML.AppendChild(node, "GameId", GameId);
        XML.AppendChild(node, "BetAmount", BetAmount.ToString());

        return xml.OuterXml;
      }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "WCP_MsgPlayConditions")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgPlayConditions not found in content area");
        }
        else
        {
          CardSessionId = Int64.Parse(XML.GetValue(reader, "CardSessionId"));
          SessionBalanceBeforePlay = Int64.Parse(XML.GetValue(reader, "SessionBalanceBeforePlay"));
          GameId = XML.GetValue(reader, "GameId");
          BetAmount = Int64.Parse(XML.GetValue(reader, "BetAmount"));
        }
      }
    }

    public class WCP_MsgPlayConditionsReply : IXml
    {
      public Int64 SessionBalanceBeforePlay;

      public String ToXml()
      {
        XmlDocument xml;
        XmlNode node;

        xml = new XmlDocument();

        node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgPlayConditionsReply", "");
        xml.AppendChild(node);

        XML.AppendChild(node, "SessionBalanceBeforePlay", SessionBalanceBeforePlay.ToString());

        return xml.OuterXml;
      }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "WCP_MsgPlayConditionsReply")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgPlayConditions not found in content area");
        }
        else
        {
          SessionBalanceBeforePlay = Int64.Parse(XML.GetValue(reader, "SessionBalanceBeforePlay"));
        }
      }
    }
  }
