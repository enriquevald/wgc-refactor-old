//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgLockCard.cs
// 
//   DESCRIPTION: WCP_MsgLockCard class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 15-SEP-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-SEP-2007 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgLockCard:IXml
  {
    public Int64 CardSessionId;
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WCP_MsgLockCard><CardSessionId>");
      str_builder.Append(CardSessionId.ToString());
      str_builder.Append("</CardSessionId></WCP_MsgLockCard>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgLockCard")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgLockCard not found in content area");
      }
      else
      {
        CardSessionId = int.Parse(XML.GetValue(reader, "CardSessionId"));
      }
    }
  }
  public class WCP_MsgLockCardReply: IXml
  { 
    public Int32 LockCardTimeout;

    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WCP_MsgLockCardReply><LockCardTimeout>");
      str_builder.Append(LockCardTimeout.ToString());
      str_builder.Append("</LockCardTimeout></WCP_MsgLockCardReply>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgLockCardReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgLockCardReply not found in content area");
      }
      else
      {
        LockCardTimeout = int.Parse(XML.GetValue(reader, "LockCardTimeout"));
      }
    }
  }
}

