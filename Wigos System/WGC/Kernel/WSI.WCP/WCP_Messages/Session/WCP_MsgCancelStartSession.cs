//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgCancelStartSession.cs
// 
//   DESCRIPTION: WCP_MsgCancelStartSession class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 20-MAR-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-MAR-2012 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgCancelStartSession:IXml
  {
    public Int64 TransactionId = 0;
    ArrayList Track;
    MultiPromos.AccountBalance CancelledBalance = MultiPromos.AccountBalance.Zero;

    public WCP_MsgCancelStartSession ()
    {
      Track = new ArrayList ();
    }

    public void SetTrackData (int TrackIndex, String TrackData)
    {
      while ( Track.Count <= TrackIndex )
      {
        Track.Add ("");
      }
      Track[TrackIndex] = TrackData;
    }

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    }

    public MultiPromos.AccountBalance GetCancelledBalance()
    {
      return CancelledBalance;
    }

    public void SetCancelledBalance(MultiPromos.AccountBalance ValueCancelledBalance)
    {
      CancelledBalance = ValueCancelledBalance;
    }

    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode parent_node;
      XmlNode track;

      xml = new XmlDocument ();
      xml.LoadXml ("<WCP_MsgCancelStartSession><Card></Card></WCP_MsgCancelStartSession>");

      node = xml.SelectSingleNode("WCP_MsgCancelStartSession/Card");

      if (TransactionId > 0)
      {
        parent_node = xml.SelectSingleNode("WCP_MsgCancelStartSession");
        XML.InsertBefore(parent_node, node, "TransactionId", TransactionId.ToString());
      }

      for ( int track_idx = 0; track_idx < Track.Count; track_idx++ )
      {
        track = xml.CreateNode (XmlNodeType.Element, "Track", "");

        node.AppendChild (track);

        XML.AppendChild (track, "TrackIndex", track_idx.ToString ());
        XML.AppendChild (track, "TrackData", Track[track_idx].ToString ());
      }
      node = xml.SelectSingleNode ("WCP_MsgCancelStartSession");

      parent_node = XML.AppendChild(node, "CancelledBalance", "");
      XML.AppendChild(parent_node, "Redeemable", ((Int64)(CancelledBalance.Redeemable * 100)).ToString());
      XML.AppendChild(parent_node, "PromoRedeemable", ((Int64)(CancelledBalance.PromoRedeemable * 100)).ToString());
      XML.AppendChild(parent_node, "PromoNotRedeemable", ((Int64)(CancelledBalance.PromoNotRedeemable * 100)).ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Xmlreader)
    {
      bool endCard;
      Int64 _transaction_id;

      if (Xmlreader.Name == "WCP_MsgCancelStartSession")
      {
        // RCI 09-AUG-2010: Added optional field TransactionId.
        if (!Xmlreader.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession Card element missing");
        }
        if (Xmlreader.Name == "TransactionId" && Xmlreader.NodeType.Equals(XmlNodeType.Element))
        {
          if (!Int64.TryParse(XML.GetValue(Xmlreader, "TransactionId"), out _transaction_id))
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession TransactionId: Can't parse value");
          }
          TransactionId = _transaction_id;
        }

        endCard = false;
        while (Xmlreader.Name != "Card" || (!Xmlreader.NodeType.Equals(XmlNodeType.Element)))
        {
          if (!Xmlreader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession Card element missing");
          }
        }
        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!endCard)
        {
          if (!Xmlreader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession Track element missing");
          }
          else
          {
            int track_index;
            String track_data;

            track_index = int.Parse(XML.GetValue(Xmlreader, "TrackIndex"));
            track_data = XML.GetValue(Xmlreader, "TrackData");
            SetTrackData(track_index, track_data);
            //advance until </track_data> element
            Xmlreader.Read();
            //advance until </track> element 
            Xmlreader.Read();
            if ((Xmlreader.Name == "Track") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              Xmlreader.Read();
              if ((Xmlreader.Name == "Card") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                endCard = true;
              }
            }
          }
        }

        CancelledBalance.LoadXml(Xmlreader);
      }
      else
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession not found in content area");
      }
    }
  }

  public class WCP_MsgCancelStartSessionReply:IXml
  {
    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgCancelStartSessionReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgCancelStartSessionReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSessionReply not found in content area");
      }
    } // LoadXml
  }
}
