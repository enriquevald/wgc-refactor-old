//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgSasMeters
// 
//   DESCRIPTION: class that represents terminal meters structure
// 
//        AUTHOR: Nelson Madrigal Reyes
// 
// CREATION DATE: 07-OCT-2013
// 
// REVISION HISTORY:
// 
// Date            Author Description
// ----------- ------ ----------------------------------------------------------
// 21-OCT-2013 NMR    First release.
// 28-NOV-2013 NMR    Reusing common SAS meters struct
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP.WCP_Messages.Meters
{
  /// <summary>
  /// Class that will control the msg creation for Terminal meters anouncement
  /// OJO: Class name MUST MATCH with definition in enum WCP_MsgTypes
  /// </summary>
  public class WCP_MsgSasMeters : IXml
  {
    protected const String MESSAGE_ENVELOPE = "WCP_MsgSasMeters";
    protected const String METERS_COMMON_NODE = "SAS_Meters";
    protected const String METERS_NODE = "SAS_Meter";

    public ArrayList Meters;

    public WCP_MsgSasMeters()
    {
      Meters = new ArrayList();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Convert internal properties to string in XML format
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String in XML format
    //
    public String ToXml()
    {
      StringBuilder _sb;
      String _attributes;
      Int32 _counter;

      _sb = new StringBuilder();
      _sb.AppendLine("<" + MESSAGE_ENVELOPE + ">");

      //
      // add SAS Meter items from array to common Node 'SAS_Meters'
      // each element will under tag 'SAS_Meter'; valid data will stored as attributes
      // format of this part will as follows:
      // <Meters>
      //    <Meter Code="15" GameId="18" DenomCents="2000" Value="198" MaxValue="999999" >1</Meter>
      //    <Meter Code="15" GameId="18" DenomCents="2000" Value="198" MaxValue="999999" >2</Meter>
      // </Meters>
      //
      _counter = 0;
      _sb.AppendLine("<" + METERS_COMMON_NODE + ">");
      foreach (SAS_Meter _meter in Meters)
      {
        // concatenate values in ATTRIBUTE format
        _attributes = " Code=\"" + _meter.Code.ToString() + "\" ";
        _attributes += "GameId=\"" + _meter.GameId.ToString() + "\" ";
        _attributes += "DenomCents=\"" + _meter.DenomCents.ToString() + "\" ";
        _attributes += "Value=\"" + _meter.Value.ToString() + "\" ";
        _attributes += "MaxValue=\"" + _meter.MaxValue.ToString() + "\" ";
        _attributes += "IgnoreDeltas=\"" + (_meter.IgnoreDeltas ? "1" : "0") + "\" ";        

        // add the METER node
        _sb.AppendLine("<" + METERS_NODE + _attributes + ">" + (++_counter) + "</" + METERS_NODE + ">");
      }
      _sb.AppendLine("</" + METERS_COMMON_NODE + ">");
      _sb.AppendLine("</" + MESSAGE_ENVELOPE + ">");

      return _sb.ToString();
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse input string, in XML format, and fill internal properties
    //           Bad structure raises WCP_Exception
    //
    //  PARAMS :
    //      - INPUT :
    //        - Reader: loaded XML structure
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      SAS_Meter _meter;
      XmlDocument _doc;
      XmlNodeList _nodes_list;

      if (!Reader.Name.Equals(MESSAGE_ENVELOPE))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag '" + MESSAGE_ENVELOPE + "' not found in content area");
      }

      // NMR: if is possible obtains the initial XML string,
      // the setting of XmlDocument could be optimized
      _doc = new XmlDocument();
      _doc.Load(Reader);

      Meters.Clear();
      _nodes_list = _doc.GetElementsByTagName(METERS_NODE);
      foreach (XmlNode _node in _nodes_list)
      {
        _meter = new SAS_Meter();
        _meter.Code = Int32.Parse(XML.GetAttributeValue(_node, "Code"));
        _meter.GameId = Int32.Parse(XML.GetAttributeValue(_node, "GameId"));
        _meter.DenomCents = Decimal.Parse(XML.GetAttributeValue(_node, "DenomCents"));
        _meter.Value = Int64.Parse(XML.GetAttributeValue(_node, "Value"));
        _meter.MaxValue = Int64.Parse(XML.GetAttributeValue(_node, "MaxValue"));
        _meter.IgnoreDeltas = XML.GetAttributeValue(_node, "IgnoreDeltas") == "1";

        Meters.Add(_meter);
      }
    }
  } // LoadXml

  /// <summary>
  /// Class than will control the message creation for Terminal meters anouncement reply
  /// </summary>
  public class WCP_MsgSasMetersReply : IXml
  {
    protected const String MESSAGE_ENVELOPE = "WCP_MsgSasMetersReply";

    //------------------------------------------------------------------------------
    // PURPOSE : Convert internal properties to string in XML format
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String in XML format
    //
    public String ToXml()
    {
      return "<" + MESSAGE_ENVELOPE + "></" + MESSAGE_ENVELOPE + ">";
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse input string, in XML format, and fill internal properties
    //           Bad structure raises WCP_Exception
    //
    //  PARAMS :
    //      - INPUT :
    //        - Reader: loaded XML structure
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      if (!Reader.Name.Equals(MESSAGE_ENVELOPE))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag " + MESSAGE_ENVELOPE + " not found in content area");
      }

    } // LoadXml
  }
}
