//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMachineMultiDenomInfo.cs
// 
//   DESCRIPTION: WCP_MsgMachineMultiDenomInfo class
// 
//        AUTHOR: Marcos Piedra
// 
// CREATION DATE: 29-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-JUL-2015 MPO    First release (based on MachineMeter)
// 07-SEP-2015 MPO    TFS ITEM 2194: SAS16: Estadísticas multi-denominación: WCP
// 25-NOV-2015 MPO    Fixed bug 6890
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgMachineMultiDenomInfo : IXml
  {

    public class Denomination
    {
      public Int32 Id;
      public Int64 Value10000; // Represents the denomination value in cents/100
      public String PaytableName;

      public List<SAS_Meter> MeterList;
    }

    public class Game
    {
      public Int32 GameNumber;
      public Int64 MaxBet;
      public Int32 ProgressiveGroup;
      public Int32 ProgressiveLevels;
      public String GameName;
      public String PaytableName;
      public Int32 WagerCategories;
      public String GameId;
      public Int64 GameDenom;
      public Int64 GamePayOut;
    public ArrayList DenominationList;
    }

    public ArrayList DenominationList;
    public ArrayList GameList;

    #region IXml Members

    private static String CleanStringAttributeValue(String AttributeValue)
    {
      StringBuilder buffer = new StringBuilder(AttributeValue.Length);//This many chars at most

      foreach (char ch in AttributeValue)
        if (!Char.IsControl(ch)) 
        {
          buffer.Append(ch);//Only add to buffer if not a control char
        }
        else
        {
          buffer.Append(' ');
        }

      return buffer.ToString();
    }

    private static Int32 GetDenomCode(String DenomValue)
    {
      Int32 _denom_code;

      _denom_code = 0x00;
      
      switch (DenomValue)
      {
        case "100":
          _denom_code = 0x01;
          break;
        case "500":
          _denom_code = 0x02;
          break;
        case "1000":
          _denom_code = 0x03;
          break;
        case "2500":
          _denom_code = 0x04;
          break;
        case "5000":
          _denom_code = 0x05;
          break;
        case "10000":
          _denom_code = 0x06;
          break;
        case "50000":
          _denom_code = 0x07;
          break;
        case "100000":
          _denom_code = 0x08;
          break;
        case "200000":
          _denom_code = 0x09;
          break;
        case "1000000":
          _denom_code = 0x0A;
          break;
        case "2000":
          _denom_code = 0x0B;
          break;
        case "20000":
          _denom_code = 0x0C;
          break;
        case "25000":
          _denom_code = 0x0D;
          break;
        case "250000":
          _denom_code = 0x0E;
          break;
        case "500000":
          _denom_code = 0x0F;
          break;
        case "2000000":
          _denom_code = 0x10;
          break;
        case "2500000":
          _denom_code = 0x11;
          break;
        case "5000000":
          _denom_code = 0x12;
          break;
        case "10000000":
          _denom_code = 0x13;
          break;
        case "20000000":
          _denom_code = 0x14;
          break;
        case "25000000":
          _denom_code = 0x15;
          break;
        case "50000000":
          _denom_code = 0x16;
          break;
        case "200":
          _denom_code = 0x17;
          break;
        case "300":
          _denom_code = 0x18;
          break;
        case "1500":
          _denom_code = 0x19;
          break;
        case "4000":
          _denom_code = 0x1A;
          break;
        case "50":
          _denom_code = 0x1B;
          break;
        case "25":
          _denom_code = 0x1C;
          break;
        case "20":
          _denom_code = 0x1D;
          break;
        case "10":
          _denom_code = 0x1E;
          break;
        case "5":
          _denom_code = 0x1F;
          break;

      } // switch

      return _denom_code;

    } // GetDenomCode

    public string ToXml()
    {
      XmlDocument _xml;
      XmlNode _root_message_node;
      XmlNode _node;
      XmlNode _denom_node;
      XmlNode _game_node;
      XmlNode _game_list_node;
      XmlNode _meter_node;
      XmlNode _meter_list_node;
      XmlAttribute _attribute;
      String _str_denom_list;

      _xml = new XmlDocument();
      _xml.LoadXml("<WCP_MsgMachineMultiDenomInfo></WCP_MsgMachineMultiDenomInfo>");
      _root_message_node = _xml.SelectSingleNode("WCP_MsgMachineMultiDenomInfo");

      _node = XML.AppendChild(_root_message_node, "DenominationList", "");

      if (DenominationList != null)
      {
        foreach (Denomination _denom in DenominationList)
        {
          _denom_node = XML.AppendChild(_node, "Denomination", "");

          _attribute = _denom_node.OwnerDocument.CreateAttribute("ID");
          _attribute.Value = _denom.Id.ToString();
          _denom_node.Attributes.Append(_attribute);

          _attribute = _denom_node.OwnerDocument.CreateAttribute("Value10000");
          _attribute.Value = _denom.Value10000.ToString();
          _denom_node.Attributes.Append(_attribute);

          _meter_list_node = XML.AppendChild(_denom_node, "MeterList", "");

          if (_denom.MeterList != null)
          {
          foreach (SAS_Meter _meter in _denom.MeterList)
            {
              _meter_node = XML.AppendChild(_meter_list_node, "Meter", "");

              _attribute = _meter_node.OwnerDocument.CreateAttribute("ID");
              _attribute.Value = _meter.Code.ToString();
              _meter_node.Attributes.Append(_attribute);

              _attribute = _meter_node.OwnerDocument.CreateAttribute("Name");
              _attribute.Value = MultiDenom_GetMeterName(_meter.Code);
              _meter_node.Attributes.Append(_attribute);

              _attribute = _meter_node.OwnerDocument.CreateAttribute("Value");
              _attribute.Value = _meter.Value.ToString();
              _meter_node.Attributes.Append(_attribute);

              _attribute = _meter_node.OwnerDocument.CreateAttribute("MaxValue");
              _attribute.Value = _meter.MaxValue.ToString();
              _meter_node.Attributes.Append(_attribute);
            }
          }
        }
      }

      _game_list_node = XML.AppendChild(_root_message_node, "GameList", "");

      if (GameList != null)
      {
        foreach (Game _game in GameList)
        {
          _game_node = XML.AppendChild(_game_list_node, "Game", "");

          _attribute = _game_node.OwnerDocument.CreateAttribute("GameNumber");
          _attribute.Value = _game.GameNumber.ToString();
          _game_node.Attributes.Append(_attribute);

          _attribute = _game_node.OwnerDocument.CreateAttribute("MaxBet");
          _attribute.Value = _game.MaxBet.ToString();
          _game_node.Attributes.Append(_attribute);

          _attribute = _game_node.OwnerDocument.CreateAttribute("ProgressiveGroup");
          _attribute.Value = _game.ProgressiveGroup.ToString();
          _game_node.Attributes.Append(_attribute);

          _attribute = _game_node.OwnerDocument.CreateAttribute("ProgressiveLevels");
          //_attribute.Value = String.Format("0x{0:X8}", _game.ProgressiveLevels);
          _attribute.Value = _game.ProgressiveLevels.ToString();
          _game_node.Attributes.Append(_attribute);

          _attribute = _game_node.OwnerDocument.CreateAttribute("GameName");
          _attribute.Value = CleanStringAttributeValue(_game.GameName);
          _game_node.Attributes.Append(_attribute);

          _attribute = _game_node.OwnerDocument.CreateAttribute("PaytableName");
          _attribute.Value = CleanStringAttributeValue(_game.PaytableName);
          _game_node.Attributes.Append(_attribute);

          _attribute = _game_node.OwnerDocument.CreateAttribute("WagerCategories");
          _attribute.Value = _game.WagerCategories.ToString();
          _game_node.Attributes.Append(_attribute);

          _attribute = _game_node.OwnerDocument.CreateAttribute("GameID");
          _attribute.Value = CleanStringAttributeValue(_game.GameId);
          _game_node.Attributes.Append(_attribute);

          _attribute = _game_node.OwnerDocument.CreateAttribute("GameDenom");
          _attribute.Value = _game.GameDenom.ToString();
          _game_node.Attributes.Append(_attribute);

          _attribute = _game_node.OwnerDocument.CreateAttribute("GamePayOut");
          _attribute.Value = _game.GamePayOut.ToString();
          _game_node.Attributes.Append(_attribute);

          _node = XML.AppendChild(_game_node, "DenominationList", "");          

          _str_denom_list = "";

          if (_game.DenominationList != null)
          {

            foreach (Denomination _denom in _game.DenominationList)
            {
              if (_str_denom_list.Length > 0)
              {
                _str_denom_list += ",";
              }

              _str_denom_list += _denom.Value10000.ToString();
            }
          }

          _attribute = _game_node.OwnerDocument.CreateAttribute("Denominations");
          _attribute.Value = _str_denom_list.ToString();
          _game_node.Attributes.Append(_attribute);
        }
      }

      return _xml.OuterXml;
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      Denomination _denom;
      Game _game;
      SAS_Meter _meter;
      String _aux_name;
      String _progressive_levels;
      Boolean _prog_lvls_hex;
      String _str_denom_list;
      String[] _denoms_array;
      
      if (!Reader.Name.Equals("WCP_MsgMachineMultiDenomInfo"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMachineMultiDenomInfo not found in content area");
      }
      else
      {
        // Main denomination list
        while (Reader.Name != "DenominationList" || !Reader.NodeType.Equals(XmlNodeType.Element))
        {
          Reader.Read();  // Read until End Element 'DenominationList'
        }

        if (DenominationList == null)
        { 
          DenominationList = new ArrayList();
        }

        DenominationList.Clear();

        Reader.Read();

        while (Reader.Name != "DenominationList" || !Reader.NodeType.Equals(XmlNodeType.EndElement))
        {
          _denom = new Denomination();

          _denom.Id = Int32.Parse(Reader.GetAttribute("ID"));          
          _denom.Value10000 = Int64.Parse(Reader.GetAttribute("Value10000"));

#if DEBUG
          //Log.Message(String.Format("Denomination: ID: {0, 2} - Value10000: {1, 10}", _denom.Id.ToString(), _denom.Value10000.ToString()));
#endif

          do
          {
            Reader.Read();
          }
          while (Reader.Name != "MeterList" || !Reader.NodeType.Equals(XmlNodeType.Element));          

          _denom.MeterList = new List<SAS_Meter>();
          _denom.MeterList.Clear();

          Reader.Read();

          while (Reader.Name == "Meter" && Reader.NodeType.Equals(XmlNodeType.Element))
          {
            _meter = new SAS_Meter();

            _meter.Code = Int32.Parse(Reader.GetAttribute("ID"));
            //_meter.Name = Reader.GetAttribute("Name");
            _aux_name = Reader.GetAttribute("Name");
            _meter.Value = Int64.Parse(Reader.GetAttribute("Value"));
            _meter.MaxValue = Int64.Parse(Reader.GetAttribute("MaxValue"));
            _meter.DenomCents = ((Decimal)_denom.Value10000) / 10000;

#if DEBUG
            //Log.Message(String.Format("     Meter[ ID: {0,2} - Name: {1} - Value: {2,10} - MaxValue: {3,10}]", _meter.Code.ToString(), _aux_name.PadRight(15), _meter.Value, _meter.MaxValue));
#endif

            Reader.Read();  

            if (Reader.Name == "Meter" && Reader.NodeType.Equals(XmlNodeType.EndElement))
            {
              _denom.MeterList.Add(_meter);

              Reader.Read(); 
            } 
          }

          if (Reader.Name == "MeterList" && Reader.NodeType.Equals(XmlNodeType.EndElement))
          {
            Reader.Read();
          }
          

          if (Reader.Name == "Denomination" && Reader.NodeType.Equals(XmlNodeType.EndElement))
          {
            DenominationList.Add(_denom);

            Reader.Read();
          }
        }

        // Game list
        while (Reader.Name != "GameList" || !Reader.NodeType.Equals(XmlNodeType.Element))
        {
          Reader.Read();  // Read until End Element 'GameList'
        }

        if (GameList == null)
        {
          GameList = new ArrayList();
        }

        GameList.Clear();

        while (Reader.Name != "GameList" || !Reader.NodeType.Equals(XmlNodeType.EndElement))
        {
          while (Reader.Name == "Game" && Reader.NodeType.Equals(XmlNodeType.Element))
          {
            _game = new Game();

            Int32.TryParse(Reader.GetAttribute("GameNumber"), out _game.GameNumber);
            Int64.TryParse(Reader.GetAttribute("MaxBet"), out _game.MaxBet);
            Int32.TryParse(Reader.GetAttribute("ProgressiveGroup"), out _game.ProgressiveGroup);

            // Progressive Levels must keep compatibility on older versions:
            // Its value can be stored on the XML on decimal and hexadecimal format
            _game.ProgressiveLevels = 0;
            _prog_lvls_hex = false;
            _progressive_levels = Reader.GetAttribute("ProgressiveLevels");

            if ( _progressive_levels.Length > 2 )
            {
              if (_progressive_levels.StartsWith("0x"))
              {
                _prog_lvls_hex = true;
              }
            }

            if (_prog_lvls_hex)
            {
              // Progressive Levels is stored in hex format
              Int32.TryParse(_progressive_levels,
                              System.Globalization.NumberStyles.HexNumber,
                              System.Threading.Thread.CurrentThread.CurrentCulture,
                              out _game.ProgressiveLevels);
            }
            else 
            {
              // Progressive Levels is stored in decimal format
              Int32.TryParse(Reader.GetAttribute("ProgressiveLevels"), out _game.ProgressiveLevels);
            }

            _game.GameName = Reader.GetAttribute("GameName");
            _game.PaytableName = Reader.GetAttribute("PaytableName");
            Int32.TryParse(Reader.GetAttribute("WagerCategories"), out _game.WagerCategories);
            _game.GameId = Reader.GetAttribute("GameID");
            Int64.TryParse(Reader.GetAttribute("GameDenom"), out _game.GameDenom);
            Int64.TryParse(Reader.GetAttribute("GamePayOut"), out _game.GamePayOut);

            // DenomList is a string list delimited by commas

            if (_game.DenominationList == null)
            {
              _game.DenominationList = new ArrayList();
            }

            _game.DenominationList.Clear();

            _str_denom_list = Reader.GetAttribute("Denominations");

            if (!String.IsNullOrEmpty(_str_denom_list))
            {
              if (_str_denom_list != "")
              {
                _denoms_array = _str_denom_list.Split(',');

                foreach (String _str_denom in _denoms_array)
                {
                  _denom = new Denomination();

                  _denom.Id = GetDenomCode(_str_denom);
                  _denom.Value10000 = Int64.Parse(_str_denom);
                  _denom.PaytableName = "";

                  _game.DenominationList.Add(_denom);

  #if DEBUG
                  Log.Message(String.Format("         * Denomination: ID: {0:X2} - Value10000: {1, 10} ", _denom.Id, _denom.Value10000));
  #endif
                }
              }
            }
#if DEBUG
            //Log.Message(String.Format(" * Game[ Number: {0} - MaxBet: {1} - ProgressiveGroup: {2} - ProgressiveLevels: {3} \n                                 Name: {4} -  PaytableName: {5} - WagerCategories: {6} - GameId: {7} \n                                 GameDenom: {8} - PayOut: {9}]",
            //                          _game.GameNumber.ToString(), _game.MaxBet.ToString(), _game.ProgressiveGroup.ToString(), _game.ProgressiveLevels.ToString(), _game.GameName,
            //                          _game.PaytableName, _game.WagerCategories.ToString(), _game.GameId, _game.GameDenom.ToString(), _game.GamePayOut.ToString()));
#endif

            while (Reader.Name != "DenominationList" || !Reader.NodeType.Equals(XmlNodeType.Element))
            {
              Reader.Read();  // Read until End Element 'DenominationList'
            }            

            Reader.Read();

            //JMM 08-MAR-2016: Keep the old DenominationList processing to ensure compatibility with previous protocol versions
            while (Reader.Name != "DenominationList" || !Reader.NodeType.Equals(XmlNodeType.EndElement))
            {
              _denom = new Denomination();

              _denom.Id = Int32.Parse(Reader.GetAttribute("ID"));
              _denom.Value10000 = Int64.Parse(Reader.GetAttribute("Value10000"));
              _denom.PaytableName = Reader.GetAttribute("PaytableName");

              _game.DenominationList.Add(_denom);

#if DEBUG
              //Log.Message(String.Format("         * Denomination: ID: {0, 2} - Value10000: {1, 10} - PaytableName: {2}", _denom.Id.ToString(), _denom.Value10000.ToString(), _denom.PaytableName));
#endif
              do
              {
                Reader.Read();
              } while (Reader.Name != "Denomination" || !Reader.NodeType.Equals(XmlNodeType.EndElement));

              Reader.Read();
            }

            GameList.Add(_game);

            Reader.Read();
          }

          Reader.Read();
        }        

        Reader.Close();
      }
    }

    #endregion

    public static String MultiDenom_GetMeterName (int MeterCode)
    {
      String _meter_name;

      switch (MeterCode)
      {
        case 0x00:
          _meter_name = "Played";
        break;

        case 0x01:
          _meter_name = "Won";
        break;

        case 0x02:
          _meter_name = "Jackpot";
        break;

        case 0x05:
          _meter_name = "Games Played";
        break;

        case 0x06:
          _meter_name = "Games Won";
        break;

        default:
          _meter_name = String.Format("{0}", MeterCode);
        break;
      }

      return _meter_name;
    }
  }

  public class WCP_MsgMachineMultiDenomInfoReply : IXml
  {
    #region IXml Members
    public string ToXml()
    {
      return "<WCP_MsgMachineMultiDenomInfoReply></WCP_MsgMachineMultiDenomInfoReply>";
    }

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      if (Xml.Name != "WCP_MsgMachineMultiDenomInfoReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMachineMultiDenomInfoReply not found in content area");
      }
    }

    #endregion
  }
}
