//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgGameMeters.cs
// 
//   DESCRIPTION: WCP_MsgGameMeters class
// 
// 
// CREATION DATE: 04-MAR-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAR-2010 AAV    First release.
// 07-AUG-2014 ACC    Add Progressive Jackpot Cents into Meters
// 16-SEP-2017 DPC    [WIGOS-4274]: [Ticket #855] Error encontrado Estadísticas - Progresivos
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgReportGameMeters : IXml
  {
    public String GameId;
    public String GameBaseName;
    public Int32  DenominationCents;
    public Int64  PlayedCount;
    public Int64  PlayedCountMaxValue;
    public Int64  PlayedCents;
    public Int64  PlayedMaxValueCents;
    public Int64  WonCount;
    public Int64  WonCountMaxValue;
    public Int64  WonCents;
    public Int64  WonMaxValueCents;
    public Int64  JackpotCents;
    public Int64  JackpotMaxValueCents;
    // ACC 04-AUG-2014 Add Jackpot Progressive Cents into Machine Meters
    public Int64  ProgressiveJackpotCents = 0;
    public Int64  ProgressiveJackpotMaxValueCents = 0;
    public Boolean IgnoreDeltas = false;

    #region IXml Members

    /// <summary>
    /// Convert to XML
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_MsgReportGameMeters></WCP_MsgReportGameMeters>");
      node = xml.SelectSingleNode("WCP_MsgReportGameMeters");

      XML.AppendChild(node, "GameId", GameId.ToString());
      XML.AppendChild(node, "GameBaseName", GameBaseName.ToString());
      XML.AppendChild(node, "DenominationCents", DenominationCents.ToString());
      XML.AppendChild(node, "PlayedCount", PlayedCount.ToString());
      XML.AppendChild(node, "PlayedCountMaxValue", PlayedCountMaxValue.ToString());
      XML.AppendChild(node, "PlayedCents", PlayedCents.ToString());
      XML.AppendChild(node, "PlayedMaxValueCents", PlayedMaxValueCents.ToString());
      XML.AppendChild(node, "WonCount", WonCount.ToString());
      XML.AppendChild(node, "WonCountMaxValue", WonCountMaxValue.ToString());
      XML.AppendChild(node, "WonCents", WonCents.ToString());
      XML.AppendChild(node, "WonMaxValueCents", WonMaxValueCents.ToString());
      XML.AppendChild(node, "JackpotCents", JackpotCents.ToString());
      XML.AppendChild(node, "JackpotMaxValueCents", JackpotMaxValueCents.ToString());
      XML.AppendChild(node, "ProgressiveJackpotCents", ProgressiveJackpotCents.ToString());
      XML.AppendChild(node, "ProgressiveJackpotMaxValueCents", ProgressiveJackpotMaxValueCents.ToString());
      XML.AppendChild(node, "IgnoreDeltas", IgnoreDeltas ? "1" : "0");

      return xml.OuterXml;
    } // ToXml

    /// <summary>
    /// Load xml
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      String _value;
      XmlDocument _xml;

      if (Reader.Name != "WCP_MsgReportGameMeters")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportGameMeters not found in content area");
      }
      else
      {
        _xml = new XmlDocument();
        _xml.Load(Reader);

        GameId = getValueXML(_xml, "GameId");
        GameBaseName = getValueXML(_xml, "GameBaseName");
        DenominationCents = Int32.Parse(getValueXML(_xml, "DenominationCents"));
        PlayedCount = Int64.Parse(getValueXML(_xml, "PlayedCount"));
        PlayedCountMaxValue = Int64.Parse(getValueXML(_xml, "PlayedCountMaxValue"));
        PlayedCents = Int64.Parse(getValueXML(_xml, "PlayedCents"));
        PlayedMaxValueCents = Int64.Parse(getValueXML(_xml, "PlayedMaxValueCents"));
        WonCount = Int64.Parse(getValueXML(_xml, "WonCount"));
        WonCountMaxValue = Int64.Parse(getValueXML(_xml, "WonCountMaxValue"));
        WonCents = Int64.Parse(getValueXML(_xml, "WonCents"));
        WonMaxValueCents = Int64.Parse(getValueXML(_xml, "WonMaxValueCents"));
        JackpotCents = Int64.Parse(getValueXML(_xml, "JackpotCents"));
        JackpotMaxValueCents = Int64.Parse(getValueXML(_xml, "JackpotMaxValueCents"));

        _value = getValueXML(_xml, "ProgressiveJackpotCents");

        if (_value != String.Empty)
        {
          ProgressiveJackpotCents = Int64.Parse(_value);
        }

        _value = getValueXML(_xml, "ProgressiveJackpotMaxValueCents");

        if (_value != String.Empty)
        {
          ProgressiveJackpotMaxValueCents = Int64.Parse(_value);
        }

        _value = getValueXML(_xml, "IgnoreDeltas");

        if (_value != String.Empty)
        {
          IgnoreDeltas = (_value == "1");
        }

        Reader.Close();
      }
    } // LoadXml

    /// <summary>
    /// Get values from XML
    /// </summary>
    /// <param name="Xml"></param>
    /// <param name="FieldXml"></param>
    /// <returns></returns>
    private String getValueXML(XmlDocument Xml, String FieldXml)
    {
      XmlNode _node;

      try
      {
        _node = Xml.SelectSingleNode(String.Format("//{0}", FieldXml));

        if (_node == null)
        {
          return String.Empty;
        }
        else
        {
          return _node.InnerXml;
        }
      }
      catch (Exception _ex)
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, _ex.Message);
    }
    } // getValueXML

    #endregion
  }

  public class WCP_MsgReportGameMetersReply : IXml
  {
    #region IXml Members
    public string ToXml()
    {
      return "<WCP_MsgReportGameMetersReply></WCP_MsgReportGameMetersReply>";

      ////XmlDocument xml;
      ////XmlNode node;

      ////xml = new XmlDocument();

      ////node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportGameMetersReply", "");
      ////xml.AppendChild(node);

      ////return xml.OuterXml; 
    }

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      if (Xml.Name != "WCP_MsgReportGameMetersReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportGameMetersReply not found in content area");
      }
    }

    #endregion
  }
}
