//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgReportMeters.cs
// 
//   DESCRIPTION: WCP_MsgReportMeters class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 15-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MoneyMeter
  {
    public WCP_CashTypes CashType;
    public WCP_MoneyTypes MoneyType;
    public Int32 FaceValue;
    public Int32 Count;
    public Int64 Amount;
  }

  public class WCP_MsgReportMeters:IXml
  {
    public Int32 PlayedCount;
    public Int64 PlayedAmount;
    public Int32 WonCount;
    public Int64 WonAmount;
    public Int64 CashInAmount;
    public Int64 CashOutAmount;

    public ArrayList MoneyMeters = new ArrayList ();

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode play_meter;
      XmlNode cash_meter;
      XmlNode money_meter;
      WCP_MoneyMeter wcp_money_meter_item;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_MsgReportMeters></WCP_MsgReportMeters>");
      node = xml.SelectSingleNode("WCP_MsgReportMeters");

      play_meter = xml.CreateNode(XmlNodeType.Element, "PlayMeter", "");
      node.AppendChild(play_meter);
      XML.AppendChild(play_meter, "PlayedCount", PlayedCount.ToString());
      XML.AppendChild(play_meter, "PlayedAmount", PlayedAmount.ToString());
      XML.AppendChild(play_meter, "WonCount", WonCount.ToString());
      XML.AppendChild(play_meter, "WonAmount", WonAmount.ToString());

      cash_meter = xml.CreateNode(XmlNodeType.Element, "CashMeter", "");
      node.AppendChild(cash_meter);
      XML.AppendChild(cash_meter, "CashInAmount", CashInAmount.ToString());
      XML.AppendChild(cash_meter, "CashOutAmount", CashOutAmount.ToString());

      for (int idx_money_meter = 0; idx_money_meter < MoneyMeters.Count; idx_money_meter++)
      {
        money_meter = xml.CreateNode(XmlNodeType.Element, "MoneyMeter", "");
        cash_meter.AppendChild(money_meter);

        wcp_money_meter_item = (WCP_MoneyMeter)MoneyMeters[idx_money_meter];

        if (wcp_money_meter_item.CashType == WCP_CashTypes.CASH_TYPE_CASH_IN)
        {
          XML.AppendChild(money_meter, "CashType", "CashIn");
        }
        else
        {
          XML.AppendChild(money_meter, "CashType", "CashOut");
        }

        if (wcp_money_meter_item.MoneyType == WCP_MoneyTypes.MONEY_TYPE_NOTE)
        {
          XML.AppendChild(money_meter, "MoneyType", "Note");
        }
        else
        {
          XML.AppendChild(money_meter, "MoneyType", "Coin");
        }

        XML.AppendChild(money_meter, "FaceValue", wcp_money_meter_item.FaceValue.ToString());
        XML.AppendChild(money_meter, "Count", wcp_money_meter_item.Count.ToString());
        XML.AppendChild(money_meter, "Amount", wcp_money_meter_item.Amount.ToString());
      }

      return xml.OuterXml;
    }
    

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Xml"></param>
    public void LoadXml(XmlReader reader)
    {
      WCP_MoneyMeter wcp_money_meter_item;
      String cash_money_type;

      if (reader.Name != "WCP_MsgReportMeters")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportMeters not found in content area");
      }
      else
      {
        //PlayMeter
        PlayedCount = Int32.Parse(XML.GetValue(reader, "PlayedCount"));
        PlayedAmount = Int64.Parse(XML.GetValue(reader, "PlayedAmount"));
        WonCount = Int32.Parse(XML.GetValue(reader, "WonCount"));
        WonAmount = Int64.Parse(XML.GetValue(reader, "WonAmount"));
        
        //CashMeter
        CashInAmount = Int64.Parse(XML.GetValue(reader, "CashInAmount"));
        CashOutAmount = Int64.Parse(XML.GetValue(reader, "CashOutAmount"));

        reader.Read();
        while ((reader.Name != "CashMeter") || (reader.NodeType != XmlNodeType.EndElement)) 
        {
          wcp_money_meter_item = new WCP_MoneyMeter();

          cash_money_type = XML.GetValue(reader, "CashType");
          if (cash_money_type == "CashIn")
          {
            wcp_money_meter_item.CashType = WCP_CashTypes.CASH_TYPE_CASH_IN;
          }
          else
          {
            wcp_money_meter_item.CashType = WCP_CashTypes.CASH_TYPE_CASH_OUT;
          }

          cash_money_type = XML.GetValue(reader, "MoneyType");
          if (cash_money_type == "Note")
          {
            wcp_money_meter_item.MoneyType = WCP_MoneyTypes.MONEY_TYPE_NOTE;
          }
          else
          {
            wcp_money_meter_item.MoneyType = WCP_MoneyTypes.MONEY_TYPE_COIN;
          }

          wcp_money_meter_item.FaceValue = Int32.Parse(XML.GetValue(reader, "FaceValue"));
          wcp_money_meter_item.Count = Int32.Parse(XML.GetValue(reader, "Count"));
          wcp_money_meter_item.Amount = Int64.Parse(XML.GetValue(reader, "Amount"));

          MoneyMeters.Add(wcp_money_meter_item);
          //End of the amount
          reader.Read();
          //End of the MoneyMeter
          reader.Read();
          //End of CashMeter or new MoneyMeter
          reader.Read();
        }
      }
    }
  }

  public class WCP_MsgReportMetersReply : IXml
  {
    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportMetersReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgReportMetersReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportMetersReply not found in content area");
      }
    }
  }
}
