//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgHPCMeter.cs
// 
//   DESCRIPTION: WCP_MsgHPCMeter class
// 
// 
// CREATION DATE: 04-MAR-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAR-2010 AAV    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{


  public class WCP_MsgReportHPCMeter : IXml
  {
    public Int64  HandPaysCents;
    public Int64  HandPaysMaxValueCents;
    public Boolean IgnoreDeltas = false;

    public ArrayList SasMeters
    {
      get
      {
        ArrayList _array_list;

        _array_list = new ArrayList(1);
        _array_list.Add(new SAS_Meter(0x03, 0, 0, HandPaysCents, HandPaysMaxValueCents));

        return _array_list;
      }
    }

    #region IXml Members

    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_MsgReportHPCMeter></WCP_MsgReportHPCMeter>");
      node = xml.SelectSingleNode("WCP_MsgReportHPCMeter");

      XML.AppendChild(node, "HandPaysCents", HandPaysCents.ToString());
      XML.AppendChild(node, "HandPaysMaxValueCents", HandPaysMaxValueCents.ToString());
      XML.AppendChild(node, "IgnoreDeltas", (IgnoreDeltas ? "1" : "0"));

      return xml.OuterXml;
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgReportHPCMeter")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportHPCMeter not found in content area");
      }
      else
      {
        HandPaysCents = Int64.Parse(XML.GetValue(Reader, "HandPaysCents"));
        HandPaysMaxValueCents = Int64.Parse(XML.GetValue(Reader, "HandPaysMaxValueCents"));
        IgnoreDeltas = XML.GetValue(Reader, "IgnoreDeltas", true) == "1";

        Reader.Close();
      }
    }

    #endregion
  }

  public class WCP_MsgReportHPCMeterReply : IXml
  {
    #region IXml Members
    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportHPCMeterReply", "");
      xml.AppendChild(node);

      return xml.OuterXml; 
    }

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      if (Xml.Name != "WCP_MsgReportHPCMeterReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportHPCMeterReply not found in content area");
      }
    }

    #endregion
  }
}
