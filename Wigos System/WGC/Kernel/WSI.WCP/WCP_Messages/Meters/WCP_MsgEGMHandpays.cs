//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ReportEGMHandpays.cs
// 
//   DESCRIPTION: class that represents terminal meters structure
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 02-DEC-2013
// 
// REVISION HISTORY:
// 
// Date            Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2013 JMM    First release.
// 05-MAR-2018 FJC    Talked with David Lasdiez. we don't add Bug to the Jira. 
//                    Fix Parse from Int32 to Int64 (TransactionId beteen SAS and WCP)
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  /// <summary>
  /// Class that will control the msg creation for Terminal meters anouncement
  /// OJO: Class name MUST MATCH with definition in enum WCP_MsgTypes
  /// </summary>
  public class WCP_MsgEGMHandpays : IXml
  {
    protected static String MESSAGE_ENVELOPE = "WCP_MsgEGMHandpays";
    protected static String HANDPAYS_COMMON_NODE = "Handpays";
    protected static String HANDPAYS_NODE = "Handpay";

    public Int64 TransactionID;
    public DateTime Datetime;
    public HANDPAY_TYPE HandpayType;
    public Int64 HandpayCents;
    public Int64 PlaySessionID;
    public Int64 PreviousPlaySessionID;
    public Byte ProgressiveGroup;
    public Byte Level;
    public Int64 PartialPayCents;
    public Byte ResetID;

    public WCP_MsgEGMHandpays()
    {      
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Convert internal properties to string in XML format
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String in XML format
    //
    public String ToXml()
    {
      StringBuilder _sb;
      String _attributes;      

      _sb = new StringBuilder();
      _sb.AppendLine("<" + MESSAGE_ENVELOPE + ">");
      
      _sb.AppendLine("<" + HANDPAYS_COMMON_NODE + ">");
     
      // concatenate values in ATTRIBUTE format
      _attributes = " TransactionID=\"" + this.TransactionID.ToString() + "\" ";
      _attributes += "Datetime=\"" + XML.XmlDateTimeString(this.Datetime) + "\" ";
      _attributes += "HandpayType=\"" + this.HandpayType.ToString() + "\" ";
      _attributes += "HandpayCents=\"" + this.HandpayCents.ToString() + "\" ";
      _attributes += "PlaySessionID=\"" + this.PlaySessionID.ToString() + "\" ";
      _attributes += "PreviousPlaySessionID=\"" + this.PreviousPlaySessionID.ToString() + "\" ";
      _attributes += "ProgressiveGroup=\"" + this.ProgressiveGroup.ToString() + "\" ";
      _attributes += "Level=\"" + this.Level.ToString() + "\" ";
      _attributes += "PartialPayCents=\"" + this.PartialPayCents.ToString() + "\" ";
      _attributes += "ResetID=\"" + this.ResetID.ToString() + "\" ";

      // add the HANDPAY node
      _sb.AppendLine("<" + HANDPAYS_NODE + _attributes + "></" + HANDPAYS_NODE + ">");
    
      _sb.AppendLine("</" + HANDPAYS_COMMON_NODE + ">");
      _sb.AppendLine("</" + MESSAGE_ENVELOPE + ">");

      return _sb.ToString();
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse input string, in XML format, and fill internal properties
    //           Bad structure raises WCP_Exception
    //
    //  PARAMS :
    //      - INPUT :
    //        - Reader: loaded XML structure
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      XmlDocument _doc;
      XmlNode _node;

      if (!Reader.Name.Equals(MESSAGE_ENVELOPE))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag '" + MESSAGE_ENVELOPE + "' not found in content area");
      }
      
      _doc = new XmlDocument();
      _doc.Load(Reader);

      _node = _doc.GetElementsByTagName(HANDPAYS_NODE).Item(0);

      TransactionID = Int64.Parse(XML.GetAttributeValue(_node, "TransactionID"));
      DateTime.TryParse(XML.GetAttributeValue(_node, "Datetime"), out Datetime);
      HandpayType = (HANDPAY_TYPE)Enum.Parse(typeof(HANDPAY_TYPE), XML.GetAttributeValue(_node, "HandpayType"));
      HandpayCents = Int64.Parse(XML.GetAttributeValue(_node, "HandpayCents"));
      PlaySessionID = Int64.Parse(XML.GetAttributeValue(_node, "PlaySessionID"));
      PreviousPlaySessionID = Int64.Parse(XML.GetAttributeValue(_node, "PreviousPlaySessionID"));
      ProgressiveGroup = Byte.Parse(XML.GetAttributeValue(_node, "ProgressiveGroup"));
      Level = Byte.Parse(XML.GetAttributeValue(_node, "Level"));
      PartialPayCents = Int64.Parse(XML.GetAttributeValue(_node, "PartialPayCents"));
      ResetID = Byte.Parse(XML.GetAttributeValue(_node, "ResetID"));

    }
  } // LoadXml

  /// <summary>
  /// Class than will control the message creation for Terminal meters anouncement reply
  /// </summary>
  public class WCP_MsgEGMHandpaysReply : IXml
  {
    protected static String MESSAGE_ENVELOPE = "WCP_MsgEGMHandpaysReply";

    //------------------------------------------------------------------------------
    // PURPOSE : Convert internal properties to string in XML format
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String in XML format
    //
    public String ToXml()
    {
      return "<" + MESSAGE_ENVELOPE + "></" + MESSAGE_ENVELOPE + ">";
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse input string, in XML format, and fill internal properties
    //           Bad structure raises WCP_Exception
    //
    //  PARAMS :
    //      - INPUT :
    //        - Reader: loaded XML structure
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      if (!Reader.Name.Equals(MESSAGE_ENVELOPE))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag " + MESSAGE_ENVELOPE + " not found in content area");
      }

    } // LoadXml
  }
}