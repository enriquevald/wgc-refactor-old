//-------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//---------------- ---------------------------------------------------
//
// MODULE NAME:   WCP_MsgPlaySession.cs
// DESCRIPTION:   
// AUTHOR:        Nelson Madrigal
// CREATION DATE: 12-NOV-2013
//
// REVISION HISTORY:
// 
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 12-NOV-2013  NMR    Initial version
// ...
// 25-MAR-2014  DDM & RCI & MPO    Fixed Bug WIGOSTITO-1169: Calculate PLAY_SESSIONS meters correctly (Redeemable and Non-Redeemable parts)
// 11-JUL-2014 XCD    Calculate estimated points awarded
// 14-SEP-2015 DHA    Product Backlog Item 3705: Added coins from terminals
// 31-AUG-2017 JMM    WIGOS-4700-Promotional credit reported as cashable after card out/in: Modify LKT-SAS-HOST for send counters
// 12-SEP-2017 JML    PBI 29479: WIGOS-4700 Promotional credit reported as cashable after card out/in
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP.WCP_Messages.Meters
{
  // machine meters
  public struct MachineMeters
  {
    public Int64 played_cents;
    public Int64 won_cents;
    public Int64 jackpot_cents;
    public Int64 num_played;
    public Int64 num_won;
    public Int64 played_reedemable_cents;
    public Int64 played_restricted_cents;
    public Int64 played_non_restricted_cents;
  };

  // TITO Session Meters
  public struct TITO_SessionMeters
  {
    public Int64 ticket_in_cashable_cents;   // SAS Meter 0x80
    public Int64 ticket_in_promo_nr_cents;   // SAS Meter 0x82
    public Int64 ticket_in_promo_re_cents;   // SAS Meter 0x84
    public Int64 ticket_out_cashable_cents;  // SAS Meter 0x0E (0x87 + 0x8B = Cashable+Debit)
    public Int64 ticket_out_promo_nr_cents;  // SAS Meter 0x88
    public Int64 cash_in_coins_in_cents;     // SAS Meter 0x08
    public Int64 cash_in_bills_in_cents;     // SAS Meter 0x0B
    public Int64 cash_in_total_in_cents;     // SAS Meter 0x08 + 0x0B
    public Int64 handpay_cents;              // SAS Meter 0x03
  };

  // FT Meters
  public struct FT_Meters
  {
    public Int64 type;
    public Int64 to_gm_cashable_cents;
    public Int64 to_gm_restricted_cents;
    public Int64 to_gm_non_restricted_cents;
    public Int64 from_gm_cashable_cents;
    public Int64 from_gm_restricted_cents;
    public Int64 from_gm_non_restricted_cents;
  }

  /// <summary>
  /// Class that will control msg creation for terminal play-session anouncement
  /// </summary>
  public class WCP_MsgPlaySessionMeters : IXml
  {
    protected const String MESSAGE_ENVELOPE = "WCP_MsgPlaySessionMeters";
    protected const String MACHINE_METERS = "MachineMeter";
    protected const String TITO_SESSION_METERS = "TITO_SessionMeter";
    protected const String CASH_IN_METERS = "Meter_CashIn";
    protected const String FT_METERS = "FT_Meters";
    protected const String TO_ACCOUNT_ON_CLOSE = "To_Account_On_Close";
    protected const String FOUND_ON_EGM = "Found_On_EGM";
    protected const String REMAINING_ON_EGM = "Remaining_On_EGM";
    protected static DateTime m_min_valid_date = new DateTime(2010, 1, 1);

    public Int64 play_session_id;
    public Boolean closed;
    public DateTime started_datetime;
    public DateTime closed_datetime;
    public String track_data1;          // COMMON_MAX_SMART_CARD_ID_LENGTH = 27
    public String track_data2;          // COMMON_MAX_SMART_CARD_ID_LENGTH
    public MachineMeters machine_meters;
    public TITO_SessionMeters tito_session_meters;
    public FT_Meters ft_meters;

    public Boolean mico2_account_session = false;
    public MultiPromos.AccountBalance to_account_on_close = MultiPromos.AccountBalance.Zero;
    public MultiPromos.AccountBalance found_on_egm = MultiPromos.AccountBalance.Zero;
    public MultiPromos.AccountBalance remaining_on_egm = MultiPromos.AccountBalance.Zero;

    public WCP_MsgPlaySessionMeters()
    {
      machine_meters = new MachineMeters();
      tito_session_meters = new TITO_SessionMeters();
    }

    public MultiPromos.EndSessionInput EndSessionInput()
    {
      MultiPromos.EndSessionInput _input;

      _input = new MultiPromos.EndSessionInput();
      _input.TitoSessionMeters = GetTITOSessionMeters();
      _input.HasMeters = true;
      _input.Meters = GetMeters();

      _input.IsMico2 = mico2_account_session;
      if (mico2_account_session)
      {
        _input.BalanceToAccount = to_account_on_close;
        _input.DeltaBalanceFoundInSession = remaining_on_egm - found_on_egm;
        if (_input.DeltaBalanceFoundInSession.TotalBalance < 0)
        {
          _input.DeltaBalanceFoundInSession = MultiPromos.AccountBalance.Zero;
        }
        _input.BalanceFromGM = _input.BalanceToAccount.TotalBalance;
        _input.RemainingInEgm = remaining_on_egm;
        _input.FoundInEgm = found_on_egm;
      }
      else
      {
        _input.BalanceFromGM = _input.TitoSessionMeters.TotalCashIn - _input.TitoSessionMeters.TotalCashOut;
      }

      _input.PlaySessionId = this.play_session_id;
      _input.IsTITO = true;

      return _input;
    } // EndSessionInput

    public void GetMeters(ref MultiPromos.PlayedWonMeters Meters, ref MultiPromos.TITOSessionMeters TITO)
    {

      Meters = GetMeters();
      TITO = GetTITOSessionMeters();

    }

    private MultiPromos.AccountBalance GetDeltaBalanceInMachine()
    {
      //MultiPromos.AccountBalance _on_start_session;
      //MultiPromos.AccountBalance _on_end_session;
      //MultiPromos.AccountBalance _delta;

      return MultiPromos.AccountBalance.Zero;

    }

    private MultiPromos.TITOSessionMeters GetTITOSessionMeters()
    {
      MultiPromos.TITOSessionMeters _tito_session_meters;

      _tito_session_meters = new MultiPromos.TITOSessionMeters();
      _tito_session_meters.TicketInCashable = ((Decimal)this.tito_session_meters.ticket_in_cashable_cents) / 100.00m;
      _tito_session_meters.TicketInPromoNr = ((Decimal)this.tito_session_meters.ticket_in_promo_nr_cents) / 100.00m;
      _tito_session_meters.TicketInPromoRe = ((Decimal)this.tito_session_meters.ticket_in_promo_re_cents) / 100.00m;
      _tito_session_meters.TicketOutCashable = ((Decimal)this.tito_session_meters.ticket_out_cashable_cents) / 100.00m;
      _tito_session_meters.TicketOutPromoNr = ((Decimal)this.tito_session_meters.ticket_out_promo_nr_cents) / 100.00m;

      // DHA: added coins/bills/total
      _tito_session_meters.CashIn.CashInCoins = ((Decimal)this.tito_session_meters.cash_in_coins_in_cents) / 100.00m;
      _tito_session_meters.CashIn.CashInBills = ((Decimal)this.tito_session_meters.cash_in_bills_in_cents) / 100.00m;
      _tito_session_meters.CashIn.CashInTotal = ((Decimal)this.tito_session_meters.cash_in_total_in_cents) / 100.00m;

      return _tito_session_meters;
    }

    private MultiPromos.PlayedWonMeters GetMeters()
    {
      MultiPromos.PlayedWonMeters _meters;

      _meters = new MultiPromos.PlayedWonMeters();
      _meters.JackpotAmount = ((Decimal)this.machine_meters.jackpot_cents) / 100.00m;
      _meters.PlayedAmount = ((Decimal)this.machine_meters.played_cents) / 100.00m;
      _meters.WonAmount = ((Decimal)this.machine_meters.won_cents) / 100.00m;
      _meters.PlayedCount = this.machine_meters.num_played;
      _meters.WonCount = this.machine_meters.num_won;
      _meters.HandpaysAmount = ((Decimal)this.tito_session_meters.handpay_cents) / 100.00m;
      _meters.PlayedReedemable = ((Decimal)this.machine_meters.played_reedemable_cents) / 100.00m;
      _meters.PlayedNonRestrictedAmount = ((Decimal)this.machine_meters.played_non_restricted_cents) / 100.00m;
      _meters.PlayedRestrictedAmount = ((Decimal)this.machine_meters.played_restricted_cents) / 100.00m;

      return _meters;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Convert internal properties to string in XML format
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String in XML format
    //
    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<" + MESSAGE_ENVELOPE + ">");

      // message common data
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "PlaySessionId", play_session_id));
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "Closed", closed ? "1" : "0"));
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "StartedDatetime", XML.XmlDateTimeString(started_datetime)));
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "FinishedDatetime", XML.XmlDateTimeString(closed_datetime)));
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "TrackData1", XML.StringToXmlValue(track_data1)));
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "TrackData2", XML.StringToXmlValue(track_data2)));

      // machine meters
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", MACHINE_METERS, "PlayedCents", machine_meters.played_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", MACHINE_METERS, "WonCents", machine_meters.won_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", MACHINE_METERS, "JackpotCents", machine_meters.jackpot_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", MACHINE_METERS, "NumPlayed", machine_meters.num_played));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", MACHINE_METERS, "NumWon", machine_meters.num_won));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", MACHINE_METERS, "PlayedReedemableCents", machine_meters.played_reedemable_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", MACHINE_METERS, "PlayedRestrictedCents", machine_meters.played_restricted_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", MACHINE_METERS, "PlayedNonRestrictedCents", machine_meters.played_non_restricted_cents));

      // tito play-session meters
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TITO_SESSION_METERS, "TicketInCashableCents", tito_session_meters.ticket_in_cashable_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TITO_SESSION_METERS, "TicketInPromoNRCents", tito_session_meters.ticket_in_promo_nr_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TITO_SESSION_METERS, "TicketInPromoRECents", tito_session_meters.ticket_in_promo_re_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TITO_SESSION_METERS, "TicketOutCashableCents", tito_session_meters.ticket_out_cashable_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TITO_SESSION_METERS, "TicketOutPromoNRCents", tito_session_meters.ticket_out_promo_nr_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TITO_SESSION_METERS, "BillInCents", tito_session_meters.cash_in_bills_in_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TITO_SESSION_METERS, "HandpayCents", tito_session_meters.handpay_cents));

      // Cash In Meters
      _sb.AppendLine("<" + CASH_IN_METERS + ">");
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", CASH_IN_METERS, "CoinsCents", tito_session_meters.cash_in_coins_in_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", CASH_IN_METERS, "BillsCents", tito_session_meters.cash_in_bills_in_cents));
      _sb.AppendLine("</" + CASH_IN_METERS + ">");

      // FT Meters
      _sb.AppendLine("<" + FT_METERS + ">");
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", FT_METERS, "ToGMCashableCents", ft_meters.to_gm_cashable_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", FT_METERS, "ToGMRestrictedCents", ft_meters.to_gm_restricted_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", FT_METERS, "ToGMNonRestrictedCents", ft_meters.to_gm_non_restricted_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", FT_METERS, "FromGMCashableCents", ft_meters.from_gm_cashable_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", FT_METERS, "FromGMRestrictedCents", ft_meters.from_gm_restricted_cents));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", FT_METERS, "FromGMNonRestrictedCents", ft_meters.from_gm_non_restricted_cents));
      _sb.AppendLine("</" + FT_METERS + ">");

      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "Mico2AccountSession", mico2_account_session ? "1" : "0"));

      // To Account On Close
      if (mico2_account_session)
      {
        _sb.AppendLine("<" + TO_ACCOUNT_ON_CLOSE + ">");
        _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TO_ACCOUNT_ON_CLOSE, "CashableCents", to_account_on_close.Redeemable));
        _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TO_ACCOUNT_ON_CLOSE, "RestrictedCents", to_account_on_close.PromoNotRedeemable));
        _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", TO_ACCOUNT_ON_CLOSE, "NonRestrictedCents", to_account_on_close.PromoRedeemable));
        _sb.AppendLine("</" + TO_ACCOUNT_ON_CLOSE + ">");
      }

      _sb.AppendLine("<" + FOUND_ON_EGM + ">");
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", FOUND_ON_EGM, "CashableCents", found_on_egm.Redeemable));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", FOUND_ON_EGM, "RestrictedCents", found_on_egm.PromoNotRedeemable));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", FOUND_ON_EGM, "NonRestrictedCents", found_on_egm.PromoRedeemable));
      _sb.AppendLine("</" + FOUND_ON_EGM + ">");

      _sb.AppendLine("<" + REMAINING_ON_EGM + ">");
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", REMAINING_ON_EGM, "CashableCents", remaining_on_egm.Redeemable));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", REMAINING_ON_EGM, "RestrictedCents", remaining_on_egm.PromoNotRedeemable));
      _sb.AppendLine(String.Format("<{0}_{1}>{2}</{0}_{1}>", REMAINING_ON_EGM, "NonRestrictedCents", remaining_on_egm.PromoRedeemable));
      _sb.AppendLine("</" + REMAINING_ON_EGM + ">");

      _sb.AppendLine("</" + MESSAGE_ENVELOPE + ">");

      return _sb.ToString();
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Get the Int64 value corresponding to a given XML tag
    //           Bad structure raises WCP_Exception
    //
    //  PARAMS :
    //      - INPUT :
    //        - Reader: loaded XML structure
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      The tag value
    //
    private long XML_GetTagAsInt64(XmlReader Reader, String SubTag1, String SubTag2)
    {
      long _return_value;

      _return_value = 0;

      //We need to ensure we are on the proper tag
      while ((Reader.Name == "" || Reader.NodeType == XmlNodeType.EndElement) && !Reader.EOF)
      {
        Reader.Read();
      }

      if (Reader.Name == String.Format("{0}_{1}", SubTag1, SubTag2))
      {
        _return_value = XML.GetAsInt64(Reader, SubTag1 + "_" + SubTag2);
      }

      return _return_value;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parse input string, in XML format, and fill internal properties
    //           Bad structure raises WCP_Exception
    //
    //  PARAMS :
    //      - INPUT :
    //        - Reader: loaded XML structure
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      if (!Reader.Name.Equals(MESSAGE_ENVELOPE))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag '" + MESSAGE_ENVELOPE + "' not found in content area");
      }

      // common data
      play_session_id = XML.GetAsInt64(Reader, "PlaySessionId");
      closed = XML.GetValue(Reader, "Closed") == "1";
      started_datetime = XML.GetAsDateTime(Reader, "StartedDatetime");
      closed_datetime = XML.GetAsDateTime(Reader, "FinishedDatetime");
      if (closed_datetime < m_min_valid_date)
      {
        closed_datetime = new DateTime(0);
      }
      track_data1 = XML.GetValue(Reader, "TrackData1");
      track_data2 = XML.GetValue(Reader, "TrackData2");

      // machine meters
      machine_meters.played_cents = XML.GetAsInt64(Reader, MACHINE_METERS + "_PlayedCents");
      machine_meters.won_cents = XML.GetAsInt64(Reader, MACHINE_METERS + "_WonCents");
      machine_meters.jackpot_cents = XML.GetAsInt64(Reader, MACHINE_METERS + "_JackpotCents");
      machine_meters.num_played = XML.GetAsInt64(Reader, MACHINE_METERS + "_NumPlayed");
      machine_meters.num_won = XML.GetAsInt64(Reader, MACHINE_METERS + "_NumWon");

      machine_meters.played_reedemable_cents = XML_GetTagAsInt64(Reader, MACHINE_METERS, "PlayedReedemableCents");
      machine_meters.played_restricted_cents = XML_GetTagAsInt64(Reader, MACHINE_METERS, "PlayedRestrictedCents");
      machine_meters.played_non_restricted_cents = XML_GetTagAsInt64(Reader, MACHINE_METERS, "PlayedNonRestrictedCents");

      long _sum_parts;
      _sum_parts = machine_meters.played_reedemable_cents + machine_meters.played_restricted_cents + machine_meters.played_non_restricted_cents;
      if (machine_meters.played_cents != _sum_parts)
      {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.AppendLine("WCP_MsgPlaySessionMeters *** MachineMeters Mismatch *** " + string.Format("PlaySessionId: {0} ", play_session_id.ToString()));
        sb.AppendLine(string.Format("Init Total:{0}, Re:{1}, PNr:{2}, PRe:{3}", machine_meters.played_cents, machine_meters.played_reedemable_cents, machine_meters.played_restricted_cents, machine_meters.played_non_restricted_cents));

        machine_meters.played_cents = Math.Min(machine_meters.played_cents, _sum_parts);
        _sum_parts = machine_meters.played_cents;
        machine_meters.played_restricted_cents = Math.Min(machine_meters.played_restricted_cents, _sum_parts); _sum_parts -= machine_meters.played_restricted_cents;
        machine_meters.played_non_restricted_cents = Math.Min(machine_meters.played_non_restricted_cents, _sum_parts); _sum_parts -= machine_meters.played_non_restricted_cents;
        machine_meters.played_reedemable_cents = Math.Min(machine_meters.played_reedemable_cents, _sum_parts); _sum_parts -= machine_meters.played_reedemable_cents;

        sb.AppendLine(string.Format("End Total:{0}, Re:{1}, PNr:{2}, PRe:{3}", machine_meters.played_cents, machine_meters.played_reedemable_cents, machine_meters.played_restricted_cents, machine_meters.played_non_restricted_cents));
        Log.Message("WARNING: " + sb.ToString());
      }

      // TITO play session meters
      tito_session_meters.ticket_in_cashable_cents = XML.GetAsInt64(Reader, TITO_SESSION_METERS + "_TicketInCashableCents");
      tito_session_meters.ticket_in_promo_nr_cents = XML.GetAsInt64(Reader, TITO_SESSION_METERS + "_TicketInPromoNRCents");
      tito_session_meters.ticket_in_promo_re_cents = XML.GetAsInt64(Reader, TITO_SESSION_METERS + "_TicketInPromoRECents");
      tito_session_meters.ticket_out_cashable_cents = XML.GetAsInt64(Reader, TITO_SESSION_METERS + "_TicketOutCashableCents");
      tito_session_meters.ticket_out_promo_nr_cents = XML.GetAsInt64(Reader, TITO_SESSION_METERS + "_TicketOutPromoNRCents");

      tito_session_meters.cash_in_coins_in_cents = 0;
      tito_session_meters.cash_in_bills_in_cents = XML.GetAsInt64(Reader, TITO_SESSION_METERS + "_BillInCents");
      tito_session_meters.cash_in_total_in_cents = tito_session_meters.cash_in_bills_in_cents;

      tito_session_meters.handpay_cents = XML_GetTagAsInt64(Reader, TITO_SESSION_METERS, "HandpayCents");

      // CashIn Meters
      while (Reader.Name != CASH_IN_METERS && !Reader.EOF)
      {
        Reader.Read();
      }

      if (Reader.Name == CASH_IN_METERS)
      {
        tito_session_meters.cash_in_coins_in_cents = XML.GetAsInt64(Reader, CASH_IN_METERS + "_CoinsCents");
        tito_session_meters.cash_in_bills_in_cents = XML.GetAsInt64(Reader, CASH_IN_METERS + "_BillsCents");
        tito_session_meters.cash_in_total_in_cents = tito_session_meters.cash_in_coins_in_cents + tito_session_meters.cash_in_bills_in_cents;
      }

      // FT Meters
      while (Reader.Name != FT_METERS && !Reader.EOF)
      {
        Reader.Read();
      }

      if (Reader.Name == FT_METERS)
      {
        ft_meters.to_gm_cashable_cents = Math.Max(XML.GetAsInt64(Reader, FT_METERS + "_ToGMCashableCents"), 0);
        ft_meters.to_gm_restricted_cents = Math.Max(XML.GetAsInt64(Reader, FT_METERS + "_ToGMRestrictedCents"), 0);
        ft_meters.to_gm_non_restricted_cents = Math.Max(XML.GetAsInt64(Reader, FT_METERS + "_ToGMNonRestrictedCents"), 0);
        ft_meters.from_gm_cashable_cents = Math.Max(XML.GetAsInt64(Reader, FT_METERS + "_FromGMCashableCents"), 0);
        ft_meters.from_gm_restricted_cents = Math.Max(XML.GetAsInt64(Reader, FT_METERS + "_FromGMRestrictedCents"), 0);
        ft_meters.from_gm_non_restricted_cents = Math.Max(XML.GetAsInt64(Reader, FT_METERS + "_FromGMNonRestrictedCents"), 0);
      }

      mico2_account_session = XML.GetValue(Reader, "Mico2AccountSession", true) == "1";

      System.Diagnostics.Debug.WriteLine(String.Format("mico2_account_session: [{0}]", mico2_account_session));

      if (mico2_account_session)
      {
        to_account_on_close.Redeemable = (Decimal)Math.Max(XML.GetAsInt64(Reader, TO_ACCOUNT_ON_CLOSE + "_CashableCents"), 0) / 100;
        to_account_on_close.PromoNotRedeemable = (Decimal)Math.Max(XML.GetAsInt64(Reader, TO_ACCOUNT_ON_CLOSE + "_RestrictedCents"), 0) / 100;
        to_account_on_close.PromoRedeemable = (Decimal)Math.Max(XML.GetAsInt64(Reader, TO_ACCOUNT_ON_CLOSE + "_NonRestrictedCents"), 0) / 100;
      }

      // Found on EGM
      while (Reader.Name != FOUND_ON_EGM && !Reader.EOF)
      {
        Reader.Read();
      }
      Reader.Read();
      found_on_egm.Redeemable = (Decimal)Math.Max(XML_GetTagAsInt64(Reader, FOUND_ON_EGM, "CashableCents"), 0) / 100;
      found_on_egm.PromoNotRedeemable = (Decimal)Math.Max(XML_GetTagAsInt64(Reader, FOUND_ON_EGM, "RestrictedCents"), 0) / 100;
      found_on_egm.PromoRedeemable = (Decimal)Math.Max(XML_GetTagAsInt64(Reader, FOUND_ON_EGM, "NonRestrictedCents"), 0) / 100;

      // Remaining on EGM
      while (Reader.Name != REMAINING_ON_EGM && !Reader.EOF)
      {
        Reader.Read();
      }
      Reader.Read();
      remaining_on_egm.Redeemable = (Decimal)Math.Max(XML_GetTagAsInt64(Reader, REMAINING_ON_EGM, "CashableCents"), 0) / 100;
      remaining_on_egm.PromoNotRedeemable = (Decimal)Math.Max(XML_GetTagAsInt64(Reader, REMAINING_ON_EGM, "RestrictedCents"), 0) / 100;
      remaining_on_egm.PromoRedeemable = (Decimal)Math.Max(XML_GetTagAsInt64(Reader, REMAINING_ON_EGM, "NonRestrictedCents"), 0) / 100;

      Reader.Close();
    } // LoadXml
  }

  /// <summary>
  /// Class that will control msg creation for terminal play-session anouncement reply
  /// </summary>
  public class WCP_MsgPlaySessionMetersReply : IXml
  {
    protected const String MESSAGE_ENVELOPE = "WCP_MsgPlaySessionMetersReply";

    public Int64 EstimatedPointsCents;

    //------------------------------------------------------------------------------
    // PURPOSE : Convert internal properties to string in XML format
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String in XML format
    //
    public String ToXml()
    {
      XmlDocument _xml;
      XmlNode _node;

      _xml = new XmlDocument();

      _node = _xml.CreateNode(XmlNodeType.Element, MESSAGE_ENVELOPE, "");
      _xml.AppendChild(_node);

      XML.AppendChild(_node, "EstimatedPointsCents", EstimatedPointsCents.ToString());

      return _xml.OuterXml;

    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse input string, in XML format, and fill internal properties
    //           Bad structure raises WCP_Exception
    //
    //  PARAMS :
    //      - INPUT :
    //        - Reader: loaded XML structure
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      EstimatedPointsCents = 0;
      if (!Reader.Name.Equals(MESSAGE_ENVELOPE))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag " + MESSAGE_ENVELOPE + " not found in content area");
      }
      else
      {
        if (!Int64.TryParse(XML.GetValue(Reader, "EstimatedPointsCents", true), out EstimatedPointsCents))
        {
          return;
        }
      }

    } // LoadXml
  }
}
