//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMachineMeters.cs
// 
//   DESCRIPTION: WCP_MsgMachineMeters class
// 
// 
// CREATION DATE: 16-NOV-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-NOV-2011 ACC    First release.
// 15-JUN-2012 ACC    Add Jackpot Cents into Machine Meters
// 07-AUG-2014 ACC    Add Progressive Jackpot Cents into Meters
// 29-MAR-2018 ACC    Add AcountingDenomCents
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgReportMachineMeters : IXml
  {
    // PLAYED WON METERS
    public Boolean PlayWonAvalaible = false;

    public Int64 PlayedCents = 0;
    public Int64 PlayedMaxValueCents = 0;
    public Int64 PlayedQuantity = 0;
    public Int64 PlayedMaxValueQuantity = 0;

    public Int64 WonCents = 0;
    public Int64 WonMaxValueCents = 0;
    public Int64 WonQuantity = 0;
    public Int64 WonMaxValueQuantity = 0;

    // ACC 15-JUN-2012 Add Jackpot Cents into Machine Meters
    public Int64 JackpotCents = 0;
    public Int64 JackpotMaxValueCents = 0;

    // ACC 04-AUG-2014 Add Jackpot Cents into Machine Meters
    public Int64 ProgressiveJackpotCents = 0;
    public Int64 ProgressiveJackpotMaxValueCents = 0;

    // FUND TRANSFER METERS
    public WCP_FundTransferType FundTransferType;

    public Int64 ToGmCents = 0;
    public Int64 ToGmMaxValueCents = 0;
    public Int64 ToGmQuantity = 0;
    public Int64 ToGmMaxValueQuantity = 0;

    public Int64 FromGmCents = 0;
    public Int64 FromGmMaxValueCents = 0;
    public Int64 FromGmQuantity = 0;
    public Int64 FromGmMaxValueQuantity = 0;

    public Boolean IgnoreDeltas = false;

    public Int64 AccountingDenomCents = 0;

    #region IXml Members

    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_MsgReportMachineMeters></WCP_MsgReportMachineMeters>");
      node = xml.SelectSingleNode("WCP_MsgReportMachineMeters");

      if (PlayWonAvalaible)
      {
        XML.AppendChild(node, "PlayWonAvalaible", WCP_PlayWonAvailable.PLAY_WON_AVAILABLE.ToString());
      }
      else
      {
        XML.AppendChild(node, "PlayWonAvalaible", WCP_PlayWonAvailable.PLAY_WON_NOT_AVAILABLE.ToString());
      }

      XML.AppendChild(node, "PlayedCents", PlayedCents.ToString());
      XML.AppendChild(node, "PlayedMaxValueCents", PlayedMaxValueCents.ToString());
      XML.AppendChild(node, "PlayedQuantity", PlayedQuantity.ToString());
      XML.AppendChild(node, "PlayedMaxValueQuantity", PlayedMaxValueQuantity.ToString());

      XML.AppendChild(node, "WonCents", WonCents.ToString());
      XML.AppendChild(node, "WonMaxValueCents", WonMaxValueCents.ToString());
      XML.AppendChild(node, "WonQuantity", WonQuantity.ToString());
      XML.AppendChild(node, "WonMaxValueQuantity", WonMaxValueQuantity.ToString());

      XML.AppendChild(node, "JackpotCents", JackpotCents.ToString());
      XML.AppendChild(node, "JackpotMaxValueCents", JackpotMaxValueCents.ToString());

      XML.AppendChild(node, "ProgressiveJackpotCents", ProgressiveJackpotCents.ToString());
      XML.AppendChild(node, "ProgressiveJackpotMaxValueCents", ProgressiveJackpotMaxValueCents.ToString());

      XML.AppendChild(node, "FundTransferType", FundTransferType.ToString());

      XML.AppendChild(node, "ToGmCents", ToGmCents.ToString());
      XML.AppendChild(node, "ToGmMaxValueCents", ToGmMaxValueCents.ToString());
      XML.AppendChild(node, "ToGmQuantity", ToGmQuantity.ToString());
      XML.AppendChild(node, "ToGmMaxValueQuantity", ToGmMaxValueQuantity.ToString());

      XML.AppendChild(node, "FromGmCents", FromGmCents.ToString());
      XML.AppendChild(node, "FromGmMaxValueCents", FromGmMaxValueCents.ToString());
      XML.AppendChild(node, "FromGmQuantity", FromGmQuantity.ToString());
      XML.AppendChild(node, "FromGmMaxValueQuantity", FromGmMaxValueQuantity.ToString());

      XML.AppendChild(node, "IgnoreDeltas", IgnoreDeltas ? "1" : "0");

      XML.AppendChild(node, "AccountingDenomCents", AccountingDenomCents.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgReportMachineMeters")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportMachineMeters not found in content area");
      }
      else
      {
        WCP_PlayWonAvailable _play_won_avalaible;
        String _aux_str;

        _aux_str = XML.GetValue(Reader, "PlayWonAvalaible");
        _play_won_avalaible = (WCP_PlayWonAvailable)Enum.Parse(typeof(WCP_PlayWonAvailable), _aux_str, true);

        if (_play_won_avalaible == WCP_PlayWonAvailable.PLAY_WON_AVAILABLE)
        {
          PlayWonAvalaible = true;
        }

        PlayedCents = Int64.Parse(XML.GetValue(Reader, "PlayedCents"));
        PlayedMaxValueCents = Int64.Parse(XML.GetValue(Reader, "PlayedMaxValueCents"));
        PlayedQuantity = Int64.Parse(XML.GetValue(Reader, "PlayedQuantity"));
        PlayedMaxValueQuantity = Int64.Parse(XML.GetValue(Reader, "PlayedMaxValueQuantity"));

        WonCents = Int64.Parse(XML.GetValue(Reader, "WonCents"));
        WonMaxValueCents = Int64.Parse(XML.GetValue(Reader, "WonMaxValueCents"));
        WonQuantity = Int64.Parse(XML.GetValue(Reader, "WonQuantity"));
        WonMaxValueQuantity = Int64.Parse(XML.GetValue(Reader, "WonMaxValueQuantity"));

        // ACC 15-JUN-2012 Add Jackpot Cents into Machine Meters. 
        //                 Check if exist this field into message.

        Reader.Read();  // Read until End Element 'WonMaxValueQuantity'
        Reader.Read();  // Read until Next Element: 'FundTransferType' OR 'JackpotCents'

        if (Reader.Name == "JackpotCents" && Reader.NodeType.Equals(XmlNodeType.Element))
        {
          JackpotCents = Int64.Parse(XML.GetValue(Reader, "JackpotCents"));
          JackpotMaxValueCents = Int64.Parse(XML.GetValue(Reader, "JackpotMaxValueCents"));

          Reader.Read();  // Read until End Element 'JackpotMaxValueCents'
          Reader.Read();  // Read until Next Element: 'FundTransferType' OR 'ProgressiveJackpotCents'
        }
        else
        {
          JackpotCents = -1;
          JackpotMaxValueCents = -1;
        }

        // ACC 15-JUN-2012 Add Jackpot Progressive Cents into Machine Meters. 
        //                 Check if exist this field into message.

        if (Reader.Name == "ProgressiveJackpotCents" && Reader.NodeType.Equals(XmlNodeType.Element))
        {
          ProgressiveJackpotCents = Int64.Parse(XML.GetValue(Reader, "ProgressiveJackpotCents"));
          ProgressiveJackpotMaxValueCents = Int64.Parse(XML.GetValue(Reader, "ProgressiveJackpotMaxValueCents"));
        }
        else
        {
          ProgressiveJackpotCents = -1;
          ProgressiveJackpotMaxValueCents = -1;
        }

        _aux_str = XML.GetValue(Reader, "FundTransferType");
        FundTransferType = (WCP_FundTransferType)Enum.Parse(typeof(WCP_FundTransferType), _aux_str, true);

        ToGmCents = Int64.Parse(XML.GetValue(Reader, "ToGmCents"));
        ToGmMaxValueCents = Int64.Parse(XML.GetValue(Reader, "ToGmMaxValueCents"));
        ToGmQuantity = Int64.Parse(XML.GetValue(Reader, "ToGmQuantity"));
        ToGmMaxValueQuantity = Int64.Parse(XML.GetValue(Reader, "ToGmMaxValueQuantity"));

        FromGmCents = Int64.Parse(XML.GetValue(Reader, "FromGmCents"));
        FromGmMaxValueCents = Int64.Parse(XML.GetValue(Reader, "FromGmMaxValueCents"));
        FromGmQuantity = Int64.Parse(XML.GetValue(Reader, "FromGmQuantity"));
        FromGmMaxValueQuantity = Int64.Parse(XML.GetValue(Reader, "FromGmMaxValueQuantity"));

        IgnoreDeltas = XML.GetValue(Reader, "IgnoreDeltas", true) == "1";
        String _value;
        _value = XML.GetValue(Reader, "AccountingDenomCents", true);
        if (!Int64.TryParse(_value, out AccountingDenomCents))
        {
          AccountingDenomCents = 0;
        }

        Reader.Close();
      }
    }

    #endregion
  }

  public class WCP_MsgReportMachineMetersReply : IXml
  {
    #region IXml Members
    public string ToXml()
    {
      return "<WCP_MsgReportMachineMetersReply></WCP_MsgReportMachineMetersReply>";
    }

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      if (Xml.Name != "WCP_MsgReportMachineMetersReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportMachineMetersReply not found in content area");
      }
    }

    #endregion
  }
}
