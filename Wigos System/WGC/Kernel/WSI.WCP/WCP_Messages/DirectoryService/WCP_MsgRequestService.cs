using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Net;
using WSI.Common;


namespace WSI.WCP
{
  public class WCP_MsgRequestService:IXml
  {
    public  String     ServiceName;
    public  IPEndPoint ReplyToUDPAddress;


    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "WCP_MsgRequestService", "");
      xml.AppendChild (node);

      XML.AppendChild (node, "ServiceName", ServiceName);
      XML.AppendChild (node, "ReplyToUDPAddress", ReplyToUDPAddress.ToString ());

      return xml.OuterXml;
    }


    public void LoadXml (XmlReader reader)
    {
      String reply_to_str;
      int idx;
      IPAddress ip;
      int port;
      if (reader.Name != "WCP_MsgRequestService")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgRequestService not found in content area");
      }
      else
      {
        ServiceName = XML.GetValue(reader, "ServiceName");
        reply_to_str = XML.GetValue(reader, "ReplyToUDPAddress");
        idx = reply_to_str.IndexOf(':');
        ip = IPAddress.Parse(reply_to_str.Substring(0, idx));
        port = int.Parse(reply_to_str.Substring(1 + idx));
        ReplyToUDPAddress = new IPEndPoint(ip, port);
      }
    }
  }

  public class WCP_MsgRequestServiceReply:IXml
  {
    public IPEndPoint ServiceAddress;

    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "WCP_MsgRequestServiceReply", "");
      xml.AppendChild (node);

      if ( ServiceAddress != null )
      {
        XML.AppendChild (node, "ServiceAddress", ServiceAddress.ToString ());
      }
      else
      {
        XML.AppendChild (node, "ServiceAddress", "");
      }

      return xml.OuterXml;
    }


    public void LoadXml(XmlReader reader)
    {
      String ipe_str;
      int idx;
      IPAddress ip;
      int port;
      
      if (reader.Name != "WCP_MsgRequestServiceReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgRequestServiceReply not found in content area");
      }
      else
      {
        ipe_str = XML.GetValue(reader, "ServiceAddress");
        idx = ipe_str.IndexOf(':');
        ip = IPAddress.Parse(ipe_str.Substring(0, idx));
        port = int.Parse(ipe_str.Substring(1 + idx));
        ServiceAddress = new IPEndPoint(ip, port);
      }
    }
  }
}
