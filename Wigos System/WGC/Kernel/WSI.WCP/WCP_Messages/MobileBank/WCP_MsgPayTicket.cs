//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgPayTicket.cs
// 
//   DESCRIPTION: WCP_MsgPayTicket class
// 
//        AUTHOR: Jordi C�rdoba
// 
// CREATION DATE: 21-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2014 JCOR   First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;
using WSI.WCP;

namespace WSI.WCP
{
  public class WCP_MsgPayTicket : IXml
  {
    public Int64 TransactionId;
    public Int32 SystemId;
    public Int64 ValidationNumberSequence;
    public Int64 TicketId;
    public Int64 AmountToPayCents;
    public ArrayList MobileBankCard;
    public String Pin;


    public WCP_MsgPayTicket()
    {
      MobileBankCard = new ArrayList();
    }

    public void MobileBankSetCardData(int CardIndex, String CardData)
    {
      while (MobileBankCard.Count <= CardIndex)
      {
        MobileBankCard.Add("");
      }
      MobileBankCard[CardIndex] = CardData;
    }

    public String MobileBankGetCardData(int TrackIndex)
    {
      if (MobileBankCard.Count <= TrackIndex)
      {
        return "";
      }

      return MobileBankCard[TrackIndex].ToString();
    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgPayTicket>");

      _sb.AppendLine("<SystemId>" + SystemId.ToString() + "</SystemId>");
      _sb.AppendLine("<ValidationNumberSequence>" + ValidationNumberSequence.ToString() + "</ValidationNumberSequence>");
      _sb.AppendLine("<TransactionId>" + TransactionId.ToString() + "</TransactionId>");
      _sb.AppendLine("<TicketId>" + TicketId.ToString() + "</TicketId>");
      _sb.AppendLine("<AmountToPayCents>" + AmountToPayCents.ToString() + "</AmountToPayCents>");

      _sb.AppendLine("<MobileBankCard>");
      for (int _card_idx = 0; _card_idx < MobileBankCard.Count; _card_idx++)
      {
        _sb.AppendLine("<CardIndex>" + _card_idx.ToString() + "</CardIndex>");
        _sb.AppendLine("<CardData>" + MobileBankCard[_card_idx].ToString() + "</CardData>");
      }
      _sb.AppendLine("</MobileBankCard>");

      _sb.AppendLine("<Pin>" + Pin + "</Pin>");

      _sb.AppendLine("</WCP_MsgPayTicket>");

      return _sb.ToString();     
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgPayTicket")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgPayTicket not found in content area");
      }

      SystemId = Int32.Parse(XML.GetValue(Reader, "SystemId"));
      ValidationNumberSequence = Int64.Parse(XML.GetValue(Reader, "ValidationNumberSequence"));
      TransactionId = Int64.Parse(XML.GetValue(Reader, "TransactionId"));
      TicketId = Int64.Parse(XML.GetValue(Reader, "TicketId"));
      AmountToPayCents = Int64.Parse(XML.GetValue(Reader, "AmountToPayCents"));


      Boolean end_card;
      end_card = false;

      while (Reader.Name != "MobileBankCard" && (Reader.NodeType.Equals(XmlNodeType.Element)))
      {
        if (!Reader.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgPayTicket MobileBankCard element missing");
        }
      }

      while (!end_card)
      {
        if (!Reader.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                  "Tag WCP_MsgPayTicket/MobileBankCard Track element missing");
        }

        MobileBankSetCardData(Int32.Parse(XML.GetValue(Reader, "CardIndex")), XML.GetValue(Reader, "CardData"));

        Reader.Read();
        Reader.Read();
        Reader.Read();

        if ((Reader.Name == "MobileBankCard") && (Reader.NodeType.Equals(XmlNodeType.EndElement)))
        {
          end_card = true;
        }
      }
      Pin = XML.GetValue(Reader, "Pin");     
    }
  }

  public class WCP_MsgPayTicketReply : IXml
  {
    public Int64 PaidAmountCents;

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgPayTicketReply>");
      _sb.AppendLine("<PaidAmountCents>" + PaidAmountCents.ToString() + "</PaidAmountCents>");
      _sb.AppendLine("</WCP_MsgPayTicketReply>");

      return _sb.ToString();
    } // ToXml

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgPayTicketReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgPayTicketReply not found in content area");
      }

      PaidAmountCents = Int64.Parse(XML.GetValue(Reader, "PaidAmountCents"));

    } // LoadXml
  }
}
