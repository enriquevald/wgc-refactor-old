//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems International Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMobileBankCardTransfer.cs
// 
//   DESCRIPTION: WCP_MsgMobileBankCardTransfer class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 27-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-MAY-2009 RRT    First release.
//------------------------------------------------------------------------------

  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Text;
  using System.Xml;
  using WSI.Common;

  namespace WSI.WCP
  {
    public class WCP_MsgMobileBankCardTransfer : IXml
    {
      public Int64 TransactionId;      
      ArrayList MobileBankCard;
      public String Pin;
      ArrayList PlayerCard;
      public Int64 AmountToTransferx100;

      public WCP_MsgMobileBankCardTransfer()
      {
        MobileBankCard = new ArrayList();
        PlayerCard = new ArrayList();
      }

      public void PlayerSetTrackData (int TrackIndex, String TrackData)
      {
        while (PlayerCard.Count <= TrackIndex)
        {
          PlayerCard.Add("");
        }
        PlayerCard[TrackIndex] = TrackData;
      }

      public String PlayerGetTrackData(int TrackIndex)
      {
        if (PlayerCard.Count <= TrackIndex)
        {
          return "";
        }

        return PlayerCard[TrackIndex].ToString();
      }

      public void MobileBankSetTrackData(int TrackIndex, String TrackData)
      {
        while (MobileBankCard.Count <= TrackIndex)
        {
          MobileBankCard.Add("");
        }
        MobileBankCard[TrackIndex] = TrackData;
      }

      public String MobileBankGetTrackData(int TrackIndex)
      {
        if (MobileBankCard.Count <= TrackIndex)
        {
          return "";
        }

        return MobileBankCard[TrackIndex].ToString();
      }

      public void MBSetPin(String Value)
      {
        Pin = Value;
      }

      public void SetAmountToTransfer(Int64 Value)
      {
        AmountToTransferx100 = Value;
      }

      public String ToXml()
      {
        XmlDocument xml;
        XmlNode node;
        XmlNode node_card;
        XmlNode track;

        xml = new XmlDocument();
        xml.LoadXml("<WCP_MsgMobileBankCardTransfer></WCP_MsgMobileBankCardTransfer>");

        // Mobile Bank Track Data
        node = xml.SelectSingleNode("WCP_MsgMobileBankCardTransfer");

        XML.AppendChild(node, "TransactionId", TransactionId.ToString());

        node_card = xml.CreateNode(XmlNodeType.Element, "MobileBankCard", "");
        for (int track_idx = 0; track_idx < MobileBankCard.Count; track_idx++)
        {
          track = xml.CreateNode(XmlNodeType.Element, "Track", "");

          node_card.AppendChild(track);

          XML.AppendChild(track, "TrackIndex", track_idx.ToString());
          XML.AppendChild(track, "TrackData", MobileBankCard[track_idx].ToString());
        }
        node.AppendChild(node_card);

        XML.AppendChild(node, "Pin", Pin);

        // Player Track Data
        node_card = xml.CreateNode(XmlNodeType.Element, "PlayerCard", "");
        for (int track_idx = 0; track_idx < PlayerCard.Count; track_idx++)
        {
          track = xml.CreateNode(XmlNodeType.Element, "Track", "");

          node_card.AppendChild(track);

          XML.AppendChild(track, "TrackIndex", track_idx.ToString());
          XML.AppendChild(track, "TrackData", PlayerCard[track_idx].ToString());
        }
        node.AppendChild(node_card);

        XML.AppendChild(node, "AmountToTransferx100", AmountToTransferx100.ToString());

        return xml.OuterXml;
      }

      public void LoadXml(XmlReader reader)
      {
        Boolean end_card;

        if (reader.Name != "WCP_MsgMobileBankCardTransfer")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer not found in content area");
        }
        else
        {
          //
          // Read TransactionId
          //
          TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));

          //
          // Read MobileBankCard
          //
          end_card = false;
          while (reader.Name != "MobileBankCard" && (reader.NodeType.Equals(XmlNodeType.Element)))
          {
            if (!reader.Read())
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer MobileBankCard element missing");
            }
          }
          //Now, the xmlreader is at the start of the MobileBankCard tag.
          //and it must advance until the end MobileBankCard tag.
          //the new GetValue method moves the XmlReader until the end of the searched tag
          // so:
          while (!end_card)
          {
            if (!reader.Read())
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer/MobileBankCard Track element missing");
            }
            else
            {
              int track_index;
              String track_data;

              track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
              track_data = XML.GetValue(reader, "TrackData");
              MobileBankSetTrackData(track_index, track_data);
              //advance until </TrackData> element
              reader.Read();
              //advance until </Track> element 
              reader.Read();
              if ((reader.Name == "Track") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                reader.Read();
                if ((reader.Name == "MobileBankCard") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
                {
                  end_card = true;
                }
              }
            }
          }

          //
          // Read Pin
          //
          Pin = XML.GetValue(reader, "Pin");

          //
          // Read PlayerCard
          //
          end_card = false;
          while (reader.Name != "PlayerCard" && (reader.NodeType.Equals(XmlNodeType.Element)))
          {
            if (!reader.Read())
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer PlayerCard element missing");
            }
          }
          //Now, the xmlreader is at the start of the PlayerCard tag.
          //and it must advance until the end PlayerCard tag.
          //the new GetValue method moves the XmlReader until the end of the searched tag
          // so:
          while (!end_card)
          {
            if (!reader.Read())
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer/PlayerCard Track element missing");
            }
            else
            {
              int track_index;
              String track_data;

              track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
              track_data = XML.GetValue(reader, "TrackData");
              PlayerSetTrackData(track_index, track_data);
              //advance until </TrackData> element
              reader.Read();
              //advance until </Track> element 
              reader.Read();
              if ((reader.Name == "Track") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                reader.Read();
                if ((reader.Name == "PlayerCard") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
                {
                  end_card = true;
                }
              }
            }
          }

          //
          // Read AmountToTransferx100
          //
          AmountToTransferx100 = Int64.Parse(XML.GetValue(reader, "AmountToTransferx100"));
        }
      }
    }

    public class WCP_MsgMobileBankCardTransferReply : IXml
    {
      public Int64 PlayerPreviousCardBalancex100;
      public Int64 PlayerCurrentCardBalancex100;

      public String ToXml()
      {
        XmlDocument xml;
        XmlNode node;

        xml = new XmlDocument();

        node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgMobileBankCardTransferReply", "");
        xml.AppendChild(node);

        XML.AppendChild(node, "PlayerPreviousCardBalancex100", PlayerPreviousCardBalancex100.ToString());
        XML.AppendChild(node, "PlayerCurrentCardBalancex100", PlayerCurrentCardBalancex100.ToString());

        return xml.OuterXml;
      }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "WCP_MsgMobileBankCardTransferReply")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransferReply not found in content area");
        }
        else
        {
          PlayerPreviousCardBalancex100 = Int64.Parse(XML.GetValue(reader, "PlayerPreviousCardBalancex100"));
          PlayerCurrentCardBalancex100 = Int64.Parse(XML.GetValue(reader, "PlayerCurrentCardBalancex100"));
        }
      }
    }
  }
