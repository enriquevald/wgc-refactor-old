//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems International Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMobileBankCardCheck.cs
// 
//   DESCRIPTION: WCP_MsgMobileBankCardCheck class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 27-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-MAY-2009 RRT    First release.
//------------------------------------------------------------------------------

  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Text;
  using System.Xml;
  using WSI.Common;

  namespace WSI.WCP
  {
    public class WCP_MsgMobileBankCardCheck : IXml
    {
      public String Pin;
      ArrayList MobileBankTrack;

      public WCP_MsgMobileBankCardCheck()
      {
        MobileBankTrack = new ArrayList();
      }

      public void MBSetPin(String Value)
      {
        Pin = Value;
      }

      public void MobileBankSetTrackData(int TrackIndex, String TrackData)
      {
        while (MobileBankTrack.Count <= TrackIndex)
        {
          MobileBankTrack.Add("");
        }
        MobileBankTrack[TrackIndex] = TrackData;
      }

      public String MobileBankGetTrackData(int TrackIndex)
      {
        if (MobileBankTrack.Count <= TrackIndex)
        {
          return "";
        }

        return MobileBankTrack[TrackIndex].ToString();
      }

      public String ToXml()
      {
        XmlDocument xml;
        XmlNode node;
        XmlNode node_card;
        XmlNode track;

        xml = new XmlDocument();
        xml.LoadXml("<WCP_MsgMobileBankCardCheck></WCP_MsgMobileBankCardCheck>");

        // Mobile Bank Track Data
        node = xml.SelectSingleNode("WCP_MsgMobileBankCardCheck");

        node_card = xml.CreateNode(XmlNodeType.Element, "MobileBankCard", "");
        for (int track_idx = 0; track_idx < MobileBankTrack.Count; track_idx++)
        {
          track = xml.CreateNode(XmlNodeType.Element, "Track", "");

          node_card.AppendChild(track);

          XML.AppendChild(track, "TrackIndex", track_idx.ToString());
          XML.AppendChild(track, "TrackData", MobileBankTrack[track_idx].ToString());
        }
        node.AppendChild(node_card);

        XML.AppendChild(node, "Pin", Pin);

        return xml.OuterXml;
      }

      public void LoadXml(XmlReader reader)
      {
        Boolean end_card;

        if (reader.Name != "WCP_MsgMobileBankCardCheck")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardCheck not found in content area");
        }
        else
        {
          //
          // Read MobileBankCard
          //
          end_card = false;
          while (reader.Name != "MobileBankCard" && (reader.NodeType.Equals(XmlNodeType.Element)))
          {
            if (!reader.Read())
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardCheck MobileBankCard element missing");
            }
          }
          //Now, the xmlreader is at the start of the MobileBankCard tag.
          //and it must advance until the end MobileBankCard tag.
          //the new GetValue method moves the XmlReader until the end of the searched tag
          // so:
          while (!end_card)
          {
            if (!reader.Read())
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardCheck/MobileBankCard Track element missing");
            }
            else
            {
              int track_index;
              String track_data;

              track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
              track_data = XML.GetValue(reader, "TrackData");
              MobileBankSetTrackData(track_index, track_data);
              //advance until </TrackData> element
              reader.Read();
              //advance until </Track> element 
              reader.Read();
              if ((reader.Name == "Track") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                reader.Read();
                if ((reader.Name == "MobileBankCard") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
                {
                  end_card = true;
                }
              }
            }
          }

          //
          // Read Pin
          //
          Pin = XML.GetValue(reader, "Pin");
        }
      }
    }

    public class WCP_MsgMobileBankCardCheckReply : IXml
    {
      public Int64 MobileBankAuthorizedAmountx100;

      public String ToXml()
      {
        XmlDocument xml;
        XmlNode node;

        xml = new XmlDocument();

        node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgMobileBankCardCheckReply", "");
        xml.AppendChild(node);

        XML.AppendChild(node, "MobileBankAuthorizedAmountx100", MobileBankAuthorizedAmountx100.ToString());

        return xml.OuterXml;
      }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "WCP_MsgMobileBankCardCheckReply")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardCheckReply not found in content area");
        }
        else
        {
          MobileBankAuthorizedAmountx100 = Int64.Parse(XML.GetValue(reader, "MobileBankAuthorizedAmountx100"));
        }
      }
    }
  }
