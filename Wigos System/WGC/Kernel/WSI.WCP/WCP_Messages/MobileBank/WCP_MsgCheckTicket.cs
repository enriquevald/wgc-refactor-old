//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgCheckTicket.cs
// 
//   DESCRIPTION: WCP_MsgCheckTicket class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 19-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2014 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;
using WSI.WCP;

namespace WSI.WCP
{
  public class WCP_MsgCheckTicket : IXml
  {
    public Int32 SystemId;
    public Int64 ValidationNumberSequence;

    public WCP_MsgCheckTicket()
    {

    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgCheckTicket>");

      _sb.AppendLine("<SystemId>" + SystemId.ToString() + "</SystemId>");
      _sb.AppendLine("<ValidationNumberSequence>" + ValidationNumberSequence.ToString() + "</ValidationNumberSequence>");

      _sb.AppendLine("</WCP_MsgCheckTicket>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgCheckTicket")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCheckTicket not found in content area");
      }
      else
      {
        SystemId = Int32.Parse(XML.GetValue(Reader, "SystemId"));
        ValidationNumberSequence = Int64.Parse(XML.GetValue(Reader, "ValidationNumberSequence"));

        Reader.Close();
      }
    }
  }

  public class WCP_MsgCheckTicketReply : IXml
  {
    public Int32 SystemId;
    public Int64 ValidationNumberSequence;
    public Int64 AmountCents;
    public DateTime ExpirationDate = WGDB.Now;
    public Int32 ValidateStatus;
    public Int64 TicketId = 0;
  
    public WCP_MsgCheckTicketReply()
    {
      ExpirationDate = new DateTime();
    }

    public Int64 GetExpirationDatetime()
    {
      return ExpirationDate.ToFileTime(); 

    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgCheckTicketReply>");
      _sb.AppendLine("<SystemId>" + SystemId.ToString() + "</SystemId>");
      _sb.AppendLine("<ValidationNumberSequence>" + ValidationNumberSequence.ToString() + "</ValidationNumberSequence>");
      _sb.AppendLine("<AmountCents>" + AmountCents.ToString() + "</AmountCents>");
      _sb.AppendLine("<ExpirationDate>" + XML.XmlDateString(ExpirationDate) + "</ExpirationDate>");
      _sb.AppendLine("<ValidateStatus>" + ValidateStatus.ToString() + "</ValidateStatus>");
      _sb.AppendLine("<TicketId>" + TicketId.ToString() + "</TicketId>");
      _sb.AppendLine("</WCP_MsgCheckTicketReply>");

      return _sb.ToString();
    } // ToXml

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgCheckTicketReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCheckTicketReply not found in content area");
      }

      SystemId = Int32.Parse(XML.GetValue(Reader, "SystemId"));
      ValidationNumberSequence = Int64.Parse(XML.GetValue(Reader, "ValidationNumberSequence"));
      AmountCents = Int64.Parse(XML.GetValue(Reader, "AmountCents"));
      ExpirationDate = XML.GetAsDate(Reader, "ExpirationDate");
      ValidateStatus = Int32.Parse(XML.GetValue(Reader, "ValidateStatus"));
      Int64.TryParse(XML.ReadTagValue(Reader, "TicketId", true), out TicketId);
   
    } // LoadXml
  }
}
