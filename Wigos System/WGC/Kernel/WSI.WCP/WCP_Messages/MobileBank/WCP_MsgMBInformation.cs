//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMBInformation.cs
// 
//   DESCRIPTION: WCP_MsgMBInformation class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 14-SEP-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-SEP-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgMBInformation : IXml
  {
    ArrayList Track;

    public WCP_MsgMBInformation()
    {
      Track = new ArrayList();
    }

    public void SetTrackData(int TrackIndex, String TrackData)
    {
      while (Track.Count <= TrackIndex)
      {
        Track.Add("");
      }
      Track[TrackIndex] = TrackData;
    }

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgMBInformation>");
      _sb.AppendLine("<Card>");

      for (int track_idx = 0; track_idx < Track.Count; track_idx++)
      {
        _sb.AppendLine("<TrackIndex>" + track_idx.ToString() + "</TrackIndex>");
        _sb.AppendLine("<TrackData>" + Track[track_idx].ToString() + "</TrackData>");
      }

      _sb.AppendLine("</Card>");
      _sb.AppendLine("</WCP_MsgMBInformation>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader XmlReader)
    {
      Boolean _end_card;

      if (XmlReader.Name != "WCP_MsgMBInformation")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMBInformation not found in content area");
      }
      else
      {
        _end_card = false;
        while (XmlReader.Name != "Card" && (XmlReader.NodeType.Equals(XmlNodeType.Element)))
        {
          if (!XmlReader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMBInformation Card element missing");
          }
        }
        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!_end_card)
        {
          if (!XmlReader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMBInformation Track element missing");
          }
          else
          {
            int track_index;
            String track_data;

            track_index = int.Parse(XML.GetValue(XmlReader, "TrackIndex"));
            track_data = XML.GetValue(XmlReader, "TrackData");
            SetTrackData(track_index, track_data);
            //advance until </track_data> element
            XmlReader.Read();
            //advance until </track> element 
            XmlReader.Read();
            if ((XmlReader.Name == "Track") && (XmlReader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              XmlReader.Read();
              if ((XmlReader.Name == "Card") && (XmlReader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                _end_card = true;
              }
            }
          }
        } // while
      }
    }
  }

  public class WCP_TransferHistoryItem
  {
    public DateTime Datetime;
    public Int64 Amountx100;
    public String TerminalName;
    public String PlayerName;

    public WCP_TransferHistoryItem()
    {
      Datetime = DateTime.Now;
      Amountx100 = 0;
      TerminalName = "";
      PlayerName = "";
    }

  } // WCP_TransferHistoryItem

  public class WCP_MsgMBInformationReply : IXml
  {
    public Int64      MbAccount;
    public String     MbName;
    public Int64      MbLimitx100;

    public ArrayList  TransferHistoryItems = new ArrayList();  // WCP_TransferHistoryItem

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgMBInformationReply>");

      _sb.AppendLine("<MbAccount>" + MbAccount.ToString() + "</MbAccount>");
      _sb.AppendLine("<MbName>" + XML.StringToXmlValue(MbName) + "</MbName>");
      _sb.AppendLine("<MbLimitx100>" + MbLimitx100.ToString() + "</MbLimitx100>");

      _sb.AppendLine("<HistoryTransfers>");

      for (int _idx = 0; _idx < TransferHistoryItems.Count; _idx++)
      {
        WCP_TransferHistoryItem p_transfer_item;

        p_transfer_item = (WCP_TransferHistoryItem)TransferHistoryItems[_idx];

        _sb.AppendLine("<TransferItem>");

        _sb.AppendLine("<Date>" + XML.XmlDateTimeString(p_transfer_item.Datetime) + "</Date>");
        _sb.AppendLine("<Amountx100>" + p_transfer_item.Amountx100.ToString() + "</Amountx100>");
        _sb.AppendLine("<TerminalName>" + XML.StringToXmlValue(p_transfer_item.TerminalName) + "</TerminalName>");
        _sb.AppendLine("<PlayerName>" + XML.StringToXmlValue(p_transfer_item.PlayerName) + "</PlayerName>");

        _sb.AppendLine("</TransferItem>");
      }

      _sb.AppendLine("</HistoryTransfers>");
      _sb.AppendLine("</WCP_MsgMBInformationReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader XmlReader)
    {
      Boolean _end_transfer_list;

      if (XmlReader.Name != "WCP_MsgMBInformationReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMBInformationReply not found in content area");
      }
      else
      {
        MbAccount = Int64.Parse(XML.GetValue(XmlReader, "MbAccount"));
        MbName = XML.GetValue(XmlReader, "MbName");
        MbLimitx100 = Int64.Parse(XML.GetValue(XmlReader, "MbLimitx100"));

        _end_transfer_list = false;

        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!_end_transfer_list)
        {
          if (!XmlReader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMBInformation HistoryTransfers element missing");
          }
          else
          {
            WCP_TransferHistoryItem p_transfer_item;

            p_transfer_item = new WCP_TransferHistoryItem();

            DateTime.TryParse(XML.GetValue(XmlReader, "Date"), out p_transfer_item.Datetime);
            p_transfer_item.Amountx100 = Int64.Parse(XML.GetValue(XmlReader, "Amountx100"));
            p_transfer_item.TerminalName = XML.GetValue(XmlReader, "TerminalName");
            p_transfer_item.PlayerName = XML.GetValue(XmlReader, "PlayerName");

            TransferHistoryItems.Add(p_transfer_item);
            XmlReader.Read(); //After this "read" the reader will be at the end of PlayerName
            XmlReader.Read(); //After this "read" the reader will be at the end of TransferItem
            XmlReader.Read(); //After this "read" the reader will be at the end of HistoryTransfers or the beginning of new a TransferItem

            if ((XmlReader.Name == "HistoryTransfers") && (XmlReader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              _end_transfer_list = true;
            }
          }
        } // while
      }
    }
  }
}
