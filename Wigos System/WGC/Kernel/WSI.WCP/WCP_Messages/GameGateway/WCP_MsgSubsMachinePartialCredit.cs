﻿//------------------------------------------------------------------------------
// Copyright © 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgSubsMachinePartialCredit.cs
// 
//   DESCRIPTION: WCP_MsgSubsMachinePartialCredit class
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 06-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-OCT-2015 FJC    First release. (Product Backlog 4704)
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;
using System.IO;
using System.IO.Compression;

namespace WSI.WCP
{
  public class WCP_MsgSubsMachinePartialCredit: IXml
  {
    public Int64 TransactionId;
    public Int64 RequestAmount;
    public Int64 ObtainedAmount;
    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        
        _sb.AppendLine("<WCP_MsgSubsMachinePartialCredit>");
        _sb.AppendLine("  <TransactionId>" + TransactionId.ToString() + "</TransactionId>");
        _sb.AppendLine("  <RequestAmount>" + RequestAmount.ToString() + "</RequestAmount>");
        _sb.AppendLine("  <ObtainedAmount>" + ObtainedAmount.ToString() + "</ObtainedAmount>");
        _sb.AppendLine("</WCP_MsgSubsMachinePartialCredit>");

        return _sb.ToString();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return String.Empty;
      }
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      try
      {
        TransactionId = XML.GetAsInt64(Reader, "TransactionId");
        RequestAmount = XML.GetAsInt64(Reader, "RequestAmount");
        ObtainedAmount = XML.GetAsInt64(Reader, "ObtainedAmount");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public void LoadXml(String XmlData)
    {
      XmlDocument _xml;
      XmlNode _node;

      try
      {
        _xml = new XmlDocument();

        _xml.LoadXml(XmlData);
        _node = _xml.SelectSingleNode("WCP_MsgSubsMachinePartialCredit");
        RequestAmount = Int64.Parse(XML.GetValue(_node, "RequestAmount"));
        ObtainedAmount = Int64.Parse(XML.GetValue(_node, "ObtainedAmount"));

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    #endregion
  }

  public class WCP_MsgSubsMachinePartialCreditReply : IXml
  {
    
    #region IXml Members

    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      try
      {
        xml = new XmlDocument();
        xml.LoadXml("<WCP_MsgSubsMachinePartialCreditReply></WCP_MsgSubsMachinePartialCreditReply>");
        node = xml.SelectSingleNode("WCP_MsgSubsMachinePartialCreditReply");

        return xml.OuterXml;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return string.Empty;
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      try
      {
        if (Reader.Name != "WCP_MsgSubsMachinePartialCreditReply")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgSubsMachinePartialCreditReply not found in content area");
        }
        else
        {

          Reader.Close();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
    #endregion
  }
}
