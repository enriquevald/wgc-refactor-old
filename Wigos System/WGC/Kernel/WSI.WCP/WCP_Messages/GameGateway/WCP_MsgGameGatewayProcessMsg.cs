﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgGameGatewayProcessMsg.cs
// 
//   DESCRIPTION: WCP_MsgGameGatewayProcessMsg class
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 10-MAR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-MAR-2016 FJC    First release. (Product Backlog 9105 (task 10362))
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;
using System.IO;
using System.IO.Compression;

namespace WSI.WCP
{
  public class WCP_MsgGameGatewayProcessMsg : IXml
  {
    public Int64 CmdId;
    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("<WCP_MsgGameGatewayProcessMsg>");
        _sb.AppendLine("  <CmdId>" + CmdId.ToString() + "</CmdId>");
        _sb.AppendLine("</WCP_MsgGameGatewayProcessMsg>");

        return _sb.ToString();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return String.Empty;
      }
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      Int64 _cmd_id;

      _cmd_id = 0;

      try
      {
        // CmdId
        if (Int64.TryParse(XML.GetValue(Reader, "CmdId"), out _cmd_id))
        {
          CmdId = _cmd_id;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public void LoadXml(String XmlData)
    {
      XmlDocument _xml;
      XmlNode _node;

      CmdId = 0;

      try
      {
        _xml = new XmlDocument();

        _xml.LoadXml(XmlData);
        _node = _xml.SelectSingleNode("WCP_MsgGameGatewayProcessMsg");
        CmdId = Int16.Parse(XML.GetValue(_node, "CmdId"));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public WCP_MsgGameGatewayProcessMsg()
    {
      CmdId = 0;
    }

    #endregion
  }

  public class WCP_MsgGameGatewayProcessMsgReply : IXml
  {

    #region IXml Members

    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      try
      {
        xml = new XmlDocument();
        xml.LoadXml("<WCP_MsgGameGatewayProcessMsgReply></WCP_MsgGameGatewayProcessMsgReply>");
        node = xml.SelectSingleNode("WCP_MsgGameGatewayProcessMsgReply");

        return xml.OuterXml;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return string.Empty;
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      try
      {
        if (Reader.Name != "WCP_MsgGameGatewayProcessMsgReply")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGameGatewayProcessMsgReply not found in content area");
        }
        else
        {
          Reader.Close();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    #endregion
  }

}
