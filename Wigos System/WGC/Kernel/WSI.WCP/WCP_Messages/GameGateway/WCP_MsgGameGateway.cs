﻿//------------------------------------------------------------------------------
// Copyright © 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgGameGateway.cs
// 
//   DESCRIPTION: WCP_MsgGameGateway class
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 06-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-OCT-2015 FJC    First release. (Product Backlog 4704)
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;
using System.IO;
using System.IO.Compression;

namespace WSI.WCP
{
  public class WCP_MsgGameGateway: IXml
  {
    public Int16 MsgType;
    public String MsgText;
    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("<WCP_MsgGameGateway>");
        _sb.AppendLine("  <MsgType>" + MsgType.ToString() + "</MsgType>");
        _sb.AppendLine("  <MsgText>" + MsgText.ToString() + "</MsgText>");
        _sb.AppendLine("</WCP_MsgGameGateway>");

        return _sb.ToString();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return String.Empty;
      }
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      Int16 _msg_type;

      _msg_type = 0;

      try
      {
        // MsgType
        if (Int16.TryParse(XML.GetValue(Reader, "MsgType"), out _msg_type))
        {
          MsgType = _msg_type;
        }

        // MsgText
        MsgText = XML.GetValue(Reader, "MsgText");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public void LoadXml(String XmlData)
    {
      XmlDocument _xml;
      XmlNode _node;

      MsgText = String.Empty;

      try
      {
        _xml = new XmlDocument();

        _xml.LoadXml(XmlData);
        _node = _xml.SelectSingleNode("WCP_MsgGameGateway");
        MsgType = Int16.Parse(XML.GetValue(_node, "MsgType"));
        MsgText = XML.GetValue(_node, "MsgText");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public WCP_MsgGameGateway()
    {
      MsgText = String.Empty;
    }

    #endregion
  }

  public class WCP_MsgGameGatewayReply : IXml
  {

    #region IXml Members

    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      try
      {
        xml = new XmlDocument();
        xml.LoadXml("<WCP_MsgGameGatewayReply></WCP_MsgGameGatewayReply>");
        node = xml.SelectSingleNode("WCP_MsgGameGatewayReply");

        return xml.OuterXml;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return string.Empty;
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      try
      {
        if (Reader.Name != "WCP_MsgGameGatewaytReply")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGameGatewayReply not found in content area");
        }
        else
        {
          Reader.Close();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    #endregion
  }

}
