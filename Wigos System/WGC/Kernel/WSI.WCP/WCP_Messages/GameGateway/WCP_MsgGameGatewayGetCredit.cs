﻿//------------------------------------------------------------------------------
// Copyright © 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgGameGatewayGetCredit.cs
// 
//   DESCRIPTION: WCP_MsgGameGatewayGetCreditclass
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 06-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-OCT-2015 FJC    First release. (Product Backlog 4704)
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;
using System.IO;
using System.IO.Compression;

namespace WSI.WCP
{
  public class WCP_MsgGameGatewayGetCredit : IXml
  {
    public Int64 AccountId;
    public Int64 RequestAmount;
    public Int64 CmdId;
    public Int16 Status;
    public Int16 MsgType;
    public Int16 TypeRequest;
    public String MsgText;

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("<WCP_MsgGameGatewayGetCredit>");
        _sb.AppendLine("  <AccountId>" + AccountId.ToString() + "</AccountId>");
        _sb.AppendLine("  <RequestAmount>" + RequestAmount.ToString() + "</RequestAmount>");
        _sb.AppendLine("  <Status>" + Status.ToString() + "</Status>");
        _sb.AppendLine("  <MsgType>" + MsgType.ToString() + "</MsgType>");
        _sb.AppendLine("  <TypeRequest>" + TypeRequest.ToString() + "</TypeRequest>");
        _sb.AppendLine("  <CmdId>" + CmdId.ToString() + "</CmdId>");
        _sb.AppendLine("  <MsgText>" + MsgText.ToString() + "</MsgText>");
        _sb.AppendLine("</WCP_MsgGameGatewayGetCredit>");

        return _sb.ToString();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return String.Empty;
      }
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      Int64 _account_id;
      Int64 _cmd_id;
      Int64 _request_amount;
      Int16 _status;
      Int16 _msg_type;
      Int16 _type_request;

      _msg_type = 0;
      _account_id = 0;
      _request_amount = 0;
      _status = 0;
      _cmd_id = 0;

      try
      {

        // AccountId
        if (Int64.TryParse(XML.GetValue(Reader, "AccountId"), out _account_id))
        {
          AccountId = _account_id;
        }
        // RequestAmount
        if (Int64.TryParse(XML.GetValue(Reader, "RequestAmount"), out _request_amount))
        {
          RequestAmount = _request_amount;
        }
        
        // Status
        if (Int16.TryParse(XML.GetValue(Reader, "Status"), out _msg_type))
        {
          Status = _status;
        }

        // MsgType
        if (Int16.TryParse(XML.GetValue(Reader, "MsgType"), out _msg_type))
        {
          MsgType = _msg_type;
        }

        // TypeRequest 
        if (Int16.TryParse(XML.GetValue(Reader, "TypeRequest"), out _type_request))
        {
          TypeRequest = _type_request;
        }

        // CmdId
        if (Int64.TryParse(XML.GetValue(Reader, "CmdId"), out _cmd_id))
        {
          CmdId = _cmd_id;
        }

        // MsgText
        MsgText = XML.GetValue(Reader, "MsgText");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public void LoadXml(String XmlData)
    {
      XmlDocument _xml;
      XmlNode _node;

      try
      {
        _xml = new XmlDocument();

        _xml.LoadXml(XmlData);
        _node = _xml.SelectSingleNode("WCP_MsgGameGatewayGetCredit");

        AccountId = Int64.Parse(XML.GetValue(_node, "AccountId"));
        RequestAmount = Int64.Parse(XML.GetValue(_node, "RequestAmount"));
        Status = Int16.Parse(XML.GetValue(_node, "Status"));
        MsgType = Int16.Parse(XML.GetValue(_node, "MsgType"));
        TypeRequest = Int16.Parse(XML.GetValue(_node, "TypeRequest"));
        CmdId = Int64.Parse(XML.GetValue(_node, "CmdId"));
        MsgText = XML.GetValue(_node, "MsgText");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public WCP_MsgGameGatewayGetCredit()
    {
      MsgText = String.Empty;
    }
    #endregion
  }

  public class WCP_MsgGameGatewayGetCreditReply : IXml
  {
    public Int64 AccountId;
    public Int64 RequestAmount;
    public Int64 CmdId;
    public Int16 Status;
    public Int16 MsgType;
    public String MsgText;

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("<WCP_MsgGameGatewayGetCreditReply>");
        _sb.AppendLine("  <AccountId>" + AccountId.ToString() + "</AccountId>");
        _sb.AppendLine("  <RequestAmount>" + RequestAmount.ToString() + "</RequestAmount>");
        _sb.AppendLine("  <Status>" + Status.ToString() + "</Status>");
        _sb.AppendLine("  <MsgType>" + MsgType.ToString() + "</MsgType>");
        _sb.AppendLine("  <MsgText>" + MsgText.ToString() + "</MsgText>");
        _sb.AppendLine("  <CmdId>" + CmdId.ToString() + "</CmdId>");
        _sb.AppendLine("</WCP_MsgGameGatewayGetCreditReply>");

        return _sb.ToString();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        
        return String.Empty;
      }
    }

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      try
      {
        if (Xml.Name != "WCP_MsgGameGatewayGetCreditReply")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGameGatewayGetCreditReply not found in content area");
        }


        while (Xml.Name != "WCP_MsgGameGatewayGetCreditReply")
        {
          if (!Xml.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGameGatewayGetCreditReply element missing");
          }
        }

        while (!((Xml.NodeType.Equals(XmlNodeType.EndElement) || Xml.IsEmptyElement) && Xml.Name == "WCP_MsgGameGatewayGetCreditReply"))
        {
          if (!Xml.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_LCD_MsgGetParametersReply GameGatewayParams element missing");
          }
          else
          {
            if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "Status")
            {
              this.Status = Int16.Parse(XML.GetValue(Xml, "Status"));
              Xml.Read();    // Consume </Status>
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public WCP_MsgGameGatewayGetCreditReply()
    {
      MsgText = String.Empty;
    }

    #endregion
  }

}
