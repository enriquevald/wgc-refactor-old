//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgEnrollTerminal.cs
// 
//   DESCRIPTION: WCP_MsgEnrollTerminal class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgEnrollTerminal:IXml
  {
    public String ProviderId;
    public TerminalTypes TerminalType = TerminalTypes.UNKNOWN;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "WCP_MsgEnrollTerminal", "");
      xml.AppendChild (node);

      XML.AppendChild(node, "ProviderId",   ProviderId);

      XML.AppendChild(node, "TerminalType", TerminalType.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Reader)
    {
      ProviderId = "";
      TerminalType = TerminalTypes.UNKNOWN;

      //Like the old function "LoadXml(String)" this only has an integrity check.
      if (Reader.Name != "WCP_MsgEnrollTerminal")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEnrollTerminal not found in content area");
      }
      
      try 
      {
        while ( Reader.Read () )
        {
          if ( Reader.Name == "ProviderId" )
          {
            if ( Reader.Read() )
            {
              if ( Reader.HasValue )
              {
                ProviderId = Reader.Value;
              }
            }
          }
          if ( Reader.Name == "TerminalType" )
          {
            if ( Reader.Read() )
            {
              if ( Reader.HasValue )
              {
                try
                {
                  TerminalType = (TerminalTypes)Enum.Parse(TerminalType.GetType(), Reader.Value);
                }
                catch
                {
                  TerminalType = TerminalTypes.UNKNOWN;
                }
              }
            }
          }
        }

        switch (TerminalType)
        {
          case TerminalTypes.SITE_JACKPOT:
          case TerminalTypes.MOBILE_BANK:
          case TerminalTypes.MOBILE_BANK_IMB:
          case TerminalTypes.ISTATS:
          case TerminalTypes.PROMOBOX:
            ProviderId = "CASINO";
            break;
          default:
            // Keep received ProviderId
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
  }

  public class WCP_MsgEnrollTerminalReply:IXml
  {
    public Int64 LastSequenceId;
    public Int64 LastTransactionId;

    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "WCP_MsgEnrollTerminalReply", "");
      xml.AppendChild (node);

      XML.AppendChild (node, "LastSequenceId", LastSequenceId.ToString ());
      XML.AppendChild (node, "LastTransactionId", LastTransactionId.ToString ());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgEnrollTerminalReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEnrollTerminalReply not found in content area");
      }
      else
      {
        LastSequenceId = Int64.Parse(XML.GetValue(reader, "LastSequenceId"));
        LastTransactionId = Int64.Parse(XML.GetValue(reader, "LastTransactionId"));
      }
    }
  }
}
