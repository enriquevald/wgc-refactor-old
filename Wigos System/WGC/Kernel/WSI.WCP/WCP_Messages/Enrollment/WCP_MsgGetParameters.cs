//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgEnrollTerminal.cs
// 
//   DESCRIPTION: WCP_MsgGetParameters class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
// 21-JUN-2012 XIT    IMPLEMENTED IXML in WCP_MsgGetParametersReply
// 19-JUL-2012 ACC    Added Sas Flags in get parameters trx.
// 26-OCT-2013 JRM    Added conditions to send tito params based on titomode. TitoMode param is always sent
// 13-NOV-2013 NMR    Fixed error: retrieving boolean values
// 04-AUG-2014 ACC    Added TitoCloseSessionWhenRemoveCard parameter.
// 21-JAN-2015 JMV    Added decimal number parameters
// 29-JUN-2015 FJC    Product Backlog Item 282.
// 30-JAN-2017 FJC    PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
// 31-MAR-2017 FJC    PBI 26015:ZitroTrack
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;
using System.Data.SqlClient;

namespace WSI.WCP
{
  public class WCP_MsgGetParameters : IXml
  {
    public String ToXml()
    {
      XmlDocument _xml;
      XmlNode _node;

      _xml = new XmlDocument();
      _node = _xml.CreateNode(XmlNodeType.Element, "WCP_MsgGetParameters", "");
      _xml.AppendChild(_node);

      return _xml.OuterXml;
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgGetParameters")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetParameters not found in content area");
      }
    }
  }

  public class WCP_MsgGetParametersReply : IXml
  {
    public int ConnectionTimeout;
    public int KeepAliveInterval;
    public String LocationName;
    public String LocationAddress;
    public String MachineName;
    public UInt32 AssetNumber;
    public WCP_NoteAcceptor_Parameters NoteAcceptorParameters;
    public UInt32 SasFlags;
    public String WelcomeMessageLine1;
    public String WelcomeMessageLine2;
    public String CurrencySymbol = "$";
    public String Language = "es";

    // Tito mode parameters
    public bool TitoMode;
    public int SystemId;
    public Int64 SequenceId;
    public Int64 MachineId;
    public Boolean AllowCashableTicketOut;
    public Boolean AllowPromotionalTicketOut;
    public Boolean AllowTicketIn;
    public Int32 ExpirationTicketsCashable;
    public Int32 ExpirationTicketsPromotional;
    public String TitoTicketLocalization;
    public String TitoTicketAddress_1;
    public String TitoTicketAddress_2;
    public String TitoTicketTitleRestricted;
    public String TitoTicketTitleDebit;
    public Int64 TicketsMaxCreationAmount;
    public Int32 BillsMetersRequired;
    public Int32 HostId;

    public Int32 SystemMode;
    public Int32 WassMachineLocked;

    public Int32 TitoCloseSessionWhenRemoveCard;

    // JMV
    public Byte FormatDecimalsFlags;
    public Byte NumDecimals;

    // XCD 22-APR-2015 Add to reponse the protocol of terminal must be use to connect
    public SMIB_COMMUNICATION_TYPE Protocol; //<!-- WCP, SAS, PULSES -->
    public Boolean RequestProtocolParameters;

    public Int32 ChangeStackerMode;
    public Int64 CurrentStackerId;

    // FJC 29-JUN-2015
    public Int32 TechnicianActivityTimeout;

    // JMM 21-MAR-2016: Mico2 mode
    public Boolean Mico2;
    public Boolean EnableDisableNoteAcceptor;

    // FJC 30-JAN-2017 PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
    public Boolean EnableDisableTerminalDraw;

    // FJC 24-MAR-2017 PBI 26015:ZitroTrack
    public Boolean EnableDisableZitroTrack;

    // JMM 13-SEP-2017: BillIn lock limit
    public Int64 BillInLimitLockThreshold;
    public Int32 BillInLimitLockMode;

    // FJC 30-NOV-2017 WIGOS-3998 MobiBank: InTouch - Card out
    public Boolean EnableDisableMobiBank;

    public WCP_MsgGetParametersReply()
    {
      ConnectionTimeout = 60; // secs
      KeepAliveInterval = 30; // secs
      AssetNumber = 0;
      SasFlags = 0;

      SystemMode = 0;
      WassMachineLocked = 0;

      TitoCloseSessionWhenRemoveCard = 0;

      FormatDecimalsFlags = 0;
      NumDecimals = 2;

      TechnicianActivityTimeout = 0;

      Mico2 = false;
      EnableDisableNoteAcceptor = false;
      
      EnableDisableTerminalDraw = false;
      
      EnableDisableZitroTrack = false;

      BillInLimitLockThreshold  = 0;
      BillInLimitLockMode       = 0;

      EnableDisableMobiBank = false;
    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgGetParametersReply>");
      _sb.AppendLine("<ConnectionTimeout>" + ConnectionTimeout.ToString() + "</ConnectionTimeout>");
      _sb.AppendLine("<KeepAliveInterval>" + KeepAliveInterval.ToString() + "</KeepAliveInterval>");
      _sb.AppendLine("<LocationName>" + XML.StringToXmlValue(LocationName) + "</LocationName>");
      _sb.AppendLine("<LocationAddress>" + XML.StringToXmlValue(LocationAddress) + "</LocationAddress>");
      // RCI 05-NOV-2010: Added optional fields for machine name and asset number.
      _sb.AppendLine("<MachineName>" + XML.StringToXmlValue(MachineName) + "</MachineName>");
      _sb.AppendLine("<AssetNumber>" + AssetNumber.ToString() + "</AssetNumber>");
      _sb.AppendLine("<WCP_NoteAcceptor_Parameters>");
      _sb.AppendLine("<ReadBarcode>" + NoteAcceptorParameters.ReadBarcode.ToString() + "</ReadBarcode>");
      _sb.AppendLine("<NumCurrencies>" + NoteAcceptorParameters.NumCurrencies.ToString() + "</NumCurrencies>");
      _sb.AppendLine("<Enabled>" + NoteAcceptorParameters.Enabled.ToString() + "</Enabled>");
      _sb.AppendLine("<Currencies>");

      foreach (WCP_NoteAcceptor_Currency _currency in NoteAcceptorParameters.AcceptedCurrencies)
      {
        _sb.AppendLine("<Currency>");
        _sb.AppendLine("<CurrencyName>" + XML.StringToXmlValue(_currency.IsoCode) + "</CurrencyName>");
        _sb.AppendLine("<Alias_1>" + XML.StringToXmlValue(_currency.Alias_1) + "</Alias_1>");
        _sb.AppendLine("<Alias_2>" + XML.StringToXmlValue(_currency.Alias_2) + "</Alias_2>");
        _sb.AppendLine("<RejectedValues>");

        foreach (Int32 _rejected in _currency.RejectedValues)
        {
          _sb.AppendLine("<RejectedValue>" + _rejected.ToString() + "</RejectedValue>");
        }
        _sb.AppendLine("</RejectedValues>");
        _sb.AppendLine("</Currency>");
      }

      _sb.AppendLine("</Currencies>");
      _sb.AppendLine("</WCP_NoteAcceptor_Parameters>");
      _sb.AppendLine("<SasFlags>" + SasFlags.ToString() + "</SasFlags>");
      _sb.AppendLine("<WelcomeMessageLine1>" + XML.StringToXmlValue(WelcomeMessageLine1) + "</WelcomeMessageLine1>");
      _sb.AppendLine("<WelcomeMessageLine2>" + XML.StringToXmlValue(WelcomeMessageLine2) + "</WelcomeMessageLine2>");
      _sb.AppendLine("<CurrencySymbol>" + XML.StringToXmlValue(CurrencySymbol) + "</CurrencySymbol>");      

      // JRM 14-Oct-2013 Tito paramenters added
      if (TitoMode)
      {
        _sb.AppendLine("<TitoMode>1</TitoMode>");
        _sb.AppendLine("<SystemId>" + SystemId.ToString() + "</SystemId>");
        _sb.AppendLine("<SequenceId>" + SequenceId.ToString() + "</SequenceId>");
        _sb.AppendLine("<AllowCashableTicketOut>" + (AllowCashableTicketOut ? "1" : "0") + "</AllowCashableTicketOut>");
        _sb.AppendLine("<AllowPromotionalTicketOut>" + (AllowPromotionalTicketOut ? "1" : "0") + "</AllowPromotionalTicketOut>");
        _sb.AppendLine("<AllowTicketIn>" + (AllowTicketIn ? "1" : "0") + "</AllowTicketIn>");
        _sb.AppendLine("<ExpirationTicketsCashable>" + ExpirationTicketsCashable.ToString() + "</ExpirationTicketsCashable>");
        _sb.AppendLine("<ExpirationTicketsPromotional>" + ExpirationTicketsPromotional.ToString() + "</ExpirationTicketsPromotional>");
        _sb.AppendLine("<TitoTicketLocalization>" + XML.StringToXmlValue(TitoTicketLocalization) + "</TitoTicketLocalization>");
        _sb.AppendLine("<TitoTicketAddress_1>" + XML.StringToXmlValue(TitoTicketAddress_1) + "</TitoTicketAddress_1>");
        _sb.AppendLine("<TitoTicketAddress_2>" + XML.StringToXmlValue(TitoTicketAddress_2) + "</TitoTicketAddress_2>");
        _sb.AppendLine("<TitoTicketTitleRestricted>" + XML.StringToXmlValue(TitoTicketTitleRestricted) + "</TitoTicketTitleRestricted>");
        _sb.AppendLine("<TitoTicketTitleDebit>" + XML.StringToXmlValue(TitoTicketTitleDebit) + "</TitoTicketTitleDebit>");
        _sb.AppendLine("<MachineId>" + MachineId.ToString() + "</MachineId>");
        _sb.AppendLine("<TicketsMaxCreationAmount>" + TicketsMaxCreationAmount.ToString() + "</TicketsMaxCreationAmount>");
        _sb.AppendLine("<BillsMetersRequired>" + BillsMetersRequired.ToString() + "</BillsMetersRequired>");
        _sb.AppendLine("<HostID>" + HostId.ToString() + "</HostID>");        
      }
      else
      {
        _sb.AppendLine("<TitoMode>0</TitoMode>");
      }

      _sb.AppendLine("<Language>" + XML.StringToXmlValue(Language) + "</Language>");

      _sb.AppendLine("<SystemMode>" + SystemMode.ToString() + "</SystemMode>");
      _sb.AppendLine("<WassMachineLocked>" + WassMachineLocked.ToString() + "</WassMachineLocked>");

      _sb.AppendLine("<TitoCloseSessionWhenRemoveCard>" + TitoCloseSessionWhenRemoveCard.ToString() + "</TitoCloseSessionWhenRemoveCard>");

      // JMV
      _sb.AppendLine("<FormatDecimalsFlags>" + FormatDecimalsFlags.ToString() + "</FormatDecimalsFlags>");
      _sb.AppendLine("<NumDecimals>" + NumDecimals.ToString() + "</NumDecimals>");

      // XCD 22-APR-2015 Send the protocol that terminal must connect
      _sb.AppendLine("<Protocol>" + ((Int32)Protocol).ToString() + "</Protocol>");
      _sb.AppendLine("<RequestProtocolParameters>" + (RequestProtocolParameters? "1":"0") + "</RequestProtocolParameters>");

      // ACC 27-APR-2015 Add Change stacker mode: 0: Machine, 1: StackerId
      _sb.AppendLine("<ChangeStackerMode>" + ChangeStackerMode.ToString() + "</ChangeStackerMode>");
      _sb.AppendLine("<CurrentStackerId>" + CurrentStackerId.ToString() + "</CurrentStackerId>");

      // FJC 29-JUN-2015
      _sb.AppendLine("<TechnicianActivityTimeout>" + TechnicianActivityTimeout.ToString() + "</TechnicianActivityTimeout>");

      // JMM 21-MAR-2016: Mico2 mode
      _sb.AppendLine("<Mico2>" + (Mico2 ? "1" : "0") + "</Mico2>");
      _sb.AppendLine("<EnableDisableNoteAcceptor>" + (EnableDisableNoteAcceptor ? "1" : "0") + "</EnableDisableNoteAcceptor>");      

      // FJC 30-JAN-2017 PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
      _sb.AppendLine("<EnableDisableTerminalDraw>" + (EnableDisableTerminalDraw ? "1" : "0") + "</EnableDisableTerminalDraw>");      

      // FJC 24-MAR-2017 PBI 26015:ZitroTrack
      _sb.AppendLine("<EnableDisableZitroTrack>" + (EnableDisableZitroTrack ? "1" : "0") + "</EnableDisableZitroTrack>");      

      // JMM 13-SEP-2017: BillIn lock limit
      _sb.AppendLine("<BillInLimitLockThreshold>" + BillInLimitLockThreshold.ToString() + "</BillInLimitLockThreshold>");
      _sb.AppendLine("<BillInLimitLockMode>" + BillInLimitLockMode.ToString() + "</BillInLimitLockMode>");

      // FJC 30-NOV-2017 WIGOS-3998 MobiBank: InTouch - Card out
      _sb.AppendLine("<EnableDisableMobiBank>" + (EnableDisableMobiBank ? "1" : "0") + "</EnableDisableMobiBank>");      

      _sb.AppendLine("</WCP_MsgGetParametersReply>");     

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Reader)
    {
      Boolean _is_new_version;
      String _is_new_version_txt;
      WCP_NoteAcceptor_Currency _currency;
      Int32 _is_tito_mode_int;
      Int32 _smib_protocol;
      Int32 _smib_protocol_request;
      Int32 _mico2;      
      Int32 _enable_disable_note_acceptor;
      Int32 _enable_disable_terminal_draw;
      Int32 _enable_disable_zitro_track;
      Int32 _enable_disable_mobibank;

      _is_tito_mode_int = 0;

      if (Reader.Name != "WCP_MsgGetParametersReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetParametersReply not found in content area");
      }
      else
      {
        Reader.Read();
        ConnectionTimeout = Int32.Parse(XML.ReadTagValue(Reader, "ConnectionTimeout"));
        KeepAliveInterval = Int32.Parse(XML.ReadTagValue(Reader, "KeepAliveInterval"));
        LocationName = XML.ReadTagValue(Reader, "LocationName");
        LocationAddress = XML.ReadTagValue(Reader, "LocationAddress");
        // RCI 05-NOV-2010: Added optional fields for machine name and asset number.
        MachineName = XML.ReadTagValue(Reader, "MachineName", true);
        UInt32.TryParse(XML.ReadTagValue(Reader, "AssetNumber", true), out AssetNumber);

        _is_new_version_txt = XML.ReadTagValue(Reader, "ReadBarcode", true);

        if (_is_new_version_txt != string.Empty)
        {
          _is_new_version = true;
        }
        else
        {
          _is_new_version = false;
        }
        if (_is_new_version)
        {
          NoteAcceptorParameters = new WCP_NoteAcceptor_Parameters();
          NoteAcceptorParameters.ReadBarcode = Boolean.Parse(_is_new_version_txt);
          NoteAcceptorParameters.NumCurrencies = Int32.Parse(XML.ReadTagValue(Reader, "NumCurrencies"));
          NoteAcceptorParameters.Enabled = Boolean.Parse(XML.ReadTagValue(Reader, "Enabled"));
          NoteAcceptorParameters.AcceptedCurrencies = new List<WCP_NoteAcceptor_Currency>();

          while ((Reader.Name != "WCP_NoteAcceptor_Parameters") || (Reader.NodeType != XmlNodeType.EndElement))
          {
            if ((Reader.Name == "Currency") && (Reader.NodeType == XmlNodeType.Element))
            {
              _currency = new WCP_NoteAcceptor_Currency();
              _currency.IsoCode = XML.ReadTagValue(Reader, "CurrencyName");
              _currency.Alias_1 = XML.ReadTagValue(Reader, "Alias_1");
              _currency.Alias_2 = XML.ReadTagValue(Reader, "Alias_2");

              while ((Reader.Name != "Currency") || (Reader.NodeType != XmlNodeType.EndElement))
              {
                if (Reader.Name == "RejectedValue")
                {
                  _currency.RejectedValues.Add(Int32.Parse(XML.ReadTagValue(Reader, "RejectedValue")));
                }
                Reader.Read();
              }
              this.NoteAcceptorParameters.AcceptedCurrencies.Add(_currency);
            }
            Reader.Read();
          }

          UInt32.TryParse(XML.ReadTagValue(Reader, "SasFlags", true), out SasFlags);
          WelcomeMessageLine1 = XML.ReadTagValue(Reader, "WelcomeMessageLine1", true);
          WelcomeMessageLine2 = XML.ReadTagValue(Reader, "WelcomeMessageLine2", true);

          Reader.Read(); // After this "read" the reader will be at the end of WelcomeMessageLine2
          Reader.Read(); // After this "read" the reader will be at the beginning of CurrencySymbol or end of WCP_MsgGetParametersReply

          // The default is "$", if ReadTagValue with optional remove default value.
          if (Reader.Name == "CurrencySymbol")
          {
            CurrencySymbol = XML.ReadTagValue(Reader, "CurrencySymbol", true);
          }
        }

        // Check if the system is on TITO Mode. If so, add the TITO Params
        Int32.TryParse(XML.ReadTagValue(Reader, "TitoMode", true), out _is_tito_mode_int);
        TitoMode = _is_tito_mode_int == 1;

        if (TitoMode)
        {
          SystemId = Int32.Parse(XML.ReadTagValue(Reader, "SystemId", true));
          SequenceId = Int32.Parse(XML.ReadTagValue(Reader, "SequenceId", true));
          AllowCashableTicketOut = XML.ReadTagValue(Reader, "AllowCashableTicketOut") == "1";
          AllowPromotionalTicketOut = XML.ReadTagValue(Reader, "AllowPromotionalTicketOut") == "1";
          AllowTicketIn = XML.ReadTagValue(Reader, "AllowTicketIn") == "1";
          ExpirationTicketsCashable = Int32.Parse(XML.ReadTagValue(Reader, "ExpirationTicketsCashable", true));
          ExpirationTicketsPromotional = Int32.Parse(XML.ReadTagValue(Reader, "ExpirationTicketsPromotional", true));
          TitoTicketLocalization = XML.ReadTagValue(Reader, "TitoTicketLocalization", true);
          TitoTicketAddress_1 = XML.ReadTagValue(Reader, "TitoTicketAddress_1", true);
          TitoTicketAddress_2 = XML.ReadTagValue(Reader, "TitoTicketAddress_2", true);
          TitoTicketTitleRestricted = XML.ReadTagValue(Reader, "TitoTicketTitleRestricted", true);
          TitoTicketTitleDebit = XML.ReadTagValue(Reader, "TitoTicketTitleDebit", true);
          MachineId = Int64.Parse(XML.ReadTagValue(Reader, "MachineId", true));
          TicketsMaxCreationAmount = Int64.Parse(XML.ReadTagValue(Reader, "TicketsMaxCreationAmount", true));
          BillsMetersRequired = XML.GetAsInt32(Reader, "BillsMetersRequired");

          if (Reader.Name == "HostID")
          {
            HostId = XML.GetAsInt32(Reader, "HostID");          
          }


          Reader.Read(); // After this "read" the reader will be at the end of BillsMetersRequired
        }

        Reader.Read(); // After this "read" the reader will be at the end of TitoMode
        Reader.Read(); // After this "read" the reader will be at the beginning of Language or end of WCP_MsgGetParametersReply

        // The default is "es", if ReadTagValue with optional remove default value.
        if (Reader.Name == "Language")
        {
          Language = XML.ReadTagValue(Reader, "Language", true);
        }

        Reader.Read(); // After this "read" the reader will be at the end of Language
        Reader.Read(); // After this "read" the reader will be at the beginning of SystemMode or end of WCP_MsgGetParametersReply

        if (Reader.Name == "SystemMode")
        {
          SystemMode = Int32.Parse(XML.ReadTagValue(Reader, "SystemMode", true));
          WassMachineLocked = Int32.Parse(XML.ReadTagValue(Reader, "WassMachineLocked", true));
        }

        Int32.TryParse(XML.ReadTagValue(Reader, "TitoCloseSessionWhenRemoveCard", true), out TitoCloseSessionWhenRemoveCard);

        // JMV
        if (Byte.TryParse(XML.ReadTagValue(Reader, "FormatDecimalsFlags", true), out FormatDecimalsFlags))
        {
          NumDecimals = Byte.Parse(XML.ReadTagValue(Reader, "NumDecimals", true));
        }

        // XCD 22-APR-2015 Send the protocol that terminal must connect
        if (Int32.TryParse(XML.ReadTagValue(Reader, "Protocol", true), out _smib_protocol))
        {
          Protocol = (SMIB_COMMUNICATION_TYPE)_smib_protocol;
          Int32.TryParse(XML.ReadTagValue(Reader, "RequestProtocolParameters", true), out _smib_protocol_request);
          RequestProtocolParameters = _smib_protocol_request > 0 ? true : false;
        }
        else
        {
          Protocol = WSI.Common.SMIB_COMMUNICATION_TYPE.NONE;
          RequestProtocolParameters = false;
        }

        // ACC 27-APR-2015 Add change stacker mode (0: Machine, 1: StackerId)
        ChangeStackerMode = 0;
        Int32.TryParse(XML.ReadTagValue(Reader, "ChangeStackerMode", true), out ChangeStackerMode);
        CurrentStackerId = 0;
        Int64.TryParse(XML.ReadTagValue(Reader, "CurrentStackerId", true), out CurrentStackerId);

        // FJC 29-JUN-2015
        TechnicianActivityTimeout = 0;
        Int32.TryParse(XML.ReadTagValue(Reader, "TechnicianActivityTimeout", true), out TechnicianActivityTimeout);

        // JMM 21-MAR-2016: Mico2 mode
        Int32.TryParse(XML.ReadTagValue(Reader, "Mico2", true), out _mico2);
        Mico2 = _mico2 > 0 ? true : false;

        Int32.TryParse(XML.ReadTagValue(Reader, "EnableDisableNoteAcceptor", true), out _enable_disable_note_acceptor);
        EnableDisableNoteAcceptor = _enable_disable_note_acceptor > 0 ? true : false;        
     
        // FJC 30-JAN-2017 PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
        Int32.TryParse(XML.ReadTagValue(Reader, "EnableDisableTerminalDraw", true), out _enable_disable_terminal_draw);
        EnableDisableTerminalDraw = _enable_disable_terminal_draw > 0 ? true : false;   

        // FJC 24-MAR-2017 PBI 26015:ZitroTrack
        Int32.TryParse(XML.ReadTagValue(Reader, "EnableDisableZitroTrack", true), out _enable_disable_zitro_track);
        EnableDisableZitroTrack = _enable_disable_zitro_track > 0 ? true : false;

        // JMM 13-SEP-2017: BillIn lock limit
        BillInLimitLockThreshold = 0;
        Int64.TryParse(XML.ReadTagValue(Reader, "BillInLimitLockThreshold", true), out BillInLimitLockThreshold);

        BillInLimitLockMode = 0;
        Int32.TryParse(XML.ReadTagValue(Reader, "BillInLimitLockMode", true), out BillInLimitLockMode);

        // FJC 30-NOV-2017 WIGOS-3998 MobiBank: InTouch - Card out
        Int32.TryParse(XML.ReadTagValue(Reader, "EnableDisableMobiBank", true), out _enable_disable_mobibank);
        EnableDisableMobiBank = _enable_disable_mobibank == 1 ? true : false;
     }
    }

    public class WCP_NoteAcceptor_Parameters
    {
      #region Fields
      private Boolean m_read_barcode;
      private Int32 m_num_currencies;
      private Boolean m_enabled;
      private List<WCP_NoteAcceptor_Currency> m_accepted_currencies;
      #endregion

      #region Properties
      public Boolean ReadBarcode
      {
        get { return m_read_barcode; }
        set { m_read_barcode = value; }
      }

      public Int32 NumCurrencies
      {
        get { return m_num_currencies; }
        set { m_num_currencies = value; }
      }

      public Boolean Enabled
      {
        get { return m_enabled; }
        set { m_enabled = value; }
      }

      public List<WCP_NoteAcceptor_Currency> AcceptedCurrencies
      {
        get { return m_accepted_currencies; }
        set { m_accepted_currencies = value; }
      }

      #endregion

      public WCP_NoteAcceptor_Parameters()
      {
        this.ReadBarcode = false;
        this.AcceptedCurrencies = new List<WCP_NoteAcceptor_Currency>();
        this.NumCurrencies = 0;
      }
    }

    public class WCP_NoteAcceptor_Currency
    {
      #region Fields
      private String m_iso_code;
      private String m_alias1;
      private String m_alias2;
      private List<Int32> m_rejected_values;
      #endregion

      #region Properties
      public String IsoCode
      {
        get { return m_iso_code; }
        set { m_iso_code = value; }
      }

      public String Alias_1
      {
        get { return m_alias1; }
        set { m_alias1 = value; }
      }

      public String Alias_2
      {
        get { return m_alias2; }
        set { m_alias2 = value; }
      }

      public List<Int32> RejectedValues
      {
        get { return m_rejected_values; }
        set { m_rejected_values = value; }
      }
      #endregion

      public WCP_NoteAcceptor_Currency()
      {
        this.RejectedValues = new List<Int32>();
        this.IsoCode = String.Empty;
        this.Alias_1 = String.Empty;
        this.Alias_2 = String.Empty;
      }
    }

    public class FindCurrency
    {
      private String m_name;
      private WSI.WCP.WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency CurrencyToFind;

      public String Name
      {
        get { return m_name; }
        set
        {
          m_name = value;
          CurrencyToFind = new WCP_NoteAcceptor_Currency();
          CurrencyToFind.IsoCode = m_name;
        }
      }

      public bool FindCurrencyByName(WSI.WCP.WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency Currency)
      {
        return Currency.IsoCode == this.Name;
      }
    }

  }
}





