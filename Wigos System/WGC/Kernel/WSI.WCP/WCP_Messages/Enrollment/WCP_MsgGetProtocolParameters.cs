//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgEnrollTerminal.cs
// 
//   DESCRIPTION: WCP_MsgGetParameters class
// 
// CREATION DATE: 28-APR-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2015 XCD    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_MsgGetProtocolParameters : IXml
  {
    public Int32 Protocol;

    public String ToXml()
    {
      XmlDocument _xml;
      XmlNode _node;

      _xml = new XmlDocument();
      _node = _xml.CreateNode(XmlNodeType.Element, "WCP_MsgGetProtocolParameters", "");
      _xml.AppendChild(_node);

      XML.AppendChild(_node, "Protocol", Protocol.ToString());

      return _xml.OuterXml;
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgGetProtocolParameters")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetProtocolParameters not found in content area");
      }

      Reader.Read();
      if (!Int32.TryParse(XML.ReadTagValue(Reader, "Protocol"), out Protocol))
      {
        Protocol = (Int32)SMIB_COMMUNICATION_TYPE.NONE;
      }
    }
  }

  public class WCP_MsgGetProtocolParametersReply : IXml
  {
    public SmibProtocols SmibConfig;

    public WCP_MsgGetProtocolParametersReply()
    {
      SmibConfig = new SmibProtocols();
    }

    public String ToXml()
    {
      XmlDocument _xml;
      XmlNode _node;

      _xml = new XmlDocument();
      _node = _xml.CreateNode(XmlNodeType.Element, "WCP_MsgGetProtocolParametersReply", "");

      //_xml.AppendChild(XML.AppendChild(_node, "Protocol", SmibConfig.Protocol.ToString()));      
      XML.AppendChild(_node, "Protocol", SmibConfig.Protocol.ToString());

      switch (SmibConfig.Protocol)
      {
        case SMIB_COMMUNICATION_TYPE.WCP: AddWcpParameters(ref _node); break;
        case SMIB_COMMUNICATION_TYPE.SAS: AddSasParameters(ref _node); break;
        case SMIB_COMMUNICATION_TYPE.PULSES: AddPulsesParameters(ref _node); break;
        case SMIB_COMMUNICATION_TYPE.NONE:
        default:
          break;
      }

      _xml.AppendChild(_node);

      return _xml.OuterXml;

    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgGetProtocolParametersReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetProtocolParametersReply not found in content area");
      }
      else
      {
        Reader.Read();
        SmibConfig.Protocol = SmibConfig.GetProtocol(XML.ReadTagValue(Reader, "Protocol"));

        switch (SmibConfig.Protocol)
        {
          case SMIB_COMMUNICATION_TYPE.WCP: ReadWcpParameters(ref Reader); break;
          case SMIB_COMMUNICATION_TYPE.SAS: ReadSasParameters(ref Reader); break;
          case SMIB_COMMUNICATION_TYPE.PULSES: ReadPulsesParameters(ref Reader); break;
          case SMIB_COMMUNICATION_TYPE.NONE:
          default:
            break;
        }
      }
    }

    #region "SAS"
    private void ReadSasParameters(ref XmlReader Reader)
    {
      throw new Exception("The method or operation is not implemented.");
    }

    private void AddSasParameters(ref XmlNode ParentNode)
    {
      throw new Exception("The method or operation is not implemented.");
    }
    #endregion

    #region "WCP"
    private void ReadWcpParameters(ref XmlReader Reader)
    {
      throw new Exception("The method or operation is not implemented.");
    }

    private void AddWcpParameters(ref XmlNode ParentNode)
    {
      throw new Exception("The method or operation is not implemented.");
    }
    #endregion

    #region "Pulses"
    private void ReadPulsesParameters(ref XmlReader Reader)
    {
      Int32 _idx_port_read;

      SmibProtocols.PCDProtocolConfiguration.Polling _polling_params;
      SmibProtocols.PCDProtocolConfiguration.PCDPort _pcd_port;

      Reader.Read();

      while ((!(Reader.Name == "Parameters" && Reader.NodeType == XmlNodeType.EndElement)) && (Reader.Name != "Inputs"))
      {
        Reader.Read();
        if ((Reader.Name == "Polling") && (Reader.NodeType != XmlNodeType.EndElement))
        {
          _polling_params = SmibConfig.PcdParams.PollingParams;
          UInt32.TryParse(Reader.GetAttribute("CashoutFinished"), out _polling_params.CashoutFinished);
          UInt32.TryParse(Reader.GetAttribute("MetersIdle"), out _polling_params.MetersIdle);
          UInt32.TryParse(Reader.GetAttribute("MetersInSession"), out _polling_params.MetersInSession);
          UInt32.TryParse(Reader.GetAttribute("MetersOnCashout"), out _polling_params.MetersOnCashout);
          SmibConfig.PcdParams.PollingParams = _polling_params;
        }
      }

      _idx_port_read = 0;
      while ((!(Reader.Name == "Inputs" && Reader.NodeType == XmlNodeType.EndElement)) && (Reader.Name != "Outputs"))
      {
        Reader.Read();
        if ((Reader.Name == "Input") && (Reader.NodeType != XmlNodeType.EndElement))
        {
          _pcd_port = SmibConfig.PcdParams.InputPort(_idx_port_read);
          PcdIoGetNodeProperties(ref Reader, ref _pcd_port);          
          _idx_port_read++;
        }
      }

      _idx_port_read = 0;
      while ((!(Reader.Name == "Outputs" && Reader.NodeType == XmlNodeType.EndElement)) && (Reader.Name != "Pulses"))
      {
        Reader.Read();
        if ((Reader.Name == "Output") && (Reader.NodeType != XmlNodeType.EndElement))
        {
          _pcd_port = SmibConfig.PcdParams.OutputPort(_idx_port_read);
          PcdIoGetNodeProperties(ref Reader, ref _pcd_port);          
          _idx_port_read++;
        }
      }
    }

    private void PcdIoGetNodeProperties(ref XmlReader Reader, ref SmibProtocols.PCDProtocolConfiguration.PCDPort PcdPort)
    {
      Int32 _aux_value;

      PcdPort.PcdIoType = SmibConfig.PcdParams.GetPortType(Reader.GetAttribute("Type"));
      Int32.TryParse(Reader.GetAttribute("Port"), out _aux_value);
      PcdPort.PcdIoNumber = _aux_value;

      switch (PcdPort.PcdIoType)
      {
        case SmibProtocols.PCDProtocolConfiguration.PortType.CASH_IN:
          Decimal.TryParse(Reader.GetAttribute("Multiplier"), out PcdPort.CodeNumberMultiplier);
          PcdPort.PulseEdgeType = SmibConfig.PcdParams.GetEdgeType(Reader.GetAttribute("Edge"));          
          UInt32.TryParse(Reader.GetAttribute("T0"), out PcdPort.TimePulseDownMs);
          UInt32.TryParse(Reader.GetAttribute("T1"), out PcdPort.TimePulseUpMs);
          break;

        case SmibProtocols.PCDProtocolConfiguration.PortType.SHUTDOWN:
          PcdPort.PulseEdgeType = SmibConfig.PcdParams.GetEdgeType(Reader.GetAttribute("On"));
          break;

        case SmibProtocols.PCDProtocolConfiguration.PortType.METER:
          Int64.TryParse(Reader.GetAttribute("MeterCode"), out PcdPort.CodeNumber);
          Decimal.TryParse(Reader.GetAttribute("Multiplier"), out PcdPort.CodeNumberMultiplier);
          break;

        case SmibProtocols.PCDProtocolConfiguration.PortType.DOOR:
          Int64.TryParse(Reader.GetAttribute("DoorId"), out PcdPort.CodeNumber);
          PcdPort.PulseEdgeType = SmibConfig.PcdParams.GetEdgeType(Reader.GetAttribute("Edge"));
          break;

        default:
          break;
      }
    }

    private void AddPulsesParameters(ref XmlNode ParentNode)
    {
      XmlNode _node;
      XmlNode _p_node;
      XmlAttribute _attribute;

      _node = XML.AppendChild(ParentNode, "Pulses", "");

      // PArameters     
      _node = XML.AppendChild(_node, "Parameters", "");
      _node = XML.AppendChild(_node, "Polling", "");

      _attribute = ParentNode.OwnerDocument.CreateAttribute("MetersIdle");
      _attribute.Value = SmibConfig.PcdParams.PollingParams.MetersIdle.ToString();
      _node.Attributes.Append(_attribute);
      _attribute = ParentNode.OwnerDocument.CreateAttribute("MetersInSession");
      _attribute.Value = SmibConfig.PcdParams.PollingParams.MetersInSession.ToString();
      _node.Attributes.Append(_attribute);
      _attribute = ParentNode.OwnerDocument.CreateAttribute("MetersOnCashout");
      _attribute.Value = SmibConfig.PcdParams.PollingParams.MetersOnCashout.ToString();
      _node.Attributes.Append(_attribute);
      _attribute = ParentNode.OwnerDocument.CreateAttribute("CashoutFinished");
      _attribute.Value = SmibConfig.PcdParams.PollingParams.CashoutFinished.ToString();
      _node.Attributes.Append(_attribute);

      // Inputs
      _node = ParentNode.SelectSingleNode("Pulses");
      _p_node = XML.AppendChild(_node, "Inputs", "");
      for (Int16 _idx_input = 0; _idx_input < SmibProtocols.PCDProtocolConfiguration.PCD_NUM_INPUT_PORTS; _idx_input++)
      {
        if (SmibConfig.PcdParams.InputPort(_idx_input).CodeNumber == SmibProtocols.PCDProtocolConfiguration.PCD_NULL_VALUE)
        {
          continue;
        }
        _node = XML.AppendChild(_p_node, "Input", "");

        PcdIoSetNodeProperties(ref _node, SmibConfig.PcdParams.InputPort(_idx_input));
      }

      // Outputs
      _node = ParentNode.SelectSingleNode("Pulses");
      _p_node = XML.AppendChild(_node, "Outputs", "");
      for (Int16 _idx_output = 0; _idx_output < SmibProtocols.PCDProtocolConfiguration.PCD_NUM_OUTPUT_PORTS; _idx_output++)
      {
        if (SmibConfig.PcdParams.OutputPort(_idx_output).CodeNumber == SmibProtocols.PCDProtocolConfiguration.PCD_NULL_VALUE)
        {
          continue;
        }
        _node = XML.AppendChild(_p_node, "Output", "");
        PcdIoSetNodeProperties(ref _node, SmibConfig.PcdParams.OutputPort(_idx_output));
      }
    }

    private void PcdIoSetNodeProperties(ref XmlNode Node, SmibProtocols.PCDProtocolConfiguration.PCDPort PcdPort)
    {
      XmlAttribute _attribute;
      String _aux_multiplier;

      // Common attributes
      _attribute = Node.OwnerDocument.CreateAttribute("Type");
      _attribute.Value = PcdPort.PcdIoType.ToString();
      Node.Attributes.Append(_attribute);
      _attribute = Node.OwnerDocument.CreateAttribute("Port");
      _attribute.Value = PcdPort.PcdIoNumber.ToString();
      Node.Attributes.Append(_attribute);

      // Port special properties
      switch (PcdPort.PcdIoType)
      {
        case SmibProtocols.PCDProtocolConfiguration.PortType.CASH_IN:
          _attribute = Node.OwnerDocument.CreateAttribute("Multiplier");
          _aux_multiplier = PcdPort.CodeNumberMultiplier.ToString("G29");
          _attribute.Value = _aux_multiplier.Replace(",", ".");
          Node.Attributes.Append(_attribute);
          _attribute = Node.OwnerDocument.CreateAttribute("Edge");
          _attribute.Value = PcdPort.PulseEdgeType.ToString();
          Node.Attributes.Append(_attribute);
          _attribute = Node.OwnerDocument.CreateAttribute("T0");
          _attribute.Value = PcdPort.TimePulseDownMs.ToString();
          Node.Attributes.Append(_attribute);
          _attribute = Node.OwnerDocument.CreateAttribute("T1");
          _attribute.Value = PcdPort.TimePulseUpMs.ToString();
          Node.Attributes.Append(_attribute);
          break;

        case SmibProtocols.PCDProtocolConfiguration.PortType.SHUTDOWN:
          switch (PcdPort.PulseEdgeType)
          {
            case SmibProtocols.PCDProtocolConfiguration.EdgeType.PULSE_UP:
              _attribute = Node.OwnerDocument.CreateAttribute("On");
              _attribute.Value = SmibProtocols.PCDProtocolConfiguration.EdgeType.PULSE_UP.ToString();
              Node.Attributes.Append(_attribute);
              _attribute = Node.OwnerDocument.CreateAttribute("Off");
              _attribute.Value = SmibProtocols.PCDProtocolConfiguration.EdgeType.PULSE_DOWN.ToString();
              Node.Attributes.Append(_attribute);
              break;

            case SmibProtocols.PCDProtocolConfiguration.EdgeType.PULSE_DOWN:
              _attribute = Node.OwnerDocument.CreateAttribute("On");
              _attribute.Value = SmibProtocols.PCDProtocolConfiguration.EdgeType.PULSE_DOWN.ToString();
              Node.Attributes.Append(_attribute);
              _attribute = Node.OwnerDocument.CreateAttribute("Off");
              _attribute.Value = SmibProtocols.PCDProtocolConfiguration.EdgeType.PULSE_UP.ToString();
              Node.Attributes.Append(_attribute);
              break;

            default:
              break;
          }
          break;

        case SmibProtocols.PCDProtocolConfiguration.PortType.METER:
          _attribute = Node.OwnerDocument.CreateAttribute("MeterCode");
          _attribute.Value = PcdPort.CodeNumber.ToString();
          Node.Attributes.Append(_attribute);
          _attribute = Node.OwnerDocument.CreateAttribute("Multiplier");
          _aux_multiplier = PcdPort.CodeNumberMultiplier.ToString("G29");
          _attribute.Value = _aux_multiplier.Replace(",", ".");
          Node.Attributes.Append(_attribute);
          break;

        case SmibProtocols.PCDProtocolConfiguration.PortType.DOOR:
          _attribute = Node.OwnerDocument.CreateAttribute("DoorId");
          _attribute.Value = PcdPort.CodeNumber.ToString();
          Node.Attributes.Append(_attribute);
          _attribute = Node.OwnerDocument.CreateAttribute("Edge");
          _attribute.Value = PcdPort.PulseEdgeType.ToString();
          Node.Attributes.Append(_attribute);
          break;

        default:
          break;
      }
    }
  }
    #endregion

}
