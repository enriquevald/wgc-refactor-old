//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgUnenrollTerminal.cs
// 
//   DESCRIPTION: WCP_MsgUnenrollTerminal class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;


namespace WSI.WCP
{
  public class WCP_MsgUnenrollTerminal:IXml
  {
    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "WCP_MsgUnenrollTerminal", "");
      xml.AppendChild (node);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgUnenrollTerminal")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgUnenrollTerminal not found in content area");
      }
    }
  }

  public class WCP_MsgUnenrollTerminalReply:IXml
  {
    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "WCP_MsgUnenrollTerminalReply", "");
      xml.AppendChild (node);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgUnenrollTerminalReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgUnenrollTerminalReply not found in content area");
      }
    }
  }
}
