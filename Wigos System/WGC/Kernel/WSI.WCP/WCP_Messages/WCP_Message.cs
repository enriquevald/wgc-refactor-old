//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Mesage.cs
// 
//   DESCRIPTION: WCP_Mesage class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
// 06-NOV-2013 NMR    Messages reorganization
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using WSI.Common;


namespace WSI.WCP
{
  public interface IXml
  {
    String ToXml();
    void LoadXml(XmlReader Xml);
  }

  public class WCP_Exception : Exception
  {
    WCP_ResponseCodes response_code;
    String response_code_text;

    public WCP_ResponseCodes ResponseCode
    {
      get
      {
        return response_code;
      }
    }
    public String ResponseCodeText
    {
      get
      {
        return response_code_text;
      }
    }

    public WCP_Exception (WCP_ResponseCodes Value)
    {
      response_code = Value;
    }

    public WCP_Exception (WCP_ResponseCodes Code, String Text):base(Text)
    {
      response_code = Code;
      response_code_text = Text;
    }

    public WCP_Exception (WCP_ResponseCodes Code, String Text, Exception InnerException): base (Text, InnerException)
    {
      response_code = Code;
      response_code_text = Text;
    }
  }


  public partial class WCP_Message
  {
    //private static Statistics m_wcp_messages = null; // new Statistics();
    //private static int m_wcp_tick = Misc.GetTickCount();

    public int m_created_tick;

    public WCP_MsgHeader MsgHeader;
    public IXml MsgContent;

    public Int64 TransactionId
    {
      get
      {
        switch ( MsgHeader.MsgType )
        {
          case WCP_MsgTypes.WCP_MsgReportPlay:
            return ( (WCP_MsgReportPlay) MsgContent ).TransactionId;

          case WCP_MsgTypes.WCP_MsgAddCredit:
            return ( (WCP_MsgAddCredit) MsgContent ).TransactionId;

          case WCP_MsgTypes.WCP_MsgMobileBankCardTransfer:
            return ((WCP_MsgMobileBankCardTransfer)MsgContent).TransactionId;

          case WCP_MsgTypes.WCP_MsgStartCardSession:
            return ((WCP_MsgStartCardSession)MsgContent).GetTransactionId();

          case WCP_MsgTypes.WCP_MsgCancelStartSession:
            return ((WCP_MsgCancelStartSession)MsgContent).TransactionId;

          case WCP_MsgTypes.WCP_MsgEndSession:
            return ((WCP_MsgEndSession)MsgContent).TransactionId;

          case WCP_MsgTypes.WCP_MsgMoneyEvent:
            return ((WCP_MsgMoneyEvent)MsgContent).TransactionId;

          case WCP_MsgTypes.WCP_TITO_MsgCreateTicket:
            return ((WCP_TITO_MsgCreateTicket)MsgContent).TransactionId;
 
          default:
            return 0;
        }
      }
    }

    private WCP_Message ()
    {
      m_created_tick = Misc.GetTickCount();
    }


    public static WCP_Message CreateMessage(String Xml)
    {
      WCP_Message _wcp_message;
      XmlReader   _xml_reader;
      System.IO.StringReader _txt_reader;
      bool _content_found;
      WCP_MsgHeader _wcp_header;
      

      _wcp_message = null;
      _xml_reader = null;
      _txt_reader = null;
      _content_found = false;


      try
      {
        _wcp_header = new WCP_MsgHeader();
        _txt_reader = new System.IO.StringReader(Xml);
        _xml_reader = XmlReader.Create(_txt_reader);
        
        //Loads the message header
        _wcp_header.LoadXml(_xml_reader);
        if (_wcp_header.MsgType == WCP_MsgTypes.WCP_MsgUnknown)
        {
          return null;
        }
        //Creates the message if the message header is correct.
        _wcp_message = CreateMessage(_wcp_header);

        //Put the xmlreader at the beginning of the Message Content
        while (_xml_reader.Read() && !_content_found)
        {
          if ((_xml_reader.Name == "WCP_MsgContent") && (_xml_reader.NodeType.Equals(XmlNodeType.Element)))
          {
            _content_found = true;
          }
        }
        if (_content_found)
        {
          _wcp_message.MsgContent.LoadXml(_xml_reader);
          if (_wcp_message.MsgContent == null)
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag not found: " + _wcp_message.MsgHeader.MsgType.ToString());
          }
        }
        else
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag not found: " + _wcp_message.MsgHeader.MsgType.ToString());
        }
      }
      catch (WCP_Exception wcp_ex)
      {
        throw wcp_ex;
      }
      catch (Exception ex)
      {
        // Format Error in message
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, ex.Message, ex);
      }
      finally 
      {
        //if (m_wcp_messages != null)
        //{
        //  if (_wcp_message != null)
        //  {
        //    m_wcp_messages.AddSample(_wcp_message.MsgHeader.MsgType.ToString(), 1, Xml.Length, 0);
        //  }
        //}

        if ( _xml_reader != null )
        {
          _xml_reader.Close ();
        }
        if ( _txt_reader != null )
        {
          _txt_reader.Close ();
          _txt_reader.Dispose ();
        }
      }
      return _wcp_message;
    }

    private static WCP_Message CreateMessage(WCP_MsgHeader Header)
    {
      WCP_Message _wcp_msg;
      IXml _xml_content;
      Assembly _assembly;
      String _wcp_msg_type_name;
      Type _target;
      WCP_MsgTypes _msg_type;


      _msg_type = Header.MsgType;

      _wcp_msg = new WCP_Message();
      _wcp_msg.MsgHeader = Header;

      if (_msg_type == WCP_MsgTypes.WCP_MsgReportPlay)
      {
        _wcp_msg.MsgContent = new WSI.WCP.WCP_MsgReportPlay();

        return _wcp_msg;
      }

      // The Power of Reflection
      _xml_content = null;

      _wcp_msg_type_name = "WSI.WCP." + _msg_type.ToString();

      _assembly = Assembly.GetExecutingAssembly();

      _target = _assembly.GetType(_wcp_msg_type_name);
      if (_target == null)
      {
        if ((_msg_type == WCP_MsgTypes.WCP_MsgSasMeters || _msg_type == WCP_MsgTypes.WCP_MsgSasMetersReply) ||
            (_msg_type == WCP_MsgTypes.WCP_MsgPlaySessionMeters || _msg_type == WCP_MsgTypes.WCP_MsgPlaySessionMetersReply))
        {
          _wcp_msg_type_name = "WSI.WCP.WCP_Messages.Meters." + _msg_type.ToString();
          _target = _assembly.GetType(_wcp_msg_type_name);
        }
      }

      Type[] _wcp_msg_types = new Type[0];
      ConstructorInfo _constructor = _target.GetConstructor(_wcp_msg_types);
      _xml_content = (IXml)_constructor.Invoke(null);

      _wcp_msg.MsgContent = _xml_content;

      return _wcp_msg;
    }
    

    public static WCP_Message CreateMessage(WCP_MsgTypes MsgType)
    {
      WCP_MsgHeader _wcp_header;
      
      _wcp_header = new WCP_MsgHeader ();
      _wcp_header.MsgType = MsgType;
      
      return CreateMessage (_wcp_header);
    }



    public String ToXml ()
    {
      String xml;
      String xml_header;
      String xml_content;

      xml_header = MsgHeader.ToXml ();
      xml_content = MsgContent.ToXml ();

      xml = "";
      xml += "<WCP_Message>";
      xml += xml_header;
      xml += "<WCP_MsgContent>";
      xml += xml_content;
      xml += "</WCP_MsgContent>";
      xml += "</WCP_Message>";


      //if (m_wcp_messages != null)
      //{
      //  m_wcp_messages.AddSample(MsgHeader.MsgType.ToString(), 1, xml.Length, 0);

      //  if (Misc.GetElapsedTicks(m_wcp_tick) >= 60000)
      //  {
      //    m_wcp_tick = Misc.GetTickCount();
      //    m_wcp_messages.Save("WCP");
      //    m_wcp_tick = Misc.GetTickCount();
      //  }
      //}

      return xml;
    }
  }

}
