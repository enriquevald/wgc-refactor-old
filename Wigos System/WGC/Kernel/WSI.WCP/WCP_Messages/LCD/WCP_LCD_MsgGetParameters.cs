//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_LCD_MsgGetParameters.cs
// 
//   DESCRIPTION: WCP_LCD_MsgGetParameters class
// 
//        AUTHOR: Joaquim Calero & Xavi Cots
// 
// CREATION DATE: 25-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author           Description
// ----------- ------           ----------------------------------------------------------
// 25-JUL-2014 JCA & XCD        First version.
// 19-JAN-2016 FJC & ACC & JCA  Task 8544:Error Params BonoPLAY: SAS-HOST nuevo --> WCP antiguo 
// 01-FEB-2016 FJC              Bug 8876:BonoPlay: Error en el Servicio WCP cuando tenemos carácteres especiales en el GP: Provider.001.Uri
// 22-FEB-2016 FJC              Product Backlog Item 9434:PimPamGo: Otorgar Premios.
// 04-APR-2016 FJC              PBItem 9105:BonoPlay: LCD: Cambios varios
// 21-SEP-2017 FJC              WIGOS-5280 Reserve EGM - Allow to reserve EGM in the LCD Intouch.
// 13-DIC-2017 FJC              Fixed Bug: WIGOS-7157 Terminal Draw - draw doesn't appear in LCD InTouch (GP)
//----------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using WSI.Common;


namespace WSI.WCP
{
  public class WCP_LCD_MsgGetParameters : IXml
  {
    public Int64 Transaction_Id = 0;
    public Int32 Terminal_Id = 0;

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_LCD_MsgGetParameters>");

      _sb.AppendLine("<TerminalId>");
      _sb.AppendLine(Terminal_Id.ToString());
      _sb.AppendLine("</TerminalId>");

      _sb.AppendLine("</WCP_LCD_MsgGetParameters>");

      return _sb.ToString();

    } //ToXml

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_LCD_MsgGetParameters")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_LCD_MsgGetParameters not found in content area");
      }

      while (((Xml.Name != "WCP_LCD_MsgGetParameters") || (Xml.NodeType != XmlNodeType.EndElement)) && !Xml.EOF)
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_LCD_MsgGetParametersReply EnabledFunctionalities element missing 2_1");
        }
        else
        {
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "TerminalId")
          {
            Terminal_Id = Int32.Parse(XML.GetValue(Xml, "TerminalId"));
            Xml.Read();    // Consume </TerminalId>
          }
        }
      }

    } //LoadXml

    #endregion //IXml Members
  } //WCP_LCD_MsgGetParameters

  public class WCP_LCD_MsgGetParametersReply : IXml, WCP_WKT.IResources
  {
    public List<Int32> Functions;
    public WCP_WKT.WKT_Images Images;
    public WCP_WKT.WKT_ResourceInfoList m_resources;
    public LCDMessages.LcdGameGateWayParams m_game_gateway_params;
    public LCDMessages.LcdFBParams m_fb_params;
    public LCDMessages.LcdTerminalDrawParams m_terminal_draw_params;
    public LCDMessages.LcdTerminalReserveParams m_terminal_reserve_params;
    public LCDMessages.LcdMobiBankParams m_mobibank_params;
    public Int32 _m_terminal_id;
    public string m_intouch_web_url;

    #region IXml Members


    public WCP_LCD_MsgGetParametersReply()
    {
      // GameGateway
      m_game_gateway_params = new LCDMessages.LcdGameGateWayParams();
      m_game_gateway_params.PartnerId = String.Empty;
      m_game_gateway_params.ProviderName = string.Empty;
      m_game_gateway_params.Url = String.Empty;
      m_game_gateway_params.UrlTest = String.Empty;

      // FB
      m_fb_params = new LCDMessages.LcdFBParams();
      m_fb_params.Enabled = 0;
      m_fb_params.PinRequest = 0;
      m_fb_params.HostAddress = String.Empty;
      m_fb_params.Url = String.Empty;
      m_fb_params.Login = String.Empty;
      m_fb_params.Password = String.Empty;

      //Intouch Web
      m_intouch_web_url = String.Empty;

      // Terminal Draw
      m_terminal_draw_params = new LCDMessages.LcdTerminalDrawParams();
      m_terminal_draw_params.Enabled = 0;

      // Terminal Reserve
      m_terminal_reserve_params = new LCDMessages.LcdTerminalReserveParams();
      m_terminal_reserve_params.Enabled = 0;

      // MobiBank
      m_mobibank_params = new LCDMessages.LcdMobiBankParams();
      m_mobibank_params.Enabled = 0;

    }

    public string ToXml()
    {
      StringBuilder _sb;

      // Kiosc general parameters contain
      //    - Kiosk Functionalities (only enabled)
      //    - Kiosk Customized Images
      //    - Customized Messages
      //    - Operation schedule interval

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_LCD_MsgGetParametersReply>");

      //    - Kiosk Functionalities (only enabled)
      _sb.AppendLine("<EnabledFunctionalities>");
      if (Functions != null)
      {
        foreach (Int32 _id_function in Functions)
        {
          _sb.Append("<FunctionId>" + _id_function.ToString() + "</FunctionId>");
        }
      }
      _sb.AppendLine("</EnabledFunctionalities>");

      //    - Kiosk Customized Images
      _sb.AppendLine("<CustomizedImages>");
      if (Images != null)
      {
        _sb.Append(Images.ToXml());
      }
      _sb.AppendLine("</CustomizedImages>");

      _sb.AppendLine("<m_resources>");
      if (m_resources != null)
      {
        _sb.Append(m_resources.ToXml());
      }
      _sb.AppendLine("</m_resources>");

      //    - InTouchWebURL
      _sb.AppendLine("<InTouchWeb>");
      _sb.Append("<URL>" + m_intouch_web_url + "</URL>");
      _sb.AppendLine("</InTouchWeb>");

      //    - SpecialParams
      _sb.AppendLine("<SpecialParams>");

      //    - GameGateWay
      //_sb.AppendLine("<GameGatewayParams>");
      _sb.Append("<GameGatewayEnabled>" + m_game_gateway_params.GameGateway.ToString() + "</GameGatewayEnabled>");
      _sb.Append("<GameGatewayPartnerId>" + m_game_gateway_params.PartnerId.ToString() + "</GameGatewayPartnerId>");
      _sb.Append("<GameGatewayProviderName>" + m_game_gateway_params.ProviderName.ToString() + "</GameGatewayProviderName>");

      //01-FEB-2016 FJC Bug 8876:BonoPlay: Error en el Servicio WCP cuando tenemos carácteres especiales en el GP: Provider.001.Uri
      if (!XML.TryParse("<test>" + m_game_gateway_params.Url.ToString() + "</test>"))
      {
        m_game_gateway_params.Url = String.Empty;

        Log.Error("WCP_LCD_MsgGetParametersReply.ToXml() -GP GameGateway.Provider.{N}.Uri has a incorrect format-");
      }

      _sb.Append("<GameGatewayUrl>" + m_game_gateway_params.Url.ToString() + "</GameGatewayUrl>");
      _sb.Append("<GameGatewayReservedCredit>" + m_game_gateway_params.ReservedCredit.ToString() + "</GameGatewayReservedCredit>");
      _sb.Append("<GameGatewayAwardPrizes>" + m_game_gateway_params.AwardPrizes.ToString() + "</GameGatewayAwardPrizes>");
      _sb.Append("<GameGatewayUrlTest>" + m_game_gateway_params.UrlTest.ToString() + "</GameGatewayUrlTest>");
      //_sb.AppendLine("</GameGatewayParams>");

      //    - FB
      _sb.Append("<FBEnabled>" + m_fb_params.Enabled.ToString() + "</FBEnabled>");
      _sb.Append("<FBPinRequest>" + m_fb_params.PinRequest.ToString() + "</FBPinRequest>");
      _sb.Append("<FBLogin>" + m_fb_params.Login.ToString() + "</FBLogin>");
      _sb.Append("<FBPassword>" + m_fb_params.Password.ToString() + "</FBPassword>");

      if (!XML.TryParse("<test>" + m_fb_params.HostAddress.ToString() + "</test>"))
      {
        m_fb_params.HostAddress = String.Empty;

        Log.Error("WCP_LCD_MsgGetParametersReply.ToXml() -GP FB.HostAddress has a incorrect format-");
      }
      _sb.Append("<FBHostAddress>" + m_fb_params.HostAddress.ToString() + "</FBHostAddress>");

      if (!XML.TryParse("<test>" + m_fb_params.Url.ToString() + "</test>"))
      {
        m_fb_params.Url = String.Empty;

        Log.Error("WCP_LCD_MsgGetParametersReply.ToXml() - GP FB.Web.Url has a incorrect format-");
      }
      _sb.Append("<FBUrl>" + m_fb_params.Url.ToString() + "</FBUrl>");

      //    - Terminal Draw
      _sb.Append("<TerminalDrawEnabled>" + m_terminal_draw_params.Enabled.ToString() + "</TerminalDrawEnabled>");

      //    - Terminal Reserve
      _sb.Append("<TerminalReserveEnabled>" + m_terminal_reserve_params.Enabled.ToString() + "</TerminalReserveEnabled>");

      //    - MobiBank
      _sb.Append("<MobiBankEnabled>" + m_mobibank_params.Enabled.ToString() + "</MobiBankEnabled>");

      _sb.AppendLine("</SpecialParams>");

      _sb.AppendLine("</WCP_LCD_MsgGetParametersReply>");

      return _sb.ToString();


    } //ToXml

    public void LoadXml(XmlReader Xml)
    {
      Functions = new List<int>();
      Images = new WCP_WKT.WKT_Images();
      m_resources = new WCP_WKT.WKT_ResourceInfoList();

      m_game_gateway_params = new LCDMessages.LcdGameGateWayParams();
      m_fb_params = new LCDMessages.LcdFBParams();

      if (Xml.Name != "WCP_LCD_MsgGetParametersReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_LCD_MsgGetParametersReply not found in content area");
      }

      //    Move reader to <EnabledFunctionalities>
      while (Xml.Name != "EnabledFunctionalities")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_LCD_MsgGetParametersReply EnabledFunctionalities element missing 1");
        }
      }
      while (!((Xml.NodeType.Equals(XmlNodeType.EndElement) || Xml.IsEmptyElement) && Xml.Name == "EnabledFunctionalities"))
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_LCD_MsgGetParametersReply EnabledFunctionalities element missing 2");
        }
        else
        {
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "FunctionId")
          {
            Functions.Add(Int32.Parse(XML.GetValue(Xml, "FunctionId")));
            Xml.Read();    // Consume </FunctionId>
          }
        }
      }

      //    Move reader to <CustomizedImages>
      while (Xml.Name != "CustomizedImages")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_LCD_MsgGetParametersReply CustomizedImages element missing 1");
        }
      }
      Images.LoadXml(Xml);

      //    Move reader to <m_resources>
      while (Xml.Name != "m_resources")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_LCD_MsgGetParametersReply m_resources Images element missing");
        }
      }

      // Load Reusources
      m_resources.LoadXml(Xml);

      Xml.Read();
      Xml.Read();

      // InTouchWeb
      if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "InTouchWeb")
      {
        // URL
        m_intouch_web_url = XML.ReadTagValue(Xml, "URL");
        Xml.Read();
      }

      //Task 8544:Error Params BonoPLAY: SAS-HOST nuevo --> WCP antiguo (FJC & ACC & JCA)
      Xml.Read();
      Xml.Read();

      if (Xml.Name != "SpecialParams")
      {
        return;
      }

      while (!((Xml.NodeType.Equals(XmlNodeType.EndElement) || Xml.IsEmptyElement) && Xml.Name == "SpecialParams"))
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_LCD_MsgGetParametersReply SpecialParams element missing");
        }
        else
        {
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "GameGatewayEnabled")
          {
            m_game_gateway_params.GameGateway = Int32.Parse(XML.GetValue(Xml, "GameGatewayEnabled"));
            Xml.Read();    // Consume </GameGatewayEnabled>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "GameGatewayPartnerId")
          {
            m_game_gateway_params.PartnerId = XML.GetValue(Xml, "GameGatewayPartnerId");
            Xml.Read();    // Consume </GameGatewayPartnerId>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "GameGatewayProviderName")
          {
            m_game_gateway_params.ProviderName = XML.GetValue(Xml, "GameGatewayProviderName");
            Xml.Read();    // Consume </GameGatewayProviderName>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "GameGatewayUrl")
          {
            m_game_gateway_params.Url = XML.GetValue(Xml, "GameGatewayUrl");
            Xml.Read();    // Consume </GameGatewayUrl>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "GameGatewayReservedCredit")
          {
            m_game_gateway_params.ReservedCredit = Int32.Parse(XML.GetValue(Xml, "GameGatewayReservedCredit"));
            Xml.Read();    // Consume </GameGatewayReservedCredit>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "GameGatewayAwardPrizes")
          {
            m_game_gateway_params.AwardPrizes = Int32.Parse(XML.GetValue(Xml, "GameGatewayAwardPrizes"));
            Xml.Read();    // Consume </GameGatewayAwardPrizes>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "GameGatewayUrlTest")
          {
            m_game_gateway_params.UrlTest = XML.GetValue(Xml, "GameGatewayUrlTest");
            Xml.Read();    // Consume </GameGatewayUrlTest>
          }

          // FB
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "FBEnabled")
          {
            m_fb_params.Enabled = Int32.Parse(XML.GetValue(Xml, "FBEnabled"));
            Xml.Read();    // Consume </FBEnabled>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "FBPinRequest")
          {
            m_fb_params.PinRequest = Int32.Parse(XML.GetValue(Xml, "FBPinRequest"));
            Xml.Read();    // Consume </FBPinRequest>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "FBHostAddress")
          {
            m_fb_params.HostAddress = XML.GetValue(Xml, "FBHostAddress");
            Xml.Read();    // Consume </FBHostAddress>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "FBUrl")
          {
            m_fb_params.Url = XML.GetValue(Xml, "FBUrl");
            Xml.Read();    // Consume </FBUrl>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "FBLogin")
          {
            m_fb_params.Login = XML.GetValue(Xml, "FBLogin");
            Xml.Read();    // Consume </FBLogin>
          }
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "FBPassword")
          {
            m_fb_params.Password = XML.GetValue(Xml, "FBPassword");
            Xml.Read();    // Consume </FBPassword>
          }

          // Terminal Draw
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "TerminalDrawEnabled")
          {
            m_terminal_draw_params.Enabled = Int32.Parse(XML.GetValue(Xml, "TerminalDrawEnabled"));
            Xml.Read();    // Consume </TerminalDrawEnabled>
          }

          // Terminal Reserve
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "TerminalReserveEnabled")
          {
            m_terminal_reserve_params.Enabled = Int32.Parse(XML.GetValue(Xml, "TerminalReserveEnabled"));
            Xml.Read();    // Consume </TerminalReserveEnabled>
          }

          // MobiBank
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "MobiBankEnabled")
          {
            m_mobibank_params.Enabled = Int32.Parse(XML.GetValue(Xml, "MobiBankEnabled"));
            Xml.Read();    // Consume </MobiBankEnabled>
          }
        }
      }
     
    } //LoadXml

    #endregion //IXml Members


    #region IResources Members

    public WCP_WKT.WKT_ResourceInfoList Resources
    {
      get
      {
        return m_resources;
      }
      set
      {
        m_resources = value;
      }
    }

    #endregion


  } //WCP_LCD_MsgGetParametersReply 
}