//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgPrintRechargesList.cs
// 
//   DESCRIPTION: WCP_WKT_MsgPrintRechargesList class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 29-OCT-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-OCT-2012 JMM    First version.
// 22-FEB-2016 FJC    Product Backlog Item 9434:PimPamGo: Otorgar Premios.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_LCD_MsgGetPromoBalance : IXml, WCP_WKT.IExtendedPlayer
  {
    public WCP_WKT.WKT_Player Player;
    public Int64 AccountId = 0;
    public Int16 GameGatewayPromoBalanceAction = (Int16)MultiPromos.ENUM_GAME_GATEWAY_PROMO_BALANCE_ACTION.GAME_GATEWAY_PROMO_BALANCE_ACTION_NONE;
    

    public WCP_LCD_MsgGetPromoBalance()
    {
      this.Player = new WCP_WKT.WKT_Player();
    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_LCD_MsgGetPromoBalance>");
      _sb.Append("<Pin>" + XML.StringToXmlValue(Player.Pin) + "</Pin>");
      _sb.Append("<Trackdata>" + XML.StringToXmlValue(Player.Trackdata) + "</Trackdata>");
      _sb.Append("<AccountId>" + AccountId.ToString() + "</AccountId>");
      _sb.Append("<GameGatewayPromoBalanceAction>" + GameGatewayPromoBalanceAction.ToString() + "</GameGatewayPromoBalanceAction>");

      _sb.AppendLine("</WCP_LCD_MsgGetPromoBalance>");
      return _sb.ToString();

    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      Int16 _aux_int16_value;
      Int64 _aux_int64_value;

      if (reader.Name != "WCP_LCD_MsgGetPromoBalance")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgPrintRechargesList not found in content area");
      }

      Player.Pin = XML.GetValue(reader, "Pin");
      Player.Trackdata = XML.GetValue(reader, "Trackdata");

      if (Int64.TryParse(XML.ReadTagValue(reader, "AccountId", true), out _aux_int64_value))
      {
        AccountId = _aux_int64_value;
      }

      if (Int16.TryParse(XML.ReadTagValue(reader, "GameGatewayPromoBalanceAction", true), out _aux_int16_value))
      {
        GameGatewayPromoBalanceAction = _aux_int16_value;
      }

      
    } // LoadXml


    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get
      {
        return this.Player;
      }
      set
      {
        this.Player = value;
      }
    }

    #endregion
  } // WCP_LCD_MsgGetPromoBalanceReply

  public class WCP_LCD_MsgGetPromoBalanceReply : IXml
  {
    public MultiPromos.PromoBalance PromoBalance = new MultiPromos.PromoBalance();
    public Decimal GameGateWayAwardedPrizeAmount = 0;

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_LCD_MsgGetPromoBalanceReply>");
      _sb.AppendLine("<Points>" + (Convert.ToInt64(Math.Floor(PromoBalance.Points))).ToString() + "</Points>");
      _sb.AppendLine("<Balance>");
      _sb.AppendLine(PromoBalance.Balance.ToXml());
      _sb.AppendLine("</Balance>");
      // FJC GrantPrize
      _sb.AppendLine("<GameGatewayGrantedPrize>" + ((Int64)(GameGateWayAwardedPrizeAmount * 100)).ToString() + "</GameGatewayGrantedPrize>");

      _sb.AppendLine("</WCP_LCD_MsgGetPromoBalanceReply>");

      return _sb.ToString();

    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      Int64 _aux_value;
      
      if (reader.Name != "WCP_LCD_MsgGetPromoBalanceReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgPrintRechargesListReply not found in content area");
      }
      else
      {
        if (Int64.TryParse(XML.GetValue(reader, "Points", true), out _aux_value))
        {
          PromoBalance.Points = Convert.ToDecimal(_aux_value);
        }
        PromoBalance.Balance.LoadXml(reader);

        // FJC 
        if (Int64.TryParse(XML.ReadTagValue(reader, "GameGatewayGrantedPrize", true), out _aux_value))
        {
          GameGateWayAwardedPrizeAmount = ((Decimal)_aux_value) / 100;
        }

      }
    } // LoadXml

  } // WCP_LCD_MsgGetPromoBalanceReply

} // namespace WSI.WCP
