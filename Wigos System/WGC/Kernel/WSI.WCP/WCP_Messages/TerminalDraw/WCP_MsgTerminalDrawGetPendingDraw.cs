﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgTerminalDrawGetPendingDraw.cs
// 
//   DESCRIPTION: This Message, Gets Pending TerminalDraws for one Account
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 02-JAN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JAN-2017 FJC    First release.
// 30-MAY-2017 FJC    PBI 27761:Sorteo de máquina. Modificiones en la visualización de las pantallas (LCD y InTouch)). 
// 22-AUG-2017 FJC    Fixed Defect: WIGOS-4649 Terminal Draw: The Draw does not appear in the terminal and continue with start card session
// 29-AUG-2017 FJC    WIGOS-4771 Terminal Draw. Include GameId for each recharge
// 04-SEP-2017 FJC    PBI 29532:Terminal Draw. Apply retrocompatibility (18.315 & 18.319)
// 18-OCT-2017 FJC    WIGOS-5928 Terminal draw does not work neither in LCD nor 4 lines
// 16-FEB-2018 FJC    WIGOS-8082 [Ticket #12237] Sorteo en Terminal
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using WSI.Common.TerminalDraw;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.WCP
{
  public class WCP_MsgTerminalDrawGetPendingDraw : IXml
  {
    public Int64 AccountId;
    public Int32 TerminalId;

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgTerminalDrawGetPendingDraw>");
      _sb.AppendLine("<AccountId>" + AccountId.ToString() + "</AccountId>");
      _sb.AppendLine("<TerminalId>" + TerminalId.ToString() + "</TerminalId>");
      _sb.AppendLine("</WCP_MsgTerminalDrawGetPendingDraw>");

      return _sb.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (!Reader.Name.Equals("WCP_MsgTerminalDrawGetPendingDraw"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgGetPendingTerminalDraw not found in content area");
      }
      Int64.TryParse(XML.ReadTagValue(Reader, "AccountId", true), out AccountId);
    }
  }

  public class WCP_MsgTerminalDrawGetPendingDrawReply : IXml
  {
    public TerminalDrawAccountEntity PendingDraw;

    #region "Constructor"
    public WCP_MsgTerminalDrawGetPendingDrawReply()
    {
      PendingDraw = new TerminalDrawAccountEntity();
    }
    #endregion

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgTerminalDrawGetPendingDrawReply>");
      _sb.AppendLine("<AccountId>" + PendingDraw.AccountId.ToString() + "</AccountId>");

      _sb.AppendLine("<Games>");
      if (PendingDraw.Games.Count > 0)
      {
        foreach (TerminalDrawGamesEntity _pending_games in PendingDraw.Games)
        {
          _sb.AppendLine("  <Game>");
          _sb.AppendLine("    <GameId>-1</GameId>");
          _sb.AppendLine("    <GameUrl>" + _pending_games.GameUrl + "</GameUrl>");

          // First Screen
          _sb.AppendLine("    <FirstScreenMessageLine0>" + _pending_games.FirstScreenMessageLine0 + "</FirstScreenMessageLine0>");
          _sb.AppendLine("    <FirstScreenMessageLine1>" + _pending_games.FirstScreenMessageLine1 + "</FirstScreenMessageLine1>");
          _sb.AppendLine("    <FirstScreenMessageLine2>" + _pending_games.FirstScreenMessageLine2 + "</FirstScreenMessageLine2>");
          _sb.AppendLine("    <FirstScreenTimeOut>" + _pending_games.FirstScreenTimeOut + "</FirstScreenTimeOut>");
          _sb.AppendLine("    <TimeOutExpiresParticipateInDraw>" + _pending_games.TimeoutExpiresParticipateInDraw.ToString() + "</TimeOutExpiresParticipateInDraw>");
          _sb.AppendLine("    <ForceParticipateInDraw>" + _pending_games.ForceParticipateInDraw.ToString() + "</ForceParticipateInDraw>");

          // Second Screen
          _sb.AppendLine("    <SecondScreenMessageLine0>" + _pending_games.SecondScreenMessageLine0 + "</SecondScreenMessageLine0>");
          _sb.AppendLine("    <SecondScreenMessageLine1>" + _pending_games.SecondScreenMessageLine1 + "</SecondScreenMessageLine1>");
          _sb.AppendLine("    <SecondScreenMessageLine2>" + _pending_games.SecondScreenMessageLine2 + "</SecondScreenMessageLine2>");
          _sb.AppendLine("    <SecondScreenMessageLine3>" + _pending_games.SecondScreenMessageLine3 + "</SecondScreenMessageLine3>");
          _sb.AppendLine("    <SecondScreenTimeOut>" + _pending_games.SecondScreenTimeOut + "</SecondScreenTimeOut>");
          _sb.AppendLine("    <ForceExecuteDraw>" + _pending_games.ForceExecuteDraw + "</ForceExecuteDraw>");

          // PlaySession Id
          _sb.AppendLine("    <PlaySessionId>" + _pending_games.PlaySessionId.ToString() + "</PlaySessionId>");

          // Bet Amount
          _sb.AppendLine("    <TotalBetAmount>" + ((Int64)(_pending_games.TotalBetAmount * 100)).ToString() + "</TotalBetAmount>");

          // Balance
          _sb.AppendLine("    <BalanceAmount>" + ((Int64)(_pending_games.BalanceAmount * 100)).ToString() + "</BalanceAmount>");

          // Recharges (Max 20 foreach game)
          foreach (TerminalDrawRechargeEntity _recharges in _pending_games.Recharges)
          {
            _sb.AppendLine("  <Recharges>");
            _sb.AppendLine("      <OperationId>" + _recharges.OperationId.ToString() + "</OperationId>");
            _sb.AppendLine("      <AmountTotalCashIn>" + ((Int64)(_recharges.AmountTotalCashIn) * 100).ToString() + "</AmountTotalCashIn>");
            _sb.AppendLine("      <AmountReBet>" + ((Int64)(_recharges.AmountReBet) * 100).ToString() + "</AmountReBet>");
            _sb.AppendLine("      <AmountNrBet>" + ((Int64)(_recharges.AmountNrBet) * 100).ToString() + "</AmountNrBet>");
            _sb.AppendLine("      <AmountPointsBet>" + _recharges.AmountPointsBet.ToString() + "</AmountPointsBet>");
            _sb.AppendLine("      <GameId>" + _recharges.GameId.ToString() + "</GameId>");
            _sb.AppendLine("      <GameType>" + _recharges.GameType.ToString() + "</GameType>");
            _sb.AppendLine("  </Recharges>");
          }

          _sb.AppendLine("  </Game>");
        }
      }
      _sb.AppendLine("</Games>");

      _sb.AppendLine("</WCP_MsgTerminalDrawGetPendingDrawReply>");

      return _sb.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      Boolean _force_execute_draw;
      WSI.Common.TerminalDraw.Persistence.RechargeData _recharge_data;

      _recharge_data = new WSI.Common.TerminalDraw.Persistence.RechargeData();


      if (!Reader.Name.Equals("WCP_MsgTerminalDrawGetPendingDrawReply"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgTerminalDrawGetPendingDrawReply not found in content area");
      }

      Reader.Read();

      _recharge_data.AccountId = Int64.Parse(XML.ReadTagValue(Reader, "AccountId"));
      PendingDraw.AccountId = _recharge_data.AccountId;

      while ((Reader.Name != "Games") || (Reader.NodeType != XmlNodeType.EndElement))
      {
        if ((Reader.Name == "GameUrl") && (Reader.NodeType == XmlNodeType.Element))
        {
          _recharge_data.AmountReBet = 0;
          _recharge_data.AmountTotalCashIn = 0;
          _recharge_data.AmountNrBet = 0;
          _recharge_data.AmountPointsBet = 0;

          _recharge_data.GameUrl = XML.ReadTagValue(Reader, "GameUrl");

          // First Screen
          _recharge_data.FirstScreenMessageLine0 = XML.ReadTagValue(Reader, "FirstScreenMessageLine0");
          _recharge_data.FirstScreenMessageLine1 = XML.ReadTagValue(Reader, "FirstScreenMessageLine1");
          _recharge_data.FirstScreenMessageLine2 = XML.ReadTagValue(Reader, "FirstScreenMessageLine2");
          _recharge_data.FirstScreenTimeOut = Int64.Parse(XML.ReadTagValue(Reader, "FirstScreenTimeOut"));
          _recharge_data.TimeOutExpiresParticipateInDraw = Boolean.Parse(XML.ReadTagValue(Reader, "TimeOutExpiresParticipateInDraw"));
          _recharge_data.ForceParticipateInDraw = Boolean.Parse(XML.ReadTagValue(Reader, "ForceParticipateInDraw"));

          //Second Screen
          _recharge_data.SecondScreenMessageLine0 = XML.ReadTagValue(Reader, "SecondScreenMessageLine0");
          _recharge_data.SecondScreenMessageLine1 = XML.ReadTagValue(Reader, "SecondScreenMessageLine1");
          _recharge_data.SecondScreenMessageLine2 = XML.ReadTagValue(Reader, "SecondScreenMessageLine2");
          _recharge_data.SecondScreenMessageLine3 = XML.ReadTagValue(Reader, "SecondScreenMessageLine3");
          _recharge_data.SecondScreenTimeOut = Int64.Parse(XML.ReadTagValue(Reader, "SecondScreenTimeOut"));

          Reader.Read();
          Reader.Read();
          if (Reader.Name == "ForceExecuteDraw")
          {
            Boolean.TryParse(XML.ReadTagValue(Reader, "ForceExecuteDraw"), out _force_execute_draw);
            _recharge_data.ForceExecuteDraw = _force_execute_draw;
          }

          //PlaySession Id
          _recharge_data.PlaySessionId = Int64.Parse(XML.ReadTagValue(Reader, "PlaySessionId"));

          // Bet Amount
          _recharge_data.TotalBetAmount = Int64.Parse(XML.ReadTagValue(Reader, "TotalBetAmount"));

          // Balance Amount
          _recharge_data.BalanceAmount = Int64.Parse(XML.ReadTagValue(Reader, "BalanceAmount"));

          Reader.Read();

          while (Reader.Name != "Game")
          {
            if ((Reader.Name == "OperationId") && (Reader.NodeType == XmlNodeType.Element))
            {
              _recharge_data.OperationId = Int64.Parse(XML.ReadTagValue(Reader, "OperationId"));
            }
            if ((Reader.Name == "AmountTotalCashIn") && (Reader.NodeType == XmlNodeType.Element))
            {
              _recharge_data.AmountTotalCashIn = Int64.Parse(XML.ReadTagValue(Reader, "AmountTotalCashIn"));

            }
            if ((Reader.Name == "AmountReBet") && (Reader.NodeType == XmlNodeType.Element))
            {
              _recharge_data.AmountReBet = Int64.Parse(XML.ReadTagValue(Reader, "AmountReBet"));
            }
            if ((Reader.Name == "GameId") && (Reader.NodeType == XmlNodeType.Element))
            {
              _recharge_data.GameId = Int64.Parse(XML.ReadTagValue(Reader, "GameId"));
            }
            if ((Reader.Name == "GameType") && (Reader.NodeType == XmlNodeType.Element))
            {
              _recharge_data.GameType = Int64.Parse(XML.ReadTagValue(Reader, "GameType"));
            }
            if (Reader.Name == "Recharges" && Reader.NodeType == XmlNodeType.EndElement)
            {
              _recharge_data.AmountTotalCashIn /= 100;
              _recharge_data.AmountReBet /= 100;
              _recharge_data.AmountNrBet /= 100;

              PendingDraw.AddRecharge(_recharge_data);
            }

            Reader.Read();
          }
                                  
        }
        Reader.Read();
      }
    }
  }

}
