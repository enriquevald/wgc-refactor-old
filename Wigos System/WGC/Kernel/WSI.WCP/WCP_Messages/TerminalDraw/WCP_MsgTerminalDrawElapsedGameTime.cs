﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgTerminalDrawGetPendingDraw.cs
// 
//   DESCRIPTION: WCP_MsgTerminalDrawGetPendingDraw class
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 02-JAN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JAN-2017 FJC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using WSI.Common.TerminalDraw;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.WCP
{
  public class WCP_MsgTerminalDrawGameTimeElapsed : IXml
  {
    public Int64 PlaySessionId;
    public Int64 GameTimeElapsed;
    public Boolean ClosePlaySession;

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgTerminalDrawGameTimeElapsed>");
      _sb.AppendLine("<PlaySessionId>" + PlaySessionId.ToString() + "</PlaySessionId>");
      _sb.AppendLine("<TerminalDrawGametimeElapsed>" + GameTimeElapsed.ToString() + "</TerminalDrawGametimeElapsed>");
      _sb.AppendLine("<ClosePlaySession>" + ClosePlaySession.ToString() + "</ClosePlaySession>");
      _sb.AppendLine("</WCP_MsgTerminalDrawGameTimeElapsed>");

      return _sb.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (!Reader.Name.Equals("WCP_MsgTerminalDrawGameTimeElapsed"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgTerminalDrawGameTimeElapsed not found in content area");
      }
      Int64.TryParse(XML.ReadTagValue(Reader, "PlaySessionId", true), out PlaySessionId);
      Int64.TryParse(XML.ReadTagValue(Reader, "TerminalDrawGametimeElapsed", true), out GameTimeElapsed);
      ClosePlaySession = Boolean.Parse(XML.ReadTagValue(Reader, "ClosePlaySession"));
    }
  }
               
  public class WCP_MsgTerminalDrawGameTimeElapsedReply : IXml
  {

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgTerminalDrawGameTimeElapsedReply>");
      _sb.AppendLine("</WCP_MsgTerminalDrawGameTimeElapsedReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgTerminalDrawGameTimeElapsedReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgTerminalDrawGameTimeElapsedReply not found in content area");
      }
    } // LoadXml
  }
}