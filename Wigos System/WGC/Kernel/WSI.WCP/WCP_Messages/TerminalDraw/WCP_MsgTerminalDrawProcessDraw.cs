﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgTerminalDrawGetPendingDraw.cs
// 
//   DESCRIPTION: WCP_MsgTerminalDrawGetPendingDraw class
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 02-JAN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JAN-2017 FJC    First release.
// 21-FEB-2017 ETP    Code standardization
// 30-MAY-2017 FJC    PBI 27761:Sorteo de máquina. Modificiones en la visualización de las pantallas (LCD y InTouch)). 
// 22-AUG-2017 FJC    Fixed Defect: WIGOS-4649 Terminal Draw: The Draw does not appear in the terminal and continue with start card session
// 29-AUG-2017 FJC    WIGOS-4771 Terminal Draw. Include GameId for each recharge
// 04-SEP-2017 FJC    PBI 29532:Terminal Draw. Apply retrocompatibility (18.315 & 18.319)
// 18-OCT-2017 FJC    WIGOS-5928 Terminal draw does not work neither in LCD nor 4 lines
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using WSI.Common.TerminalDraw;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.WCP
{
  public class WCP_MsgTerminalDrawProcessDraw : IXml
  {
    #region " Member "

    private TerminalDrawAccountEntity m_process_pending_draw;

    #endregion " Member "

    #region " Properties "

    public TerminalDrawAccountEntity ProcessPendingDraw
    {
      get { return m_process_pending_draw; }
      set { m_process_pending_draw = value; }
    }

    #endregion " Properties "

    #region " Constructor "

    /// <summary>
    /// Constructor
    /// </summary>
    public WCP_MsgTerminalDrawProcessDraw()
    {
      ProcessPendingDraw = new TerminalDrawAccountEntity();

    }  // WCP_MsgTerminalDrawProcessDraw

    #endregion " Constructor "

    #region " Public Methods "

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgTerminalDrawProcessDraw>");
      _sb.AppendLine("<AccountId>" + ProcessPendingDraw.AccountId.ToString() + "</AccountId>");
      _sb.AppendLine("<TerminalId>" + ProcessPendingDraw.TerminalId.ToString() + "</TerminalId>");

      foreach (TerminalDrawGamesEntity _pending_games in ProcessPendingDraw.Games)
      {
        _sb.AppendLine("<Games>");
        _sb.AppendLine("  <Game>");
        _sb.AppendLine("    <GameId>-1</GameId>");
        _sb.AppendLine("    <PlaySessionId>" + _pending_games.PlaySessionId.ToString() + "</PlaySessionId>");

        foreach (TerminalDrawRechargeEntity _recharges in _pending_games.Recharges)
        {
          _sb.AppendLine("  <Recharges>");
          _sb.AppendLine("      <OperationId>" + _recharges.OperationId.ToString() + "</OperationId>");
          _sb.AppendLine("      <AmountTotalCashIn>" + ((Int64)(_recharges.AmountTotalCashIn) * 100).ToString() + "</AmountTotalCashIn>");
          _sb.AppendLine("      <AmountReBet>" + ((Int64)(_recharges.AmountReBet) * 100).ToString() + "</AmountReBet>");
          _sb.AppendLine("      <AmountNrBet>" + ((Int64)(_recharges.AmountNrBet) * 100).ToString() + "</AmountNrBet>");
          _sb.AppendLine("      <AmountPointsBet>" + _recharges.AmountPointsBet.ToString() + "</AmountPointsBet>");
          _sb.AppendLine("      <GameId>" + _recharges.GameId.ToString() + "</GameId>");
          _sb.AppendLine("  </Recharges>");
        }
        
        _sb.AppendLine("  </Game>");
        _sb.AppendLine("</Games>");
      }
      _sb.AppendLine("</WCP_MsgTerminalDrawProcessDraw>");

      return _sb.ToString();
    } // ToXml

    /// <summary>
    /// Load Xml from a reader
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      Int64 _amount_re_total = 0;
      Int64 _amount_nr_total = 0;
      Int64 _amount_total_cash_in = 0;
      Int64 _game_id = 0;

      WSI.Common.TerminalDraw.Persistence.RechargeData _recharge_data;

      _recharge_data = new WSI.Common.TerminalDraw.Persistence.RechargeData();

      if (!Reader.Name.Equals("WCP_MsgTerminalDrawProcessDraw"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgTerminalDrawProcessDraw not found in content area");
      }

      Reader.Read();
      _recharge_data.AccountId = Int64.Parse(XML.ReadTagValue(Reader, "AccountId"));

      Reader.Read();
      _recharge_data.TerminalId = Int32.Parse(XML.ReadTagValue(Reader, "TerminalId"));

      while ((Reader.Name != "Games") || (Reader.NodeType != XmlNodeType.EndElement))
      {
        if ((Reader.Name == "PlaySessionId") && (Reader.NodeType == XmlNodeType.Element))
        {
          _recharge_data.PlaySessionId = Int64.Parse(XML.ReadTagValue(Reader, "PlaySessionId"));
          
          _recharge_data.AmountReBet = 0;
          _recharge_data.AmountTotalCashIn = 0;
          _recharge_data.OperationId = 0;

          Reader.Read();
          while (Reader.Name != "Game")
          {
            if ((Reader.Name == "OperationId") && (Reader.NodeType == XmlNodeType.Element))
            {
              _recharge_data.OperationId = Int64.Parse(XML.ReadTagValue(Reader, "OperationId"));
            }
            if ((Reader.Name == "AmountTotalCashIn") && (Reader.NodeType == XmlNodeType.Element))
            {
              _amount_total_cash_in = Int64.Parse(XML.ReadTagValue(Reader, "AmountTotalCashIn"));
            }
            if ((Reader.Name == "AmountReBet") && (Reader.NodeType == XmlNodeType.Element))
            {
              _amount_re_total = Int64.Parse(XML.ReadTagValue(Reader, "AmountReBet"));
            }
            if ((Reader.Name == "AmountNrBet") && (Reader.NodeType == XmlNodeType.Element))
            {
              _amount_nr_total = Int64.Parse(XML.ReadTagValue(Reader, "AmountNrBet"));
            }
            if ((Reader.Name == "GameId") && (Reader.NodeType == XmlNodeType.Element))
            {
              _game_id = Int64.Parse(XML.ReadTagValue(Reader, "GameId"));
            }

            if (Reader.Name == "Recharges" && Reader.NodeType == XmlNodeType.EndElement)
            {
              _recharge_data.AmountTotalCashIn = (_amount_total_cash_in / 100);
              _recharge_data.AmountReBet = (_amount_re_total / 100);
              _recharge_data.AmountNrBet = (_amount_nr_total / 100);

              ProcessPendingDraw.AddRecharge(_recharge_data);
            }

            Reader.Read(); 
          }
        }

        Reader.Read();
      }

    }  // LoadXml

    #endregion " Public Methods "

  } // WCP_MsgTerminalDrawProcessDraw

  public class WCP_MsgTerminalDrawProcessDrawReply : IXml
  {
    #region " Member "

    private TerminalDrawPrizePlanGames m_prize_plan_games;

    #endregion " Member "

    #region " Properties "

    public TerminalDrawPrizePlanGames PrizePlanGames
    {
      get { return m_prize_plan_games; }
      set { m_prize_plan_games = value; }
  }

    #endregion " Properties "

    #region " Constructor "

    public WCP_MsgTerminalDrawProcessDrawReply()
    {
      PrizePlanGames = new TerminalDrawPrizePlanGames();
      
    } // WCP_MsgTerminalDrawProcessDrawReply

    #endregion " Constructor "

    #region " Public Methods "

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgTerminalDrawProcessDrawReply>");

      _sb.AppendLine("<Games>");

      foreach (TerminalDrawPrizePlanDetail _prize_plan_games in PrizePlanGames.Games)
      {
        _sb.AppendLine("  <Game>");
        _sb.AppendLine("    <GameId>" + _prize_plan_games.GameId.ToString() + "</GameId>");
        _sb.AppendLine("    <GameType>" + _prize_plan_games.GameType.ToString() + "</GameType>");
        _sb.AppendLine("    <TerminalGameUrl>" + _prize_plan_games.TerminalGameUrl + "</TerminalGameUrl>");
        _sb.AppendLine("    <TerminalGameName>" + _prize_plan_games.TerminalGameName + "</TerminalGameName>");
        
        //GP Message Line LCD 4 lines

        // Result Winner
        _sb.AppendLine("    <ResultWinnerScreenMessageLine0>" + _prize_plan_games.ResultWinnerScreenMessageLine0 + "</ResultWinnerScreenMessageLine0>");
        _sb.AppendLine("    <ResultWinnerScreenMessageLine1>" + _prize_plan_games.ResultWinnerScreenMessageLine1 + "</ResultWinnerScreenMessageLine1>");
        _sb.AppendLine("    <ResultWinnerScreenMessageLine2>" + _prize_plan_games.ResultWinnerScreenMessageLine2 + "</ResultWinnerScreenMessageLine2>");
        _sb.AppendLine("    <ResultWinnerScreenMessageLine3>" + _prize_plan_games.ResultWinnerScreenMessageLine3 + "</ResultWinnerScreenMessageLine3>");

        // Result Looser
        _sb.AppendLine("    <ResultLooserScreenMessageLine0>" + _prize_plan_games.ResultLooserScreenMessageLine0 + "</ResultLooserScreenMessageLine0>");
        _sb.AppendLine("    <ResultLooserScreenMessageLine1>" + _prize_plan_games.ResultLooserScreenMessageLine1 + "</ResultLooserScreenMessageLine1>");
        _sb.AppendLine("    <ResultLooserScreenMessageLine2>" + _prize_plan_games.ResultLooserScreenMessageLine2 + "</ResultLooserScreenMessageLine2>");
        _sb.AppendLine("    <ResultLooserScreenMessageLine3>" + _prize_plan_games.ResultLooserScreenMessageLine3 + "</ResultLooserScreenMessageLine3>");

        // Is Winner or not
        _sb.AppendLine("    <IsWinner>" + _prize_plan_games.IsWinner.ToString() + "</IsWinner>");

        // Data
        _sb.AppendLine("    <AmountRePlayedDraw>" + ((Int64)(_prize_plan_games.AmountRePlayedDraw * 100)).ToString() + "</AmountRePlayedDraw>");
        _sb.AppendLine("    <AmountRePlayedWon>" + ((Int64)(_prize_plan_games.AmountRePlayedWon * 100)).ToString() + "</AmountRePlayedWon>");
        _sb.AppendLine("    <AmountNrPlayedDraw>" + ((Int64)(_prize_plan_games.AmountNrPlayedDraw * 100)).ToString() + "</AmountNrPlayedDraw>");
        _sb.AppendLine("    <AmountNrPlayedWon>" + ((Int64)(_prize_plan_games.AmountNrPlayedWon * 100)).ToString() + "</AmountNrPlayedWon>");
        _sb.AppendLine("    <AmountPointsPlayedDraw>" + _prize_plan_games.AmountPointsPlayedDraw.ToString() + "</AmountPointsPlayedDraw>");
        _sb.AppendLine("    <AmountPointsPlayedWon>" + _prize_plan_games.AmountPointsPlayedWon.ToString() + "</AmountPointsPlayedWon>");
        _sb.AppendLine("    <TerminalGameTimeOut>" + ((Int64)(_prize_plan_games.TerminalGameTimeOut)).ToString() + "</TerminalGameTimeOut>");
        _sb.AppendLine("    <AmountTotalBalance>" + ((Int64)(_prize_plan_games.AmountTotalBalance * 100)).ToString() + "</AmountTotalBalance>");
        _sb.AppendLine("    <PlaySessionId>" + ((Int64)(_prize_plan_games.PlaySessionId)).ToString() + "</PlaySessionId>");
        _sb.AppendLine("    <Forced>" + _prize_plan_games.Forced.ToString() + "</Forced>");
        
        _sb.AppendLine("  </Game>");
      }
      _sb.AppendLine("</Games>");

      _sb.AppendLine("</WCP_MsgTerminalDrawProcessDrawReply>");

      return _sb.ToString();
    } // ToXml

    /// <summary>
    /// Load Xml from a reader
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      PrizePlanData _prize_plan_data;

      _prize_plan_data = new PrizePlanData();

      if (!Reader.Name.Equals("WCP_MsgTerminalDrawProcessDrawReply"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgTerminalDrawProcessDrawReply not found in content area");
      }

      while ((Reader.Name != "Games") || (Reader.NodeType != XmlNodeType.EndElement))
      {
        if ((Reader.Name == "GameId") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.GameId = Int64.Parse(XML.ReadTagValue(Reader, "GameId"));
        }

        if ((Reader.Name == "GameType") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.GameType = Int64.Parse(XML.ReadTagValue(Reader, "GameType"));
        }

        if ((Reader.Name == "TerminalGameUrl") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.TerminalGameUrl = XML.ReadTagValue(Reader, "TerminalGameUrl");
        }

        if ((Reader.Name == "TerminalGameName") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.TerminalGameName = XML.ReadTagValue(Reader, "TerminalGameName");
        }

        //Result Winner
        if ((Reader.Name == "ResultWinnerScreenMessageLine0") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.ResultWinnerScreenMessageLine0 = XML.ReadTagValue(Reader, "ResultWinnerScreenMessageLine0");
        }

        if ((Reader.Name == "ResultWinnerScreenMessageLine1") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.ResultWinnerScreenMessageLine1 = XML.ReadTagValue(Reader, "ResultWinnerScreenMessageLine1");
        }

        if ((Reader.Name == "ResultWinnerScreenMessageLine2") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.ResultWinnerScreenMessageLine2 = XML.ReadTagValue(Reader, "ResultWinnerScreenMessageLine2");
        }

        if ((Reader.Name == "ResultWinnerScreenMessageLine3") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.ResultWinnerScreenMessageLine3 = XML.ReadTagValue(Reader, "ResultWinnerScreenMessageLine3");
        }

        //Result Looser
        if ((Reader.Name == "ResultLooserScreenMessageLine0") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.ResultLooserScreenMessageLine0 = XML.ReadTagValue(Reader, "ResultLooserScreenMessageLine0");
        }

        if ((Reader.Name == "ResultLooserScreenMessageLine1") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.ResultLooserScreenMessageLine1 = XML.ReadTagValue(Reader, "ResultLooserScreenMessageLine1");
        }

        if ((Reader.Name == "ResultLooserScreenMessageLine2") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.ResultLooserScreenMessageLine2 = XML.ReadTagValue(Reader, "ResultLooserScreenMessageLine2");
        }

        if ((Reader.Name == "ResultLooserScreenMessageLine3") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.ResultLooserScreenMessageLine3 = XML.ReadTagValue(Reader, "ResultLooserScreenMessageLine3");
        }

        // Is Winner?
        if ((Reader.Name == "IsWinner") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.IsWinner= Boolean.Parse(XML.ReadTagValue(Reader, "IsWinner"));
        }

        // Data
        if ((Reader.Name == "AmountRePlayedDraw") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.AmountRePlayedDraw = Int64.Parse(XML.ReadTagValue(Reader, "AmountRePlayedDraw"));
        }

        if ((Reader.Name == "AmountRePlayedWon") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.AmountRePlayedWon = Int64.Parse(XML.ReadTagValue(Reader, "AmountRePlayedWon"));
        }

        if ((Reader.Name == "AmountNrPlayedDraw") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.AmountNrPlayedDraw = Int64.Parse(XML.ReadTagValue(Reader, "AmountNrPlayedDraw"));
        }

        if ((Reader.Name == "AmountNrPlayedWon") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.AmountNrPlayedWon = Int64.Parse(XML.ReadTagValue(Reader, "AmountNrPlayedWon"));
        }

        if ((Reader.Name == "AmountPointsPlayedDraw") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.AmountPointsPlayedDraw = Int64.Parse(XML.ReadTagValue(Reader, "AmountPointsPlayedDraw"));
        }

        if ((Reader.Name == "AmountPointsPlayedWon") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.AmountPointsPlayedWon = Int64.Parse(XML.ReadTagValue(Reader, "AmountPointsPlayedWon"));
        }

        if ((Reader.Name == "TerminalGameTimeOut") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.TerminalGameTimeOut = Int64.Parse(XML.ReadTagValue(Reader, "TerminalGameTimeOut"));
        }

        if ((Reader.Name == "AmountTotalBalance") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.AmountTotalBalance = Int64.Parse(XML.ReadTagValue(Reader, "AmountTotalBalance"));
        }

        if ((Reader.Name == "PlaySessionId") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.PlaySessionId = Int64.Parse(XML.ReadTagValue(Reader, "PlaySessionId"));
        }

        if ((Reader.Name == "Forced") && (Reader.NodeType == XmlNodeType.Element))
        {
          _prize_plan_data.Forced = Boolean.Parse(XML.ReadTagValue(Reader, "Forced"));
        }

        if ((Reader.Name == "Game") && (Reader.NodeType == XmlNodeType.EndElement))
        {
          PrizePlanGames.AddPrizePlan(_prize_plan_data);
        }

        Reader.Read();
      }

    } // LoadXml

    #endregion " Public Methods "

  } // WCP_MsgTerminalDrawProcessDrawReply

}
