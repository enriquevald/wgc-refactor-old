//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TITO_MsgValidationNumber.cs
// 
//   DESCRIPTION: WCP_TITO_MsgValidationNumber class; message for ValidationNumber request
// 
//        AUTHOR: Nelson Madrigal Reyes
// 
// CREATION DATE: 20-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-NOV-2013 NMR    First release.
// 26-NOV-2013 JRM    Changed none standard code
// 13-FEB-2014 JCOR   Added Type/Cents to the XML.
// 13-OCT-2016 FJC    PBI 18258:TITO: Nuevo estado "Pending Print" en tickets - Creaci�n nuevo estado
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_TITO_MsgValidationNumber : IXml
  {
    public Int32 TicketType = 0;
    public Int64 AmountCents = 0;
    public Int64 TransactionId = 0;

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_TITO_MsgValidationNumber>");
      _sb.AppendLine("<AmountCents>" + AmountCents + "</AmountCents>");
      _sb.AppendLine("<TicketType>" + TicketType + "</TicketType>");
      _sb.AppendLine("<TransactionId>" + TransactionId + "</TransactionId>");
      _sb.AppendLine("</WCP_TITO_MsgValidationNumber>");
      return _sb.ToString();
    } // ToXml

    public void LoadXml(XmlReader Reader)
    {
      if (!Reader.Name.Equals("WCP_TITO_MsgValidationNumber"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag 'WCP_TITO_MsgValidationNumber' not found in content area");
      }

      Int64.TryParse(XML.ReadTagValue(Reader, "AmountCents", true), out AmountCents);
      Int32.TryParse(XML.ReadTagValue(Reader, "TicketType", true), out TicketType);
      Int64.TryParse(XML.ReadTagValue(Reader, "TransactionId", true), out TransactionId);

      Reader.Close();
    } // LoadXml
  }

  public class WCP_TITO_MsgValidationNumberReply : IXml
  {
    public Int32 system_id;
    public Int32 machine_id;
    public Int32 sequence_id;
    public Int64 validation_number_sequence;
    public Int64 transaction_id;

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_TITO_MsgValidationNumberReply>");

      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "SystemId", system_id));
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "MachineId", machine_id));
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "SequenceId", sequence_id));
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "ValidationNumberSequence", validation_number_sequence));
      _sb.AppendLine(String.Format("<{0}>{1}</{0}>", "TransactionId", transaction_id));

      _sb.AppendLine("</WCP_TITO_MsgValidationNumberReply>");
      return _sb.ToString();
    }

    public void LoadXml(XmlReader Reader)
    {
      if (!Reader.Name.Equals("WCP_TITO_MsgValidationNumberReply"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag 'WCP_TITO_MsgValidationNumberReply' not found in content area");
      }

      system_id = XML.GetAsInt32(Reader, "SystemId");
      machine_id = XML.GetAsInt32(Reader, "MachineId");
      sequence_id = XML.GetAsInt32(Reader, "SequenceId");
      validation_number_sequence = XML.GetAsInt64(Reader, "ValidationNumberSequence");
      transaction_id = XML.GetAsInt64(Reader, "TransactionId");

      Reader.Close();
    }
  }
}
