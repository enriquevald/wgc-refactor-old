 

//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:  // class WCP_TITO_MsgEgmParameters
// 
//   DESCRIPTION:  class
// 
//        AUTHOR: Joshwa Marcalle
// 
// CREATION DATE: 07-OCT-2013
// 
// REVISION HISTORY:
// 
// Date            Author Description
// ----------- ------ ----------------------------------------------------------
// 07-OCT-2013 JRM    First release.
// 09-OCT-2013 JRM    Change of type of some members. 
// 24-OCT-2013 NMR    SystemId removed from message; renamed MachineValidation
// 26-NOV-2013 JRM    Code cleaning
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_TITO_MsgEgmParameters : IXml
  {
    public Int32 ValidationType;  
    public Boolean AllowCashableTicketOut;
    public Boolean AllowPromotionalTicketOut;
    public Boolean AllowTicketIn;
    public Int64 ValidationSequence; // sequence number used for ticket creation. 
    public Int32 MachineId;   // MachineId == TerminalId
    public String SASVersion = "";
    public String SASMachineName = "";
    public Int32 Bonus;
    public Int32 Features;
    public String SASSerialNumber = "";
    public Int64 SASAssetNumber;
    public Int64 SASAccountDenomCents;


    public WCP_TITO_MsgEgmParameters()
    {

    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_TITO_MsgEgmParameters></WCP_TITO_MsgEgmParameters>");

      node = xml.SelectSingleNode("WCP_TITO_MsgEgmParameters");

      XML.AppendChild(node, "ValidationType", ValidationType.ToString());
      XML.AppendChild(node, "AllowCashableTicketOut", AllowCashableTicketOut.ToString());
      XML.AppendChild(node, "AllowPromotionalTicketOut", AllowPromotionalTicketOut.ToString());
      XML.AppendChild(node, "AllowTicketIn", AllowTicketIn.ToString());
      XML.AppendChild(node, "ValidationSequence", ValidationSequence.ToString());
      XML.AppendChild(node, "MachineId", MachineId.ToString());
      XML.AppendChild(node, "SASVersion", SASVersion);
      XML.AppendChild(node, "SASMachineName", SASMachineName);
      XML.AppendChild(node, "Bonus", Bonus.ToString());
      XML.AppendChild(node, "Features", Features.ToString());
      XML.AppendChild(node, "SASSerialNumber", SASSerialNumber.ToString());
      XML.AppendChild(node, "SASAssetNumber", SASAssetNumber.ToString());
      XML.AppendChild(node, "SASAccountDenom", SASAccountDenomCents.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_TITO_MsgEgmParameters")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgEgmParameters not found in content area");
      }
      else
      {
        ValidationType = Int32.Parse(XML.GetValue(Reader, "ValidationType"));
        AllowCashableTicketOut = Boolean.Parse(XML.GetValue(Reader, "AllowCashableTicketOut"));
        AllowPromotionalTicketOut = Boolean.Parse(XML.GetValue(Reader, "AllowPromotionalTicketOut"));
        AllowTicketIn = Boolean.Parse(XML.GetValue(Reader, "AllowTicketIn"));
        ValidationSequence = Int32.Parse(XML.GetValue(Reader, "ValidationSequence"));
        MachineId = Int32.Parse(XML.GetValue(Reader, "MachineId"));
        SASVersion = XML.GetValue(Reader, "SASVersion", true);
        SASMachineName = XML.GetValue(Reader, "SASMachineName", true);
        Int32.TryParse(XML.ReadTagValue(Reader, "Bonus", true), out Bonus);
        Int32.TryParse(XML.ReadTagValue(Reader, "Features", true), out Features);
        SASSerialNumber = XML.GetValue(Reader, "SASSerialNumber", true);
        if (SASSerialNumber != String.Empty)
        {
          Int64.TryParse(XML.ReadTagValue(Reader, "SASAssetNumber", true), out SASAssetNumber);
        }
        Int64.TryParse(XML.ReadTagValue(Reader, "SASAccountDenom", true), out SASAccountDenomCents);
        Reader.Close();
      }
    }
  }

  public class WCP_TITO_MsgEgmParametersReply : IXml
  {
    public WCP_TITO_MsgEgmParametersReply()
    {

    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_TITO_MsgEgmParametersReply>");
      _sb.AppendLine("</WCP_TITO_MsgEgmParametersReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_TITO_MsgEgmParametersReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgEgmParametersReply not found in content area");
      }
    }
  }
}

