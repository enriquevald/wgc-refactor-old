//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgIsValidTicket.cs
// 
//   DESCRIPTION: WCP_MsgIsValidTicket class
// 
//        AUTHOR: Joshwa Marcalle
// 
// CREATION DATE: 26-SEP-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-SEP-2013 JRM    First release.
// 08-OCT-2013 NMR    Fixed errors in message processing: IsValidTicket and CancelTicket
// 08-OCT-2013 JRM    Change validationNumber and amount to Int64.
// 17-FEB-2014 JCOR   Added TickedId to message.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;
using WSI.WCP;

namespace WSI.WCP
{
  public class WCP_TITO_MsgIsValidTicket : IXml
  {
    public Int32 SystemId;
    public Int64 AmountCents;
    public Int64 ValidationNumberSequence;

    public WCP_TITO_MsgIsValidTicket()
    {

    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_TITO_MsgIsValidTicket></WCP_TITO_MsgIsValidTicket>");

      node = xml.SelectSingleNode("WCP_TITO_MsgIsValidTicket");

      XML.AppendChild(node, "SystemId", SystemId.ToString());
      XML.AppendChild(node, "AmountCents", AmountCents.ToString());
      XML.AppendChild(node, "ValidationNumberSequence", ValidationNumberSequence.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_TITO_MsgIsValidTicket")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgIsValidTicket not found in content area");
      }
      else
      {
        SystemId = Int32.Parse(XML.GetValue(Reader, "SystemId"));
        AmountCents = Int64.Parse(XML.GetValue(Reader, "AmountCents"));
        ValidationNumberSequence = Int64.Parse(XML.GetValue(Reader, "ValidationNumberSequence"));

        Reader.Close();
      }
    }
  }

  public class WCP_TITO_MsgIsValidTicketReply : IXml
  {
    public Int32 SystemId;
    public Int64 AmountCents;
    public DateTime ExpirationDate;
    public Int64 PoolId;
    public Int32 ValidateStatus;
    public Int64 TicketId = 0;

  
    public WCP_TITO_MsgIsValidTicketReply()
    {
      ExpirationDate = new DateTime();
    }

    public Int64 GetExpirationDatetime()
    {
      return ExpirationDate.ToFileTime(); 

    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_TITO_MsgIsValidTicketReply>");
      _sb.AppendLine("<SystemId>" + SystemId.ToString() + "</SystemId>");
      _sb.AppendLine("<AmountCents>" + AmountCents.ToString() + "</AmountCents>");
      _sb.AppendLine("<ExpirationDate>" + XML.XmlDateString(ExpirationDate) + "</ExpirationDate>");
      _sb.AppendLine("<PoolId>" + PoolId.ToString() + "</PoolId>");
      _sb.AppendLine("<ValidateStatus>" + ValidateStatus.ToString() + "</ValidateStatus>");
      _sb.AppendLine("<TicketId>" + TicketId.ToString() + "</TicketId>");
      _sb.AppendLine("</WCP_TITO_MsgIsValidTicketReply>");

      return _sb.ToString();
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_TITO_MsgIsValidTicketReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgIsValidTicketReply not found in content area");
      }

      SystemId = Int32.Parse(XML.GetValue(reader, "SystemId"));
      AmountCents = Int64.Parse(XML.GetValue(reader, "AmountCents"));
      ExpirationDate = XML.GetAsDate(reader, "ExpirationDate"); 
      PoolId = Int64.Parse(XML.GetValue(reader, "PoolId"));
      ValidateStatus = Int32.Parse(XML.GetValue(reader, "ValidateStatus"));
      Int64.TryParse(XML.ReadTagValue(reader, "TicketId", true), out TicketId);
   
    } // LoadXml
  }
}
