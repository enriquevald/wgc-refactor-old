//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgCancelTicket.cs
// 
//   DESCRIPTION: WCP_MsgCancelTicket class
// 
//        AUTHOR: Joshwa Marcalle
// 
// CREATION DATE: 27-SEP-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-SEP-2013 JRM    First release.
// 09-OCT-2013 JRM    Change in member types  
// 17-FEB-2014 JCOR   Added TickedId to message.
// 03-NOV-2016 FJC    PBI 19605:TITO: SAS_HOST - informar motivo de rechazo
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_TITO_MsgCancelTicket : IXml
  {
    public Int32 SystemId;
    public Int64 ValidationNumberSequence;
    public Boolean StopCancellation;
    public Int64 TicketId;
    public ENUM_TITO_REDEEM_TICKET_TRANS_STATUS MachineStatus;  // "Ticket Reject Reason EGM"
    public ENUM_TITO_IS_VALID_TICKET_MSG ValidateStatus;        // "Ticket Reject Reason WCP"  

    public WCP_TITO_MsgCancelTicket()
    {
      ValidationNumberSequence = 0;
    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_TITO_MsgCancelTicket></WCP_TITO_MsgCancelTicket>");

      node = xml.SelectSingleNode("WCP_TITO_MsgCancelTicket");

      XML.AppendChild(node, "ValidationNumberSequence", ValidationNumberSequence.ToString());
      XML.AppendChild(node, "SystemId", SystemId.ToString());
      XML.AppendChild(node, "StopCancellation", StopCancellation ? "1" : "0");
      XML.AppendChild(node, "TicketId", TicketId.ToString());
      XML.AppendChild(node, "MachineStatus", MachineStatus.ToString());
      XML.AppendChild(node, "ValidateStatus", ValidateStatus.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_TITO_MsgCancelTicket")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgCancelTicket not found in content area");
      }

      ValidationNumberSequence = Int64.Parse(XML.GetValue(Reader, "ValidationNumberSequence"));
      SystemId = Int32.Parse(XML.GetValue(Reader, "SystemId"));
      StopCancellation = XML.GetValue(Reader, "StopCancellation") == "1";
      Int64.TryParse(XML.ReadTagValue(Reader, "TicketId", true), out TicketId);

      Reader.Read();
      if (Reader.Name == "MachineStatus")
      { 
        MachineStatus = (ENUM_TITO_REDEEM_TICKET_TRANS_STATUS)Enum.Parse(typeof(ENUM_TITO_REDEEM_TICKET_TRANS_STATUS), XML.GetValue(Reader, "MachineStatus"));
        Reader.Read();
      }

      Reader.Read();
      if (Reader.Name == "ValidateStatus")
      {
        ValidateStatus = (ENUM_TITO_IS_VALID_TICKET_MSG)Enum.Parse(typeof(ENUM_TITO_IS_VALID_TICKET_MSG), XML.GetValue(Reader, "ValidateStatus"));
      }
    }
  }

  public class WCP_TITO_MsgCancelTicketReply : IXml
  {
    public WCP_TITO_MsgCancelTicketReply()
    {

    }

    public String ToXml()
    {
      StringBuilder _sb;
     
      _sb = new StringBuilder();
       
      _sb.AppendLine("<WCP_TITO_MsgCancelTicketReply>");
      _sb.AppendLine("</WCP_TITO_MsgCancelTicketReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_TITO_MsgCancelTicketReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgCancelTicketReply not found in content area");
      }
    }
  }
}
