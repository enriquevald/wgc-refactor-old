//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:  TITO_ProcessMessage.cs
// 
//   DESCRIPTION: Process TITO messages
//
//   AUTHOR     : JRM
// 
//   REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-NOV-2013 JRM    First version.
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_TITO
{
  public static Boolean TITO_ProcessRequest(SecureTcpClient TcpClient, WCP_Message TitoRequest, WCP_Message TitoResponse)
  {
    Boolean _send_response;

    _send_response = false;

    switch (TitoRequest.MsgHeader.MsgType)
    {
      case WCP_MsgTypes.WCP_TITO_MsgIsValidTicket:
        TITO_EnqueueMessage(TcpClient, TitoRequest, TitoResponse);
        break;

      case WCP_MsgTypes.WCP_TITO_MsgCancelTicket:
        TITO_EnqueueMessage(TcpClient, TitoRequest, TitoResponse);
        break;

      case WCP_MsgTypes.WCP_TITO_MsgCreateTicket:
        TITO_EnqueueMessage(TcpClient, TitoRequest, TitoResponse);
        break;

      case WCP_MsgTypes.WCP_TITO_MsgEgmParameters:
        _send_response = true;
        TITO_ProcessEgmParameters(TcpClient, TitoRequest, TitoResponse);
        break;

      case WCP_MsgTypes.WCP_TITO_MsgValidationNumber:
        _send_response = false;
        TITO_ProcessValidationNumber(TcpClient, TitoRequest, TitoResponse);
        break;

      default:
        TitoResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TITO_MESSAGE_NOT_SUPPORTED;
        TitoResponse.MsgHeader.ResponseCodeText = "TITO Message not supported.";
        break;

    } // switch (TitoRequest.MsgHeader.MsgType)

    return _send_response;
  }
}
