//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgIsValidTicket.cs
// 
//   DESCRIPTION: WCP_MsgIsValidTicket class
// 
// 
// CREATION DATE: 26-SEP-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-SEP-2013 JRM    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{

  public class WCP_MsgIsValidTicket : IXml
  {
    // IN Params
    public Int64 MachineId;
    public Currency Quantity;
    public String ValidationNumber;
  
    #region IXml Members

    public string ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      xml.LoadXml("<WCP_MsgIsValidTicket></WCP_MsgIsValidTicket>");
      node = xml.SelectSingleNode("WCP_MsgIsValidTicket");

      XML.AppendChild(node, "MachineId", MachineId.ToString());
      XML.AppendChild(node, "Quantity", Quantity.ToString());
      XML.AppendChild(node, "ValidationNumber", ValidationNumber);

      return xml.OuterXml;
    }

    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgIsValidTicket")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgIsValidTicket not found in content area");
      }
      else
      {
        MachineId = Int64.Parse(XML.GetValue(Reader, "MachineId"));
        Quantity = Int32.Parse(XML.GetValue(Reader, "Quantity"));
        ValidationNumber = XML.GetValue(Reader, "DenominationCents");

        Reader.Close();
      }
    }

    #endregion
  }

  public class WCP_MsgIsValidTicketReply : IXml
  {
    #region IXml Members
    public string ToXml()
    {
      return "<WCP_MsgIsValidTicketReply></WCP_MsgIsValidTicketReply>";
    }

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      if (Xml.Name != "WCP_MsgIsValidTicketReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgIsValidTicketReply not found in content area");
      }
    }

    #endregion
  }
}
