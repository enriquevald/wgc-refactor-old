 
//  class WCP_TITO_MsgPrintTicket


//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgPrintTicket.cs
// 
//   DESCRIPTION: WCP_MsgPrintTicket class
// 
//        AUTHOR: Joshwa Marcalle
// 
// CREATION DATE: 03-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-OCT-2013 JRM    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_TITO_MsgPrintTicket: IXml
  {
    public Int32 ValidationType;
    public Int64 Date;
    public String ValidationNumber;
    public Int32 Amount;
    public String TicketNumber;
    public Int32 ValidationSystemID;
    public Int64 ExpirationDate;
    public Int64 PoolID;

    // Helper variables not in original VO
    public String TerminalID;
    public Int32 TerminalType;
    public Int64 CreatedAccountID;
    public Int64 CreatedCashierSession;
    public ENUM_TITO_TICKET_TYPE TicketType;

    public WCP_TITO_MsgPrintTicket()
    {

    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_TITO_MsgPrintTicket></WCP_TITO_MsgPrintTicket>");

      node = xml.SelectSingleNode("WCP_TITO_MsgPrintTicket");

      XML.AppendChild(node, "ValidationType", ValidationType.ToString());
      XML.AppendChild(node, "Date", Date.ToString());
      XML.AppendChild(node, "ValidationNumber", ValidationNumber);
      XML.AppendChild(node, "Amount", Amount.ToString());
      XML.AppendChild(node, "TicketNumber", TicketNumber);
      XML.AppendChild(node, "ValidationSystemID", ValidationSystemID.ToString());
      XML.AppendChild(node, "ExpirationDate", ExpirationDate.ToString());
      XML.AppendChild(node, "PoolID", PoolID.ToString());
      XML.AppendChild(node, "TerminalID", TerminalID.ToString());
      XML.AppendChild(node, "TerminalType", TerminalType.ToString());
      XML.AppendChild(node, "CreatedAccountID", CreatedAccountID.ToString());
      XML.AppendChild(node, "CreatedCashierSession", CreatedCashierSession.ToString());

      return xml.OuterXml;
    }
    
    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_TITO_MsgPrintTicket")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgPrintTicket not found in content area");
      }
      else
      {
        ValidationType = Int32.Parse(XML.GetValue(Reader, "ValidationType"));
        Date = Int64.Parse(XML.GetValue(Reader, "Date"));
        ValidationNumber = XML.GetValue(Reader, "ValidationNumber");
        Amount = Int32.Parse(XML.GetValue(Reader, "Amount"));
        ValidationSystemID = Int32.Parse(XML.GetValue(Reader, "ValidationSystemID"));
        ExpirationDate = Int64.Parse(XML.GetValue(Reader, "ExpirationDate"));
        PoolID = Int64.Parse(XML.GetValue(Reader, "PoolID"));
        TerminalID = XML.GetValue(Reader, "TerminalID");
        TerminalType = Int32.Parse(XML.GetValue(Reader, "TerminalType"));
        CreatedAccountID = Int64.Parse(XML.GetValue(Reader, "CreatedAccountID"));
        CreatedCashierSession = Int64.Parse(XML.GetValue(Reader, "CreatedCashierSession"));

        Reader.Close();
      }
    }
  }

  public class WCP_TITO_MsgPrintTicketReply : IXml
  {
    public WCP_TITO_MsgPrintTicketReply()
    {

    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
   
      _sb.AppendLine("<WCP_TITO_MsgPrintTicketReply>");
   
      _sb.AppendLine("</WCP_TITO_MsgPrintTicketReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_TITO_MsgPrintTicketReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgPrintTicketReply not found in content area");
      }
      else
      {
      }
    } 
  }
}

