 
//  class WCP_TITO_MsgCreateTicket


//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgCreateTicket.cs
// 
//   DESCRIPTION: WCP_MsgCreateTicket class
// 
//        AUTHOR: Joshwa Marcalle
// 
// CREATION DATE: 03-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-OCT-2013 JRM    First release.
// 09-OCT-2013 JRM    Chaneg of type in some members, removal of some members
// 26-OCT-2013 JRM    Amount is now processed in/to cents representation
// 19-NOV-2013 NMR    Added new ticket property
// 26-NOV-2013 JRM    Code cleaning
// 26-NOV-2013 JRM    Add property TransactionId to duplicated tickets
// 03-DIC-2013 NMR    Add property PlaySessionId to create tickets
// 12-DIC-2013 NMR    All tickets wit NO EXPIRATION DATE reported by terminal, will have Default Expiration date
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_TITO_MsgCreateTicket: IXml
  {
    public Int64 TransactionId;
    public Int32 ValidationType;
    public DateTime Date;
    public Int64 ValidationNumberSequence;
    public Int32 MachineTicketNumber;
    public Int64 AmountCents;
    public Int32 SystemId;
    public DateTime ExpirationDate;
    public Int64 PoolId;
    public Int64 PlaySessionId;

    // Helper variables not in original VO
    public String TerminalID;
    public Int32 TerminalType;
    public Int64 CreatedAccountID;
    public Int32 TicketType;

    // date will limit MAX ticket expiration date
    protected static DateTime MIN_VALID_DATE_EXPIRATION = new DateTime(2010, 1, 1);

    public WCP_TITO_MsgCreateTicket()
    {

    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_TITO_MsgCreateTicket></WCP_TITO_MsgCreateTicket>");

      node = xml.SelectSingleNode("WCP_TITO_MsgCreateTicket");

      XML.AppendChild(node, "TransactionId", TransactionId.ToString());
      XML.AppendChild(node, "ValidationType", ValidationType.ToString());
      XML.AppendChild(node, "Date", XML.XmlDateTimeString(Date));
      XML.AppendChild(node, "ValidationNumberSequence", ValidationNumberSequence.ToString());
      XML.AppendChild(node, "MachineTicketNumber", MachineTicketNumber.ToString());
      XML.AppendChild(node, "AmountCents", AmountCents.ToString());
      XML.AppendChild(node, "SystemId", SystemId.ToString());
      XML.AppendChild(node, "ExpirationDate", XML.XmlDateString(ExpirationDate));
      XML.AppendChild(node, "PoolId", PoolId.ToString());
      XML.AppendChild(node, "TerminalType", TerminalType.ToString());
      XML.AppendChild(node, "TicketType", TicketType.ToString());
      XML.AppendChild(node, "PlaySessionId", PlaySessionId.ToString());

      return xml.OuterXml;
    }

    public void SetDatetime(Int64 FileTime)
    {
      Date = DateTime.FromFileTime(FileTime);
    }

    public void SetExpirationDatetime(Int64 FileTime)
    {
      if (FileTime > 0)
      {
        ExpirationDate = DateTime.FromFileTime(FileTime);
      }
      else
      {
        ExpirationDate = DateTime.MinValue;
      }
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_TITO_MsgCreateTicket")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgCreateTicket not found in content area");
      }
      else
      {
        TransactionId = XML.GetAsInt64(Reader, "TransactionId");
        ValidationType = Int32.Parse(XML.GetValue(Reader, "ValidationType"));
        Date = XML.GetAsDateTime(Reader, "Date"); 
        ValidationNumberSequence = Int64.Parse(XML.GetValue(Reader, "ValidationNumberSequence"));
        MachineTicketNumber = Int32.Parse(XML.GetValue(Reader, "MachineTicketNumber"));
        AmountCents = Int64.Parse(XML.GetValue(Reader, "AmountCents"));
        SystemId = Int32.Parse(XML.GetValue(Reader, "SystemId"));
        ExpirationDate = XML.GetAsDate(Reader, "ExpirationDate"); 
        if (ExpirationDate < MIN_VALID_DATE_EXPIRATION)
        {
          ExpirationDate = DateTime.MinValue;
        }
        PoolId = Int64.Parse(XML.GetValue(Reader, "PoolId"));
        TerminalType = Int32.Parse(XML.GetValue(Reader, "TerminalType"));
        TicketType = Int32.Parse(XML.GetValue(Reader, "TicketType"));
        PlaySessionId = XML.GetAsInt64(Reader, "PlaySessionId");

        Reader.Close();
      }
    }
  }

  public class WCP_TITO_MsgCreateTicketReply : IXml
  {
    public WCP_TITO_MsgCreateTicketReply()
    {

    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
   
      _sb.AppendLine("<WCP_TITO_MsgCreateTicketReply>");
      _sb.AppendLine("</WCP_TITO_MsgCreateTicketReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_TITO_MsgCreateTicketReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgCreateTicketReply not found in content area");
      }
    } 
  }
}

