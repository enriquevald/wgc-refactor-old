//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgSetPlayerFields.cs
// 
//   DESCRIPTION: WCP_WKT_MsgSetPlayerFields message class
// 
//        AUTHOR: Artur Nebot
// 
// CREATION DATE: 06-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-JUL-2012 ANG    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Xml;
using System.IO;

namespace WSI.WCP
{
  public abstract class WCP_WKT_MsgGenericSetPlayerFields : WCP_WKT.WKT_IGetPlayerField
  {
      #region "Members"
      public WCP_WKT.WKT_PlayerFieldsTable m_player_fields_table;
      #endregion

      #region "Constructor"
      public WCP_WKT_MsgGenericSetPlayerFields()
      {
        this.m_player_fields_table = new WCP_WKT.WKT_PlayerFieldsTable();
      }
      #endregion

      #region WKT_IGetPlayerField Members

      int WCP_WKT.WKT_IGetPlayerField.FieldsNumber
      {
        get { return this.m_player_fields_table.Rows.Count; }
      }

      WCP_WKT.WKT_IPlayerField WCP_WKT.WKT_IGetPlayerField.GetField(int Idx)
      {
        return (WCP_WKT.WKT_IPlayerField)this.m_player_fields_table.Rows[Idx];
      }

      public WCP_WKT.WKT_PlayerFieldsTable PlayerFields
      {
        get { return this.m_player_fields_table; }
        set { this.m_player_fields_table = value; } 
      }

      #endregion

      #region "Methods"
      //------------------------------------------------------------------------------
      // PURPOSE: Fill WKT_PlayerFieldsTable from reader
      //
      //  PARAMS:
      //      - INPUT:
      //          - XmlReader with message
      //
      //      - OUTPUT:
      //          - WKT_PlayerFieldsTable filled with message data.
      //
      // RETURNS:
      //      Bool status code
      //
      // NOTE :
      //      This method is PUBLIC because also it's called from WCP_WKT_MsgGetPlayerInfo class.
      public static bool FillFieldsFromXml(XmlReader Reader, ref WCP_WKT.WKT_PlayerFieldsTable Table)
      {
        String _xml_player_fields;

        while (!Reader.EOF && Reader.Name != "Fields") Reader.Read();

        if (Reader.EOF || Reader.Name != "Fields")
        {
          Table = new WCP_WKT.WKT_PlayerFieldsTable();
          return false;
        }

        _xml_player_fields = Reader.ReadOuterXml();

        using (XmlReader _reader = XmlReader.Create(new StringReader(_xml_player_fields)))
        {
          Table = new WCP_WKT.WKT_PlayerFieldsTable();
          Table.LoadXml(_reader);
        }


        return true;

      } // FillFieldsFromXml

      #endregion
    }

  public class WCP_WKT_MsgSetPlayerFields : WCP_WKT_MsgGenericSetPlayerFields, IXml, WCP_WKT.IExtendedPlayer
  {
    public WCP_WKT.WKT_Player m_player;

    #region "Constructor"
    public WCP_WKT_MsgSetPlayerFields() : base () {
      this.m_player = new WCP_WKT.WKT_Player();
    }
    #endregion

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb_xml;
      _sb_xml = new StringBuilder();

      _sb_xml.AppendLine("<WCP_WKT_MsgSetPlayerFields>");
      _sb_xml.AppendLine(m_player.ToXml());
      _sb_xml.AppendLine(m_player_fields_table.ToXml());
      _sb_xml.AppendLine("</WCP_WKT_MsgSetPlayerFields>");

      return _sb_xml.ToString();
    }

    public void LoadXml(XmlReader Reader)
    {

      if (Reader.Name != "WCP_WKT_MsgSetPlayerFields")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgSetPlayerFields not found in content area");
      }

      m_player.LoadXml(Reader);
      FillFieldsFromXml(Reader, ref m_player_fields_table);

    }

    #endregion

    #region IExtendedPlayer Members

    public WCP_WKT.WKT_Player Player
    {
      get
      {
        return m_player;
      }
      set
      {
        this.m_player = value;
      }
    }

    #endregion

  }

  public class WCP_WKT_MsgSetPlayerFieldsReply : WCP_WKT_MsgGenericSetPlayerFields, IXml
  {

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb_xml;
      _sb_xml = new StringBuilder();

      _sb_xml.AppendLine("<WCP_WKT_MsgSetPlayerFieldsReply>");
      _sb_xml.AppendLine(m_player_fields_table.ToXml());
      _sb_xml.AppendLine("</WCP_WKT_MsgSetPlayerFieldsReply>");
      
      return _sb_xml.ToString();
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_WKT_MsgSetPlayerFieldsReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgSetPlayerFields not found in content area");
      }
      FillFieldsFromXml(Reader, ref m_player_fields_table);

    }

    #endregion

    #region "Methods"
    public void SetPlayerFieldTable(WCP_WKT.WKT_PlayerFieldsTable Table)
    {
      this.m_player_fields_table = Table;
    }
    #endregion

  }
  
}
