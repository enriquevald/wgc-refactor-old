//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgGetNextAdStep.cs
// 
//   DESCRIPTION: WCP_WKT_MsgGetNextAdStep message class
// 
//        AUTHOR: Artur Nebot
// 
// CREATION DATE: 26-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUL-2012 ANG    First version.
// 24-AUG-2012 XIT    Code Refactored
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WKT_AdvSteps = WCP_WKT.WKT_Advertisement.WKT_AdvSteps;
using WKT_AdvStep = WCP_WKT.WKT_Advertisement.WKT_AdvStep;
using WKT_AdvStaticImage = WCP_WKT.WKT_Advertisement.WKT_AdvStaticImage;

namespace WSI.WCP
{
  public class WCP_WKT_MsgGetNextAdvStep : IXml
  {
    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.Append("<WCP_WKT_MsgGetNextAdStep>");
      _sb.Append("</WCP_WKT_MsgGetNextAdStep>");

      return _sb.ToString();

    } //ToXml

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_WKT_MsgGetNextAdStep")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetNextAdStep not found in content area");
      }
    } //LoadXml

    #endregion
  }

  public class WCP_WKT_MsgGetNextAdvStepReply : IXml, WCP_WKT.IResources
  {
    private WCP_WKT.WKT_ResourceInfoList m_resources;

    public WKT_AdvSteps m_steps;

    public WCP_WKT_MsgGetNextAdvStepReply()
    {
      this.m_steps = new WKT_AdvSteps();
      this.m_resources = new WCP_WKT.WKT_ResourceInfoList();
    }

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_WKT_MsgGetNextAdvStepReply>");
      _sb.Append(this.m_steps.ToXml());
      _sb.AppendLine("<Resources>");
      _sb.Append(Resources.ToXml());
      _sb.AppendLine("</Resources>");
      _sb.AppendLine("</WCP_WKT_MsgGetNextAdvStepReply>");

      return _sb.ToString();
    }

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      if (Xml.Name != "WCP_WKT_MsgGetNextAdvStepReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetNextAdvStepReply not found in content area");
      }

      this.m_steps.LoadXml(Xml);

      // Move reader to starting resources list node
      while (Xml.Name != "Resources" && Xml.NodeType == XmlNodeType.Element)
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetNextAdvStepReply Resources element missing");
        }
      }

      Resources.LoadXml(Xml);
    }

    #endregion

    #region IResources Members

    // Interface what allow to acces to Resources info in message
    public WCP_WKT.WKT_ResourceInfoList Resources
    {
      get
      {
        return this.m_resources;
      }
      set
      {
        this.m_resources = value;
      }
    }

    #endregion
  }
}
