//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgPlayerRequestPromo.cs
// 
//   DESCRIPTION: WCP_WKT_MsgPlayerRequestPromo class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 09-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-OCT-2013 JMM    First version.
// 27-NOV-2015 SGB    Product Backlog Item 6050: Add parameters for General Params
// 27-NOV-2015 SGB    Bug 7982: Don't show create ticket datetime
// 16-FEB-2016 ETP    Bug 9346: added compatibily for promotions in version 20150930k
// 01-DIC-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_WKT_MsgPlayerRequestPromo : IXml, WCP_WKT.IExtendedPlayer
  {

    public WCP_WKT.WKT_Player Player;
    public Int64 PromoId;
    public Decimal PromoReward;
    public Int64 PromoCreditType;
    public bool TitoPrinterIsReady;

    public WCP_WKT_MsgPlayerRequestPromo()
    {
      this.Player = new WCP_WKT.WKT_Player();
    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgPlayerRequestPromo>");
      _sb.Append("<Pin>" + XML.StringToXmlValue(Player.Pin) + "</Pin>");
      _sb.Append("<Trackdata>" + XML.StringToXmlValue(Player.Trackdata) + "</Trackdata>");
      _sb.Append("<PromoId>" + PromoId.ToString() + "</PromoId>");
      _sb.Append("<PromoReward>" + ((Int64)((Decimal)PromoReward * 100)).ToString() + "</PromoReward>");      
      _sb.Append("<PromoCreditType>" + PromoCreditType.ToString() + "</PromoCreditType>");
      _sb.Append("<TitoPrinterIsReady>" + (TitoPrinterIsReady ? "1" : "0") + "</TitoPrinterIsReady>");
      
      _sb.AppendLine("</WCP_WKT_MsgPlayerRequestPromo>");
      return _sb.ToString();

    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_WKT_MsgPlayerRequestPromo")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgPlayerRequestPromo not found in content area");
      }
      Player.Pin = XML.GetValue(reader, "Pin");
      Player.Trackdata = XML.GetValue(reader, "Trackdata");

      PromoId = Int64.Parse(XML.GetValue(reader, "PromoId"));
      PromoReward = ((Decimal)Int64.Parse(XML.GetValue(reader, "PromoReward"))) / 100;
      PromoCreditType = Int64.Parse(XML.GetValue(reader, "PromoCreditType"));

      reader.Read();
      reader.Read();
      if (reader.Name == "TitoPrinterIsReady")
      {
        TitoPrinterIsReady = XML.ReadTagValue(reader, "TitoPrinterIsReady") == "1";
      }
      
    } // LoadXml


    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get
      {
        return this.Player;
      }
      set
      {
        this.Player = value;
      }
    }

    #endregion
  } // WCP_WKT_MsgPlayerRequestPromo

  public class WCP_WKT_MsgPlayerRequestPromoReply : IXml, WCP_WKT.IVouchers
  {
    public MultiPromos.PromoBalance PromoBalance = new MultiPromos.PromoBalance();

    private String _deflated_voucher;
    private String _inflated_voucher;

    private ArrayList _voucherList;

    private Ticket _ticket = new Ticket();


    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgPlayerRequestPromoReply>");
      _sb.AppendLine("<Vouchers>");

      if (VoucherList != null)
      {
        foreach (Voucher _voucher in VoucherList)
        {
          Misc.DeflateString(_voucher.VoucherHTML.ToString(), out _deflated_voucher);
          _sb.Append("<Voucher>" + XML.StringToXmlValue(_deflated_voucher) + "</Voucher>");
        }
      }
      _sb.AppendLine("</Vouchers>");

      _sb.AppendLine("<Points>" + (Convert.ToInt64(Math.Floor(PromoBalance.Points))).ToString() + "</Points>");
      _sb.AppendLine("<Balance>");
      _sb.AppendLine(PromoBalance.Balance.ToXml());
      _sb.AppendLine("</Balance>");

       _sb.AppendLine("<TITOTicket>");
      _sb.AppendLine("<ValidationNumber>" + _ticket.ValidationNumber.ToString() + "</ValidationNumber>");
      _sb.AppendLine("<MachineTicketNumber>" + _ticket.MachineTicketNumber.ToString() + "</MachineTicketNumber>");
      _sb.AppendLine("<TicketType>" + ((Int32)_ticket.TicketType).ToString() + "</TicketType>");
      _sb.AppendLine("<AmountCents>" + ((Int64)(_ticket.Amount * 100)).ToString() + "</AmountCents>");
      _sb.AppendLine("<CreatedDate>" + XML.XmlDateTimeString(_ticket.CreatedDateTime) + "</CreatedDate>");
      _sb.AppendLine("<ExpirationDate>" + XML.XmlDateTimeString(_ticket.ExpirationDateTime) + "</ExpirationDate>");
      _sb.AppendLine("<Address1>" + _ticket.Address1 + "</Address1>");
      _sb.AppendLine("<Address2>" + _ticket.Address2 + "</Address2>");
      _sb.AppendLine("</TITOTicket>");
      _sb.AppendLine("</WCP_WKT_MsgPlayerRequestPromoReply>");

      return _sb.ToString();

    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      Int64 _aux_value;

      VoucherList = new ArrayList();

      if (reader.Name != "WCP_WKT_MsgPlayerRequestPromoReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgPlayerRequestPromoReply not found in content area");
      }
      else
      {
        while ((!(reader.Name == "Vouchers" && reader.NodeType == XmlNodeType.EndElement)) && (!(reader.Name == "WCP_WKT_MsgPlayerRequestPromoReply" && reader.NodeType == XmlNodeType.EndElement)))
        {
          reader.Read();
          if ((reader.Name == "Voucher") && (reader.NodeType != XmlNodeType.EndElement))
          {
            _deflated_voucher = XML.GetValue(reader, "Voucher");
            Misc.InflateString(_deflated_voucher, out _inflated_voucher);
            VoucherList.Add(_inflated_voucher);
          }
        }

        if (Int64.TryParse(XML.GetValue(reader, "Points", true), out _aux_value))
        {
          PromoBalance.Points = Convert.ToDecimal(_aux_value);
        }

        PromoBalance.Balance.LoadXml(reader);

        // read tag <TITOTicket>
        while ((!(reader.Name == "TITOTicket" && reader.NodeType == XmlNodeType.EndElement)) 
            && (!(reader.Name == "WCP_WKT_MsgPlayerRequestPromoReply" && reader.NodeType == XmlNodeType.EndElement))
            && !reader.EOF)
        {          
          reader.Read();
          if ((reader.Name == "TITOTicket") && (reader.NodeType != XmlNodeType.EndElement))
          {
            Ticket.ValidationNumber = XML.GetAsInt64(reader, "ValidationNumber");
            Ticket.MachineTicketNumber = XML.GetAsInt32(reader, "MachineTicketNumber");
            Ticket.TicketType = (TITO_TICKET_TYPE)XML.GetAsInt32(reader, "TicketType");
            Ticket.Amount = ((Decimal)XML.GetAsInt64(reader, "AmountCents")) / 100;
            Ticket.CreatedDateTime = XML.GetAsDateTime(reader, "CreatedDate");
            Ticket.ExpirationDateTime = XML.GetAsDateTime(reader, "ExpirationDate");

            if ((reader.Name == "Address1") && (reader.NodeType != XmlNodeType.EndElement))
            {
              Ticket.Address1 = XML.GetValue(reader, "Address1");
            }

            if ((reader.Name == "Address2") && (reader.NodeType != XmlNodeType.EndElement))
            {
              Ticket.Address2 = XML.GetValue(reader, "Address2");
            }
          }
        }
      }
    } // LoadXml


    #region IVoucher Members

    public ArrayList VoucherList
    {
      get
      {
        return _voucherList;
      }
      set
      {
        this._voucherList = value;
      }
    }
    
    public Ticket Ticket
    {
      get
      {
        return _ticket;
      }
      set
      {
        this._ticket = value;
      }
    }

    #endregion
  } // WCP_WKT_MsgPlayerRequestPromoReply

} // namespace WSI.WCP
