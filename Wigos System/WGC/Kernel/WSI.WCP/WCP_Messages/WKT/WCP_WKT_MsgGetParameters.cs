//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgGetParameters.cs
// 
//   DESCRIPTION: WCP_WKT_MsgGetParameters class
// 
//        AUTHOR: Joaquim Cid 
// 
// CREATION DATE: 30-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAY-2012 JCM    First version.
// 05-SEP-2012 XIT    Modified ToXmls
// 27-NOV-2015 SGB    Product Backlog Item 6050: Add parameters for General Params
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_WKT_MsgGetParameters : IXml
  {
    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgGetParameters>");
      _sb.AppendLine("</WCP_WKT_MsgGetParameters>");

      return _sb.ToString();

    } //ToXml

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_WKT_MsgGetParameters")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParameters not found in content area");
      }

    } //LoadXml

    #endregion //IXml Members
  } //WCP_WKT_MsgGetParameters

  public class WCP_WKT_MsgGetParametersReply : IXml, WCP_WKT.IResources
  {
    public List<Int32> Functionalities;
    public WCP_WKT.WKT_Images Images;
    public WCP_WKT.WKT_ResourceInfoList m_resources;
    public WCP_WKT.WKT_MessageList m_messages;
    public WCP_WKT.WKT_ScheduledTime m_schedule_time;
    public WCP_WKT.WKT_GiftsSplit m_gifts_split;
    public WCP_WKT.WKT_GeneralParamsList m_general_params;

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      // Kiosc general parameters contain
      //    - Kiosk Functionalities (only enabled)
      //    - Kiosk Customized Images
      //    - Customized Messages
      //    - Operation schedule interval

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgGetParametersReply>");

      //    - Kiosk Functionalities (only enabled)
      _sb.AppendLine("<EnabledFunctionalities>");
      foreach (Int32 _id_function in Functionalities)
      {
        _sb.Append("<FunctionId>" + _id_function.ToString() + "</FunctionId>");
      }
      _sb.AppendLine("</EnabledFunctionalities>");

      //    - Kiosk Customized Images
      _sb.AppendLine("<CustomizedImages>");
      _sb.Append(Images.ToXml());
      _sb.AppendLine("</CustomizedImages>");

      _sb.AppendLine("<m_resources>");
      _sb.Append(m_resources.ToXml());
      _sb.AppendLine("</m_resources>");

      //    - Customized Messages
      _sb.AppendLine("<CustomizedMessages>");
      _sb.Append(m_messages.ToXml());
      _sb.AppendLine("</CustomizedMessages>");

      //    - Operation schedule interval
      _sb.AppendLine("<ScheduledTime>");
      _sb.Append(m_schedule_time.ToXml());
      _sb.AppendLine("</ScheduledTime>");

      //    - Gifts split
      _sb.AppendLine("<GiftsSplit>");
      _sb.Append(m_gifts_split.ToXml());
      _sb.AppendLine("</GiftsSplit>");

      //    - General param
        _sb.AppendLine("<GeneralParams>");
        if (!m_general_params.IsEmpty())
        {        
          _sb.Append(m_general_params.ToXml());
        }
        _sb.AppendLine("</GeneralParams>");
      
      


      _sb.AppendLine("</WCP_WKT_MsgGetParametersReply>");

      return _sb.ToString();

    } //ToXml

    public void LoadXml(XmlReader Xml)
    {
      Functionalities = new List<int>();
      Images = new WCP_WKT.WKT_Images();
      m_resources = new WCP_WKT.WKT_ResourceInfoList();
      m_messages = new WCP_WKT.WKT_MessageList();
      m_schedule_time = new WCP_WKT.WKT_ScheduledTime();
      m_gifts_split = new WCP_WKT.WKT_GiftsSplit();
      m_general_params = new WCP_WKT.WKT_GeneralParamsList();

      if (Xml.Name != "WCP_WKT_MsgGetParametersReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParametersReply not found in content area");
      }

      //    Move reader to <EnabledFunctionalities>
      while (Xml.Name != "EnabledFunctionalities")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParametersReply EnabledFunctionalities element missing 1");
        }
      }
      while (!((Xml.NodeType.Equals(XmlNodeType.EndElement) || Xml.IsEmptyElement) && Xml.Name == "EnabledFunctionalities"))
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParametersReply EnabledFunctionalities element missing 2");
        }
        else
        {
          if (Xml.NodeType.Equals(XmlNodeType.Element) && Xml.Name == "FunctionId")
          {
            Functionalities.Add(Int32.Parse(XML.GetValue(Xml, "FunctionId")));
            Xml.Read();    // Consume </FunctionId>
          }
        }
      }

      //    Move reader to <CustomizedImages>
      while (Xml.Name != "CustomizedImages")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParametersReply CustomizedImages element missing 1");
        }
      }
      Images.LoadXml(Xml);

      //    Move reader to <m_resources>
      while (Xml.Name != "m_resources")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParametersReply m_resources Images element missing");
        }
      }
      // Load Reusources
      m_resources.LoadXml(Xml);

      //    Move reader to <CustomizedMessages>
      while (Xml.Name != "CustomizedMessages")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParametersReply CustomizedMessages element missing");
        }
      }
      // Load CustomizedMessages
      m_messages.LoadXml(Xml);

      //    Move reader to <ScheduledTime>
      while (Xml.Name != "ScheduledTime")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParametersReply ScheduledTime element missing");
        }
      }
      // Load ScheduledTime
      m_schedule_time.LoadXml(Xml);

      //    Move reader to <GiftsSplit>
      while (Xml.Name != "GiftsSplit")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParametersReply GiftsSplit element missing");
        }
      }
      // Load ScheduledTime
      m_gifts_split.LoadXml(Xml);

      //    Move reader to <GeneralParams>
      while (Xml.Name != "GeneralParams")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetParametersReply GeneralParams element missing");
        }
        // Load GeneralParams
      }
      m_general_params.LoadXml(Xml);

      

    } //LoadXml

    #endregion //IXml Members


    #region IResources Members

    public WCP_WKT.WKT_ResourceInfoList Resources
    {
      get
      {
        return m_resources;
      }
      set
      {
        m_resources = value;
      }
    }

    #endregion

    public WCP_WKT.WKT_MessageList Messages
    {
      get
      {
        return m_messages;
      }
      set
      {
        m_messages = value;
      }
    }

    public WCP_WKT.WKT_ScheduledTime ScheduleTime
    {
      get
      {
        return m_schedule_time;
      }
      set
      {
        m_schedule_time = value;
      }
    }

    public WCP_WKT.WKT_GiftsSplit GiftsSplit
    {
      get
      {
        return m_gifts_split;
      }
      set
      {
        m_gifts_split = value;
      }
    }

    public WCP_WKT.WKT_GeneralParamsList WKT_GeneralParams
    {
      get
      {
        return m_general_params;
      }
      set
      {
        m_general_params = value;
      }
    }


  } //WCP_WKT_MsgGetParametersReply 
}