//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgGetResource.cs
// 
//   DESCRIPTION: WCP_WKT_MsgGetResource class
// 
//        AUTHOR: Susana Sams�
// 
// CREATION DATE: 14-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAY-2012 SSC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_WKT_MsgGetResource : IXml
  {
    public Int64 WktResourceId;
    public Int32 Offset;
    public Int32 ChunkDataLength;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_WKT_MsgGetResource", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "ResourceId", WktResourceId.ToString());

      if (Offset != 0 || ChunkDataLength != 0)
      {
        XML.AppendChild(node, "Offset", Offset.ToString());
        XML.AppendChild(node, "ChunkDataLength", ChunkDataLength.ToString());
      }

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_WKT_MsgGetResource")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetResource not found in content area");
      }

      WktResourceId = Int64.Parse(XML.GetValue(reader, "ResourceId"));

      if (XML.GetValue(reader, "Offset",true) != null && XML.GetValue(reader, "Offset",true) != "")
      {
        Offset = Int32.Parse(XML.GetValue(reader, "Offset", true));
        ChunkDataLength = Int32.Parse(XML.GetValue(reader, "ChunkDataLength", true));
      }

    } // LoadXml

  } // WCP_WKT_MsgGetResource

  public class WCP_WKT_MsgGetResourceReply : IXml
  {
    public Int64 WktResourceId;
    public String Extension;
    public Byte[] Hash;
    public Int32 DataLength;
    public Int32 Offset;
    public Int32 ChunkDataLength;
    public Byte[] ChunkData;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_WKT_MsgGetResourceReply", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "ResourceId", WktResourceId.ToString());
      XML.AppendChild(node, "Extension", Extension);
      if (Hash != null)
      {
        XML.AppendChild(node, "Hash", Convert.ToBase64String(Hash));
      }
      else
      {
        XML.AppendChild(node, "Hash", "");
      }
      XML.AppendChild(node, "DataLength", DataLength.ToString());
      XML.AppendChild(node, "Offset", Offset.ToString());
      XML.AppendChild(node, "ChunkDataLength", ChunkDataLength.ToString());

      if (ChunkDataLength > 0 && ChunkData != null)
      {
        XML.AppendChild(node, "ChunkData", Convert.ToBase64String(ChunkData, 0, ChunkDataLength));
      }  
     
      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_WKT_MsgGetResourceReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetResourceReply not found in content area");
      }
      else
      {
        WktResourceId = Int64.Parse(XML.GetValue(reader, "ResourceId"));
        Extension = XML.GetValue(reader, "Extension");
        Hash = Convert.FromBase64String(XML.GetValue(reader, "Hash"));
        DataLength = Int32.Parse(XML.GetValue(reader, "DataLength"));
        Offset = Int32.Parse(XML.GetValue(reader, "Offset"));    
        ChunkDataLength = Int32.Parse(XML.GetValue(reader, "ChunkDataLength"));

        if (ChunkDataLength > 0)
        {
          ChunkData = Convert.FromBase64String(XML.GetValue(reader, "ChunkData"));
        }

      }
    } // LoadXml

  } // WCP_WKT_MsgGetResourceReply

} // namespace WSI.WCP
