//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgPrintRechargesList.cs
// 
//   DESCRIPTION: WCP_WKT_MsgPrintRechargesList class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 29-OCT-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-OCT-2012 JMM    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_WKT_MsgPrintRechargesList : IXml, WCP_WKT.IExtendedPlayer
  {
    public WCP_WKT.WKT_Player Player;
    public List<double> NotesList;
    public double TotalRecharges;

    public WCP_WKT_MsgPrintRechargesList()
    {
      this.Player = new WCP_WKT.WKT_Player();
    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgPrintRechargesList>");
      _sb.Append("<Pin>" + XML.StringToXmlValue(Player.Pin) + "</Pin>");
      _sb.Append("<Trackdata>" + XML.StringToXmlValue(Player.Trackdata) + "</Trackdata>");

      _sb.Append("<TotalRecharges>" + TotalRecharges.ToString() + "</TotalRecharges>");

      if (NotesList.Count > 0)
      {
        _sb.Append("<NotesList>");

        foreach (Int32 _note_value in NotesList)
        {
          _sb.Append("<Note>");
          _sb.Append("<Value>" + _note_value.ToString() + "</Value>");
          _sb.AppendLine("</Note>");
        }

        _sb.AppendLine("</NotesList>");
      }      
      
      _sb.AppendLine("</WCP_WKT_MsgPrintRechargesList>");
      return _sb.ToString();

    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      String _str_note_value;      

      this.NotesList = new List<double>();

      if (reader.Name != "WCP_WKT_MsgPrintRechargesList")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgPrintRechargesList not found in content area");
      }

      Player.Pin = XML.GetValue(reader, "Pin");
      Player.Trackdata = XML.GetValue(reader, "Trackdata");

      _str_note_value = XML.GetValue(reader, "TotalRecharges");

      if (_str_note_value != "")
      {
        TotalRecharges = double.Parse(_str_note_value);
      }

      while (!((reader.Name == "WCP_WKT_MsgPrintRechargesList")&& reader.NodeType == XmlNodeType.EndElement))
      {
        reader.Read();

        if ((reader.Name == "Note") && (reader.NodeType != XmlNodeType.EndElement))
        {           
          _str_note_value = XML.ReadTagValue(reader, "Value");
          this.NotesList.Add(double.Parse(_str_note_value));          
        }
      }
    } // LoadXml


    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get
      {
        return this.Player;
      }
      set
      {
        this.Player = value;
      }
    }

    #endregion
  } // WCP_WKT_MsgPrintRechargesList

  public class WCP_WKT_MsgPrintRechargesListReply : IXml, WCP_WKT.IVouchers
  {
    private String _deflated_voucher;
    private String _inflated_voucher;

    private ArrayList _voucherList;

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgPrintRechargesListReply>");
      _sb.AppendLine("<Vouchers>");
      foreach (Voucher _voucher in VoucherList)
      {
        Misc.DeflateString(_voucher.VoucherHTML.ToString(), out _deflated_voucher);
        _sb.Append("<Voucher>" + XML.StringToXmlValue(_deflated_voucher) + "</Voucher>");
      }
      _sb.AppendLine("</Vouchers>");
      _sb.AppendLine("</WCP_WKT_MsgPrintRechargesListReply>");

      return _sb.ToString();

    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      VoucherList = new ArrayList();

      if (reader.Name != "WCP_WKT_MsgPrintRechargesListReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgPrintRechargesListReply not found in content area");
      }
      else
      {
        while ((!(reader.Name == "Vouchers" && reader.NodeType == XmlNodeType.EndElement)) && (!(reader.Name == "WCP_WKT_MsgPrintRechargesListReply" && reader.NodeType == XmlNodeType.EndElement)))
        {
          reader.Read();
          if ((reader.Name == "Voucher") && (reader.NodeType != XmlNodeType.EndElement))
          {
            _deflated_voucher = XML.GetValue(reader, "Voucher");
            Misc.InflateString(_deflated_voucher, out _inflated_voucher);
            VoucherList.Add(_inflated_voucher);
          }
        }
      }
    } // LoadXml


    #region IVoucher Members

    public ArrayList VoucherList
    {
      get
      {
        return _voucherList;
      }
      set
      {
        this._voucherList = value;
      }
    }

    #endregion
  } // WCP_WKT_MsgPrintRechargesListReply

} // namespace WSI.WCP
