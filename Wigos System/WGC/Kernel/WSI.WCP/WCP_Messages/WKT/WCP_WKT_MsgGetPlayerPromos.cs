//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgGetPlayerPromos
// 
//   DESCRIPTION: WCP_WKT_MsgGetPlayerPromos class
// 
//        AUTHOR: Xavier Iglesia 
// 
// CREATION DATE: 01-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2012 XIT    First version.
// 09-DEC-2014 JMM    Added by recharges promotions to the promotion list sent to the WKT
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_WKT_MsgGetPlayerPromos : IXml, WCP_WKT.IExtendedPlayer
  {
    public WCP_WKT.WKT_Player Player;

    public WCP_WKT_MsgGetPlayerPromos()
    {
      this.Player = new WCP_WKT.WKT_Player();
    }

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.Append("<WCP_WKT_MsgGetPlayerPromos>");
      _sb.Append(Player.ToXml());
      _sb.Append("</WCP_WKT_MsgGetPlayerPromos>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_WKT_MsgGetPlayerPromos")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetPlayerPromos not found in content area");
      }
      Player.Pin = XML.ReadTagValue(Xml, "Pin");
      Player.Trackdata = XML.ReadTagValue(Xml, "TrackData");

    }
    #endregion

    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get { return this.Player; }
      set { this.Player = value; }
    }

    #endregion
  }


  public class WCP_WKT_MsgGetPlayerPromosReply : IXml, WCP_WKT.IResources
  {
    public List<WCP_WKT.WKT_Promo>[] Promos;
    public List<WCP_WKT.WKT_ApplicablePromo> ApplicablePromos;
    private WCP_WKT.WKT_ResourceInfoList  m_resources;


    public WCP_WKT_MsgGetPlayerPromosReply()
    {
      Promos = new List<WCP_WKT.WKT_Promo>[3];
      Promos[0] = new List<WCP_WKT.WKT_Promo>();
      Promos[1] = new List<WCP_WKT.WKT_Promo>();
      Promos[2] = new List<WCP_WKT.WKT_Promo>();
      ApplicablePromos = new List<WCP_WKT.WKT_ApplicablePromo>();
      Resources = new WCP_WKT.WKT_ResourceInfoList();
    }

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;
      
      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgGetPlayerPromosReply>");
      _sb.AppendLine("<Promotions>");
      _sb.Append(ListPromotionsToXml("CurrentPromotions", Promos[0]));
      _sb.Append(ListPromotionsToXml("NextPromotions", Promos[1]));
      _sb.Append(ListPromotionsToXml("FuturePromotions", Promos[2]));
      _sb.Append(ListApplicablePromotionsToXml("ApplicablePromotions", ApplicablePromos));
      _sb.AppendLine("</Promotions>");
      _sb.AppendLine("<Resources>");
      _sb.Append(Resources.ToXml());
      _sb.AppendLine("</Resources>");
      _sb.AppendLine("</WCP_WKT_MsgGetPlayerPromosReply>");

      return _sb.ToString();
    }

    private String ListPromotionsToXml(string Name, List<WCP_WKT.WKT_Promo> ListPromos)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.AppendLine("<" + Name + ">");

      if (ListPromos != null)
      {
        foreach (WCP_WKT.WKT_Promo _promo in ListPromos)
        {
          _sb.AppendLine("<Promotion>");
          _sb.Append("<Id>" + _promo.Id.ToString() + "</Id>");
          _sb.Append("<Name>" + XML.StringToXmlValue(_promo.Name) + "</Name>");
          _sb.Append("<Description>" + XML.StringToXmlValue(_promo.Description) + "</Description>");
          _sb.Append("<SmallResourceId>" + _promo.SmallResourceId.ToString() + "</SmallResourceId>");
          _sb.Append("<LargeResourceId>" + _promo.LargeResourceId.ToString() + "</LargeResourceId>");
          _sb.AppendLine("</Promotion>");
        }
      }

      _sb.AppendLine("</" + Name + ">");

      return _sb.ToString();
    }

    private String ListApplicablePromotionsToXml(string Name, List<WCP_WKT.WKT_ApplicablePromo> ListPromos)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.AppendLine("<" + Name + ">");
      foreach (WCP_WKT.WKT_ApplicablePromo _promo in ListPromos)
      {
        _sb.AppendLine("<Promotion>");
        _sb.Append("<Id>" + _promo.Id.ToString() + "</Id>");
        _sb.Append("<Name>" + XML.StringToXmlValue(_promo.Name) + "</Name>");
        _sb.Append("<Description>" + XML.StringToXmlValue(_promo.Description) + "</Description>");
        _sb.Append("<SmallResourceId>" + _promo.SmallResourceId.ToString() + "</SmallResourceId>");
        _sb.Append("<LargeResourceId>" + _promo.LargeResourceId.ToString() + "</LargeResourceId>");
        _sb.Append("<CreditType>" + _promo.PromoCreditType.ToString() + "</CreditType>"); //UNKNOWN=0; NR1=1; NR2=2; REDEEMABLE=3; POINT=4 
        _sb.Append("<MinSpent>" + ((Int64)((Decimal)_promo.MinSpent * 100)).ToString() + "</MinSpent>");
        _sb.Append("<MinSpentReward>" + ((Int64)((Decimal)_promo.MinSpentReward * 100)).ToString() + "</MinSpentReward>");
        _sb.Append("<Spent>" + ((Int64)((Decimal)_promo.Spent * 100)).ToString() + "</Spent>");
        _sb.Append("<SpentReward>" + ((Int64)((Decimal)_promo.SpentReward * 100)).ToString() + "</SpentReward>");
        _sb.Append("<CurrentReward>" + ((Int64)((Decimal)_promo.CurrentReward * 100)).ToString() + "</CurrentReward>");
        _sb.Append("<RechargesReward>" + ((Int64)((Decimal)_promo.RechargesReward * 100)).ToString() + "</RechargesReward>");        
        _sb.Append("<AvailableLimit>" + ((Int64)((Decimal)_promo.AvailableLimit * 100)).ToString() + "</AvailableLimit>");
        _sb.AppendLine("</Promotion>");
      }
      _sb.AppendLine("</" + Name + ">");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_WKT_MsgGetPlayerPromosReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                "Tag WCP_WKT_MsgGetPlayerPromosReply not found in content area");
      }

      Promos[0] = LoadXmlListPromos(Xml, "CurrentPromotions");
      Promos[1] = LoadXmlListPromos(Xml, "NextPromotions");
      Promos[2] = LoadXmlListPromos(Xml, "FuturePromotions");
      ApplicablePromos = LoadXmlListApplicablePromos(Xml, "ApplicablePromotions");

      // Move reader to starting resources list node
      while (Xml.Name != "Resources")
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                  "Tag WCP_WKT_MsgGetPlayerPromosReply Resources Promos element missing");
        }
      }

      Resources.LoadXml(Xml);
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Gets the promotions list encapsulated inside the tag
    //  PARAMS:
    //      - INPUT:
    //          - XmlReader Xml    
    //          - String ListTag : Tag used to delimite the objects of the list
    //
    //      - OUTPUT:
    //         
    //
    //      - RETURNS: List<WCP_WKT.WKT_Promo> : List with the promotions
    //       
    //
    private List<WCP_WKT.WKT_Promo> LoadXmlListPromos(XmlReader Xml,String ListTag)
    {
      List <WCP_WKT.WKT_Promo> _list_promos;
      WCP_WKT.WKT_Promo _promo;

      _list_promos = new List<WCP_WKT.WKT_Promo>();

      while ((!(Xml.Name == ListTag && Xml.NodeType == XmlNodeType.EndElement)) && (!(Xml.Name == "Promotions" && Xml.NodeType == XmlNodeType.EndElement)))
      {
        Xml.Read();
        if ((Xml.Name == "Promotion") && (Xml.NodeType != XmlNodeType.EndElement))
        {
          _promo = new WCP_WKT.WKT_Promo();

          _promo.Id =   Int64.Parse(XML.ReadTagValue(Xml, "Id"));
          _promo.Name = XML.ReadTagValue(Xml, "Name");
          _promo.Description = XML.ReadTagValue(Xml, "Description");
          _promo.SmallResourceId = Int64.Parse(XML.ReadTagValue(Xml, "SmallResourceId"));
          _promo.LargeResourceId = Int64.Parse(XML.ReadTagValue(Xml, "LargeResourceId"));

          _list_promos.Add(_promo);
        }
      }

      return _list_promos;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Gets the applicable promotions list encapsulated inside the tag
    //  PARAMS:
    //      - INPUT:
    //          - XmlReader Xml    
    //          - String ListTag : Tag used to delimite the objects of the list
    //
    //      - OUTPUT:
    //         
    //
    //      - RETURNS: List<WCP_WKT.WKT_ApplicablePromo> : List with the applicable promotions
    //       
    //
    private List<WCP_WKT.WKT_ApplicablePromo> LoadXmlListApplicablePromos(XmlReader Xml, String ListTag)
    {
      List<WCP_WKT.WKT_ApplicablePromo> _list_promos;
      WCP_WKT.WKT_ApplicablePromo _promo;

      _list_promos = new List<WCP_WKT.WKT_ApplicablePromo>();

      while ((!(Xml.Name == ListTag && Xml.NodeType == XmlNodeType.EndElement)) && (!(Xml.Name == "Promotions" && Xml.NodeType == XmlNodeType.EndElement)))
      {
        Xml.Read();
        if ((Xml.Name == "Promotion") && (Xml.NodeType != XmlNodeType.EndElement))
        {
          _promo = new WCP_WKT.WKT_ApplicablePromo();

          _promo.Id = Int64.Parse(XML.ReadTagValue(Xml, "Id"));
          _promo.Name = XML.ReadTagValue(Xml, "Name");
          _promo.Description = XML.ReadTagValue(Xml, "Description");
          _promo.SmallResourceId = Int64.Parse(XML.ReadTagValue(Xml, "SmallResourceId"));
          _promo.LargeResourceId = Int64.Parse(XML.ReadTagValue(Xml, "LargeResourceId"));
          _promo.PromoCreditType = (ACCOUNT_PROMO_CREDIT_TYPE)Enum.Parse(typeof(ACCOUNT_PROMO_CREDIT_TYPE), XML.ReadTagValue(Xml, "CreditType"));
          _promo.MinSpent = ((Decimal)Int64.Parse(XML.GetValue(Xml, "MinSpent")));
          _promo.MinSpentReward = ((Decimal)Int64.Parse(XML.GetValue(Xml, "MinSpentReward")));
          _promo.Spent = ((Decimal)Int64.Parse(XML.GetValue(Xml, "Spent")));
          _promo.SpentReward = ((Decimal)Int64.Parse(XML.GetValue(Xml, "SpentReward")));
          _promo.CurrentReward = ((Decimal)Int64.Parse(XML.GetValue(Xml, "CurrentReward")));

          Xml.Read();
          Xml.Read();
          if (Xml.Name == "RechargesReward")
          {
            _promo.RechargesReward = ((Decimal)Int64.Parse(XML.GetValue(Xml, "RechargesReward")));
          }

          _promo.AvailableLimit = ((Decimal)Int64.Parse(XML.GetValue(Xml, "AvailableLimit")));

          _list_promos.Add(_promo);
        }
      }

      return _list_promos;
    }

    #endregion


    #region IResources Members

    public WCP_WKT.WKT_ResourceInfoList Resources
    {
      get { return m_resources; }
      set
      {
        m_resources = value;
      }
    }
    #endregion

  }


}
