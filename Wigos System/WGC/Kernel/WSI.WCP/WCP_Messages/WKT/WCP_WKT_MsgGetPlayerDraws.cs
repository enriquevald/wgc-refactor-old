//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgGetPlayerDraws
// 
//   DESCRIPTION: WCP_WKT_MsgGetPlayerDraws class
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-APR-2013 JMM    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  // Player draw list
  //    - WCP_WKT_MsgGetPlayerDraws :       Pin + Trackdata to get list of draws
  //    - WCP_WKT_MsgGetPlayerDrawsReply :  List of draws containing the amount of pending numbers for each draw

  //    - WCP_WKT_MsgGetPlayerDraws :       Pin + Trackdata to get list of draws
  public class WCP_WKT_MsgGetPlayerDraws : IXml, WCP_WKT.IExtendedPlayer
  {
    public string MSG_NAME = typeof (WCP_WKT_MsgGetPlayerDraws).Name;

    public WCP_WKT.WKT_Player m_player;

    public WCP_WKT_MsgGetPlayerDraws()
    {
      this.m_player = new WCP_WKT.WKT_Player ();
    }

    #region IXml Members

    public String ToXml ()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine ("<" + MSG_NAME + ">");
      _sb.Append (m_player.ToXml ());
      _sb.AppendLine ("</" + MSG_NAME + ">");

      return _sb.ToString();
    }

    public void LoadXml (XmlReader Reader)
    {
      if ( Reader.Name != MSG_NAME )
      {
        throw new WCP_Exception (WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                 "Tag " + MSG_NAME + " not found in content area");
      }

      m_player.Pin       = XML.ReadTagValue (Reader, "Pin");
      m_player.Trackdata = XML.ReadTagValue (Reader, "TrackData");
    }
    #endregion

    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get { return this.m_player; }
      set { this.m_player = value; }
    }

    #endregion
  }

  //    - WCP_WKT_MsgGetPlayerDrawsReply :  List of draws containing the amount of pending numbers for each draw
  public class WCP_WKT_MsgGetPlayerDrawsReply : IXml
  {
    public string MSG_NAME = typeof (WCP_WKT_MsgGetPlayerDrawsReply).Name;

    public List<WCP_WKT.WKT_Draw> Draws;

    public WCP_WKT_MsgGetPlayerDrawsReply()
    {
      this.Draws = new List<WCP_WKT.WKT_Draw>();
    }

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<" + MSG_NAME + ">");

      _sb.AppendLine("<Draws>");
      foreach (WCP_WKT.WKT_Draw _draw in Draws)
      {
        _sb.AppendLine("<Draw>");
        _sb.Append("<DrawID>" + _draw.DrawID + "</DrawID>");
        _sb.Append("<Name>" + XML.StringToXmlValue(_draw.Name) + "</Name>");
        _sb.Append ("<PendingNumbers>" + _draw.PendingNumbers + "</PendingNumbers>");
        _sb.Append ("<RealPendingNumbers>" + _draw.RealPendingNumbers + "</RealPendingNumbers>");
        _sb.AppendLine ("</Draw>");
      }
      _sb.AppendLine("</Draws>");
      _sb.AppendLine ("</" + MSG_NAME + ">");

      return _sb.ToString();
    }

    public void LoadXml (XmlReader Reader)
    {
      WCP_WKT.WKT_Draw _draw;
      this.Draws = new List<WCP_WKT.WKT_Draw>();

      if ( Reader.Name != MSG_NAME )
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                "Tag " + MSG_NAME + " not found in content area");
      }

      while ( !( ( Reader.Name == MSG_NAME ) && Reader.NodeType == XmlNodeType.EndElement ) )
      {
        Reader.Read();

        if ( Reader.Name == "Draw" && Reader.NodeType != XmlNodeType.EndElement )
        {
          _draw = new WCP_WKT.WKT_Draw();          
          _draw.DrawID = Int32.Parse(XML.GetValue(Reader, "DrawID"));
          _draw.Name = XML.ReadTagValue(Reader, "Name");
          _draw.PendingNumbers = Int64.Parse (XML.GetValue (Reader, "PendingNumbers"));
          _draw.RealPendingNumbers = Int64.Parse (XML.GetValue (Reader, "RealPendingNumbers"));
          this.Draws.Add (_draw);
        }
      }
    }

    #endregion
  }
}

