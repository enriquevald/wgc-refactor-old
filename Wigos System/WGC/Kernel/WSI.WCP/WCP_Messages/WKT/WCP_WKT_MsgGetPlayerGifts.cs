//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgPlayerRequestGift.cs
// 
//   DESCRIPTION: WCP_WKT_MsgPlayerRequestGift class
// 
//        AUTHOR: Joaquim Cid 
// 
// CREATION DATE: 23-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAY-2012 JCM    First version.
// 10-MAY-2013 JMA    Changed queries and other features to limit the amount of gifts redemptions
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using WSI.Common;
using System.IO;

namespace WSI.WCP
{
  public class WCP_WKT_MsgGetPlayerGifts : IXml, WCP_WKT.IExtendedPlayer
  {
    public WCP_WKT.WKT_Player Player;

    public WCP_WKT_MsgGetPlayerGifts()
    {
      Player = new WCP_WKT.WKT_Player();
    }


    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.Append("<WCP_WKT_MsgGetPlayerGifts>");
      _sb.Append(Player.ToXml());
      _sb.Append("</WCP_WKT_MsgGetPlayerGifts>");

      return _sb.ToString();
    } // ToXml
    
    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_WKT_MsgGetPlayerGifts")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetPlayerGifts not found in content area");
      }
      Player.LoadXml(Reader);

    } // LoadXml

    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get { return this.Player; }
      set { this.Player = value; }
    }

    #endregion

  } // WCP_WKT_MsgGetPlayerGifts

  public class WCP_WKT_MsgGetPlayerGiftsReply : IXml, WCP_WKT.IResources
  {
    public DataSet Gifts;
    private WCP_WKT.WKT_ResourceInfoList m_resources;


    public WCP_WKT_MsgGetPlayerGiftsReply()
    {
      InitGifts();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Init Gifts DataSet with schema.
    //
    //  PARAMS:
    //      - INPUT:
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void InitGifts()
    {
      DataTable _tbl_gift;

      Gifts = new DataSet();

      foreach (String _table_name in new String[] { "RedeemeablesGifts", "NearRedeemableGifts" })
      {
        _tbl_gift = new DataTable(_table_name);

        _tbl_gift.Columns.Add("GI_GIFT_ID", Type.GetType("System.Int64"));
        _tbl_gift.Columns.Add("GI_POINTS", Type.GetType("System.Decimal"));
        _tbl_gift.Columns.Add("GI_NAME", Type.GetType("System.String"));
        _tbl_gift.Columns.Add("GI_DESCRIPTION", Type.GetType("System.String"));
        _tbl_gift.Columns.Add("GI_SMALL_RESOURCE_ID", Type.GetType("System.Int64"));
        _tbl_gift.Columns.Add("GI_LARGE_RESOURCE_ID", Type.GetType("System.Int64"));
        _tbl_gift.Columns.Add("GI_TYPE", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_CURRENT_STOCK", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("MAX_ALLOWED_UNITS", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_NON_REDEEMABLE", Type.GetType("System.Decimal")); 
        _tbl_gift.Columns.Add("GI_DRAW_ID", Type.GetType("System.Int64"));

        _tbl_gift.Columns.Add("GI_ACCOUNT_DAILY_LIMIT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_ACCOUNT_MONTHLY_LIMIT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_GLOBAL_DAILY_LIMIT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_GLOBAL_MONTHLY_LIMIT", Type.GetType("System.Int32"));

        _tbl_gift.Columns.Add("REQ_ACCOUNT_DAILY_TOTAL_SPENT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("REQ_ACCOUNT_MONTHLY_TOTAL_SPENT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("REQ_GLOBAL_DAILY_TOTAL_SPENT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("REQ_GLOBAL_MONTHLY_TOTAL_SPENT", Type.GetType("System.Int32"));

        Gifts.Tables.Add(_tbl_gift);
      }

    } //InitGifts

    public DataRow GetPlayerGift(Int32 IdxGift, Int32 IdxTable)
    {
      return Gifts.Tables[IdxTable].Rows[IdxGift];
    }


    public String ToXml()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_WKT_MsgGetPlayerGiftsReply>");

      _sb.AppendLine("<RedeemeablesGifts>");

      _sb.Append(GetListGifts(Gifts.Tables[0]));
      _sb.AppendLine("</RedeemeablesGifts>");

      _sb.AppendLine("<NearRedeemableGifts>");
      _sb.Append(GetListGifts(Gifts.Tables[1]));
      _sb.AppendLine("</NearRedeemableGifts>");

      _sb.AppendLine("<Resources>");
      _sb.Append(Resources.ToXml());
      _sb.AppendLine("</Resources>");

      _sb.AppendLine("</WCP_WKT_MsgGetPlayerGiftsReply>");

      return _sb.ToString();

    } // ToXml


    private String GetListGifts(DataTable Table)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      foreach (DataRow _dr in Table.Rows)
      {
        _sb.AppendLine("<Gift>");
        _sb.Append("<GiftId>");
        _sb.Append(_dr[0].ToString() != "0" ? _dr[0].ToString() : _dr[10].ToString());
        _sb.Append("</GiftId>");
        _sb.AppendLine("<Points>" + ((Int64)((Decimal)_dr[1] * 100)).ToString() + "</Points>");
        _sb.AppendLine("<Name>" + XML.StringToXmlValue(_dr[2].ToString()) + "</Name>");
        _sb.AppendLine("<Description>" + XML.StringToXmlValue(_dr[3].ToString()) + "</Description>");
        _sb.AppendLine("<SmallResourceId>" + _dr[4].ToString() + "</SmallResourceId>");
        _sb.AppendLine("<LargeResourceId>" + _dr[5].ToString() + "</LargeResourceId>");
        _sb.AppendLine("<Type>" + _dr[6].ToString() + "</Type>");
        _sb.AppendLine("<Stock>" + _dr[7].ToString() + "</Stock>");
        _sb.AppendLine("<MaximumUnits>" + _dr[8].ToString() + "</MaximumUnits>");
        _sb.AppendLine("<NumCredits>" + ((Int64)((Decimal)_dr[9] * 100)).ToString() + "</NumCredits>");
        _sb.AppendLine("</Gift>");
      }

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Reader)
    {
      Int64 _idx_parser;
      
      InitGifts();
      
      //Init Resources List
      Resources = new WCP_WKT.WKT_ResourceInfoList();

      // Verify is correct XML message                  
      if (Reader.Name != "WCP_WKT_MsgGetPlayerGiftsReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetPlayerGiftsReply not found in content area");
      }

      //Read Redeem and Near Redeem Gifts
      foreach (DataTable _table in Gifts.Tables)
      {
        // Move reader to RedeemableGifts Parent Node
        while (Reader.Name != _table.TableName)
        {
          if (!Reader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                    "Tag WCP_WKT_MsgGetPlayerGiftsReply " + _table.TableName + " element missing");
          }
        }

        // While not end of current node, read
        while (
          !((Reader.NodeType.Equals(XmlNodeType.EndElement) || Reader.IsEmptyElement) && Reader.Name == _table.TableName))
        {
          if (!Reader.Read())
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                    "Tag WCP_WKT_MsgGetPlayerGiftsReply " + _table.TableName + " missing");
          }
          else
          {

            if (Reader.Name == "GiftId" && Reader.NodeType.Equals(XmlNodeType.Element))
            {
              DataRow _row;
              _row = _table.NewRow();
              _row[0] = Int64.Parse(XML.GetValue(Reader, "GiftId"));

              _row[1] = ((Decimal)Int64.Parse(XML.GetValue(Reader, "Points"))) / 100;

              _row[2] = XML.GetValue(Reader, "Name");
              _row[3] = XML.GetValue(Reader, "Description");

              if (Int64.TryParse(XML.GetValue(Reader, "SmallResourceId"), out _idx_parser))
              {
                _row[4] = _idx_parser;
              }
              else
              {
                _row[4] = 0;
              }

              if (Int64.TryParse(XML.GetValue(Reader, "LargeResourceId"), out _idx_parser))
              {
                _row[5] = _idx_parser;
              }
              else
              {
                _row[5] = 0;
              }

              _row[6] = Int32.Parse(XML.GetValue(Reader, "Type"));
              _row[7] = Int32.Parse(XML.GetValue(Reader, "Stock"));
              _row[8] = Int32.Parse(XML.GetValue(Reader, "MaximumUnits"));
              _row[9] = ((Decimal)Int64.Parse(XML.GetValue(Reader, "NumCredits"))) / 100;
              _table.Rows.Add(_row);
            }
          }
        }
      }

      // Move reader to starting resources list node
      while (Reader.Name != "Resources")
      {
        if (!Reader.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetPlayerGiftsReply Resources Gifts element missing");
        }
      }

      // Load Reusources
      Resources.LoadXml(Reader);

    } // LoadXml


    #region IResources Members

    #endregion


    #region IResources Members

    public WCP_WKT.WKT_ResourceInfoList Resources
    {
      get
      {
        return m_resources; 
      }
      set
      {
        m_resources = value;
      }
    }

    #endregion
  } //WCP_WKT_MsgGetPlayerGiftsReply
}