//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgPlayerRequestGift.cs
// 
//   DESCRIPTION: WCP_WKT_MsgPlayerRequestGift class
// 
//        AUTHOR: Susana Sams�
// 
// CREATION DATE: 21-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2012 SSC    First version.
// 08-AUG-2012 XIT    Added type player, removed balance and points, appendchild-->stringbuilder
// 12-SEP-2016 ETP    PBI 17561: Print Tito Ticket in Promobox Gifts.
// 03-OCT-2016 ETP    Fixed Bug 18380: LCD Intouch: Gift delivery error.
// 01-DIC-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_WKT_MsgPlayerRequestGift : IXml, WCP_WKT.IExtendedPlayer
  {

    public WCP_WKT.WKT_Player Player;
    public Int64 GiftId;
    public Decimal GiftPrice;
    public Int32 GiftNumUnits;
    public Int64 DrawId;
    public Boolean TitoPrinterIsReady;

    public WCP_WKT_MsgPlayerRequestGift()
    {
      this.Player = new WCP_WKT.WKT_Player();
    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgPlayerRequestGift>");
      _sb.Append("<Pin>" + XML.StringToXmlValue(Player.Pin) + "</Pin>");
      _sb.Append("<Trackdata>" + XML.StringToXmlValue(Player.Trackdata) + "</Trackdata>");
      _sb.Append("<GiftId>" + GiftId.ToString() + "</GiftId>");
      _sb.Append("<GiftPrice>" + GiftPrice.ToString() + "</GiftPrice>");
      _sb.Append("<GiftNumUnits>" + GiftNumUnits.ToString() + "</GiftNumUnits>");
      _sb.Append("<DrawId>" + DrawId.ToString() + "</DrawId>");
      _sb.Append("<TitoPrinterIsReady>" + (TitoPrinterIsReady ? "1" : "0") + "</TitoPrinterIsReady>");
      _sb.AppendLine("</WCP_WKT_MsgPlayerRequestGift>");
      return _sb.ToString();

    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_WKT_MsgPlayerRequestGift")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgPlayerRequestGift not found in content area");
      }
      Player.Pin = XML.GetValue(reader, "Pin");
      Player.Trackdata = XML.GetValue(reader, "Trackdata");

      GiftId = Int64.Parse(XML.GetValue(reader, "GiftId"));
      GiftPrice = Decimal.Parse(XML.GetValue(reader, "GiftPrice"));
      GiftNumUnits = Int32.Parse(XML.GetValue(reader, "GiftNumUnits"));
      DrawId = Int32.Parse(XML.GetValue(reader, "DrawId"));

      while ((reader.Name != "TitoPrinterIsReady" || !reader.NodeType.Equals(XmlNodeType.Element))
        && !reader.EOF)
      {
        reader.Read();  // Read until End Element 'TitoPrinterIsReady'
      }

      if (reader.Name == "TitoPrinterIsReady")
      {
        TitoPrinterIsReady = XML.ReadTagValue(reader, "TitoPrinterIsReady") == "1";
      }

    } // LoadXml


    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get
      {
        return this.Player;
      }
      set
      {
        this.Player = value;
      }
    }

    #endregion
  } // WCP_WKT_MsgPlayerRequestGift

  public class WCP_WKT_MsgPlayerRequestGiftReply : IXml, WCP_WKT.IVouchers
  {
    public MultiPromos.PromoBalance PromoBalance = new MultiPromos.PromoBalance();

    private String _deflated_voucher;
    private String _inflated_voucher;

    private Ticket _ticket = new Ticket();

    private ArrayList _voucherList;

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgPlayerRequestGiftReply>");
      _sb.AppendLine("<Vouchers>");
      foreach (Voucher _voucher in VoucherList)
      {
        Misc.DeflateString(_voucher.VoucherHTML.ToString(), out _deflated_voucher);
        _sb.Append("<Voucher>" + XML.StringToXmlValue(_deflated_voucher) + "</Voucher>");
      }
      _sb.AppendLine("</Vouchers>");

      _sb.AppendLine("<Points>" + (Convert.ToInt64(Math.Floor(PromoBalance.Points))).ToString() + "</Points>");
      _sb.AppendLine("<Balance>");
      _sb.AppendLine(PromoBalance.Balance.ToXml());
      _sb.AppendLine("</Balance>");

      _sb.AppendLine("<TITOTicket>");
      _sb.AppendLine("<ValidationNumber>" + _ticket.ValidationNumber.ToString() + "</ValidationNumber>");
      _sb.AppendLine("<MachineTicketNumber>" + _ticket.MachineTicketNumber.ToString() + "</MachineTicketNumber>");
      _sb.AppendLine("<TicketType>" + ((Int32)_ticket.TicketType).ToString() + "</TicketType>");
      _sb.AppendLine("<AmountCents>" + ((Int64)(_ticket.Amount * 100)).ToString() + "</AmountCents>");
      _sb.AppendLine("<CreatedDate>" + XML.XmlDateTimeString(_ticket.CreatedDateTime) + "</CreatedDate>");
      _sb.AppendLine("<ExpirationDate>" + XML.XmlDateTimeString(_ticket.ExpirationDateTime) + "</ExpirationDate>");
      _sb.AppendLine("<Address1>" + _ticket.Address1 + "</Address1>");
      _sb.AppendLine("<Address2>" + _ticket.Address2 + "</Address2>");
      _sb.AppendLine("</TITOTicket>");

      _sb.AppendLine("</WCP_WKT_MsgPlayerRequestGiftReply>");

      return _sb.ToString();

    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      Int64 _aux_value;
      VoucherList = new ArrayList();

      if (reader.Name != "WCP_WKT_MsgPlayerRequestGiftReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgPlayerRequestGiftReply not found in content area");
      }
      else
      {
        while ((!(reader.Name == "Vouchers" && reader.NodeType == XmlNodeType.EndElement)) && (!(reader.Name == "WCP_WKT_MsgPlayerRequestGiftReply" && reader.NodeType == XmlNodeType.EndElement)))
        {
          reader.Read();
          if ((reader.Name == "Voucher") && (reader.NodeType != XmlNodeType.EndElement))
          {
            _deflated_voucher = XML.GetValue(reader, "Voucher");
            Misc.InflateString(_deflated_voucher, out _inflated_voucher);
            VoucherList.Add(_inflated_voucher);
          }
        }

        if (Int64.TryParse(XML.GetValue(reader, "Points", true), out _aux_value))
        {
          PromoBalance.Points = Convert.ToDecimal(_aux_value);
        }
        PromoBalance.Balance.LoadXml(reader);

        // read tag <TITOTicket>
        while ((!(reader.Name == "TITOTicket" && reader.NodeType == XmlNodeType.EndElement))
            && (!(reader.Name == "WCP_WKT_MsgPlayerRequestPromoReply" && reader.NodeType == XmlNodeType.EndElement))
            && !reader.EOF)
        {
          reader.Read();
          if ((reader.Name == "TITOTicket") && (reader.NodeType != XmlNodeType.EndElement))
          {
            Ticket.ValidationNumber = XML.GetAsInt64(reader, "ValidationNumber");
            Ticket.MachineTicketNumber = XML.GetAsInt32(reader, "MachineTicketNumber");
            Ticket.TicketType = (TITO_TICKET_TYPE)XML.GetAsInt32(reader, "TicketType");
            Ticket.Amount = ((Decimal)XML.GetAsInt64(reader, "AmountCents")) / 100;
            Ticket.CreatedDateTime = XML.GetAsDateTime(reader, "CreatedDate");
            Ticket.ExpirationDateTime = XML.GetAsDateTime(reader, "ExpirationDate");

            if ((reader.Name == "Address1") && (reader.NodeType != XmlNodeType.EndElement))
            {
              Ticket.Address1 = XML.GetValue(reader, "Address1");
            }

            if ((reader.Name == "Address2") && (reader.NodeType != XmlNodeType.EndElement))
            {
              Ticket.Address2 = XML.GetValue(reader, "Address2");
            }
          }
        }
      }
    } // LoadXml

    #region IVoucher Members

    public ArrayList VoucherList
    {
      get
      {
        return _voucherList;
      }
      set
      {
        this._voucherList = value;
      }
    }

    public Ticket Ticket
    {
      get
      {
        return _ticket;
      }
      set
      {
        this._ticket = value;
      }
    }

    #endregion
  } // WCP_WKT_MsgPlayerRequestGiftReply

} // namespace WSI.WCP
