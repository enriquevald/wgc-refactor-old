//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgGetScript.cs
// 
//   DESCRIPTION: WCP_WKT_MsgGetScript class
// 
//        AUTHOR: Susana Sams�
// 
// CREATION DATE: 15-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-MAY-2012 SSC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_WKT_MsgGetScript : IXml
  {
   
    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_WKT_MsgGetScript", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_WKT_MsgGetResource")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetResource not found in content area");
      }


    } // LoadXml

  } // WCP_WKT_MsgGetScript

  public class WCP_WKT_MsgGetScriptReply : IXml
  {
    //public List<Step> StepList;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_WKT_MsgGetScriptReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_WKT_MsgGetScriptReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetScriptReply not found in content area");
      }
      else
      {
      
      }
    } // LoadXml

  } // WCP_WKT_MsgGetScriptReply

} // namespace WSI.WCP
