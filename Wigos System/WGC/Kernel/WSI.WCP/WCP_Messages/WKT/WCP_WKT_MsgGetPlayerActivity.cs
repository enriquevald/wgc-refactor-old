//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgGetPlayerActivity
// 
//   DESCRIPTION: WCP_WKT_MsgGetPlayerActivity class
// 
//        AUTHOR: Xavier Iglesia 
// 
// CREATION DATE: 14-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-AUG-2012 XIT    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_WKT_MsgGetPlayerActivity : IXml, WCP_WKT.IExtendedPlayer
  {
    public WCP_WKT.WKT_Player Player;

    public WCP_WKT_MsgGetPlayerActivity()
    {
      this.Player = new WCP_WKT.WKT_Player();
    }

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.Append("<WCP_WKT_MsgGetPlayerActivity>");
      _sb.Append(Player.ToXml());
      _sb.Append("</WCP_WKT_MsgGetPlayerActivity>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_WKT_MsgGetPlayerActivity")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetPlayerActivity not found in content area");
      }
      Player.Pin = XML.ReadTagValue(Xml, "Pin");
      Player.Trackdata = XML.ReadTagValue(Xml, "TrackData");

    }
    #endregion

    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get { return this.Player; }
      set { this.Player = value; }
    }

    #endregion
  }


  public class WCP_WKT_MsgGetPlayerActivityReply : IXml
  {
    public List<WCP_WKT.WKT_Activity> Activities;


    public WCP_WKT_MsgGetPlayerActivityReply()
    {
      this.Activities = new List<WCP_WKT.WKT_Activity>();
    }

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;
      
      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgGetPlayerActivityReply>");
      _sb.AppendLine("<Activities>");
      foreach (WCP_WKT.WKT_Activity _activity in Activities)
      {
        _sb.AppendLine("<Activity>");
        _sb.Append("<Date>" + XML.StringToXmlValue(_activity.Date) + "</Date>");
        _sb.Append("<Description>" + XML.StringToXmlValue(_activity.Description) + "</Description>");
        _sb.Append("<Quantity>" + XML.StringToXmlValue(_activity.Quantity) + "</Quantity>");
        _sb.AppendLine("</Activity>");
      }
      _sb.AppendLine("</Activities>");
      _sb.AppendLine("</WCP_WKT_MsgGetPlayerActivityReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      WCP_WKT.WKT_Activity _activity;
      this.Activities = new List<WCP_WKT.WKT_Activity>();

      if (Xml.Name != "WCP_WKT_MsgGetPlayerActivityReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                "Tag WCP_WKT_MsgGetPlayerActivityReply not found in content area");
      }

      while (!((Xml.Name == "WCP_WKT_MsgGetPlayerActivityReply")&& Xml.NodeType == XmlNodeType.EndElement))
      {
        Xml.Read();
        if ((Xml.Name == "Activity") && (Xml.NodeType != XmlNodeType.EndElement))
        {
          _activity = new WCP_WKT.WKT_Activity();
          _activity.Date = XML.ReadTagValue(Xml, "Date");
          _activity.Description = XML.ReadTagValue(Xml, "Description");
          _activity.Quantity = XML.ReadTagValue(Xml, "Quantity");
          this.Activities.Add(_activity);
        }
      }
    }

    #endregion

  }


}
