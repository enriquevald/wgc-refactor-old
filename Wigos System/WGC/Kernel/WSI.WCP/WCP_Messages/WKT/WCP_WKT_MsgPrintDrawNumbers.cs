//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgRequestPlayerDrawNumbers.cs
// 
//   DESCRIPTION: WCP_WKT_MsgRequestPlayerDrawNumbers class
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-MAY-2013 JMM    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  // Player draw numbers request 
  //    - WCP_WKT_MsgRequestPlayerDrawNumbers :       Pin + Trackdata + List of draws to get the numbers for each draw
  //    - WCP_WKT_MsgRequestPlayerDrawNumbersReply :  List of vouchers containing the numbers for each draw

  //    - WCP_WKT_MsgRequestPlayerDrawNumbers :       Pin + Trackdata + List of draws to get the numbers for each draw
  public class WCP_WKT_MsgRequestPlayerDrawNumbers : IXml, WCP_WKT.IExtendedPlayer
  {
    public string MSG_NAME = typeof (WCP_WKT_MsgRequestPlayerDrawNumbers).Name;

    public WCP_WKT.WKT_Player m_player;
    public List<Int64>        m_draws_list;

    public WCP_WKT_MsgRequestPlayerDrawNumbers()
    {
      this.m_player = new WCP_WKT.WKT_Player ();
    }

    #region IXml Members

    public String ToXml ()
    {
      StringBuilder _sb;

      _sb = new StringBuilder ();
      _sb.AppendLine ("<" + MSG_NAME + ">");
      _sb.Append (m_player.ToXml ());

      if ( m_draws_list.Count > 0 )
      {
        _sb.Append ("<DrawsList>");

        foreach ( Int64 _draw_id in m_draws_list )
        {
          _sb.Append ("<Draw>");
          _sb.Append ("<DrawID>" + _draw_id.ToString () + "</DrawID>");
          _sb.AppendLine ("</Draw>");
        }

        _sb.AppendLine ("</DrawsList>");
      }

      _sb.AppendLine ("</" + MSG_NAME + ">");

      return _sb.ToString ();

    } // ToXml

    public void LoadXml (XmlReader Reader)
    {
      String _str_draw_id;

      this.m_draws_list = new List<Int64> ();

      if ( Reader.Name != MSG_NAME )
      {
        throw new WCP_Exception (WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                 "Tag " + MSG_NAME + " not found in content area");
      }

      m_player.Pin = XML.ReadTagValue (Reader, "Pin");
      m_player.Trackdata = XML.ReadTagValue (Reader, "TrackData");

      while ( !( ( Reader.Name == MSG_NAME ) && Reader.NodeType == XmlNodeType.EndElement ) )
      {
        Reader.Read ();

        if ( ( Reader.Name == "Draw" ) && ( Reader.NodeType != XmlNodeType.EndElement ) )
        {
          _str_draw_id = XML.ReadTagValue (Reader, "DrawID");
          this.m_draws_list.Add (Int64.Parse (_str_draw_id));
        }
      }
    } // LoadXml

    #endregion

    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get { return this.m_player; }
      set { this.m_player = value; }
    }

    #endregion
  } // WCP_WKT_MsgRequestPlayerDrawNumbers

  //    - WCP_WKT_MsgRequestPlayerDrawNumbersReply :  List of vouchers containing the numbers for each draw
  public class WCP_WKT_MsgRequestPlayerDrawNumbersReply : IXml, WCP_WKT.IVouchers
  {
    public string MSG_NAME = typeof (WCP_WKT_MsgRequestPlayerDrawNumbersReply).Name;

    private ArrayList m_voucher_list;

    public String ToXml()
    {
      StringBuilder _sb;
      String        _deflated_voucher;

      _sb = new StringBuilder();
      _sb.AppendLine ("<" + MSG_NAME + ">");
      _sb.AppendLine("<Vouchers>");

      if ( VoucherList != null )
      {
        foreach ( Voucher _voucher in VoucherList )
        {
          Misc.DeflateString(_voucher.VoucherHTML.ToString(), out _deflated_voucher);
          _sb.Append("<Voucher>" + XML.StringToXmlValue(_deflated_voucher) + "</Voucher>");
        }
       }
 
      _sb.AppendLine("</Vouchers>");
      _sb.AppendLine ("</" + MSG_NAME + ">");

      return _sb.ToString();

    } // ToXml

    public void LoadXml (XmlReader Reader)
    {
      String  _inflated_voucher;
      String  _deflated_voucher;

      VoucherList = new ArrayList();

      if ( Reader.Name != MSG_NAME )
      {
        throw new WCP_Exception (WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR,
                                 "Tag " + MSG_NAME + " not found in content area");
      }
      else
      {
        while ( ( !( Reader.Name == "Vouchers" && Reader.NodeType == XmlNodeType.EndElement ) ) 
             && ( !( Reader.Name == MSG_NAME && Reader.NodeType == XmlNodeType.EndElement ) ) )
        {
          Reader.Read();
          if ((Reader.Name == "Voucher") && (Reader.NodeType != XmlNodeType.EndElement))
          {
            _deflated_voucher = XML.GetValue(Reader, "Voucher");
            Misc.InflateString(_deflated_voucher, out _inflated_voucher);
            VoucherList.Add(_inflated_voucher);
          }
        }
      }
    } // LoadXml

    #region IVoucher Members

    public ArrayList VoucherList
    {
      get
      {
        return m_voucher_list;
      }
      set
      {
        this.m_voucher_list = value;
      }
    }

    #endregion
  } // WCP_WKT_MsgRequestPlayerDrawNumbersReply

} // namespace WSI.WCP
