//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WKT_MsgGetPlayerInfo
// 
//   DESCRIPTION: WCP_WKT_MsgGetPlayerInfo class
// 
//        AUTHOR: Xavier Iglesia 
// 
// CREATION DATE: 06-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-JUL-2012 XIT    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_WKT_MsgGetPlayerInfo : IXml, WCP_WKT.IExtendedPlayer
  {
    public WCP_WKT.WKT_Player Player;

    public WCP_WKT_MsgGetPlayerInfo()
    {
      this.Player = new WCP_WKT.WKT_Player();
    }


    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.Append("<WCP_WKT_MsgGetPlayerInfo>");
      _sb.Append(Player.ToXml());
      _sb.Append("</WCP_WKT_MsgGetPlayerInfo>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_WKT_MsgGetPlayerInfo")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetPlayerInfo not found in content area");
      }
      Player.Pin = XML.ReadTagValue(Xml, "Pin");
      Player.Trackdata = XML.ReadTagValue(Xml, "TrackData");

    }
    #endregion

    #region IExtendedPlayer Members

    WCP_WKT.WKT_Player WCP_WKT.IExtendedPlayer.Player
    {
      get { return this.Player; }
      set { this.Player = value; }
    }

    #endregion
  }


  public class WCP_WKT_MsgGetPlayerInfoReply : IXml, WCP_WKT.IExtendedPlayerInfo, WCP_WKT.WKT_IGetPlayerField
  {
    private WCP_WKT.WKT_PlayerInfo m_player_info;
    private WCP_WKT.WKT_PlayerFieldsTable m_player_fields;


    public WCP_WKT_MsgGetPlayerInfoReply()
    {
      this.m_player_info = new WCP_WKT.WKT_PlayerInfo();
      this.PlayerFields = new WCP_WKT.WKT_PlayerFieldsTable();
    }

    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_WKT_MsgGetPlayerInfoReply>");
      _sb.Append(this.m_player_info.ToXml());
      _sb.Append(this.PlayerFields.ToXml());
      _sb.AppendLine("</WCP_WKT_MsgGetPlayerInfoReply>");

      //XmlParser();

      return _sb.ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      if (Xml.Name != "WCP_WKT_MsgGetPlayerInfoReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_MsgGetPlayerInfoReply not found in content area");
      }
      this.m_player_info.LoadXml(Xml);

      WCP_WKT_MsgSetPlayerFields.FillFieldsFromXml(Xml, ref m_player_fields);
      
    }

    #endregion

    #region IExtendedPlayerInfo Members

    public WCP_WKT.WKT_PlayerInfo PlayerInfo
    {
      get { return this.m_player_info; }
      set { this.m_player_info = value; }
    }

    #endregion

    #region WKT_IGetPlayerField Members

    public int FieldsNumber
    {
      get { return this.PlayerFields.Rows.Count; }
    }

    public WCP_WKT.WKT_IPlayerField GetField(int Idx)
    {
      return this.PlayerFields[Idx];
    }

    public WCP_WKT.WKT_PlayerFieldsTable PlayerFields
    {
      get { return m_player_fields; }
      set { m_player_fields = value; }
    }

    #endregion
  }


}
