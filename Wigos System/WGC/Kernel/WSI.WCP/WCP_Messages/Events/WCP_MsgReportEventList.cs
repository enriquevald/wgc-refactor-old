//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgReportEventList.cs
// 
//   DESCRIPTION: WCP_MsgReportEventList class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 09-APR-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-APR-2009 ACC    First release.
// 29-JUN-2015 FJC    Product Backlog Item 282.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_DeviceStatus
  {
    public DeviceCodes Code;      // DeviceCodes
    public Int32 Status;    // DeviceStatus
    public DevicePriority Priority;  // DevicePriority
    public DateTime LocalTime;

    public WCP_DeviceStatus()
    {
    }

    public WCP_DeviceStatus(String DeviceStatusXml)
    {
      System.IO.StringReader text_reader;
      XmlReader reader;

      text_reader = new System.IO.StringReader(DeviceStatusXml);
      reader = XmlReader.Create(text_reader);

      Code = (DeviceCodes)WCP_MsgHeader.Decode(XML.GetValue(reader, "Code"), DeviceCodes.WCP_DEVICE_CODE_NO_DEVICE);
      Status = WCP_DeviceStatus.GetStatus(Code, XML.GetValue(reader, "Status"));
      Priority = (DevicePriority)WCP_MsgHeader.Decode(XML.GetValue(reader, "Priority"), DevicePriority.WCP_DEVICE_PRIORITY_OK);
      DateTime.TryParse(XML.GetValue(reader, "LocalTime"), out LocalTime);
    }

    static public Int32 GetStatus(DeviceCodes DeviceCode, String Status)
    {
      Int32 status;

      status = DeviceStatus.WCP_DEVICE_STATUS_UNKNOWN;

      if (Status == "WCP_DEVICE_STATUS_OK")
      {
        status = DeviceStatus.WCP_DEVICE_STATUS_OK;
      }
      else if (Status == "WCP_DEVICE_STATUS_ERROR")
      {
        status = DeviceStatus.WCP_DEVICE_STATUS_ERROR;
      }
      else if (Status == "WCP_DEVICE_STATUS_NOT_INSTALLED")
      {
        status = DeviceStatus.WCP_DEVICE_STATUS_NOT_INSTALLED;

        return status;
      }

      switch (DeviceCode)
      {
        case DeviceCodes.WCP_DEVICE_CODE_NO_DEVICE:
        break;

        case DeviceCodes.WCP_DEVICE_CODE_PRINTER:
        {
          if (Status == "WCP_DEVICE_STATUS_PRINTER_OFF")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_PRINTER_OFF;
          }
          else if (Status == "WCP_DEVICE_STATUS_PRINTER_READY")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_PRINTER_READY;
          }
          else if (Status == "WCP_DEVICE_STATUS_PRINTER_NOT_READY")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_PRINTER_NOT_READY;
          }
          else if (Status == "WCP_DEVICE_STATUS_PRINTER_PAPER_OUT")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_PRINTER_PAPER_OUT;
          }
          else if (Status == "WCP_DEVICE_STATUS_PRINTER_PAPER_LOW")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_PRINTER_PAPER_LOW;
          }
          else if (Status == "WCP_DEVICE_STATUS_PRINTER_OFFLINE")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_PRINTER_OFFLINE;
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_NOTE_ACCEPTOR:
        {
          if (Status == "WCP_DEVICE_STATUS_NOTE_ACCEPTOR_JAM")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_NOTE_ACCEPTOR_JAM;
          }
          else if (Status == "WCP_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR;
          }
          else if (Status == "WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL;
          }
          else if (Status == "WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN;
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_SMARTCARD_READER:
        {
          if (Status == "WCP_DEVICE_STATUS_SMARTCARD_REMOVED")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_SMARTCARD_REMOVED;
          }
          else if (Status == "WCP_DEVICE_STATUS_SMARTCARD_INSERTED")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_SMARTCARD_INSERTED;
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_COIN_ACCEPTOR:
        {
          if (Status == "WCP_DEVICE_STATUS_COIN_ACCEPTOR_JAM")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_COIN_ACCEPTOR_JAM;
          }
          else if (Status == "WCP_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR;
          }
          else if (Status == "WCP_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL;
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_UPS:
        {
          if (Status == "WCP_DEVICE_STATUS_UPS_NO_AC")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_UPS_NO_AC;
          }
          else if (Status == "WCP_DEVICE_STATUS_UPS_BATTERY_LOW")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_UPS_BATTERY_LOW;
          }
          else if (Status == "WCP_DEVICE_STATUS_UPS_OVERLOAD")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_UPS_OVERLOAD;
          }
          else if (Status == "WCP_DEVICE_STATUS_UPS_OFF")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_UPS_OFF;
          }
          else if (Status == "WCP_DEVICE_STATUS_UPS_BATTERY_FAIL")
          {
            status = DeviceStatus.WCP_DEVICE_STATUS_UPS_BATTERY_FAIL;
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_UPPER_DISPLAY:
        case DeviceCodes.WCP_DEVICE_CODE_BAR_CODE_READER:
        case DeviceCodes.WCP_DEVICE_CODE_MODULE_IO:
        case DeviceCodes.WCP_DEVICE_CODE_CUSTOMER_DISPLAY:
        default:
          // No special status
        break;
      } // switch

      return status;

    } // GetStatus

    static public String StatusToString(DeviceCodes DeviceCode, Int32 Status)
    {
      String status_str;

      status_str = "WCP_DEVICE_STATUS_UNKNOWN";

      if (Status == DeviceStatus.WCP_DEVICE_STATUS_OK)
      {
        status_str = "WCP_DEVICE_STATUS_OK";

        return status_str;
      }
      else if (Status == DeviceStatus.WCP_DEVICE_STATUS_ERROR)
      {
        status_str = "WCP_DEVICE_STATUS_ERROR";

        return status_str;
      }
      else if (Status == DeviceStatus.WCP_DEVICE_STATUS_NOT_INSTALLED)
      {
        status_str = "WCP_DEVICE_STATUS_NOT_INSTALLED";

        return status_str;
      }

      switch (DeviceCode)
      {
        case DeviceCodes.WCP_DEVICE_CODE_NO_DEVICE:
        break;

        case DeviceCodes.WCP_DEVICE_CODE_PRINTER:
        {
          if (Status == DeviceStatus.WCP_DEVICE_STATUS_PRINTER_OFF)
          {
            status_str = "WCP_DEVICE_STATUS_PRINTER_OFF";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_PRINTER_READY)
          {
            status_str = "WCP_DEVICE_STATUS_PRINTER_READY";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_PRINTER_NOT_READY)
          {
            status_str = "WCP_DEVICE_STATUS_PRINTER_NOT_READY";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_PRINTER_PAPER_OUT)
          {
            status_str = "WCP_DEVICE_STATUS_PRINTER_PAPER_OUT";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_PRINTER_PAPER_LOW)
          {
            status_str = "WCP_DEVICE_STATUS_PRINTER_PAPER_LOW";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_PRINTER_OFFLINE)
          {
            status_str = "WCP_DEVICE_STATUS_PRINTER_OFFLINE";
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_NOTE_ACCEPTOR:
        {
          if (Status == DeviceStatus.WCP_DEVICE_STATUS_NOTE_ACCEPTOR_JAM)
          {
            status_str = "WCP_DEVICE_STATUS_NOTE_ACCEPTOR_JAM";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR)
          {
            status_str = "WCP_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL)
          {
            status_str = "WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN)
          {
            status_str = "WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN";
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_SMARTCARD_READER:
        {
          if (Status == DeviceStatus.WCP_DEVICE_STATUS_SMARTCARD_REMOVED)
          {
            status_str = "WCP_DEVICE_STATUS_SMARTCARD_REMOVED";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_SMARTCARD_INSERTED)
          {
            status_str = "WCP_DEVICE_STATUS_SMARTCARD_INSERTED";
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_COIN_ACCEPTOR:
        {
          if (Status == DeviceStatus.WCP_DEVICE_STATUS_COIN_ACCEPTOR_JAM)
          {
            status_str = "WCP_DEVICE_STATUS_COIN_ACCEPTOR_JAM";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR)
          {
            status_str = "WCP_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL)
          {
            status_str = "WCP_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL";
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_UPS:
        {
          if (Status == DeviceStatus.WCP_DEVICE_STATUS_UPS_NO_AC)
          {
            status_str = "WCP_DEVICE_STATUS_UPS_NO_AC";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_UPS_BATTERY_LOW)
          {
            status_str = "WCP_DEVICE_STATUS_UPS_BATTERY_LOW";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_UPS_OVERLOAD)
          {
            status_str = "WCP_DEVICE_STATUS_UPS_OVERLOAD";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_UPS_OFF)
          {
            status_str = "WCP_DEVICE_STATUS_UPS_OFF";
          }
          else if (Status == DeviceStatus.WCP_DEVICE_STATUS_UPS_BATTERY_FAIL)
          {
            status_str = "WCP_DEVICE_STATUS_UPS_BATTERY_FAIL";
          }
        }
        break;

        case DeviceCodes.WCP_DEVICE_CODE_UPPER_DISPLAY:
        case DeviceCodes.WCP_DEVICE_CODE_BAR_CODE_READER:
        case DeviceCodes.WCP_DEVICE_CODE_MODULE_IO:
        case DeviceCodes.WCP_DEVICE_CODE_CUSTOMER_DISPLAY:
        default:
          // No special status
        break;

      } // switch

      return status_str;

    } // StatusToString

  }

  public class WCP_Operation
  {
    public OperationCodes Code;  // OperationCodes
    public UInt32 Data;
    public DateTime LocalTime;
    public Int64 TechnicalAccountId;
    public TechModeEnter TechMode;

    public WCP_Operation()
    {
    }

    public WCP_Operation(String OperationXml)
    {
      System.IO.StringReader text_reader;
      XmlReader reader;

      text_reader = new System.IO.StringReader(OperationXml);
      reader = XmlReader.Create(text_reader);

      Code = (OperationCodes)WCP_MsgHeader.Decode(XML.GetValue(reader, "Code"), OperationCodes.WCP_OPERATION_CODE_NO_OPERATION);
      Data = UInt32.Parse(XML.GetValue(reader, "Data"));
      DateTime.TryParse(XML.GetValue(reader, "LocalTime"), out LocalTime);
      TechMode = (TechModeEnter)WCP_MsgHeader.Decode(XML.GetValue(reader, "TechMode"), TechModeEnter.WCP_TECH_MODE_UNKNOWN);
      Int64.TryParse(XML.GetValue(reader, "TechnicalAccountId"), out TechnicalAccountId);
    }
  }

  public class WCP_MsgReportEventList:IXml
  {
    public int NumDeviceStatus;
    public ArrayList DeviceStatusList = new ArrayList();  // WCP_DeviceStatus
    public int NumOperations;
    public ArrayList OperationList = new ArrayList();  // WCP_Operation

    public String ToXml ()
    {
      WCP_DeviceStatus device_status;
      WCP_Operation operation;
      StringBuilder str_builder;
      String str;


      str_builder = new StringBuilder();

      str_builder.Append("<WCP_MsgReportEventList><DeviceStatusList><NumDeviceStatus>");
      str_builder.Append(NumDeviceStatus.ToString());
      str_builder.Append("</NumDeviceStatus>");
      for (int idx_device_status = 0; idx_device_status < DeviceStatusList.Count; idx_device_status++)
      {
        device_status = (WCP_DeviceStatus)DeviceStatusList[idx_device_status];
        str_builder.Append("<DeviceStatus><Code>");
        str_builder.Append(device_status.Code.ToString());
        str_builder.Append("</Code><Status>");
        str_builder.Append(WCP_DeviceStatus.StatusToString(device_status.Code, device_status.Status));
        str_builder.Append("</Status><Priority>");
        str_builder.Append(device_status.Priority.ToString());
        str_builder.Append("</Priority><LocalTime>");

        // Build LocalTime
        str = XML.XmlDateTimeString(device_status.LocalTime);

        str_builder.Append(str);
        str_builder.Append("</LocalTime></DeviceStatus>");
      }
      str_builder.Append("</DeviceStatusList><OperationList><NumOperations>");
      str_builder.Append(NumOperations.ToString());
      str_builder.Append("</NumOperations>");
      for (int idx_operation = 0; idx_operation < OperationList.Count; idx_operation++)
      {
        operation = (WCP_Operation)OperationList[idx_operation];
        str_builder.Append("<Operation><Code>");
        str_builder.Append(operation.Code.ToString());
        str_builder.Append("</Code><Data>");
        str_builder.Append(operation.Data.ToString());
        str_builder.Append("</Data><LocalTime>");

        // Build LocalTime
        str = XML.XmlDateTimeString(operation.LocalTime);
        str_builder.Append(str);

        // Build TechnicalAccountId & TechMode
        str_builder.Append("</LocalTime><TechMode>");
        str_builder.Append(operation.TechMode);
        str_builder.Append("</TechMode><TechnicalAccountId>");
        str_builder.Append(operation.TechnicalAccountId.ToString());
        str_builder.Append("</TechnicalAccountId></Operation>");
        
      }
      str_builder.Append("</OperationList></WCP_MsgReportEventList>");

      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      WCP_DeviceStatus  device_status;
      WCP_Operation     operation;
      Int64             technical_account_id;
      


      if (reader.Name != "WCP_MsgReportEventList")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportEventList not found in content area");
      }
      else
      {
        NumDeviceStatus = int.Parse(XML.GetValue(reader, "NumDeviceStatus"));
        if (NumDeviceStatus > 0)
        {
          while (((reader.Name != "DeviceStatusList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
          {
            device_status = new WCP_DeviceStatus();

            device_status.Code = (DeviceCodes)WCP_MsgHeader.Decode(XML.GetValue(reader, "Code"), DeviceCodes.WCP_DEVICE_CODE_NO_DEVICE);
            device_status.Status = WCP_DeviceStatus.GetStatus(device_status.Code, XML.GetValue(reader, "Status"));
            device_status.Priority = (DevicePriority)WCP_MsgHeader.Decode(XML.GetValue(reader, "Priority"), DevicePriority.WCP_DEVICE_PRIORITY_OK);

            DateTime.TryParse(XML.GetValue(reader, "LocalTime"), out device_status.LocalTime);

            DeviceStatusList.Add(device_status);
            reader.Read(); //After this "read" the reader will be at the end of LocalTime
            reader.Read(); //After this "read" the reader will be at the end of DeviceStatus
            reader.Read(); //After this "read" the reader will be at the end of DeviceStatusList or the beginning of new a DeviceStatus

            if (DeviceStatusList.Count >= NumDeviceStatus)
            {
              break;
            }
          }

          if (DeviceStatusList.Count != NumDeviceStatus)
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportEventList wrong NumDeviceStatus");
          }
        }

        NumOperations = int.Parse(XML.GetValue(reader, "NumOperations"));
        if (NumOperations > 0)
        {
          while (((reader.Name != "OperationList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
          {
            operation = new WCP_Operation();
            operation.Code = (OperationCodes)WCP_MsgHeader.Decode(XML.GetValue(reader, "Code"), OperationCodes.WCP_OPERATION_CODE_NO_OPERATION);

            operation.Data = UInt32.Parse(XML.GetValue(reader, "Data"));
            DateTime.TryParse(XML.GetValue(reader, "LocalTime"), out operation.LocalTime);

            reader.Read(); // After this "read" the reader will be at the end of LocalTime
            reader.Read(); // After this "read" the reader will be at the begin of TechMode (if exists)

            if (reader.Name == "TechMode")
            {
              //Tech Mode
              operation.TechMode = (TechModeEnter)WCP_MsgHeader.Decode(XML.GetValue(reader, "TechMode"), TechModeEnter.WCP_TECH_MODE_UNKNOWN);

              //Technical AccountId
              if (!Int64.TryParse(XML.GetValue(reader, "TechnicalAccountId", true), out technical_account_id))
              {
                technical_account_id = -1;
              }
              operation.TechnicalAccountId = technical_account_id;

              reader.Read(); //After this "read" the reader will be at the end of TechnicalAccountId
              reader.Read(); //After this "read" the reader will be at the end of Operation
            }

            OperationList.Add(operation);

            reader.Read(); //After this "read" the reader will be at the end of OperationList or the beginning of new a Operation

            if (OperationList.Count >= NumOperations)
            {
              break;
            }
          }

          if (OperationList.Count != NumOperations)
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportEventList wrong NumOperations");
          }
        }
      }
    }
  }

  public class WCP_MsgReportEventListReply : IXml
  {
    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportEventListReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    }

    public void LoadXml (XmlReader reader)
    {
      if (reader.Name != "WCP_MsgReportEventListReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportEventListReply not found in content area");
      }
    }
  }
}
