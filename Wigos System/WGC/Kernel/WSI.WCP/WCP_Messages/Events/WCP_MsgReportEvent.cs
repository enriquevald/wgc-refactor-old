//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgReportEvent.cs
// 
//   DESCRIPTION: WCP_MsgReportEvent class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MsgReportEvent:IXml
  {
    public WCP_EventTypesCodes EventType;
    public String EventData;
    public DateTime EventDateTime;

    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportEvent", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "EventType", EventType.ToString());
      XML.AppendChild(node, "EventData", EventData);
      XML.AppendChild(node, "EventDateTime", EventDateTime.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgReportEvent")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportEvent not found in content area");
      }
      else
      {
        EventType = (WCP_EventTypesCodes)Enum.Parse(typeof(WCP_EventTypesCodes), XML.GetValue(reader, "EventType"), true);
        EventData = XML.GetValue(reader, "EventData");
        EventDateTime = XML.GetAsDateTime (reader, "EventDateTime");
      }
    }

  }

  public class WCP_MsgReportEventReply : IXml
  {
    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportEventReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    }

    public void LoadXml (XmlReader reader)
    {
      if (reader.Name != "WCP_MsgReportEventReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportEventReply not found in content area");
      }
    }
  }
}
