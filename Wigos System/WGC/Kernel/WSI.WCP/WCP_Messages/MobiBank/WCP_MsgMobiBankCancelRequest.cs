﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMobiBankCancelRequest.cs
// 
//   DESCRIPTION: WCP_MsgMobiBankCancelRequest class
//                Msg for Cancel a Request of Recharge (-from LCD-Intouch (Button) to WCP (Center)- OR -from App Movil-)
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 29-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-NOV-2017 FJC    First release.
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using WSI.Common.TerminalDraw;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.WCP
{
  public class WCP_MsgMobiBankCancelRequest : IXml
  {
    // Cancel From InTouch
    public Int64 AccountId;
    public MobileBankStatusCancelReason CancelReason;

    // Cancel From App
    //public Int64 CmdId;
    //public Int64 RequestId;

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgMobiBankCancelRequest>");
      _sb.AppendLine("<AccountId>" + AccountId.ToString() + "</AccountId>");
      _sb.AppendLine("<CancelReason>" + (Int32)CancelReason + "</CancelReason>");
      _sb.AppendLine("</WCP_MsgMobiBankCancelRequest>");

      return _sb.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      Int32 _cancel_reason;
      if (!Reader.Name.Equals("WCP_MsgMobiBankCancelRequest"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankCancelRequest not found in content area");
      }
      // Accountid
      Int64.TryParse(XML.ReadTagValue(Reader, "AccountId", true), out AccountId);

      // CancelReason
      Int32.TryParse(XML.ReadTagValue(Reader, "CancelReason", true), out _cancel_reason);
      CancelReason = (MobileBankStatusCancelReason)Enum.Parse(typeof(MobileBankStatusCancelReason), _cancel_reason.ToString());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="XmlString"></param>
    public void LoadXml(String XmlData)
    {
      Int32 _cancel_reason;
      XmlDocument _xml;
      XmlNode _node;

      try
      {
        _xml = new XmlDocument();

        _xml.LoadXml(XmlData);
        _node = _xml.SelectSingleNode("WCP_MsgMobiBankCancelRequest");

        AccountId = Int64.Parse(XML.GetValue(_node, "AccountId"));
        _node = _xml.SelectSingleNode("WCP_MsgMobiBankCancelRequest/Parameters");
        _cancel_reason = Int32.Parse(XML.GetValue(_node, "CancelReason"));
        CancelReason = (MobileBankStatusCancelReason)Enum.Parse(typeof(MobileBankStatusCancelReason), _cancel_reason.ToString());
  
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
  }

  public class WCP_MsgMobiBankCancelRequestReply : IXml
  {

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgMobiBankCancelRequestReply>");
      _sb.AppendLine("</WCP_MsgMobiBankCancelRequestReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgMobiBankCancelRequestReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankCancelRequestReply not found in content area");
      }
    } // LoadXml
  }
}