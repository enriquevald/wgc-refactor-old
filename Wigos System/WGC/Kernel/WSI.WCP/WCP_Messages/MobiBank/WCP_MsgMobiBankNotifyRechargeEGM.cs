﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMobiBankNotifyRechargeEGM.cs
// 
//   DESCRIPTION: WCP_MsgMobiBankNotifyRechargeEGM class
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 20-FEB-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-FEB-2018 FJC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using WSI.Common.TerminalDraw;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.WCP
{
  public class WCP_MsgMobiBankNotifyRechargeEGM : IXml
  {
    public Int64 CmdId;
    public MobileBankRechargeintoEGMStatus Status;
    public Int64 RequestId;

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgMobiBankNotifyRechargeEGM>");
      _sb.AppendLine("<CmdId>" + CmdId.ToString() + "</CmdId>");
      _sb.AppendLine("<Status>" + (Int32)Status + "</Status>");
      _sb.AppendLine("<RequestId>" + (Int64)RequestId + "</RequestId>");
      _sb.AppendLine("</WCP_MsgMobiBankNotifyRechargeEGM>");

      return _sb.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      Int32 _status;

      if (!Reader.Name.Equals("WCP_MsgMobiBankNotifyRechargeEGM"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankNotifyRechargeEGM not found in content area");
      }
      // CmdId
      Int64.TryParse(XML.ReadTagValue(Reader, "CmdId", true), out CmdId);

      // Status
      Int32.TryParse(XML.ReadTagValue(Reader, "Status", true), out _status);
      Status = (MobileBankRechargeintoEGMStatus)Enum.Parse(typeof(MobileBankRechargeintoEGMStatus), _status.ToString());

      // RequestId
      Int64.TryParse(XML.ReadTagValue(Reader, "RequestId", true), out RequestId);
    }
  }

  public class WCP_MsgMobiBankNotifyRechargeEGMReply : IXml
  {

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgMobiBankNotifyRechargeEGMReply>");
      _sb.AppendLine("</WCP_MsgMobiBankNotifyRechargeEGMReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgMobiBankNotifyRechargeEGMReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankNotifyRechargeEGMReply not found in content area");
      }
    } // LoadXml
  }
}