﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMobiBankRechargeNotification.cs
// 
//   DESCRIPTION: WCP_MsgMobiBankRechargeNotification class
//                Msg for Manage a Notification to EGM that money has transfered to Account Balance or EGM (used in WCP_Command)
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 06-FEB-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-FEB-2018 FJC    First release.
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using WSI.Common.TerminalDraw;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.WCP
{
  public class WCP_MsgMobiBankRechargeNotification : IXml
  {
    public Int64 AccountId;
    public MobileBankRechargeType TransFerType;
    public Int64 TransFerAmount;

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgMobiBankRechargeNotification>");
      _sb.AppendLine("<AccountId>" + AccountId.ToString() + "</AccountId>");
      _sb.AppendLine("</WCP_MsgMobiBankRechargeNotification>");

      return _sb.ToString();
    }

    /// <summary>
    /// Load XML
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (!Reader.Name.Equals("WCP_MsgMobiBankRechargeNotification"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankRechargeNotification not found in content area");
      }
      Int64.TryParse(XML.ReadTagValue(Reader, "AccountId", true), out AccountId);
    }

    /// <summary>
    /// Load XML
    /// </summary>
    /// <param name="XmlData"></param>
    public void LoadXml(String XmlData)
    {
      XmlDocument _xml;
      XmlNode _node;
      Int32 _transfer_type;
      
      try
      {
        _xml = new XmlDocument();

        _xml.LoadXml(XmlData);
        _node = _xml.SelectSingleNode("WCP_MsgMobiBankRechargeNotification");
        
        // AccountId
        AccountId = Int64.Parse(XML.GetValue(_node, "AccountId"));

        // * PARAMETERS
        _node = _xml.SelectSingleNode("WCP_MsgMobiBankRechargeNotification/Parameters");

        // TransFer Type
        Int32.TryParse(XML.GetValue(_node, "TransferType"), out _transfer_type);
        TransFerType = (MobileBankRechargeType)Enum.Parse(typeof(MobileBankRechargeType), _transfer_type.ToString());

        // Transfer Amount
        TransFerAmount = Int64.Parse(XML.GetValue(_node, "TransferAmount"));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
  }

  public class WCP_MsgMobiBankRechargeNotificationReply : IXml
  {
    public Int64 AccountId;

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgMobiBankRechargeNotificationReply>");
      _sb.AppendLine("<AccountId>" + AccountId.ToString() + "</AccountId>");
      _sb.AppendLine("</WCP_MsgMobiBankRechargeNotificationReply>");

      return _sb.ToString();
    }

    /// <summary>
    /// Load XML
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(XmlReader Reader)
    {
      if (!Reader.Name.Equals("WCP_MsgMobiBankRechargeNotificationReply"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankRechargeNotificationReply not found in content area");
      }
      Int64.TryParse(XML.ReadTagValue(Reader, "AccountId", true), out AccountId);
    } // LoadXml
  }
}