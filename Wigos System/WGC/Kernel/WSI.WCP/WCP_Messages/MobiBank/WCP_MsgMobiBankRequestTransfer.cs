﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMobiBankRequestTransfer.cs
// 
//   DESCRIPTION: WCP_MsgMobiBankRequestTransfer class
//                Msg for Manage a Transfer of money to Balance ==> EGM (used in WCP_Command)
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 20-FEB-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-FEB-2018 FJC    First release.
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using WSI.Common.TerminalDraw;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.WCP
{
  public class WCP_MsgMobiBankRequestTransfer : IXml
  {
    public Int64 CmdId;
    public Int64 RequestId;

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgMobiBankRequestTransfer>");
      _sb.AppendLine("<CmdId>" + CmdId.ToString() + "</CmdId>");
      _sb.AppendLine("<Parameters>");
      _sb.AppendLine("<RequestId>" + RequestId.ToString() + "</RequestId>");
      _sb.AppendLine("</Parameters>");

      _sb.AppendLine("</WCP_MsgMobiBankRequestTransfer>");

      return _sb.ToString();
    }

    /// <summary>
    /// Load XML
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (!Reader.Name.Equals("WCP_MsgMobiBankRequestTransfer"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankRequestTransfer not found in content area");
      }
    }

    /// <summary>
    /// Load XML
    /// </summary>
    /// <param name="XmlData"></param>
    public void LoadXml(String XmlData)
    {
      XmlDocument _xml;
      XmlNode _node;
      
      try
      {
        _xml = new XmlDocument();

        _xml.LoadXml(XmlData);
        _node = _xml.SelectSingleNode("WCP_MsgMobiBankRequestTransfer");
        
        // Cmd id
        CmdId = Int64.Parse(XML.GetValue(_node, "CmdId"));

        // * PARAMETERS
        _node = _xml.SelectSingleNode("WCP_MsgMobiBankRequestTransfer/Parameters");

        //  RequestId
        RequestId = Int64.Parse(XML.GetValue(_node, "RequestId"));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
  }

  public class WCP_MsgMobiBankRequestTransferReply : IXml
  {
    public Int64 CmdId;

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgMobiBankRequestTransferReply>");
      _sb.AppendLine("<CmdId>" + CmdId.ToString() + "</CmdId>");
      _sb.AppendLine("</WCP_MsgMobiBankRequestTransferReply>");

      return _sb.ToString();
    }

    /// <summary>
    /// Load XML
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(XmlReader Reader)
    {
      if (!Reader.Name.Equals("WCP_MsgMobiBankRequestTransferReply"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankRequestTransferReply not found in content area");
      }
      Int64.TryParse(XML.ReadTagValue(Reader, "CmdId", true), out CmdId);
    } // LoadXml
  }
}