﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgMobiBankRecharge.cs
// 
//   DESCRIPTION: WCP_MsgMobiBankRecharge class
//                Msg for Insert a Request of Recharge (from LCD-Intouch (Button) to WCP (Center)
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 28-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-NOV-2017 FJC    First release.
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using WSI.Common.TerminalDraw;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.WCP
{
  public class WCP_MsgMobiBankRecharge : IXml
  {
    public Int64 AccountId;

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_MsgMobiBankRecharge>");
      _sb.AppendLine("<AccountId>" + AccountId.ToString() + "</AccountId>");
      _sb.AppendLine("</WCP_MsgMobiBankRecharge>");

      return _sb.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Reader"></param>
    public void LoadXml(System.Xml.XmlReader Reader)
    {
      if (!Reader.Name.Equals("WCP_MsgMobiBankRecharge"))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankRecharge not found in content area");
      }
      Int64.TryParse(XML.ReadTagValue(Reader, "AccountId", true), out AccountId);
    }
  }

  public class WCP_MsgMobiBankRechargeReply : IXml
  {

    /// <summary>
    /// Convert internal properties to string in XML format
    /// </summary>
    /// <returns></returns>
    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgMobiBankRechargeReply>");
      _sb.AppendLine("</WCP_MsgMobiBankRechargeReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgMobiBankRechargeReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobiBankRechargeReply not found in content area");
      }
    } // LoadXml
  }
}