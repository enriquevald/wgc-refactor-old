//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MsgHeader.cs
// 
//   DESCRIPTION: WCP Message Header
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
// 24-OCT-2011 ACC    Add Operation code: WCP_OPERATION_CODE_EVENT
// 18-NOV-2011 ACC    Added Machine meters
// 19-ABR-2012 SSC    Added Operation code: WCP_OPERATION_CODE_CHANGE_STACKER
// 26-OCT-2013 JRM    New error code for tito invalid validation number
// 29-OCT-2013 NMR    New message code for tito meters
// 20-NOV-2013 NMR    New message code for request TITO ValidationNumberSequence
// 05-MAR-2014 JCOR   Added PENDING_TRANSFER.
// 01-APR-2014 JCOR   Added WCP_CMD_REQUEST_SOFTWARE_VALIDATION.
// 03-JUL-2014 JMM    Added Technician card events
// 12-NOV-2014 FJC    Added SiteJackPotAwarded message code for showing in display LCD
// 05-AUG-2015 YNM    TFS-3109: Annonymous account min allowed cash in amount recharge
// 06-OCT-2015 FJC    Product Backlog 4704
// 20-JAN-2016 ACC    ToXml get SystemTime from object
// 16-MAR-2016 FJC    PBI 9105: BonoPlay: LCD: Cambios varios
// 10-AUG-2016 ETP    Fixed bug 16475: Add description to alarm.
// 27-SEP-2016 JMM    PBI 18229:WCP - Proceso de la alarma Ticket Amount Not Multiple of Machine Denomination
// 28-NOV-2016 FAV    PBI 20372: EGASA - Allow repeats cards
// 28-NOV-2016 FAV    PBI 20347: EGASA: Login with card
// 15-FEB-2017 AMF    PBI 24612:WebService FBM: Nuevo Evento inserci�n de billetes
// 29-NOV-2017 FJC    WIGOS-3592 MobiBank: InTouch - Recharge request
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using WSI.Common;

namespace WSI.WCP
{
  /// <summary>
  /// WCP message types
  /// </summary>
  public enum WCP_MsgTypes
  { 
      // Request                          // Reply
      WCP_MsgRequestService,              WCP_MsgRequestServiceReply
    , WCP_MsgEnrollServer,                WCP_MsgEnrollServerReply
    , WCP_MsgEnrollTerminal,              WCP_MsgEnrollTerminalReply
    , WCP_MsgGetParameters,               WCP_MsgGetParametersReply
    , WCP_MsgUnenrollTerminal,            WCP_MsgUnenrollTerminalReply
    , WCP_MsgUnenrollServer,              WCP_MsgUnenrollServerReply
    , WCP_MsgGetCardType,                 WCP_MsgGetCardTypeReply
    , WCP_MsgLockCard,                    WCP_MsgLockCardReply
    , WCP_MsgStartCardSession,            WCP_MsgStartCardSessionReply
    , WCP_MsgPlayConditions,              WCP_MsgPlayConditionsReply
    , WCP_MsgReportPlay,                  WCP_MsgReportPlayReply
    , WCP_MsgAddCredit,                   WCP_MsgAddCreditReply
    , WCP_MsgEndSession,                  WCP_MsgEndSessionReply
    , WCP_MsgReportEvent,                 WCP_MsgReportEventReply
    , WCP_MsgExecuteCommand,              WCP_MsgExecuteCommandReply
    , WCP_MsgReportMeters,                WCP_MsgReportMetersReply
    , WCP_MsgIntegrityCheck,              WCP_MsgIntegrityCheckReply
    , WCP_MsgKeepAlive,                   WCP_MsgKeepAliveReply
    , WCP_MsgReportEventList,             WCP_MsgReportEventListReply
    , WCP_MsgMobileBankCardCheck,         WCP_MsgMobileBankCardCheckReply
    , WCP_MsgMobileBankCardTransfer,      WCP_MsgMobileBankCardTransferReply
    , WCP_MsgError
    , WCP_MsgUnknown
    , WCP_MsgReportGameMeters,            WCP_MsgReportGameMetersReply
    , WCP_MsgGetLogger,                   WCP_MsgGetLoggerReply
    , WCP_MsgReportHPCMeter,              WCP_MsgReportHPCMeterReply
    , WCP_MsgUpdateCardSession,           WCP_MsgUpdateCardSessionReply
    , WCP_MsgCardInformation,             WCP_MsgCardInformationReply    // Mobile bank: iPOD.
    , WCP_MsgReportMachineMeters,         WCP_MsgReportMachineMetersReply
    , WCP_MsgMoneyEvent,                  WCP_MsgMoneyEventReply
    , WCP_MsgCancelStartSession,          WCP_MsgCancelStartSessionReply
    , WCP_MsgCashSessionPendingClosing,   WCP_MsgCashSessionPendingClosingReply
    , WCP_MsgMBInformation,               WCP_MsgMBInformationReply    // Mobile bank: iPOD.

    // generic messages related with TITO development
    , WCP_MsgSasMeters,                   WCP_MsgSasMetersReply
    , WCP_MsgPlaySessionMeters,           WCP_MsgPlaySessionMetersReply
    , WCP_MsgEGMHandpays,                 WCP_MsgEGMHandpaysReply
    , WCP_MsgBonusTransferStatus,         WCP_MsgBonusTransferStatusReply
    , WCP_MsgVersionSignature,            WCP_MsgVersionSignatureReply

    , WCP_MsgCheckTicket,                 WCP_MsgCheckTicketReply // Mobile bank: iPOD.
    , WCP_MsgPayTicket,                   WCP_MsgPayTicketReply

    //JMM 03-JUL-2014: PIN validation message and its reply
    , WCP_MsgPlayerCheckPin,              WCP_MsgPlayerCheckPinReply
    //XCD 30-APR-2014: Smib protocol parameters to send
    , WCP_MsgGetProtocolParameters,       WCP_MsgGetProtocolParametersReply

    //JMM 4-AUG-2015 Multi-denom
    , WCP_MsgMachineMultiDenomInfo,       WCP_MsgMachineMultiDenomInfoReply      
    
    //FJC 02-JAN-2016 (Feature 22239:Sorteo de m�quina)
    , WCP_MsgTerminalDrawGetPendingDraw,  WCP_MsgTerminalDrawGetPendingDrawReply
    , WCP_MsgTerminalDrawProcessDraw,     WCP_MsgTerminalDrawProcessDrawReply      
    
    //JMM 01-FEB-2017 Reserve Terminal
    , WCP_MsgReserveTerminal,             WCP_MsgReserveTerminalReply

    //FJC (Terminal Draw)
    , WCP_MsgTerminalDrawGameTimeElapsed, WCP_MsgTerminalDrawGameTimeElapsedReply

    //FJC 27-11-2017: WIGOS-3592 MobiBank: InTouch - Recharge request
    , WCP_MsgMobiBankRecharge,            WCP_MsgMobiBankRechargeReply
    , WCP_MsgMobiBankCancelRequest,       WCP_MsgMobiBankCancelRequestReply
    , WCP_MsgMobiBankNotifyRechargeEGM,   WCP_MsgMobiBankNotifyRechargeEGMReply

    //
    // AJQ 18-MAY-2012, WKT Messages
    // 
    // Reserved range: 1000..1999
    , WCP_WKT_MsgGetParameters            = 10001,  WCP_WKT_MsgGetParametersReply            = 10002
    , WCP_WKT_MsgGetResource              = 10003,  WCP_WKT_MsgGetResourceReply              = 10004
    , WCP_WKT_MsgGetNextAdvStep           = 10005,  WCP_WKT_MsgGetNextAdvStepReply           = 10006
    , WCP_WKT_MsgGetPlayerGifts           = 10007,  WCP_WKT_MsgGetPlayerGiftsReply           = 10008
    , WCP_WKT_MsgPlayerRequestGift        = 10009,  WCP_WKT_MsgPlayerRequestGiftReply        = 10010
    , WCP_WKT_MsgSetPlayerFields          = 10011,  WCP_WKT_MsgSetPlayerFieldsReply          = 10012
    , WCP_WKT_MsgGetPlayerInfo            = 10013,  WCP_WKT_MsgGetPlayerInfoReply            = 10014
    , WCP_WKT_MsgGetPlayerPromos          = 10015,  WCP_WKT_MsgGetPlayerPromosReply          = 10016
    , WCP_WKT_MsgGetPlayerActivity        = 10017,  WCP_WKT_MsgGetPlayerActivityReply        = 10018
    , WCP_WKT_MsgPrintRechargesList       = 10019,  WCP_WKT_MsgPrintRechargesListReply       = 10020
    , WCP_WKT_MsgGetPlayerDraws           = 10021,  WCP_WKT_MsgGetPlayerDrawsReply           = 10022
    , WCP_WKT_MsgRequestPlayerDrawNumbers = 10023,  WCP_WKT_MsgRequestPlayerDrawNumbersReply = 10024
    , WCP_WKT_MsgPlayerRequestPromo       = 10025,  WCP_WKT_MsgPlayerRequestPromoReply       = 10026


    // TITO Messages. 20000..20300 Reserved
    , WCP_TITO_MsgIsValidTicket           = 20000, WCP_TITO_MsgIsValidTicketReply            = 20001
    , WCP_TITO_MsgCancelTicket            = 20002, WCP_TITO_MsgCancelTicketReply             = 20003
    , WCP_TITO_MsgCreateTicket            = 20004, WCP_TITO_MsgCreateTicketReply             = 20005
    , WCP_TITO_MsgEgmParameters           = 20006, WCP_TITO_MsgEgmParametersReply            = 20007
    , WCP_TITO_MsgValidationNumber        = 20008, WCP_TITO_MsgValidationNumberReply         = 20009

    // LCD Display Messages 21000..22000
    , WCP_LCD_MsgGetParameters            = 21001, WCP_LCD_MsgGetParametersReply             = 21002
    , WCP_LCD_MsgGetPromoBalance          = 21003, WCP_LCD_MsgGetPromoBalanceReply           = 21004

    // GAMEGATEWAY Messages 22000..23000
      // FJC 06-OCT-2015 (Product Bakclog 4704)
    , WCP_MsgSubsMachinePartialCredit     = 22000, WCP_MsgSubsMachinePartialCreditReply      = 22001
    , WCP_MsgGameGatewayGetCredit         = 22002, WCP_MsgGameGatewayGetCreditReply          = 22003
      // FJC 10-MAR-2016 (PBI 9105:BonoPlay: LCD: Changes; Task 10362: BonoPlay LCD: Refactorizar mensajer�a.)
    , WCP_MsgGameGatewayProcessMsg        = 22004, WCP_MsgGameGatewayProcessMsgReply         = 22005

  }
  
  /// <summary>
  /// WCP response codes
  /// </summary>
  public enum WCP_ResponseCodes
  {
    WCP_RC_OK = 0,
    WCP_RC_ERROR = 1,
    WCP_RC_DATABASE_OFFLINE = 2,
    WCP_RC_SERVICE_NOT_AVAILABLE = 3,
    WCP_RC_SERVICE_NOT_FOUND = 4,
    WCP_RC_SERVER_NOT_AUTHORIZED = 5,
    WCP_RC_SERVER_SESSION_NOT_VALID = 6,
    WCP_RC_TERMINAL_NOT_AUTHORIZED = 7,
    WCP_RC_TERMINAL_SESSION_NOT_VALID = 8,
    WCP_RC_CARD_NOT_VALID = 9,
    WCP_RC_CARD_BLOCKED = 10,
    WCP_RC_CARD_EXPIRED = 11,
    WCP_RC_CARD_ALREADY_IN_USE = 12,
    WCP_RC_CARD_NOT_VALIDATED = 13,
    WCP_RC_CARD_BALANCE_MISMATCH = 14,
    WCP_RC_CARD_REMOTE_BALANCE_MISMATCH = 15,
    WCP_RC_CARD_SESSION_NOT_CLOSED = 16,
    WCP_RC_CARD_SESSION_NOT_VALID = 17,
    WCP_RC_CARD_WRONG_PIN = 18,
    WCP_RC_INVALID_SESSION_ID = 19,
    WCP_RC_UNKNOWN_GAME_ID = 20,
    WCP_RC_INVALID_AMOUNT = 21,
    WCP_RC_EVENT_NOT_VALID = 22,
    WCP_RC_CRC_MISMATCH = 23,
    WCP_RC_NOT_ENOUGH_PLAYERS = 24,

    WCP_RC_MSG_FORMAT_ERROR = 25,
    WCP_RC_INVALID_MSG_TYPE = 26,

    WCP_RC_INVALID_PROTOCOL_VERSION = 27,
    WCP_RC_INVALID_SEQUENCE_ID = 28,
    WCP_RC_INVALID_SERVER_ID = 29,
    WCP_RC_INVALID_TERMINAL_ID = 30,
    WCP_RC_INVALID_SERVER_SESSION_ID = 31,
    WCP_RC_INVALID_TERMINAL_SESSION_ID = 32,
    WCP_RC_INVALID_COMPRESSION_METHOD = 33,
    WCP_RC_INVALID_RESPONSE_CODE = 34,

    WCP_RC_UNKNOWN = 35,
    WCP_RC_LAST = 36,

    WCP_RC_SESSION_TIMEOUT = 37,

    // ACC 12-AUG-2013
    WCP_RC_RECHARGE_NOT_AUTHORIZED = 38,

    // JML 14-NOV-2014
    WCP_RC_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT = 39,
    WCP_RC_MB_EXCEEDED_RECHARGE_LIMIT = 40,
    WCP_RC_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT = 41,
    // JML 14-NOV-2014
    WCP_RC_EXCEEDED_MAX_ALLOWED_CASH_IN = 42,
    WCP_RC_EXCEEDED_MAX_ALLOWED_DAILY_CASH_IN = 43,
    WCP_RC_ML_RECHARGE_NOT_AUTHORIZED = 44,

    // JML 21-NOV-2014 
    WCP_RC_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED = 45,
    WCP_RC_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED = 46,
    WCP_RC_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED = 47,
    WCP_RC_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED = 48,

    WCP_RC_MB_SESSION_CLOSED = 49,

    // ACC 27-APR-2015 
    WCP_RC_STACKER_ID_NOT_FOUND = 50,      
    WCP_RC_STACKER_ID_ALREADY_IN_USE = 51,

    // JBC 11-MAY-2015
    WCP_RC_GAMING_DAY_EXPIRED = 52, 

    // YNM 28-JUL-2015
    WCP_RC_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT = 53,

    // SGB 16-NOV-2015
    WCP_RC_GENERAL_PARAM_DUPLICATED = 54,

    // JMM 08-FEB-2017
    WCP_RC_TERMINAL_RESERVED = 55,
    WCP_RC_ANOTHER_TERMINAL_RESERVED = 56,

    // AJQ 18-MAY-2012, WKT Response code
    WCP_RC_WKT_OK                     = 10000,  // OK
    WCP_RC_WKT_DISABLED               = 10001,  // Kiosk disabled         / Kiosco deshabilitado
    WCP_RC_WKT_ERROR                  = 10002,  // Error                  / Error
    WCP_RC_WKT_MESSAGE_NOT_SUPPORTED  = 10003,  // Message Not Supported  / Mensaje no soportado
    WCP_RC_WKT_RESOURCE_NOT_FOUND     = 10004,  // Resource Not found     / Recurso no encontrado

    // Login
    WCP_RC_WKT_CARD_NOT_VALID         = 10010,  // Card Not Valid     / Tarjeta no v�lida
    WCP_RC_WKT_ACCOUNT_BLOCKED        = 10011,  // Acccount Blocked   / Cuenta bloqueada
    WCP_RC_WKT_ACCOUNT_NO_PIN         = 10012,  // PIN not configured / PIN sin configurar
    WCP_RC_WKT_WRONG_PIN              = 10013,  // Wrong PIN          / PIN err�neo

    //GetRequest
    WKT_UNKNOWN                              = 10014,
    WKT_CARD_ERROR                           = 10015,
    WKT_NOT_ENOUGH_POINTS                    = 10016,
    WKT_NOT_AVAILABLE                        = 10017,
    WKT_NOT_ENOUGH_STOCK                     = 10018,
    WKT_CARD_HAS_NON_REDEEMABLE              = 10019,
    WCP_RC_WKT_FUNCTIONALITY_OFF_SCHEDULE    = 10020,
    WKT_PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS  = 10021,
    WCP_RC_WKT_FUNCTIONALITY_NOT_ENABLED     = 10022,    

    //TITO GetRequest
    WCP_RC_TITO_NOT_ALLOWED_ISSUANCE          = 10023,
    WCP_RC_TITO_TICKET_ERROR                  = 10024,
    WCP_RC_TITO_MESSAGE_NOT_SUPPORTED         = 10025,
    WCP_RC_TITO_COULD_NOT_CANCEL_TICKET       = 10026,

    WKT_PROMO_NOT_CURRENTLY_APPLICABLE        = 10027,
    WCP_RC_TITO_INVALID_VALIDATION_NUMBER     = 10028,
    WCP_RC_TITO_TICKET_EMISSION_NOT_ALLOWED   = 10029,
    WCP_RC_TITO_TICKET_OUT_AMOUNT_EXCEEDED    = 10030,

    //WKT & TITO Errors
    WCP_RC_WKT_TITO_PRINTER_ERROR             = 10031,
  }

  public enum WCP_EventTypesCodes
  {
    WCP_EV_DEVICE_STATUS = 1,
    WCP_EV_OPERATION     = 2,
    
    WCP_EV_DOOR_OPENED = 101,
    WCP_EV_DOOR_CLOSED = 102,
    WCP_EV_NOTE_ACCEPTOR_JAM = 103,
    WCP_EV_NOTE_ACCEPTOR_FULL = 104,
    WCP_EV_NOTE_ACCEPTOR_ERROR = 105,
    WCP_EV_PRINTER_NO_PAPER = 106,
    WCP_EV_PRINTER_JAM = 107,
    WCP_EV_PRINTER_ERROR = 108,
    WCP_EV_POWER_FAILURE = 109,
    WCP_EV_LOST_CONNECTION = 110,
    WCP_EV_STARTUP  = 111, 
    WCP_EV_SHUTDOWN = 112,
    WCP_EV_CREDIT_LIMIT = 113,
    WCP_EV_DISPLAY_OFF = 114,
    WCP_EV_CARDREADER_OFF = 115,
  }

  public enum WCP_CashTypes
  {
    CASH_TYPE_CASH_IN   = 1
  , CASH_TYPE_CASH_OUT  = 2
  }


  public enum WCP_MoneyTypes
  {
    MONEY_TYPE_NOTE     = 1
  , MONEY_TYPE_COIN     = 2
  }

  public enum WCP_CommandTypes
  {
     WCP_CMD_SHUTDOWN = 1
   , WCP_CMD_RESTART = 2
   , WCP_CMD_RELOAD_PARAMETERS = 3
   , WCP_CMD_RESET_METERS = 4
   , WCP_CMD_REPORT_METERS = 5
   , WCP_CMD_LOCK = 6
   , WCP_CMD_DISCONNECT = 7
   , WCP_CMD_INTEGRITY_CHECK = 8
   , WCP_CMD_SHOW_MESSAGE = 9

   , WCP_CMD_BONUS_AWARD  = 10
   , WCP_CMD_BONUS_STATUS = 11
   , WCP_CMD_REQUEST_SOFTWARE_VALIDATION = 12
   , WCP_CMD_SITE_JACKPOT_AWARDED = 13        //FJC 12-NOV-2014
   , WCP_CMD_REQUEST_TRANSFER = 14        
   // JMM 15-OCT-2015
   , WCP_CMD_REQUEST_METERS = 15

   // JMM 11-NOV-2015
   , WCP_CMD_REQUEST_CHANGE_STACKER = 16
   
   // FJC GameGateWay
   , WCP_CMD_SUBS_MACHINE_PARTIAL_CREDIT = 17
   , WCP_CMD_GAMEGATEWAY = 18
   , WCP_CMD_GAMEGATEWAY_GET_CREDIT = 19

   // JMM 22-DEC-2015
   , WCP_CMD_REQUEST_UPDATE_PCD_METERS = 20

   // FJC 21-FEB-2018
   , WCP_CMD_MOBIBANK_RECHARGE_NOTIFICATION = 21
   , WCP_CMD_MOBIBANK_REQUEST_TRANSFER = 22
   , WCP_CMD_MOBIBANK_CANCEL_REQUEST = 23         //Cancel Request from Movile Aplicattion

}

  public enum WCP_FundTransferType
  {
    FUND_TRANSFER_UNKNOWN = 0
  , FUND_TRANSFER_AFT = 1
  , FUND_TRANSFER_EFT = 2
  }

  public enum WCP_PlayWonAvailable
  {
    PLAY_WON_NOT_AVAILABLE = 0
  , PLAY_WON_AVAILABLE = 1  
  }

  /// <summary>
  /// WCP compression methods
  /// </summary>
  public enum WCP_CompressionMethod
  { 
    NONE
  }

  /// <summary>
  /// Device Codes
  /// </summary>
  public enum DeviceCodes
  {
    WCP_DEVICE_CODE_NO_DEVICE = 0
  , WCP_DEVICE_CODE_PRINTER = 1
  , WCP_DEVICE_CODE_NOTE_ACCEPTOR = 2
  , WCP_DEVICE_CODE_SMARTCARD_READER = 3
  , WCP_DEVICE_CODE_CUSTOMER_DISPLAY = 4
  , WCP_DEVICE_CODE_COIN_ACCEPTOR = 5
  , WCP_DEVICE_CODE_UPPER_DISPLAY = 6
  , WCP_DEVICE_CODE_BAR_CODE_READER = 7
  , WCP_DEVICE_CODE_INTRUSION = 8
  , WCP_DEVICE_CODE_UPS = 9
  , WCP_DEVICE_CODE_MODULE_IO = 15
  , WCP_DEVICE_CODE_DISPLAY_MAIN = 17
  , WCP_DEVICE_CODE_DISPLAY_TOP = 18
  , WCP_DEVICE_CODE_TOUCH_PAD = 19
  , WCP_DEVICE_CODE_KEYBOARD = 20
  , WCP_DEVICE_CODE_UNICASH_KIT_LCD = 21
  , WCP_DEVICE_CODE_UNICASH_KIT_KEYPAD = 22
  , WCP_DEVICE_CODE_UNICASH_KIT_CARD_READER = 23
  , WCP_DEVICE_CODE_UNICASH_KIT_BOARD = 24
  }

  /// <summary>
  /// Device Status
  /// </summary>
  static public class DeviceStatus
  {
    static public readonly Int32 WCP_DEVICE_STATUS_UNKNOWN = 0;
    static public readonly Int32 WCP_DEVICE_STATUS_OK = 1;
    static public readonly Int32 WCP_DEVICE_STATUS_ERROR = 2;
    static public readonly Int32 WCP_DEVICE_STATUS_NOT_INSTALLED = 100;
    static public readonly Int32 WCP_DEVICE_STATUS_PRINTER_OFF = 3;
    static public readonly Int32 WCP_DEVICE_STATUS_PRINTER_READY = 4;
    static public readonly Int32 WCP_DEVICE_STATUS_PRINTER_NOT_READY = 5;
    static public readonly Int32 WCP_DEVICE_STATUS_PRINTER_PAPER_OUT = 6;
    static public readonly Int32 WCP_DEVICE_STATUS_PRINTER_PAPER_LOW = 7;
    static public readonly Int32 WCP_DEVICE_STATUS_PRINTER_OFFLINE = 8;
    static public readonly Int32 WCP_DEVICE_STATUS_NOTE_ACCEPTOR_JAM = 3;
    static public readonly Int32 WCP_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR = 4;
    static public readonly Int32 WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL = 5;
    static public readonly Int32 WCP_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN = 6;
    static public readonly Int32 WCP_DEVICE_STATUS_UPS_NO_AC = 3;
    static public readonly Int32 WCP_DEVICE_STATUS_UPS_BATTERY_LOW = 4;
    static public readonly Int32 WCP_DEVICE_STATUS_UPS_OVERLOAD = 5;
    static public readonly Int32 WCP_DEVICE_STATUS_UPS_OFF = 6;
    static public readonly Int32 WCP_DEVICE_STATUS_UPS_BATTERY_FAIL = 7;
    static public readonly Int32 WCP_DEVICE_STATUS_SMARTCARD_REMOVED = 3;
    static public readonly Int32 WCP_DEVICE_STATUS_SMARTCARD_INSERTED = 4;
    static public readonly Int32 WCP_DEVICE_STATUS_COIN_ACCEPTOR_JAM = 3;
    static public readonly Int32 WCP_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR = 4;
    static public readonly Int32 WCP_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL = 5;
  }

  /// <summary>
  /// Priority
  /// </summary>
  public enum DevicePriority
  {
    WCP_DEVICE_PRIORITY_OK = 0
  , WCP_DEVICE_PRIORITY_WARNING = 1
  , WCP_DEVICE_PRIORITY_ERROR = 2
  }

  /// <summary>
  /// Operations
  /// </summary>
  public enum OperationCodes
  {
    WCP_OPERATION_CODE_NO_OPERATION = 0
  , WCP_OPERATION_CODE_START = 1
  , WCP_OPERATION_CODE_SHUTDOWN = 2
  , WCP_OPERATION_CODE_RESTART = 3
  , WCP_OPERATION_CODE_TUNE_SCREEN = 4
  , WCP_OPERATION_CODE_LAUNCH_EXPLORER = 5
  , WCP_OPERATION_CODE_ASSIGN_KIOSK = 6
  , WCP_OPERATION_CODE_DEASSIGN_KIOSK = 7
  , WCP_OPERATION_CODE_UNLOCK_DOOR = 8
  , WCP_OPERATION_CODE_LOGIN = 9
  , WCP_OPERATION_CODE_LOGOUT = 10
  , WCP_OPERATION_CODE_DISPLAY_SETTINGS = 11
  , WCP_OPERATION_CODE_DOOR_OPENED = 12
  , WCP_OPERATION_CODE_DOOR_CLOSED = 13
  , WCP_OPERATION_CODE_BLOCK_KIOSK = 14
  , WCP_OPERATION_CODE_UNBLOCK_KIOSK = 15
  , WCP_OPERATION_CODE_JACKPOT_WON = 16
  , WCP_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE = 17
  , WCP_OPERATION_CODE_CALL_ATTENDANT = 18
  , WCP_OPERATION_CODE_TEST_MODE_ENTER = 19
  , WCP_OPERATION_CODE_TEST_MODE_LEAVE = 20
  , WCP_OPERATION_CODE_LOGIN_ERROR = 21
  , WCP_OPERATION_CODE_ATTENDANT_MENU_ENTER = 22
  , WCP_OPERATION_CODE_ATTENDANT_MENU_EXIT = 23
  , WCP_OPERATION_CODE_OPERATOR_MENU_ENTER = 24
  , WCP_OPERATION_CODE_OPERATOR_MENU_EXIT = 25
  , WCP_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED = 26
  , WCP_OPERATION_CODE_HANDPAY_REQUESTED = 27
  , WCP_OPERATION_CODE_HANDPAY_RESET = 28
  , WCP_OPERATION_CODE_CARD_ABANDONED = 29
  , WCP_OPERATION_CODE_SECURITY_CASHOUT_CENTS = 30
  , WCP_OPERATION_CODE_TERMINAL_STATUS_CHANGE = 31
  , WCP_OPERATION_CODE_EVENT = 32
  , WCP_OPERATION_CODE_CHANGE_STACKER = 33
  , WCP_OPERATION_CODE_SECURITY_CASHOUT_UNITS = 34
  , WCP_OPERATION_CODE_GAME_FATAL_ERROR = 35
  , WCP_OPERATION_CODE_PENDING_TRANSFER_IN = 36
  , WCP_OPERATION_CODE_PENDING_TRANSFER_OUT = 37
  , WCP_OPERATION_CODE_TECH_OFFLINE_INSERTED = 38
  , WCP_OPERATION_CODE_TECH_OFFLINE_REMOVED = 39
  , WCP_OPERATION_CODE_DATABASE_COPY_FAIL = 40
  , WCP_OPERATION_CODE_DATABASE_ALL_FAIL = 41
  , WCP_OPERATION_CODE_VLT_BLOCKED = 42
  , WCP_OPERATION_CODE_VLT_UNBLOCKED = 43
  , WCP_OPERATION_CODE_TERMINAL_TRANSFER_STATUS = 44
  , WCP_OPERATION_CODE_TITO_WRONG_PRINTED_TICKET_ID = 45
  , WCP_OPERATION_CODE_TITO_CANT_READ_TICKET_INFO = 46
  , WCP_OPERATION_CODE_AFT_NOT_AUTHORIZED = 47
  // OPERATION ID 48..64 reserved for Wigos Center
  , WCP_OPERATION_CODE_USER_BLOCKED = 65
  , WCP_OPERATION_CODE_LOCK_AND_AFT_AMOUNT_MISMATCH = 66
  , WCP_OPERATION_CODE_TICKET_NOT_EVEN_MULTIPLE_MACHINE_DENOM = 67
  , WCP_OPERATION_CODE_BILL_IN = 68

  , WCP_OPERATION_CODE_BILL_IN_LIMIT_NOTE_ACCEPTOR_LOCK = 70
  , WCP_OPERATION_CODE_BILL_IN_LIMIT_EGM_LOCK           = 71
  , WCP_OPERATION_CODE_BILL_IN_LIMIT_HANDPAY_LOCK       = 72
}


public enum TechModeEnter
{
    WCP_TECH_MODE_UNKNOWN                 = 0
  , WCP_TECH_MODE_PIN_PAD                 = 1
  , WCP_TECH_MODE_CARD_WITH_VALIDATION    = 2
  , WCP_TECH_MODE_CARD_WITHOUT_VALIDATION = 3
 
}

  /// <summary>
  /// The WCP message header
  /// </summary>
  public class WCP_MsgHeader  
  {
    #region CONSTANTS

    public const string PROTOCOL_VERSION = "1.0.0.6";
    const string COMPRESSION_METHOD = "NONE";
    
    #endregion // CONSTANTS

    #region MEMBERS

    private string ProtocolVersion;
    public  string CompressionMethod;
    private DateTime SystemTime = DateTime.MinValue;
    private Int32 TimeZone;
    public string ServerId;
    public string TerminalId;
    public Int64 ServerSessionId;
    public Int64 TerminalSessionId;
    public WCP_MsgTypes MsgType;
    public Int64 SequenceId;
    public int MsgContentSize;
    public WCP_ResponseCodes ResponseCode;
    public string ResponseCodeText;
    //////public static Int64 SequenceIdResponse = 0;

    #endregion // MEMBERS

    #region PUBLIC

    /// <summary>
    ///   Gets the Protocol Version
    /// </summary>
    public string GetProtocolVersion
    {
      get { return ProtocolVersion; }
    }

    /// <summary>
    ///   Gets the Compression Method
    /// </summary>
    WCP_CompressionMethod GetCompressionMethod
    {
      get { return WCP_CompressionMethod.NONE; }
    }

    /// <summary>
    /// Gets the System Time
    /// </summary>
    public DateTime GetSystemTime
    {
      get { return SystemTime; }
    }

    /// <summary>
    /// Get time Zone
    /// </summary>
    public Int32 GetTimeZone
    {
      get { return TimeZone; }
    }

    /// <summary>
    /// Creates the reply header.
    /// The following fields are copied from the request:
    /// - ServerId
    /// - TerminalId
    /// - ServerSessionId
    /// - TerminalSessionId
    /// - SequenceId
    /// </summary>
    /// <param name="ResponseCode">Response Code</param>
    /// <returns>The reply header</returns>
    public WCP_MsgHeader ReplyHeader (WCP_ResponseCodes ResponseCode)
    {
      WCP_MsgHeader reply;

      reply = new WCP_MsgHeader ();
      reply.ServerId = ServerId;
      reply.TerminalId = TerminalId;
      reply.ServerSessionId = ServerSessionId;
      reply.TerminalSessionId = TerminalSessionId;
      reply.MsgType = MsgType;
      reply.MsgType++;
      reply.SequenceId = SequenceId;
      reply.ResponseCode = ResponseCode;
      reply.ResponseCodeText = "";

      return reply;
    }

    public void PrepareReply (WCP_MsgHeader RequestHeader)
    {
      ServerId = RequestHeader.ServerId;
      TerminalId = RequestHeader.TerminalId;
      ServerSessionId = RequestHeader.ServerSessionId;
      TerminalSessionId = RequestHeader.TerminalSessionId;
      SequenceId = RequestHeader.SequenceId;

      if ( MsgType == WCP_MsgTypes.WCP_MsgUnknown )
      {
        MsgType = RequestHeader.MsgType + 1;
      }
    }


    /// <summary>
    /// Extracts an enum object value from a 
    /// Enum Object giving the string name of 
    /// the value and getting the index.
    /// </summary>
    /// <param name="ResponseCode">Response Code</param>
    /// <returns>The reply header</returns>
    public static Object Decode(String str, Object defaultValue)
    {
      foreach (String s in System.Enum.GetNames(defaultValue.GetType()))
      {
        if (s.Equals(str))
        {
          return System.Enum.Parse(defaultValue.GetType(), s);
        }
      }
      return defaultValue;
    }

    public void LoadXml(XmlReader reader)
    {
      ProtocolVersion = XML.GetValue(reader, "ProtocolVersion");
      CompressionMethod = XML.GetValue(reader, "CompressionMethod");

      string time;
      string time_zone_hours;
      string time_zone_minutes;
      Int32 minutes;

      time = XML.GetValue(reader, "SystemTime");

      DateTime.TryParse(time, out SystemTime);

      time_zone_hours = time.Substring(time.Length - 6, 3);
      time_zone_minutes = time.Substring(time.Length - 2, 2);

      Int32.TryParse(time_zone_hours, out TimeZone);
      Int32.TryParse(time_zone_minutes, out minutes);
      TimeZone *= 60;
      if (TimeZone >= 0)
      {
        TimeZone += minutes;
      }
      else
      {
        TimeZone -= minutes;
      }

      ServerId = XML.GetValue(reader, "ServerId");
      TerminalId = XML.GetValue(reader, "TerminalId");
      long.TryParse(XML.GetValue(reader, "ServerSessionId"), out ServerSessionId);
      long.TryParse(XML.GetValue(reader, "TerminalSessionId"), out TerminalSessionId);
      MsgType = (WCP_MsgTypes)Decode(XML.GetValue(reader, "MsgType"), WCP_MsgTypes.WCP_MsgUnknown);
      long.TryParse(XML.GetValue(reader, "SequenceId"), out SequenceId);
      int.TryParse(XML.GetValue(reader, "MsgContentSize"), out MsgContentSize);
      ResponseCode = (WCP_ResponseCodes)Decode(XML.GetValue(reader, "ResponseCode"), WCP_ResponseCodes.WCP_RC_UNKNOWN);
      ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
    }

    public String ToXml ()
    {

      String _str_date;
      StringBuilder _strtemp = new StringBuilder(1024);

      if (SystemTime == DateTime.MinValue )
      {
        SystemTime = DateTime.Now;
      }

      _str_date = XML.XmlDateTimeString(SystemTime);

      _strtemp.Append("<WCP_MsgHeader><ProtocolVersion>");
      _strtemp.Append(ProtocolVersion);
      _strtemp.Append("</ProtocolVersion><CompressionMethod>");
      _strtemp.Append(CompressionMethod);
      _strtemp.Append("</CompressionMethod><SystemTime>");
      _strtemp.Append(_str_date);
      _strtemp.Append("</SystemTime><ServerId>");
      _strtemp.Append(XML.StringToXmlValue(ServerId));
      _strtemp.Append("</ServerId><TerminalId>");
      _strtemp.Append(XML.StringToXmlValue(TerminalId));
      _strtemp.Append("</TerminalId><ServerSessionId>");
      _strtemp.Append(ServerSessionId);
      _strtemp.Append("</ServerSessionId><TerminalSessionId>");
      _strtemp.Append(TerminalSessionId);
      _strtemp.Append("</TerminalSessionId><MsgType>");
      _strtemp.Append(MsgType);
      _strtemp.Append("</MsgType><SequenceId>");
      _strtemp.Append(SequenceId);
      _strtemp.Append("</SequenceId><MsgContentSize>");
      _strtemp.Append(MsgContentSize);
      _strtemp.Append("</MsgContentSize><ResponseCode>");
      _strtemp.Append(ResponseCode);
      _strtemp.Append("</ResponseCode><ResponseCodeText>");
      _strtemp.Append(XML.StringToXmlValue(ResponseCodeText));
      _strtemp.Append("</ResponseCodeText></WCP_MsgHeader>");
      
      return _strtemp.ToString();
    }
    /// <summary>
    /// Adds the header to the given Xml WCP message
    /// </summary>
    /// <param name="XmlWcpMessage">The message</param>
    public void ToXml (XmlDocument XmlWcpMessage)
    {
      XmlNode root;
      XmlNode header;
      XmlNode old_header;
      DateTime now;
      string str;

      now = DateTime.Now;
      str = XML.XmlDateTimeString(now);

      root = XmlWcpMessage.SelectSingleNode ("WCP_Message");
      header = XmlWcpMessage.CreateNode (XmlNodeType.Element, "", "WCP_MsgHeader", "");

      XML.AppendChild (header, "ProtocolVersion", ProtocolVersion);
      XML.AppendChild (header, "CompressionMethod", CompressionMethod);
      XML.AppendChild (header, "SystemTime", str);
      XML.AppendChild (header, "ServerId", ServerId);
      XML.AppendChild (header, "TerminalId", TerminalId);
      XML.AppendChild (header, "ServerSessionId", ServerSessionId.ToString ());
      XML.AppendChild (header, "TerminalSessionId", TerminalSessionId.ToString ());
      XML.AppendChild (header, "MsgType", MsgType.ToString ());
      XML.AppendChild (header, "SequenceId", SequenceId.ToString ());
      XML.AppendChild (header, "MsgContentSize", MsgContentSize.ToString ());
      XML.AppendChild (header, "ResponseCode", ResponseCode.ToString ());
      XML.AppendChild (header, "ResponseCodeText", ResponseCodeText);

      old_header = root.SelectSingleNode ("WCP_MsgHeader");
      if ( old_header != null )
      {
        root.RemoveChild (old_header);
      }

      if ( root.FirstChild != null )
      {
        root.InsertBefore (header, root.FirstChild);
      }
      else
      {
        root.AppendChild (header);
      }
    }

    /// <summary>
    /// Creates a header
    /// </summary>
    public WCP_MsgHeader ()
    {
      Init ();
    }

    #endregion // PUBLIC

    #region PRIVATE

    /// <summary>
    /// Initializes the header
    /// </summary>
    private void Init()
    {
      ProtocolVersion = PROTOCOL_VERSION;
      CompressionMethod = COMPRESSION_METHOD;
      SystemTime = System.DateTime.Now;
      ServerId = "";
      TerminalId = "";
      ServerSessionId = 0;
      TerminalSessionId = 0;
      MsgType = WCP_MsgTypes.WCP_MsgUnknown;
      SequenceId = 0;
      MsgContentSize = 0;
      ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
      ResponseCodeText = "";
    }


    #endregion // PRIVATE
  }
}
