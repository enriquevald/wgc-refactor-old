//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TerminalGroups.cs
// 
//   DESCRIPTION: Terminal groups updates
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 06-MAR-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-MAR-2014 AMF    First release.
// 27-MAR-2014 MPO    Performance improved for terminal generations
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;

namespace WSI.WCP
{
  public static class WCP_TerminalGroups
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes Terminal groups thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(ExploitTerminalGroups);
      _thread.Name = "ExploitTerminalGroups";
      _thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Exploit Terminal Groups
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static private void ExploitTerminalGroups()
    {
      while (true)
      {
        try
        {
          // min => 1 min. max => 24 h. - Default 5 min
          TimeSpan _time_span = new TimeSpan(0, GeneralParam.GetInt32("WCP", "Groups.CheckChangesAfterMinutes", 5, 1, 1440), 0);
          //TimeSpan _time_span = new TimeSpan(0, 0, 2); FOR TEST PURPOSE
          Thread.Sleep(_time_span);

          if (Services.IsPrincipal("WCP"))
          {
            if (!GroupsService.GenerateTerminals())
            {
              Log.Warning("WCP_TerminalGroups. Exception in function: ExploitTerminals");
            }
          }

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // while
    } // ExploitTerminalGroups

  } // class WCP_TerminalGroups

} // namespace WSI.WCP
