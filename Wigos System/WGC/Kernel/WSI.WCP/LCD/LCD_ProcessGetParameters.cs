//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LCD_ProcessGetPlayerGifts.cs
// 
//   DESCRIPTION: LCD_ProcessGetPlayerGifts class
// 
//        AUTHOR: Joaquim Calero & Xavi Cots
// 
// CREATION DATE: 25-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 25-JUL-2014 JCA & XCD  First version.
// 11-DIC-2015 JCA & FJC  BackLog Item 7311: Bonoplay=> Mixed Mode.
// 22-FEB-2016 FJC        Product Backlog Item 9434:PimPamGo: Otorgar Premios
// 04-APR-2016 FJC        PBI 9105:BonoPlay: LCD: Cambios varios
// 22-JUN-2017 FJC        WIGOS-3142 WS2S - Set / Retrieve Reserved Credit on the Cashier
// 21-SEP-2017 FJC        WIGOS-5280 Reserve EGM - Allow to reserve EGM in the LCD Intouch.
// 29-NOV-2017 FJC        WIGOS-3592 MobiBank: InTouch - Recharge request
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_WKT
{

  private static void LCD_ProcessGetParameters(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_LCD_MsgGetParametersReply _response;
    WCP_LCD_MsgGetParameters _request;
    List<Int32> _list_message_id;
    String _provider_id;
    TerminalList _terminal_list;

    _request = (WCP_LCD_MsgGetParameters)WktRequest.MsgContent;
    _response = (WCP_LCD_MsgGetParametersReply)WktResponse.MsgContent;

    _list_message_id = new List<Int32>();
    _list_message_id.Clear();
    _provider_id = String.Empty;
    _terminal_list = new TerminalList();

    try
    {
      InitializeResponse(ref _response);

      _response.m_game_gateway_params.AwardPrizes = GeneralParam.GetInt32("GameGateway", "AwardPrizes", 1);

      DB_ExecuteToGetParams(ref _response);

      //*** FILL GAMEGATEWAY
      // 1. Get GP's about GameGateWay 
      // 2. Transfer Parameters to structure
      if (Misc.IsGameGatewayEnabled() && _terminal_list.IsGameGatewayTerminalEnabled(_request.Terminal_Id))
      {
        _provider_id = GeneralParam.GetString("GameGateway", "Provider", string.Empty);
        if (GeneralParam.GetBoolean("GameGateway", String.Format("Provider.{0}.Enabled", _provider_id), false))
        {
          FillGameGateway(ref _response, _provider_id);
        }
      }

      //*** FILL FB
      // 1. Get GP's about GameGateWay 
      // 2. Transfer Parameters to structure
      if (GeneralParam.GetBoolean("FB", "Enabled", false))
      {
        FillFB(ref _response);
      }

      //*** FILL TerminalDraw
      // 1. Get GP's about TerminalDraw
      // 2. Transfer Parameters to structure
      if (Misc.IsTerminalDrawEnabled())
      {
        _response.m_terminal_draw_params.Enabled = 1;
      }

      //*** FILL InTouchWebURL
      _response.m_intouch_web_url = GeneralParam.GetString("DisplayTouch", "InTouchWeb.Url", string.Empty);

      //*** FILL TerminalReserve
      // 1. Get GP's about TerminalReserve
      // 2. Transfer Parameters to structure
      if (Misc.IsTerminalReservationEnabled())
      {
        _response.m_terminal_reserve_params.Enabled = 1;
      }

      //*** FILL MobiBank
      if (MobileBankConfig.MobileBankLinkDeviceMode == MobileBankLinkDeviceMode.DeviceWithLCDInTouch)
      {
        _response.m_mobibank_params.Enabled = 1;
      }

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;

    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);

      ResetResponse(ref _response);

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }

  }

  /// <summary>
  /// Filter functionalities form GP value
  /// </summary>
  /// <returns></returns>
  private static String GetSqlWhereFunctionalities()
  {
    String _where;

    _where = String.Empty;

    // GameGateway
    if (!Misc.IsGameGatewayEnabled())
    {
      _where += String.Format(" AND FUN_FUNCTION_ID != {0}", (Int32)TYPE_LCD_FUNCTIONALITIES.GAMEGATEWAY);
    }

    // FB
    if(!GeneralParam.GetBoolean("FB", "Enabled", false))
    {
      _where += String.Format(" AND FUN_FUNCTION_ID != {0}", (Int32)TYPE_LCD_FUNCTIONALITIES.FB);
    }

    return _where;
  } // GetSqlWhereFunctionalities

  /// <summary>
  /// Sql to get lcd Funcionalities
  /// </summary>
  /// <param name="_sb"></param>
  private static String DB_SqlLcdfuncionalitiesAndResources()
  {
    StringBuilder _sb;

    _sb = new StringBuilder();

    _sb.AppendLine("    SELECT   FUN_FUNCTION_ID                    ");
    _sb.AppendLine("      FROM   LCD_FUNCTIONALITIES                ");
    _sb.AppendLine("     WHERE   FUN_ENABLED = 1                    ");
    _sb.AppendLine(GetSqlWhereFunctionalities());

    _sb.AppendLine(" ;                                              ");
    _sb.AppendLine("    SELECT   RES_RESOURCE_ID                    ");
    _sb.AppendLine("           , RES_EXTENSION                      ");
    _sb.AppendLine("           , RES_HASH                           ");
    _sb.AppendLine("           , RES_LENGTH                         ");
    _sb.AppendLine("           , CIM_IMAGE_ID                       ");
    _sb.AppendLine("      FROM   WKT_RESOURCES                      ");
    _sb.AppendLine("INNER JOIN   LCD_IMAGES                         ");
    _sb.AppendLine("        ON   RES_RESOURCE_ID = CIM_RESOURCE_ID  ");

    return _sb.ToString();

  } // DB_SqlLcdfuncionalities

  /// <summary>
  /// Fill GameWateWay
  /// </summary>
  /// <param name="_response"></param>
  private static void FillGameGateway(ref WCP_LCD_MsgGetParametersReply _response, String _provider_id)
  {
    _response.m_game_gateway_params.GameGateway = 1;
    _response.m_game_gateway_params.ReservedCredit = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);
    _response.m_game_gateway_params.PartnerId = GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.PartnerId", _provider_id), String.Empty);
    _response.m_game_gateway_params.ProviderName = GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.Name", _provider_id), String.Empty);
    _response.m_game_gateway_params.Url = GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.Uri", _provider_id), String.Empty);
    _response.m_game_gateway_params.UrlTest = GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.UriTest", _provider_id), String.Empty);
    _response.m_game_gateway_params.AwardPrizes = GeneralParam.GetInt32("GameGateway", "AwardPrizes", 1);
  } // FillGameGateway

  /// <summary>
  /// Fill FB data
  /// </summary>
  private static void FillFB(ref WCP_LCD_MsgGetParametersReply _response)
  {
    _response.m_fb_params.Enabled = 1;
    _response.m_fb_params.PinRequest = GeneralParam.GetInt32("FB", "PinRequest", 0);
    _response.m_fb_params.HostAddress = GeneralParam.GetString("FB", "HostAddress", String.Empty);
    _response.m_fb_params.Url = GeneralParam.GetString("FB", "Web.Url", String.Empty);
    _response.m_fb_params.Login = GeneralParam.GetString("FB", "Login", String.Empty);
    _response.m_fb_params.Password = GeneralParam.GetString("FB", "Password", String.Empty);
  } // FillFB

  /// <summary>
  /// Reset Response data
  /// </summary>
  private static void ResetResponse(ref WCP_LCD_MsgGetParametersReply _response)
  {
    _response.Functions = new List<Int32>();
    _response.Images = new WKT_Images();
    _response.Resources = new WKT_ResourceInfoList();
    _response.m_game_gateway_params = new LCDMessages.LcdGameGateWayParams();
    _response.m_fb_params = new LCDMessages.LcdFBParams();
  } // ResetResponse

  /// <summary>
  /// set default values in response data
  /// </summary>
  /// <param name="_response"></param>
  private static void InitializeResponse(ref WCP_LCD_MsgGetParametersReply _response)
  {
    _response.Functions = new List<Int32>();
    _response.Images = new WKT_Images();
    _response.Resources = new WKT_ResourceInfoList();

    _response.m_game_gateway_params = new LCDMessages.LcdGameGateWayParams();
    _response.m_game_gateway_params.ProviderName = string.Empty;
    _response.m_game_gateway_params.PartnerId = string.Empty;
    _response.m_game_gateway_params.Url = string.Empty;
    _response.m_game_gateway_params.UrlTest = string.Empty;

    _response.m_fb_params = new LCDMessages.LcdFBParams();
    _response.m_fb_params.Enabled = 0;
    _response.m_fb_params.PinRequest = 0;
    _response.m_fb_params.HostAddress = String.Empty;
    _response.m_fb_params.Url = String.Empty;
    _response.m_fb_params.Login = String.Empty;
    _response.m_fb_params.Password = String.Empty;
  } // InitializeResponse

  private static void DB_ExecuteToGetParams(ref WCP_LCD_MsgGetParametersReply _response)
  {
    using (DB_TRX _db_trx = new DB_TRX())
    {
      // Get 
      //    - Kiosk Functionalities (only enabled)
      //    - Kiosk Customized Images
      //    - Customized Messages
      //      Access to GENERAL_PARAMS table to obtain the Id of every customized message. 
      //      Message contents will be accessed in the ordinary way with GeneralParam.Value ()
      //    - Operation schedule interval

      using (SqlCommand _sql_cmd = new SqlCommand(DB_SqlLcdfuncionalitiesAndResources()))
      {
        using (SqlDataReader _sql_dr = _db_trx.ExecuteReader(_sql_cmd))
        {
          // Fill Functionalities
          while (_sql_dr.Read())
          {
            _response.Functions.Add(_sql_dr.GetInt32(0));
          }

          // Fill Custom Images, and Resources of this Images
          if (_sql_dr.NextResult())
          {
            while (_sql_dr.Read())
            {
              _response.Images.Add(new WKT_Image(_sql_dr.GetInt32(4), _sql_dr.GetInt64(0)));
              _response.Resources.Add(new WKT_ResourceInfo(_sql_dr.GetInt64(0), _sql_dr.GetString(1), _sql_dr.GetSqlBytes(2).Value, _sql_dr.GetInt32(3)));
            }
          }
        }
      }
    } // Using DB_TRX
  }
}