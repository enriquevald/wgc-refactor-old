//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LCD_ProcessGetPlayerGifts.cs
// 
//   DESCRIPTION: LCD_ProcessGetPromoBalance class
// 
//        AUTHOR: Xavi Cots
// 
// CREATION DATE: 25-NOV-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 25-NOV-2014 XCD  First version.
// 04-FEB-2016 ETP  Fixed bug 8976 added new Trx_GetTerminalInfoByExternalId 
// 19-FEB-2016 ETP  Product Backlog Item 9748: Multiple buckets: Canje de buckets RE
// 22-FEB-2016 FJC  Product Backlog Item 9434:PimPamGo: Otorgar Premios
// 16-MAR-2016 FJC  PBI 9105: BonoPlay: LCD: Cambios varios
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_WKT
{
  private static void LCD_ProcessGetPromoBalance(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_LCD_MsgGetPromoBalanceReply _response;
    WCP_LCD_MsgGetPromoBalance _request;
    WCP_ResponseCodes _rsp_code;

    String _trackdata;
    String _pin;    
    Int64 _account_id;
    Decimal _player_points;
    MultiPromos.AccountBalance _balance;
    MultiPromos.AccountInfo _acc_info;
    WSI.Common.Terminal.TerminalInfo _terminal_info;

    _request = (WCP_LCD_MsgGetPromoBalance)WktRequest.MsgContent;
    
    _response = (WCP_LCD_MsgGetPromoBalanceReply)WktResponse.MsgContent;

    String _terminalId = WktRequest.MsgHeader.TerminalId;

    try
    {
      _trackdata = _request.Player.Trackdata;
      _pin = _request.Player.Pin;

      if (_request.GameGatewayPromoBalanceAction == (Int16)MultiPromos.ENUM_GAME_GATEWAY_PROMO_BALANCE_ACTION.GAME_GATEWAY_PROMO_BALANCE_ACTION_COLLECT_PRIZE)
      {
        // GameGateway Granted Prizes
        WSI.Common.GameGateway.ManageGameGatewayBulk(_request.AccountId);
      }

      if ( (_request.GameGatewayPromoBalanceAction == (Int16)MultiPromos.ENUM_GAME_GATEWAY_PROMO_BALANCE_ACTION.GAME_GATEWAY_PROMO_BALANCE_ACTION_WITHOUT_PIN ||
            _request.GameGatewayPromoBalanceAction == (Int16)MultiPromos.ENUM_GAME_GATEWAY_PROMO_BALANCE_ACTION.GAME_GATEWAY_PROMO_BALANCE_ACTION_COLLECT_PRIZE) &&
            _request.AccountId > 0)
      {
        _account_id = _request.AccountId;
      }
      else
      {
        if (!WKT_PlayerLogin(_trackdata, _pin, out _account_id, out _rsp_code))
        {
          WktResponse.MsgHeader.ResponseCode = _rsp_code;

          return;
        }
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        FlagBucketId _flags_buckets;

        _flags_buckets = FlagBucketId.NR;

        //ETP 10-FEB-2016 Include flags for NR and RE Buckets
        if (!WSI.Common.TITO.Utils.IsTitoMode())
        {
          _flags_buckets |= FlagBucketId.RE;
        }

        // JMV & RCI 26-JAN-2016: Include NR bucket credit
        if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, String.Empty, MultiPromos.AccountBalance.Zero, 0, false, _flags_buckets, out _acc_info, _db_trx.SqlTransaction))
        {
          WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;

          return;
        }

        if (!WSI.Common.Terminal.Trx_GetTerminalInfoByExternalId(_terminalId, out _terminal_info, _db_trx.SqlTransaction))
        {
          WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
        
          return;
        }

        _balance = CurrencyExchange.GetExchange(_acc_info.FinalBalance, CurrencyExchange.GetNationalCurrency(), _terminal_info.CurrencyCode, _db_trx.SqlTransaction);

        _player_points = _acc_info.FinalPoints;
      }

      _response.PromoBalance = new MultiPromos.PromoBalance(_balance, _player_points);
      
      //GameGateway Awarded Prizes (check awarded prizes)
      _response.GameGateWayAwardedPrizeAmount = WSI.Common.GameGateway.GetGameGatewayTotalPrizesPendingAmount(_request.AccountId);

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);

      _response.PromoBalance = new MultiPromos.PromoBalance();
      
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }

  } //WKT_ProcessGetParameters

}