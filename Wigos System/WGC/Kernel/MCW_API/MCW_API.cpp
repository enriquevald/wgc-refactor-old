//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : MCW_API.cpp
// 
//   DESCRIPTION : Functions and Methods to handle Magnetic Card Writers communications.
// 
//        AUTHOR : Xavier Ib��ez
// 
// CREATION DATE : 22-SEP-2008
// 
// REVISION HISTORY
// 
// Date        Author Description
//----------- ------ -----------------------------------------------------------
// 22-SEP-2008 XID    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_API
#define INCLUDE_QUEUE_API
#define INCLUDE_NLS_API
#define INCLUDE_MCW_API
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC

#include "CommonDef.h"

#include "MCW_APIInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

// Common API data
TYPE_COMMON_API_DATA  GLB_API;

TYPE_MODULE_DATA      GLB_ModuleData;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

BOOL GetModelName (WORD IdxModel, TCHAR * pModelName);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE : The MCW public interface
//
//  PARAMS :
//     - INPUT :
//      - pApiInputParams
//      - pApiInputUserData
//     - OUTPUT :
//      - pApiOutputParams
//      - pApiOutputUserData
//
// RETURNS :
//     - API_STATUS_OK
//     - API_STATUS_PARAMETER_ERROR
//     - API_STATUS_CB_SIZE_ERROR
//     - API_STATUS_EVENT_OBJECT_ERROR
//     - API_STATUS_NO_RESOURCES_AVAILABLE
//
// NOTES :

MCW_API WORD WINAPI Mcw_API (API_INPUT_PARAMS     * pApiInputParams,
                             API_INPUT_USER_DATA  * pApiInputUserData,
                             API_OUTPUT_PARAMS    * pApiOutputParams,
                             API_OUTPUT_USER_DATA * pApiOutputUserData)
{
  static TCHAR _function_name[] = _T("Mcw_API");

  return Common_API_Entry (pApiInputParams,
                           pApiInputUserData,
                           pApiOutputParams,
                           pApiOutputUserData,
                           _function_name);

} // Mcw_API

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Fills model dll names.
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//

void  Mcw_InitDLLEntryNames (void)
{
  memset (&GLB_ModuleData.model_dll_entry_names, 0, sizeof (GLB_ModuleData.model_dll_entry_names));
  GLB_ModuleData.model_dll_entry_names.num_of_entries = 0;
  _tcscpy (GLB_ModuleData.model_dll_entry_names.dll_entry_name[MCW_DLL_ENTRY_IDX_INIT], _T("Mcw_Init"));
  GLB_ModuleData.model_dll_entry_names.num_of_entries++;
  _tcscpy (GLB_ModuleData.model_dll_entry_names.dll_entry_name[MCW_DLL_ENTRY_IDX_STATUS], _T("Mcw_Status"));
  GLB_ModuleData.model_dll_entry_names.num_of_entries++;
  _tcscpy (GLB_ModuleData.model_dll_entry_names.dll_entry_name[MCW_DLL_ENTRY_IDX_WRITE], _T("Mcw_Write"));
  GLB_ModuleData.model_dll_entry_names.num_of_entries++;
  _tcscpy (GLB_ModuleData.model_dll_entry_names.dll_entry_name[MCW_DLL_ENTRY_IDX_CLOSE], _T("Mcw_Close"));
  GLB_ModuleData.model_dll_entry_names.num_of_entries++;
  _tcscpy (GLB_ModuleData.model_dll_entry_names.dll_entry_name[MCW_DLL_ENTRY_IDX_CANCEL_WRITE], _T("Mcw_CancelWrite"));
  GLB_ModuleData.model_dll_entry_names.num_of_entries++;

  return;

} // Mcw_InitDLLEntryNames

//------------------------------------------------------------------------------
// PURPOSE : Loads specific model dll and gets dll function address
//
//  PARAMS :
//      - INPUT :
//          - Model
//          - pDllEntryList
//
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

BOOL  Mcw_LoadModelDll (WORD                     Model,
                        TYPE_MCW_DLL_ENTRY_LIST  * pDllEntryList)
{
  static TCHAR _function_name[] = _T("Mcw_LoadModelDll");
  TCHAR  _log_param1[MCW_AUX_STRING_LENGTH];

  TCHAR     _dll_name[MCW_MAX_DLL_NAME];
  HMODULE   _dll_handle;
  WORD      _idx_entry;

  memset (pDllEntryList, 0, sizeof (TYPE_MCW_DLL_ENTRY_LIST));

  // Build dll name
  _stprintf (_dll_name, MCW_MODEL_DLLNAME, (int) Model);

  // Load dll
  _dll_handle = LoadLibrary (_dll_name);
  if ( _dll_handle == NULL )
  {
    // Logger message
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_LoggerMsg (NLS_ID_LOGGER(2), MODULE_NAME, _function_name, _log_param1, _T("LoadLibrary"));

    return FALSE;
  }
  pDllEntryList->dll_handle = _dll_handle;

  // Load DLL functions addresses
  for ( _idx_entry = 0; _idx_entry < GLB_ModuleData.model_dll_entry_names.num_of_entries; _idx_entry++ )
  {
    pDllEntryList->dll_entry_address[_idx_entry] =
       (MCW_DLL_ENTRY_ADDRESS) Common_GetProcAddress (_dll_handle,
                                                      GLB_ModuleData.model_dll_entry_names.dll_entry_name[_idx_entry]);
     if ( pDllEntryList->dll_entry_address[_idx_entry] == NULL )
     {
       // Logger message
       _stprintf (_log_param1, _T("%s.%s"), _dll_name, GLB_ModuleData.model_dll_entry_names.dll_entry_name[_idx_entry]);
       Common_LoggerMsg (NLS_ID_LOGGER(7), MODULE_NAME, _T("GetProcAddress"), _log_param1);
     }
   } // for

  return TRUE;

} // Mcw_LoadModelDll

//------------------------------------------------------------------------------
// PURPOSE : Unloads specific model dll
//
//  PARAMS :
//      - INPUT :
//        - pDllEntryList
//
//      - OUTPUT :
//        - pDllEntryList
//
// RETURNS :
//
//   NOTES :

BOOL  Mcw_UnloadModelDll (TYPE_MCW_DLL_ENTRY_LIST  * pDllEntryList)
{
  static TCHAR _function_name[] = _T("Mcw_UnloadModelDll");
  TCHAR  _log_param1[MCW_AUX_STRING_LENGTH];
  BOOL _bool_rc;

  // Unload dll
  _bool_rc = FreeLibrary (pDllEntryList->dll_handle);
  if ( !_bool_rc )
  {
    // Logger message
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_LoggerMsg (NLS_ID_LOGGER(2), MODULE_NAME, _function_name, _log_param1, _T("FreeLibrary"));

    return FALSE;
  }

  memset (pDllEntryList, 0, sizeof (TYPE_MCW_DLL_ENTRY_LIST));

  return TRUE;

} // Mcw_UnloadModelDll

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Module Init"
//            - Reads configuration
//            - Initializes tcp/ip
//            - Initializes node data in mcw_node table
//
//  PARAMS :
//
//      - INPUT :
//        - ApiRequestParams
//
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void Mcw_ModuleInit (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                     WORD * pRequestOutput1, 
                     WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("Mcw_ModuleInit");
  MCW_OUD_MODULE_INIT * p_mcw_oud_module_init;
  WORD  _num_models;
  WORD  _idx_model;
  TCHAR _model_name[MCW_MODEL_NAME_SIZE];
  BOOL  _bool_rc;

  * pRequestOutput1 = MCW_STATUS_OK;
  * pRequestOutput2 = 0;

  // Set initialization already tried
  GLB_API.init_tried = TRUE;

  // Return model list
  p_mcw_oud_module_init = (MCW_OUD_MODULE_INIT *) pApiRequestParams->p_api_output_user_data;

  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_mcw_oud_module_init->control_block, 
                                           sizeof (MCW_OUD_MODULE_INIT),
                                           _function_name, 
                                           _T("OUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  // Get model data
  _num_models = 0;
  for ( _idx_model = 1; _idx_model <= MCW_MAX_MODELS; _idx_model++ )
  {
    _bool_rc = GetModelName (_idx_model, _model_name);
    if ( _bool_rc )
    {
      p_mcw_oud_module_init->model_data[_num_models].model = _idx_model;
      _tcsncpy (p_mcw_oud_module_init->model_data[_num_models].model_name, _model_name, MCW_MODEL_NAME_SIZE);
      p_mcw_oud_module_init->model_data[_num_models].model_name[MCW_MODEL_NAME_SIZE] = 0;
      _num_models++;
    }
  } // for

  p_mcw_oud_module_init->num_models = _num_models;

  // Module initialized
  GLB_API.init = TRUE;

  return;

} // Mcw_ModuleInit

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Device Init"
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void Mcw_DeviceInit (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                     WORD * pRequestOutput1, 
                     WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("Mcw_DeviceInit");
  MCW_IUD_DEVICE_INIT * p_mcw_iud_device_init;
  TYPE_MCW_INIT_INPUT  _mcw_dll_input;
  TYPE_MCW_INIT_OUTPUT _mcw_dll_output;
  BOOL _bool_rc;
  WORD _rc;

  * pRequestOutput1 = MCW_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  p_mcw_iud_device_init = (MCW_IUD_DEVICE_INIT *) pApiRequestParams->api_input_user_data_buffer;

  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_mcw_iud_device_init->control_block, 
                                           sizeof (MCW_IUD_DEVICE_INIT),
                                           _function_name, 
                                           _T("IUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  GLB_ModuleData.model_dll_data.model = p_mcw_iud_device_init->model;
  GLB_ModuleData.model_dll_data.init  = FALSE;

  // Load DLL and init dll entry addresses
  _bool_rc = Mcw_LoadModelDll (GLB_ModuleData.model_dll_data.model, &GLB_ModuleData.model_dll_data.dll_entry_list);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }
  GLB_ModuleData.model_dll_data.init = TRUE;

  // Call Model Dll

  _mcw_dll_input.control_block = sizeof (_mcw_dll_input);
  _mcw_dll_output.control_block = sizeof (_mcw_dll_output);

  if ( GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_INIT] )
  {
    _rc = (GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_INIT]) (&_mcw_dll_input, &_mcw_dll_output);
  }
  else
  {
    // Function not available, default return status
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  * pRequestOutput1 = _rc;

  return;

} // Mcw_DeviceInit

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Device Close"
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void Mcw_DeviceClose (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                      WORD * pRequestOutput1, 
                      WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("Mcw_DeviceClose");
  TYPE_MCW_CLOSE_INPUT  _mcw_dll_input;
  TYPE_MCW_CLOSE_OUTPUT _mcw_dll_output;
  BOOL _bool_rc;
  WORD _rc;

  * pRequestOutput1 = MCW_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  // Call Model Dll

  _mcw_dll_input.control_block = sizeof (_mcw_dll_input);
  _mcw_dll_output.control_block = sizeof (_mcw_dll_output);

  if ( GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_CLOSE] )
  {
    _rc = (GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_CLOSE]) (&_mcw_dll_input, &_mcw_dll_output);

    // Unload dll
    _bool_rc = Mcw_UnloadModelDll (&GLB_ModuleData.model_dll_data.dll_entry_list);
    memset (&GLB_ModuleData.model_dll_data, 0, sizeof (GLB_ModuleData.model_dll_data));
    if ( !_bool_rc )
    {
      * pRequestOutput1 = MCW_STATUS_ERROR;
      return;
    }
  }
  else
  {
    // Function not available, default return status
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  * pRequestOutput1 = _rc;

  return;

} // Mcw_DeviceClose

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Device Status Query"
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void Mcw_DeviceStatusQuery (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                            WORD * pRequestOutput1, 
                            WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("Mcw_DeviceStatusQuery");
  MCW_OUD_DEVICE_STATUS_QUERY * p_mcw_oud_device_status_query;
  TYPE_MCW_GET_STATUS_INPUT   _mcw_dll_input;
  TYPE_MCW_GET_STATUS_OUTPUT  _mcw_dll_output;
  BOOL _bool_rc;
  WORD _rc;

  * pRequestOutput1 = MCW_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  p_mcw_oud_device_status_query = (MCW_OUD_DEVICE_STATUS_QUERY *) pApiRequestParams->p_api_output_user_data;

  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_mcw_oud_device_status_query->control_block, 
                                           sizeof (MCW_OUD_DEVICE_STATUS_QUERY),
                                           _function_name, 
                                           _T("OUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  // Call Model Dll

  _mcw_dll_input.control_block = sizeof (_mcw_dll_input);
  _mcw_dll_output.control_block = sizeof (_mcw_dll_output);

  if ( GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_STATUS] )
  {
    _rc = (GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_STATUS]) (&_mcw_dll_input, &_mcw_dll_output);
  }
  else
  {
    // Function not available, default return status
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  p_mcw_oud_device_status_query->status = _mcw_dll_output.status;
  * pRequestOutput1 = _rc;

  return;

} // Mcw_DeviceStatusQuery

//------------------------------------------------------------------------------
// PURPOSE : Process for "Write" request.
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void Mcw_Write (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                WORD * pRequestOutput1, 
                WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("Mcw_Write");
  MCW_IUD_WRITE        * p_mcw_iud_write;
  TYPE_MCW_WRITE_INPUT  _mcw_dll_input;
  TYPE_MCW_WRITE_OUTPUT _mcw_dll_output;
  BOOL _bool_rc;
  WORD _rc;

  * pRequestOutput1 = MCW_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  p_mcw_iud_write = (MCW_IUD_WRITE *) pApiRequestParams->api_input_user_data_buffer;

  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_mcw_iud_write->control_block, 
                                           sizeof (MCW_IUD_WRITE),
                                           _function_name, 
                                           _T("IUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  _mcw_dll_input.card_data  = p_mcw_iud_write->card_data;

  // Call Model Dll

  _mcw_dll_input.control_block = sizeof (_mcw_dll_input);
  _mcw_dll_output.control_block = sizeof (_mcw_dll_output);

  if ( GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_WRITE] )
  {
    _rc = (GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_WRITE]) (&_mcw_dll_input, &_mcw_dll_output);
  }
  else
  {
    // Function not available, default return status
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  * pRequestOutput1 = _rc;

  return;

} // Mcw_Write

//------------------------------------------------------------------------------
// PURPOSE : Process for "Cancel Write" request.
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void Mcw_CancelWrite (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                      WORD * pRequestOutput1, 
                      WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("Mcw_CancelWrite");
  TYPE_MCW_CANCEL_WRITE_INPUT  _mcw_dll_input;
  TYPE_MCW_CANCEL_WRITE_OUTPUT _mcw_dll_output;
  BOOL _bool_rc;
  WORD _rc;

  * pRequestOutput1 = MCW_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  // Call Model Dll

  _mcw_dll_input.control_block = sizeof (_mcw_dll_input);
  _mcw_dll_output.control_block = sizeof (_mcw_dll_output);

  if ( GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_CANCEL_WRITE] )
  {
    _rc = (GLB_ModuleData.model_dll_data.dll_entry_list.dll_entry_address[MCW_DLL_ENTRY_IDX_CANCEL_WRITE]) (&_mcw_dll_input, &_mcw_dll_output);
  }
  else
  {
    // Function not available, default return status
    * pRequestOutput1 = MCW_STATUS_ERROR;
    return;
  }

  * pRequestOutput1 = _rc;

  return;

} // Mcw_CancelWrite

//------------------------------------------------------------------------------
// PURPOSE : Common processing of user requests
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT : None
//
// RETURNS :
//
//   NOTES :

void Mcw_ProcessRequest (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                         TYPE_API_THREAD_DATA    * pThreadData)
{
  static TCHAR  _function_name[] = _T("Mcw_ProcessRequest");
  TCHAR  _log_param1[MCW_AUX_STRING_LENGTH];

  WORD   _request_output_1 = MCW_STATUS_ERROR;
  WORD   _request_output_2 = 0;
  BOOL   _request_exec_end = TRUE;

  switch ( pApiRequestParams->api_input_paramas.function_code )
  {
    case MCW_CODE_MODULE_INIT:
      Mcw_ModuleInit (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;
    
    case MCW_CODE_DEVICE_INIT:
      Mcw_DeviceInit (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;

    case MCW_CODE_DEVICE_CLOSE:
      Mcw_DeviceClose (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;

    case MCW_CODE_DEVICE_STATUS_QUERY:
      Mcw_DeviceStatusQuery (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;
    
    case MCW_CODE_WRITE:
      Mcw_Write (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;

    case MCW_CODE_CANCEL_WRITE:
      Mcw_CancelWrite (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;

    default:
      // Unknown function code
      _stprintf (_log_param1, _T("%hu"), pApiRequestParams->api_input_paramas.function_code);
      Common_LoggerMsg (NLS_ID_LOGGER(7), MODULE_NAME, _function_name, _T("FunctionCode"), _log_param1);

      _request_output_1 = MCW_STATUS_ERROR;
      _request_output_2 = 0;
    break;

  } // switch

  if ( _request_exec_end )
  {

    // Notify completion status to user
    Common_API_NotifyRequestStatus (_request_output_1,
                                    _request_output_2,
                                    &pApiRequestParams->api_input_paramas,
                                    pApiRequestParams->p_api_output_params);
  }

} // Mcw_ProcessRequest

//------------------------------------------------------------------------------
// PURPOSE : API thread to handle user requests.
//
//  PARAMS :
//      - INPUT :
//        - ThreadParameter
//      - OUTPUT : None
//
// RETURNS :
//
//   NOTES :

DWORD WINAPI Mcw_APIThread (LPVOID ThreadParameter)
{
  static TCHAR  _function_name[] = _T("Mcw_APIThread");
  TCHAR  _log_param1[MCW_AUX_STRING_LENGTH];

  WORD   _call_status;
  DWORD  _wait_status;
  DWORD  _request_buffer_length;
  TYPE_API_REQUEST_PARAMS _api_request_params;
  TYPE_API_THREAD_DATA    * p_thread_data;

  p_thread_data = (TYPE_API_THREAD_DATA *) ThreadParameter;

  // First call to read queue
  _call_status = Queue_Read (p_thread_data->request_queue, // Queue
                             p_thread_data->queue_event,   // Notification Event object
                             NULL,                         // APC routine
                             NULL,                         // APC data
                             MCW_QUEUE_ITEM_SIZE,          // Output buffer size
                             &_api_request_params,         // User Buffer
                             &_request_buffer_length,      // Actual Read buffer length
                             NULL);                        // Read handle
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Critical error: Thread end
    // Logger message
    _stprintf (_log_param1, _T("%hu"), _call_status);
    Common_LoggerMsg (NLS_ID_LOGGER(2), MODULE_NAME, _function_name, _log_param1, _T("Queue_Read"));

    return 0;
  }

  // Infinite loop to handle requests and events
  while ( TRUE ) 
  {
    // Wait with using the private event
    _wait_status = WaitForSingleObjectEx (p_thread_data->queue_event,  // Handle
                                          INFINITE,                    // Milliseconds
                                          TRUE);                       // Alertable
    switch ( _wait_status )
    {
      case WAIT_IO_COMPLETION:
        // Completion routine
      break;
      
      case WAIT_OBJECT_0:
        // Request received
        Mcw_ProcessRequest (&_api_request_params, p_thread_data);

        // Call again to read snd queue
        _call_status = Queue_Read (p_thread_data->request_queue, // Queue
                                   p_thread_data->queue_event,   // Notification Event object
                                   NULL,                         // APC routine
                                   NULL,                         // APC data
                                   MCW_QUEUE_ITEM_SIZE,          // Output buffer size
                                   &_api_request_params,         // User Buffer
                                   &_request_buffer_length,      // Actual Read buffer length
                                   NULL);                        // Read handle
        if ( _call_status != QUEUE_STATUS_OK )
        {
          // Critical error: Thread end
          // Logger message
          _stprintf (_log_param1, _T("%hu"), _call_status);
          Common_LoggerMsg (NLS_ID_LOGGER(2), MODULE_NAME, _function_name, _log_param1, _T("Queue_Read"));

          return 0;
        }
      break;
      
      default:
        // Error
        // Logger message
        _stprintf (_log_param1, _T("%lu"), GetLastError ());
        Common_LoggerMsg (NLS_ID_LOGGER(2), MODULE_NAME, _function_name, _log_param1, _T("WaitForSingleObjectsEx"));
   
        return 0;

      break;
      
    } // switch
      
  } // while

  return 0;

} // Mcw_APIThread

//------------------------------------------------------------------------------
// PURPOSE : Init API thread data and resources (events and queues).
//
//  PARAMS :
//      - INPUT :
//        - pApiThreadData
//      - OUTPUT : None
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//
//   NOTES :

BOOL Mcw_InitApiThreadData (TYPE_API_THREAD_DATA * pApiThreadData)
{
  static TCHAR _function_name[] = _T("Mcw_Misc_InitApiThreadData");
  TCHAR  _log_param1[MCW_AUX_STRING_LENGTH];

  WORD   _call_status;
  HANDLE _event;

  // Queue event
  _event = CreateEvent (NULL,  // Event attributes   
                        FALSE, // ManualReset
                        FALSE, // InitialState
                        NULL); // Name
  if ( _event == NULL )
  {
    // Logger message
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_LoggerMsg (NLS_ID_LOGGER(2), MODULE_NAME, _function_name, _log_param1, _T("CreateEvent"));

    return FALSE;
  }

  // Set Queue event
  pApiThreadData->queue_event = _event;
  
  // Queue
  _call_status = Queue_CreateQueue (MCW_QUEUE_NUMBER_OF_ITEMS * MCW_QUEUE_ITEM_SIZE,
                                    &pApiThreadData->request_queue);
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_log_param1, _T("%hu"), _call_status);
    Common_LoggerMsg (NLS_ID_LOGGER(2), MODULE_NAME, _function_name, _log_param1, _T("Queue_CreateQueue"));

    return FALSE;
  }
  
  return TRUE;

} // Mcw_InitApiThreadData

//------------------------------------------------------------------------------
// PURPOSE : Checks API execution mode depending on function code
//
//  PARAMS :
//      - INPUT :
//        - pApiInputParams
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: Execution Mode ok
//  - FALSE: Execution Mode error
//
//   NOTES :

BOOL Mcw_API_CheckMode (const API_INPUT_PARAMS * pApiInputParams)
{
  // Check API mode for some operations
  if (   pApiInputParams->mode == API_MODE_NO_WAIT 
      && pApiInputParams->function_code == MCW_CODE_DEVICE_STATUS_QUERY )
  {
    return FALSE;
  }
  
  return TRUE;

} // Mcw_API_CheckMode

//------------------------------------------------------------------------------
// PURPOSE : Select API queue depengin on function code
//
//  PARAMS :
//      - INPUT :
//        - pApiInputParams
//      - OUTPUT :
//
// RETURNS : Queue handle
//
//   NOTES :

QUEUE_HANDLE Mcw_API_SelectQueue (const API_INPUT_PARAMS * pApiInputParams)
{
  QUEUE_HANDLE _request_queue;

  // Select queue depending on request
  switch ( pApiInputParams->function_code )
  {
    case MCW_CODE_CANCEL_WRITE:
      _request_queue = GLB_ModuleData.api_threads.special_cmd_thread_data.request_queue;
    break;

    case MCW_CODE_WRITE:
    case MCW_CODE_MODULE_INIT:
    case MCW_CODE_DEVICE_INIT:
    case MCW_CODE_DEVICE_CLOSE:
    case MCW_CODE_DEVICE_STATUS_QUERY:
    default:
      _request_queue = GLB_ModuleData.api_threads.device_thread_data.request_queue;
    break;
    
  } // switch
  
  return _request_queue;

} // Mcw_API_SelectQueue

//------------------------------------------------------------------------------
// PURPOSE : Execute a "Module Init" request
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

void Mcw_ExecuteModuleInit (void)
{
  WORD  _module_init_status_1;
  WORD  _module_init_status_2;

  // Try to initialize
  Mcw_ModuleInit (NULL, &_module_init_status_1, &_module_init_status_2);

} // Mcw_ExecuteModuleInit

//------------------------------------------------------------------------------
// PURPOSE : Gets model descritpion
//
//  PARAMS :
//      - INPUT :
//        - IdxModel
//      - OUTPUT :
//        - ModelName
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//
//   NOTES :

BOOL GetModelName (WORD IdxModel, TCHAR * pModelName)
{
  static TCHAR _function_name[] = _T("GetModelName");
  TCHAR   * p_model_name;
  TCHAR   _dll_name[MCW_MAX_DLL_NAME];
  HMODULE _dll_handle;
  MCW_MODEL_ENTRY_DEV_DESC _device_description;

  pModelName[0] = 0;

  // Build dll name
  _stprintf (_dll_name, MCW_MODEL_DLLNAME, (int) IdxModel);

  _dll_handle = LoadLibrary (_dll_name);
  if ( _dll_handle == NULL )
  {
    return FALSE;
  }

  _device_description = (MCW_MODEL_ENTRY_DEV_DESC) Common_GetProcAddress (_dll_handle, MCW_MODEL_ENTRY_NAME_DEV_DESC);
  if ( _device_description != NULL )
  {
    p_model_name = _device_description ();
    _tcsncpy (pModelName, p_model_name, MCW_MODEL_NAME_SIZE);
  }

  FreeLibrary (_dll_handle);

  return TRUE;

} // GetModelName

//-----------------------------------------------------------------------------
// PURPOSE : DLL standard main function.
//
//  PARAMS :
//     - INPUT : None
//
//     - OUTPUT : None
//
//
// RETURNS :
//     - TRUE: OK
//     - FALSE: Force dll unload
//
// NOTES :

BOOL APIENTRY DllMain (HANDLE Module, 
                       DWORD  ReasonForCall, 
                       LPVOID Reserved)
{
  BOOL   _bool_rc;

  switch ( ReasonForCall )
  {
    case DLL_PROCESS_ATTACH:
    {
      // DLL load action
      memset (&GLB_API, 0, sizeof (GLB_API));
      memset (&GLB_ModuleData, 0, sizeof (GLB_ModuleData));

      Mcw_InitDLLEntryNames ();

      // Common API settings
      GLB_API.p_api_name        = MODULE_NAME;
      GLB_API.max_iud_size      = MCW_MAX_IUD_SIZE;
      GLB_API.uses_private_log  = FALSE;
      GLB_API.p_logger          = Common_LoggerMsg;
      GLB_API.p_private_log     = NULL;
      GLB_API.p_check_mode      = Mcw_API_CheckMode;
      GLB_API.p_select_queue    = Mcw_API_SelectQueue;
      GLB_API.p_try_init        = Mcw_ExecuteModuleInit;
      
      // API Threads
      _bool_rc = Mcw_InitApiThreadData (&GLB_ModuleData.api_threads.device_thread_data);
      if ( !_bool_rc )
      {
        // Critical error
        // Force to unload DLL
        return FALSE;
      }

      _bool_rc = Common_API_StartThread (Mcw_APIThread,                                       // Thread routine
                                         &GLB_ModuleData.api_threads.device_thread_data,      // Thread parameter
                                         NULL,                                                // Start notify event
                                         _T("Mcw_APIThread.Dev"));
      if ( !_bool_rc )
      {
        // Critical error
        // Force to unload DLL
        return FALSE;
      }

      _bool_rc = Mcw_InitApiThreadData (&GLB_ModuleData.api_threads.special_cmd_thread_data);
      if ( !_bool_rc )
      {
        // Critical error
        // Force to unload DLL
        return FALSE;
      }

      _bool_rc = Common_API_StartThread (Mcw_APIThread,                                       // Thread routine
                                         &GLB_ModuleData.api_threads.special_cmd_thread_data, // Thread parameter
                                         NULL,                                                // Start notify event
                                         _T("Mcw_APIThread.Dev"));
      if ( !_bool_rc )
      {
        // Critical error
        // Force to unload DLL
        return FALSE;
      }
    }
    break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
      // Empty
    break;

  } // switch

  return TRUE;

} // DllMain
