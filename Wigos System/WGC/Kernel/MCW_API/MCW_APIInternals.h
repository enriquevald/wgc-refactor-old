//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MCW_APIInternals.h
//   DESCRIPTION : Constants, types, variables definitions and prototypes
//                 internals for the MCW_API
//        AUTHOR : Xavier Ib��ez
// CREATION DATE : 22-SEP-2008
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 22-SEP-2008 XID    Initial draft.
//
//------------------------------------------------------------------------------

#ifndef __MCW_API_INTERNALS_H
#define __MCW_API_INTERNALS_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------

#define MODULE_NAME                         _T("MCW_API")

// API queue size and number of items
#define MCW_QUEUE_ITEM_SIZE                 sizeof (TYPE_API_REQUEST_PARAMS) // bytes
#define MCW_QUEUE_NUMBER_OF_ITEMS           30 // items

// Tcp send queue
#define MCW_TCP_SEND_QUEUE_ITEM_SIZE        sizeof (TYPE_TCP_SEND_REQUEST)  // bytes
#define MCW_TCP_SEND_QUEUE_NUMBER_OF_ITEMS  4 // items

#define MCW_MAX_IUD_SIZE                    1024
#define MCW_AUX_STRING_LENGTH               512

#define MCW_DLL_MAX_ENTRIES                 25
#define MCW_MAX_DLL_ENTRY_NAME              40
#define MCW_MAX_DLL_NAME                    40

#define MCW_DLL_ENTRY_IDX_INIT              0
#define MCW_DLL_ENTRY_IDX_STATUS            1
#define MCW_DLL_ENTRY_IDX_WRITE             2
#define MCW_DLL_ENTRY_IDX_CLOSE             3
#define MCW_DLL_ENTRY_IDX_CANCEL_WRITE      4

#define MCW_MODEL_DLLNAME                   _T("MCW_MODEL_%03d.DLL")
#define MCW_MODEL_ENTRY_NAME_DEV_DESC       _T("DeviceDescription")

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------

typedef TCHAR * (WINAPI *MCW_MODEL_ENTRY_DEV_DESC) (void);

typedef TCHAR MCW_DLL_ENTRY_NAME [MCW_MAX_DLL_ENTRY_NAME];

typedef WORD (WINAPI *MCW_DLL_ENTRY_ADDRESS) (void *, void *);

typedef struct
{
  HMODULE               dll_handle;
  MCW_DLL_ENTRY_ADDRESS dll_entry_address[MCW_DLL_MAX_ENTRIES];

} TYPE_MCW_DLL_ENTRY_LIST;

typedef struct
{
  BOOL                    init;
  WORD                    model;
  TYPE_MCW_DLL_ENTRY_LIST dll_entry_list;

} TYPE_MCW_MODEL_DATA;

typedef struct
{
  WORD               num_of_entries;
  MCW_DLL_ENTRY_NAME dll_entry_name [MCW_DLL_MAX_ENTRIES];

} TYPE_MCW_DLL_ENTRIES;

// User parameters to execute an MCW request
typedef struct
{
  API_INPUT_PARAMS      api_input_paramas;
  API_OUTPUT_PARAMS     * p_api_output_params;
  API_OUTPUT_USER_DATA  * p_api_output_user_data;
  BYTE                  api_input_user_data_buffer[MCW_MAX_IUD_SIZE];

} TYPE_API_REQUEST_PARAMS;

// Api thread data
typedef struct
{
    HANDLE          queue_event;
    QUEUE_HANDLE    request_queue;        // Thread queue to receive user requests
    DWORD           num_executing_req;    // Number of pending operations. Used to manage number of threads
                                          // processing some type of requests (for example send requests).
    
} TYPE_API_THREAD_DATA;

typedef struct
{
  TYPE_API_THREAD_DATA  device_thread_data;
  TYPE_API_THREAD_DATA  special_cmd_thread_data;
                  
} TYPE_API_THREADS;

typedef struct
{
  TYPE_API_THREADS      api_threads;
  TYPE_MCW_DLL_ENTRIES  model_dll_entry_names;
  TYPE_MCW_MODEL_DATA   model_dll_data;

} TYPE_MODULE_DATA;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

#endif // __MCW_API_INTERNALS_H
