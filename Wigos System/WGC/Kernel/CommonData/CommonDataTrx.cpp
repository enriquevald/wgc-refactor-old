//------------------------------------------------------------------------------
// Copyright © 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonDataTrx.cpp
//
//   DESCRIPTION : Functions and Methods to handle common data Transaction Manager.
//
//        AUTHOR : Ignasi Ripoll
//
// CREATION DATE : 24-NOV-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 24-NOV-2003 IRP    Initial draft.
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_NLS_API
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC

#include "CommonDef.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------
  
//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

// Transaction Audit NLS_ID's
static DWORD GLB_TrxNlsId [] = { NLS_ID_TRX_AUDIT(260),     // "---"
                                 NLS_ID_TRX_AUDIT(201),     // "Parámetros de Agencia"
                                 NLS_ID_TRX_AUDIT(202),     // "Parámetros de Juego"
                                 NLS_ID_TRX_AUDIT(203),     // "Lista de Usuarios"
                                 NLS_ID_TRX_AUDIT(204),     // "Sincronización"
                                 NLS_ID_TRX_AUDIT(205),     // "Estado de Agencia"
                                 NLS_ID_TRX_AUDIT(206),     // "Estado de Dispositivos"
                                 NLS_ID_TRX_AUDIT(207),     // "Petición de Inventario"
                                 NLS_ID_TRX_AUDIT(208),     // "Bajada de Libro"
                                 NLS_ID_TRX_AUDIT(209),     // "Bloqueo de Libro"
                          /*10*/ NLS_ID_TRX_AUDIT(210),     // "Retorno de Libro"
                                 NLS_ID_TRX_AUDIT(211),     // "Información de Pozo"
                                 NLS_ID_TRX_AUDIT(212),     // "Asignación de Pozo"
                                 NLS_ID_TRX_AUDIT(213),     // "Desasignación de Pozo"
                                 NLS_ID_TRX_AUDIT(214),     // "Confirmación de Pozo"
                                 NLS_ID_TRX_AUDIT(215),     // "Petición de Saldos de tarjetas"
                                 NLS_ID_TRX_AUDIT(216),     // "Transferencia de Saldos de tarjetas"
                                 NLS_ID_TRX_AUDIT(217),     // "Mensaje de Usuario"
                                 NLS_ID_TRX_AUDIT(218),     // "Cancelación de Libro"
                                 NLS_ID_TRX_AUDIT(219),     // "Comprobación de Integridad"
                          /*20*/ NLS_ID_TRX_AUDIT(220),     // "Cambio de Clave RSA en LKC"
                                 NLS_ID_TRX_AUDIT(221),     // "Petición de cambio de Clave RSA en LKAS"
                                 NLS_ID_TRX_AUDIT(222),     // "Confirmación de cambio de Clave RSA en LKAS"
                                 NLS_ID_TRX_AUDIT(223),     // "Petición de Datos Contables"
                                 NLS_ID_TRX_AUDIT(224),     // "Comprobación de Premio"
                                 NLS_ID_TRX_AUDIT(225),     // "Cobro de Premio"
                                 NLS_ID_TRX_AUDIT(226),     // "Informe de Ventas por Horas"
                                 NLS_ID_TRX_AUDIT(227),     // "Informe de Ventas por Terminal"
                                 NLS_ID_TRX_AUDIT(228),     // "Petición de Saldos no redimidos"
                                 NLS_ID_TRX_AUDIT(229),     // "Transferencia de Saldos no redimidos"
                          /*30*/ NLS_ID_TRX_AUDIT(230),     // "Inicio de sesión Batch"
                                 NLS_ID_TRX_AUDIT(231),     // "Software Download"
                                 NLS_ID_TRX_AUDIT(232),     // "Online Report Request"
                                 NLS_ID_TRX_AUDIT(233),     // "Inicio de sesión Batch"
                                 NLS_ID_TRX_AUDIT(234),     // "Retorno de Libros"
                                 NLS_ID_TRX_AUDIT(235),     // "Informe de Eventos"
                                 NLS_ID_TRX_AUDIT(236),     // "Informe de Vouchers"
                                 NLS_ID_TRX_AUDIT(237),     // "Cambio de Estado de Libro"
                                 NLS_ID_TRX_AUDIT(238),     // "Bloqueo de Libros"
                                 NLS_ID_TRX_AUDIT(239),     // "Change User Password"
                          /*40*/ NLS_ID_TRX_AUDIT(240),     // "Login"
                                 NLS_ID_TRX_AUDIT(241),     // "Logout"
                                 NLS_ID_TRX_AUDIT(242),     // "Shutdown"
                                 NLS_ID_TRX_AUDIT(243),     // "Send Allowed Terminals"
                                 NLS_ID_TRX_AUDIT(244),     // "Configured Terminals"
                                 NLS_ID_TRX_AUDIT(245),     // "Get Pages"
                                 NLS_ID_TRX_AUDIT(246),     // "Set Pages"
                                 NLS_ID_TRX_AUDIT(247),     // "Wings Sales"
                                 NLS_ID_TRX_AUDIT(248),     // "Wings Random Numbers"


                               };

static DWORD GLB_TrxBatch [] = {  0, 1,  2,  3,  4,  5,  6,  7,  8,  9,  10,
                                 11, 15, 16, 17, 18, 19, 20, 21, 22, 26, 27, 28, 29, 34, 35, 36, 37, 38, 43, 44, 47
                               };

static DWORD GLB_TrxOnline [] = { 0, 6, 18, 19, 24, 25, 26, 27, 30, 32, 33, 35, 37, 39, 40, 41, 47, 48 };

static DWORD GLB_TrxSoftDownload [] = { 0, 31 };


// GUI_Auditor NLS_ID's
// static DWORD GLB_AuditorNlsId [] = { 

    // "Jerarquías"
    // "Alarmas"
    // "Series"
    // "Login / Logout"
    // "Juegos"
    // "Usuarios"
    // "Perfiles"
    // "Planes de Premios"
    // "Stocks"
    // "Nodos"
    // "Computadoras"
    // "Conexiones"
    // "Parámetros de Música"
    // "Libros"
    // "Parámetros de Pozos"
    // "Pantalla Cliente"
    // "Opciones Regionales"
    // "Parametros Generales"
    
//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PURPOSE : Gets transaction parameters number of the Transaction Manager 
//
//  PARAMS :
//      - INPUT : 
//          - TrxMgr:Transaction Manager type
//
//      - OUTPUT : None
//
// RETURNS : IPC nodes Transaction Manager number
//
//   NOTES :
//
COMMON_DATA_API DWORD WINAPI Common_GetTrxMgrNumTrxs (DWORD TrxMgr)
{
  DWORD _nums_trx;

  switch ( TrxMgr )
  {
    case TRX_BATCH:
      _nums_trx = sizeof (GLB_TrxBatch) / sizeof (DWORD);
    break;

    case TRX_ONLINE:
      _nums_trx = sizeof (GLB_TrxOnline) / sizeof (DWORD);
    break;

    case TRX_SOFTWARE:
      _nums_trx = sizeof (GLB_TrxSoftDownload) / sizeof (DWORD);
    break;

  // .....
    default:
      _nums_trx = 0;
  }

  return _nums_trx;
} // GetNumTrx

//------------------------------------------------------------------------------
// PURPOSE : Gets the NlsId of the transaction parameter
//
//  PARAMS :
//      - INPUT : 
//          - TrxType: Transaction Manager type
//
//      - OUTPUT : 
//          - pTrxTypeNlsId: the NlsId of the transaction parameter
//
// RETURNS : 
//     - TRUE, on success
//    - FALSE, otherwise
//   NOTES :
//
COMMON_DATA_API BOOL WINAPI Common_GetTrxMgrTrxNlsId (DWORD TrxType, DWORD * pTrxTypeNlsId)
{
  if ( TrxType >= sizeof (GLB_TrxNlsId) / sizeof (DWORD) )
  {
    return FALSE;
  }
  
  * pTrxTypeNlsId = GLB_TrxNlsId[TrxType];
  
  return TRUE;      

} // Common_GetTrxMgrTrxNlsId

//------------------------------------------------------------------------------
// PURPOSE : Gets the position inside GLB_TrxNlsId where is the transaction parameter indicated by Index
//
//  PARAMS :
//      - INPUT : 
//          - TrxMgr: Transaction Manager
//          - Index: Parameter
//
//      - OUTPUT : 
//          - pTrxType: pointer to the GLB_TrxNlsId
//
// RETURNS : 
//    - TRUE, on success
//    - FALSE, otherwise
//   NOTES :
//
COMMON_DATA_API BOOL WINAPI Common_GetTrxMgrTrxType (DWORD TrxMgr, DWORD Index, DWORD * pTrxType)
{
  if ( Index >= Common_GetTrxMgrNumTrxs (TrxMgr) )
  {
    return FALSE;
  }

  switch ( TrxMgr )
  {
    case TRX_BATCH:
     * pTrxType = GLB_TrxBatch[Index];    
    break;

    case TRX_ONLINE:
     * pTrxType = GLB_TrxOnline[Index];       
    break;

    case TRX_SOFTWARE:
       * pTrxType = GLB_TrxSoftDownload[Index];
    break;

    default :
      return FALSE;
    break;
  }
  return TRUE;

} // Common_GetTrxMgrTrxType
