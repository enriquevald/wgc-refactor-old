//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonDataAuditor.cpp
//
//   DESCRIPTION : Functions and Methods to handle common data Auditor.
//
//        AUTHOR : Ignasi Ripoll
//
// CREATION DATE : 15-DEC-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 15-DEC-2003 IRP    Initial draft.
// 22-MAY-2012 ACC    Add AUDIT_NAME_ACCEPTORS code.
// 13-NOV-2013 LJM    Add AUDIT_CODE_USER_ACTIVITY code.
// 13-NOV-2013 JCA    Add AUDIT_CODE_CAGE code.
// 17-DEC-2013 JBP    Add AUDIT_CODE_GAMING_TABLES code.
// 19-FEB-2015 FJC    Add GUI_NAME: Layout.
// 17-NOV-2015 JBC    Add GUI_NAME: SmartFloor App.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_NLS_API
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC

#include "CommonDef.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------

#define GUI_WIGOS              14
#define CASHIER                15
#define WINUP                  16
#define MOBIBANK               17
#define ISTATS                 104
#define MULTISITE              200
#define CASHDESK_DRAWS         201
#define SMARTFLOOR             203
#define SMARTFLOOR_APP         204

// Audit names are defined on WigosGUI on the following mdl_globals.
// Changes on the current codes will make incompatible audit codes with older versions display.
// GUI_CommonMisc:
#define AUDIT_NAME_GENERIC                0
// GUI_Controls:
#define AUDIT_NAME_LOGIN_LOGOUT           4
#define AUDIT_NAME_TIME_ZONE              24
// WigosGUI:
#define AUDIT_NAME_USERS                      7
#define AUDIT_NAME_PROFILES                   8
#define AUDIT_NAME_TERMINALS                  9
#define AUDIT_NAME_GENERAL_PARAMS             20
#define AUDIT_NAME_SOFTWARE_DOWNLOAD          23
#define AUDIT_NAME_JACKPOT_PARAMETERS         25
#define AUDIT_NAME_CASHIER_CONFIGURATION      26
#define AUDIT_NAME_ACCOUNT                    27
#define AUDIT_NAME_DRAWS                      28
#define AUDIT_CODE_TERMINAL_GAME_RELATION     29
#define AUDIT_CODE_PLAYER_PROMOTIONS          30
#define AUDIT_CODE_GIFTS                      31
#define AUDIT_NAME_SITE_JACKPOT_PARAMETERS    32
#define AUDIT_NAME_MAILING_PROGRAMMING        33
#define AUDIT_NAME_CARD_WRITER                34
#define AUDIT_NAME_TERMINAL_RETIREMENT        36
#define AUDIT_NAME_ALARMS                     37
#define AUDIT_NAME_ACCEPTORS                  38
#define AUDIT_NAME_CASHIER_SESSIONS           39
#define AUDIT_NAME_MULTISITE                  40
#define AUDIT_NAME_REPRINT_VOUCHERS           41
#define AUDIT_CODE_PROVIDERS                  42
#define AUDIT_CODE_USER_ACTIVITY              44
#define AUDIT_CODE_CAGE                       45
#define AUDIT_CODE_GAMING_TABLES              46
#define AUDIT_CODE_BONUSES                    47
#define AUDIT_CODE_PLAY_SESSIONS              48
#define AUDIT_CODE_PATTERNS                   49
#define AUDIT_CODE_PROGRESSIVE_PROVISION      50
#define AUDIT_CODE_PROGRESSIVE                51
#define AUDIT_CODE_METERS                     52
#define AUDIT_CODE_PRINT_QR_CODES             53
#define AUDIT_CODE_CREDIT_LINES               57
#define AUDIT_CODE_JUNKETS                    58
#define AUDIT_TERMINALS_BOOKING               59
#define AUDIT_CUSTOMER_NOTICES                60
//..  AUDIT_NAME_PROMOGAME As Integer = 61 �?
//..    AUDIT_USERS_ASSIGNATION As Integer = 62 �?
#define AUDIT_EGM_METERS                      63

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

static TYPE_GUI_NAME GLB_GUIName[] =
{
  { sizeof(TYPE_GUI_NAME), GUI_WIGOS, NLS_ID_GUI_AUDIT(100) }
  , { sizeof(TYPE_GUI_NAME), CASHIER, NLS_ID_GUI_STATISTICS(367) }
  , { sizeof(TYPE_GUI_NAME), ISTATS, NLS_ID_GUI_CONF(413) }
  , { sizeof(TYPE_GUI_NAME), MULTISITE, NLS_ID_GUI_AUDIT(87) }
  , { sizeof(TYPE_GUI_NAME), CASHDESK_DRAWS, NLS_ID_GUI_AUDIT(475) }
  , { sizeof(TYPE_GUI_NAME), SMARTFLOOR, NLS_ID_GUI_PLAYER_TRACKING(5897) }
  , { sizeof(TYPE_GUI_NAME), SMARTFLOOR_APP, NLS_ID_GUI_PLAYER_TRACKING(6932) }
  , { sizeof(TYPE_GUI_NAME), WINUP, NLS_ID_GUI_AUDIT(488) }
  , { sizeof(TYPE_GUI_NAME), MOBIBANK, NLS_ID_GUI_AUDIT(495) }
};

// Audit codes defined here should be those who wanted to have a grouped operations
static TYPE_AUDIT_NAME GLB_AuditName[] =
{
  { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_LOGIN_LOGOUT, NLS_ID_GUI_AUDIT(81) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_USERS, NLS_ID_GUI_AUDIT(56) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_PROFILES, NLS_ID_GUI_AUDIT(57) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_TERMINALS, NLS_ID_GUI_AUDIT(59) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_GENERAL_PARAMS, NLS_ID_GUI_AUDIT(67) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_SOFTWARE_DOWNLOAD, NLS_ID_GUI_AUDIT(71) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_JACKPOT_PARAMETERS, NLS_ID_GUI_AUDIT(72) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_CASHIER_CONFIGURATION, NLS_ID_GUI_AUDIT(78) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_ACCOUNT, NLS_ID_GUI_AUDIT(79) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_DRAWS, NLS_ID_GUI_AUDIT(80) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_TERMINAL_GAME_RELATION, NLS_ID_GUI_AUDIT(73) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_PLAYER_PROMOTIONS, NLS_ID_GUI_AUDIT(74) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_GIFTS, NLS_ID_GUI_AUDIT(75) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_SITE_JACKPOT_PARAMETERS, NLS_ID_GUI_AUDIT(76) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_MAILING_PROGRAMMING, NLS_ID_GUI_AUDIT(77) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_CARD_WRITER, NLS_ID_GUI_AUDIT(82) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_TERMINAL_RETIREMENT, NLS_ID_GUI_AUDIT(83) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_ALARMS, NLS_ID_GUI_AUDIT(84) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_ACCEPTORS, NLS_ID_GUI_AUDIT(85) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_CASHIER_SESSIONS, NLS_ID_GUI_AUDIT(86) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_MULTISITE, NLS_ID_GUI_AUDIT(87) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_NAME_REPRINT_VOUCHERS, NLS_ID_GUI_AUDIT(88) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_PROVIDERS, NLS_ID_GUI_AUDIT(89) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_USER_ACTIVITY, NLS_ID_GUI_AUDIT(92) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_CAGE, NLS_ID_GUI_AUDIT(93) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_GAMING_TABLES, NLS_ID_GUI_AUDIT(94) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_BONUSES, NLS_ID_GUI_AUDIT(95) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_PLAY_SESSIONS, NLS_ID_GUI_AUDIT(96) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_PATTERNS, NLS_ID_GUI_AUDIT(97) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_PROGRESSIVE_PROVISION, NLS_ID_GUI_AUDIT(98) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_PROGRESSIVE, NLS_ID_GUI_AUDIT(99) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_METERS, NLS_ID_GUI_AUDIT(483) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_PRINT_QR_CODES, NLS_ID_GUI_AUDIT(486) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_CREDIT_LINES, NLS_ID_GUI_AUDIT(491) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CODE_JUNKETS, NLS_ID_GUI_AUDIT(492) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_TERMINALS_BOOKING, NLS_ID_GUI_AUDIT(493) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_CUSTOMER_NOTICES, NLS_ID_GUI_AUDIT(494) }
  , { sizeof(TYPE_AUDIT_NAME), AUDIT_EGM_METERS, NLS_ID_GUI_AUDIT(496) }
};

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Return the number of GUI�s registered.
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS : Number of GUI�s registered.
//
//   NOTES :
//
COMMON_DATA_API DWORD WINAPI Common_GUINameNumNodes(void)
{
  return sizeof(GLB_GUIName) / sizeof(TYPE_GUI_NAME);

} // Common_GUINameNumNodes

//------------------------------------------------------------------------------
// PURPOSE : Gets GUI Name node
//
//   PARAMS :
//
//     - INPUT :
//       - NodeIndex: The index of the requested node
//
//     - OUTPUT :
//       - pGUINameNode: The node GUI Name
//
// RETURNS :
//  - TRUE,  on success
//  - FALSE, otherwise
//
//   NOTES :

COMMON_DATA_API BOOL WINAPI Common_GUINameGetNode(DWORD         NodeIndex,
  TYPE_GUI_NAME * pGUINameNode)
{
  if (pGUINameNode->control_block != sizeof(TYPE_GUI_NAME))
  {
    return FALSE;
  }

  if (NodeIndex >= sizeof(GLB_GUIName) / sizeof(TYPE_GUI_NAME))
  {
    return FALSE;
  }

  memcpy(pGUINameNode, &GLB_GUIName[NodeIndex], sizeof(TYPE_GUI_NAME));

  return TRUE;

} // Common_GUINameGetNode


//------------------------------------------------------------------------------
// PURPOSE : Gets Audit Names nodes number
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT : None
//
// RETURNS : Audit Names nodes number
//
//   NOTES :
//
COMMON_DATA_API DWORD WINAPI Common_AuditNameNumNodes(void)
{
  return sizeof(GLB_AuditName) / sizeof(TYPE_AUDIT_NAME);

} // Common_AuditNameNumNodes

//------------------------------------------------------------------------------
// PURPOSE : Gets Audit Name node
//
//   PARAMS :
//
//     - INPUT :
//       - NodeIndex: The index of the requested node
//
//     - OUTPUT :
//       - pGUIAuditNode: The node Audit Name
//
// RETURNS :
//  - TRUE,  on success
//  - FALSE, otherwise
//
//   NOTES :
COMMON_DATA_API BOOL WINAPI Common_AuditNameGetNode(DWORD           NodeIndex,
  TYPE_AUDIT_NAME * pGUIAuditNode)
{
  if (pGUIAuditNode->control_block != sizeof(TYPE_AUDIT_NAME))
  {
    return FALSE;
  }

  if (NodeIndex >= sizeof(GLB_AuditName) / sizeof(TYPE_AUDIT_NAME))
  {
    return FALSE;
  }

  memcpy(pGUIAuditNode, &GLB_AuditName[NodeIndex], sizeof(TYPE_AUDIT_NAME));

  return TRUE;

} // Common_AuditNameGetNode

