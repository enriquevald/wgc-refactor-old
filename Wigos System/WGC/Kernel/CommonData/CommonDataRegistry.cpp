//------------------------------------------------------------------------------
// Copyright © 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonDataRegistry.cpp
//
//   DESCRIPTION : Functions and Methods to handle Common Data Registry.
//
//        AUTHOR : Luis Rodríguez
//
// CREATION DATE : 17-FEB-2004
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 17-FEB-2004 LRS    Initial draft.
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API

#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

typedef TYPE_COMMON_VECTOR TYPE_REGISTRY_LIST; 

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

static TYPE_REGISTRY GLB_Registry_Launch_Bar [] = {  
	//Key Name, Value Name, Value Type, , Value Data, Auto Create
  { _T("GUI_LAUNCHBAR"), _T("AutoHide"), REG_DWORD, _T("0"), TRUE},
	{ _T("GUI_LAUNCHBAR"), _T("Position"), REG_DWORD, _T("1"), TRUE} 
};
//
//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Gets Registry definition
//
//   PARAMS :
//
//     - INPUT :
//       - RegType: The index of the requested Registry
//
//     - OUTPUT :
//       - pRegistry    : The Registry definition
//
// RETURNS :
//  - TRUE, on success
//  - FALSE, otherwise
//
////   NOTES :
//
//COMMON_DATA_API BOOL WINAPI Common_RegistryDefsIni (TYPE_REGISTRY_LIST	* pRegistry)
//{
//	TCHAR   _function_name [] = _T("Common_RegistryDefsIni");
//  DWORD   _rc;
//	WORD		_idx;
//		
//	TYPE_REGISTRY_DATA_ITEM * _p_reg_data_item;
//	TYPE_REGISTRY_DATA_LIST * _p_reg_data_list;
//	TYPE_REGISTRY *_p_items_defs;
//	TYPE_REGISTRY_DATA_ITEM  *_p_items_data;
//	
//	_p_items_defs = (TYPE_REGISTRY *) pRegistry->p_items;  
//	_p_items_data = (TYPE_REGISTRY_DATA_ITEM *) pRegistry->p_items;  
//	
//	for ( _idx = 0; _idx < pRegistry->num_items ; _idx++)
//	{
//		memset(_p_reg_data_item, 0, sizeof(_p_reg_data_item);
//		
//		if ( _p_items_defs[_idx].auto_create = TRUE )
//		{
//			strcpy(_p_reg_data_item->key_name, _p_items_defs[_idx].key_name);
//			strcpy(_p_reg_data_item->value_name , _p_items_defs[_idx].value_name );
//			_p_reg_data_item->value_type, _p_items_defs[_idx].value_type;
//			
//			_rc = Common_GetRegistryValue(_p_reg_data_item);
//			if ( _rc == 2) // temp
//			{
//				strcpy(_p_reg_data_item->value_data, _p_items_defs[_idx].value_data);
//				_p_reg_data_item->value_length = _tcslen(_p_items_defs[_idx].value_data);
//				memcpy (pRegistry->p_items, GLB_Registry_Launch_Bar, sizeof (GLB_Registry_Launch_Bar));
//			}
//		  
//
//	return TRUE;
//}

COMMON_DATA_API BOOL WINAPI Common_RegistryDefsGet (DWORD								RegType, 
																									 TYPE_REGISTRY_LIST	* pRegistry)
{
	switch ( RegType )
	{
	case REG_TYPE_LAUNCH_BAR:
		 memcpy (pRegistry->p_items, GLB_Registry_Launch_Bar, sizeof (GLB_Registry_Launch_Bar));
     pRegistry->num_items = (sizeof (GLB_Registry_Launch_Bar) / sizeof (TYPE_REGISTRY));
	break;
	
	// .....
  default:
	;
		
	}	

	return TRUE;
} // Common_RegistryGet
