//------------------------------------------------------------------------------
// Copyright © 2004 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonDateCode.cpp
//
//   DESCRIPTION : Functions and Methods related to DateCode utility.
//
//        AUTHOR : Ronald Rodríguez T.
//
// CREATION DATE : 26-JAN-2004
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 26-JAN-2004 RRT    Initial draft.
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// STANDARD INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_NLS_API
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC

#include "CommonDef.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------
#define DATE_CODE_INTERNAL_KEY    _T("1234567891234567")
#define MAX_DATE_CODE_LENGTH      5
#define SBOX_LEN                  16

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : DateCode algorithm
//
//  PARAMS :
//      - INPUT :
//            - ClientId: Client identifier
//            - AgencyId: Agency identifier
//            - ActualDay: Current day
//            - ActualMonth: Current month
//            - ActualYear: Current year
//
//      - OUTPUT :
//
// RETURNS :
//      - Common_DateCode: Resultant calculated date code value
//
//   NOTES :
//

COMMON_DATA_API DWORD WINAPI Common_DateCode (DWORD ClientId,
                                              DWORD AgencyId,
                                              DWORD ActualDay,
                                              DWORD ActualMonth,
                                              DWORD ActualYear)
{
  TCHAR   _sbox1[SBOX_LEN];       // Data Sbox1
  TCHAR   _sbox2[SBOX_LEN];       // Data Sbox2
  TCHAR   _temp;                  // Sboxes scrambling auxiliar variable
  TCHAR   _output_data[SBOX_LEN]; // Output resultant data
  DWORD   _result;                // Resultant datecode
  DWORD   _output_idx;            // Output date creation auxiliar variable
  DWORD   _scramb_idx1;           // Scrambling auxiliar variable
  DWORD   _scramb_idx2;           // Scrambling auxiliar variable
  TCHAR   * p_internal_key;       // Local Pointer to the Internal key
  DWORD   _internal_key_len;      // Internal key length

  p_internal_key = DATE_CODE_INTERNAL_KEY;
  _internal_key_len = _tcslen (p_internal_key);

  _scramb_idx1 = 0;
  _scramb_idx2 = 0;
  _output_idx = 0;
  _temp = 0;
  _result = 0;

  // Always initialize the arrays with zero
  memset (_sbox1, 0, sizeof (_sbox1));
  memset (_sbox2, 0, sizeof (_sbox2));
  memset (_output_data, 0, sizeof (_output_data));

  // Initialize first SBox
  _scramb_idx2 = _stprintf (_output_data, "%u", ClientId);
  _scramb_idx2 += _stprintf (_output_data + _scramb_idx2, "%u", AgencyId);
  _scramb_idx2 += _stprintf (_output_data + _scramb_idx2, "%u", ActualDay);
  _scramb_idx2 += _stprintf (_output_data + _scramb_idx2, "%u", ActualMonth);
  _scramb_idx2 += _stprintf (_output_data + _scramb_idx2, "%u", ActualYear);

  // Initialize first Sbox
  for ( _scramb_idx1 = 0; _scramb_idx1 < SBOX_LEN; _scramb_idx1++ )
  {
    if (_scramb_idx2 == _tcslen (_output_data) || ( _output_data[_scramb_idx2] == 0 ))
    {
      _scramb_idx2 = 0;
    }
    _sbox1[_scramb_idx1] = (TCHAR)_output_data[_scramb_idx2++];
  } // for

  _scramb_idx2 = 0;

  // Initialize second Sbox2 with the internal key
  for ( _scramb_idx1 = 0; _scramb_idx1 < SBOX_LEN; _scramb_idx1++ )
  {
    if ( _scramb_idx2 == _internal_key_len )
    {
      _scramb_idx2 = 0;
    }
    _sbox2[_scramb_idx1] = p_internal_key[_scramb_idx2++];
  } // for

  _scramb_idx1 = 0;
  _scramb_idx2 = 0;

  // Scramble the two boxes: Sbox1 with Sbox2
  for ( _scramb_idx1 = 0; _scramb_idx1 < SBOX_LEN; _scramb_idx1++ )
  {
    // Get the scrambling index depending on the value
    _scramb_idx2 = (_scramb_idx2 + (DWORD) _sbox1[_scramb_idx1] + (DWORD) _sbox2[_scramb_idx1]) % SBOX_LEN;
    if (_sbox1[_scramb_idx1] > 0 && _sbox2[_scramb_idx2] > 0)
    {
      _temp =  _sbox1[_scramb_idx1];
      _sbox1[_scramb_idx1] = _sbox2[_scramb_idx2];
      _sbox2[_scramb_idx2] =  _temp;
    }
  } // for

  // Create output data
  _scramb_idx1 = 0;
  _scramb_idx2 = 0;
  _output_idx = 0;

  memset (_output_data, 0, sizeof(_output_data));

  for ( _output_idx = 0; _output_idx < SBOX_LEN; _output_idx++ )
  {
    _scramb_idx1 = (_scramb_idx1 + 1) % SBOX_LEN;
    _scramb_idx2 = (_scramb_idx2 + 1) % SBOX_LEN;

    //Scramble Sbox 1 so encryption routine will repeat itself at great interval
    _temp = _sbox1[_scramb_idx1];
    _sbox1[_scramb_idx1] = _sbox1[_scramb_idx2] ;
    _sbox1[_scramb_idx2] = _temp;

    // XOR with the data
    // Note: To achieve higher TCHAR values in order to obtain correct integer values
    //       after assign, the ascii: '0' is being added.
    _output_data[_output_idx] =  __toascii ('0') + (_sbox1[_output_idx] ^ _sbox2[_output_idx]) % 10;
  } // for

  // Limit date code result to maximum possible length
  _output_data[MAX_DATE_CODE_LENGTH] = 0;

  // return resultant code
  _result = _tstol (_output_data);

  return _result;

} // Common_DateCode
