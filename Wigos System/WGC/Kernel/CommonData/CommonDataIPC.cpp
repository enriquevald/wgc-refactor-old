//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonData.cpp
//
//   DESCRIPTION : Functions and Methods to handle common data.
//
//        AUTHOR : Sergio Calleja
//
// CREATION DATE : 04-FEB-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 04-FEB-2003 SCM    Initial draft.
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_NLS_API
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC

#include "CommonDef.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------

typedef struct
{
  DWORD line_type_id;
  DWORD nls_id;
  DWORD is_unique;

} TYPE_COMM_LINE_DEFINITION;
  
//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

// This ranges are MANDATORY!!!!
//   0       Base Ports
//   1 -  10 Services
//  11 -  19 GUI's
//  20       CommMgr
//  21 -  29 Reserved
//  30 -  99 TrxMgr's
//  DON'T CHANGE IPC INTERNAL_NODE_ID!!!!

static TYPE_IPC_NODE_DEFINITION GLB_NodeDef [] = 
{
  { sizeof (TYPE_IPC_NODE_DEFINITION),  0, NODE_SERVICE, TRX_NONE,     IPC_NODE_TYPE_BASE_PORT, IPC_NODE_NAME_BASE_PORT,        _T("** Base Ports **")   },
  { sizeof (TYPE_IPC_NODE_DEFINITION),  1, NODE_SERVICE, TRX_NONE,     IPC_NODE_TYPE_CLONABLE,  IPC_NODE_NAME_APP_LOG,          _T("Application Log")    },
  { sizeof (TYPE_IPC_NODE_DEFINITION),  2, NODE_SERVICE, TRX_NONE,     IPC_NODE_TYPE_CLONABLE,  IPC_NODE_NAME_ALARM_MGR,        _T("Alarm Manager")      },
  { sizeof (TYPE_IPC_NODE_DEFINITION),  3, NODE_SERVICE, TRX_NONE,     IPC_NODE_TYPE_CLONABLE,  IPC_NODE_NAME_RESOURCE_MONITOR, _T("Resource Monitor")   },
  { sizeof (TYPE_IPC_NODE_DEFINITION), 11, NODE_GUI,     TRX_NONE,     IPC_NODE_TYPE_CLONABLE,  IPC_NODE_NAME_GUI_COMM_MGR,     _T("GUI Communications") },
  { sizeof (TYPE_IPC_NODE_DEFINITION), 12, NODE_GUI,     TRX_NONE,     IPC_NODE_TYPE_CLONABLE,  IPC_NODE_NAME_GUI_ALARMS,       _T("GUI Alarms")         },
  { sizeof (TYPE_IPC_NODE_DEFINITION), 13, NODE_GUI,     TRX_NONE,     IPC_NODE_TYPE_CLONABLE,  IPC_NODE_NAME_GUI_SYSMON,       _T("GUI System Monitor") },
  { sizeof (TYPE_IPC_NODE_DEFINITION), 20, NODE_SERVICE, TRX_NONE,     IPC_NODE_TYPE_UNIQUE,    IPC_NODE_NAME_COMM_MGR,         _T("Communication Mgr.") },
  { sizeof (TYPE_IPC_NODE_DEFINITION), 30, NODE_SERVICE, TRX_BATCH,    IPC_NODE_TYPE_UNIQUE,    IPC_NODE_NAME_BATCH,            _T("Batch Trx. Mgr.")    },
  { sizeof (TYPE_IPC_NODE_DEFINITION), 31, NODE_SERVICE, TRX_ONLINE,   IPC_NODE_TYPE_UNIQUE,    IPC_NODE_NAME_ONLINE,           _T("Online Trx. Mgr.")   },
  { sizeof (TYPE_IPC_NODE_DEFINITION), 32, NODE_SERVICE, TRX_JACKPOT,  IPC_NODE_TYPE_UNIQUE,    IPC_NODE_NAME_JACKPOT,          _T("Jackpot Trx. Mgr.")  },
  { sizeof (TYPE_IPC_NODE_DEFINITION), 33, NODE_SERVICE, TRX_SOFTWARE, IPC_NODE_TYPE_UNIQUE,    IPC_NODE_NAME_SOFTWARE,         _T("Software Trx. Mgr.") },
};

static TYPE_COMM_LINE_DEFINITION GLB_CommLineDef [] = { { 1, NLS_ID_GUI_COMM(335), 1 /* UDP */ }, 
                                                        { 2, NLS_ID_GUI_COMM(336), 1 /* TCP */ }
                                                      };

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Gets IPC nodes number
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT : None
//
// RETURNS : IPC nodes number
//
//   NOTES :
//
COMMON_DATA_API DWORD WINAPI Common_IpcDefNumNodes (void)
{
  return sizeof (GLB_NodeDef) / sizeof (TYPE_IPC_NODE_DEFINITION);

} //Common_IpcDefNumNodes

//------------------------------------------------------------------------------
// PURPOSE : Gets IPC nodes definition
//
//   PARAMS :
//
//     - INPUT :
//       - NodeIndex: The index of the requested node
//
//     - OUTPUT :
//       - pIpcNode: The node definition
//
// RETURNS :
//  - TRUE, on success
//  - FALSE, otherwise
//
//   NOTES :

COMMON_DATA_API BOOL WINAPI Common_IpcDefGetNode (DWORD                     NodeIndex, 
                                                  TYPE_IPC_NODE_DEFINITION  * pIpcNode)
{
  if ( pIpcNode->control_block != sizeof (TYPE_IPC_NODE_DEFINITION) )
  {
    return FALSE;
  }

  if ( NodeIndex >= sizeof (GLB_NodeDef) / sizeof (TYPE_IPC_NODE_DEFINITION) )
  {
    return FALSE;
  }

  memcpy (pIpcNode, &GLB_NodeDef[NodeIndex], sizeof (TYPE_IPC_NODE_DEFINITION));

  return TRUE;

} // Common_IpcDefGetNode

//------------------------------------------------------------------------------
// PURPOSE : Gets Communication line types number
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT : None
//
// RETURNS : Communication line types number
//
//   NOTES :

COMMON_DATA_API DWORD WINAPI Common_GetNumCommLineTypes (void)
{
  return sizeof (GLB_CommLineDef) / sizeof (TYPE_COMM_LINE_DEFINITION);
} // Common_GetNumCommLineTypes

//------------------------------------------------------------------------------
// PURPOSE : Gets communication line type and its nls_id
//
//   PARAMS :
//     - INPUT :
//       - LineIndex: The index of the requested communication line type
//
//     - OUTPUT :
//       - pCommLineTypeList: The communication line type
//
// RETURNS :
//   - TRUE, on success
//   - FALSE, otherwise
//
// NOTES :
//
COMMON_DATA_API BOOL  WINAPI Common_GetCommLineType (DWORD                LineIndex,
                                                     TYPE_COMM_LINE_TYPE  * pCommLineType)
{
  // Check control block
  if ( pCommLineType->control_block != sizeof (TYPE_COMM_LINE_TYPE) )
  {
    return FALSE;
  }

  if ( LineIndex >= sizeof (GLB_CommLineDef) / sizeof (TYPE_COMM_LINE_DEFINITION) )
  {
    return FALSE;
  }

  pCommLineType->line_type = GLB_CommLineDef[LineIndex].line_type_id;
  pCommLineType->nls_id    = GLB_CommLineDef[LineIndex].nls_id ;
  pCommLineType->is_unique = GLB_CommLineDef[LineIndex].is_unique;

  return TRUE;
} // Common_GetCommLineType
