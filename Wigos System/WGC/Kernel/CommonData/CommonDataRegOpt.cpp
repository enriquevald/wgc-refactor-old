//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : CommonDataTrx.cpp
//
//   DESCRIPTION : Functions and Methods to handle common data Transaction Manager.
//
//        AUTHOR : Ignasi Ripoll
//
// CREATION DATE : 24-NOV-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 24-NOV-2003 IRP    Initial draft.
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_NLS_API
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC

#include "CommonDef.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------

typedef TCHAR TYPE_REG_OPT_ITEM [TYPE_REG_OPT_ITEM_MAX_LEN + 1 + 1 + 1]; // 21+1+1+1 = 24

typedef TYPE_COMMON_VECTOR TYPE_REG_OPT_LIST; 

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------


// Regional Options 
static TYPE_REG_OPT_ITEM GLB_NumbersDecimalSymbol []  = {_T("."), _T(",")};
static TYPE_REG_OPT_ITEM GLB_NumbersGroupSymbol []    = {_T("."), _T(",")};
static TYPE_REG_OPT_ITEM GLB_NumbersDecimalDigits []  = {_T("0"), _T("1"), _T("2")};

static TYPE_REG_OPT_ITEM GLB_CurrencySymbolPos []     = {_T("�1.23"), 
                                                         _T("1.23�"),
                                                         _T("� 1.23"), 
                                                         _T("1.23 �")
                                                        };
                                                           
static TYPE_REG_OPT_ITEM GLB_CurrencyDecimalSymbol [] = {_T("."), _T(",")};
static TYPE_REG_OPT_ITEM GLB_CurrencyGroupSymbol []   = {_T("."), _T(",")};
static TYPE_REG_OPT_ITEM GLB_CurrencyDecimalDigits [] = {_T("0"), _T("1"), _T("2")};

static TYPE_REG_OPT_ITEM GLB_DateFormat []             = {_T("dd/MM/yyyy"), _T("MM/dd/yyyy"), _T("yyyy/MM/dd")}; 
static TYPE_REG_OPT_ITEM GLB_DateGroupSymbol []        = {_T("/"), _T("-")};
static TYPE_REG_OPT_ITEM GLB_TimeFormat []             = {_T("HH:mm"), _T("HH:mm:ss")};
static TYPE_REG_OPT_ITEM GLB_TimeGroupSymbol []        = {_T(":"), _T(".")};

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Gets the Regional Options for the specified parameter
//
//  PARAMS :
//      - INPUT : OptionId -> Id of the option
//
//      - OUTPUT : None
//
// RETURNS : IPC nodes number
//
//   NOTES :
//

COMMON_DATA_API BOOL WINAPI Common_GetRegOpt (DWORD OptionId, TYPE_REG_OPT_LIST * pRegOpt)
{
    
  switch ( OptionId )
  {
    case NUMBERS_DECIMAL_SYMBOL:
      memcpy (pRegOpt->p_items, GLB_NumbersDecimalSymbol, sizeof (GLB_NumbersDecimalSymbol));
      pRegOpt->num_items = (sizeof (GLB_NumbersDecimalSymbol) / sizeof (TYPE_REG_OPT_ITEM));
    break;
    
    case NUMBERS_GROUP_SYMBOL:
      memcpy (pRegOpt->p_items, GLB_NumbersGroupSymbol, sizeof (GLB_NumbersGroupSymbol));
      pRegOpt->num_items = sizeof (GLB_NumbersGroupSymbol) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    case NUMBERS_DECIMAL_DIGITS:
      memcpy (pRegOpt->p_items, GLB_NumbersDecimalDigits, sizeof (GLB_NumbersDecimalDigits));
      pRegOpt->num_items = sizeof (GLB_NumbersDecimalDigits) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    case CURRENCY_SYMBOL_POS:
      memcpy (pRegOpt->p_items, GLB_CurrencySymbolPos, sizeof (GLB_CurrencySymbolPos));
      pRegOpt->num_items = sizeof (GLB_CurrencySymbolPos) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    case CURRENCY_DECIMAL_SYMBOL:
      memcpy (pRegOpt->p_items, GLB_CurrencyDecimalSymbol, sizeof (GLB_CurrencyDecimalSymbol));
      pRegOpt->num_items = sizeof (GLB_CurrencyDecimalSymbol) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    case CURRENCY_GROUP_SYMBOL:
      memcpy (pRegOpt->p_items, GLB_CurrencyGroupSymbol, sizeof (GLB_CurrencyGroupSymbol));
      pRegOpt->num_items = sizeof (GLB_CurrencyGroupSymbol) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    case CURRENCY_DECIMAL_DIGITS:
      memcpy (pRegOpt->p_items, GLB_CurrencyDecimalDigits, sizeof (GLB_CurrencyDecimalDigits));
      pRegOpt->num_items = sizeof (GLB_CurrencyDecimalDigits) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    case DATE_FORMAT:
      memcpy (pRegOpt->p_items, GLB_DateFormat, sizeof (GLB_DateFormat));
      pRegOpt->num_items = sizeof (GLB_DateFormat) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    case DATE_GROUP_SYMBOL:
      memcpy (pRegOpt->p_items, GLB_DateGroupSymbol, sizeof (GLB_DateGroupSymbol));
      pRegOpt->num_items = sizeof (GLB_DateGroupSymbol) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    case TIME_FORMAT:
      memcpy (pRegOpt->p_items, GLB_TimeFormat, sizeof (GLB_TimeFormat));
      pRegOpt->num_items = sizeof (GLB_TimeFormat) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    case TIME_GROUP_SYMBOL:
      memcpy (pRegOpt->p_items, GLB_TimeGroupSymbol, sizeof (GLB_TimeGroupSymbol));
      pRegOpt->num_items = sizeof (GLB_TimeGroupSymbol) / sizeof (TYPE_REG_OPT_ITEM);
    break;
    
    default:   
    ;   
   }
  
   return TRUE;
} // Common_GetRegOpt