//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
//
//   MODULE NAME: Queue_API.cpp
//
//   DESCRIPTION: Functions and Methods to manage queues.
//
//        AUTHOR: Toni Jord�
//
// CREATION DATE: 15-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-MAR-2002 TJG    First release.
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_MISC
#define INCLUDE_QUEUE_API
#define INCLUDE_PRIVATE_LOG
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

#define QUEUE_API_NAME _T("QUEUE_API")

#define QUEUE_AUX_STRING_LENGTH     256

#define QUEUE_MAX_NUMBER_OF_QUEUES  512
#define QUEUE_MIN_NUM_ENTRIES       256

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

typedef struct
{
  DWORD data_length;                // Length of entry data (bytes) 

} TYPE_QUEUE_ENTRY_HEADER;          // Control area for every queue entry

typedef struct
{
  CRITICAL_SECTION      queue_cs;             // Protects access to queue private areas 
  BYTE                  * data_area;          // Points to the data area 
  HANDLE                ev_queue_not_full;    // Event to signal when queue is full 
  BOOL                  read_req_pending;     // Flag to indicate a user is waiting to read 
  DWORD                 current_read_handle;  // Handle to identify current read operation. Used in CancelRead request
  DWORD                 read_handle_counter;  // Read handle counter, used to assign read handle in read operations
  HANDLE                user_read_event;      // User Event signaled when read operation completed
  PAPCFUNC              p_user_apc_routine;   // User APC compoetion routine
  HANDLE                user_thread_apc;      // Thread Handle to queue APC routine
  VOID                  * p_user_apc_data;    // User data ponter passed to APC completion routine
  VOID                  * reply_buffer;       // Pointer to the output area (Async. mode) 
  DWORD                 * reply_data_length;  // Data length in the user output area 
  DWORD                 reply_buffer_size;    // Length available in the user output area 
  DWORD                 first_byte;           // Index to the first byte in queue 
  DWORD                 last_byte;            // Index to the last byte in queue 
  DWORD                 number_of_items;      // Number of elements in queue 
  DWORD                 queue_size;           // Queue size (bytes)
  DWORD                 queue_free_size;      // Queue current free space

  TCHAR                 filename[_MAX_PATH];

} TCOMON_QUEUE;

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

static TCOMON_QUEUE     GLB_QueuePool [QUEUE_MAX_NUMBER_OF_QUEUES];
static WORD             GLB_NumberOfQueues;

CRITICAL_SECTION        GLB_CreateQueueCS;    // Critical section to protect queue creation 

#define QUEUE_ENTRY_HEADER_SIZE sizeof (TYPE_QUEUE_ENTRY_HEADER)

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

static BOOL Queue_NotifyUser (TCOMON_QUEUE * pQueue);

static BOOL Queue_WaitQueueNotFull (TCOMON_QUEUE * pQueue,
                                    DWORD        Timeout,
                                    BOOL         * pQueueNotFull);

static BOOL Queue_SetFullState (TCOMON_QUEUE * pQueue,
                                BOOL         QueueFull);

static void Queue_CopyDataFromQueue  (TCOMON_QUEUE  * pQueue,
                                      BYTE          * pBuffer,
                                      DWORD         BufferSize,
                                      BOOL          RemoveData,
                                      BOOL          CopyData);

static void Queue_CopyDataToQueue  (TCOMON_QUEUE  * pQueue,
                                    BYTE          * pBuffer,
                                    DWORD         BufferSize);

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: DLL main
// 
//  PARAMS:
//      - INPUT:
//          - ModuleHandle
//          - ReasonForCall
//          - pReserved
//
//      - OUTPUT: None
//
// RETURNS: 
// 
//   NOTES:

BOOL APIENTRY       DllMain (HANDLE     ModuleHandle,
                             DWORD      ReasonForCall,
                             LPVOID     pReserved)
{
  switch ( ReasonForCall )
  {
    case DLL_PROCESS_ATTACH:

      // DLL load action 
      InitializeCriticalSection (&GLB_CreateQueueCS);   //  CriticalSection for queue creation 
      memset (&GLB_QueuePool, 0, sizeof (GLB_QueuePool));   // Initialize Queue pool area 
      GLB_NumberOfQueues = 0;
    break;

    case DLL_THREAD_ATTACH :
    case DLL_THREAD_DETACH :
    case DLL_PROCESS_DETACH :
    break;
  }   //  switch 

    return TRUE;

} // DllMain 


//------------------------------------------------------------------------------
// PURPOSE: Initialize and create a new queue and data buffer.
// 
//  PARAMS:
//      - INPUT:
//          - UserQueueSize: bytes. Note that each entry on the queue has an overhead 
//                           of 2 bytes (size of TYPE_QUEUE_ENTRY_HEADER) to store the entry data size.
//
//      - OUTPUT: 
//          - QHandle : Queue's handle
//
// RETURNS: None
// 
//   NOTES:
//
QUEUE_API WORD WINAPI   Queue_CreateQueueEx (DWORD        UserQueueSize,
                                             QUEUE_HANDLE * QHandle,
                                             TCHAR        * pFilename,
                                             DWORD        LineNumber)
{
  TCHAR  _aux_tchar_1[QUEUE_AUX_STRING_LENGTH];

  void          * p_buffer;         // Auxiliary pointer for malloc operations 
  TCOMON_QUEUE  * p_queue;          // Shortcut to access the queue 
  DWORD         _queue_size;

  // Check input parameter values 
  if ( ! UserQueueSize )
  {
    // Queue size must be > 0
    return QUEUE_STATUS_ERROR;
  }

  // Protect access to Queue Manager private areas 
  EnterCriticalSection (&GLB_CreateQueueCS);

  if ( GLB_NumberOfQueues >= QUEUE_MAX_NUMBER_OF_QUEUES )
  {
    LeaveCriticalSection (&GLB_CreateQueueCS);
    *QHandle = QUEUE_NULL_HANDLE;

    return QUEUE_STATUS_ERROR;  // Maximum number of queues exceeded 
    
  } // if

  // Obtain a handle for the new queue 
  *QHandle = GLB_NumberOfQueues++;
  p_queue  = &GLB_QueuePool[*QHandle];   // Create a shortcut to new queue 

  _stprintf (p_queue->filename, _T("%s:%lu"), pFilename, LineNumber);

  LeaveCriticalSection (&GLB_CreateQueueCS);

  // Allocate memory to hold queue entries.
  // Note that additionally to user queue size,
  // room for at least 256 entry headers is also allocated
  _queue_size = UserQueueSize + QUEUE_MIN_NUM_ENTRIES * QUEUE_ENTRY_HEADER_SIZE;
  p_buffer = (BYTE *) malloc (_queue_size);
  if (p_buffer == NULL)
  {
    // Logger message
    PrivateLog (2, QUEUE_API_NAME, _T(__FUNCTION__), _T("NULL"), _T("malloc"));

    // Roolback 
    EnterCriticalSection (&GLB_CreateQueueCS);
    GLB_NumberOfQueues--;                   // Delete created queue 
    LeaveCriticalSection (&GLB_CreateQueueCS);
    *QHandle = QUEUE_NULL_HANDLE;

    return QUEUE_STATUS_ERROR;

  } // if

  p_queue->queue_size      = _queue_size;
  p_queue->queue_free_size = _queue_size;
  p_queue->data_area       = (BYTE *) p_buffer;
  memset (p_buffer, 0, _queue_size);  // Initialize data area 

  // Initialize queue header to initial values 
  p_queue->first_byte      = 0;
  p_queue->last_byte       = 0;
  p_queue->number_of_items = 0;

  // Create event queue not full
  p_queue->ev_queue_not_full = CreateEvent (NULL,  // Event attributes 
                                            TRUE,  // ManualReset       
                                            TRUE,  // InitialState     
                                            NULL); // Name             
  if ( p_queue->ev_queue_not_full == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    PrivateLog (2, QUEUE_API_NAME, _T(__FUNCTION__), _aux_tchar_1, _T("CreateEvent"));

    free (p_queue->data_area);
    EnterCriticalSection (&GLB_CreateQueueCS);
    GLB_NumberOfQueues--;                   // Delete created queue 
    LeaveCriticalSection (&GLB_CreateQueueCS);
    *QHandle = QUEUE_NULL_HANDLE;

    return QUEUE_STATUS_ERROR;

  } // if
  
  p_queue->read_req_pending     = FALSE;
  p_queue->read_handle_counter  = 0;
  p_queue->reply_buffer         = NULL;
  p_queue->reply_data_length    = 0;
  p_queue->reply_buffer_size    = 0;

  // Initialize critical section for this queue to protect it 
  // from being accessed by more than one thread at a a time 
  InitializeCriticalSection (&p_queue->queue_cs);

  return QUEUE_STATUS_OK;

} // Queue_CreateQueue


//------------------------------------------------------------------------------
// PURPOSE: Reads first available data buffer from the queue.
//          Note that two notification methods can be used:
//            - EventObject
//            - User APC completion routine (callback). The APC function must 
//              fit the following prototype: VOID CALLBACK APCProc(ULONG_PTR dwParam);
// 
//  PARAMS:
//      - INPUT:
//          - QHandle: Queue's handle
//          - EventObject: (optional) Event to raise when read is finished. 
//                         NULL if APC notification method is used.
//          - pUserAPCRoutine: (optional) User APC routine called when read operation has completed.
//                             NULL if EventObject notification method is used.
//          - pUserAPCData: (optional) User data pointer passed to APC completion routine.
//                          NULL if EventObject notification method is used.
//          - UserBufferSize
//
//      - OUTPUT: 
//          - pUserBuffer : Pointer to the reception buffer.
//          - pActualDataLength : (optional) Actual data length in user buffer (bytes).
//          - pUserReadHandle : Queue read id (used to cancel pending read operation)
//
// RETURNS: 
//      - QUEUE_STATUS_OK
//      - QUEUE_STATUS_NOT_INITIALIZED
//      - QUEUE_STATUS_ERROR
// 
//   NOTES:
//
QUEUE_API WORD WINAPI   Queue_Read (QUEUE_HANDLE    QHandle, 
                                    HANDLE          EventObject,
                                    VOID            * pUserAPCRoutine,
                                    VOID            * pUserAPCData,
                                    DWORD           UserBufferSize, 
                                    void            * pUserBuffer,
                                    DWORD           * pActualDataLength,
                                    DWORD           * pUserReadHandle)
{
  TCHAR  _aux_tchar_1[QUEUE_AUX_STRING_LENGTH];

  BOOL          _bool_rc;
  TCOMON_QUEUE  * p_queue;
  TYPE_QUEUE_ENTRY_HEADER _entry_header;

  // Check if queue is initialized or not 
  if ( QHandle >= GLB_NumberOfQueues )
  {
    return QUEUE_STATUS_NOT_INITIALIZED;
  }

  // Create a shortcut to access the queue 
  p_queue = &GLB_QueuePool[QHandle];

  __try
  {
    // Protect access to private queue fields 
    EnterCriticalSection (&p_queue->queue_cs);

    // Check already pending read
    if ( p_queue->read_req_pending )
    {
      // Two simultaneous read in the same queue are not allowed
      return QUEUE_STATUS_PENDING_READ;
    }

    // Save read request parameters
    p_queue->read_req_pending     = TRUE;
    p_queue->current_read_handle  = p_queue->read_handle_counter++;
    p_queue->user_read_event      = EventObject;
    p_queue->p_user_apc_routine   = (PAPCFUNC) pUserAPCRoutine;
    p_queue->user_thread_apc      = NULL;
    p_queue->p_user_apc_data      = pUserAPCData;
    p_queue->reply_buffer         = pUserBuffer;
    p_queue->reply_data_length    = pActualDataLength;
    p_queue->reply_buffer_size    = UserBufferSize;
    
    // Get thread handle if APC is requested
    if ( p_queue->p_user_apc_routine != NULL )
    {
      _bool_rc = DuplicateHandle (GetCurrentProcess(),        // Source process
                                  GetCurrentThread(),         // Original handle
                                  GetCurrentProcess(),        // Destination process
                                  &p_queue->user_thread_apc,  // New duplicate handle
                                  0,                          // Access rights (overridden by last parameter) 
                                  FALSE,                      // Children do not inherit the handle 
                                  DUPLICATE_SAME_ACCESS);     // Copy access rights from original handle
      if ( !_bool_rc )
      {
        // Error getting thread handle
        _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
        PrivateLog (2, QUEUE_API_NAME, _T(__FUNCTION__), _aux_tchar_1, _T("DuplicateHandle"));

        // Read request aborted
        p_queue->read_req_pending = FALSE;
        return QUEUE_STATUS_ERROR;
      }
    }

    // Set user read handle
    if ( pUserReadHandle != NULL )
    {
      * pUserReadHandle = p_queue->current_read_handle;
    }
    
    // If queue has no data, thread must wait for new data to be written 
    if ( p_queue->number_of_items == 0 )
    {
      return QUEUE_STATUS_OK;
    }

    // Check user buffer size with current queue entry
    Queue_CopyDataFromQueue (p_queue,
                             (BYTE *) &_entry_header,
                             QUEUE_ENTRY_HEADER_SIZE,
                             FALSE,                   // Do not remove data from queue
                             TRUE);                   // Copy data

    // Check buffer length 
    if ( UserBufferSize < _entry_header.data_length )
    {
      // Read request aborted
      p_queue->read_req_pending = FALSE;
      return QUEUE_STATUS_TOO_BIG;
    }
    
    // Remove entry header
    Queue_CopyDataFromQueue (p_queue,
                             NULL,
                             QUEUE_ENTRY_HEADER_SIZE,
                             TRUE,                    // Remove data from queue
                             FALSE);                  // Do not copy data

    // Copy entry data
    Queue_CopyDataFromQueue (p_queue,
                             (BYTE *) pUserBuffer,
                             _entry_header.data_length,
                             TRUE,                    // Remove data from queue
                             TRUE);                   // Copy data

    // User data length    
    if ( pActualDataLength != NULL )
    {
      * pActualDataLength = _entry_header.data_length;
    }

    // Remove entry from the queue 
    p_queue->number_of_items--;

    // Set QueueNotFull event to signal there are free positions 
    Queue_SetFullState (p_queue, FALSE);

    // Read operation completed
    p_queue->read_req_pending = FALSE;

    // User data available: notify user (event or APC)
    _bool_rc = Queue_NotifyUser (p_queue);
    if ( !_bool_rc )
    {
      return QUEUE_STATUS_ERROR;
    }
    else
    {
      return QUEUE_STATUS_OK;
    }
   } // try
   
   __finally
   {
   
     LeaveCriticalSection (&p_queue->queue_cs);
     
   } // finally
  
} // Queue_Read 

//------------------------------------------------------------------------------
// PURPOSE: Writes data to the queue.
// 
//  PARAMS:
//      - INPUT:
//          - QHandle : Queue's handle
//          - pUserBuffer : Pointer to the data containing the data to add.
//          - UserBufferLength : Length of this buffer (in bytes).
//          - QueueMode: specifies what the bahaviour will be when the queue is full
//
//      - OUTPUT: 
//
// RETURNS: 
//      - QUEUE_STATUS_OK
//      - QUEUE_STATUS_NOT_INITIALIZED
//      - QUEUE_STATUS_TOO_BIG
//      - QUEUE_STATUS_ERROR
//      - QUEUE_STATUS_FULL
// 
//   NOTES:
//
QUEUE_API WORD WINAPI   Queue_Write (QUEUE_HANDLE   QHandle, 
                                     void           * pUserBuffer, 
                                     DWORD          UserBufferLength,
                                     WORD           QueueMode)
{
  BOOL          _bool_rc;
  BOOL          _queue_full;
  TCOMON_QUEUE  * p_queue;
  TYPE_QUEUE_ENTRY_HEADER _entry_header;

  // Check if queue is initialized or not 
  if ( QHandle >= GLB_NumberOfQueues )
  {
    return QUEUE_STATUS_NOT_INITIALIZED;
  }

   // Create a shortcut to access the queue 
  p_queue = &GLB_QueuePool[QHandle];

  // Check if queue can hold the user data 
  if ( UserBufferLength + QUEUE_ENTRY_HEADER_SIZE > p_queue->queue_size )
  {
    return QUEUE_STATUS_TOO_BIG;
  }

  while ( TRUE )
  {
    // If the queue is full, return QUEUE FULL error code or wait for a free  
    // buffer position to write, depending on QueueMode parameter 
    _bool_rc = Queue_WaitQueueNotFull (p_queue,
                                       ( QueueMode == QUEUE_MODE_NO_WAIT ) ? 0 : INFINITE,
                                       &_queue_full);
    if ( !_bool_rc )
    {
      return QUEUE_STATUS_ERROR;
    }
    if ( _queue_full )
    {
      return QUEUE_STATUS_FULL;
    }
    
    // Protect access to private queue fields 
    EnterCriticalSection (&p_queue->queue_cs);
    
    // Check if there is enough free space in the queue to hold the user data + entry header
    if ( p_queue->queue_free_size >= UserBufferLength + QUEUE_ENTRY_HEADER_SIZE )
    {
      // Enough free space to store the new entry
      // Loop end
      break;
    }
    else
    {
      // Set queue full to wait for a future read operation in order to free queue space
      _bool_rc = Queue_SetFullState (p_queue, TRUE);
      if ( !_bool_rc )
      {
        LeaveCriticalSection (&p_queue->queue_cs);
        return QUEUE_STATUS_ERROR;
      }
      // Continue waiting
    }

    LeaveCriticalSection (&p_queue->queue_cs);
    
  } // while

  __try
  {
    // Check for any thread waiting for incoming data 
    if ( p_queue->read_req_pending )
    {
      // Check if user buffer has enough space in his buffer to hold the data
      if ( p_queue->reply_buffer_size < UserBufferLength )
      {
        TCHAR _aux_log_param1[PRIVATELOG_SIZE_PARAM];
        TCHAR _aux_log_param2[PRIVATELOG_SIZE_PARAM];

        _stprintf (_aux_log_param1, "%ld", p_queue->reply_buffer_size);
        _stprintf (_aux_log_param2, "%ld", UserBufferLength);
        PrivateLog (3, _T("QUEUE_API"), _T("Queue_Write"), 
                    _T("Size Mismatch in Read/Write Buffers"), _aux_log_param1, _aux_log_param2); 

        return QUEUE_STATUS_TOO_BIG;
      }
      
      // Copy data area 
      memcpy (p_queue->reply_buffer, pUserBuffer, UserBufferLength);
      if ( p_queue->reply_data_length != NULL )
      {
        * p_queue->reply_data_length = UserBufferLength;
      }

      // User data available: notify user (event or APC)
      Queue_NotifyUser (p_queue);

      // Reset Read Request Pending indicator 
      p_queue->read_req_pending = FALSE;

      return QUEUE_STATUS_OK;
    }

    // Store new entry in the queue
    _entry_header.data_length = UserBufferLength;

    // Entry header
    Queue_CopyDataToQueue (p_queue,
                           (BYTE *) &_entry_header,
                           QUEUE_ENTRY_HEADER_SIZE);

    // Entry data    
    Queue_CopyDataToQueue (p_queue,
                           (BYTE *) pUserBuffer,
                           UserBufferLength);

    // Add new data item to the queue 
    p_queue->number_of_items++;

    // If the queue is full (or can not hold at least one entry of one byte), reset event QueueNotFull 
    if ( p_queue->queue_free_size < QUEUE_ENTRY_HEADER_SIZE + 1 )
    {
      Queue_SetFullState (p_queue, TRUE);
    }

    return QUEUE_STATUS_OK;

  } // try
  
  __finally
  {

    LeaveCriticalSection (&p_queue->queue_cs);

  } // finally

} // Queue_Write

//------------------------------------------------------------------------------
// PURPOSE: Purge queue data.
// 
//  PARAMS:
//      - INPUT:
//          - QHandle : Queue's handle
//
//      - OUTPUT: 
//
// RETURNS: 
//      - QUEUE_STATUS_OK
//      - QUEUE_STATUS_ERROR
//      - QUEUE_STATUS_NOT_INITIALIZED
// 
//   NOTES:
//
QUEUE_API WORD WINAPI   Queue_Purge (QUEUE_HANDLE   QHandle)
{
  TCOMON_QUEUE  * p_queue;   // Shortcut to access the queue 

  // Check if queue is initialized or not 
  if ( QHandle >= GLB_NumberOfQueues )
  {
    return QUEUE_STATUS_NOT_INITIALIZED;
  }

  // Create a shortcut to access the queue 
  p_queue = &GLB_QueuePool[QHandle];

  // Protect access to private queue fields 
  EnterCriticalSection (&p_queue->queue_cs);

  // Purge queue messages
  p_queue->first_byte      = 0;
  p_queue->last_byte       = 0;
  p_queue->number_of_items = 0;

  LeaveCriticalSection (&p_queue->queue_cs);

  return QUEUE_STATUS_OK;

} // Queue_Purge

//------------------------------------------------------------------------------
// PURPOSE: Cancels any read operation pending on the queue
// 
//  PARAMS:
//      - INPUT:
//          - QHandle : Queue's handle
//
//      - OUTPUT: 
//
// RETURNS: 
//      - QUEUE_STATUS_OK
//      - QUEUE_STATUS_ERROR
//      - QUEUE_STATUS_NOT_INITIALIZED
//      - QUEUE_STATUS_PARAM_ERROR
// 
//   NOTES:
//
QUEUE_API WORD WINAPI   Queue_CancelRead (QUEUE_HANDLE   QHandle, 
                                          DWORD          ReadHandle)
{
  TCOMON_QUEUE  * p_queue;   // Shortcut to access the queue 

  // Check if queue is initialized or not 
  if ( QHandle >= GLB_NumberOfQueues )
  {
    return QUEUE_STATUS_NOT_INITIALIZED;
  }

  // Create a shortcut to access the queue 
  p_queue = &GLB_QueuePool[QHandle];

  // Protect access to private queue fields 
  EnterCriticalSection (&p_queue->queue_cs);

  // Check current read operation
  if (   ! p_queue->read_req_pending 
      || p_queue->current_read_handle != ReadHandle )
  {
      LeaveCriticalSection (&p_queue->queue_cs);
      
      // No current read operation or user read handle does not match current read operation
      return QUEUE_STATUS_PARAM_ERROR;
  }
  
  // Cancel any pending request
  p_queue->read_req_pending = FALSE;

  LeaveCriticalSection (&p_queue->queue_cs);

  return QUEUE_STATUS_OK;

} // Queue_CancelRead

//------------------------------------------------------------------------------
// PURPOSE: Cancels any read operation pending on the queue
// 
//  PARAMS:
//      - INPUT:
//          - QHandle : Queue's handle
//
//      - OUTPUT: 
//          - pNumItems: Number of entries in the queue
//
// RETURNS: 
//      - QUEUE_STATUS_OK
//      - QUEUE_STATUS_NOT_INITIALIZED
// 
//   NOTES:
//
QUEUE_API WORD WINAPI   Queue_GetNumItems (QUEUE_HANDLE   QHandle, 
                                           DWORD          * pNumItems)
{
  TCOMON_QUEUE  * p_queue;   // Shortcut to access the queue 

  // Check if queue is initialized or not 
  if ( QHandle >= GLB_NumberOfQueues )
  {
    return QUEUE_STATUS_NOT_INITIALIZED;
  }

  // Create a shortcut to access the queue 
  p_queue = &GLB_QueuePool[QHandle];

  // Protect access to private queue fields 
  EnterCriticalSection (&p_queue->queue_cs);
  
  * pNumItems = p_queue->number_of_items;

  LeaveCriticalSection (&p_queue->queue_cs);

  return QUEUE_STATUS_OK;

} // Queue_GetNumItems

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Notifies read operation to user using one of the available methods:
//            - Event object
//            - APC
// 
//  PARAMS:
//      - INPUT:
//          - pQueue : Queue's pointer
//
//      - OUTPUT: 
//
// RETURNS: 
//      - QUEUE_STATUS_OK
//      - QUEUE_STATUS_ERROR
//      - QUEUE_STATUS_NOT_INITIALIZED
// 
//   NOTES:
//
BOOL Queue_NotifyUser (TCOMON_QUEUE * pQueue)
{
  TCHAR  _aux_tchar_1[QUEUE_AUX_STRING_LENGTH];

  DWORD _rc;
  BOOL  _bool_rc;

  if ( pQueue->user_read_event != NULL )
  {
    // Signal user event
    _bool_rc = SetEvent (pQueue->user_read_event);
    if ( ! _bool_rc )
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      PrivateLog (2, QUEUE_API_NAME, _T(__FUNCTION__), _aux_tchar_1, _T("SetEvent"));
      
      return FALSE;
    }
  }
  else if ( pQueue->p_user_apc_routine != NULL )
  {
    // Queue user APC
    _rc = QueueUserAPC (pQueue->p_user_apc_routine,            // APC function
                        pQueue->user_thread_apc,               // handle to thread
                        (ULONG_PTR) pQueue->p_user_apc_data);  // APC function parameter
    if ( !_rc )
    {
      // APC error
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      PrivateLog (2, QUEUE_API_NAME, _T(__FUNCTION__), _aux_tchar_1, _T("QueueUserAPC"));
    }
    
    // Close thread handle
    CloseHandle (pQueue->user_thread_apc);

    if ( !_rc )
    {
      return FALSE;
    }
  }
  
  return TRUE;

} // Queue_NotifyUser

//------------------------------------------------------------------------------
// PURPOSE: Waits until queue is not full using the event
// 
//  PARAMS:
//      - INPUT:
//          - pQueue : Queue's pointer
//          - Timeout : Wait timeout
//
//      - OUTPUT: 
//          - pQueueFull : Queue status after wait
//
// RETURNS: 
//      - TRUE: execution ok
//      - FALSE: execution error
// 
//   NOTES:
//
BOOL Queue_WaitQueueNotFull (TCOMON_QUEUE * pQueue,
                             DWORD Timeout,
                             BOOL * pQueueFull)
{
  TCHAR  _aux_tchar_1[QUEUE_AUX_STRING_LENGTH];

  DWORD _wait_rc;

  _wait_rc = WaitForSingleObjectEx (pQueue->ev_queue_not_full,  // Handle
                                    Timeout,                    // Timeout
                                    FALSE);                     // Alertable
  switch ( _wait_rc )
  {
    case WAIT_OBJECT_0:
      // Queue not full
      * pQueueFull = FALSE;
    break;
    
    case WAIT_TIMEOUT:
      // Queue full
      * pQueueFull = TRUE;
    break;
    
    case WAIT_FAILED:
    default:
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      PrivateLog (2, QUEUE_API_NAME, _T(__FUNCTION__), _aux_tchar_1, _T("WaitForSingleObject"));

      return FALSE;

    break;
    
  } // switch
  
  return TRUE;
  
} // Queue_WaitQueueNotFull

//------------------------------------------------------------------------------
// PURPOSE: Sets queue full state using the event.
// 
//  PARAMS:
//      - INPUT:
//          - pQueue : Queue's pointer
//          - QueueFull : Queue full state
//
//      - OUTPUT: 
//
// RETURNS: 
//      - TRUE: execution ok
//      - FALSE: execution error
// 
//   NOTES:
//
BOOL Queue_SetFullState (TCOMON_QUEUE * pQueue,
                         BOOL QueueFull)
{
  TCHAR  _aux_tchar_1[QUEUE_AUX_STRING_LENGTH];
  
  BOOL _bool_rc;

  if ( QueueFull )
  {
    _bool_rc = ResetEvent (pQueue->ev_queue_not_full);
    if ( ! _bool_rc )
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      PrivateLog (2, QUEUE_API_NAME, _T(__FUNCTION__), _aux_tchar_1, _T("ResetEvent"));
      
      return FALSE;
    }
  }
  else
  {
    _bool_rc = SetEvent (pQueue->ev_queue_not_full);
    if ( ! _bool_rc )
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      PrivateLog (2, QUEUE_API_NAME, _T(__FUNCTION__), _aux_tchar_1, _T("SetEvent"));
      
      return FALSE;
    }
  }

  return TRUE;
  
} // Queue_SetFullState

//------------------------------------------------------------------------------
// PURPOSE: Circular copy from queue to buffer
// 
//  PARAMS:
//      - INPUT:
//          - pQueue
//          - BufferSize
//          - RemoveData: If TRUE data is removed from queue
//          - CopyData: Only if TRUE data is copied to output buffer
//
//      - OUTPUT: 
//          - pBuffer
//
// RETURNS:
// 
//   NOTES:
//
void Queue_CopyDataFromQueue (TCOMON_QUEUE  * pQueue,
                              BYTE          * pBuffer,
                              DWORD         BufferSize,
                              BOOL          RemoveData,
                              BOOL          CopyData)
{
  DWORD _data_length_1;  
  DWORD _data_length_2;  
  DWORD _data_idx;   

  if ( CopyData )
  {    
    _data_idx = pQueue->first_byte;
    if ( _data_idx + BufferSize > pQueue->queue_size )
    {
      // Data split up
      _data_length_1 = pQueue->queue_size - _data_idx;
      _data_length_2 = BufferSize - _data_length_1;
    }
    else
    {
      // Straight data
      _data_length_1 = BufferSize;
      _data_length_2 = 0;
    }

    memcpy (&pBuffer[0], &pQueue->data_area[_data_idx], _data_length_1);
    if ( _data_length_2 )
    {
      memcpy (&pBuffer[_data_length_1], &pQueue->data_area[0], _data_length_2);
    }
  }

  // Remove data from queue only if it was requested
  if ( RemoveData )
  {
    pQueue->first_byte       = (pQueue->first_byte + BufferSize) % pQueue->queue_size;
    pQueue->queue_free_size += BufferSize;
  }

} // Queue_CopyDataFromQueue

//------------------------------------------------------------------------------
// PURPOSE: Circular copy from buffer to queue
//          This function assumes there is enough free space in the queue
// 
//  PARAMS:
//      - INPUT:
//          - pQueue
//          - pBuffer
//          - BufferSize
//
//      - OUTPUT: 
//
// RETURNS:
// 
//   NOTES:
//
static void Queue_CopyDataToQueue  (TCOMON_QUEUE * pQueue,
                                    BYTE * pBuffer,
                                    DWORD BufferSize)
{
  DWORD _data_length_1;  
  DWORD _data_length_2;  
  DWORD _data_idx;   

  _data_idx = pQueue->last_byte;
  if ( _data_idx + BufferSize > pQueue->queue_size )
  {
    // Data split up
    _data_length_1 = pQueue->queue_size - _data_idx;
    _data_length_2 = BufferSize - _data_length_1;
  }
  else
  {
    // Straight data
    _data_length_1 = BufferSize;
    _data_length_2 = 0;
  }

  memcpy (&pQueue->data_area[_data_idx], &pBuffer[0], _data_length_1);
  if ( _data_length_2 )
  {
    memcpy (&pQueue->data_area[0], &pBuffer[_data_length_1], _data_length_2);
  }

  pQueue->last_byte        = (pQueue->last_byte + BufferSize) % pQueue->queue_size;
  pQueue->queue_free_size -= BufferSize;

} // Queue_CopyDataToQueue
