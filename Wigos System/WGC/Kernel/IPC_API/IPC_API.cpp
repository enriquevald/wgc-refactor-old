//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : IPC_API.cpp
// 
//   DESCRIPTION : Functions and Methods to handle IPC communications.
// 
//        AUTHOR : Andreu Juli� / Xavier Ib��ez
// 
// CREATION DATE : 08-MAR-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
//----------- ------ -----------------------------------------------------------
// 08-MAR-2002 AJQ    Initial draft.
// 26-MAR-2002 XID    Module implementation
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_API
#define INCLUDE_QUEUE_API
#define INCLUDE_IPC_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_COMMON_DATA

#include "CommonDef.h"

#include "IPC_APIInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

#define IPC_MIN_LOCAL_NODE_INPUT_BUFFER_SIZE  32768   // bytes. 32 Kb
#define IPC_MAX_PARALLEL_SENDS                16      // Maximum number of parallel
                                                      // sends queued to system thread pool
                                                      // using WT_EXECUTELONGFUNCTION

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

// Common API data
TYPE_COMMON_API_DATA  GLB_API;

// Nodes & Module data
TYPE_IPC_NODE_TABLE   GLB_IpcNodeTable;
TYPE_API_THREADS      GLB_ApiThreads;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE : The IPC public interface
//
//  PARAMS :
//     - INPUT :
//      - pApiInputParams
//      - pApiInputUserData
//     - OUTPUT :
//      - pApiOutputParams
//      - pApiOutputUserData
//
// RETURNS :
//     - API_STATUS_OK
//     - API_STATUS_PARAMETER_ERROR
//     - API_STATUS_CB_SIZE_ERROR
//     - API_STATUS_EVENT_OBJECT_ERROR
//     - API_STATUS_NO_RESOURCES_AVAILABLE
//
// NOTES :

IPC_API WORD WINAPI Ipc_API (API_INPUT_PARAMS     *pApiInputParams,
                             API_INPUT_USER_DATA  *pApiInputUserData,
                             API_OUTPUT_PARAMS    *pApiOutputParams,
                             API_OUTPUT_USER_DATA *pApiOutputUserData)
{
  static TCHAR _function_name[] = _T("Ipc_API");

  return Common_API_Entry (pApiInputParams,
                           pApiInputUserData,
                           pApiOutputParams,
                           pApiOutputUserData,
                           _function_name);

} // Ipc_API

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Module Init"
//            - Reads configuration
//            - Initializes tcp/ip
//            - Initializes node data in ipc_node table
//
//  PARAMS :
//
//      - INPUT :
//        - ApiRequestParams
//
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void IPC_ModuleInit (TYPE_API_REQUEST_PARAMS *pApiRequestParams, 
                     WORD * pRequestOutput1, 
                     WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("IPC_ModuleInit");

  BOOL    _bool_rc;
  DWORD   _node_idx;
  DWORD   _idx_instance;
  DWORD   _local_inst_idx;
  BOOL    _addr_found;
  DWORD   _local_addr;
  WORD    _local_tcp_port;
 
  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;

  __try
  {
    // Synchronize module initialization. It is necessary because 
    // module init can be called from several threads at the same time.
    EnterCriticalSection (&GLB_IpcNodeTable.init_ipc_node_table_cs);

    // Check if Module already initialized or at least someone tried to initialize it
    if (   GLB_API.init
        || GLB_API.init_tried )
    {
      if ( ! GLB_API.init )
      {
        // Module init tried, but not initialized. Set user error.
        * pRequestOutput1 = IPC_STATUS_ERROR;
        PrivateLog (6, MODULE_NAME, _function_name, _T("Ipc_init_tried"), _T("TRUE"));
        PrivateLog (6, MODULE_NAME, _function_name, _T("Ipc_initialized"), _T("FALSE"));
      }
      return;
    }

    // Set initialization already tried
    GLB_API.init_tried = TRUE;
    
    // Get IPC configuration
    _bool_rc = IPC_Conf_ReadConfig (&GLB_IpcNodeTable);
    if ( !_bool_rc )
    {
      // Configuration error
      * pRequestOutput1 = IPC_STATUS_ERROR;
      
      return;
    }
    
    // Tcp initialization
    _bool_rc = IPC_Tcp_Init ();
    if ( !_bool_rc )
    {
      // Tcp error
      * pRequestOutput1 = IPC_STATUS_ERROR;
      
      return;
    }
     
    // Initialize node data
    for ( _node_idx = 0; _node_idx < GLB_IpcNodeTable.num_nodes; _node_idx++)
    {
      // Find ip address in local IP list
      _addr_found = FALSE;
      for (_idx_instance = 0; 
              _idx_instance < GLB_IpcNodeTable.ipc_node[_node_idx].instances_data.num_instances
          && !_addr_found;
          _idx_instance++)
      {
        if ( GLB_IpcNodeTable.ipc_node[_node_idx].instances_data.instance_entry[_idx_instance].in_use )
        {
          _local_addr     = GLB_IpcNodeTable.ipc_node[_node_idx].instances_data.instance_entry[_idx_instance].ip_addr;
          _local_tcp_port = GLB_IpcNodeTable.ipc_node[_node_idx].instances_data.instance_entry[_idx_instance].tcp_port;
          _addr_found     = IPC_Tcp_FindLocalAddr (_local_addr);
          _local_inst_idx = _idx_instance;
        }

      } // for

      // IP address found in local adaptors: Set local node
      if ( _addr_found )
      {
        // Local instance flag
        GLB_IpcNodeTable.ipc_node[_node_idx].instances_data.instance_entry[_local_inst_idx].local_instance = TRUE;
        // Local node parameters
        GLB_IpcNodeTable.ipc_node[_node_idx].local_node_data.local_node = TRUE;
        GLB_IpcNodeTable.ipc_node[_node_idx].local_node_data.ip_addr    = _local_addr;
        GLB_IpcNodeTable.ipc_node[_node_idx].local_node_data.tcp_port   = _local_tcp_port;
      }

    } // for

    // Module initialized
    GLB_API.init = TRUE;

    return;
    
  } // try
  
  __finally
  {
  
    LeaveCriticalSection (&GLB_IpcNodeTable.init_ipc_node_table_cs);
    
  } // finally

} // IPC_ModuleInit

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Module stop"
//          Sets stop flag to TRUE
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void IPC_ModuleStop (TYPE_API_REQUEST_PARAMS *pApiRequestParams, 
                     WORD * pRequestOutput1, 
                     WORD * pRequestOutput2)
{
  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;

  GLB_API.stopped = TRUE;

  return;

} // IPC_ModuleStop

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Local Node Init"
//            - Creates local node receive queue
//            - Initializes tcp/ip for node
//            - Creates tcp service
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//        - pThreadData
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void IPC_LocalNodeInit (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                        TYPE_API_THREAD_DATA    * pThreadData,
                        WORD * pRequestOutput1, 
                        WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("IPC_LocalNodeInit");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  DWORD                   _queue_buffer_size;
  BOOL                    _bool_rc;
  WORD                    _call_status;
  DWORD                   _node_inst_id;
  TYPE_IPC_NODE           * p_ipc_node;
  IPC_IUD_INIT_LOCAL_NODE * p_ipc_iud_init_local_node;
  IPC_OUD_INIT_LOCAL_NODE * p_ipc_oud_init_local_node;
  DWORD                   _node_idx;

  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = IPC_STATUS_ERROR;
    
    return;
  }

  p_ipc_iud_init_local_node = (IPC_IUD_INIT_LOCAL_NODE *) pApiRequestParams->api_input_user_data_buffer;
  p_ipc_oud_init_local_node = (IPC_OUD_INIT_LOCAL_NODE *) pApiRequestParams->p_api_output_user_data;

  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_ipc_iud_init_local_node->control_block, 
                                           sizeof (IPC_IUD_INIT_LOCAL_NODE),
                                           _function_name, 
                                           _T("IUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    
    return;
  }
  _bool_rc = Common_API_CheckControlBlock (p_ipc_oud_init_local_node->control_block, 
                                           sizeof (IPC_OUD_INIT_LOCAL_NODE),
                                           _function_name, 
                                           _T("OUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }

  // Reset output user data
  memset (p_ipc_oud_init_local_node, 0, sizeof (IPC_OUD_INIT_LOCAL_NODE));

  // Find node
  _bool_rc = IPC_Misc_FindNodeInstId (p_ipc_iud_init_local_node->node_name, 
                                      &_node_inst_id, 
                                      TRUE);      // Local node instance
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_NODE_NAME_NOT_FOUND;
    return;
  }

  // Get node and instances
  _bool_rc = IPC_Misc_GetNodeIdxAndInst (_node_inst_id, &_node_idx);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_INVALID_NODE_INST_ID;
    return;
  }

  // Pointer to node data
  p_ipc_node = &GLB_IpcNodeTable.ipc_node[_node_idx];

  // Request user data output
  p_ipc_oud_init_local_node->node_inst_id = _node_inst_id;
  
  // Check node already init
  if ( p_ipc_node->local_node_data.node_init )
  {
    return;
  }
  
  // Set receive queue buffer size
  _queue_buffer_size = p_ipc_iud_init_local_node->input_buffer_size;
  if ( _queue_buffer_size < IPC_MIN_LOCAL_NODE_INPUT_BUFFER_SIZE )
  {
    _queue_buffer_size = IPC_MIN_LOCAL_NODE_INPUT_BUFFER_SIZE;
  }
  
  // Create receive queue
  _call_status = Queue_CreateQueue (_queue_buffer_size,
                                    &p_ipc_node->local_node_data.receive_queue);
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_CreateQueue"));

    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }
  
  // Specific tcp initialization for local nodes
  _bool_rc = IPC_Tcp_LocalNodeInit (_node_idx, pThreadData);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }

  // Local node initialized
  p_ipc_node->local_node_data.node_init = TRUE;

  return;

} // IPC_LocalNodeInit

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Purge Node"
//          Empties local node receive queue.
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void IPC_PurgeNode (TYPE_API_REQUEST_PARAMS *pApiRequestParams, 
                    WORD * pRequestOutput1, 
                    WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("IPC_PurgeNode");
  TCHAR _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  TYPE_IPC_NODE         * p_ipc_node;
  TYPE_RECEIVE_PENDING  * p_receive_pending;
  IPC_IUD_PURGE_NODE    * p_ipc_iud_purge_node;
  BOOL                  _bool_rc;
  WORD                  _call_status;
  DWORD                 _node_idx;

  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }

  p_ipc_iud_purge_node = (IPC_IUD_PURGE_NODE *) pApiRequestParams->api_input_user_data_buffer;

  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_ipc_iud_purge_node->control_block, 
                                           sizeof (IPC_IUD_PURGE_NODE),
                                           _function_name, 
                                           _T("IUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }

  // Get node and instances
  _bool_rc = IPC_Misc_GetNodeIdxAndInst (p_ipc_iud_purge_node->node_inst_id, &_node_idx);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_INVALID_NODE_INST_ID;
    return;
  }
  p_ipc_node = &GLB_IpcNodeTable.ipc_node[_node_idx];

  // Check IPC node
  _bool_rc = IPC_Misc_CheckNode (p_ipc_iud_purge_node->node_inst_id, _node_idx, IPC_NODE_LOCAL, pRequestOutput1);
  if ( !_bool_rc )
  {
    return;
  }

  // Get a free client buffer
  _bool_rc = IPC_Misc_GetFreeReceivePending (p_ipc_node, &p_receive_pending);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_OVRFLW_RCV_NODE;
    return;
  }

  // Set receive parameters
  p_receive_pending->receive_queue          = p_ipc_node->local_node_data.receive_queue;
  p_receive_pending->api_input_paramas      = pApiRequestParams->api_input_paramas;
  p_receive_pending->p_api_output_params    = pApiRequestParams->p_api_output_params;
  p_receive_pending->p_api_output_user_data = pApiRequestParams->p_api_output_user_data;
  
  // Clear queue contents
  _call_status = Queue_Purge (p_receive_pending->receive_queue);
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_Purge"));

    * pRequestOutput1 = IPC_STATUS_ERROR;
  }
      
  IPC_Misc_ReleaseReceivePending (p_receive_pending);
  
} // IPC_PurgeNode

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Get Node Inst Id"
//          Searches node name in ipc_node table
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void IPC_GetNodeInstId (TYPE_API_REQUEST_PARAMS *pApiRequestParams, 
                        WORD * pRequestOutput1, 
                        WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("IPC_GetNodeId");

  IPC_IUD_GET_NODE_INST_ID * p_ipc_iud_get_node_inst_id;
  IPC_OUD_GET_NODE_INST_ID * p_ipc_oud_get_node_inst_id;
  BOOL                     _bool_rc;
  DWORD                    _node_inst_id;

  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }
  
  p_ipc_iud_get_node_inst_id = (IPC_IUD_GET_NODE_INST_ID *) pApiRequestParams->api_input_user_data_buffer;
  p_ipc_oud_get_node_inst_id = (IPC_OUD_GET_NODE_INST_ID *) pApiRequestParams->p_api_output_user_data;
  
  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_ipc_iud_get_node_inst_id->control_block, 
                                           sizeof (IPC_IUD_GET_NODE_INST_ID),
                                           _function_name, 
                                           _T("IUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    
    return;
  }
  _bool_rc = Common_API_CheckControlBlock (p_ipc_oud_get_node_inst_id->control_block, 
                                           sizeof (IPC_OUD_GET_NODE_INST_ID),
                                           _function_name, 
                                           _T("OUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }

  // Reset output user data
  memset (p_ipc_oud_get_node_inst_id, 0, sizeof (IPC_OUD_GET_NODE_INST_ID));

  // Find node
  _bool_rc = IPC_Misc_FindNodeInstId (p_ipc_iud_get_node_inst_id->node_name, 
                                      &_node_inst_id,
                                      p_ipc_iud_get_node_inst_id->local_instance);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_NODE_NAME_NOT_FOUND;
    return;
  }

  // Request user data output
  p_ipc_oud_get_node_inst_id->node_inst_id = _node_inst_id;

  return;

} // IPC_GetNodeInstId

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Get Node Inst Id"
//          Searches node name in ipc_node table
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void IPC_GetNodeInstIdList (TYPE_API_REQUEST_PARAMS *pApiRequestParams, 
                            WORD * pRequestOutput1, 
                            WORD * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("IPC_GetNodeId");

  IPC_IUD_GET_NODE_INST_ID_LIST * p_ipc_iud_get_node_inst_id_list;
  IPC_OUD_GET_NODE_INST_ID_LIST * p_ipc_oud_get_node_inst_id_list;
  BOOL                     _bool_rc;

  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }
  
  p_ipc_iud_get_node_inst_id_list = (IPC_IUD_GET_NODE_INST_ID_LIST *) pApiRequestParams->api_input_user_data_buffer;
  p_ipc_oud_get_node_inst_id_list = (IPC_OUD_GET_NODE_INST_ID_LIST *) pApiRequestParams->p_api_output_user_data;
  
  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_ipc_iud_get_node_inst_id_list->control_block, 
                                           sizeof (IPC_IUD_GET_NODE_INST_ID_LIST),
                                           _function_name, 
                                           _T("IUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    
    return;
  }
  _bool_rc = Common_API_CheckControlBlock (p_ipc_oud_get_node_inst_id_list->control_block, 
                                           sizeof (IPC_OUD_GET_NODE_INST_ID_LIST),
                                           _function_name, 
                                           _T("OUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;

    return;
  }

  // ** AJQ, 09-DEC-2002 memset not done. It resets the control block! **
  // Reset output user data
  // memset (p_ipc_oud_get_node_inst_id_list, 0, sizeof (IPC_OUD_GET_NODE_INST_ID_LIST));

  // Find node
  _bool_rc = IPC_Misc_FindNodeInstIdList (p_ipc_iud_get_node_inst_id_list->node_name, 
                                          &p_ipc_oud_get_node_inst_id_list->num_instances,
                                          p_ipc_oud_get_node_inst_id_list->cfg_inst_id,
                                          p_ipc_oud_get_node_inst_id_list->node_inst_id,
                                          p_ipc_oud_get_node_inst_id_list->local_instance);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_NODE_NAME_NOT_FOUND;

    return;
  }

  return;

} // IPC_GetNodeInstIdList

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Get Inst Id Data"
//
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void IPC_GetInstIdData (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                        WORD                    * pRequestOutput1, 
                        WORD                    * pRequestOutput2)
{
  static TCHAR _function_name[] = _T("IPC_GetInstIdData");

  IPC_IUD_GET_INST_ID_DATA * p_ipc_iud_get_inst_id_data;
  IPC_OUD_GET_INST_ID_DATA * p_ipc_oud_get_inst_id_data;
  BOOL                     _bool_rc;
  WORD                     _computer_id;
  WORD                     _node_id;

  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }
  
  p_ipc_iud_get_inst_id_data = (IPC_IUD_GET_INST_ID_DATA *) pApiRequestParams->api_input_user_data_buffer;
  p_ipc_oud_get_inst_id_data = (IPC_OUD_GET_INST_ID_DATA *) pApiRequestParams->p_api_output_user_data;
  
  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_ipc_iud_get_inst_id_data->control_block, 
                                           sizeof (IPC_IUD_GET_INST_ID_DATA),
                                           _function_name, 
                                           _T("IUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    
    return;
  }
  _bool_rc = Common_API_CheckControlBlock (p_ipc_oud_get_inst_id_data->control_block, 
                                           sizeof (IPC_OUD_GET_INST_ID_DATA),
                                           _function_name, 
                                           _T("OUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }

  // Reset output user data
  memset (p_ipc_oud_get_inst_id_data, 0, sizeof (IPC_OUD_GET_INST_ID_DATA));

  // Find node
  _bool_rc = IPC_Misc_GetInstIdData (p_ipc_iud_get_inst_id_data->node_inst_id, 
                                     &_computer_id,
                                     &_node_id);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    return;
  }

  // Request user data output
  p_ipc_oud_get_inst_id_data->ipc_computer_id = _computer_id;
  p_ipc_oud_get_inst_id_data->ipc_node_id = _node_id;

  return;
} // IPC_GetInstIdData

//------------------------------------------------------------------------------
// PURPOSE : Process for "Send" request.
//          Send user message to all requested node instances. For each one a 
//          work is queued to be executed by te system thread pool. 
//          Local variables are used to pass execution parameters for each work. 
//          The synchronization between each work queued and the parent is done 
//          using a event signaled by the queued work once started.
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void IPC_Send (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
               WORD * pRequestOutput1, 
               WORD * pRequestOutput2,
               BOOL * pRequestExecEnd)
{
  static TCHAR _function_name[] = _T("IPC_Send");

  BOOL                  _bool_rc;
  IPC_IUD_SEND          * p_ipc_iud_send;
  DWORD                 _target_node_idx;
  DWORD                 _first_target_node_inst_idx;
  DWORD                 _last_target_node_inst_idx;
  DWORD                 _source_node_idx;
  TYPE_TCP_SEND_REQUEST _tcp_send_request;
  
  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;
  * pRequestExecEnd = FALSE;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    return;
  }

  p_ipc_iud_send = (IPC_IUD_SEND *) pApiRequestParams->api_input_user_data_buffer;

  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_ipc_iud_send->control_block, 
                                           sizeof (IPC_IUD_SEND),
                                           _function_name, 
                                           _T("IUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    return;
  }

  // Get node and instances
  _bool_rc = IPC_Misc_GetNodeIdxAndInst (p_ipc_iud_send->target_node_inst_id, 
                                         &_target_node_idx, &_first_target_node_inst_idx, &_last_target_node_inst_idx);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_INVALID_NODE_INST_ID;
    * pRequestExecEnd = TRUE;
    return;
  }

  // Get node and instances
  if ( p_ipc_iud_send->source_node_inst_id != IPC_NODE_UNKNOWN )
  {
    _bool_rc = IPC_Misc_GetNodeIdxAndInst (p_ipc_iud_send->source_node_inst_id, &_source_node_idx);
    if ( !_bool_rc )
    {
      * pRequestOutput1 = IPC_STATUS_INVALID_NODE_INST_ID;
      * pRequestExecEnd = TRUE;
      return;
    }
  }
  else
  {
    _source_node_idx = 0;
  }

  // Check IPC nodes
  _bool_rc = IPC_Misc_CheckNode (p_ipc_iud_send->target_node_inst_id, _target_node_idx, IPC_NODE_REMOTE, pRequestOutput1);
  if ( !_bool_rc )
  {
    * pRequestExecEnd = TRUE;
    return;
  }
  _bool_rc = IPC_Misc_CheckNode (p_ipc_iud_send->source_node_inst_id, _source_node_idx, IPC_NODE_LOCAL_SOURCE, pRequestOutput1);
  if ( !_bool_rc )
  {
    * pRequestExecEnd = TRUE;
    return;
  }

  // Build message to send to destination. Note that format used is the
  // same as used in the reception side
  _tcp_send_request.ipc_oud_receive.source_node_inst_id  = p_ipc_iud_send->source_node_inst_id;
  _tcp_send_request.ipc_oud_receive.msg_cmd              = p_ipc_iud_send->msg_cmd;
  _tcp_send_request.ipc_oud_receive.msg_status           = p_ipc_iud_send->msg_status;
  _tcp_send_request.ipc_oud_receive.msg_id               = p_ipc_iud_send->msg_id;
  _tcp_send_request.ipc_oud_receive.user_dword           = p_ipc_iud_send->user_dword;
  _tcp_send_request.ipc_oud_receive.user_buffer_length   = p_ipc_iud_send->user_buffer_length;
  memcpy (_tcp_send_request.ipc_oud_receive.user_buffer, p_ipc_iud_send->user_buffer, p_ipc_iud_send->user_buffer_length);
  
  // To manage tcp fragmentation, user control block is used to 
  // indicate the actual message length instead of the structure size
  _tcp_send_request.ipc_oud_receive.control_block =   sizeof (_tcp_send_request.ipc_oud_receive)              // Total struct size
                                                    - sizeof (_tcp_send_request.ipc_oud_receive.user_buffer)  // - Variable buffer size 
                                                    + p_ipc_iud_send->user_buffer_length;                     // + Actual user buffer length
  
  // Target node tcp initialization
  _bool_rc = IPC_Tcp_RemoteNodeInit (_target_node_idx);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    return;
  }

  // Send parameters and data
  _tcp_send_request.request_type        = IPC_TCP_SEND_REQ_DATA;
  _tcp_send_request.node_idx            = _target_node_idx;
  _tcp_send_request.first_node_inst_idx = _first_target_node_inst_idx;
  _tcp_send_request.last_node_inst_idx  = _last_target_node_inst_idx;
  _tcp_send_request.api_input_paramas   = pApiRequestParams->api_input_paramas;
  _tcp_send_request.p_api_output_params = pApiRequestParams->p_api_output_params;
  _tcp_send_request.control_block       =   sizeof (_tcp_send_request)
                                          - sizeof (_tcp_send_request.ipc_oud_receive)
                                          + _tcp_send_request.ipc_oud_receive.control_block;

  // Write send request to tcp send node queue
  _bool_rc = IPC_Tcp_WriteSendRequest (_target_node_idx, &_tcp_send_request);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    return;
  }
  
} // IPC_Send

//------------------------------------------------------------------------------
// PURPOSE : Receive timer callbak routine executed after read timer expired
//
//  PARAMS :
//      - INPUT :
//        - Paramter: Data value
//        - TimerLowValue: Timer low value, not used.
//        - TimerHighValue: Timer high value, not used.
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

VOID CALLBACK IPC_TimerReceiveCallback (LPVOID Parameter,      
                                        DWORD  TimerLowValue,   
                                        DWORD  TimerHighValue)  
{
  static TCHAR _function_name[] = _T("IPC_TimerReceiveCallback");
  TCHAR _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  API_INPUT_PARAMS     _api_input_params;
  API_OUTPUT_PARAMS    * p_api_output_params;
  TYPE_RECEIVE_PENDING * p_receive_pending;
  WORD _call_status;

  p_receive_pending = (TYPE_RECEIVE_PENDING *) Parameter;

  // Timer elapsed: receive timeout

  // Cancel receive queue read operation
  _call_status = Queue_CancelRead (p_receive_pending->receive_queue,      // Queue handle
                                   p_receive_pending->queue_read_handle); // Read handle
  if ( _call_status == QUEUE_STATUS_PARAM_ERROR )
  {
    // Read operation cannot be cancelled because read handle does not match
    // current queue read
    // Do nothing
    return;
  }
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_CancelRead"));
  }

  // Save data to notify user
  _api_input_params   = p_receive_pending->api_input_paramas;
  p_api_output_params = p_receive_pending->p_api_output_params;
  
  // Release Receive Pending entry
  IPC_Misc_ReleaseReceivePending (p_receive_pending);
  
  // Notify completion status to user
  Common_API_NotifyRequestStatus (IPC_STATUS_TIMEOUT,
                                  0,
                                  &_api_input_params,
                                  p_api_output_params);
                                     
} // IPC_TimerReceiveCallback

//------------------------------------------------------------------------------
// PURPOSE : Completion routine executed after read operation in local node 
//          receive queue
//
//  PARAMS :
//      - INPUT :
//        - ErrorCode: Completion status code
//        - NumberOfBytesTransfered: Number of bytes transferred
//        - pOverlapped: I/O information buffer
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

VOID CALLBACK IPC_ReadQueueCompletionRoutine (ULONG_PTR pParameter)
{
  static TCHAR _function_name[] = _T("IPC_ReadQueueCompletionRoutine");
  TCHAR _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  API_INPUT_PARAMS     _api_input_params;
  API_OUTPUT_PARAMS    * p_api_output_params;
  TYPE_RECEIVE_PENDING * p_receive_pending;
  BOOL _bool_rc;

  p_receive_pending = (TYPE_RECEIVE_PENDING *) pParameter;

  // Cancel timeout
  _bool_rc = CancelWaitableTimer (p_receive_pending->timer);
  if ( ! _bool_rc )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("CancelWaitableTimer"));
  }

  // Note that receive operation can not be NO_WAIT. Output can be modified
  // without checking request mode (always SYNC or ASYNC)
  
  // Save data to notify user
  _api_input_params   = p_receive_pending->api_input_paramas;
  p_api_output_params = p_receive_pending->p_api_output_params;
  
  // Release Receive Pending entry
  IPC_Misc_ReleaseReceivePending (p_receive_pending);
  
  // Notify completion status to user
  Common_API_NotifyRequestStatus (IPC_STATUS_OK,
                                  0,
                                  &_api_input_params,
                                  p_api_output_params);

} // IPC_ReadQueueCompletionRoutine

//------------------------------------------------------------------------------
// PURPOSE : Process for request "Receive".
//          Reads data from local node receive queue and starts a timer 
//          if necessary.
//          Both operations are completed by the system with a callback.
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//        - pRequestExecEnd: TRUE if request execution is finished, FALSE is it is timeout
//
// RETURNS :
//
//   NOTES :

void IPC_Receive (TYPE_API_REQUEST_PARAMS *pApiRequestParams, 
                  WORD * pRequestOutput1, 
                  WORD * pRequestOutput2,
                  BOOL * pRequestExecEnd)
{
  static TCHAR _function_name[] = _T("IPC_Receive");
  TCHAR _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  TYPE_IPC_NODE         * p_ipc_node;
  TYPE_RECEIVE_PENDING  * p_receive_pending;
  IPC_IUD_RECEIVE       * p_ipc_iud_receive;
  IPC_OUD_RECEIVE       * p_ipc_oud_receive;
  BOOL                  _bool_rc;
  WORD                  _call_status;
  DWORD                 _node_idx;
  DWORD                 _queue_num_items;
  LARGE_INTEGER         _receive_timeout;

  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;
  * pRequestExecEnd = FALSE;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    return;
  }

  p_ipc_iud_receive = (IPC_IUD_RECEIVE *) pApiRequestParams->api_input_user_data_buffer;
  p_ipc_oud_receive = (IPC_OUD_RECEIVE *) pApiRequestParams->p_api_output_user_data;

  // Check control blocks
  _bool_rc = Common_API_CheckControlBlock (p_ipc_iud_receive->control_block, 
                                           sizeof (IPC_IUD_RECEIVE),
                                           _function_name, 
                                           _T("IUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    return;
  }
  _bool_rc = Common_API_CheckControlBlock (p_ipc_oud_receive->control_block, 
                                           sizeof (IPC_OUD_RECEIVE),
                                           _function_name, 
                                           _T("OUDControlBlock"));
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    return;
  }

  // Get node and instances
  _bool_rc = IPC_Misc_GetNodeIdxAndInst (p_ipc_iud_receive->node_inst_id, &_node_idx);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_INVALID_NODE_INST_ID;
    * pRequestExecEnd = TRUE;
    return;
  }
  p_ipc_node = &GLB_IpcNodeTable.ipc_node[_node_idx];

  // Check IPC node
  _bool_rc = IPC_Misc_CheckNode (p_ipc_iud_receive->node_inst_id, _node_idx, IPC_NODE_LOCAL, pRequestOutput1);
  if ( !_bool_rc )
  {
    * pRequestExecEnd = TRUE;
    return;
  }

  // Get number of entries in the queue pending to be read
  _queue_num_items = 0;
  _call_status = Queue_GetNumItems (p_ipc_node->local_node_data.receive_queue,  // Queue
                                    &_queue_num_items);                         // Current number of entries in thr queue
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_GetNumEntries"));
  }
      
  // Special processing for no timeout
  if (   p_ipc_iud_receive->timeout == 0
      && _queue_num_items == 0 )
  {
    // No timeout & no entries in the queue: receive timeout
    * pRequestOutput1 = IPC_STATUS_TIMEOUT;
    * pRequestExecEnd = TRUE;
    return;
  }
  
  // Get a free client buffer
  _bool_rc = IPC_Misc_GetFreeReceivePending (p_ipc_node, &p_receive_pending);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_OVRFLW_RCV_NODE;
    * pRequestExecEnd = TRUE;
    return;
  }

  // Set receive parameters
  p_receive_pending->receive_queue              = p_ipc_node->local_node_data.receive_queue;
  p_receive_pending->api_input_paramas          = pApiRequestParams->api_input_paramas;
  p_receive_pending->p_api_output_params        = pApiRequestParams->p_api_output_params;
  p_receive_pending->p_api_output_user_data     = pApiRequestParams->p_api_output_user_data;

  _call_status = Queue_Read (p_receive_pending->receive_queue,             // Queue
                             NULL,                                         // Notification Event object
                             IPC_ReadQueueCompletionRoutine,               // APC routine
                             p_receive_pending,                            // APC data
                             sizeof (IPC_OUD_RECEIVE),                     // Output buffer size
                             &p_ipc_oud_receive->control_block,            // User Buffer
                             &p_receive_pending->received_buffer_length,   // Actual Read buffer length
                             &p_receive_pending->queue_read_handle);       // Read handle
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_Read"));

    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    IPC_Misc_ReleaseReceivePending (p_receive_pending);
    return;
  }

  // Start timer only if
  //  - timer > 0
  //  - timer != infinite
  //  - timer == 0 and number of entries in the queue = 0.
  // Note that if number of entries in the queue is > 0, the read operation 
  // always will be able to be executed
  if (   p_ipc_iud_receive->timeout != IPC_TIMEOUT_INFINITE
      && _queue_num_items == 0 )
  {
    // User receive timeout
    // Convert milliseconds to nanoseconds
    // Note time is negatiuve because it is a relative timer
    _receive_timeout.QuadPart = p_ipc_iud_receive->timeout * (__int64) (-10000);
  
    // Set timer using the client specified timer
    _bool_rc = SetWaitableTimer (p_receive_pending->timer,  // Handle to timer
                                &_receive_timeout,          // Timer due time
                                0,                          // Periodic timer interval
                                IPC_TimerReceiveCallback,   // Completion routine
                                p_receive_pending,          // Completion routine parameter
                                FALSE);                     // Resume state
    if ( !_bool_rc )
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
      PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("SetWaitableTimer"));
      
      // Receive cannot be cancelled because ReadFileEx is already called
      return;
    }
  }

} // IPC_Receive

//------------------------------------------------------------------------------
// PURPOSE : Sends a special request to the tcp send thread in order to notify
//          user when the send queue is empty (= the special request arrive to
//          the send thread)
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//        - pThreadData: Used to access to send token list
//      - OUTPUT :
//        - pRequestOutput1: request output status
//        - pRequestOutput2: request output status
//
// RETURNS :
//
//   NOTES :

void IPC_WaitSendQueueEmpty (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                             TYPE_API_THREAD_DATA    * pThreadData,
                             WORD * pRequestOutput1, 
                             WORD * pRequestOutput2,
                             BOOL * pRequestExecEnd)
{
  static TCHAR _function_name[] = _T("IPC_WaitSendQueueEmpty");

  TYPE_TCP_SEND_REQUEST         _tcp_send_request;
  TYPE_SEND_TOKEN               * p_send_token;
  BOOL                          _bool_rc;
  DWORD                         _idx_node_list;
  TYPE_NODE_IDX_LIST            _node_idx_list;
  

  * pRequestOutput1 = IPC_STATUS_OK;
  * pRequestOutput2 = 0;
  * pRequestExecEnd = FALSE;

  _bool_rc = Common_API_CheckStatus (_function_name);
  if ( !_bool_rc )
  {
    // Module not initialized
    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    return;
  }

  // Get list of initialized remote nodes to synchronize  
  IPC_Tcp_GetRemoteNodes (&_node_idx_list);
  if ( _node_idx_list.num_nodes == 0 )
  {
    // None to synchronize
    * pRequestOutput1 = IPC_STATUS_OK;
    * pRequestExecEnd = TRUE;
    return;
  }

  // Get send token
  _bool_rc = IPC_Misc_GetFreeSendToken (pThreadData->send_token, &p_send_token);
  if ( !_bool_rc )
  {
    * pRequestOutput1 = IPC_STATUS_ERROR;
    * pRequestExecEnd = TRUE;
    return;
  }

  // Update token counter with number of remote node send queues to check
  EnterCriticalSection (&p_send_token->cs_token_counter);
  p_send_token->token_counter = _node_idx_list.num_nodes;
  LeaveCriticalSection (&p_send_token->cs_token_counter);
  
  // Special send request parameters and data
  _tcp_send_request.request_type        = IPC_TCP_SEND_REQ_WAIT;
  _tcp_send_request.first_node_inst_idx = 0;
  _tcp_send_request.last_node_inst_idx  = 0;
  _tcp_send_request.api_input_paramas   = pApiRequestParams->api_input_paramas;
  _tcp_send_request.p_api_output_params = pApiRequestParams->p_api_output_params;
  memcpy (&_tcp_send_request.ipc_oud_receive, &p_send_token, sizeof (p_send_token));
  _tcp_send_request.control_block       =   sizeof (_tcp_send_request)                  // Total struct size
                                          - sizeof (_tcp_send_request.ipc_oud_receive)  // - Variable buffer size 
                                          + sizeof (p_send_token);                      // + Actual buffer length

  for ( _idx_node_list = 0; _idx_node_list < _node_idx_list.num_nodes; _idx_node_list++ )
  {
    // Write send request to tcp send node queue
     _tcp_send_request.node_idx = _node_idx_list.node_idx[_idx_node_list];
    _bool_rc = IPC_Tcp_WriteSendRequest (_node_idx_list.node_idx[_idx_node_list], &_tcp_send_request);
    if ( !_bool_rc )
    {
      * pRequestOutput1 = IPC_STATUS_ERROR;
      * pRequestExecEnd = TRUE;
      return;
    }
  } // for
  
} // IPC_WaitSendQueueEmpty

//------------------------------------------------------------------------------
// PURPOSE : Common processing of user requests
//
//  PARAMS :
//      - INPUT :
//        - ApiRequestParams
//      - OUTPUT : None
//
// RETURNS :
//
//   NOTES :

void IPC_ProcessRequest (TYPE_API_REQUEST_PARAMS * pApiRequestParams, 
                         TYPE_API_THREAD_DATA    * pThreadData)
{
  static TCHAR  _function_name[] = _T("IPC_ProcessRequest");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  WORD   _request_output_1 = IPC_STATUS_ERROR;
  WORD   _request_output_2 = 0;
  BOOL   _request_exec_end = TRUE;

  switch ( pApiRequestParams->api_input_paramas.function_code )
  {
    case IPC_CODE_MODULE_INIT:
      IPC_ModuleInit (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;
    
    case IPC_CODE_MODULE_STOP:
      IPC_ModuleStop (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;

    case IPC_CODE_INIT_LOCAL_NODE:
      IPC_LocalNodeInit (pApiRequestParams, pThreadData, &_request_output_1, &_request_output_2);
    break;

    case IPC_CODE_PURGE_NODE:
      IPC_PurgeNode (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;

    case IPC_CODE_GET_NODE_INST_ID:
      IPC_GetNodeInstId (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;

    // AJQ, 09-DEC-2002
    case IPC_CODE_GET_NODE_INST_ID_LIST:
      IPC_GetNodeInstIdList (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;

    // ACC on 14-MAR-2005
    case IPC_CODE_GET_INST_ID_DATA:
      IPC_GetInstIdData (pApiRequestParams, &_request_output_1, &_request_output_2);
    break;

    case IPC_CODE_SEND:
      IPC_Send (pApiRequestParams, &_request_output_1, &_request_output_2, &_request_exec_end);
    break;

    case IPC_CODE_RECEIVE:
      IPC_Receive (pApiRequestParams, &_request_output_1, &_request_output_2, &_request_exec_end);
    break;
    
    case IPC_CODE_WAIT_SEND_QUEUE_EMPTY:
      IPC_WaitSendQueueEmpty (pApiRequestParams, pThreadData, &_request_output_1, &_request_output_2, &_request_exec_end);
    break;

    default:
      // Unknown function code
      _stprintf (_aux_tchar_1, _T("%hu"), pApiRequestParams->api_input_paramas.function_code);
      PrivateLog (6, MODULE_NAME, _function_name, _T("FunctionCode"), _aux_tchar_1);

      _request_output_1 = IPC_STATUS_ERROR;
      _request_output_2 = 0;
    break;

  } // switch

  if ( _request_exec_end )
  {

    // Notify completion status to user
    Common_API_NotifyRequestStatus (_request_output_1,
                                    _request_output_2,
                                    &pApiRequestParams->api_input_paramas,
                                    pApiRequestParams->p_api_output_params);
  }

} // IPC_ProcessRequest

//------------------------------------------------------------------------------
// PURPOSE : API thread to handle user requests.
//
//  PARAMS :
//      - INPUT :
//        - ThreadParameter
//      - OUTPUT : None
//
// RETURNS :
//
//   NOTES :

DWORD WINAPI Ipc_APIThread (LPVOID ThreadParameter)
{
  static TCHAR  _function_name[] = _T("Ipc_APIThread");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  WORD   _call_status;
  DWORD  _wait_status;
  DWORD  _request_buffer_length;
  TYPE_API_REQUEST_PARAMS _api_request_params;
  TYPE_API_THREAD_DATA    * p_thread_data;

  p_thread_data = (TYPE_API_THREAD_DATA *) ThreadParameter;

  // First call to read queue
  _call_status = Queue_Read (p_thread_data->request_queue, // Queue
                             p_thread_data->queue_event,   // Notification Event object
                             NULL,                         // APC routine
                             NULL,                         // APC data
                             IPC_QUEUE_ITEM_SIZE,          // Output buffer size
                             &_api_request_params,         // User Buffer
                             &_request_buffer_length,      // Actual Read buffer length
                             NULL);                        // Read handle
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Critical error: Thread end
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_Read"));

    return 0;
  }

  // Infinite loop to handle requests and events
  while ( TRUE ) 
  {
    // Wait with using the private event
    _wait_status = WaitForSingleObjectEx (p_thread_data->queue_event,  // Handle
                                          INFINITE,                    // Milliseconds
                                          TRUE);                       // Alertable
    switch ( _wait_status )
    {
      case WAIT_IO_COMPLETION:
        // Completion routine
      break;
      
      case WAIT_OBJECT_0:
        // Request received
        IPC_ProcessRequest (&_api_request_params, p_thread_data);

        // Call again to read snd queue
        _call_status = Queue_Read (p_thread_data->request_queue, // Queue
                                   p_thread_data->queue_event,   // Notification Event object
                                   NULL,                         // APC routine
                                   NULL,                         // APC data
                                   IPC_QUEUE_ITEM_SIZE,          // Output buffer size
                                   &_api_request_params,         // User Buffer
                                   &_request_buffer_length,      // Actual Read buffer length
                                   NULL);                        // Read handle
        if ( _call_status != QUEUE_STATUS_OK )
        {
          // Critical error: Thread end
          // Logger message
          _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
          PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_Read"));

          return 0;
        }
      break;
      
      default:
        // Error
        // Logger message
        _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
        PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("WaitForSingleObjectsEx"));
   
        return 0;

      break;
      
    } // switch
      
  } // while

  return 0;

} // Ipc_APIThread

//------------------------------------------------------------------------------
// PURPOSE : Init API thread data and resources (events and queues).
//
//  PARAMS :
//      - INPUT :
//        - pApiThreadData
//      - OUTPUT : None
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//
//   NOTES :

BOOL Ipc_InitApiThreadData (TYPE_API_THREAD_DATA * pApiThreadData)
{
  static TCHAR _function_name[] = _T("Ipc_Misc_InitApiThreadData");
  TCHAR  _aux_tchar_1[IPC_AUX_STRING_LENGTH];

  WORD   _call_status;
  HANDLE _event;

  // Queue event
  _event = CreateEvent (NULL,  // Event attributes   
                        FALSE, // ManualReset
                        FALSE, // InitialState
                        NULL); // Name
  if ( _event == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("CreateEvent"));

    return FALSE;
  }

  // Set Queue event
  pApiThreadData->queue_event = _event;
  
  // Queue
  _call_status = Queue_CreateQueue (IPC_QUEUE_NUMBER_OF_ITEMS * IPC_QUEUE_ITEM_SIZE,
                                    &pApiThreadData->request_queue);
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    PrivateLog (2, MODULE_NAME, _function_name, _aux_tchar_1, _T("Queue_CreateQueue"));

    return FALSE;
  }
  
  return TRUE;

} // Ipc_InitApiThreadData

//------------------------------------------------------------------------------
// PURPOSE : Checks API execution mode depending on function code
//
//  PARAMS :
//      - INPUT :
//        - pApiInputParams
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: Execution Mode ok
//  - FALSE: Execution Mode error
//
//   NOTES :

BOOL IPC_API_CheckMode (const API_INPUT_PARAMS * pApiInputParams)
{
  // Check API mode for some operations
  if (   pApiInputParams->mode == API_MODE_NO_WAIT 
      && (   pApiInputParams->function_code == IPC_CODE_RECEIVE
          || pApiInputParams->function_code == IPC_CODE_WAIT_SEND_QUEUE_EMPTY
          || pApiInputParams->function_code == IPC_CODE_INIT_LOCAL_NODE
          || pApiInputParams->function_code == IPC_CODE_GET_NODE_INST_ID 
          || pApiInputParams->function_code == IPC_CODE_GET_INST_ID_DATA ) )
  {
    return FALSE;
  }
  
  return TRUE;

} // IPC_API_CheckMode

//------------------------------------------------------------------------------
// PURPOSE : Select API queue depengin on function code
//
//  PARAMS :
//      - INPUT :
//        - pApiInputParams
//      - OUTPUT :
//
// RETURNS : Queue handle
//
//   NOTES :

QUEUE_HANDLE IPC_API_SelectQueue (const API_INPUT_PARAMS * pApiInputParams)
{
  QUEUE_HANDLE _request_queue;

  // Select queue depending on request
  switch ( pApiInputParams->function_code )
  {
    case IPC_CODE_SEND:
    case IPC_CODE_WAIT_SEND_QUEUE_EMPTY:
      _request_queue = GLB_ApiThreads.send_thread_data.request_queue;
    break;
    
    case IPC_CODE_RECEIVE:
    case IPC_CODE_PURGE_NODE:
      _request_queue = GLB_ApiThreads.receive_thread_data.request_queue;
    break;
    
    case IPC_CODE_MODULE_INIT:
    case IPC_CODE_MODULE_STOP:
    case IPC_CODE_INIT_LOCAL_NODE:
    case IPC_CODE_GET_NODE_INST_ID:
    case IPC_CODE_GET_INST_ID_DATA:
    default:
      _request_queue = GLB_ApiThreads.config_thread_data.request_queue;
    break;
    
  } // switch
  
  return _request_queue;

} // IPC_API_SelectQueue

//------------------------------------------------------------------------------
// PURPOSE : Execute a "Module Init" request
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

void IPC_ExecuteModuleInit (void)
{
  WORD  _module_init_status_1;
  WORD  _module_init_status_2;

  // Try to initialize
  IPC_ModuleInit (NULL, &_module_init_status_1, &_module_init_status_2);

} // IPC_ExecuteModuleInit

//-----------------------------------------------------------------------------
// PURPOSE : DLL standard main function.
//
//  PARAMS :
//     - INPUT : None
//
//     - OUTPUT : None
//
//
// RETURNS :
//     - TRUE: OK
//     - FALSE: Force dll unload
//
// NOTES :

BOOL APIENTRY DllMain (HANDLE Module, 
                       DWORD  ReasonForCall, 
                       LPVOID Reserved)
{
  BOOL   _bool_rc;
  DWORD  _idx_node, _idx_instance;
  TYPE_IPC_NODE * p_ipc_node;

  switch ( ReasonForCall )
  {
    case DLL_PROCESS_ATTACH:

      // DLL load action
      memset (&GLB_API, 0, sizeof (GLB_API));
      memset (&GLB_ApiThreads, 0, sizeof (GLB_ApiThreadsData));
      memset (&GLB_IpcNodeTable, 0, sizeof (GLB_IpcNodeTable));
      InitializeCriticalSection (&GLB_IpcNodeTable.init_ipc_node_table_cs);
      
      // Common API settings
      GLB_API.p_api_name        = MODULE_NAME;
      GLB_API.max_iud_size      = IPC_MAX_IUD_SIZE;
      GLB_API.uses_private_log  = TRUE;
      GLB_API.p_logger          = NULL;
      GLB_API.p_private_log     = PrivateLog;
      GLB_API.p_check_mode      = IPC_API_CheckMode;
      GLB_API.p_select_queue    = IPC_API_SelectQueue;
      GLB_API.p_try_init        = IPC_ExecuteModuleInit;
      
      // Set instance initial data
      for ( _idx_node = 0; _idx_node < IPC_MAX_NODES; _idx_node++ )
      {
        p_ipc_node = &GLB_IpcNodeTable.ipc_node[_idx_node];
        for (_idx_instance = 0; _idx_instance < IPC_MAX_INST_NODE; _idx_instance++)
        {
          p_ipc_node->instances_data.instance_entry[_idx_instance].in_use        = FALSE;
          p_ipc_node->instances_data.instance_entry[_idx_instance].node_socket   = NULL;
          p_ipc_node->instances_data.instance_entry[_idx_instance].socket_status = IPC_SOCKET_NOT_CONNECTED;
        }
      }
      
      // API Threads
      _bool_rc = Ipc_InitApiThreadData (&GLB_ApiThreads.config_thread_data);
      if ( _bool_rc )
      {
        _bool_rc = Common_API_StartThread (Ipc_APIThread,                       // Thread routine
                                           &GLB_ApiThreads.config_thread_data,  // Thread parameter
                                           NULL,                                // Start notify event
                                           _T("Ipc_APIThread.Cfg"));
      }
      if ( _bool_rc )
      {
        _bool_rc = Ipc_InitApiThreadData (&GLB_ApiThreads.receive_thread_data);
      }
      if ( _bool_rc )
      {
        _bool_rc = Common_API_StartThread (Ipc_APIThread,                       // Thread routine
                                           &GLB_ApiThreads.receive_thread_data, // Thread parameter
                                           NULL,                                // Start notify event
                                           _T("Ipc_APIThread.Rcv"));
      }
      if ( _bool_rc )
      {
        _bool_rc = Ipc_InitApiThreadData (&GLB_ApiThreads.send_thread_data);
      }
      if ( _bool_rc )
      {
        _bool_rc = Common_API_StartThread (Ipc_APIThread,                       // Thread routine
                                           &GLB_ApiThreads.send_thread_data,    // Thread parameter
                                           NULL,                                // Start notify event
                                           _T("Ipc_APIThread.Snd"));
      }
      if ( !_bool_rc )
      {
        // Critical error
        // Force to unload DLL
        return FALSE;
      }

    break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
      // Empty
    break;

  } // switch

  return TRUE;

} // DllMain
