//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : IPC_APIInternals.h
//   DESCRIPTION : Constants, types, variables definitions and prototypes
//                internals for the IPC_API
//        AUTHOR : Xavier Ib��ez
// CREATION DATE : 26-MAR-2002
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 26-MAR-2002 XID    Initial draft.
//
//------------------------------------------------------------------------------

#ifndef __IPC_API_INTERNALS_H
#define __IPC_API_INTERNALS_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------

#define MODULE_NAME             _T("IPC_API")

#define IPC_AUX_STRING_LENGTH   256

#define IPC_MAX_IUD_SIZE        sizeof (IPC_IUD_SEND)     // Maximum user data size

#define IPC_MAX_NODES           300
#define IPC_MAX_IDX_NODES       1000                // Node ID between 0 and 999
#define IPC_MAX_INST_NODE       IPC_MAX_INSTANCES_PER_NODE
#define IPC_MAX_REQ_ENTRY_NODE  10
#define IPC_MAX_EVENT_LIST      MAXIMUM_WAIT_OBJECTS

// API queue size and number of items
#define IPC_QUEUE_ITEM_SIZE          sizeof (TYPE_API_REQUEST_PARAMS) // bytes
#define IPC_QUEUE_NUMBER_OF_ITEMS    30 // items

// Tcp send queue
#define IPC_TCP_SEND_QUEUE_ITEM_SIZE        sizeof (TYPE_TCP_SEND_REQUEST)  // bytes
#define IPC_TCP_SEND_QUEUE_NUMBER_OF_ITEMS  4 // items

typedef enum
{
  IPC_SOCKET_NOT_CONNECTED = 0,
  IPC_SOCKET_CONNECTED     = 1

} ENUM_IPC_SOCKET_STATUS;

#define IPC_WSA_BUFFER_SIZE                 sizeof (IPC_OUD_RECEIVE)
#define IPC_MAX_LOCAL_NODE_CLIENTS          64
#define IPC_MAX_LOCAL_NODE_RECEIVE_PENDINGS 1
#define IPC_MAX_SEND_TOKEN                  16

#define IPC_MASK_NODE_IDX                   0x00FFFFFF
#define IPC_MASK_NODE_INST                  0xFF000000
#define IPC_ANY_NODE_INST                   0xFF
#define IPC_SHIFT_NODE_INST                 24

#define IPC_NODE_LOCAL_SOURCE               1
#define IPC_NODE_LOCAL                      2
#define IPC_NODE_REMOTE                     3

#define IPC_TCP_SEND_REQ_DATA               0
#define IPC_TCP_SEND_REQ_WAIT               1

#define MAX_BUFFER_LINES                    20000    // AJQ, 20 thousand lines
#define IPC_CONF_LINE_BUFFER_SIZE           82       // AJQ, 80 cols + \n + \0
#define IPC_MAX_DUAL_NODES                  (IPC_MAX_NODES / 2)
#define IPC_BASE_WDOG                       500
#define IPC_NUM_INSTANCE_FIELDS             6


// AJQ, 27-MAY-2003 Allow the Loopback IP
#define LOOPBACK_IP_ADDR                    "127.0.0.1" // Must be char, not TCHAR

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------

// User parameters to execute an IPC request
typedef struct
{
  API_INPUT_PARAMS      api_input_paramas;
  API_OUTPUT_PARAMS     * p_api_output_params;
  API_OUTPUT_USER_DATA  * p_api_output_user_data;
  BYTE                  api_input_user_data_buffer[IPC_MAX_IUD_SIZE];

} TYPE_API_REQUEST_PARAMS;

typedef struct
{
  DWORD num_nodes;
  DWORD node_idx[IPC_MAX_NODES];

} TYPE_NODE_IDX_LIST;

// IPC local node data
typedef struct
{
  BOOL            in_use;
  SOCKET          socket_c;
  QUEUE_HANDLE    receive_queue;
  DWORD           wsa_received_bytes;
  WSAOVERLAPPED   wsa_overlapped;
  DWORD           wsa_buffer_size;
  DWORD           wsa_buffer_index;
  BYTE            * p_wsa_buffer;

} TYPE_TCP_CLIENT_BUFFER;

typedef struct
{
  BOOL                  in_use;
  HANDLE                timer;
  QUEUE_HANDLE          receive_queue;
  DWORD                 queue_read_handle;
  DWORD                 received_buffer_length;
  API_INPUT_PARAMS       api_input_paramas;
  API_OUTPUT_PARAMS     * p_api_output_params;
  API_OUTPUT_USER_DATA  * p_api_output_user_data;

} TYPE_RECEIVE_PENDING;

typedef struct
{
  BOOL                  in_use;
  BOOL                  init;
  CRITICAL_SECTION      cs_token_counter;
  DWORD                 token_counter;

} TYPE_SEND_TOKEN;

typedef struct
{
  BOOL    local_node;      // TRUE for local nodes
  BOOL    node_init;       // TRUE if tcp service is ready to accept new connections
  DWORD   ip_addr;
  WORD    tcp_port;
  SOCKET  node_socket;
  HANDLE  tcp_accept_event;
  QUEUE_HANDLE receive_queue;
  TYPE_TCP_CLIENT_BUFFER tcp_client_buffer[IPC_MAX_LOCAL_NODE_CLIENTS];
  TYPE_RECEIVE_PENDING receive_pending[IPC_MAX_LOCAL_NODE_RECEIVE_PENDINGS];

} TYPE_LOCAL_NODE_DATA;

// IPC instance node data
typedef struct
{
  BOOL  in_use;
  WORD  cfg_inst_id;
  BOOL  local_instance; // TRUE if instance IP address is the same as one of the local mahcine IP address
  DWORD ip_addr;
  WORD  tcp_port;

  ENUM_IPC_SOCKET_STATUS socket_status;
  DWORD                  last_status_time;  // msec
  SOCKET                 node_socket;

} TYPE_INSTANCE_ENTRY;

typedef struct
{
  BOOL                tcp_send_init;      // TRUE if tcp remote node has been initialized
  HANDLE              tcp_send_queue_event;
  QUEUE_HANDLE        tcp_send_queue; 
  
  DWORD               num_instances;
  TYPE_INSTANCE_ENTRY instance_entry[IPC_MAX_INST_NODE];

} TYPE_INSTANCES_DATA;

// IPC node data
typedef struct
{
  TCHAR                 node_name[IPC_NODE_NAME_LENGTH];
  DWORD                 cfg_node_id;
  TYPE_INSTANCES_DATA   instances_data;
  TYPE_LOCAL_NODE_DATA  local_node_data;
  
} TYPE_IPC_NODE;

// IPC node index data: relation between configuration node_id and index to ipc_node table
typedef struct
{
  BOOL  in_use;
  DWORD node_idx;

} TYPE_IPC_NODE_INDEX;

// List of configured IPC nodes
typedef struct
{
  CRITICAL_SECTION    init_ipc_node_table_cs;
  DWORD               num_nodes;              // Number of configured IPC nodes in ipc_node and ipc_node_index tables
  TYPE_IPC_NODE       ipc_node[IPC_MAX_NODES];
  TYPE_IPC_NODE_INDEX ipc_node_index[IPC_MAX_IDX_NODES];

} TYPE_IPC_NODE_TABLE;

// Tcp (send or receive) thread data
typedef struct
{
    HANDLE          start_notify_event;
    HANDLE          tcp_accept_event;
    DWORD           node_idx;

} TYPE_TCP_THREAD_DATA;

// Api thread data
typedef struct
{
    HANDLE          queue_event;
    QUEUE_HANDLE    request_queue;        // Thread queue to receive user requests
    DWORD           num_executing_req;    // Number of pending operations. Used to manage number of threads
                                          // processing some type of requests (for example send requests).
    TYPE_SEND_TOKEN send_token[IPC_MAX_SEND_TOKEN];
    
} TYPE_API_THREAD_DATA;

typedef struct
{
  TYPE_API_THREAD_DATA  config_thread_data;
  TYPE_API_THREAD_DATA  receive_thread_data;
  TYPE_API_THREAD_DATA  send_thread_data;
                  
} TYPE_API_THREADS;

// Send parameters and data
typedef struct
{
  CONTROL_BLOCK     control_block;
  WORD              request_type;         // IPC_TCP_SEND_REQ_DATA, IPC_TCP_SEND_REQ_WAIT
  DWORD             node_idx;
  DWORD             first_node_inst_idx;
  DWORD             last_node_inst_idx;
  API_INPUT_PARAMS   api_input_paramas;
  API_OUTPUT_PARAMS * p_api_output_params;
  IPC_OUD_RECEIVE   ipc_oud_receive;
  
} TYPE_TCP_SEND_REQUEST;

typedef struct
{
  TCHAR line[IPC_CONF_LINE_BUFFER_SIZE];

} TYPE_LINE;

typedef struct
{
  DWORD         num_lines;
  DWORD         current_line;
  TYPE_LINE     buffer[MAX_BUFFER_LINES];

} TYPE_IPC_BUFFER;

typedef struct
{
  FILE                * p_ipc_file;
  WORD                current_state;
  TCHAR               current_line_buffer [IPC_CONF_LINE_BUFFER_SIZE];
  WORD                current_line_number;
  WORD                current_line_type;
  BOOL                configuration_result;
  TYPE_IPC_NODE       private_ipc_node;
  TYPE_IPC_NODE_TABLE * p_ipc_node_table;
  DWORD               source;
  TYPE_IPC_BUFFER     * p_ipc_buffer;

} TYPE_STATE_PROCESS;

// IPC instance node data
typedef struct
{ 
  DWORD  computer_id;
  TCHAR  computer_name [IPC_COMPUTER_NAME_LEN  + 1];
  TCHAR  ip_addr [IP_MAX_LEN + 1];
  DWORD  tcp_port;

} TYPE_INSTANCE_ENTRY_DB;

typedef struct
{
  DWORD                   node_def_id;  // Definition Id
  DWORD                   node_cfg_id;  // API Internal Id
  TCHAR                   node_name [IPC_LEN_NODE_NAME + 1];
  DWORD                   num_instances;
  TYPE_INSTANCE_ENTRY_DB  instance_entry [IPC_MAX_INST];

} TYPE_IPC_NODE_DB;

typedef struct
{
  TYPE_IPC_NODE_DB  node;
  TYPE_IPC_NODE_DB  wdog;

} TYPE_IPC_DUAL_NODE;

typedef struct
{
  DWORD               num_nodes;
  TYPE_IPC_DUAL_NODE  dual_node [IPC_MAX_DUAL_NODES];

} TYPE_IPC_DUAL_NODE_LIST;

typedef struct
{
  WSAOVERLAPPED   wsa_overlapped;
  WSABUF          wsa_buffer;
  BYTE            user_buffer[sizeof (IPC_OUD_RECEIVE)];

} TYPE_TCP_SEND_BUFFER;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

// Local nodes
extern TYPE_IPC_NODE_TABLE   GLB_IpcNodeTable;
extern TYPE_API_THREADS      GLB_ApiThreadsData;

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

// Tcp module

BOOL          IPC_Tcp_Init (void);

BOOL          IPC_Tcp_LocalNodeInit (DWORD NodeIdx, 
                                     TYPE_API_THREAD_DATA * pThreadData);
                                             
BOOL          IPC_Tcp_FindLocalAddr (DWORD IpAddr);

BOOL          IPC_Tcp_CheckRemoteNodeAvail (DWORD NodeIdx,
                                            DWORD NodeInstIdx);
                                             
BOOL          IPC_Tcp_RemoteNodeInit (DWORD NodeIdx);

void          IPC_Tcp_GetRemoteNodes (TYPE_NODE_IDX_LIST * pNodeIdxList);

BOOL          IPC_Tcp_WriteSendRequest (DWORD NodeIdx,
                                        TYPE_TCP_SEND_REQUEST * pTcpSendRequest);

// Misc module

BOOL          IPC_Misc_GetFreeReceivePending (TYPE_IPC_NODE * pIpcNode, 
                                              TYPE_RECEIVE_PENDING ** pReceivePending);
                                               
void          IPC_Misc_ReleaseReceivePending (TYPE_RECEIVE_PENDING * pReceivePending);

BOOL          IPC_Misc_GetFreeSendToken (TYPE_SEND_TOKEN *  pSendTokenList,
                                         TYPE_SEND_TOKEN ** pSendToken);

void          IPC_Misc_ReleaseSendToken (TYPE_SEND_TOKEN * pSendToken);

BOOL          IPC_Misc_CheckControlBlock (DWORD ReceivedCB, 
                                          DWORD RequestedCB, 
                                          TCHAR * pFunctionName, 
                                          TCHAR * pStructName);
                                               
BOOL          IPC_Misc_FindNodeInstId (TCHAR *pNodeName, 
                                       DWORD *pNodeInstId,
                                       BOOL  LocalInstance);

BOOL IPC_Misc_FindNodeInstIdList (TCHAR * pNodeName,
                                  DWORD * pNumInstances,
                                  DWORD * pCfgInstId,
                                  DWORD * pNodeInstId,
                                  BOOL  * pLocalInstance);

BOOL          IPC_Misc_CheckNode (DWORD NodeInstId, 
                                  DWORD NodeIdx,
                                  WORD  NodeType, 
                                  WORD  * pRequestOutput1);
                                               
BOOL          IPC_Misc_GetNodeIdxAndInst (DWORD NodeInstId, 
                                          DWORD * pNodeIndex, 
                                          DWORD * pFirstNodeInstIdx = NULL, 
                                          DWORD * pLastNodeInstIdx = NULL);
                                               
BOOL          IPC_Misc_GetNodeIdx (DWORD NodeInstId, 
                                   DWORD * pNodeIndex, 
                                   BOOL  * pNewNode);

BOOL          IPC_Misc_GetInstIdData (DWORD NodeInstId,
                                      WORD  * pComputerId,
                                      WORD  * pNodeId);

// Conf module

BOOL        IPC_Conf_ReadConfig (TYPE_IPC_NODE_TABLE * pIpcNodeTable);

void        IPC_Conf_NodeToBuffer (TYPE_IPC_BUFFER * pBuffer, TYPE_IPC_NODE_DB   * pNode);

void        IPC_Conf_NodeToDualList (TYPE_IPC_DUAL_NODE_LIST * pDualList, 
                                     TYPE_IPC_NODE_DB        * pNode,
                                     TYPE_IPC_NODE_DB        * pWatchdog);

BOOL       IPC_LoadConfigFromDataBase (TYPE_IPC_BUFFER          * pBuffer, 
                                       TYPE_IPC_DUAL_NODE_LIST  * pDualList);

#endif // __IPC_API_INTERNALS_H
