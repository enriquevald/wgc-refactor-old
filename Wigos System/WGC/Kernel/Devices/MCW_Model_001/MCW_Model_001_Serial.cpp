//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME : MCW_Model_001_Serial.cpp
//
//   DESCRIPTION : Functions and Methods to handle serial communications.
//
//        AUTHOR : Xavier Ib��ez
//
// CREATION DATE : 23-SEP-2008
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-SEP-2008 XID    First release.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API
#define INCLUDE_MCW_API

#include "CommonDef.h"

#include "MCW_Model_001.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

BOOL Serial_UARTConfigPort (void);
BOOL Serial_UARTClosePort (void);
BOOL Serial_UARTReadPort (BYTE * pBuffer, DWORD BufferLength, DWORD * pDataLengthRead);
BOOL Serial_UARTWritePort (BYTE * pBuffer, DWORD BufferSize);

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Configures serial port.
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//      
//   NOTES :
//
BOOL Serial_ConfigPort (WORD PortType, WORD PortNumber)
{
  GLB_ModuleData.port_type   = PortType;
  GLB_ModuleData.port_number = PortNumber;

  switch ( GLB_ModuleData.port_type )
  {
    case COMMON_PORT_TYPE_SERIAL:
      return Serial_UARTConfigPort ();
    break;
    
    default:
      return FALSE;
    break;
    
  } // switch

  return FALSE;

} // Serial_ConfigPort

//------------------------------------------------------------------------------
// PURPOSE : Configures serial port.
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//      
//   NOTES :
//
BOOL Serial_ClosePort (void)
{
  switch ( GLB_ModuleData.port_type )
  {
    case COMMON_PORT_TYPE_SERIAL:
      return Serial_UARTClosePort ();
    break;
    
    default:
      return FALSE;
    break;
    
  } // switch

  return FALSE;

} // Serial_ClosePort

//------------------------------------------------------------------------------
// PURPOSE : Write a buffer of bytes to the serial port.
//
//  PARAMS :
//      - INPUT :
//          - pBuffer
//          - BufferSize
//
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//      
//   NOTES :
//
BOOL Serial_WritePort (BYTE * pBuffer, DWORD BufferSize)
{
  switch ( GLB_ModuleData.port_type )
  {
    case COMMON_PORT_TYPE_SERIAL:
      return Serial_UARTWritePort (pBuffer, BufferSize);
    break;
    
    default:
      return FALSE;
    break;
    
  } // switch

  return FALSE;

} // Serial_WritePort

//------------------------------------------------------------------------------
// PURPOSE : Reads data from serial port.
//
//  PARAMS :
//      - INPUT :
//          - BufferLength
//
//      - OUTPUT :
//          - pBuffer
//          - pDataLengthRead
//
// RETURNS :
//  - TRUE
//  - FALSE
//      
//   NOTES :
//
BOOL Serial_ReadPort (BYTE * pBuffer, DWORD BufferLength, DWORD * pDataLengthRead)
{
  switch ( GLB_ModuleData.port_type )
  {
    case COMMON_PORT_TYPE_SERIAL:
      return Serial_UARTReadPort (pBuffer, BufferLength, pDataLengthRead);
    break;
    
    default:
      return FALSE;
    break;
    
  } // switch

  return FALSE;

} // Serial_ReadPort

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Reads data from serial port in a local UART.
//
//  PARAMS :
//      - INPUT :
//          - BufferLength
//
//      - OUTPUT :
//          - pBuffer
//          - pDataLengthRead
//
// RETURNS :
//  - TRUE
//  - FALSE
//      
//   NOTES :
//
BOOL Serial_UARTReadPort (BYTE * pBuffer, DWORD BufferLength, DWORD * pDataLengthRead)
{
  TCHAR                 _function_name[] = _T("Serial_UARTReadPort");
  TYPE_LOGGER_PARAM     _log_param1;

  DWORD         _length_to_read;
  DWORD         _length_read;
  OVERLAPPED    _overlapped;
  BOOL          _bool_rc;
  DWORD         _wait_status;
  static HANDLE _wait_event = NULL;

  // Create event for overlapped operation
  if ( _wait_event == NULL )
  {
    _wait_event = CreateEvent (NULL,  // Event attributes
                                TRUE,  // ManualReset
                                FALSE, // InitialState
                                NULL); // Name
    if ( _wait_event == NULL )
    {
      // Logger message
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      Common_Logger (4, _T("CreateEvent"), _log_param1, _T(""));

      return FALSE;

    }
  }
  
  // Set overlapped wait event  
  _overlapped.hEvent     = _wait_event;
  _overlapped.Offset     = 0;
  _overlapped.OffsetHigh = 0;

  // Reset event 
  ResetEvent (_wait_event);

  _length_to_read = BufferLength;
  _bool_rc = ReadFile (GLB_ModuleData.port_handle,  // File                  
                       pBuffer,                     // Buffer                
                       _length_to_read,             // NumberOfBytesToRead   
                       &_length_read,               // NumberOfBytesRead     
                       &_overlapped);               // Overlapped
  if (   !_bool_rc
      && GetLastError () != ERROR_IO_PENDING )
  {
    return FALSE;
  }

  // Wait for overlapped operation completion
  _wait_status = WaitForSingleObject (_wait_event, // Handle       
                                      INFINITE);   // Milliseconds  
  switch ( _wait_status )
  {
    case WAIT_OBJECT_0:
      _bool_rc = GetOverlappedResult (GLB_ModuleData.port_handle, 
                                      &_overlapped, 
                                      &_length_read, 
                                      FALSE);

      // Check number of bytes written
      if ( !_bool_rc )
      {
        return FALSE;
      }
      * pDataLengthRead = _length_read;

    break;

    case WAIT_FAILED:
    default:

      // Logger message
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      Common_Logger (4, _T("WaitForSingleObject"), _log_param1, _T(""));

      return FALSE;

    break;
  } // switch

  return TRUE; 

} // Serial_UARTReadPort

//------------------------------------------------------------------------------
// PURPOSE : Write a buffer of bytes to the serial port in a local UART.
//
//  PARAMS :
//      - INPUT :
//          - pBuffer
//          - BufferSize
//
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//      
//   NOTES :
//    Write action is executed in exclusive mode using a critical section. No parallel
//    write operation can be performed.
//    Overlapped mode is used for com port read and write operations, because they are concurrents
//
BOOL Serial_UARTWritePort (BYTE * pBuffer, DWORD BufferSize)
{
  TCHAR             _function_name[] = _T("Serial_UARTWritePort");
  TYPE_LOGGER_PARAM _log_param1;
  DWORD             _length_to_send;
  DWORD             _length_sent;
  OVERLAPPED        _overlapped;
  BOOL              _bool_rc;
  DWORD             _wait_status;
  static HANDLE     _wait_event = NULL;

  // Create event for overlapped operation
  if ( _wait_event == NULL )
  {
    _wait_event = CreateEvent (NULL,  // Event attributes
                                TRUE,  // ManualReset
                                FALSE, // InitialState
                                NULL); // Name
    if ( _wait_event == NULL )
    {
      // Logger message
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      Common_Logger (4, _T("CreateEvent"), _log_param1, _T(""));

      return FALSE;

    }
  }
  
  // Set overlapped wait event  
  _overlapped.hEvent     = _wait_event;
  _overlapped.Offset     = 0;
  _overlapped.OffsetHigh = 0;

  // Reset event 
  ResetEvent (_wait_event);

  _length_to_send = BufferSize;
  _bool_rc        = WriteFile (GLB_ModuleData.port_handle,  // File                 
                               pBuffer,                     // Buffer               
                               _length_to_send,             // NumberOfBytesToWrite 
                               &_length_sent,               // NumberOfBytesWritten 
                               &_overlapped);               // Overlapped           
  if (   !_bool_rc
      && GetLastError () != ERROR_IO_PENDING )
  {
    return FALSE;
  }

  // Wait for overlapped operation completion
  _wait_status = WaitForSingleObject (_wait_event, /* Handle       */ 
                                      INFINITE);   /* Milliseconds */ 
  switch ( _wait_status )
  {
    case WAIT_OBJECT_0:
      _bool_rc = GetOverlappedResult (GLB_ModuleData.port_handle, 
                                      &_overlapped, 
                                      &_length_sent, 
                                      FALSE);

      // Check number of bytes written
      if (  !_bool_rc 
          || _length_sent != _length_to_send )
      {
        return FALSE;
      }
    break;

    case WAIT_FAILED:
    default:

      // Logger message
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      Common_Logger (4, _T("WaitForSingleObject"), _log_param1, _T(""));

      return FALSE;

    break;
  } // switch

  return TRUE; 
 
} // Serial_UARTWritePort

//------------------------------------------------------------------------------
// PURPOSE : Configures serial port in local UART.
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//      
//   NOTES :
//
BOOL Serial_UARTConfigPort (void)
{
  TCHAR               _function_name[] = _T("Serial_UARTConfigPort");
  TYPE_LOGGER_PARAM   _log_param1;
  HANDLE              _port_handle;
  TCHAR               _port_name[128];
  DCB                 _dcb;
  COMMTIMEOUTS        _comm_timeouts;

  // Close previous handle   
  if ( GLB_ModuleData.port_handle )
  {
    CloseHandle (GLB_ModuleData.port_handle);
    GLB_ModuleData.port_handle = INVALID_HANDLE_VALUE;
  }

  // Create new handle   
  _stprintf (_port_name, _T("COM%d:"), (int) GLB_ModuleData.port_number);
  _port_handle = CreateFile (_port_name,                    // FileName                
                             GENERIC_READ | GENERIC_WRITE,  // DesiredAccess           
                             0,                             // ShareMode              
                             NULL,                          // SecurityAttributes      
                             OPEN_EXISTING,                 // CreationDistribution    
                             FILE_FLAG_OVERLAPPED,          // FlagsAndAttributes     
                             NULL);                         // TemplateFile           

  if ( _port_handle == INVALID_HANDLE_VALUE )
  {
    // Logger message   
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_Logger (21, _T("CreateFile"), _log_param1, _port_name);

    return FALSE;
  } 

  // Get port timeouts   
  if ( ! GetCommTimeouts (_port_handle,      // File           
                          &_comm_timeouts) ) // CommTimeouts   
  {
    // Logger message   
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_Logger (21, _T("GetCommTimeouts"), _log_param1, _port_name);

    return FALSE;
  }  

  // Timeout, note values in msec.   
  _comm_timeouts.ReadIntervalTimeout         = MAXDWORD;
  _comm_timeouts.ReadTotalTimeoutMultiplier  = 0;
  _comm_timeouts.ReadTotalTimeoutConstant    = 0;
  _comm_timeouts.WriteTotalTimeoutMultiplier = 0;
  _comm_timeouts.WriteTotalTimeoutConstant   = 5000;

  if ( ! SetCommTimeouts (_port_handle,      // File           
                          &_comm_timeouts) ) // CommTimeouts   
  {
    // Logger message   
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_Logger (21, _T("SetCommTimeouts"), _log_param1, _port_name);

    return FALSE;
  } 

  // Set port parameters   
  if ( ! GetCommState (_port_handle, // File   
                       &_dcb))       // DCB    
  {
    // Logger message   
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_Logger (21, _T("GetCommState"), _log_param1, _port_name);

    return FALSE;
  } 

  // Set DCB parameters   
  _dcb.fBinary           = TRUE;
  _dcb.BaudRate          = 9600;
  _dcb.fParity           = FALSE;
  _dcb.fDsrSensitivity   = 0;
  _dcb.fTXContinueOnXoff = 0;
  _dcb.fOutX             = 0;
  _dcb.fInX              = 0;
  _dcb.ByteSize          = 8;
  _dcb.Parity            = NOPARITY;
  _dcb.StopBits          = ONESTOPBIT;
  // Flow control
  _dcb.fOutxCtsFlow      = FALSE;
  _dcb.fOutxDsrFlow      = FALSE;
  _dcb.fDtrControl       = DTR_CONTROL_DISABLE;
  _dcb.fRtsControl       = RTS_CONTROL_DISABLE;

  // Flow control
  _dcb.fOutxCtsFlow      = FALSE;
  _dcb.fOutxDsrFlow      = FALSE;
  _dcb.fDtrControl       = DTR_CONTROL_DISABLE;
  _dcb.fRtsControl       = RTS_CONTROL_ENABLE;
  
  if ( ! SetCommState (_port_handle,  // File   
                       &_dcb) )       // DCB    
  {
    // Logger message   
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_Logger (21, _T("SetCommState"), _log_param1, _port_name);

    return FALSE;
  } 

  // New port handle assigned
  GLB_ModuleData.port_handle = _port_handle;

  return TRUE;

} // Serial_UARTConfigPort

//------------------------------------------------------------------------------
// PURPOSE : Closes serial port in local UART.
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//  - TRUE: OK
//  - FALSE: Error
//      
//   NOTES :
//
BOOL Serial_UARTClosePort (void)
{
  TCHAR _function_name[] = _T("Serial_UARTClosePort");

  // Close previous handle   
  if ( GLB_ModuleData.port_handle )
  {
    CloseHandle (GLB_ModuleData.port_handle);
  }

  GLB_ModuleData.port_handle = INVALID_HANDLE_VALUE;

  return TRUE;

} // Serial_UARTClosePort

