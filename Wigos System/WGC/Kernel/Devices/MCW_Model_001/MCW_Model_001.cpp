//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : MCW_Model_001.cpp
// 
//   DESCRIPTION : Functions and Methods to handle Magnetic Card model 1.
// 
//        AUTHOR : Xavier Ib��ez
// 
// CREATION DATE : 23-SEP-2008
// 
// REVISION HISTORY
// 
// Date        Author Description
//----------- ------ -----------------------------------------------------------
// 23-SEP-2008 XID    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_QUEUE_API
#define INCLUDE_NLS_API
#define INCLUDE_MCW_API
#define INCLUDE_COMMON_DATA
#define INCLUDE_COMMON_MISC

#include "CommonDef.h"

#include "MCW_Model_001.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

#define DEVICE_DESCRIPTION  _T("MSR206")

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

TYPE_MODULE_DATA  GLB_ModuleData;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Gets the Device Description 
//
//  PARAMS:
//      - INPUT:
//
//      - OUTPUT:
//
// RETURNS:
//      -  The Device Description 
//        
//   NOTES:
//
MCW_MODEL_001 TCHAR * WINAPI DeviceDescription (void)
{ 

  return DEVICE_DESCRIPTION; 

} // DeviceDescription  

//------------------------------------------------------------------------------
// PURPOSE : Initialize Device.
//
//  PARAMS :
//      - INPUT :
//          - pInputData
//
//      - OUTPUT :
//          - pOutputData
//
// RETURNS :
//    - MCW_STATUS_OK
//    - MCW_STATUS_ERROR
//
//   NOTES :
//
MCW_MODEL_001 WORD WINAPI Mcw_Init (TYPE_MCW_INIT_INPUT  * pInputData,
                                    TYPE_MCW_INIT_OUTPUT * pOutputData)
{
  static TCHAR  _function_name[] = _T("Mcw_Init");
  BOOL   _bool_rc;
  WORD   _comm_port;

  // Remember that Common_CheckCB macro exits the function and returns the specified 
  // error if the control block size is not right
  Common_CheckCB (pInputData->control_block, TYPE_MCW_INIT_INPUT, MCW_STATUS_ERROR);
  Common_CheckCB (pOutputData->control_block, TYPE_MCW_INIT_OUTPUT, MCW_STATUS_ERROR);

  // Start logic thread
  _bool_rc = MSR206_CreateLogicThread ();
  if ( ! _bool_rc )
  {
    return FALSE;
  }

  GLB_ModuleData.port_type = COMMON_PORT_TYPE_SERIAL;

  // Try all the comm ports (from COM1 to COM16)
  for ( _comm_port = 1; _comm_port <= 16; _comm_port++ )
  {
    GLB_ModuleData.port_number = _comm_port;

    _bool_rc = MSR206_Init ();
    if ( _bool_rc )
    {
      return MCW_STATUS_OK;
    }
  } // for

  return MCW_STATUS_ERROR;

} // Mcw_Init

//------------------------------------------------------------------------------
// PURPOSE : Close Device.
//
//  PARAMS :
//      - INPUT :
//          - pInputData
//
//      - OUTPUT :
//          - pOutputData
//
// RETURNS :
//    - MCW_STATUS_OK
//    - MCW_STATUS_ERROR
//
//   NOTES :
//
MCW_MODEL_001 WORD WINAPI Mcw_Close (TYPE_MCW_CLOSE_INPUT  * pInputData,
                                     TYPE_MCW_CLOSE_OUTPUT * pOutputData)
{
  BOOL _bool_rc;

  // Remember that Common_CheckCB macro exits the function and returns the specified 
  // error if the control block size is not right
  Common_CheckCB (pInputData->control_block, TYPE_MCW_CLOSE_INPUT, MCW_STATUS_ERROR);
  Common_CheckCB (pOutputData->control_block, TYPE_MCW_CLOSE_OUTPUT, MCW_STATUS_ERROR);

  MSR206_TerminateLogicThread ();

  _bool_rc = MSR206_Close ();
  if ( !_bool_rc )
  {
    return MCW_STATUS_ERROR;
  }

  return MCW_STATUS_OK;

} // Mcw_Close

//------------------------------------------------------------------------------
// PURPOSE : Returns Device status.
//
//  PARAMS :
//      - INPUT :
//          - pInputData
//
//      - OUTPUT :
//          - pOutputData
//
// RETURNS :
//    - MCW_STATUS_OK
//    - MCW_STATUS_ERROR
//
//   NOTES :
//
MCW_MODEL_001 WORD WINAPI Mcw_Status (TYPE_MCW_GET_STATUS_INPUT  * pInputData,
                                      TYPE_MCW_GET_STATUS_OUTPUT * pOutputData)
{
  // Remember that Common_CheckCB macro exits the function and returns the specified 
  // error if the control block size is not right
  Common_CheckCB (pInputData->control_block, TYPE_MCW_GET_STATUS_INPUT, MCW_STATUS_ERROR);
  Common_CheckCB (pOutputData->control_block, TYPE_MCW_GET_STATUS_OUTPUT, MCW_STATUS_ERROR);

  MSR206_GetStatus (&pOutputData->status);

  return MCW_STATUS_OK;

} // Mcw_Status

//------------------------------------------------------------------------------
// PURPOSE : Writes to card.
//
//  PARAMS :
//      - INPUT :
//          - pInputData
//
//      - OUTPUT :
//          - pOutputData
//
// RETURNS :
//    - MCW_STATUS_OK
//    - MCW_STATUS_ERROR
//
//   NOTES :
//
MCW_MODEL_001 WORD WINAPI Mcw_Write (TYPE_MCW_WRITE_INPUT  * pInputData,
                                     TYPE_MCW_WRITE_OUTPUT * pOutputData)
{
  BOOL _bool_rc;

  // Remember that Common_CheckCB macro exits the function and returns the specified 
  // error if the control block size is not right
  Common_CheckCB (pInputData->control_block, TYPE_MCW_WRITE_INPUT, MCW_STATUS_ERROR);
  Common_CheckCB (pOutputData->control_block, TYPE_MCW_WRITE_OUTPUT, MCW_STATUS_ERROR);

  _bool_rc = MSR206_Write (&pInputData->card_data);
  if ( !_bool_rc )
  {
    return MCW_STATUS_ERROR;
  }

  return MCW_STATUS_OK;

} // Mcw_Write

//------------------------------------------------------------------------------
// PURPOSE : Write operation cancellation.
//
//  PARAMS :
//      - INPUT :
//          - pInputData
//
//      - OUTPUT :
//          - pOutputData
//
// RETURNS :
//    - MCW_STATUS_OK
//    - MCW_STATUS_ERROR
//
//   NOTES :
//
MCW_MODEL_001 WORD WINAPI Mcw_CancelWrite (TYPE_MCW_CANCEL_WRITE_INPUT  * pInputData,
                                           TYPE_MCW_CANCEL_WRITE_OUTPUT * pOutputData)
{
  // Remember that Common_CheckCB macro exits the function and returns the specified 
  // error if the control block size is not right
  Common_CheckCB (pInputData->control_block, TYPE_MCW_CANCEL_WRITE_INPUT, MCW_STATUS_ERROR);
  Common_CheckCB (pOutputData->control_block, TYPE_MCW_CANCEL_WRITE_OUTPUT, MCW_STATUS_ERROR);

  MSR206_CancelWrite ();

  return MCW_STATUS_OK;

} // Mcw_CancelWrite

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE : DLL standard main function.
//
//  PARAMS :
//     - INPUT : None
//
//     - OUTPUT : None
//
//
// RETURNS :
//     - TRUE: OK
//     - FALSE: Force dll unload
//
// NOTES :

BOOL APIENTRY DllMain (HANDLE Module, 
                       DWORD  ReasonForCall, 
                       LPVOID Reserved)
{
  switch ( ReasonForCall )
  {
    case DLL_PROCESS_ATTACH:

      // DLL load action
      memset (&GLB_ModuleData, 0, sizeof (GLB_ModuleData));
      GLB_ModuleData.port_handle    = INVALID_HANDLE_VALUE;
      GLB_ModuleData.device_status  = MCW_STATUS_ERROR;

      InitializeCriticalSection (&GLB_ModuleData.device_access);

    break;

    case DLL_PROCESS_DETACH:

      MSR206_TerminateLogicThread ();

    break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    break;

  } // switch

  return TRUE;

} // DllMain
