//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MCW_Model_001Internals.h
//   DESCRIPTION : Constants, types, variables definitions and prototypes
//                 internals for the MCW_Model_001
//        AUTHOR : Xavier Ib��ez
// CREATION DATE : 23-SEP-2008
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 23-SEP-2008 XID    Initial draft.
//
//------------------------------------------------------------------------------

#ifndef __MCW_MODEL_001_INTERNALS_H
#define __MCW_MODEL_001_INTERNALS_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------

#define MODULE_NAME                         _T("MCW_Model_001")

#ifdef MCW_MODEL_001_EXPORTS
#define MCW_MODEL_001 __declspec(dllexport)
#else
#define MCW_MODEL_001 __declspec(dllimport)
#endif

#define MCW_AUX_STRING_LENGTH               512

#define MSR206_COMM_TEST_LOOP               2000  // msec. Time between communication tests

#define MSR206_MAX_PACKET_SIZE              1024
#define MSR206_ESC                          0x1B
#define MSR206_START_DATA_BLOCK             0x73
#define MSR206_END_DATA_BLOCK_1             0x3F
#define MSR206_END_DATA_BLOCK_2             0x1C
#define MSR206_TRACK_01                     0x01
#define MSR206_TRACK_02                     0x02
#define MSR206_TRACK_03                     0x03

#define MSR206_CMD_PACKET_HEADER_SIZE       1

#define MSR206_STATUS_COMM_TEST             0x79
#define MSR206_STATUS_CMD_OK                0x30
#define MSR206_STATUS_CMD_WRITE_READ_ERROR  0x31
#define MSR206_STATUS_CMD_FORMAT_ERROR      0x32
#define MSR206_STATUS_CMD_INVALID_CMD       0x34
#define MSR206_STATUS_CMD_INVALID_SWIPE     0x39
#define MSR206_STATUS_LOW_CO                0x4C
#define MSR206_STATUS_HI_CO                 0x48

#define MSR206_READ_TIMEOUT                 1000      // msec
#define MSR206_READ_LONG_TIMEOUT            INFINITE  // msec
#define MSR206_T_FREE_TIME                  200       // msec, time between requests
#define MSR206_MAX_TRIES                    3

typedef enum
{
  NO_WAIT_FOR_ANSWER = 1,
  WAIT_FOR_ANSWER_NORMAL_TIMEOUT = 2,
  WAIT_FOR_ANSWER_WRITE_OPERATION = 3,

} ENUM_COMMAND_TYPE;

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------

typedef struct
{
  BOOL              init;
  WORD              port_type;
  WORD              port_number;
  HANDLE            port_handle;
  ENUM_CARD_TYPE    card_type;
  WORD              device_status;

  HANDLE            thread_handle;
  BOOL              exit_thread;

  BOOL              cancel_write;

  CRITICAL_SECTION  device_access;

} TYPE_MODULE_DATA;

// Protocol packet formats
typedef struct
{
  BYTE esc;
  BYTE data[MSR206_MAX_PACKET_SIZE];
  
} TYPE_MSR206_CMD_PACKET;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

extern TYPE_MODULE_DATA GLB_ModuleData;

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

// Device
BOOL MSR206_CreateLogicThread (void);
void MSR206_TerminateLogicThread (void);
DWORD WINAPI MSR206_Thread (LPVOID ThreadParameter);
BOOL MSR206_Init (void);
BOOL MSR206_Close (void);
BOOL MSR206_Write (MCW_CARD_DATA * pCardData);
BOOL MSR206_CancelWrite (void);
void MSR206_GetStatus (WORD * pDeviceStatus);


// Serial
BOOL Serial_ConfigPort (WORD PortType, WORD PortNumber);
BOOL Serial_ClosePort (void);
BOOL Serial_WritePort (BYTE * pBuffer, DWORD BufferSize);
BOOL Serial_ReadPort (BYTE * pBuffer, DWORD BufferLength, DWORD * pDataLengthRead);

#endif // __MCW_MODEL_001_INTERNALS_H
