//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME : MCW_Model_001_Logic.cpp
//
//   DESCRIPTION : Functions and Methods to handle magnetic card writer MSR206
//
//        AUTHOR : Xavier Ib��ez
//
// CREATION DATE : 23-SEP-2008
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-SEP-2008 XID    First release.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API
#define INCLUDE_MCW_API

#include "CommonDef.h"

#include "MCW_Model_001.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

BOOL MSR206_OpenPort (void);
BOOL MSR206_ClosePort (void);
BOOL MSR206_ProtCommTest (void);
BOOL MSR206_ProtInit (void);
BOOL MSR206_ProtWrite (MCW_CARD_DATA * pCardData);

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Creates logic thread
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT : None
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//      
BOOL MSR206_CreateLogicThread (void)
{
  static TCHAR  _function_name[] = _T("MSR206_CreateLogicThread");
  TCHAR  _log_param1[MCW_AUX_STRING_LENGTH];
  DWORD  _thread_id;

  // Create logic thread
  if ( ! GLB_ModuleData.thread_handle )
  {
    GLB_ModuleData.thread_handle = CreateThread (NULL,                                   // ThreadAttributes 
                                                 THREAD_MAX_STACK,                       // StackSize        
                                                 (LPTHREAD_START_ROUTINE) MSR206_Thread, // StartAddress     
                                                 (LPVOID) NULL,                          // Parameter        
                                                 0,                                      // CreationFlags    
                                                 &_thread_id);                           // ThreadId       
    if ( ! GLB_ModuleData.thread_handle )
    {
      // Error creating thread 
      // Logger message
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      Common_LoggerMsg (NLS_ID_LOGGER(2), MODULE_NAME, _function_name, _log_param1, _T("CreateThread"));

      return FALSE;
    }
  }

  return TRUE;

} // MSR206_CreateLogicThread

//------------------------------------------------------------------------------
// PURPOSE : Terminates logic thread
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT : None
//
// RETURNS :
//      
void MSR206_TerminateLogicThread (void)
{
  DWORD _rc;

  if ( GLB_ModuleData.thread_handle )
  {
    EnterCriticalSection (&GLB_ModuleData.device_access);
    GLB_ModuleData.exit_thread = TRUE;
    LeaveCriticalSection (&GLB_ModuleData.device_access);
    _rc = WaitForSingleObject (GLB_ModuleData.thread_handle, 10000);
  }

} // MSR206_TerminateLogicThread

//------------------------------------------------------------------------------
// PURPOSE : Main thread
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT : None
//
// RETURNS :
//      
DWORD WINAPI MSR206_Thread (LPVOID ThreadParameter)
{
  TCHAR  _function_name[] = _T("MSR206_Thread");
  BOOL   _bool_rc;

  while ( TRUE )
  {
    __try
    {
      EnterCriticalSection (&GLB_ModuleData.device_access);

      if ( GLB_ModuleData.exit_thread )
      {
        return 0;
      }

      if ( GLB_ModuleData.init )
      {
        _bool_rc = MSR206_ProtCommTest ();
        GLB_ModuleData.device_status = ( _bool_rc ? MCW_STATUS_OK : MCW_STATUS_ERROR );
      }
      else
      {
        GLB_ModuleData.device_status = MCW_STATUS_ERROR;
      }

    } // __try

    __finally
    {
      LeaveCriticalSection (&GLB_ModuleData.device_access);
    } // __finally

    Sleep (MSR206_COMM_TEST_LOOP);

  } // while

  return 0;

} // MSR206_Thread

//------------------------------------------------------------------------------
// PURPOSE : Device init
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT : None
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//      
BOOL MSR206_Init (void)
{
  BOOL _bool_rc;

  __try
  {
    EnterCriticalSection (&GLB_ModuleData.device_access);

    GLB_ModuleData.init = FALSE;

    _bool_rc = MSR206_ClosePort ();
    if ( !_bool_rc )
    {
      return FALSE;
    }

    _bool_rc = MSR206_OpenPort ();
    if ( !_bool_rc )
    {
      MSR206_ClosePort ();
      return FALSE;
    }

    _bool_rc = MSR206_ProtInit ();
    if ( !_bool_rc )
    {
      MSR206_ClosePort ();
      return FALSE;
    }

    GLB_ModuleData.init = TRUE;

    return TRUE;

  } // __try

  __finally
  {
    LeaveCriticalSection (&GLB_ModuleData.device_access);
  } // __finally

} // MSR206_Init

//------------------------------------------------------------------------------
// PURPOSE : Device init
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT : None
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//      
BOOL MSR206_Close (void)
{
  BOOL _bool_rc;

  EnterCriticalSection (&GLB_ModuleData.device_access);

  GLB_ModuleData.init = FALSE;

  _bool_rc = MSR206_ClosePort ();

  LeaveCriticalSection (&GLB_ModuleData.device_access);

  return _bool_rc;

} // MSR206_Close

//------------------------------------------------------------------------------
// PURPOSE : Card write
//
//  PARAMS :
//      - INPUT :
//        - pCardData
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_Write (MCW_CARD_DATA * pCardData)
{
  BOOL _bool_rc;

  EnterCriticalSection (&GLB_ModuleData.device_access);

  GLB_ModuleData.cancel_write = FALSE;

  _bool_rc = MSR206_ProtWrite (pCardData);

  LeaveCriticalSection (&GLB_ModuleData.device_access);

  return _bool_rc;

} // MSR206_Write

//------------------------------------------------------------------------------
// PURPOSE : Cancels write operation activating the cancel_write flag
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_CancelWrite (void)
{

  GLB_ModuleData.cancel_write = TRUE;

  return TRUE;

} // MSR206_CancelWrite

//------------------------------------------------------------------------------
// PURPOSE : Gets current device status
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//        - pDeviceStatus
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
void MSR206_GetStatus (WORD * pDeviceStatus)
{

  EnterCriticalSection (&GLB_ModuleData.device_access);

  * pDeviceStatus = GLB_ModuleData.device_status;

  LeaveCriticalSection (&GLB_ModuleData.device_access);

  return;

} // MSR206_GetStatus

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Updates timer. Decreases timer from last tickcount
//
//  PARAMS :
//      - INPUT :
//        - pReadTimer (msec)
//        - pLastTickCount (msec)
//
//      - OUTPUT :
//        - pReadTimer (msec)
//        - pLastTickCount (msec)
//
// RETURNS :
//
DWORD GetPerfTickCount (void)
{
  static LARGE_INTEGER  _perf_freq = {0};
  LARGE_INTEGER         _current_time;

  if ( !_perf_freq.QuadPart )
  {
    QueryPerformanceFrequency (&_perf_freq);
    //  Convert frequency from tick/sec to ticks/msec
    _perf_freq.QuadPart /= 1000;
  }

  QueryPerformanceCounter (&_current_time);
  _current_time.QuadPart /= _perf_freq.QuadPart;
  return _current_time.LowPart;

} // GetPerfTickCount

//------------------------------------------------------------------------------
// PURPOSE : Updates timer. Decreases timer from last tickcount
//
//  PARAMS :
//      - INPUT :
//        - pReadTimer (msec)
//        - pLastTickCount (msec)
//
//      - OUTPUT :
//        - pReadTimer (msec)
//        - pLastTickCount (msec)
//
// RETURNS :
//
void MSR206_UpdateTimer (DWORD * pReadTimer, DWORD * pLastTickCount)
{
  DWORD _current_tick_count;
  DWORD _time_slapsed;

  if ( * pReadTimer == 0 )
  {
    // Already timedout
    return;
  }

  _current_tick_count = GetPerfTickCount ();
  
  _time_slapsed = 0;
  if ( _current_tick_count >= * pLastTickCount )
  {
    _time_slapsed = _current_tick_count - * pLastTickCount;
  }
  
  if ( _time_slapsed < * pReadTimer )
  {
    * pReadTimer = * pReadTimer - _time_slapsed;
  }
  else
  {
    // Timer expired
    * pReadTimer = 0;
  }
  
  * pLastTickCount = _current_tick_count;
  
} // MSR206_UpdateTimer

//------------------------------------------------------------------------------
// PURPOSE : Serial channel purge
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
void MSR206_PurgeSerial (void)
{
  BOOL  _bool_rc;
  BYTE  _buffer[512];
  DWORD _length_read;

  while ( TRUE )
  {
    _bool_rc = Serial_ReadPort (_buffer, sizeof (_buffer), &_length_read);
    if ( !_bool_rc )
    {
      break;
    }

    if ( _length_read == 0 )
    {
      break;
    }
  } // while

} // MSR206_PurgeSerial

//------------------------------------------------------------------------------
// PURPOSE : Open communication port
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_OpenPort (void)
{
  BOOL _bool_rc;

  _bool_rc = Serial_ConfigPort (GLB_ModuleData.port_type, GLB_ModuleData.port_number);
  if ( !_bool_rc )
  {
    return FALSE;
  }
  
  return TRUE;

} // MSR206_OpenPort

//------------------------------------------------------------------------------
// PURPOSE : Close communication port
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_ClosePort (void)
{
  BOOL _bool_rc;

  _bool_rc = Serial_ClosePort ();
  if ( !_bool_rc )
  {
    return FALSE;
  }
  
  return TRUE;

} // MSR206_ClosePort

//------------------------------------------------------------------------------
// PURPOSE : Cmd packet sent
//
//  PARAMS :
//      - INPUT :
//        - pRqstData
//        - DataLength
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_SendPacket (BYTE * pData, DWORD DataLength)
{
  BOOL  _bool_rc;
  BYTE  * p_packet;
  DWORD _packet_length;
  TYPE_MSR206_CMD_PACKET _cmd_packet;

  memset ((BYTE *) &_cmd_packet, 0, sizeof (_cmd_packet));
  _cmd_packet.esc = MSR206_ESC;
  memcpy (_cmd_packet.data, pData, DataLength);

  p_packet = (BYTE *) &_cmd_packet;
  _packet_length = MSR206_CMD_PACKET_HEADER_SIZE + DataLength;

  // Send request packet
  _bool_rc = Serial_WritePort (p_packet, _packet_length);
  if ( !_bool_rc )
  {
    return FALSE;
  }
    
  return TRUE;

} // MSR206_SendPacket

//------------------------------------------------------------------------------
// PURPOSE : Cmd packet reception. Answer packet has the following format:
//            [DATA] ESC [STATUS BYTE]
//
//  PARAMS :
//      - INPUT :
//        - CommandType
//
//      - OUTPUT :
//        - pAnswerData
//        - pAnswerDataLength
//        - pStatusByte
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_ReceivePacket (ENUM_COMMAND_TYPE CommandType, BYTE * pAnswerData, DWORD * pAnswerDataLength, BYTE * pStatusByte)
{
  BOOL  _bool_rc;
  BYTE  * p_packet;
  DWORD _idx_byte;
  BOOL  _read_end;
  DWORD _length_to_read;
  DWORD _length_read;
  DWORD _read_timer;
  DWORD _last_tick_count;
  BYTE  _packet[MSR206_MAX_PACKET_SIZE];

  memset (&_packet, 0, sizeof (_packet));
  p_packet = (BYTE *) &_packet;

  * pAnswerDataLength = 0;
  * pStatusByte       = 0;

  _read_timer         = ( CommandType == WAIT_FOR_ANSWER_WRITE_OPERATION ) ? MSR206_READ_LONG_TIMEOUT : MSR206_READ_TIMEOUT;
  _last_tick_count    = GetPerfTickCount ();

  _length_to_read     = sizeof (_packet);
  _read_end           = FALSE;
  _idx_byte           = 0;

  while ( ! _read_end )
  {
    _bool_rc = Serial_ReadPort (&p_packet[_idx_byte], _length_to_read, &_length_read);
    if ( !_bool_rc )
    {
      // Read error
      _read_end = TRUE;
      continue;
    }

    MSR206_UpdateTimer (&_read_timer, &_last_tick_count);
    if ( _read_timer == 0 )
    {
      // Timeout
      break;
    }

    // Byte accepted
    _idx_byte += _length_read;
    if ( _idx_byte >= sizeof (_packet) )
    {
      // Buffer overflow
      break;
    }

    // Check eot
    if ( _idx_byte >= 2 )
    {
      if ( p_packet[_idx_byte - 2] == MSR206_ESC )
      {
        // end of message
        * pStatusByte =  p_packet[_idx_byte - 1];
        * pAnswerDataLength = _idx_byte - 2;
        memcpy (pAnswerData, _packet, * pAnswerDataLength);
        return TRUE;
      }
    }

    _length_to_read -= _length_read;
    if ( _length_to_read > 0 )
    {
      // Check if operation (write) must be cancelled
      if (   GLB_ModuleData.cancel_write
          && CommandType == WAIT_FOR_ANSWER_WRITE_OPERATION )
      {
        return FALSE;
      }

      Sleep (1);
      continue;
    }
    
  } // while

  return FALSE;

} // MSR206_ReceivePacket

//------------------------------------------------------------------------------
// PURPOSE : Transaction process against peripheral
//
//  PARAMS :
//      - INPUT :
//        - CommandType
//        - pRqstData, RqstLength
//
//      - OUTPUT :
//        - pAnswerData, pAnswerDataLength, pStatusByte: They can be NULL if no wait for answer
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_ProcessRequest (ENUM_COMMAND_TYPE CommandType,
                            BYTE  * pRqstData,
                            DWORD RqstLength,
                            BYTE  * pAnswerData,
                            DWORD * pAnswerDataLength,
                            BYTE  * pStatusByte)
{
  TCHAR         _function_name[] = _T("MSR206_ProcessRequest");
  DWORD         _num_tries;
  BOOL          _bool_rc;
  BOOL          _answer_received;
  DWORD         _max_tries;
  static DWORD  _last_tick_t_free = GetPerfTickCount ();
  static DWORD  _timer_t_free = MSR206_T_FREE_TIME;

  if ( CommandType != NO_WAIT_FOR_ANSWER )
  {
    * pAnswerDataLength = 0;
    * pStatusByte       = 0;
  }

  // Note only one try for long timeout commands
  _max_tries = ( CommandType == WAIT_FOR_ANSWER_WRITE_OPERATION ) ? 1 : MSR206_MAX_TRIES;

  // Loop sending and receiving answer
  _answer_received = FALSE;
  for ( _num_tries = 0; _num_tries < _max_tries; _num_tries++ )
  {
    // Minum time between two requests
    while ( TRUE )
    {
      MSR206_UpdateTimer (&_timer_t_free, &_last_tick_t_free);
      if ( _timer_t_free == 0 )
      {
        // Minimum time elapsed. 
        break;
      }
      Sleep (1);
    } // while

    // Reset timer
    _timer_t_free = MSR206_T_FREE_TIME;

    // Purge serial channel before sending command
    MSR206_PurgeSerial ();

    // Send request packet
    _bool_rc = MSR206_SendPacket (pRqstData, RqstLength);
    if ( !_bool_rc )
    {
      // Error sending packet
      // Reset timer for next request
      _timer_t_free = MSR206_T_FREE_TIME;

      continue;
    }

    if ( CommandType != NO_WAIT_FOR_ANSWER )
    {
      // Receive answer packet
      _bool_rc = MSR206_ReceivePacket (CommandType, pAnswerData, pAnswerDataLength, pStatusByte);
      if ( !_bool_rc )
      {
        // Error receiving packet
        // Reset timer for next request
        _timer_t_free = MSR206_T_FREE_TIME;

        continue;
      }
    }

    // Reset timer for next request
    _timer_t_free = MSR206_T_FREE_TIME;

    // Exit loop
    _answer_received = TRUE;
    break;
    
  } // while

  if ( !_answer_received )
  {
    // Error
    return FALSE;
  }

  return TRUE;

} // MSR206_ProcessRequest

//------------------------------------------------------------------------------
// PURPOSE : Protocol device reset
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_ProtReset (void)
{
  BOOL  _bool_rc;
  BYTE  _command[1] = { 0x61 };

  _bool_rc = MSR206_ProcessRequest (NO_WAIT_FOR_ANSWER,
                                    _command,
                                    sizeof (_command),
                                    NULL,
                                    NULL,
                                    NULL);
  if ( ! _bool_rc )
  {
    return FALSE;
  }

  return TRUE;

} // MSR206_ProtReset

//------------------------------------------------------------------------------
// PURPOSE : Communication Test
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_ProtCommTest (void)
{
  BOOL  _bool_rc;
  BYTE  _status_byte;
  BYTE  _command[1] = { 0x65 };
  BYTE  _answer_data[MSR206_MAX_PACKET_SIZE];
  DWORD _answer_length;

  _bool_rc = MSR206_ProcessRequest (WAIT_FOR_ANSWER_NORMAL_TIMEOUT,
                                    _command,
                                    sizeof (_command),
                                    _answer_data,
                                    &_answer_length,
                                    &_status_byte);
  if ( ! _bool_rc )
  {
    return FALSE;
  }

  if ( _status_byte == MSR206_STATUS_COMM_TEST )
  {
    return TRUE;
  }

  return FALSE;

} // MSR206_ProtCommTest

//------------------------------------------------------------------------------
// PURPOSE : Sets device card coercivity
//
//  PARAMS :
//      - INPUT :
//        - CardType
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_ProtSetCoercivity (ENUM_CARD_TYPE CardType)
{
  BOOL  _bool_rc;
  BYTE  * p_cmd;
  BYTE  _command_hi_co[1]  = { 0x78 };
  BYTE  _command_low_co[1] = { 0x79 };
  BYTE  _command_get_co[1] = { 0x64 };
  BYTE  _status_byte;
  BYTE  _waited_co;
  BYTE  _answer_data[MSR206_MAX_PACKET_SIZE];
  DWORD _answer_length;

  ( CardType == CARD_HI_CO ) ? p_cmd = _command_hi_co : p_cmd = _command_low_co;
  ( CardType == CARD_HI_CO ) ? _waited_co = MSR206_STATUS_HI_CO : _waited_co = MSR206_STATUS_LOW_CO;

  _bool_rc = MSR206_ProcessRequest (WAIT_FOR_ANSWER_NORMAL_TIMEOUT,
                                    p_cmd,
                                    sizeof (_command_hi_co),
                                    _answer_data,
                                    &_answer_length,
                                    &_status_byte);

  if ( _bool_rc )
  {
    if ( _status_byte == MSR206_STATUS_CMD_WRITE_READ_ERROR )
    {
      // Let the device to be reconfigured
      Sleep (1000);
    }
  }

  // Check Lo or Hi Co
  _bool_rc = MSR206_ProcessRequest (WAIT_FOR_ANSWER_NORMAL_TIMEOUT,
                                    _command_get_co,
                                    sizeof (_command_get_co),
                                    _answer_data,
                                    &_answer_length,
                                    &_status_byte);
  if ( ! _bool_rc )
  {
    return FALSE;
  }

  if ( _status_byte == _waited_co )
  {
    // Update current device card type
    GLB_ModuleData.card_type = CardType;

    return TRUE;
  }

  return FALSE;

} // MSR206_ProtSetCoercivity

//------------------------------------------------------------------------------
// PURPOSE : Protocol device initialization
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_ProtInit (void)
{
  BOOL _bool_rc;

  // Reset
  _bool_rc = MSR206_ProtReset ();
  if ( !_bool_rc )
  {
    return FALSE;
  }

  // test if device connected
  _bool_rc = MSR206_ProtCommTest ();
  if ( !_bool_rc )
  {
    return FALSE;
  }

  // Reset again
  _bool_rc = MSR206_ProtReset ();
  if ( !_bool_rc )
  {
    return FALSE;
  }

  // Set default coercivity (Low)
  _bool_rc = MSR206_ProtSetCoercivity (CARD_LOW_CO);
  if ( !_bool_rc )
  {
    return FALSE;
  }

  return TRUE;

} // MSR206_ProtInit

//------------------------------------------------------------------------------
// PURPOSE : Protocol card write
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//    - TRUE: OK
//    - FALSE: Error
//     
BOOL MSR206_ProtWrite (MCW_CARD_DATA * pCardData)
{
  BOOL  _bool_rc;
  BYTE  _status_byte;
  DWORD _idx_byte;
  BYTE  _command[MSR206_MAX_PACKET_SIZE];
  BYTE  _answer_data[MSR206_MAX_PACKET_SIZE];
  DWORD _answer_length;

  // Check card type value and force low if unknown
  if (   pCardData->card_type != CARD_HI_CO
      && pCardData->card_type != CARD_LOW_CO )
  {
    pCardData->card_type = CARD_LOW_CO;
  }

  // Check current coercivity
  if ( GLB_ModuleData.card_type != pCardData->card_type )
  {
    _bool_rc = MSR206_ProtSetCoercivity (pCardData->card_type);
    if ( !_bool_rc )
    {
      return FALSE;
    }
  }

  _idx_byte = 0;
  _command[_idx_byte++] = 0X77;

  // Data block
  _command[_idx_byte++] = MSR206_ESC;
  _command[_idx_byte++] = MSR206_START_DATA_BLOCK;

  // Card data
  _command[_idx_byte++] = MSR206_ESC;
  _command[_idx_byte++] = MSR206_TRACK_01;
  memcpy (&_command[_idx_byte], pCardData->data1, pCardData->data1_length);
  _idx_byte += pCardData->data1_length;

  _command[_idx_byte++] = MSR206_ESC;
  _command[_idx_byte++] = MSR206_TRACK_02;
  memcpy (&_command[_idx_byte], pCardData->data2, pCardData->data2_length);
  _idx_byte += pCardData->data2_length;

  _command[_idx_byte++] = MSR206_ESC;
  _command[_idx_byte++] = MSR206_TRACK_03;
  memcpy (&_command[_idx_byte], pCardData->data3, pCardData->data3_length);
  _idx_byte += pCardData->data3_length;

  _command[_idx_byte++] = MSR206_END_DATA_BLOCK_1;
  _command[_idx_byte++] = MSR206_END_DATA_BLOCK_2;

  _bool_rc = MSR206_ProcessRequest (WAIT_FOR_ANSWER_WRITE_OPERATION,
                                    _command,
                                    _idx_byte,
                                    _answer_data,
                                    &_answer_length,
                                    &_status_byte);

  // Reset cancel operation flag (just in case the operation was cancelled)
  GLB_ModuleData.cancel_write = FALSE;

  if ( ! _bool_rc )
  {
    MSR206_ProtReset ();
    return FALSE;
  }

  if ( _status_byte == MSR206_STATUS_CMD_OK )
  {
    return TRUE;
  }

  return FALSE;

} // MSR206_ProtWrite
