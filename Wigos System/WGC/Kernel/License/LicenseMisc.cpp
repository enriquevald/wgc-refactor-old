//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : LicenseMisc.c
//
//   DESCRIPTION : License miscellaneous functions.
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 01-AUG-2002
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//  INCLUDES
//-------------------------------------------------------------------------------

#define INCLUDE_VERSION_CONTROL
#define INCLUDE_LICENSE
#include "CommonDef.h"

#include "LicenseInternals.h"

//-------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PURPOSE : Check internal structures sizes
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : OK
//      - FALSE : Error
//
//   NOTES :
//      - TYPE_LICENSE size must be multiple of 8 (DES encryption purposes)
//      - TYPE_LICENSE_RECORD size must be multiple of 8 (CRC purposes)

BOOL        CheckLicenseStructSize ()
{
  // TYPE_LICENSE size must be multiple of 8 (DES encryption purposes)
  if ( sizeof (TYPE_LICENSE) % 8 != 0 )
  {
    return FALSE;
  }

  // TYPE_LICENSE_RECORD size must be multiple of 8 (CRC purposes)
  if ( sizeof (TYPE_LICENSE_RECORD) % 8 != 0 )
  {
    return FALSE;
  }
                                    
  return TRUE;
} // CheckLicenseStructSize