//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : LicenseCreate.c
//
//   DESCRIPTION : Functions and Methods to create the LK system's license.
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 01-AUG-2002
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 04-JUL-2002 CIR    Initial draft.
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//  INCLUDES
//-------------------------------------------------------------------------------

#define INCLUDE_VERSION_CONTROL
#define INCLUDE_LICENSE
#include "CommonDef.h"

#include "LicenseInternals.h"

//-------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PURPOSE : Create data to license file
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : License OK
//      - FALSE : Invalid License creation
//
//   NOTES :

BOOL        CreateLicense (TYPE_LICENSE * pLicense)
{
  HANDLE                _file;
  DWORD                 _num_bytes_write;
  TYPE_LICENSE_RECORD   _record;
  TYPE_LICENSE_RECORD   _record_encrypted;
  DWORD                 _crc_size;
  int                   _rc_encrypt;
  BOOL                  _rc_bool;

  if ( ! CheckLicenseStructSize () )
  {
    return FALSE;
  }

  // Copy intup structure
  memcpy (&_record.license, pLicense, sizeof (TYPE_LICENSE));

  // Calculate CRC
  _crc_size = sizeof (TYPE_LICENSE_RECORD) - sizeof (DWORD);
  _record.crc = Version_Crc32_ComputeCRC ((BYTE *) &_record, _crc_size);

  // Encrypt structure
  _rc_encrypt = Version_Encrypt ((BYTE *) &_record,             // Input buffer
                                 sizeof (TYPE_LICENSE_RECORD),              
                                 (BYTE *) &_record_encrypted,   // Output buffer
                                 sizeof (TYPE_LICENSE_RECORD),              
                                 (BYTE *) LKL_ENCRYPTION_KEY,   // Encryption key
                                 LKL_DES_ENCRYPT,               // Operation code = encrypt
                                 LKL_ENCRYPT_DES_TRIPLE);       // Type = Triple DES
  if ( ! _rc_encrypt )
  {
    return FALSE;
  }

  //    - Create license file
  _file = CreateFile (LICENSE_FILE_NAME, 
                      GENERIC_READ | GENERIC_WRITE,
                      0,
                      NULL,
                      CREATE_ALWAYS,
                      0,
                      NULL);

  if ( _file == INVALID_HANDLE_VALUE )
  {
    // Could not create license file
    return FALSE;
  }

  // Write data to license file
  // Write register to file
  _rc_bool = WriteFile (_file, 
                        (BYTE *) &_record_encrypted, 
                        sizeof (TYPE_LICENSE_RECORD), 
                        &_num_bytes_write,
                        NULL);
  if ( ! _rc_bool )
  {
    // Could not write license file data
    CloseHandle (_file);

    return FALSE;
  }

  CloseHandle (_file);
  
  return TRUE;
} // CreateLicense

