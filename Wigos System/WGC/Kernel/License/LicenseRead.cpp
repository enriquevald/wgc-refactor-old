//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : LicenseRead.c
//
//   DESCRIPTION : Functions and Methods to read the LK system's license.
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 01-AUG-2002
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//  INCLUDES
//-------------------------------------------------------------------------------

#define INCLUDE_VERSION_CONTROL
#define INCLUDE_LICENSE
#include "CommonDef.h"

#include "LicenseInternals.h"

//-------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// PURPOSE : Reads data from license file
//
//  PARAMS :
//      - INPUT :
//          - pFilePath
//
//      - OUTPUT :
//          - pLicense
//
// RETURNS :
//      - TRUE : License OK
//      - FALSE : Invalid License information
//
//   NOTES :

BOOL        ReadLicense (TYPE_LICENSE   * pLicense,
                         TCHAR          * pFilePath)
{
  HANDLE                _file;
  TYPE_LICENSE_RECORD   _record;
  DWORD                 _crc;
  TYPE_LICENSE_RECORD   _record_encrypted;
  int                   _rc_encrypt;
  DWORD                 _bytes_read;
  BOOL                  _rc_bool;
  TCHAR                 _file_name [MAX_PATH];
  DWORD                 _path_length;

  if ( ! CheckLicenseStructSize () )
  {
    return FALSE;
  }

  _file_name[0] = 0x00;

  // Format file name (add path if available)
  _path_length = _tcslen (pFilePath);
  if ( _path_length > 0 )
  {
    _tcscpy (_file_name, pFilePath);
    if ( pFilePath[_path_length-1] != _T('\\') )
    {
      _tcscat (_file_name, _T("\\"));
    }
  }

  _tcscat (_file_name, LICENSE_FILE_NAME);

  // Open license file
  _file = CreateFile (_file_name, 
                      GENERIC_READ,
                      FILE_SHARE_READ,
                      NULL,
                      OPEN_EXISTING,
                      0,
                      NULL);

  if ( _file == INVALID_HANDLE_VALUE )
  {
    // ERROR : File does not exist
    return FALSE;
  }

  // Read license from file 
  _rc_bool = ReadFile (_file, 
                       &_record_encrypted,
                       sizeof (TYPE_LICENSE_RECORD),
                       &_bytes_read,
                       NULL);
  if ( !_rc_bool )
  {
    // Error reading file
    CloseHandle (_file);

    return FALSE;
  }

  // Close license file
  CloseHandle (_file);

  // Unencrypt the license data
  _rc_encrypt = Version_Encrypt ((BYTE *) &_record_encrypted,   // Input buffer
                                 sizeof (TYPE_LICENSE_RECORD),      
                                 (BYTE *) &_record,             // Output buffer
                                 sizeof (TYPE_LICENSE_RECORD),
                                 (BYTE *) LKL_ENCRYPTION_KEY,   // Encryption key
                                 LKL_DES_DECRYPT,               // Operation code = decrypt
                                 LKL_ENCRYPT_DES_TRIPLE);       // Type = Triple DES
  if ( ! _rc_encrypt )
  {
    return FALSE;
  }

  // Check the CRC
  _crc = Version_Crc32_ComputeCRC ((BYTE *) &_record, 
                                   sizeof (_record) - sizeof (_record.crc));
  if ( _crc != _record.crc )
  {
    // ERROR : Invalid crc
    return FALSE;
  }

  * pLicense = _record.license;

  return TRUE;
} // ReadLicense
