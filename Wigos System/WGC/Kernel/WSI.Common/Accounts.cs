//------------------------------------------------------------------------------
// Copyright © 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Accounts.cs
// 
//   DESCRIPTION: Class to manage operations
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 21-SEP-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-SEP-2010 ACC    First release.
// 25-OCT-2011 ACC    Change InitialNotRedeemable for active play sessions in 
//                    LKT SAS HOST machine when add balance.
// 24-NOV-2011 RCI    Added method DB_CheckAccountPIN() to be used by Cashier and WCP.
// 28-NOV-2011 JMM    Added PIN length constants.
// 20-FEB-2012 RCI    Added routines to do BalanceParts using the DB Store Procedures.
// 27-FEB-2012 RCI    Changed routine DB_BalancePartsPreCashout() to work using maths, not using Store Procedures.
// 27-FEB-2012 RCI    BalanceParts: Fixed NR1 amount when exists a WonLock.
// 09-MAR-2012 SSC    Added new Operation Codes for Note Acceptor
// 07-JUN-2012 JCM    Added redeem played by player on each provider.
//                    Added customers that their last visit was at most 365 days (Value Defined in General Params).
// 13-MAY-2013 JMM    Added next level points calculation
// 16-MAY-2013 RBG    Added function to calculate Hourly Liabilities
// 17-MAY-2013 JMM    Added GetBlockReason function
// 29-JUL-2013 RCI    Added Anti-Money Laundering support: Added functions to check account threshold and account data completeness.
// 08-AUG-2013 RCI    Fixed Bug WIG-102: If an account crossed the WarningReport limit (in one recharge), he/she can do the recharge even it's not identified.
// 09-SEP-2013 FBA    Added new block reasons
// 19-NOV-2013 JMM    DB_CheckAccountPIN modified to allow avoid PIN check
// 18-DEC-2013 DRV    AC_VIRTUAL_TERMINAL will be deleted, the information is now on the tables terminals and cashier_terminals
// 29-JAN-2014 RCI    Added CashIn tax support.
// 17-MAR-2014 JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 20-MAR-2014 SMN    Read liabilities from CASHIER_MOVEMENTS_GROUPED_BY_HOUR
// 27-MAR-2014 JBC    Added new functionality. Check First Recharge to Anonymous Accounts
// 01-ABR-2014 JBC    Fixed Bug WIG-781: Added TrackData parameter.
// 11-APR-2014 LEM    Deleted PinRequestSource.PROMOBOX logic.
// 02-JUN-2014 RRR    Fixed Bug WIG-959: PIN required in anonymous accounts when general param disables it
// 08-JUL-2014 JMM    Next Level query optimization
// 05-AUG-2014 RCI & DRV    Fixed Bug WIG-1156: Deposit value isn't updated when user sale chips.
// 17-SEP-2014 AJQ & RCI    DB_ReadNextLevel: Optimized query: Only one access to ACCOUNT_MOVEMENTS (before there were two).
// 20-OCT-2014 SMN    Added new block reason description (EXTERNAL_SYSTEM_CANCELED)
// 23-OCT-2014 SMN    Added new functionality. BlockReason is a bit mask to indicate more than one lock reason.
// 15-ENE-2014 JML    Product Backlog Item 151:AccountPointsCache --> Task 156: Cashier & GUI, DB_ReadNextLevel
// 11-MAR-2015 DHA    Added function to load Account data for mailing
// 29-APR-2015 DHA & DDM    Fixed Bug WIG-2259
// 19-JUN-2015 AMF    Backlog Item 2117
// 05-AUG-2015 YNM    TFS-3109: Annonymous account min allowed cash in amount recharge
// 01-OCT-2015 ETP    Backlog Item 4893 Add new PLD control
// 17-NOV-2015 DLL    Backlog Item 6505: Apply tax to all withdrawal. Without Devolution.
// 06-JAN-2016 SGB    Backlog Item 7910: Change column AC_POINTS to bucket 1.
// 07-JUN-2016 JBP    PBI 13253:C2GO - Implementación métodos WS Wigos
// 06-JUN-2016 PDM    Product Backlog Item 13828:Mejora Recepción - Controlar Log Errores 
// 02-AGO-2016 JRC    PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 20-OCT-2016 XGJ    Allow to redeem tickets with status Pending Printing
// 22-NOV-2016 JMM    PBI 19744:Pago automático de handpays
// 23-MAY-2017 FAV    Bug 27543:Problemas de pagos manuales
// 30-AGU-2017 DHA    Bug 29497:WIGOS-4252 [Ticket #7645] Caida del Sistema/ Severidad 1
// 21-FEB-2018 JML    Fixed Bug 31631:WIGOS-8328 [Ticket #12675] Fallo: Tiempo de computar Devolución en Mesas PG [PrizeComputationMode] Versión V03.06.0040
// 06-JUL-2018 JGC    Bug 33513:[WIGOS-13404] : Emailing Cash Summary has Liabilities values to 0 for Casino Colonial
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common.Entrances;

namespace WSI.Common
{
  enum LIABILITY_MOVEMENTS
  {
    ACCOUNT = 0,
    ACCOUNT_IN_SESSION = 1,
    TICKET = 2,
    BUCKET_START = 1000,
    BUCKET_END = 1020
  } // LIABILITY_MOVEMENTS

  public partial class Accounts
  {
    public const Int32 PIN_MIN_LENGTH = 4;
    public const Int32 PIN_MAX_SAVE_LENGTH = 4;
    public const Int32 PIN_MAX_VALIDATING_LENGTH = 12;
    private const Int32 MAX_HOURS_OPERATION_CHECK = 99999;

    [Flags]
    public enum PinRequestOperationType
    {
      WITHDRAWAL = 1,                 // 000001
      PLAY = 2,                       // 000010
      GIFT_REQUEST = 4                // 000100
    }

    public enum PinRequestSource
    {
      CASHIER = 0,
      EBOX = 1
    }

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Determine if a any requestPin flags are set in all 3 RequestPin Flags
    //  PARAMS :
    //      - INPUT :
    //          
    //      - OUTPUT :
    //
    // RETURNS :
    //   True or false  
    //
    //   NOTES :  
    // 
    public static Boolean DoPinRequestFlagsExist(Boolean IsAnonymous)
    {
      //String _subject_key;
      Boolean _request_pin;
      Int32 _pin_request_mask;

      if (IsAnonymous && GeneralParam.GetBoolean("Account", "RequestPin.DisabledForAnonymous"))
      {
        return false;
      }

      _pin_request_mask = GeneralParam.GetInt32("Account", "RequestPin.Cashier");
      _pin_request_mask |= GeneralParam.GetInt32("Account", "RequestPin.eBox");

      _request_pin = _pin_request_mask > 0;

      return _request_pin;
    } // DoPinRequestFlagsExist

    //------------------------------------------------------------------------------
    // PURPOSE : Determine if a certain operation needs to request user pin number to be completed
    //  PARAMS :
    //      - INPUT :
    //          - RequestedOperation Enum 
    //          - RequestSource Enum
    //      - OUTPUT :
    //
    // RETURNS :
    //   True or false to check if the requested accesses / operation require pin or not
    //
    //   NOTES : This method can be changed to accept a combination of operations into a single parameter. For example:
    //    you can request in the same call if the system requires for pin for withdraw and cashing gifts by adding the values of
    //    the respective enum values.
    // 
    public static Boolean DoesActionRequirePlayerPinRequest(PinRequestSource RequestSource, PinRequestOperationType RequestedOperation, Boolean IsAnonymous)
    {
      String _subject_key;
      Boolean _request_pin;
      Int32 _pin_request_mask;
      Int32 _mask_blend;

      _pin_request_mask = 0;

      if (IsAnonymous && GeneralParam.GetBoolean("Account", "RequestPin.DisabledForAnonymous"))
      {
        return false;
      }

      if (TITO.Utils.IsTitoMode() && !Misc.IsMico2Mode())
      {
        // In TITO mode, ask PIN only for gift request (in all sources: Cashier, PromoBOX or eBox).
        if (RequestedOperation != PinRequestOperationType.GIFT_REQUEST)
        {
          return false;
        }
      }

      switch (RequestSource)
      {
        case PinRequestSource.CASHIER:
          _subject_key = "RequestPin.Cashier";
          break;

        case PinRequestSource.EBOX:
          _subject_key = "RequestPin.eBox";
          break;

        default:
          Log.Message("DoesActionRequirePlayerPinRequest: Unexpected Source: " + RequestSource.ToString());
          return true;
      }

      _pin_request_mask = GeneralParam.GetInt32("Account", _subject_key);

      _mask_blend = _pin_request_mask | (Int32)RequestedOperation;

      _request_pin = _mask_blend == _pin_request_mask;

      return _request_pin;

    } // DoesActionRequirePlayerPinRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Sum up 'Cash-In' on the last 'N' hours.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - SumCashIn
    //          - IsChipsPurchaseWithCashOut
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Currency
    //
    public static Boolean SumLastHoursCashIn(Int64 AccountId, out Currency SumCashIn, Boolean IsChipsPurchaseWithCashOut, SqlTransaction SqlTrx)
    {
      int _num_hours;
      int _limit_to_opening_time;
      DateTime _date_limit;
      DateTime _today_opening;
      StringBuilder _sql_txt;
      PrizeComputationMode _prize_mode;
      TYPE_SPLITS _splits;
      Boolean _chips_sale_mode;

      SumCashIn = 0;

      try
      {
        _prize_mode = (PrizeComputationMode)GeneralParam.GetInt32("Cashier", "PrizeComputationMode");

        if (_prize_mode == PrizeComputationMode.KeepCountingInitialCashIn && !IsChipsPurchaseWithCashOut)
        {
          return true;
        }

        _chips_sale_mode = FeatureChips.CashierChipsSaleMode(false);

        _num_hours = GeneralParam.GetInt32("Cashier", "InitialCashIn.LastHours");
        _limit_to_opening_time = GeneralParam.GetInt32("Cashier", "InitialCashIn.LimitToOpeningTime");

        if (IsChipsPurchaseWithCashOut && _prize_mode != PrizeComputationMode.KeepCountingInitialCashIn)
        {
          _num_hours = GeneralParam.GetInt32("GamingTables", "InitialCashIn.LastHours", _num_hours);
          _limit_to_opening_time = GeneralParam.GetInt32("GamingTables", "InitialCashIn.LimitToOpeningTime", _limit_to_opening_time);
        }
        else if (IsChipsPurchaseWithCashOut)
        {
          _num_hours = MAX_HOURS_OPERATION_CHECK;
        }

        if (_num_hours <= 0)
        {
          return true;
        }

        _date_limit = WGDB.Now.AddHours(-_num_hours);

        // RCI 02-DEC-2010: Limit to opening time.
        if (_limit_to_opening_time != 0)
        {
          _today_opening = Misc.TodayOpening();
          if (DateTime.Compare(_date_limit, _today_opening) < 0)
          {
            _date_limit = _today_opening;
          }
        }

        if (!Split.ReadSplitParameters(out _splits))
        {
          return false;
        }

        // TODO: Atacar a Account Operations
        _sql_txt = new StringBuilder();
        if (IsChipsPurchaseWithCashOut && _chips_sale_mode)
        {
          _sql_txt.AppendLine("SELECT   ISNULL (SUM (AM_ADD_AMOUNT + AM_SUB_AMOUNT), 0) ");
        }
        else
        {
          _sql_txt.AppendLine("SELECT   ISNULL (SUM (AM_ADD_AMOUNT), 0) ");
        }
        _sql_txt.AppendLine("  FROM   ACCOUNT_MOVEMENTS WITH(INDEX(IX_am_account_id_type_datetime)) ");
        _sql_txt.AppendLine("INNER JOIN   ACCOUNT_OPERATIONS ");
        _sql_txt.AppendLine("        ON   AM_OPERATION_ID = AO_OPERATION_ID ");

        if (IsChipsPurchaseWithCashOut)
        {
          if (_chips_sale_mode)
          {
            _sql_txt.AppendLine("       AND   AO_CODE IN (@pOpCodeChipsSaleWithRecharge, @pOpCodeChipsSale) ");
        }
        else
        {
            _sql_txt.AppendLine("       AND   AO_CODE = @pOpCodeChipsSaleWithRecharge ");
        }
        }
        else
        {
          _sql_txt.AppendLine("       AND   AO_CODE IN ( @pOpCodeCashIn, @pOpCodePromo, @pOpCodeMBCashIn, @pOpCodeMBPromo, @pOpCodeNACashIn, @pOpCodeNAPromo, @pOpCodeGiftRedeeAsCashIn ) ");
        }

        _sql_txt.AppendLine(" WHERE   AM_ACCOUNT_ID  = @pAccountId ");
        _sql_txt.AppendLine("   AND   AM_DATETIME   >= @pDateLimit ");

        if (IsChipsPurchaseWithCashOut && _chips_sale_mode)
        {
          _sql_txt.AppendLine("   AND   AM_TYPE = @pMovementTypeChipsSaleTotal ");
        }
        else
        {
          _sql_txt.AppendLine("   AND   AM_TYPE = @pMovementTypeCashIn ");
        }
        _sql_txt.AppendLine("   AND   AM_OPERATION_ID > (SELECT   ISNULL (MAX (AO_OPERATION_ID), 0) ");
        _sql_txt.AppendLine("                              FROM   ACCOUNT_OPERATIONS WITH(INDEX(IX_ao_code_date_account)) ");
        _sql_txt.AppendLine("                             WHERE   AO_ACCOUNT_ID  = @pAccountId ");
        _sql_txt.AppendLine("                               AND   AO_DATETIME   >= @pDateLimit ");
        if (IsChipsPurchaseWithCashOut)
        {
          _sql_txt.AppendLine("                               AND   AO_CODE        = @pOpChipsPurchaseWithCashOut )");
        }
        else
        {
          _sql_txt.AppendLine("                               AND   AO_CODE        = @pOpCodeCashOut )");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
        {
        _sql_cmd.Parameters.Add("@pDateLimit", SqlDbType.DateTime).Value = _date_limit;
        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        _sql_cmd.Parameters.Add("@pMovementTypeCashIn", SqlDbType.Int).Value = (Int32)MovementType.CashIn;

        if (IsChipsPurchaseWithCashOut)
        {
            _sql_cmd.Parameters.Add("@pOpChipsPurchaseWithCashOut", SqlDbType.Int).Value = (Int32)OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT;
          _sql_cmd.Parameters.Add("@pOpCodeChipsSaleWithRecharge", SqlDbType.Int).Value = (Int32)OperationCode.CHIPS_SALE_WITH_RECHARGE;

            if (_chips_sale_mode)
            {
              _sql_cmd.Parameters.Add("@pOpCodeChipsSale", SqlDbType.Int).Value = (Int32)OperationCode.CHIPS_SALE;
              _sql_cmd.Parameters.Add("@pMovementTypeChipsSaleTotal", SqlDbType.Int).Value = (Int32)MovementType.ChipsSaleTotal;
        }
          }
        else
        {
          _sql_cmd.Parameters.Add("@pOpCodeCashOut", SqlDbType.Int).Value = (Int32)OperationCode.CASH_OUT;
          _sql_cmd.Parameters.Add("@pOpCodeCashIn", SqlDbType.Int).Value = (Int32)OperationCode.CASH_IN;
          _sql_cmd.Parameters.Add("@pOpCodePromo", SqlDbType.Int).Value = (Int32)OperationCode.PROMOTION;
          _sql_cmd.Parameters.Add("@pOpCodeMBCashIn", SqlDbType.Int).Value = (Int32)OperationCode.MB_CASH_IN;
          _sql_cmd.Parameters.Add("@pOpCodeMBPromo", SqlDbType.Int).Value = (Int32)OperationCode.MB_PROMOTION;
          _sql_cmd.Parameters.Add("@pOpCodeNACashIn", SqlDbType.Int).Value = (Int32)OperationCode.NA_CASH_IN;
          _sql_cmd.Parameters.Add("@pOpCodeNAPromo", SqlDbType.Int).Value = (Int32)OperationCode.NA_PROMOTION;
          _sql_cmd.Parameters.Add("@pOpCodeGiftRedeeAsCashIn", SqlDbType.Int).Value = (Int32)OperationCode.GIFT_REDEEMABLE_AS_CASHIN;
        }

        SumCashIn = (Decimal)_sql_cmd.ExecuteScalar();

          if (SumCashIn < 0)
          {
            SumCashIn = 0;
          }
        }

        // AJQ 20-SEP-2012, The "CashIn" contains only the "Empresa A" part.
        if (!_splits.prize_coupon && !IsChipsPurchaseWithCashOut)
        {
          //if (IsChipsPurchaseWithCashOut)
          //{
          //  SumCashIn = Math.Round(SumCashIn * 100.00m / _splits.company_a.chips_sale_pct, 2, MidpointRounding.AwayFromZero);
          //}
          //else
          //{
          SumCashIn = Math.Round(SumCashIn * 100.00m / _splits.company_a.cashin_pct, 2, MidpointRounding.AwayFromZero);
          //}
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SumLastHoursCashIn

    public static Boolean GetMaxDevolution(Int64 AccountId, Currency InitialCashIn, out Currency MaxDevolution, SqlTransaction Trx)
    {
      return GetMaxDevolution(AccountId, InitialCashIn, false, out MaxDevolution, Trx);
    }

    public static Boolean GetMaxDevolution(Int64 AccountId, Currency InitialCashIn, Boolean IsChipsPurchaseWithCashOut, out Currency MaxDevolution, SqlTransaction Trx)
    {
      Currency _sum_cash_in;

      MaxDevolution = 0;

      if (Misc.PrizeComputationWithoutDevolution())
      {
        return true;
      }

      if (!SumLastHoursCashIn(AccountId, out _sum_cash_in, IsChipsPurchaseWithCashOut, Trx))
      {
        return false;
      }

      if (IsChipsPurchaseWithCashOut)
      {
        MaxDevolution = _sum_cash_in;
      }
      else
      {
        MaxDevolution = Math.Max(InitialCashIn, _sum_cash_in);
      }

      return true;
    } // GetMaxDevolution

    //------------------------------------------------------------------------------
    // PURPOSE: Get all the 'Cash-In' from the LowerLimitDateTime to now.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - LowerLimitDateTime
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - AllCashIn
    //
    // RETURNS:
    //
    public static void GetCashInFromDate(Int64 AccountId,
                                         DateTime LowerLimitDateTime,
                                         out DataTable AllCashIn,
                                         SqlTransaction SqlTrx)
    {
      StringBuilder _sql_txt;

      AllCashIn = new DataTable("CASH_IN");

      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("SELECT   AO_DATETIME DATETIME ");
      _sql_txt.AppendLine("       , AO_AMOUNT   AMOUNT ");
      _sql_txt.AppendLine("       , 1.0         OFFER ");
      _sql_txt.AppendLine("  FROM   ACCOUNT_OPERATIONS ");
      if (TITO.Utils.IsTitoMode())
      {
        // DRV 18-DEC-2013: the virtual account for each terminal it's now in terminals and cashier_terminals
        _sql_txt.AppendLine(" INNER JOIN ACCOUNTS ON AC_ACCOUNT_ID = AO_ACCOUNT_ID AND AC_ACCOUNT_ID NOT IN ");
        _sql_txt.AppendLine("         ( SELECT TE_VIRTUAL_ACCOUNT_ID FROM TERMINALS  WHERE TE_VIRTUAL_ACCOUNT_ID IS NOT NULL ");
        _sql_txt.AppendLine("           UNION SELECT CT_VIRTUAL_ACCOUNT_ID FROM CASHIER_TERMINALS WHERE CT_VIRTUAL_ACCOUNT_ID IS NOT NULL )");
      }

      _sql_txt.AppendLine(" WHERE   AO_DATETIME  >= @pLowerLimitDatetime ");
      _sql_txt.AppendLine("   AND   AO_ACCOUNT_ID = @pAccountId ");
      _sql_txt.AppendLine("   AND   AO_CODE      IN ( @pOpCashIn, @pOpCashInPromo, @pMBCashIn, @pMBCashInPromo, @pNACashIn, @pNACashInPromo ) ");
      // RCI 15-NOV-2013: Only take in account Operations with UNDO_STATUS = None
      _sql_txt.AppendLine("   AND   ISNULL(AO_UNDO_STATUS, @pOperationUndoNone) = @pOperationUndoNone ");
      _sql_txt.AppendLine("ORDER BY AO_DATETIME ASC ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        _sql_cmd.Parameters.Add("@pLowerLimitDatetime", SqlDbType.DateTime).Value = LowerLimitDateTime;
        _sql_cmd.Parameters.Add("@pOpCashIn", SqlDbType.Int).Value = (Int32)OperationCode.CASH_IN;
        _sql_cmd.Parameters.Add("@pOpCashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.PROMOTION;
        _sql_cmd.Parameters.Add("@pMBCashIn", SqlDbType.Int).Value = (Int32)OperationCode.MB_CASH_IN;
        _sql_cmd.Parameters.Add("@pMBCashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.MB_PROMOTION;
        //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
        _sql_cmd.Parameters.Add("@pNACashIn", SqlDbType.Int).Value = (Int32)OperationCode.NA_CASH_IN;
        _sql_cmd.Parameters.Add("@pNACashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.NA_PROMOTION;
        _sql_cmd.Parameters.Add("@pOperationUndoNone", SqlDbType.Int).Value = (Int32)UndoStatus.None;

        using (SqlDataAdapter _sql_adap = new SqlDataAdapter(_sql_cmd))
        {
          _sql_adap.Fill(AllCashIn);
        }
      }
    } // GetCashInFromDate

    //------------------------------------------------------------------------------
    // PURPOSE: Sum all the today 'Cash-In'.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Currency
    //
    public static Currency SumTodayCashIn(Int64 AccountId)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          return SumTodayCashIn(AccountId, _db_trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return 0;
      }
    } // SumTodayCashIn

    //------------------------------------------------------------------------------
    // PURPOSE: Sum all the today 'Cash-In'.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Currency
    //
    public static Currency SumTodayCashIn(Int64 AccountId,
                                          SqlTransaction SqlTrx)
    {
      DataTable _list_cash_in;
      Object _obj;
      Currency _today_cash_in;

      _today_cash_in = 0;

      try
      {
        GetCashInFromDate(AccountId, Misc.TodayOpening(), out _list_cash_in, SqlTrx);

        _obj = _list_cash_in.Compute("SUM(AMOUNT)", "");

        if (_obj != null && _obj != DBNull.Value)
        {
          _today_cash_in = (Decimal)_obj;
        }
      }
      catch (Exception _ex)
      {
        _today_cash_in = 0;
        Log.Exception(_ex);
      }

      return _today_cash_in;
    } // SumTodayCashIn

    //------------------------------------------------------------------------------
    // PURPOSE: Check the Account PIN, and block the account if too many failures.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - PinToCheck
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - PinCheckStatus
    //
    public static PinCheckStatus DB_CheckAccountPIN(Int64 AccountId,
                                                    String PinToCheck)
    {
      StringBuilder _sql_txt;
      PinCheckStatus _status_pin;

      _status_pin = PinCheckStatus.ERROR;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE   ACCOUNTS ");
        _sql_txt.AppendLine("   SET   AC_PIN_FAILURES  = 0 ");
        _sql_txt.AppendLine(" WHERE   AC_ACCOUNT_ID    = @pAccountId ");
        _sql_txt.AppendLine("   AND   AC_BLOCKED       = 0 ");
        _sql_txt.AppendLine("   AND   ( AC_PIN         = @pPin ");
        _sql_txt.AppendLine("           OR @pCheckPin  = 0 )");
        _sql_txt.AppendLine("; ");
        _sql_txt.AppendLine("UPDATE   ACCOUNTS ");
        _sql_txt.AppendLine("   SET   AC_PIN_FAILURES  = AC_PIN_FAILURES + 1 ");
        _sql_txt.AppendLine(" WHERE   AC_ACCOUNT_ID    = @pAccountId ");
        _sql_txt.AppendLine("   AND   AC_BLOCKED       = 0 ");
        _sql_txt.AppendLine("   AND   AC_PIN          <> @pPin ");
        _sql_txt.AppendLine("   AND   @pCheckPin       = 1 ");
        _sql_txt.AppendLine("; ");
        _sql_txt.AppendLine("UPDATE   ACCOUNTS ");
        _sql_txt.AppendLine("   SET   AC_BLOCKED       = 1 ");
        _sql_txt.AppendLine("       , AC_BLOCK_REASON  = dbo.BlockReason_ToNewEnumerate(AC_BLOCK_REASON) | @pBlockReason ");
        _sql_txt.AppendLine("       , AC_PIN_FAILURES  = 0 ");
        _sql_txt.AppendLine(" WHERE   AC_ACCOUNT_ID    = @pAccountId ");
        //_sql_txt.AppendLine("   AND   AC_BLOCKED       = 0 ");
        _sql_txt.AppendLine("   AND   AC_PIN_FAILURES >= 3 ");
        _sql_txt.AppendLine("; ");
        _sql_txt.AppendLine("SELECT   CASE WHEN AC_BLOCKED = 1  THEN 2 ELSE ");
        _sql_txt.AppendLine("           CASE WHEN AC_PIN IS NULL  THEN 4 ELSE ");
        _sql_txt.AppendLine("             CASE WHEN AC_PIN <> @pPin AND @pCheckPin = 1 THEN 1 ELSE 0 END ");
        _sql_txt.AppendLine("           END");
        _sql_txt.AppendLine("         END");
        _sql_txt.AppendLine("  FROM   ACCOUNTS ");
        _sql_txt.AppendLine(" WHERE   AC_ACCOUNT_ID    = @pAccountId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          try
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
              _sql_cmd.Parameters.Add("@pPin", SqlDbType.NVarChar).Value = PinToCheck;
              _sql_cmd.Parameters.Add("@pBlockReason", SqlDbType.Int).Value = AccountBlockReason.MAX_PIN_FAILURES;
              _sql_cmd.Parameters.Add("@pCheckPin", SqlDbType.Bit).Value = 1;  //JMM 19-NOV-2013:  @pCheckPin should contain a GP value

              _status_pin = (PinCheckStatus)((Int32)_db_trx.ExecuteScalar(_sql_cmd));
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
          finally
          {
            _db_trx.Commit();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _status_pin;
    } // DB_CheckAccountPIN

    //------------------------------------------------------------------------------
    // PURPOSE: Get customers that their last visit was at most 365 days (Value Defined in General Params)
    //
    //  PARAMS:
    //      - INPUT:
    //          - Transaction     : DB Transaction
    //
    //      - OUTPUT:
    //          - Accounts        : DataTable of Accounts
    //      - RETURN: 
    //          - Boolean         : True, executed correctly
    //                            : False, there was any error
    //
    public static Boolean ReadActiveAccounts(SqlTransaction SqlTrx, out DataTable Accounts)
    {
      StringBuilder _sb;

      Accounts = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   AC_ACCOUNT_ID, AC_HOLDER_LEVEL, AC_HOLDER_BIRTH_DATE");
        _sb.AppendLine("   FROM   ACCOUNTS WITH (INDEX (IX_ac_last_activity)) ");
        _sb.AppendLine("  WHERE   AC_LAST_ACTIVITY >= @pOldestActivity");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOldestActivity", SqlDbType.DateTime).Value = Misc.TodayOpening().AddDays(-366);

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(Accounts);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //ReadActiveAccounts

    //------------------------------------------------------------------------------
    // PURPOSE: Check if exists an account
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId       : AccountId to check
    //          - Transaction     : DB Transaction
    //
    //      - OUTPUT:
    //          - Exists          : true / false
    //      - RETURN: 
    //          - Boolean         : True, executed correctly
    //                            : False, there was any error
    //
    public static Boolean ExistsAccount(Int64 AccountId, out Boolean Exists, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      object _obj;

      Exists = false;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT  1                              ");
      _sb.AppendLine("   FROM  ACCOUNTS                       ");
      _sb.AppendLine("  WHERE  AC_ACCOUNT_ID = @pAccountId    ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          _obj = _cmd.ExecuteScalar();

          if ((_obj != null) && (_obj != DBNull.Value))
          {
            Exists = true;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ExistsAccount

    //------------------------------------------------------------------------------
    // PURPOSE: Get customers data necessary for generate mailing Cash Summary
    //
    //  PARAMS:
    //      - INPUT:
    //          - Transaction     : DB Transaction
    //          - DateTime        : Date from
    //          - DateTime        : Date to
    //
    //      - REFERENCE:
    //          - TYPE_ACCOUNTS_SESSION_STATS        : Accounts Data
    //      - RETURN: 
    //          - Boolean         : True, executed correctly
    //                            : False, there was any error
    //
    public static Boolean ReadAccountsSessionData(SqlTransaction SqlTrx,
                                                  DateTime DateFrom,
                                                  DateTime DateTo,
                                                  ref TYPE_ACCOUNTS_SESSION_STATS AccountsSessionData)
    {
      StringBuilder _sb;
      Object _obj;

      AccountsSessionData = new TYPE_ACCOUNTS_SESSION_STATS();

      try
      {
        // Get accounts created in a period
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   COUNT(*) ");
        _sb.AppendLine("   FROM   ACCOUNTS ");
        _sb.AppendLine("  WHERE   AC_CREATED >= @pCreatedFrom ");
        _sb.AppendLine("    AND   AC_CREATED < @pCreatedTo ");
        // DHA & DDM    Fixed Bug WIG-2259
        _sb.AppendLine("    AND   AC_TYPE NOT IN ( @pTypeVirtualCashier, @pTypeVirtualTerminal )");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pCreatedFrom", SqlDbType.DateTime).Value = DateFrom;
          _sql_cmd.Parameters.Add("@pCreatedTo", SqlDbType.DateTime).Value = DateTo;
          _sql_cmd.Parameters.Add("@pTypeVirtualCashier", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_CASHIER;
          _sql_cmd.Parameters.Add("@pTypeVirtualTerminal", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_TERMINAL;

          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            AccountsSessionData.accounts_created = (Int32)_obj;
          }
        }

        // Get active30, active90, active180 and inactive accounts
        using (SqlCommand _sql_cmd = new SqlCommand("CustomersActivitySummary", SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;
          _sql_cmd.CommandTimeout = Mailing.SQL_COMMAND_TIMEOUT;

          _sql_cmd.Parameters.Add("@v_FECHAINI", SqlDbType.DateTime).Value = DBNull.Value;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!_sql_reader.IsDBNull(1))
              {
                AccountsSessionData.accounts_active30 = _sql_reader.GetInt32(1);
              }

              if (!_sql_reader.IsDBNull(3))
              {
                AccountsSessionData.accounts_active90 = _sql_reader.GetInt32(3);
              }

              if (!_sql_reader.IsDBNull(5))
              {
                AccountsSessionData.accounts_active180 = _sql_reader.GetInt32(5);
              }

              if (!_sql_reader.IsDBNull(7))
              {
                AccountsSessionData.accounts_inactive = _sql_reader.GetInt32(7);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //ReadAccountsSessionData

    //------------------------------------------------------------------------------
    // PURPOSE: Get customers accoutn creation date
    //
    //  PARAMS:
    //      - INPUT:
    //          - Transaction     : DB Transaction
    //          - long            : account id
    //
    //      - RETURN: 
    //          - DateTime        : Account creation date
    //
    public static DateTime GetAccountCreationDate(long AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        // Get accounts created in a period
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   AC_CREATED ");
        _sb.AppendLine("   FROM   ACCOUNTS ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccId", SqlDbType.BigInt).Value = AccountId;

          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            return (DateTime)_obj;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return default(DateTime);
    } //GetAccountCreationDate

    /// <summary>
    /// Get Account holder name
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static String GetAccountHolderName(Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      String _holder_name;

      _holder_name = String.Empty;

      try
      {
        // Get account holder name
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   ISNULL(AC_HOLDER_NAME, '')  ");
        _sb.AppendLine("   FROM   ACCOUNTS                    ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _holder_name = _reader.GetString(0);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _holder_name;
    } //GetAccountHolderName

    //------------------------------------------------------------------------------
    // PURPOSE: Get the amount of money played for each player in each Provider between IniDate and FinDate
    //
    //  PARAMS:
    //      - INPUT:
    //          - IniDate     : Minium date of played session to check
    //          - FinDate     : Maxium date of played session to check
    //          - SqlTrx      : Transaction of the queue
    //      - OUTPUT:
    //          - PlayedPerAccountAndProvider : DataTable result with columns: [AccountId] [ProviderId] [AmountRedeemPlayed]
    //      - RETURN: 
    //          - Boolean         : True, executed correctly
    //                            : False, there was any error
    //
    public static Boolean ReadPlayedPerAccountAndProvider(DateTime IniDate, DateTime FinDate, SqlTransaction SqlTrx, out DataTable PlayedPerAccountAndProvider)
    {
      StringBuilder _sb;
      PlayedPerAccountAndProvider = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("DECLARE @INI_DATE AS DATETIME");
        _sb.AppendLine("DECLARE @FIN_DATE AS DATETIME");

        _sb.AppendLine("SET @INI_DATE = @pIniDate");
        _sb.AppendLine("SET @FIN_DATE = @pFinDate");

        _sb.AppendLine("   SELECT   PS_ACCOUNT_ID");
        _sb.AppendLine("          , TE_PROVIDER_ID");
        _sb.AppendLine("          , SUM (PS_REDEEMABLE_PLAYED)");
        _sb.AppendLine("     FROM   PLAY_SESSIONS, TERMINALS");
        _sb.AppendLine("    WHERE   PS_FINISHED    >= @INI_DATE");
        _sb.AppendLine("      AND   PS_FINISHED    <  @FIN_DATE");
        _sb.AppendLine("      AND   PS_STATUS      <> @pStatusOpened");
        _sb.AppendLine("      AND   PS_TERMINAL_ID =  TE_TERMINAL_ID");
        _sb.AppendLine(" GROUP BY   PS_ACCOUNT_ID, TE_PROVIDER_ID");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pIniDate", SqlDbType.DateTime).Value = IniDate;
          _sql_cmd.Parameters.Add("@pFinDate", SqlDbType.DateTime).Value = FinDate;
          _sql_cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(PlayedPerAccountAndProvider);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadPlayedPerAccountAndProvider


    //------------------------------------------------------------------------------
    // PURPOSE: Get the amount of money played for each player in each Provider between IniDate and FinDate
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //          
    //      - RETURN: 
    //          - Boolean         : True, executed correctly
    //                            : False, there was any error
    //
    public static void GetMinExpiration(out Boolean ExpireOnDate, out DateTime MinExpiration)
    {
      Int32 _closing_time;
      DateTime _today;
      String _str_day_month;

      _closing_time = 0;
      Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("WigosGUI", "ClosingTime"), out _closing_time);
      _today = WGDB.Now.Date;
      _today = _today.AddHours((double)_closing_time);

      _str_day_month = GeneralParam.GetString("PlayerTracking", "Levels.ExpirationDayMonth");

      ExpireOnDate = false;
      MinExpiration = _today.AddDays(1); // Tomorrow

      if (!String.IsNullOrEmpty(_str_day_month))
      {
        int _idx;
        Int32 _day;
        Int32 _month;

        _str_day_month = _str_day_month.Replace("-", "/");
        _str_day_month = _str_day_month.Replace(".", "/");
        _idx = _str_day_month.IndexOf("/");

        if (_idx > 0
        && Int32.TryParse(_str_day_month.Substring(0, _idx), out _day)
        && Int32.TryParse(_str_day_month.Substring(_idx + 1), out _month))
        {
          if ((_month >= 1 && _month <= 12) && (_day >= 1 && _day <= 31))
          {
            while (_day > 0)
            {
              try
              {
                MinExpiration = new DateTime(_today.Year, _month, _day).AddHours((double)_closing_time);
                MinExpiration = MinExpiration.AddDays(1);
                ExpireOnDate = true;

                break;
              }
              catch
              {
                _day = _day - 1;
              }
            }
          }
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : UpdateHourlyLiabilities  : Calculate and SUM the Liablilites 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :  Result Succesfully
    //
    //   NOTES :
    public static Boolean UpdateHourlyLiabilities()
    {
      StringBuilder _sb;
      String _aux_sb;
      DateTime _date_now;
      Int32 _interval_days;
      Boolean _allow_pay_tickets_pending_print;

      _date_now = WGDB.Now;
      _date_now = new DateTime(_date_now.Year, _date_now.Month, _date_now.Day, _date_now.Hour, 0, 0);
      // Inteval Days =  MAX(CreditsExpireAfterDays, PrizesExpireAfterDays)  + 10 days
      _interval_days = Math.Max(GeneralParam.GetInt32("Cashier", "CreditsExpireAfterDays"), GeneralParam.GetInt32("Cashier", "PrizesExpireAfterDays")) + 10;
      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "Tickets.AllowPayPendingPrint", true);

      _sb = new StringBuilder();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb.Length = 0;

          _sb.AppendLine(" DECLARE  @_RE_BALANCE money ");
          _sb.AppendLine(" DECLARE  @_PROMO_RE_BALANCE money ");
          _sb.AppendLine(" DECLARE  @_PROMO_NR_BALANCE money ");
          _sb.AppendLine(" DECLARE  @_IN_SESSION_RE_TO_GM money ");
          _sb.AppendLine(" DECLARE  @_IN_SESSION_PROMO_RE_TO_GM money ");
          _sb.AppendLine(" DECLARE  @_IN_SESSION_PROMO_NR_TO_GM money ");
          _sb.AppendLine(" DECLARE  @_POINTS money ");
          _sb.AppendLine(" DECLARE  @_NUM_ACCOUNTS money ");
          _sb.AppendLine(" DECLARE  @_IN_SESSION_NUM_ACCOUNTS money ");

          _sb.AppendLine(" IF NOT EXISTS   (SELECT 1 FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR WHERE CM_DATE = @pBaseHour AND CM_SUB_TYPE = 2) "); // CM_SUB_TYPE: 0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES
          _sb.AppendLine(" BEGIN ");

          // Refactor applied to liabilities to set up by System.Mode
          LiabilitiesByMode(_allow_pay_tickets_pending_print, out _aux_sb);
          _sb.AppendLine(_aux_sb);

          // Saldo de los diferentes Buckets (se realiza tanto si es TITO como si no)
          _sb.AppendLine(" INSERT INTO  CASHIER_MOVEMENTS_GROUPED_BY_HOUR ");
          _sb.AppendLine("             ( CM_DATE ");
          _sb.AppendLine("             , CM_TYPE ");
          _sb.AppendLine("             , CM_SUB_TYPE ");
          _sb.AppendLine("             , CM_CURRENCY_ISO_CODE ");
          _sb.AppendLine("             , CM_CURRENCY_DENOMINATION ");
          _sb.AppendLine("             , CM_TYPE_COUNT ");
          _sb.AppendLine("             , CM_SUB_AMOUNT ");
          _sb.AppendLine("             , CM_ADD_AMOUNT ");
          _sb.AppendLine("             , CM_AUX_AMOUNT ");
          _sb.AppendLine("             , CM_INITIAL_BALANCE ");
          _sb.AppendLine("             , CM_FINAL_BALANCE ) ");
          _sb.AppendLine("        SELECT @pBaseHour ");
          _sb.AppendLine("             , @pTypeBucketStart + CBU_BUCKET_ID ");
          _sb.AppendLine("             , 2 "); //0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES
          _sb.AppendLine("             , @pCurrencyIsoCode ");
          _sb.AppendLine("             , 0 ");
          _sb.AppendLine("             , 0 ");
          _sb.AppendLine("             , 0 ");
          _sb.AppendLine("             , 0 ");
          _sb.AppendLine("             , 0 ");
          _sb.AppendLine("             , SUM(CBU_VALUE) ");
          _sb.AppendLine("             , 0 ");
          _sb.AppendLine("        FROM CUSTOMER_BUCKET ");
          _sb.AppendLine("        GROUP BY CBU_BUCKET_ID ");
          _sb.AppendLine("        HAVING SUM(CBU_VALUE) <> 0 ");

          _sb.AppendLine(" END ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = _date_now;
            _cmd.Parameters.Add("@pIntevalDays", SqlDbType.Int).Value = _interval_days;
            _cmd.Parameters.Add("@pTicketTypeCashable", SqlDbType.Int).Value = (Int32)TITO_TICKET_TYPE.CASHABLE;
            _cmd.Parameters.Add("@pTicketTypePromoRedeem", SqlDbType.Int).Value = (Int32)TITO_TICKET_TYPE.PROMO_REDEEM;
            _cmd.Parameters.Add("@pTicketTypePromoNonRedeem", SqlDbType.Int).Value = (Int32)TITO_TICKET_TYPE.PROMO_NONREDEEM;
            _cmd.Parameters.Add("@pTicketTypeHandpay", SqlDbType.Int).Value = (Int32)TITO_TICKET_TYPE.HANDPAY;
            _cmd.Parameters.Add("@pTicketTypeJackpot", SqlDbType.Int).Value = (Int32)TITO_TICKET_TYPE.JACKPOT;

            if (_allow_pay_tickets_pending_print)
            {
              _cmd.Parameters.Add("@pTicketStatusValid", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.VALID;
              _cmd.Parameters.Add("@pTicketStatusPendingPrint", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.PENDING_PRINT;
            }
            else
            {
            _cmd.Parameters.Add("@pTicketStatusValid", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.VALID;
            }

            _cmd.Parameters.Add("@pTypeAccounts", SqlDbType.Int).Value = LIABILITY_MOVEMENTS.ACCOUNT;
            _cmd.Parameters.Add("@pTypeAccountsInSession", SqlDbType.Int).Value = LIABILITY_MOVEMENTS.ACCOUNT_IN_SESSION;
            _cmd.Parameters.Add("@pTypeTicket", SqlDbType.Int).Value = LIABILITY_MOVEMENTS.TICKET;
            _cmd.Parameters.Add("@pCurrencyIsoCode", SqlDbType.NVarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
            _cmd.Parameters.Add("@pBucket", SqlDbType.Int).Value = (Int32)Buckets.BucketId.RedemptionPoints;
            _cmd.Parameters.Add("@pTypeBucketStart", SqlDbType.Int).Value = LIABILITY_MOVEMENTS.BUCKET_START;

            _db_trx.ExecuteNonQuery(_cmd);
          }

          _db_trx.Commit();
        }

        return true;
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != 2601 && _sql_ex.Number != 2627) //Not unique Key Violation
        {
          Log.Exception(_sql_ex);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }//  UpdateHourlyLiabilities

    //------------------------------------------------------------------------------
    // PURPOSE : Convert old block reason to new enumerate
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 BlockReason
    //          - Int32 Mask
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - Conversion (Int32)
    //
    public static Int32 ConvertOldBlockReason(Int32 BlockReason)
    {
      if (BlockReason > 0 && BlockReason < 256)
      {
        return (Int32)Math.Pow(2, BlockReason + 7);
      }
      else
      {
        return BlockReason;
      }
    } // ConvertOldBlockReason

    //------------------------------------------------------------------------------
    // PURPOSE : Check if a BlockReason is activated in a bit mask.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountBlockReason BlockReason
    //          - Int32 Mask
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True: BlockReason is activated in Mask
    //          - False: BlockReason is not activated in Mask
    //
    public static Boolean ExistsBlockReasonInMask(AccountBlockReason BlockReason, Int32 Mask)
    {
      return ((Int32)BlockReason != 0 && (Mask & (Int32)BlockReason) == (Int32)BlockReason);
    } // ExistsBlockReasonInMask

    public static Boolean ExistsBlockReasonInMask(List<AccountBlockReason> BlockReasonList, Int32 Mask, out AccountBlockReason WhichOneExists)
    {
      WhichOneExists = AccountBlockReason.NONE;

      foreach (AccountBlockReason _block_reason in BlockReasonList)
      {
        if (ExistsBlockReasonInMask(_block_reason, Mask))
        {
          WhichOneExists = _block_reason;

          return true;
        }
      }

      return false;

    } // ExistsBlockReasonInMask

    public static Boolean ExistsBlockReasonInMask(List<AccountBlockReason> BlockReasonList, Int32 Mask)
    {
      AccountBlockReason _which_one_exists;

      return ExistsBlockReasonInMask(BlockReasonList, Mask, out _which_one_exists);
    } // ExistsBlockReasonInMask

    //------------------------------------------------------------------------------
    // PURPOSE : Get block reason texts of a bitmask.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountBlockReason BlockReason
    //          - Int32 Mask
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - Dictionary
    //
    public static Dictionary<AccountBlockReason, String> GetBlockReason(Int32 BitMask)
    {
      Dictionary<AccountBlockReason, String> _block_reasons;
      String _block_reason_text;
      Boolean _resource_found;
      Int32 _bit_mask;

      _block_reasons = new Dictionary<AccountBlockReason, String>();

      _bit_mask = ConvertOldBlockReason(BitMask);

      foreach (AccountBlockReason _block_reason in Enum.GetValues(typeof(AccountBlockReason)))
      {
        if (ExistsBlockReasonInMask(_block_reason, _bit_mask))
        {
          switch (_block_reason)
          {
            case AccountBlockReason.EXTERNAL_SYSTEM_CANCELED:
              _block_reason_text = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "DescriptionCanceledAccount", "Cancelada por SPACE");
              break;

            case AccountBlockReason.EXTERNAL_AML:
              _block_reason_text = GeneralParam.GetString("AntiMoneyLaundering", "ExternalControl.BlockDescription", "por PLD (externo)"); ;
              break;

            default:
              _block_reason_text = Resource.String(String.Format("STR_UC_CARD_BLOCK_REASON_{0}", _block_reason.ToString()), out _resource_found);
              if (!_resource_found)
              {
                _block_reason_text = Resource.String("STR_UC_CARD_BLOCK_REASON_UNKNOWN");
              }
              break;
          }

          _block_reasons.Add(_block_reason, _block_reason_text);
        }
      } // foreach

      if (_block_reasons.Count == 0)
      {
        _block_reasons.Add(AccountBlockReason.NONE, Resource.String("STR_UC_CARD_BLOCK_REASON_UNKNOWN"));
      }

      return _block_reasons;
    } // GetBlockReason

    //------------------------------------------------------------------------------
    // PURPOSE: Check the account threshold and the account data completeness for the alert's system.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - AccountID
    //          - CreditsDirection: Defines if it is a recharge or a redeem
    //          - Amount
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - AntiMLLevel
    //          - ThresholdCrossed
    //          - AntiMLFieldsCompleted
    //
    // RETURNS: 
    //      - Boolean: True: No error. False: Error.
    //
    public static Boolean CheckAntiMoneyLaunderingFilters(Int64 AccountId,
                                                          ENUM_CREDITS_DIRECTION CreditsDirection,
                                                          Currency Amount,
                                                          SqlTransaction Trx,
                                                          out ENUM_ANTI_MONEY_LAUNDERING_LEVEL AntiMLLevel,
                                                          out Boolean ThresholdCrossed,
                                                          out Boolean AntiMLFieldsCompleted)
    {
      CardData _card_data;

      AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None;
      ThresholdCrossed = false;
      AntiMLFieldsCompleted = true;

      if (!AntiMoneyLaundering_CheckAccountThreshold(AccountId, Amount, CreditsDirection, Trx, out AntiMLLevel, out ThresholdCrossed))
      {
        return false;
      }

      if (AntiMLLevel != ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None)
      {
        _card_data = new CardData();

        if (!CardData.DB_CardGetPersonalData(AccountId, _card_data, Trx))
        {
          return false;
        }

        AntiMLFieldsCompleted = CardData.AreAntiMoneyLaunderingFieldsCompleted(_card_data);

        // RCI 08-JUL-2013: If the account fields are NOT all completed and the Level is ReportWarning, the correct level must be Identification.
        if (!AntiMLFieldsCompleted && AntiMLLevel == ENUM_ANTI_MONEY_LAUNDERING_LEVEL.ReportWarning)
        {
          AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification;
          Log.Warning(String.Format("The account fields are NOT all completed and the Level is ReportWarning. AntiMLLevel:[{0}], AccountId:[{1}], Amount:[{2}]", AntiMLLevel, AccountId, Amount));
        }
      }

      return true;
    } // CheckAntiMoneyLaunderingFilters


    //------------------------------------------------------------------------------
    // PURPOSE: Amount External control for AntimoneyLaudnering
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - TotalRedeem

    //
    //      - OUTPUT:

    // RETURNS: Total Reddemed if ExternalControl is enabled and Prize if not.

    public static Currency AmountByExternalControl(TYPE_CASH_REDEEM Redeem)
    {
      if (GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled"))
      {
        return Redeem.TotalRedeemed;
      }
      else
      {
        return Redeem.prize;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check the account threshold for the alert's system.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - AccountID
    //          - Amount
    //          - CreditsDirection: Defines if it is a recharge or a redeem
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - AntiMLLevel
    //          - ThresholdCrossed
    //
    // RETURNS: 
    //      - Boolean: True: No error. False: Error.
    //
    private static Boolean AntiMoneyLaundering_CheckAccountThreshold(Int64 AccoundId,
                                                                     Currency Amount,
                                                                     ENUM_CREDITS_DIRECTION CreditsDirection,
                                                                     SqlTransaction Trx,
                                                                     out ENUM_ANTI_MONEY_LAUNDERING_LEVEL AntiMLLevel,
                                                                     out Boolean ThresholdCrossed)
    {
      Currency _total_amount;
      Currency _base_amount;
      Currency _amount_to_check;
      Decimal _identification_warning_limit;
      Decimal _identification_limit;
      Decimal _report_warning_limit;
      Decimal _report_limit;
      Decimal _maximum_limit;
      String _gp_prefix;
      TYPE_SPLITS _splits;
      SqlParameter _sql_recharges;
      SqlParameter _sql_prizes;
      Currency _cash_in_tax;
      DataRow _dr;
      DateTime _file_update;
      Boolean _external_control_enabled;
      AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None;
      ThresholdCrossed = false;

      _external_control_enabled = GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled");

      _total_amount = 0;

      try
      {
        if (Amount == 0)
        {
          return true;
        }

        if (!GeneralParam.GetBoolean("AntiMoneyLaundering", "Enabled"))
        {
          return true;
        }

        switch (CreditsDirection)
        {
          case ENUM_CREDITS_DIRECTION.Recharge:

            _gp_prefix = "Recharge";

            if (_external_control_enabled)
            {
              _amount_to_check = Amount;
            }
            else
            {
              if (!Split.ReadSplitParameters(out _splits))
              {
                return false;
              }
              _amount_to_check = Math.Round(Amount * _splits.company_a.cashin_pct / 100, 2, MidpointRounding.AwayFromZero);

              if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled"))
              {
                _cash_in_tax = CalculateCashInTax(_amount_to_check);
                _amount_to_check -= _cash_in_tax;
              }
            }

            break;

          case ENUM_CREDITS_DIRECTION.Redeem:
            _gp_prefix = "Prize";

            _amount_to_check = Amount;
            break;

          default:
            Log.Error("Anti MoneyLaundering: Account " + AccoundId.ToString() + ", CreditsDirection unknown: " + CreditsDirection.ToString() + ".");

            return false;
        }

        _base_amount = GeneralParam.GetCurrency("AntiMoneyLaundering", "BaseAmount");
        _identification_warning_limit = GeneralParam.GetDecimal("AntiMoneyLaundering", _gp_prefix + ".Identification.Warning.Limit") * _base_amount;
        _identification_limit = GeneralParam.GetDecimal("AntiMoneyLaundering", _gp_prefix + ".Identification.Limit") * _base_amount;
        _report_warning_limit = GeneralParam.GetDecimal("AntiMoneyLaundering", _gp_prefix + ".Report.Warning.Limit") * _base_amount;
        _report_limit = GeneralParam.GetDecimal("AntiMoneyLaundering", _gp_prefix + ".Report.Limit") * _base_amount;
        _maximum_limit = GeneralParam.GetDecimal("AntiMoneyLaundering", _gp_prefix + ".MaxAllowed.Limit") * _base_amount;

        if (_external_control_enabled)
        {
          if (_amount_to_check > _identification_limit)
          {
            _dr = SelectAccounts(AccoundId, Trx);

            if (_dr["AC_EXTERNAL_AML_FILE_UPDATED"] == DBNull.Value || _dr["AC_EXTERNAL_AML_FILE_SEQUENCE"] == DBNull.Value)
            {
              AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification;
              ThresholdCrossed = true;

              Log.Message(String.Format("AntiMoneyLaundering_CheckAccountThreshold(1). AntiMLLevel:[{0}] ", AntiMLLevel));
              return true;
            }


            _file_update = (DateTime)_dr["AC_EXTERNAL_AML_FILE_UPDATED"];

            // Guid.Empty represents: 00000000-0000-0000-0000-000000000000
            if ((Guid)_dr["AC_EXTERNAL_AML_FILE_SEQUENCE"] == Guid.Empty ||
               _file_update.AddDays((Double)GeneralParam.GetInt32("AntiMoneyLaundering", "ExternalControl.FileExpirationDays")) < WGDB.Now)
            {
              AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification;
              ThresholdCrossed = true;

              Log.Message(String.Format("AntiMoneyLaundering_CheckAccountThreshold(2). AntiMLLevel:[{0}] ", AntiMLLevel));
              return true;
            }
          }

          AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None;
          ThresholdCrossed = false;

          return true;
        }
        else
        {
          using (SqlCommand _cmd = new SqlCommand("AML_AccountCurrentMonth", Trx.Connection, Trx))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = AccoundId;
            _sql_recharges = _cmd.Parameters.Add("@SplitA", SqlDbType.Decimal);
            _sql_recharges.Direction = ParameterDirection.Output;
            _sql_prizes = _cmd.Parameters.Add("@Prizes", SqlDbType.Decimal);
            _sql_prizes.Direction = ParameterDirection.Output;

            _cmd.ExecuteNonQuery();

            switch (CreditsDirection)
            {
              case ENUM_CREDITS_DIRECTION.Recharge:
                _total_amount = (Decimal)_sql_recharges.Value;
                break;

              case ENUM_CREDITS_DIRECTION.Redeem:
                _total_amount = (Decimal)_sql_prizes.Value;
                break;
            }
          }
        }

        if (_maximum_limit > 0 && _total_amount + _amount_to_check > _maximum_limit)
        {
          AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.MaxAllowed;
          ThresholdCrossed = (_total_amount <= _maximum_limit);

          Log.Message(String.Format("AntiMoneyLaundering_CheckAccountThreshold. AntiMLLevel:[{0}], ThresholdCrossed[{1}], _maximum_limit:[{2}], _total_amount:[{3}], _amount_to_check:[{4}]",
                      AntiMLLevel, ThresholdCrossed, _maximum_limit, _total_amount, _amount_to_check));
          return true;
        }

        if (_total_amount + _amount_to_check > _report_limit)
        {
          AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Report;
          ThresholdCrossed = (_total_amount <= _report_limit);

          Log.Message(String.Format("AntiMoneyLaundering_CheckAccountThreshold. AntiMLLevel:[{0}], ThresholdCrossed[{1}], _report_limit:[{2}], _total_amount:[{3}], _amount_to_check:[{4}]",
                      AntiMLLevel, ThresholdCrossed, _report_limit, _total_amount, _amount_to_check));
          return true;
        }
        if (_report_warning_limit > 0 && _total_amount + _amount_to_check > _report_warning_limit)
        {
          AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.ReportWarning;
          ThresholdCrossed = (_total_amount <= _report_warning_limit);

          Log.Message(String.Format("AntiMoneyLaundering_CheckAccountThreshold. AntiMLLevel:[{0}], ThresholdCrossed[{1}], _report_warning_limit:[{2}], _total_amount:[{3}], _amount_to_check:[{4}]",
                      AntiMLLevel, ThresholdCrossed, _report_warning_limit, _total_amount, _amount_to_check));
          return true;
        }

        if (_total_amount + _amount_to_check > _identification_limit)
        {
          AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification;
          ThresholdCrossed = (_total_amount <= _identification_limit);

          Log.Message(String.Format("AntiMoneyLaundering_CheckAccountThreshold. AntiMLLevel:[{0}], ThresholdCrossed[{1}], _identification_limit:[{2}], _total_amount:[{3}], _amount_to_check:[{4}]",
                      AntiMLLevel, ThresholdCrossed, _identification_limit, _total_amount, _amount_to_check));
          return true;
        }
        if (_identification_warning_limit > 0 && _total_amount + _amount_to_check > _identification_warning_limit)
        {
          AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.IdentificationWarning;
          ThresholdCrossed = (_total_amount <= _identification_warning_limit);

          Log.Message(String.Format("AntiMoneyLaundering_CheckAccountThreshold. AntiMLLevel:[{0}], ThresholdCrossed[{1}], _identification_warning_limit:[{2}], _total_amount:[{3}], _amount_to_check:[{4}]",
                      AntiMLLevel, ThresholdCrossed, _identification_warning_limit, _total_amount, _amount_to_check));
          return true;
        }

        AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None;
        ThresholdCrossed = false;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        AntiMLLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None;
        ThresholdCrossed = false;
      }

      return false;
    } // AntiMoneyLaundering_CheckAccountThreshold

    private static DataRow SelectAccounts(Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      DataTable _external_file;
      _external_file = new DataTable();

      try
      {
        _sb = new StringBuilder();
        // NR Part
        _sb.AppendLine("SELECT   AC_EXTERNAL_AML_FILE_SEQUENCE ");
        _sb.AppendLine("       , AC_EXTERNAL_AML_FILE_UPDATED  ");
        _sb.AppendLine(" FROM    ACCOUNTS                      ");
        _sb.AppendLine("WHERE    ac_account_id = @pAccountId   ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_external_file);
          }
        }

            if (_external_file.Rows.Count > 0)
            {
              return _external_file.Rows[0];
            }
          }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _external_file.NewRow();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Create_AML_Daily
    //
    //  PARAMS :
    //      - INPUT :
    //          - _sql_conn
    //      - OUTPUT :
    //
    // RETURNS :  Result Succesfully
    //
    //   NOTES :
    public static Boolean Create_AML_Daily()
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("AML_DailyWork", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandTimeout = 15 * 60; // 15 minutes

            _db_trx.ExecuteNonQuery(_cmd);
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }//  Create_AML_Daily

    //------------------------------------------------------------------------------
    // PURPOSE: Check if account can recharge.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - AccountID
    //          - Trx
    //
    //      - OUTPUT:
    //          - AllowRecharge
    //          - ErrorCode
    //
    // RETURNS: 
    //      - Boolean: True: No error. False: Error.
    //
    public static Boolean AccountRechargeAllowed(Int64 AccountId,
                                                 String ExternalTrackData,
                                                 Int32 AccountLevel,
                                                 Currency RedeemableAmountToAdd,
                                                 SqlTransaction Trx,
                                             out Boolean AllowRecharge,
                                             out RECHARGE_ERROR_CODE ErrorCode)
    {
      StringBuilder _str_query;
      Int32 _global_max_allowed_cash_in_count;
      Currency _global_max_allowed_cash_in_amount;
      Int32 _daily_max_allowed_cash_in_count;
      Currency _daily_max_allowed_cash_in_amount;
      String _level;
      Int32 _global_cash_in_count;
      Currency _global_cash_in_amount;
      Int32 _daily_cash_in_count;
      Currency _daily_cash_in_amount;
      Boolean _only_check_daily;
      Currency _min_cash_in_amount;

      AllowRecharge = false;

      _global_cash_in_count = 0;
      _global_cash_in_amount = 0;
      _daily_cash_in_count = 0;
      _daily_cash_in_amount = 0;
      _min_cash_in_amount = 0;

      ErrorCode = RECHARGE_ERROR_CODE.ERROR_GENERIC;

      try
      {
        if (TITO.Utils.IsTitoMode())
        {
          AllowRecharge = true;

          return true;
        }

        _level = "Level" + AccountLevel.ToString().PadLeft(2, '0');

        _global_max_allowed_cash_in_count = GeneralParam.GetInt32("Account", _level + ".Global.MaxAllowedCashIn.Count", 0, 0, Int32.MaxValue);
        _global_max_allowed_cash_in_amount = GeneralParam.GetCurrency("Account", _level + ".Global.MaxAllowedCashIn.Amount", 0);
        _daily_max_allowed_cash_in_count = GeneralParam.GetInt32("Account", _level + ".Daily.MaxAllowedCashIn.Count", 0, 0, Int32.MaxValue);
        _daily_max_allowed_cash_in_amount = GeneralParam.GetInt32("Account", _level + ".Daily.MaxAllowedCashIn.Amount", 0);
        _min_cash_in_amount = GeneralParam.GetInt32("Account", _level + ".MinAllowedCashIn.Amount", 0, 0, Int32.MaxValue);

        if (_global_max_allowed_cash_in_count == 0
            && _global_max_allowed_cash_in_amount == 0
            && _daily_max_allowed_cash_in_count == 0
            && _daily_max_allowed_cash_in_amount == 0
            && _min_cash_in_amount == 0)
        {
          AllowRecharge = true;

          return true;
        }

        _only_check_daily = _global_max_allowed_cash_in_count == 0 && _global_max_allowed_cash_in_amount == 0;

        _str_query = new StringBuilder();
        _str_query.AppendLine(" SELECT   ISNULL(SUM(CASE WHEN (AO_DATETIME >= @pTodayOpening) THEN 1 ELSE 0 END), 0)          AS DAILY_COUNT ");   // 0
        _str_query.AppendLine("        , ISNULL(SUM(CASE WHEN (AO_DATETIME >= @pTodayOpening) THEN AO_AMOUNT ELSE 0 END), 0)  AS DAILY_AMOUNT ");  // 1

        if (!_only_check_daily)
        {
          _str_query.AppendLine("        , ISNULL(SUM(1), 0)                                                                    AS GLOBAL_COUNT ");  // 2 
          _str_query.AppendLine("        , ISNULL(SUM(AO_AMOUNT), 0)                                                            AS GLOBAL_AMOUNT "); // 3 
        }

        _str_query.AppendLine("   FROM   ACCOUNT_OPERATIONS  ");
        _str_query.AppendLine("  INNER   JOIN (SELECT AM_OPERATION_ID, MAX(AM_TRACK_DATA) AS AM_TRACK_DATA FROM ACCOUNT_MOVEMENTS GROUP BY AM_OPERATION_ID) AS A ON AO_OPERATION_ID = A.AM_OPERATION_ID ");
        _str_query.AppendLine("  WHERE   AO_ACCOUNT_ID  = @pAccountID ");
        _str_query.AppendLine("    AND   AO_CODE IN ( @pOpCashIn, @pOpCashInPromo, @pMBCashIn, @pMBCashInPromo, @pNACashIn, @pNACashInPromo ) ");
        _str_query.AppendLine("    AND   AO_UNDO_STATUS IS NULL ");
        _str_query.AppendLine("    AND   AO_AMOUNT <> 0 ");

        if (GeneralParam.GetInt32("AntiMoneyLaundering", "GroupByMode", 0, 0, 3) != 0)
        {
          _str_query.AppendLine("    AND   A.AM_TRACK_DATA = @pTrackData ");
        }

        if (_only_check_daily)
        {
          _str_query.AppendLine("    AND   AO_DATETIME >= @pTodayOpening ");
        }

        using (SqlCommand _cmd = new SqlCommand(_str_query.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pOpCashIn", SqlDbType.Int).Value = (Int32)OperationCode.CASH_IN;
          _cmd.Parameters.Add("@pOpCashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.PROMOTION;
          _cmd.Parameters.Add("@pMBCashIn", SqlDbType.Int).Value = (Int32)OperationCode.MB_CASH_IN;
          _cmd.Parameters.Add("@pMBCashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.MB_PROMOTION;
          _cmd.Parameters.Add("@pNACashIn", SqlDbType.Int).Value = (Int32)OperationCode.NA_CASH_IN;
          _cmd.Parameters.Add("@pNACashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.NA_PROMOTION;
          _cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = Misc.TodayOpening();

          if (GeneralParam.GetInt32("AntiMoneyLaundering", "GroupByMode", 0, 0, 3) != 0)
          {
            _cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).Value = ExternalTrackData;
          }

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _daily_cash_in_count = _reader.GetInt32(0) + 1;
              _daily_cash_in_amount = _reader.GetDecimal(1) + RedeemableAmountToAdd;

              if (!_only_check_daily)
              {
                _global_cash_in_count = _reader.GetInt32(2) + 1;
                _global_cash_in_amount = _reader.GetDecimal(3) + RedeemableAmountToAdd;
              }
            }
          }

          if (_global_max_allowed_cash_in_count > 0 && _global_cash_in_count > _global_max_allowed_cash_in_count)
          {
            ErrorCode = RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED;

            return true;
          }

          if (_global_max_allowed_cash_in_amount > 0 && _global_cash_in_amount > _global_max_allowed_cash_in_amount)
          {
            ErrorCode = RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED;

            return true;
          }

          if (_daily_max_allowed_cash_in_count > 0 && _daily_cash_in_count > _daily_max_allowed_cash_in_count)
          {
            ErrorCode = RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED;

            return true;
          }

          if (_daily_max_allowed_cash_in_amount > 0 && _daily_cash_in_amount > _daily_max_allowed_cash_in_amount)
          {
            ErrorCode = RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED;

            return true;
          }
        }

        if (_min_cash_in_amount > 0 && _min_cash_in_amount > RedeemableAmountToAdd)
        {
          ErrorCode = RECHARGE_ERROR_CODE.ERROR_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT;

          return true;
        }

        AllowRecharge = true;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // AccountRechargeAllowed

    public static String getAMLName()
    {
      return GeneralParam.GetString("AntiMoneyLaundering", "Name", Resource.String("STR_MSG_ANTIMONEYLAUNDERING"));
    }

    public static Int64 GetOrCreateVirtualAccount(String TerminalName, SqlTransaction SqlTrx)
    {
      return GetOrCreateVirtualAccount(TerminalName, AccountType.ACCOUNT_VIRTUAL_TERMINAL, SqlTrx);
    }

    public static Int64 GetOrCreateVirtualAccount(Int32 TerminalId, SqlTransaction SqlTrx)
    {
      return GetOrCreateVirtualAccount(TerminalId, AccountType.ACCOUNT_VIRTUAL_TERMINAL, SqlTrx);
    }

    public static Int64 GetOrCreateVirtualAccount(Int32 TerminalId)
    {
      Int64 _account_id;

      _account_id = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _account_id = GetOrCreateVirtualAccount(TerminalId, AccountType.ACCOUNT_VIRTUAL_TERMINAL, _db_trx.SqlTransaction);

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _account_id;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Create virtual account for play session
    //
    //  PARAMS :
    //      - INPUT :
    //         - TerminalId
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  
    //
    //   NOTES :
    //
    public static Int64 GetOrCreateVirtualAccount(String TerminalIdentifier, AccountType AccountType, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int64 _account_id;
      Int32 _terminal_id;

      _terminal_id = 0;
      _account_id = 0;

      try
      {
        _sb = new StringBuilder();

        if (AccountType == AccountType.ACCOUNT_VIRTUAL_TERMINAL)
        {
          _sb.AppendLine(" SELECT   ISNULL(TE_TERMINAL_ID, 0)     ");
          _sb.AppendLine("   FROM   TERMINALS                     ");
          _sb.AppendLine("  WHERE   TE_EXTERNAL_ID = @pIdentifier ");
        }
        else
        {
          _sb.AppendLine(" SELECT   ISNULL(CT_CASHIER_ID, 0)     ");
          _sb.AppendLine("   FROM   CASHIER_TERMINALS            ");
          _sb.AppendLine("  WHERE   CT_NAME = @pIdentifier       ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pIdentifier", SqlDbType.NVarChar, 40).Value = TerminalIdentifier;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _terminal_id = _reader.GetInt32(0);
            }
          }
        }

        if (_terminal_id != 0)
        {
          _account_id = GetOrCreateVirtualAccount(_terminal_id, AccountType, SqlTrx);
        }
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return 0;
      }
#else
      catch
      {
        return 0;
      }
#endif

      return _account_id;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Create virtual account for play session
    //
    //  PARAMS :
    //      - INPUT :
    //         - TerminalId
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  
    //
    //   NOTES :
    //
    public static Int64 GetOrCreateVirtualAccount(Int32 TerminalId, AccountType AccountType, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _param;
      Int64 _account_id;
      String _track_data;
      Int64 _current_site_id;
      const String VIRTUAL_FLAG_TRACK_DATA = "-RECYCLED-VIRTUAL-";  // As defined in CardData.VIRTUAL_FLAG_TRACK_DATA

      _account_id = 0;

      try
      {
        _sb = new StringBuilder();

        // DRV 18-DEC-2013: the virtual account for each terminal it's now in terminals and cashier_terminals
        if (AccountType == AccountType.ACCOUNT_VIRTUAL_CASHIER)
        {
          _sb.AppendLine("    SELECT   AC_ACCOUNT_ID ");
          _sb.AppendLine("      FROM   ACCOUNTS ");
          _sb.AppendLine(" LEFT JOIN   CASHIER_TERMINALS ON AC_ACCOUNT_ID = CT_VIRTUAL_ACCOUNT_ID ");
          _sb.AppendLine("     WHERE   CT_CASHIER_ID = @pTerminalId ");
        }
        else
        {
          _sb.AppendLine("    SELECT   AC_ACCOUNT_ID ");
          _sb.AppendLine("      FROM   ACCOUNTS ");
          _sb.AppendLine(" LEFT JOIN   TERMINALS ON AC_ACCOUNT_ID = TE_VIRTUAL_ACCOUNT_ID ");
          _sb.AppendLine("     WHERE   TE_TERMINAL_ID = @pTerminalId ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _account_id = _reader.GetInt64(0);
            }
          }
        }

        if (TerminalId == 0)
        {
          return 0;
        }

        if (_account_id > 0)
        {
          return _account_id;
        }

        if (!Int64.TryParse(MultiPromos.ReadGeneralParams("Site", "Identifier", SqlTrx), out _current_site_id))
        {
          return 0;
        }

        _track_data = "00000000000000000000" + VIRTUAL_FLAG_TRACK_DATA;

        if (String.IsNullOrEmpty(_track_data))
        {
          return 0;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("DECLARE @AccountId AS BIGINT                             ");
        _sb.AppendLine("EXECUTE   CreateAccount @pAccountId OUTPUT               ");
        _sb.AppendLine("SET @AccountId = @pAccountId                             ");
        _sb.AppendLine(" UPDATE   ACCOUNTS                                       ");
        _sb.AppendLine("    SET   AC_TYPE             = @pAccountType            ");
        _sb.AppendLine("        , AC_BLOCKED          = 0                        ");
        _sb.AppendLine("        , AC_BALANCE          = 0                        ");
        _sb.AppendLine("        , AC_RE_BALANCE       = 0                        ");
        _sb.AppendLine("        , AC_CARD_PAID        = 1                        ");
        _sb.AppendLine("        , AC_TRACK_DATA       = @pTrackData + CONVERT(VARCHAR(19), @AccountId)  ");
        _sb.AppendLine(" WHERE    AC_ACCOUNT_ID = @pAccountId                    ");

        if (AccountType == AccountType.ACCOUNT_VIRTUAL_CASHIER)
        {
          _sb.AppendLine(" UPDATE   CASHIER_TERMINALS                 ");
          _sb.AppendLine("    SET   CT_VIRTUAL_ACCOUNT_ID  = @AccountId ");
          _sb.AppendLine("  WHERE   CT_CASHIER_ID       = @pVirtualId ");
        }
        else
        {
          _sb.AppendLine(" UPDATE   TERMINALS                         ");
          _sb.AppendLine("    SET   TE_VIRTUAL_ACCOUNT_ID  = @AccountId ");
          _sb.AppendLine("  WHERE   TE_TERMINAL_ID      = @pVirtualId ");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pVirtualId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pAccountType", SqlDbType.Int).Value = (Int32)AccountType;
          _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50).Value = _track_data;

          _param = _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          _sql_cmd.ExecuteNonQuery();
        }

        if (_param.Value == null || _param.Value == DBNull.Value)
        {
          return 0;
        }

        _account_id = (Int64)_param.Value;
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return 0;
      }
#else
      catch
      {
        return 0;
      }
#endif

      return _account_id;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the account id corresponding to a given external track data
    //
    //  PARAMS :
    //      - INPUT :
    //         - ExternalTrackData
    //         - SqlTrx
    //
    //      - OUTPUT :
    //         - AccountId    
    //
    // RETURNS :
    //      - True:  When AccountId is found
    //      - False:  Otherwise
    //
    //   NOTES :
    //
    public static Boolean GetAccountIdFromExternalTrackData(String ExternalTrackData, SqlTransaction SqlTrx, out Int64 AccountId)
    {
      StringBuilder _sql_txt;
      String _internal_track_data;
      Int32 _dummy;

      AccountId = 0;

      try
      {
        // is card valid --> Not found WCP_RC_WKT_CARD_NOT_VALID
        if (!CardNumber.TrackDataToInternal(ExternalTrackData, out _internal_track_data, out _dummy))
        {
          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_txt = new StringBuilder();

          _sql_txt.AppendLine("SELECT   AC_ACCOUNT_ID                 ");
          _sql_txt.AppendLine("  FROM   ACCOUNTS                      ");
          _sql_txt.AppendLine(" WHERE   AC_TRACK_DATA = @pIntTrackData");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            Object _obj;

            _sql_cmd.Parameters.Add("@pIntTrackData", SqlDbType.NVarChar, 50).Value = _internal_track_data;
            _obj = _db_trx.ExecuteScalar(_sql_cmd);

            if (_obj == null)
            {
              return false;
            }

            AccountId = (Int64)_obj;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetAccountIdFromExternalTrackData

    #endregion // Public Methods

    #region Private Methods

    #region Liabilites Queries

    /// <summary>
    /// Set up queries to calculate liabilities by system mode
    /// </summary>
    /// <param name="AllowPayTickets"></param>
    /// <param name="SB"></param>
    private static void LiabilitiesByMode(Boolean AllowPayTickets, out String SB)
    {
      SB = String.Empty;

      try
      {
        switch (Misc.SystemMode())
        {
          case SYSTEM_MODE.MICO2:
            SB = String.Format("{0}{1}", SB, AddAccounts());
            SB = String.Format("{0}{1}", SB, AddTickets(AllowPayTickets));
            break;
          case SYSTEM_MODE.TITO:
            SB = String.Format("{0}{1}", SB, AddTickets(AllowPayTickets));
            break;
          case SYSTEM_MODE.CASHLESS:
          case SYSTEM_MODE.WASS:
          case SYSTEM_MODE.GAMINGHALL:
          default:
            SB = String.Format("{0}{1}", SB, AddAccounts());
            break;
        }
      }
      catch (Exception)
      {
        Log.Error("LiabilitesByMode: Error setting query for liabilites.");
      }
    }

    /// <summary>
    /// Add account query
    /// </summary>
    /// <returns></returns>
    private static String AddAccounts()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      // Queries
      _sb.AppendLine("        SELECT @_RE_BALANCE = ISNULL( SUM ( AC_RE_BALANCE       ), 0) --AS RE_BALANCE ");
      _sb.AppendLine("             , @_PROMO_RE_BALANCE = ISNULL( SUM ( AC_PROMO_RE_BALANCE ), 0) --AS PROMO_RE_BALANCE ");
      _sb.AppendLine("             , @_PROMO_NR_BALANCE = ISNULL( SUM ( AC_PROMO_NR_BALANCE ), 0) --AS PROMO_NR_BALANCE ");
      _sb.AppendLine("             , @_IN_SESSION_RE_TO_GM = ISNULL( SUM ( CASE WHEN (ISNULL(AC_CURRENT_PLAY_SESSION_ID,0) = 0) THEN 0 ELSE AC_IN_SESSION_RE_TO_GM       END ), 0) ");
      _sb.AppendLine("             , @_IN_SESSION_PROMO_RE_TO_GM = ISNULL( SUM ( CASE WHEN (ISNULL(AC_CURRENT_PLAY_SESSION_ID,0) = 0) THEN 0 ELSE AC_IN_SESSION_PROMO_RE_TO_GM END ), 0) ");
      _sb.AppendLine("             , @_IN_SESSION_PROMO_NR_TO_GM = ISNULL( SUM ( CASE WHEN (ISNULL(AC_CURRENT_PLAY_SESSION_ID,0) = 0) THEN 0 ELSE AC_IN_SESSION_PROMO_NR_TO_GM END ), 0) ");
      _sb.AppendLine("             , @_POINTS = ISNULL( SUM ( DBO.GETBUCKETVALUE(@pBucket, AC_ACCOUNT_ID) ), 0) ");
      _sb.AppendLine("             , @_NUM_ACCOUNTS = ISNULL( SUM ( 1 ), 0) --AS ONE ");
      _sb.AppendLine("             , @_IN_SESSION_NUM_ACCOUNTS = ISNULL( SUM ( CASE WHEN (ISNULL(AC_CURRENT_PLAY_SESSION_ID, 0) = 0) THEN 0 ELSE 1 END ), 0) --AS IS_CURRENT ");
      _sb.AppendLine("        FROM   ACCOUNTS ");
      _sb.AppendLine("       WHERE   AC_LAST_ACTIVITY >= DATEADD(DAY,-@pIntevalDays, GETDATE()) ");

      _sb.AppendLine(AddCashierMovement());

      // Accounts
      _sb.AppendLine("        SELECT @pBaseHour ");
      _sb.AppendLine("             , @pTypeAccounts ");
      _sb.AppendLine("             , 2 "); //0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES
      _sb.AppendLine("             , @pCurrencyIsoCode ");
      _sb.AppendLine("             , 0 ");
      _sb.AppendLine("             , 0 ");
      _sb.AppendLine("             , @_RE_BALANCE ");
      _sb.AppendLine("             , @_PROMO_RE_BALANCE ");
      _sb.AppendLine("             , @_PROMO_NR_BALANCE ");
      _sb.AppendLine("             , @_POINTS ");
      _sb.AppendLine("             , @_NUM_ACCOUNTS ");
      _sb.AppendLine("        UNION ");

      // Accounts in session
      _sb.AppendLine("        SELECT @pBaseHour ");
      _sb.AppendLine("             , @pTypeAccountsInSession ");
      _sb.AppendLine("             , 2 "); //0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES
      _sb.AppendLine("             , @pCurrencyIsoCode ");
      _sb.AppendLine("             , 0 ");
      _sb.AppendLine("             , 0 ");
      _sb.AppendLine("             , @_IN_SESSION_RE_TO_GM ");
      _sb.AppendLine("             , @_IN_SESSION_PROMO_RE_TO_GM ");
      _sb.AppendLine("             , @_IN_SESSION_PROMO_NR_TO_GM ");
      _sb.AppendLine("             , @_POINTS ");
      _sb.AppendLine("             , @_IN_SESSION_NUM_ACCOUNTS ");

      return _sb.ToString();
    }  // AddAccounts

    /// <summary>
    /// Add tickets query
    /// </summary>
    /// <param name="AllowPayTickets"></param>
    /// <returns></returns>
    private static String AddTickets(Boolean AllowPayTickets)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      // Queries
      _sb.AppendLine("        SELECT @_RE_BALANCE = ISNULL( SUM ( CASE WHEN (TI_TYPE_ID IN (@pTicketTypeCashable, ");
      _sb.AppendLine("                                                       @pTicketTypeHandpay, ");
      _sb.AppendLine("                                                       @pTicketTypeJackpot))       THEN TI_AMOUNT ELSE 0 END ), 0) --AS RE_BALANCE ");
      _sb.AppendLine("             , @_PROMO_RE_BALANCE = ISNULL( SUM ( CASE WHEN (TI_TYPE_ID  =  @pTicketTypePromoRedeem)    THEN TI_AMOUNT ELSE 0 END ), 0) --AS PROMO_RE_BALANCE ");
      _sb.AppendLine("             , @_PROMO_NR_BALANCE = ISNULL( SUM ( CASE WHEN (TI_TYPE_ID  =  @pTicketTypePromoNonRedeem) THEN TI_AMOUNT ELSE 0 END ), 0) --AS PROMO_NR_BALANCE ");
      _sb.AppendLine("             , @_POINTS = 0 ");
      _sb.AppendLine("             , @_NUM_ACCOUNTS = 0 ");
      _sb.AppendLine("        FROM   TICKETS ");

      if (AllowPayTickets)
      {
        _sb.AppendLine("       WHERE   TI_STATUS = @pTicketStatusValid ");
        _sb.AppendLine("          OR  TI_STATUS = @pTicketStatusPendingPrint ");
      }
      else
      {
        _sb.AppendLine("       WHERE   TI_STATUS = @pTicketStatusValid ");
      }

      _sb.AppendLine(AddCashierMovement());

      // Ticket
      _sb.AppendLine("        SELECT @pBaseHour ");
      _sb.AppendLine("             , @pTypeTicket ");
      _sb.AppendLine("             , 2 "); //0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES
      _sb.AppendLine("             , @pCurrencyIsoCode ");
      _sb.AppendLine("             , 0 ");
      _sb.AppendLine("             , 0 ");
      _sb.AppendLine("             , @_RE_BALANCE ");
      _sb.AppendLine("             , @_PROMO_RE_BALANCE ");
      _sb.AppendLine("             , @_PROMO_NR_BALANCE ");
      _sb.AppendLine("             , @_POINTS ");
      _sb.AppendLine("             , @_NUM_ACCOUNTS ");

      return _sb.ToString();
    } // AddTickets

    /// <summary>
    /// Add cashier movement query
    /// </summary>
    /// <returns></returns>
    private static String AddCashierMovement()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      // Query
      _sb.AppendLine(" INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR ");
      _sb.AppendLine("             ( CM_DATE ");
      _sb.AppendLine("             , CM_TYPE ");
      _sb.AppendLine("             , CM_SUB_TYPE ");
      _sb.AppendLine("             , CM_CURRENCY_ISO_CODE ");
      _sb.AppendLine("             , CM_CURRENCY_DENOMINATION ");
      _sb.AppendLine("             , CM_TYPE_COUNT ");
      _sb.AppendLine("             , CM_SUB_AMOUNT ");
      _sb.AppendLine("             , CM_ADD_AMOUNT ");
      _sb.AppendLine("             , CM_AUX_AMOUNT ");
      _sb.AppendLine("             , CM_INITIAL_BALANCE ");
      _sb.AppendLine("             , CM_FINAL_BALANCE) ");

      return _sb.ToString();
    } // AddCashierMovement

    #endregion // Liabilities Queries

    #endregion // Private Methods
  } // Accounts

  public class AccountNextLevel
  {
    #region Members

    private Int32 m_level_id;
    private String m_level_name;
    private Points m_points_to_reach;
    private Points m_points_generated;
    private Points m_points_disc;

    #endregion // Members

    #region Properties

    public Int32 LevelId
    {
      get { return m_level_id; }
      set { m_level_id = value; }
    }

    public String LevelName
    {
      get { return m_level_name; }
      set { m_level_name = value; }
    }

    public Points PointsToReach
    {
      get { return m_points_to_reach; }
      set { m_points_to_reach = value; }
    }

    public Points PointsGenerated
    {
      get { return m_points_generated; }
      set { m_points_generated = value; }
    }

    public Points PointsDiscretionaries
    {
      get { return m_points_disc; }
      set { m_points_disc = value; }
    }

    #endregion // Properties

    #region Constructors

    public AccountNextLevel()
    {
      m_level_id = -1;
      m_level_name = "";
      m_points_to_reach = -1;
      m_points_generated = -1;
      m_points_disc = -1;
    }

    public AccountNextLevel(Int64 AccountId)
      : this(AccountId, null)
    {
    }

    public AccountNextLevel(Int64 AccountId, SqlTransaction SqlTrx)
      : this()
    {
      DB_TRX _db_trx;

      if (SqlTrx == null)
      {
        _db_trx = new DB_TRX();

        SqlTrx = _db_trx.SqlTransaction;

        if (DB_ReadNextLevel(AccountId, SqlTrx))
        {
          SqlTrx.Commit();
        }
        else
        {
          SqlTrx.Rollback();
        }
      }
      else
      {
        DB_ReadNextLevel(AccountId, SqlTrx);
      }
    }

    #endregion // Constructors

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Reloads account's next level info from DB
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId    
    //
    //      - OUTPUT:    
    //          
    //      - RETURN: 
    //          - Boolean         : True, executed correctly
    //                            : False, there was any error
    public bool Reload(Int64 AccountId)
    {
      return Reload(AccountId, null);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Reloads account's next level info from DB
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - SqlTrx    
    //
    //      - OUTPUT:    
    //          
    //      - RETURN: 
    //          - Boolean         : True, executed correctly
    //                            : False, there was any error
    public bool Reload(Int64 AccountId, SqlTransaction SqlTrx)
    {
      DB_TRX _db_trx;

      if (SqlTrx == null)
      {
        _db_trx = new DB_TRX();

        SqlTrx = _db_trx.SqlTransaction;

        if (DB_ReadNextLevel(AccountId, SqlTrx))
        {
          SqlTrx.Commit();
          return true;
        }
        else
        {
          SqlTrx.Rollback();
          return false;
        }
      }
      else
      {
        return DB_ReadNextLevel(AccountId, SqlTrx);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Sets account's next level info
    //
    //  PARAMS:
    //      - INPUT:
    //             - PointsGenerated
    //             - PointsDiscretionaries
    //             - CurrentLevelId
    //
    //      - OUTPUT:    
    //          
    //      - RETURN: 
    public void SetLevel(Points PointsGenerated, Points PointsDiscretionaries, Int32 CurrentLevelId)
    {
      Int32 _days_counting_points;
      Int32 _next_level_id;
      String _level_name;
      Points _points_to_enter;
      Points _points_left;

      _days_counting_points = GeneralParam.GetInt32("PlayerTracking", "Levels.DaysCountingPoints");

      if (CurrentLevelId == 0           //Anounymous account, there is no "next level"
         || _days_counting_points == 0)  // Points system disabled, the next level is the current
      {
        this.LevelId = CurrentLevelId;
        this.LevelName = "";
        this.PointsToReach = 0;
        this.PointsGenerated = 0;
        this.PointsDiscretionaries = 0;

        return;
      }

      _next_level_id = Math.Max(0, Math.Min(CurrentLevelId + 1, 4));  // Level id range: 0..4

      _level_name = GeneralParam.GetString("PlayerTracking", "Level0" + _next_level_id + ".Name");
      _points_to_enter = GeneralParam.GetInt32("PlayerTracking", "Level0" + _next_level_id + ".PointsToEnter");

      if (CurrentLevelId == _next_level_id)
      {
        // the account is already on the top level
        _points_left = 0;
      }
      else
      {
        _points_left = Math.Max(0, _points_to_enter - Math.Truncate(PointsGenerated + PointsDiscretionaries));
      }

      this.LevelId = _next_level_id;
      this.LevelName = _level_name;
      this.PointsToReach = _points_left;
      this.PointsGenerated = PointsGenerated;
      this.PointsDiscretionaries = PointsDiscretionaries;

    } // SetNextLevel

    #endregion // Public Functions

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Read account's next level info from DB
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - SqlTrx    
    //
    //      - OUTPUT:    
    //          
    //      - RETURN: 
    //          - Boolean         : True, executed correctly
    //                            : False, there was any error
    //
    private bool DB_ReadNextLevel(Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sql_txt;
      Int32 _current_level_id;
      Points _movements_points;
      Points _points_left;
      Points _points_generated;
      Points _points_disc;

      _current_level_id = -1;
      _movements_points = -1;
      _points_generated = -1;
      _points_disc = -1;
      _points_left = -1;

      try
      {
        _sql_txt = new StringBuilder();

        _sql_txt.Append("execute   GetAccountPointsCache ");
        _sql_txt.Append("          @pAccountId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = Misc.TodayOpening(); // _day_start_counting_points;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return false;
            }

            _current_level_id = _sql_reader.GetInt32(_sql_reader.GetOrdinal("AC_CURRENT_HOLDER_LEVEL"));
            _points_generated = _sql_reader.GetDecimal(_sql_reader.GetOrdinal("AM_POINTS_GENERATED"));
            _points_disc = _sql_reader.GetDecimal(_sql_reader.GetOrdinal("AM_POINTS_DISCRETIONARIES"));
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        this.SetLevel(_points_generated, _points_disc, _current_level_id);
      }
    }

    #endregion // Private Functions

  } // AccountNextLevel

  public class AccountBlockUnblock
  {
    #region Members

    private CardData m_card;
    AccountBlockUnblockSource m_block_reason_source;
    String m_text_description;
    Boolean m_add_to_blacklist;

    #endregion // Members

    #region Properties

    public CardData Card
    {
      get { return m_card; }
      set { m_card = value; }
    }

    public AccountBlockUnblockSource BlockReasonSource
    {
      get { return m_block_reason_source; }
      set { m_block_reason_source = value; }
    }

    public String TextDescription
    {
      get { return m_text_description; }
      set { m_text_description = value; }
    }

    public Boolean AddToBlackList
    {
      get { return m_add_to_blacklist; }
      set { m_add_to_blacklist = value; }
    }

    #endregion // Properties

    #region Constructors

    public AccountBlockUnblock(CardData Card, AccountBlockUnblockSource Type, String TextDescription, Boolean AddToBlacklist)
    {
      this.Card = Card;
      this.BlockReasonSource = Type;
      this.TextDescription = TextDescription;
      this.AddToBlackList = AddToBlackList;

    }

    #endregion // Constructors

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Check if exist external block.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public Boolean ProceedToBlockUnblockAccount(CashierSessionInfo SessionInfo)
    {
      CardData _card_data;
      return this.ProceedToBlockUnblockAccount(SessionInfo, out _card_data);
    }

    public Boolean ProceedToBlockUnblockAccount(CashierSessionInfo SessionInfo, SqlTransaction SqlTrans)
    {
      CardData _card_data;
      return this.ProceedToBlockUnblockAccount(SessionInfo, !this.Card.Blocked, SqlTrans, out _card_data);
    }

    public Boolean ProceedToBlockUnblockAccount(CashierSessionInfo SessionInfo, out CardData pCardData)
    {
      Boolean _result;
      using (DB_TRX _db_trx = new DB_TRX())
      {
        _result = this.ProceedToBlockUnblockAccount(SessionInfo, !this.Card.Blocked, _db_trx.SqlTransaction, out pCardData);

        if (_result)
        {
          _db_trx.Commit();
        }
      }

      return _result;
    }

    public Boolean ProceedToBlockUnblockAccount(CashierSessionInfo SessionInfo, Boolean Blocked, SqlTransaction SqlTrans, out CardData pCardData)
    {
      AccountBlockReason _previous_block_reason;
      CardData _card;

      pCardData = this.Card;

      _previous_block_reason = this.Card.BlockReason;
      _card = this.SetCardDataBlockReason(Blocked);

      // Block / Unblock card
      if (!CardData.DB_BlockUnblockCard(_card, SessionInfo, _previous_block_reason, SqlTrans))
      {
        return false;
      }

      // Only add cashier movements when source is Cashier
      switch (this.BlockReasonSource)
      {
        case AccountBlockUnblockSource.CASHIER:
        case AccountBlockUnblockSource.RECEPTION_BLACKLIST:
          if (!this.AddCashierMovements(SessionInfo, _card.Blocked, SqlTrans))
          {
            return false;
          }
          break;
        default:
          break;
      }

      //EOR 29-MAR-2016 Asign Value Property CardData
      pCardData = _card;

      return true;
    }


    #endregion // Public Functions

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Cashier movements manager.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private Boolean AddCashierMovements(CashierSessionInfo SessionInfo, Boolean Blocked, SqlTransaction SqlTrans)
    {
      CASHIER_MOVEMENT _cashier_movement_type;
      CashierMovementsTable _cashier_movements;

      // Set cashier movements
      _cashier_movements = new CashierMovementsTable(SessionInfo);

      // Set cashier movement type
      _cashier_movement_type = this.GetBlockReasonMovementType(Blocked);

      // Add cashier movement
      _cashier_movements.Add(0, _cashier_movement_type, 0, this.Card.AccountId, this.Card.TrackData);

      // Save cashier movements
      if (!_cashier_movements.Save(SqlTrans))
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Cashier movements manager.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public Boolean AddCashierMovementsBlacklist(CashierSessionInfo SessionInfo, CASHIER_MOVEMENT Movement, SqlTransaction SqlTrans)
    {
      if (!Misc.IsReceptionEnabled())
      {
        return true;
      }

      CASHIER_MOVEMENT _cashier_movement_type;
      CashierMovementsTable _cashier_movements;

      // Set cashier movements
      _cashier_movements = new CashierMovementsTable(SessionInfo);

      // Set cashier movement type
      _cashier_movement_type = Movement;

      // Add cashier movement
      _cashier_movements.Add(0, _cashier_movement_type, 0, this.Card.AccountId, this.Card.TrackData);

      // Save cashier movements
      if (!_cashier_movements.Save(SqlTrans))
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Set card data block reason.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private CardData SetCardDataBlockReason(Boolean Blocked)
    {
      CardData _card;

      // Don't touch the m_card object.
      _card = new CardData();

      _card.AccountId = this.Card.AccountId;
      _card.CurrentBalance = this.Card.CurrentBalance;

      //_card.Blocked = !this.Card.Blocked;
      _card.Blocked = Blocked;
      _card.BlockDescription = this.TextDescription;

      _card.BlockReason = _card.Blocked ? this.GetBlockReasonType() : AccountBlockReason.NONE;

      return _card;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get block reason type
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private AccountBlockReason GetBlockReasonType()
    {
      AccountBlockReason _block_reason;

      switch (this.BlockReasonSource)
      {
        case AccountBlockUnblockSource.C2GO:
          _block_reason = AccountBlockReason.C2GO_INTERNAL;
          break;

        case AccountBlockUnblockSource.GUI:
          _block_reason = AccountBlockReason.FROM_GUI;
          break;

        case AccountBlockUnblockSource.RECEPTION_BLACKLIST:
          _block_reason = AccountBlockReason.FROM_CASHIER | AccountBlockReason.BLACKLIST;
          break;

        default:
        case AccountBlockUnblockSource.CASHIER:
          _block_reason = AccountBlockReason.FROM_CASHIER;

          break;
      }

      if (this.AddToBlackList)
      {
        _block_reason = _block_reason | AccountBlockReason.BLACKLIST;
      }

      return _block_reason;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get cashier movement block / unblock. 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private CASHIER_MOVEMENT GetBlockReasonMovementType(Boolean Blocked)
    {
      CASHIER_MOVEMENT _block_reason;

      switch (BlockReasonSource)
      {
        case AccountBlockUnblockSource.C2GO:
          _block_reason = (Blocked) ? CASHIER_MOVEMENT.C2GO_LOCK_ACCOUNT_INTERNAL : CASHIER_MOVEMENT.C2GO_LOCK_ACCOUNT_INTERNAL;
          break;

        default:
        case AccountBlockUnblockSource.CASHIER:
        case AccountBlockUnblockSource.RECEPTION_BLACKLIST:
          _block_reason = (Blocked) ? CASHIER_MOVEMENT.ACCOUNT_BLOCKED : CASHIER_MOVEMENT.ACCOUNT_UNBLOCKED;
          break;
      }

      return _block_reason;
    }

    #endregion // Private Functions

  }

} // WSI.Common
