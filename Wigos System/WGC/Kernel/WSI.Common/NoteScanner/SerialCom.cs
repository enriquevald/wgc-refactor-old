//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SerialCom.cs
// 
//   DESCRIPTION: Class to init and use serial com port.
// 
//        AUTHOR: Xavier Iglesia & David Rigal
// 
// CREATION DATE: 24-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-JUL-2014 XIT & DRV First Release
// 30-JUL-2014 XIT   Updated Version
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using WSI.Common;
using System.IO.Ports;
using WSI.Common.NoteScanner;

namespace WSI.Common.NoteScanner
{
  public class SerialCom : ICommunication
  {
    #region Constants
    private const Byte STX = 0x02;
    private const Byte ETX = 0x03;
    private const Byte ACK = 0x06;
    private const Byte NACK = 0x15;
    #endregion

    #region Members
    private SerialConfigParams m_config_params;
    private CommunicationParams m_com_params;
    private SerialPort m_serial_port;
 
    #endregion

    #region Constructor
    public SerialCom(SerialConfigParams ConfigParams)
    {
      m_config_params = ConfigParams;
    }
    #endregion

    #region ICommunication Members

    //------------------------------------------------------------------------------
    // PURPOSE: Initializes specified port 
    //          
    //  PARAMS:
    //      - INPUT:
    //          -  CommunicationParams: Configuration port parameters
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: Port initialized and open correctly
    // 
    public Boolean Initialize(Object Params)
    {
      Int32 _baud_rate;
      Int32 _data_bits;

      _baud_rate = Int32.Parse(CommonScannerFunctions.ReturnStringBaudrate(m_config_params.BaudRate));
      _data_bits = Int32.Parse(CommonScannerFunctions.ReturnStringDatabits(m_config_params.DataBits));
      
      try
      {
        m_com_params = (CommunicationParams)Params;
        if (m_serial_port == null)
        {
          m_serial_port = new SerialPort(m_config_params.ComPort,
          _baud_rate, m_config_params.ParityCom,
          _data_bits, m_config_params.StopBits);
        }
        if (m_serial_port.IsOpen)
        {
          m_serial_port.Close();
        }
        m_serial_port.Open();
        m_serial_port.DtrEnable = true;    //Activar de Data Terminal Ready
        m_serial_port.RtsEnable = true;    //Activar Request To Send


        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks the communication port status
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: Port initialized and open correctly
    // 
    public Boolean IsInitialized()
    {
      try
      {
        return m_serial_port.IsOpen;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Sends information to configured port.
    //          
    //  PARAMS:
    //      - INPUT:
    //           -  Command: Data to send 
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: Sent properly    
    // 
    public Boolean SendCommand(String Command)
    {
      Byte[] ByteQuery;

      ByteQuery = GetBytes(Command);

      try
      {
        lock (m_serial_port)
        {
          // Clear buffer
          m_serial_port.DiscardInBuffer();

          // Send data through serial port
          m_serial_port.Write(ByteQuery, 0, ByteQuery.Length);

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Reads information from port configured. Reads until Timeout, completed message received or maximum size received.
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //            -  Command: Data received 
    //
    // RETURNS: 
    //      - Boolean: Received message    
    // 
    public Boolean ReceiveCommand(out String Command)
    {
      Byte _byte;
      Int32 _ts0;
      Int64 _elapsed;
      MemoryStream _ms;

      Boolean _full_message = false;
      _ms = new MemoryStream();
      Command = "";
      try
      {
        lock (m_serial_port)
        {
          _ts0 = Environment.TickCount;
          _elapsed = Misc.GetElapsedTicks(_ts0);

          while (_elapsed <= m_com_params.ACK_TIMEOUT && _ms.Length <= m_com_params.MAX_SIZE && (!_full_message))
          {
            if (!m_serial_port.IsOpen)
            {
              return false;
            }
            // Check if there are some data to be read
            if (m_serial_port.BytesToRead > 0)
            {
              _byte = (Byte)m_serial_port.ReadByte();

              // Check if it has received an STX character
              if (_byte.Equals(STX))
              {
                _ms = new MemoryStream();
              }

              // Check if it has received an ETX character
              if (_byte.Equals(ETX))
              {
                _full_message = true;
              }

              if ((_byte.Equals(ACK))|| (_byte.Equals(NACK)))
              {
                _full_message = true;
                _ms = new MemoryStream();
              }

              //All bytes are saved, including STX and ETX
              _ms.WriteByte(_byte);
            }
            else
            {
              System.Threading.Thread.Sleep(10);
            }

            _elapsed = Misc.GetElapsedTicks(_ts0);
          }
        }

        Command = GetString(_ms.ToArray());

        return _full_message;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Command = string.Empty;
        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Closes Serial port
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: If closed properly    
    // 
    public Boolean CloseConnection()
    {
      try
      {
        if (m_serial_port != null)
        {
          m_serial_port.DiscardInBuffer();
          m_serial_port.Close();
          m_serial_port.Dispose();
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }
    #endregion

    #region Convert Methods
    static Byte[] GetBytes(String str)
    {
      byte[] bytes = new byte[str.Length * sizeof(char)];
      System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
      return bytes;
    }

    static String GetString(Byte[] bytes)
    {
      return Encoding.UTF8.GetString(bytes);
    }
    #endregion

  }

}
