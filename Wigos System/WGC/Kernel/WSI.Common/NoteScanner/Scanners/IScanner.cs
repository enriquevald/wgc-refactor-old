//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ScannerObj.cs
// 
//   DESCRIPTION: Class that contains interfaces that must be implemented for new scanners.
// 
//        AUTHOR: Xavier Iglesia & David Rigal
// 
// CREATION DATE: 24-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-JUL-2014 XIT & DRV First Release
// 30-JUL-2014 XIT   Updated Version
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;

namespace WSI.Common.NoteScanner
{
  internal interface IScanner
  {
    event NoteCounterReceivedEventHandler NoteCounterReceivedEvent;     //IScanned message received
    Boolean Initialize();                                           //Initializes the scanner
    Boolean IsInitialized();                                        //Asks for scanner initialization
    void PauseRead();                                               //Pauses reading
    void ResumeRead();                                              //resumes reading
    Boolean CloseConnection();                                      //Closes the connection with the scanner

  }

  public interface ICommunication
  {
    Boolean Initialize(Object Params);
    Boolean IsInitialized();
    Boolean SendCommand(String Command);
    Boolean ReceiveCommand(out String Command);
    Boolean CloseConnection();
  }

  public interface IScanned
  {
    ScannedType GetScannedType();
  }

}
