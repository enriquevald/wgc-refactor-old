//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ScannerObj.cs
// 
//   DESCRIPTION: Class that contains different objects used on JetScan scanner.
// 
//        AUTHOR: Xavier Iglesia & David Rigal
// 
// CREATION DATE: 24-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-JUL-2014 XIT & DRV First Release
// 30-JUL-2014 XIT   Updated Version
// 23-SEP-2014 DRV   Deleted Status messages
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.NoteScanner
{

  public enum JetScanErrorCode
  {
    No_Call = 'A',
    Stranger_Detected = 'B',
    Strap_Limit = 'C',
    Denomination_Change = 'D',
    Suspect_Document = 'E',
    Chain_Detected = 'F',
    Double_Detected = 'G',
    Jam_Detected = 'H',
    Facing_Error = 'I',
    Orientation_Error = 'J',
    Combined_Denom_And_Strap = 'K',
    No_Count = 'L',
    Stacker_Full = 'N',
    UndefinedElements = 'X' //DRV: Undefined by Jetscan
  }

  public class JetScanError
  {
    private JetScanErrorCode m_code;
    private Char m_denomination;

    public JetScanErrorCode Code
    {
      get { return m_code; }
      set { m_code = value; }
    }

    public Char Denomination
    {
      get { return m_denomination; }
      set { m_denomination = value; }
    }

    public AlarmCode GetAlarmCode()
    {
      AlarmCode _alarm_code;
      switch (m_code)
      {
        case JetScanErrorCode.No_Call:
          _alarm_code = AlarmCode.Reader_No_Call;
          break;
        case JetScanErrorCode.Stranger_Detected:
          _alarm_code = AlarmCode.Reader_Stranger_Detected;
          break;
        case JetScanErrorCode.Strap_Limit:
          _alarm_code = AlarmCode.Reader_Strap_Limit;
          break;
        case JetScanErrorCode.Denomination_Change:
          _alarm_code = AlarmCode.Reader_Denomination_Changed;
          break;
        case JetScanErrorCode.Suspect_Document:
          _alarm_code = AlarmCode.Reader_Suspect_Document;
          break;
        case JetScanErrorCode.Chain_Detected:
          _alarm_code = AlarmCode.Reader_Chain_Detected;
          break;
        case JetScanErrorCode.Double_Detected:
          _alarm_code = AlarmCode.Reader_Double_Detected;
          break;
        case JetScanErrorCode.Jam_Detected:
          _alarm_code = AlarmCode.Reader_Jam_Detected;
          break;
        case JetScanErrorCode.Facing_Error:
          _alarm_code = AlarmCode.Reader_Facing_Error;
          break;
        case JetScanErrorCode.Orientation_Error:
          _alarm_code = AlarmCode.Reader_Orientation_Error;
          break;
        case JetScanErrorCode.Combined_Denom_And_Strap:
          _alarm_code = AlarmCode.Reader_Combined_Denomination_Change_Strap_Limit;
          break;
        case JetScanErrorCode.Stacker_Full:
          _alarm_code = AlarmCode.Reader_Stacker_Full;
          break;
        case JetScanErrorCode.UndefinedElements:
          _alarm_code = AlarmCode.Reader_Undefined_Elements;
          break;
        case JetScanErrorCode.No_Count:
          _alarm_code = AlarmCode.Reader_No_Count;
          break;

        default:
          _alarm_code = AlarmCode.Unknown;
          break;
      }
      return _alarm_code;
    }

    public AlarmSeverity GetAlarmSeverity()
    {
      AlarmSeverity _alarm_severity;
      
      switch (m_denomination)
      {
        case ((char)JetScanErrorCode.No_Call):
        case ((char)JetScanErrorCode.Stranger_Detected):
        case ((char)JetScanErrorCode.Strap_Limit):
        case ((char)JetScanErrorCode.Denomination_Change):
        case ((char)JetScanErrorCode.Suspect_Document):
        case ((char)JetScanErrorCode.Chain_Detected):
        case ((char)JetScanErrorCode.Double_Detected):
        case ((char)JetScanErrorCode.Jam_Detected):
        case ((char)JetScanErrorCode.Facing_Error):
        case ((char)JetScanErrorCode.Orientation_Error):
        case ((char)JetScanErrorCode.Combined_Denom_And_Strap):
        case ((char)JetScanErrorCode.Stacker_Full):
        case ((char)JetScanErrorCode.UndefinedElements):
        case ((char)JetScanErrorCode.No_Count):
          {
            _alarm_severity = AlarmSeverity.Warning;
            break;
          }
        default:
          _alarm_severity = AlarmSeverity.Error;
          break;
      }

      return _alarm_severity;
    }
  }

  public class NoteCounterJetScan4091Info : NoteCounterInfo
  {
    public SerialConfigParams m_serial_config;
    public List<AcceptedCurrencies> m_currencies_accepted;
    public String m_configured_currency;

    public NoteCounterJetScan4091Info(SerialConfigParams SerialConfig, List<AcceptedCurrencies> CurrenciesAccepted, String ConfiguredCurrency)
      : base()
    {
      m_serial_config = SerialConfig;
      m_currencies_accepted = CurrenciesAccepted;
      m_configured_currency = ConfiguredCurrency;
    }

    public SerialConfigParams SerialConfig
    {
      get { return m_serial_config; }
      set { m_serial_config = value; }
    }

    public List<AcceptedCurrencies> CurrenciesAccepted
    {
      get { return m_currencies_accepted; }
      set { m_currencies_accepted = value; }
    }

    public String ConfiguredCurrency
    {
      get { return m_configured_currency; }
      set { m_configured_currency = value; }
    }
  }

  //Unused
  public enum JetScanOperatingMode
  {
    Count = 'H',
    Stranger = 'I',
    Stranger_Face = 'J',
    Stranger_Orient = 'K',
    Mixed = 'L',
    Mixed_Face = 'M',
    Mixed_Orient = 'N',
    Sort = 'O',
    Sort_Face = 'P',
    Sort_Orient = 'Q',
    OffSort = 'R'
  }

  
  /// <summary>
  /// Contains JetScan Status. Unused
  /// </summary>
  internal class JetScanInfo
  {
    #region Fields
    private JetScanOperatingMode m_operating_mode;
    
    #endregion

    #region properties
    public JetScanOperatingMode OperatingMode
    {
      get { return m_operating_mode; }
      set { m_operating_mode = value; }
    }

    
    #endregion

  }



}
