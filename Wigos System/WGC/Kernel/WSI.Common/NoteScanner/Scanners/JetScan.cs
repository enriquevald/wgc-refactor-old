using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using WSI.Common.NoteCounter;

namespace WSI.Common.NoteScanner.Scanners
{
  public class JetScan : IScanner
  {
    #region Commands
    private const Byte STX = 0x02;
    private const Byte ETX = 0x03;
    private const Byte ACK = 0x06;
    private const Byte NACK = 0x15;
    private const Byte FS = 0x1C;


    private const String STATUS = "S";
    private const String POWER_UP = "M";

    private const Int32 ACK_TIMEOUT = 100;
    private const Int32 RESPONSE_TIMEOUT = 10000;
    private const Int32 MAX_SIZE = 1024;

    private const Int32 VALUE_TO_CALCULATE_CHECKSUM = 32; //Hexadecimal
    #endregion


    //Events
    public event TicketReceivedEventHandler TicketsReceivedEvent;
    public event CurrenciesReceivedEventHandler CurrenciesReceivedEvent;
    public event ErrorReceivedEventHandler ErrorReceivedEvent;

    private ManualResetEvent ev_sort = new System.Threading.ManualResetEvent(false);
    private ManualResetEvent ev_close = new System.Threading.ManualResetEvent(false);

    //Handlers
    //private NoteCounterSortingHandler m_sorting_handler = null;
    //private NoteCounterStatusHandler m_status_handler = null;

    SerialCommunication m_serial_config;
    CommunicationParams m_jetscan_config;
    Thread m_counter;

    public JetScan()
    {
      EncapsulateJetScanConfig();
      m_serial_config = new SerialCommunication(m_jetscan_config);
    }

    //public JetScan(NoteCounterStatusHandler StatusHandler)
    //{
    //  this.m_status_handler = StatusHandler;
    //}

    private void EncapsulateJetScanConfig()
    {
      m_jetscan_config = new CommunicationParams();
      m_jetscan_config.ACK_TIMEOUT = ACK_TIMEOUT;
      m_jetscan_config.ACK = ACK;
      m_jetscan_config.NACK = NACK;
      m_jetscan_config.STX = STX;
      m_jetscan_config.ETX = ETX;
      m_jetscan_config.MAX_SIZE = MAX_SIZE;
      m_jetscan_config.RESPONSE_TIMEOUT = RESPONSE_TIMEOUT;
    }

    //Polling thread
    private void Thread()
    {
      Boolean m_stop_thread;
      Int32 _index, _waitHint;
      WaitHandle[] _events;
      NoteCounterStatusEvent _status_event;
      JetScanInfo _status;

      m_stop_thread = false;
      _status_event = new NoteCounterStatusEvent();
      _status_event.Error = new NoteCounterError();
      _events = new WaitHandle[] { ev_sort, ev_close };
      _waitHint = 0; // Initial wait time set to 0s

      while (!m_stop_thread)
      {
        try
        {
          // Reset sort event to force wait
          ev_sort.Reset();

          // Whait for an event or timeout
          _index = WaitHandle.WaitAny(_events, _waitHint, false);

          // Check if has occurred a close event
          if (ev_close.WaitOne(0))
          {
            m_serial_config.CloseConnection();
            m_stop_thread = true;
            continue;
          }

          m_serial_config.OpenPort();

          //if (!GetJetScanStatus(out _status) || IsJetScanErrorStatus(_status))
          //{
          //  //throw error
          //  //log errror
          //  //continue
          //}
          //else
          //{
          if (GetMessage())
          {
            // Set wait time 5s
            _waitHint = 5000;
            //reading at this moment
            //ListeningForInfo();
          }

          // Check if has occurred a sorting event
          if (ev_sort.WaitOne(0))
          {
            //ListeningForInfo();
            TicketReceived _my_ticket = new TicketReceived();
            //if readed ticket message(out _my_ticket):
            List<TicketReceived> _mylist = new List<TicketReceived>();
            _mylist.Add(_my_ticket);
            TicketsReceivedEvent(_mylist);
          }

          else
          {
            //if (IsJetScanIdle(_status))
            {
              //waiting to start
              // Check if machine is Idle
              // Set wait time 1s
              _waitHint = 1000;
              continue;
            }
          }

          //}
        }
        catch (UnauthorizedAccessException ex)
        {
          //Log
          //_status_event.Status = NOTE_COUNTER_STATUS.ERROR;
          //_status_event.Error.ErrorPriority = NOTE_COUNTER_ERROR_PRIORITY.FATAL;
          //_status_event.Error.ErrorMessage = ex.Message;
          //_status_event.Error.ErrorAction = "Check if it's connected to right port.";
          //m_status_handler(_status_event);

          // Set wait time 5s
          _waitHint = 5000;
        }
        catch (Exception ex)
        {
          // Log
          //_status_event.Status = NOTE_COUNTER_STATUS.ERROR;
          //_status_event.Error.ErrorPriority = NOTE_COUNTER_ERROR_PRIORITY.FATAL;
          //_status_event.Error.ErrorMessage = ex.Message;
          //m_status_handler(_status_event);

          // Set wait time 10s
          _waitHint = 10000;
        }
      }
    }

    private Boolean GetMessage()
    {
      byte[] _byte_message;
      String _message;
      if (ReceiveCommand(out _byte_message))
      {
        _message = Encoding.UTF8.GetString(_byte_message, 0, _byte_message.Length);
        _message = RemoveTrxParameters(_message);
        //First character can be ' ' or '_'
        if (char.IsLower(_message[0]))
        {
          List<CurrencyReceived> _currencies_list;
          ParseCurrenciesMessage(_message, out _currencies_list);
          CurrenciesReceivedEvent(_currencies_list);
          return true;
        }
        if (char.IsUpper(_message[0]))
        {
          ErrorReceived _error_received;
          JetScanError _jet_scan_error;

          ParseErrorMessage(_message, out _jet_scan_error);

          _error_received = new ErrorReceived(_jet_scan_error.ToString());
          ErrorReceivedEvent(_error_received);

          return true;
        }

        if (char.IsNumber(_message[0]))
        {
          //Status MESSAGE
          return true;
        }
        if (_message[0] == '_')
        {
          TicketReceived _ticket;
          List<TicketReceived> _ticket_list = new List<TicketReceived>();
          ParseTicketMessage(_message, out _ticket);
          _ticket_list.Add(_ticket);
          TicketsReceivedEvent(_ticket_list);
          return true;
        }
      }
      return false;
    }


    private Boolean IsJetScanErrorStatus(JetScanInfo _status)
    {
      return _status.HopperStatus != HopperStatus.Unblocked ||
             _status.StackerStatus != StackerStatus.Unblocked;
    }

    private Byte[] FormatCommand(String Query)
    {
      MemoryStream _ms;
      Byte[] _byte_query;

      _ms = new MemoryStream();

      // Encode query 
      _ms.WriteByte(STX);
      _ms.Write(Encoding.ASCII.GetBytes(Query), 0, Query.Length);
      _ms.WriteByte(ETX);
      _byte_query = _ms.ToArray();
      return _byte_query;
    }


    private Boolean IsJetScanReading(JetScanInfo _status)
    {
      return _status.HopperStatus == HopperStatus.Unblocked && _status.StackerStatus == StackerStatus.Unblocked && _status.MotorStatus == MotorStatus.Running;
    }


    private static Boolean IsJetScanIdle(JetScanInfo _status)
    {
      return _status.HopperStatus == HopperStatus.Unblocked && _status.StackerStatus == StackerStatus.Unblocked && _status.MotorStatus == MotorStatus.At_Rest;
    }

    private static String RemoveTrxParameters(String _message)
    {
      _message = _message.Remove(0, 1);                   //Extract STX 
      _message = _message.Remove(_message.Length - 1, 1); //Extract ETX 
      _message = _message.Trim(' ');                        //Extract initial and final white-spaces

      return _message;
    }

    //TRX Actualiza limites de billetes
    private Boolean SetStrapLimits()
    {
      //GET FROM GENERAL PARAMS in aXXX;bXXX;cXXX;dXXX;eXX;fXXX;gXXX format. Range values between 0 - 100. 0 means No limit
      String _strap_limits;
      _strap_limits = "a0;b0;c0;d0;e0;f0;g0";
      _strap_limits = _strap_limits.Replace(";", "");
      _strap_limits = _strap_limits.PadLeft(_strap_limits.Length + 1, ' ');
      _strap_limits = _strap_limits.PadRight(_strap_limits.Length + 1, ' ');
      return SendValidatedCommand(_strap_limits);
    }

    //TRX Cambia modo de operación de la maquina
    private Boolean SetOperatingMode(JetScanOperatingMode NewOperatingMode)
    {
      return SendValidatedCommand(NewOperatingMode.ToString());
    }

    //TRX Consulta el estado de la maquina
    private Boolean GetJetScanStatus(out JetScanInfo Status)
    {
      Byte[] _response;
      String _message;

      Status = null;
      if (SendCommand(STATUS))
      {
        if (ReceiveCommand(out _response))
        {
          Status = new JetScanInfo();
          _message = Encoding.UTF8.GetString(_response, 0, _response.Length);
          _message = RemoveTrxParameters(_message);

          Status.HopperStatus = ((HopperStatus)Int32.Parse(_message[0].ToString()));
          Status.StackerStatus = ((StackerStatus)Int32.Parse(_message[0].ToString()));
          Status.MotorStatus = ((MotorStatus)Int32.Parse(_message[0].ToString()));
        }
      }

      return (Status != null);
    }

    //Parsea mensaje con ticket recibido 
    private Boolean ParseTicketMessage(String Message, out TicketReceived Ticket)
    {
      String _message;
      Ticket = null;

      try
      {
        //Message composed by: 2 '_' + 18 'ticketid' + 10 'value' 
        if (Message.Length == 30)
        {
          Ticket = new TicketReceived();
          _message = Message.Remove(0, 2);                                    //Extract __ 
          Ticket.ValidationNumber = Int64.Parse(_message.Substring(0, 18));   //Get Validation number
          Ticket.Amount = Int64.Parse(_message.Substring(18, 10));            //Retrieves Ticket Amount. TODO: Check if better define amount as decimal
        }
        else
        {
          //log incorrect ticket message size
          return false;
        }
        return true;
      }
      catch (Exception Ex)
      {
        //Log Ex;
        return false;
      }
    }

    //Parsea Mensaje con billetes recibido
    private Boolean ParseCurrenciesMessage(String Message, out List<CurrencyReceived> _list_currencies)
    {
      try
      {
        _list_currencies = new List<CurrencyReceived>();
        CurrencyReceived _currency;
        Boolean EndOfMessage = false;
        Int32 _position = 0;
        string[] parts = Regex.Split(Message, @"([abcdefg])"); //TODO: OPTIMIZE
        while (!EndOfMessage)
        {
          if (parts[_position].Length > 0 && (char.IsLetter(parts[_position][0])))
          {
            _currency = new CurrencyReceived();
            switch (parts[_position][0])
            {
              case 'a': { _currency.Amount = 1; break; }
              case 'b': { _currency.Amount = 2; break; }
              case 'c': { _currency.Amount = 5; break; }
              case 'd': { _currency.Amount = 10; break; }
              case 'e': { _currency.Amount = 20; break; }
              case 'f': { _currency.Amount = 50; break; }
              case 'g': { _currency.Amount = 100; break; }
              default: { continue; }
            }
            _position++;
            _currency.Quantity = Int32.Parse(parts[_position]);
            _list_currencies.Add(_currency);
          }
          _position++;
          if (_position >= parts.Length) EndOfMessage = true;
        }

        return true;
      }
      catch (Exception Ex)
      {
        //Log Ex
        throw Ex;
      }
    }


    //Parsea mensaje de error recibido
    private Boolean ParseErrorMessage(String Message, out JetScanError ErrorCode)
    {
      ErrorCode = new JetScanError();
      if (Message.Length > 0) 
      {
        ErrorCode.Code = ((JetScanErrorCode)Message[0]);

      }

      ErrorCode.Denomination = Message[1];

      return true;
    }

    private Boolean SendCommand(String Query)
    {
      try
      {
        Boolean _result;
        Byte[] _formatted_query;

        _formatted_query = FormatCommand(Query);
        _result = m_serial_config.SendCommand(_formatted_query);

        return _result;
      }
      catch (Exception Ex)
      {
        //log
        return false;
      }
    }

    private Boolean SendValidatedCommand(String Query)
    {
      try
      {
        Boolean _result;
        Byte[] _formatted_query;

        _formatted_query = FormatCommand(Query);
        _result = m_serial_config.SendValidatedCommand(_formatted_query);

        return _result;
      }
      catch (Exception Ex)
      {
        //log
        return false;
      }
    }

    public Boolean ReceiveCommand(out Byte[] Message)
    {
      MemoryStream _ms;
      Boolean _received;
      Boolean _full_message;
      Int32 _retries;
      //Int32 _max_bytes;

      _received = false;
      _retries = 0;
      //_max_bytes = 1024;
      _ms = new MemoryStream();

      Message = null;
      while ((_retries < 1000) && (!_received))
      {
        _full_message = m_serial_config.ReceiveCommand(ref  _ms);

        if (_full_message && CheckSum(_ms.ToArray()))
        {
          _received = true;
          Message = _ms.ToArray();
        }
        _retries++;
      }

      //****JETSCAN DOESN'T REQUIRE ACK RESPONSE****
      //if (_received)
      //{
      //  m_serial_config.AnswerReceivedCommand(new Byte[1] { ACK });
      //}
      //else
      //{
      //  m_serial_config.AnswerReceivedCommand(new Byte[1] { NACK });
      //}

      return _received;
    }


    // PURPOSE: CheckSum of a received message
    //
    //  PARAMS:
    //      - INPUT: Message to calculate checksum
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True if LRC is equal, False otherwise
    //
    private Boolean CheckSum(Byte[] Message)
    {

      Byte _LRC;
      MemoryStream _ms;

      _LRC = new Byte();
      _ms = new MemoryStream();

      try
      {
       
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean CheckSum(string Message)
    {
      byte[] _message_in_bytes;
      byte[] _checksum_in_bytes;
      byte[] _original_checksum;
      string _binaryData;
      string _checksum;
      const int TOSUBSTRACT = 32;

      _binaryData = "";
      _checksum = "#1";
      _original_checksum = new byte[2];

      _checksum_in_bytes = Encoding.ASCII.GetBytes(_checksum);
      _message_in_bytes = Encoding.ASCII.GetBytes(Message);

      if (_message_in_bytes.Length > 3) //checksum is in 2nd and 3th position so at least it has to have lenght = 3
      {
        _original_checksum[0] = _message_in_bytes[1]; //Hight part
        _original_checksum[1] = _message_in_bytes[2]; //Low part
      }
      else
        return false;

      for (int i = 0; i < _checksum_in_bytes.Length; i++)
      {
        _checksum_in_bytes[i] = (byte)(_checksum_in_bytes[i] - TOSUBSTRACT);
      //  _binaryData += CompleteBits(Convert.ToString(_checksum_in_bytes[i], 2));
      }

      return _original_checksum.Equals(_checksum_in_bytes);
      //Alarm.Register(
    }


    private string CompleteBits(string OriginalBits)
    {
      string _fixed_bits;

      _fixed_bits = OriginalBits;

      while (_fixed_bits.Length < 6)
      {
        _fixed_bits = "0" + _fixed_bits;
      }

      return _fixed_bits;
    }

    // PURPOSE: Generate LRC value to be used with Checksum operations
    //
    //  PARAMS:
    //      - INPUT: Byte array to calculate LRC value
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Byte LRC value
    //
    private static Byte CalculateLRC(Byte[] Bytes)
    {
      Byte LRC = 0;
      foreach (Byte _byte in Bytes)
      {
        LRC ^= _byte;
      }

      return LRC;
    }

    #region IScannerModel Members
    void IScanner.Initialize()
    {
      m_serial_config.Initialize();

      // Create new Thread
      m_counter = new Thread(this.Thread);
      m_counter.Start();
    }

    void IScanner.StartScanning()
    {
      SetOperatingMode(JetScanOperatingMode.OffSort);
      //MUST ESTABLISH TICKETS AS TARGET
      //MUST ESTABLISH JTCH AS PROTOCOL
      ev_sort.Set();
    }

    void IScanner.StopScanning()
    {
    }

    void IScanner.CloseScanner()
    {

    }
    #endregion

    #region IScanner Members


    #endregion
  }
}
