//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JetScan4091.cs
// 
//   DESCRIPTION: Class that controls comunication with JetScan 4091 scanner
// 
//        AUTHOR: Xavier Iglesia & David Rigal
// 
// CREATION DATE: 24-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-JUL-2014 XIT & DRV First Release
// 06-AUG-2014 XIT   Updated Version
// 09-SEP-2014 HBB   Added list of accepted currencies 
// 23-SEP-2014 DRV   Deleted status messages
// 30-SEP-2014 DRV   Fixed Bug WIG-1328; Multicurrency not supported
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using WSI.Common.NoteScanner;

namespace WSI.Common.NoteScanner
{
  public class JetScan4091 : IScanner
  {
    #region Constants
    private const Byte STX = 0x02;
    private const Byte ETX = 0x03;
    private const Byte ACK = 0x06;
    private const Byte NACK = 0x15;
    private const Int32 ACK_TIMEOUT = 250;     //Timeout for external ack responses
    private const Int32 NUM_RETRIES = 3;       //Total timeout = ACK_TIMEOUT x NUM_RETRIES
    private const Int32 MAX_SIZE = 1024;
    #endregion

    #region Commands
    private const String STATUS = "S";         //Request status to billcounter
    private const String POWER_UP = "M";       //After power up billcounter this command is sent
    #endregion

    #region Fields
    public event NoteCounterReceivedEventHandler NoteCounterReceivedEvent;

    private ICommunication m_communication;   //Specifies communication type: "FileCom" / "SerialCom"
    private CommunicationParams m_jetscan_config;
    private Thread m_thread;
    private Boolean m_stop_counting;

    private ManualResetEvent ev_shutdown = new ManualResetEvent(false);
    private ManualResetEvent ev_resume = new ManualResetEvent(false);
    private List<AcceptedCurrencies> m_currencies_accepted;
    private String m_configured_currency;
    #endregion

    public JetScan4091(NoteCounterJetScan4091Info ConfigInfo)
    {
      String _channel;

      _channel = "";

      switch (ConfigInfo.Communication)
      {
        case NoteCounterCommunication.Serial:
          _channel = "SerialCom";
          break;
        case NoteCounterCommunication.File:
          _channel = "FileCom";
          break;
      }

      Type _scanner_communication_type = Type.GetType("WSI.Common.NoteScanner." + _channel);
      m_communication = (ICommunication)Activator.CreateInstance(_scanner_communication_type, new object[] { ConfigInfo.m_serial_config });

      m_jetscan_config = new CommunicationParams();
      m_jetscan_config.ACK_TIMEOUT = ACK_TIMEOUT;
      m_jetscan_config.MAX_SIZE = MAX_SIZE;
      m_currencies_accepted = ConfigInfo.m_currencies_accepted;
      m_configured_currency = ConfigInfo.ConfiguredCurrency;
      for (int _idx = 0; _idx <= m_currencies_accepted.Count -1; _idx++)
      {
        if(m_currencies_accepted[_idx].Currency != ConfigInfo.ConfiguredCurrency)
        {
          m_currencies_accepted.RemoveAt(_idx);
          _idx--;
        }
      }

    }

    #region IScanner Members

    //------------------------------------------------------------------------------
    // PURPOSE: Initializes specified communication mode and initiates thread to read communicated info
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: Initialized properly
    // 
    public Boolean Initialize()
    {
      try
      {
        if (!m_communication.Initialize(m_jetscan_config)) 
        {
          return false;
        }

        m_stop_counting = false;
        ev_resume.Set();

        if (m_thread == null || (m_thread.ThreadState != ThreadState.Running && m_thread.ThreadState != ThreadState.WaitSleepJoin))
        {
          m_thread = new Thread(this.Thread);
          m_thread.Name = "JetScanThread";
          m_thread.Start();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks the thread that receives the communicated information and the communication state
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: If is running properly 
    // 
    public Boolean IsInitialized()
    {
      if (m_thread == null)
      {
        return false;
      }

      return (m_communication.IsInitialized() && m_thread.IsAlive);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Ends thread and communication used
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: If closed properly    
    // 
    public Boolean CloseConnection()
    {
      m_stop_counting = true;
      ev_resume.Set();
      m_thread = null;

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Pause thread blocking until resumes or ends.
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    public void PauseRead() 
    {
      ev_resume.Reset();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: resume thread starting to read events
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    public void ResumeRead()
    {
      ev_resume.Set();
    }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Reads for a input message
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //            - Message: message received
    //
    // RETURNS: 
    //      - Boolean: If message was received
    // 
    private Boolean ReceiveCommand(out String Message)
    {
      String _full_message;
      Int32 _retries;
      Boolean _received_message;

      _received_message = false;
      _retries = 0;
      _full_message = String.Empty;
      Message = null;

      while (_retries < NUM_RETRIES)
      {
        _received_message = m_communication.ReceiveCommand(out _full_message);
        Message += _full_message;
        if (_received_message) break;

        _retries++;
      }

      return _received_message;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks and fixes the status of the communication if needed, reads for a message periodically and notifies
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    private void Thread()
    {
      Int32 _wait_hint = 0;

      try
      {
        while (!this.m_stop_counting)
        {
          System.Threading.Thread.Sleep(_wait_hint);

          if (!m_communication.IsInitialized())
          {
            if (!m_communication.Initialize(m_jetscan_config))
            {
              //ErrorReceived _error = new ErrorReceived("Notecounter connection lost");
              ErrorReceived _error = new ErrorReceived();
              _error.AlarmCode = AlarmCode.Reader_Comunication_Error;
              _error.Severity = AlarmSeverity.Warning;

              NoteCounterReceivedEvent(_error);
            }
            continue;
          }

          if (ReadMessage()) _wait_hint = 60;
          else  _wait_hint = 100;

          ev_resume.WaitOne(); //blocks thread if paused and only resumes if resume or shutdown.
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        try
        {
          //Try to notify the exception
          //ErrorReceived _error = new ErrorReceived(ex.Message);
          ErrorReceived _error = new ErrorReceived();
          _error.AlarmCode = AlarmCode.Unknown;
          _error.Severity = AlarmSeverity.Error;

          NoteCounterReceivedEvent(_error);
        }
        catch { ;}
      }
      finally
      {
        try
        {
          if (!m_communication.CloseConnection())
          {
            Log.Error(String.Format("Cannot Close Port Connection"));
          }
        }
        catch { ;}
      }
    }

    #region Process ReceivedMessage

    //------------------------------------------------------------------------------
    // PURPOSE: Try to read an input message, process and notify
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: If message was received
    // 
    private Boolean ReadMessage()
    {
      String _message = String.Empty;
      try
      {
        if (ReceiveCommand(out _message))
        {
          _message = RemoveTrxParameters(_message);

          if (_message.Length > 0)
          {
            if (ProcessCurrencyMessage(_message)) return true;
            if (ProcessTicketMessage(_message)) return true;
            if (ProcessErrorMessage(_message)) return true;
            if (ProcessInitializeMessage(_message)) return true;
            Log.Message("NoteCounter: Unprocessed message: '" + _message + "'");
          }
        }
      }
      catch
      {
        Log.Error("Undefined message received: " + _message);

        try
        {
          //Try to notify the error
          ErrorReceived _error = new ErrorReceived();
          _error.AlarmCode = AlarmCode.Reader_Undefined_Message;
          _error.Severity = AlarmSeverity.Error;

          NoteCounterReceivedEvent(_error);
        }
        catch { ; }
      }

      return false;
    }

    private Boolean ProcessCurrencyMessage(String _message)
    {
      List<NoteReceived> _currencies_list;
      NotesReceived _currencies_received;

      if (char.IsLower(_message[0]))
      {
        _currencies_received = new NotesReceived();
        _currencies_received.IsoCode = m_configured_currency;
        if (ParseCurrenciesMessage(_message, out _currencies_list))
        {
          _currencies_received.Notes = _currencies_list;
          NoteCounterReceivedEvent(_currencies_received);
        }

        return true;
      }

      return false;
    }

    private Boolean ParseCurrenciesMessage(String Message, out List<NoteReceived> _list_currencies)
    {
      _list_currencies = new List<NoteReceived>();
      NoteReceived _currency;
      Boolean EndOfMessage = false;
      Int32 _position = 0;
      String _currency_id;
      Boolean _found = false;


      String[] parts = Regex.Split(Message, @"([abcdefg])"); //TODO: OPTIMIZE REGEX
      while (!EndOfMessage)
      {
        if (parts[_position].Length > 0 && (char.IsLetter(parts[_position][0])))
        {
          _currency = new NoteReceived();
          _currency_id = parts[_position];

          for (int i = 0; i < this.m_currencies_accepted.Count; i++)
          {
            _found = false;
            if (_currency_id == this.m_currencies_accepted[i].Id)
            {
              _currency.Denomination = this.m_currencies_accepted[i].Denomination;
              _position++; //move to next part of CurrencyReceived 
              _currency.Quantity = Int32.Parse(parts[_position]);
              _list_currencies.Add(_currency);
              _found = true;
            }
            if (_found) break;
          }
          if (!_found)
          {
            //Sends information event to notify each currency not configured.
            InformationReceived _info = new InformationReceived();
            _position++;
            _info.Info = _currency_id + "-" + Int32.Parse(parts[_position]).ToString();
            _info.InformationType = InformationType.UNDEFINED_CURRENCY;
            NoteCounterReceivedEvent(_info);
          }
        }
        _position++;  //move to next element
        if (_position >= parts.Length) EndOfMessage = true;
      }

      return (_list_currencies.Count > 0);
    }

    private Boolean ProcessTicketMessage(String _message)
    {
      TicketReceived _ticket;

      if (_message[0] == '_')
      {
        if (!ParseTicketMessage(_message, out _ticket))
        {
          Log.Message("ErrorParsing Message: " + _message);

          return false;
        }
        NoteCounterReceivedEvent(_ticket);

        return true;
      }

      return false;
    }

    private Boolean ParseTicketMessage(String Message, out TicketReceived Ticket)
    {
      String _message;
      Ticket = null;

      //Message composed by: 2 '_' + 18 'ticketid' + 10 'value' 
      if (Message.Length == 30)
      {
        Ticket = new TicketReceived();
        _message = Message.Remove(0, 2);                                    //Extract __ 
        Ticket.ValidationNumber = Int64.Parse(_message.Substring(0, 18));   //Get Validation number
        Ticket.Amount = Int64.Parse(_message.Substring(18, 10));            //Retrieves Ticket Amount. TODO: Check if better define amount as decimal
      }
      else
      {
        Log.Error("Incorrect ticket size: " + Message);

        return false;
      }

      return true;
    }

    private Boolean ProcessErrorMessage(String _message)
    {
      ErrorReceived _error_received;
      InformationReceived _info_received;
      JetScanError _jetscan_error;

      if (char.IsUpper(_message[0]) && _message != POWER_UP)
      {
        if (ParseErrorMessage(_message, out _jetscan_error))
        {
          //_error_received = new ErrorReceived(_jetscan_error.ToString());
          _error_received = new ErrorReceived();
          _error_received.AlarmCode = _jetscan_error.GetAlarmCode();
          _error_received.Severity = _jetscan_error.GetAlarmSeverity();

          NoteCounterReceivedEvent(_error_received);

          return true;
        }
        else
        {
          if (_message.Length > 1)
          {
            if (char.IsUpper(_message[1]))
            {
              _info_received = new InformationReceived();
              _info_received.InformationType = InformationType.LABEL_RECEIVED;

              NoteCounterReceivedEvent(_info_received);
            }
          }
        }
      }
      return false;
    }

    private Boolean ParseErrorMessage(String Message, out JetScanError ErrorCode)
    {
      ErrorCode = null;

      ErrorCode = new JetScanError();
      ErrorCode.Code = ((JetScanErrorCode)Message[0]);
      if (Message.Length > 1)
      {
        if (!char.IsUpper(Message[1]))
        {
          ErrorCode.Denomination = Message[1];
        }
        else
        {
          return false;
        }
      }

      return true;
    }

    private Boolean ProcessInitializeMessage(String _message)
    {
      InformationReceived _info;

      if ((_message.Length == 1) && (_message == POWER_UP))
      {
        _info = new InformationReceived();
        _info.InformationType = InformationType.NOTECOUNTER_INITIALIZED;

        NoteCounterReceivedEvent(_info);
        
        return true;
      }
      return false;
    }

    private String RemoveTrxParameters(String _message)
    {
      if (_message.Length >= 2)
      {
        _message = _message.Trim(' ');                      //Extract initial and final white-spaces
        if (Convert.ToByte(_message[0]) == STX)
        {
          _message = _message.Remove(0, 1);                   //Extract STX 
        }
        if (Convert.ToByte(_message[_message.Length - 1]) == ETX)
        {
          _message = _message.Remove(_message.Length - 1, 1); //Extract ETX 
        }
        _message = _message.Trim(' ');                      //Extract initial and final white-spaces
      }
      return _message;
    }

    #endregion

    /// <summary>
    /// Unused functions due lack of sent messages to notecounter
    /// </summary>
    /// <param name="Command"></param>
    /// <returns></returns>
    #region Unused Functions


    #region Parse Send Commands

    //------------------------------------------------------------------------------
    // PURPOSE: Sets maximum number of bills accepted in notecounter. Range values between 0 - 100. 0 means No limit
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: Message was sent properly
    // 
    private Boolean SetStrapLimits()
    {
      String _strap_limits;

      _strap_limits = GeneralParam.GetString("Cage", "BillCounter.DenominationLimits");
      //_strap_limits = "a0;b0;c0;d0;e0;f0;g0";
      _strap_limits = _strap_limits.Replace(";", "");
      _strap_limits = _strap_limits.PadLeft(_strap_limits.Length + 1, ' ');
      _strap_limits = _strap_limits.PadRight(_strap_limits.Length + 1, ' ');
      if (!SendCommand(_strap_limits))
      {
        Log.Error(String.Format("Cannot set strap limits: ", _strap_limits));
        return false;
      }
      return ReceiveACKCommand();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Sets new operation mode in notecounter
    //          
    //  PARAMS:
    //      - INPUT:
    //            - JetScanOperatingMode: New operating mode
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: Message was sent properly
    // 
    private Boolean SetOperatingMode(JetScanOperatingMode NewOperatingMode)
    {
      if (!SendCommand(NewOperatingMode.ToString()))
      {
        Log.Error(String.Format("Cannot Set Operating Mode: ", NewOperatingMode.ToString()));
        return false;
      }
      return ReceiveACKCommand();
    }

    #endregion

    #region Communication Methods

    private Boolean SendCommand(String Command)
    {
      try
      {
        MemoryStream _ms;
        Byte[] _byte_query;
        _ms = new MemoryStream();

        _ms.WriteByte(STX);
        _ms.Write(Encoding.ASCII.GetBytes(Command), 0, Command.Length);
        _ms.WriteByte(ETX);
        _byte_query = _ms.ToArray();

        return m_communication.SendCommand(GetString(_byte_query));
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Waits for a Acknowledge command
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Boolean: Ack received
    // 
    public Boolean ReceiveACKCommand()
    {
      String _full_message;
      Boolean _received_message;

      try
      {
        _full_message = String.Empty;
        _received_message = m_communication.ReceiveCommand(out _full_message);

        if (!_received_message)
        {
          Log.Message("ReceivedACKCommand: None message received");

          return false;
        }
        if (GetBytes(_full_message)[0] == NACK)
        {
          Log.Message("ReceivedACKCommand: Received NACK");

          return false;
        }

        if (GetBytes(_full_message)[0] == ACK)
        {
          return true;
        }

        Log.Message("ReceivedACKCommand: Unknown Message");
        return false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
    }


    private void SetUpJetScan()
    {
      SendCommand(((Char)JetScanOperatingMode.OffSort).ToString());  //Configuration command for notecounter
      if (!ReceiveACKCommand())
      {
        Log.Error(String.Format("Configuration cannot be setted"));
      }
    }


    public Boolean CheckSum(String Message)
    {
      //TODO: validate
      //return true;

      Byte[] _message_in_bytes;
      Byte[] _checksum_in_bytes;
      Byte[] _original_checksum;
      String _checksum;
      Int32 _substract_for_checksum = 32;

      //      _binaryData = "";
      _checksum = "#1";
      _original_checksum = new byte[2];

      _checksum_in_bytes = Encoding.UTF8.GetBytes(_checksum); //Encoding.ASCII
      _message_in_bytes = Encoding.UTF8.GetBytes(Message); //Encoding.ASCII

      if (_message_in_bytes.Length > 3) //checksum is in 2nd and 3th position so at least it has to have lenght = 3
      {
        _original_checksum[0] = _message_in_bytes[1]; //High part
        _original_checksum[1] = _message_in_bytes[2]; //Low part
      }
      else
        return false;

      for (int i = 0; i < _checksum_in_bytes.Length; i++)
      {
        _checksum_in_bytes[i] = (byte)(_checksum_in_bytes[i] - _substract_for_checksum);
      }

      return _original_checksum.Equals(_checksum_in_bytes);
    }

    static Byte[] GetBytes(String str)
    {
      System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
      Byte[] bytes = encoding.GetBytes(str);
      return bytes;
    }

    static String GetString(Byte[] bytes)
    {
      return Encoding.UTF8.GetString(bytes);
    }

    #endregion

    #endregion

  }
}
