//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ScannerManager.cs
// 
//   DESCRIPTION: Manager class to control NoteCounters configured.
// 
//        AUTHOR: Xavier Iglesia & David Rigal
// 
// CREATION DATE: 24-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-JUL-2014 XIT & DRV First Release
// 06-AUG-2014 XIT   Updated Version
// 09-SEP-2014 XIT & HBB Added common scanner functions
// 30-SEP-2014 DRV   Fixed Bug WIG-1328; Multicurrency not supported
// 26-FEB-2016 JPJ   Fixed Bug 10036: Note counter events are removed while in validation screen
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using WSI.Common.NoteScanner;
using System.Threading;
using System.Runtime.Remoting.Messaging;
using System.Data.SqlClient;
using System.Data;
using System.IO.Ports;

namespace WSI.Common.NoteScanner
{
  public class ScannerManager
  {

    #region Fields
    public event NoteCounterReceivedEventHandler NoteCountReceivedEvent;  //Notify events to GUI

    private ManualResetEvent ev_pending = new ManualResetEvent(false);  //Setted when there are pending events in the queue
    private ManualResetEvent ev_shutdown = new ManualResetEvent(false); //Setted when notescanner must be terminated

    private IScanner m_scanner;                                         //Scanner used
    private WaitableQueue m_queue;                                      //Queue of events pending to notify to GUI

    private Thread m_thread;                                            //Dequeue events pending to notify to GUI. 
    #endregion


    public ScannerManager(NoteCounterInfo ConfigInfo)
    {
      Type _scanner_type;

      _scanner_type = Type.GetType("WSI.Common.NoteScanner." + ConfigInfo.Model);
      m_scanner = (IScanner)Activator.CreateInstance(_scanner_type, ConfigInfo);
      m_scanner.NoteCounterReceivedEvent += new NoteCounterReceivedEventHandler(m_scanner_NoteCountReceivedEvent);
      m_queue = new WaitableQueue();
      m_thread = new Thread(MessagesThread);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initializes notecounter opening the port and starting to wait messages
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    public Boolean InitializeScanner()
    {
      try
      {
        if (!this.m_scanner.Initialize())
        {
          return false;
        }

        if (m_thread == null || m_thread.ThreadState != ThreadState.Running)
        {
          ev_shutdown.Reset();
          m_thread = new Thread(MessagesThread);
          m_thread.SetApartmentState(ApartmentState.STA);
          m_thread.Name = "ScannerManagerThread";
          m_thread.Start();
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Error("Error Initializing BillCounter: " + ex.Message);
        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Thread function that notifies messages received to GUI
    //          
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    public void MessagesThread()
    {
      Int32 _wait_hint;
      Object _note_count_scanned;
      WaitHandle[] _events;

      try
      {
        _wait_hint = 100;
        _events = new WaitHandle[] { ev_pending, ev_shutdown };

        while (!ev_shutdown.WaitOne(0))
        {
          WaitHandle.WaitAny(_events, _wait_hint, false);

          if (!m_queue.Dequeue(out _note_count_scanned, 500))
          {
            _wait_hint = 1000;
            ev_pending.Reset();
          }
          else
          {
            NotifyEvent((IScanned)_note_count_scanned);
          }
        }
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);

        try
        {
          //Try to notify to GUI
          //ErrorReceived _error = new ErrorReceived("Scanner thread aborted");
          ErrorReceived _error = new ErrorReceived();
          _error.AlarmCode = AlarmCode.Unknown;
          _error.Severity = AlarmSeverity.Error;
          NoteCountReceivedEvent(_error);
        }
        catch { ; }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Notifies events to GUI.
    //          
    //  PARAMS:
    //      - INPUT: IScanned: Event to notify.
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    private void NotifyEvent(IScanned IScanned)
    {
      try
      {
        NoteCounterReceivedEventHandler _handler;
        if (NoteCountReceivedEvent != null)
        {
          _handler = NoteCountReceivedEvent;
          _handler(IScanned);
        }
      }
      catch (ObjectDisposedException Ex)
      {
        Log.Error("Se cerr� la ventana de forma inesperada durante la inserci�n de elementos en UI: " + Ex.Message);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Notifies if the NoteCountReceivedEvent events is active.
    //          
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    public bool IsEventHandlerRegistered()
    {

      return this.NoteCountReceivedEvent != null;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Receives messages from notecounter and enqueues to send later
    //          
    //  PARAMS:
    //      - INPUT: ReceivedEvent: Message to enqueue
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    void m_scanner_NoteCountReceivedEvent(IScanned ReceivedEvent)
    {
      this.m_queue.Enqueue(ReceivedEvent);
      this.ev_pending.Set();
    }


    public void CloseScanner()
    {
      if (this.m_scanner != null)
      {
        try
        {
          this.m_scanner.CloseConnection();
        }
        catch
        {
          Log.Error("Error Closing Scanner");
        }

        this.m_scanner = null;
      }

      ev_shutdown.Set();
      m_thread = null;

    }

    public void PauseScanner()
    {
      //Will stop receiving IScanned documents, m_thread will be paused automatically by ev_pending when empty queue. 
      //Can't pause before because must report all documents received
      this.m_scanner.PauseRead();
    }

    public void ResumeScanner()
    {
      //starts to read events again.
      this.m_scanner.ResumeRead();
    }
   
  }

  #region CommonScannerFunctions

  public class CommonScannerFunctions
  {
    public static NoteCounterInfo GetScannerInfo(Int32 ApplicationId)
    {
      StringBuilder _sb;
      NoteCounterModel _model;
      String _model_name_info;
      NoteCounterCommunication _communication_type;
      String _port;
      Parity _parity;
      NoteCounterDatabits _data_bits;
      StopBits _stop_bits;
      NoteCounterBaudrate _baudrate;
      String _currencies_xml;
      String _curency;
      DataTable _note_counter_info_datatable;
      NoteCounterInfo _note_counter_info;
      SerialConfigParams _serial_config_params;
      List<AcceptedCurrencies> _currencies_accepted;

      _sb = new StringBuilder();
      _model = 0;
      _communication_type = 0;
      _port = "";
      _parity = 0;
      _data_bits = 0;
      _stop_bits = StopBits.None;
      _baudrate = 0;
      _currencies_xml = "";
      _curency = "";
      _model_name_info = "";
      _currencies_accepted = new List<AcceptedCurrencies>();

      _note_counter_info_datatable = GetScannerInfoDataTable(ApplicationId);

      if (_note_counter_info_datatable.Rows.Count > 0)
      {
        _model = (NoteCounterModel)_note_counter_info_datatable.Rows[0][0];

        //now, there is just one model but the switch is prepared to have more options
        switch (_model)
        {
          case NoteCounterModel.JetScan4091:
            _model_name_info = "NoteCounterJetScan4091Info";
            break;
          default:
            return null;
        }
      }
      else 
      {
        return null;
      }

      Type _scanner_type = Type.GetType("WSI.Common.NoteScanner." + _model_name_info);

      if (_note_counter_info_datatable.Rows[0][1] != System.DBNull.Value)
      {
        _communication_type = (NoteCounterCommunication)_note_counter_info_datatable.Rows[0][1];
      }
      if (_note_counter_info_datatable.Rows[0][2] != System.DBNull.Value)
      {
        _port = ((NoteCounterPort)_note_counter_info_datatable.Rows[0][2]).ToString();
      }
      if (_note_counter_info_datatable.Rows[0][3] != System.DBNull.Value)
      {
        _parity = (Parity)_note_counter_info_datatable.Rows[0][3];
      }
      if (_note_counter_info_datatable.Rows[0][5] != System.DBNull.Value)
      {
        _stop_bits = (StopBits)_note_counter_info_datatable.Rows[0][5];
      }
      if (_note_counter_info_datatable.Rows[0][4] != System.DBNull.Value)
      {
        _data_bits = (NoteCounterDatabits)_note_counter_info_datatable.Rows[0][4];
      }
      if (_note_counter_info_datatable.Rows[0][6] != System.DBNull.Value)
      {
        _baudrate = (NoteCounterBaudrate)_note_counter_info_datatable.Rows[0][6];
      }
      if (_note_counter_info_datatable.Rows[0][7] != System.DBNull.Value)
      {
        _currencies_xml = _note_counter_info_datatable.Rows[0][7].ToString();
      }
      if (_note_counter_info_datatable.Rows[0][8] != System.DBNull.Value)
      {
        _curency = _note_counter_info_datatable.Rows[0][8].ToString();
      }

      _serial_config_params = new SerialConfigParams(_port, _parity, _data_bits, _stop_bits, _baudrate);
      _currencies_accepted = CurrenciesFromXml(_currencies_xml);
      _note_counter_info = (NoteCounterInfo)Activator.CreateInstance(_scanner_type, new object[] { _serial_config_params, _currencies_accepted, _curency});
      _note_counter_info.Communication = _communication_type;
      _note_counter_info.Model = _model;

      return _note_counter_info;
    }

    public static DataTable GetScannerInfoDataTable(Int32 ApplicationId)
    {
      DataTable _note_counter_info;
      StringBuilder _sb = new StringBuilder();

      _note_counter_info = new DataTable();
      _sb = new StringBuilder();
      
        _sb.AppendLine("   SELECT   NC_MODEL                            ");
        _sb.AppendLine("          , NC_COMMUNICATION_TYPE               ");
        _sb.AppendLine("          , NC_PORT                             ");
        _sb.AppendLine("          , NC_PARITY                           ");
        _sb.AppendLine("          , NC_DATA_BITS                        ");
        _sb.AppendLine("          , NC_STOP_BITS                        ");
        _sb.AppendLine("          , NC_BAUD_RATE                        ");
        _sb.AppendLine("          , NC_DENOMINATIONS                    ");
        _sb.AppendLine("          , NC_CURRENCY_ISO_CODE                ");
        _sb.AppendLine("     FROM   NOTE_COUNTERS                       ");
        _sb.AppendLine("    WHERE   NC_APPLICATION_ID = @pApplicationId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pApplicationId", SqlDbType.BigInt).Value = ApplicationId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, _note_counter_info);
                            
            }
          }
        }
      return _note_counter_info;
    }

    public static List<AcceptedCurrencies> CurrenciesFromXml(String Xml)
    {
      DataSet _ds;
      DataTable _dt;
      String _curency_id;
      Decimal _currency_denomination;
      Int32 _currency_limit;
      String _currency;
      List<AcceptedCurrencies> _currencies_accepted;

      _currencies_accepted = new List<AcceptedCurrencies>();

      if (Xml != "")
      {
        _ds = GetDataSetCurrenciesFromXml(Xml);

        if (_ds.Tables.Count > 0)
        {
          _dt = _ds.Tables[0];
          foreach (DataRow _dr in _dt.Rows)
          {
            if (_dr[0] != null && _dr[1] != null && _dr[2] != null)
            {
              _currency = (String)_dr[0];
              _curency_id = (String)_dr[1];
              _currency_denomination = (Decimal)_dr[2];
              _currency_limit = (Int32)_dr[3];
              _currencies_accepted.Add(new AcceptedCurrencies(_curency_id, _currency_denomination, _currency_limit, _currency));
            }
          }
        }
      }

      return _currencies_accepted;

    } // FromXml

    public static DataSet GetDataSetCurrenciesFromXml(String Xml)
    {
      DataSet _ds;
      DataTable _currencies;

      _ds = new DataSet("CurrenciesList");

      _currencies = new DataTable("Currencies");
      _currencies.Columns.Add("ISO_CODE", Type.GetType("System.String"));
      _currencies.Columns.Add("CurrencyId", Type.GetType("System.String"));
      _currencies.Columns.Add("Denomination", Type.GetType("System.Decimal"));
      _currencies.Columns.Add("Limit", Type.GetType("System.Int32"));

      _ds.Tables.Add(_currencies);

      Misc.DataSetFromXml(_ds, Xml, XmlReadMode.IgnoreSchema);

      return _ds;
    }

    public static String ReturnStringBaudrate(NoteCounterBaudrate BaudRate)
    {
      return BaudRate.ToString().Split('_')[1];
    }

    public static String ReturnStringDatabits(NoteCounterDatabits DataBits)
    {
      return DataBits.ToString().Split('_')[1];
    }

    public static Boolean GetApplicationId(out Int32 ApplicationId)
    {
      StringBuilder _sb ;
      Object _data = null;
      
      ApplicationId = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   APP_ID                 ");
          _sb.AppendLine("   FROM   APPLICATIONS           ");
          _sb.AppendLine("  WHERE   APP_NAME =    @pName   ");
          _sb.AppendLine("    AND   APP_MACHINE = @pMachine");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = "WigosGUI";
            _cmd.Parameters.Add("@pMachine", SqlDbType.NVarChar).Value = Environment.MachineName;
            _data = _cmd.ExecuteScalar();
          }
        }
      }
      catch
      {
        return false;
      }

      if ((_data != null))
      {
        ApplicationId = Convert.ToInt32(_data);
        return true;
      }
      else
      {
        return false;
      }
    }    
  }
  #endregion

}