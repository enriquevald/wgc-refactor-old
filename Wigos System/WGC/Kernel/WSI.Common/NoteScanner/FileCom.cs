//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FileCom.cs
// 
//   DESCRIPTION: Class to Simulate the note counter, reading data from a text file.
// 
//        AUTHOR: Xavier Iglesia & David Rigal
// 
// CREATION DATE: 24-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-JUL-2014 XIT & DRV First Release
// 30-JUL-2014 XIT   Updated Version
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using WSI.Common;
using WSI.Common.NoteScanner;


namespace WSI.Common.NoteScanner
{
  public class FileCom : ICommunication
  {
    StreamReader m_reader;

    #region ICommunication Members

    public FileCom(Object Params) 
    {
 

    }


    public Boolean Initialize(Object Params)
    {
      try
      {
        String path = System.IO.Path.GetDirectoryName(
        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

        path += "\\VirtualCounter.txt";
        path = path.Remove(0, 6);

        m_reader = new StreamReader(path);

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
    }

    public Boolean IsInitialized()
    {
      if (m_reader == null) return false;
      else return true;
    }

    public Boolean SendCommand(string Command)
    {
      return true;
    }


    public Boolean ReceiveCommand(out String Command)
    {
      try
      {
        String _line;

        Command = "";

        if ((_line = m_reader.ReadLine()) != null)
        {
          Command = _line;

          return true;
        }
        return false;
      }
      catch (Exception ex)
      {
        throw(ex);
      }
    }

    public Boolean CloseConnection()
    {
      if (m_reader != null)
      {
        m_reader.Close();
        m_reader = null;
      }

      return true;
    }

    #endregion
  }
}
