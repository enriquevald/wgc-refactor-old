//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ScannerDefinitions.cs
// 
//   DESCRIPTION: Class that contains different objects used on scanner.
// 
//        AUTHOR: Xavier Iglesia & David Rigal
// 
// CREATION DATE: 24-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-JUL-2014 XIT & DRV First Release
// 30-JUL-2014 XIT   Updated Version
// 09-SEP-2014 HBB   Added new Enumerations
// 30-SEP-2014 DRV   Fixed Bug WIG-1328; Multicurrency not supported
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;

namespace WSI.Common.NoteScanner
{
  public class NoteCounterInfo
  {
    public NoteCounterModel Model;
    public NoteCounterCommunication Communication;
    public Int64 ApplicationId;
  }

  public delegate void NoteCounterReceivedEventHandler(IScanned e);

  #region ENUMS
  

  public enum ScannedType
  {
    Currencies = 0,
    Tickets = 1,
    Error = 2,
    Information = 3
  }

  public enum NoteCounterModel
  {
    None = 0,
    JetScan4091 = 1
  }

  public enum NoteCounterCommunication
  {
    Serial = 0,
    File = 1 //File communication type must be allways the last one
  }

  public enum NoteCounterPort
  {
    COM1 = 0,
    COM2 = 1,
    COM3 = 2,
    COM4 = 3,
    COM5 = 4,
    COM6 = 5,
    COM7 = 6,
    COM8 = 7,
    COM9 = 8,
    COM10 = 9,
    COM11 = 10,
    COM12 = 11,
    COM13 = 12,
    COM14 = 13,
    COM15 = 14,
    COM16 = 15,
    COM17 = 16,
    COM18 = 17,
    COM19 = 18,
    COM20 = 19
  }

  public enum NoteCounterBaudrate
  {
    BAUDRATE_75 = 0,
    BAUDRATE_110 = 1,
    BAUDRATE_134 = 2,
    BAUDRATE_150 = 3,
    BAUDRATE_300 = 4,
    BAUDRATE_600 = 5,
    BAUDRATE_1200 = 6,
    BAUDRATE_1800 = 7,
    BAUDRATE_2400 = 8,
    BAUDRATE_4800 = 9,
    BAUDRATE_7200 = 10,
    BAUDRATE_9600 = 11,
    BAUDRATE_14400 = 12,
    BAUDRATE_19200 = 13,
    BAUDRATE_38400 = 14,
    BAUDRATE_57600 = 15,
    BAUDRATE_115200 = 16,
    BAUDRATE_128000 = 17
  }

  public enum NoteCounterDatabits
  {
    DATABITS_5 = 0,
    DATABITS_6 = 1,
    DATABITS_7 = 2,
    DATABITS_8 = 3
  }

  public enum InformationType
  {
    UNDEFINED_CURRENCY = 0,
    NOTECOUNTER_INITIALIZED = 1,
    LABEL_RECEIVED = 2
  }

#endregion

  public class TicketReceived : EventArgs ,IScanned
  {
    #region Fields
    private Int64 m_validation_number;
    private Int64 m_amount;
    #endregion

    #region Properties
    public Int64 ValidationNumber
    {
      get { return m_validation_number; }
      set { m_validation_number = value; }
    }

    public Int64 Amount
    {
      get { return m_amount; }
      set { m_amount = value; }
    }
    #endregion

    #region IScanned Members

    public ScannedType GetScannedType()
    {
      return ScannedType.Tickets;
    }

    #endregion
  }

  public class NotesReceived : EventArgs, IScanned
  {
    private List<NoteReceived> m_notes;
    private String m_iso_code;

    public String IsoCode
    {
      get { return m_iso_code; }
      set { m_iso_code = value; }
    }

    public List<NoteReceived> Notes
    {
      get { return m_notes; }
      set { m_notes = value; }
    }

    public ScannedType GetScannedType()
    {
      return ScannedType.Currencies;
    }
  }

  public class NoteReceived : EventArgs
  {
    #region Fields
    Decimal m_denomination;
    Int32 m_quantity;
    #endregion

    #region Properties
    public Decimal Denomination
    {
      get { return m_denomination; }
      set { m_denomination = value; }
    }

    public Int32 Quantity
    {
      get { return m_quantity; }
      set { m_quantity = value; }
    }
    #endregion
  }

  public class InformationReceived : EventArgs, IScanned
  {
    private String m_message;
    private InformationType m_type;


    public InformationType InformationType
    {
      get { return m_type; }
      set { m_type = value; }
    }

    public String Info
    {
      get { return m_message; }
      set { m_message = value; }
    }


    public ScannedType GetScannedType()
    {
      return ScannedType.Information;
    }

  }

  public class ErrorReceived : EventArgs , IScanned 
  {
    #region Fields
    AlarmCode m_alarm_code;
    AlarmSeverity m_severity;
    #endregion

    #region Properties

    public AlarmCode AlarmCode
    {
      get { return m_alarm_code; }
      set { m_alarm_code = value; }
    }

    public AlarmSeverity Severity
    {
      get { return m_severity; }
      set { m_severity = value; }
    }

    #endregion

    public ScannedType GetScannedType()
    {
      return ScannedType.Error;
    }
  }

  public class SerialConfigParams
  {
    #region Fields
    private String m_com_port;
    private Parity m_parity;
    private NoteCounterDatabits m_data_bits;
    private StopBits m_stop_bits;
    private NoteCounterBaudrate m_baud_rate;
    #endregion

    public SerialConfigParams(String ComPort, Parity Parity, NoteCounterDatabits DataBits, StopBits StopBits, NoteCounterBaudrate BaudRate) 
    {
      m_com_port = ComPort;
      m_parity = Parity;
      m_data_bits = DataBits;
      m_stop_bits = StopBits;
      m_baud_rate = BaudRate;
    }

    #region Properties
    public String ComPort
    {
      get { return m_com_port; }
      set { m_com_port = value; }
    }

    public Parity ParityCom
    {
      get { return m_parity; }
      set { m_parity = value; }
    }

    public NoteCounterDatabits DataBits
    {
      get { return m_data_bits; }
      set { m_data_bits = value; }
    }

    public StopBits StopBits
    {
      get { return m_stop_bits; }
      set { m_stop_bits = value; }
    }

    public NoteCounterBaudrate BaudRate
    {
      get { return m_baud_rate; }
      set { m_baud_rate = value; }
    }
    #endregion
  }

  public class AcceptedCurrencies
  {
    private String m_id;
    private Decimal m_denomination;
    private Int32 m_limit;
    private String m_currency;

    public AcceptedCurrencies(string Id, Decimal Denomination, Int32 Limit, String Currency) 
    {
      m_id = Id;
      m_denomination = Denomination;
      m_limit = Limit;
      m_currency = Currency;
    }

    public string Id
    {
      get { return m_id; }
      set { m_id = value; }
    }

    public Decimal Denomination
    {
      get { return m_denomination; }
      set { m_denomination = value; }
    }

    public Int32 Limit
    {
      get { return m_limit; }
      set { m_limit = value; }
    }

    public String Currency
    {
      get { return m_currency; }
      set { m_currency = value; }
    }
  }

  public class CommunicationParams
  {
    public Int32 ACK_TIMEOUT;
    public Int32 MAX_SIZE;
  }

}
