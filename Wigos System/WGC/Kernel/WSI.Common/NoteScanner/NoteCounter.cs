//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: NoteCounter.cs
// 
//   DESCRIPTION: Classes and Interfaces to manage note counter machines.
// 
//        AUTHOR: Ignasi Carre�o
// 
// CREATION DATE: 27-DEC-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-DEC-2012 ICS    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace WSI.Common.NoteCounter
{
  #region Interface

  ///<summary>
  ///Interface
  ///</summary>
  public interface INoteCounter
  {
    //NoteCounterError Init(NoteCounterStatusHandler StatusHandler);
    NoteCounterProperties Properties();
    //void StartSorting(NoteCounterSortingParams SortingParams, NoteCounterSortingHandler SortingHandler);
    void Close();
  }

  #endregion

  #region Enums

  public enum NOTE_COUNTER_ERROR_PRIORITY
  {
    NO_ERROR = 0,     // No error
    WARNING = 1,      // WARNING (low priority advice to the user)
    CLEAR = 2,        // CLEAR (remove notes and try again)
    RETRY = 3,        // RETRY (repeat the batch)
    INTERVENTION = 4, // INTERVENTION (correct the fault and retry)
    FATAL = 5         // FATAL (no recovery possible, machine will reset)
  }

  public enum NOTE_COUNTER_STATUS
  {
    IDLE = 0,          // Waiting for commands.
    DISCONNECTED = 1,  // Unable to connect.
    BUSY = 2,          // Machine is busy.
    ERROR = 3,         // Has been an error.
    SORTING = 4        // Machine is sorting.
  }

  #endregion

  #region Classes

  public class NoteCounterProperties
  {
    public Int32 DeviceModel;
    public String DeviceName;
    public Boolean CurrenciesAvailable;
    public List<String> Currencies;
  }

  public class NoteCounterMeter
  {
    public String Currency;
    public Decimal Value;
    public Int32 Count;
  }
  
  public class NoteCounterSortingEvent
  {
    public List<NoteCounterMeter> Notes;
  }

  public class NoteCounterStatusEvent
  {
    public NOTE_COUNTER_STATUS Status;
    public NoteCounterError Error;
  }

  public class NoteCounterSortingParams
  {
    public String Currency;
    public Int32 Batch;
  }

  public class NoteCounterError
  {
    #region Members

    public NOTE_COUNTER_ERROR_PRIORITY ErrorPriority;
    public Int32 ErrorCode;
    public String ErrorMessage;
    public String ErrorAction;

    #endregion

    # region Constructors

    //------------------------------------------------------------------------------
    // PURPOSE : Default constructor for a NoteCounterError instance
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public NoteCounterError()
    {
      this.ErrorPriority = NOTE_COUNTER_ERROR_PRIORITY.NO_ERROR;
      this.ErrorCode = 0;
      this.ErrorMessage = String.Empty;
      this.ErrorAction = String.Empty;
    }

    #endregion
  }
  //definicion, void, no devuelve nada. event es el tipo parametro entrada...
  //public delegate void NoteCounterSortingHandler(NoteCounterSortingEvent Event);
  //public delegate void NoteCounterStatusHandler(NoteCounterStatusEvent Event);

  /// <summary>
  /// Generic Class
  /// </summary>
  public static class NoteCounter
  {
    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Return a specific NoteCounter
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int32: Device Model Number
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - INoteCounter
    //
    public static INoteCounter DeviceModel(Int32 DeviceModelNumber)
    {
      //try
      //{
      //  switch (DeviceModelNumber)
      //  {
      //    case 901: 
      //      return new NoteCounter_model_901();

      //    case 001:
      //      return new NoteCounter_model_001();

      //    default:
      //      // Log Unknown model
      //      break;
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  // Log.Exception()
      //}

      return null;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns a NoteCountersProperties list of available models
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - A NoteCounterProperties list
    // 
    //
    public static NoteCounterProperties[] Models()
    {
      NoteCounterProperties[] _models;

      _models = new NoteCounterProperties[1];

      return _models;
    }

    #endregion
  }

  #endregion
}
