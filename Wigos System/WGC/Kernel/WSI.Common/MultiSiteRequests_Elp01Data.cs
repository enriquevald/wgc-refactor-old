//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSiteRequests_Elp01Data.cs
// 
//   DESCRIPTION: MultiSiteRequests_Elp01 data functions.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 25-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author    Description
// ----------- ------    ----------------------------------------------------------
// 25-APR-2013 ACC       First release.
// 14-AUG-2013 SMN       Add Elp01Vouchers class
// 04-MAR-2015 JCA & AMF Fixed Bug WIG-2015: Space points sometimes not displayed
// 26-MAR-2018 JGC       PBI 32313:[WIGOS-9404]: Codere_Mexico: Call to space to get token
// 26-MAR-2018 JGC       PBI 32312:[WIGOS-9407]: Codere_Mexico Space: Use token to call space servicesModification to add token security for calls to Space
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.IO;

namespace WSI.Common
{
   
  public enum ELP01_Requests
  {
    ELP01_REQUEST_UNKNOWN = 0
  ,
    ELP01_REQUEST_QUERY_POINTS = 1
  ,
    ELP01_REQUEST_VOUCHER_REDEEM = 2
  , ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM = 3
  }

  public enum ELP01_ResponseCodes
  {
    ELP01_RESPONSE_CODE_UNKNOWN = 0
  ,
    ELP01_RESPONSE_CODE_OK = 1
  ,
    ELP01_RESPONSE_CODE_ERROR = 2
  ,
    ELP01_RESPONSE_CODE_WS_URL_FAILED = 3
  ,
    ELP01_RESPONSE_CODE_TIMEOUT = 4
  , ELP01_RESPONSE_CODE_INCORRECT_VOUCHER = 5

  , ELP01_RESPONSE_CODE_TOKEN_ERROR = 6
  }


  public class Elp01InputData
  {
    public ELP01_Requests RequestType;
    public Int64 AccountId;
    public String VoucherId;
    public Int32 TerminalId;

    #region Public Methods

    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    public String ToXml()
    {
      XmlDocument _xml;
      XmlNode _node;

      _xml = new XmlDocument();

      try
      {
        _node = _xml.CreateNode(XmlNodeType.Element, "data", "");
        _xml.AppendChild(_node);

        switch (RequestType)
        {
          case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
            XML.AppendChild(_node, "elp01_request_type", RequestType.ToString());
            XML.AppendChild(_node, "account_id", AccountId.ToString());
            XML.AppendChild(_node, "terminal_id", TerminalId.ToString());
            break;

          case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
            XML.AppendChild(_node, "elp01_request_type", RequestType.ToString());
            XML.AppendChild(_node, "account_id", AccountId.ToString());
            XML.AppendChild(_node, "voucher_id", VoucherId);
            break;

          case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
            XML.AppendChild(_node, "elp01_request_type", RequestType.ToString());
            XML.AppendChild(_node, "account_id", AccountId.ToString());
            XML.AppendChild(_node, "voucher_id", VoucherId);
            break;

          case ELP01_Requests.ELP01_REQUEST_UNKNOWN:
          default:
            XML.AppendChild(_node, "elp01_request_type", RequestType.ToString());
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw _ex;
      }

      return _xml.InnerXml;
    }

    // PURPOSE : Fill the properties of this class from a XML String representation.
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(String XmlData)
    {
      XmlReader _xml;

      try
      {
        _xml = XmlReader.Create(new StringReader(XmlData));

        RequestType = (ELP01_Requests)Enum.Parse(typeof(ELP01_Requests), XML.ReadTagValue(_xml, "elp01_request_type"), true);

        switch (RequestType)
        {
          case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
            AccountId = Int64.Parse(XML.ReadTagValue(_xml, "account_id"));
            TerminalId = Int32.Parse(XML.ReadTagValue(_xml, "terminal_id"));
            break;

          case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
            AccountId = Int64.Parse(XML.ReadTagValue(_xml, "account_id"));
            VoucherId = XML.ReadTagValue(_xml, "voucher_id");
            break;

          case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
            AccountId = Int64.Parse(XML.ReadTagValue(_xml, "account_id"));
            VoucherId = XML.ReadTagValue(_xml, "voucher_id");
            break;

          case ELP01_Requests.ELP01_REQUEST_UNKNOWN:
          default:
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw _ex;
      }
    }

    #endregion
  }

  public class Elp01OutputData
  {
    public ELP01_Requests RequestType;
    public ELP01_ResponseCodes ResponseCodes;
    public Int64 Points;
    public Int64 AmountRedeemableCents;
    public Int64 AmountNonRedeemableCents;
    public Int32 RestrictedToGroupId;

    #region Public Methods

    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    public String ToXml()
    {
      XmlDocument _xml;
      XmlNode _node;

      _xml = new XmlDocument();

      try
      {
        _node = _xml.CreateNode(XmlNodeType.Element, "data", "");
        _xml.AppendChild(_node);

        XML.AppendChild(_node, "elp01_request_type", RequestType.ToString());

        switch (RequestType)
        {
          case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
            XML.AppendChild(_node, "elp01_response_status_code", ResponseCodes.ToString());
            XML.AppendChild(_node, "points", Points.ToString());
            break;

          case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
            XML.AppendChild(_node, "elp01_response_status_code", ResponseCodes.ToString());
            XML.AppendChild(_node, "amount_non_redeemable", AmountNonRedeemableCents.ToString());
            XML.AppendChild(_node, "amount_redeemable", AmountRedeemableCents.ToString());
            XML.AppendChild(_node, "RestrictedToGroupId", RestrictedToGroupId.ToString());

            break;

          case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
            XML.AppendChild(_node, "elp01_response_status_code", ResponseCodes.ToString());
            break;

          case ELP01_Requests.ELP01_REQUEST_UNKNOWN:
          default:
            XML.AppendChild(_node, "elp01_response_status_code", ResponseCodes.ToString());
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw _ex;
      }

      return _xml.InnerXml;
    }

    // PURPOSE : Fill the properties of this class from a XML String representation.
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(String XmlData)
    {
      XmlReader _xml;

      try
      {
        _xml = XmlReader.Create(new StringReader(XmlData));

        RequestType = (ELP01_Requests)Enum.Parse(typeof(ELP01_Requests), XML.ReadTagValue(_xml, "elp01_request_type"), true);

        switch (RequestType)
        {
          case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
            ResponseCodes = (ELP01_ResponseCodes)Enum.Parse(typeof(ELP01_ResponseCodes), XML.ReadTagValue(_xml, "elp01_response_status_code"), true);
            Points = Int64.Parse(XML.ReadTagValue(_xml, "points"));
            break;

          case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
            ResponseCodes = (ELP01_ResponseCodes)Enum.Parse(typeof(ELP01_ResponseCodes), XML.ReadTagValue(_xml, "elp01_response_status_code"), true);
            AmountNonRedeemableCents = Int64.Parse(XML.ReadTagValue(_xml, "amount_non_redeemable"));
            AmountRedeemableCents = Int64.Parse(XML.ReadTagValue(_xml, "amount_redeemable"));

            Int32.TryParse(XML.ReadTagValue(_xml, "RestrictedToGroupId", true), out RestrictedToGroupId);

            break;

          case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
            ResponseCodes = (ELP01_ResponseCodes)Enum.Parse(typeof(ELP01_ResponseCodes), XML.ReadTagValue(_xml, "elp01_response_status_code"), true);
            break;

          case ELP01_Requests.ELP01_REQUEST_UNKNOWN:
          default:
            ResponseCodes = (ELP01_ResponseCodes)Enum.Parse(typeof(ELP01_ResponseCodes), XML.ReadTagValue(_xml, "elp01_response_status_code"), true);
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw _ex;
      }

    }

    #endregion

  }

  public static class Elp01Requests
  {

    #region Public Methods

    // PURPOSE : Convert a DataRow to a Xml String representation
    //
    //  PARAMS :
    //      - INPUT :
    //          - dr: DataRow to convert
    //
    //      - OUTPUT :
    //          - XmlString : Xml String representation
    //
    // RETURNS :
    //      - True / False
    //
    public static Boolean ConvertRowToXml(DataRow Dr, Boolean isError, out String XmlString)
    {
      DataSet _ds;
      DataTable _dt;
      String _name_ds;
      String _name_dt;
      String _name_row_error_column;

      // Initialization
      if (isError)
      {
        _name_ds = "Error";
      }
      else
      {
        _name_ds = "SPACE";
      }
      _name_dt = "Row";
      _name_row_error_column = "RowError";
      _ds = new DataSet();
      _dt = new DataTable();
      XmlString = "";

      try
      {
        // Create a new DataTable with the same schema as Dr
        _dt = Dr.Table.Clone();

        // Set a name to the DataSet / DataTable
        _ds.DataSetName = _name_ds;
        _dt.TableName = _name_dt;

        // Insert Dr values into DataTable
        _dt.Rows.Add(Dr.ItemArray);

        // Add new column
        _dt.Columns.Add(_name_row_error_column, typeof(String));
        _dt.Rows[0][_name_row_error_column] = Dr.RowError;

        // Insert DataTable into DataSet
        _ds.Tables.Add(_dt);

        // Get Xml representation
        XmlString = _ds.GetXml();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    // PURPOSE : Obtain the Responses of a query points. It gets the Requests whose status is not Pending and InProgress.
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - RequestResponse : DataTable with results
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean GetResponsesQueryPoints(out DataTable RequestResponse, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Elp01InputData _input_data;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;
      RequestResponse = new DataTable();

      // Get Requests that status is not "pending" and is not "inprogress"
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT    MR_UNIQUE_ID                                 ");
        _sb.AppendLine("         , MR_REQUEST_TYPE                              ");
        _sb.AppendLine("         , MR_INPUT_DATA                                ");
        _sb.AppendLine("         , MR_OUTPUT_DATA                               ");
        _sb.AppendLine("         , MR_STATUS                                    ");
        _sb.AppendLine("         , MR_STATUS_CHANGED                            ");
        _sb.AppendLine("   FROM    MULTISITE_REQUESTS                           ");
        _sb.AppendLine("  WHERE    MR_STATUS NOT IN (@pMrStatus1, @pMrStatus2)  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMrStatus1", SqlDbType.Int).Value = WWP_MSRequestStatus.MS_REQUEST_STATUS_PENDING;
          _sql_cmd.Parameters.Add("@pMrStatus2", SqlDbType.Int).Value = WWP_MSRequestStatus.MS_REQUEST_STATUS_INPROGRESS;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _nr = _sql_da.Fill(RequestResponse);
          }
        } //SqlCommand _sql_cmd

        // Select Requests whose type is ELP01_REQUEST_QUERY_POINTS
        if (RequestResponse.Rows.Count > 0)
        {
          _input_data = new Elp01InputData();
          foreach (DataRow _request in RequestResponse.Rows)
          {
            _input_data.LoadXml(_request["MR_INPUT_DATA"].ToString());
            if (_input_data.RequestType != ELP01_Requests.ELP01_REQUEST_QUERY_POINTS)
            {
              _request.Delete();
            }
          }
          RequestResponse.AcceptChanges();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        //throw _ex;
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }
      RequestResponse.Clear();

      return false;
    }

    #endregion

  }

  public static class Elp01Vouchers
  {

    #region Public Methods

    // PURPOSE : Verify if exists Voucher by VoucherId and AccountId fields
    //
    //  PARAMS :
    //      - INPUT:
    //            - AccountId
    //            - VoucherId
    //
    //      - OUTPUT :
    //            - ExistsVoucher : Boolean
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean FindVouchersById(Int64 AccountId,
                                           String VoucherId,
                                           out Boolean ExistsVoucher,
                                           SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _tick0;
      Object _obj;

      // Initialize parameters
      ExistsVoucher = false;
      _tick0 = Environment.TickCount;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT    count(*)                    ");
        _sb.AppendLine("   FROM    elp01_vouchers              ");
        _sb.AppendLine("  WHERE    ev_account_id = @AccountId  ");
        _sb.AppendLine("    AND    ev_voucher_id = @VoucherId  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@VoucherId", SqlDbType.NVarChar).Value = VoucherId;

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null && (Int32)_obj >= 1)
          {
            ExistsVoucher = true;
          }
        } //SqlCommand _sql_cmd

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // FindVouchersById

    // PURPOSE : Insert Elp01_Vouchers.
    //
    //  PARAMS :
    //      - INPUT:
    //            - AccountId
    //            - VoucherId
    //            - CreditType
    //            - Money
    //            - OperationId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean InsertVouchers(Int64 AccountId,
                                         String VoucherId,
                                         ACCOUNT_PROMO_CREDIT_TYPE CreditType,
                                         Decimal Money,
                                         Int64 OperationId,
                                         SqlTransaction SqlTrx,
                                         out Boolean VoucherAlreadyPaid)
    {
      StringBuilder _sb;
      Int32 _tick0;

      // Initialize parameters
      _tick0 = Environment.TickCount;
      VoucherAlreadyPaid = false;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO  elp01_vouchers  (ev_account_id    ");
        _sb.AppendLine("                             , ev_voucher_id    ");
        _sb.AppendLine("                             , ev_credit_type   ");
        _sb.AppendLine("                             , ev_amount        ");
        _sb.AppendLine("                             , ev_datetime      ");
        _sb.AppendLine("                             , ev_operation_id) ");
        _sb.AppendLine("                     VALUES   (@AccountId       ");
        _sb.AppendLine("                             , @VoucherId       ");
        _sb.AppendLine("                             , @CreditType      ");
        _sb.AppendLine("                             , @Amount          ");
        _sb.AppendLine("                             , GETDATE()        ");
        _sb.AppendLine("                             , @OperationId)    ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@VoucherId", SqlDbType.NVarChar).Value = VoucherId;
          _sql_cmd.Parameters.Add("@CreditType", SqlDbType.Int).Value = (Int32)CreditType;
          _sql_cmd.Parameters.Add("@Amount", SqlDbType.Money).Value = Money;
          _sql_cmd.Parameters.Add("@OperationId", SqlDbType.BigInt).Value = OperationId;

          return (_sql_cmd.ExecuteNonQuery() == 1);
        } //SqlCommand _sql_cmd

      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == 2601 || _sql_ex.Number == 2627)
        {
          //Unique Key Violation
          VoucherAlreadyPaid = true;
          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertVouchers

    #endregion // Public Methods

  } // class Elp01Vouchers

  public static class ELP01_DownloadSpaceAccount
  {
    public const String HOLDER_NAME = "SPACE - NUEVA ALTA";
  } // InputThreadConstants
}
