//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LCDMessages.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: JML
// 
// CREATION DATE: 05-DIC-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-DIC-2014 JML    First release.
// 17-DEC-2014 DCS    Fixed Bug #WIG-1850: Message visible if at least one site is selected
// 22-FEB-2016 FJC    Product Backlog Item 9434:PimPamGo: Otorgar Premios
// 04-APR-2016 FJC    PBI 9105:BonoPlay: LCD: Cambios varios
// 21-SEP-2017 FJC    WIGOS-5280 Reserve EGM - Allow to reserve EGM in the LCD Intouch.
// 29-NOV-2017 FJC    WIGOS-3592 MobiBank: InTouch - Recharge request
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.IO;

namespace WSI.Common
{
  public static class LCDMessages
  {

    #region Constants

    private const Int32 WAIT_FOR_NEXT_TRY = 5;  // 5 minutes

    public const Int32 COLUMN_TERMINAL_ID = 0; //"TERMINAL_ID";
    public const Int32 COLUMN_MESSAGE_TYPE = 1; //"MESSAGE_TYPE";
    public const Int32 COLUMN_MESSAGE = 2; //"MESSAGE";
    public const Int32 COLUMN_STATUS = 3; //"STATUS";

    public const Int32 MSG_LONG_LINE_LCD = 20; // Max long per line in the LCD

    #endregion Constants

    #region "Structures"

    public struct LcdGameGateWayParams
    {
      public int GameGateway;
      public String Url;
      public String UrlTest;
      public int TerminalId;
      public String PartnerId;
      public String ProviderName;
      public int ReservedCredit;
      public int AwardPrizes;
    }

    public struct LcdFBParams
    {
      public int Enabled;
      public int PinRequest;
      public String HostAddress;
      public String Url;
      public String Login;
      public String Password;
    }

    public struct LcdTerminalDrawParams
    {
      public int Enabled;
    }

    public struct LcdTerminalReserveParams
    {
      public int Enabled;
    }

    public struct LcdMobiBankParams
    {
      public int Enabled;
    }


    #endregion


    #region Enums

    public enum ENUM_TYPE_MESSAGE
    {
      WITHOUT_CARD = 0,
      ANONYMOUS_ACCOUNT = 1,
      CANCELED_ACCOUNT = 2,
    }

    private enum ELEMENT_STATUS
    {
      Changed = 0,
      Added = 1,
      Deleted = 2,
    }

    #endregion Enums

    #region Attributes

    private static Thread m_thread;
    private static DataTable m_terminals_lcd_messages = null;
    private static ReaderWriterLock m_data_lock = new ReaderWriterLock();

    public static DataTable TerminalsLCDMessages
    {
      get
      {
        try
        {
          m_data_lock.AcquireWriterLock(Timeout.Infinite);

          return LCDMessages.m_terminals_lcd_messages;
        }
        finally
        {
          m_data_lock.ReleaseWriterLock();
        }
      }
      set
      {
        try
        {
          m_data_lock.AcquireWriterLock(Timeout.Infinite);

          LCDMessages.m_terminals_lcd_messages = value;
        }
        finally
        {
          m_data_lock.ReleaseWriterLock();
        }
      }
    }


    #endregion Attributes

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize LCDMessages class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void Init()
    {
      m_thread = new Thread(ExploitTerminalLCDMessagesThread);
      m_thread.Name = "ExploitTerminalLCDMessagesThread";

      // Thread starts
      m_thread.Start();
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Return two message lines for this terminal and message type 
    //
    //  PARAMS :
    //      - INPUT :
    //          - Terminal ID
    //          - Type Message
    //
    //      - OUTPUT :
    //          - Message Line 1
    //          - Message Line 2
    //
    // RETURNS :
    //          - True / False
    //
    //   NOTES :
    public static Boolean GetLcdMessage(Int32 TerminalID, ENUM_TYPE_MESSAGE MessageType, out String MessageLine1, out String MessageLine2)
    {
      DataRow _dr;
      Object[] _row_to_find;

      MessageLine1 = String.Empty;
      MessageLine2 = String.Empty;

      try
      {
        _row_to_find = new Object[] { TerminalID, MessageType };

        m_data_lock.AcquireReaderLock(Timeout.Infinite);

        _dr = m_terminals_lcd_messages.Rows.Find(_row_to_find);
        if (_dr == null)
        {
          return false;
        }

        return SplitLcdMessage((String)_dr[COLUMN_MESSAGE], out MessageLine1, out MessageLine2);

      }
      catch (Exception _ex)
      {
        Log.Error("LCDMessages. Exception in function: GetLcdMessage");
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        m_data_lock.ReleaseReaderLock();
      }


    } // GetLcdMessage

    //------------------------------------------------------------------------------
    // PURPOSE : Return two message lines for this terminal and message type 
    //
    //  PARAMS :
    //      - INPUT :
    //          - Xml Message
    //
    //      - OUTPUT :
    //          - Message Line 1
    //          - Message Line 2
    //
    // RETURNS :
    //          - True / False
    //
    //   NOTES :
    public static Boolean SplitLcdMessage(String MessageXml, out String MessageLine1, out String MessageLine2)
    {
      XmlReader _xml_reader;

      MessageLine1 = String.Empty;
      MessageLine2 = String.Empty;

      try
      {
        // Validate XML format
        if (!XML.TryParse(MessageXml))
        {
          return false;
        }
        _xml_reader = XmlReader.Create(new StringReader(MessageXml));

        MessageLine1 = XML.ReadTagValue(_xml_reader, "Line1");
        MessageLine2 = XML.ReadTagValue(_xml_reader, "Line2");

        if (MessageLine1.Length > MSG_LONG_LINE_LCD)
        {
          MessageLine1.Remove(MSG_LONG_LINE_LCD);
        }
        if (MessageLine2.Length > MSG_LONG_LINE_LCD)
        {
          MessageLine2.Remove(MSG_LONG_LINE_LCD);
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("LCDMessages. Exception in function: SplitLcdMessage");
        Log.Exception(_ex);
      }
      return false;

    } // SplitLcdMessage

    //------------------------------------------------------------------------------
    // PURPOSE : Return formatted XML with message
    //
    //  PARAMS :
    //      - INPUT :
    //          - Message Line 1
    //          - Message Line 2
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True / False
    //
    //   NOTES :
    public static Boolean JoinLcdMessage(String MessageLine1, String MessageLine2, out String MessageXml)
    {
      XmlDocument _xml;
      XmlElement _xml_element;

      MessageXml = String.Empty;
      _xml = new XmlDocument();

      if (String.IsNullOrEmpty(MessageLine1) && String.IsNullOrEmpty(MessageLine2))
      {
        return false;
      }

      try
      {
        if (MessageLine1.Length > MSG_LONG_LINE_LCD)
        {
          MessageLine1.Remove(MSG_LONG_LINE_LCD);
        }
        if (MessageLine2.Length > MSG_LONG_LINE_LCD)
        {
          MessageLine2.Remove(MSG_LONG_LINE_LCD);
        }

        _xml_element = _xml.CreateElement("Message");
        _xml.AppendChild(_xml_element);

        XML.AppendChild(_xml_element, "Line1", MessageLine1);
        XML.AppendChild(_xml_element, "Line2", MessageLine2);

        MessageXml = _xml.InnerXml;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("LCDMessages. Exception in function: JoinLcdMessage");
        Log.Exception(_ex);
        return false;
      }
    } // JoinLcdMessage

    #endregion Public Functions

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static void ExploitTerminalLCDMessagesThread()
    {
      Int32 _step;
      Int32 _tick0;
      Int64[] _ellapsed;
      TimeSpan _time_span;
      Int32 _wait;

      DataTable AllLCDMessages;

      _ellapsed = new Int64[2];
      _step = 0;
      _time_span = new TimeSpan(0, GeneralParam.GetInt32("WCP", "Groups.CheckChangesAfterMinutes", 5, 1, 1440), 0);
      _wait = (Int32)_time_span.TotalMilliseconds;
      TerminalsLCDMessages = CreateMemoryTable();

      while (true)
      {
        Thread.Sleep(_wait);
        _time_span = new TimeSpan(0, GeneralParam.GetInt32("WCP", "LCDMessages.MaintenanceChangesAfterMinutes", WAIT_FOR_NEXT_TRY, 1, 60), 0);
        _wait = (Int32)_time_span.TotalMilliseconds;

        try
        {

          //
          // Get data from file for first active LCD message for terminal.
          // 
          _step = 0;
          _tick0 = Environment.TickCount;
          if (!LCDMessages.ReadLCDMessages(out AllLCDMessages))
          {
            Log.Warning("Thread LCDMessages. Exception in function: " + "ReadLCDMessages");
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          _step += 1;
          _tick0 = Environment.TickCount;
          if (!LCDMessages.ProcessLCDMessages(AllLCDMessages))
          {
            Log.Warning("Thread LCDMessages. Exception in function: " + "ProcessLCDMessages");
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

#if DEBUG
          for (int _idx_step = 0; _idx_step <= _step; _idx_step++)
          {
            if (_ellapsed[_idx_step] >= 10000)
            {
              Log.Warning("ExploitTerminalLCDMessagesThread.Step[" + _idx_step.ToString() + "], Duration: " + _ellapsed[_idx_step].ToString() + " ms.");
            }
          }
#endif
        }
        catch (Exception _ex)
        {
          Log.Error("LCDMessages.ExploitTerminalLCDMessagesThread");
          Log.Exception(_ex);
        }
      } // while

    } // ExploitTerminalLCDMessagesThread

    //------------------------------------------------------------------------------
    // PURPOSE: Create the relation terminal-message table structure.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      DataTable
    //
    private static DataTable CreateMemoryTable()
    {
      DataTable _terminal_LCD_message = new DataTable();

      _terminal_LCD_message.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      _terminal_LCD_message.Columns.Add("MESSAGE_TYPE", Type.GetType("System.Int32"));
      _terminal_LCD_message.Columns.Add("MESSAGE", Type.GetType("System.String"));
      _terminal_LCD_message.Columns.Add("STATUS", Type.GetType("System.Int32"));

      _terminal_LCD_message.PrimaryKey = new DataColumn[2] { _terminal_LCD_message.Columns[COLUMN_TERMINAL_ID]
                                                           , _terminal_LCD_message.Columns[COLUMN_MESSAGE_TYPE] };

      return _terminal_LCD_message;
    } // CreateMemoryTable

    //------------------------------------------------------------------------------
    // PURPOSE : Read active LCD Messages by exploit terminals.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          -- DataTable with all active LCD Messages by terminal
    //
    // RETURNS :
    //      True: successful otherwise False
    private static Boolean ReadLCDMessages(out DataTable AllLCDMessages)
    {
      StringBuilder _sql_txt;
      Int32 _site_id;
      DateTime _date_time_now;

      _site_id = GeneralParam.GetInt32("Site", "Identifier");
      _date_time_now = WGDB.Now;

      AllLCDMessages = new DataTable();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine(" SELECT   MSG_ORDER ");
          _sql_txt.AppendLine("        , MSG_COMPUTED_ORDER ");
          _sql_txt.AppendLine("        , MSG_SITE_LIST ");
          _sql_txt.AppendLine("        , MSG_TYPE ");
          _sql_txt.AppendLine("        , MSG_MESSAGE ");
          _sql_txt.AppendLine("        , TG_TERMINAL_ID  AS TERMINAL_ID ");
          _sql_txt.AppendLine("        , MSG_ACCOUNT_LIST ");
          _sql_txt.AppendLine("        , MSG_MASTER_SEQUENCE_ID ");
          _sql_txt.AppendLine("        , MSG_SCHEDULE_WEEKDAY ");
          _sql_txt.AppendLine("        , MSG_SCHEDULE1_TIME_FROM ");
          _sql_txt.AppendLine("        , MSG_SCHEDULE1_TIME_TO ");
          _sql_txt.AppendLine("        , MSG_SCHEDULE2_ENABLED ");
          _sql_txt.AppendLine("        , MSG_SCHEDULE2_TIME_FROM ");
          _sql_txt.AppendLine("        , MSG_SCHEDULE2_TIME_TO ");
          _sql_txt.AppendLine("   FROM   LCD_MESSAGES WITH(INDEX (IX_MSG_ENABLED_START)) ");
          _sql_txt.AppendLine("  OUTER   APPLY MSG_SITE_LIST.nodes('/Sites/Site') AS X(p) ");
          _sql_txt.AppendLine("  INNER   JOIN TERMINAL_GROUPS ON MSG_UNIQUE_ID = TG_ELEMENT_ID AND TG_ELEMENT_TYPE = @pElementType ");
          _sql_txt.AppendLine("  INNER   JOIN TERMINALS ON TG_TERMINAL_ID = TE_TERMINAL_ID ");
          _sql_txt.AppendLine("  WHERE   MSG_ENABLED = 1 ");
          _sql_txt.AppendLine("    AND   MSG_SCHEDULE_START <= @pNow  ");
          _sql_txt.AppendLine("    AND   (MSG_SCHEDULE_END IS NULL OR MSG_SCHEDULE_END > @pNow) ");
          _sql_txt.AppendLine("    AND   ((MSG_MASTER_SEQUENCE_ID > 0 AND p.value('./@Id', 'NVARCHAR(4)') =  @pSiteId )");
          _sql_txt.AppendLine("          OR   ");
          _sql_txt.AppendLine("          (MSG_MASTER_SEQUENCE_ID = 0 AND MSG_SITE_LIST IS NULL))   ");
          _sql_txt.AppendLine("    AND   TE_STATUS = @pStatus ");
          _sql_txt.AppendLine("    AND   TE_BLOCKED = @pBlocked ");
          _sql_txt.AppendLine("  ORDER   BY CASE WHEN MSG_MASTER_SEQUENCE_ID = 0 THEN 0 ELSE 1 END ");
          _sql_txt.AppendLine("        , MSG_COMPUTED_ORDER DESC ");

          using (SqlCommand _cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = _date_time_now;
            _cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.LCD_MESSAGE;
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = _site_id;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TerminalStatus.ACTIVE;
            _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = false;

            using (SqlDataAdapter _sql_adap = new SqlDataAdapter(_cmd))
            {
              _db_trx.Fill(_sql_adap, AllLCDMessages);
            }
          }
        }// DB_TRX
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // ReadLCDMessages

    //------------------------------------------------------------------------------
    // PURPOSE : Process table lcd_messages and set memory table.
    //
    //  PARAMS :
    //      - INPUT :
    //          -- DataTable with all LCD Messages
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    private static Boolean ProcessLCDMessages(DataTable AllLCDMessages)
    {
      Int32 _site_id;
      DateTime _date_time_now;
      Int32 _closing_time;
      DayOfWeek _day_of_week;
      Boolean _is_day_of_week_in_schedule;

      DataTable _temp_terminals_lcd_msg;

      _site_id = GeneralParam.GetInt32("Site", "Identifier");
      _date_time_now = WGDB.Now;

      try
      {
        _temp_terminals_lcd_msg = TerminalsLCDMessages.Copy();

        foreach (DataRow _row in _temp_terminals_lcd_msg.Rows)
        {
          _row[COLUMN_STATUS] = ELEMENT_STATUS.Deleted;
        }

        if (AllLCDMessages.Rows.Count == 0)
        {
          if (DeleteOldMessages(_temp_terminals_lcd_msg))
          {
            TerminalsLCDMessages = _temp_terminals_lcd_msg;
          }

          return true;
        }

        _closing_time = GeneralParam.GetInt32("WigosGUI", "ClosingTime");

        // Update LCD Messages with appropriate values.
        foreach (DataRow _dr_lcd_messages in AllLCDMessages.Rows)
        {
          _date_time_now = WGDB.Now;
          _day_of_week = Promotion.GetWeekDay(_date_time_now, _closing_time);
          _is_day_of_week_in_schedule = ScheduleDay.WeekDayIsInSchedule(_day_of_week, (Int32)_dr_lcd_messages["MSG_SCHEDULE_WEEKDAY"]);

          // Check for enable LCD Messages
          //    If day of week is enable
          //       If Schedule is enable
          if (_is_day_of_week_in_schedule)
          {
            if (ScheduleDay.IsTimeInRange(_date_time_now.TimeOfDay, (Int32)_dr_lcd_messages["MSG_SCHEDULE1_TIME_FROM"], (Int32)_dr_lcd_messages["MSG_SCHEDULE1_TIME_TO"])
                || ((Boolean)_dr_lcd_messages["MSG_SCHEDULE2_ENABLED"]
                    && ScheduleDay.IsTimeInRange(_date_time_now.TimeOfDay, (Int32)_dr_lcd_messages["MSG_SCHEDULE2_TIME_FROM"], (Int32)_dr_lcd_messages["MSG_SCHEDULE2_TIME_TO"])))
            {
              AddMessageToTable(_temp_terminals_lcd_msg, (Int32)_dr_lcd_messages["TERMINAL_ID"], (ENUM_TYPE_MESSAGE)_dr_lcd_messages["MSG_TYPE"], _dr_lcd_messages["MSG_MESSAGE"]);
            }
          }
        } // end foreach

        if (DeleteOldMessages(_temp_terminals_lcd_msg))
        {
          TerminalsLCDMessages = _temp_terminals_lcd_msg;
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ProcessLCDMessages

    //------------------------------------------------------------------------------
    // PURPOSE : Check if exist the "terminal-message type" in memory table. 
    //                                  If not exists will add with status "Added"
    //                                  If exists the status will changed to "Changed"
    //  PARAMS :
    //      - INPUT :
    //          TerminalsLCDMsg 
    //          TerminaId
    //          ElementId
    //          ElementType
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    private static Boolean AddMessageToTable(DataTable TerminalsLCDMsg, Int32 TerminaId, ENUM_TYPE_MESSAGE MessageType, Object Message)
    {
      DataRow _terminal_group;
      Object[] _row_to_find;

      try
      {
        //Check if exist the "terminal-message type"
        _row_to_find = new Object[] { TerminaId, MessageType };
        _terminal_group = TerminalsLCDMsg.Rows.Find(_row_to_find);
        if (_terminal_group == null)
        {
          // If not exists the "terminal-message type" in memory table, will be added to table.
          _row_to_find = new Object[] { TerminaId, MessageType, Message, (Int32)ELEMENT_STATUS.Added };
          TerminalsLCDMsg.Rows.Add(_row_to_find);
        }
        else
        {
          if ((ELEMENT_STATUS)_terminal_group[COLUMN_STATUS] == ELEMENT_STATUS.Deleted)
          {
            // If "terminal-message type" exists in memory table and status is "deleted",  status will be changed to "Changed".
            _terminal_group[COLUMN_MESSAGE] = Message;
            _terminal_group[COLUMN_STATUS] = (Int32)ELEMENT_STATUS.Changed;
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("Exception: LCDMessages.AddMessageToTable");

        Log.Exception(_ex);
      }
      return false;

    } // AddMessageToTable

    //------------------------------------------------------------------------------
    // PURPOSE : Delete old terminal messages in memory. 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    private static Boolean DeleteOldMessages(DataTable TerminalsLCDMsg)
    {
      try
      {
        foreach (DataRow _row in TerminalsLCDMsg.Rows)
        {
          if ((Int32)_row[COLUMN_STATUS] == (Int32)ELEMENT_STATUS.Deleted)
          {
            _row.Delete();
          }
        }
        TerminalsLCDMsg.AcceptChanges();

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("Exception: LCDMessages.DeleteOldMessages");

        Log.Exception(_ex);
      }
      return false;
    } // DeleteOldMessages

    #endregion Private Functions

  }
}
