//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MetersGroup.cs 
// 
//   DESCRIPTION: MetersGroup class
//  
//        AUTHOR: David Hern�ndez & Jos� Martinez
// 
// CREATION DATE: 27-APR-2015
// 
// REVISION HISTORY:
// 
// Date        Author      Description
// ----------- ----------- -----------------------------------------------------
// 27-APR-2015 DHA & JML   First version.  
// 04-MAY-2015 DHA & JML   Fase 3.
// 07-FEB-2018 JML         Fixed Bug 31432:WIGOS-7898 [Ticket #12012] Incremento en Payout Te�rico por Salto de Contadores
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public enum MetersComputedOperations
  {
    OPERATION_NONE = 0,
    OPERATION_SUM = 1,
    OPERATION_SUM_BY_TOTAL_METER_DENOMINATION = 2,
  }

  public class Meter
  {
    public Int64 Code;
    public String Name;
    public Type type;
    public Dictionary<String, String> MeterRelation;

    public Boolean IsVisible;
    public Boolean OnlyDelta;

    public Decimal? DenominationValue;

    public Boolean IsComputed;
    public MetersComputedOperations ComputedOperation;
    public List<Int64> ComputedMeters;

    public Meter(Int64 Code, String Name, Type type)
    {
      this.Code = Code;
      this.Name = Name;
      this.type = type;
      this.IsVisible = true;
      this.OnlyDelta = false;
      this.DenominationValue = null;
      this.IsComputed = false;
      this.ComputedOperation = MetersComputedOperations.OPERATION_NONE;
      this.ComputedMeters = null;
    }

    public Meter(Int64 Code, String Name, Type type, Decimal? DenominationValue)
    {
      this.Code = Code;
      this.Name = Name;
      this.type = type;
      this.IsVisible = true;
      this.OnlyDelta = false;
      this.DenominationValue = DenominationValue;
      this.IsComputed = false;
      this.ComputedOperation = MetersComputedOperations.OPERATION_NONE;
      this.ComputedMeters = null;
    }

    public Meter(Int64 Code, String Name, Type type, Boolean IsVisible, Boolean OnlyDelta)
    {
      this.Code = Code;
      this.Name = Name;
      this.type = type;
      this.IsVisible = IsVisible;
      this.OnlyDelta = OnlyDelta;
      this.DenominationValue = null;
      this.IsComputed = false;
      this.ComputedOperation = MetersComputedOperations.OPERATION_NONE;
      this.ComputedMeters = null;
    }

    public Meter(Int64 Code, String Name, Type type, Boolean IsVisible, Boolean OnlyDelta, Decimal? DenominationValue)
    {
      this.Code = Code;
      this.Name = Name;
      this.type = type;
      this.IsVisible = IsVisible;
      this.OnlyDelta = OnlyDelta;
      this.DenominationValue = DenominationValue;
      this.IsComputed = false;
      this.ComputedOperation = MetersComputedOperations.OPERATION_NONE;
      this.ComputedMeters = null;
    }

    public Meter(Int64 Code, String Name, Type type, MetersComputedOperations ComputedOperation, List<Int64> ComputedMeters)
    {
      this.Code = Code;
      this.Name = Name;
      this.type = type;
      this.IsVisible = true;
      this.OnlyDelta = false;
      this.DenominationValue = null;
      this.IsComputed = true;
      this.ComputedOperation = ComputedOperation;
      this.ComputedMeters = ComputedMeters;
    }

    public Meter(Int64 Code, String Name, Type type, Boolean IsVisible, Boolean OnlyDelta, MetersComputedOperations ComputedOperation, List<Int64> ComputedMeters)
    {
      this.Code = Code;
      this.Name = Name;
      this.type = type;
      this.IsVisible = IsVisible;
      this.OnlyDelta = OnlyDelta;
      this.DenominationValue = null;
      this.IsComputed = true;
      this.ComputedOperation = ComputedOperation;
      this.ComputedMeters = ComputedMeters;
    }

  } // class Meter

  public static class MeterComputeColumns
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Compute MeterFill calculate data fro read meters
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Meter: Meter to process
    //          - List<Meter>: Meters from group of meters
    //          - DataTable: Data to process
    //          - String: name of the column where is the meter code
    //          - String: name of the column to compute and save value
    //      
    //      - OUTPUT:
    //          - Decimal: computed value
    //
    //  RETURNS:
    //      - Boolean if can be computed
    //
    public static Boolean ComputeMeterColumn(Int64 MeterCode, List<Meter> MetersList, DataTable MetersTable, String ColumnNameMeterCode, String ColumnNameToCompute)
    {
      Meter _meter;

      _meter = MetersList.Find(delegate(Meter _find_meter)
       {
         return _find_meter.Code == MeterCode;
       });

      return ComputeMeterColumn(_meter, MetersList, MetersTable, ColumnNameMeterCode, ColumnNameToCompute);
    }

    public static Boolean ComputeMeterColumn(Meter Meter, List<Meter> MetersList, DataTable MetersTable, String ColumnNameMeterCode, String ColumnNameToCompute)
    {
      Decimal _value;
      String _filter_meters;
      Meter _meter;
      List<String> _meters_code_to_compute;
      DataRow[] _meters_filted;

      _value = 0;
      _filter_meters = String.Empty;
      _meters_filted = MetersTable.Select(String.Format("{0} = {1}", ColumnNameMeterCode, Meter.Code));

      if (_meters_filted.Length == 0)
      {
        return false;
      }

      // If meters to process no are defined it will applie for all
      if (Meter.ComputedMeters.Count > 0)
      {
        _meters_code_to_compute = Meter.ComputedMeters.ConvertAll<String>(delegate(Int64 i) { return i.ToString(); });
        _filter_meters = String.Format("{0} IN ({1})", ColumnNameMeterCode, String.Join(", ", _meters_code_to_compute.ToArray()));
      }
      else
      {
        _filter_meters = String.Format("{0} NOT IN ({1})", ColumnNameMeterCode, Meter.Code);
      }

      if (Meter.IsComputed)
      {
        switch (Meter.ComputedOperation)
        {
          case MetersComputedOperations.OPERATION_NONE:
            break;

          case MetersComputedOperations.OPERATION_SUM:

            foreach (DataRow _row in MetersTable.Rows)
            {
              if (_row[ColumnNameToCompute] != DBNull.Value && Meter.ComputedMeters.Contains((Int64)_row[ColumnNameMeterCode]))
              {
                _value += (Decimal)(Int64)_row[ColumnNameToCompute];
              }
            }
            break;

          //case MetersComputedOperations.OPERATION_MIN:

          //  _value = (Decimal)MetersTable.Compute(String.Format("MIN({0})", ColumnNameToCompute), _filter_meters);
          //  break;

          //case MetersComputedOperations.OPERATION_MAX:

          //  _value = (Decimal)MetersTable.Compute(String.Format("MAX({0})", ColumnNameToCompute), _filter_meters);
          //  break;

          //case MetersComputedOperations.OPERATION_AVERAGE:

          //  _value = (Decimal)MetersTable.Compute(String.Format("AVG({0})", ColumnNameToCompute), _filter_meters);
          //  break;

          case MetersComputedOperations.OPERATION_SUM_BY_TOTAL_METER_DENOMINATION:

            if (MetersList == null)
            {
              return false;
            }

            foreach (DataRow _row in MetersTable.Rows)
            {
              if (_row[ColumnNameToCompute] != DBNull.Value && Meter.ComputedMeters.Contains((Int64)_row[ColumnNameMeterCode]))
              {
                _meter = MetersList.Find(delegate(Meter _find_meter)
                 {
                   return _find_meter.Code == (Int64)_row[ColumnNameMeterCode];
                 });

                if (_meter.DenominationValue.HasValue && _row[ColumnNameToCompute] != DBNull.Value)
                {
                  _value += (Int64)_row[ColumnNameToCompute] * _meter.DenominationValue.Value;
                }
              }
            }
            break;

          default:
            return false;
        }

        _meters_filted[0][ColumnNameToCompute] = _value;
        return true;
      }

      return false;
    }

    public static Boolean ComputeMeterColumn(Meter Meter, List<Meter> MetersList, DataTable MetersTable, Int32 ColumnMeterCode, Int32 ColumnToCompute)
    {
      return ComputeMeterColumn(Meter, MetersList, MetersTable, MetersTable.Columns[ColumnMeterCode].ColumnName, MetersTable.Columns[ColumnToCompute].ColumnName);
    }
  }

  public class MeterRelationBD
  {
    public String TableName;
    public String FieldName;
    public String FieldType;

    public MeterRelationBD(String TableName, String FieldName, String FieldType)
    {
      this.TableName = TableName;
      this.FieldName = FieldName;
      this.FieldType = FieldType;
    } // MeterRelationBD

  } // class MeterRelationBD

  public class MeterRelationsBD
  {
    public Dictionary<Int64, List<MeterRelationBD>> MeterRelations;
    public MeterRelationsBD()
    {
      MeterRelations = new Dictionary<Int64, List<MeterRelationBD>>();
    }

    public void Add(Int64 Code, String Table, String Field, String Type)
    {
      List<MeterRelationBD> _meter_relation_BD;

      _meter_relation_BD = new List<MeterRelationBD>();

      // Verificar que existe el contador
      if (MeterRelations.ContainsKey(Code))
      {
        MeterRelations.TryGetValue(Code, out _meter_relation_BD);
        _meter_relation_BD.Add(new MeterRelationBD(Table, Field, Type));
      }
      else
      {
        _meter_relation_BD.Add(new MeterRelationBD(Table, Field, Type));
        MeterRelations.Add(Code, _meter_relation_BD);
      }
    } // Add

    public void Add(Int64 Code, List<MeterRelationBD> MeterRelationDB)
    {
      MeterRelations.Add(Code, MeterRelationDB);
    }  // Add

    public static MeterRelationsBD GetMetersRelationsBD(ITerminalMeterGroup MeterGroup)
    {
      MeterRelationsBD _meter_relations_db;

      _meter_relations_db = new MeterRelationsBD();

      if (MeterGroup is MeterGroup_Statistics)
      {
        _meter_relations_db.Add(0X0000, "MACHINE_STATS_PER_HOUR", "MSH_PLAYED_AMOUNT", "System.Decimal");
        _meter_relations_db.Add(0x0000, "SALES_PER_HOUR", "SPH_PLAYED_AMOUNT", "System.Decimal");

        _meter_relations_db.Add(0x0001, "MACHINE_STATS_PER_HOUR", "MSH_WON_AMOUNT", "System.Decimal");
        _meter_relations_db.Add(0x0001, "SALES_PER_HOUR", "SPH_WON_AMOUNT", "System.Decimal");

        _meter_relations_db.Add(0x0002, "MACHINE_STATS_PER_HOUR", "MSH_JACKPOT_AMOUNT", "System.Decimal");
        _meter_relations_db.Add(0x0002, "SALES_PER_HOUR", "SPH_JACKPOT_AMOUNT", "System.Decimal");

        _meter_relations_db.Add(0x0005, "MACHINE_STATS_PER_HOUR", "MSH_PLAYED_COUNT", "System.Int64");
        _meter_relations_db.Add(0x0005, "SALES_PER_HOUR", "SPH_PLAYED_COUNT", "System.Int64");

        _meter_relations_db.Add(0x0006, "MACHINE_STATS_PER_HOUR", "MSH_WON_COUNT", "System.Int64");
        _meter_relations_db.Add(0x0006, "SALES_PER_HOUR", "SPH_WON_COUNT", "System.Int64");

      }
      else if (MeterGroup is MeterGroup_ElectronicsFundsTransfer)
      {
        _meter_relations_db.Add(0x00A0, "MACHINE_STATS_PER_HOUR", "MSH_TO_GM_AMOUNT", "System.Decimal");

        _meter_relations_db.Add(0x00B8, "MACHINE_STATS_PER_HOUR", "MSH_FROM_GM_AMOUNT", "System.Decimal");

        _meter_relations_db.Add(0x0003, "MACHINE_STATS_PER_HOUR", "MSH_HPC_HANDPAYS_AMOUNT", "System.Decimal");

      }
      else if (MeterGroup is MeterGroup_Tickets)
      {
        // Only Common tables
      }
      else if (MeterGroup is MeterGroup_Bills)
      {
        // Only Common tables
      }
      else
      {
        throw new Exception();
      }
      return _meter_relations_db;

    } // GetMetersRelationsBD 

    public static Dictionary<String, List<MeterRelationBD>> GetMetersFieldsByTable(Dictionary<Int64, List<MeterRelationBD>> MetersRelationsDB)
    {
      Dictionary<String, List<MeterRelationBD>> _meters_fields_by_table;
      List<MeterRelationBD> _fields;
      Boolean _exist;

      _meters_fields_by_table = new Dictionary<string, List<MeterRelationBD>>();

      foreach (List<MeterRelationBD> _meter_relation_list in MetersRelationsDB.Values)
      {
        foreach (MeterRelationBD _meter_relation in _meter_relation_list)
        {
          if (_meters_fields_by_table.ContainsKey(_meter_relation.TableName))
          {
            _fields = null;
            _meters_fields_by_table.TryGetValue(_meter_relation.TableName, out _fields);

            _exist = _fields.Exists(delegate(MeterRelationBD MeterRelation)
              {
                return MeterRelation.FieldName == _meter_relation.FieldName;
              });

            if (!_exist)
            {
              _fields.Add(_meter_relation);
            }
          }
          else
          {
            _fields = new List<MeterRelationBD>();
            _fields.Add(_meter_relation);
            _meters_fields_by_table.Add(_meter_relation.TableName, _fields);
          }
        }
      }
      return _meters_fields_by_table;
    } // GetMetersFieldsByTable

    public static SqlCommand GetCommandMetersRelationBD(KeyValuePair<String, List<MeterRelationBD>> _fields_by_table)
    {
      StringBuilder _sb;
      SqlCommand _sql_cmd;
      Decimal _terminal_default_payout;

      _sb = new StringBuilder();
      _sql_cmd = new SqlCommand();

      _terminal_default_payout = GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout", 95) / 100m;

      switch (_fields_by_table.Key)
      {
        case "SALES_PER_HOUR":
          _sb.AppendLine("  EXECUTE Update_Meters_SalesPerHour ");
          _sb.AppendLine("  @pBaseHour = @pBaseHour ");
          _sb.AppendLine(" ,@pTerminalId = @pTerminalId ");
          _sb.AppendLine(" ,@pGameId = @pGameId ");
          _sb.AppendLine(" ,@pTerminalDefaultPayout = @pTerminalDefaultPayout ");

          _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "DATETIME";
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GAME_ID";
          _sql_cmd.Parameters.Add("@pTerminalDefaultPayout", SqlDbType.Decimal).Value = _terminal_default_payout;

          break;
        case "MACHINE_STATS_PER_HOUR":
          _sb.AppendLine("  EXECUTE Update_Meters_MachineStatsPerHour  ");
          _sb.AppendLine("  @pBaseHour = @pBaseHour ");
          _sb.AppendLine(" ,@pTerminalId = @pTerminalId ");

          _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "DATETIME";
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";

          break;
        default:
          throw new Exception();
      }

      foreach (MeterRelationBD _field in _fields_by_table.Value)
      {
        SqlDbType _sql_db_type;
        _sql_db_type = SqlDbType.BigInt;
        if (Type.GetType(_field.FieldType) == typeof(Decimal))
        {
          _sql_db_type = SqlDbType.Money;
        }
        _sql_cmd.Parameters.Add("@p" + _field.FieldName.ToString(), _sql_db_type).SourceColumn = _field.FieldName.ToString();
        _sb.AppendLine(" ,@p" + _field.FieldName.ToString() + " = @p" + _field.FieldName.ToString());

      }
      _sql_cmd.CommandText = _sb.ToString();

      return _sql_cmd;

    } // GetCommandMetersRelationBD

  } // class MeterRelationsBD

} // WSI.Common
