//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WcpCommands.cs
// 
//   DESCRIPTION: WcpCommands Class
//                
// 
//        AUTHOR: Fernando Jim�nez
// 
// CREATION DATE: 04-NOV-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-NOV-2014 FJC    First release.
// 06-OCT-2015 FJC    Product Backlog 4704
// 19-JAN-2016 FJC    Product Backlog Item 7732:Salones(2): Introducci�n contadores PCD desde el Cajero: Parte WCP/SAS-Host
// 16-MAR-2016 FJC    PBI 9105: BonoPlay: LCD: Cambios varios
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Xml;

namespace WSI.Common
{
  #region Enums
  public enum WCP_CommandCode
  {
    Unknown = 0,
    GetLogger = 1,
    Restart = 2,
    SiteJackpotAwarded = 3, //FJC 12-NOV-2014
    RequestTransfer = 4,
    //JMM 15-OCT-2015
    RequestMeters = 5,
    //FJC GameGateway
    SubsMachinePartialCredit = 6,
    GameGateway = 7, //Check function InsertCommand and Enum in GUI

    //JMM 11-NOV-2015
    RequestChangeStacker = 8,

    //FJC GameGateway 18/11/2015
    GameGatewayGetCredit = 9,

    //JMM 22-DEC-2015
    RequestUpdatePCDMeters = 10,

    //FJC 05-FEB-2018 MoviBank
    MobiBankRechargeNotification = 11,
    MobiBankRequestTransFer = 12,
    MobiBankCancelRequest = 13,         //Cancel Request from Movile Aplicattion
  }

  public enum GetCreditStatus
  {
    PENDING = 0,
    UPDATED = 1,
    FINISHED = 2,
    CANCELLED = 3
  }
  #endregion

  public static class WcpCommands
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Inserts a command into WCP_COMMANDS
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - PlaySessionId
    //          - Code
    //          - Parameter
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //        
    //   NOTES :
    //
    public static Boolean InsertWcpCommand(Int32 TerminalId, WCP_CommandCode Code, String Parameter, SqlTransaction Trx)
    {
      return InsertWcpCommand(TerminalId, 0, Code, Parameter, Trx);
    }
    public static Boolean InsertWcpCommand(Int32 TerminalId, Int64 PlaySessionId, WCP_CommandCode Code, String Parameter, SqlTransaction Trx)
    {
      StringBuilder _sb;
      
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO WCP_COMMANDS ");
        _sb.AppendLine("           (CMD_TERMINAL_ID ");
        _sb.AppendLine("          , CMD_CODE ");
        _sb.AppendLine("          , CMD_PARAMETER ");
        _sb.AppendLine("          , CMD_STATUS ");
        _sb.AppendLine("          , CMD_CREATED ");
        _sb.AppendLine("          , CMD_STATUS_CHANGED ");
        _sb.AppendLine("          , CMD_PS_ID) ");
        _sb.AppendLine("     VALUES ( @TerminalId , @Code , @Parameter , 0 , GETDATE() , GETDATE() ,@PlaySessionId )");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@Code", SqlDbType.Int).Value = Code;
          _cmd.Parameters.Add("@Parameter", SqlDbType.NVarChar).Value = Parameter;
          if (PlaySessionId == 0)
          {
            _cmd.Parameters.Add("@PlaySessionId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@PlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          }
          
          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertWcpCommand

    //------------------------------------------------------------------------------
    // PURPOSE: Inserts a command into WCP_COMMANDS
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - Code
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //          - long:               CMD_ID
    //        
    //   NOTES :
    //
    public static long InsertEmptyWcpCommand(Int32 TerminalId, WCP_CommandCode Code, SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _param;
      long _id = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO WCP_COMMANDS ");
        _sb.AppendLine("           (CMD_TERMINAL_ID ");
        _sb.AppendLine("          , CMD_CODE ");
        _sb.AppendLine("          , CMD_PARAMETER ");
        _sb.AppendLine("          , CMD_STATUS ");
        _sb.AppendLine("          , CMD_CREATED ");
        _sb.AppendLine("          , CMD_STATUS_CHANGED ");
        _sb.AppendLine("          , CMD_PS_ID) ");
        _sb.AppendLine("     VALUES (@TerminalId , @Code , '' , 0 , GETDATE() , GETDATE() ,NULL )");
        _sb.AppendLine(" SET @pWCPId = SCOPE_IDENTITY()           ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@Code", SqlDbType.Int).Value = Code;

          _param = _cmd.Parameters.Add("@pWCPId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            _id = (Int64)_param.Value;
            return _id;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _id;
    } // InsertEmptyWcpCommand


    //------------------------------------------------------------------------------
    // PURPOSE: Update WCP commands
    //
    //  PARAMS:
    //     - INPUT:
    //          - Parameters
    //          - SqlTransaction
    //
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - True or False
    //
    public static Boolean UpdateCommandMessage(long IdCmd, String Parameter, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   WCP_COMMANDS  ");
        _sb.AppendLine("    SET   CMD_PARAMETER = @pParameter   ");
        _sb.AppendLine("  WHERE   CMD_ID        = @pWCPId       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pParameter", SqlDbType.NVarChar).Value = Parameter;
          _sql_cmd.Parameters.Add("@pWCPId", SqlDbType.BigInt).Value = IdCmd;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateGamegatewayCommandMessage

    #region GAMEGATEWAY COMMAND

    //------------------------------------------------------------------------------
    // PURPOSE: Update Reponse field command into WCP_COMMANDS
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int64 CmdId
    //          - GetCreditStatus SetValue
    //          - GetCreditStatus CompareValue
    //          - Boolean EqualValue
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //        
    //   NOTES :
    //
    public static Boolean UpdateCreditWcpCommandResponse(Int64 CmdId, 
                                                         GetCreditStatus SetValue, 
                                                         GetCreditStatus CompareValue, 
                                                         Boolean EqualValue)
    {
      StringBuilder _sb;
      
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   GAMEGATEWAY_COMMAND_MESSAGES                     ");
        _sb.AppendLine("    SET   GCM_RESPONSE       = @pSetResponse               ");
        _sb.AppendLine("        , GCM_STATUS_CHANGED = getdate()                   ");
        _sb.AppendLine("   FROM   GAMEGATEWAY_COMMAND_MESSAGES                     ");
        _sb.AppendLine("  WHERE   GCM_ID             = @pCommandId                 ");
        if (EqualValue)
        {
          if (CompareValue == GetCreditStatus.PENDING)
          {
            _sb.AppendLine("  AND   ISNULL(GCM_RESPONSE,'PENDING')  = @pResponse   ");
          }
          else
          {
            _sb.AppendLine("  AND   GCM_RESPONSE  = @pResponse                     ");
          }
        }
        else
        {
          _sb.AppendLine("  AND   GCM_RESPONSE  != @pResponse                      ");
        }
        
        //1. Update Status PENDING to Status UPDATED .
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pCommandId", SqlDbType.BigInt).Value = CmdId;
            _sql_cmd.Parameters.Add("@pResponse", SqlDbType.NVarChar).Value = CompareValue;
            _sql_cmd.Parameters.Add("@pSetResponse", SqlDbType.NVarChar).Value = SetValue.ToString();
            
            if (_sql_cmd.ExecuteNonQuery()==1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //UpdateCreditWcpCommandResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Update Reponse Process field (WCP_COMMANDS) For Get Credit (GameGateway
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int64 CmdId
    //          - GetCreditStatus SetValue
    //          - GetCreditStatus CompareValue
    //          - Boolean EqualValue
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //        
    //   NOTES :
    //
    public static Boolean GetGetCreditWcpCommandResponse(Int64 CmdId, 
                                                         Int32 SecondsExpire)
    {
      StringBuilder _sb;
      Object _obj;
      DateTime _init_process = WGDB.Now;
      
      try
      {
        
        //1. Update Status PENDING to Status UPDATED .
        if (!UpdateCreditWcpCommandResponse(CmdId, 
                                            GetCreditStatus.UPDATED, 
                                            GetCreditStatus.PENDING,
                                            true))
        {
          
          return false; //Error
  }

        //2. Wait N seconds for Status changing
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GCM_RESPONSE                  ");
        _sb.AppendLine("   FROM   GAMEGATEWAY_COMMAND_MESSAGES  ");
        _sb.AppendLine("  WHERE   GCM_ID        = @pCommandId   ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pCommandId", SqlDbType.BigInt).Value = CmdId;

            while (_init_process.AddSeconds(SecondsExpire) > WGDB.Now)
            {
              _obj = _sql_cmd.ExecuteScalar();

              if (_obj == null)
              {
                return false;                   //Error
              }
              else if (_obj != DBNull.Value)
              {
                if ((String)_obj == GetCreditStatus.FINISHED.ToString())  //GetCreditStatus.FINISHED
                {
                  return true;
                }
              }
              System.Threading.Thread.Sleep(500);
            }
          }
        }

        //3. Update Status FROM PENDING to CANCELLED if all went KO
        if (UpdateCreditWcpCommandResponse(CmdId,
                                           GetCreditStatus.CANCELLED,
                                           GetCreditStatus.FINISHED,
                                           false))
        {
          
          return false; //Status Setted to CANCELLED ==> ERROR
        }
        else
        {
          return true;  //Another Process has setted FINISHED
        }
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetGetCreditWcpCommandResponse
    #endregion

  }

  public class SiteJackpotAwarded
  {
    //JackPot Data
    public Int64 JackpotId = 0;
    public DateTime JackpotDatetime = new DateTime();      
    public Int32 JackpotLevelId = 0;
    public String JackpotLevelName = "";
    public Int64 JackpotAmountCents = 0;
    public String JackpotMachineName = "";

    //Winner JackPot Data
    public String WinnerTrackData = "";
    public String WinnerHolderName = "";
      
    //Parameters JackPot Data
    public Int16 ParametersShowOnlyToWinner = 0;
    public Int32 ParametersWinnerShowTimeSeconds = 0;
    public Int32 ParametersNoWinnerShowTimeSeconds = 0;

    public String ToXml()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("<SiteJackpotAwarded>");

        //JackPot Data
        _sb.AppendLine("<Jackpot>");
        _sb.AppendLine("<Id>" + JackpotId.ToString() + "</Id>");
        _sb.AppendLine("<Datetime>" + XML.XmlDateTimeString(JackpotDatetime) + "</Datetime>");
        _sb.AppendLine("<LevelId>" + JackpotLevelId.ToString() + "</LevelId>");
        _sb.AppendLine("<LevelName>" + JackpotLevelName + "</LevelName>");
        _sb.AppendLine("<AmountCents>" + JackpotAmountCents.ToString() + "</AmountCents>");
        _sb.AppendLine("<MachineName>" + JackpotMachineName + "</MachineName>");
        _sb.AppendLine("</Jackpot>");

        //Winner JackPot Data
        _sb.AppendLine("<Winner>");
        _sb.AppendLine("<Trackdata>" + WinnerTrackData + "</Trackdata>");
        _sb.AppendLine("<HolderName>" + WinnerHolderName + "</HolderName>");
        _sb.AppendLine("</Winner>");

        //Parameters JackPot Data
        _sb.AppendLine("<Parameters>");
        _sb.AppendLine("<ShowOnlyToWinner>" + ParametersShowOnlyToWinner.ToString() + "</ShowOnlyToWinner>");
        _sb.AppendLine("<WinnerShowTimeSeconds>" + ParametersWinnerShowTimeSeconds.ToString() + "</WinnerShowTimeSeconds>");
        _sb.AppendLine("<NoWinnerShowTimeSeconds>" + ParametersNoWinnerShowTimeSeconds.ToString() + "</NoWinnerShowTimeSeconds>");
        _sb.AppendLine("</Parameters>");

        _sb.AppendLine("</SiteJackpotAwarded> ");

        return _sb.ToString();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return String.Empty;
      }
    }

    public void LoadXml(String XmlData)
    {
      XmlDocument _xml;
      XmlNode _node;

      _xml = new XmlDocument();

      try
      {
        _xml.LoadXml(XmlData);
        
        //JackPot Data
        _node = _xml.SelectSingleNode("SiteJackpotAwarded/Jackpot");
        JackpotId = Int64.Parse(XML.GetValue(_node, "Id"));
        DateTime.TryParse(XML.GetValue(_node, "Datetime"), out JackpotDatetime);
        JackpotLevelId = Int32.Parse(XML.GetValue(_node, "LevelId"));
        JackpotLevelName= XML.GetValue(_node, "LevelName");
        JackpotAmountCents = Int64.Parse(XML.GetValue(_node, "AmountCents"));
        JackpotMachineName = XML.GetValue(_node, "MachineName");

        //Winner JackPot Data
        _node = _xml.SelectSingleNode("SiteJackpotAwarded/Winner");
        WinnerTrackData = XML.GetValue(_node, "Trackdata");
        WinnerHolderName = XML.GetValue(_node, "HolderName");

        //Parameters JackPot Data
        _node = _xml.SelectSingleNode("SiteJackpotAwarded/Parameters");
        ParametersShowOnlyToWinner = Int16.Parse(XML.GetValue(_node, "ShowOnlyToWinner"));
        ParametersWinnerShowTimeSeconds = Int32.Parse(XML.GetValue(_node, "WinnerShowTimeSeconds"));
        ParametersNoWinnerShowTimeSeconds = Int32.Parse(XML.GetValue(_node, "NoWinnerShowTimeSeconds"));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
  }

  public class PCD_UpdatePCDMeters
  {
    public String ParamsStr = String.Empty;
    public List<UpdatablePCDMeter> UpdatablePCDMeterList;

    public PCD_UpdatePCDMeters()
    { 
      UpdatablePCDMeterList = new List<UpdatablePCDMeter>();
    }

    public class UpdatablePCDMeter
    {
      public Int32 Code;
      public Int64 Value;
      public Int32 Action;      
    }

    public void LoadXml(String XmlData)
    {
      XmlDocument _xml;
      XmlReader _reader;
      UpdatablePCDMeter _updatable_pcd_meter;

      _xml = new XmlDocument();

      try
      {
        ParamsStr = XmlData;

        _reader = XmlReader.Create(new System.IO.StringReader(XmlData));
        _reader.Read();

        if (!_reader.Name.Equals("PCDMeters"))
        {
          Log.Message("Tag PCDMeters not found in content area");
        }
        else
        {
          // Main denomination list
          while (_reader.Name != "Meter" || !_reader.NodeType.Equals(XmlNodeType.Element))
          {
            _reader.Read();  // Read until End Element 'DenominationList'
          }

          while (_reader.Name == "Meter" && !_reader.NodeType.Equals(XmlNodeType.EndElement))
          {
            _updatable_pcd_meter = new UpdatablePCDMeter();
            
            Int32.TryParse(_reader.GetAttribute("Code"), out _updatable_pcd_meter.Code);
            Int64.TryParse(_reader.GetAttribute("Value"), out _updatable_pcd_meter.Value);
            Int32.TryParse(_reader.GetAttribute("Action"), out _updatable_pcd_meter.Action);

            UpdatablePCDMeterList.Add(_updatable_pcd_meter);

            _reader.Read();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
  }  
}