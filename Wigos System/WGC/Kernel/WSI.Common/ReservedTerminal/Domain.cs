﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reserved Terminal
// 
//   DESCRIPTION: Domain entities for the module ReservedTerminal
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 03-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-AUG-2016 RGR    Initial Version
// 09-AUG-2016 RGR    Add new models
// 12-AUG-2016 RGR    Remove Issues
// 17-FEB-2017 FAV    GetMinutesReservedByAccoount added
// 24-JUL-2017 ATB    PBI 28710: EGM Reserve – Settings
// 31-JUL-2017 ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI
// 01-AUG-2017 ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.ReservedTerminal
{
  #region Enums
  public enum ReservedStatusType
  {
    Reserved = 1,
    Lapsed = 2,
    Completed = 3
  }
  #endregion

  #region Models
  public class AccountTimeLevel
  {
    public HolderLevelType LevelType { get; set; }

    public int Minutes { get; set; }

    public string TerminalList { get; set; }

    public IList<int> Terminals { get; set; }

    public bool Enabled { get; set; }

    public decimal CoinIn { get; set; }
  }

  public class ReservedTerminalConfiguration
  {
    public string TerminalList { get; set; }

    public IList<AccountTimeLevel> TimeLevels { get; set; }
  }

  public class ReservedTerminalTransaction
  {
    public long ID { get; set; }

    public ReservedTerminalReservedRequest Keys { get; set; }

    public DateTime StartReserved { get; set; }

    public DateTime EndReserved { get; set; }

    public DateTime StartUtc { get; set; }

    public DateTime EndUtc { get; set; }

    public int MaxMinutes { get; set; }

    public int TotalMinutes { get; set; }

    public ReservedStatusType Status { get; set; }

    internal void CalculateMinutes()
    {
      TimeSpan _time;

      this.EndReserved = DateTime.Now;
      this.EndUtc = this.EndReserved.ToUniversalTime();
      this.Status = ReservedStatusType.Completed;
      _time = this.EndUtc - this.StartUtc;
      this.TotalMinutes = _time.Minutes;
    }
  }
  #endregion

  #region Request
  public class ReservedTerminalSaveRequest
  {
    public IList<AccountTimeLevel> TimeLevels { get; set; }
  }

  public class ReservedTerminalReservedRequest
  {
    public int TerminalID { get; set; }

    public long AccountID { get; set; }

    public string TerminalName { get; set; }
  }
  #endregion

  #region Response
  public class ReservedTerminalResponseBase
  {
    public string Message { get; set; }

    public int Code { get; set; }

    public bool Success { get; set; }

    public string MessageExtended { get; set; }
  }

  public class ReservedTerminalConfigurationResponse : ReservedTerminalResponseBase
  {
    public ReservedTerminalConfiguration Configurations { get; set; }
  }

  public class ReservedTerminalResponse<T> : ReservedTerminalResponseBase
  {
    public T Result { get; set; }
  }

  #endregion

  #region Interfaces

  public interface IReservedTerminalConfigurationService : IDisposable
  {
    ReservedTerminalResponseBase Save(ReservedTerminalSaveRequest request);

    ReservedTerminalResponseBase Read();
  }

  public interface IReservedTerminalService : IDisposable
  {
    ReservedTerminalResponse<bool> IsReservedTerminal(int terminalID);

    ReservedTerminalResponse<bool> IsReservedAccount(long accountID);

    ReservedTerminalResponse<bool> TerminalItCanReserve(ReservedTerminalReservedRequest request);

    ReservedTerminalResponse<bool> ExistsPlaySession(ReservedTerminalReservedRequest request);

    ReservedTerminalResponse<bool> Add(ReservedTerminalReservedRequest request);

    ReservedTerminalResponse<bool> Remove(ReservedTerminalReservedRequest request);

    ReservedTerminalResponse<bool> RemoveAll();

    ReservedTerminalResponse<int> GetMinutesReservedByAccoount(long accountID);

    ReservedTerminalTransaction GetTransaction(ReservedTerminalReservedRequest request);
  }

  #endregion
}
