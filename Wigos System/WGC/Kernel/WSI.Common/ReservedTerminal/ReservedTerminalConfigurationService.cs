﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reserved Terminal
// 
//   DESCRIPTION: Implement IReservedTerminalService
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 03-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-AUG-2016 RGR    Initial Version
// 12-AUG-2016 RGR    Remove Issues
// 24-JUL-2017 ATB    PBI 28710: EGM Reserve – Settings
// 08-AUG-2017 ATB    PBI 28710: EGM Reserve – Settings
// 10-AUG-2017 ATB    PBI 28710: EGM Reserve – Settings (WIGOS-4297)
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.Utilities;

namespace WSI.Common.ReservedTerminal
{
  public class ReservedTerminalConfigurationService : IReservedTerminalConfigurationService
  {
    #region Members
    private SqlDataAdapter m_adapter;
    private DataTable m_table;
    private DB_TRX m_db_trx;
    #endregion   
  
    #region Constructor
    public ReservedTerminalConfigurationService()
    {
      DataColumn[] _keys;

      m_db_trx = new DB_TRX();
      m_table = new DataTable();
      _keys = new DataColumn[1];
      m_adapter = new SqlDataAdapter();
    

      m_adapter.SelectCommand = this.GetReadCommand();
      m_adapter.InsertCommand = this.GetCreateCommand();
      m_adapter.UpdateCommand = this.GetUpdateCommand();
      m_adapter.Fill(m_table);
      _keys[0] = this.m_table.Columns["RTC_HOLDER_LEVEL"];
      m_table.PrimaryKey = _keys;
    }
    #endregion

    #region IReservedTerminalConfigurationService
    public ReservedTerminalResponseBase Save(ReservedTerminalSaveRequest request)
    {
      ReservedTerminalResponseBase _response;
      DataRow _find;
      try
      {
        foreach (var item in request.TimeLevels)
        {
          _find = this.Find(item.LevelType);

          if (_find == null)
          {
            this.Create(item);
          }
          else
          {
            this.Update(item, _find);
          }
        }
        this.m_adapter.Update(m_table);
        this.m_db_trx.Commit(); 
        _response = ReservedTerminalResponseBaseFactory.Create(1, "Transaction complete", true);
      }
      catch (Exception ex)
      {
        _response = ReservedTerminalResponseBaseFactory.Create(-1, ex.Message, false);
      }
      return _response;
    }

    public ReservedTerminalResponseBase Read()
    {
      ReservedTerminalResponseBase _response;
      try
      {
        _response = ReservedTerminalResponseBaseFactory.Create(m_table);
      }
      catch (Exception ex)
      {
        _response = ReservedTerminalResponseBaseFactory.Create(-1, ex.Message, false);
      }
      return _response;
    }
    #endregion

    #region IDisposable
    public void Dispose()
    {
      if (m_table != null)
      {
        m_table.Dispose();
        m_table = null;
      }

      if (m_adapter != null)
      {
        m_adapter.Dispose();
        m_adapter = null;
      }

      if (m_db_trx != null)
      {
        m_db_trx.Dispose();
        m_db_trx = null;
      }
    }
    #endregion

    #region Private Methods
    private SqlCommand GetReadCommand()
    {
      SqlCommand _cmd;
      StringBuilder _sql;

      _sql = new StringBuilder();
      _sql.Append("SELECT RTC_HOLDER_LEVEL               ");
      _sql.Append("      ,RTC_MINUTES                    ");
      _sql.Append("      ,RTC_TERMINAL_LIST              ");
      _sql.Append("      ,RTC_SELECTED_TERMINALS         ");
      _sql.Append("      ,RTC_CREATE_DATE                ");
      _sql.Append("      ,RTC_UPDATE_DATE                ");
      _sql.Append("      ,RTC_ENABLED                    ");
      _sql.Append("      ,RTC_COIN_IN                    ");
      _sql.Append("      ,RTC_STATUS                     ");
      _sql.Append("FROM RESERVED_TERMINAL_CONFIGURATION  ");

      _cmd = new SqlCommand(_sql.ToString(), m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction);

      return _cmd;
    }

    private SqlCommand GetCreateCommand()
    {
      SqlCommand _cmd;
      StringBuilder _sql;

      _sql = new StringBuilder();
      _sql.Append("INSERT INTO RESERVED_TERMINAL_CONFIGURATION   ");
      _sql.Append("       (RTC_HOLDER_LEVEL                      ");
      _sql.Append("       ,RTC_MINUTES                           ");
      _sql.Append("       ,RTC_TERMINAL_LIST                     ");
      _sql.Append("       ,RTC_SELECTED_TERMINALS                ");
      _sql.Append("       ,RTC_CREATE_DATE                       ");
      _sql.Append("       ,RTC_UPDATE_DATE                       ");
      _sql.Append("       ,RTC_ENABLED                           ");
      _sql.Append("       ,RTC_COIN_IN                           ");
      _sql.Append("       ,RTC_STATUS)                           ");
      _sql.Append("       VALUES                                 ");
      _sql.Append("       (@pHolderLevel                         ");
      _sql.Append("       ,@pMinutes                             ");
      _sql.Append("       ,@pTerminalList                        ");
      _sql.Append("       ,@pSelectedTerminals                   ");
      _sql.Append("       ,@pCreateDate                          ");
      _sql.Append("       ,@pUpdateDate                          ");
      _sql.Append("       ,@pEnabled                             ");
      _sql.Append("       ,@pCoinIn                              ");
      _sql.Append("       ,@pStatus)                             ");

      _cmd = new SqlCommand(_sql.ToString(), m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction);
      _cmd.Parameters.Add("@pHolderLevel", SqlDbType.Int).SourceColumn = "RTC_HOLDER_LEVEL";
      _cmd.Parameters.Add("@pMinutes", SqlDbType.Int).SourceColumn = "RTC_MINUTES";
      _cmd.Parameters.Add("@pTerminalList", SqlDbType.Xml).SourceColumn = "RTC_TERMINAL_LIST";
      _cmd.Parameters.Add("@pSelectedTerminals", SqlDbType.Xml).SourceColumn = "RTC_SELECTED_TERMINALS";
      _cmd.Parameters.Add("@pCreateDate ", SqlDbType.DateTime).SourceColumn = "RTC_CREATE_DATE";
      _cmd.Parameters.Add("@pUpdateDate", SqlDbType.DateTime).SourceColumn = "RTC_UPDATE_DATE";
      _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).SourceColumn = "RTC_ENABLED";
      _cmd.Parameters.Add("@pCoinIn", SqlDbType.Decimal).SourceColumn = "RTC_COIN_IN";
      _cmd.Parameters.Add("@pStatus", SqlDbType.Decimal).SourceColumn = "RTC_STATUS";

      return _cmd;
    }

    private SqlCommand GetUpdateCommand()
    {
      SqlCommand _cmd;
      StringBuilder _sql;

      _sql = new StringBuilder();
      _sql.Append("UPDATE  RESERVED_TERMINAL_CONFIGURATION  SET         ");
      _sql.Append("        RTC_MINUTES = @pMinutes                      ");
      _sql.Append("       ,RTC_TERMINAL_LIST = @pTerminalList           ");
      _sql.Append("       ,RTC_SELECTED_TERMINALS = @pSelectedTerminals ");
      _sql.Append("       ,RTC_UPDATE_DATE =  @pUpdateDate              ");
      _sql.Append("       ,RTC_ENABLED =  @pEnabled                     ");
      _sql.Append("       ,RTC_COIN_IN =  @pCoinIn                      ");
      _sql.Append("WHERE   RTC_HOLDER_LEVEL = @pHolderLevel             ");

      _cmd = new SqlCommand(_sql.ToString(), m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction);
      _cmd.Parameters.Add("@pHolderLevel", SqlDbType.Int).SourceColumn = "RTC_HOLDER_LEVEL";
      _cmd.Parameters.Add("@pMinutes", SqlDbType.Int).SourceColumn = "RTC_MINUTES";
      _cmd.Parameters.Add("@pTerminalList", SqlDbType.Xml).SourceColumn = "RTC_TERMINAL_LIST";
      _cmd.Parameters.Add("@pSelectedTerminals", SqlDbType.Xml).SourceColumn = "RTC_SELECTED_TERMINALS";
      _cmd.Parameters.Add("@pUpdateDate", SqlDbType.DateTime).SourceColumn = "RTC_UPDATE_DATE";
      _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).SourceColumn = "RTC_ENABLED";
      _cmd.Parameters.Add("@pCoinIn", SqlDbType.Decimal).SourceColumn = "RTC_COIN_IN";

      return _cmd;
    }

    private DataRow Find(HolderLevelType levelType)
    {
      object[] _keys;
      DataRow _rowFind;

      _keys = new object[1];
      _keys[0] = (int)levelType;
     
      _rowFind = this.m_table.Rows.Find(_keys);

      return _rowFind;
    }

    private void Create(AccountTimeLevel accountTimeLevel)
    {
      DataRow _row;

      _row = this.m_table.NewRow();
      _row["RTC_HOLDER_LEVEL"] = (int)accountTimeLevel.LevelType;
      _row["RTC_MINUTES"] = accountTimeLevel.Minutes;
      _row["RTC_TERMINAL_LIST"] = accountTimeLevel.TerminalList;
      _row["RTC_SELECTED_TERMINALS"] = XmlSerializationHelper.CleanSerialize<ProviderList>(ProviderListFactory.Create(accountTimeLevel.Terminals));
      _row["RTC_CREATE_DATE"] = WGDB.Now;
      _row["RTC_UPDATE_DATE"] = WGDB.Now;
      _row["RTC_ENABLED"] = accountTimeLevel.Enabled;
      _row["RTC_COIN_IN"] = accountTimeLevel.CoinIn;
      _row["RTC_STATUS"] = 1;

      this.m_table.Rows.Add(_row);
    }

    private void Update(AccountTimeLevel accountTimeLevel, DataRow rowFind)
    {
      rowFind["RTC_MINUTES"] = accountTimeLevel.Minutes;
      rowFind["RTC_TERMINAL_LIST"] = accountTimeLevel.TerminalList;
      rowFind["RTC_SELECTED_TERMINALS"] = XmlSerializationHelper.CleanSerialize<ProviderList>(ProviderListFactory.Create(accountTimeLevel.Terminals));
      rowFind["RTC_UPDATE_DATE"] = WGDB.Now;
      rowFind["RTC_ENABLED"] = accountTimeLevel.Enabled;
      rowFind["RTC_COIN_IN"] = accountTimeLevel.CoinIn;
    }
    #endregion
  }
}
