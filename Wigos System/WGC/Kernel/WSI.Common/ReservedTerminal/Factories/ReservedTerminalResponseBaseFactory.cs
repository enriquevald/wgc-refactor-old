﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reserved Terminal
// 
//   DESCRIPTION: Factory ReservedTerminalResponseBaseFactory
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 03-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-AUG-2016 RGR    Initial Version
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WSI.Common.ReservedTerminal
{
  public class ReservedTerminalResponseBaseFactory
  {
    internal static ReservedTerminalResponseBase Create(int code, string message, bool success)
    {
      ReservedTerminalResponseBase _response;

      _response = new ReservedTerminalResponseBase();
      _response.Code = code;
      _response.Message = message;
      _response.Success = success;

      return _response;
    }

    internal static ReservedTerminalResponseBase Create(DataTable table)
    {
      ReservedTerminalConfigurationResponse _response;

      _response = new ReservedTerminalConfigurationResponse();
      _response.Code = 1;
      _response.Message = "Done! get data collection from reserved_terminal_configuration";
      _response.Success = true;
      _response.Configurations = ReservedTerminalConfigurationFactory.Create(table);
      return _response;
    }  
  }
}
