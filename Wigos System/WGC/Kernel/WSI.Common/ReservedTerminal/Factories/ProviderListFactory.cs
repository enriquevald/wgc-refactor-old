﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reserved Terminal
// 
//   DESCRIPTION: Domain entities for the module ReservedTerminal
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 03-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2016 RGR    Initial Version
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.ReservedTerminal
{
  internal class ProviderListFactory
  {

    internal static ProviderList Create(IList<int> list)
    {
      ProviderList _provider;
      
      _provider = new ProviderList();
    
      if (list != null)
      {
        _provider.Terminals = new int[list.Count];
        for (int i = 0; i < _provider.Terminals.Length; i++)
        {
          _provider.Terminals[i] = list[i];
        }
      }

      return _provider;
    }
  }
}
