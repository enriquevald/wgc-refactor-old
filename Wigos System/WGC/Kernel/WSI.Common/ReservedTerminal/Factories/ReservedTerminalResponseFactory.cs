﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reserved Terminal
// 
//   DESCRIPTION: Factory ReservedTerminalResponse
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 09-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-AUG-2016 RGR    Initial Version
// 31-JUL-2017 ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI
// 01-AUG-2017 ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.ReservedTerminal
{
  public class ReservedTerminalResponseFactory<T>
  {

    public static ReservedTerminalResponse<T> Create(T result, int code, string message, bool success, string messageExtended = "")
    {
      ReservedTerminalResponse<T> _response = new ReservedTerminalResponse<T>();

      _response.Code = code;
      _response.Message = message;
      _response.Success = success;
      _response.Result = result;
      _response.MessageExtended = messageExtended;

      return _response;
    }   
  }
}
