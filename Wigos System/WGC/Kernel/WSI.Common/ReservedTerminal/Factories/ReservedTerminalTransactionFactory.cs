﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reserved Terminal
// 
//   DESCRIPTION: Factory ReservedTerminalTransaction
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 09-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-AUG-2016 RGR    Initial Version
// 01-AUG-2017 ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.ReservedTerminal
{
  public class ReservedTerminalTransactionFactory
  {
    public static ReservedTerminalTransaction Create(ReservedTerminalReservedRequest keys, int maxMinutes)
    {
      ReservedTerminalTransaction _transaction;

      _transaction = new ReservedTerminalTransaction();
      _transaction.Keys = keys;
      _transaction.MaxMinutes = maxMinutes;
      _transaction.StartReserved = DateTime.Now;
      _transaction.StartUtc = _transaction.StartReserved.ToUniversalTime();
      _transaction.Status = ReservedStatusType.Reserved;

      return _transaction;
    }

    public static ReservedTerminalTransaction Create(SqlDataReader read)
    {
      ReservedTerminalTransaction _transaction;
      _transaction = new ReservedTerminalTransaction();
      _transaction.ID = (long)read["RTT_TRANSACTION_ID"];
      _transaction.Keys = new ReservedTerminalReservedRequest();
      _transaction.Keys.AccountID = (long)read["RTT_ACCOUNT_ID"];
      _transaction.Keys.TerminalID = (int)read["RTT_TERMINAL_ID"];
      _transaction.StartReserved = (DateTime)read["RTT_START_RESERVED"];
      _transaction.StartUtc = (DateTime)read["RTT_START_UTC"];
      _transaction.MaxMinutes = (int)read["RTT_MAX_MINUTES"];
      _transaction.Status = (ReservedStatusType)read["RTT_STATUS"];
      return _transaction;
    }
  }
}
