﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reserved Terminal
// 
//   DESCRIPTION: Factory ReservedTerminalConfigurationFactory
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 03-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-AUG-2016 RGR    Initial Version
// 12-AUG-2016 RGR    Remove Issues
// 24-JUL-2017 ATB    PBI 28710: EGM Reserve – Settings
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WSI.Common.ReservedTerminal
{
  public class ReservedTerminalConfigurationFactory
  {
    internal static ReservedTerminalConfiguration Create(DataTable table)
    {
      ReservedTerminalConfiguration _configuration;
      AccountTimeLevel _level;

      _configuration = new ReservedTerminalConfiguration();
      _configuration.TimeLevels = new List<AccountTimeLevel>();
      _configuration.TerminalList = string.Empty;

      foreach (DataRow _item in table.Rows)
      {
        _level = new AccountTimeLevel();
        _level.LevelType = (HolderLevelType)_item["RTC_HOLDER_LEVEL"];
        _level.Minutes = (int)_item["RTC_MINUTES"];
        if (_item["RTC_ENABLED"] is DBNull)
        {
          _level.Enabled = false;
        }
        else
        {
          _level.Enabled = (bool)_item["RTC_ENABLED"];
        }
        if (_item["RTC_COIN_IN"] is DBNull)
        {
          _level.CoinIn = 0;
        }
        else
        {
          _level.CoinIn = (decimal)_item["RTC_COIN_IN"];
        }

        if (_item["RTC_TERMINAL_LIST"] != DBNull.Value)
        {
          _level.TerminalList = (string)_item["RTC_TERMINAL_LIST"];
        }
        _configuration.TimeLevels.Add(_level);
      }

      if (table.Rows.Count > 0)
      {
        _configuration.TerminalList = table.Rows[0]["RTC_TERMINAL_LIST"].ToString();
      }

      return _configuration;
    }
  }
}
