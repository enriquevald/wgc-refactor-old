﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reserve Terminal
// 
//   DESCRIPTION: Implement IReserveTerminalService
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 04-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-AUG-2016 RGR    Initial Version
// 12-AUG-2016 RGR    Remove Issues
// 17-FEB-2017 FAV    GetMinutesReservedByAccoount added
// 31-JUL-2017 ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI
// 01-AUG-2017 ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI
// 03-AUG-2017 ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI (WIGOS-4000)
// 02-AUG-2017 JMM    PBI 28983:EGM Reserve - Phase 1 - Reserve on EGM by Carded Player
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.ReservedTerminal;
using WSI.Common.Utilities;

namespace WSI.Common.ReservedTerminal
{
  public class ReservedTerminalService : IReservedTerminalService
  {
    #region Structures
    struct TERMINAL_DATA
    {
      string Name;
      string Id;
    }
    #endregion

    #region Members
    private DB_TRX m_db_trx;
    #endregion

    #region Constructor
    public ReservedTerminalService()
    {
      m_db_trx = new DB_TRX();

    }
    #endregion

    #region IReservedTerminalService
    public ReservedTerminalResponse<bool> IsReservedTerminal(int terminalID)
    {
      StringBuilder _sb;
      ReservedTerminalResponse<bool> _response;

      _sb = new StringBuilder();
      _sb.Append("SELECT ISNULL(TS_IS_RESERVED, 0) AS TS_IS_RESERVED ");
      _sb.Append("FROM TERMINAL_STATUS                               ");
      _sb.Append("WHERE TS_TERMINAL_ID = @pResultIn                  ");
      _response = this.Result<bool, int>(terminalID, _sb.ToString());

      return _response;
    }

    public ReservedTerminalResponse<bool> IsReservedAccount(long accountID)
    {
      StringBuilder _sb;
      ReservedTerminalResponse<bool> _response;

      _sb = new StringBuilder();
      _sb.Append("SELECT CAST(CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END AS BIT) ");
      _sb.Append("FROM RESERVED_TERMINAL_TRANSACTION                           ");
      _sb.Append("WHERE RTT_ACCOUNT_ID = @pResultIn AND RTT_STATUS = 1         ");

      _response = this.Result<bool, long>(accountID, _sb.ToString());

      return _response;
    }

    public ReservedTerminalResponse<int> GetMinutesReservedByAccoount(long accountID)
    {
      StringBuilder _sb;
      ReservedTerminalResponse<int> _response;

      _sb = new StringBuilder();
      _sb.Append("SELECT RTC_MINUTES                         ");
      _sb.Append("FROM ACCOUNTS                              ");
      _sb.Append("INNER JOIN RESERVED_TERMINAL_CONFIGURATION ");
      _sb.Append("ON AC_HOLDER_LEVEL = RTC_HOLDER_LEVEL      ");
      _sb.Append("WHERE AC_ACCOUNT_ID = @pResultIn           ");

      _response = this.Result<int, long>(accountID, _sb.ToString());

      return _response;
    }

    public ReservedTerminalResponse<bool> TerminalItCanReserve(ReservedTerminalReservedRequest request)
    {
      StringBuilder _sb;
      ReservedTerminalResponse<bool> _response;
      ReservedTerminalResponse<string> _providers;
      ProviderList _providerList;

      _sb = new StringBuilder();
      _sb.Append("SELECT RTC_SELECTED_TERMINALS              ");
      _sb.Append("FROM ACCOUNTS                              ");
      _sb.Append("INNER JOIN RESERVED_TERMINAL_CONFIGURATION ");
      _sb.Append("ON AC_HOLDER_LEVEL = RTC_HOLDER_LEVEL      ");
      _sb.Append("WHERE AC_ACCOUNT_ID = @pResultIn           ");

      _providers = this.Result<string, long>(request.AccountID, _sb.ToString());

      if (_providers.Result != null)
      {
        try
        {
          _providerList = XmlSerializationHelper.Deserialize<ProviderList>(_providers.Result);
          if (this.ContainsTerminal(request.TerminalID, _providerList))
          {
            _response = ReservedTerminalResponseFactory<bool>.Create(true, 1, Resource.String("STR_RESERVATION_MSG_TERMINAL_CAN_BE_RESERVED"), true);
          }
          else
          {
            _response = ReservedTerminalResponseFactory<bool>.Create(false, 2, Resource.String("STR_RESERVATION_MSG_TERMINAL_CANNOT_BE_RESERVED"), true);
          }
        }
        catch (Exception ex)
        {
          _response = ReservedTerminalResponseFactory<bool>.Create(false, -1, ex.Message, false);
        }
      }
      else
      {
        _response = ReservedTerminalResponseFactory<bool>.Create(false, _providers.Code, _providers.Message, _providers.Success);
      }
      return _response;
    }

    public ReservedTerminalResponse<bool> ExistsPlaySession(ReservedTerminalReservedRequest request)
    {
      ReservedTerminalResponse<bool> _response;
      bool _success;
      try
      {
        using (SqlCommand _cmd = new SqlCommand("ExistsPlaySession", m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = request.AccountID;
          _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = request.TerminalID;
          _success = (bool)_cmd.ExecuteScalar();
          if (_success)
          {
            _response = ReservedTerminalResponseFactory<bool>.Create(true, 1, Resource.String("STR_RESERVATION_MSG_EXIST_PLAY_SESSION"), true);
          }
          else
          {
            _response = ReservedTerminalResponseFactory<bool>.Create(false, 2, Resource.String("STR_RESERVATION_MSG_NOT_EXISTS_PLAY_SESSION"), true);
          }
        }
      }
      catch (Exception ex)
      {
        _response = _response = ReservedTerminalResponseFactory<bool>.Create(false, -1, ex.Message, false);
      }
      return _response;
    }

    public ReservedTerminalResponse<bool> Add(ReservedTerminalReservedRequest request)
    {
      ReservedTerminalResponse<bool> _response;

      _response = this.IsReservedTerminal(request.TerminalID);
      if (!_response.Success || _response.Result)
      {
        return ReservedTerminalResponseFactory<bool>.Create(false, 6, Resource.String("STR_RESERVATION_MSG_TERMINAL_RESERVED"), _response.Success);
      }

      _response = this.IsReservedAccount(request.AccountID);
      if (!_response.Success || _response.Result)
      {
        return ReservedTerminalResponseFactory<bool>.Create(false, 5, Resource.String("STR_RESERVATION_MSG_ACCOUNT_RESERVED"), _response.Success);
      }

      _response = this.TerminalItCanReserve(request);
      if (!_response.Success || !_response.Result)
      {
        return ReservedTerminalResponseFactory<bool>.Create(false, 4, Resource.String("STR_RESERVATION_MSG_TERMINAL_CANNOT_BE_RESERVED"), _response.Success);
      }

      _response = this.ExistsPlaySession(request);

      if (!_response.Success || !_response.Result)
      {
        return ReservedTerminalResponseFactory<bool>.Create(false, 3, Resource.String("STR_RESERVATION_MSG_NOT_EXISTS_PLAY_SESSION"), _response.Success);
      }

      _response = this.CreateReservedTransaction(request);

      return _response;
    }

    public ReservedTerminalResponse<bool> Remove(ReservedTerminalReservedRequest request)
    {
      ReservedTerminalResponse<bool> _response;
      ReservedTerminalResponse<ReservedTerminalTransaction> _transaction;

      _response = this.IsReservedTerminal(request.TerminalID);

      if (!_response.Success || !_response.Result)
      {
        return ReservedTerminalResponseFactory<bool>.Create(false, 3, Resource.String("STR_RESERVATION_MSG_TERMINAL_NOT_RESERVED"), true);
      }

      _transaction = this.GetReservedTerminalTransaction(request);

      if (_transaction.Result == null)
      {
        return ReservedTerminalResponseFactory<bool>.Create(false, 2, Resource.String("STR_RESERVATION_MSG_ACCOUNT_TERMINAL_NOT_MATCH"), true);
      }

      _transaction.Result.CalculateMinutes();
      _response = this.UpdateReservedTransaction(_transaction.Result);

      if (!_response.Result)
      {
        return _response;
      }

      _response = this.UpdateTerminalStatus(request.TerminalID, false);
      if (!_response.Result)
      {
        return _response;
      }

      this.m_db_trx.Commit();
      _response = ReservedTerminalResponseFactory<bool>.Create(true, 1, Resource.String("STR_RESERVATION_MSG_REMOVE_TRANSACTION_OK"), true);

      return _response;
    }

    /// <summary>
    /// Search all the reserved terminals and unlocks them
    /// </summary>
    /// <returns></returns>
    public ReservedTerminalResponse<bool> RemoveAll()
    {
      ReservedTerminalResponse<bool> _response;

      StringBuilder _sb;
      StringBuilder _terminals_resume;
      List<ReservedTerminalReservedRequest> _reserved_terminals;
      ReservedTerminalResponse<ReservedTerminalTransaction> _transaction;
      StringBuilder _output_message;
      List<string> _ok_unlocked_terminals;
      List<string> _ko_unlocked_terminals;
      _ok_unlocked_terminals = new List<string>();
      _ko_unlocked_terminals = new List<string>();
      _terminals_resume = new StringBuilder();
      _output_message = new StringBuilder();

      // Getting all the reserved terminals
      _reserved_terminals = new List<ReservedTerminalReservedRequest>();

      _sb = new StringBuilder();
      _sb.Append("SELECT            RTT.RTT_TERMINAL_ID, RTT.RTT_ACCOUNT_ID, T.TE_NAME        ");
      _sb.Append("FROM              RESERVED_TERMINAL_TRANSACTION RTT                         ");
      _sb.Append("INNER JOIN        TERMINALS T ON RTT.RTT_TERMINAL_ID = T.TE_TERMINAL_ID     ");
      _sb.Append("WHERE             RTT.RTT_STATUS = 1                                        ");
      _sb.Append("AND               RTT.RTT_END_RESERVED IS NULL                              ");

      _reserved_terminals = this.ReservedTerminalsList(_sb.ToString());

      if (_reserved_terminals.Count > 0)
      {
        foreach (ReservedTerminalReservedRequest _reserved_terminal in _reserved_terminals)
        {
          m_db_trx = new DB_TRX();
          _response = this.IsReservedTerminal(_reserved_terminal.TerminalID);

          if (!_response.Success || !_response.Result)
          {
            _ko_unlocked_terminals.Add(_reserved_terminal.TerminalName);
          }

          _transaction = this.GetReservedTerminalTransaction(_reserved_terminal);

          if (_transaction.Result == null)
          {
            _ko_unlocked_terminals.Add(_reserved_terminal.TerminalName);
          }

          _transaction.Result.CalculateMinutes();
          _response = this.UpdateReservedTransaction(_transaction.Result);

          if (!_response.Result)
          {
            _ko_unlocked_terminals.Add(_reserved_terminal.TerminalName);
          }

          _response = this.UpdateTerminalStatus(_reserved_terminal.TerminalID, false);
          if (!_response.Result)
          {
            _ko_unlocked_terminals.Add(_reserved_terminal.TerminalName);
          }

          this.m_db_trx.Commit();

          _ok_unlocked_terminals.Add(_reserved_terminal.TerminalName);

        }

        if (_ok_unlocked_terminals.Count > 0)
        {
          foreach (string _ok_terminal in _ok_unlocked_terminals)
          {
            _terminals_resume.Append(_ok_terminal);


            if (!_ok_terminal.Equals(_ok_unlocked_terminals[_ok_unlocked_terminals.Count - 1]))
            {
              _terminals_resume.Append(",");
            }
          }
        }

        if (_ko_unlocked_terminals.Count > 0)
        {
          _terminals_resume.Append("|");
          foreach (string _ko_terminal in _ko_unlocked_terminals)
          {
            _terminals_resume.Append(_ko_terminal);
            if (!_ko_terminal.Equals(_ko_unlocked_terminals[_ko_unlocked_terminals.Count - 1]))
            {
              _terminals_resume.Append(",");
            }
          }
          _output_message.AppendLine(Resource.String("STR_RESERVATION_MSG_SOME_TERMINALS_RELEASED"));
          _output_message.AppendLine("OK: " + _terminals_resume.ToString().Split('|')[0].ToString());
          _output_message.AppendLine("KO: " + _terminals_resume.ToString().Split('|')[1].ToString());
        }
        else
        {
          _output_message.Append(Resource.String("STR_RESERVATION_MSG_ALL_TERMINALS_RELEASED"));
        }
      }
      else
      {
        _output_message.Append(Resource.String("STR_RESERVATION_MSG_ALL_TERMINALS_NOT_SELECTED"));
      }

      _response = ReservedTerminalResponseFactory<bool>.Create(true, 1, _output_message.ToString(), true, _terminals_resume.ToString());

      return _response;
    }

    public ReservedTerminalTransaction GetTransaction(ReservedTerminalReservedRequest request)
    {
      ReservedTerminalTransaction _transaction;
      int _maxMinutes;
      _transaction = new ReservedTerminalTransaction();

      _maxMinutes = this.GetMinutesReserved(request.AccountID);
      _transaction = ReservedTerminalTransactionFactory.Create(request, _maxMinutes);
      return _transaction;
    }
    #endregion

    #region Private Methods

    private bool ContainsTerminal(int terminalID, ProviderList providerList)
    {
      bool _contains;

      _contains = false;

      if (providerList == null)
      {
        return _contains;
      }

      if (providerList.Terminals == null)
      {
        return _contains;
      }

      foreach (var item in providerList.Terminals)
      {
        if (item == terminalID)
        {
          _contains = true;
          break;
        }
      }

      return _contains;
    }

    private int GetMinutesReserved(long accountID)
    {
      StringBuilder _sb;
      ReservedTerminalResponse<int> _response;

      _sb = new StringBuilder();
      _sb.Append("SELECT RTC_MINUTES                         ");
      _sb.Append("FROM ACCOUNTS                              ");
      _sb.Append("INNER JOIN RESERVED_TERMINAL_CONFIGURATION ");
      _sb.Append("ON AC_HOLDER_LEVEL = RTC_HOLDER_LEVEL      ");
      _sb.Append("WHERE AC_ACCOUNT_ID = @pResultIn           ");

      _response = this.Result<int, long>(accountID, _sb.ToString());

      return _response.Result;
    }

    private ReservedTerminalResponse<bool> CreateReservedTransaction(ReservedTerminalReservedRequest request)
    {
      int _maxMinutes;
      ReservedTerminalResponse<bool> _response;
      ReservedTerminalTransaction _transaction;

      _maxMinutes = this.GetMinutesReserved(request.AccountID);

      if (_maxMinutes <= 0)
      {
        return ReservedTerminalResponseFactory<bool>.Create(false, 2, Resource.String("STR_RESERVATION_MSG_ZERO_MINUTES"), true);
      }

      _transaction = ReservedTerminalTransactionFactory.Create(request, _maxMinutes);

      _response = this.CreateReservedTransaction(_transaction);
      if (!_response.Result)
      {
        return _response;
      }

      _response = this.UpdateTerminalStatus(request.TerminalID, true);
      if (!_response.Result)
      {
        return _response;
      }
      this.m_db_trx.Commit();

      _response = ReservedTerminalResponseFactory<bool>.Create(true, 1, Resource.String("STR_RESERVATION_MSG_TRANSACTION_OK"), true);

      return _response;
    }


    private ReservedTerminalResponse<bool> UpdateTerminalStatus(int terminalID, bool isReserved)
    {
      StringBuilder _sb;
      ReservedTerminalResponse<bool> _response;

      _sb = new StringBuilder();

      _sb.Append("UPDATE TERMINAL_STATUS              ");
      _sb.Append("SET                                 ");
      _sb.Append("  TS_IS_RESERVED = @pIsReserved     ");
      _sb.Append("WHERE TS_TERMINAL_ID = @pTerminalID ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.Text;
          _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = terminalID;
          _cmd.Parameters.Add("@pIsReserved", SqlDbType.Bit).Value = isReserved;
          _cmd.ExecuteNonQuery();
          _response = ReservedTerminalResponseFactory<bool>.Create(true, 1, Resource.String("STR_RESERVATION_MSG_UPDATE_TERMINALSTATUS_OK"), true);
        }
      }
      catch (Exception ex)
      {
        _response = ReservedTerminalResponseFactory<bool>.Create(false, 1, ex.Message, false);
      }

      return _response;
    }

    private ReservedTerminalResponse<bool> CreateReservedTransaction(ReservedTerminalTransaction transaction)
    {
      StringBuilder _sb;
      ReservedTerminalResponse<bool> _response;

      _sb = new StringBuilder();
      _sb.Append("INSERT INTO RESERVED_TERMINAL_TRANSACTION( ");
      _sb.Append("      RTT_ACCOUNT_ID                         ");
      _sb.Append("     ,RTT_TERMINAL_ID                        ");
      _sb.Append("     ,RTT_START_RESERVED                     ");
      _sb.Append("     ,RTT_START_UTC                          ");
      _sb.Append("     ,RTT_MAX_MINUTES                        ");
      _sb.Append("     ,RTT_STATUS)                            ");
      _sb.Append("  VALUES(                                    ");
      _sb.Append("      @pAccountID                            ");
      _sb.Append("     ,@pTerminalID                           ");
      _sb.Append("     ,@pStartReserved                        ");
      _sb.Append("     ,@pStartUtc                             ");
      _sb.Append("     ,@pMaxMinutes                           ");
      _sb.Append("     ,@pStatus)                              ");


      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.Text;
          _cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = transaction.Keys.AccountID;
          _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = transaction.Keys.TerminalID;
          _cmd.Parameters.Add("@pStartReserved", SqlDbType.DateTime).Value = transaction.StartReserved;
          _cmd.Parameters.Add("@pStartUtc", SqlDbType.DateTime).Value = transaction.StartUtc;
          _cmd.Parameters.Add("@pMaxMinutes", SqlDbType.Int).Value = transaction.MaxMinutes;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)transaction.Status;
          _cmd.ExecuteNonQuery();
          _response = ReservedTerminalResponseFactory<bool>.Create(true, 1, Resource.String("STR_RESERVATION_MSG_INSERT_IN_RESERVED_TERMINAL_CONFIG_OK"), true);
        }
      }
      catch (Exception ex)
      {
        _response = ReservedTerminalResponseFactory<bool>.Create(false, -1, ex.Message, false);
      }

      return _response;
    }

    private ReservedTerminalResponse<bool> UpdateReservedTransaction(ReservedTerminalTransaction transaction)
    {
      StringBuilder _sb;
      ReservedTerminalResponse<bool> _response;


      _sb = new StringBuilder();
      _sb.Append("UPDATE RESERVED_TERMINAL_TRANSACTION         ");
      _sb.Append(" SET                                         ");
      _sb.Append("      RTT_END_RESERVED  = @pEndReserved      ");
      _sb.Append("     ,RTT_END_UTC       = @pEndUtc           ");
      _sb.Append("     ,RTT_TOTAL_MINUTES = @pTotalMinutes     ");
      _sb.Append("     ,RTT_STATUS        = @pStatus           ");
      _sb.Append("WHERE RTT_TRANSACTION_ID = @pID              ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.Text;
          _cmd.Parameters.Add("@pID", SqlDbType.BigInt).Value = transaction.ID;
          _cmd.Parameters.Add("@pEndReserved", SqlDbType.DateTime).Value = transaction.EndReserved;
          _cmd.Parameters.Add("@pEndUtc", SqlDbType.DateTime).Value = transaction.EndUtc;
          _cmd.Parameters.Add("@pTotalMinutes", SqlDbType.Int).Value = transaction.TotalMinutes;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)transaction.Status;
          _cmd.ExecuteNonQuery();
          _response = ReservedTerminalResponseFactory<bool>.Create(true, 1, Resource.String("STR_RESERVATION_MSG_UPDATE_IN_RESERVED_TERMINAL_TRANSACTION_OK"), true);
        }
      }
      catch (Exception ex)
      {
        _response = ReservedTerminalResponseFactory<bool>.Create(false, 1, ex.Message, false);
      }

      return _response;
    }

    private ReservedTerminalResponse<ReservedTerminalTransaction> GetReservedTerminalTransaction(ReservedTerminalReservedRequest request)
    {
      ReservedTerminalResponse<ReservedTerminalTransaction> _response;
      StringBuilder _sb;
      ReservedTerminalTransaction _reservedTerminalTransaction;

      _reservedTerminalTransaction = null;
      _sb = new StringBuilder();
      _sb.Append("SELECT                                  ");
      _sb.Append("    RTT_TRANSACTION_ID                  ");
      _sb.Append("   ,RTT_ACCOUNT_ID                      ");
      _sb.Append("   ,RTT_TERMINAL_ID                     ");
      _sb.Append("   ,RTT_START_RESERVED                  ");
      _sb.Append("   ,RTT_END_RESERVED                    ");
      _sb.Append("   ,RTT_START_UTC                       ");
      _sb.Append("   ,RTT_END_UTC                         ");
      _sb.Append("   ,RTT_MAX_MINUTES                     ");
      _sb.Append("   ,RTT_TOTAL_MINUTES                   ");
      _sb.Append("   ,RTT_STATUS                          ");
      _sb.Append("FROM RESERVED_TERMINAL_TRANSACTION      ");
      _sb.Append("WHERE RTT_TRANSACTION_ID =              ");
      _sb.Append("    (SELECT MAX(RTT_TRANSACTION_ID)     ");
      _sb.Append("     FROM RESERVED_TERMINAL_TRANSACTION ");
      _sb.Append("     WHERE RTT_ACCOUNT_ID = @pAccountID ");
      _sb.Append("     AND RTT_TERMINAL_ID = @pTerminalID ");
      _sb.Append("     AND RTT_STATUS = @pStatus)         ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.Text;
          _cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = request.AccountID;
          _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = request.TerminalID;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)ReservedStatusType.Reserved;
          using (SqlDataReader _read = _cmd.ExecuteReader())
          {
            if (_read.Read())
            {
              _reservedTerminalTransaction = ReservedTerminalTransactionFactory.Create(_read);
              _response = ReservedTerminalResponseFactory<ReservedTerminalTransaction>.Create(_reservedTerminalTransaction, 1, Resource.String("STR_RESERVATION_MSG_MATCH_OK"), true);
            }
            else
            {
              _response = ReservedTerminalResponseFactory<ReservedTerminalTransaction>.Create(_reservedTerminalTransaction, 2, Resource.String("STR_RESERVATION_MSG_MATCH_KO"), true);
            }
          }
        }
      }
      catch (Exception ex)
      {
        _response = ReservedTerminalResponseFactory<ReservedTerminalTransaction>.Create(_reservedTerminalTransaction, -1, ex.Message, true);
      }

      return _response;
    }
    
    private ReservedTerminalResponse<T> Result<T, U>(U resultIn, string sqlQuery)
    {
      T _result;
      object _obj;
      ReservedTerminalResponse<T> _response;

      _result = default(T);
      try
      {
        using (SqlCommand _cmd = new SqlCommand(sqlQuery, m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.Text;
          _cmd.Parameters.Add("@pResultIn", SqlDbType.BigInt).Value = resultIn;
          _obj = _cmd.ExecuteScalar();
          if (_obj == null)
          {
            _response = ReservedTerminalResponseFactory<T>.Create(_result, 1, Resource.String("STR_RESERVATION_MSG_NULL_AT_EXECUTESCALAR"), true);
          }
          else
          {
            _result = (T)_obj;
            _response = ReservedTerminalResponseFactory<T>.Create(_result, 1, Resource.String("STR_RESERVATION_MSG_EXECUTESCALAR_OK"), true);
          }
        }
      }
      catch (Exception ex)
      {
        _response = ReservedTerminalResponseFactory<T>.Create(_result, -1, ex.Message, false);
      }
      return _response;
    }

    /// <summary>
    /// Returns all the reserved terminals in a list format
    /// </summary>
    /// <param name="sqlQuery"></param>
    /// <returns></returns>
    private List<ReservedTerminalReservedRequest> ReservedTerminalsList(string sqlQuery)
    {
      List<ReservedTerminalReservedRequest> _list_reserved_terminals;

      _list_reserved_terminals = new List<ReservedTerminalReservedRequest>();
      try
      {
        using (SqlCommand _cmd = new SqlCommand(sqlQuery, m_db_trx.SqlTransaction.Connection, m_db_trx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.Text;
          SqlDataReader _reader = _cmd.ExecuteReader();
          while (_reader.Read())
          {
            ReservedTerminalReservedRequest _reserved_terminal = new ReservedTerminalReservedRequest();
            _reserved_terminal.TerminalID = _reader.GetInt32(0);
            _reserved_terminal.AccountID = _reader.GetInt64(1);
            _reserved_terminal.TerminalName = _reader.GetString(2);
            _list_reserved_terminals.Add(_reserved_terminal);
          }
          _reader.Close();
        }
      }
      catch (Exception)
      {
        return null;
      }
      return _list_reserved_terminals;
    }
    #endregion

    #region Dispose
    public void Dispose()
    {
      if (m_db_trx != null)
      {
        m_db_trx.Dispose();
        m_db_trx = null;
      }
    }
    #endregion
  }
}
