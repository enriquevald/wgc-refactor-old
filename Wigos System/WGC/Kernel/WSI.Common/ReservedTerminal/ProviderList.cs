﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reserved Terminal
// 
//   DESCRIPTION: ProviderList
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 09-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-AUG-2016 RGR    Initial Version
// 12-AUG-2016 RGR    Remove Issues
//------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Xml.Serialization;
using WSI.Common.Utilities;

namespace WSI.Common.ReservedTerminal
{
  public class ProviderList
  {
    public int[] Terminals { get; set; }
  }
}
