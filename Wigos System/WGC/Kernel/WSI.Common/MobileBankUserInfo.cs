﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Card.cs
// 
//   DESCRIPTION: Class to retrieve data form Mobile bank user info
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 13-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-NOV-2017 FGB    First release.
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public class MobileBankUserInfo
  {
    #region "Constants"
    private const Int16 FIELD_USERID = 0;
    private const Int16 FIELD_USERNAME = 1;
    private const Int16 FIELD_USER_HOLDERNAME = 2;
    private const Int16 FIELD_MOBILE_BANK_ID = 3;
    private const Int16 FIELD_MOBILE_BANK_HOLDERNAME = 4;
    private const Int16 FIELD_MOBILE_TRACKDATA = 5;
    private const Int16 FIELD_MOBILE_BLOCKED = 6;
    private const Int16 FIELD_MOBILE_PIN = 7;

    private const Int64 MOBILE_BANK_ID_EMPTY = 0;
    private const Int32 USER_ID_EMPTY = 0;
    #endregion

    #region "Public Methods"
    /// <summary>
    /// Get mobile bank and user info
    /// <param name="MobileBankUserId"></param>
    /// <param name="MBUserInfoData"></param>
    /// <returns></returns>
    public Boolean GetMobileBankUserInfo(Int32 MobileBankUserId, out MobileBankUserInfoData MBUserInfoData)
    {
      return this.DB_GetMobileBankUserInfo(MobileBankUserId, out MBUserInfoData);
    }

    /// <summary>
    /// Create mobile bank for user
    /// </summary>
    /// <param name="MBUserInfoData"></param>
    /// <returns></returns>
    public Boolean CreateMobileBankForUser(ref MobileBankUserInfoData MBUserInfoData)
    {
      return DB_CreateMobileBankForUser(ref MBUserInfoData);
    }

    /// <summary>
    /// Is user id empty
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    public static Boolean IsUserIdEmpty(Int32 UserId)
    {
      return (UserId == USER_ID_EMPTY);
    }

    /// <summary>
    /// Get empty user id
    /// </summary>
    /// <returns></returns>
    public static Int32 GetEmptyUserId()
    {
      return USER_ID_EMPTY;
    }

    /// <summary>
    /// Is mobile bank id empty
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    public static Boolean IsMobileBankIdEmpty(Int64 MobileBankId)
    {
      return (MobileBankId == MOBILE_BANK_ID_EMPTY);
    }

    /// <summary>
    /// Get empty mobile bank id
    /// </summary>
    /// <returns></returns>
    public static Int64 GetEmptyMobileBankId()
    {
      return MOBILE_BANK_ID_EMPTY;
    }
    #endregion

    #region "Private Methods"
    /// <summary>
    /// Get mobile bank and user info
    /// </summary>
    /// <param name="MobileBankUserId"></param>
    /// <param name="MBUserInfoData"></param>
    /// <returns></returns>
    private Boolean DB_GetMobileBankUserInfo(Int64 MobileBankUserId, out MobileBankUserInfoData MBUserInfoData)
    {
      MBUserInfoData = new MobileBankUserInfoData();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(GetMobileBankAndUserInfoDataQuery(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = MobileBankUserId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return false;
              }

              MBUserInfoData.UserId = (_reader[FIELD_USERID] == DBNull.Value) ? USER_ID_EMPTY : Convert.ToInt32(_reader[FIELD_USERID]);
              MBUserInfoData.UserName = (_reader[FIELD_USERNAME] == DBNull.Value) ? String.Empty : Convert.ToString(_reader[FIELD_USERNAME]).Trim();
              MBUserInfoData.UserHolderName = (_reader[FIELD_USER_HOLDERNAME] == DBNull.Value) ? String.Empty : Convert.ToString(_reader[FIELD_USER_HOLDERNAME]).Trim();
              MBUserInfoData.MobileBankId = (_reader[FIELD_MOBILE_BANK_ID] == DBNull.Value) ? MOBILE_BANK_ID_EMPTY : Convert.ToInt64(_reader[FIELD_MOBILE_BANK_ID]);
              MBUserInfoData.MobileBankHolderName = (_reader[FIELD_MOBILE_BANK_HOLDERNAME] == DBNull.Value) ? String.Empty : Convert.ToString(_reader[FIELD_MOBILE_BANK_HOLDERNAME]).Trim();
              MBUserInfoData.MobileBlocked = (_reader[FIELD_MOBILE_BLOCKED] == DBNull.Value) ? false : (Boolean)_reader[FIELD_MOBILE_BLOCKED];
              MBUserInfoData.MobilePin = (_reader[FIELD_MOBILE_PIN] == DBNull.Value) ? String.Empty : Convert.ToString(_reader[FIELD_MOBILE_PIN]).Trim();
              MBUserInfoData.MobileInternalTrackData = (_reader[FIELD_MOBILE_TRACKDATA] == DBNull.Value) ? String.Empty : Convert.ToString(_reader[FIELD_MOBILE_TRACKDATA]).Trim();
            }

            if (!GetExternalTrackDataAndVirtualCard(MBUserInfoData, _db_trx.SqlTransaction))
            {
              MBUserInfoData.MobileExternalTrackData = String.Empty;
              MBUserInfoData.IsVirtualCard = false;
            }

            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Error(ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Get external track data & virtual card
    /// </summary>
    /// <param name="MBUserInfoData"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean GetExternalTrackDataAndVirtualCard(MobileBankUserInfoData MBUserInfoData, SqlTransaction Trx)
    {
      Boolean _track_data_is_ok;

      MBUserInfoData.MobileExternalTrackData = String.Empty;

      if (!MobileBankConfig.IsMobileBankLinkedToADevice)
      {
        String _external_trackdata;
        Int32 _card_type = (Int32)MobileBankConfig.MOBILE_BANK_NEW_ACCOUNT_TYPE;

        _track_data_is_ok = CardNumber.TrackDataToExternal(out _external_trackdata, MBUserInfoData.MobileInternalTrackData, _card_type);
        MBUserInfoData.MobileExternalTrackData = _external_trackdata;
        MBUserInfoData.IsVirtualCard = false;
      }
      else
      {
        _track_data_is_ok = true;

        //Get external trackdata from Cards
        MBUserInfoData.MobileExternalTrackData = MobileBank.DB_GetMBExternalTrackdata(MBUserInfoData.MobileBankId, Trx);

        //If trackdata empty or not exists then is a virtual card
        MBUserInfoData.IsVirtualCard = String.IsNullOrEmpty(MBUserInfoData.MobileExternalTrackData);
      }

      return _track_data_is_ok;
    }

    /// <summary>
    /// Create mobile bank for user
    /// </summary>
    /// <param name="MBUserInfoData"></param>
    /// <returns></returns>
    private Boolean DB_CreateMobileBankForUser(ref MobileBankUserInfoData MBUserInfoData)
    {
      if ((MBUserInfoData == null) || IsUserIdEmpty(MBUserInfoData.UserId))
      {
        return false;
      }

      if (!IsMobileBankIdEmpty(MBUserInfoData.MobileBankId))
      {
        return true;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          //Assign mobile bank properties
          MBCardData _mb_card_data;
          _mb_card_data = new MBCardData();
          _mb_card_data.UserId = MBUserInfoData.UserId;
          _mb_card_data.HolderName = MBUserInfoData.UserHolderName;

          if (!MobileBank.DB_CreateEmptyMobileBank(_mb_card_data, _db_trx.SqlTransaction))
          {
            return false;
          }

          MBUserInfoData.MobileBankId = _mb_card_data.CardId;
          MBUserInfoData.MobileExternalTrackData = _mb_card_data.TrackData;
          MBUserInfoData.IsVirtualCard = _mb_card_data.IsVirtualCard;
          MBUserInfoData.MobilePin = _mb_card_data.Pin;

          _db_trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Query to Get mobile bank & user info from DB
    /// </summary>
    /// <returns></returns>
    private String GetMobileBankAndUserInfoDataQuery()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("          SELECT   GU_USER_ID                 ");
      _sb.AppendLine("                 , GU_USERNAME                ");
      _sb.AppendLine("                 , GU_FULL_NAME               ");
      _sb.AppendLine("                 , MB_ACCOUNT_ID              ");
      _sb.AppendLine("                 , MB_HOLDER_NAME             ");
      _sb.AppendLine("                 , MB_TRACK_DATA              ");
      _sb.AppendLine("                 , MB_BLOCKED                 ");
      _sb.AppendLine("                 , MB_PIN                     ");
      _sb.AppendLine("            FROM   GUI_USERS                  ");
      _sb.AppendLine(" LEFT OUTER JOIN   MOBILE_BANKS               ");
      _sb.AppendLine("              ON   GU_USER_ID  =  MB_USER_ID  ");
      _sb.AppendLine("           WHERE   GU_USER_ID  =  @pUserId    ");

      return _sb.ToString();
    }
    #endregion
  }
}
