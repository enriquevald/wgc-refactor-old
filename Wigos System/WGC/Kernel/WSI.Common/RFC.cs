//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
//  
//   MODULE NAME : rfc.cs
// 
//   DESCRIPTION : RFC validation
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-OCT-2012 AJQ    Initial version
// 06-SEP-2013 ANG    Fix Bug WIG-193
// 19-SEP-2013 ANG    Fix Bug WIG-209

using System;
using System.Collections.Generic;
using System.Text;


namespace WSI.Common
{
  using System;
  using System.Collections;

  public static class RFC
  {
    private static ArrayList EliminarArticulos(String Texto)
    {
      String[] _articles;
      ArrayList _fin_words;
      Boolean _found_article;
      String[] _ini_words;

      _articles = new String[] { "DE", "LA", "LAS", "MC", "VON", "DEL", "LOS", "Y", "MAC", "VAN", "MI" };
      _fin_words = new ArrayList();

      _ini_words = Texto.Split(new String[] { " " }, StringSplitOptions.RemoveEmptyEntries);

      if (_ini_words.Length == 1)
      {
        _fin_words.Add(_ini_words[0]);
      }
      else
      {
        foreach (String _word in _ini_words)
        {
          _found_article = false;
          foreach (String _article in _articles)
          {
            if (_article == _word)
            {
              _found_article = true;

              break;
            }
          }

          if (!_found_article)
          {
            _fin_words.Add(_word);
          }
        }
      }

      return _fin_words;
    }

    private static String ExtraeLetrasApellidos(ArrayList apellidos)
    {
      List<String> _vocals;
      String _apellido;
      String _letras;

      _vocals = new List<String>();
      _vocals.Add("A");
      _vocals.Add("E");
      _vocals.Add("I");
      _vocals.Add("O");
      _vocals.Add("U");

      if (apellidos.Count <= 0)
      {
        return "";
      }

      _apellido = (String)apellidos[0];

      if (_apellido.Length <= 0)
      {
        return "";
      }

      if (_apellido.StartsWith("CH"))
      {
        _apellido = "C" + _apellido.Substring(2);
      }
      else if (_apellido.StartsWith("LL"))
      {
        _apellido = "L" + _apellido.Substring(2);
      }

      _letras = _apellido.Substring(0, 1);

      _apellido = _apellido.Substring(1);
      for (int _i = 0; _i < _apellido.Length; _i++)
      {
        String _letra;
        _letra = _apellido.Substring(_i, 1);
        if (_vocals.Contains(_letra)) // Is Vocal?
        {
          _letras = _letras + _letra;

          break;
        }
      }

      return _letras;
    }

    private static String ExtraeNombre(String nombr, String rfc)
    {
      ArrayList list = EliminarArticulos(nombr + " ");
      // If name has middle name and name starts by 'MARIA' or 'JOSE',Will remove first element of compound name
      if (list.Count > 1 && 
         (  list[0].Equals("MARIA") || list[0].Equals("MA")
         || list[0].Equals("JOSE") || list[0].Equals("J")))
      {
        list.RemoveAt(0);
      }
      char[] chArray = list[0].ToString().ToCharArray();
      if ( chArray.Length > 2 && ((chArray[0].ToString() + chArray[1].ToString()).Equals("CH") || (chArray[0].ToString() + chArray[1].ToString()).Equals("LL")))
      {
        if (rfc.Length > 2)  // Has second surname
        {
          return chArray[0].ToString();
        }
        return (chArray[0].ToString() + chArray[2].ToString());
      }
      if (rfc.Length > 2)     // Has second surname
      {
        return chArray[0].ToString();
      }
        return (chArray[0].ToString() + ((chArray.Length > 1)? chArray[1].ToString():"X") );
    }

    public static String ObtieneDigitoVerificador(String rfc12pocisiones)
    {
      rfc12pocisiones = rfc12pocisiones.Replace("-", "");
      if (rfc12pocisiones.Length < 12)
      {
        rfc12pocisiones = " " + rfc12pocisiones;
      }
      String[,] strArray = new String[,] { 
                { "0", "00" }, { "1", "01" }, { "2", "02" }, { "3", "03" }, { "4", "04" }, { "5", "05" }, { "6", "06" }, { "7", "07" }, { "8", "08" }, { "9", "09" }, { "A", "10" }, { "B", "11" }, { "C", "12" }, { "D", "13" }, { "E", "14" }, { "F", "15" }, 
                { "G", "16" }, { "H", "17" }, { "I", "18" }, { "J", "19" }, { "K", "20" }, { "L", "21" }, { "M", "22" }, { "N", "23" }, { "&", "24" }, { "O", "25" }, { "P", "26" }, { "Q", "27" }, { "R", "28" }, { "S", "29" }, { "T", "30" }, { "U", "31" }, 
                { "V", "32" }, { "W", "33" }, { "X", "34" }, { "Y", "35" }, { "Z", "36" }, { " ", "37" }, { "�", "38" }
             };
      String str = null;
      int index = 0;
      int a = 0;
      while (index != rfc12pocisiones.ToCharArray().Length)
      {
        bool _found = false;
        for (int num3 = 0; num3 < strArray.Length; num3++)
        {
          if (strArray[num3, 0].Equals(rfc12pocisiones.ToCharArray()[index].ToString()))
          {
            str = str + strArray[num3, 1];
            _found = true;
            break;
          }
        }
        if (!_found)
        {
          str = str + "00";
        }
        index++;
      }
      index = 0;
      for (int i = 0; index < (str.Length - 1); i++)
      {
        a += int.Parse(str.Substring(index, 2)) * (13 - i);
        index += 2;
      }
      Math.DivRem(a, 11, out a);
      str = "";
      if (a > 0)
      {
        a = 11 - a;
        str = a.ToString();
      }
      switch (a)
      {
        case 0:
          return "0";

        case 10:
          return "A";
      }
      return str;
    }

    public static String ObtieneHomonimia(String ApellidoPaterno, String ApellidoMaterno, String Nombre)
    {
      String[,] strArray = new String[,] { 
                { " ", "00" }, { "0", "00" }, { "1", "01" }, { "2", "02" }, { "3", "03" }, { "4", "04" }, { "5", "05" }, { "6", "06" }, { "7", "07" }, { "8", "08" }, { "9", "09" }, { "&", "10" }, { "A", "11" }, { "B", "12" }, { "C", "13" }, { "D", "14" }, 
                { "E", "15" }, { "F", "16" }, { "G", "17" }, { "H", "18" }, { "I", "19" }, { "J", "21" }, { "K", "22" }, { "L", "23" }, { "M", "24" }, { "N", "25" }, { "O", "26" }, { "P", "27" }, { "Q", "28" }, { "R", "29" }, { "S", "32" }, { "T", "33" }, 
                { "U", "34" }, { "V", "35" }, { "W", "36" }, { "X", "37" }, { "Y", "38" }, { "Z", "39" }, { "�", "40" }
             };
      String[,] strArray2 = new String[,] { 
                { "0", "1" }, { "1", "2" }, { "2", "3" }, { "3", "4" }, { "4", "5" }, { "5", "6" }, { "6", "7" }, { "7", "8" }, { "8", "9" }, { "9", "A" }, { "10", "B" }, { "11", "C" }, { "12", "D" }, { "13", "E" }, { "14", "F" }, { "15", "G" }, 
                { "16", "H" }, { "17", "I" }, { "18", "J" }, { "19", "K" }, { "20", "L" }, { "21", "M" }, { "22", "N" }, { "0", "O" }, { "23", "P" }, { "24", "Q" }, { "25", "R" }, { "26", "S" }, { "27", "T" }, { "28", "U" }, { "29", "V" }, { "30", "W" }, 
                { "31", "X" }, { "32", "Y" }, { "33", "Z" }
             };
      String str = "";
      if (ApellidoMaterno.Equals(""))
      {
        str = ApellidoPaterno + " " + Nombre;
      }
      else
      {
        str = ApellidoPaterno + " " + ApellidoMaterno + " " + Nombre;
      }

      String str2 = null;
      char[] chArray = str.ToCharArray();
      int index = 0;
      double num2 = 0.0;
      String str3 = "0";
      bool flag = false;
      while (index != chArray.Length)
      {
        int num3 = 0;
        flag = false;
        while (!flag && num3 <= strArray.GetUpperBound(0))
        {
          if (strArray[num3, 0].Equals(chArray[index].ToString()))
          {
            str3 = str3 + strArray[num3, 1];
            flag = true;
          }
          num3++;
        }
        index++;
      }
      for (index = 0; index < (str3.Length - 1); index++)
      {
        String str4 = str3.Substring(index, 2) + "X" + str3.Substring(index + 1, 1);
        num2 += double.Parse(str3.Substring(index, 2)) * double.Parse(str3.Substring(index + 1, 1));
      }
      if (num2.ToString().Length > 3)
      {
        num2 = int.Parse(num2.ToString().Substring(1, 3));
      }
      int result = 0;
      Math.DivRem((int)num2, 0x22, out result);
      int num5 = ((int)num2) / 0x22;
      flag = false;
      for (index = 0; !flag; index++)
      {
        if (strArray2[index, 0].Equals(num5.ToString()))
        {
          str2 = str2 + strArray2[index, 1];
          flag = true;
        }
      }
      flag = false;
      for (index = 0; !flag; index++)
      {
        if (strArray2[index, 0].Equals(result.ToString()))
        {
          str2 = str2 + strArray2[index, 1];
          flag = true;
        }
      }
      return str2;
    }

    private static String QuitaPalabrasMalas(String RFC)
    {
      String[] _dirty_words = new String[] { 
                "BUEI", "BUEY", "CACA", "CACO", "CAGA", "CAGO", "CAKA", "CAKO", "COGE", "COJA", "COJE", "COJI", "COJO", "CULO", "FETO", "GUEY", 
                "JOTO", "KACA", "KACO", "KAGA", "KAGO", "KOGE", "KOJO", "KAKA", "KULO", "MAME", "MAMO", "MEAR", 
                "MEAS", "MEON", "MION", "MOCO", "MULA", "PEDA", "PEDO", "PENE", "PUTA", "PUTO", "QULO", "RATA", "RUIN"
             };

      foreach (String _word in _dirty_words)
      {
        if (_word == RFC)
        {
          return RFC.Substring(0, 3) + "X";
        }
      }

      return RFC;
    }

    private static String RFC10(String ApellidoPaterno, String ApellidoMaterno, String Nombre, String FechaNacimiento)
    {
      String _partial_rfc;
      String _aux_materno;

      FechaNacimiento = FechaNacimiento.Replace("/", "");

      _partial_rfc = ExtraeLetrasApellidos(EliminarArticulos(ApellidoPaterno + " "));
      _aux_materno = ExtraeLetrasApellidos(EliminarArticulos(ApellidoMaterno + " "));
      if (!String.IsNullOrEmpty(_aux_materno))
      {
        _partial_rfc = _partial_rfc + _aux_materno.Substring(0, 1);
      }
      _partial_rfc = _partial_rfc + ExtraeNombre(Nombre + " ", _partial_rfc);
      return (QuitaPalabrasMalas(_partial_rfc) + FechaNacimiento.Trim());
    }

    public static String RemoveTilde(String Texto)
    {
      String _text;

      _text = Texto.ToUpper();

      _text = _text.Replace("�", "A");
      _text = _text.Replace("�", "E");
      _text = _text.Replace("�", "I");
      _text = _text.Replace("�", "O");
      _text = _text.Replace("�", "U");

      _text = _text.Replace("�", "I");
      _text = _text.Replace("�", "U");

      _text = _text.Replace("�", "'");
      _text = _text.Replace("`", "'");

      _text = _text.Replace("'", "APOSTROFE");
      _text = _text.Replace(".", "PUNTO");

      // AJQ --- :-)
      _text = _text.Replace("�", "A");
      _text = _text.Replace("�", "E");
      _text = _text.Replace("�", "I");
      _text = _text.Replace("�", "O");
      _text = _text.Replace("�", "U");

      _text = _text.Replace("�", "A");
      _text = _text.Replace("�", "E");
      _text = _text.Replace("�", "O");

      _text = _text.Replace("�", "A");
      _text = _text.Replace("�", "E");
      _text = _text.Replace("�", "I");
      _text = _text.Replace("�", "O");
      _text = _text.Replace("�", "U");

      _text = _text.Replace("�", "C");


      _text = _text.Replace(",", "");     // ANG  Remove comma (',')


      return _text;
    }

    public static String RFC13(String ApellidoPaterno, String ApellidoMaterno, String Nombre, DateTime FechaNacimiento)
    {
      ApellidoPaterno = ApellidoPaterno.Trim();
      ApellidoMaterno = ApellidoMaterno.Trim();
      Nombre = Nombre.Trim();

      if (String.IsNullOrEmpty(ApellidoPaterno))
      {
        ApellidoPaterno = ApellidoMaterno;
        ApellidoMaterno = "";
      }

      ApellidoPaterno = RemoveTilde(ApellidoPaterno);
      ApellidoMaterno = RemoveTilde(ApellidoMaterno);
      Nombre = RemoveTilde(Nombre);


      String str = RFC10(ApellidoPaterno, ApellidoMaterno, Nombre, FechaNacimiento.ToString("yyMMdd"));
      String str2 = ObtieneHomonimia(ApellidoPaterno, ApellidoMaterno, Nombre);
      return (str + str2 + ObtieneDigitoVerificador(str + str2));
    }
  }
}
