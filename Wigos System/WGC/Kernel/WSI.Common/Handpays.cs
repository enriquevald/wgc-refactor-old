//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Handpays.cs
// 
//   DESCRIPTION: Handpays class
// 
//        AUTHOR: Xavi Cots
// 
// CREATION DATE: 05-JUL-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUL-2013 XCD    First version.
// 15-JUL-2013 LEM    Moved HandpayRegisterOrCancelParameters from Cashier.frm_handpays
//                    Moved Handpays.GetName from Cashier.frm_handpays
//                    Added enum JACKPOT_PAYOUT_MODE
//                    Added Handpays.JackpotPayoutMode for read GP "WC2.JackpotPayoutMode" 
// 14-AUG-2013 LEM    Moved GetParameters and GetHandpaySessionPlayedAmount from Cashier.Handpay
// 22-OCT-2013 LEM    Unify saving account and cashier movements using SharedMovementsTables clases
// 04-APR-2014 ICS    Fixed Bug #WIGOSTITO-1189: Error paying a Handpay without ticket generated in a terminal with a player card inserted
// 25-APR-2014 DHA & JML Rename/Added TITO Cashier/Account Movements
// 08-SEP-2014 MPO    Handpay status management
// 08-SEP-2014 JCO    Added level, progressiveId, handpayId.
// 08-OCT-2014 OPC    Fixed Bug WIG-1435: Added an out parameter to get the HandPay Id.
// 15-OCT-2014 MPO    WIG-1504: The ticket jackpot is registered with incorrect movement
// 17-OCT-2014 MPO    Added undo handpay functions
// 27-OCT-2014 OPC    Fixed Bug WIG-1580: Modified function "CanApplyNoTax".
// 03-NOV-2014 MPO    WIG-1615: incorrect behavior when insert manual handpay
// 28-NOV-2014 MPO    WIG-1775: When Cashier.HandPays - LimitToAllowPayment = 0, don't use it
// 02-DEC-2014 MPO    WIG-1788: You must pay when the state is just pending
// 02-DEC-2014 MPO    Fixed bugs WIG-1793 / WIG-1788: need check status after update
// 18-DEC-2014 OPC    Fixed bugs WIG-1859: can't undo last handpay
// 17-DIC-2014 JBC    Fixed Bug WIG-1853: UndoOperations cash advance bank transaction
// 19-DIC-2014 OPC    WIG-1884: Handpay barcode is wrong (for voucher authorization)
// 16-JAN-2015 JMM & MPO  Heed on partial pay field comming from the 1B SAS poll
// 19-JAN-2015 OPC    Fixed bug WIG-1937: taxes always apply on tax base amount.
// 11-FEB-2015 DRV    Fixed Bug WIG-2041: Authourization vouhcer barcode allways show the same value.
// 24-FEB-2015 OPC    Fixed Bug WIG-2049: fixed error when try's to undo a handpay on tito.
// 10-MAR-2015 FOS    TASK-333: Televisa:Pagos Manuales de entrada Manual.
// 10-JUN-2015 FAV    WIG-241: HandPay, show message when trying to cancel handpay created by another cash session.
// 20-OCT-2015 ETP    Product backlog item 4690 Added funcionality for gaming hall Mode.
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 01-DEC-2015 FOS    Fixed Bug 7205: Protected taxWaiverMode and renamed function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015 FOS    Fixed Bug 7208: Incorrect Tax Value on Handpays
// 19-JAN-2016 FOS    Backlog Item 7554: Add column OperationId in table Handpays
// 19-JAN-2016 FOS    Backlog Item 7969: Apply Taxes in handpay authorization
// 15-FEB-2016 RAB    Bug 9140:No funcionan los pagos manuales por cr�ditos cancelados en Cashless
// 24-FEB-2016 EOR    Product Backlog Item 7402:SAS20: Add Comments HandPays
// 04-MAR-2016 EOR    Bug 10341:Handpays: incluir el OperationId en la tabla de handpays
// 07-MAR-2016 RGR    Task 10343: Handpays: Modify the form and class frm_handpays.vb
// 21-MAR-2016 RAB    Bug 9146: Pay terminal on Manual: can not be aborted after paying it
// 23-MAR-2016 JML    Fixed Bug 10877:Dual currency - Play sessions: "Jackpot" is reported in national currency for terminals configured in exchange currency
// 29-SEP-2016 FJC    Fixed Bug (Reopened) 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
// 22-NOV-2016 JMM    PBI 19744:Pago autom�tico de handpays
// 30-NOV-2016 ETP    Bug 21102 Old handpays are not payed after update
// 01-DEC-2016 AMF    Bug 21215:LimitToAllowPayment: aplica en salas Cashless
// 10-GEN-2017 XGJ    Bug 16372: Problemas con Jackpots
// 29-AUG-2017 DPC    [WIGOS-4348]: Tax value displayed with 1 cent less in Handpay ticket for cashier
// 16-SEP-2017 DPC    [WIGOS-4274]: [Ticket #855] Error encontrado Estad�sticas - Progresivos
// 04-JUL-2018 AGS    Bug 33481:WIGOS-13373 Undo Handpay linked to a player account is not linked later when selecting HandPay again
//--------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace WSI.Common
{
  #region ENUM

  public enum HANDPAY_PAYMENT_MODE
  {
    MANUAL = 0,
    AUTOMATIC = 1,
    BONUSING = 2
  }

  enum HANDPAY_QUERY_TYPE
  {
    NONE,
    UPDATE_TO_PAY,
    CANCEL_PAY,
    INSERT_PAY,
  }

  #endregion

  public class Handpays
  {
    public enum HANDPAY_MSG       // Handpay messages
    {
        REGISTER_CONF = 0
      , REGISTER_ERROR = 1
      , REGISTER_NOT_ALLOWED = 2
      , CANCEL_CONF = 3
      , CANCEL_NOT_FOUND = 4
      , CANCEL_NOT_ALLOWED = 5
      , CANCEL_EXPIRED = 6
      , REGISTER_AUTHORIZATION = 7
      , REGISTER_CONF_TITO = 8
      , REGISTER_ERROR_ALREADY_PAID = 9
      , REGISTER_ERROR_VOIDED = 10
    };

    #region Properties

    private Int32 m_hp_terminal_id;

    public Int32 Hp_Terminal_Id
    {
      get { return m_hp_terminal_id; }
      set { m_hp_terminal_id = value; }
    }
    private String m_hp_game_base_name;

    public String Hp_Game_Base_Name
    {
      get { return m_hp_game_base_name; }
      set { m_hp_game_base_name = value; }
    }
    private DateTime m_hp_datetime;

    public DateTime Hp_Datetime
    {
      get { return m_hp_datetime; }
      set { m_hp_datetime = value; }
    }
    private DateTime m_hp_previous_meters;

    public DateTime Hp_Previous_Meters
    {
      get { return m_hp_previous_meters; }
      set { m_hp_previous_meters = value; }
    }
    private Currency m_hp_amount;

    public Currency Hp_Amount
    {
      get { return m_hp_amount; }
      set { m_hp_amount = value; }
    }
    private String m_hp_te_name;

    public String Hp_Te_Name
    {
      get { return m_hp_te_name; }
      set { m_hp_te_name = value; }
    }
    private String m_hp_te_provider_id;

    public String Hp_Te_Provider_Id
    {
      get { return m_hp_te_provider_id; }
      set { m_hp_te_provider_id = value; }
    }
    private Int64 m_hp_movement_id;

    public Int64 Hp_Movement_Id
    {
      get { return m_hp_movement_id; }
      set { m_hp_movement_id = value; }
    }
    private Int32 m_hp_type;

    public Int32 Hp_Type
    {
      get { return m_hp_type; }
      set { m_hp_type = value; }
    }
    private Int64 m_hp_play_session_id;

    public Int64 Hp_Play_Session_Id
    {
      get { return m_hp_play_session_id; }
      set { m_hp_play_session_id = value; }
    }
    private Int32 m_hp_site_jackpot_index;

    public Int32 Hp_Site_Jackpot_Index
    {
      get { return m_hp_site_jackpot_index; }
      set { m_hp_site_jackpot_index = value; }
    }
    private String m_hp_site_jackpot_name;

    public String Hp_Site_Jackpot_Name
    {
      get { return m_hp_site_jackpot_name; }
      set { m_hp_site_jackpot_name = value; }
    }
    private Int32 m_hp_site_jackpot_awarded_on_terminal_id;

    public Int32 Hp_Site_Jackpot_Awarded_On_Terminal_Id
    {
      get { return m_hp_site_jackpot_awarded_on_terminal_id; }
      set { m_hp_site_jackpot_awarded_on_terminal_id = value; }
    }
    private Int64 m_hp_site_jackpot_awarded_to_account_id;

    public Int64 Hp_Site_Jackpot_Awarded_To_Account_Id
    {
      get { return m_hp_site_jackpot_awarded_to_account_id; }
      set { m_hp_site_jackpot_awarded_to_account_id = value; }
    }
    private Int32 m_hp_status;

    public Int32 Hp_Status
    {
      get { return m_hp_status; }
      set { m_hp_status = value; }
    }
    private Boolean m_hp_site_jackpot_notified;

    public Boolean Hp_Site_Jackpot_Notified
    {
      get { return m_hp_site_jackpot_notified; }
      set { m_hp_site_jackpot_notified = value; }
    }
    private Int64 m_hp_ticket_id;

    public Int64 Hp_Ticket_Id
    {
      get { return m_hp_ticket_id; }
      set { m_hp_ticket_id = value; }
    }
    private Int64 m_hp_transaction_id;

    public Int64 Hp_Transaction_Id
    {
      get { return m_hp_transaction_id; }
      set { m_hp_transaction_id = value; }
    }
    private Int64 m_hp_candidate_play_session_id;

    public Int64 Hp_Candidate_Play_Session_Id
    {
      get { return m_hp_candidate_play_session_id; }
      set { m_hp_candidate_play_session_id = value; }
    }
    private Int64 m_hp_candidate_prev_play_session_id;

    public Int64 Hp_Candidate_Prev_Play_Session_Id
    {
      get { return m_hp_candidate_prev_play_session_id; }
      set { m_hp_candidate_prev_play_session_id = value; }
    }
    private Int64 m_hp_progressive_id;

    public Int64 Hp_Progressive_Id
    {
      get { return m_hp_progressive_id; }
      set { m_hp_progressive_id = value; }
    }
    private Int32 m_hp_level;

    public Int32 Hp_Level
    {
      get { return m_hp_level; }
      set { m_hp_level = value; }
    }
    private Int64 m_hp_id;

    public Int64 Hp_Id
    {
      get { return m_hp_id; }
      set { m_hp_id = value; }
    }
    private DateTime m_hp_status_changed;

    public DateTime Hp_Status_Changed
    {
      get { return m_hp_status_changed; }
      set { m_hp_status_changed = value; }
    }
    private Decimal m_hp_tax_base_amount;

    public Decimal Hp_Tax_Base_Amount
    {
      get { return m_hp_tax_base_amount; }
      set { m_hp_tax_base_amount = value; }
    }
    private Decimal m_hp_tax_amount;

    public Decimal Hp_Tax_Amount
    {
      get { return m_hp_tax_amount; }
      set { m_hp_tax_amount = value; }
    }
    private Decimal m_hp_tax_pct;

    public Decimal Hp_Tax_Pct
    {
      get { return m_hp_tax_pct; }
      set { m_hp_tax_pct = value; }
    }

    private Decimal m_hp_partial_pay;

    public Decimal Hp_Partial_Pay
    {
      get { return m_hp_partial_pay; }
      set { m_hp_partial_pay = value; }
    }

    private Int64 m_hp_operation_id;

    public Int64 Hp_Operation_Id
    {
      get { return m_hp_operation_id; }
      set { m_hp_operation_id = value; }
    }

    private Currency m_hp_amt_0;

    public Currency Hp_Amt_0
    {
      get { return m_hp_amt_0; }
      set { m_hp_amt_0 = value; }
    }

    private String m_hp_cur_0;

    public String Hp_Cur_0
    {
      get { return m_hp_cur_0; }
      set { m_hp_cur_0 = value; }
    }

    private Currency m_hp_amt_1;

    public Currency Hp_Amt_1
    {
      get { return m_hp_amt_1; }
      set { m_hp_amt_1 = value; }
    }

    private String m_hp_cur_1;

    public String Hp_Cur_1
    {
      get { return m_hp_cur_1; }
      set { m_hp_cur_1 = value; }
    }

    #endregion

    public static Boolean GetHandpayFromHandpayId(Int64 HandpayId, out Handpays Handpays)
    {
      Boolean _result;
      StringBuilder _sb;

      Handpays = new Handpays();
      _result = true;
      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   HP_TERMINAL_ID");
      _sb.AppendLine("       , HP_GAME_BASE_NAME");
      _sb.AppendLine("       , HP_DATETIME");
      _sb.AppendLine("       , HP_PREVIOUS_METERS");
      _sb.AppendLine("       , dbo.ApplyExchange2(ISNULL(HP_AMT0, HP_AMOUNT), ISNULL(HP_CUR0, @pNatCur), @pNatCur) AS HP_AMOUNT ");
      _sb.AppendLine("       , HP_TE_NAME");
      _sb.AppendLine("       , HP_TE_PROVIDER_ID");
      _sb.AppendLine("       , HP_MOVEMENT_ID");
      _sb.AppendLine("       , HP_TYPE");
      _sb.AppendLine("       , HP_PLAY_SESSION_ID");
      _sb.AppendLine("       , HP_SITE_JACKPOT_INDEX");
      _sb.AppendLine("       , HP_SITE_JACKPOT_NAME");
      _sb.AppendLine("       , HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID");
      _sb.AppendLine("       , HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID");
      _sb.AppendLine("       , HP_STATUS");
      _sb.AppendLine("       , HP_SITE_JACKPOT_NOTIFIED");
      _sb.AppendLine("       , HP_TICKET_ID");
      _sb.AppendLine("       , HP_TRANSACTION_ID");
      _sb.AppendLine("       , HP_CANDIDATE_PLAY_SESSION_ID");
      _sb.AppendLine("       , HP_CANDIDATE_PREV_PLAY_SESSION_ID");
      _sb.AppendLine("       , HP_PROGRESSIVE_ID");
      _sb.AppendLine("       , HP_LEVEL");
      _sb.AppendLine("       , HP_ID");
      _sb.AppendLine("       , HP_STATUS_CHANGED");
      _sb.AppendLine("       , HP_TAX_BASE_AMOUNT");
      _sb.AppendLine("       , HP_TAX_AMOUNT");
      _sb.AppendLine("       , HP_TAX_PCT");
      _sb.AppendLine("       , isnull (HP_LONG_POLL_1B_DATA.value('(/WCP_MsgEGMHandpays/Handpays/Handpay/@PartialPayCents)[1]', 'BigInt'), 0) AS HP_PARTIAL_PAY_CENTS");
      _sb.AppendLine("       , HP_OPERATION_ID");
      _sb.AppendLine("       , ISNULL(HP_AMT0, HP_AMOUNT) AS HP_AMT0 ");
      _sb.AppendLine("       , ISNULL(HP_CUR0, '') AS HP_CUR0 ");
      _sb.AppendLine("       , HP_AMT1 ");
      _sb.AppendLine("       , HP_CUR1 ");
      _sb.AppendLine("  FROM   HANDPAYS");
      _sb.AppendLine(" WHERE   HP_ID = @pHpId");

      using (DB_TRX _db = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db.SqlTransaction.Connection, _db.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pHpId", SqlDbType.BigInt).Value = HandpayId;
          _sql_cmd.Parameters.Add("@pNatCur", SqlDbType.NVarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "");

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              _result = false;
            }

            Handpays.Hp_Terminal_Id = _reader.IsDBNull(0) ? -1 : _reader.GetInt32(0);
            Handpays.Hp_Game_Base_Name = _reader.IsDBNull(1) ? String.Empty : _reader.GetString(1);
            Handpays.Hp_Datetime = _reader.GetDateTime(2);
            Handpays.Hp_Previous_Meters = _reader.IsDBNull(3) ? WGDB.Now : _reader.GetDateTime(3);
            Handpays.Hp_Amount = (Currency)_reader.GetDecimal(4);
            Handpays.Hp_Te_Name = _reader.IsDBNull(5) ? String.Empty : _reader.GetString(5);
            Handpays.Hp_Te_Provider_Id = _reader.IsDBNull(6) ? String.Empty : _reader.GetString(6);
            Handpays.Hp_Movement_Id = _reader.IsDBNull(7) ? -1 : _reader.GetInt64(7);
            Handpays.Hp_Type = _reader.GetInt32(8);
            Handpays.Hp_Play_Session_Id = _reader.IsDBNull(9) ? -1 : _reader.GetInt64(9);
            Handpays.Hp_Site_Jackpot_Index = _reader.IsDBNull(10) ? -1 : _reader.GetInt32(10);
            Handpays.Hp_Site_Jackpot_Name = _reader.IsDBNull(11) ? String.Empty : _reader.GetString(11);
            Handpays.Hp_Site_Jackpot_Awarded_On_Terminal_Id = _reader.IsDBNull(12) ? -1 : _reader.GetInt32(12);
            Handpays.Hp_Site_Jackpot_Awarded_To_Account_Id = _reader.IsDBNull(13) ? -1 : _reader.GetInt64(13);
            Handpays.Hp_Status = _reader.IsDBNull(14) ? -1 : _reader.GetInt32(14);
            Handpays.Hp_Site_Jackpot_Notified = _reader.GetBoolean(15);
            Handpays.Hp_Ticket_Id = _reader.IsDBNull(16) ? -1 : _reader.GetInt64(16);
            Handpays.Hp_Transaction_Id = _reader.IsDBNull(17) ? -1 : _reader.GetInt64(17);
            Handpays.Hp_Candidate_Play_Session_Id = _reader.IsDBNull(18) ? -1 : _reader.GetInt64(18);
            Handpays.Hp_Candidate_Prev_Play_Session_Id = _reader.IsDBNull(19) ? -1 : _reader.GetInt64(19);
            Handpays.Hp_Progressive_Id = _reader.IsDBNull(20) ? -1 : _reader.GetInt64(20);
            Handpays.Hp_Level = _reader.IsDBNull(21) ? -1 : _reader.GetInt32(21);
            Handpays.Hp_Id = _reader.GetInt64(22);
            Handpays.Hp_Status_Changed = _reader.IsDBNull(23) ? WGDB.Now : _reader.GetDateTime(23); ;
            Handpays.Hp_Tax_Base_Amount = _reader.IsDBNull(24) ? 0 : _reader.GetDecimal(24);
            Handpays.Hp_Tax_Amount = _reader.IsDBNull(25) ? 0 : _reader.GetDecimal(25);
            Handpays.Hp_Tax_Pct = _reader.IsDBNull(26) ? 0 : _reader.GetDecimal(26);
            Handpays.Hp_Partial_Pay = _reader.IsDBNull(27) ? 0 : ((Decimal)_reader.GetInt64(27) / 100);
            Handpays.Hp_Operation_Id = _reader.IsDBNull(28) ? 0 : ((Int64)_reader.GetInt64(28));
            Handpays.Hp_Amt_0 = _reader.IsDBNull(29) ? 0 : ((Currency)_reader.GetDecimal(29));
            Handpays.Hp_Cur_0 = _reader.IsDBNull(30) ? String.Empty : _reader.GetString(30);
            Handpays.Hp_Amt_1 = _reader.IsDBNull(31) ? 0 : ((Currency)_reader.GetDecimal(31));
            Handpays.Hp_Cur_1 = _reader.IsDBNull(32) ? String.Empty : _reader.GetString(32);
          }
        }
      }

      return _result;
    }

    public class HandpayRegisterOrCancelParameters
    {
      public HANDPAY_TYPE HandpayType = HANDPAY_TYPE.UNKNOWN;
      public DateTime HandpayDate = DateTime.MinValue;
      public Decimal HandpayAmount = 0;
      public Boolean CancelHandpay = false;
      public Int64 CancelPlaySessionId = 0;
      public Int64 CancelMovementId = 0;
      public CardData CardData = null;
      public Int32 TerminalId = 0;
      public String TerminalProvider = "";
      public String TerminalName = "";
      public Currency SessionPlayedAmount = 0;
      public String DenominationStr = "";
      public String DenominationValue = "";
      public Voucher Voucher = null;
      public Int32 SiteJackpotAwardedOnTerminalId;
      public Int32 Level;
      public Int64 ProgressiveId;
      public Int64 HandpayId = 0;
      public Int32 Status;
      public Boolean IsTicket = false;
      public Decimal TaxBaseAmount = 0;
      public Decimal TaxBasePct = 0;
      public Decimal TaxAmount = 0;
      public TITO_TICKET_STATUS TicketStatus;
      public Int64 AccountOperationReasonId = 0;
      public String AccountOperationComment = "";
      public Boolean IsApplyTaxes = true;
      public Decimal HandpayAmt0 = 0;
      public String HandpayCur0 = "";
      public Decimal HandpayAmt1 = 0;
      public String HandpayCur1 = "";
      //EOR 24-FEB-2016 Add Comments HandPays
      public String AccountOperationCommentHandPay = "";
    }

    #region PUBLICS

    public static HANDPAY_PAYMENT_MODE SiteJackpotPaymentMode
    {
      get
      {
        if (GeneralParam.GetBoolean("SiteJackpot", "Bonusing.Enabled", false))
        {
          return HANDPAY_PAYMENT_MODE.BONUSING;
        }

        if (GeneralParam.GetInt32("SiteJackpot", "PaymentMode") == 1)
        {
          return HANDPAY_PAYMENT_MODE.AUTOMATIC;
        }

        return HANDPAY_PAYMENT_MODE.MANUAL;
      }
    }

    public static String HandpayTypeString(HANDPAY_TYPE HandpayType, Int32 Level)
    {
      Boolean _is_manual;

      _is_manual = false;
      if ((Int32)HandpayType >= 1000)
      {
        HandpayType = HandpayType - 1000;
        _is_manual = true;
      }

      switch (HandpayType)
      {
        case HANDPAY_TYPE.CANCELLED_CREDITS:
          return Resource.String("STR_FRM_HANDPAYS_017");

        case HANDPAY_TYPE.JACKPOT:
          if (Level >= 1 && Level <= 32)
          {
            return Resource.String("STR_FRM_HANDPAYS_029");
          }
          else if (Level == 64)
          {
            return Resource.String("STR_FRM_HANDPAYS_046");
          }
          else
          {
            return Resource.String("STR_FRM_HANDPAYS_018");
          }

        case HANDPAY_TYPE.ORPHAN_CREDITS:
          return Resource.String("STR_FRM_HANDPAYS_019");

        case HANDPAY_TYPE.MANUAL:
          return _is_manual ? Resource.String("STR_FRM_HANDPAYS_047") : Resource.String("STR_FRM_HANDPAYS_020");

        case HANDPAY_TYPE.SITE_JACKPOT:
          return Resource.String("STR_FRM_HANDPAYS_026");

        case HANDPAY_TYPE.SPECIAL_PROGRESSIVE:
          return Resource.String("STR_FRM_HANDPAYS_044");

        case HANDPAY_TYPE.UNKNOWN:
        default:
          break;
      }

      return "";
    } //HandpayTypeString

    public static String HandpayStatusString(Int32 Status)
    {
      if (StatusActivated(Status, HANDPAY_STATUS.VOIDED))
      {
        return Resource.String("STR_FRM_HANDPAYS_037");
      }

      if (StatusActivated(Status, HANDPAY_STATUS.PAID))
      {
        return Resource.String("STR_FRM_HANDPAYS_035");
      }

      if (StatusActivated(Status, HANDPAY_STATUS.AUTHORIZED))
      {
        return Resource.String("STR_FRM_HANDPAYS_038");
      }

      if (StatusActivated(Status, HANDPAY_STATUS.PENDING))
      {
        return Resource.String("STR_FRM_HANDPAYS_034");
      }

      if (StatusActivated(Status, HANDPAY_STATUS.EXPIRED))
      {
        return Resource.String("STR_FRM_HANDPAYS_036");
      }

      return "";

    } //HandpayTypeString

    public static Boolean RegisterOrCancel(HandpayRegisterOrCancelParameters InputHandpay,
                                           CashierSessionInfo SessionInfo,
                                           OperationCode OperationCode,
                                           out Int16 HandpayErrorMsg,
                                           SqlTransaction Trx)
    {
      Int64 _operation_id;
      _operation_id = 0;

      return RegisterOrCancel(InputHandpay, SessionInfo, true, OperationCode, ref _operation_id, out HandpayErrorMsg, Trx);
    }

    public static Boolean RegisterOrCancel(HandpayRegisterOrCancelParameters InputHandpay,
                                       CashierSessionInfo SessionInfo,
                                           OperationCode OperationCode,
                                           Int64? UpdatePlaySessionId, 
                                           SqlTransaction Trx)
    {
      Int64 _operation_id;
      Int64 _play_session_id;
      Int16 HandpayErrorMsg;

      _operation_id = 0;

      return RegisterOrCancel(InputHandpay, SessionInfo, true, null, OperationCode, UpdatePlaySessionId, ref _operation_id, out _play_session_id, out HandpayErrorMsg, Trx);
    }

    public static Boolean RegisterOrCancel(HandpayRegisterOrCancelParameters InputHandpay,
                                       CashierSessionInfo SessionInfo,
                                       Boolean InsertOperation,
                                       OperationCode OperationCode,
                                       ref Int64 OperationId,
                                       out Int16 HandpayErrorMsg,
                                       SqlTransaction Trx)
    {
      Int64 _play_session_id;

      return RegisterOrCancel(InputHandpay, SessionInfo, true, null, OperationCode, ref OperationId, out _play_session_id, out HandpayErrorMsg, Trx);
    }
    public static Boolean RegisterOrCancel(HandpayRegisterOrCancelParameters InputHandpay,
                                       CashierSessionInfo SessionInfo,
                                       Boolean InsertOperation,
                                       ref Int64 OperationId,
                                       out Int64 PlaySessionId,
                                       out Int16 HandpayErrorMsg,
                                       SqlTransaction Trx)
    {
      return RegisterOrCancel(InputHandpay, SessionInfo, InsertOperation, null, OperationCode.NOT_SET, ref OperationId, out PlaySessionId, out HandpayErrorMsg, Trx);
    }

    public static Boolean RegisterOrCancel(HandpayRegisterOrCancelParameters InputHandpay,
                                           CashierSessionInfo SessionInfo,
                                           Boolean InsertOperation,
                                           VoucherValidationNumber Ticket,
                                           OperationCode OperationCode,
                                           ref Int64 OperationId,
                                           out Int64 PlaySessionId,
                                           out Int16 HandpayErrorMsg,
                                           SqlTransaction Trx)
    {
      return RegisterOrCancel(InputHandpay, SessionInfo, InsertOperation, null, OperationCode.NOT_SET, null, ref OperationId, out PlaySessionId, out HandpayErrorMsg, Trx);
    }

    public static Boolean RegisterOrCancel(HandpayRegisterOrCancelParameters InputHandpay,
                                           CashierSessionInfo SessionInfo,
                                           Boolean InsertOperation,
                                           VoucherValidationNumber Ticket,
                                           OperationCode OperationCode,
                                           Int64? UpdatePlaySessionId,
                                           ref Int64 OperationId,
                                           out Int64 PlaySessionId,
                                           out Int16 HandpayErrorMsg,
                                           SqlTransaction Trx)
    {
      Int64 _movement_id;
      TerminalTypes _terminal_type;
      CardData _card_data;

      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _fin_balance;
      MultiPromos.AccountBalance _playable_balance;
      MultiPromos.AccountBalance _handpay_balance;
      MultiPromos.AccountBalance _handpay_balance_egm;
      MultiPromos.AccountBalance _previous_hp_balance;
      MultiPromos.AccountBalance _dummy_balance;
      MultiPromos.AccountBalance _won;
      MultiPromos.AccountBalance _won_egm;
      MultiPromos.InSessionParameters _params;

      MovementType _movement_type;
      //String _user_and_terminal_name;
      CASHIER_MOVEMENT _cashier_movement_type;
      VoucherCardHandpay _hp_pay_voucher;
      VoucherCardHandpayCancellation _hp_cancel_voucher;
      Voucher _voucher;
      StringBuilder _sql_handpay;
      Currency _account_sub_amount;
      Currency _account_add_amount;
      Currency _cashier_sub_amount;
      Currency _cashier_add_amount;
      StringBuilder _err_msg;

      OperationCode _operation_code;
      Currency _amount;

      CashierMovementsTable _cashier_mov_table;
      AccountMovementsTable _account_mov_table;

      // Tito
      StringBuilder _sb;
      Boolean _is_jackpot;   // used to calculate max devolution
      Boolean _upd_progressive;
      HANDPAY_QUERY_TYPE _query_string;
      SqlParameter _new_hp_id_param;
      Boolean _is_tito;

      Decimal _sys_amt;
      String _egm_iso;
      Decimal _egm_amt;

      Int32 _reg_afected;

      // Steps
      //    - Block account
      //    - Know playable balance available
      //    - Played and Won of playable balance
      //    - New Play Session
      //    - Update account balance
      //    - New Card Movement ; New Cashier Movement 
      //    - Update / Insert the HandPay record
      //    - Generate handpay voucher

      _card_data = InputHandpay.CardData;

      _previous_hp_balance = MultiPromos.AccountBalance.Zero;
      _fin_balance = MultiPromos.AccountBalance.Zero;
      _sql_handpay = new StringBuilder();
      _err_msg = new StringBuilder();
      _upd_progressive = false;
      _query_string = HANDPAY_QUERY_TYPE.NONE;

      _is_tito = WSI.Common.TITO.Utils.IsTitoMode() || Misc.IsGamingHallMode();

      PlaySessionId = 0;
      HandpayErrorMsg = (Int16)Handpays.HANDPAY_MSG.REGISTER_ERROR;

      try
      {
        //
        //  Block account
        //

        if (!MultiPromos.Trx_UpdateAccountBalance(_card_data.AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out PlaySessionId, Trx))
        {
          return false;
        }

        //
        // Initialize handpay query/type of movements/add play session
        // 

        switch (InputHandpay.HandpayType)
        {
          case HANDPAY_TYPE.CANCELLED_CREDITS:
          case HANDPAY_TYPE.JACKPOT:
          case HANDPAY_TYPE.SITE_JACKPOT:
          case HANDPAY_TYPE.ORPHAN_CREDITS:

            if (InputHandpay.CancelHandpay)
            {
              _movement_type = MovementType.HandpayCancellation;
              _cashier_movement_type = CASHIER_MOVEMENT.HANDPAY_CANCELLATION;

              _query_string = HANDPAY_QUERY_TYPE.CANCEL_PAY;
            }
            else
            {
              // DHA 25-APR-2014 modified/added TITO tickets movements
              // MPO 15-OCT-2014 WIG-1504: The ticket jackpot is registered with incorrect movement
              if (InputHandpay.IsTicket)
              {
                if (InputHandpay.HandpayType == HANDPAY_TYPE.JACKPOT)
                {
                  if (InputHandpay.TicketStatus == TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION)
                  {
                    _movement_type = MovementType.TITO_TicketCashierExpiredPaidJackpot;
                    _cashier_movement_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_JACKPOT;
                  }
                  else
                  {
                    _movement_type = MovementType.TITO_TicketCashierPaidJackpot;
                    _cashier_movement_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_JACKPOT;
                  }
                }
                else
                {
                  if (InputHandpay.TicketStatus == TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION)
                  {
                    _movement_type = MovementType.TITO_TicketCashierExpiredPaidHandpay;
                    _cashier_movement_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_HANDPAY;
                  }
                  else
                  {
                    _movement_type = MovementType.TITO_TicketCashierPaidHandpay;
                    _cashier_movement_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_HANDPAY;
                  }
                }
              }
              else
              {
                _movement_type = MovementType.Handpay;
                _cashier_movement_type = CASHIER_MOVEMENT.HANDPAY;
              }

              _query_string = HANDPAY_QUERY_TYPE.UPDATE_TO_PAY;

            }

            break;

          case HANDPAY_TYPE.MANUAL:
          // Manual subtypes
          case HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS:
          case HANDPAY_TYPE.MANUAL_JACKPOT:
          case HANDPAY_TYPE.MANUAL_OTHERS:

            if (InputHandpay.CancelHandpay)
            {
              _movement_type = MovementType.ManualHandpayCancellation;
              _cashier_movement_type = CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION;
            }
            else
            {
              _movement_type = MovementType.ManualHandpay;
              _cashier_movement_type = CASHIER_MOVEMENT.MANUAL_HANDPAY;
            }

            _upd_progressive = (InputHandpay.HandpayType == HANDPAY_TYPE.MANUAL_JACKPOT && InputHandpay.Level >= 0);
            if (InputHandpay.HandpayId == 0)
            {
              _query_string = HANDPAY_QUERY_TYPE.INSERT_PAY;
            }
            else
            {
              if (InputHandpay.CancelHandpay)
              {
                _query_string = HANDPAY_QUERY_TYPE.INSERT_PAY;
              }
              else
              {
                _query_string = HANDPAY_QUERY_TYPE.UPDATE_TO_PAY;
              }
            }

            break;

          default:
            Log.Error("Unexpected HandPayType " + InputHandpay.HandpayType.ToString());

            return false;
        }

        switch (_query_string)
        {
          case HANDPAY_QUERY_TYPE.UPDATE_TO_PAY:
            _sql_handpay.Length = 0;
            _sql_handpay.AppendLine(" UPDATE   HANDPAYS                                   ");
            _sql_handpay.AppendLine("    SET   HP_MOVEMENT_ID     = @pMovementId          ");
            _sql_handpay.AppendLine("        , HP_PLAY_SESSION_ID = @pPlaySessionId       ");
            _sql_handpay.AppendLine("        , HP_STATUS          = HP_STATUS & ~(@pMaskStatus + @pMaskTax) + @pStatusPaid + @pStatusTax");
            _sql_handpay.AppendLine("        , HP_STATUS_CHANGED  = GETDATE()             ");
            _sql_handpay.AppendLine("        , HP_AMOUNT          = @pInsertHandpayAmount ");
            _sql_handpay.AppendLine("        , HP_TAX_BASE_AMOUNT = @pTaxBaseAmount       ");
            _sql_handpay.AppendLine("        , HP_TAX_AMOUNT      = @pTaxAmount           ");
            _sql_handpay.AppendLine("        , HP_TAX_PCT         = @pTaxPercentage       ");
            _sql_handpay.AppendLine("        , HP_AMT1            = @pAmount1             ");
            _sql_handpay.AppendLine("        , HP_CUR1            = @pCurren1             ");

            //XGJ 10-GEN-2017
            _sql_handpay.AppendLine("        , HP_OPERATION_ID    = @pOperationId         ");

            _sql_handpay.AppendLine("  WHERE   HP_TERMINAL_ID     = @pTerminalId          ");
            _sql_handpay.AppendLine("    AND   HP_DATETIME        = @pDateTime            ");
            _sql_handpay.AppendLine("    AND   isNull(HP_AMT0, HP_AMOUNT) = isNull(@pAmount0, @pUpdateHandpayAmount) ");
            _sql_handpay.AppendLine("    AND   HP_MOVEMENT_ID     IS NULL                 ");
            _sql_handpay.AppendLine("    AND   HP_STATUS & @pMaskStatus = @pStatusPending ");

            break;
          case HANDPAY_QUERY_TYPE.CANCEL_PAY:

            _sql_handpay.Length = 0;
            _sql_handpay.AppendLine(" UPDATE   HANDPAYS                                   ");
            _sql_handpay.AppendLine("    SET   HP_MOVEMENT_ID     = NULL                  ");
            _sql_handpay.AppendLine("        , HP_STATUS          = @pStatusPending       ");
            _sql_handpay.AppendLine("        , HP_STATUS_CHANGED  = GETDATE()             ");
            _sql_handpay.AppendLine("  WHERE   HP_TERMINAL_ID     = @pTerminalId          ");
            _sql_handpay.AppendLine("    AND   HP_DATETIME        = @pDateTime            ");
            _sql_handpay.AppendLine("    AND   isNull(HP_AMT0, HP_AMOUNT) = isNull(@pAmount0, @pUpdateHandpayAmount) ");
            _sql_handpay.AppendLine("    AND   HP_MOVEMENT_ID     = @pCancelMovementId    ");
            _sql_handpay.AppendLine("    AND   HP_PLAY_SESSION_ID = @pCancelPlaySessionId ");
            _sql_handpay.AppendLine("    AND   HP_STATUS & @pMaskStatus = @pStatusPaid    ");

            break;
          case HANDPAY_QUERY_TYPE.INSERT_PAY:

            _sql_handpay.Length = 0;
            _sql_handpay.AppendLine(" INSERT INTO   HANDPAYS              ");
            _sql_handpay.AppendLine("             ( HP_TERMINAL_ID        ");
            _sql_handpay.AppendLine("             , HP_GAME_BASE_NAME     ");
            _sql_handpay.AppendLine("             , HP_DATETIME           ");
            _sql_handpay.AppendLine("             , HP_PREVIOUS_METERS    ");
            _sql_handpay.AppendLine("             , HP_AMOUNT             ");
            _sql_handpay.AppendLine("             , HP_TE_NAME            ");
            _sql_handpay.AppendLine("             , HP_TE_PROVIDER_ID     ");
            _sql_handpay.AppendLine("             , HP_MOVEMENT_ID        ");
            _sql_handpay.AppendLine("             , HP_TYPE               ");
            _sql_handpay.AppendLine("             , HP_STATUS             ");

            //EOR 04-MAR-2016 Add Column hp_operation_id
            _sql_handpay.AppendLine("             , HP_OPERATION_ID       ");

            if (_upd_progressive)
            {
              _sql_handpay.AppendLine("             , HP_PROGRESSIVE_ID ");
              _sql_handpay.AppendLine("             , HP_LEVEL ");
            }

            _sql_handpay.AppendLine("             , HP_PLAY_SESSION_ID    ");
            _sql_handpay.AppendLine("             , HP_STATUS_CHANGED     ");
            _sql_handpay.AppendLine("             , HP_TAX_BASE_AMOUNT    ");
            _sql_handpay.AppendLine("             , HP_TAX_AMOUNT         ");
            _sql_handpay.AppendLine("             , HP_TAX_PCT            ");
            _sql_handpay.AppendLine("             , HP_AMT0               ");
            _sql_handpay.AppendLine("             , HP_CUR0               ");
            _sql_handpay.AppendLine("             , HP_AMT1               ");
            _sql_handpay.AppendLine("             , HP_CUR1)              ");
            _sql_handpay.AppendLine("      VALUES                         ");
            _sql_handpay.AppendLine("             ( @pTerminalId          ");
            _sql_handpay.AppendLine("             , NULL                  ");
            _sql_handpay.AppendLine("             , GETDATE()             ");
            _sql_handpay.AppendLine("             , NULL                  ");
            _sql_handpay.AppendLine("             , @pInsertHandpayAmount ");
            _sql_handpay.AppendLine("             , @pTerminalName        ");
            _sql_handpay.AppendLine("             , @pTerminalProvider    ");
            _sql_handpay.AppendLine("             , @pMovementId          ");
            _sql_handpay.AppendLine("             , @pType                ");
            _sql_handpay.AppendLine("             , @pStatusPaid          ");

            //EOR 04-MAR-2016 Add Parameter hp_operation_id
            _sql_handpay.AppendLine("             , @pOperationId         ");

            if (_upd_progressive)
            {
              _sql_handpay.AppendLine("             , @pProgressiveId ");
              _sql_handpay.AppendLine("             , @pLevel ");
            }

            _sql_handpay.AppendLine("             , @pPlaySessionId       ");
            _sql_handpay.AppendLine("             , GETDATE()             ");
            _sql_handpay.AppendLine("             , @pTaxBaseAmount       ");
            _sql_handpay.AppendLine("             , @pTaxAmount           ");
            _sql_handpay.AppendLine("             , @pTaxPercentage       ");
            _sql_handpay.AppendLine("             , @pAmount0             ");
            _sql_handpay.AppendLine("             , @pCurren0             ");
            _sql_handpay.AppendLine("             , @pAmount1             ");
            _sql_handpay.AppendLine("             , @pCurren1)            ");
            _sql_handpay.AppendLine("  SET   @pNewHpId = SCOPE_IDENTITY()");

            break;
          case HANDPAY_QUERY_TYPE.NONE:
          default:
            Log.Error("Unexpected HandPayType " + InputHandpay.HandpayType.ToString());

            return false;
        }

        //
        // Know payable balance available
        //
        if (!_is_tito)
        {
          if (!MultiPromos.Trx_GetPlayableBalance(_card_data.AccountId, InputHandpay.TerminalId,
                                                  MultiPromos.HANDPAY_VIRTUAL_PLAY_SESSION_ID, 0, _ini_balance, out _playable_balance, Trx))
          {
            Log.Error("Trx_GetPlayableBalance failed.");

            return false;
          }
        }

        //
        // Played and Won of playable balance
        //    
        // 
        _ini_balance = MultiPromos.AccountBalance.Zero;
        if (!_is_tito && InputHandpay.CancelHandpay)
        {
          using (SqlCommand _sql_cmd = new SqlCommand("SELECT PS_REDEEMABLE_CASH_OUT, PS_NON_REDEEMABLE_CASH_OUT FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @pPlaySessionId", Trx.Connection, Trx))
          {
            _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = InputHandpay.CancelPlaySessionId;
            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                Log.Error("Read HandPay amounts to be cancelled.");

                return false;
              }
              // cambiar nombre a previous
              _ini_balance = new MultiPromos.AccountBalance(_reader.GetDecimal(0), 0, _reader.GetDecimal(1));
              _previous_hp_balance = new MultiPromos.AccountBalance(_ini_balance);
            }
          }
        }

        if (!_is_tito && InputHandpay.HandpayType != HANDPAY_TYPE.SITE_JACKPOT)
        {
          Decimal _aux_delta_won;

          _aux_delta_won = 0;

          _params = new MultiPromos.InSessionParameters();
          _params.AccountId = _card_data.AccountId;
          _params.Balance = new MultiPromos.AccountBalance(_ini_balance);
          _params.PlaySessionId = MultiPromos.HANDPAY_VIRTUAL_PLAY_SESSION_ID;
          if (InputHandpay.CancelHandpay)
          {
            _params.DeltaPlayed = InputHandpay.HandpayAmount;
            _params.DeltaWon = 0;
          }
          else
          {
            _params.DeltaPlayed = 0;
            _params.DeltaWon = InputHandpay.HandpayAmount;
          }

          if (UpdatePlaySessionId != null && InputHandpay.HandpayType == HANDPAY_TYPE.CANCELLED_CREDITS)
          {
            _aux_delta_won = _params.DeltaWon;
            _params.DeltaWon = 0;
          }

          if (!MultiPromos.Trx_UpdatePlayedAndWon(_params, _err_msg, true, Trx))
          {
            Log.Message(_err_msg.ToString());
            Log.Error("Trx_UpdatePlayedAndWon failed.");

            return false;
          }

          if (UpdatePlaySessionId != null && InputHandpay.HandpayType == HANDPAY_TYPE.CANCELLED_CREDITS)
          {
            _params.DeltaWon = _aux_delta_won;
            _params.Balance.Redeemable += _params.DeltaWon;
          }

          _handpay_balance = _params.Balance - _ini_balance;
          _handpay_balance_egm = _handpay_balance;
        }
        else
        {
          _handpay_balance = new MultiPromos.AccountBalance(InputHandpay.HandpayAmount, 0, 0);
          _handpay_balance_egm = new MultiPromos.AccountBalance(InputHandpay.HandpayAmt0, 0, 0);

          if (_is_tito && InputHandpay.CancelHandpay)
          {
            _previous_hp_balance = new MultiPromos.AccountBalance(_handpay_balance);
            _handpay_balance = MultiPromos.AccountBalance.Negate(_handpay_balance);
            _handpay_balance_egm = MultiPromos.AccountBalance.Negate(_handpay_balance_egm);
          }
        }

        if (!_is_tito && InputHandpay.CancelHandpay)
        {
          _dummy_balance = MultiPromos.AccountBalance.Negate(_handpay_balance);
          if (_dummy_balance.Redeemable != _previous_hp_balance.Redeemable ||
              _dummy_balance.PromoRedeemable != _previous_hp_balance.PromoRedeemable ||
              _dummy_balance.PromoNotRedeemable != _previous_hp_balance.PromoNotRedeemable)
          {
            Log.Error("HandPay balance mismatch.");

            return false;
          }
        }

        if (!_is_tito)
        {
          if (!MultiPromos.Trx_UnlinkPromotionsInSession(_card_data.AccountId, MultiPromos.HANDPAY_VIRTUAL_PLAY_SESSION_ID, 0, Trx))
          {
            Log.Error("Trx_UnlinkPromotionsInSession failed.");

            return false;
          }
        }

        //
        // New Play Session
        //

        if (!Terminal.Trx_GetTerminalType(InputHandpay.TerminalId, Trx, out _terminal_type))
        {
          Log.Error("Terminal.Trx_GetTerminalType failed.");

          return false;
        }

        _won = MultiPromos.AccountBalance.Zero;
        _won_egm = MultiPromos.AccountBalance.Zero;
        if (InputHandpay.HandpayType == HANDPAY_TYPE.JACKPOT
            || InputHandpay.HandpayType == HANDPAY_TYPE.SITE_JACKPOT)
        {
          _won = new MultiPromos.AccountBalance(_handpay_balance);
          _won_egm = new MultiPromos.AccountBalance(_handpay_balance_egm);
        }

        if (InputHandpay.CancelHandpay)
        {
          if (!MultiPromos.Trx_HandpayInsertPlaySession(InputHandpay.HandpayType, _card_data.AccountId, InputHandpay.TerminalId,
                                              _terminal_type, _previous_hp_balance, _handpay_balance_egm, out PlaySessionId, Trx))
          {
            Log.Error("Trx_HandpayInsertPlaySession failed.");

            return false;
          }

          if (!MultiPromos.Trx_PlaySessionSetMeters(PlaySessionId, 
                                                    _previous_hp_balance, 
                                                    MultiPromos.AccountBalance.Zero, 
                                                    _won, 
                                                    MultiPromos.AccountBalance.Zero, 
                                                    0, 
                                                    Trx))
          {
            Log.Error("Trx_PlaySessionSetMeters Cancel failed.");

            return false;
          }
        }
        else
        {
          if (OperationCode != OperationCode.HANDPAY_AUTOMATICALLY_PAYMENT)
          {
          if (!MultiPromos.Trx_HandpayInsertPlaySession(InputHandpay.HandpayType, _card_data.AccountId, InputHandpay.TerminalId,
                                              _terminal_type, MultiPromos.AccountBalance.Zero, _handpay_balance_egm, out PlaySessionId, Trx))
          {
            Log.Error("Trx_HandpayInsertPlaySession failed.");

            return false;
          }
          }

          if (!MultiPromos.Trx_PlaySessionSetMeters(PlaySessionId, 
                                                    MultiPromos.AccountBalance.Zero, 
                                                    MultiPromos.AccountBalance.Zero, 
                                                    _won_egm, 
                                                    _handpay_balance_egm, 
                                                    0, 
                                                    Trx))
          {
            Log.Error("Trx_PlaySessionSetMeters Register failed.");

            return false;
          }
        }

        //
        // Update account balance
        //
        if (!(_is_tito && InputHandpay.CancelHandpay))
        {
          if (!MultiPromos.Trx_UpdateAccountBalance(_card_data.AccountId, _handpay_balance, out _fin_balance, Trx))
          {
            Log.Error("Trx_UpdateAccountBalance failed.");

            return false;
          }
        }

        // If IsModeTITO: Must update Initial Cash In
        if (_is_tito)
        {
          _is_jackpot = false;

          if (InputHandpay.HandpayType == HANDPAY_TYPE.JACKPOT
            || InputHandpay.HandpayType == HANDPAY_TYPE.MANUAL_JACKPOT
            || InputHandpay.HandpayType == HANDPAY_TYPE.SITE_JACKPOT)
          {
            _is_jackpot = true;
          }

          _sb = new StringBuilder();
          _sb.AppendLine("UPDATE   ACCOUNTS ");
          _sb.AppendLine("   SET   AC_INITIAL_CASH_IN = AC_INITIAL_CASH_IN + @pDevolution ");
          _sb.AppendLine(" WHERE   AC_ACCOUNT_ID      = @pAccountId");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd.Parameters.Add("@pDevolution", SqlDbType.Money).Value = WSI.Common.TITO.Utils.MaxDevolutionAmount(InputHandpay.HandpayAmount, _is_jackpot).SqlMoney;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _card_data.AccountId;

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Error("RegisterOrCancel: Update Initial CashIn. AccountId: " + _card_data.AccountId);

              return false;
            }
          }
        }

        if (!MultiPromos.Trx_SetActivity(_card_data.AccountId, Trx))
        {
          Log.Error("Trx_SetActivity failed.");

          return false;
        }

        //
        // New Card Movement ; New Cashier Movement 
        //

        _ini_balance = new MultiPromos.AccountBalance(_fin_balance);

        if (_handpay_balance.TotalBalance < 0)
        {
          _account_sub_amount = Math.Abs(_handpay_balance.TotalBalance);
          _account_add_amount = 0;
          _ini_balance -= _handpay_balance;

          _operation_code = OperationCode.HANDPAY_CANCELLATION;
          _amount = _account_sub_amount;
        }
        else
        {
          _account_sub_amount = 0;
          _account_add_amount = _handpay_balance.TotalBalance;
          _ini_balance -= _handpay_balance;

          _operation_code = OperationCode == OperationCode.HANDPAY_AUTOMATICALLY_PAYMENT ? OperationCode : OperationCode.HANDPAY;
          _amount = _account_add_amount;
        }

        _cashier_add_amount = _account_sub_amount;
        _cashier_sub_amount = _account_add_amount;

        if (InsertOperation || OperationId == 0)
        {
          // 27-MAR-2013 RBG Create operation id before handpays.
          if (!Operations.DB_InsertOperation(_operation_code,
                                             _card_data.AccountId,
                                             SessionInfo.CashierSessionId,
                                             0, // MbAccountId
                                             0, // promo id
                                             _amount, // currency amount
                                             0, // currency NR
                                             0, // Operation Data
                                             InputHandpay.AccountOperationReasonId,
                                             InputHandpay.AccountOperationComment,
                                             InputHandpay.AccountOperationCommentHandPay, // EOR 24-FEB-2016 Add Parameter Comments HandPays
                                             out OperationId, Trx))
          {
            Log.Error("DB_Handpays. Error calling to DB_InsertOperation. CardId: " + _card_data.AccountId);

            return false;
          }
        }

        _account_mov_table = new AccountMovementsTable(SessionInfo);

        if (!_account_mov_table.Add(OperationId, _card_data.AccountId, _movement_type, _ini_balance.TotalBalance, _account_sub_amount, _account_add_amount, _fin_balance.TotalBalance))
        {
          Log.Error("HandpayRegisterOrCancel: Error adding card movement. Operation: " + OperationId);

          return false;
        }
        if (!_account_mov_table.Save(Trx))
        {
          Log.Error("HandpayRegisterOrCancel: Error adding card movement. Operation: " + OperationId);

          return false;
        }

        _movement_id = _account_mov_table.LastSavedMovementId;

        //    
        // Update / Insert the HandPay record
        //

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_handpay.ToString(), Trx.Connection, Trx))
        {
          Int32 _status_paid;

          WSI.Common.TITO.HandPay.GetFloorDualCurrency(InputHandpay.HandpayAmt0, InputHandpay.TerminalId, out _sys_amt, out _egm_amt, out _egm_iso, Trx);

          _sql_cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = _movement_id;
          _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          //Update
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = InputHandpay.TerminalId;
          _sql_cmd.Parameters.Add("@pInsertHandpayAmount", SqlDbType.Money).Value = (Int32)(InputHandpay.CancelHandpay ? -1 : 1) * _sys_amt;
          _sql_cmd.Parameters.Add("@pUpdateHandpayAmount", SqlDbType.Money).Value = _egm_amt;
          _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = InputHandpay.HandpayDate;
          _sql_cmd.Parameters.Add("@pAmount1", SqlDbType.Money).Value = (Decimal)_sys_amt;
          _sql_cmd.Parameters.Add("@pCurren1", SqlDbType.NVarChar, 3).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "");


          //Cancel
          _sql_cmd.Parameters.Add("@pCancelMovementId", SqlDbType.BigInt).Value = InputHandpay.CancelMovementId;
          _sql_cmd.Parameters.Add("@pCancelPlaySessionId", SqlDbType.BigInt).Value = InputHandpay.CancelPlaySessionId;

          //Insert
          _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).Value = InputHandpay.TerminalName;
          _sql_cmd.Parameters.Add("@pTerminalProvider", SqlDbType.NVarChar, 50).Value = InputHandpay.TerminalProvider;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = InputHandpay.HandpayType;
          _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = InputHandpay.ProgressiveId == 0 ? DBNull.Value : (Object)InputHandpay.ProgressiveId;
          _sql_cmd.Parameters.Add("@pLevel", SqlDbType.Int).Value = InputHandpay.Level;

          _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.PENDING;

          _status_paid = (Int32)(OperationCode == OperationCode.HANDPAY_AUTOMATICALLY_PAYMENT ? HANDPAY_STATUS.AUTOMATICALLY_PAID : HANDPAY_STATUS.PAID);
          _sql_cmd.Parameters.Add("@pStatusPaid", SqlDbType.Int).Value = _status_paid;
          _sql_cmd.Parameters.Add("@pStatusVoided", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.VOIDED;

          _sql_cmd.Parameters.Add("@pMaskStatus", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.MASK_STATUS;
          _sql_cmd.Parameters.Add("@pMaskTax", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.MASK_TAX;

          _sql_cmd.Parameters.Add("@pTaxBaseAmount", SqlDbType.Money).Value = InputHandpay.TaxBaseAmount;
          _sql_cmd.Parameters.Add("@pTaxAmount", SqlDbType.Money).Value = InputHandpay.TaxAmount;
          _sql_cmd.Parameters.Add("@pTaxPercentage", SqlDbType.Decimal).Value = InputHandpay.TaxBasePct;

          _sql_cmd.Parameters.Add("@pAmount0", SqlDbType.Money).Value = (Decimal)_egm_amt;
          _sql_cmd.Parameters.Add("@pCurren0", SqlDbType.NVarChar, 3).Value = _egm_iso;

          //EOR 04-MAR-2016 Add Parameter Value @pOperationId
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;


          _new_hp_id_param = new SqlParameter("@pNewHpId", SqlDbType.BigInt);
          _new_hp_id_param.Direction = ParameterDirection.Output;
          _sql_cmd.Parameters.Add(_new_hp_id_param);

          if (StatusActivated(InputHandpay.Status, HANDPAY_STATUS.NO_TAX))
          {
            _sql_cmd.Parameters.Add("@pStatusTax", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.NO_TAX;
          }
          else if (StatusActivated(InputHandpay.Status, HANDPAY_STATUS.TAXED))
          {
            _sql_cmd.Parameters.Add("@pStatusTax", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.TAXED;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pStatusTax", SqlDbType.Int).Value = (Int32)0;
          }

          _reg_afected = _sql_cmd.ExecuteNonQuery();

          if (_reg_afected != 1)
          {
            int _current_hp_status;
            String _error_msg_hp_status;
            Boolean _is_error; // else is warning

            _error_msg_hp_status = "Update/Insert HandPay failed.";
            _is_error = true;
            
            if (!CheckHandpayStatus(InputHandpay, out _current_hp_status, Trx))
            {
              Log.Error("Handpays.RegisterOrCancel: CheckHandpayStatus");
              return false;
            }

            if (StatusActivated(_current_hp_status, HANDPAY_STATUS.PAID))
            {
              HandpayErrorMsg = (Int16)Handpays.HANDPAY_MSG.REGISTER_ERROR_ALREADY_PAID;
              _error_msg_hp_status = "Handpay already payed.";
              _is_error = false;
            }

            if (StatusActivated(_current_hp_status, HANDPAY_STATUS.VOIDED))
            {
              HandpayErrorMsg = (Int16)Handpays.HANDPAY_MSG.REGISTER_ERROR_VOIDED;
              _error_msg_hp_status = "Handpay voided.";
              _is_error = false;
            }

            if (_is_error)
            {
              Log.Error(_error_msg_hp_status);
            }
            else
            {
              Log.Warning(_error_msg_hp_status);
            }

            return false;
          }

          if (_query_string == HANDPAY_QUERY_TYPE.INSERT_PAY
             && _new_hp_id_param.Value != DBNull.Value)
          {
            InputHandpay.HandpayId = (Int64)_new_hp_id_param.Value;
          }
        }

        //// Cashier movement
        _cashier_mov_table = new CashierMovementsTable(SessionInfo);

        if (!_cashier_mov_table.Add(OperationId, _cashier_movement_type, _amount, _card_data.AccountId, _card_data.TrackData))
        {
          Log.Error("HandpayRegisterOrCancel: Error adding cashier movement. Operation: " + OperationId);

          return false;
        }

        if (InputHandpay.HandpayId > 0)
        {
          _cashier_mov_table.AddRelatedIdInLastMov(InputHandpay.HandpayId);
        }

        if (!_cashier_mov_table.Save(Trx))
        {
          Log.Error("HandpayRegisterOrCancel: Error saving cashier movement. Operation: " + OperationId);

          return false;
        }

        _cashier_mov_table = new CashierMovementsTable(SessionInfo);

        if (_is_tito && !InputHandpay.CancelHandpay)
        {
          // New movement handpay withholding tax
          if (!_cashier_mov_table.Add(OperationId, CASHIER_MOVEMENT.HANDPAY_WITHHOLDING, _amount, _card_data.AccountId, _card_data.TrackData, String.Empty, String.Empty, 0, 0, 0, InputHandpay.TaxBaseAmount))
          {
            Log.Error("HandpayRegisterOrCancel: Error adding cashier movement. Operation: " + OperationId);

            return false;
          }
          _cashier_mov_table.AddSubAmountInLastMov(InputHandpay.TaxAmount);
        }

        if (InputHandpay.HandpayId > 0)
        {
          _cashier_mov_table.AddRelatedIdInLastMov(InputHandpay.HandpayId);
        }

        if (!_cashier_mov_table.Save(Trx))
        {
          Log.Error("HandpayRegisterOrCancel: Error saving cashier movement. Operation: " + OperationId);

          return false;
        }

        // 
        // Generate handpay voucher 
        //

        _voucher = null;
        if (InputHandpay.CancelHandpay)
        {
          //    - Handpay Cancellation voucher
          if (!_is_tito)
          {
            VoucherCardHandpayCancellation.VoucherCardHandpayCancellationInputParams _input_params;

            _input_params = new VoucherCardHandpayCancellation.VoucherCardHandpayCancellationInputParams();

            _input_params.VoucherAccountInfo = _card_data.VoucherAccountInfo();
            _input_params.TerminalName = InputHandpay.TerminalName;
            _input_params.TerminalProvider = InputHandpay.TerminalProvider;
            _input_params.Type = InputHandpay.HandpayType;
            _input_params.Level = InputHandpay.Level;
            _input_params.InitialAmount = _ini_balance.TotalBalance;
            _input_params.FinalAmount = _fin_balance.TotalBalance;
            _input_params.HandpayAmount = InputHandpay.HandpayAmount;

            _hp_cancel_voucher = new VoucherCardHandpayCancellation(_input_params, PrintMode.Print, OperationCode.HANDPAY_CANCELLATION, Trx);
            _voucher = _hp_cancel_voucher;
          }
        }
        else
        {
          Currency _pay_amount;
          Currency _total_amount;

          _pay_amount = InputHandpay.HandpayAmount;
          _total_amount = _pay_amount;

          if (_is_tito)
          {
            Handpays.HPAmountsCalculated _calculated;
            if (!CalculateCashAmountHandpay(InputHandpay.Status,
                                            InputHandpay.HandpayAmount,
                                            InputHandpay.TaxBaseAmount,
                                            InputHandpay.HandpayType,
                                            InputHandpay.CardData,
                                            InputHandpay.IsApplyTaxes,
                                            false,
                                            out  _calculated))
            {
              return false;
            }

            _pay_amount = _calculated.CashRedeem.TotalPaid;
            _total_amount = _calculated.CashRedeem.TotalRedeemed;
          }

          VoucherCardHandpay.VoucherCardHandpayInputParams _input_params;
          _input_params = new VoucherCardHandpay.VoucherCardHandpayInputParams();

          _input_params.VoucherAccountInfo = _card_data.VoucherAccountInfo();
          _input_params.TerminalName = InputHandpay.TerminalName;
          _input_params.TerminalProvider = InputHandpay.TerminalProvider;
          _input_params.Type = InputHandpay.HandpayType;
          _input_params.TypeName = HandpayTypeString(InputHandpay.HandpayType, InputHandpay.Level);
          _input_params.CardBalance = _ini_balance.TotalBalance;
          _input_params.Amount = _pay_amount;
          _input_params.SessionPlayedAmount = InputHandpay.SessionPlayedAmount;
          _input_params.DenominacionStr = InputHandpay.DenominationStr;
          _input_params.DenominacionValue = InputHandpay.DenominationValue;
          _input_params.Mode = PrintMode.Print;
          _input_params.ValidationNumber = Ticket;
          _input_params.InitialAmount = _total_amount;

          _input_params.TaxAmount = InputHandpay.TaxAmount;
          _input_params.TaxBaseAmount = InputHandpay.TaxBaseAmount;
          _input_params.TaxPct = InputHandpay.TaxBasePct;
          if (InputHandpay.IsApplyTaxes)
          {
            _input_params.Deductions = InputHandpay.TaxAmount;
          }
          _hp_pay_voucher = new VoucherCardHandpay(_input_params, OperationCode, (OperationCode == OperationCode.HANDPAY_AUTOMATICALLY_PAYMENT), Trx);

          _voucher = _hp_pay_voucher;
        }

        //// Save voucher
        if (_voucher != null && !_voucher.Save(OperationId, Trx))
        {
          Log.Error("VoucherCardHandpay failed.");

          return false;
        }

        Decimal _tax_base_amount;
        _tax_base_amount = InputHandpay.TaxBaseAmount;

        if (GeneralParam.GetBoolean("Progressives", "Enabled")
            && (InputHandpay.HandpayType == HANDPAY_TYPE.JACKPOT || InputHandpay.HandpayType == HANDPAY_TYPE.MANUAL_JACKPOT)
            && InputHandpay.Level >= 0x01
            && InputHandpay.Level <= 0x20
            && InputHandpay.ProgressiveId > 0
            && InputHandpay.HandpayType != HANDPAY_TYPE.MANUAL_JACKPOT)
        {

          _tax_base_amount = InputHandpay.HandpayAmount;

          if (_tax_base_amount > 0)
        {
          if (!Progressives.ProgressiveJackpotPaid(InputHandpay.ProgressiveId,
                                                    InputHandpay.Level,
                                                    _tax_base_amount,
                                                    InputHandpay.TerminalId,
                                                    InputHandpay.HandpayId,
                                                    InputHandpay.CancelHandpay,
                                                    Trx))
          {
            return false;
          }
        }
        }

        InputHandpay.Voucher = _voucher;

        if (UpdatePlaySessionId != null)
        {
          // If handpay succesfully registered, update the related play session  
          if (!Handpays.UpdatePlaySession((Int64)UpdatePlaySessionId, InputHandpay.HandpayAmount, Trx))
          {
            Log.Message("*** WCP_MsgEGMHandpays: Error occurred updating the related PlaySession the Handpay!  PlaySessionId: " + UpdatePlaySessionId);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // RegisterOrCancel


    public static Boolean CheckHandpayStatus(HandpayRegisterOrCancelParameters Handpays, out int Status, SqlTransaction Sqltrx)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      Status = -1;

      _sb.AppendLine(" SELECT   HP_STATUS ");
      _sb.AppendLine("   FROM   HANDPAYS ");
      _sb.AppendLine("  WHERE   HP_TERMINAL_ID     = @pTerminalId          ");
      _sb.AppendLine("    AND   HP_DATETIME        = @pDateTime            ");
      _sb.AppendLine("    AND   isNull(HP_AMT0, HP_AMOUNT) = isNull(@pAmount0, @pUpdateHandpayAmount) ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Sqltrx.Connection, Sqltrx))
      {
        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Handpays.TerminalId;
        _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = Handpays.HandpayDate;
        _sql_cmd.Parameters.Add("@pAmount0", SqlDbType.Money).Value = Handpays.HandpayAmt0;
        _sql_cmd.Parameters.Add("@pUpdateHandpayAmount", SqlDbType.Money).Value = Handpays.HandpayAmount;

        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (!_reader.Read())
          {
            return false;
          }
          Status = (_reader.IsDBNull(0) ? -1 : _reader.GetInt32(0));
        }
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Retrieves the card price settings from the general params table
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - TimePeriod
    //          - CancelTimePeriod
    //
    // RETURNS :
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES :
    public static void GetParameters(out int TimePeriod, out int CancelTimePeriod)
    {
      String _str_value;
      int _int_value;

      TimePeriod = 60;      // Default value = 60 minutes
      CancelTimePeriod = 5;       // Default value = 5 minutes

      try
      {
        TimePeriod = GeneralParam.GetInt32("Cashier.HandPays", "TimePeriod", 60, 5, 2500);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      try
      {
        _str_value = GeneralParam.Value("Cashier.HandPays", "TimeToCancel");
        if (!String.IsNullOrEmpty(_str_value))
        {
          _str_value = Format.DBNumberToLocalNumber(_str_value);
          if (int.TryParse(_str_value, out _int_value))
          {
            CancelTimePeriod = _int_value;
          }
        }
      } // try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // GetParameters

    //------------------------------------------------------------------------------
    // PURPOSE : Get handpay progressive data.
    //
    //  PARAMS :
    //      - INPUT :
    //            - AccountId
    //            - TerminalId
    //            - HandpayDate
    //
    //      - OUTPUT :
    //            - Currency PlayedAmount
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    public static Boolean GetHandpayProgressiveInfo(Int32 TerminalId
                                                  , DateTime HandpayDate
                                                  , Decimal HandpayAmount
                                                  , out Int32 Level
                                                  , out Int64 ProgressiveId
                                                  , out Int64 HandpayId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetHandpayProgressiveInfo(TerminalId, HandpayDate, HandpayAmount, _db_trx.SqlTransaction, out Level, out ProgressiveId, out HandpayId);
      }
    }
    public static Boolean GetHandpayProgressiveInfo(Int32 TerminalId
                                                  , DateTime HandpayDate
                                                  , Decimal HandpayAmount
                                                  , SqlTransaction Trx
                                                  , out Int32 Level
                                                  , out Int64 ProgressiveId
                                                  , out Int64 HandpayId)
    {
      StringBuilder _sb;

      Level = 0;
      ProgressiveId = 0;
      HandpayId = 0;

      try
      {
        // Get the pair (play_session_id, played_amount) of all the candidated play_sessions to produce the Handpay.
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT TOP (1) HP_ID                          ");
        _sb.AppendLine("        , ISNULL (HP_LEVEL, 0)                 ");
        _sb.AppendLine("        , ISNULL (HP_PROGRESSIVE_ID, 0)        ");
        _sb.AppendLine("   FROM   HANDPAYS                             ");
        _sb.AppendLine("  WHERE   HP_TERMINAL_ID     = @pTerminalId    ");
        _sb.AppendLine("    AND   HP_DATETIME        = @pDateTime      ");
        _sb.AppendLine("    AND   HP_AMOUNT          = @pAmount        ");
        _sb.AppendLine(" ORDER BY HP_DATETIME DESC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = HandpayDate;
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = HandpayAmount;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Message("Error on Get Progressive Info from handpay: TerminalId " + TerminalId.ToString() + ", Amount: " + HandpayAmount.ToString());

              return false;
            }

            HandpayId = _reader.GetInt64(0);
            Level = _reader.GetInt32(1);
            ProgressiveId = _reader.GetInt64(2);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetHandpayProgressiveInfo

    //------------------------------------------------------------------------------
    // PURPOSE : Get the played amount for the play session that produces the Handpay (i.e. the Jackpot).
    //
    //  PARAMS :
    //      - INPUT :
    //            - HandPay HandpayData
    //
    //      - OUTPUT :
    //            - Currency PlayedAmount
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    public static Boolean GetHandpaySessionPlayedAmount(Int64 AccountId
                                                      , Int32 TerminalId
                                                      , DateTime HandpayDate
                                                      , out Currency PlayedAmount
                                                      , out String DenominationStr
                                                      , out String DenominationValue)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetHandpaySessionPlayedAmount(AccountId, TerminalId, HandpayDate, _db_trx.SqlTransaction, out PlayedAmount, out DenominationStr, out DenominationValue);
      }
    }

    public static Boolean GetHandpaySessionPlayedAmount(Int64 AccountId
                                                      , Int32 TerminalId
                                                      , DateTime HandpayDate
                                                      , SqlTransaction Trx
                                                      , out Currency PlayedAmount
                                                      , out String DenominationStr
                                                      , out String DenominationValue)
    {
      StringBuilder _sb;
      Int32 _handpay_time_period;
      Int32 _handpay_cancel_time_period;
      DataTable _dt_sessions;
      Currency _cu_denomination;

      PlayedAmount = 0;
      DenominationStr = "";
      DenominationValue = "";

      _handpay_time_period = 0;
      _handpay_cancel_time_period = 0;

      try
      {
        // Get the Handpay settings from the General Parameters table
        Handpays.GetParameters(out _handpay_time_period, out _handpay_cancel_time_period);

        _dt_sessions = new DataTable();

        // Get the pair (play_session_id, played_amount) of all the candidated play_sessions to produce the Handpay.
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   PS_PLAY_SESSION_ID ");
        _sb.AppendLine("        , PS_PLAYED_AMOUNT ");
        _sb.AppendLine("        , TE_DENOMINATION ");
        _sb.AppendLine("        , TE_MULTI_DENOMINATION ");
        _sb.AppendLine("   FROM   PLAY_SESSIONS, TERMINALS ");
        _sb.AppendLine("  WHERE   PS_TERMINAL_ID = TE_TERMINAL_ID ");
        if (!TITO.Utils.IsTitoMode())
        {
          _sb.AppendLine("    AND   PS_ACCOUNT_ID  = @AccountId ");
        }
        _sb.AppendLine("    AND   PS_TERMINAL_ID = @HpTerminalId ");
        _sb.AppendLine("    AND   DATEADD (MINUTE, @TimePeriod, @HpDatetime) >= GETDATE() ");
        _sb.AppendLine("    AND   PS_STARTED    <= @HpDatetime ");
        _sb.AppendLine("    AND ((PS_FINISHED IS NOT NULL AND ABS(DATEDIFF (MINUTE, PS_FINISHED, @HpDatetime)) <= @TimePeriod ");
        _sb.AppendLine("          AND PS_FINISHED - PS_STARTED > 0) ");
        _sb.AppendLine("           OR (PS_FINISHED IS NULL     AND ABS(DATEDIFF (MINUTE, GETDATE(), @HpDatetime)) <= @TimePeriod)) ");
        _sb.AppendLine(" ORDER BY PS_PLAY_SESSION_ID DESC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@HpTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@TimePeriod", SqlDbType.Int).Value = _handpay_time_period;
          if (HandpayDate != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@HpDatetime", SqlDbType.DateTime).Value = HandpayDate;
          }
          else
          {
            _sql_cmd.Parameters.Add("@HpDatetime", SqlDbType.DateTime).Value = DBNull.Value;
          }

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(WGDB.ParametersToVariables(_sql_cmd)))
          {
            _sql_da.Fill(_dt_sessions);
          }
        }

        // Must exist at least one session that produces the Handpay.
        if (_dt_sessions.Rows.Count == 0)
        {
          return false;
        }

        // Get the played_amount of the latest play_session related to the Handpay.
        foreach (DataRow _row in _dt_sessions.Rows)
        {
          if (_row.IsNull(2))
          {
            DenominationStr = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_MULTIDENOMINATION");

            if (!_row.IsNull(3))
            {
              DenominationValue = _row[3].ToString();
            }
          }
          else
          {
            _cu_denomination = (Decimal)_row[2];

            DenominationStr = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION");
            DenominationValue = _cu_denomination.ToString();
          }

          PlayedAmount = (Decimal)_row[1];
          if (PlayedAmount > 0)
          {
            break;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // GetHandpaySessionPlayedAmount

    public static Int32 ChangeStatus(Int32 CurrentStatus, HANDPAY_STATUS NewStatus)
    {
      if (((Int32)NewStatus & (Int32)HANDPAY_STATUS.MASK_AUTHORIZATION) == (Int32)NewStatus)
      {
        return (CurrentStatus & ~(Int32)HANDPAY_STATUS.MASK_AUTHORIZATION) | ((Int32)NewStatus);
      }

      if (((Int32)NewStatus & (Int32)HANDPAY_STATUS.MASK_STATUS) == (Int32)NewStatus)
      {
        return (CurrentStatus & ~(Int32)HANDPAY_STATUS.MASK_STATUS) | ((Int32)NewStatus);
      }

      if (((Int32)NewStatus & (Int32)HANDPAY_STATUS.MASK_TAX) == (Int32)NewStatus)
      {
        return (CurrentStatus & ~(Int32)HANDPAY_STATUS.MASK_TAX) | ((Int32)NewStatus);
      }

      return 0;
    }

    public static Boolean StatusActivated(Int32 HpStatus, HANDPAY_STATUS Status)
    {

      switch (Status)
      {
        case HANDPAY_STATUS.PENDING:
          return (HpStatus & (Int32)HANDPAY_STATUS.MASK_STATUS) == (Int32)Status;
        case HANDPAY_STATUS.PAID:
          return (HpStatus & (Int32)HANDPAY_STATUS.MASK_STATUS) == (Int32)Status;
        case HANDPAY_STATUS.EXPIRED:
          return (HpStatus & (Int32)HANDPAY_STATUS.MASK_STATUS) == (Int32)Status;
        case HANDPAY_STATUS.VOIDED:
          return (HpStatus & (Int32)HANDPAY_STATUS.MASK_STATUS) == (Int32)Status;
        case HANDPAY_STATUS.AUTOMATICALLY_PAID:
          return (HpStatus & (Int32)HANDPAY_STATUS.MASK_STATUS) == (Int32)Status;
        case HANDPAY_STATUS.AUTHORIZED:
          return (HpStatus & (Int32)HANDPAY_STATUS.MASK_AUTHORIZATION) == (Int32)Status;
        case HANDPAY_STATUS.TAXED:
          return (HpStatus & (Int32)HANDPAY_STATUS.MASK_TAX) == (Int32)Status;
        case HANDPAY_STATUS.NO_TAX:
          return (HpStatus & (Int32)HANDPAY_STATUS.MASK_TAX) == (Int32)Status;
        default:
          return false;
      }

    }

    public static Boolean IsAuthorizable(Int32 Status, Decimal Amount)
    {

      if (!StatusActivated(Status, HANDPAY_STATUS.PENDING))
      {
        return false;
      }

      // Currently is authorized
      if (StatusActivated(Status, HANDPAY_STATUS.AUTHORIZED))
      {
        return false;
      }

      // With taxed or no tax
      if (StatusActivated(Status, HANDPAY_STATUS.TAXED) || StatusActivated(Status, HANDPAY_STATUS.NO_TAX))
      {
        return false;
      }

      return true;
    }

    public static Boolean IsPayable(Int32 Status, Decimal Amount)
    {
      Decimal _limit;

      if (StatusActivated(Status, HANDPAY_STATUS.PAID))
      {
        return false;
      }

      if (TITO.Utils.IsTitoMode())
      {
        _limit = GeneralParam.GetDecimal("Cashier.HandPays", "LimitToAllowPayment", 0);
        if (_limit > 0 && Amount > _limit)
        {
          if (!StatusActivated(Status, HANDPAY_STATUS.AUTHORIZED))
          {
            return false;
          }
        }
      }
      return true;
    }

    public static Boolean IsVoidable(Int32 Status)
    {
      if (StatusActivated(Status, HANDPAY_STATUS.PAID))
      {
        return false;
      }

      if (StatusActivated(Status, HANDPAY_STATUS.VOIDED))
      {
        return false;
      }

      return true;
    }

    public static Boolean ChangeToVoidedOrPendingStatus(Int64 HpId,
                                                        Boolean IsVoided,
                                                        CardData CardData,
                                                        CashierMovementsTable CashierMovements,
                                                        SqlTransaction Trx)
    {
      StringBuilder _sb;
      HANDPAY_STATUS _status_to_change;
      Int32 _new_status;
      Int32 _old_status;
      CASHIER_MOVEMENT _cm;

      try
      {
        if (IsVoided)
        {
          _status_to_change = HANDPAY_STATUS.PENDING;
        }
        else
        {
          _status_to_change = HANDPAY_STATUS.VOIDED;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("DECLARE @result TABLE (i_status INT, d_status INT); ");
        _sb.AppendLine(" UPDATE   HANDPAYS");
        _sb.AppendLine("    SET   HP_STATUS = HP_STATUS & ~@pMaskStatus + @pNewStatus");
        _sb.AppendLine("        , HP_STATUS_CHANGED = GETDATE()");
        _sb.AppendLine(" OUTPUT   INSERTED.hp_status ");
        _sb.AppendLine("        , DELETED.hp_status ");
        _sb.AppendLine("   INTO   @result ");
        _sb.AppendLine("  WHERE   HP_ID = @pHpId");
        _sb.AppendLine(" SELECT i_status, d_status FROM @result; ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pMaskStatus", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.MASK_STATUS;
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = (Int32)_status_to_change;
          _cmd.Parameters.Add("@pHpId", SqlDbType.BigInt).Value = HpId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Message("Error on ChangeToVoidedOrPendingStatus: HpId " + HpId.ToString());

              return false;
            }

            _new_status = _reader.GetInt32(0);
            _old_status = _reader.GetInt32(1);
          }


          if (IsVoided)
          {
            if (StatusActivated(_old_status, HANDPAY_STATUS.PAID))
            {
              Log.Message("ChangeToVoidedOrPendingStatus: Status not allowed -> HpId " + HpId.ToString() + " Status:" + _old_status);

              return false;
            }

            if (StatusActivated(_old_status, HANDPAY_STATUS.PENDING))
            {
              Log.Message("ChangeToVoidedOrPendingStatus: Status not allowed -> HpId " + HpId.ToString() + " Status:" + _old_status);

              return false;
            }

            _cm = CASHIER_MOVEMENT.HANDPAY_VOIDED_UNDONE;
          }
          else
          {
            if (!IsVoidable(_old_status))
            {
              Log.Message("ChangeToVoidedOrPendingStatus: Status not allowed -> HpId " + HpId.ToString() + " Status:" + _old_status);

              return false;
            }

            _cm = CASHIER_MOVEMENT.HANDPAY_VOIDED;
          }

          if (!CashierMovements.Add(0, _cm, 0, CardData.AccountId, CardData.TrackData))
          {
            return false;
          }
          CashierMovements.AddRelatedIdInLastMov(HpId);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public class HandpayAuthorizedInputParams
    {
      public Int64 HpId;
      public HANDPAY_STATUS MaskTaxes;
      public HANDPAY_TYPE HpType;
      public Int32 TerminalId;
      public String TerminalName;
      public String TerminalProvider;
      public String TypeName;
      public Decimal Amount;
      public Int64 ProgressiveId;
      public Int32 Level;
      public Decimal TaxPct;
      public Decimal TaxAmount;
      public Decimal TaxBaseAmount;
      public Int64 OperationId;
    }

    public static Boolean ChangeToAuthorizedStatus(HandpayAuthorizedInputParams InputParams,
                                                   CashierMovementsTable CashierMovements,
                                                   CardData CardData,
                                                   Boolean IsApplyTaxes,
                                                   Boolean IsParamEnabled,
                                                   out VoucherHandpayAuthorized Voucher,
                                                   out Int64 HpIdUpdated,
                                                   SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _new_status;
      Int32 _old_status;
      Decimal _amount;
      Barcode _voucher_number;
      Boolean _upd_progressive;
      Boolean _hp_to_insert;
      SqlParameter _new_hp_id_param;

      String _egm_iso;
      Decimal _egm_amt;
      Decimal _sys_amt;

      Voucher = null;
      _sb = new StringBuilder();
      _upd_progressive = false;
      _hp_to_insert = false;
      _amount = 0;
      HpIdUpdated = 0;

      try
      {
        if (!(InputParams.MaskTaxes == 0 || InputParams.MaskTaxes == HANDPAY_STATUS.TAXED || InputParams.MaskTaxes == HANDPAY_STATUS.NO_TAX))
        {
          Log.Message("ChangeToAuthorizedStatus: MaskTaxes " + InputParams.MaskTaxes.ToString() + ", Hp Id " + InputParams.HpId.ToString());

          return false;
        }

        _sb.Length = 0;
        _sb.AppendLine("DECLARE @result TABLE (i_status INT, d_status INT, i_amount MONEY); ");
        _sb.AppendLine(" UPDATE   HANDPAYS ");
        _sb.AppendLine("    SET   HP_STATUS = HP_STATUS + @pStatusAdded ");
        _sb.AppendLine("        , HP_STATUS_CHANGED = GETDATE() ");
        _sb.AppendLine("        , HP_TAX_BASE_AMOUNT = @pTaxBaseAmount ");
        _sb.AppendLine("        , HP_TAX_AMOUNT = @pTaxAmount ");
        _sb.AppendLine("        , HP_TAX_PCT = @pTaxPct ");
        _sb.AppendLine(" OUTPUT   INSERTED.hp_status ");
        _sb.AppendLine("        , DELETED.hp_status ");
        _sb.AppendLine("        , INSERTED.hp_amount INTO @result ");
        _sb.AppendLine(" WHERE    HP_ID = @pHpId; ");
        _sb.AppendLine(" SELECT i_status, d_status, i_amount FROM @result; ");

        switch (InputParams.HpType)
        {
          case HANDPAY_TYPE.CANCELLED_CREDITS:
          case HANDPAY_TYPE.JACKPOT:
          case HANDPAY_TYPE.ORPHAN_CREDITS:
          case HANDPAY_TYPE.SITE_JACKPOT:
            break;

          case HANDPAY_TYPE.SPECIAL_PROGRESSIVE:
          case HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS:
          case HANDPAY_TYPE.MANUAL_JACKPOT:
          case HANDPAY_TYPE.MANUAL_OTHERS:

            if (InputParams.HpId == 0)
            {
              _hp_to_insert = true;
              _upd_progressive = (InputParams.HpType == HANDPAY_TYPE.MANUAL_JACKPOT && InputParams.Level > 0);

              _sb.Length = 0;
              _sb.AppendLine(" INSERT INTO   HANDPAYS ");
              _sb.AppendLine("             ( HP_TERMINAL_ID        ");
              _sb.AppendLine("             , HP_GAME_BASE_NAME     ");
              _sb.AppendLine("             , HP_DATETIME           ");
              _sb.AppendLine("             , HP_PREVIOUS_METERS    ");
              _sb.AppendLine("             , HP_AMOUNT             ");
              _sb.AppendLine("             , HP_TE_NAME            ");
              _sb.AppendLine("             , HP_TE_PROVIDER_ID     ");
              _sb.AppendLine("             , HP_MOVEMENT_ID        ");
              _sb.AppendLine("             , HP_TYPE               ");
              _sb.AppendLine("             , HP_STATUS             ");
              if (_upd_progressive)
              {
                _sb.AppendLine("             , HP_PROGRESSIVE_ID ");
                _sb.AppendLine("             , HP_LEVEL ");
              }
              _sb.AppendLine("             , HP_PLAY_SESSION_ID    ");
              _sb.AppendLine("             , HP_STATUS_CHANGED     ");
              _sb.AppendLine("             , HP_TAX_BASE_AMOUNT    ");
              _sb.AppendLine("             , HP_TAX_AMOUNT         ");

              _sb.AppendLine("             , HP_OPERATION_ID       ");

              _sb.AppendLine("             , HP_TAX_PCT            ");
              _sb.AppendLine("             , HP_AMT0               ");
              _sb.AppendLine("             , HP_CUR0)              ");
              _sb.AppendLine("      VALUES                         ");
              _sb.AppendLine("             ( @pTerminalId          ");
              _sb.AppendLine("             , NULL                  ");
              _sb.AppendLine("             , GETDATE()             ");
              _sb.AppendLine("             , NULL                  ");
              _sb.AppendLine("             , @pInsertHandpayAmount ");
              _sb.AppendLine("             , @pTerminalName        ");
              _sb.AppendLine("             , @pTerminalProvider    ");
              _sb.AppendLine("             , @pMovementId          ");
              _sb.AppendLine("             , @pType ");
              _sb.AppendLine("             , @pStatusPendingAuthorized ");
              if (_upd_progressive)
              {
                _sb.AppendLine("             , @pProgressiveId ");
                _sb.AppendLine("             , @pLevel ");
              }
              _sb.AppendLine("             , @pPlaySessionId    ");
              _sb.AppendLine("             , GETDATE()          ");
              _sb.AppendLine("             , @pTaxBaseAmount    ");
              _sb.AppendLine("             , @pTaxAmount        ");

              _sb.AppendLine("             , @pOperationId      ");

              _sb.AppendLine("             , @pTaxPct           ");
              _sb.AppendLine("             , @pAmount0          ");
              _sb.AppendLine("             , @pCurren0 )        ");
              _sb.AppendLine(" SET @pNewHPId = SCOPE_IDENTITY() ");
            }

            break;

          case HANDPAY_TYPE.UNKNOWN:
          default:

            return false;

          // Unreachable code
          //break;
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          TITO.HandPay.GetFloorDualCurrency(InputParams.Amount, InputParams.TerminalId, out _sys_amt, out _egm_amt, out _egm_iso, Trx);

          _cmd.Parameters.Add("@pHpId", SqlDbType.BigInt).Value = InputParams.HpId;
          if (Tax.EnableTitoTaxWaiver(true))
          {
            _cmd.Parameters.Add("@pStatusAdded", SqlDbType.Int).Value = InputParams.TaxAmount > 0 ? (Int32)HANDPAY_STATUS.AUTHORIZED + (Int32)HANDPAY_STATUS.TAXED : (Int32)HANDPAY_STATUS.AUTHORIZED + (Int32)HANDPAY_STATUS.NO_TAX;
          }
          else
          {
            _cmd.Parameters.Add("@pStatusAdded", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.AUTHORIZED + (Int32)InputParams.MaskTaxes;
          }

          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = InputParams.TerminalId;
          _cmd.Parameters.Add("@pInsertHandpayAmount", SqlDbType.Decimal).Value = _sys_amt;
          _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).Value = InputParams.TerminalName;
          _cmd.Parameters.Add("@pTerminalProvider", SqlDbType.NVarChar, 50).Value = InputParams.TerminalProvider;
          _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = DBNull.Value;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = InputParams.HpType;

          if (Tax.EnableTitoTaxWaiver(true))
          {
            _cmd.Parameters.Add("@pStatusPendingAuthorized", SqlDbType.Int).Value = InputParams.TaxAmount > 0 ? (Int32)HANDPAY_STATUS.AUTHORIZED + (Int32)HANDPAY_STATUS.TAXED : (Int32)HANDPAY_STATUS.AUTHORIZED + (Int32)HANDPAY_STATUS.NO_TAX;
          }
          else
          {
            _cmd.Parameters.Add("@pStatusPendingAuthorized", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.AUTHORIZED + (Int32)InputParams.MaskTaxes;
          }

          if (_upd_progressive)
          {
            _cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = InputParams.ProgressiveId == 0 ? DBNull.Value : (Object)InputParams.ProgressiveId;
            _cmd.Parameters.Add("@pLevel", SqlDbType.Int).Value = InputParams.Level;
          }

          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = DBNull.Value;

          _cmd.Parameters.Add("@pTaxBaseAmount", SqlDbType.Money).Value = InputParams.TaxBaseAmount;
          _cmd.Parameters.Add("@pTaxAmount", SqlDbType.Money).Value = InputParams.TaxAmount;


          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = InputParams.OperationId;

          _cmd.Parameters.Add("@pTaxPct", SqlDbType.Decimal).Value = InputParams.TaxPct;
          _cmd.Parameters.Add("@pAmount0", SqlDbType.Money).Value = (Decimal)_egm_amt;
          _cmd.Parameters.Add("@pCurren0", SqlDbType.NVarChar, 3).Value = _egm_iso;

          _new_hp_id_param = new SqlParameter("@pNewHPId", SqlDbType.BigInt);
          _new_hp_id_param.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_new_hp_id_param);

          if (_hp_to_insert)
          {
            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }

            if (Tax.EnableTitoTaxWaiver(true))
            {
              _new_status = InputParams.TaxAmount > 0 ? (Int32)HANDPAY_STATUS.AUTHORIZED + (Int32)HANDPAY_STATUS.TAXED : (Int32)HANDPAY_STATUS.AUTHORIZED + (Int32)HANDPAY_STATUS.NO_TAX;
            }
            else
            {
              _new_status = (Int32)HANDPAY_STATUS.AUTHORIZED + (Int32)InputParams.MaskTaxes;
            }

            _old_status = 0;
            _amount = InputParams.Amount;

            HpIdUpdated = (Int64)_new_hp_id_param.Value;
          }
          else
          {
            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                Log.Message("Error on ChangeToAuthorizedStatus: HpId " + InputParams.HpId.ToString());

                return false;
              }

              _new_status = _reader.GetInt32(0);
              _old_status = _reader.GetInt32(1);
              _amount = _reader.GetDecimal(2);
              HpIdUpdated = InputParams.HpId;
            }
          }

          if (!IsAuthorizable(_old_status, _amount))
          {
            Log.Message("ChangeToAuthorizedStatus: Inccorect old status " + _old_status.ToString() + " Hp Id " + InputParams.HpId.ToString());

            return false;
          }

          if (!CashierMovements.Add(0, CASHIER_MOVEMENT.HANDPAY_AUTHORIZED, _amount, CardData.AccountId, CardData.TrackData))
          {
            return false;
          }

          CashierMovements.AddRelatedIdInLastMov(InputParams.HpId);

          //// New movement handpay withholding tax
          //if (!CashierMovements.Add(0, CASHIER_MOVEMENT.HANDPAY_WITHHOLDING, _amount, CardData.AccountId, CardData.TrackData, String.Empty, String.Empty, 0, 0, 0, InputParams.TaxBaseAmount))
          //{
          //  return false;
          //}

          CashierMovements.AddSubAmountInLastMov(InputParams.TaxAmount);

          if (InputParams.HpId > 0)
          {
            CashierMovements.AddRelatedIdInLastMov(InputParams.HpId);
          }

          Handpays.HPAmountsCalculated _calculated;
          if (!CalculateCashAmountHandpay(_new_status,
                                          _amount,
                                          InputParams.TaxBaseAmount,
                                          InputParams.HpType,
                                          CardData,
                                          IsApplyTaxes,
                                          IsParamEnabled,
                                          out _calculated))
          {
            return false;
          }

          _voucher_number = Barcode.Create(BarcodeType.Handpay, HpIdUpdated);

          VoucherHandpayAuthorized.VoucherHandpayAuthorizedInputParams _param;

          _param = new VoucherHandpayAuthorized.VoucherHandpayAuthorizedInputParams();

          _param.TerminalName = InputParams.TerminalName;
          _param.TerminalProvider = InputParams.TerminalProvider;
          _param.TypeName = InputParams.TypeName;
          _param.TotalRedeemed = _calculated.CashRedeem.TotalRedeemed;
          _param.Taxes = _calculated.CashRedeem.TotalTaxes;
          _param.TotalToPay = _calculated.CashRedeem.TotalPaid;
          _param.Mode = PrintMode.Print;
          _param.HandpayNumber = _voucher_number.ExternalCode;
          _param.TaxBaseAmount = InputParams.TaxBaseAmount;
          _param.TaxAmount = InputParams.TaxAmount;
          _param.TaxPct = InputParams.TaxPct;

          Voucher = new VoucherHandpayAuthorized(_param, OperationCode.HANDPAY_VALIDATION, Trx);

          Voucher.Save(InputParams.OperationId, Trx);

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean ResetAuthorizedStatus(Int64 HandPayId, CardData Card, CashierMovementsTable CashierMovements, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _new_status;

      try
      {
        _new_status = 0;

        _sb = new StringBuilder();
        _sb.AppendLine(" DECLARE @result TABLE(i_status INT); ");
        _sb.AppendLine(" UPDATE   HANDPAYS ");
        _sb.AppendLine("    SET   HP_STATUS = @pStatusPending ");
        _sb.AppendLine("        , HP_STATUS_CHANGED = GETDATE() ");
        _sb.AppendLine("        , HP_TAX_BASE_AMOUNT = @pTaxBaseAmount ");
        _sb.AppendLine("        , HP_TAX_AMOUNT = @pTaxAmount ");
        _sb.AppendLine("        , HP_TAX_PCT = @pTaxPct ");
        _sb.AppendLine(" OUTPUT   INSERTED.HP_STATUS INTO @result ");
        _sb.AppendLine(" WHERE    HP_ID = @pHandPayId ");
        _sb.AppendLine("   AND    HP_STATUS & cast(@pMaskAutho as INT) = @pStatusAuthorized; ");
        _sb.AppendLine(" SELECT i_status FROM @result;");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pHandPayId", SqlDbType.BigInt).Value = HandPayId;
          _cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.PENDING;
          _cmd.Parameters.Add("@pStatusAuthorized", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.AUTHORIZED;
          _cmd.Parameters.Add("@pMaskAutho", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.MASK_AUTHORIZATION;
          _cmd.Parameters.Add("@pMaskStatus", SqlDbType.Int).Value = (Int32)HANDPAY_STATUS.MASK_STATUS;

          _cmd.Parameters.Add("@pTaxBaseAmount", SqlDbType.Money).Value = DBNull.Value;
          _cmd.Parameters.Add("@pTaxAmount", SqlDbType.Money).Value = DBNull.Value;
          _cmd.Parameters.Add("@pTaxPct", SqlDbType.Decimal).Value = DBNull.Value;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Message("Error on ResetAuthorizedStatus: HandPayId = " + HandPayId);

              return false;
            }

            _new_status = _reader.GetInt32(0);
          }
        }

        if (!StatusActivated(_new_status, HANDPAY_STATUS.PENDING))
        {
          return false;
        }

        if (!CashierMovements.Add(0, CASHIER_MOVEMENT.HANDPAY_UNAUTHORIZED, 0, Card.AccountId, Card.TrackData))
        {
          Log.Error("CreateUndoAuthorizedHandpayCashierMovement: Error adding cashier movement. HandPayId: " + HandPayId);

          return false;
        }

        CashierMovements.AddRelatedIdInLastMov(HandPayId);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean CanApplyNoTax(Decimal HpAmount, HANDPAY_TYPE HpType)
    {
      Boolean _is_jackpot;

      _is_jackpot = false;

      if (GeneralParam.GetDecimal("Cashier.HandPays", "LimitToApplyNoTax", 0) == 0)
      {
        return false;
      }

      if (HpType == HANDPAY_TYPE.JACKPOT ||
          HpType == HANDPAY_TYPE.SITE_JACKPOT ||
          HpType == HANDPAY_TYPE.MANUAL_JACKPOT)
      {
        _is_jackpot = true;
      }

      if (WSI.Common.TITO.Utils.MaxDevolutionAmount(HpAmount, _is_jackpot) == HpAmount)
      {
        return false;
      }

      if (HpAmount > GeneralParam.GetDecimal("Cashier.HandPays", "LimitToApplyNoTax"))
      {
        return false;
      }

      return true;

    }

    public class HPAmountsCalculated
    {
      public TYPE_CASH_REDEEM CashRedeem = new TYPE_CASH_REDEEM();
      public TYPE_CASH_REDEEM CashRedeemB = new TYPE_CASH_REDEEM();
      public TYPE_CASH_REDEEM CashRedeemA = new TYPE_CASH_REDEEM();

      public Percentage TaxPct = 0;
      public Decimal TaxBaseAmount = 0;
      public Decimal TaxAmount = 0;
    }

    public static Boolean CalculateCashAmountHandpay(Int32 HpStatus,
                                                     Decimal HpAmount,
                                                     Decimal HpBaseAmount,
                                                     HANDPAY_TYPE HpType,
                                                     CardData CardData,
                                                     Boolean IsApplyTaxes,
                                                     Boolean IsEnabledParam,
                                                     out HPAmountsCalculated HpAmountsCalculated)
    {
      Boolean _calculate_tax;
      MultiPromos.PromoBalance _ini_balance;
      MultiPromos.PromoBalance _fin_balance;
      TYPE_SPLITS _splits;
      Boolean _is_jackpot;
      Decimal _base_amount;
      CashAmount _cash;

      _cash = new CashAmount();
      HpAmountsCalculated = new HPAmountsCalculated();

      _calculate_tax = true;
      _is_jackpot = false;
      if (IsApplyTaxes)
      {
        _base_amount = HpBaseAmount;
      }
      else
      {
        _base_amount = 0;
      }

      // No tax activated
      if (StatusActivated(HpStatus, HANDPAY_STATUS.NO_TAX) || StatusActivated(HpStatus, HANDPAY_STATUS.TAXED))
      {
        _calculate_tax = StatusActivated(HpStatus, HANDPAY_STATUS.TAXED);
      }
      else
      {
        if (HpType == HANDPAY_TYPE.JACKPOT || HpType == HANDPAY_TYPE.SITE_JACKPOT || HpType == HANDPAY_TYPE.MANUAL_JACKPOT)
        {
          _is_jackpot = true;
        }

        if (WSI.Common.TITO.Utils.MaxDevolutionAmount(HpBaseAmount, _is_jackpot) == HpBaseAmount)
        {
          _calculate_tax = false;
        }
      }

      if (!Split.ReadSplitParameters(out _splits))
      {
        return false;
      }

      MultiPromos.AccountBalance _balance;

      _balance = new MultiPromos.AccountBalance(CardData.AccountBalance);
      _balance.Redeemable += HpAmount;

      if (_calculate_tax)
      {
        CardData.MaxDevolution = _balance.TotalBalance - Math.Min(_base_amount, _balance.TotalBalance);
      }
      else
      {
        CardData.MaxDevolution = _balance.TotalBalance;
      }

      _ini_balance = new MultiPromos.PromoBalance(_balance, CardData.PlayerTracking.TruncatedPoints);


      if (!_cash.CalculateCashAmount(CASH_MODE.TOTAL_REDEEM, _splits,
                                        _ini_balance, 0,
                                        false,
                                        IsApplyTaxes,
                                        IsEnabledParam,
                                        CardData,
                                        out _fin_balance,
                                        out HpAmountsCalculated.CashRedeem,
                                        out HpAmountsCalculated.CashRedeemA,
                                        out HpAmountsCalculated.CashRedeemB))
      {
        return false;
      }

      HpAmountsCalculated.TaxPct = HpAmountsCalculated.CashRedeemA.TotalTaxesPct;
      HpAmountsCalculated.TaxBaseAmount = HpBaseAmount;
      HpAmountsCalculated.TaxAmount = HpAmountsCalculated.CashRedeemA.TotalTaxes;

      return true;
    }

    public static Boolean GetHandpayParamsForUndo(Int64 HandpayId, Int64 AccountId, out  HandpayRegisterOrCancelParameters HpParams, SqlTransaction Trx)
    {
      StringBuilder _sb;
      CardData _card_data;

      _card_data = new CardData();
      HpParams = null;

      try
      {
        if (!CardData.DB_CardGetAllData(AccountId, _card_data, false, Trx))
        {
          return false;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   HP_TYPE                               ");
        _sb.AppendLine("       , HP_DATETIME                           ");
        _sb.AppendLine("       , HP_AMOUNT                             ");
        _sb.AppendLine("       , HP_PLAY_SESSION_ID                    ");
        _sb.AppendLine("       , HP_MOVEMENT_ID                        ");
        _sb.AppendLine("       , HP_TERMINAL_ID                        ");
        _sb.AppendLine("       , HP_TE_PROVIDER_ID                     ");
        _sb.AppendLine("       , HP_TE_NAME                            ");
        _sb.AppendLine("       , HP_LEVEL                              ");
        _sb.AppendLine("       , HP_PROGRESSIVE_ID                     ");
        _sb.AppendLine("       , HP_ID                                 ");
        _sb.AppendLine("       , HP_STATUS                             ");
        _sb.AppendLine("       , HP_TAX_BASE_AMOUNT                    ");
        _sb.AppendLine("       , HP_TAX_AMOUNT                         ");
        _sb.AppendLine("       , HP_TAX_PCT                            ");
        // RAB 21-MAR-2016: Bug 9146 & Bug 9140
        _sb.AppendLine("       , ISNULL (HP_AMT0,HP_AMOUNT) AS HP_AMT0 ");
        _sb.AppendLine("  FROM   HANDPAYS                              ");
        _sb.AppendLine(" WHERE   HP_ID = @pHandpayId                   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pHandpayId", SqlDbType.BigInt).Value = HandpayId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Message("GetHandpayData: Error reading hp, HpId = " + HandpayId.ToString());

              return false;
            }

            HpParams = new HandpayRegisterOrCancelParameters();
            HpParams.HandpayType = (HANDPAY_TYPE)_reader.GetInt32(0);
            HpParams.HandpayDate = _reader.GetDateTime(1);
            HpParams.HandpayAmount = _reader.GetDecimal(2);
            HpParams.CancelPlaySessionId = _reader.GetInt64(3);
            HpParams.CancelMovementId = _reader.GetInt64(4);
            HpParams.CardData = _card_data;
            HpParams.TerminalId = _reader.GetInt32(5);
            HpParams.TerminalProvider = _reader.GetString(6);
            HpParams.TerminalName = _reader.GetString(7);
            HpParams.Level = _reader.IsDBNull(8) ? 0 : _reader.GetInt32(8);
            HpParams.ProgressiveId = _reader.IsDBNull(9) ? 0 : _reader.GetInt64(9);
            HpParams.HandpayId = _reader.GetInt64(10);
            HpParams.Status = _reader.GetInt32(11);
            HpParams.TaxBaseAmount = _reader.IsDBNull(12) ? 0 : _reader.GetDecimal(12);
            HpParams.TaxAmount = _reader.IsDBNull(13) ? 0 : _reader.GetDecimal(13);
            HpParams.TaxBasePct = _reader.IsDBNull(14) ? 0 : _reader.GetDecimal(14);
            // RAB 21-MAR-2016: Bug 9146 & Bug 9140
            HpParams.HandpayAmt0 = _reader.IsDBNull(15) ? 0 : _reader.GetDecimal(15);
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean GetHandpayForUndo(Int64 CashierId,
                                            Int64 SessionId,
                                            Int64 HandPayId,
                                            out Int64 OperationId,
                                            out DateTime DateTimeOperation,
                                            out HandpayRegisterOrCancelParameters HpParams)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetHandpayForUndo(CashierId, SessionId, HandPayId, out OperationId, out DateTimeOperation, out HpParams, _db_trx.SqlTransaction);
      }
    }

    public static Boolean GetHandpayForUndo(Int64 CashierId,
                                            Int64 SessionId,
                                            Int64 HandPayId,
                                            out Int64 OperationId,
                                            out DateTime DateTimeOperation,
                                            out HandpayRegisterOrCancelParameters HpParams,
                                            SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _time_period;
      Int32 _cancel_time_period;
      Int64 _operation_id;
      Int64 _handpay_id;
      Int64 _account_id;
      DataTable _dt;

      OperationId = 0;
      DateTimeOperation = DateTime.MinValue;
      HpParams = null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   ISNULL(CM_OPERATION_ID,0) ");
        _sb.AppendLine("         , ISNULL(CM_RELATED_ID,0) ");
        _sb.AppendLine("         , ISNULL(CM_ACCOUNT_ID,0) ");
        _sb.AppendLine("         , CM_DATE ");
        _sb.AppendLine("    FROM   CASHIER_MOVEMENTS ");
        _sb.AppendLine("   WHERE   CM_TYPE IN (@pHandpay, @pManualHandpay) ");
        _sb.AppendLine("     AND   CM_CASHIER_ID = @pCashierId");
        _sb.AppendLine("     AND   CM_SESSION_ID = @pSessionId");
        _sb.AppendLine("     AND   CM_DATE > DATEADD(minute,-@pCancelTimePeriod,GETDATE()) ");
        _sb.AppendLine("     AND   CM_RELATED_ID = @pHanPayId ");
        _sb.AppendLine("     AND   CM_UNDO_STATUS IS NULL ");
        _sb.AppendLine("ORDER BY   CM_MOVEMENT_ID DESC ");

        Handpays.GetParameters(out _time_period, out _cancel_time_period);

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCancelTimePeriod", SqlDbType.Int).Value = _cancel_time_period;
          _cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = CashierId;
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
          _cmd.Parameters.Add("@pHandpay", SqlDbType.Int).Value = CASHIER_MOVEMENT.HANDPAY;
          _cmd.Parameters.Add("@pManualHandpay", SqlDbType.Int).Value = CASHIER_MOVEMENT.MANUAL_HANDPAY;
          _cmd.Parameters.Add("@pHanPayId", SqlDbType.Int).Value = HandPayId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            _dt = new DataTable();
            _dt.Load(_reader);
          }
        }

        foreach (DataRow _dr in _dt.Rows)
        {
          OperationId = 0;
          DateTimeOperation = DateTime.MinValue;
          HpParams = null;

          _operation_id = (Int64)_dr[0];
          _handpay_id = (Int64)_dr[1];
          _account_id = (Int64)_dr[2];

          if (_account_id == 0 || _handpay_id == 0 || _operation_id == 0)
          {
            Log.Message("GetHandpayForUndo: Data not found: OperationId: " + _operation_id.ToString() + ", HpId = " + _handpay_id.ToString() + ", AccountId " + _account_id.ToString());

            return false;
          }

          if (!GetHandpayParamsForUndo(_handpay_id, _account_id, out HpParams, Trx))
          {
            return false;
          }

          if (!StatusActivated(HpParams.Status, HANDPAY_STATUS.PAID))
          {
            continue;
          }

          if (HpParams.HandpayType == HANDPAY_TYPE.SPECIAL_PROGRESSIVE ||
              HpParams.HandpayType == HANDPAY_TYPE.SITE_JACKPOT)
          {
            continue;
          }
          else
          {
            OperationId = _operation_id;
            DateTimeOperation = (DateTime)_dr[3];

            break;
          }
        }

        if (OperationId == 0)
        {
          HpParams = null;
          DateTimeOperation = DateTime.MinValue;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean UndoHandpay(HandpayRegisterOrCancelParameters HpParams,
                                      Int64 OperationIdForUndo,
                                      DateTime DateTimeUndoOperation,
                                      CashierSessionInfo SessionInfo,
                                      out String ErrorMessage,
                                      out  ArrayList VoucherGenerated,
                                      SqlTransaction Trx)
    {
      Int32 _time_period;
      Int32 _time_period_cancel;
      ArrayList _voucher_list;
      Int64 _operation_id;
      Int16 _handpay_error_msg;

      ErrorMessage = "";
      VoucherGenerated = null;

      Handpays.GetParameters(out _time_period, out _time_period_cancel);

      if (DateTimeUndoOperation <= WGDB.Now.AddMinutes(-_time_period_cancel))
      {
        ErrorMessage = Resource.String("STR_FRM_HANDPAYS_039").Replace("\\r\\n", "\r\n");

        return false;
      }

      if (!StatusActivated(HpParams.Status, HANDPAY_STATUS.PAID))
      {
        return false;
      }

      HpParams.CancelHandpay = true;

      _operation_id = 0;
      if (!RegisterOrCancel(HpParams,
                            SessionInfo,
                            true,
                            OperationCode.HANDPAY_CANCELLATION,
                            ref _operation_id,
                            out _handpay_error_msg,
                            Trx))
      {
        ErrorMessage = Resource.String("STR_FRM_HANDPAYS_040");

        return false;
      }

      OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
      _params.CodeOperation = OperationCode.HANDPAY_CANCELLATION;
      _params.CardData = HpParams.CardData;
      _params.OperationIdForUndo = OperationIdForUndo;
      _params.CashierSessionInfo = SessionInfo;
      _params.OperationId = _operation_id;
      _params.GenerateVoucherForUndo = true;

      if (!OperationUndo.UndoOperation(_params, false, out _voucher_list, Trx))
      {
        ErrorMessage = Resource.String("STR_FRM_HANDPAYS_040");

        return false;
      }

      VoucherGenerated = _voucher_list;

      return true;
    }

    //FOS 10-03-2015
    //------------------------------------------------------------------------------
    // PURPOSE : Get the General param value, than define if disable type selection.
    //
    //  PARAMS :
    //      - INPUT :
    //          
    //      - OUTPUT :
    //            
    //
    // RETURNS :
    //      - Boolean value
    //
    //   NOTES :
    public static Boolean IsDisableTypeSelection()
    {
      return GeneralParam.GetBoolean("Cashier.HandPays", "DisableTypeSelection", false);
    }


    //FAV 10-JUN-2015
    //------------------------------------------------------------------------------
    // PURPOSE : Get the UserID and UserName for a Cashier movement using the HandPayId
    //
    //  PARAMS :
    //      - INPUT :
    //          Int64 HandPayId
    //          
    //      - OUTPUT :
    //          int CashierSessionID
    //          string CashierSessionName
    //
    // RETURNS :
    //      - Boolean value
    //
    //   NOTES :
    public static Boolean GetUserForManualHandpayMovement(Int64 HandPayId,
                          out Int64 CashierSessionID,
                          out string CashierSessionName,
                          out HandpayRegisterOrCancelParameters HpParams)
    {
      CashierSessionID = -1;
      CashierSessionName = string.Empty;
      HpParams = null;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        SqlTransaction Trx = _db_trx.SqlTransaction;

        {
          StringBuilder _sb;
          Int32 _time_period;
          Int32 _cancel_time_period;
          Int64 _account_id;
          Int64 cashier_session_id;
          WSI.Common.Cashier.CashierSessionData cashier_session_data;
          DataTable _dt;

          try
          {
            _sb = new StringBuilder();
            _sb.AppendLine("  SELECT   CM_SESSION_ID ");
            _sb.AppendLine("         , ISNULL(CM_ACCOUNT_ID,0) ");
            _sb.AppendLine("    FROM   CASHIER_MOVEMENTS ");
            _sb.AppendLine("   WHERE   CM_TYPE = @pManualHandpay ");
            _sb.AppendLine("     AND   CM_DATE > DATEADD(minute,-@pCancelTimePeriod,GETDATE()) ");
            _sb.AppendLine("     AND   CM_RELATED_ID = @pHanPayId ");
            _sb.AppendLine("     AND   CM_UNDO_STATUS IS NULL ");

            Handpays.GetParameters(out _time_period, out _cancel_time_period);

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
            {
              _cmd.Parameters.Add("@pCancelTimePeriod", SqlDbType.Int).Value = _cancel_time_period;
              _cmd.Parameters.Add("@pManualHandpay", SqlDbType.Int).Value = CASHIER_MOVEMENT.MANUAL_HANDPAY;
              _cmd.Parameters.Add("@pHanPayId", SqlDbType.Int).Value = HandPayId;

              using (SqlDataReader _reader = _cmd.ExecuteReader())
              {
                _dt = new DataTable();
                _dt.Load(_reader);
              }
            }

            foreach (DataRow _dr in _dt.Rows)
            {
              if (_dr[0] != null)
              {
                cashier_session_id = Convert.ToInt64(_dr[0]);
                _account_id = (Int64)_dr[1];

                if (!GetHandpayParamsForUndo(HandPayId, _account_id, out HpParams, Trx))
                {
                  return false;
                }

                Cashier.GetCashierSessionData(cashier_session_id, out cashier_session_data);

                CashierSessionID = cashier_session_id;
                CashierSessionName = cashier_session_data.Cashier_Name;
                return true;
              }
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
          return false;
        }
      }
    }

    public static Boolean AutomaticallyPayHandPay(Int64 AccountId, Handpays.HandpayRegisterOrCancelParameters InputHandpay, SqlTransaction Trx)
    {
      MultiPromos.AccountBalance _ini_balance;
      Int64 _play_session_id;
      String _terminal_name;
      CashierSessionInfo _session_info;
      Currency _played_amount;
      String _denomination_str;
      String _denomination_value;
      Boolean _print_played_amount;
      Int16 _handpay_error_msg;

      try
      {
        if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _play_session_id, Trx))
        {
          Log.Error("Error Trx_UpdateAccountBalance. AccountId: " + AccountId.ToString());

          return false;
        }

        // AJQ 31-OCT-2014, Let's pay the SiteJackpot event when the account is in session (the most usual situation). 

        InputHandpay.CardData = new CardData();
        if (!CardData.DB_CardGetAllData(AccountId, InputHandpay.CardData, Trx))
        {
          Log.Error("Error DB_CardGetAllData on AutomaticallyPayHandPay");

          return false;
        }

        if (!Misc.GetTerminalName(InputHandpay.TerminalId, out _terminal_name, Trx))
        {
          Log.Error("Error GetTerminalName on AutomaticallyPayHandPay");

          return false;
        }

        _session_info = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM, Trx);

        if (_session_info == null)
        {
          Log.Error("Error GetSystemCashierSessionInfo on AutomaticallyPayHandPay");

          return false;
        }

        _session_info.TerminalName = _terminal_name;

        _print_played_amount = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.PrintPlayedAmount");

        if (_print_played_amount)
        {
          if (!Handpays.GetHandpaySessionPlayedAmount(AccountId
                                                    , InputHandpay.SiteJackpotAwardedOnTerminalId
                                                    , InputHandpay.HandpayDate
                                                    , out _played_amount
                                                    , out _denomination_str
                                                    , out _denomination_value))
          {
            Log.Error("Error GetHandpaySessionPlayedAmount on AutomaticallyPayHandPay");

            return false;
          }

          InputHandpay.SessionPlayedAmount = _played_amount;
          InputHandpay.DenominationStr = _denomination_str;
          InputHandpay.DenominationValue = _denomination_value;
        }
        else
        {
          InputHandpay.SessionPlayedAmount = -1;
        }


        if (!Handpays.RegisterOrCancel(InputHandpay, _session_info, OperationCode.HANDPAY, out _handpay_error_msg, Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // AutomaticallyPayHandPay

    
    public static Boolean UpdatePlaySession(Int64 PlaySessionId, Decimal HandpayAmount, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   PLAY_SESSIONS ");
        _sb.AppendLine("   SET   PS_HANDPAYS_PAID_AMOUNT = isnull (PS_HANDPAYS_PAID_AMOUNT, 0) + @pHandpayPaidAmount ");
        _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID = @pPlaySessionID ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pHandpayPaidAmount", SqlDbType.Money).Value = HandpayAmount;
          _sql_cmd.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("Handpays.UpdatePlaySession failed. PlaySessionId: " + PlaySessionId);

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // UpdatePlaysession

    #endregion
  }
}

