//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TemplateWord.cs
// 
//   DESCRIPTION: Handle word application
//                Reference code: http://midnightprogrammer.net/post/Word-Automation-in-C.aspx
// 
//        AUTHOR: Marcos Piedra
// 
// CREATION DATE: 31-JAN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-JAN-2012 MPO    First release.
// 02-MAY-2012 MPO    Change Name of function FillField to Fill
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop;
using Word = Microsoft.Office.Interop.Word;
using System.IO;
using System.Threading;
using WSI.Common;

namespace WSI.Common
{
  public class TemplateWord : Template
  {

    #region Members

    Word.Application m_application_word;
    Word.Document m_document;
    Boolean m_showed;

    #endregion // Members

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize properties.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TemplateWord(String TemplateWord)
    {

      m_application_word = new Microsoft.Office.Interop.Word.Application();
      m_document = null;
      m_showed = false;
      SetFileNameTemplate(TemplateWord);

    } //TemplateWord

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize properties.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TemplateWord(MemoryStream TemplateWord)
    {
      String _temporary_template;

      m_application_word = new Microsoft.Office.Interop.Word.Application();
      m_document = null;
      m_showed = false;

      _temporary_template = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".dotx");
      Save(TemplateWord, _temporary_template);
      SetFileNameTemplate(_temporary_template);

    } //TemplateWord

    #endregion // Constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Show a word file using the COM object
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void Show()
    {
      if (m_application_word != null)
      {

        CheckCurrentDocument();

        m_showed = true;
        m_application_word.Visible = true;
      }
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE: Print a word file using the COM object.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void Print()
    {
      Object _missing;

      CheckCurrentDocument();

      _missing = System.Reflection.Missing.Value;

      if (m_document != null)
      {
        Object _copies = "1";
        Object _pages = "1";
        Object _range = Word.WdPrintOutRange.wdPrintCurrentPage;
        Object _items = Word.WdPrintOutItem.wdPrintDocumentContent;
        Object _page_type = Word.WdPrintOutPages.wdPrintAllPages;
        Object _true = true;
        Object _false = false;

        m_document.PrintOut(ref _true, ref _false, ref _range, ref _missing, ref _missing, ref _missing,
                       ref _items, ref _copies, ref _pages, ref _page_type, ref _false, ref _true,
                       ref _missing, ref _false, ref _missing, ref _missing, ref _missing, ref _missing);

        System.Threading.Thread.Sleep(1000);

      }
    } // Print

    //------------------------------------------------------------------------------
    // PURPOSE: Save the word into a file
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: File where the word document is saved
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void SaveAs(String FileName)
    {
      Object _missing;

      CheckCurrentDocument();

      _missing = System.Reflection.Missing.Value;

      if (m_document != null)
      {

        Object _file_name = FileName;
        Object _file_format = (Object)Word.WdSaveFormat.wdFormatDocumentDefault;
        Object _lock_comments = _missing;
        Object _password = _missing;
        Object _add_to_recent_files = _missing;
        Object _write_password = _missing;
        Object _read_only_recommended = _missing;
        Object _embed_true_type_fonts = _missing;
        Object _save_native_picture_format = _missing;
        Object _save_forms_data = _missing;
        Object _save_as_AOCE_letter = _missing;
        Object _encoding = _missing;
        Object _insert_line_breaks = _missing;
        Object _allow_substitutions = _missing;
        Object _line_ending = _missing;
        Object _addBiDiMarks = _missing;

        m_document.SaveAs(ref _file_name, ref _file_format, ref _lock_comments,
                    ref _password, ref _add_to_recent_files, ref _write_password,
                    ref _read_only_recommended, ref _embed_true_type_fonts, ref _save_native_picture_format,
                    ref _save_forms_data, ref _save_as_AOCE_letter, ref _encoding, ref _insert_line_breaks,
                    ref _allow_substitutions, ref _line_ending, ref _addBiDiMarks);

      }
    } // SaveAs

    //------------------------------------------------------------------------------
    // PURPOSE: Dispose the Word Interop objects
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void Close()
    {
      Object _save_changes;
      Object _original_fomat;
      Object _route_doc;

      if (!m_showed)
      {

        _save_changes = Word.WdSaveOptions.wdDoNotSaveChanges;
        _original_fomat = Word.WdOriginalFormat.wdOriginalDocumentFormat;
        _route_doc = false;

        CloseCurrentDocument();

        if (m_application_word != null)
        {

          GC.Collect();
          ThreadPool.QueueUserWorkItem(delegate
          {
            GC.WaitForPendingFinalizers();
            GC.Collect();
          });

          ((Word._Application)m_application_word).Quit(ref _save_changes, ref _original_fomat, ref _route_doc);
          ReleaseComObject(m_application_word);
          m_application_word = null;
        }

      }

    } // Close

    //------------------------------------------------------------------------------
    // PURPOSE: Filling the template word that contains the fields
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: Template word that contains the fields
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: Is correct the process of filling?
    // 
    //   NOTES:
    // 
    public override Boolean Fill()
    {
      Int16 _num_shape;
      Object _obj;
      Word.Fields _fields;
      Object _obj_path;
      Object _missing;

      _missing = System.Reflection.Missing.Value;

      try
      {

        _obj_path = (Object)m_path_template;

        if (m_document != null) CloseCurrentDocument();
        m_document = m_application_word.Documents.Add(ref _obj_path, ref _missing, ref _missing, ref _missing);
        m_document.Select();

        _fields = m_document.Fields;
        // Replace the fields in the document
        ReplaceInFields(_fields);

        // Replace the fields in the shapes (textbox) of the document
        for (_num_shape = 1; _num_shape <= m_document.Shapes.Count; _num_shape++)
        {
          _obj = (Object)_num_shape;
          if (m_document.Shapes.get_Item(ref _obj).TextFrame.HasText == -1)
          {
            _fields = m_document.Shapes.get_Item(ref _obj).TextFrame.TextRange.Fields;
            ReplaceInFields(_fields);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    } // FillField

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current stream that contain the template has filled.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Stream:
    // 
    //   NOTES:
    // 
    public override Stream GetStream()
    {
      MemoryStream _memory_stream;
      String _current_docx;
      FileStream _input;
      Boolean _is_filled;

      _is_filled = Fill();

      if (_is_filled)
      {
        _current_docx = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".docx");
        this.SaveAs(_current_docx);
        CloseCurrentDocument();
        _input = new FileStream(_current_docx, FileMode.Open);
        _memory_stream = CopyToMemory(_input);
        _memory_stream.Position = 0;
        m_changed = true;

        return _memory_stream;
      }
      else
      {

        return null;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Is it exists an application that open the word document?
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: true if exists, false if exists the application
    // 
    //   NOTES:
    //    
    static public Boolean ApplicationAvailable()
    {
      Word.Application _application_word;
      Object _save_changes;
      Object _original_fomat;
      Object _route_doc;

      _save_changes = Word.WdSaveOptions.wdDoNotSaveChanges;
      _original_fomat = Word.WdOriginalFormat.wdOriginalDocumentFormat;
      _route_doc = false;

      try
      {
        _application_word = new Microsoft.Office.Interop.Word.Application();

        if (_application_word == null)
        {
          return false;
        }

        ((Word._Application)_application_word).Quit(ref _save_changes, ref _original_fomat, ref _route_doc);
        ReleaseComObject(_application_word);
        _application_word = null;

        return true;
      }
      catch
      {

        return false;
      }

    } // ApplicationAvailable

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Replace the field with the value of the dictionary
    //
    //  PARAMS:
    //      - INPUT:
    //          - WordFields: Collection of fields
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private void ReplaceInFields(Word.Fields WordFields)
    {
      Word.Range _range;

      foreach (Word.Field _field in WordFields)
      {
        if (_field.Type == Microsoft.Office.Interop.Word.WdFieldType.wdFieldMergeField)
        {
          _range = _field.Code;

          foreach (String _key in m_fields.Keys)
          {
            if (_range.Text.Contains(_key))
            {
              _field.Select();
              m_application_word.Selection.Font = _field.Result.Font;
              m_application_word.Selection.TypeText(m_fields[_key]);

              break;
            }
          }

        }
      }

    } // ReplaceInFields

    //------------------------------------------------------------------------------
    // PURPOSE: Dispose the current document if exists
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void CloseCurrentDocument()
    {

      Object _save_changes;
      Object _original_fomat;
      Object _route_doc;

      if (m_document != null)
      {

        GC.Collect();
        ThreadPool.QueueUserWorkItem(delegate
        {
          GC.WaitForPendingFinalizers();
          GC.Collect();
        });

        _save_changes = Word.WdSaveOptions.wdDoNotSaveChanges;
        _original_fomat = Word.WdOriginalFormat.wdOriginalDocumentFormat;
        _route_doc = false;

        ((Word._Document)m_document).Close(ref _save_changes, ref _original_fomat, ref _route_doc);
        ReleaseComObject(m_document);
        m_document = null;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check if it have changed and filled the fields if is necessary
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private void CheckCurrentDocument()
    {

      if (m_changed)
      {

        Fill();
        m_changed = false;

      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Release a COM object.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object Obj
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static private void ReleaseComObject(Object Obj)
    {
      Int32 _num_ref;
      try
      {
        if (Obj != null)
        {
          do
          {
            _num_ref = System.Runtime.InteropServices.Marshal.ReleaseComObject(Obj);
          } while (_num_ref > 0);
        }
        Obj = null;
      }
      catch
      {
        Obj = null;
      }
    } // ReleaseComObject

    #endregion // Private Methods

  } // TemplateWord
} // Wrapper
