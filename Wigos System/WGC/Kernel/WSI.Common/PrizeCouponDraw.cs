using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public class PrizeCouponDraw
  {
    public static void Test ()
    {
      PrizeCouponDrawParameters _param;
      PrizeCouponDraw _draw;

      _param = new PrizeCouponDrawParameters();

      _param.num_losers = 1;
      _param.num_draws = 10000;


      _param.num_numbers = 20;
      _param.num_drawn_numbers   = 20;
      _param.num_winning_numbers = 10;
      _param.num_selected_numbers = 4;
      _param.num_hits_to_win = 4;

      for (Int64 _draw_id = 1; _draw_id <= _param.num_draws * 2; _draw_id++)
      {
        _draw = NewDraw(_draw_id, _param);
      }
    }
    public class PrizeCouponDrawParameters
    {
      internal int num_losers;
      internal int num_draws;

      internal int num_numbers;
      internal int num_selected_numbers;
      internal int num_winning_numbers;

      internal int num_hits_to_win;
      internal int num_drawn_numbers;
    }

    public static PrizeCouponDraw NewDraw(Int64 DrawId, PrizeCouponDrawParameters Param)
    {
      Random _rng;
      Boolean _loser;
      List<int> _selection;
      List<int> _drawn;
      List<int> _numbers;
      int _idx;
      int _count;

      int _number;

      _rng = new Random((int)DrawId);
      _loser = (_rng.Next(0, Param.num_draws) < Param.num_losers);

      _selection = new List<int>();
      _drawn = new List<int>();

      //
      // 'Bombo' Draw Numbers
      // 
      _numbers = new List<int>();
      for (_number = 1; _number <= Param.num_numbers; _number++)
      {
        _numbers.Add(_number);
      }

      // Draw all the numbers
      while (_numbers.Count > 0)
      {
        _idx = _rng.Next(0, _numbers.Count);
        _number = _numbers[_idx];
        _numbers.Remove(_number);
        _drawn.Add(_number);
      }

      if (_loser)
      {
        // Loser
        if (Param.num_hits_to_win > 1)
        {
          _count = _rng.Next(0, Param.num_hits_to_win - 1);
        }
        else
        {
          _count = 0;
        }
      }
      else
      {
        // Winner
        _count = Param.num_hits_to_win;
      }

      while (_selection.Count < _count)
      {
        _idx = _rng.Next(0, Param.num_winning_numbers);
        _number = _drawn[_idx];
        if (!_selection.Contains(_number))
        {
          _selection.Add(_number);
        }
      }

      while (_selection.Count < Param.num_selected_numbers)
      {
        _idx = _rng.Next(Param.num_winning_numbers + 1, Param.num_numbers);
        _number = _drawn[_idx];
        if (!_selection.Contains(_number))
        {
          _selection.Add(_number);
        }
      }

      while (_drawn.Count > Param.num_drawn_numbers)
      {
        _drawn.Remove(_drawn[_drawn.Count - 1]);
      }

      //
      // Sort the selection
      // 
      _selection.Sort();

      return null;
    }
  }
}
