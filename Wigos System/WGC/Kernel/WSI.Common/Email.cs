//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Email.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 27-DEC-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-DEC-2010 RCI    First release.
// 13-MAY-2011 LCM    Added Occupation section in StatsMailingReport
// 14-DEC-2011 MPO    Added fields hold and num. terminal in StatsMailingReport
// 13-APR-2012 MPO    Defect 219: Added SiteJackpot in StatsMailingReport
// 14-MAY-2013 RBG    Added liabilities to cashier summary mail
// 11-MAR-2015 DHA    Added capacity and accounts data to summary mail
// 03-MAR-2016 JML    Product Backlog Item 10085:Winpot - Tax Provisions: Include movement reports
// 29-JUN-2016 JMV    Product Backlog Item 14927:TITO Hold vs Win: Update
// 28-AGU-2017 DHA    PBI29473:WIGOS-4739 Email - Cash summary - Add "Total en m�quinas"
// 19-FEB-2018 AGS    Bug 31578:WIGOS-8124 [Ticket #12343] Resumen de Caja enviado por correo no Incluye Resultado de Mesas Version V03.06.0035
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;

namespace WSI.Common
{
  public class Email : Voucher
  {
    #region Constructor

    public Email()
    {
    } // Email

    #endregion

    public new string GetFileName ()
    {
      FileStream _file_stream;
      StreamWriter _file_writer;
      String _filename;

      _file_writer = null;
      _filename = "Email.html";

      try
      {
        // Create a temporary file with voucher to print
        _filename = Directory.GetCurrentDirectory () + "\\" + _filename;

        _file_stream = new FileStream (_filename, FileMode.Create, FileAccess.Write);
        _file_writer = new StreamWriter (_file_stream);

        // Write voucher
        _file_writer.BaseStream.Seek (0, SeekOrigin.Begin);
        _file_writer.Write (VoucherHTML);

      }
      catch ( Exception ex )
      {
        Log.Error ("Email.GetFileName. Error writing html. File: " + _filename.ToString ());
        Log.Exception (ex);
      }
      finally
      {
        _file_writer.Close ();

        VoucherFileName = _filename;
      }

      return _filename;
    } // GetFileName

    public String GetHTML()
    {
      return VoucherHTML;
    } // GetHTML

  } // Email

  public class HoldvsWinReport: Email
  {
    public HoldvsWinReport(TYPE_MAILING_HOLDVSWIN MailingReport)
    {
      LoadVoucher("TITOHoldvsWin");

      AddString("NLEVEL1", GeneralParam.GetString("PlayerTracking", "Level01.Name"));
      AddString("NLEVEL2", GeneralParam.GetString("PlayerTracking", "Level02.Name"));
      AddString("NLEVEL3", GeneralParam.GetString("PlayerTracking", "Level03.Name"));
      AddString("NLEVEL4", GeneralParam.GetString("PlayerTracking", "Level04.Name"));

      AddString("SITE_NAME", MailingReport.site_name);
      AddString("DATE_FROM", MailingReport.date_from.ToString("g"));
      AddString("DATE_TO", MailingReport.date_to.ToString("g"));

      AddInteger("VISITS", MailingReport.reception_data.Visits);
      if (MailingReport.reception_data.VisitsLevel0 != 0)
      {
        AddInteger("LEVEL0", MailingReport.reception_data.VisitsLevel0);
        AddString("ANON1", GeneralParam.GetString("PlayerTracking", "NoLevel.Name", Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_00")));
      }
      else
      {
        AddString("LEVEL0", "");
        AddString("ANON1", "");
      }
      AddInteger("LEVEL1", MailingReport.reception_data.VisitsLevel1);
      AddInteger("LEVEL2", MailingReport.reception_data.VisitsLevel2);
      AddInteger("LEVEL3", MailingReport.reception_data.VisitsLevel3);
      AddInteger("LEVEL4", MailingReport.reception_data.VisitsLevel4);
      AddInteger("LEVEL5", MailingReport.reception_data.VisitsLevel5);
      AddInteger("MALE", MailingReport.reception_data.VisitsMale);
      AddInteger("FEMALE", MailingReport.reception_data.VisitsFemale);
      if (MailingReport.reception_data.VisitsAnonymous != 0)
      {
        AddInteger("G_ANON", MailingReport.reception_data.VisitsAnonymous);
        AddString("ANON2", GeneralParam.GetString("PlayerTracking", "NoGender.Name", Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_00")));
      }
      else
      {
        AddString("G_ANON", "");
        AddString("ANON2", "");
      }

      AddCurrency("TOTAL_IN", MailingReport.egm_cash_total_in);
      AddCurrency("TOTAL_OUT", MailingReport.egm_cash_total_out);
      AddCurrency("NETWIN", MailingReport.egm_cash_netwin);
      AddString("PC_NETWIN", MailingReport.egm_cash_netwin_percent);
      AddCurrency("BILL_IN", MailingReport.egm_cash_bill_in);
      AddCurrency("TIN_RE", MailingReport.egm_cash_ticket_in_redimible);
      AddCurrency("TOUT_RE", MailingReport.egm_cash_ticket_out_redimible);
      AddCurrency("RE_NETWIN", MailingReport.egm_cash_netwin_redimible);
      AddString("PC_NW", MailingReport.egm_cash_netwin_redimible_percent);

      AddCurrency("COIN_IN", MailingReport.egm_total_coin_in);
      AddCurrency("TOTAL_WON", MailingReport.egm_total_total_won);
      AddCurrency("HOLD", MailingReport.egm_total_hold);
      AddString("PC_HOLD", MailingReport.egm_total_hold_percent);

      // JMV:29-JUN-2016
      AddCurrency("JACKPOTS", MailingReport.egm_cash_jackpots);
      AddCurrency("HANDPAYS", MailingReport.egm_cash_handpays);
      AddCurrency("DN_BILL_IN", MailingReport.cashier_data.cashier_bill_in);
      AddCurrency("TICKETS_CREATED", MailingReport.cashier_data.cashier_tickets_created);
      AddCurrency("CASH_PROMOS", MailingReport.cashier_data.cashier_cash_promos);
      AddCurrency("TICKETS_PAID", MailingReport.cashier_data.cashier_tickets_paid);
      AddCurrency("DAILY_NEWTIN", MailingReport.cashier_data.cashier_daily_netwin);

      // JMV: 22-JUL-2016
      AddCurrency("PAID_HANDPAYS", MailingReport.cashier_data.cashier_handpays_paid);

      AddString("PC_DAILY_NEWTIN", MailingReport.cashier_data.cashier_netwin_percent);

    }

  }
  // HoldvsWinReport

  public class StatsMailingReport : Email
  {

    #region Constructor

    public StatsMailingReport(TYPE_MAILING_REPORT MailingReport)
    {
      TYPE_MAILING_PROVIDER_STATS _total_stats;
      String _total_machine_stats;
      String _gaming_table_status;
      String _provider_stats;
      String _tabs;
      String _played_label;
      String _netwin_label;
      String _netwin_per_terminal_label;
      String _num_terminals_label;
      String _hold_label;
      String _hold_str;
      Currency _netwin_per_terminal;
      Currency _liability_to_re;
      Currency _liability_to_nr;
      Currency _liability_from_re;
      Currency _liability_from_nr;
      Currency _liability_increase_re;
      Currency _liability_increase_nr;
      Percentage _hold;
      String _value;

      _played_label = Resource.String("STR_MAILING_PLAYED_LABEL");
      _netwin_label = Resource.String("STR_MAILING_NETWIN_LABEL");
      _netwin_per_terminal_label = Resource.String("STR_MAILING_NETWIN_PER_TERMINAL_LABEL");
      _num_terminals_label = Resource.String("STR_MAILING_NUM_TERMINALS");  // "N� Terminales"; 
      _hold_label = Resource.String("STR_MAILING_HOLD");  // "Hold"

      _hold = 0;

      LoadVoucher("StatsMailingReport");

      AddString("SITE_NAME", MailingReport.site_name);
      AddString("DATE_FROM", MailingReport.date_from.ToString("g"));
      AddString("DATE_TO", MailingReport.date_to.ToString("g"));
      AddCurrency("CASHIER_INPUT_AMOUNT", MailingReport.cashier_inputs);

      // DHA 11-MAR-2015: Show Cash In Tax
      if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled"))
      {
        _value = "<tr>";
        _value += "<td>" + GeneralParam.GetString("Cashier", "CashInTax.Name") + ":</td><td align=\"right\">@CASHIER_CASH_IN_TAX</td>";
        _value += "</tr>";

        SetParameterValue("@CASHIER_CASH_IN_TAX", _value);
        AddCurrency("CASHIER_CASH_IN_TAX", MailingReport.cashier_cash_in_tax);
      }
      else
      {
        AddString("CASHIER_CASH_IN_TAX", String.Empty);
      }

      // JML 03-MAR-2016: Show Taxprovisions
      if (Misc.IsTaxProvisionsEnabled())
      {
        _value = "<tr>";
        _value += "<td>" + GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_PROVISIONS_TITLE")) + ":</td><td align=\"right\">@CASHIER_TAX_PROVISIONS</td>";
        _value += "</tr>";

        SetParameterValue("@CASHIER_TAX_PROVISIONS", _value);
        AddCurrency("CASHIER_TAX_PROVISIONS", MailingReport.tax_provisions);
      }
      else
      {
        AddString("CASHIER_TAX_PROVISIONS", String.Empty);
      }

      AddCurrency("CASHIER_OUTPUT_AMOUNT", MailingReport.cashier_outputs);
      AddCurrency("CASHIER_PRIZE_TAXES_AMOUNT", MailingReport.cashier_prize_taxes);
      AddCurrency("CASHIER_RESULT_AMOUNT", MailingReport.cashier_result);

      if (MailingReport.liabilities_to.has_data)
      {
        _liability_to_re = MailingReport.liabilities_to.promo_re_balance + MailingReport.liabilities_to.re_balance;
        _liability_to_nr = MailingReport.liabilities_to.promo_nr_balance;
        AddCurrency("LIABILITIES_DATE_TO_RE", _liability_to_re );
        AddCurrency("LIABILITIES_DATE_TO_NR", _liability_to_nr );
      }
      else
      {
        _liability_to_re = 0;
        _liability_to_nr = 0;
        AddString("LIABILITIES_DATE_TO_RE", "---");
        AddString("LIABILITIES_DATE_TO_NR", "---");
      }

      if (MailingReport.liabilities_from.has_data && MailingReport.liabilities_to.has_data)
      {
        _liability_from_re = MailingReport.liabilities_from.promo_re_balance + MailingReport.liabilities_from.re_balance;
        _liability_from_nr = MailingReport.liabilities_from.promo_nr_balance;
        _liability_increase_re = _liability_to_re - _liability_from_re;
        _liability_increase_nr = _liability_to_nr - _liability_from_nr;
        AddCurrency("LIABILITIES_INCREASE_RE", _liability_increase_re);
        AddCurrency("LIABILITIES_INCREASE_NR", _liability_increase_nr);
      }
      else
      {
        AddString("LIABILITIES_INCREASE_RE", "---");
        AddString("LIABILITIES_INCREASE_NR", "---");
      }

      AddCurrency("CASHIER_SITE_JACKPOT_AMOUNT", MailingReport.site_jackpot);

      AddString("OCCUPATION_SESSIONS", MailingReport.occupation.is_set_num_opened_sessions ?
                                       MailingReport.occupation.num_opened_sessions.ToString() :
                                       Resource.String("STR_MAILING_UNAVAILABLE_DATA"));

      AddString("OCCUPATION_NOW", MailingReport.occupation.is_set_now ?
                                  MailingReport.occupation.now.ToString() :
                                  Resource.String("STR_MAILING_UNAVAILABLE_DATA"));

      AddString("OCCUPATION_LAST_HOUR", MailingReport.occupation.is_set_last_hour ?
                                        MailingReport.occupation.last_hour.ToString() :
                                        Resource.String("STR_MAILING_UNAVAILABLE_DATA"));

      AddString("OCCUPATION_TILL_NOW", MailingReport.occupation.is_set_till_now ?
                                       MailingReport.occupation.till_now.ToString() :
                                       Resource.String("STR_MAILING_UNAVAILABLE_DATA"));

      // DHA 11-MAR-2015 capacity table
      AddInteger("CAPACITY_SESSIONS_MALES", MailingReport.capacity_stats.male_capacity);
      AddInteger("CAPACITY_SESSIONS_FEMALES", MailingReport.capacity_stats.female_capacity);
      AddInteger("CAPACITY_SESSIONS_ANONYMOUS", MailingReport.capacity_stats.anonymous_capacity);
      AddInteger("CAPACITY_SESSIONS_TOTAL", MailingReport.capacity_stats.male_capacity + MailingReport.capacity_stats.female_capacity + MailingReport.capacity_stats.anonymous_capacity);

      // Set accounts
      AddInteger("ACCOUNTS_SESSIONS_CREATED", MailingReport.accounts_stats.accounts_created);
      AddInteger("ACCOUNTS_SESSIONS_ACTIVE30", MailingReport.accounts_stats.accounts_active30);
      AddInteger("ACCOUNTS_SESSIONS_ACTIVE90", MailingReport.accounts_stats.accounts_active90);
      AddInteger("ACCOUNTS_SESSIONS_ACTIVE180", MailingReport.accounts_stats.accounts_active180);

      if (GeneralParam.GetBoolean("Mailing", "CashReport.IncludeTotalNumberOfAccounts", false))
      {
        _value = "<tr>";
        _value += "<td>" + Resource.String("STR_MAILING_CASH_SUMMARY_ACCOUNTS_INACTIVE") + ":</td><td align=\"right\">@ACCOUNTS_SESSION_INACTIVE</td>";
        _value += "</tr>";
        _value += "<tr>";
        _value += "<td>" + Resource.String("STR_MAILING_CASH_SUMMARY_ACCOUNTS_RECORDS") + ":</td><td align=\"right\">@ACCOUNTS_SESSION_TOTAL</td>";
        _value += "</tr>";

        SetParameterValue("@ACCOUNTS_SESSION_DATA", _value);
        AddInteger("ACCOUNTS_SESSION_INACTIVE", MailingReport.accounts_stats.accounts_inactive);
        AddInteger("ACCOUNTS_SESSION_TOTAL", MailingReport.accounts_stats.accounts_active30 + MailingReport.accounts_stats.accounts_active90 + MailingReport.accounts_stats.accounts_active180 + MailingReport.accounts_stats.accounts_inactive);
      }
      else
      {
        AddString("ACCOUNTS_SESSION_DATA", String.Empty);
      }


      _total_stats.name = "Total:";
      _total_stats.played = 0;
      _total_stats.netwin = 0;
      _total_stats.num_terminals = 0;

      _tabs = "          ";
      _provider_stats = "";
      _total_machine_stats = "";
      _gaming_table_status = "";

      foreach (TYPE_MAILING_PROVIDER_STATS _prov_stat in MailingReport.provider_stats)
      {
        _netwin_per_terminal = (_prov_stat.num_terminals > 0) ? _prov_stat.netwin / _prov_stat.num_terminals : 0;
        _hold = (_prov_stat.played > 0) ? _prov_stat.netwin / _prov_stat.played : 0.00m;
        _hold_str = _hold.ToString();

        _provider_stats += _tabs + "<tr>" + Environment.NewLine +
                           _tabs + "  <td colspan=\"2\"><b>" + _prov_stat.name + "</b></td>" + Environment.NewLine +
                           _tabs + "</tr>" + Environment.NewLine +
                           _tabs + "<tr>" + Environment.NewLine +
                           _tabs + "  <td>" + _played_label + "</td>" + Environment.NewLine +
                           _tabs + "  <td align=\"right\">" + _prov_stat.played + "</td>" + Environment.NewLine +
                           _tabs + "</tr>" + Environment.NewLine +
                           _tabs + "<tr>" + Environment.NewLine +
                           _tabs + "  <td>" + _netwin_label + "</td>" + Environment.NewLine +
                           _tabs + "  <td align=\"right\">" + _prov_stat.netwin + "</td>" + Environment.NewLine +
                           _tabs + "</tr>" + Environment.NewLine +
                           _tabs + "<tr>" + Environment.NewLine +
                           _tabs + "  <td>" + _netwin_per_terminal_label + "</td>" + Environment.NewLine +
                           _tabs + "  <td align=\"right\">" + _netwin_per_terminal + "</td>" + Environment.NewLine +
                           _tabs + "</tr>" + Environment.NewLine +
                           _tabs + "<tr>" + Environment.NewLine +
                           _tabs + "  <td>" + _num_terminals_label + "</td>" + Environment.NewLine +
                           _tabs + "  <td align=\"right\">" + _prov_stat.num_terminals + "</td>" + Environment.NewLine +
                           _tabs + "</tr>" + Environment.NewLine +
                           _tabs + "<tr>" + Environment.NewLine +
                           _tabs + "  <td>" + _hold_label + "</td>" + Environment.NewLine +
                           _tabs + "  <td align=\"right\">" + _hold_str + "</td>" + Environment.NewLine +
                           _tabs + "</tr>" + Environment.NewLine;


        _total_stats.played += _prov_stat.played;
        _total_stats.netwin += _prov_stat.netwin;
        _total_stats.num_terminals += _prov_stat.num_terminals;
      }

      _netwin_per_terminal = (_total_stats.num_terminals > 0) ? _total_stats.netwin / _total_stats.num_terminals : 0;
      _hold = (_total_stats.played > 0) ? _total_stats.netwin / _total_stats.played : 0;
      _hold_str = _hold.ToString();

      _provider_stats += _tabs + "<tr>" + Environment.NewLine +
                         _tabs + "  <td colspan=\"2\"><b>" + _total_stats.name + "</b></td>" + Environment.NewLine +
                         _tabs + "</tr>" + Environment.NewLine +
                         _tabs + "<tr>" + Environment.NewLine +
                         _tabs + "  <td>" + _played_label + "</td>" + Environment.NewLine +
                         _tabs + "  <td align=\"right\">" + _total_stats.played + "</td>" + Environment.NewLine +
                         _tabs + "</tr>" + Environment.NewLine +
                         _tabs + "<tr>" + Environment.NewLine +
                         _tabs + "  <td>" + _netwin_label + "</td>" + Environment.NewLine +
                         _tabs + "  <td align=\"right\">" + _total_stats.netwin + "</td>" + Environment.NewLine +
                         _tabs + "</tr>" + Environment.NewLine +
                         _tabs + "<tr>" + Environment.NewLine +
                         _tabs + "  <td>" + _netwin_per_terminal_label + "</td>" + Environment.NewLine +
                         _tabs + "  <td align=\"right\">" + _netwin_per_terminal + "</td>" + Environment.NewLine +
                         _tabs + "</tr>" + Environment.NewLine +
                         _tabs + "<tr>" + Environment.NewLine +
                         _tabs + "  <td>" + _num_terminals_label + "</td>" + Environment.NewLine +
                         _tabs + "  <td align=\"right\">" + _total_stats.num_terminals + "</td>" + Environment.NewLine +
                         _tabs + "</tr>" + Environment.NewLine +
                         _tabs + "<tr>" + Environment.NewLine +
                         _tabs + "  <td>" + _hold_label + "</td>" + Environment.NewLine +
                         _tabs + "  <td align=\"right\">" + _hold_str + "</td>" + Environment.NewLine +
                         _tabs + "</tr>" + Environment.NewLine;

      SetParameterValue("@PROVIDER_STATS", _provider_stats);

      _total_machine_stats += _tabs + "<tr>" + Environment.NewLine +
                                _tabs + "  <td>" + _played_label + "</td>" + Environment.NewLine +
                                _tabs + "  <td align=\"right\">" + _total_stats.played + "</td>" + Environment.NewLine +
                                _tabs + "</tr>" + Environment.NewLine +
                                _tabs + "<tr>" + Environment.NewLine +
                                _tabs + "  <td>" + _netwin_label + "</td>" + Environment.NewLine +
                                _tabs + "  <td align=\"right\">" + _total_stats.netwin + "</td>" + Environment.NewLine +
                                _tabs + "</tr>" + Environment.NewLine +
                                _tabs + "<tr>" + Environment.NewLine +
                                _tabs + "  <td>" + _netwin_per_terminal_label + "</td>" + Environment.NewLine +
                                _tabs + "  <td align=\"right\">" + _netwin_per_terminal + "</td>" + Environment.NewLine +
                                _tabs + "</tr>" + Environment.NewLine +
                                _tabs + "<tr>" + Environment.NewLine +
                                _tabs + "  <td>" + _num_terminals_label + "</td>" + Environment.NewLine +
                                _tabs + "  <td align=\"right\">" + _total_stats.num_terminals + "</td>" + Environment.NewLine +
                                _tabs + "</tr>" + Environment.NewLine +
                                _tabs + "<tr>" + Environment.NewLine +
                                _tabs + "  <td>" + _hold_label + "</td>" + Environment.NewLine +
                                _tabs + "  <td align=\"right\">" + _hold_str + "</td>" + Environment.NewLine +
                                _tabs + "</tr>" + Environment.NewLine;

      SetParameterValue("@TOTAL_MACHINES_STATS", _total_machine_stats);

      _gaming_table_status += _tabs + "<tr>" + Environment.NewLine +
                              _tabs + "  <td>" + Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_IN") + "</td>" + Environment.NewLine +
                              _tabs + "  <td align=\"right\">" + MailingReport.chips_sale + "</td>" + Environment.NewLine +
                              _tabs + "</tr>" + Environment.NewLine +
                              _tabs + "<tr>" + Environment.NewLine +
                              _tabs + "  <td>" + Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_OUT") + "</td>" + Environment.NewLine +
                              _tabs + "  <td align=\"right\">" + MailingReport.chips_purchase + "</td>" + Environment.NewLine +
                              _tabs + "</tr>" + Environment.NewLine +
                              _tabs + "<tr>" + Environment.NewLine +
                              _tabs + "  <td>" + Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT") + "</td>" + Environment.NewLine +
                              _tabs + "  <td align=\"right\">" + MailingReport.game_profit + "</td>" + Environment.NewLine +
                              _tabs + "</tr>" + Environment.NewLine;

      SetParameterValue("@TOTAL_GAMING_TABLE_STATS", _gaming_table_status);
    } // MailingReport

    #endregion // Constructor

  } // MailingReport

} // WSI.Common
