//------------------------------------------------------------------------------
// Copyright © 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SqlStatistics.cs
// 
//   DESCRIPTION: Class to manage SQL Statistics
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 28-DEC-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-DEC-2010 RCI    First release.
// 13-APR-2012 MPO    Defect 219: Filter by site using a new parameter --> GetNumTerminalsConnectedByProvider and GetNetWinByProvider
// 28-NOV-2012 RRB    Added routine GetNumWinTerminalsConnectedByDay.
// 20-FEB-2013 RRB    ConnectedTerminals have in account the terminal type (for the providers).
// 28-MAR-2013 HBB & RCI    Stats: Calculate and show the theoretical payout %.
// 17-MAY-2013 JCA    Update for  MasterTablesMode
// 30-MAY-2013 JCA    Update In Center  PROVIDERS_GAMES table is GAMES. Also add CONST GAME_ID_FOR_GROUPS 
// 30-JUL-2014 XCD & LEM    Fixed Bug WIG-1132: Wrong first day week in GetQuerySalesByDateAndProvider 
// 18-AUG-2014 AMF    Progressive Jackpot
// 03-SEP-2014 LEM    Add TE_TERMINAL_ID to queries with terminals info.
// 01-OCT-2014 LEM    Fixed Bug WIG-1367: Don't find TE_TERMINAL_ID in Provider-Terminals subqueries 
// 07-OCT-2014 ACC    Fixed bug WIG-1431: Estadísticas: Error subtotal proveedor y fecha en el netwin
// 25-MAR-2015 FOS    TASK 727: Terminals Conected, change Terminal_id for master_id
// 26-MAR-2015 ANM    TerminalList is always a query
// 10-JUN-2015 DHA    Error when bigger numbers of money type
// 15-JUN-2015 FAV    WIG-2381: Added GetTerminalsPlayedByProvider method
// 19-MAY-2016 EOR    Product Backlog Item 12810:(PHASE 2) Activity in Tables: Mailing
// 13-JUN-2016 FAV    Fixed Bug 12538: Logrand - Daily Report with wrong machines number
// 17-OCT-2016 LTC    Bug 17755:502 Meier - KEOPS - Wrong Machine quantity in CashInformation report
// 19-DEC-2016 ETP    Bug 21548: Provider activity is not showed in mailing
// 25-MAY-2017 DPC    Bug 21257:Error de cálculo tito netwin
// 24-JUL-2017 RAB    PBI 28627: WIGOS-3193 Score report - Report - Send by email
// 03-AUG-2017 RAB    Bug 29168:WIGOS-4198 Score report - The amount of Final drop sum all drop amounts. It is the same than Total Drop
// 07-AUG-2017 RAB    Bug 29129:WIGOS-4131 Score report - The score report received by e-mail shows a incorrect Win/Loss amount
// 09-AUG-2017 RAB    Bug 29293:WIGOS-4303 'Hourly score report' reports an incorrect Drop
// 16-SEP-2017 DPC    [WIGOS-4274]: [Ticket #855] Error encontrado Estadísticas - Progresivos
// 15-JUN-2018 GDA    Bug 33063 - WIGOS-12243 - [Ticket #14517] Fallo – Reporte de Estadística agrupada por fecha, proveedor, terminal- Jugadas Version V03.08.0001
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace WSI.Common
{
  public static class SqlStatistics
  {
    #region CONSTANTS
    public const int GAME_ID_FOR_GROUPS = -1;
    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Return SQL Query to get the sales statistics by provider
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - Boolean HasTerminal
    //          - Boolean OnlyOneTerminal
    //          - String TerminalList
    //          - Boolean HasGame
    //          - String GameId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String: SQL Query
    //
    public static String GetQuerySalesByProvider(DateTime DateFrom,
                                                 DateTime DateTo,
                                                 Boolean HasTerminal,
                                                 Boolean OnlyOneTerminal,
                                                 String TerminalList,
                                                 Boolean HasGame,
                                                 String GameId)
    {
      String _sql_txt;
      String _str_where;

      _str_where = "";

      if (DateFrom != DateTime.MinValue)
      {
        _str_where += " AND (SPH_BASE_HOUR >= " + GUI_FormatDateDB(DateFrom) + ") ";
      }
      if (DateTo != DateTime.MaxValue)
      {
        _str_where += " AND (SPH_BASE_HOUR < " + GUI_FormatDateDB(DateTo) + ") ";
      }
      if (HasTerminal)
      {
        if (OnlyOneTerminal)
        {
          _str_where = _str_where + " AND SPH_TERMINAL_ID = " + TerminalList;
        }
        else
        {
          _str_where = _str_where + " AND SPH_TERMINAL_ID IN " + TerminalList;
        }
      }
      if (HasGame)
      {
        _str_where = _str_where + " AND SPH_GAME_ID = " + GameId;
      }
      if (!String.IsNullOrEmpty(_str_where))
      {
        _str_where = _str_where.Substring(5);
        _str_where = " WHERE " + _str_where;
      }

      _sql_txt = "SELECT   Provider " +
                "       , SUM(PlayedAmount) AS PAmount " +
                "       , SUM(WonAmount) AS WAmount " +
                "       , CASE SUM(x.PlayedAmount) WHEN 0 THEN 0 ELSE ((SUM(WonAmount) +  SUM(ProgressiveProvisionAmount) - SUM(ProgressiveJackpotAmount0))*100/SUM(x.PlayedAmount)) END as AmountPerCent " +
                "       , SUM(PlayedAmount) - (SUM(WonAmount) +  SUM(ProgressiveProvisionAmount) - SUM(ProgressiveJackpotAmount0)) as Netwin " +
                "       , CASE SUM(x.PlayedAmount) WHEN 0 THEN 0 ELSE ((SUM(PlayedAmount) - (SUM(WonAmount) +  SUM(ProgressiveProvisionAmount) - SUM(ProgressiveJackpotAmount0)))*100/SUM(x.PlayedAmount)) END as NetwinPc " +
                "       , SUM(PlayedCount) AS PCount " +
                "       , SUM(WonCount) AS WCount " +
                "       , CASE SUM(x.PlayedCount) WHEN 0 THEN 0 ELSE ((SUM(x.WonCount) * 100.0) / SUM(x.PlayedCount)) END AS CountPerCent " +
                "  FROM   (SELECT   SPH_TERMINAL_ID " +
                "                 , SUM(SPH_PLAYED_AMOUNT) AS PlayedAmount " +
                "                 , SUM(SPH_WON_AMOUNT) AS WonAmount " +
                "                 , CASE WHEN SUM(SPH_WON_COUNT)>SUM(SPH_PLAYED_COUNT) THEN SUM(SPH_WON_COUNT) ELSE SUM(SPH_PLAYED_COUNT) END AS PlayedCount " +
                "                 , SUM(SPH_WON_COUNT) AS WonCount " +
                "                 , SUM(ISNULL(SPH_PROGRESSIVE_PROVISION_AMOUNT,0)) AS ProgressiveProvisionAmount " +
                "                 , SUM(ISNULL(SPH_JACKPOT_AMOUNT,0) - ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS NonProgressiveJackpotAmount " +
                "                 , SUM(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS ProgressiveJackpotAmount " +
                "                 , SUM(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)) AS ProgressiveJackpotAmount0 " +
                "                 , (SELECT   TE_PROVIDER_ID " +
                "                      FROM   TERMINALS " +
                "                     WHERE   TE_TERMINAL_ID = SALES_PER_HOUR_V2.SPH_TERMINAL_ID " +
                "                   ) AS Provider " +
                "                 , MIN(SPH_GAME_ID) AS MinGameId " +
                "                 , MAX(SPH_GAME_ID) AS MaxGameId " +
                "                 , (SELECT   PG_GAME_NAME AS GM_NAME " +
                "                      FROM   PROVIDERS_GAMES " +
                "                     WHERE   PG_GAME_ID = MIN(SALES_PER_HOUR_V2.SPH_GAME_ID) " +
                "                   ) AS MinGameName " +
                "            FROM   SALES_PER_HOUR_V2 " +
                _str_where +
                "          GROUP BY SPH_TERMINAL_ID " +
                "         ) AS x " +
                "GROUP BY Provider " +
                "ORDER BY Provider ";

      return _sql_txt;
    } // GetQuerySalesByProvider

    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the sales statistics by provider
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetSalesByProvider(DateTime DateFrom,
                                               DateTime DateTo,
                                               SqlTransaction SqlTrx)
    {
      String _sql_txt;
      String[] _pk_name = { "Provider" };

      _sql_txt = GetQuerySalesByProvider(DateFrom, DateTo, true, false,
                                         "(SELECT   TE_TERMINAL_ID " +
                                         "   FROM   TERMINALS " +
                                         "  WHERE   TE_TERMINAL_TYPE IN " +
                                         "            (" + (Int16)TerminalTypes.WIN +
                                         "           , " + (Int16)TerminalTypes.T3GS +
                                         "           , " + (Int16)TerminalTypes.SAS_HOST +
                                         "            )" +
                                         ")",
                                         false, "");

      return GetDataTableFromQuery("PROVIDERS", _pk_name, _sql_txt, SqlTrx);
    } // GetSalesByProvider

    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the number of connected terminals by provider
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetNumTerminalsConnectedByProvider(DateTime DateFrom,
                                                               DateTime DateTo,
                                                               Boolean FilterByTerminalSite,
                                                               SqlTransaction SqlTrx)
    {
      StringBuilder _sql_sb;
      String _str_where_dates;
      DateTime _opening_from;
      DateTime _opening_to;
      String[] _pk_name;

      _pk_name = new String[1];
      _pk_name.SetValue("Provider", 0);
      _sql_sb = new StringBuilder();

      if (FilterByTerminalSite)
      {

        _sql_sb.AppendLine("SELECT    TOP 1 TE_PROVIDER_ID AS Provider ");
        _sql_sb.AppendLine("        , 1                    AS NumTerminals ");
        _sql_sb.AppendLine("  FROM   TERMINALS ");
        _sql_sb.AppendLine(" WHERE   TE_TERMINAL_TYPE  = " + (Int16)TerminalTypes.SITE);
        _sql_sb.AppendLine(" ORDER BY TE_PROVIDER_ID ");

      }
      else
      {

        _opening_from = DateTime.MinValue;
        _opening_to = DateTime.MaxValue;

        if (DateFrom != DateTime.MinValue)
        {
          _opening_from = Misc.Opening(DateFrom);
          // Eliminate the Minutes part.
          _opening_from = _opening_from.AddMinutes(-_opening_from.Minute);
        }
        if (DateTo != DateTime.MaxValue)
        {
          _opening_to = Misc.Opening(DateTo);
          // Eliminate the Minutes part.
          _opening_to = _opening_to.AddMinutes(-_opening_to.Minute);
          if (DateTo == _opening_to)
          {
            _opening_to = _opening_to.AddDays(-1);
          }
        }
        // LTC 17-OCT-2016
        _str_where_dates = "";
        if (_opening_from != DateTime.MinValue)
        {
          _str_where_dates += " AND TC.TC_DATE >= " + GUI_FormatDateDB(_opening_from.Date) + " ";
        }
        if (_opening_to != DateTime.MaxValue)
        {
          _str_where_dates += " AND TC.TC_DATE <= " + GUI_FormatDateDB(_opening_to.Date) + " ";
        }

        _sql_sb.AppendLine("SELECT                                      ");
        _sql_sb.AppendLine("    A.TE_PROVIDER_ID AS Provider,           ");
        _sql_sb.AppendLine("    COUNT(A.te_terminal_id) AS NumTerminals ");
        _sql_sb.AppendLine("FROM                                        ");
        _sql_sb.AppendLine("(                                           ");
        _sql_sb.AppendLine("  SELECT DISTINCT TE.TE_PROVIDER_ID,        ");
        _sql_sb.AppendLine("                  TE.te_terminal_id         ");
        _sql_sb.AppendLine("  FROM   TERMINALS_CONNECTED TC             ");
        _sql_sb.AppendLine("  INNER JOIN TERMINALS TE                   ");
        _sql_sb.AppendLine("    ON TE.TE_TERMINAL_ID = TC.TC_TERMINAL_ID");
        _sql_sb.AppendLine("  WHERE TC.TC_CONNECTED      = 1            ");
        _sql_sb.AppendLine("    AND TE.TE_TYPE           = 1            ");
        _sql_sb.AppendLine("    AND TE.TE_TERMINAL_TYPE IN              ");
        _sql_sb.AppendLine("    (                                       ");
        _sql_sb.AppendLine("            " + (Int16)TerminalTypes.WIN);
        _sql_sb.AppendLine("          , " + (Int16)TerminalTypes.T3GS);
        _sql_sb.AppendLine("          , " + (Int16)TerminalTypes.SAS_HOST);
        _sql_sb.AppendLine("           )");
        _sql_sb.AppendLine(_str_where_dates);
        _sql_sb.AppendLine(")AS A                                       ");
        _sql_sb.AppendLine("GROUP BY A.TE_PROVIDER_ID                   ");
                
      }

      return GetDataTableFromQuery("PROVIDERS", _pk_name, _sql_sb.ToString(), SqlTrx);
    } // GetNumTerminalsConnectedByProvider

    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the netwin by provider
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetNetWinByProvider(DateTime DateFrom,
                                                DateTime DateTo,
                                                Boolean FilterByTerminalSite,
                                                SqlTransaction SqlTrx)
    {
      StringBuilder _sql_sb;
      String _str_where_dates;
      String _str_terminal_type;
      String[] _pk_name;

      _pk_name = new String[1];
      _pk_name.SetValue("Provider", 0);
      _sql_sb = new StringBuilder();
      _str_where_dates = "";
      _str_terminal_type = "";

      if (DateFrom != DateTime.MinValue)
      {
        _str_where_dates += " AND PS_FINISHED >= " + GUI_FormatDateDB(DateFrom) + " ";
      }
      if (DateTo != DateTime.MaxValue)
      {
        _str_where_dates += " AND PS_FINISHED < " + GUI_FormatDateDB(DateTo) + " ";
      }

      if (FilterByTerminalSite)
      {
        _str_terminal_type = Convert.ToString((Int16)TerminalTypes.SITE);
      }
      else
      {
        _str_terminal_type = (Int16)TerminalTypes.WIN + " , " + (Int16)TerminalTypes.T3GS + " , " + (Int16)TerminalTypes.SAS_HOST;
      }

      _sql_sb.AppendLine("SELECT   Provider ");
      _sql_sb.AppendLine("       , SUM(cash_in)  AS CashIn ");
      _sql_sb.AppendLine("       , SUM(cash_out) AS CashOut ");
      _sql_sb.AppendLine("  FROM   (SELECT   SUM(ps_total_cash_in)  AS cash_in ");
      _sql_sb.AppendLine("                 , SUM(ps_total_cash_out) AS cash_out ");
      _sql_sb.AppendLine("                 , (SELECT   te_provider_id ");
      _sql_sb.AppendLine("                      FROM   terminals ");
      _sql_sb.AppendLine("                     WHERE   te_terminal_id = PLAY_SESSIONS.PS_terminal_id ");
      _sql_sb.AppendLine("                   ) AS Provider ");
      _sql_sb.AppendLine("            FROM   PLAY_SESSIONS ");
      _sql_sb.AppendLine("           WHERE   PS_STATUS   <> 0 ");
      _sql_sb.AppendLine("             AND   PS_PROMO    =  0 ");
      _sql_sb.AppendLine("         " + _str_where_dates);
      _sql_sb.AppendLine("             AND   PS_TERMINAL_ID IN (SELECT   te_terminal_id ");
      _sql_sb.AppendLine("                                        FROM   terminals ");
      _sql_sb.AppendLine("                                       WHERE   te_terminal_type IN (" + _str_terminal_type + ")");
      _sql_sb.AppendLine("                                     ) ");
      _sql_sb.AppendLine("          GROUP BY PS_TERMINAL_ID ");
      _sql_sb.AppendLine("         ) AS x ");
      _sql_sb.AppendLine("GROUP BY Provider ");
      _sql_sb.AppendLine("ORDER BY Provider ");

      return GetDataTableFromQuery("PROVIDERS", _pk_name, _sql_sb.ToString(), SqlTrx);
    } // GetNetWinByProvider

    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the sales games info
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetGamesInfoSales(DateTime DateFrom,
                                              DateTime DateTo,
                                              SqlTransaction SqlTrx)
    {
      DataTable _dt_games;
      DataTable _dt_jackpot;
      DataTable _dt_games_info_sales;
      Decimal _jackpot_contrib;
      Int32 _site_id;
      String _site_name;

      _jackpot_contrib = DB_GetJackpotContributionPct(SqlTrx);
      Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier", SqlTrx), out _site_id);
      _site_name = Misc.ReadGeneralParams("Site", "Name", SqlTrx);

      _dt_games_info_sales = new DataTable("GAMES");

      _dt_games_info_sales.Columns.Add("SiteIdentifier", Type.GetType("System.Int32")).DefaultValue = _site_id;
      _dt_games_info_sales.Columns.Add("SiteName", Type.GetType("System.String")).DefaultValue = _site_name;
      _dt_games_info_sales.Columns.Add("Game", Type.GetType("System.String"));
      _dt_games_info_sales.Columns.Add("Date", Type.GetType("System.DateTime"));
      _dt_games_info_sales.Columns.Add("PlayedAmount", Type.GetType("System.Decimal"));
      _dt_games_info_sales.Columns.Add("WonAmount", Type.GetType("System.Decimal"));
      _dt_games_info_sales.Columns.Add("PlayedCount", Type.GetType("System.Int64"));
      _dt_games_info_sales.Columns.Add("WonCount", Type.GetType("System.Int64"));
      // NumTerminals is the number of distinct terminals that has sales of the game.
      _dt_games_info_sales.Columns.Add("NumTerminals", Type.GetType("System.Int32"));
      _dt_games_info_sales.Columns.Add("Jackpot", Type.GetType("System.Decimal")).DefaultValue = 0;
      _dt_games_info_sales.Columns.Add("JackpotContributionPct", Type.GetType("System.Decimal")).DefaultValue = _jackpot_contrib;

      _dt_games_info_sales.PrimaryKey = new DataColumn[] { _dt_games_info_sales.Columns["Game"],
                                                           _dt_games_info_sales.Columns["Date"] };

      _dt_games = GetSalesByGameAndDay(DateFrom, DateTo, SqlTrx);
      if (_dt_games == null)
      {
        Log.Error(" GetGamesInfoSales: Can't obtain SalesByGameAndDay.");

        return null;
      }

      _dt_jackpot = GetJackpotsByGameAndDay(DateFrom, DateTo, SqlTrx);
      if (_dt_jackpot == null)
      {
        Log.Error(" GetGamesInfoSales: Can't obtain JackpotsByGameAndDay.");

        return null;
      }

      _dt_games_info_sales.Merge(_dt_games);
      _dt_games_info_sales.Merge(_dt_jackpot);

      return _dt_games_info_sales;
    } // GetGamesInfoSales

    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the number of connected WIN-terminals by day
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetNumWinTerminalsConnectedByDay(DateTime DateFrom,
                                                             DateTime DateTo,
                                                             SqlTransaction SqlTrx)
    {
      String _sql_txt;
      String _str_where_dates;
      DateTime _opening_from;
      DateTime _opening_to;
      String[] _pk_name = { "Date" };
      Int32 _init_hour;
      String _str_date;
      String _str_since_days;
      String _str_since;

      _opening_from = DateTime.MinValue;
      _opening_to = DateTime.MaxValue;

      if (DateFrom != DateTime.MinValue)
      {
        _opening_from = Misc.Opening(DateFrom);
        // Eliminate the Minutes part.
        _opening_from = _opening_from.AddMinutes(-_opening_from.Minute);
      }
      if (DateTo != DateTime.MaxValue)
      {
        _opening_to = Misc.Opening(DateTo);
        // Eliminate the Minutes part.
        _opening_to = _opening_to.AddMinutes(-_opening_to.Minute);
        if (DateTo == _opening_to)
        {
          _opening_to = _opening_to.AddDays(-1);
        }
      }

      _init_hour = Misc.TodayOpening().Hour;
      _str_date = "01-01-2007 " + _init_hour.ToString("00") + ":00:00:000";
      _str_since_days = "DATEDIFF(HOUR, '" + _str_date + "', DATEADD(HOUR, " + _init_hour + ", TC_DATE)) / 24";
      _str_since = "DATEADD(DAY, " + _str_since_days + ", '" + _str_date + "')";

      _str_where_dates = "";
      if (_opening_from != DateTime.MinValue)
      {
        _str_where_dates += " AND TC_DATE >= " + GUI_FormatDateDB(_opening_from.Date) + " ";
      }
      if (_opening_to != DateTime.MaxValue)
      {
        _str_where_dates += " AND TC_DATE <= " + GUI_FormatDateDB(_opening_to.Date) + " ";
      }

      _sql_txt = "SELECT   " + _str_since + "   AS Date " +
                 "       , COUNT(*)             AS NumTerminals " +
                 "  FROM   TERMINALS_CONNECTED " +
                 "       , TERMINALS " +
                 " WHERE   TC_TERMINAL_ID    = TE_TERMINAL_ID " +
                 "   AND   TC_CONNECTED      = 1 " +
                 "   AND   TE_TYPE           = 1 " +
                 "   AND   TE_TERMINAL_TYPE  = " + (Int16)TerminalTypes.WIN +
                 _str_where_dates +
                 "GROUP BY " + _str_since_days +
                 "ORDER BY " + _str_since_days;

      return GetDataTableFromQuery("CONNECTED", _pk_name, _sql_txt, SqlTrx);
    } // GetNumWinTerminalsConnectedByDay

    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the number of connected terminals by provider and day
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetNumTerminalsConnectedByProviderAndDay(DateTime DateFrom,
                                                                     DateTime DateTo,
                                                                     SqlTransaction SqlTrx)
    {
      String _sql_txt;
      String _str_where_dates;
      DateTime _opening_from;
      DateTime _opening_to;
      String[] _pk_name = { "Provider", "TerminalType", "Date" };
      Int32 _site_id;
      String _site_name;
      Int32 _init_hour;
      String _str_date;
      String _str_since_days;
      String _str_since;

      _opening_from = DateTime.MinValue;
      _opening_to = DateTime.MaxValue;

      if (DateFrom != DateTime.MinValue)
      {
        _opening_from = Misc.Opening(DateFrom);
        // Eliminate the Minutes part.
        _opening_from = _opening_from.AddMinutes(-_opening_from.Minute);
      }
      if (DateTo != DateTime.MaxValue)
      {
        _opening_to = Misc.Opening(DateTo);
        // Eliminate the Minutes part.
        _opening_to = _opening_to.AddMinutes(-_opening_to.Minute);
        if (DateTo == _opening_to)
        {
          _opening_to = _opening_to.AddDays(-1);
        }
      }

      Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier", SqlTrx), out _site_id);
      _site_name = Misc.ReadGeneralParams("Site", "Name", SqlTrx);

      _init_hour = Misc.TodayOpening().Hour;
      _str_date = "01-01-2007 " + _init_hour.ToString("00") + ":00:00:000";
      _str_since_days = "DATEDIFF(HOUR, '" + _str_date + "', DATEADD(HOUR, " + _init_hour + ", TC_DATE)) / 24";
      _str_since = "DATEADD(DAY, " + _str_since_days + ", '" + _str_date + "')";

      _str_where_dates = "";
      if (_opening_from != DateTime.MinValue)
      {
        _str_where_dates += " AND TC_DATE >= " + GUI_FormatDateDB(_opening_from.Date) + " ";
      }
      if (_opening_to != DateTime.MaxValue)
      {
        _str_where_dates += " AND TC_DATE <= " + GUI_FormatDateDB(_opening_to.Date) + " ";
      }

      _sql_txt = "SELECT   " + _site_id + "     AS SiteIdentifier " +
                 "       , '" + _site_name + "' AS SiteName " +
                 "       , TE_PROVIDER_ID       AS Provider " +
                 "       , TE_TERMINAL_TYPE     AS TerminalType " +
                 "       , " + _str_since + "   AS Date " +
                 "       , COUNT(*)             AS NumTerminals " +
                 "  FROM   TERMINALS_CONNECTED " +
                 "       , TERMINALS " +
                 " WHERE   TC_TERMINAL_ID    = TE_TERMINAL_ID " +
                 "   AND   TC_CONNECTED      = 1 " +
                 "   AND   TE_TYPE           = 1 " +
                 "   AND   TE_TERMINAL_TYPE IN " +
                 "           (" + (Int16)TerminalTypes.WIN +
                 "          , " + (Int16)TerminalTypes.T3GS +
                 "          , " + (Int16)TerminalTypes.SAS_HOST +
                 "           )" +
                 _str_where_dates +
                 "GROUP BY TE_PROVIDER_ID, TE_TERMINAL_TYPE, " + _str_since_days +
                 "ORDER BY TE_PROVIDER_ID, TE_TERMINAL_TYPE, " + _str_since_days;

      return GetDataTableFromQuery("PROVIDERS", _pk_name, _sql_txt, SqlTrx);
    } // GetNumTerminalsConnectedByProviderAndDay

    // EOR 19-MAY-2016
    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the activity tables by date
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime YearMonth
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetActivityTables(DateTime YearMonth,
                                              SqlTransaction SqlTrx)
    {
      SqlParameter[] _sql_parameters = { new SqlParameter("@pDate", YearMonth) };

      return GetDataTableFromStored("ACTIVITY", _sql_parameters, "sp_GetTablesActivity", SqlTrx);
    } // GetActivityTables

    // FAV 15-JUN-2015
    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the number of connected terminals by provider and day
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - bool     GroupByTerminalType,
    //          - SqlTransaction SqlTrx
    //          - List<TerminalTypes> TerminalTypeList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetNumTerminalsConnectedByProviderAndDay(DateTime DateFrom,
                                                                     DateTime DateTo,
                                                                     bool GroupByTerminalType,
                                                                     SqlTransaction SqlTrx,
                                                                     List<TerminalTypes> TerminalTypeList = null)
    {
        String _sql_txt;
        String _str_where_dates;
        DateTime _opening_from;
        DateTime _opening_to;
        String[] _pk_name = { "Provider", "TerminalType", "Date" };
        Int32 _site_id;
        String _site_name;
        Int32 _init_hour;
        String _str_date;
        String _str_since_days;
        String _str_since;
        String _str_terminal_type;

        _opening_from = DateTime.MinValue;
        _opening_to = DateTime.MaxValue;

        if (DateFrom != DateTime.MinValue)
        {
            _opening_from = Misc.Opening(DateFrom);
            // Eliminate the Minutes part.
            _opening_from = _opening_from.AddMinutes(-_opening_from.Minute);
        }
        if (DateTo != DateTime.MaxValue)
        {
            _opening_to = Misc.Opening(DateTo);
            // Eliminate the Minutes part.
            _opening_to = _opening_to.AddMinutes(-_opening_to.Minute);
            if (DateTo == _opening_to)
            {
                _opening_to = _opening_to.AddDays(-1);
            }
        }

        if (TerminalTypeList != null && TerminalTypeList.Count > 0)
        {
            _str_terminal_type = string.Join(" , ", TerminalTypeList.ConvertAll(x => ((int)x).ToString()).ToArray());
        }
        else
        {
            // default values for Terminal Types
            _str_terminal_type = (Int16)TerminalTypes.WIN + " , " + (Int16)TerminalTypes.T3GS + " , " + (Int16)TerminalTypes.SAS_HOST;
        }

        Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier", SqlTrx), out _site_id);
        _site_name = Misc.ReadGeneralParams("Site", "Name", SqlTrx);

        _init_hour = Misc.TodayOpening().Hour;
        _str_date = "01-01-2007 " + _init_hour.ToString("00") + ":00:00:000";
        _str_since_days = "DATEDIFF(HOUR, '" + _str_date + "', DATEADD(HOUR, " + _init_hour + ", TC_DATE)) / 24";
        _str_since = "DATEADD(DAY, " + _str_since_days + ", '" + _str_date + "')";

        _str_where_dates = "";
        if (_opening_from != DateTime.MinValue)
        {
            _str_where_dates += " AND TC_DATE >= " + GUI_FormatDateDB(_opening_from.Date) + " ";
        }
        if (_opening_to != DateTime.MaxValue)
        {
            _str_where_dates += " AND TC_DATE <= " + GUI_FormatDateDB(_opening_to.Date) + " ";
        }

        _sql_txt = "SELECT   " + _site_id + "     AS SiteIdentifier " +
                   "       , '" + _site_name + "' AS SiteName " +
                   "       , TE_PROVIDER_ID       AS Provider " +
                   (GroupByTerminalType ? "       , TE_TERMINAL_TYPE     AS TerminalType ": "") +
                   "       , " + _str_since + "   AS Date " +
                   "       , COUNT(*)             AS NumTerminals " +
                   "  FROM   TERMINALS_CONNECTED " +
                   "       , TERMINALS " +
                   " WHERE   TC_TERMINAL_ID    = TE_TERMINAL_ID " +
                   "   AND   TC_CONNECTED      = 1 " +
                   "   AND   TE_TYPE           = 1 " +
                   "   AND   TE_TERMINAL_TYPE IN  (" + _str_terminal_type +  ")" +
                   _str_where_dates +
                   "GROUP BY TE_PROVIDER_ID, " + (GroupByTerminalType ? "TE_TERMINAL_TYPE, " : "")  + _str_since_days +
                   "ORDER BY TE_PROVIDER_ID, " + (GroupByTerminalType ? "TE_TERMINAL_TYPE, " : "") + _str_since_days;

        return GetDataTableFromQuery("PROVIDERS", _pk_name, _sql_txt, SqlTrx);
    } // GetNumTerminalsConnectedByProviderAndDay

    //------------------------------------------------------------------------------
    // PURPOSE : Return SQL Query to get the sales statistics by date (day, week or month) and provider
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - GroupDateFilter DateFilter
    //          - QuerySalesProviderFilter SalesProviderFilter
    //          - Boolean HasTerminal
    //          - Boolean OnlyOneTerminal
    //          - String TerminalList
    //          - Boolean HasGame
    //          - String GameId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String: SQL Query
    //
    public static String GetQuerySalesByDateAndProvider(DateTime FromDate,
                                                        DateTime ToDate,
                                                        GroupDateFilter DateFilter,
                                                        QuerySalesProviderFilter SalesProviderFilter,
                                                        Boolean HasTerminal,
                                                        Boolean OnlyOneTerminal,
                                                        String TerminalList,
                                                        Boolean HasGame,
                                                        String GameId,
                                                        Boolean IsCenter,
                                                        String SiteId,
                                                        Int16 MasterTablesMode)
    {
      String _sql_txt;
      String _str_where;
      Int32 _init_hour;
      String _str_date;
      String _str_date_time;
      String _str_date_days;
      String _str_date_weeks;
      String _str_date_months;
      String _str_since;
      String _str_until;
      String _str_provider;
      String _str_terminal;
      String _str_terminal_id;
      String _str_min_game;
      String _str_max_game;
      String _str_min_game_name;
      String _str_left_join_terminals;
      String _group_by;
      String _order_by;
      String _str_select_sites;
      DateTime _first_date;
      Int32 _offset_days;

      //NO_FILTER = 0,
      //DATE_PROVIDER_TERMINAL = 1,
      //DATE_PROVIDER = 2,
      //PROVIDER_TERMINAL = 3,
      //PROVIDER = 4,
      //DATE = 5

      if (SalesProviderFilter == QuerySalesProviderFilter.NO_FILTER)
      {
        return "";
      }

      _str_where = "";
      _str_since = "";
      _str_until = "";
      _str_provider = "''";
      _str_terminal = "''";
      _str_terminal_id = "''";
      _str_min_game = "0";
      _str_max_game = "0";
      _str_min_game_name = "''";

      _str_left_join_terminals = " LEFT OUTER JOIN TERMINALS " +
                                 " ON SPH_TERMINAL_ID = TE_TERMINAL_ID ";
      if (IsCenter)
      {
        _str_left_join_terminals += " AND SITE_ID = TE_SITE_ID ";
        _str_select_sites = " SITE_ID ";
      }
      else
      {
        _str_select_sites = " 0 ";
      }

      _group_by = "";
      _order_by = "x.SiteId";

      if (FromDate != DateTime.MinValue)
      {
        _str_where += " AND (SPH_BASE_HOUR >= " + GUI_FormatDateDB(FromDate) + ") ";
      }
      if (ToDate != DateTime.MaxValue)
      {
        _str_where += " AND (SPH_BASE_HOUR < " + GUI_FormatDateDB(ToDate) + ") ";
      }
      if (HasTerminal)
      {
        _str_where = _str_where + " AND SPH_TERMINAL_ID IN " + TerminalList;
      }
      if (HasGame)
      {
        _str_where = _str_where + " AND SPH_GAME_ID = " + GameId;
      }
      if (IsCenter && !String.IsNullOrEmpty(SiteId))
      {
        _str_where += " AND SITE_ID IN (" + SiteId + ") ";
      }

      if (!String.IsNullOrEmpty(_str_where))
      {
        _str_where = _str_where.Substring(5);
        _str_where = " WHERE " + _str_where;
      }

      if (SalesProviderFilter == QuerySalesProviderFilter.DATE ||
          SalesProviderFilter == QuerySalesProviderFilter.DATE_PROVIDER ||
          SalesProviderFilter == QuerySalesProviderFilter.DATE_PROVIDER_TERMINAL)
      {
        _first_date = WGDB.MinDate;

        if (DateFilter == GroupDateFilter.WEEKLY)
        {
          _offset_days = (Int32)(CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek - _first_date.DayOfWeek);
          _offset_days = (_offset_days + 7) % 7;

          _first_date = _first_date.AddDays(_offset_days);
        }

        _str_date = _first_date.ToString("yyyy-MM-dd");
        _init_hour = Misc.TodayOpening().Hour;
        _str_date_time = _str_date + "T" + String.Format("{0:00}:00:00", _init_hour);

        switch (DateFilter)
        {
          case GroupDateFilter.HOURLY:
            _str_date_days = "DATEDIFF(HOUR, '" + _str_date_time + "', SPH_BASE_HOUR)";
            _str_since = "DATEADD(HOUR, " + _str_date_days + ", '" + _str_date_time + "')";
            _str_until = "DATEADD(HOUR, 1, " + _str_since + ")";
            _group_by = _str_date_days;
            break;

          case GroupDateFilter.G24_HOUR:
            _str_date_days = "(24 + DATEDIFF(HOUR, '" + _str_date_time + "', SPH_BASE_HOUR)) % 24";
            _str_since = "DATEADD(HOUR, " + _str_date_days + ", '" + _str_date_time + "')";
            _str_until = "DATEADD(HOUR, 1, " + _str_since + ")";
            _group_by = _str_date_days;
            break;

          case GroupDateFilter.DAILY:
          default:
            _str_date_days = "DATEDIFF(HOUR, '" + _str_date_time + "', SPH_BASE_HOUR) / 24";
            _str_since = "DATEADD(DAY, " + _str_date_days + ", '" + _str_date_time + "')";
            _str_until = "DATEADD(HOUR, 24, " + _str_since + ")";
            _group_by = _str_date_days;
            break;

          case GroupDateFilter.WEEKLY:
            _str_date_days = "DATEDIFF(HOUR, '" + _str_date_time + "', SPH_BASE_HOUR) / 24 / 7 * 7";
            _str_date_weeks = "DATEDIFF(HOUR, '" + _str_date_time + "', SPH_BASE_HOUR) / 24 / 7";
            _str_since = "DATEADD(DAY, " + _str_date_days + ", '" + _str_date_time + "')";
            _str_until = "DATEADD(DAY, 7, " + _str_since + ")";
            _group_by = _str_date_weeks;
            break;

          case GroupDateFilter.MONTHLY:
            _str_date_months = "DATEDIFF(MONTH, '" + _str_date_time + "', DATEADD(HOUR, -" + _init_hour.ToString() + ", SPH_BASE_HOUR))";
            _str_since = "DATEADD(MONTH, " + _str_date_months + ", '" + _str_date + "')";
            _str_until = "DATEADD(MONTH, 1, " + _str_since + ")";
            _group_by = _str_date_months;
            break;
        }

        _order_by += ", x.Since ASC";

        if (SalesProviderFilter == QuerySalesProviderFilter.DATE)
        {
          _str_left_join_terminals = "";
        }

        if (SalesProviderFilter == QuerySalesProviderFilter.DATE_PROVIDER)
        {
          _str_provider = "TE_PROVIDER_ID";
          _group_by += ", te_provider_id";
          _order_by += ", Provider";
        }

        if (SalesProviderFilter == QuerySalesProviderFilter.DATE_PROVIDER_TERMINAL)
        {
          _str_provider = "TE_PROVIDER_ID";
          _str_terminal = "TE_NAME";
          _str_terminal_id = "TE_TERMINAL_ID";
          _str_min_game = "MIN(SPH_GAME_ID)";
          _str_max_game = "MAX(SPH_GAME_ID)";

          if (IsCenter)
          {
            _str_min_game_name = "(SELECT   GM_GAME_NAME ";
            _str_min_game_name += "   FROM   GAMES ";
            _str_min_game_name += "  WHERE   GM_GAME_ID = MIN(SPH_GAME_ID) ";
          }
          else
          {
            _str_min_game_name = "(SELECT   PG_GAME_NAME AS GM_NAME ";
            _str_min_game_name += "   FROM   PROVIDERS_GAMES ";
            _str_min_game_name += "  WHERE   PG_GAME_ID = MIN(SPH_GAME_ID) ";
          }
          //MasterTablesMode = 0 || 2 : Providers & Providers_Games set in SITE
          //MasterTablesMode = 1 : Providers & Providers_Games set in MULTISITE
          if (IsCenter && MasterTablesMode != 1)
          {
            //_str_min_game_name += "    AND   GM_SITE_ID = SITE_ID ";
            _str_min_game_name += "    AND   GM_SITE_ID = SITE_ID ";
          }
          _str_min_game_name += ")";

          _group_by += ", te_provider_id, te_name, te_terminal_id";
          _order_by += ", Provider, Terminal";
        }
      }
      else if (SalesProviderFilter == QuerySalesProviderFilter.PROVIDER_TERMINAL ||
               SalesProviderFilter == QuerySalesProviderFilter.PROVIDER)
      {
        _str_since = "MIN(SPH_BASE_HOUR)";
        _str_until = "MAX(SPH_BASE_HOUR)";
        _str_provider = "TE_PROVIDER_ID";
        _group_by = "te_provider_id";
        _order_by += ", Provider";

        if (SalesProviderFilter == QuerySalesProviderFilter.PROVIDER_TERMINAL)
        {
          _str_terminal = "TE_NAME";
          _str_terminal_id = "TE_TERMINAL_ID";
          _str_min_game = "MIN(SPH_GAME_ID)";
          _str_max_game = "MAX(SPH_GAME_ID)";

          //_str_min_game_name = "(SELECT   GM_NAME ";
          //_str_min_game_name += "   FROM   GAMES ";
          //_str_min_game_name += "  WHERE   GM_GAME_ID = MIN(SPH_GAME_ID) ";
          if (IsCenter)
          {
            _str_min_game_name = "(SELECT   GM_GAME_NAME ";
            _str_min_game_name += "   FROM   GAMES ";
            _str_min_game_name += "  WHERE   GM_GAME_ID = MIN(SPH_GAME_ID) ";
          }
          else
          {
            _str_min_game_name = "(SELECT   PG_GAME_NAME AS GM_NAME ";
            _str_min_game_name += "   FROM   PROVIDERS_GAMES ";
            _str_min_game_name += "  WHERE   PG_GAME_ID = MIN(SPH_GAME_ID) ";
          }
          if (IsCenter)
          {
            //_str_min_game_name += "    AND   GM_SITE_ID = SITE_ID ";
            _str_min_game_name += "    AND   GM_SITE_ID = SITE_ID ";
          }
          _str_min_game_name += ")";

          _group_by += ", te_name, te_terminal_id";
          _order_by += ", Terminal";
        }
      }

      // RCI 07-APR-2011: Need to trim spaces to not duplicate rows.
      if (!String.IsNullOrEmpty(_str_provider))
      {
        _str_provider = "LTRIM(RTRIM(" + _str_provider + "))";
      }

      if (IsCenter)
      {
        _group_by += ", SITE_ID";
      }

      _sql_txt = "SELECT   x.Since " +
                "       , x.Until " +
                "       , x.te_name AS Terminal" +
                "       , x.PlayedAmount " +
                "       , x.WonAmount " +
                "       , CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0)*100/x.PlayedAmount) END as AmountPc " +
                "       , PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0) as Netwin " +
                "       , CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0))*100/x.PlayedAmount) END as NetwinPc " +
                "       , x.PlayedCount " +
                "       , x.WonCount " +
                "       , CASE x.PlayedCount WHEN 0 THEN 0 ELSE((x.WonCount* 100.0)/x.PlayedCount) END as CountPc " +
                "       , x.te_provider_id AS Provider " +
                "       , ISNULL(x.MinGameId,0) AS MinGameId " +
                "       , ISNULL(x.MaxGameId,0) AS MaxGameId " +
                "       , ISNULL(x.MinGameName, 'UNKNOWN') AS MinGameName " +
                "       , CASE x.PlayedAmount WHEN 0 THEN 0 ELSE (x.TheoreticalWonAmount*100/PlayedAmount) END as TheoreticalAmountPc " +
                "       , x.TheoreticalWonAmount " +
                "       , x.SiteId " +
                "       , x.ProgressiveProvisionAmount " +
                "       , x.NonProgressiveJackpotAmount " +
                "       , x.ProgressiveJackpotAmount " +
                "       , x.ProgressiveJackpotAmount0 " +
                "       , x.TE_TERMINAL_ID " +
                "  FROM   (SELECT   " + _str_provider + " AS TE_PROVIDER_ID " +
                "                 , " + _str_terminal + " AS TE_NAME" +
                "                 , " + _str_terminal_id + " AS TE_TERMINAL_ID" +
                "                 , " + _str_since + " AS SINCE " +
                "                 , " + _str_until + " AS UNTIL " +
                "                 , SUM(CAST(SPH_PLAYED_AMOUNT AS DECIMAL(22,4))) AS PlayedAmount " +
                "                 , SUM(CAST(SPH_WON_AMOUNT AS DECIMAL(22,4))) AS WonAmount " +
                "                 , CASE WHEN SUM(SPH_WON_COUNT)>SUM(SPH_PLAYED_COUNT) THEN SUM(SPH_WON_COUNT) ELSE SUM(SPH_PLAYED_COUNT) END AS PlayedCount " +
                "                 , SUM(SPH_WON_COUNT) AS WonCount " +
                "                 , " + _str_min_game + " AS MinGameId " +
                "                 , " + _str_max_game + " AS MaxGameId " +
                "                 , " + _str_min_game_name + " AS MinGameName " +
                "                 , SUM(SPH_THEORETICAL_WON_AMOUNT) AS TheoreticalWonAmount " +
                "                 , " + _str_select_sites + " AS SiteId " +
                "                 , SUM(ISNULL(SPH_PROGRESSIVE_PROVISION_AMOUNT,0)) AS ProgressiveProvisionAmount " +
                "                 , SUM(ISNULL(SPH_JACKPOT_AMOUNT,0) - ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS NonProgressiveJackpotAmount " +
                "                 , SUM(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS ProgressiveJackpotAmount " +
                "                 , SUM(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)) AS ProgressiveJackpotAmount0 " +
                "            FROM   SALES_PER_HOUR_V2 " +
                _str_left_join_terminals +
                _str_where +
                "          GROUP BY " + _group_by +
                "         ) AS x " +
                "ORDER BY " + _order_by;

      return _sql_txt;
    } // GetQuerySalesByDateAndProvider

    //------------------------------------------------------------------------------
    // PURPOSE : Return DataSet with diffent data of the sales by date and provider
    //
    //  PARAMS :
    //      - INPUT :
    //          - String SqlTxt
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - GroupDateFilter DateFilter
    //          - QuerySalesProviderFilter SalesProviderFilter
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataSet
    //
    public static DataSet GetSalesByDateAndProvider(String SqlTxt,
                                                    DateTime FromDate,
                                                    DateTime ToDate,
                                                    GroupDateFilter DateFilter,
                                                    QuerySalesProviderFilter SalesProviderFilter,
                                                    SqlTransaction SqlTrx)
    {
      String[] _pk_sdpt = { "SiteId", "Since", "Provider", "Terminal" };
      String[] _pk_sdp = { "SiteId", "Since", "Provider" };
      String[] _pk_spt = { "SiteId", "Provider", "Terminal" };
      String[] _pk_sp = { "SiteId", "Provider" };
      String[] _pk_sd = { "SiteId", "Since" };
      String[] _pk_s = { "SiteId" };
      DataTable _table_sdpt;
      DataTable _table_sdp;
      DataTable _table_spt;
      DataTable _table_sp;
      DataTable _table_sd;
      DataTable _table_s;
      DataSet _set;

      _table_sdpt = null;
      _table_sdp = null;
      _table_spt = null;
      _table_sp = null;
      _table_sd = null;
      _table_s = null;

      switch (SalesProviderFilter)
      {
        case QuerySalesProviderFilter.DATE_PROVIDER_TERMINAL:
          _table_sdpt = GetDataTableFromQuery("DATA_SDPT", _pk_sdpt, SqlTxt, SqlTrx);
          _table_sdpt = InsertEmptyRows(FromDate, ToDate, DateFilter, _table_sdpt);
          _table_sdp = GroupData(_table_sdpt, "DATA_SDP");
          _table_spt = GroupData(_table_sdpt, "DATA_SPT");

          _table_sp = GroupData(_table_sdp, "DATA_SP");
          _table_sd = GroupData(_table_sdp, "DATA_SD");

          _table_s = GroupData(_table_sd, "DATA_S");
          break;

        case QuerySalesProviderFilter.DATE_PROVIDER:
          _table_sdp = GetDataTableFromQuery("DATA_SDP", _pk_sdp, SqlTxt, SqlTrx);
          _table_sdp = InsertEmptyRows(FromDate, ToDate, DateFilter, _table_sdp);

          _table_sp = GroupData(_table_sdp, "DATA_SP");
          _table_sd = GroupData(_table_sdp, "DATA_SD");

          _table_s = GroupData(_table_sd, "DATA_S");
          break;

        case QuerySalesProviderFilter.DATE:
          _table_sd = GetDataTableFromQuery("DATA_SD", _pk_sd, SqlTxt, SqlTrx);
          _table_sd = InsertEmptyRows(FromDate, ToDate, DateFilter, _table_sd);

          _table_s = GroupData(_table_sd, "DATA_S");
          break;

        case QuerySalesProviderFilter.PROVIDER_TERMINAL:
          _table_spt = GetDataTableFromQuery("DATA_SPT", _pk_spt, SqlTxt, SqlTrx);

          _table_sp = GroupData(_table_spt, "DATA_SP");

          _table_s = GroupData(_table_sp, "DATA_S");
          break;

        case QuerySalesProviderFilter.PROVIDER:
          _table_sp = GetDataTableFromQuery("DATA_SP", _pk_sp, SqlTxt, SqlTrx);

          _table_s = GroupData(_table_sp, "DATA_S");
          break;
      }

      _set = new DataSet("DATA");
      if (_table_sdpt != null)
      {
        _set.Tables.Add(_table_sdpt);
      }
      if (_table_sdp != null)
      {
        _set.Tables.Add(_table_sdp);
      }
      if (_table_spt != null)
      {
        _set.Tables.Add(_table_spt);
      }
      if (_table_sp != null)
      {
        _set.Tables.Add(_table_sp);
      }
      if (_table_sd != null)
      {
        _set.Tables.Add(_table_sd);
      }
      if (_table_s != null)
      {
        _set.Tables.Add(_table_s);
      }

      // Relations en DataSet.
      if (_table_s != null && _table_sd != null)
      {
        _set.Relations.Add("SiteId_SD", _table_s.Columns["SiteId"], _table_sd.Columns["SiteId"], true);
      }
      if (_table_s != null && _table_sp != null)
      {
        _set.Relations.Add("SiteId_SP", _table_s.Columns["SiteId"], _table_sp.Columns["SiteId"], true);
      }
      if (_table_sp != null && _table_sdp != null)
      {
        _set.Relations.Add("Provider_DP",
                            new DataColumn[] { _table_sp.Columns["SiteId"], _table_sp.Columns["Provider"] },
                            new DataColumn[] { _table_sdp.Columns["SiteId"], _table_sdp.Columns["Provider"] });
      }
      if (_table_sp != null && _table_spt != null)
      {
        _set.Relations.Add("Provider_PT",
                            new DataColumn[] { _table_sp.Columns["SiteId"], _table_sp.Columns["Provider"] },
                            new DataColumn[] { _table_spt.Columns["SiteId"], _table_spt.Columns["Provider"] });
      }
      if (_table_sd != null && _table_sdp != null)
      {
        _set.Relations.Add("Date",
                            new DataColumn[] { _table_sd.Columns["SiteId"], _table_sd.Columns["Since"] },
                            new DataColumn[] { _table_sdp.Columns["SiteId"], _table_sdp.Columns["Since"] });
      }
      if (_table_sdp != null && _table_sdpt != null)
      {
        _set.Relations.Add("Date_Provider",
                           new DataColumn[] { _table_sdp.Columns["SiteId"], _table_sdp.Columns["Since"], _table_sdp.Columns["Provider"] },
                           new DataColumn[] { _table_sdpt.Columns["SiteId"], _table_sdpt.Columns["Since"], _table_sdpt.Columns["Provider"] });
      }
      if (_table_spt != null && _table_sdpt != null)
      {
        _set.Relations.Add("Provider_Terminal",
                           new DataColumn[] { _table_spt.Columns["SiteId"], _table_spt.Columns["Provider"], _table_spt.Columns["Terminal"] },
                           new DataColumn[] { _table_sdpt.Columns["SiteId"], _table_sdpt.Columns["Provider"], _table_sdpt.Columns["Terminal"] });
      }

      return _set;
    } // GetSalesByDateAndProvider

    //------------------------------------------------------------------------------
    // PURPOSE : Date in DB format
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime Value
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static String GUI_FormatDateDB(DateTime Value)
    {
      const String FORMAT_DB_DATE_TIME = "yyyy/MM/dd HH:mm:ss";
      String str_date;

      str_date = Value.ToString(FORMAT_DB_DATE_TIME);
      str_date = "CAST('" + str_date + "' AS DATETIME)";

      return str_date;
    } // GUI_FormatDateDB

    // FAV 15-JUN-2015
    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with Number of machines that have been played
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - SqlTransaction SqlTrx
    //          - List<TerminalTypes> TerminalTypeList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetTerminalsPlayedByProvider(DateTime DateFrom,
                                                DateTime DateTo,
                                                SqlTransaction SqlTrx,
                                                List<TerminalTypes> TerminalTypeList = null)
    {
      StringBuilder _sql_sb;
      String _str_where_dates;
      String _str_terminal_type;
      String[] _pk_name;

      _pk_name = new String[1];
      _pk_name.SetValue("Provider", 0);
      _sql_sb = new StringBuilder();
      _str_where_dates = "";
      _str_terminal_type = "";

      if (DateFrom != DateTime.MinValue)
      {
        _str_where_dates += " AND PS_FINISHED >= " + GUI_FormatDateDB(DateFrom) + " ";
      }
      if (DateTo != DateTime.MaxValue)
      {
        _str_where_dates += " AND PS_FINISHED < " + GUI_FormatDateDB(DateTo) + " ";
      }

      if (TerminalTypeList != null && TerminalTypeList.Count > 0)
      {
          _str_terminal_type = string.Join(" , ", TerminalTypeList.ConvertAll(x => ((int)x).ToString()).ToArray());
      }
      else
      {
          // default values for Terminal Types
        _str_terminal_type = (Int16)TerminalTypes.WIN + " , " + (Int16)TerminalTypes.T3GS + " , " + (Int16)TerminalTypes.SAS_HOST;
      }

      _sql_sb.AppendLine("SELECT   Provider "); 
      _sql_sb.AppendLine("       , COUNT	(TE_TERMINAL_ID)  AS TerminalsPlayed ");
      _sql_sb.AppendLine("  FROM   (SELECT   TE_PROVIDER_ID AS provider "); 
      _sql_sb.AppendLine("			     , TE_TERMINAL_ID ");
      _sql_sb.AppendLine("            FROM   PLAY_SESSIONS PS ");
      _sql_sb.AppendLine("	  INNER JOIN   TERMINALS T ");
      _sql_sb.AppendLine("	    	  ON PS.PS_TERMINAL_ID = T.TE_TERMINAL_ID ");
      _sql_sb.AppendLine("           WHERE   PS_STATUS   <> 0 "); 
      _sql_sb.AppendLine("             AND   PS_PROMO    =  0 ");
      _sql_sb.AppendLine("         " + _str_where_dates);
      _sql_sb.AppendLine("             AND   PS_TERMINAL_ID IN (SELECT   TE_TERMINAL_ID ");
      _sql_sb.AppendLine("                                        FROM   TERMINALS ");
      _sql_sb.AppendLine("                                       WHERE   TE_TERMINAL_TYPE IN (" + _str_terminal_type + ")");
      _sql_sb.AppendLine("                                     ) ");
      _sql_sb.AppendLine("			GROUP BY TE_PROVIDER_ID, TE_TERMINAL_ID ");
      _sql_sb.AppendLine("         ) AS x "); 
      _sql_sb.AppendLine("GROUP BY Provider ");
      _sql_sb.AppendLine("ORDER BY Provider "); 	


      return GetDataTableFromQuery("PROVIDERS", _pk_name, _sql_sb.ToString(), SqlTrx);
    } // GetTerminalsPlayedByProvider

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the sales statistics by game and day
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    private static DataTable GetSalesByGameAndDay(DateTime DateFrom,
                                                  DateTime DateTo,
                                                  SqlTransaction SqlTrx)
    {
      String _sql_txt;
      String _str_where_dates;
      String[] _pk_name = { "Game", "Date" };
      Int32 _init_hour;
      String _str_date;
      String _str_since_days;
      String _str_since;

      _str_where_dates = "";
      if (DateFrom != DateTime.MinValue)
      {
        _str_where_dates += " AND SPH_BASE_HOUR >= " + GUI_FormatDateDB(DateFrom) + " ";
      }
      if (DateTo != DateTime.MaxValue)
      {
        _str_where_dates += " AND SPH_BASE_HOUR  < " + GUI_FormatDateDB(DateTo) + " ";
      }

      _init_hour = Misc.TodayOpening().Hour;
      _str_date = "01-01-2007 " + _init_hour.ToString("00") + ":00:00:000";
      _str_since_days = "DATEDIFF(HOUR, '" + _str_date + "', sph_base_hour) / 24";
      _str_since = "DATEADD(DAY, " + _str_since_days + ", '" + _str_date + "')";

      _sql_txt = "SELECT   GM_NAME AS Game " +
                 "       , x.SINCE AS Date " +
                 "       , x.PlayedAmount " +
                 "       , x.WonAmount " +
                 "       , x.PlayedCount " +
                 "       , x.WonCount " +
                 "       , x.NumTerminals " +
                 "  FROM   GAMES INNER JOIN " +
                 "        (SELECT   SPH_GAME_ID " +
                 "                , " + _str_since + " AS SINCE " +
                 "                , SUM(SPH_PLAYED_AMOUNT)          AS PlayedAmount " +
                 "                , SUM(SPH_WON_AMOUNT)             AS WonAmount " +
                "                 , CASE WHEN SUM(SPH_WON_COUNT)>SUM(SPH_PLAYED_COUNT) THEN SUM(SPH_WON_COUNT) ELSE SUM(SPH_PLAYED_COUNT) END AS PlayedCount " +
                 "                , SUM(SPH_WON_COUNT)              AS WonCount " +
                 "                , COUNT(DISTINCT SPH_TERMINAL_ID) AS NumTerminals " +
                 "           FROM   SALES_PER_HOUR " +
                 "          WHERE   SPH_TERMINAL_ID IN (SELECT   TE_TERMINAL_ID " +
                 "                                        FROM   TERMINALS " +
                 "                                       WHERE   TE_TERMINAL_TYPE = " + (Int16)TerminalTypes.WIN +
                 "                                     ) " +
                 "            AND   SPH_GAME_ID     IN (SELECT   GM_GAME_ID " +
                 "                                        FROM   GAMES " +
                 "                                       WHERE   GM_NAME LIKE 'WSI - %' " +
                 "                                     ) " +
                 _str_where_dates +
                 "         GROUP BY SPH_GAME_ID, " + _str_since_days +
                 "        ) x ON GM_GAME_ID = x.SPH_GAME_ID " +
                 "ORDER BY Game, Date ";


      return GetDataTableFromQuery("GAMES", _pk_name, _sql_txt, SqlTrx);
    } // GetSalesByGameAndDay

    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the jackpot info by game and day
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom
    //          - DateTime DateTo
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    private static DataTable GetJackpotsByGameAndDay(DateTime DateFrom,
                                                     DateTime DateTo,
                                                     SqlTransaction SqlTrx)
    {
      String _sql_txt;
      String _str_where_dates;
      String[] _pk_name = { "Game", "Date" };
      Int32 _init_hour;
      String _str_date;
      String _str_awarded_days;
      String _str_awarded;

      _str_where_dates = "";
      if (DateFrom != DateTime.MinValue)
      {
        _str_where_dates += " AND C2JH_AWARDED >= " + GUI_FormatDateDB(DateFrom) + " ";
      }
      if (DateTo != DateTime.MaxValue)
      {
        _str_where_dates += " AND C2JH_AWARDED  < " + GUI_FormatDateDB(DateTo) + " ";
      }

      _init_hour = Misc.TodayOpening().Hour;
      _str_date = "01-01-2007 " + _init_hour.ToString("00") + ":00:00:000";
      _str_awarded_days = "DATEDIFF(HOUR, '" + _str_date + "', C2JH_AWARDED) / 24";
      _str_awarded = "DATEADD(DAY, " + _str_awarded_days + ", '" + _str_date + "')";

      _sql_txt = "SELECT   GM_NAME   AS Game " +
                 "       , x.AWARDED AS Date " +
                 "       , x.AMOUNT  AS Jackpot " +
                 "  FROM   GAMES INNER JOIN " +
                 "        (SELECT   C2JH_GAME_ID " +
                 "                , " + _str_awarded + " AS AWARDED " +
                 "                , SUM(C2JH_AMOUNT)     AS AMOUNT " +
                 "           FROM   C2_JACKPOT_HISTORY " +
                 "          WHERE   C2JH_AMOUNT   > 0 " +
                 _str_where_dates +
                 "         GROUP BY C2JH_GAME_ID, " + _str_awarded_days +
                 "        ) x ON GM_GAME_ID = x.C2JH_GAME_ID " +
                 "ORDER BY Game, Date ";

      return GetDataTableFromQuery("JACKPOTS", _pk_name, _sql_txt, SqlTrx);
    } // GetJackpotsByGameAndDay


    //------------------------------------------------------------------------------
    // PURPOSE : Return DataTable with the terminals without activity
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetTerminalsWithoutActivity(SqlTransaction SqlTrx)
    {
      StringBuilder _sql_sb;
      String[] _pk_name = { "TE_AREA", "TE_BANK", "TE_PROVIDER_ID", "TE_NAME" };
      Int32 _no_activity_hours;

      _sql_sb = new StringBuilder();

      _no_activity_hours = GeneralParam.GetInt32("Report.TerminalsWithoutActivity", "MaxNoActivityHours", 24);

      _no_activity_hours = Math.Max(_no_activity_hours, 2); // At least two hours due to SPH precision

      _sql_sb.AppendLine(" DECLARE   @pNoActivityHours     AS INT                                             ");
      _sql_sb.AppendLine(" DECLARE   @pNoActivityHistory   AS INT                                             ");
      _sql_sb.AppendLine(" DECLARE   @pNow                 AS DATETIME                                        ");
      _sql_sb.AppendLine(" DECLARE   @pFrom                AS DATETIME                                        ");

      _sql_sb.AppendLine(" SET   @pNoActivityHours    = " + _no_activity_hours.ToString());
      _sql_sb.AppendLine(" SET   @pNoActivityHistory  = 8                                                     ");

      _sql_sb.AppendLine(" SET   @pNow  = GETDATE ()                                                          ");
      _sql_sb.AppendLine(" SET   @pNow  = DATEADD (MILLISECOND, - DATEPART(MILLISECOND, @pNow), @pNow)        ");
      _sql_sb.AppendLine(" SET   @pNow  = DATEADD (SECOND, - DATEPART(SECOND, @pNow), @pNow)                  ");
      _sql_sb.AppendLine(" SET   @pNow  = DATEADD (MINUTE, - DATEPART(MINUTE, @pNow), @pNow)                  ");
      _sql_sb.AppendLine(" SET   @pFrom = DATEADD (HOUR, -@pNoActivityHours, @pNow)                           ");
      _sql_sb.AppendLine(" SET   @pFrom = DATEADD (DAY,  -@pNoActivityHistory, @pFrom)                        ");

      _sql_sb.AppendLine(" SELECT   ISNULL(AR_NAME, '')                 TE_AREA                               ");
      _sql_sb.AppendLine("        , ISNULL(BK_NAME, '')                 TE_BANK                               ");
      _sql_sb.AppendLine("        , ISNULL(TE_PROVIDER_ID, '')          TE_PROVIDER_ID                        ");
      _sql_sb.AppendLine("        , ISNULL(TE_NAME, '')                 TE_NAME                               ");
      _sql_sb.AppendLine("        , ISNULL(TE_FLOOR_ID, '')             TE_FLOOR_ID                           ");
      _sql_sb.AppendLine("        , TE_TERMINAL_TYPE                    TE_TERMINAL_TYPE                      ");
      _sql_sb.AppendLine("        , ISNULL(SPH,@pNoActivityHistory*24)  TE_LAST_ACTIVITY                      ");
      _sql_sb.AppendLine("   FROM   TERMINALS WITH (INDEX (PK_terminals))                                     ");
      _sql_sb.AppendLine("   LEFT JOIN (                                                                      ");
      _sql_sb.AppendLine(" SELECT   SPH_TERMINAL_ID                                                           ");
      _sql_sb.AppendLine("        , DATEDIFF (HOUR, MAX(SPH_BASE_HOUR), @pNow) AS SPH                         ");
      _sql_sb.AppendLine("   FROM   SALES_PER_HOUR WITH (INDEX (PK_sales_per_hour))                           ");
      _sql_sb.AppendLine("  WHERE   SPH_BASE_HOUR >= @pFrom                                                   ");
      _sql_sb.AppendLine("  GROUP BY SPH_TERMINAL_ID                                                          ");
      _sql_sb.AppendLine(" ) X ON X.SPH_TERMINAL_ID = TE_TERMINAL_ID                                          ");
      _sql_sb.AppendLine(" LEFT JOIN BANKS ON TE_BANK_ID = BK_BANK_ID                                         ");
      _sql_sb.AppendLine(" LEFT JOIN AREAS ON BK_AREA_ID = AR_AREA_ID                                         ");
      _sql_sb.AppendLine("  WHERE   TE_TERMINAL_TYPE IN (                                                     ");
      _sql_sb.AppendLine("						                  " + (Int16)TerminalTypes.WIN + "                     ");
      _sql_sb.AppendLine("						                , " + (Int16)TerminalTypes.T3GS + "                     ");
      _sql_sb.AppendLine("						                , " + (Int16)TerminalTypes.SAS_HOST + " )               ");
      _sql_sb.AppendLine("    AND   TE_STATUS = 0                                                             ");
      _sql_sb.AppendLine("    AND   ( SPH IS NULL OR SPH >= @pNoActivityHours )                               ");
      _sql_sb.AppendLine(" ORDER BY TE_AREA, TE_BANK, TE_PROVIDER_ID, TE_NAME                                 ");

      return GetDataTableFromQuery("TERMINALS_WITHOUT_ACTIVITY", _pk_name, _sql_sb.ToString(), SqlTrx);
    } // GetTerminalsWithoutActivity

    /// <summary>
    /// Return dataTable with hourly score data
    /// </summary>
    /// <param name="SqlTrx">SQL Transaction</param>
    /// <param name="DateFrom">DateFrom obtains hourly score values</param>
    /// <param name="HourlyScoreReport">Out hourly score report datatable</param>
    /// <returns>True: Processed OK. False: Otherwise.</returns>
    public static Boolean GetHourlyScoreReport(SqlTransaction SqlTrx, DateTime DateFrom, out DataTable HourlyScoreReport)
    {
      SqlParameter[] _sql_parameters = { new SqlParameter("@pDateFrom", DateFrom),
                                         new SqlParameter("@pCageFillerIn", (Int32)CASHIER_MOVEMENT.CAGE_FILLER_IN),
                                         new SqlParameter("@pCageFillerOut", (Int32)CASHIER_MOVEMENT.CAGE_FILLER_OUT),
                                         new SqlParameter("@pChipRE", (Int32)CurrencyExchangeType.CASINO_CHIP_RE),
                                         new SqlParameter("@pSaleChips", (Int32)PlayerTrackingMovementType.SaleChips)
                                       };

      HourlyScoreReport = GetDataTableFromStored(Resource.String("STR_MAILING_HOURLY_SCORE_REPORT"), _sql_parameters, "ScoreReport", SqlTrx);
      return HourlyScoreReport.Rows.Count > 0;
    } // GetHourlyScoreReport

    //------------------------------------------------------------------------------
    // PURPOSE : Return a DataTable that is the result of grouping the given table.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String NewTableName
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    private static DataTable GroupData(DataTable Table, String NewTableName)
    {
      DataTable _dt_min_games;
      DataRow[] _min_games_rows;
      DataSetHelper _dsh;
      DataTable _data;
      Decimal _played_amount;
      Decimal _won_amount;
      Decimal _theoretical_won_amount;
      Decimal _played_count;
      Decimal _won_count;
      Decimal _progressive_provision;
      Decimal _progressive_jackpot0;
      Decimal _netwin;
      Boolean _empty_provider;
      Boolean _empty_terminal;
      Boolean _empty_game;

      _empty_provider = false;
      _empty_terminal = false;
      _empty_game = false;

      _data = null;
      _dsh = new DataSetHelper();

      switch (NewTableName)
      {
        case "DATA_SDP":
          _data = _dsh.SelectGroupByInto(NewTableName, Table,
                          "Since,Until,Terminal,sum(PlayedAmount) PlayedAmount,sum(WonAmount) WonAmount,AmountPc,Netwin,NetwinPc," +
                          "sum(PlayedCount) PlayedCount,sum(WonCount) WonCount,CountPc,Provider,min(MinGameId) MinGameId,max(MaxGameId) MaxGameId," +
                          "MinGameName,TheoreticalAmountPc,sum(TheoreticalWonAmount) TheoreticalWonAmount,SiteId,sum(ProgressiveProvisionAmount) ProgressiveProvisionAmount, " +
                          "sum(NonProgressiveJackpotAmount) NonProgressiveJackpotAmount, sum(ProgressiveJackpotAmount) ProgressiveJackpotAmount, sum(ProgressiveJackpotAmount0) ProgressiveJackpotAmount0, TE_TERMINAL_ID", "", "SiteId,Since,Provider");
          _empty_terminal = true;
          _empty_game = true;
          break;

        case "DATA_SPT":
          _data = _dsh.SelectGroupByInto(NewTableName, Table,
                          "min(Since) Since,max(Until) Until,Terminal,sum(PlayedAmount) PlayedAmount,sum(WonAmount) WonAmount,AmountPc,Netwin,NetwinPc," +
                          "sum(PlayedCount) PlayedCount,sum(WonCount) WonCount,CountPc,Provider,min(MinGameId) MinGameId,max(MaxGameId) MaxGameId," +
                          "MinGameName,TheoreticalAmountPc,sum(TheoreticalWonAmount) TheoreticalWonAmount,SiteId,sum(ProgressiveProvisionAmount) ProgressiveProvisionAmount," +
                          "sum(NonProgressiveJackpotAmount) NonProgressiveJackpotAmount, sum(ProgressiveJackpotAmount) ProgressiveJackpotAmount, sum(ProgressiveJackpotAmount0) ProgressiveJackpotAmount0, TE_TERMINAL_ID", "", "SiteId,Provider,Terminal");
          break;

        case "DATA_SP":
          _data = _dsh.SelectGroupByInto(NewTableName, Table,
                          "min(Since) Since,max(Until) Until,Terminal,sum(PlayedAmount) PlayedAmount,sum(WonAmount) WonAmount,AmountPc,Netwin,NetwinPc," +
                          "sum(PlayedCount) PlayedCount,sum(WonCount) WonCount,CountPc,Provider,min(MinGameId) MinGameId,max(MaxGameId) MaxGameId," +
                          "MinGameName,TheoreticalAmountPc,sum(TheoreticalWonAmount) TheoreticalWonAmount,SiteId,sum(ProgressiveProvisionAmount) ProgressiveProvisionAmount," +
                          "sum(NonProgressiveJackpotAmount) NonProgressiveJackpotAmount, sum(ProgressiveJackpotAmount) ProgressiveJackpotAmount, sum(ProgressiveJackpotAmount0) ProgressiveJackpotAmount0, TE_TERMINAL_ID", "", "SiteId,Provider");
          _empty_terminal = true;
          _empty_game = true;
          break;

        case "DATA_SD":
          _data = _dsh.SelectGroupByInto(NewTableName, Table,
                          "Since,Until,Terminal,sum(PlayedAmount) PlayedAmount,sum(WonAmount) WonAmount,AmountPc,Netwin,NetwinPc," +
                          "sum(PlayedCount) PlayedCount,sum(WonCount) WonCount,CountPc,Provider,min(MinGameId) MinGameId,max(MaxGameId) MaxGameId," +
                          "MinGameName,TheoreticalAmountPc,sum(TheoreticalWonAmount) TheoreticalWonAmount,SiteId,sum(ProgressiveProvisionAmount) ProgressiveProvisionAmount," +
                          "sum(NonProgressiveJackpotAmount) NonProgressiveJackpotAmount, sum(ProgressiveJackpotAmount) ProgressiveJackpotAmount, sum(ProgressiveJackpotAmount0) ProgressiveJackpotAmount0, TE_TERMINAL_ID", "", "SiteId,Since");
          _empty_provider = true;
          _empty_terminal = true;
          _empty_game = true;
          break;

        case "DATA_S":
          _data = _dsh.SelectGroupByInto(NewTableName, Table,
                          "Since,Until,Terminal,sum(PlayedAmount) PlayedAmount,sum(WonAmount) WonAmount,AmountPc,Netwin,NetwinPc," +
                          "sum(PlayedCount) PlayedCount,sum(WonCount) WonCount,CountPc,Provider,min(MinGameId) MinGameId,max(MaxGameId) MaxGameId," +
                          "MinGameName,TheoreticalAmountPc,sum(TheoreticalWonAmount) TheoreticalWonAmount,SiteId,sum(ProgressiveProvisionAmount) ProgressiveProvisionAmount, " +
                          "sum(NonProgressiveJackpotAmount) NonProgressiveJackpotAmount, sum(ProgressiveJackpotAmount) ProgressiveJackpotAmount, " +
                          "sum(ProgressiveJackpotAmount0) ProgressiveJackpotAmount0, TE_TERMINAL_ID", "", "SiteId");
          _empty_provider = true;
          _empty_terminal = true;
          _empty_game = true;
          break;
      }

      // To obtain the name of the games.
      _dt_min_games = Table.DefaultView.ToTable(true, new String[] { "MinGameId", "MinGameName" });
      _dt_min_games.PrimaryKey = new DataColumn[] { _dt_min_games.Columns["MinGameId"] };

      // Need to recalculate: AmountPc, Netwin, NetwinPc, CountPc
      foreach (DataRow _row in _data.Rows)
      {
        if (_row["AmountPc"] != DBNull.Value)
        {
          _played_amount = (Decimal)_row["PlayedAmount"];
          _won_amount = (Decimal)_row["WonAmount"];
          _theoretical_won_amount = (Decimal)_row["TheoreticalWonAmount"];
          _played_count = (Int64)_row["PlayedCount"];
          _won_count = (Int64)_row["WonCount"];

          _progressive_provision = (Decimal)_row["ProgressiveProvisionAmount"];
          _progressive_jackpot0 = (Decimal)_row["ProgressiveJackpotAmount0"];

          _netwin = _played_amount - (_won_amount + _progressive_provision - _progressive_jackpot0);

          _row["AmountPc"] = _played_amount == 0 ? 0 : (_won_amount + _progressive_provision - _progressive_jackpot0) * 100 / _played_amount;
          _row["Netwin"] = _netwin;
          _row["NetwinPc"] = _played_amount == 0 ? 0 : _netwin * 100 / _played_amount;
          _row["CountPc"] = _played_count == 0 ? 0 : _won_count * 100 / _played_count;
          _row["TheoreticalAmountPc"] = _played_amount == 0 ? 0 : _theoretical_won_amount * 100 / _played_amount;
        }

        if (_empty_game)
        {
          _row["MinGameId"] = 0;
          _row["MaxGameId"] = 0;
          _row["MinGameName"] = "";
        }
        else
        {
          _min_games_rows = _dt_min_games.Select("MinGameId = " + _row["MinGameId"]);
          if (_min_games_rows.Length > 0)
          {
            _row["MinGameName"] = _min_games_rows[0]["MinGameName"];
          }
        }
        if (_empty_provider)
        {
          _row["Provider"] = "";
        }
        else
        {
          _row["Provider"] = ((String)_row["Provider"]).ToUpper();
        }
        if (_empty_terminal)
        {
          _row["Terminal"] = "";
          _row["TE_TERMINAL_ID"] = DBNull.Value;
        }
      }

      return _data;
    } // GroupData

    //------------------------------------------------------------------------------
    // PURPOSE : Return a DataTable from Sql Query
    //
    //  PARAMS :
    //      - INPUT :
    //          - String TableName
    //          - String[] PrimaryKeys
    //          - String SqlTxt
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    private static DataTable GetDataTableFromQuery(String TableName,
                                                   String[] PrimaryKeys,
                                                   String SqlTxt,
                                                   SqlTransaction SqlTrx)
    {
      DataTable _dt_providers;
      SqlDataAdapter _sql_da;
      SqlCommand _sql_cmd;
      DataColumn[] _keys;
      Int32 _idx_pk;

      try
      {
        _dt_providers = new DataTable(TableName);

        _sql_da = new SqlDataAdapter();
        _sql_cmd = new SqlCommand(SqlTxt, SqlTrx.Connection, SqlTrx);
        _sql_cmd.CommandTimeout = Mailing.SQL_COMMAND_TIMEOUT;

        _sql_da.SelectCommand = _sql_cmd;
        _sql_da.Fill(_dt_providers);

        _sql_cmd.Dispose();
        _sql_da.Dispose();

        if (PrimaryKeys.Length > 0)
        {
          _keys = new DataColumn[PrimaryKeys.Length];
          for (_idx_pk = 0; _idx_pk < PrimaryKeys.Length; _idx_pk++)
          {
            _keys[_idx_pk] = _dt_providers.Columns[PrimaryKeys[_idx_pk]];
          }
          _dt_providers.PrimaryKey = _keys;
        }

        return _dt_providers;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    } // GetDataTableFromQuery

    // EOR 19-MAY-2016
    //------------------------------------------------------------------------------
    // PURPOSE : Return a DataTable from Stored Procedure
    //
    //  PARAMS :
    //      - INPUT :
    //          - String TableName
    //          - SqlParameter[] Parameters
    //          - String NameStoredProcedure
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    private static DataTable GetDataTableFromStored(String TableName,
                                                   SqlParameter[] SqlParameters,
                                                   String NameStoredProcedure,
                                                   SqlTransaction SqlTrx)
    {
      DataTable _dt_result;
      SqlDataAdapter _sql_da;
      SqlCommand _sql_cmd;
      

      try
      {
        _dt_result = new DataTable(TableName);
        _sql_da = new SqlDataAdapter();

        _sql_cmd = new SqlCommand();
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;
        _sql_cmd.CommandType = CommandType.StoredProcedure;
        _sql_cmd.CommandTimeout = Mailing.SQL_COMMAND_TIMEOUT;
        _sql_cmd.CommandText = NameStoredProcedure;
        _sql_cmd.Parameters.AddRange(SqlParameters);
         
        _sql_da.SelectCommand = _sql_cmd;
        _sql_da.Fill(_dt_result);

        _sql_cmd.Dispose();
        _sql_da.Dispose();

        return _dt_result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    } // GetDataTableFromStored

    //------------------------------------------------------------------------------
    // PURPOSE : Insert empty rows to Table and return the Table
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - GroupDateFilter DateFilter
    //          - DataTable Table
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    private static DataTable InsertEmptyRows(DateTime FromDate,
                                             DateTime ToDate,
                                             GroupDateFilter DateFilter,
                                             DataTable Table)
    {
      DataTable _new_table;
      DataTable _site_id_table;
      DateTime _from_date;
      DateTime _to_date;
      DateTime _current_date;
      DateTime _row_date;
      DataRow[] _rows;
      DataRow[] _site_id_rows;
      Int32 _add_days_to_date;
      Int32 _hours_until;
      Int32 _site_id;

      _new_table = Table.Clone();
      _rows = Table.Select();

      _site_id_table = Table.DefaultView.ToTable(true, "SiteId");
      _site_id_rows = _site_id_table.Select();

      _hours_until = 0;
      _from_date = FromDate;
      _to_date = ToDate;

      switch (DateFilter)
      {
        case GroupDateFilter.HOURLY:
          _hours_until = 1;
          break;

        case GroupDateFilter.G24_HOUR:
          // Magic day: 2007-01-01
          _from_date = new DateTime(2007, 1, 1, Misc.TodayOpening().Hour, 0, 0);
          _to_date = _from_date.AddDays(1);
          _hours_until = 1;
          break;

        case GroupDateFilter.DAILY:
          _hours_until = 24;
          break;

        case GroupDateFilter.WEEKLY:
          _hours_until = 24 * 7;
          break;
      }

      if (_from_date == DateTime.MinValue)
      {
        if (Table.Rows.Count > 0)
        {
          _from_date = (DateTime)_rows[0]["Since"];
        }
        else
        {
          if (_to_date == DateTime.MaxValue || DateFilter == GroupDateFilter.HOURLY)
          {
            _from_date = WGDB.Now;
          }
          else
          {
            _from_date = _to_date;
            if (DateFilter == GroupDateFilter.DAILY)
            {
              _from_date = _to_date.AddDays(-1);
            }
          }
        }
      }
      if (_to_date == DateTime.MaxValue)
      {
        _to_date = Misc.TodayOpening().AddDays(1); // TodayClosing()
        if (DateFilter == GroupDateFilter.HOURLY)
        {
          _to_date = WGDB.Now;
        }
      }

      if (DateFilter == GroupDateFilter.WEEKLY)
      {
        _add_days_to_date = (Int32)(CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek - _from_date.DayOfWeek);
        if (_add_days_to_date > 0) // if it is positive, puts first day in next week
        {
          _add_days_to_date = _add_days_to_date - 7;
        }
        _from_date = _from_date.AddDays(_add_days_to_date);
      }
      else if (DateFilter == GroupDateFilter.MONTHLY)
      {
        _from_date = _from_date.AddDays(-_from_date.Day + 1);
      }

      foreach (DataRow _row_site_id in _site_id_rows)
      {
        _rows = Table.Select("SiteId = " + _row_site_id["SiteId"]);
        _site_id = (Int32)_row_site_id["SiteId"];
        _current_date = _from_date;

        foreach (DataRow _row in _rows)
        {
          _row_date = (DateTime)_row["Since"];

          while (_current_date < _row_date)
          {
            InsertEmptyRow(DateFilter, _current_date, _hours_until, _site_id, _new_table);
            if (DateFilter == GroupDateFilter.MONTHLY)
            {
              _current_date = _current_date.AddMonths(1);
            }
            else
            {
              _current_date = _current_date.AddHours(_hours_until);
            }
          }

          _row["Provider"] = ((String)_row["Provider"]).ToUpper();

          _new_table.ImportRow(_row);

          if (DateFilter == GroupDateFilter.MONTHLY)
          {
            _current_date = _row_date.AddMonths(1);
          }
          else
          {
            _current_date = _row_date.AddHours(_hours_until);
          }
        }

        while (_current_date < _to_date)
        {
          InsertEmptyRow(DateFilter, _current_date, _hours_until, _site_id, _new_table);

          if (DateFilter == GroupDateFilter.MONTHLY)
          {
            _current_date = _current_date.AddMonths(1);
          }
          else
          {
            _current_date = _current_date.AddHours(_hours_until);
          }
        }
      }

      DataView dv = _new_table.DefaultView;
      dv.Sort = "SiteId asc";
      DataTable sortedDT = dv.ToTable();

      return sortedDT;
    } // InsertEmptyRows

    //------------------------------------------------------------------------------
    // PURPOSE : Insert an empty row to Table
    //
    //  PARAMS :
    //      - INPUT :
    //          - GroupDateFilter DateFilter
    //          - DateTime Date
    //          - Int32 HoursUntil
    //          - DataTable Table
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void InsertEmptyRow(GroupDateFilter DateFilter,
                                       DateTime Date,
                                       Int32 HoursUntil,
                                       Int32 SiteId,
                                       DataTable Table)
    {
      DataRow _row;

      _row = Table.NewRow();
      _row["Since"] = Date;
      if (DateFilter == GroupDateFilter.MONTHLY)
      {
        _row["Until"] = Date.AddMonths(1);
      }
      else
      {
        _row["Until"] = Date.AddHours(HoursUntil);
      }
      _row["Terminal"] = "";
      _row["PlayedAmount"] = 0;
      _row["WonAmount"] = 0;
      _row["AmountPc"] = DBNull.Value;
      _row["Netwin"] = 0;
      _row["NetwinPc"] = DBNull.Value;
      _row["PlayedCount"] = DBNull.Value;
      _row["WonCount"] = DBNull.Value;
      _row["CountPc"] = DBNull.Value;
      _row["Provider"] = "";
      _row["MinGameId"] = GAME_ID_FOR_GROUPS;
      _row["MaxGameId"] = GAME_ID_FOR_GROUPS;
      _row["MinGameName"] = "";
      _row["TheoreticalAmountPc"] = DBNull.Value;
      _row["SiteId"] = SiteId;

      Table.Rows.Add(_row);
    } // InsertEmptyRow

    //------------------------------------------------------------------------------
    // PURPOSE : Get jackpot contribution pct parameter
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Decimal
    //
    private static Decimal DB_GetJackpotContributionPct(SqlTransaction SqlTrx)
    {
      SqlCommand _sql_cmd;
      String _sql_txt;
      Object _obj;
      Decimal _sql_value;

      _sql_value = 0;

      try
      {
        _sql_txt = "SELECT   C2JP_CONTRIBUTION_PCT " +
                   "  FROM   C2_JACKPOT_PARAMETERS ";

        _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx);

        _obj = _sql_cmd.ExecuteScalar();
        if (_obj != null)
        {
          _sql_value = (Decimal)_obj;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return _sql_value;
    } // DB_GetJackpotContributionPct

    #endregion // Private Methods

  } // SqlStatistics

} // WSI.Common
