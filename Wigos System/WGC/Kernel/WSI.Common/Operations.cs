//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Operations.cs
// 
//   DESCRIPTION : Class to manage operations
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-SEP-2010 RCI    First release.
// 04-OCT-2010 TJG    Gift's operation parameters
// 22-NOV-2011 JMM    Account's PIN Change operation added
// 17-FEB-2012 RCI    Allow to insert operation of type NOT_REDEEMABLE2_CREDITS_EXPIRED
// 22-FEB-2012 RCI    Split NR1 and NR2 in two fields when creating account operations: routine DB_InsertOperation().
// 09-MAR-2012 SSC    Added new Operation Codes for Note Acceptor
// 07-NOV-2012 JMM    Added PROMOBOX_TOTAL_RECHARGES_PRINT operation code for PromoBOX total recharges ticket
// 05-DEC-2012 RCI    Support for multiple cancellation
// 17-SEP-2013 NMR    Added features for new TITO operations.
// 18-OCT-2013 ICS    Added new class to contain account operation information
// 31-JAN-2014 DHA    Added set/update Operation Balance
// 18-SEP-2014 DLL    Added Cash Advance operation
// 21-JAN-2015 JPJ    Added new operation on mobile bank deposit
// 16-MAR-2015 DHA    Insert the operation id to exports to SPACE
// 02-APR-2015 DHA    Avoid to send Chips sale register on table operations to MultiSite
// 28-APR-2015 FJC    Add new OPeration (WinLossStatement)
// 24-AUG-2015 FAV    TFS-3257: Transfer between cashier sessions. 
// 29-OCT-2015 FAV    PBI 4695: HOPPER_FILLER_IN_REQUEST operation type for gaming hall
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 01-DEC-2015 FOS    Fixed Bug 7205: Protect taxWaiverMode and rename function ApplyTaxCollection to EnableTitoTaxWaiver
// 07-DIC-2015 FJC    Fixed BUG: 7360 (Added Cashier Movement when we reserve credit)
// 19-JAN-2016 FOS    Backlog Item 7969: Apply Taxes in handpay authorization
// 20-JAN-2016 ETP    Backlog Item 7941: ADD SUB values from buckets
// 23-FEB-2016 AVZ    Product Backlog Item 9363:Promo RE impuestos: Kernel: Promociones Redimibles: Aplicar impuestos
// 24-FEB-2016 EOR    Product Backlog Item 7402:SAS20: Add Comments HandPays
// 24-MAR-2016 RAB    Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 03-MAY-2016 FAV    Product Backlog Item 12653:Cajero: Currency exchange for multiple denominations (Mallorca)
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 30-NOV-2016 FOS    PBI 19197: Reopen cashier sessions and gambling tables sessions
// 09-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 15-MAR-2017 FAV    PBI 25262: Exped - Payment authorisation
// 24-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 18-JUN-2017 FGB    Bug 27741: Error witholding document - "Folio" field does not show the required value
// 01-JAN-2018 DHA    Bug 31392:WIGOS-7261 Cashier - Prize payment operation doesn't work. Warning mesage "No es posible realizar la operaci�n" is displayed.
// 01-JAN-2018 DHA    Bug 31393:WIGOS-7793 Service charge: wrong calculation with Terminal draw
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common.ExternalPaymentAndSaleValidation;

namespace WSI.Common
{
  public class Operations
  {
    #region Constants
    private const string OPERATION_TYPE_MESAS = "M";      //Mesas operation
    private const string OPERATION_TYPE_TERMINAL = "T";   //Terminal operation
    #endregion

    #region Public Methods

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                              Int64 AccountId,
                                              Int64 CashierSessionId,
                                              Int64 MbAccountId,
                                              Int64 PromotionId,
                                              Currency Amount,
                                              Currency NonRedeemable,
                                              Int64 OperationData,
                                              Int64 AccountOperationReasonId,
                                              String AccountOperationComment,
                                              out Int64 OperationId,
                                              out String ErrorMessage,
                                              SqlTransaction Trx)
    {
      return DB_InsertOperation(OperationCode,
                                 AccountId,
                                 CashierSessionId,
                                 MbAccountId,
                                 Amount,
                                 PromotionId,
                                 NonRedeemable,
                                 0,
                                 OperationData,
                                 AccountOperationReasonId,
                                 AccountOperationComment,
                                 out OperationId,
                                 out ErrorMessage,
                                 Trx);
    }

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                              Int64 AccountId,
                                              Int64 CashierSessionId,
                                              Int64 MbAccountId,
                                              Int64 PromotionId,
                                              Currency Amount,
                                              Currency NonRedeemable,
                                              Int64 OperationData,
                                              Int64 AccountOperationReasonId,
                                              String AccountOperationComment,
                                              out Int64 OperationId,
                                              out String ErrorMessage,
                                              SqlTransaction Trx,
                                              ExternalValidationInputParameters ExpedInputParams,
                                              ExternalValidationOutputParameters ExpedOutputParams)
    {
      return DB_InsertOperation(OperationCode,
                                 AccountId,
                                 CashierSessionId,
                                 MbAccountId,
                                 Amount,
                                 PromotionId,
                                 NonRedeemable,
                                 0,
                                 OperationData,
                                 AccountOperationReasonId,
                                 AccountOperationComment,
                                 out OperationId,
                                 out ErrorMessage,
                                 Trx,
                                 ExpedInputParams,
                                 ExpedOutputParams);
    }

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                              Int64 AccountId,
                                              Int64 CashierSessionId,
                                              Int64 MbAccountId,
                                              Int64 PromotionId,
                                              Currency Amount,
                                              Currency NonRedeemable,
                                              Int64 OperationData,
                                              Int64 AccountOperationReasonId,
                                              String AccountOperationComment,
                                              out Int64 OperationId,
      IDbTransaction Trx)
    {
      SqlTransaction _sql_trx = Trx as SqlTransaction;

      if (_sql_trx != null)
      {
        return DB_InsertOperation(OperationCode,
                                  AccountId,
                                  CashierSessionId,
                                  MbAccountId,
                                  PromotionId,
                                  Amount,
                                  NonRedeemable,
                                  OperationData,
                                  AccountOperationReasonId,
                                  AccountOperationComment,
                                  out OperationId,
                                  _sql_trx);
      }
      //TODO:Presume unit test other type db
      else
      {
        OperationId = -1;
        return true;
      }
    }

    /// <summary>
    /// PromoGame OperationId
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <param name="AccountId"></param>
    /// <param name="PromotionId"></param>
    /// <param name="Amount"></param>
    /// <param name="NonRedeemable"></param>
    /// <param name="OperationId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_InsertOperation(OperationCode OperationCode, Int64 AccountId, Int64 PromotionId, Currency Amount, Currency NonRedeemable, out Int64 OperationId, SqlTransaction SqlTrx)
    {
      return DB_InsertOperation(OperationCode, AccountId, 0, 0, PromotionId, Amount, NonRedeemable, -1, 0, String.Empty, out OperationId, SqlTrx);
    } // DB_InsertOperation

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                              Int64 AccountId,
                                              Int64 CashierSessionId,
                                              Int64 MbAccountId,
                                              Int64 PromotionId,
                                              Currency Amount,
                                              Currency NonRedeemable,
                                              Int64 OperationData,
                                              Int64 AccountOperationReasonId,
                                              String AccountOperationComment,
                                              out Int64 OperationId,
                                              SqlTransaction Trx)
    {
      String _error_message;
      return DB_InsertOperation(OperationCode,
                                 AccountId,
                                 CashierSessionId,
                                 MbAccountId,
                                 Amount,
                                 PromotionId,
                                 NonRedeemable,
                                 0,
                                 OperationData,
                                 AccountOperationReasonId,
                                 AccountOperationComment,
                                 out OperationId,
                                 out _error_message,
                                 Trx);
    }

    //EOR 24-FEB-2016 OVERLOAD METHOD ADD PARAMETER AO_COMMENT_HANDPAY
    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                              Int64 AccountId,
                                              Int64 CashierSessionId,
                                              Int64 MbAccountId,
                                              Int64 PromotionId,
                                              Currency Amount,
                                              Currency NonRedeemable,
                                              Int64 OperationData,
                                              Int64 AccountOperationReasonId,
                                              String AccountOperationComment,
                                              String AccountOperationCommentHandPay,
                                              out Int64 OperationId,
                                              out String ErrorMessage,
                                              SqlTransaction Trx)
    {
      return DB_InsertOperation(OperationCode,
                                 AccountId,
                                 CashierSessionId,
                                 MbAccountId,
                                 Amount,
                                 PromotionId,
                                 NonRedeemable,
                                 0,
                                 0,
                                 OperationData,
                                 false,
                                 AccountOperationReasonId,
                                 AccountOperationComment,
                                 AccountOperationCommentHandPay,
                                 out OperationId,
                                 out ErrorMessage,
                                 Trx);
    }

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                              Int64 AccountId,
                                              Int64 CashierSessionId,
                                              Int64 MbAccountId,
                                              Int64 PromotionId,
                                              Currency Amount,
                                              Currency NonRedeemable,
                                              Int64 OperationData,
                                              Int64 AccountOperationReasonId,
                                              String AccountOperationComment,
                                              String AccountOperationCommentHandPay,
                                              out Int64 OperationId,
                                              SqlTransaction Trx)
    {
      String _error_message;
      return DB_InsertOperation(OperationCode,
                                 AccountId,
                                 CashierSessionId,
                                 MbAccountId,
                                 Amount,
                                 PromotionId,
                                 NonRedeemable,
                                 0,
                                 0,
                                 OperationData,
                                 false,
                                 AccountOperationReasonId,
                                 AccountOperationComment,
                                 AccountOperationCommentHandPay,
                                 out OperationId,
                                 out _error_message,
                                 Trx);
    }

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                             Int64 AccountId,
                                             Int64 CashierSessionId,
                                             Int64 MbAccountId,
                                             Currency Amount,
                                             Int64 PromotionId,
                                             Currency NonRedeemable,
                                             Currency NonRedeemableWonLock,
                                             Int64 OperationData,
                                             Int64 AccountOperationReasonId,
                                             String AccountOperationComment,
                                             out Int64 OperationId,
                                             SqlTransaction Trx)
    {
      String _error_message;
      return DB_InsertOperation(OperationCode,
                                 AccountId,
                                 CashierSessionId,
                                 MbAccountId,
                                 Amount,
                                 PromotionId,
                                 NonRedeemable,
                                 NonRedeemableWonLock,
                                 0,
                                 OperationData,
                                 false,
                                 AccountOperationReasonId,
                                 AccountOperationComment,
                                 String.Empty, //EOR 24-FEB-2016 ADD COMMENTS HANDPAY DEFAULT VALUE
                                 out OperationId,
                                 out _error_message,
                                 Trx);
    }

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                             Int64 AccountId,
                                             Int64 CashierSessionId,
                                             Int64 MbAccountId,
                                             Currency Amount,
                                             Int64 PromotionId,
                                             Currency NonRedeemable,
                                             Currency NonRedeemableWonLock,
                                             Int64 OperationData,
                                             Int64 AccountOperationReasonId,
                                             String AccountOperationComment,
                                             out Int64 OperationId,
                                             out String ErrorMessage,
                                             SqlTransaction Trx,
                                             ExternalValidationInputParameters ExpedInputParams = null,
                                             ExternalValidationOutputParameters ExpedOutputParams = null)
    {
      return DB_InsertOperation(OperationCode,
                                 AccountId,
                                 CashierSessionId,
                                 MbAccountId,
                                 Amount,
                                 PromotionId,
                                 NonRedeemable,
                                 NonRedeemableWonLock,
                                 0,
                                 OperationData,
                                 false,
                                 AccountOperationReasonId,
                                 AccountOperationComment,
                                 String.Empty, //EOR 24-FEB-2016 ADD COMMENTS HANDPAY DEFAULT VALUE
                                 out OperationId,
                                 out ErrorMessage,
                                 Trx,
                                 ExpedInputParams,
                                 ExpedOutputParams);
    }

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                             Int64 AccountId,
                                             Int64 CashierSessionId,
                                             Int64 MbAccountId,
                                             Currency Amount,
                                             Int64 PromotionId,
                                             Currency NonRedeemable,
                                             Currency NonRedeemableWonLock,
                                             Currency NonRedeemable2,
                                             Int64 OperationData,
                                             Int64 AccountOperationReasonId,
                                             String AccountOperationComment,
                                             out Int64 OperationId,
                                             SqlTransaction Trx)
    {
      String _error_message;
      return DB_InsertOperation(OperationCode,
                                 AccountId,
                                 CashierSessionId,
                                 MbAccountId,
                                 Amount,
                                 PromotionId,
                                 NonRedeemable,
                                 NonRedeemableWonLock,
                                 NonRedeemable2,
                                 OperationData,
                                 false,
                                 AccountOperationReasonId,
                                 AccountOperationComment,
                                 String.Empty, //EOR 24-FEB-2016 ADD COMMENTS HANDPAY DEFAULT VALUE
                                 out OperationId,
                                 out _error_message,
                                 Trx);
    }

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                             Int64 AccountId,
                                             Int64 CashierSessionId,
                                             Int64 MbAccountId,
                                             Currency Amount,
                                             Int64 PromotionId,
                                             Currency NonRedeemable,
                                             Currency NonRedeemableWonLock,
                                             Currency NonRedeemable2,
                                             Int64 OperationData,
                                             Int64 AccountOperationReasonId,
                                             String AccountOperationComment,
                                             out Int64 OperationId,
                                             out String ErrorMessage,
                                             SqlTransaction Trx)
    {
      return DB_InsertOperation(OperationCode,
                                 AccountId,
                                 CashierSessionId,
                                 MbAccountId,
                                 Amount,
                                 PromotionId,
                                 NonRedeemable,
                                 NonRedeemableWonLock,
                                 NonRedeemable2,
                                 OperationData,
                                 false,
                                 AccountOperationReasonId,
                                 AccountOperationComment,
                                 String.Empty, //EOR 24-FEB-2016 ADD COMMENTS HANDPAY DEFAULT VALUE
                                 out OperationId,
                                 out ErrorMessage,
                                 Trx);
    }

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                             Int64 AccountId,
                                             Int64 CashierSessionId,
                                             Int64 MbAccountId,
                                             Currency Amount,
                                             Int64 PromotionId,
                                             Currency NonRedeemable,
                                             Currency NonRedeemableWonLock,
                                             Currency NonRedeemable2,
                                             Int64 OperationData,
                                             Boolean IsChipsSaleRegister,
                                             Int64 AccountOperationReasonId,
                                             String AccountOperationComment,
                                             String AccountOperationCommentHandPay,
                                             out Int64 OperationId,
                                             SqlTransaction Trx)
    {
      String _error_message;
      return DB_InsertOperation(OperationCode,
                                AccountId,
                                CashierSessionId,
                                MbAccountId,
                                Amount,
                                PromotionId,
                                NonRedeemable,
                                NonRedeemableWonLock,
                                NonRedeemable2,
                                OperationData,
                                IsChipsSaleRegister,
                                AccountOperationReasonId,
                                AccountOperationComment,
                                AccountOperationCommentHandPay,
                                out OperationId,
                                out _error_message,
                                Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert an operation.
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationCode : Operation Code values: CASH_IN, CASH_OUT, PROMOTION, MB_CASH_IN.
    //          - AccountId
    //          - CashierSessionId
    //          - MbAccountId
    //          - Amount
    //          - PromotionId
    //          - NonRedeemable
    //          - NonRedeemableWonLock
    //          - NonRedeemable2
    //          - OperationData
    //          - Trx
    //
    //      - OUTPUT:
    //          - OperationId
    //
    // RETURNS:
    //      - Boolean: If operation has been inserted correctly.

    public static Boolean DB_InsertOperation(OperationCode OperationCode,
                                             Int64 AccountId,
                                             Int64 CashierSessionId,
                                             Int64 MbAccountId,
                                             Currency Amount,
                                             Int64 PromotionId,
                                             Currency NonRedeemable,
                                             Currency NonRedeemableWonLock,
                                             Currency NonRedeemable2,
                                             Int64 OperationData,
                                             Boolean IsChipsSaleRegister,
                                             Int64 AccountOperationReasonId,
                                             String AccountOperationComment,
                                             String AccountOperationCommentHandPay,
                                             out Int64 OperationId,
                                             out String ErrorMessage,
                                             SqlTransaction Trx,
                                             ExternalValidationInputParameters ExpedInputParams = null,
                                             ExternalValidationOutputParameters ExpedOutputParams = null,
                                             String HolderName = "")
    {
      StringBuilder _sql_txt;
      SqlCommand _sql_command;
      SqlParameter _parameter;
      Int32 _num_rows_inserted;
      Boolean _enabled_tax;

      OperationId = 0;
      ErrorMessage = String.Empty;

      _enabled_tax = (OperationCode != OperationCode.MULTIPLE_BUCKETS_MANUAL);

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("INSERT INTO ACCOUNT_OPERATIONS ( AO_DATETIME");
        _sql_txt.AppendLine("                               , AO_CODE");
        _sql_txt.AppendLine("                               , AO_ACCOUNT_ID");
        _sql_txt.AppendLine("                               , AO_CASHIER_SESSION_ID");
        _sql_txt.AppendLine("                               , AO_MB_ACCOUNT_ID");
        _sql_txt.AppendLine("                               , AO_PROMO_ID");
        _sql_txt.AppendLine("                               , AO_AMOUNT");
        _sql_txt.AppendLine("                               , AO_NON_REDEEMABLE ");
        _sql_txt.AppendLine("                               , AO_WON_LOCK");
        _sql_txt.AppendLine("                               , AO_OPERATION_DATA");
        _sql_txt.AppendLine("                               , AO_NON_REDEEMABLE2 ");

        if (Tax.EnableTitoTaxWaiver(_enabled_tax))
        {
          _sql_txt.AppendLine("                               , AO_REASON_ID");
          _sql_txt.AppendLine("                               , AO_COMMENT ");
        }
        else if (!_enabled_tax)
        {
          _sql_txt.AppendLine("                               , AO_COMMENT ");
        }

        //EOR 24-FEB-2016 ADD COLUMN AO_COMMENT_HANDPAY
        _sql_txt.AppendLine("                               , AO_COMMENT_HANDPAY ");

        _sql_txt.AppendLine("                               )");
        _sql_txt.AppendLine("                       VALUES ( GETDATE()");
        _sql_txt.AppendLine("                              , @pCode");
        _sql_txt.AppendLine("                              , @pAccountId");
        _sql_txt.AppendLine("                              , @pCashierSessionId");
        _sql_txt.AppendLine("                              , @pMbAccountId");
        _sql_txt.AppendLine("                              , @pPromoId");
        _sql_txt.AppendLine("                              , @pAmount");
        _sql_txt.AppendLine("                              , @pNonRedeemable");
        _sql_txt.AppendLine("                              , @pNonRedeemableWonLock");
        _sql_txt.AppendLine("                              , @pOperationData");
        _sql_txt.AppendLine("                              , @pNonRedeemable2");

        if (Tax.EnableTitoTaxWaiver(_enabled_tax))
        {
          _sql_txt.AppendLine("                              , @pAccountReasonId");
          _sql_txt.AppendLine("                              , @pAccountComment");
        }
        else if (!_enabled_tax)
        {
          _sql_txt.AppendLine("                              , @pAccountComment");
        }

        //EOR 24-FEB-2016 ADD PARAMETER AO_COMMENT_HANDPAY
        _sql_txt.AppendLine("                              , @pAccountCommentHandPay");

        _sql_txt.AppendLine("                              )");
        _sql_txt.AppendLine("SET @pOperationId = SCOPE_IDENTITY()");

        _sql_command = new SqlCommand(_sql_txt.ToString());
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@pCode", SqlDbType.Int).Value = (Int32)OperationCode;
        _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        _sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
        _sql_command.Parameters.Add("@pMbAccountId", SqlDbType.BigInt).Value = DBNull.Value;
        _sql_command.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = DBNull.Value;
        _sql_command.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount.SqlMoney;
        _sql_command.Parameters.Add("@pNonRedeemable", SqlDbType.Money).Value = NonRedeemable.SqlMoney;
        _sql_command.Parameters.Add("@pNonRedeemableWonLock", SqlDbType.Money).Value = NonRedeemableWonLock.SqlMoney;

        //If OperationData is -1= no comission
        if (OperationData == -1)
        {
          _sql_command.Parameters.Add("@pOperationData", SqlDbType.BigInt).Value = DBNull.Value;
        }
        else
        {
          _sql_command.Parameters.Add("@pOperationData", SqlDbType.BigInt).Value = (Int64)OperationData;
        }

        _sql_command.Parameters.Add("@pNonRedeemable2", SqlDbType.Money).Value = NonRedeemable2.SqlMoney;

        if (string.IsNullOrEmpty(AccountOperationComment))
        {
          AccountOperationComment = String.Empty;
        }

        if (Tax.EnableTitoTaxWaiver(_enabled_tax))
        {
          _sql_command.Parameters.Add("@pAccountReasonId", SqlDbType.BigInt).Value = AccountOperationReasonId;
          _sql_command.Parameters.Add("@pAccountComment", SqlDbType.NVarChar).Value = AccountOperationComment;
        }
        else if (!_enabled_tax)
        {
          _sql_command.Parameters.Add("@pAccountComment", SqlDbType.NVarChar).Value = AccountOperationComment;
        }

        //EOR 24-FEB-2016 ADD VALUE AO_COMMENT_HANDPAY
        if (string.IsNullOrEmpty(AccountOperationCommentHandPay))
        {
          _sql_command.Parameters.Add("@pAccountCommentHandPay", SqlDbType.NVarChar).Value = DBNull.Value;
        }
        else
        {
          _sql_command.Parameters.Add("@pAccountCommentHandPay", SqlDbType.NVarChar).Value = AccountOperationCommentHandPay;
        }

        switch (OperationCode)
        {
          case OperationCode.CASH_IN:
          case OperationCode.CASH_OUT:
          case OperationCode.REDEEMABLE_CREDITS_EXPIRED:
          case OperationCode.NOT_REDEEMABLE_CREDITS_EXPIRED:
          case OperationCode.NOT_REDEEMABLE2_CREDITS_EXPIRED:
          case OperationCode.POINTS_EXPIRED:
          case OperationCode.GIFT_REQUEST:
          case OperationCode.HOLDER_LEVEL_CHANGED:
          case OperationCode.DRAW_TICKET:
          case OperationCode.GIFT_DRAW_TICKET:
          case OperationCode.ACCOUNT_CREATION:
          case OperationCode.ACCOUNT_PERSONALIZATION:
          case OperationCode.ACCOUNT_PIN_CHANGED:
          case OperationCode.ACCOUNT_PIN_RANDOM:
          case OperationCode.PROMOBOX_TOTAL_RECHARGES_PRINT:
          case OperationCode.HANDPAY:
          case OperationCode.HANDPAY_CANCELLATION:
          case OperationCode.GIFT_REDEEMABLE_AS_CASHIN:
          case OperationCode.IMPORT_POINTS:
          case OperationCode.IMPORT_GAMING_TABLE_PLAY_SESSIONS:
          case OperationCode.CHIPS_PURCHASE:
          case OperationCode.CHIPS_SALE:
          case OperationCode.TRANSFER_CREDIT_IN:
          case OperationCode.TRANSFER_CREDIT_OUT:
          case OperationCode.TITO_OFFLINE:
          case OperationCode.TITO_REISSUE:
          case OperationCode.TITO_TICKET_VALIDATION:
          case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
          case OperationCode.CASH_ADVANCE:
          case OperationCode.CASH_WITHDRAWAL:
          case OperationCode.CASH_DEPOSIT:
          case OperationCode.SAFE_KEEPING_DEPOSIT:
          case OperationCode.SAFE_KEEPING_WITHDRAW:
          case OperationCode.WIN_LOSS_STATEMENT_REQUEST:
          case OperationCode.CARD_REPLACEMENT:
          case OperationCode.TRANSFER_CASHIER_SESSIONS_DEPOSIT:
          case OperationCode.TRANSFER_CASHIER_SESSIONS_WITHDRAWAL:
          case OperationCode.TERMINAL_REFILL_HOPPER:
          case OperationCode.TERMINAL_COLLECT_DROPBOX:
          case OperationCode.CASHIER_COLLECT_REFILL:
          case OperationCode.CASHIER_COLLECT_REFILL_CAGE:
          case OperationCode.CASHIER_REFILL_HOPPER:
          case OperationCode.CASHIER_REFILL_HOPPER_CAGE:
          case OperationCode.TERMINAL_CHANGE_STACKER_REQUEST:
          case OperationCode.GAMEGATEWAY_RESERVE_CREDIT:
          case OperationCode.CUSTOMER_ENTRANCE:
          case OperationCode.HANDPAY_VALIDATION:
          case OperationCode.MULTIPLE_BUCKETS_EXPIRED:
          case OperationCode.MULTIPLE_BUCKETS_ENDSESSION:
          case OperationCode.MULTIPLE_BUCKETS_MANUAL:
          case OperationCode.TITO_VOID_MACHINE_TICKET:
          case OperationCode.CURRENCY_EXCHANGE_CHANGE:
          case OperationCode.CURRENCY_EXCHANGE_DEVOLUTION:

          // RAB 24-MAR-2016: Bug 10745
          case OperationCode.CARD_ASSOCIATE:
          case OperationCode.CHIPS_SWAP:
          case OperationCode.CLOSE_OPERATION:
          case OperationCode.REOPEN_OPERATION:
          case OperationCode.HANDPAY_AUTOMATICALLY_PAYMENT:

          // AMF 16-MAR-2017
          case OperationCode.CREDIT_LINE:
          //FGB 19-MAY-2017
          case OperationCode.GT_TOURNAMENTS_PRIZE_PAYMENT:
          case OperationCode.GT_TOURNAMENTS_ENROLL:
          case OperationCode.GT_TOURNAMENTS_REBUY:
          case OperationCode.PLAYCASH_DRAW:
          case OperationCode.PLAYREWARD_DRAW:
          case OperationCode.PLAYPOINT_DRAW:
            //Do nothing
            break;

          // JGC 26-JUL-2018: Operation code for Cashout Receipt
          case OperationCode.AUTO_PRINT_VOUCHER_CASH_OUT:
            // Do nothing
            break;

          case OperationCode.GIFT_DELIVERY:
          case OperationCode.CANCEL_GIFT_INSTANCE:
          case OperationCode.CHIPS_SALE_WITH_RECHARGE:
            if (PromotionId != 0)
            {
              _sql_command.Parameters["@pPromoId"].Value = PromotionId;
            }
            break;

          //AVZ 23-FEB-2016
          case OperationCode.PROMOTION_WITH_TAXES:
          case OperationCode.PROMOTION:
          case OperationCode.PRIZE_PAYOUT:
          case OperationCode.PRIZE_PAYOUT_AND_RECHARGE:
            _sql_command.Parameters["@pPromoId"].Value = PromotionId;
            break;

          //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
          case OperationCode.NA_CASH_IN:
          case OperationCode.MB_CASH_IN:
          case OperationCode.MB_DEPOSIT:
            _sql_command.Parameters["@pMbAccountId"].Value = MbAccountId;
            break;

          //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
          case OperationCode.NA_PROMOTION:
          case OperationCode.MB_PROMOTION:
            _sql_command.Parameters["@pPromoId"].Value = PromotionId;
            _sql_command.Parameters["@pMbAccountId"].Value = MbAccountId;
            break;

          default:
            if (OperationCode >= OperationCode.CAGE_CONCEPTS && OperationCode <= OperationCode.CAGE_CONCEPTS_LAST)
            {
              // Do nothing
              break;
            }

            Log.Error("Inserting Operation. Code: " + OperationCode + " UNKNOWN. AccountId: " + AccountId
                     + ", Amount: " + Amount + ", CashierSessionId: " + CashierSessionId
                     + ", MbAccountId: " + MbAccountId + ", PromotionId: " + PromotionId
                     + ", Non Redeemable: " + NonRedeemable + ", Non Redeemable2: " + NonRedeemable2
                     + ", OperationData: " + OperationData
                     + ".");

            return false;
        }

        _parameter = _sql_command.Parameters.Add("@pOperationId", SqlDbType.BigInt);
        _parameter.Direction = ParameterDirection.Output;

        _num_rows_inserted = _sql_command.ExecuteNonQuery();

        // Inserted correctly?
        if (_num_rows_inserted != 1)
        {
          Log.Error("Inserting Operation. Code: " + OperationCode + ", AccountId: " + AccountId
                   + ", Amount: " + Amount + ", CashierSessionId: " + CashierSessionId
                   + ", MbAccountId: " + MbAccountId + ", PromotionId: " + PromotionId
                   + ", Non Redeemable: " + NonRedeemable + ", Non Redeemable2: " + NonRedeemable2
                   + ", OperationData: " + OperationData
                   + ".");

          return false;
        }

        OperationId = (Int64)_parameter.Value;

        // DHA 16-MAR-2015
        if (!InsertPendingOperationMultiSite(OperationId, OperationCode, Amount, IsChipsSaleRegister, Trx))
        {
          return false;
        }

        if (OperationCode == Common.OperationCode.CASH_IN || OperationCode == Common.OperationCode.MB_CASH_IN)
        {
          ExpedInputParams = new ExternalValidationInputParameters(AccountId
                                                                 , Amount     //NetAmount
                                                                 , 0
                                                                 , OperationId
                                                                 , OperationCode);

          ExpedOutputParams = new ExternalValidationOutputParameters();
        }

        // Check EXPED
        if (ExpedInputParams == null)
        {
          ExpedInputParams = Misc.ExternalWS.InputParams;
        }
        if (ExpedOutputParams == null)
        {
          ExpedOutputParams = Misc.ExternalWS.OutputParams;
        }
        if (ExpedInputParams != null && ExpedOutputParams != null)
        {
          ExpedInputParams.OperationId = OperationId;
          if (!ExternalPaymentAndSaleValidationCommon.ExternalPaymentValidationGetAuthorization(ExpedInputParams, ExpedOutputParams, Trx))
          {
            Log.Error(String.Format("Inserting Operation. Code: {0}, AccountId: {1}"
                                  + ", Amount: {2}, CashierSessionId: {3}"
                                  + ", MbAccountId: {4}, PromotionId: {5}"
                                  + ", Non Redeemable: {6}, Non Redeemable2: {7}"
                                  + ", OperationData: {8}"
                                  + ". NOT EXTERNAL VALIDATED: {9}"
                                , OperationCode, AccountId
                                , Amount, CashierSessionId
                                , MbAccountId, PromotionId
                                , NonRedeemable, NonRedeemable2
                                , OperationData
                                , Misc.ExternalWS.OutputParams.ValidationMessage));

            ErrorMessage = ExternalPaymentAndSaleValidationCommon.GetErrorMessageFromExternalProvider(AccountId, HolderName, Amount);

            return false;
          }
        }

        switch (OperationCode)
        {
          //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
          case OperationCode.NA_CASH_IN:
          case OperationCode.NA_PROMOTION:
          case OperationCode.MB_CASH_IN:
          case OperationCode.MB_PROMOTION:
          case OperationCode.CASH_IN:
          case OperationCode.PROMOTION:
          case OperationCode.GIFT_REDEEMABLE_AS_CASHIN:
          case OperationCode.PROMOTION_WITH_TAXES:
          case OperationCode.CHIPS_SALE_WITH_RECHARGE:

            return DB_SetCancellableOperationId(AccountId, OperationId, Trx);

          default:
            return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // DB_InsertOperation

    //------------------------------------------------------------------------------
    // PURPOSE: Set cancellable an operation. Save OperationId in table ACCOUNTS.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - OperationId
    //          - Trx
    //
    // RETURNS:
    //      - Boolean: If operation has been set correctly.
    //
    public static Boolean DB_SetCancellableOperationId(Int64 AccountId,
                                                       Int64 OperationId,
                                                       SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlCommand _sql_command;
      Int32 _num_rows_inserted;

      try
      {
        // RCI 05-DEC-2012: Set cancellable operation only if it is NULL.
        //                  If it's not NULL, keep the first operation id to allow multiple cancellation.
        // TODO ANDREU: Posar un GeneralParams per decidir si es cancel�laci� multiple o no ??????

        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE ACCOUNTS ");
        _sql_txt.AppendLine("   SET AC_CANCELLABLE_OPERATION_ID = CASE WHEN AC_CANCELLABLE_OPERATION_ID IS NULL ");
        _sql_txt.AppendLine("                                          THEN @pOperationId ");
        _sql_txt.AppendLine("                                          ELSE AC_CANCELLABLE_OPERATION_ID END ");
        _sql_txt.AppendLine(" WHERE AC_ACCOUNT_ID               = @pAccountId ");

        _sql_command = new SqlCommand(_sql_txt.ToString());
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        _sql_command.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

        _num_rows_inserted = _sql_command.ExecuteNonQuery();

        if (_num_rows_inserted != 1)
        {
          Log.Error("Set Operation Cancellable. AccountId: " + AccountId + ", OperationId: " + OperationId + ".");

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // DB_SetCancellableOperationId

    //------------------------------------------------------------------------------
    // PURPOSE: Get the AccountId associated to an operation.
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountId
    //
    // RETURNS:
    //      - Boolean: If AccountId has been retrieved correctly.
    //
    public static Boolean DB_GetAccountIdFromOperation(Int64 OperationId,
                                                       out Int64 AccountId,
                                                       SqlTransaction SqlTrx)
    {
      StringBuilder _sql_txt;
      Object _obj;

      AccountId = 0;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT   AO_ACCOUNT_ID ");
        _sql_txt.AppendLine("  FROM   ACCOUNT_OPERATIONS ");
        _sql_txt.AppendLine(" WHERE   AO_OPERATION_ID = @pOperationId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            AccountId = (Int64)_obj;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetAccountIdFromOperation

    //------------------------------------------------------------------------------
    // PURPOSE: Get the Operation Code associated to an operation.
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - OperationCode
    //
    // RETURNS:
    //      - Boolean: If AccountId has been retrieved correctly.
    //
    public static Boolean DB_GetOperationCode(Int64 OperationId,
                                                       out OperationCode OperationCode,
                                                       SqlTransaction SqlTrx)
    {
      StringBuilder _sql_txt;
      Object _obj;

      OperationCode = 0;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT   AO_CODE ");
        _sql_txt.AppendLine("  FROM   ACCOUNT_OPERATIONS ");
        _sql_txt.AppendLine(" WHERE   AO_OPERATION_ID = @pOperationId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            OperationCode = (OperationCode)_obj;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetOperationCode


    public static Boolean Trx_SetOperationBalance(Int64 OperationId,
                                                  MultiPromos.AccountBalance OperationBalance,
                                                  SqlTransaction Trx)
    {
      StringBuilder _sb_sql;

      try
      {
        _sb_sql = new StringBuilder();
        _sb_sql.AppendLine(" UPDATE   ACCOUNT_OPERATIONS ");
        _sb_sql.AppendLine("    SET   AO_REDEEMABLE           = @pRedeemable ");
        _sb_sql.AppendLine("        , AO_PROMO_REDEEMABLE     = @pPromoRedeemable ");
        _sb_sql.AppendLine("        , AO_PROMO_NOT_REDEEMABLE = @pPromoNotRedeemable ");
        _sb_sql.AppendLine("  WHERE   AO_OPERATION_ID         = @pOperationId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb_sql.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pRedeemable", SqlDbType.Money).Value = OperationBalance.Redeemable;
          _sql_cmd.Parameters.Add("@pPromoRedeemable", SqlDbType.Money).Value = OperationBalance.PromoRedeemable;
          _sql_cmd.Parameters.Add("@pPromoNotRedeemable", SqlDbType.Money).Value = OperationBalance.PromoNotRedeemable;
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Trx_SetOperationBalanceTITO(Int64 OperationId,
                                                        MultiPromos.AccountBalance OperationBalance,
                                                        SqlTransaction Trx)
    {
      StringBuilder _sb_sql;

      try
      {
        _sb_sql = new StringBuilder();
        _sb_sql.AppendLine(" UPDATE   ACCOUNT_OPERATIONS ");
        _sb_sql.AppendLine("    SET   AO_REDEEMABLE           = @pRedeemable ");
        _sb_sql.AppendLine("        , AO_PROMO_REDEEMABLE     = @pPromoRedeemable ");
        _sb_sql.AppendLine("        , AO_PROMO_NOT_REDEEMABLE = @pPromoNotRedeemable ");
        _sb_sql.AppendLine("        , AO_AMOUNT               = @pRedeemable ");
        _sb_sql.AppendLine("        , AO_NON_REDEEMABLE       = @pPromoRedeemable + @pPromoNotRedeemable ");
        _sb_sql.AppendLine("  WHERE   AO_OPERATION_ID         = @pOperationId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb_sql.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pRedeemable", SqlDbType.Money).Value = OperationBalance.Redeemable;
          _sql_cmd.Parameters.Add("@pPromoRedeemable", SqlDbType.Money).Value = OperationBalance.PromoRedeemable;
          _sql_cmd.Parameters.Add("@pPromoNotRedeemable", SqlDbType.Money).Value = OperationBalance.PromoNotRedeemable;
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Trx_UpdateOperationBalanceTITO(Int64 OperationId,
                                                        MultiPromos.AccountBalance OperationBalance,
                                                        SqlTransaction Trx)
    {
      StringBuilder _sb_sql;

      try
      {
        _sb_sql = new StringBuilder();
        _sb_sql.AppendLine(" UPDATE   ACCOUNT_OPERATIONS ");
        _sb_sql.AppendLine("    SET   AO_REDEEMABLE           = AO_REDEEMABLE + @pRedeemable ");
        _sb_sql.AppendLine("        , AO_PROMO_REDEEMABLE     = AO_PROMO_REDEEMABLE + @pPromoRedeemable ");
        _sb_sql.AppendLine("        , AO_PROMO_NOT_REDEEMABLE = AO_PROMO_NOT_REDEEMABLE + @pPromoNotRedeemable ");
        _sb_sql.AppendLine("        , AO_AMOUNT               = AO_AMOUNT + @pRedeemable ");
        _sb_sql.AppendLine("        , AO_NON_REDEEMABLE       = AO_NON_REDEEMABLE + @pPromoRedeemable + @pPromoNotRedeemable ");
        _sb_sql.AppendLine("  WHERE   AO_OPERATION_ID         = @pOperationId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb_sql.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pRedeemable", SqlDbType.Money).Value = OperationBalance.Redeemable;
          _sql_cmd.Parameters.Add("@pPromoRedeemable", SqlDbType.Money).Value = OperationBalance.PromoRedeemable;
          _sql_cmd.Parameters.Add("@pPromoNotRedeemable", SqlDbType.Money).Value = OperationBalance.PromoNotRedeemable;
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert pending operation in MultiSite
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - Trx
    //
    // RETURNS:
    //      - Boolean: If operatin id is inserted
    //
    public static Boolean InsertPendingOperationMultiSite(Int64 OperationId, OperationCode OperationCode, Currency Amount, Boolean IsChipsSaleRegister, SqlTransaction Trx)
    {
      StringBuilder _sb_sql;
      Int32 _master_mode;

      try
      {
        if (!GeneralParam.GetBoolean("MultiSite", "EnableTask68", true))
        {
          return true;
        }

        _master_mode = GeneralParam.GetInt32("MultiSite", "TablesMasterMode", 0);

        // Recharges with amount 0 not will be send
        if (Amount <= 0 && (OperationCode == OperationCode.PROMOTION
                            || OperationCode == OperationCode.MB_PROMOTION
                            || OperationCode == OperationCode.NA_PROMOTION))
        {
          return true;
        }

        // Chips sale register on table not will send
        if (IsChipsSaleRegister && OperationCode == OperationCode.CHIPS_SALE_WITH_RECHARGE)
        {
          return true;
        }

        if (GeneralParam.GetBoolean("Site", "MultiSiteMember", false) && _master_mode == 1)
        {
          switch (OperationCode)
          {
            case OperationCode.CASH_IN:
            case OperationCode.CASH_OUT:
            case OperationCode.PROMOTION:
            case OperationCode.MB_CASH_IN:
            case OperationCode.MB_PROMOTION:
            case OperationCode.GIFT_REDEEMABLE_AS_CASHIN:
            case OperationCode.CHIPS_SALE:
            case OperationCode.CHIPS_SALE_WITH_RECHARGE:
            case OperationCode.CHIPS_PURCHASE:
            case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
            case OperationCode.NA_CASH_IN:
            case OperationCode.NA_PROMOTION:
              _sb_sql = new StringBuilder();
              _sb_sql.AppendLine(" INSERT   INTO MS_PENDING_TASK68_WORK_DATA ");
              _sb_sql.AppendLine("        ( MPTWD_OPERATION_ID");
              _sb_sql.AppendLine("        , MPTWD_DATETIME ");
              _sb_sql.AppendLine("        ) ");
              _sb_sql.AppendLine(" VALUES ( @pOperationId ");
              _sb_sql.AppendLine("        , GETDATE()");
              _sb_sql.AppendLine("        ) ");

              using (SqlCommand _sql_cmd = new SqlCommand(_sb_sql.ToString(), Trx.Connection, Trx))
              {
                _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

                if (_sql_cmd.ExecuteNonQuery() != 1)
                {
                  return false;
                }
              }
              break;

            default:
              break;
          }
        }
        
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Return 'NumeroRecibo' for Folio from Operation
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="NumeroReciboForFolio"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_GetNumeroReciboForFolioFromOperation(Int64 OperationId,
                                                                  out Int64 NumeroReciboForFolio,
                                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sql_txt;
      Object _obj;

      NumeroReciboForFolio = 0;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT   MIN(CV_VOUCHER_ID)  AS  VOUCHER_ID                          ");
        _sql_txt.AppendLine("  FROM   ACCOUNT_OPERATIONS                                          ");
        _sql_txt.AppendLine("  JOIN   CASHIER_VOUCHERS    ON CV_OPERATION_ID = AO_OPERATION_ID    ");
        _sql_txt.AppendLine(" WHERE   AO_OPERATION_ID = @pOperationId                             ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            NumeroReciboForFolio = (Int64)_obj;

        return true;
      }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Get from OperationCode if it is a Terminal or "Mesas" operation
    /// </summary>
    public static String GetOperationTypeForFolio(OperationCode OpCode)
    {
      switch (OpCode)
      {
        case OperationCode.CHIPS_SALE:
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
        case OperationCode.CHIPS_PURCHASE:
        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
        case OperationCode.CHIPS_SWAP:
          return OPERATION_TYPE_MESAS;      //Mesas operation

        default:
          return OPERATION_TYPE_TERMINAL;   //Terminal operation
      }
    }

    #endregion // Public Methods

  } // Operations

  //Moved the class to Account_Operations

} // WSI.Common
