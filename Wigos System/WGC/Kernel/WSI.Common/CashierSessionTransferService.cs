﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashierSessionTransferService.cs
// 
//   DESCRIPTION: It has methods to manage transfers between two opened cashier sessions.
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 17-AUG-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-AUG-2015 FAV    First release.
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 04-APR-2016 DHA    Product Backlog Item 9756: added chips (RE, NR and Color)
// 31-OCT-2016 JML        Fixed Bug 19865: Gamming tables stock control - not displayed in the cashier
// 31-OCT-2016 JML        Fixed Bug 19866: Chips shange control - not displayed in the cashier
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace WSI.Common
{
  public static class CashierSessionTransferService
  {

    #region Attributes

    private static VoucherTransferCashiersReceived m_voucher_transfer_received = null;
    private static VoucherTransferCashiersSent m_voucher_transfer_sent = null;

    #endregion
    
    #region Public Methods
    
    /// <summary>
    /// It transfers credit from a cashier session (Source) to other cashier session (Target)
    /// </summary>
    /// <param name="SourceSessionInfo"></param>
    /// <param name="TargetCashierSessionId"></param>
    /// <param name="TransferAmount"></param>
    /// <param name="SessionTransferInfoSent"></param>
    /// <param name="VoucherTransferSent"></param>
    /// <param name="SourceGamingTableId"></param>
    /// <param name="ShowBalanceInVoucher"></param>
    /// <returns></returns>
    public static Boolean TransferCreditToCashierSession(CashierSessionInfo SourceSessionInfo, Int64 TargetCashierSessionId,
                                               SortedDictionary<CurrencyIsoType, Decimal> TransferAmount,
                                               out CashierSessionTransferInfo SessionTransferInfoSent,
                                               out VoucherTransferCashiersSent VoucherTransferSent,
                                               int? SourceGamingTableId = null, Boolean ShowBalanceInVoucher = false)
    {
      SqlConnection _sql_conn = null;
      SqlTransaction _sql_trx = null;
      Int64 _operation_source_id;

      SessionTransferInfoSent = null;
      VoucherTransferSent = null;

      try
      {
        Log.Message("TransferCreditToCashierSession: Starting");

        _sql_conn = WSI.Common.WGDB.Connection();

        using (_sql_trx = _sql_conn.BeginTransaction())
        {
          if (RuleValidationsSend(TransferAmount, SourceSessionInfo, TargetCashierSessionId, _sql_trx))
          {
            Log.Message(string.Format("TransferCreditToCashierSession: Transfer Session ID [{0}] to Session ID [{1}]",
                 SourceSessionInfo.CashierSessionId, TargetCashierSessionId));

            InitSentTransferVoucher(SourceSessionInfo, TargetCashierSessionId, _sql_trx);

            _operation_source_id = InsertOperation(CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND, SourceSessionInfo, _sql_trx);

            if (_operation_source_id != 0 
                && InsertMovements(CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND, _operation_source_id, SourceSessionInfo,
                    SourceSessionInfo.CashierSessionId, TargetCashierSessionId, TransferAmount,
                    SourceGamingTableId, ShowBalanceInVoucher, _sql_trx)
                && SaveVoucher(CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND, _operation_source_id, _sql_trx)
                && UpdateGamingTableSession(CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND, SourceSessionInfo, TransferAmount, SourceGamingTableId, _sql_trx)
                && SetTransferWithStatusSent(SourceSessionInfo, TargetCashierSessionId, _operation_source_id, TransferAmount, out SessionTransferInfoSent, _sql_trx))
            {
              VoucherTransferSent = m_voucher_transfer_sent;

              _sql_trx.Commit();
              Log.Message("TransferCreditToCashierSession completed successfully");

              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (_sql_trx != null)
        _sql_trx.Rollback();

      Log.Error("TransferCreditToCashierSession has not been completed");

      return false;
    }
    
    /// <summary>
    /// It receives credit of a transfer from a cashier session (Source) to a cashier session (receiving session)
    /// </summary>
    /// <param name="ReceiveSessionInfo"></param>
    /// <param name="SessionTransferInfoSent"></param>
    /// <param name="SessionTransferInfoReceived"></param>
    /// <param name="VoucherTransferReceived"></param>
    /// <param name="TargetGamingTableId"></param>
    /// <param name="ShowBalanceInVoucher"></param>
    /// <returns></returns>
    public static Boolean ReceiveCreditFromCashierSession(CashierSessionInfo ReceiveSessionInfo, CashierSessionTransferInfo SessionTransferInfoSent,
                                                out CashierSessionTransferInfo SessionTransferInfoReceived,
                                                out VoucherTransferCashiersReceived VoucherTransferReceived,
                                                int? TargetGamingTableId = null, Boolean ShowBalanceInVoucher = false)
    {
      SqlConnection _sql_conn = null;
      SqlTransaction _sql_trx = null;
      Int64 _operation_target_id;

      SessionTransferInfoReceived = null;
      VoucherTransferReceived = null;

      try
      {
        Log.Message("ReceiveCreditFromCashierSession: Starting");

        _sql_conn = WSI.Common.WGDB.Connection();

        using (_sql_trx = _sql_conn.BeginTransaction())
        {
          if (RuleValidationsReceive(SessionTransferInfoSent, ReceiveSessionInfo, _sql_trx))
          {

            Log.Message(string.Format("ReceiveCreditFromCashierSession: Transfer Session ID [{0}] to Session ID [{1}]",
                        SessionTransferInfoSent.SessionSourceId, ReceiveSessionInfo.CashierSessionId));

            InitReceivedTransferVoucher(SessionTransferInfoSent.SessionSourceId, ReceiveSessionInfo, _sql_trx);

            _operation_target_id = InsertOperation(CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE, ReceiveSessionInfo, _sql_trx);

            if (_operation_target_id != 0
                && InsertMovements(CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE, _operation_target_id, ReceiveSessionInfo,
                    SessionTransferInfoSent.SessionSourceId, ReceiveSessionInfo.CashierSessionId,
                    SessionTransferInfoSent.DenominationDetail, TargetGamingTableId, ShowBalanceInVoucher, _sql_trx)
                && SaveVoucher(CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE, _operation_target_id, _sql_trx)
                && UpdateGamingTableSession(CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE, ReceiveSessionInfo, SessionTransferInfoSent.DenominationDetail, TargetGamingTableId, _sql_trx)
                &&SetTransferWithStatusReceived(SessionTransferInfoSent, _operation_target_id, out SessionTransferInfoReceived, _sql_trx))
            { 
              VoucherTransferReceived = m_voucher_transfer_received;

              _sql_trx.Commit();
              Log.Message("ReceiveCreditFromCashierSession completed successfully");

              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (_sql_trx != null)
        _sql_trx.Rollback();

      Log.Error("ReceiveCreditFromCashierSession has not been completed");

      return false;
    }

    /// <summary>
    /// Gets a CashierSessionTransferInfo by Id
    /// </summary>
    /// <param name="TransferId"></param>
    /// <param name="TransferInfo"></param>
    /// <returns></returns>
    public static Boolean GetCashierSessionTransferById(Int64 TransferId, out CashierSessionTransferInfo TransferInfo)
    {

      SqlConnection _sql_conn = null;
      SqlTransaction _sql_trx = null;
      TransferInfo = null;
      try
      {
        Log.Message("GetCashierSessionTransferById: Starting");
        _sql_conn = WSI.Common.WGDB.Connection();

        using (_sql_trx = _sql_conn.BeginTransaction())
        {
          TransferInfo = CashierSessionTransfer.GetTransferById(TransferId, _sql_trx);

          _sql_trx.Commit();
          Log.Message("GetCashierSessionTransferById: completed successfully");

          return true;

        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (_sql_trx != null)
        _sql_trx.Rollback();

      Log.Error("GetCashierSessionTransferById has not been completed");

      return false;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Insert the operation in the database
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <param name="SessionInfo"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Int64 InsertOperation(CASHIER_MOVEMENT MovementType, CashierSessionInfo SessionInfo, SqlTransaction Trx)
    {
      Int64 _operation_id = 0;
      OperationCode _operation_code = OperationCode.NOT_SET;
      switch (MovementType)
      {
        case CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE:
          _operation_code = OperationCode.TRANSFER_CASHIER_SESSIONS_DEPOSIT;
          break;
        case CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND:
          _operation_code = OperationCode.TRANSFER_CASHIER_SESSIONS_WITHDRAWAL;
          break;
        default:
          return 0;
      }

      if (Operations.DB_InsertOperation(_operation_code, 0, SessionInfo.CashierSessionId, 0, 0, 0, 0, 0, 0, string.Empty, out _operation_id, Trx))
      {
        return _operation_id;
      }

      Log.Error("Could not insert the operation");
      return 0;
    }

    /// <summary>
    /// Insert the movement or movements to the cashier movements table
    /// </summary>
    /// <param name="MovementType"></param>
    /// <param name="OperationId"></param>
    /// <param name="MovementCashierSession"></param>
    /// <param name="FillAmount"></param>
    /// <param name="GamingTableID"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean InsertMovements(CASHIER_MOVEMENT MovementType, Int64 OperationId, CashierSessionInfo MovementCashierSession,
                                 Int64 SourceCashierSessionId, Int64 TargetCashierSessionId,
                                 SortedDictionary<CurrencyIsoType, Decimal> FillAmount, int? GamingTableID, Boolean ShowBalanceInVoucher, SqlTransaction Trx)
    {
      Decimal _aux_amount;
      CurrencyExchange _currency_exchange;
      CurrencyExchangeResult _exchange_result;
      GamingTablesSessions _gt_session;
      CashierMovementsTable _cm_mov_table;
      Decimal _balance_currency;
      String _national_currency;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      _cm_mov_table = new CashierMovementsTable(MovementCashierSession);

      if (IsGamingTable(GamingTableID) && MovementType == CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND)
      {
        if (!GamingTablesSessions.GetOrOpenSession(out _gt_session, GamingTableID.Value, MovementCashierSession.CashierSessionId, Trx))
        {
          Log.Error("CashierSessionOpen. Error creating GamingTable sesion. CashierSessionId: " + MovementCashierSession.CashierSessionId.ToString());
          return false;
        }

        if (!FeatureChips.CreateGameCollectedMovements(MovementCashierSession, FillAmount, _gt_session.GamingTableSessionId, false, OperationId, Trx))
        {
          Log.Error("CreateGameCollectedMovements. Error calling to the method. CashierSessionId: " + MovementCashierSession.CashierSessionId.ToString());
          return false;
        }
      }

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in FillAmount)
      {
        _aux_amount = -1;
        switch (_amount.Key.Type)
        {
          case CurrencyExchangeType.CURRENCY:
          case CurrencyExchangeType.CASINOCHIP:

            if (_amount.Key.IsoCode != _national_currency && _amount.Key.IsoCode != Cage.CHIPS_ISO_CODE)
            {
              CurrencyExchange.ReadCurrencyExchange(_amount.Key.Type, _amount.Key.IsoCode, out _currency_exchange, Trx);
              _currency_exchange.ApplyExchange(_amount.Value, out _exchange_result);
              _aux_amount = _exchange_result.GrossAmount;
            }

            if (IsGamingTable(GamingTableID) && MovementType == CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND)
            {
              if (!GamingTablesSessions.GetOrOpenSession(out _gt_session, GamingTableID.Value, MovementCashierSession.CashierSessionId, Trx))
              {
                Log.Error("CashierSessionOpen. Error creating GamingTable sesion. Amount.Key: " + _amount.Key.ToString());
                return false;
              }

              if (_national_currency == _amount.Key.IsoCode)
              {
                _cm_mov_table.Add(OperationId, MovementType, FillAmount[_amount.Key], 0, String.Empty, String.Empty, String.Empty, 0, 0, _gt_session.GamingTableSessionId);
              }
              else
              {
                _cm_mov_table.Add(OperationId, MovementType, FillAmount[_amount.Key], 0, String.Empty, String.Empty, _amount.Key.IsoCode, 0, 0, _gt_session.GamingTableSessionId, _aux_amount, _amount.Key.Type, null, null);
              }
            }
            else
            {
              if (_amount.Key.IsoCode == _national_currency)
              {
                _cm_mov_table.Add(OperationId, MovementType, FillAmount[_amount.Key], 0, "");
              }
              else
              {
                _cm_mov_table.Add(OperationId, MovementType, _amount.Value, 0, String.Empty, String.Empty, _amount.Key.IsoCode, 0, 0, 0, _aux_amount, _amount.Key.Type, null, null);
              }
            }
            break;

          default:
            return false;
        }

        if (_amount.Key.IsoCode == _national_currency && _amount.Key.Type == CurrencyExchangeType.CURRENCY)
        {
          _balance_currency = Cashier.UpdateSessionBalance(Trx, MovementCashierSession.CashierSessionId, 0);
        }
        else
        {
          _balance_currency = Cashier.UpdateSessionBalance(Trx, MovementCashierSession.CashierSessionId, 0, _amount.Key.IsoCode, _amount.Key.Type);
        }

        // Concatenated detail for the voucher
        AddDetailToVoucher(MovementType, _amount, _balance_currency, ShowBalanceInVoucher);
      }

      if (_cm_mov_table.Save(Trx))
      {
        return true;
      }
      else
      {
        Log.Error("Could not insert the movement or movements");
        return false;
      }
      
    }

    /// <summary>
    /// Insert the transfer 
    /// </summary>
    /// <param name="SourceSession"></param>
    /// <param name="TargetSession"></param>
    /// <param name="OperationSourceId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean SetTransferWithStatusSent(CashierSessionInfo SourceSession, Int64 TargetCashierSessionId,
                                           Int64 OperationSourceId, SortedDictionary<CurrencyIsoType, Decimal> FillAmount,
                                           out CashierSessionTransferInfo SessionTransferInfoSent, SqlTransaction Trx)
    { 
      CashierSessionTransferInfo _transfer_cashier_info;
      Boolean _result;
      long _id = 0;

      SessionTransferInfoSent = null;

      _transfer_cashier_info = new CashierSessionTransferInfo()
      {
        SessionSourceId = SourceSession.CashierSessionId,
        SessionTargeId = TargetCashierSessionId,
        OperationSourceId = OperationSourceId,
        OperationTargetId = null,
        DateTime = DateTime.Now,
        DenominationDetailXML = XMLDictionary.SortedDictionaryToXML(FillAmount),
        DenominationDetail = FillAmount,
        Status = CashierSessionTransferStatus.Sent,
      };

      _result = CashierSessionTransfer.InsertTransfer(_transfer_cashier_info, out _id, Trx);

      if (_result)
      {
        _transfer_cashier_info.CashierSessionTransferId = _id;
        SessionTransferInfoSent = _transfer_cashier_info;

        return true;
      }
      else
      {
        Log.Error("There was an exception inserting the tranfer cashier record");

        return false;
      }
    }

    /// <summary>
    /// Update the transfer 
    /// </summary>
    /// <param name="TransferCashierInfo"></param>
    /// <param name="OperationTargetId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean SetTransferWithStatusReceived(CashierSessionTransferInfo TransferCashierInfo,
                                               Int64 OperationTargetId,
                                               out CashierSessionTransferInfo SessionTransferInfoReceived, SqlTransaction Trx)
    {
      CashierSessionTransferInfo _transfer_cashier_info = (CashierSessionTransferInfo)TransferCashierInfo.Clone();
      Boolean _result;
      SessionTransferInfoReceived = null;

      _transfer_cashier_info.Status = CashierSessionTransferStatus.Received;
      _transfer_cashier_info.OperationTargetId = OperationTargetId;
      _transfer_cashier_info.DateTime = DateTime.Now;

      _result = CashierSessionTransfer.UpdateTransfer(_transfer_cashier_info, Trx);

      if (_result)
      {
        SessionTransferInfoReceived = _transfer_cashier_info;

        return true;
      }
      else
      {
        Log.Error("There was an exception updating the tranfer cashier record");

        return false;
      }
    }

    /// <summary>
    /// Rule to validate the parameters when send a transfer
    /// </summary>
    /// <param name="Trx"></param>
    private static Boolean RuleValidationsSend(SortedDictionary<CurrencyIsoType, Decimal> FillAmount,
                                     CashierSessionInfo SourceSession, Int64 TargetCashierSessionId, SqlTransaction Trx)
    {
      if (FillAmount == null || FillAmount.Count == 0)
      {
        Log.Error("The FillAmount parameter doesn't have elements, it cannot to do a transfer without elements (currency or casino chip)");

        return false;
      }

      if (!Cashier.IsSessionOpen(TargetCashierSessionId, Trx))
      {
        Log.Error("The target session is closed. The session should be open.");

        return false;
      }

      if (!Cashier.IsSessionOpen(SourceSession.CashierSessionId, Trx))
      {
        Log.Error("The source session is closed. The session should be open.");

        return false;
      }

      return true;
    }

    /// <summary>
    /// Rule to validate the parameters when receive a transfer
    /// </summary>
    /// <param name="Trx"></param>
    private static Boolean RuleValidationsReceive(CashierSessionTransferInfo TransferCashierInfo, CashierSessionInfo TargetSession, SqlTransaction Trx)
    {
      if (TransferCashierInfo == null)
      {
        Log.Error("The TransferCashierInfo parameter is mandatory and it should be different to NULL");

        return false;
      }

      if (TransferCashierInfo.SessionTargeId != TargetSession.CashierSessionId)
      {
        Log.Error("The TransferCashierInfo parameter has a 'SessionTargetId' different to the TargetSession parameter");

        return false;
      }

      if (TransferCashierInfo.Status != CashierSessionTransferStatus.Sent)
      {
        Log.Error(string.Format("The TransferCashierInfo parameter has a status different to 'Sent'. The current status is '{0}'", TransferCashierInfo.Status));

        return false;
      }

      if (!Cashier.IsSessionOpen(TargetSession.CashierSessionId, Trx))
      {
        Log.Error("The target session is closed. The session should be open.");

        return false;
      }

      if (!Cashier.IsSessionOpen(TransferCashierInfo.SessionSourceId, Trx))
      {
        Log.Error("The source session is closed. The session should be open.");

        return false;
      }

      return true;
    }

    #region Private functions about Vouchers 

    /// <summary>
    /// Initialize the received transfer voucher 
    /// </summary>
    /// <param name="MovementType"></param>
    /// <param name="Trx"></param>
    private static void InitReceivedTransferVoucher(Int64 SourceCashierSessionId, CashierSessionInfo TargetCashierSession, SqlTransaction Trx)
    {
      CashierSessionInfo _source_cashier_session = GetOpenedCashierSessionById(SourceCashierSessionId);

      m_voucher_transfer_received = new VoucherTransferCashiersReceived(CashierVoucherType.CashierTransferCashDeposit, _source_cashier_session, TargetCashierSession, PrintMode.Print, Trx);
    }

    /// <summary>
    /// Initialize the sent transfer voucher
    /// </summary>
    /// <param name="SourceCashierSession"></param>
    /// <param name="TargetCashierSessionId"></param>
    /// <param name="Trx"></param>
    private static void InitSentTransferVoucher(CashierSessionInfo SourceCashierSession, Int64 TargetCashierSessionId, SqlTransaction Trx)
    {
      CashierSessionInfo _target_cashier_session = GetOpenedCashierSessionById(TargetCashierSessionId);

      m_voucher_transfer_sent = new VoucherTransferCashiersSent(CashierVoucherType.CashierTransferCashWithdrawal,  SourceCashierSession, _target_cashier_session, PrintMode.Print, Trx);
    }

    /// <summary>
    /// Add information about the credit to the Voucher
    /// </summary>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="Amount"></param>
    /// <param name="BalanceCurrency"></param>
    /// <param name="ShowBalance"></param>
    private static void AddDetailToVoucher(CASHIER_MOVEMENT MovementType, KeyValuePair<CurrencyIsoType, Decimal> Amount, Decimal BalanceCurrency, Boolean ShowBalance)
    {
        switch (MovementType)
        {
          case CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE:
            VoucherCashDeskOpenBody _voucher_open_body;
            _voucher_open_body = new VoucherCashDeskOpenBody(CASHIER_MOVEMENT.FILLER_IN, Amount.Key, BalanceCurrency, Amount.Value, ShowBalance);
            m_voucher_transfer_received.SetParameterValue("@VOUCHER_CASH_DESK_OPEN_BODY", _voucher_open_body.VoucherHTML);
            break;
          case CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND:
            VoucherCashDeskWithdrawBody _voucher_body;
            _voucher_body = new VoucherCashDeskWithdrawBody(Amount.Key, BalanceCurrency, Amount.Value, ShowBalance);
            m_voucher_transfer_sent.SetParameterValue("@VOUCHER_CASH_DESK_WITHDRAW_BODY", _voucher_body.VoucherHTML);
            break;
        }
    }
   
    /// <summary>
    /// Save the voucher in the vouchers table
    /// </summary>
    /// <param name="MovementType"></param>
    /// <param name="OperationId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean SaveVoucher(CASHIER_MOVEMENT MovementType, Int64 OperationId, SqlTransaction Trx)
    {
      Boolean _save_voucher = false;

      switch (MovementType)
      {
        case CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE:
          m_voucher_transfer_received.AddString("VOUCHER_CASH_DESK_OPEN_BODY", "");
          _save_voucher = m_voucher_transfer_received.Save(OperationId, Trx);
          break;
        case CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND:
          m_voucher_transfer_sent.AddString("VOUCHER_CASH_DESK_WITHDRAW_BODY", ""); 
          _save_voucher = m_voucher_transfer_sent.Save(OperationId, Trx);
          break;
      }

      if (!_save_voucher)
        Log.Error("There was an exception recording the voucher in the table");

      return _save_voucher;
    }

    /// <summary>
    /// Get the CashierSessionInfo by Id
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <returns></returns>
    private static CashierSessionInfo GetOpenedCashierSessionById(Int64 CashierSessionId)
    {
      Int32 _user_id;
      String _user_name;
      Int32 _cashier_id;
      String _cashier_name;
      Currency _final_movement_balance;
      DateTime _dummy_gaming_day;
      CashierSessionInfo CashierSessionInfo = null;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Common.Cashier.DB_ReadOpenedCashierSession(CashierSessionId, _db_trx.SqlTransaction, out _user_id, out _user_name, 
                                                       out _cashier_id, out _cashier_name, out _final_movement_balance, out _dummy_gaming_day))
        {
          CashierSessionInfo = new CashierSessionInfo()
          {
            CashierSessionId = CashierSessionId,
            GamingDay = _dummy_gaming_day,
            TerminalId = _cashier_id,
            TerminalName = _cashier_name,
            UserId = _user_id,
            UserName = _user_name,

            ////******************************
            //IsMobileBank = false,
            //AuthorizedByUserId = _user_id,
            //AuthorizedByUserName = _user_name,
            //UserType = GU_USER_TYPE.USER
            ////******************************
          };
        }
      }

      return CashierSessionInfo;
    }

    #endregion

    #region Private functions about Gaming Table
    /// <summary>
    /// Update total chips and collected for a Gaming table session
    /// </summary>
    /// <param name="MovementType"></param>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="FillAmount"></param>
    /// <param name="GamingTableId"></param>
    /// <param name="sql_trx"></param>
    /// <returns></returns>
    private static Boolean UpdateGamingTableSession(CASHIER_MOVEMENT MovementType, 
                                 CashierSessionInfo CashierSessionInfo, SortedDictionary<CurrencyIsoType, Decimal> FillAmount,
                                 int? GamingTableId, SqlTransaction Trx)
    {
      GamingTablesSessions _gt_session;
      SortedDictionary<CurrencyIsoType, Decimal> _total_amounts;
      GTS_UPDATE_TYPE _gts_update_type = 0;
      Decimal _total_national_amount;

      _total_national_amount = 0;

      if (MovementType == CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND)
        _gts_update_type = GTS_UPDATE_TYPE.FillOut;
      else if (MovementType == CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE)
        _gts_update_type = GTS_UPDATE_TYPE.FillIn;
      else
        return false;
      
      if (IsGamingTable(GamingTableId))
      {
        if (!GamingTablesSessions.GetOrOpenSession(out _gt_session
                                                  , GamingTableId.Value
                                                  , CashierSessionInfo.CashierSessionId
                                                  , Trx))
        {
          Log.Error("UpdateGamingTableSession. Error creating GamingTable sesion. CashierSessionId: " + CashierSessionInfo.CashierSessionId.ToString());
          return false;
        }

        GamingTablesSessions.AcumCurrenciesAndChips(FillAmount, out _total_amounts, out _total_national_amount,  Trx);

        //UPDATE TOTAL CHIPS IN GAMING TABLE SESION
        if (!_gt_session.UpdateSession(_gts_update_type, _total_amounts, Trx))
        {
          Log.Error("UpdateGamingTableSession error. Update total chips in gaming table session ");
          return false;
        }

        WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _session_stats;

        _session_stats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();
        WSI.Common.Cashier.ReadCashierSessionData(Trx, CashierSessionInfo.CashierSessionId, ref _session_stats);

        GamingTablesSessions.GetCollectedAmount(_session_stats, out _total_amounts, out _total_national_amount, Trx);

        // Update total currency amount converted to national amount
        if (!_gt_session.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _total_national_amount, Trx))
        {
          Log.Error("UpdateGamingTableSession error. Update total national amount in gaming table session ");
          return false;
        }

        if (!_gt_session.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _total_amounts, Trx))
        {
          Log.Error("UpdateGamingTableSession error. Update total amount in gaming table session ");
          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// Checks if is gaming table
    /// </summary>
    /// <param name="GamingTableId"></param>
    /// <returns></returns>
    private static Boolean IsGamingTable(int? GamingTableId)
    {
      return (GamingTableBusinessLogic.IsGamingTablesEnabled() && GamingTableId.HasValue && GamingTableId.Value > 0);
    }

    #endregion

    #endregion

  }
}
