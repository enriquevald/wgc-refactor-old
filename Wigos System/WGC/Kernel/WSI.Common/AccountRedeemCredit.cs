//------------------------------------------------------------------------------
// Copyright � 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AccountRedeemCredit.cs
// 
//   DESCRIPTION: Class to redeem credit to accounts
// 
//        AUTHOR: 
// 
// CREATION DATE: 26-FEB-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-FEB-2016 GDA    Product Backlog Item 9363: Pasaje de la funci�n DB_CardCreditRedeem al c�digo com�n
// 25-MAY-2016 ETP    Fixed bug 13631: added some logs.
// 10-JUN-2016 EOR    Product Backlog Item 13612,13613,13614:Generaci�n de constancias: Refactorizaci�n m�dulo de constancias
// 08-JUL-2016 RGR    Bug 12921:Federal and state taxes marked with the discretionary application / removal of active duty
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 13-SEP-2016 EOR    PBI 17468: Televisa -Service Charge: Cashier Movements
// 05-OCT-2016 EOR    PBI 17963: Televisa - Service Charge: Modifications Voucher/Report/Settings
// 30-NOV-2016 JML    PBI 21149: Generation of Withholdind - Direct printing
// 19-DEC-2016 FAV    Bug 21719:CountR: Invalid tax when Cashier.TITOTaxWaiver is enabled
// 13-GEN-2017 JML    Fised Bug 22056: Negative numbers in account movements
// 07-FEB-2017 XGJ&ETP PBI 22249: Machine Draw
// 08-FEB-2017 FAV    PBI 25268:Third TAX - Payment Voucher
// 09-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 16-MAR-2017 ATB    PBI 25737:Third TAX - Report columns
// 20-APR-2017 JML    Fixed Bug 26807: Custody - Incorrect custody amount to be refunded
// 09-MAY-2017 FJC    Fixed Bug 27235: Recarga banco m�vil - Perdida de saldo (Concepto premio)
// 15-JUN-2017 FGB    PBI 27831: [2021] Gaming table tournament - Undo tournament payout to winner (single player)
// 18-JUN-2017 FGB    Bug 27741: Error witholding document - "Folio" field does not show the required value
// 26-JUL-2017 RGR    Bug 28901:WIGOS-3816 Negative cash in Cashier
// 21-NOV-2017 RGR    Bug 30878:WIGOS-6654 WigosGUI is displaying auto generated movements in the screen "Cash movements" with amount 0$ and without any NLS 
// 01-JAN-2018 DHA    Bug 31392:WIGOS-7261 Cashier - Prize payment operation doesn't work. Warning mesage "No es posible realizar la operaci�n" is displayed.
// 01-JAN-2018 DHA    Bug 31393:WIGOS-7793 Service charge: wrong calculation with Terminal draw
// 09-MAR-2018 EOR    Bug 31854: WIGOS-8110 Wrong amount in a withdrawal having terminal and games draws enabled 
//------------------------------------------------------------------------------
using System;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using WSI.Common.TITO;
using WSI.Common.ExternalPaymentAndSaleValidation;

namespace WSI.Common
{
  public partial class AccountsRedeem
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Save the major prize (insert a new record into ACCOUNT_MAJOR_PRIZES)
    // 
    //  PARAMS :
    //      - INPUT :
    //          - OperationId
    //          - Card
    //          - CashRedeem
    //          - SqlTx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public static bool DB_SaveMajorPrize(Int64 OperationId,
                                     WithholdData Withhold,
                                     TYPE_CASH_REDEEM CashRedeem,
                                     SqlTransaction SqlTx)
    {
      SqlCommand _sql_cmd;
      SqlParameter _sql_param;
      String _sql_str;
      Int32 _num_updated_rows;
      String _business_name;
      String _business_id_1;
      String _business_id_2;
      String _representative_name;
      String _representative_id_1;
      String _representative_id_2;
      Int32 _update_account;

      try
      {
        // Obtain business data
        //    - Business 
        //    - Representative

        //    - Business 
        _business_name = Misc.ReadGeneralParams("Witholding.Document", "Business.Name", SqlTx);
        _business_id_1 = Misc.ReadGeneralParams("Witholding.Document", "Business.ID1", SqlTx);
        _business_id_2 = Misc.ReadGeneralParams("Witholding.Document", "Business.ID2", SqlTx);

        //    - Representative
        _representative_name = Misc.ReadGeneralParams("Witholding.Document", "Representative.Name", SqlTx);
        _representative_id_1 = Misc.ReadGeneralParams("Witholding.Document", "Representative.ID1", SqlTx);
        _representative_id_2 = Misc.ReadGeneralParams("Witholding.Document", "Representative.ID2", SqlTx);

        // Add the major prize
        _sql_str = " INSERT INTO ACCOUNT_MAJOR_PRIZES"
                            + "( AMP_OPERATION_ID"
                            + ", AMP_ACCOUNT_ID"
                            + ", AMP_DATETIME"
                            + ", AMP_PRIZE"
                            + ", AMP_WITHOLDING_TAX1"
                            + ", AMP_WITHOLDING_TAX2"
                            + ", AMP_WITHOLDING_TAX3"
                            + ", AMP_PLAYER_NAME"
                            + ", AMP_PLAYER_ID1"
                            + ", AMP_PLAYER_ID2"
                            + ", AMP_BUSINESS_NAME"
                            + ", AMP_BUSINESS_ID1"
                            + ", AMP_BUSINESS_ID2"
                            + ", AMP_REPRESENTATIVE_NAME"
                            + ", AMP_REPRESENTATIVE_ID1"
                            + ", AMP_REPRESENTATIVE_ID2"
                            + ", AMP_PLAYER_NAME1"
                            + ", AMP_PLAYER_NAME2"
                            + ", AMP_PLAYER_NAME3"
                            + ", AMP_PLAYER_GENDER"
                            + ", AMP_PLAYER_BIRTH_DATE"
                            + ", AMP_GENERATED_DOCUMENT"
                             + ")"
                      + " VALUES"
                            + "( @pOperationId"
                            + ", @pAccountId"
                            + ", GETDATE()"
                            + ", @pPrizeAmount"
                            + ", @pWitholdingTax1"
                            + ", @pWitholdingTax2"
                            + ", @pWitholdingTax3"
                            + ", @pPlayerName"
                            + ", @pHolderId1"
                            + ", @pHolderId2"
                            + ", @pBusinessName"
                            + ", @pBusinessId1"
                            + ", @pBusinessId2"
                            + ", @pRepresentativeName"
                            + ", @pRepresentativeId1"
                            + ", @pRepresentativeId2"
                            + ", @pPlayerName1"
                            + ", @pPlayerName2"
                            + ", @pPlayerName3"
                            + ", @pGender"
                            + ", @pBirth"
                            + ", @pGenerateDocument"
                            + ")";
        using (_sql_cmd = new SqlCommand(_sql_str, SqlTx.Connection, SqlTx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Withhold.AccountId;
          _sql_cmd.Parameters.Add("@pPrizeAmount", SqlDbType.Money).Value = CashRedeem.prize.SqlMoney;
          _sql_cmd.Parameters.Add("@pWitholdingTax1", SqlDbType.Money).Value = CashRedeem.tax1.SqlMoney;
          _sql_cmd.Parameters.Add("@pWitholdingTax2", SqlDbType.Money).Value = CashRedeem.tax2.SqlMoney;
          _sql_cmd.Parameters.Add("@pWitholdingTax3", SqlDbType.Money).Value = CashRedeem.tax3.SqlMoney;
          _sql_cmd.Parameters.Add("@pPlayerName", SqlDbType.NVarChar, 150).Value = "";
          _sql_cmd.Parameters.Add("@pHolderId1", SqlDbType.NVarChar, 20).Value = Withhold.HolderId1;
          _sql_cmd.Parameters.Add("@pHolderId2", SqlDbType.NVarChar, 20).Value = Withhold.HolderId2;
          _sql_cmd.Parameters.Add("@pBusinessName", SqlDbType.NVarChar, 150).Value = _business_name;
          _sql_cmd.Parameters.Add("@pBusinessId1", SqlDbType.NVarChar, 20).Value = _business_id_1;
          _sql_cmd.Parameters.Add("@pBusinessId2", SqlDbType.NVarChar, 20).Value = _business_id_2;
          _sql_cmd.Parameters.Add("@pRepresentativeName", SqlDbType.NVarChar, 150).Value = _representative_name;
          _sql_cmd.Parameters.Add("@pRepresentativeId1", SqlDbType.NVarChar, 20).Value = _representative_id_1;
          _sql_cmd.Parameters.Add("@pRepresentativeId2", SqlDbType.NVarChar, 20).Value = _representative_id_2;
          _sql_cmd.Parameters.Add("@pPlayerName1", SqlDbType.NVarChar, 50).Value = Withhold.HolderName1;
          _sql_cmd.Parameters.Add("@pPlayerName2", SqlDbType.NVarChar, 50).Value = Withhold.HolderName2;
          _sql_cmd.Parameters.Add("@pPlayerName3", SqlDbType.NVarChar, 50).Value = Withhold.HolderName3;
          _sql_cmd.Parameters.Add("@pGender", SqlDbType.Int).Value = Withhold.HolderGender;
          _sql_cmd.Parameters.Add("@pBirth", SqlDbType.DateTime).Value = Withhold.HolderBirthDate;
          _sql_cmd.Parameters.Add("@pGenerateDocument", SqlDbType.Bit).Value = Witholding.GenerateWitholdingDocument();

          _num_updated_rows = _sql_cmd.ExecuteNonQuery();

          if (_num_updated_rows != 1)
          {
            Log.Error("DB_SaveMajorPrize. Op= " + OperationId.ToString() + " Card=" + Withhold.AccountId.ToString());

            return false;
          }
        }

        // EOR 10-JUN-2016 
        if (!DB_SaveMajorPrizeToGenerate(OperationId, SqlTx))
        {
          return false;
        }

        if (Witholding.WitholdingDocumentModeGenerate() == Witholding.WITHHOLDING_PRINT_MODE.IMMEDIATE)
        {
          if (Witholding.DB_CreateWitholdingDoc(OperationId, SqlTx).Count <= 0)
          {
            return false;
          }
        }

        // Update Account when the account is "personal" and G.P. UpdateAccount is true.
        Int32.TryParse(GeneralParam.Value("Witholding.Document", "UpdateAccount"), out _update_account);
        if (Withhold.HolderLevel == 0 || _update_account == 0)
        {
          return true;
        }

        _sql_str = "  SELECT   AC_HOLDER_NAME = @pOldName"
                 + "    FROM   ACCOUNTS"
                 + "   WHERE   AC_ACCOUNT_ID = @pAccountId";

        _sql_str += ";";

        _sql_str += " UPDATE   ACCOUNTS"
                   + "   SET   AC_HOLDER_NAME         = @pNewName"
                   + "       , AC_HOLDER_NAME1        = @pHolderName1"
                   + "       , AC_HOLDER_NAME2        = @pHolderName2"
                   + "       , AC_HOLDER_NAME3        = @pHolderName3"
                   + "       , AC_HOLDER_ID           = @pHolderId"
                   + "       , AC_HOLDER_ID1          = CASE WHEN @pHolderId1 IS NULL THEN AC_HOLDER_ID1 ELSE @pHolderId1 END"
                   + "       , AC_HOLDER_ID2          = CASE WHEN @pHolderId2 IS NULL THEN AC_HOLDER_ID2 ELSE @pHolderId2 END"
                   + "       , AC_HOLDER_DOCUMENT_ID1 = @pHolderDocId1"
                   + "       , AC_HOLDER_DOCUMENT_ID2 = @pHolderDocId2"
                   + "       , AC_HOLDER_GENDER       = @pGender"
                   + "       , AC_HOLDER_BIRTH_DATE   = @pBirth"
                   + "       , AC_USER_TYPE           = @pUserType"
                   + " WHERE   AC_ACCOUNT_ID          = @pAccountId";


        using (_sql_cmd = new SqlCommand(_sql_str, SqlTx.Connection, SqlTx))
        {
          _sql_cmd.Parameters.Add("@pNewName", SqlDbType.NVarChar, 200).Value = Withhold.AccountHolderName;
          _sql_cmd.Parameters.Add("@pHolderName1", SqlDbType.NVarChar, 50).Value = Withhold.HolderName1;
          _sql_cmd.Parameters.Add("@pHolderName2", SqlDbType.NVarChar, 50).Value = Withhold.HolderName2;
          _sql_cmd.Parameters.Add("@pHolderName3", SqlDbType.NVarChar, 50).Value = Withhold.HolderName3;
          _sql_cmd.Parameters.Add("@pHolderId", SqlDbType.NVarChar, 20).Value = Withhold.HolderId;

          // Do not update RFC and CURP for generic RFCs
          // DHA 03-NOV-2014 when is RFC generic, don't update RFC & CURP on account
          if (!ValidateFormat.IsGenericRFC(Withhold.HolderId1))
          {
            _sql_cmd.Parameters.Add("@pHolderId1", SqlDbType.NVarChar, 20).Value = Withhold.HolderId1;
            _sql_cmd.Parameters.Add("@pHolderId2", SqlDbType.NVarChar, 20).Value = Withhold.HolderId2;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pHolderId1", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pHolderId2", SqlDbType.NVarChar, 20).Value = DBNull.Value;
          }

          _sql_cmd.Parameters.Add("@pHolderDocId1", SqlDbType.BigInt).Value = Withhold.HolderDocumentId1;
          _sql_cmd.Parameters.Add("@pHolderDocId2", SqlDbType.BigInt).Value = Withhold.HolderDocumentId2;
          _sql_cmd.Parameters.Add("@pGender", SqlDbType.Int).Value = Withhold.HolderGender;
          _sql_cmd.Parameters.Add("@pBirth", SqlDbType.DateTime).Value = Withhold.HolderBirthDate;
          _sql_cmd.Parameters.Add("@pUserType", SqlDbType.Int).Value = ACCOUNT_USER_TYPE.PERSONAL;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = Withhold.AccountId;

          _sql_param = _sql_cmd.Parameters.Add("@pOldName", SqlDbType.NVarChar, 50);
          _sql_param.Direction = ParameterDirection.Output;

          _num_updated_rows = _sql_cmd.ExecuteNonQuery();

          if (_num_updated_rows != 1)
          {
            Log.Error("DB_UpdateWithold. Card not updated.");

            return false;
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

        return false;
    } // DB_SaveMajorPrize

    // EOR 10-JUN-2016 
    // PURPOSE : Save id major prizes to generate (insert a new record into major_prizes_to_generate)
    // 
    //  PARAMS :
    //      - INPUT :
    //          - OperationId
    //          - SqlTx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    private static bool DB_SaveMajorPrizeToGenerate(Int64 OperationId,
                                                    SqlTransaction SqlTx)
    {
      SqlCommand _sql_cmd;
      String _sql_str;
      
      try
      {
        // Add the major prize
        _sql_str = " IF NOT EXISTS (SELECT mpg_operation_id FROM major_prizes_to_generate "
                 + " WHERE mpg_operation_id = @pOperationId) "
                 + " BEGIN "
                 + " INSERT INTO major_prizes_to_generate(mpg_operation_id) VALUES(@pOperationId) "
                 + " END ";

        using (_sql_cmd = new SqlCommand(_sql_str, SqlTx.Connection, SqlTx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("DB_SaveMajorPrizeToGenerate. Op= " + OperationId.ToString());

            return false;
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

        return false;
      }

    /// <summary>
    /// Subtract cash from card: The amount to redeem is substracted from initial cash in
    /// and the rest is taken from won amount. (+ taxes to make achieve the amount to redeem)
    /// Redeem amount priority order:
    ///   3.1 Credit.
    ///   3.2 Not redemable credit.
    ///   3.3 Prize Amount.
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="AmountToSubstract">Initial Cash In amount to substract.</param>
    /// <param name="CashMode">PARTIAL or TOTAL REDEEM.</param>
    /// <returns>true:  Amount substracted.
    ///          false: Error substracting amount. </returns>
    ///     

    public static Boolean DB_CardCreditRedeem(Int64 AccountId,
                                              WithholdData Withhold,
                                              PaymentOrder PayOrder,
                                              Currency AmountToSubstract,
                                              CASH_MODE CashMode,
                                              List<VoucherValidationNumber> ValidationNumbers,
                                              CreditRedeemSourceOperationType OperationType,
                                              CardData SourceCardData,
                                              OperationCode OpCode,
                                              SqlTransaction SqlTrx,
                                              Boolean IsApplyTaxes,
                                              CashierSessionInfo SessionInfo,
                                              out Currency TotalTaxes,
                                              out ArrayList VoucherList,
                                              out Boolean WitholdingNeeded,
                                              out String ErrorMessage,
                                              ref Int64 OperationId,
                                              ExternalValidationInputParameters ExpedInputParams = null,
                                              ExternalValidationOutputParameters ExpedOutputParams = null)
    {
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      Boolean _redeem_success;

      _account_movements = new AccountMovementsTable(SessionInfo);
      _cashier_movements = new CashierMovementsTable(SessionInfo);

      ErrorMessage = String.Empty;

      if (DB_CardCreditRedeem(AccountId, Withhold, PayOrder, AmountToSubstract, CashMode, ValidationNumbers,
                              OperationType, SourceCardData, OpCode, SqlTrx, IsApplyTaxes, _account_movements, _cashier_movements,
                              out TotalTaxes, out VoucherList, out WitholdingNeeded, out _redeem_success, out ErrorMessage, ref OperationId,
                              ExpedInputParams, ExpedOutputParams))
      {
        try
        {
          if (!_redeem_success)
          {
            if (!_account_movements.Save(SqlTrx))
            {
              Log.Error("DB_CardCreditRedeem: Could not save account movements");
              return false;
            }

            if (!_cashier_movements.Save(SqlTrx))
            {
              Log.Error("DB_CardCreditRedeem: Could not save cashier movements");
              return false;
            }

            // Success ALL SAVED IN DB
            ErrorMessage = String.Empty;
            _redeem_success = true;

            return true;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          if (!_redeem_success)
          {
            // TODO: Error Gen�rico
          }
        }
      }

      Log.Error("DB_CardCreditRedeem: Operation not completed");

      return false;
    } // DB_CardCreditRedeem    

    public static Boolean DB_CardCreditRedeem(Int64 AccountId,
                                              WithholdData Withhold,
                                              PaymentOrder PayOrder,
                                              Currency AmountToSubstract,
                                              CASH_MODE CashMode,
                                              List<VoucherValidationNumber> ValidationNumbers,
                                              CreditRedeemSourceOperationType OperationType,
                                              CardData SourceCardData,
                                              OperationCode OpCode,
                                              SqlTransaction SqlTrx,
                                              Boolean IsApplyTaxes,
                                              AccountMovementsTable AccountMovements,
                                              CashierMovementsTable CashierMovements,
                                              out Currency TotalTaxes,
                                              out ArrayList VoucherList,
                                              out Boolean WitholdingNeeded,
                                              out Boolean RedeemSuccess,
                                              out String ErrorMessage,
                                              ref Int64 OperationId,
                                              ExternalValidationInputParameters ExpedInputParams = null,
                                              ExternalValidationOutputParameters ExpedOutputParams = null)
    {
      String _sql_str;
      ArrayList _voucher_promo_list;
      Currency _new_max_devolution;
      TYPE_CASH_REDEEM _cash_redeem_ab;
      TYPE_CASH_REDEEM _cash_company_a;
      TYPE_CASH_REDEEM _cash_company_b;
      TYPE_SPLITS _splits;
      Int32 _gen_param_value;

      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _fin_balance;
      MultiPromos.AccountBalance _to_add;
      MultiPromos.AccountBalance _aux_balance;
      Decimal _ini_points;
      MultiPromos.PromoBalance _ini_promo_balance;
      MultiPromos.PromoBalance _fin_promo_balance;

      CardData _card_data;
      Int64 _play_session_id;

      Currency _payment_order_min_amount;
      DataTable _banks_table;
      DataRow[] _bank_selected;

      ENUM_ANTI_MONEY_LAUNDERING_LEVEL _anti_ml_level;
      Boolean _account_already_registered;
      Boolean _threshold_crossed;

      Boolean _is_chips_purchase_with_cash_out;

      Currency _balance_before_cash_out;
      CashAmount _cash;

      String _error_msg;

      // EOR 05-OCT-2016
      Currency _tax_split2;
      Currency _base_tax_split2;

      _cash = new CashAmount();

      // Initialize out variables.
      VoucherList = new ArrayList();
      WitholdingNeeded = false;
      RedeemSuccess = false;
      ErrorMessage = String.Empty;

      _payment_order_min_amount = 0;
      TotalTaxes = 0;

      _is_chips_purchase_with_cash_out = (OperationType == CreditRedeemSourceOperationType.CHIPS_PURCHASE_WITH_CASHOUT);

      try
      {
        // Lock the account
        if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _ini_points, out _play_session_id, SqlTrx))
        {
          Log.Error("DB_CardCreditRedeem: Could not lock account: " + AccountId);
          return false;
        }

        // RCI 12-AUG-2014: When Integrated chip operation, the cash out operation is inserted before MaxDevolution is calculated.
        //                  This produces that MaxDevolution is 0. It is necessary to use the MaxDevolution calculated previous to insert the cash out operation.
        if (OperationType == CreditRedeemSourceOperationType.PAY_HANDPAY ||
           (_is_chips_purchase_with_cash_out))
        {
          _card_data = SourceCardData;
        }
        else
        {
          // Re-read account data
          _card_data = new CardData();
          if (!CardData.DB_CardGetAllData(AccountId, _card_data, SqlTrx))
          {
            Log.Error("DB_CardCreditRedeem: Could not get card data. Accout_Id = " + AccountId);
            return false;
          }
        }
        
        // SDS  14-06-2016 
        if (OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE || OpCode == OperationCode.PROMOTION_WITH_TAXES)
        {
          _card_data.Cancellable.Operations.Clear();
          _card_data.Cancellable.TotalBalance = MultiPromos.AccountBalance.Zero;
          _card_data.Cancellable.TotalOperationAmount = 0;
          _card_data.Cancellable.TotalCashInTaxAmount = 0;
        }
        else
        {
          if (!_card_data.IsVirtualCard && _play_session_id != 0)
          {
            Log.Warning(String.Format("DB_CardCreditRedeem: Virtual account = {0} Or player is in play session = {1}", _card_data.AccountId, _play_session_id));
            return false;
          }
        }

        // Check for enough balance to subtract and make maths
        if (!Split.ReadSplitParameters(out _splits))
        {
          return false;
        }

        if (_is_chips_purchase_with_cash_out && !Utils.IsTitoMode())
        {
          _ini_balance = _card_data.AccountBalance;
        }
        _ini_promo_balance = new MultiPromos.PromoBalance(_ini_balance, _ini_points);

        // TODO: Review if CHECK (si cal propagar canvis cap a dintre de CalculateCashAmount
        //CASH_MODE _cash_mode = (CashMode == CASH_MODE.TOTAL_REDEEM_CHECK ? CASH_MODE.TOTAL_REDEEM : CashMode);

        if (!_is_chips_purchase_with_cash_out)
        {
          if (Misc.IsTerminalDrawEnabled() && !TerminalDraw.TerminalDraw.DB_DeleteTerminalDrawRechargeByAccountId(_card_data.AccountId, TerminalDraw.TerminalDraw.StatusTerminalDraw.PENDING, SqlTrx))
          {
            Log.Error("DB_CardCreditRedeem: Error in DB_DeleteTerminalDrawRechargeByAccountId()");

            return false;
          }
        }

        if (OpCode != OperationCode.PROMOTION_WITH_TAXES && OpCode != OperationCode.PRIZE_PAYOUT && OpCode != OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
        {
          if (!_cash.CalculateCashAmount(CashMode,
                                         _splits,
                                         _ini_promo_balance,
                                         AmountToSubstract, // Only used in CASH_MODE.PARTIAL_REDEEM
                                         _is_chips_purchase_with_cash_out,
                                         IsApplyTaxes,
                                         false,
                                         _card_data,
                                         out _fin_promo_balance,
                                         out _cash_redeem_ab,
                                         out _cash_company_a,
                                         out _cash_company_b))
          {
            return false;
          }
        }
        else
        {
          if (!_cash.CalculateCashAmountWithRedeemableTaxes(CashMode,
                                                            _splits,
                                                            _ini_promo_balance,
                                                            AmountToSubstract, // Only used in CASH_MODE.PARTIAL_REDEEM
                                                            _is_chips_purchase_with_cash_out,
                                                            false,
                                                            _card_data,
                                                            OpCode,
                                                            out _fin_promo_balance,
                                                            out _cash_redeem_ab,
                                                            out _cash_company_a,
                                                            out _cash_company_b))
          {
            return false;
          }
        }

        _fin_balance = _fin_promo_balance.Balance;
        TotalTaxes = _cash_redeem_ab.TotalTaxes;

        if (_fin_balance.TotalBalance == _ini_balance.TotalBalance && _cash_redeem_ab.TotalPaid == 0)
        {
          // Balance Not Changed and Nothing to pay to the player
          RedeemSuccess = true;

          return true;
        }

        // RCI 29-JUL-2013: Anti money laundering check
        if (!Accounts.CheckAntiMoneyLaunderingFilters(AccountId, ENUM_CREDITS_DIRECTION.Redeem, Accounts.AmountByExternalControl(_cash_redeem_ab), SqlTrx,
                                                      out _anti_ml_level, out _threshold_crossed, out _account_already_registered))
        {
          return false;
        }

        switch (_anti_ml_level)
        {
          case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None:
          case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.IdentificationWarning:
          case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.ReportWarning:
            break;

          case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification:
            if (!_account_already_registered)
            {
              Log.Error("DB_CardCreditRedeem: Account " + AccountId.ToString() + ". Identificacion: Must identify in CashDesk.");

              return false;
            }
            break;

          case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Report:
            if (!_account_already_registered)
            {
              Log.Error("DB_CardCreditRedeem: Account " + AccountId.ToString() + ". Report: Must identify in CashDesk.");

              return false;
            }
            break;

          case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.MaxAllowed:
            if (!_account_already_registered)
            {
              Log.Error("DB_CardCreditRedeem: Account " + AccountId.ToString() + ". MaxAllowed: Must identify in CashDesk.");

              return false;
            }

            break;

          default:
            Log.Error("DB_CardCreditRedeem: Account " + AccountId.ToString() + ". Anti MoneyLaundering level unknown: " + _anti_ml_level.ToString() + ".");

            return false;
        }
        // END Anti money laundering check

        _aux_balance = new MultiPromos.AccountBalance(_ini_balance);

        if (_cash_company_a.lost_balance.Balance.TotalBalance > 0 || _cash_company_a.lost_balance.Points > 0)
        {
          MultiPromos.PromoBalance _lost_balance;
          Int32 _num_applied;

          // Cancel (_cash_company_a.lost_nr_promos)
          _num_applied = AccountPromotion.Trx_PromotionApplySystemAction(AccountPromotion.Trx_CancelByPlayerPromotion,
                                                                         _cash_company_a.lost_promotions, false, out _lost_balance, SqlTrx);
          if (_num_applied != _cash_company_a.lost_promotions.Rows.Count
              || _lost_balance.Balance.TotalBalance > _cash_company_a.lost_balance.Balance.TotalBalance
              || _lost_balance.Points > _cash_company_a.lost_balance.Points)
          {
            return false;
          }

          _aux_balance = _ini_balance - _lost_balance.Balance;
        }

        _aux_balance.CustodyProvision = Math.Min(_aux_balance.CustodyProvision, _cash_company_a.DevolutionCustody);

        // TODO ANDREU URGENTE: Solo la parte de A
        _to_add = _fin_balance - _aux_balance;

        // 04-NOV-2015 AVZ    Backlog Item 6145:BonoPLUS: Reserved credit
        //Do not use the General Param - Always update the reserved field
        if (CashMode == CASH_MODE.TOTAL_REDEEM)
        {
          _to_add.Reserved = -_ini_balance.Reserved;
        }
        else
        {
          decimal redeemableAfterRedeem;
          redeemableAfterRedeem = _ini_balance.TotalRedeemable + _to_add.TotalRedeemable;
          if (redeemableAfterRedeem < _ini_balance.Reserved)
          {
            _to_add.Reserved = redeemableAfterRedeem - _ini_balance.Reserved;
          }
        }

        if (_is_chips_purchase_with_cash_out && !Utils.IsTitoMode())
        {
          DevolutionPrizeParts _parts;
          _parts = new DevolutionPrizeParts(OperationType == CreditRedeemSourceOperationType.CHIPS_PURCHASE_WITH_CASHOUT);

          _parts.MaxDevolutionCustody = _card_data.Cancellable.TotalBalance.CustodyProvision;          
          _to_add.CustodyProvision -= _parts.CalculateDevolutionCustody(_to_add.Redeemable * -1, _card_data.MaxDevolutionForChips, OperationType == CreditRedeemSourceOperationType.CHIPS_PURCHASE_WITH_CASHOUT);
        }

        if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, _to_add, out _aux_balance, SqlTrx))
        {
          return false;
        }

        if (_fin_balance != _aux_balance && !(_is_chips_purchase_with_cash_out && !Utils.IsTitoMode()))
        {
          return false;
        }

        if (_cash_redeem_ab.is_deposit)
        {
          // ACC 16-FEB-2010 Future DepositAnounimous and DepositNonAnounimous Flag in general parameters
          _sql_str = "UPDATE   ACCOUNTS " +
                     "   SET   AC_DEPOSIT    = 0 " +
                     "       , AC_CARD_PAID  = 0 " +
                     " WHERE   AC_ACCOUNT_ID = @pAccountId ";

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _card_data.AccountId;
            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }
        }

        // RCI 28-AUG-2014: It doesn't matter if it's a purchase with cashout, if TotalRedeemable is 0, new max devolution will be 0.
        if (_fin_balance.TotalRedeemable == 0 && !(OpCode == OperationCode.PROMOTION_WITH_TAXES || OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)) // BUG: 14234 
        {
          _new_max_devolution = 0;
        }
        else
        {
          _new_max_devolution = Math.Max(0, _card_data.MaxDevolution - _cash_company_a.TotalDevolution);
        }

        if (!_is_chips_purchase_with_cash_out || Utils.IsTitoMode())
        {

          _sql_str = "UPDATE ACCOUNTS " +
                     "SET AC_INITIAL_CASH_IN          = @pNewMaxDevolution, ";
          if (!(OpCode == OperationCode.PROMOTION_WITH_TAXES || OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE))
            _sql_str += "    AC_CANCELLABLE_OPERATION_ID = NULL, ";

          _sql_str += "    AC_LAST_ACTIVITY            = GETDATE() "; 
          _sql_str += "WHERE AC_ACCOUNT_ID             = @pAccountId ";

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pNewMaxDevolution", SqlDbType.Money).Value = _new_max_devolution.SqlMoney;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _card_data.AccountId;

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }
        }

        if (PayOrder.CheckPayment > 0)
        {
          if (PayOrder.TotalPayment != PayOrder.CheckPayment + PayOrder.CashPayment)
          {
            return false;
          }

          if ((!Utils.IsTitoMode()) && (PayOrder.TotalPayment != _cash_redeem_ab.TotalPaid))
          {
            return false;
          }
        }

        if (OperationId == 0)
        {
          // RCI 15-SEP-2010: Insert Operation before movements. Need to pass OperationId to DB_InsertCardMovement().
          if (!Operations.DB_InsertOperation(OperationCode.CASH_OUT, AccountId, CashierMovements.CashierSessionInfo.CashierSessionId,
                                             0, 0, _cash_redeem_ab.TotalPaid, 0,
                                             0,                      // Operation Data
                                             0,
                                             string.Empty,
                                             out OperationId,
                                             out _error_msg,
                                             SqlTrx,
                                             ExpedInputParams,
                                             ExpedOutputParams))
          {
            ErrorMessage = _error_msg;
            Log.Error("DB_CardCreditRedeem. Error llamando a DB_InsertOperation. CardId: " + _card_data.AccountId);

            return false;
          }
        }

        Boolean[] _witholding_required;
        if (!Witholding.CheckWitholding(_cash_redeem_ab.prize, out WitholdingNeeded, out _witholding_required))
        {
          return false;
        }

        //Check withholding
        if (WitholdingNeeded)
        {
          if ((CashMode == CASH_MODE.PARTIAL_REDEEM) && (!_is_chips_purchase_with_cash_out) && (Withhold == null))
          {
            Log.Error("DB_CardCreditRedeem. Can't do a partial redeem if Withold is needed. CardId: " + _card_data.AccountId.ToString());

            return false;
          }
        }

        // Must generate Payment Order document.
        if (PayOrder.CheckPayment > 0)
        {
          // Payment Orders disabled
          if (!PaymentOrder.IsEnabled())
          {
            return false;
          }
          // Check payment can't be less than the minimum amount of the payment orders.
          // Minimum amount has been changed 
          if (PayOrder.CheckPayment < PaymentOrder.MinAmount())
          {
            return false;
          }
          // Get the enabled bank accounts.
          if (!PaymentOrder.BanksAccounts(true, out _banks_table, SqlTrx))
          {
            return false;
          }

          // DCS 16-03-2015: Only save account info and bank info when generate document is enabled
          if (PaymentOrder.IsGenerateDocumentEnabled())
          {
            // Check the selected bank id is enabled (can be changed while in redeem process...)
            _bank_selected = _banks_table.Select("BA_ACCOUNT_ID = " + PayOrder.BankId);
            if (_bank_selected.Length == 0)
            {
              return false;
            }

            // Update bank account data if changed (while in progress...)
            PayOrder.BankName = (String)_bank_selected[0][1];
            PayOrder.EffectiveDays = (String)_bank_selected[0][3];
            PayOrder.BankCustomerNumber = (String)_bank_selected[0][4];
            PayOrder.BankAccountNumber = (String)_bank_selected[0][5];
            PayOrder.PaymentType = (String)_bank_selected[0][6];
          }
          // Generate Payment Order documents
          //    - Create Order Payment
          //    - Generate Order Payment and link them to the order payment

          PayOrder.AccountBalance = _card_data.AccountBalance.TotalBalance;
          PayOrder.CashRedeem = _cash_redeem_ab;

          if (!PaymentOrder.DB_Save(OperationId, PayOrder, SqlTrx))
          {
            Log.Error("DB_CardCreditRedeem.Save Payment Order OpId=" + OperationId.ToString()
                                                        + "CardId=" + _card_data.AccountId.ToString());

            return false;
          }

          // DCS 16-03-2015: Only create document when generate document is enabled
          if (PaymentOrder.IsGenerateDocumentEnabled())
          {
            if (!PaymentOrder.DB_CreatePaymentOrderDoc(OperationId, SqlTrx))
            {
              Log.Error("DB_CardCreditRedeem.Save Payment Order OpId=" + OperationId.ToString()
                                                          + "CardId=" + _card_data.AccountId.ToString());

              return false;
            }
          }
        }

        // DDM 12-SEP-2012: Calculate and save promotion cost.
        if (!AccountPromotion.Trx_SetRedeemableCostOfPromotionNRE(_card_data.AccountId, _cash_company_a.lost_promotions, _cash_redeem_ab.TotalRedeemed,
                                                                  _fin_balance.TotalRedeemable, SqlTrx))
        {
          return false;
        }

        // Insert Account Movements:
        // 1. Cash Out. (If Exists)
        // 2. Won amount. 
        // 3. Taxes from won. (If exists)

        // Insert Cashier movements:
        // 1. Taxes: Federal and State.
        // 2. Prizes 

        Currency x;

        x = _card_data.CurrentBalance;

        if (_is_chips_purchase_with_cash_out && (!Utils.IsTitoMode() || Misc.IsReceptionEnabled()))
        {
          Currency _chips_amount;

          _chips_amount = _ini_balance.Redeemable;// _card_data.CurrentBalance;
          // Re-read account data
          _card_data = new CardData();
          if (!CardData.DB_CardGetAllData(AccountId, _card_data, SqlTrx))
          {
            return false;
          }
          _card_data.AccountBalance.Redeemable += _chips_amount;
          _card_data.CurrentBalance += _chips_amount;

          x = _card_data.CurrentBalance;
        }

        // INSERT MOVEMENT CHEQUE, DEPOSITS ON THE CASHIER BALANCE THE AMOUNT OF DE BANK CHECK
        if (PayOrder.CheckPayment > 0)
        {
          CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CHECK_PAYMENT, PayOrder.CheckPayment, _card_data.AccountId, _card_data.TrackData);
        }

        if (IsApplyTaxes)
        {
          CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CASH_OUT, _cash_redeem_ab.TotalRedeemAfterTaxes, _card_data.AccountId, _card_data.TrackData);
        }
        else
        {
          CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CASH_OUT, _cash_redeem_ab.TotalRedeemed, _card_data.AccountId, _card_data.TrackData);
        }

        // Insert Account Movements:
        // 1. Cash Out.
        if (_cash_company_a.TotalDevolution > 0)
        {
          // AJQ 12-AUG-2013, Split "Total Devolution" between Cancellation & Devolution
          //////// Dev & Cancel together
          //////AccountMovements.Add(_operation_id, _card_data.AccountId, MovementType.Devolution, x, _cash_company_a.TotalDevolution, 0, x - _cash_company_a.TotalDevolution);
          //////x = x - _cash_company_a.TotalDevolution;

          // Dev & Cancellation parts
          if (_cash_company_a.cancellation > 0)
          {
            AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.Cancellation, x, _cash_company_a.cancellation, 0, x - _cash_company_a.cancellation);
            x = x - _cash_company_a.cancellation;
          }

          if (_cash_company_a.devolution > 0)
          {
            AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.Devolution, x, _cash_company_a.devolution, 0, x - _cash_company_a.devolution);
            x = x - _cash_company_a.devolution;
          }
        }

        if (_cash_company_a.cancellation > 0)
        {
          CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CANCEL_SPLIT1, _cash_company_a.cancellation, _card_data.AccountId, _card_data.TrackData);
        }

        // 09-MAY-2017 FJC    Fixed Bug 27235:Recarga banco m�vil - Perdida de saldo (Concepto premio)
        if ((_cash_company_a.cancellation > 0 && _splits.company_b.cancellation_pct > 0)
           || (Misc.Common_CheckPendingTerminalDrawsRecharges(_card_data.AccountId) && _splits.company_b.cancellation_pct > 0))
          {
            CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CANCEL_SPLIT2, _cash_company_b.cancellation, _card_data.AccountId, _card_data.TrackData);

            // JBP 07-03-2017: Only need withdraw real promotions redeemable amount (Not terminal draw)
            Currency _cancellation_b;

            _cancellation_b = _cash_redeem_ab.lost_balance.Balance.PromoRedeemable;

            if (Misc.Common_CheckPendingTerminalDrawsRecharges(AccountId))
            {
              _cancellation_b = (_cash_redeem_ab.lost_balance.Balance.PromoRedeemable - Misc.Common_GetPendingAmountTerminalDrawsRecharges(AccountId));
            }

            if (_cancellation_b >= _cash_company_b.cancellation)
            {
              Decimal _promo_redeemable_lost;

              AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.CancelPromotionRedeemable, x, _cash_company_b.cancellation, 0, x - _cash_company_b.cancellation);
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE, _cash_company_b.cancellation, _card_data.AccountId, _card_data.TrackData);
              // Update PromoRedeemableLost
              _promo_redeemable_lost = _cancellation_b - _cash_company_b.cancellation;
              x = x - _cash_company_b.cancellation;

              if (_promo_redeemable_lost != 0)
              {
                // TODO: HARL!!!
              }
            }
          }

        if (_cash_company_a.devolution > 0)
        {
          CashierMovements.Add(OperationId, CASHIER_MOVEMENT.DEV_SPLIT1, _cash_company_a.devolution, _card_data.AccountId, _card_data.TrackData);
          if (_splits.company_b.devolution_pct > 0)
          {
            CashierMovements.Add(OperationId, CASHIER_MOVEMENT.DEV_SPLIT2, _cash_company_b.devolution, _card_data.AccountId, _card_data.TrackData);           
          }
        }

        if (_cash_redeem_ab.prize > 0)
        {
          // 2. Prize
          AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.CashOut, x, _cash_redeem_ab.prize, _cash_redeem_ab.TotalTaxes, x - _cash_redeem_ab.prize + _cash_redeem_ab.TotalTaxes);
          CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZES, _cash_redeem_ab.prize, _card_data.AccountId, _card_data.TrackData);
          x = x - _cash_redeem_ab.prize + _cash_redeem_ab.TotalTaxes;

          // Tax 1
          #region Tax 1
          if (OpCode == OperationCode.PROMOTION_WITH_TAXES || OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
          {
            AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.PromotionRedeemableFederalTax, x, _cash_redeem_ab.tax1, 0, x - _cash_redeem_ab.tax1);
          }
          else
          {
            AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.TaxOnPrize1, x, _cash_redeem_ab.tax1, 0, x - _cash_redeem_ab.tax1);
          }

          if (Tax.EnableTitoTaxWaiver(true))
          {
            Decimal _tax_1;
            Decimal _tax_recalculate;

            if (IsApplyTaxes)
            {
              if (OperationType == WSI.Common.CreditRedeemSourceOperationType.CHIPS_PURCHASE_WITH_CASHOUT)
              {
                _tax_1 = Math.Round(GeneralParam.GetDecimal("GamingTables", "Tax.OnPrize.1.Pct"), 2, MidpointRounding.AwayFromZero);
              }
              else
              {
                _tax_1 = Math.Round(GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.1.Pct"), 2, MidpointRounding.AwayFromZero);
              }
              _tax_recalculate = (_tax_1 / 100) * (Math.Round(_cash_redeem_ab.prize, 2, MidpointRounding.AwayFromZero));
              _tax_recalculate = Math.Round(_tax_recalculate, 2, MidpointRounding.AwayFromZero);
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_ON_PRIZE1, _tax_recalculate, _card_data.AccountId, _card_data.TrackData);
            }
          }
          else
          {
            if (OpCode == OperationCode.PROMOTION_WITH_TAXES || OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX, _cash_redeem_ab.tax1, _card_data.AccountId, _card_data.TrackData);
            }
            else
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_ON_PRIZE1, _cash_redeem_ab.tax1, _card_data.AccountId, _card_data.TrackData);
            }
          }
          x = x - _cash_redeem_ab.tax1;
          #endregion

          // Tax 2
          #region Tax 2
          if (OpCode == OperationCode.PROMOTION_WITH_TAXES || OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
          {
            AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.PromotionRedeemableStateTax, x, _cash_redeem_ab.tax2, 0, x - _cash_redeem_ab.tax2);
          }
          else
          {
            AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.TaxOnPrize2, x, _cash_redeem_ab.tax2, 0, x - _cash_redeem_ab.tax2);
          }

          if (Tax.EnableTitoTaxWaiver(true))
          {
            Decimal _tax_2;
            Decimal _tax_recalculate;

            if (IsApplyTaxes)
            {
              if (OperationType == WSI.Common.CreditRedeemSourceOperationType.CHIPS_PURCHASE_WITH_CASHOUT)
              {
                _tax_2 = Math.Round(GeneralParam.GetDecimal("GamingTables", "Tax.OnPrize.2.Pct"), 2, MidpointRounding.AwayFromZero);
              }
              else
              {
                _tax_2 = Math.Round(GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.2.Pct"), 2, MidpointRounding.AwayFromZero);
              }

              _tax_recalculate = (_tax_2 / 100) * (_cash_redeem_ab.prize);
              _tax_recalculate = Math.Round(_tax_recalculate, 2, MidpointRounding.AwayFromZero);
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_ON_PRIZE2, _tax_recalculate, _card_data.AccountId, _card_data.TrackData);
            }
          }
          else
          {
            if (OpCode == OperationCode.PROMOTION_WITH_TAXES || OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX, _cash_redeem_ab.tax2, _card_data.AccountId, _card_data.TrackData);
            }
            else
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_ON_PRIZE2, _cash_redeem_ab.tax2, _card_data.AccountId, _card_data.TrackData);
            }
          }
          x = x - _cash_redeem_ab.tax2;
          #endregion

          // Tax 3
          #region Tax 3
          if (OpCode == OperationCode.PROMOTION_WITH_TAXES || OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
          {
            AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.PromotionRedeemableCouncilTax, x, _cash_redeem_ab.tax3, 0, x - _cash_redeem_ab.tax3);
          }
          else
          {
            if (_cash_redeem_ab.tax3 > 0)
            {
              AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.TaxOnPrize3, x, _cash_redeem_ab.tax3, 0, x - _cash_redeem_ab.tax3);
            }
          }

          if (Tax.EnableTitoTaxWaiver(true))
          {
            Decimal _tax_3;
            Decimal _tax_recalculate;

            if (IsApplyTaxes)
            {
              if (OperationType == WSI.Common.CreditRedeemSourceOperationType.CHIPS_PURCHASE_WITH_CASHOUT)
              {
                _tax_3 = Math.Round(GeneralParam.GetDecimal("GamingTables", "Tax.OnPrize.3.Pct"), 2, MidpointRounding.AwayFromZero);
              }
              else
              {
                _tax_3 = Math.Round(GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.3.Pct"), 2, MidpointRounding.AwayFromZero);
              }

              _tax_recalculate = (_tax_3 / 100) * (_cash_redeem_ab.prize);
              _tax_recalculate = Math.Round(_tax_recalculate, 2, MidpointRounding.AwayFromZero);
              if (_tax_recalculate > 0)
              {
                CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_ON_PRIZE3, _tax_recalculate, _card_data.AccountId, _card_data.TrackData);
              }
            }
          }
          else
          {
            if (OpCode == OperationCode.PROMOTION_WITH_TAXES || OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX, _cash_redeem_ab.tax3, _card_data.AccountId, _card_data.TrackData);
            }
            else
            {
              if (_cash_redeem_ab.tax3 > 0)
              {
                CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_ON_PRIZE3, _cash_redeem_ab.tax3, _card_data.AccountId, _card_data.TrackData);
              }
            }
          }
          x = x - _cash_redeem_ab.tax3;
          #endregion


          // RRB 10-SEP-2012
          if (_cash_redeem_ab.tax_return > 0)
          {
            AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.TaxReturningOnPrizeCoupon, x, _cash_redeem_ab.tax_return, _cash_redeem_ab.tax_return, x);
            CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON, _cash_redeem_ab.tax_return, _card_data.AccountId, _card_data.TrackData);
            x = x - _cash_redeem_ab.tax_return + _cash_redeem_ab.tax_return;
          }
        }

        //EOR 20-APR-2016 
        if (_cash_redeem_ab.DevolutionCustody > 0)
        {
          AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.TaxReturnCustody, _cash_redeem_ab.DevolutionCustody, _cash_redeem_ab.DevolutionCustody, _cash_redeem_ab.DevolutionCustody, x);
          CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_RETURNING_CUSTODY, _cash_redeem_ab.DevolutionCustody, _card_data.AccountId, _card_data.TrackData);
          x = x - _cash_redeem_ab.DevolutionCustody + _cash_redeem_ab.DevolutionCustody;
        }

        // RRB 10-SEP-2012
        if (_cash_redeem_ab.decimal_rounding != 0)
        {
          AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.DecimalRounding, x, _cash_redeem_ab.decimal_rounding, _cash_redeem_ab.decimal_rounding, x);
          CashierMovements.Add(OperationId, CASHIER_MOVEMENT.DECIMAL_ROUNDING, _cash_redeem_ab.decimal_rounding, _card_data.AccountId, _card_data.TrackData);
          x = x - _cash_redeem_ab.decimal_rounding + _cash_redeem_ab.decimal_rounding;
        }

        //EOR 13-SEP-2016 Service Charge
        if (_cash_redeem_ab.service_charge > 0)
        {
          Accounts.CalculateSplit2Tax(_cash_redeem_ab.service_charge, out _base_tax_split2, out _tax_split2);

          AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.ServiceCharge, x, _cash_redeem_ab.service_charge, _cash_redeem_ab.service_charge, x);

          CASHIER_MOVEMENT _service_charge_movement;
          if (!_is_chips_purchase_with_cash_out)
          {
            _service_charge_movement = CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A;
            if (GeneralParam.GetString("Cashier.ServiceCharge", "AttributedToCompany", "B").ToUpper() == "B")
            {
              _service_charge_movement = CASHIER_MOVEMENT.SERVICE_CHARGE;
            }
          }
          else
          {
            _service_charge_movement = CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A;
            if (GeneralParam.GetString("GamingTables.ServiceCharge", "AttributedToCompany", "B").ToUpper() == "B")
            {
              _service_charge_movement = CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE;
            }
          }


          CashierMovements.Add(OperationId, _service_charge_movement, _cash_redeem_ab.service_charge, _card_data.AccountId, _card_data.TrackData, "", "", 0, 0, 0, _tax_split2);

          x = x - _cash_redeem_ab.service_charge + _cash_redeem_ab.service_charge;
        }

        _voucher_promo_list = null;

        // During a total redeem all non-redeemable balance is cancelled
        if (_cash_redeem_ab.lost_balance.Balance.TotalBalance > 0 || _cash_redeem_ab.lost_balance.Points > 0)
        {
          //SDS 18-05-2016 Bug 13001
          if (_cash_redeem_ab.lost_balance.Balance.TotalNotRedeemable > 0)
          {
            if (OpCode != OperationCode.PRIZE_PAYOUT && OpCode != OperationCode.PRIZE_PAYOUT_AND_RECHARGE && OpCode != OperationCode.PROMOTION_WITH_TAXES)
            {
              AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.CancelNotRedeemable, x, _cash_redeem_ab.lost_balance.Balance.TotalNotRedeemable, 0, x - _cash_redeem_ab.lost_balance.Balance.TotalNotRedeemable);
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE, _cash_redeem_ab.lost_balance.Balance.TotalNotRedeemable, _card_data.AccountId, _card_data.TrackData);
              x = x - _cash_redeem_ab.lost_balance.Balance.TotalNotRedeemable;
            }
          }

          if (_cash_redeem_ab.lost_balance.Points > 0)
          {
            AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.CancelPromotionPoint, _ini_points, _cash_redeem_ab.lost_balance.Points, 0, _ini_points - _cash_redeem_ab.lost_balance.Points);
            CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT, _cash_redeem_ab.lost_balance.Points, _card_data.AccountId, _card_data.TrackData);
          }

          if (_cash_company_a.lost_promotions != null && _cash_company_a.lost_promotions.Rows.Count > 0)
          {
            if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier.Voucher", "VoucherOnPromotionCancelled"), out _gen_param_value))
            {
              _gen_param_value = 0;
            }

            if (_gen_param_value > 0)
            {
              if (!VoucherBuilder.PromoLost(_card_data.VoucherAccountInfo(), _card_data.AccountId
                                                          , _splits, (_gen_param_value == 1), _cash_company_a.lost_promotions, false
                                                          , OperationId, PrintMode.Print
                                                          , SqlTrx, out _voucher_promo_list))
              {
                return false;
              }
            }
          }
        }

        if (_cash_redeem_ab.is_deposit && _cash_redeem_ab.card_deposit > 0) // RCI 24-SEP-2010
        {
          // AJQ 12-AUG-2013, Only add the "Card-Deposit-Out" when the amount is greater than 0.
          AccountMovements.Add(OperationId, _card_data.AccountId, MovementType.DepositOut, x, _cash_redeem_ab.card_deposit, 0, x);

          // DCS 07-JUL-2015: Add card player refundable to Company B
          CashierMovements.Add(OperationId, Misc.IsCardPlayerToCompanyB() ? CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT : CASHIER_MOVEMENT.CARD_DEPOSIT_OUT, _cash_redeem_ab.card_deposit, _card_data.AccountId, _card_data.TrackData);
        }

        // DHA 08-OCT-2014: advice if a recharge is going to refund without plays
        if (_cash_company_b.not_played_recharge > 0 && !_is_chips_purchase_with_cash_out)
        {
          if (!CashierMovements.Add(OperationId, CASHIER_MOVEMENT.NOT_PLAYED_RECHARGE_REFUNDED, _cash_company_b.not_played_recharge, AccountId, _card_data.TrackData))
          {
            return false;
          }
        }

        // Insert voucher
        _balance_before_cash_out = _is_chips_purchase_with_cash_out ? (Currency)_ini_balance.Redeemable : _card_data.CurrentBalance;

          VoucherList = VoucherBuilder.CashOut(_card_data.VoucherAccountInfo(), _card_data.AccountId, _balance_before_cash_out,
                                               _splits, _cash_company_a, _cash_company_b,
                                               OperationId, PrintMode.Print, ValidationNumbers, PayOrder, OpCode, SqlTrx, _is_chips_purchase_with_cash_out, IsApplyTaxes, OperationType == CreditRedeemSourceOperationType.DEFAULT);

          if (_voucher_promo_list != null)
          {
          foreach (Voucher _voucher in _voucher_promo_list)
          {
            VoucherList.Add(_voucher);
          }
        }

        // 18-JUN-2017 FGB    Bug 27741: Error witholding document - "Folio" field does not show the required value        
        // We move here this code (that was near the top of the method), because to generate the document's folio with correct information.
        // To do that we need to generate the Voucher before the folio's value.
        if (WitholdingNeeded)
        {
          // Generate Witholding documents
          //    - Create the major prize
          //    - Generate Witholding Documents and link them to the major prize 

          //    - Create the major prize
          if (!DB_SaveMajorPrize(OperationId, Withhold, _cash_redeem_ab, SqlTrx))
          {
            Log.Error("DB_CardCreditRedeem.DB_SaveMajorPrize OpId=" + OperationId.ToString()
                                                        + "CardId=" + _card_data.AccountId.ToString());

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (!RedeemSuccess)
        {
          // TODO: Error Gen�rico
        }
      }

      return false;
    } // DB_CardCreditRedeem

  } // Accounts

} // WSI.Common
