//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerTrackingThreads.cs
//  
//   DESCRIPTION: Contains threads from player tracking 
// 
//        AUTHOR: Dani Dom�nguez
// 
// CREATION DATE: 01-SEP-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- ----------------------------------------------------------
// 01-AUG-2013 DDM        First release.
// 02-OCT-2014 JML & DDM  Fixed Bug  WIG-1385
// 11-JUL-2016 JML        Product Backlog Item 15065: Gaming tables (Fase 4): Player tracking. Multicurrency
// 03-OCT-2017 RAB        PBI 29842: MES13 Specific alarms: Gaming table player sitting time
// 23-NOV-2017 RAB        Bug 30887:WIGOS-6816 Gaming tables alarm: the "PlayerSittingTimeMinutes" alarm is not triggered when the player is playing on the table over to time of GP
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Xml;
using System.Threading;

namespace WSI.Common
{
  public static class PlayerTracking_CalculatePlays
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes PlayerTracking_CalculatePlaysThread thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(PlayerTracking_CalculatePlaysThread);
      _thread.Name = "PlayerTracking_CalculatePlaysThread";
      _thread.Start();
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : PlayerTracking_CalculatePlaysThread thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void PlayerTracking_CalculatePlaysThread()
    {
      StringBuilder _sb;
      DataTable _tables;
      DataTable _play_sessions;
      Int64 _play_time;
      //Object _object;
      Int32 _num_play_sessions;
      Int32 _gaming_table_id;
      Int32 _wait_hint;
      Int64 _ellapsed;
      Boolean _is_gt_sesion_open;
      CashierSessionInfo _cs_info;
      Seat _seat;
      Int64 _seat_id;
      Boolean _is_principal;
      DataRow[] _dr_play_sessions;
      DateTime _last_tick_date;
      TimeSpan _lost_ticks;
      Int64 _actual_tick;
      Int32 _plays;

      _is_principal = false;

      _sb = new StringBuilder();
      _wait_hint = 0;

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);

          if (!GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
          {
            _wait_hint = 60000; //15 seg
            continue;
          }

          if (!Services.IsPrincipal("WCP"))
          {
            _wait_hint = 15000; //15 seg
            // If this is the first time as principal, after being secondary(first time: _is_principal=false), update all tables with actual machine-tick
            _is_principal = false;

            continue;
          }
          _wait_hint = 2000;

          _tables = new DataTable();
          _play_sessions = new DataTable();

          // Select tables in play
          _sb.Length = 0;
          _sb.AppendLine("SELECT   GTPS_PLAY_SESSION_ID");
          _sb.AppendLine("       , GT_GAMING_TABLE_ID");
          _sb.AppendLine("       , GT_LAST_TICK_PLAYS_COUNT");
          _sb.AppendLine("       , GT_PLAYS_COUNT");
          _sb.AppendLine("       , GT_TABLE_SPEED_SLOW ");
          _sb.AppendLine("       , GT_TABLE_SPEED_NORMAL");
          _sb.AppendLine("       , GT_TABLE_SPEED_FAST ");
          _sb.AppendLine("       , GT_TABLE_SPEED_SELECTED");
          _sb.AppendLine("       , GTB_MIN_BET AS GT_BET_MIN");
          _sb.AppendLine("       , GTB_MAX_BET AS GT_BET_MAX");
          _sb.AppendLine("       , GT_PLAYER_TRACKING_PAUSE");
          _sb.AppendLine("       , GTPS_CURRENT_BET");
          _sb.AppendLine("       , GTPS_IN_SESSION_PLAYS_TABLE");
          _sb.AppendLine("       , GTPS_IN_SESSION_LAST_PLAYED_AMOUNT");
          _sb.AppendLine("       , GTPS_IN_SESSION_LAST_PLAY");
          _sb.AppendLine("       , GTPS_GAMING_TABLE_SESSION_ID");
          _sb.AppendLine("       , GTPS_SEAT_ID");
          _sb.AppendLine("       , GTPS_STATUS");
          _sb.AppendLine("       , GTPS_BET_MIN");
          _sb.AppendLine("       , GTPS_BET_MAX");
          _sb.AppendLine("       , GT_LAST_TICK_DATE");
          _sb.AppendLine("       , GTPS_ISO_CODE");
          _sb.AppendLine("  FROM   GT_PLAY_SESSIONS");
          _sb.AppendLine(" INNER   JOIN GAMING_TABLES     ON GTPS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID");
          _sb.AppendLine(" INNER   JOIN GAMING_TABLE_BET  ON GTB_GAMING_TABLE_ID  = GTPS_GAMING_TABLE_ID AND GTB_ISO_CODE = GTPS_ISO_CODE");
          _sb.AppendLine(" WHERE   GTPS_STATUS        IN (@pStatusOpened, @pStatusAway)");
          _sb.AppendLine("   AND   GT_ENABLED         = @pTableEnable");
          _sb.AppendLine(" ORDER   BY GT_GAMING_TABLE_ID, GTPS_PLAY_SESSION_ID");

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;
              _sql_cmd.Parameters.Add("@pStatusAway", SqlDbType.Int).Value = GTPlaySessionStatus.Away;
              _sql_cmd.Parameters.Add("@pTableEnable", SqlDbType.Bit).Value = 1;

              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
              {
                _sql_da.Fill(_play_sessions);
              }
            } // SqlCommand
          }//DB_TRX

          _tables = _play_sessions.DefaultView.ToTable(true, "GT_GAMING_TABLE_ID"
                                                           , "GT_PLAYS_COUNT"
                                                           , "GT_LAST_TICK_PLAYS_COUNT"
                                                           , "GT_TABLE_SPEED_SLOW"
                                                           , "GT_TABLE_SPEED_NORMAL"
                                                           , "GT_TABLE_SPEED_FAST"
                                                           , "GT_TABLE_SPEED_SELECTED"
                                                           , "GT_LAST_TICK_DATE");

          foreach (DataRow _table in _tables.Rows)
          {
            _gaming_table_id = (Int32)_table["GT_GAMING_TABLE_ID"];
            _ellapsed = 0;
            _num_play_sessions = 0;

            // -- Future use
            //_object = _play_sessions.Compute("COUNT(GTPS_PLAY_SESSION_ID)", "GT_GAMING_TABLE_ID=" + _gaming_table_id.ToString());
            //_num_play_sessions = _object == DBNull.Value ? 0 : (Int32)_object;

            CalculatePlayTime(_table, _num_play_sessions, out _play_time);

            if (_is_principal)
            {
              if (!_table.IsNull("GT_LAST_TICK_PLAYS_COUNT"))
              {
                _ellapsed = Misc.GetElapsedTicks((Int32)_table["GT_LAST_TICK_PLAYS_COUNT"]);
                if (_ellapsed < _play_time * 1000)
                {
                  _wait_hint = Math.Min(_wait_hint, (Int32)(_play_time * 1000 - _ellapsed));

                  continue;
                }
              }
            }

            using (DB_TRX _db_trx = new DB_TRX())
            {
              // check if the table session is open
              IsGTSessionOpen(_gaming_table_id, _db_trx.SqlTransaction, out _is_gt_sesion_open);

              if (!_is_gt_sesion_open)
              {
                // TODO: Pendiente de revisar, PERFORMANCE. 
                // if table session is closed, should  close the  play sessions!
                _cs_info = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);

                foreach (DataRow _play_session in _play_sessions.Rows)
                {
                  if ((Int32)_play_session["GT_GAMING_TABLE_ID"] != _gaming_table_id)
                  {
                    continue;
                  }

                  _seat_id = (Int64)_play_session["GTPS_SEAT_ID"];
                  if (!GamingTableBusinessLogic.ReadSeat(_gaming_table_id, _seat_id
                                                       , _play_session.IsNull("GTPS_GAMING_TABLE_SESSION_ID") ? 0 : (Int64)_play_session["GTPS_GAMING_TABLE_SESSION_ID"]
                                                       , _db_trx.SqlTransaction, out _seat))
                  {
                    Log.Error("PlayerTracking_CalculatePlaysThread. ReadSeat");
                  }
                  if (GamingTableBusinessLogic.EndGTPlaySession(_seat, _cs_info.UserId, _cs_info.TerminalId
                                                              , _cs_info.TerminalName, _db_trx.SqlTransaction) == GamingTableBusinessLogic.GT_RESPONSE_CODE.ERROR_GENERIC)
                  {
                    Log.Error("PlayerTracking_CalculatePlaysThread. EndGTPlaySession");
                  }
                }

                _db_trx.Commit();

                continue;
              }

              // Update play of the gaming_table,
              _sb.Length = 0;
              _sb.AppendLine("UPDATE   GAMING_TABLES");
              _sb.AppendLine("   SET   GT_LAST_TICK_PLAYS_COUNT  = @pTick");
              _sb.AppendLine("       , GT_PLAYS_COUNT            = CASE WHEN GT_LAST_TICK_PLAYS_COUNT  IS NULL THEN GT_PLAYS_COUNT else ISNULL(GT_PLAYS_COUNT,0) + @pPlays END ");
              _sb.AppendLine("       , GT_LAST_TICK_DATE         = @pLastTickDate ");
              _sb.AppendLine(" WHERE   GT_GAMING_TABLE_ID        = @pGamingTableId");
              _sb.AppendLine("   AND   GT_ENABLED                = @pTableEnable");
              _sb.AppendLine("   AND   GT_PLAYER_TRACKING_PAUSE  = @pTableIsNotPause");

              // JML 14-OCT-2014 Minimize change primary service
              // Plays to add by default
              _plays = 1;
              // Date save the tick
              _last_tick_date = WGDB.Now;
              // Actual tick of machine. If it is the first iteration it can change.
              _actual_tick = Misc.GetTickCount();

              // First iteration
              if (!_is_principal)
              {
                // Table is not paused
                if (!_table.IsNull("GT_LAST_TICK_DATE"))
                {
                  _last_tick_date = (DateTime)_table["GT_LAST_TICK_DATE"];
                }
                // Lost ticks while this thread change to principal
                _lost_ticks = (WGDB.Now - _last_tick_date);
                // Player is not paused
                if (!_table.IsNull("GT_LAST_TICK_PLAYS_COUNT"))
                {
                  // Plays lost during service switch
                  _plays = (Int32)Math.Truncate(_lost_ticks.TotalMilliseconds / (_play_time * 1000));
                  // Remainder of ticks lost during service switch
                  _lost_ticks = new TimeSpan(0, 0, 0, 0, (Int32)(_lost_ticks.TotalMilliseconds - (_plays * (_play_time * 1000))));
                  // Set the actual tick as if it had not lost ticks.
                  _actual_tick -= (Int64)_lost_ticks.TotalMilliseconds;
                }
              }
              // JML 14-OCT-2014 End: minimize change primary service

              using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
              {
                _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = _gaming_table_id;
                _sql_cmd.Parameters.Add("@pTick", SqlDbType.Int).Value = _actual_tick;
                _sql_cmd.Parameters.Add("@pLastTickDate", SqlDbType.DateTime).Value = _last_tick_date;
                _sql_cmd.Parameters.Add("@pTableIsNotPause", SqlDbType.Bit).Value = 0;
                _sql_cmd.Parameters.Add("@pTableEnable", SqlDbType.Bit).Value = 1;
                _sql_cmd.Parameters.Add("@pPlays", SqlDbType.Int).Value = _plays;

                if (_sql_cmd.ExecuteNonQuery() == 0)
                {
                  //Log.Warning(String.Format("No updated gt_plays_count for gaming_tables. GamingTableId: {0}", _gaming_table_id.ToString()));

                  continue;
                }
              }

              _dr_play_sessions = _play_sessions.Select("GT_GAMING_TABLE_ID = " + _gaming_table_id.ToString()
                                                 + " AND GTPS_STATUS = " + ((int)GTPlaySessionStatus.Opened).ToString()
                                                 + " AND GT_PLAYER_TRACKING_PAUSE = 0"
                                                 + " AND GT_LAST_TICK_PLAYS_COUNT IS NOT NULL");
              if (!GTPlaySession_UpdatePlaysCount(_dr_play_sessions, _db_trx.SqlTransaction))
              {
                //_wait_hint = 10000;
                continue;
              }

              _db_trx.Commit();

            }// DB_TRX
          } // foreach tables

          // Change the variable to true, after the first loop.
          _is_principal = true;

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          _wait_hint = 10000;
        }
      } // while(true)

    } // PlayerTracking_CalculatePlaysThread

    //------------------------------------------------------------------------------
    // PURPOSE: Calculated the Play Time with speed selected.
    //
    //  PARAMS:
    //      - INPUT:
    //            - Table: Information table.
    //            - NumberOfPlayers: Number of player in table. --> Future use
    //
    //      - OUTPUT:
    //            - PlayTime:
    //
    // RETURNS: - NONE
    //
    private static void CalculatePlayTime(DataRow Table, Int32 NumberOfPlayers, out Int64 PlayTime)
    {
      GTPlayerTrackingSpeed _speed;

      _speed = (GTPlayerTrackingSpeed)(Int32)(Table.IsNull("GT_TABLE_SPEED_SELECTED") ? GTPlayerTrackingSpeed.Unknown : Table["GT_TABLE_SPEED_SELECTED"]);

      switch (_speed)
      {
        case GTPlayerTrackingSpeed.Slow:
          PlayTime = Table.IsNull("GT_TABLE_SPEED_SLOW") ? 0 : (Int32)Table["GT_TABLE_SPEED_SLOW"];
          break;
        case GTPlayerTrackingSpeed.Medium:
          PlayTime = Table.IsNull("GT_TABLE_SPEED_NORMAL") ? 0 : (Int32)Table["GT_TABLE_SPEED_NORMAL"];
          break;
        case GTPlayerTrackingSpeed.Fast:
          PlayTime = Table.IsNull("GT_TABLE_SPEED_FAST") ? 0 : (Int32)Table["GT_TABLE_SPEED_FAST"];
          break;
        case GTPlayerTrackingSpeed.Unknown:
        default:
          //Log.Warning(String.Format("The speed of the gambling is not defined.GamingTableId= {0}", _gaming_table_id));
          PlayTime = GamingTableBusinessLogic.GetDefaultTimeOfAHand();

          break;
      }

      // play time can not be 0!
      PlayTime = (PlayTime == 0) ? GamingTableBusinessLogic.GetDefaultTimeOfAHand() : PlayTime;

    }//CalculatePlayTime

    //------------------------------------------------------------------------------
    // PURPOSE: Update plays_count, max, min and current to GT_Play_session table
    //
    //  PARAMS:
    //      - INPUT:
    //            - PlaySessions: 
    //            - Trx: Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - True:  ok
    //      - False: Otherwhise.
    private static Boolean GTPlaySession_UpdatePlaysCount(DataRow[] PlaySessions, SqlTransaction Trx)
    {
      StringBuilder _sb;

      Int32 _gt_plays_count;
      Int32 _ps_plays_count;
      Int32 _ps_last_plays_count;
      Int64 _ps_play_sesion_id;
      Decimal _ps_played_amount;
      Decimal _ps_bet_min;
      Decimal _ps_bet_max;
      Decimal _gt_bet_min;
      Decimal _gt_bet_max;
      String _ps_iso_code;
      String _national_iso_code;

      try
      {
        _sb = new StringBuilder();

        _national_iso_code = CurrencyExchange.GetNationalCurrency();

        foreach (DataRow _play_session in PlaySessions)
        {
          _ps_played_amount = 0;
          _ps_play_sesion_id = (Int64)_play_session["GTPS_PLAY_SESSION_ID"];

          _gt_plays_count = _play_session.IsNull("GT_PLAYS_COUNT") ? 0 : (Int32)_play_session["GT_PLAYS_COUNT"];
          _ps_plays_count = _play_session.IsNull("GTPS_IN_SESSION_PLAYS_TABLE") ? 1 : _gt_plays_count - (Int32)_play_session["GTPS_IN_SESSION_PLAYS_TABLE"] + 1;// + 1--> next play

          _ps_last_plays_count = _play_session.IsNull("GTPS_IN_SESSION_LAST_PLAY") ? 0 : (Int32)_play_session["GTPS_IN_SESSION_LAST_PLAY"];

          if (!_play_session.IsNull("GTPS_IN_SESSION_LAST_PLAYED_AMOUNT"))
          {
            _ps_played_amount = (Decimal)_play_session["GTPS_IN_SESSION_LAST_PLAYED_AMOUNT"];
          }
          _ps_played_amount += (Decimal)_play_session["GTPS_CURRENT_BET"] * (_ps_plays_count - _ps_last_plays_count);

          //calculate bet min and max
          _ps_bet_min = _play_session.IsNull("GTPS_BET_MIN") ? 0 : (Decimal)_play_session["GTPS_BET_MIN"];
          _ps_bet_max = _play_session.IsNull("GTPS_BET_MAX") ? 0 : (Decimal)_play_session["GTPS_BET_MAX"];
          _ps_iso_code = _play_session.IsNull("GTPS_ISO_CODE") ? _national_iso_code : (String)_play_session["GTPS_ISO_CODE"];
          _gt_bet_min = _play_session.IsNull("GT_BET_MIN") ? (Decimal)_play_session["GTPS_CURRENT_BET"] : (Decimal)_play_session["GT_BET_MIN"];
          _gt_bet_max = _play_session.IsNull("GT_BET_MAX") ? (Decimal)_play_session["GTPS_CURRENT_BET"] : (Decimal)_play_session["GT_BET_MAX"];

          ValidateGTPlaySessionMinMaxBet(_gt_bet_min, _gt_bet_max, (Decimal)_play_session["GTPS_CURRENT_BET"], ref _ps_bet_min, ref _ps_bet_max);

          _sb.Length = 0;
          _sb.AppendLine(" UPDATE   GT_PLAY_SESSIONS ");
          _sb.AppendLine("    set   GTPS_PLAYS           = @pPlaySessionPlaysCount");
          _sb.AppendLine("        , GTPS_IN_SESSION_PLAYS_TABLE = CASE WHEN  (GTPS_IN_SESSION_PLAYS_TABLE  IS NULL ) ");
          _sb.AppendLine("                                            THEN  @pTablePlaysCount ELSE  GTPS_IN_SESSION_PLAYS_TABLE END");
          _sb.AppendLine("        , GTPS_PLAYED_AMOUNT  =  @pPlayedAmount");
          _sb.AppendLine("        , GTPS_PLAYED_AVERAGE =  CASE WHEN @pPlaySessionPlaysCount = 0 THEN   0 else ISNULL(@pPlayedAmount / @pPlaySessionPlaysCount,0) END ");
          _sb.AppendLine("        , GTPS_BET_MAX     = @pPlayedMax");
          _sb.AppendLine("        , GTPS_BET_MIN     = @pPlayedMin");
          _sb.AppendLine("        , GTPS_ISO_CODE    = @pIsoCode");
          _sb.AppendLine("  WHERE   GTPS_PLAY_SESSION_ID = @pPlaySessionId");
          _sb.AppendLine("    AND   GTPS_STATUS          = @pStatusOpened");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = _ps_play_sesion_id;
            _sql_cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;
            _sql_cmd.Parameters.Add("@pTablePlaysCount", SqlDbType.Int).Value = _gt_plays_count;
            _sql_cmd.Parameters.Add("@pPlaySessionPlaysCount", SqlDbType.Int).Value = _ps_plays_count;
            _sql_cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Decimal).Value = _ps_played_amount;
            _sql_cmd.Parameters.Add("@pPlayedMax", SqlDbType.Decimal).Value = _ps_bet_max;
            _sql_cmd.Parameters.Add("@pPlayedMin", SqlDbType.Decimal).Value = _ps_bet_min;
            _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = _ps_iso_code;

            if (_sql_cmd.ExecuteNonQuery() == 0)
            {
              //Log.Warning(String.Format("No updated gt_plays_count for gaming_tables. GamingTableId: {0}", _gaming_table_id.ToString()));

              continue;
            }
          }
        }//foreach play_sessions
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // GTPlaySession_UpdatePlaysCount

    //------------------------------------------------------------------------------
    // PURPOSE: Validated if gaming table session is open
    //
    //  PARAMS:
    //      - INPUT:
    //            - GamingTableId: 
    //            - Trx: Transaction
    //
    //      - OUTPUT:
    //            - IsOpened:
    //
    // RETURNS: 
    //      - True:  ok
    //      - False: Otherwhise.
    private static Boolean IsGTSessionOpen(Int64 GamingTableId, SqlTransaction Trx, out Boolean IsOpened)
    {
      IsOpened = false;
      Object _obj;
      Int32 _open_sesions;

      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   COUNT(1)");
        _sb.AppendLine("  FROM   GAMING_TABLES_SESSIONS ");
        _sb.AppendLine(" INNER   JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID");
        _sb.AppendLine(" WHERE   GTS_GAMING_TABLE_ID = @pGamingTableID");
        _sb.AppendLine("   AND   CS_STATUS = @pStatusOpened ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pGamingTableID", SqlDbType.BigInt).Value = GamingTableId;
          _sql_cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _open_sesions = (Int32)_obj;
            if (_open_sesions >= 1)
            {
              IsOpened = true;
            }
            if (_open_sesions > 1)
            {
              Log.Warning("IsGTSessionOpen: More that one open cashier session found.");
            }
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // IsGTSessionOpen

    //------------------------------------------------------------------------------
    // PURPOSE: Validated min and max 
    //
    //  PARAMS:
    //      - INPUT:
    //            - TableMin 
    //            - TableMax
    //            - CurrentBet 
    //
    //      - OUTPUT:
    //            - ref Decimal PlaySessionPlayedMin
    //            - ref Decimal PlaySessionPlayedMax
    //
    // RETURNS: 
    //      - True:  ok
    //      - False: Otherwhise.
    private static void ValidateGTPlaySessionMinMaxBet(Decimal TableMin, Decimal TableMax, Decimal CurrentBet, ref Decimal PlaySessionPlayedMin, ref Decimal PlaySessionPlayedMax)
    {
      if (CurrentBet > 0)
      {
        if (CurrentBet <= TableMin)
        {
          PlaySessionPlayedMin = TableMin;
        }
        else
        {
          PlaySessionPlayedMin = Math.Min(PlaySessionPlayedMin, CurrentBet);

          if (PlaySessionPlayedMin == 0)
          {
            PlaySessionPlayedMin = CurrentBet;
          }
        }

        if (CurrentBet > TableMax)
        {
          if (TableMax == 0)
          {
            PlaySessionPlayedMax = CurrentBet;
          }
          else
          {
            PlaySessionPlayedMax = TableMax;
          }
        }
        else
        {
          PlaySessionPlayedMax = Math.Max(PlaySessionPlayedMax, CurrentBet);
        }
      }
    } // ValidateGTPlaySessionMinMaxBet


  }//PlayerTracking_CalculatePlaysThread

  public static class PlayerTracking_CalculatePlayerSittingTime
  {
    private static String m_service_name;

    /// <summary>
    /// Initializes PlayerTracking_CalculatePlayerSittingTimeThread thread
    /// </summary>
    public static void Init(String ServiceName)
    {
      Thread _thread;

      _thread = new Thread(PlayerTracking_CalculatePlayerSittingTimeThread);
      _thread.Name = "PlayerTracking_CalculatePlayerSittingTimeThread";
      m_service_name = ServiceName;
      _thread.Start();
    } // Init

    /// <summary>
    /// PlayerTracking_CalculatePlayerSittingTimeThread thread
    /// </summary>
    private static void PlayerTracking_CalculatePlayerSittingTimeThread()
    {
      StringBuilder _sb;
      Int32 _wait_hint;
      DataTable _play_sessions;
      Int32 _sitting_time_parameter;

      _sitting_time_parameter = GamingTableAlarms.GetPlayerSittingTime();
      _sb = new StringBuilder();
      _wait_hint = 0;

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);

          if (!GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
          {
            _wait_hint = 60000; // 60 seconds
            continue;
          }

          if (!Services.IsPrincipal("WCP"))
          {
            _wait_hint = 15000; // 15 seconds
            continue;
          }
          _wait_hint = Int32.Parse(TimeSpan.FromMinutes(1).TotalMilliseconds.ToString()); // 1 minute
          _play_sessions = new DataTable();

          _sb.Length = 0;
          _sb.AppendLine("SELECT   GTPS_ACCOUNT_ID                                                                                                           ");
          _sb.AppendLine("  FROM   GT_PLAY_SESSIONS                                                                                                          ");
          _sb.AppendLine(" INNER   JOIN GAMING_TABLES  ON GTPS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID                                                          ");
          _sb.AppendLine(" WHERE   GTPS_STATUS         IN (@pStatusOpened, @pStatusClosed, @pStatusAway)                                                     ");
          _sb.AppendLine("   AND   GT_ENABLED          = @pTableEnable                                                                                       ");
          _sb.AppendLine("   AND   ISNULL(GTPS_FINISHED, GETDATE()) >= DATEADD(SS, -@pThreadTime, GETDATE())                                                 ");
          _sb.AppendLine("   AND   DATEDIFF(SS, GTPS_STARTED, ISNULL(ISNULL(GTPS_START_WALK, GTPS_FINISHED), GETDATE())) - ISNULL(GTPS_WALK, 0)  > @pSittingTime        ");
          _sb.AppendLine("   AND   DATEDIFF(SS, GTPS_STARTED, ISNULL(ISNULL(GTPS_START_WALK, GTPS_FINISHED), GETDATE())) - ISNULL(GTPS_WALK, 0)  <= (@pSittingTime + @pThreadTime) ");
          _sb.AppendLine(" ORDER   BY GT_GAMING_TABLE_ID, GTPS_PLAY_SESSION_ID                                                                               ");

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;
              _sql_cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = GTPlaySessionStatus.Closed;
              _sql_cmd.Parameters.Add("@pStatusAway", SqlDbType.Int).Value = GTPlaySessionStatus.Away;
              _sql_cmd.Parameters.Add("@pTableEnable", SqlDbType.Bit).Value = 1;
              _sql_cmd.Parameters.Add("@pThreadTime", SqlDbType.BigInt).Value = Int32.Parse(TimeSpan.FromMilliseconds(_wait_hint).TotalSeconds.ToString()); // In seconds
              _sql_cmd.Parameters.Add("@pSittingTime", SqlDbType.Int).Value = Int32.Parse(TimeSpan.FromMinutes(_sitting_time_parameter).TotalSeconds.ToString()); // In seconds

              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
              {
                _sql_da.Fill(_play_sessions);
              }
            }
          }

          foreach (DataRow _current_play_session in _play_sessions.Rows)
          {
            Alarm.Register(AlarmSourceCode.User
                           , 0
                           , Environment.MachineName + "\\" + m_service_name
                           , AlarmCode.User_GamingTables_PlayerSittingTime
                           , Resource.String("STR_ALARM_GAMBLING_TABLES_PLAYER_SITTING_TIME", _sitting_time_parameter, Int64.Parse(_current_play_session["GTPS_ACCOUNT_ID"].ToString())));
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          _wait_hint = 10000; // 10 seconds
        }
      }
    }
  }

  public class PlayerTracking_InterfaceRefresh
  {
    public delegate void GamingTablesValuesChanged(Int64 ThreadTimestamp, Dictionary<Int32, GamingTable> GamingTables);
    public event GamingTablesValuesChanged GamingTablesValuesChangedEvent;

    private Int32? m_bank_id;
    private Boolean m_pause_thread;
    private Dictionary<Int32, GamingTable> m_gaming_tables;
    private Int64 m_internal_sequence; // change sequence

    private static AutoResetEvent m_ev_gaming_table_changed = new AutoResetEvent(true);

    public const Int32 GAMING_TABLE_READ_TIMEOUT = 10 * 1000;  // 10 secs
    public const Int32 GAMING_TABLE_READ_TIME = 2 * 1000;  // 2 secs

    public Dictionary<Int32, GamingTable> GamingTablesInterface
    {
      get
      {
        lock (this)
        {
          return m_gaming_tables;
        }
      }
      set
      {
        lock (this)
        {
          m_gaming_tables = value;
        }
      }
    }

    public Int32? BankId
    {
      get
      {
        return m_bank_id;
      }
      set
      {
        if (m_bank_id != value)
        {
          GamingTablesInterface = null;
        }
        m_bank_id = value;
      }
    }

    public Boolean PauseThread
    {
      get
      {
        return m_pause_thread;
      }
      set
      {
        m_pause_thread = value;
      }
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Initializes PlayerTracking_RefreshGamingTableThread thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void Init()
    {
      Thread _thread;

      _thread = new Thread(PlayerTracking_RefreshGamingTableThread);
      _thread.Name = "PlayerTracking_RefreshGamingTableThread";
      _thread.Start();
    } // InitInterface

    //------------------------------------------------------------------------------
    // PURPOSE : PlayerTracking_RefreshGamingTableThread thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void PlayerTracking_RefreshGamingTableThread()
    {
      Dictionary<Int32, GamingTable> _gaming_tables_out;

      while (true)
      {
        try
        {
          m_ev_gaming_table_changed.WaitOne(GAMING_TABLE_READ_TIME, false);

          _gaming_tables_out = null;

          if (!m_bank_id.HasValue || PauseThread)
          {
            continue;
          }

          lock (this)
          {
            if (GamingTableBusinessLogic.ReadTables(m_bank_id.Value, out _gaming_tables_out))
            {
              m_internal_sequence++;
              GamingTablesInterface = _gaming_tables_out;

              if (GamingTablesValuesChangedEvent != null)
              {
                GamingTablesValuesChangedEvent(m_internal_sequence, GamingTablesInterface);
              }
            }
          } // lock this
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    }

    public Int64 ForceRefresh()
    {
      Int64 _next_sequence;
      Int64 _current_sequence;

      lock (this)
      {
        _current_sequence = m_internal_sequence;
        _next_sequence = _current_sequence + 1;
        m_ev_gaming_table_changed.Set();
      }

      return _next_sequence;
    }
  } // PlayerTracking_InterfaceRefresh

}
