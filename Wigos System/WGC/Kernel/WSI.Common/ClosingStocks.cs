﻿//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ClosingStock.cs  
// 
//   DESCRIPTION: Closing Stock class
//
//        AUTHOR: David Hernández
// 
// CREATION DATE: 09-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- ------------------------------------------------------
// 09-JUN-2016 DHA        First release.
// 29-JUN-2016 FOS        Product Backlog Item 13403: Tables: GUI modifications (Phase 1)
// 06-JUL-2016 FOS        Product Backlog Item 15070: Tables: GUI configuration (Phase 4)
// 30-NOV-2016 FOS        PBI 19197: Reopen cashier sessions and gambling tables sessions
// 13-FEB-2017 DHA        Bug 24541: Error on closing stock fill out movements amounts
// 14-JUL-2017 RAB        Bug 28689: WIGOS-3587 Error when we try to open a table
// 28-JUL-2017 DHA        Bug 29039:WIGOS-3469 [Ticket #6619] Información de reportes de mesas con montos duplicados / Information of tables reports with duplicate amounts
// 02-AGU-2017 DHA        Bug 29104:WIGOS-4159 Tables - Fixed Banking: close fixed banking gaming table registers a ClosingStocks error.
// 09-AUG-2017 RAB        Bug 29071:WIGOS-4070 Tables - It is no allowed to close the gaming table with integrated cashier including the bankcard and check operations
// 16-NOV-2017 RAB        Bug 30827:WIGOS-6659 Error on logs closing a cashier with a integrated and linked tablegame for a fixed gaming table, making a lower "fill in" after trying to close the cashier (Autocage)
// 08-MAR-2018 JML        Fixed Bug 31847:WIGOS-8651 Error opening a table with fixed float for 2 cashiers on the same time
// 21-MAR-2018 AGS        Bug 32007: WIGOS-9153 Error when opening gambling tables - Exception error: invalid column name
// 26-JUN-2018 AGS        Bug 33314:WIGOS-13109 Some card bank or check amounts in close session
//------------------------------------------------------------------------------ 

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace WSI.Common
{
  public class ClosingStocks : ICloneable
  {
    #region Enums

    public enum ClosingStockType
    {
      NONE = 0,
      FIXED = 1,
      ROLLING = 2,
    }

    #endregion Enums

    #region Members

    private DataTable m_currencies_stocks_dt;
    private String m_currencies_stocks;
    private ClosingStockType m_type;
    private Int64 m_cashier_id;
    private Boolean m_sleeps_on_table;
    private DataTable m_currencies_stocks_fills;
    private DataTable m_currencies_stocks_credits;
    private CageDenominationsItems.CageDenominationsDictionary m_currencies_stocks_fills_user;
    private CageDenominationsItems.CageDenominationsDictionary m_currencies_stocks_credits_user;
    private Boolean m_is_closing;
    private Int64 m_cashier_id_old;

    #endregion Members

    #region Properties

    public DataTable CurrenciesStocks
    {
      get { return m_currencies_stocks_dt; }
      set { m_currencies_stocks_dt = value; }
    }

    public ClosingStockType Type
    {
      get { return m_type; }
      set { m_type = value; }
    }

    public Int64 CashierId
    {
      get { return m_cashier_id; }
      set { m_cashier_id = value; }
    }

    public Boolean SleepsOnTable
    {
      get { return m_sleeps_on_table; }
      set { m_sleeps_on_table = value; }
    }

    public DataTable CurrenciesStocksFills
    {
      get { return m_currencies_stocks_fills; }
    }

    public DataTable CurrenciesStocksCredits
    {
      get { return m_currencies_stocks_credits; }
    }

    // Fill introduced by the user when the system proposed a fill (Fixed Bank)
    public CageDenominationsItems.CageDenominationsDictionary CurrenciesStocksFillsUser
    {
      get { return m_currencies_stocks_fills_user; }
      set { m_currencies_stocks_fills_user = value; }
    }

    // Credit introduced by the user when the system proposed a credit (Fixed Bank)
    public CageDenominationsItems.CageDenominationsDictionary CurrenciesStocksCreditsUser
    {
      get { return m_currencies_stocks_credits_user; }
      set { m_currencies_stocks_credits_user = value; }
    }

    public Boolean IsClosing
    {
      get { return m_is_closing; }
    }

    public Int64 CashierIdOld
    {
      get { return m_cashier_id_old; }
      set { m_cashier_id_old = value; }
    }
    #endregion Properties

    #region Public Methods

    public ClosingStocks(Int64 CashierId)
    {
      this.LoadClosingStockData(CashierId);
    }

    public ClosingStocks(Int64 CashierId, SqlTransaction Trx)
    {
      LoadClosingStockData(CashierId, Trx);
    }

    /// <summary>
    /// Validate opening stock, if the stock not mach with closing stock the system will register an alarm
    /// </summary>
    /// <param name="CurrenciesStockByUser"></param>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="IsoCodeColumnName"></param>
    /// <param name="DenominationColumnName"></param>
    /// <param name="TypeColumnName"></param>
    /// <param name="ChipIdColumnName"></param>
    /// <param name="QuantityColumnName"></param>
    public void ValidateOpeningStock(CageDenominationsItems.CageDenominationsDictionary CurrenciesStockByUser, CashierSessionInfo CashierSessionInfo, String IsoCodeColumnName, String DenominationColumnName, String TypeColumnName, String ChipIdColumnName, String QuantityColumnName)
    {
      DataTable _currencies_stock_by_user_dt;

      // No validate
      if (this.Type == ClosingStockType.NONE)
      {
        return;
      }

      _currencies_stock_by_user_dt = null;

      // Acumulate all the currencies
      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _item in CurrenciesStockByUser)
      {
        if (_currencies_stock_by_user_dt == null)
        {
          _currencies_stock_by_user_dt = new DataTable();
          _currencies_stock_by_user_dt = _item.Value.ItemAmounts.Clone();
        }

        foreach (DataRow _row in _item.Value.ItemAmounts.Rows)
        {
          _currencies_stock_by_user_dt.Rows.Add(_row.ItemArray);
        }
      }

      ValidateOpeningStock(_currencies_stock_by_user_dt, CashierSessionInfo, IsoCodeColumnName, DenominationColumnName, TypeColumnName, ChipIdColumnName, QuantityColumnName);
    }
    /// <summary>
    /// Validate opening stock, if the stock not mach with closing stock the system will register an alarm
    /// </summary>
    /// <param name="CurrenciesStockByUser"></param>
    /// <param name="UserName"></param>
    /// <param name="IsoCodeColumnName"></param>
    /// <param name="DenominationColumnName"></param>
    /// <param name="TypeColumnName"></param>
    /// <param name="ChipIdColumnName"></param>
    /// <param name="QuantityColumnName"></param>
    public void ValidateOpeningStock(DataTable CurrenciesStockByUser, CashierSessionInfo CashierSessionInfo, String IsoCodeColumnName, String DenominationColumnName, String TypeColumnName, String ChipIdColumnName, String QuantityColumnName)
    {
      DataRow[] _rows;
      String _filter;
      String _alarm_description;
      Int32 _quantity_by_user;
      Int32 _quantity_closing;

      foreach (DataRow _row in CurrenciesStockByUser.Rows)
      {
        _quantity_by_user = 0;
        _quantity_closing = 0;
        _alarm_description = String.Empty;

        if (this.CurrenciesStocks != null)
        {
          _filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND CAGE_CURRENCY_TYPE = {2} ", _row[IsoCodeColumnName], Format.LocalNumberToDBNumber(_row[DenominationColumnName].ToString()), (Int32)_row[TypeColumnName]);

          if (_row[ChipIdColumnName] != DBNull.Value)
          {
            _filter += String.Format(" AND CHIP_ID = {0}", _row[ChipIdColumnName]);
          }

          _rows = this.CurrenciesStocks.Select(_filter);

          if (_rows.Length > 0)
          {
            _quantity_by_user = _row[QuantityColumnName] == DBNull.Value ? 0 : Int32.Parse(_row[QuantityColumnName].ToString());
            _quantity_closing = _rows[0]["QUANTITY"] == DBNull.Value ? 0 : (Int32)_rows[0]["QUANTITY"];

            if (_quantity_by_user != _quantity_closing)
            {
              String _type_resource;

              switch ((CageCurrencyType)_row[TypeColumnName])
              {
                case CageCurrencyType.Bill:
                  _type_resource = String.Format("{0} ({1})", Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL"), _row[IsoCodeColumnName].ToString());
                  break;
                case CageCurrencyType.Coin:
                  _type_resource = String.Format("{0} ({1})", Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN"), _row[IsoCodeColumnName].ToString());
                  break;
                case CageCurrencyType.ChipsRedimible:
                case CageCurrencyType.ChipsNoRedimible:
                case CageCurrencyType.ChipsColor:
                  _type_resource = FeatureChips.GetChipTypeDescription(FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row[TypeColumnName]), _row[IsoCodeColumnName].ToString());
                  break;
                default:
                  _type_resource = String.Empty;
                  break;
              }

              // Register alarm
              if (_row[ChipIdColumnName] != DBNull.Value)
              {
                FeatureChips.Chips.Chip _chip;
                String _chip_description;

                _chip = FeatureChips.Chips.GetChip((Int64)_row[ChipIdColumnName]);
                _chip_description = String.Format("{0}|{1}|{2}", FeatureChips.ChipsSets.GetChipsSetByChip(_chip).Name, _chip.Name, _chip.Drawing);

                if ((CageCurrencyType)_row[TypeColumnName] == CageCurrencyType.ChipsColor)
                {
                  _alarm_description = Resource.String("STR_ALARM_CLOSING_STOCK_MISMATCH_CHIPS_COLOR", CashierSessionInfo.TerminalName, _type_resource, _chip_description, _quantity_by_user, _quantity_closing);

                }
                else
                {
                  _alarm_description = Resource.String("STR_ALARM_CLOSING_STOCK_MISMATCH_CHIPS_RE_NR", CashierSessionInfo.TerminalName, _type_resource, _row[DenominationColumnName], _chip_description, _quantity_by_user, _quantity_closing);
                }
              }
              else
              {
                _alarm_description = Resource.String("STR_ALARM_CLOSING_STOCK_MISMATCH_CASH", CashierSessionInfo.TerminalName, _type_resource, _row[DenominationColumnName], _quantity_by_user, _quantity_closing);
              }

              Alarm.Register(AlarmSourceCode.User, 0, CashierSessionInfo.AuthorizedByUserName + "@" + Environment.MachineName, AlarmCode.User_DenominationDepositUnbalanced, _alarm_description);

            }
          }
          else
          {
            // Currency not exist
            // TODO: mirar que poner aqui
          }
        }
        else
        {
          // It's the first time and there are no closing stock yet
          return;
        }
      }
    }

    /// <summary>
    /// Validate the closing stock with the fixed stock, and return fills and credits
    /// </summary>
    /// <param name="CurrenciesStockByUser"></param>
    /// <param name="IsoCodeColumnName"></param>
    /// <param name="DenominationColumnName"></param>
    /// <param name="TypeColumnName"></param>
    /// <param name="ChipIdColumnName"></param>
    /// <param name="QuantityColumnName"></param>
    /// <param name="CurrenciesToCredit"></param>
    /// <param name="CurrenciesToFill"></param>
    public void ValidateClosingStock(CageDenominationsItems.CageDenominationsDictionary CurrenciesStockByUser, String IsoCodeColumnName, String DenominationColumnName, String TypeColumnName, String ChipIdColumnName, String QuantityColumnName, String TotalColumnName)
    {
      DataTable _currencies_stock_by_user_dt;
      decimal _check_amount = 0;
      decimal _bankcard_amount = 0;

      // No validate
      if (this.Type == ClosingStockType.NONE)
      {
        return;
      }

      _currencies_stock_by_user_dt = null;

      // Acumulate all the currencies
      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _item in CurrenciesStockByUser)
      {
        if (_currencies_stock_by_user_dt == null)
        {
          _currencies_stock_by_user_dt = _item.Value.ItemAmounts.Clone();
        }
        foreach (DataRow _row in _item.Value.ItemAmounts.Rows)
        {
          if (!string.IsNullOrEmpty(_row[TypeColumnName].ToString()))
          {
            switch (Convert.ToInt16(_row[TypeColumnName]))
            {
              case -1:
                if (!string.IsNullOrEmpty(_row["TOTAL"].ToString()) && Decimal.Parse(_row["TOTAL"].ToString()) > 0)
                {
                  if (_bankcard_amount == 0)
                    _currencies_stock_by_user_dt.Rows.Add(_row.ItemArray);

                  _bankcard_amount += Decimal.Parse(_row["TOTAL"].ToString());
                  _currencies_stock_by_user_dt.Rows[_currencies_stock_by_user_dt.Rows.Count - 1]["TOTAL"] = _bankcard_amount;
                }

                break;
              case -2:
                if (!string.IsNullOrEmpty(_row["TOTAL"].ToString()) && Decimal.Parse(_row["TOTAL"].ToString()) > 0)
                {
                  if (_check_amount == 0)
                    _currencies_stock_by_user_dt.Rows.Add(_row.ItemArray);

                  _check_amount += Decimal.Parse(_row["TOTAL"].ToString());
                  _currencies_stock_by_user_dt.Rows[_currencies_stock_by_user_dt.Rows.Count - 1]["TOTAL"] = _check_amount;
                }
                break;
              default:
                _currencies_stock_by_user_dt.Rows.Add(_row.ItemArray);
                break;
            }
          }
        }
      }

      ValidateClosingStock(_currencies_stock_by_user_dt, IsoCodeColumnName, DenominationColumnName, TypeColumnName, ChipIdColumnName, QuantityColumnName, TotalColumnName);

    }

    /// <summary>
    /// Recalculate the closing amount for the current session considering the credit/fill for fixed bank
    /// </summary>
    /// <param name="ClosingAmount"></param>
    /// <param name="RecalculateClosingAmount"></param>
    /// <param name="IsoCodeColumnName"></param>
    /// <param name="DenominationColumnName"></param>
    /// <param name="TypeColumnName"></param>
    /// <param name="ChipIdColumnName"></param>
    /// <param name="QuantityColumnName"></param>
    /// <param name="TotalColumnName"></param>
    /// <returns></returns>
    public Boolean FixedBankRecalculateClosingAmount(CageDenominationsItems.CageDenominationsDictionary ClosingAmount, out CageDenominationsItems.CageDenominationsDictionary RecalculateClosingAmount, String IsoCodeColumnName, String DenominationColumnName, String TypeColumnName, String ChipIdColumnName, String QuantityColumnName, String TotalColumnName)
    {
      String _filter;
      DataRow[] _rows;

      Decimal _quantity_closing;
      Decimal _quantity_credit;
      Decimal _quantity_fill;

      Decimal _total_closing;
      Decimal _total_credit;
      Decimal _total_fill;

      try
      {

        // Closing - Credit
        if (this.m_currencies_stocks_credits_user != null)
        {
          foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _item in this.m_currencies_stocks_credits_user)
          {
            foreach (DataRow _row in _item.Value.ItemAmounts.Rows)
            {
              if (_row[QuantityColumnName] != DBNull.Value && Decimal.Parse(_row[QuantityColumnName].ToString()) > 0 || _row[TotalColumnName] != DBNull.Value && Decimal.Parse(_row[TotalColumnName].ToString()) > 0)
              {
                _filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND CAGE_CURRENCY_TYPE_ENUM = {2} ", _row[IsoCodeColumnName], Format.LocalNumberToDBNumber(_row[DenominationColumnName].ToString()), _row[TypeColumnName]);

                if (_row[ChipIdColumnName] != DBNull.Value)
                {
                  _filter += String.Format("AND CHIP_ID = {0}", _row[ChipIdColumnName]);
                }

                _rows = ClosingAmount[_item.Key].ItemAmounts.Select(_filter);

                if (_rows.Length > 0)
                {
                  _quantity_closing = _rows[0][QuantityColumnName] == DBNull.Value ? 0 : Decimal.Parse(_rows[0][QuantityColumnName].ToString());
                  _quantity_credit = _row[QuantityColumnName] == DBNull.Value ? 0 : Decimal.Parse(_row[QuantityColumnName].ToString());

                  _rows[0][QuantityColumnName] = _quantity_closing - _quantity_credit;

                  _total_closing = Decimal.Parse(_rows[0][TotalColumnName].ToString());
                  _total_credit = Decimal.Parse(_row[TotalColumnName].ToString());

                  _rows[0][TotalColumnName] = _total_closing - _total_credit;
                }
              }
            }
          }
        }

        // Closing + Fill
        if (this.m_currencies_stocks_fills_user != null && Cage.IsCageAutoMode())
        {
          foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _item in this.m_currencies_stocks_fills_user)
          {
            foreach (DataRow _row in _item.Value.ItemAmounts.Rows)
            {
              if (_row[QuantityColumnName] != DBNull.Value && Decimal.Parse(_row[QuantityColumnName].ToString()) > 0 || _row[TotalColumnName] != DBNull.Value && Decimal.Parse(_row[TotalColumnName].ToString()) > 0)
              {
                _filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND CAGE_CURRENCY_TYPE_ENUM = {2} ", _row[IsoCodeColumnName], Format.LocalNumberToDBNumber(_row[DenominationColumnName].ToString()), _row[TypeColumnName]);

                if (_row[ChipIdColumnName] != DBNull.Value)
                {
                  _filter += String.Format("AND CHIP_ID = {0}", _row[ChipIdColumnName]);
                }

                _rows = ClosingAmount[_item.Key].ItemAmounts.Select(_filter);

                if (_rows.Length > 0)
                {
                  _quantity_closing = _rows[0][QuantityColumnName] == DBNull.Value ? 0 : Decimal.Parse(_rows[0][QuantityColumnName].ToString());
                  _quantity_fill = _row[QuantityColumnName] == DBNull.Value ? 0 : Decimal.Parse(_row[QuantityColumnName].ToString());

                  _rows[0][QuantityColumnName] = _quantity_closing - _quantity_fill;

                  if (_rows[0][TotalColumnName].ToString() == String.Empty)
                  {
                    _total_closing = 0;
                  }
                  else
                  {
                    _total_closing = Decimal.Parse(_rows[0][TotalColumnName].ToString());
                  }
                  _total_fill = Decimal.Parse(_row[TotalColumnName].ToString());

                  _rows[0][TotalColumnName] = _total_closing - _total_fill;
                }
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("ClosingStocks - FixedBankRecalculateClosingAmount(): {0}", _ex.Message));

        return false;
      }
      finally
      {
        RecalculateClosingAmount = ClosingAmount;
      }

      return true;
    }

    /// <summary>
    /// Validate the closing stock with the fixed stock, and return fills and credits
    /// </summary>
    /// <param name="CurrenciesStockByUser"></param>
    /// <param name="IsoCodeColumnName"></param>
    /// <param name="DenominationColumnName"></param>
    /// <param name="TypeColumnName"></param>
    /// <param name="ChipIdColumnName"></param>
    /// <param name="QuantityColumnName"></param>
    /// <param name="CurrenciesToCredit"></param>
    /// <param name="CurrenciesToFill"></param>
    public void ValidateClosingStock(DataTable CurrenciesStockByUser, String IsoCodeColumnName, String DenominationColumnName, String TypeColumnName, String ChipIdColumnName, String QuantityColumnName, String TotalColumnName)
    {
      DataRow[] _rows;
      String _filter;
      Int32 _quantity_by_user;
      Decimal _total_by_user;
      Int32 _quantity_expected;
      Decimal _total_expected;

      m_currencies_stocks_fills = null;
      m_currencies_stocks_credits = null;
      _filter = String.Empty;
      m_is_closing = true;

      // No validate
      if (this.Type == ClosingStockType.NONE || this.Type == ClosingStockType.ROLLING)
      {
        return;
      }

      m_currencies_stocks_credits = CreateDatatable();
      m_currencies_stocks_fills = CreateDatatable();

      foreach (DataRow _row in CurrenciesStockByUser.Rows)
      {
        _filter = String.Format("ISO_CODE = '{0}' AND DENOMINATION = {1} AND CAGE_CURRENCY_TYPE = {2} ", _row[IsoCodeColumnName], Format.LocalNumberToDBNumber(_row[DenominationColumnName].ToString()), _row[TypeColumnName]);

        if (_row[ChipIdColumnName] != DBNull.Value)
        {
          _filter += String.Format("AND CHIP_ID = {0}", _row[ChipIdColumnName]);
        }
        _rows = this.CurrenciesStocks.Select(_filter);

        m_currencies_stocks_credits.Rows.Add(_row[IsoCodeColumnName], _row[DenominationColumnName], _row[TypeColumnName], _row[ChipIdColumnName], 0);
        m_currencies_stocks_fills.Rows.Add(_row[IsoCodeColumnName], _row[DenominationColumnName], _row[TypeColumnName], _row[ChipIdColumnName], 0);

        _quantity_by_user = _row[QuantityColumnName] == DBNull.Value ? 0 : Int32.Parse(_row[QuantityColumnName].ToString());
        _total_by_user = _row[TotalColumnName] == DBNull.Value ? 0 : Decimal.Parse(_row[TotalColumnName].ToString());

        if (_rows.Length > 0)
        {
          _quantity_expected = _rows[0]["QUANTITY"] == DBNull.Value ? 0 : (Int32)_rows[0]["QUANTITY"];
          _total_expected = _rows[0]["TOTAL"] == DBNull.Value ? 0 : Decimal.Parse(_rows[0]["TOTAL"].ToString());

          // Stock not match
          // To Credit
          if (_quantity_by_user > _quantity_expected || _total_by_user > _total_expected)
          {
            m_currencies_stocks_credits.Rows[m_currencies_stocks_credits.Rows.Count - 1]["QUANTITY"] = _quantity_by_user - _quantity_expected;
            m_currencies_stocks_fills.Rows[m_currencies_stocks_fills.Rows.Count - 1]["QUANTITY"] = 0;

            m_currencies_stocks_credits.Rows[m_currencies_stocks_credits.Rows.Count - 1]["TOTAL"] = _total_by_user - _total_expected;
            m_currencies_stocks_fills.Rows[m_currencies_stocks_fills.Rows.Count - 1]["TOTAL"] = 0;
          }
          // To Fill
          else if (_quantity_by_user < _quantity_expected || _total_by_user < _total_expected)
          {
            m_currencies_stocks_credits.Rows[m_currencies_stocks_credits.Rows.Count - 1]["QUANTITY"] = 0;
            m_currencies_stocks_fills.Rows[m_currencies_stocks_fills.Rows.Count - 1]["QUANTITY"] = _quantity_expected - _quantity_by_user;

            m_currencies_stocks_credits.Rows[m_currencies_stocks_credits.Rows.Count - 1]["TOTAL"] = 0;
            m_currencies_stocks_fills.Rows[m_currencies_stocks_fills.Rows.Count - 1]["TOTAL"] = _total_expected - _total_by_user;
          }
          // Stock match
          else
          {
            m_currencies_stocks_credits.Rows[m_currencies_stocks_credits.Rows.Count - 1]["QUANTITY"] = 0;
            m_currencies_stocks_fills.Rows[m_currencies_stocks_fills.Rows.Count - 1]["QUANTITY"] = 0;

            m_currencies_stocks_credits.Rows[m_currencies_stocks_credits.Rows.Count - 1]["TOTAL"] = 0;
            m_currencies_stocks_fills.Rows[m_currencies_stocks_fills.Rows.Count - 1]["TOTAL"] = 0;
          }
        }
        // Row not found in fixed stock
        // User has introduced a quantity for anything that no was defined on fixed stock. In this case will add to credit
        else
        {
          m_currencies_stocks_credits.Rows[m_currencies_stocks_credits.Rows.Count - 1]["QUANTITY"] = _quantity_by_user;
          m_currencies_stocks_fills.Rows[m_currencies_stocks_fills.Rows.Count - 1]["QUANTITY"] = 0;

          m_currencies_stocks_credits.Rows[m_currencies_stocks_credits.Rows.Count - 1]["TOTAL"] = _total_by_user;
          m_currencies_stocks_fills.Rows[m_currencies_stocks_fills.Rows.Count - 1]["TOTAL"] = 0;
        }
      }
    }

    /// <summary>
    /// Save closing stock
    /// </summary>
    /// <param name="ClosingStock"></param>
    /// <param name="IsoCodeColumnName"></param>
    /// <param name="DenominationColumnName"></param>
    /// <param name="TypeColumnName"></param>
    /// <param name="ChipIdColumnName"></param>
    /// <param name="QuantityColumnName"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean Save(CageDenominationsItems.CageDenominationsDictionary ClosingStock, String IsoCodeColumnName, String DenominationColumnName, String TypeColumnName, String ChipIdColumnName, String QuantityColumnName, String TotalColumnName, SqlTransaction Trx)
    {
      DataTable _currencies_stock_closing_dt;

      _currencies_stock_closing_dt = null;

      // Acumulate all the currencies
      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _item in ClosingStock)
      {
        if (_currencies_stock_closing_dt == null)
        {
          _currencies_stock_closing_dt = _item.Value.ItemAmounts.Clone();
        }

        foreach (DataRow _row in _item.Value.ItemAmounts.Rows)
        {
          _currencies_stock_closing_dt.Rows.Add(_row.ItemArray);
        }
      }

      return Save(_currencies_stock_closing_dt, IsoCodeColumnName, DenominationColumnName, TypeColumnName, ChipIdColumnName, QuantityColumnName, TotalColumnName, Trx);
    }

    /// <summary>
    /// Save closing stock
    /// </summary>
    /// <param name="ClosingStock"></param>
    /// <param name="IsoCodeColumnName"></param>
    /// <param name="DenominationColumnName"></param>
    /// <param name="TypeColumnName"></param>
    /// <param name="ChipIdColumnName"></param>
    /// <param name="QuantityColumnName"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean Save(DataTable ClosingStock, String IsoCodeColumnName, String DenominationColumnName, String TypeColumnName, String ChipIdColumnName, String QuantityColumnName, String TotalColumnName, SqlTransaction Trx)
    {
      if (this.Type != ClosingStockType.FIXED)
      {
        if (this.Type == ClosingStockType.NONE)
        {
          this.m_currencies_stocks_dt = null;
        }
        else if (this.Type == ClosingStockType.ROLLING)
        {
          // Delete row tickets TITO
          foreach (DataRow _row_ticket in ClosingStock.Select("DENOMINATION = " + Cage.TICKETS_CODE))
          {
            _row_ticket.Delete();
          }

          ClosingStock.AcceptChanges();

          MapDatatable(ClosingStock, IsoCodeColumnName, DenominationColumnName, TypeColumnName, ChipIdColumnName, QuantityColumnName, TotalColumnName);
        }

        if (!UpdateClosingStock(Trx))
        {
          Log.Error("Error ClosingStocks.Save()");

          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// Save closing stock
    /// </summary>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean Save(SqlTransaction Trx, bool saveStock = true)
    {
      if (!UpdateClosingStock(Trx, saveStock))
      {
        Log.Error("Error ClosingStocks.Save()");

        return false;
      }

      return true;
    }

    /// <summary>
    /// Map and fill currency stock (variable)
    /// </summary>
    /// <param name="CurrenciesTable"></param>
    /// <param name="IsoCodeColumnName"></param>
    /// <param name="DenominationColumnName"></param>
    /// <param name="TypeColumnName"></param>
    /// <param name="ChipIdColumnName"></param>
    /// <param name="QuantityColumnName"></param>
    public void MapDatatable(DataTable CurrenciesTable, String IsoCodeColumnName, String DenominationColumnName, String TypeColumnName, String ChipIdColumnName, String QuantityColumnName, String TotalColumnName)
    {
      m_currencies_stocks_dt = CreateDatatable();

      foreach (DataRow _row in CurrenciesTable.Rows)
      {
        m_currencies_stocks_dt.Rows.Add(_row[IsoCodeColumnName], _row[DenominationColumnName], _row[TypeColumnName], _row[ChipIdColumnName], _row[QuantityColumnName], _row[TotalColumnName]);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
      ClosingStocks _closing_stocks;

      _closing_stocks = (ClosingStocks)this.MemberwiseClone();

      _closing_stocks.m_currencies_stocks_dt = this.m_currencies_stocks_dt.Copy();
      _closing_stocks.m_currencies_stocks = this.m_currencies_stocks;
      _closing_stocks.m_type = this.m_type;
      _closing_stocks.m_cashier_id = this.m_cashier_id;
      _closing_stocks.m_sleeps_on_table = this.m_sleeps_on_table;
      _closing_stocks.m_currencies_stocks_fills = this.m_currencies_stocks_fills;
      _closing_stocks.m_currencies_stocks_credits = this.m_currencies_stocks_credits;
      _closing_stocks.m_is_closing = this.m_is_closing;

      return _closing_stocks;
    }

    /// <summary>
    /// Define closing stock table structure
    /// </summary>
    public static DataTable CreateDatatable()
    {
      DataTable _currencies_stock;

      _currencies_stock = new DataTable("CLOSING_STOCKS");
      _currencies_stock.Columns.Add("ISO_CODE", typeof(System.String));
      _currencies_stock.Columns.Add("DENOMINATION", typeof(System.Decimal));
      _currencies_stock.Columns.Add("CAGE_CURRENCY_TYPE", typeof(CageCurrencyType));
      _currencies_stock.Columns.Add("CHIP_ID", typeof(System.Int64));
      _currencies_stock.Columns.Add("QUANTITY", typeof(System.Int32));
      _currencies_stock.Columns.Add("TOTAL", typeof(System.Decimal));

      return _currencies_stock;
    }

    public Boolean AddFillerOutMovement(CashierSessionInfo CashierSessionInfo, SortedDictionary<CurrencyIsoType, Decimal> ClosingAmount, GamingTablesSessions _gt_session, Int64 OperationId, DB_TRX _db_trx)
    {

      if (this != null && this.SleepsOnTable)
      {
        CashierMovementsTable _new_cashier_movements;

        _new_cashier_movements = new CashierMovementsTable(CashierSessionInfo);

        foreach (KeyValuePair<CurrencyIsoType, Decimal> _closing_amount in ClosingAmount)
        {
          if (_closing_amount.Value != 0)
          {
            if (_closing_amount.Key.IsoCode == CurrencyExchange.GetNationalCurrency() && _closing_amount.Key.Type == CurrencyExchangeType.CURRENCY)
            {
              _new_cashier_movements.Add(OperationId, CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK, _closing_amount.Value, 0, String.Empty, String.Empty, String.Empty, 0, 0, _gt_session.GamingTableSessionId, -1, _closing_amount.Key.Type, null, null);
            }
            else
            {
              _new_cashier_movements.Add(OperationId, CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK, _closing_amount.Value, 0, String.Empty, String.Empty, _closing_amount.Key.IsoCode, 0, 0, _gt_session.GamingTableSessionId, -1, _closing_amount.Key.Type, FeatureChips.ConvertToCageCurrencyType(_closing_amount.Key.Type), null);
            }
          }
        }

        if (!_new_cashier_movements.Save(_db_trx.SqlTransaction))
        {
          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// Set closing amount on close fixed type
    /// </summary>
    /// <param name="ClosingAmount"></param>
    /// <param name="ClosingStocks"></param>
    /// <returns></returns>
    public Boolean SetClosingAmountOnCloseFixedType(SortedDictionary<CurrencyIsoType, Decimal> ClosingAmount, ClosingStocks ClosingStocks)
    {

      try
      {
        if (ClosingStocks.Type == ClosingStockType.FIXED && ClosingStocks.SleepsOnTable)
        {

          ClosingAmount.Clear();

          ClosingAmount.Add(new CurrencyIsoType(CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CURRENCY), 0);

          foreach (DataRow _row in ClosingStocks.CurrenciesStocks.Rows)
          {
            if (_row["TOTAL"] != System.DBNull.Value && (Decimal)_row["TOTAL"] > 0)
            {
              if (FeatureChips.IsChipsType((CageCurrencyType)_row["CAGE_CURRENCY_TYPE"]))
              {
                if (FeatureChips.ChipsSets.GetChipsSetByChip((Int64)_row["CHIP_ID"]) == null || FeatureChips.ChipsSets.GetChipsSetByChip((Int64)_row["CHIP_ID"]) != null && !FeatureChips.ChipsSets.GetChipsSetByChip((Int64)_row["CHIP_ID"]).Allowed)
                {
                  continue;
                }
              }

              if (!ClosingAmount.ContainsKey(new CurrencyIsoType(_row["ISO_CODE"].ToString(), FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CAGE_CURRENCY_TYPE"]))))
              {
                ClosingAmount.Add(new CurrencyIsoType(_row["ISO_CODE"].ToString(), FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CAGE_CURRENCY_TYPE"])), 0);
              }

              ClosingAmount[new CurrencyIsoType(_row["ISO_CODE"].ToString(), FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CAGE_CURRENCY_TYPE"]))] += (Decimal)_row["TOTAL"];
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    public Boolean Reset(SqlTransaction Trx, bool saveStock = true)
    {
      if (!ResetClosingStock(Trx, saveStock))
      {
        Log.Error("Error ClosingStocks.Reset()");

        return false;
      }

      return true;
    }

    #endregion Public Methods

    #region Private Methods

    /// <summary>
    /// Load closing stock data
    /// </summary>
    /// <param name="CashierId"></param>
    /// <returns></returns>
    private Boolean LoadClosingStockData(Int64 CashierId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return LoadClosingStockData(CashierId, _db_trx.SqlTransaction);
      }
    }

    /// <summary>
    /// Load closing stock data
    /// </summary>
    /// <param name="CashierId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean LoadClosingStockData(Int64 CashierId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      this.m_cashier_id = CashierId;
      this.Type = ClosingStockType.NONE;
      this.SleepsOnTable = false;
      this.m_currencies_stocks = String.Empty;

      try
      {
        _sb.AppendLine(" SELECT   CS_CASHIER_ID ");
        _sb.AppendLine("        , CS_CLOSING_STOCK_TYPE ");
        _sb.AppendLine("        , CS_SLEEPS_ON_TABLE ");
        _sb.AppendLine("        , CS_STOCK ");
        _sb.AppendLine("   FROM   CLOSING_STOCKS ");
        _sb.AppendLine("  WHERE   CS_CASHIER_ID = @pCashierId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCashierId", SqlDbType.Decimal).Value = CashierId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              this.m_cashier_id = _reader.GetInt64(0);
              this.Type = (ClosingStockType)_reader.GetInt32(1);
              this.SleepsOnTable = _reader.GetBoolean(2);
              this.m_currencies_stocks = _reader.IsDBNull(3) ? String.Empty : _reader.GetString(3);
            }
          }
        }

        // Load datatable from xml
        this.m_currencies_stocks_dt = LoadDatatableFromXML(this.m_currencies_stocks);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    /// <summary>
    /// Load closing stock table from XML
    /// </summary>
    /// <param name="XML"></param>
    /// <returns></returns>
    private DataTable LoadDatatableFromXML(String XML)
    {
      DataSet _ds;
      DataTable _dt;
      StringReader _sr;

      if (String.IsNullOrEmpty(XML))
      {
        return CreateDatatable();
      }

      _ds = new DataSet();
      _sr = new StringReader(XML);

      _dt = CreateDatatable();
      _ds.Tables.Add(_dt);
      _ds.ReadXml(_sr, XmlReadMode.IgnoreSchema);

      return _ds.Tables[0];
    }

    /// <summary>
    /// Creates XML from closing stock table
    /// </summary>
    /// <param name="CurrenciesStocks"></param>
    /// <returns></returns>
    private String DatatableToXML(DataTable CurrenciesStocks)
    {
      DataSet _ds;
      String _xml;

      _ds = new DataSet();
      _xml = String.Empty;

      if (CurrenciesStocks != null && CurrenciesStocks.Rows.Count > 0)
      {
        _ds.Tables.Add(CurrenciesStocks);

        using (StringWriter _sw = new StringWriter())
        {
          _ds.WriteXml(_sw, XmlWriteMode.WriteSchema);
          _xml = _sw.ToString();
        }
      }

      _ds.Tables.Clear();
      return _xml;
    }

    /// <summary>
    /// Save closing stock data
    /// </summary>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean UpdateClosingStock(SqlTransaction Trx, bool saveStock = true)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" IF EXISTS ( SELECT   1 ");
        _sb.AppendLine("                   FROM   CLOSING_STOCKS");
        _sb.AppendLine("                  WHERE   CS_CASHIER_ID = @pCashierId ) ");

        _sb.AppendLine(" UPDATE   CLOSING_STOCKS ");
        _sb.AppendLine("    SET   CS_CLOSING_STOCK_TYPE = @pType ");
        _sb.AppendLine("        , CS_SLEEPS_ON_TABLE = @pSleepsOnTable ");
        if (saveStock)
        {
          _sb.AppendLine("        , CS_STOCK = @pStock ");
        }
        _sb.AppendLine("  WHERE   CS_CASHIER_ID = @pCashierId ");

        _sb.AppendLine(" ELSE ");

        _sb.AppendLine(" INSERT   CLOSING_STOCKS ");
        _sb.AppendLine("        ( CS_CASHIER_ID ");
        _sb.AppendLine("        , CS_CLOSING_STOCK_TYPE ");
        _sb.AppendLine("        , CS_SLEEPS_ON_TABLE ");
        _sb.AppendLine("        , CS_STOCK ) ");
        _sb.AppendLine(" VALUES ( @pCashierId ");
        _sb.AppendLine("        , @pType ");
        _sb.AppendLine("        , @pSleepsOnTable ");
        _sb.AppendLine("        , @pStock ) ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pType", SqlDbType.BigInt).Value = this.Type;
          _sql_cmd.Parameters.Add("@pSleepsOnTable", SqlDbType.Bit).Value = this.SleepsOnTable;
          _sql_cmd.Parameters.Add("@pStock", SqlDbType.Xml).Value = DatatableToXML(this.CurrenciesStocks);
          _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = this.CashierId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("ClosingStock.UpdateClosingStock");

            return false;
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    /// <summary>
    /// Reset Closing Data to default values.
    /// </summary>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean ResetClosingStock(SqlTransaction Trx, bool saveStock)
    {
      StringBuilder _sb;
      String _ids;
      Int32 _cashiers;

      _ids = string.Empty;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   CLOSING_STOCKS                        ");
        _sb.AppendLine("    SET   CS_CLOSING_STOCK_TYPE = @pType        ");
        _sb.AppendLine("        , CS_SLEEPS_ON_TABLE = @pSleepsOnTable  ");
        if (saveStock)
        {
          _sb.AppendLine("        , CS_STOCK = @pStock                    ");
        }
        if (this.CashierId == this.CashierIdOld)
        {
          _sb.AppendLine("  WHERE   CS_CASHIER_ID = @pCashierIdOld        ");
          _cashiers = 1;
        }
        else
        {
          _ids = this.CashierId.ToString() + "," + this.CashierIdOld.ToString();
          _sb.Append("  WHERE   CS_CASHIER_ID IN (");
          _sb.Append(_ids);
          _sb.Append(") ");
          _cashiers = 2;
        }


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pType", SqlDbType.BigInt).Value = 0;
          _sql_cmd.Parameters.Add("@pSleepsOnTable", SqlDbType.Bit).Value = 0;
          _sql_cmd.Parameters.Add("@pStock", SqlDbType.Xml).Value = DatatableToXML(new DataTable());
          if (this.CashierId == this.CashierIdOld)
          {
            _sql_cmd.Parameters.Add("@pCashierIdOld", SqlDbType.BigInt).Value = this.CashierIdOld;
          }

          if (_sql_cmd.ExecuteNonQuery() != _cashiers)
          {
            Log.Error("ClosingStock.ResetClosingStock");

            return false;
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    #endregion Private Methods
  }
}
