﻿//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FeatureChips.cs  
// 
//   DESCRIPTION: Feature Chips class
//
//        AUTHOR: David Hernández
// 
// CREATION DATE: 01-MAR-2016
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- ------------------------------------------------------
// 01-MAR-2016 DHA        First release.
// 13-APR-2016 RAB        Product Backlog Item 10855: Tables (Phase 1): Currency/chip Selector.
// 28-APR-2016 FGB        Product Backlog Item 9758: Tables (Phase 1): Review table reports
// 09-JUN-2016 RAB        PBI 11755: Tables (Phase 1): Correction errors as a result of the backward compatibility of the chips group
// 06-JUL-2016 DHA        Product Backlog Item 15064: multicurrency chips sale/purchase
// 06-JUL-2016 DHA        Product Backlog Item 15064: multicurrency chips sale/purchase
// 11-JUL-2016 JML        Product Backlog Item 15065: Gaming tables (Fase 4): Player tracking. Multicurrency
// 20-JUL-2016 RGR        Product Backlog Item 15318: Table-21: Sales of chips A and B now
// 27-SEP-2016 DHA        Fixed Bug 17847: added gaming profit for check and bank card
// 28-SEP-2016 DHA        PBI 17747: cancel gaming tables operations
// 29-SEP-2016 RAB        Bug 18194: Cashier: GamingTable, not be can complete the sale of chips in Cashless
// 13-OCT-2016 LTC        Bug 19012: Craw on Gaming tables: won amount is not given as gaming chips
// 14-OCT-2016 DHA        Bug 19012: Craw on Gaming tables: won amount is not given as gaming chips
// 15-NOV-2016 ATB        Bug 20523:Codere - Wrong NLS in Buy/sell Operations
// 15-NOV-2016 ETP        Fixed Bug Bug 20363: Can't recharge wihtout participate in cashdesk draw 
// 12-DEC-2016 ATB        Bug 20523:Codere - Wrong NLS in Buy/sell Operations (fix)
// 28-DEC-2016 MS         Bug 21946:El contenido del ticket de cambio de fichas no es correcto
// 25-JAN-2017 JML        Fixed Bug 23342:WigosGUI: Beneficio de juego en el cierre de una mesa de juego incorrecto en los reportes
// 27-JAN-2016 JML        Fixed Bug 23692: Cashless - Sale of chips: error in integrated mode with promotion
// 02-MAR-2017 DHA        Bug 25245: error showing chips sets for gaming tables configuration
// 03-MAR-2017 JML        Fixed Bug 25394:Tax Custody erroneous. Types 0 y 2
// 23-MAR-2017 RAB        PBI 25973: MES10 Ticket validation - Dealer copy link to TITO ticket
// 24-MAR-2017 RAB        PBI 25974: MES10 Ticket validation - Dealer copy voucher
// 05-APR-2017 JML        Bug 26568:Custodia - add new general param to configure tax custody for gaming tables operations
// 20-APR-2017 JML        Fixed Bug 26807: Custody - Incorrect custody amount to be refunded
// 08-MAY-2017 MS         PBI 27045: Junket Vouchers
// 23-MAY-2017 DHA        PBI 27487: WIGOS-83 MES21 - Sales selection
// 24-MAY-2017 JML        PBI 27484:WIGOS-1226 Win / loss - Introduce the WinLoss - EDITION SCREEN - PER DENOMINATION
// 24-MAY-2017 JML        PBI 27486:WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
// 30-MAY-2017 JML        PBI 27487:WIGOS-83 MES21 - Sales selection
// 06-JUN-2017 DHA        PBI 27760:WIGOS-257: MES21 - Gaming table chair assignment with card selection
// 15-JUN-2017 JML        PBI 27998: WIGOS-2695 - Rounding - Apply rounding according to the minimum chip
// 19-JUN-2017 JML        PBI 27999:WIGOS-2699 - Rounding - Accumulate the remaining amount
// 29-JUN-2017 JML        Fixed Bug 28465:WIGOS-3241 Tax custody is not returned after sale chips with recharge playing on gaming tables draw
// 06-JUL-2017 JML        PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 06-JUL-2017 JML&RAB    Bug 28565: WIGOS-3417 Sale chips with recharge playing on gaming table draw doesn't work
// 10-JUL-2017 RAB        PBI 28002: WIGOS-2730 - Rounding - Undo operation
// 08-AUG-2018 MS         Bug 29255: WIGOS-4302 Not Possible to Close Cash Session. Creditline not a valid Currency Exchange type
// 11-OCT-2017 DHA        Bug 30224:WIGOS-5696 [Ticket #9466] error en canje de tickets por fichas con centavos (TITA)
// 22-JAN-2018 DHA        Bug 31294:WIGOS-7823 Gaming Tables raffle
// 21-FEB-2018 DHA        Bug 31619:WIGOS-8109 [Ticket #12335] Custodia
// 07-FEB-2018 AGS        Fixed Bug 31619:WIGOS-8109 [Ticket #12335] Custodia
// 12-APR-2018 JGC        Bug 32311:[WIGOS-9805]: [Ticket #13564] Reporte de "Resumen de caja"
//------------------------------------------------------------------------------ 

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public static class FeatureChips
  {

    #region Properties

    public static Boolean IsFeatureChipsEnabled
    {
      get
      {
        return GamingTableBusinessLogic.IsGamingTablesEnabled();
      }
    }

    public static Boolean IsChipsValueTypeEnabled
    {
      get
      {
        return GeneralParam.GetBoolean("FeatureChips", "ValueType.Enabled", false);
      }
    }

    public static Boolean IsChipsNonRedeemableTypeEnabled
    {
      get
      {
        return GeneralParam.GetBoolean("FeatureChips", "NonRedeemableType.Enabled", false);
      }
    }

    public static Boolean IsChipsColorTypeEnabled
    {
      get
      {
        return GeneralParam.GetBoolean("FeatureChips", "ColorType.Enabled", false);
      }
    }

    public static Boolean IsChipsStockControlEnabled
    {
      get
      {
        return GeneralParam.GetBoolean("GamingTables", "ChipsStockControl", false);
      }
    }

    public static Boolean IsChipsSaleByTicket
    {
      get
      {
        return GeneralParam.GetBoolean("GamingTables", "Cashier.Mode", false);
      }
    }

    public static Boolean IsGamingTablesEnabled
    {
      get
      {
        return (GeneralParam.GetBoolean("Features", "GamingTables", false) &&
                GamingTableBusinessLogic.IsGamingTablesEnabled() && Cage.IsCageEnabled());
      }
    }

    public static Boolean IsFeaturesGamingTablesEnabled
    {
      get
      {
        return (GeneralParam.GetBoolean("Features", "GamingTables", false) && Cage.IsCageEnabled());
      }
    }

    public static Boolean AllowedPurchaseNrChipsOnCashier
    {
      get
      {
        return GeneralParam.GetBoolean("FeatureChips", "AllowedPurchaseNrChipsOnCashier", false);
      }
    }

    #endregion Properties

    #region Enums

    public enum ChipType
    {
      RE = 1001,
      NR = 1002,
      COLOR = 1003
    };

    #endregion Enums

    #region Public Methods

    public static string GetChipTypeDescription(CurrencyExchangeType CurrencyExchangeType, String IsoCode)
    {
      if (IsChipsType(CurrencyExchangeType))
      {
        return GetChipTypeDescription(ConvertToChipType(CurrencyExchangeType), IsoCode);
      }

      return String.Empty;
    }

    public static string GetChipTypeDescription(CurrencyIsoType CurrencyIsoType)
    {
      if (IsCurrencyExchangeTypeChips(CurrencyIsoType.Type))
      {
        return GetChipTypeDescription(CurrencyIsoType.Type, CurrencyIsoType.IsoCode);
      }

      return String.Empty;
    }

    public static string GetChipTypeDescription(ChipType ChipType, String IsoCode)
    {
      String _iso_code_string;

      _iso_code_string = String.Empty;

      if (!String.IsNullOrEmpty(IsoCode))
      {
        _iso_code_string = String.Format("({0}) ", IsoCode);
      }
      // Change ISO CODE to NLS, if iso_code = Currency as CHIP_VALUE
      switch (ChipType)
      {

        case ChipType.COLOR:
          // COLOR CHIPS
          return GeneralParam.GetString("FeatureChips", "ColorType.Name", Resource.String("STR_CURRENCY_TYPE_3_ISO_X02"));

        case ChipType.RE:
          // VALUE CHIPS
          // ATB 15-NOV-2016
          return _iso_code_string + GeneralParam.GetString("FeatureChips", "ValueType.Name", Resource.String("STR_CURRENCY_TYPE_3_ISO_X01"));

        case ChipType.NR:
          // No-Redimeable CHIPS
          // ATB 15-NOV-2016
          return _iso_code_string + GeneralParam.GetString("FeatureChips", "NonRedeemableType.Name", Resource.String("STR_CURRENCY_TYPE_1002_NON_REDEEMABLE"));

        default:
          return "";

      }
    }

    public static Boolean IsChipsTypeEnabledGP(ChipType Type)
    {
      switch (Type)
      {
        case ChipType.RE:
          return FeatureChips.IsChipsValueTypeEnabled;

        case ChipType.NR:
          return FeatureChips.IsChipsNonRedeemableTypeEnabled;

        case ChipType.COLOR:
          return FeatureChips.IsChipsColorTypeEnabled;
        default:
          throw new Exception("FeatureChips.IsChipsTypeEnabled: type not found");

      }
    }

    public static Int32 NumMaxGroupsByChipType(ChipType Type)
    {
      switch (Type)
      {
        case ChipType.RE:
          return GeneralParam.GetInt32("FeatureChips", "ValueType.NumMaxGroup", 1);

        case ChipType.NR:
          return GeneralParam.GetInt32("FeatureChips", "NonRedeemableType.NumMaxGroup", 1);

        case ChipType.COLOR:
          return GeneralParam.GetInt32("FeatureChips", "ColorType.NumMaxGroup", 1);

        default:
          return 0;
      }
    }

    public static List<CurrencyExchange> FilterAllowedCurrenciesAndChips(List<CurrencyExchange> Currencies, Int64? GamingTableId)
    {
      return FilterAllowedCurrenciesAndChips(FeatureChips.ChipsOperation.Operation.NONE, Currencies, GamingTableId, null);
    }

    public static List<CurrencyExchange> FilterAllowedCurrenciesAndChips(FeatureChips.ChipsOperation.Operation OperationType, List<CurrencyExchange> Currencies, Int64? GamingTableId, String IsoCode)
    {
      List<CurrencyExchange> _currencies;

      _currencies = new List<CurrencyExchange>();

      if (Currencies == null)
      {
        return null;
      }

      foreach (CurrencyExchange _currency in Currencies)
      {
        if (!IsChipsType(_currency.Type))
        {
          if ((IsoCode == null || IsoCode == _currency.CurrencyCode) && AllowedCurrencies(OperationType, _currency.CurrencyCode, GamingTableId) && OperationType != ChipsOperation.Operation.WIN_LOSS)
          {
            _currencies.Add(_currency);
          }
        }
        else
        {
          if ((_currency.CurrencyCode != null) && IsChipsType(_currency.Type) && IsChipsTypeEnabledGP(ConvertToChipType(_currency.Type)) &&
                  AllowedChipOperation(OperationType, _currency.Type, _currency.CurrencyCode, GamingTableId))
          {
            _currencies.Add(_currency);
          }
        }
      }

      return _currencies;
    }

    public static Boolean AllowedChipOperation(FeatureChips.ChipsOperation.Operation OperationType, CurrencyExchangeType Type, String ISOCode, Int64? GamingTable)
    {
      //FeatureChips.ChipsSets.GamingTableId = GamingTable;

      if (!IsChipsTypeEnabledGP(ConvertToChipType(Type)))
      {
        return false;
      }

      foreach (KeyValuePair<Int64, ChipsSets.ChipSet> _set in FeatureChips.ChipsSets.GetChipsSetsEnabled())
      {
        if (AllowedChipOperationBySet(OperationType, _set.Value, Type, ISOCode, GamingTable))
        {
          return true;
        }
      }

      return false;
    }

    public static Boolean AllowedChipOperationBySet(FeatureChips.ChipsOperation.Operation OperationType, ChipsSets.ChipSet ChipSet, CurrencyExchangeType Type, String ISOCode, Int64? GamingTable)
    {
      if (ChipSet.Type == ConvertToChipType(Type) && ChipSet.ISOCode == ISOCode)
      {
        switch (OperationType)
        {
          case ChipsOperation.Operation.NONE:
            return false;
          case ChipsOperation.Operation.SALE:
            if (ChipSet.Type == ChipType.COLOR)
            {
              return false;
            }
            else if (!WSI.Common.TITO.Utils.IsTitoMode() && ISOCode != CurrencyExchange.GetNationalCurrency())
            {
              return false;
            }

            return ChipSet.AllowedSale;

          case ChipsOperation.Operation.SALE_AMOUNT:

            return false;

          case ChipsOperation.Operation.PURCHASE:
            if (ChipSet.Type == ChipType.COLOR)
            {
              return false;
            }
            else if (!WSI.Common.TITO.Utils.IsTitoMode() && ISOCode != CurrencyExchange.GetNationalCurrency())
            {
              return false;
            }

            return ChipSet.AllowedPurchase;

          case ChipsOperation.Operation.CHANGE_IN:
          case ChipsOperation.Operation.CHANGE_OUT:
            if (ChipSet.Type == ChipType.COLOR)
            {
              return false;
            }

            return true;

          case ChipsOperation.Operation.SWAP:
            if (ChipSet.Type == ChipType.COLOR)
            {
              return false;
            }

            return true;

          case ChipsOperation.Operation.CAGE_CONCEPTS:
            if (ChipSet.Type == ChipType.COLOR)
            {
              return false;
            }

            return true;
          case ChipsOperation.Operation.WIN_LOSS:
            if (ChipSet.Type != ChipType.RE)
            {
              return false;
            }

            return true;
        }

        return true;
      }

      return false;
    }

    private static Boolean AllowedCurrencies(ChipsOperation.Operation OperationType, String ISOCode, Int64? GamingTable)
    {
      if (OperationType == ChipsOperation.Operation.CHANGE_IN
        || OperationType == ChipsOperation.Operation.CHANGE_OUT
        || OperationType == ChipsOperation.Operation.SALE_REGISTER
        || OperationType == ChipsOperation.Operation.PURCHASE
        || OperationType == ChipsOperation.Operation.SALE
        || OperationType == ChipsOperation.Operation.SHOW_STOCK
        || OperationType == ChipsOperation.Operation.SWAP)
      {
        return false;
      }

      // Only allow currencies from chips sets associated to the gamingtable
      if (OperationType == ChipsOperation.Operation.SALE_AMOUNT && TITO.Utils.IsTitoMode())
      {
        foreach (KeyValuePair<Int64, ChipsSets.ChipSet> _set in ChipsSets.GetChipsSetsEnabled())
        {
          if (OperationType == ChipsOperation.Operation.SALE_AMOUNT && _set.Value.AllowedSale && ISOCode == _set.Value.ISOCode)
          {
            return true;
          }
        }

        return false;
      }

      return true;
    }

    private static Boolean IsIsoCodeAllowedInSets(ChipsOperation.Operation OperationType, String ISOCode)
    {
      List<String> _iso_codes_allowed;

      _iso_codes_allowed = new List<string>();

      foreach (KeyValuePair<Int64, ChipsSets.ChipSet> _set in ChipsSets.GetChipsSetsEnabled())
      {
        // First part of operation Sale Chips needs to introduce the amount and has seted a GamingTableId
        if (OperationType == ChipsOperation.Operation.NONE && _set.Value.AllowedSale &&
              !_iso_codes_allowed.Contains(_set.Value.ISOCode))
        {
          return true;
        }
      }

      return false;
    }

    public static void ReloadChipsAndSets()
    {
      Cage.ShowDenominationsMode _denominations_mode;
      _denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);

      ReloadChipsAndSets(_denominations_mode);
    }
    public static void ReloadChipsAndSets(Cage.ShowDenominationsMode DenominationsMode)
    {
      // Force reload Chips
      Chips.Create(DenominationsMode);

      // Fore reload chips Set
      ChipsSets.Create();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Validate if the Type belong to Chips
    //
    //  PARAMS :
    //      - INPUT : 
    //                CurrencyExchangeType: Type
    //                
    //      - OUTPUT : 
    //                
    //
    // RETURNS :
    //                Boolean: True if the type belong to Chips
    //
    //   NOTES :
    //
    public static Boolean IsCurrencyExchangeTypeChips(CurrencyExchangeType Type)
    {
      return IsChipsType(Type);
    }

    public static Boolean IsChipsType(CurrencyExchangeType Type)
    {
      return Type == CurrencyExchangeType.CASINOCHIP || Type == CurrencyExchangeType.CASINO_CHIP_RE ||
              Type == CurrencyExchangeType.CASINO_CHIP_NRE || Type == CurrencyExchangeType.CASINO_CHIP_COLOR;
    } // IsCurrencyExchangeTypeChips

    public static Boolean IsChipsType(CageCurrencyType Type)
    {
      return IsChipsType(ConvertToCurrencyExchangeType(Type));
    }

    public static ChipType ConvertToChipType(CurrencyExchangeType Type)
    {
      switch (Type)
      {
        case CurrencyExchangeType.CASINO_CHIP_RE:
        case CurrencyExchangeType.CASINOCHIP:
          return ChipType.RE;

        case CurrencyExchangeType.CASINO_CHIP_NRE:
          return ChipType.NR;

        case CurrencyExchangeType.CASINO_CHIP_COLOR:
          return ChipType.COLOR;

        default:
          Log.Error("FeatureChips.ConvertCurrencyExchangeType: Not supported type");
          throw new Exception("FeatureChips.ConvertCurrencyExchangeType: Not supported type");
      }
    }

    public static CurrencyExchangeType ConvertToCurrencyExchangeType(ChipType Type)
    {
      switch (Type)
      {
        case ChipType.RE:
          return CurrencyExchangeType.CASINO_CHIP_RE;

        case ChipType.NR:
          return CurrencyExchangeType.CASINO_CHIP_NRE;

        case ChipType.COLOR:
          return CurrencyExchangeType.CASINO_CHIP_COLOR;

        default:
          throw new Exception("FeatureChips.ConvertToCurrencyExchangeType: Not supported type");
      }
    }

    public static CurrencyExchangeType ConvertToCurrencyExchangeType(CageCurrencyType Type)
    {
      switch (Type)
      {
        case CageCurrencyType.ChipsRedimible:
          return CurrencyExchangeType.CASINO_CHIP_RE;

        case CageCurrencyType.ChipsNoRedimible:
          return CurrencyExchangeType.CASINO_CHIP_NRE;

        case CageCurrencyType.ChipsColor:
          return CurrencyExchangeType.CASINO_CHIP_COLOR;

        default:
          return CurrencyExchangeType.CURRENCY;
      }
    }

    public static CageCurrencyType ConvertToCageCurrencyType(CurrencyExchangeType Type)
    {
      switch (Type)
      {
        case CurrencyExchangeType.CURRENCY:
          return CageCurrencyType.Bill;

        case CurrencyExchangeType.CARD:
          return CageCurrencyType.Others;

        case CurrencyExchangeType.CHECK:
          return CageCurrencyType.Others;

        case CurrencyExchangeType.CASINOCHIP:
          return CageCurrencyType.ChipsRedimible;

        case CurrencyExchangeType.FREE:
          return CageCurrencyType.Others;

        case CurrencyExchangeType.CASINO_CHIP_RE:
          return CageCurrencyType.ChipsRedimible;

        case CurrencyExchangeType.CASINO_CHIP_NRE:
          return CageCurrencyType.ChipsNoRedimible;

        case CurrencyExchangeType.CASINO_CHIP_COLOR:
          return CageCurrencyType.ChipsColor;

        case CurrencyExchangeType.CREDITLINE:
          return CageCurrencyType.Creditline;

        default:
          throw new Exception("FeatureChips.ConvertToCageCurrencyType: Not supported type");
      }
    }

    public static CageCurrencyType ConvertToCageCurrencyType(ChipType Type)
    {
      switch (Type)
      {
        case ChipType.RE:
          return CageCurrencyType.ChipsRedimible;

        case ChipType.NR:
          return CageCurrencyType.ChipsNoRedimible;

        case ChipType.COLOR:
          return CageCurrencyType.ChipsColor;

        default:
          throw new Exception("FeatureChips.ConvertToCageCurrencyType: Type not suported.");

      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Validate if the Type belong to Chips
    //
    //  PARAMS :
    //      - INPUT : 
    //                CageCurrencyType: Type
    //                
    //      - OUTPUT : 
    //                
    //
    // RETURNS :
    //                Boolean: True if the type belong to Chips
    //
    //   NOTES :
    //
    public static Boolean IsCageCurrencyTypeChips(CageCurrencyType Type)
    {
      return Type == CageCurrencyType.ChipsRedimible || Type == CageCurrencyType.ChipsNoRedimible ||
              Type == CageCurrencyType.ChipsColor;
    } // IsCurrencyExchangeTypeChips

    //------------------------------------------------------------------------------
    // PURPOSE : Validate if Integrated Chip and Credit Operations is enabled
    //
    //  PARAMS :
    //      - INPUT : 
    //                Boolean: IsVirtualCard
    //                
    //      - OUTPUT : 
    //                
    //
    // RETURNS :
    //                Boolean: True if Integrated Chip and Credit Operations is enabled
    //
    //   NOTES :
    //
    public static Boolean IsIntegratedChipAndCreditOperations(Boolean IsVirtualCard)
    {
      if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        return false;
      }

      return IsVirtualCard || GeneralParam.GetBoolean("GamingTables", "Cashier.IntegratedChipAndCreditOperations", false);

    } // IsIntegratedChipAndCreditOperations

    public static Boolean CashierChipsSaleMode(Boolean IsVirtualCard)
    {
      if (WSI.Common.TITO.Utils.IsTitoMode() || IsVirtualCard)
      {
        return false;
      }

      return GeneralParam.GetBoolean("GamingTables", "Cashier.ChipSale.Mode", false);

    } // CashierChipsSaleMode

    public static Boolean Sale(FeatureChips.ChipsOperation ChipsAmount, Int64 OperationId, CardData Card,
                               Boolean IsTitoMode, CashierSessionInfo CashierSessionInfo, out ArrayList VoucherList,
                               IDictionary<long, decimal> tickets, SqlTransaction Trx, OperationCode OpCode,
                               Currency ChipPrize, Currency AmountToPay, out Ticket TicketCopyDealer, out String MessageError) // LTC 13-OCT-2016
    {
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _fin_balance;
      MultiPromos.AccountBalance _balance;
      Decimal _current_balance;
      VoucherChipsPurchaseSale _voucher;
      VoucherChipsSwapWithTicketTito _voucher_swap;
      VoucherChipsSaleDealerCopy _voucher_chips;
      TYPE_SPLITS _splits;
      Currency _balance_sale;
      CASHIER_MOVEMENT _cashier_movement;
      Decimal _amount;
      CurrencyExchangeType _currency_exchange_type;
      Barcode _voucher_barcode;

      ChipsSplits _operation;

      WSI.Common.MultiPromos.AccountBalance _account_balance_chips_sales_remaining_amount;

      _operation = new ChipsSplits();

      VoucherList = new ArrayList();
      _currency_exchange_type = CurrencyExchangeType.CASINO_CHIP_RE;

      MessageError = String.Empty;

      // Ticket copy dealer
      TicketCopyDealer = null;

      try
      {
        _account_balance_chips_sales_remaining_amount = MultiPromos.AccountBalance.Zero;
        _account_balance_chips_sales_remaining_amount.Operation_ChipsSalesRemainingAmount = ChipsAmount.RoundingSaleChips.RemainingAmount - ChipsAmount.DevolutionRoundingSaleChips;

        if (!MultiPromos.Trx_UpdateAccountBalance(Card.AccountId, _account_balance_chips_sales_remaining_amount, out _ini_balance, Trx))
        {
          return false;
        }

        _account_movements = new AccountMovementsTable(CashierSessionInfo);
        _cashier_movements = new CashierMovementsTable(CashierSessionInfo);

        if (!Split.ReadSplitParameters(out _splits))
        {
          return false;
        }

        // LEM & JBP: 16-JAN-2014
        if (IsTitoMode)
        {
          _balance_sale = ChipsAmount.InitialAmountNationalCurrency;

          if (ChipsAmount.PrintTicket)
          {
            _balance_sale = ChipsAmount.AmountNationalCurrency;
          }

          if (OpCode == OperationCode.CHIPS_SWAP)
          {
            _balance_sale = ChipsAmount.FillChipsAmount;
          }

          // check enough balance
          if (_balance_sale > _ini_balance.TotalRedeemable)
          {
            return false;
          }

          _operation.AmountToSale = _balance_sale;
          _operation.IsIntegratedOperation = false;
          _operation.ChipPrize = ChipPrize;
          _operation.ChipsREAmount = ChipsAmount.ChipsREAmount;
          _operation.AmountToPay = AmountToPay;

          _balance = CalculateBalance(_ini_balance, _operation); // LTC 13-OCT-2016

          // Update account balance
          if (!TITO.Utils.AccountBalanceOut(Card, _splits, _balance, 0, _ini_balance, out _fin_balance, Trx))
          {
            return false;
          }
        }
        else
        {
          if (!MultiPromos.Trx_SetActivity(Card.AccountId, Trx))
          {
            return false;
          }

          // check enough balance
          if (ChipsAmount.AmountNationalCurrency - ChipsAmount.DevolutionRoundingSaleChips > _ini_balance.TotalRedeemable + _ini_balance.Account_ChipsSalesRemainingAmount)
          {
            MessageError = Resource.String("STR_FRM_CHIPS_ERROR_LIMIT_ACOCUNT_CREDITS", ChipsAmount.ChipsAmount, (Currency)Card.AccountBalance.TotalRedeemable).Replace("\\r\\n", "\r\n");
            return false;
          }

          _operation.AmountToSale = ChipsAmount.AmountNationalCurrency;
          _operation.IsIntegratedOperation = OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE ? true : false;
          _operation.ChipPrize = ChipPrize;
          _operation.ChipsREAmount = ChipsAmount.ChipsREAmount;
          _operation.AmountToPay = AmountToPay;

          _balance = CalculateBalance(_ini_balance, _operation);

          if (_operation.IsIntegratedOperation)
          {
            _balance.CustodyProvision = 0;
          }

          // On chips sale balance considere the price of the draw
          if (ChipsAmount.CashDeskDrawResult != null)
          {
            _balance.Redeemable -= ChipsAmount.CashDeskDrawResult.m_participation_amount;

            if (ChipsAmount.CashDeskDrawResult.m_re_awarded > 0)
            {
              _balance.PromoRedeemable += ChipsAmount.CashDeskDrawResult.m_participation_amount;
            }
            else
            {
              _balance.PromoNotRedeemable += ChipsAmount.CashDeskDrawResult.m_participation_amount;
            }
          }
          // Update account balance
          if (!MultiPromos.Trx_UpdateAccountBalance(Card.AccountId, MultiPromos.AccountBalance.Negate(_balance), out _fin_balance, Trx))
          {
            return false;
          }
          
          if (FeatureChips.IsIntegratedChipAndCreditOperations(Card.IsVirtualCard))
          {
            if (!MultiPromos.Trx_SetInitialCashIn(Card.AccountId, ChipsAmount.ChipsAmount, Trx))
            {
              return false;
        }
          }

        }

        // Update Account_Operations table
        if (!Operations.Trx_SetOperationBalance(OperationId, _balance, Trx))
        {
          return false;
        }

        if (!ChipsAmount.IsChipsSaleRegister && ChipsAmount.RoundingSaleChips.RemainingAmount > 0)
        {
          _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT, ChipsAmount.RoundingSaleChips.RemainingAmount, Card.AccountId, Card.TrackData, "", CurrencyExchange.GetNationalCurrency(), 0, 0, ChipsAmount.SelectedGamingTable.GamingTableSession);
        }

        if (!ChipsAmount.IsChipsSaleRegister && ChipsAmount.DevolutionRoundingSaleChips > 0)
        {
          _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT, ChipsAmount.DevolutionRoundingSaleChips, Card.AccountId, Card.TrackData, "", CurrencyExchange.GetNationalCurrency(), 0, 0, ChipsAmount.SelectedGamingTable.GamingTableSession);
        }

        if (OpCode == OperationCode.CHIPS_SWAP)
        {
          _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_SWAP_FOR_CHIPS;
          _cashier_movements.Add(OperationId, _cashier_movement, ChipsAmount.FillChipsAmount, Card.AccountId, Card.TrackData, "", ChipsAmount.IsoCode, 0, 0, ChipsAmount.SelectedGamingTable.GamingTableSession, -1, CurrencyExchangeType.CASINOCHIP, null, null);
        }

        //MOVEMENTS PER DENOMINATION
        foreach (KeyValuePair<FeatureChips.Chips.Chip, Int32> _chip_amount in ChipsAmount.ChipsItemsUnits)
        {
          _amount = 0;
          if (_chip_amount.Value > 0)
          {
            _cashier_movement = ChipsAmount.MovementType;

            _amount = _chip_amount.Value;
            _currency_exchange_type = FeatureChips.ConvertToCurrencyExchangeType(_chip_amount.Key.Type);
            if (_chip_amount.Key.Type != FeatureChips.ChipType.COLOR)
            {
              _amount *= _chip_amount.Key.Denomination;
            }

            if (_cashier_movement == CASHIER_MOVEMENT.TITO_TICKET_CASHIER_SWAP_FOR_CHIPS)
            {
              _cashier_movement = CASHIER_MOVEMENT.CHIPS_SALE;
            }

            _cashier_movements.Add(OperationId, _cashier_movement, _amount,
                                   Card.AccountId, Card.TrackData, "", ChipsAmount.IsoCode, _chip_amount.Key.Denomination, 0,
                                   ChipsAmount.SelectedGamingTable.GamingTableSession, -1, _currency_exchange_type, FeatureChips.ConvertToCageCurrencyType(_chip_amount.Key.Type), _chip_amount.Key.ChipId);
          }
        }

        //MOVEMENT TOTAL
        _cashier_movement = ChipsAmount.IsChipsSaleRegister ? CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL : CASHIER_MOVEMENT.CHIPS_SALE_TOTAL;

        // Added movement for chips conversion to national currency
        if (ChipsAmount.IsoCode == CurrencyExchange.GetNationalCurrency() || ChipsAmount.IsoCode == String.Empty)
        {
          _cashier_movements.Add(OperationId, _cashier_movement, ChipsAmount.ChipsAmount, Card.AccountId, Card.TrackData, "", CurrencyExchange.GetNationalCurrency(), 0, 0, ChipsAmount.SelectedGamingTable.GamingTableSession, -1, _currency_exchange_type, FeatureChips.ConvertToCageCurrencyType(_currency_exchange_type), null);
        }
        else
        {
          _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE, ChipsAmount.AmountNationalCurrency, Card.AccountId, Card.TrackData, "", ChipsAmount.IsoCode, 0, 0, ChipsAmount.SelectedGamingTable.GamingTableSession, -1, _currency_exchange_type, FeatureChips.ConvertToCageCurrencyType(_currency_exchange_type), null);
          _cashier_movements.SetInitialAndFinalBalanceAmountInLastMov(ChipsAmount.ChipsAmount, ChipsAmount.AmountNationalCurrency);
        }

        _current_balance = _ini_balance.TotalBalance;

        if (OpCode != OperationCode.CHIPS_SWAP)
        {
          if (ChipsAmount.RoundingSaleChips.RemainingAmount > 0)
          {
            _account_movements.Add(OperationId, Card.AccountId, MovementType.ChipsSaleRemainingAmount,
                                   _current_balance, ChipsAmount.RoundingSaleChips.RemainingAmount, 0, _current_balance - ChipsAmount.RoundingSaleChips.RemainingAmount);
          }
          _current_balance -= ChipsAmount.RoundingSaleChips.RemainingAmount;
          if (ChipsAmount.DevolutionRoundingSaleChips > 0)
          {
            _account_movements.Add(OperationId, Card.AccountId, MovementType.ChipsSaleConsumedRemainingAmount,
                                   _current_balance, 0, ChipsAmount.DevolutionRoundingSaleChips, _current_balance + ChipsAmount.DevolutionRoundingSaleChips);
          }
          _current_balance += ChipsAmount.DevolutionRoundingSaleChips;
          _account_movements.Add(OperationId, Card.AccountId, MovementType.ChipsSaleTotal,
                                 _current_balance, ChipsAmount.AmountNationalCurrency, 0, _current_balance - ChipsAmount.ChipsAmount);
          _current_balance -= ChipsAmount.ChipsAmount;
        }
        if (OpCode != OperationCode.CHIPS_SWAP)
        {
          _current_balance -= ChipsAmount.AmountNationalCurrency + ChipsAmount.RoundingSaleChips.RemainingAmount - ChipsAmount.DevolutionRoundingSaleChips;
        }
        else
        {
          _current_balance -= ChipsAmount.FillChipsAmount;
        }

        if (IsTitoMode
          && ChipsAmount.AmountNationalCurrencyDifference > 0
          && !ChipsAmount.PrintTicket) // JBP 15-JAN-2014
        {
          // Add cashier and account movements
          _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO,
                                 ChipsAmount.AmountNationalCurrencyDifference, Card.AccountId, Card.TrackData, "", "");
          _account_movements.Add(OperationId, Card.AccountId, MovementType.ChipsSaleDevolutionForTito,
                                 _current_balance, ChipsAmount.AmountNationalCurrencyDifference, 0, _current_balance - ChipsAmount.AmountNationalCurrencyDifference);
        }

        if (!_account_movements.Save(Trx))
        {
          return false;
        }
        if (!_cashier_movements.Save(Trx))
        {
          return false;
        }

        if (OpCode == OperationCode.CHIPS_SWAP)
        {
          //Save the HTML SwapChips into CASHIER_VOUCHERS table
          _voucher_swap = new VoucherChipsSwapWithTicketTito(Card.VoucherAccountInfo(), _splits, ChipsAmount, tickets, false, PrintMode.Print, Trx);
          if (!_voucher_swap.Save(OperationId, Trx))
          {
            return false;
          }
          VoucherList.Add(_voucher_swap);

          //Save the HTML SwapChips DEALER COPY into CASHIER_VOUCHERS table
          if (GeneralParam.GetBoolean("GamingTables", "Cashier.Mode"))
          {
            _voucher_swap = new VoucherChipsSwapWithTicketTito(Card.VoucherAccountInfo(), _splits, ChipsAmount, tickets, true, PrintMode.Print, Trx);
            if (!_voucher_swap.Save(OperationId, Trx))
            {
              return false;
            }
            VoucherList.Add(_voucher_swap);
          }
        }
        else
        {
          //Save the HTML SaleChips ticket into CASHIER_VOUCHERS table
          _voucher = new VoucherChipsPurchaseSale(Card.VoucherAccountInfo(), _splits, ChipsAmount, PrintMode.Print, Trx);
          if (!_voucher.Save(OperationId, Trx))
          {
            return false;
          }
          VoucherList.Add(_voucher);


          //DLL 22-APR-2014: Print another copy for the Dealer 
          if (GeneralParam.GetBoolean("GamingTables", "Cashier.Mode"))
          {
            TYPE_SPLITS _split;
            Ticket _ticket;
            int _expiration_days;
            long _sequence;

            Split.ReadSplitParameters(out _split);
            _ticket = new Ticket();
            _ticket.TicketType = TITO_TICKET_TYPE.CHIPS_DEALER_COPY;
            _ticket.CreatedDateTime = WGDB.Now;
            _ticket.CreatedTerminalId = CashierSessionInfo.TerminalId;

            _expiration_days = WSI.Common.TITO.Utils.DefaultTicketExpiration(_ticket.TicketType);
            _ticket.ExpirationDateTime = _ticket.CreatedDateTime.AddDays(_expiration_days);
            _ticket.CreatedAccountID = Card.AccountId;
            _ticket.ValidationType = TITO_VALIDATION_TYPE.SYSTEM;
            _ticket.ValidationNumber = Cashier.Common_ComposeValidationNumber(out _sequence, Trx, CashierSessionInfo.TerminalId);

            //MachineTicketNumber are the last 4 digits from the sequence
            _ticket.MachineTicketNumber = (Int32)_sequence % 10000;

            //Amount is converted into DB_CreateTicket function
            _ticket.Amount = ChipsAmount.ChipsAmount;
            _ticket.Amt_0 = ChipsAmount.ChipsAmount;
            _ticket.Cur_0 = ChipsAmount.IsoCode;
            _ticket.TransactionId = OperationId;

            _ticket.DB_CreateTicket(Trx, Card.TrackData, false, _ticket.Amount, _cashier_movements, _account_movements);

            _voucher_barcode = Barcode.Create(BarcodeType.TitoSystemValidation, _ticket.ValidationNumber);
            _voucher_chips = new VoucherChipsSaleDealerCopy(Card.VoucherAccountInfo(), _split, ChipsAmount, _voucher_barcode.ExternalCode, PrintMode.Print, _ticket.ExpirationDateTime, Trx);
            if (!_voucher_chips.Save(OperationId, Trx))
            {
              return false;
            }
            VoucherList.Add(_voucher_chips);

            TicketCopyDealer = _ticket;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Sale

    //------------------------------------------------------------------------------
    // PURPOSE : Purchase chips operation
    //
    //  PARAMS :
    //      - INPUT : 
    //                ChipsOperation: ChipsOperation
    //                Int64: OperationId
    //                CardData: Card
    //                Boolean: IsTitoMode
    //                CashierSessionInfo: CashierSessionInfo
    //                SqlTransaction: Trx
    //                
    //      - OUTPUT : 
    //                ArrayList: VoucherList
    //                
    //
    // RETURNS :
    //                Boolean: True: Inserted ok. False: Otherwise.
    //
    //   NOTES :
    //
    public static Boolean Purchase(ChipsOperation ChipsOperation, Int64 OperationId, CardData Card,
                                   Boolean IsTitoMode, CashierSessionInfo CashierSessionInfo, out ArrayList VoucherList,
                                   SqlTransaction Trx)
    {
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _fin_balance;
      MultiPromos.AccountBalance _balance;
      VoucherChipsPurchaseSale _voucher;
      TYPE_SPLITS _splits;
      OperationCode _operation_code;

      VoucherList = new ArrayList();
      //_operation_code = GeneralParam.GetBoolean("GamingTables", "Cashier.IntegratedChipAndCreditOperations", false) ? OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT : OperationCode.CHIPS_PURCHASE;
      _operation_code = FeatureChips.IsIntegratedChipAndCreditOperations(Card.IsVirtualCard) ? OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT : OperationCode.CHIPS_PURCHASE;

      try
      {
        if (!MultiPromos.Trx_UpdateAccountBalance(Card.AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, Trx))
        {
          return false;
        }

        _account_movements = new AccountMovementsTable(CashierSessionInfo);
        _cashier_movements = new CashierMovementsTable(CashierSessionInfo);

        if (IsTitoMode)
        {
          if (!TITO.Utils.AccountBalanceIn(Card.AccountId, ChipsOperation.AmountNationalCurrency, out _ini_balance, out _fin_balance, Trx))
          {
            return false;
          }
        }
        else
        {
          if (!MultiPromos.Trx_SetActivity(Card.AccountId, Trx))
          {
            return false;
          }

          _balance = new MultiPromos.AccountBalance(ChipsOperation.AmountNationalCurrency, 0, 0);

          if (!MultiPromos.Trx_UpdateAccountBalance(Card.AccountId, _balance, out _fin_balance, Trx))
          {
            return false;
          }

          // DRV & RCI 22-AUG-2014: Update the balance in memory.
          if (_operation_code == OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT)
          {
            Card.CurrentBalance = ChipsOperation.AmountNationalCurrency;
            Card.AccountBalance = _balance;
          }
          else
          {
            Card.CurrentBalance += ChipsOperation.AmountNationalCurrency;
            Card.AccountBalance.Redeemable += ChipsOperation.AmountNationalCurrency;
          }

          // Update Account_Operations table
          if (!Operations.Trx_SetOperationBalance(OperationId, _balance, Trx))
          {
            return false;
          }
        }

        //MOVEMENTS PER DENOMINATION

        Chips.Chip _chip;
        foreach (KeyValuePair<Int64, Int32> _chip_units in ChipsOperation.ChipsUnits)
        {
          if (_chip_units.Value <= 0)
          {
            continue;
          }

          _chip = Chips.GetChip(_chip_units.Key);

          if (_chip != null)
          {
            if (_chip.Type == FeatureChips.ConvertToChipType(CurrencyExchangeType.CASINO_CHIP_COLOR))
            {
              _cashier_movements.Add(OperationId, ChipsOperation.MovementType, _chip_units.Value,
                                     Card.AccountId, Card.TrackData, "", ChipsOperation.IsoCode, 0, 0,
                                     ChipsOperation.SelectedGamingTable.GamingTableSession, -1, FeatureChips.ConvertToCurrencyExchangeType(_chip.Type), FeatureChips.ConvertToCageCurrencyType(_chip.Type), _chip.ChipId);

            }
            else
            {
              _cashier_movements.Add(OperationId, ChipsOperation.MovementType, _chip_units.Value * (Decimal)_chip.Denomination,
                                     Card.AccountId, Card.TrackData, "", ChipsOperation.IsoCode, (Decimal)_chip.Denomination, 0,
                                     ChipsOperation.SelectedGamingTable.GamingTableSession, -1, FeatureChips.ConvertToCurrencyExchangeType(_chip.Type), FeatureChips.ConvertToCageCurrencyType(_chip.Type), _chip.ChipId);

            }

          }
        }

        //MOVEMENT TOTAL

        // Added movement for chips conversion to national currency
        if (ChipsOperation.IsoCode == CurrencyExchange.GetNationalCurrency() || ChipsOperation.IsoCode == String.Empty)
        {
          _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL, ChipsOperation.ChipsAmount, Card.AccountId, Card.TrackData, "",
                                 ChipsOperation.IsoCode, 0, 0, ChipsOperation.SelectedGamingTable.GamingTableSession, -1, ChipsOperation.CurrencyExchangeType, FeatureChips.ConvertToCageCurrencyType(ChipsOperation.CurrencyExchangeType), null);
        }
        else
        {
          _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE, ChipsOperation.AmountNationalCurrency, Card.AccountId, Card.TrackData, "", ChipsOperation.IsoCode, 0, 0, ChipsOperation.SelectedGamingTable.GamingTableSession, -1, ChipsOperation.CurrencyExchangeType, FeatureChips.ConvertToCageCurrencyType(ChipsOperation.CurrencyExchangeType), null);
          _cashier_movements.SetInitialAndFinalBalanceAmountInLastMov(ChipsOperation.ChipsAmount, ChipsOperation.AmountNationalCurrency);
        }

        _account_movements.Add(OperationId, Card.AccountId, MovementType.ChipsPurchaseTotal,
                               _ini_balance.TotalBalance, 0, ChipsOperation.AmountNationalCurrency, _ini_balance.TotalBalance + ChipsOperation.AmountNationalCurrency);


        if (!_account_movements.Save(Trx))
        {
          return false;
        }
        if (!_cashier_movements.Save(Trx))
        {
          return false;
        }

        if (!Split.ReadSplitParameters(out _splits))
        {
          return false;
        }

        _voucher = new VoucherChipsPurchaseSale(Card.VoucherAccountInfo(), _splits, ChipsOperation, PrintMode.Print, Trx);
        _voucher.Save(OperationId, Trx);

        VoucherList.Add(_voucher);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Purchase

    //------------------------------------------------------------------------------
    // PURPOSE : Chips Change operation
    //
    //  PARAMS :
    //      - INPUT : 
    //                CashierSessionInfo: CashierInfo
    //                Int32: SessionId
    //                Dictionary<Int64, Int32>: ChipsIn
    //                Dictionary<Int64, Int32>: ChipsOut
    //                SqlTransaction: Trx
    //                
    //      - OUTPUT : 
    //                ArrayList: VoucherList
    //                
    //
    // RETURNS :
    //                Boolean: True if Integrated Chip and Credit Operations is enabled
    //
    //   NOTES :
    //
    public static Boolean ChipsChange(CashierSessionInfo CashierInfo, Int64 SessionId, FeatureChips.ChipsOperation ChipsIn, FeatureChips.ChipsOperation ChipsOut, out ArrayList VoucherList, SqlTransaction Trx)
    {
      VoucherChipsChange _voucher_change;
      Boolean _voucher_saved;
      TYPE_SPLITS _splits;
      VoucherList = new ArrayList();

      _voucher_saved = false;

      if (!FeatureChips.AddChipsChangeMovements(ChipsOut.ChipsItemsUnits, ChipsIn.ChipsItemsUnits, CashierInfo, Trx))
      {

        return false;
      }

      if (!Split.ReadSplitParameters(out _splits))
      {

        return false;
      }

      try
      {

        // Bug 21946:El contenido del ticket de cambio de fichas no es correcto

        _voucher_change = new VoucherChipsChange(_splits, ChipsIn.ChipsItemsUnits, ChipsOut.ChipsItemsUnits, PrintMode.Print, Trx);

        // - Save voucher
        _voucher_saved = _voucher_change.Save(Trx);

        if (!_voucher_saved)
        {
          Log.Error("Change Chips. Error saving voucher. Session id: " + SessionId.ToString());

          return false;
        }

        VoucherList.Add(_voucher_change);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      return true;
    }

    public static Boolean CreateGameCollectedMovements(CashierSessionInfo CashierInfo, SortedDictionary<CurrencyIsoType, Decimal> CurrencyAmounts, Int64 GamingTableSessionId, Boolean IsClose, Int64 OperationId, SqlTransaction Trx)
    {
      DataTable _dt_currency_amounts;

      _dt_currency_amounts = new DataTable();
      _dt_currency_amounts.Columns.Add("ISO_CODE");
      _dt_currency_amounts.Columns.Add("TYPE", typeof(CurrencyExchangeType));
      _dt_currency_amounts.Columns.Add("TOTAL");

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in CurrencyAmounts)
      {
        DataRow _aux_row = _dt_currency_amounts.NewRow();

        _aux_row["ISO_CODE"] = _amount.Key.IsoCode;
        _aux_row["TYPE"] = _amount.Key.Type;
        _aux_row["TOTAL"] = (Decimal)_amount.Value;

        _dt_currency_amounts.Rows.Add(_aux_row);
      }

      return CreateGameCollectedMovements(CashierInfo, _dt_currency_amounts, GamingTableSessionId, IsClose, OperationId, Trx);
    }

    public static Boolean CreateGameCollectedMovements(CashierSessionInfo CashierInfo, DataTable CurrencyAmountsTable, Int64 GamingTableSessionId, Boolean IsClose, SqlTransaction Trx)
    {
      return CreateGameCollectedMovements(CashierInfo, CurrencyAmountsTable, GamingTableSessionId, IsClose, 0, Trx);
    }

    public static Boolean CreateGameCollectedMovements(CashierSessionInfo CashierInfo, DataTable CurrencyAmountsTable, Int64 GamingTableSessionId, Boolean IsClose, Int64 OperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      CashierMovementsTable _new_cashier_movements;
      String _national_currency;
      CASHIER_MOVEMENT _movement_type;
      CurrencyExchangeType _type;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      _new_cashier_movements = new CashierMovementsTable(CashierInfo);
      _sb = new StringBuilder();

      try
      {

        _sb.AppendLine(" IF (@pNationalIsoCode = @pIsoCode AND @pCurrencyType = @pType) ");
        _sb.AppendLine(" BEGIN ");
        _sb.AppendLine("      SELECT (@pAmount - CS_BALANCE) AS AMOUNT_DIFF  ");
        _sb.AppendLine("       FROM CASHIER_SESSIONS  ");
        _sb.AppendLine("      WHERE CS_SESSION_ID = @pCashierSessionId ");
        if (!IsClose)
        {
          _sb.AppendLine("        AND (CS_BALANCE - @pAmount) < 0  ");
        }
        _sb.AppendLine(" END ");
        _sb.AppendLine(" ELSE ");
        _sb.AppendLine(" BEGIN ");

        _sb.AppendLine("   IF EXISTS(SELECT TOP 1 CSC_SESSION_ID   ");
        _sb.AppendLine("               FROM CASHIER_SESSIONS_BY_CURRENCY ");
        _sb.AppendLine("   	          WHERE CSC_SESSION_ID = @pCashierSessionId   ");
        _sb.AppendLine("                AND CSC_ISO_CODE   = @pIsoCode  ");
        _sb.AppendLine("                AND CSC_TYPE       = @pType)  ");
        _sb.AppendLine("   BEGIN ");

        _sb.AppendLine("    SELECT (@pAmount - CSC_BALANCE) AS AMOUNT_DIFF ");
        _sb.AppendLine("      FROM CASHIER_SESSIONS_BY_CURRENCY   ");
        _sb.AppendLine("     WHERE CSC_SESSION_ID = @pCashierSessionId ");
        _sb.AppendLine("       AND CSC_ISO_CODE = @pIsoCode  ");
        _sb.AppendLine("       AND CSC_TYPE = @pType  ");
        if (!IsClose)
        {
          _sb.AppendLine("       AND (CSC_BALANCE - @pAmount) < 0  ");
        }
        _sb.AppendLine("   END ");
        _sb.AppendLine("   ELSE  ");
        _sb.AppendLine("   BEGIN ");
        _sb.AppendLine("     SELECT @pAmount  ");
        _sb.AppendLine("   END ");
        _sb.AppendLine(" END ");

        _movement_type = CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN;
        if (IsClose)
        {
          _movement_type = CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE;
        }

        foreach (DataRow _dr in CurrencyAmountsTable.Rows)
        {

          _type = (CurrencyExchangeType)_dr["TYPE"];
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = _dr["TOTAL"];
            _cmd.Parameters.Add("@pIsoCode", SqlDbType.VarChar).Value = _dr["ISO_CODE"];
            _cmd.Parameters.Add("@pType", SqlDbType.VarChar).Value = _dr["TYPE"];
            _cmd.Parameters.Add("@pNationalIsoCode", SqlDbType.VarChar).Value = CurrencyExchange.GetNationalCurrency();
            _cmd.Parameters.Add("@pCurrencyType", SqlDbType.VarChar).Value = (Int32)CurrencyExchangeType.CURRENCY;
            _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierInfo.CashierSessionId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              // Execute Add Movement
              if (_reader.Read())
              {
                Decimal _amount = _reader.GetDecimal(0);
                if (_amount != 0)
                {
                  //DLL11111
                  if (_national_currency == (String)_dr["ISO_CODE"] && _type == CurrencyExchangeType.CURRENCY)
                  {
                    _new_cashier_movements.Add(OperationId, _movement_type, _amount, 0, String.Empty, String.Empty, String.Empty, 0, 0, GamingTableSessionId, -1, _type, null, null);
                  }
                  else if (_type == CurrencyExchangeType.CARD)
                  {
                    _new_cashier_movements.Add(OperationId, _movement_type, _amount, 0, String.Empty, String.Empty, (String)_dr["ISO_CODE"], Cage.BANK_CARD_CODE, 0, GamingTableSessionId, -1, _type, FeatureChips.ConvertToCageCurrencyType(_type), null);
                  }
                  else if (_type == CurrencyExchangeType.CHECK)
                  {
                    _new_cashier_movements.Add(OperationId, _movement_type, _amount, 0, String.Empty, String.Empty, (String)_dr["ISO_CODE"], Cage.CHECK_CODE, 0, GamingTableSessionId, -1, _type, FeatureChips.ConvertToCageCurrencyType(_type), null);
                  }
                  else
                  {
                    _new_cashier_movements.Add(OperationId, _movement_type, _amount, 0, String.Empty, String.Empty, (String)_dr["ISO_CODE"], 0, 0, GamingTableSessionId, -1, _type, FeatureChips.ConvertToCageCurrencyType(_type), null);
                  }
                }
              }
            }
          }
        }

        if (_new_cashier_movements.Save(Trx))
        {
          return true;
        }


      } //try

      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }
      return false;

    } // CreateGameCollectedMovements

    /// <summary>
    /// Return the SQL filter for the chips type
    /// </summary>
    /// <param name="ListCurrencyIsoType">List of CurrencyIsoType, if null the return all chip types</param>
    /// <returns></returns>
    public static String GetSqlFilterForChipsType(List<CurrencyIsoType> ListCurrencyIsoType)
    {
      String _filter;
      Boolean _list_X01;
      List<String> _list_filter;
      String _national_currency;

      _filter = String.Empty;
      _list_X01 = false;
      _list_filter = new List<String>();
      _national_currency = CurrencyExchange.GetNationalCurrency();

      //If ListCurrencyIsoType is null then show all chips
      if (ListCurrencyIsoType == null)
      {
        _filter = String.Format("((CM_CURRENCY_ISO_CODE = '{0}') OR ((CM_CURRENCY_ISO_CODE = '{1}') AND (CM_CAGE_CURRENCY_TYPE IN (1001, 1002, 1003))))", Cage.CHIPS_ISO_CODE, _national_currency);
        return _filter;
      }

      foreach (CurrencyIsoType _cur in ListCurrencyIsoType)
      {
        _list_filter.Add(String.Format("((CM_CURRENCY_ISO_CODE = '{0}') AND (CM_CAGE_CURRENCY_TYPE = {1}))", _cur.IsoCode, (Int32)_cur.Type));

        //Look if we are listing IsoCode = 'MXN' and CurrencyType = 1001
        if ((_cur.IsoCode == _national_currency) && (_cur.Type == CurrencyExchangeType.CASINO_CHIP_RE))
        {
          _list_X01 = true;
        }
      }

      //Concatenate all the chip filter
      if (_list_filter.Count > 0)
      {
        //If we are listing IsoCode = 'MXN' and CurrencyType = 1001 then show OLD chips movements
        if (_list_X01)
        {
          _list_filter.Insert(0, "(CM_CURRENCY_ISO_CODE = '" + Cage.CHIPS_ISO_CODE + "')");
        }

        _filter = String.Join(" OR ", _list_filter.ToArray());
      }

      if (_filter != String.Empty)
      {
        _filter = "(" + _filter + ")";
      }

      return _filter;
    }

    /// <summary>
    /// Returns the SQL filter for the amounts (no CHIPS)
    /// </summary>
    /// <returns></returns>
    public static String GetSqlFilterForAmount()
    {
      String _filter_amount;
      _filter_amount = "((CM_CURRENCY_ISO_CODE <> '" + Cage.CHIPS_ISO_CODE + "' OR CM_CURRENCY_ISO_CODE IS NULL) AND (CM_CAGE_CURRENCY_TYPE NOT IN (1001, 1002, 1003)) )";
      return _filter_amount;
    }

    #endregion Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Add movements for Chips Change operation
    //
    //  PARAMS :
    //      - INPUT : 
    //                Dictionary<Int64, Int32>: ChipsIn
    //                Dictionary<Int64, Int32>: ChipsOut
    //                CashierSessionInfo: CashierInfo
    //                SqlTransaction: Trx
    //                
    //      - OUTPUT : 
    //                
    //
    // RETURNS :
    //                Boolean: True if Integrated Chip and Credit Operations is enabled
    //
    //   NOTES :
    //
    private static Boolean AddChipsChangeMovements(Dictionary<Chips.Chip, Int32> ChipOut, Dictionary<Chips.Chip, Int32> ChipIn, CashierSessionInfo CashierSessionInfo, SqlTransaction Trx)
    {
      CashierMovementsTable _cashier_movements;
      Decimal _denomination;

      _cashier_movements = new CashierMovementsTable(CashierSessionInfo);

      //Chips in
      foreach (KeyValuePair<Chips.Chip, Int32> _chip_item in ChipIn)
      {
        if (_chip_item.Value > 0)
        {
          _denomination = 1;

          if (_chip_item.Key.Type != ChipType.COLOR)
          {
            _denomination = _chip_item.Key.Denomination;
          }

          _cashier_movements.Add(0, CASHIER_MOVEMENT.CHIPS_CHANGE_IN, _chip_item.Value * _denomination, 0,
                                  "", "", _chip_item.Key.IsoCode, _chip_item.Key.Denomination, 0,
                                  0, 0, FeatureChips.ConvertToCurrencyExchangeType(_chip_item.Key.Type), FeatureChips.ConvertToCageCurrencyType(_chip_item.Key.Type), _chip_item.Key.ChipId);
        }
      }

      //Chips out
      foreach (KeyValuePair<Chips.Chip, Int32> _chip_item in ChipOut)
      {
        if (_chip_item.Value > 0)
        {
          _denomination = 1;

          if (_chip_item.Key.Type != ChipType.COLOR)
          {
            _denomination = _chip_item.Key.Denomination;
          }

          _cashier_movements.Add(0, CASHIER_MOVEMENT.CHIPS_CHANGE_OUT, _chip_item.Value * _denomination, 0,
                                  "", "", _chip_item.Key.IsoCode, _chip_item.Key.Denomination, 0,
                                  0, 0, FeatureChips.ConvertToCurrencyExchangeType(_chip_item.Key.Type), FeatureChips.ConvertToCageCurrencyType(_chip_item.Key.Type), _chip_item.Key.ChipId);

        }
      }

      if (!_cashier_movements.Save(Trx))
      {
        return false;
      }

      return true;
    }
    // LTC 13-OCT-2016
    private static MultiPromos.AccountBalance CalculateBalance(MultiPromos.AccountBalance Initial, ChipsSplits Operation)
    {

      Decimal _cash_in_spit1_temp;
      Misc.TaxCustodyType TaxCustodyType;

      TYPE_SPLITS _splits;
      Split.ReadSplitParameters(out _splits);

      Operation.Redeemable = 0;

      if (Operation.IsIntegratedOperation)
      {
        // Calculate cash in splits
        Operation.CashInSplit1 = Math.Round(Operation.AmountToPay * _splits.company_a.chips_sale_pct / 100, 2, MidpointRounding.AwayFromZero);
        Operation.CashInSplit2 = Operation.AmountToPay - Operation.CashInSplit1;

        Operation.Redeemable = Operation.CashInSplit1 + Operation.CashInSplit2;

        Operation.CashInTaxSplit1 = 0;
        Operation.CashInTaxSplit2 = 0;
        if (GeneralParam.GetBoolean("GamingTables", "CashInTax.Enabled"))
        {
          Operation.CashInTaxSplit1 = Accounts.CalculateCashInTaxGamingTables(Operation.CashInSplit1);

          Operation.CashInSplit1 -= Operation.CashInTaxSplit1;
          Operation.Redeemable -= Operation.CashInTaxSplit1;

          if (GeneralParam.GetBoolean("GamingTables", "CashInTax.AlsoApplyToSplitB"))
          {
            Operation.CashInTaxSplit2 = Accounts.CalculateCashInTaxGamingTables(Operation.CashInSplit2);

            Operation.CashInSplit2 -= Operation.CashInTaxSplit2;
          }
        }

        Operation.TaxCustody = 0;
        TaxCustodyType = (Misc.TaxCustodyType)Misc.GetTaxCustodyType();


        switch (Misc.ApplyCustodyConfigurationCalculateRatio(Operation.IsIntegratedOperation))
        {
            case Misc.TaxCustodyMode.TaxOnSplit1:// CodereMode
              Operation.TaxCustody = Accounts.CalculateTaxCustody(Operation.CashInSplit1, true);
              Operation.CashInSplit1 -= Operation.TaxCustody;
              Operation.Redeemable -= Operation.TaxCustody;
              Operation.Redeemable -= Operation.CashInSplit2;

              break;
            case Misc.TaxCustodyMode.TaxOnDeposit:
              _cash_in_spit1_temp = Operation.CashInSplit1;
              Operation.CashInSplit1 = Math.Round((Operation.CashInSplit1 / (1 + Misc.GamingTablesTaxCustodyPct() / 100)), 2);
              Operation.TaxCustody = Accounts.CalculateTaxCustody(Operation.CashInSplit1, true);
              Operation.CashInSplit1 = _cash_in_spit1_temp - Operation.TaxCustody; // ensure we have the whole initial cashin_split1
              Operation.Redeemable -= Operation.TaxCustody;
              Operation.Redeemable -= Operation.CashInSplit2;

              break;
          default:
          case Misc.TaxCustodyMode.None:
          Operation.Redeemable = Math.Round(((Operation.AmountToPay - Operation.ChipsREAmount) * _splits.company_a.chips_sale_pct / 100), 2, MidpointRounding.AwayFromZero) - Operation.CashInTaxSplit1;
            break;
        }

        Operation.PromoRedeemable = Operation.AmountToPay - Operation.Redeemable - Operation.TaxCustody - Operation.CashInTaxSplit1 - Operation.CashInTaxSplit2;
        if (GeneralParam.GetBoolean("GamingTables", "Cashier.Mode"))
        {
          Operation.PromoRedeemable += Operation.ChipPrize;
        }
      }
      else
      {
        Operation.PromoRedeemable = Math.Min(Initial.PromoRedeemable, Operation.AmountToSale);
        Operation.Redeemable = Math.Min(Initial.Redeemable, Math.Max(0, Operation.AmountToSale - Operation.PromoRedeemable));
      }

      return new MultiPromos.AccountBalance(Operation.Redeemable, Operation.PromoRedeemable, 0, Operation.TaxCustody);
    }

    #endregion Private Methods

    public class ChipsOperation
    {

      #region Enums

      public enum Operation
      {
        NONE = 0,
        SALE = 1,
        PURCHASE = 2,
        CHANGE_IN = 3,
        CHANGE_OUT = 4,
        SHOW_STOCK = 5,
        SALE_REGISTER = 6,
        CAGE_CONCEPTS = 7,
        FILL_IN = 8,
        FILL_OUT = 9,
        OPEN_CASH = 10,
        CLOSE_CASH = 11,
        CAGE_REQUEST = 12,
        SWAP = 13,
        SALE_AMOUNT = 14,
        DROPBOX_COUNT = 15,
        WIN_LOSS = 16,
      }

      #endregion Enums

      #region Members

      private Operation m_operation_type;
      private CASHIER_MOVEMENT m_cashier_movement;
      private Decimal m_fills_chips_amount;
      private Decimal m_chips_amount;
      private Decimal m_chips_re_amount;
      private Decimal m_chips_tax_amount;
      private Decimal m_chips_net_amount;
      private String m_chips_iso_code;
      private CurrencyExchangeType m_currency_exchange_type;
      private Dictionary<Int64, Int32> m_chips_operation = new Dictionary<long, int>();
      private LinkedGamingTable m_selected_gaming_table;
      private Boolean m_print_ticket;
      private Boolean m_is_chips_sale_register; // Deberia utilizarse con el ChipsOperation
      private Boolean m_is_apply_taxes;
      private Int64 m_account_operation_reason_id;
      private String m_account_operation_comment;
      private CurrencyExchange m_currency_exchange;
      private RoundingSaleChips m_rounding_sale_chips;
      private Decimal m_devolution_rounding_sale_chips;

      #endregion Members

      #region Properties

      public Operation OperationType
      {
        get { return m_operation_type; }
        set { m_operation_type = value; }
      }

      public CASHIER_MOVEMENT MovementType
      {
        get { return m_cashier_movement; }
        set { m_cashier_movement = value; }
      }

      public Currency FillChipsAmount
      {
        get { return m_fills_chips_amount; }
        set { m_fills_chips_amount = value; }
      }

      public Currency ChipsAmount
      {
        get { return m_chips_amount; }
        set { m_chips_amount = value; }
      }

      public Currency ChipsREAmount
      {
        get { return m_chips_re_amount; }
        set { m_chips_re_amount = value; }
      }

      public Currency ChipsTaxAmount
      {
        get { return m_chips_tax_amount; }
        set { m_chips_tax_amount = value; }
      }  

      public Currency ChipsNetAmount  
      {
        get { return m_chips_net_amount; }
        set { m_chips_net_amount = value; }
      }  

      private Result m_cash_desk_draw_result;

      public Result CashDeskDrawResult
      {
        get { return m_cash_desk_draw_result; }
        set { m_cash_desk_draw_result = value; }
      }

      public Currency InitialAmountNationalCurrency
      {
        get
        {
          if (m_currency_exchange_type == CurrencyExchangeType.CASINO_CHIP_COLOR)
          {
            return 0;
          }
          else if (m_chips_iso_code == CurrencyExchange.GetNationalCurrency() || m_chips_iso_code == String.Empty)
          {
            return m_fills_chips_amount;
          }
          else
          {
            CurrencyExchangeResult _result;

            m_currency_exchange.ApplyExchange(m_fills_chips_amount, out _result);

            return _result.GrossAmount;
          }
        }
      }

      public Currency AmountNationalCurrency
      {
        get
        {
          if (m_currency_exchange_type == CurrencyExchangeType.CASINO_CHIP_COLOR)
          {
            return 0;
          }
          else if (m_chips_iso_code == CurrencyExchange.GetNationalCurrency() || m_chips_iso_code == String.Empty)
          {
            return m_chips_amount;
          }
          else
          {
            CurrencyExchangeResult _result;

            m_currency_exchange.ApplyExchange(m_chips_amount, out _result);

            return _result.GrossAmount;
          }
        }
      }

      public Currency ChipsDifference
      {
        get { return m_fills_chips_amount - m_chips_amount; }
      }

      public Currency AmountNationalCurrencyDifference
      {
        get { return InitialAmountNationalCurrency - AmountNationalCurrency; }
      }


      public LinkedGamingTable SelectedGamingTable
      {
        get { return m_selected_gaming_table; }
        set { m_selected_gaming_table = value; }
      }

      public Boolean PrintTicket
      {
        get { return m_print_ticket; }
        set { m_print_ticket = value; }
      }

      public Boolean IsChipsSaleRegister
      {
        get { return m_is_chips_sale_register; }
        set { m_is_chips_sale_register = value; }
      }

      public Dictionary<Int64, Int32> ChipsUnits
      {
        get
        {
          return m_chips_operation;
        }
      }

      public Dictionary<FeatureChips.Chips.Chip, Int32> ChipsItemsUnits
      {
        get
        {
          return GetChipsInfoUnits();
        }
      }

      public String IsoCode
      {
        get
        {
          return m_chips_iso_code;
        }

        set
        {
          if (m_chips_iso_code != value)
          {
            m_chips_iso_code = value;

            CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, m_chips_iso_code, out m_currency_exchange);
          }
        }
      }

      public CurrencyExchangeType CurrencyExchangeType
      {
        get { return m_currency_exchange_type; }
        set { m_currency_exchange_type = value; }
      }

      public Boolean IsApplyTaxes
      {
        get { return m_is_apply_taxes; }
        set { m_is_apply_taxes = value; }
      }

      public Int64 AccountOperationReasonId
      {
        get { return m_account_operation_reason_id; }
        set { m_account_operation_reason_id = value; }
      }

      public String AccountOperationComment
      {
        get { return m_account_operation_comment; }
        set { m_account_operation_comment = value; }
      }

      public RoundingSaleChips RoundingSaleChips
      {
        get { return m_rounding_sale_chips; }
        set { m_rounding_sale_chips = value; }
      }

      public Decimal DevolutionRoundingSaleChips
      {
        get { return m_devolution_rounding_sale_chips; }
        set { m_devolution_rounding_sale_chips = value; }
      }

      #endregion Properties

      #region Public Methods

      //------------------------------------------------------------------------------
      // PURPOSE : Constructor ChipsOperation
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64: SessionId
      //                CASHIER_MOVEMENT: CashierMovement
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public ChipsOperation(Int64 SessionId, CASHIER_MOVEMENT CashierMovement)
        : this(SessionId, 0, 0, 0, CurrencyExchange.GetNationalCurrency(), CashierMovement)
      {
      }

      public ChipsOperation(Int64 SessionId, Decimal Total, CASHIER_MOVEMENT CashierMovement)
        : this(SessionId, 0, Total, 0, CurrencyExchange.GetNationalCurrency(), CashierMovement)
      {
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Constructor ChipsOperation
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64: SessionId
      //                Decimal: Total
      //                CASHIER_MOVEMENT: CashierMovement
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public ChipsOperation(Int64 SessionId, Int64 OperationId, Decimal Total, Decimal PromoRE, String IsoCode, CASHIER_MOVEMENT CashierMovement)
      {
        m_fills_chips_amount = Total;
        m_chips_amount = 0;
        m_cashier_movement = CashierMovement;

        m_chips_operation = new Dictionary<long, int>();

        this.IsoCode = IsoCode;
        m_currency_exchange_type = Common.CurrencyExchangeType.CASINO_CHIP_RE;

        switch (CashierMovement)
        {
          case CASHIER_MOVEMENT.CHIPS_SALE:
            m_operation_type = Operation.SALE;
            break;
          case CASHIER_MOVEMENT.CHIPS_PURCHASE:
            m_operation_type = Operation.PURCHASE;
            break;
          case CASHIER_MOVEMENT.CHIPS_CHANGE_IN:
            m_operation_type = Operation.CHANGE_IN;
            break;
          case CASHIER_MOVEMENT.CHIPS_CHANGE_OUT:
            m_operation_type = Operation.CHANGE_OUT;
            break;
          // LTC 07-MAR-2016
          case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_SWAP_FOR_CHIPS:
            m_operation_type = Operation.SWAP;
            break;
          default:
            m_operation_type = Operation.SHOW_STOCK;
            break;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          ReloadChipsAndSets();

          if (m_operation_type == Operation.SALE || m_operation_type == Operation.CHANGE_OUT || m_operation_type == Operation.SHOW_STOCK || m_operation_type == Operation.SWAP)
          {
            if (!FeatureChips.Chips.ReadStock(SessionId, _db_trx.SqlTransaction))
            {
              throw new Exception(String.Format("FeatureChips.Chips.ReadStock error: Movement {0}, SessionId {1}, Amount {2}",
                                                CashierMovement, SessionId, Total));
            }
          }

          if (OperationId > 0)
          {
            if (!LoadChipsOperation(SessionId, OperationId, _db_trx.SqlTransaction))
            {
              throw new Exception(String.Format("FeatureChips.ChipsOperation.LoadChipsOperation error: Movement {0}, SessionId {1}, OperationId {2}",
                                                CashierMovement, SessionId, OperationId));
            }
          }
        }

        if (m_operation_type == Operation.SALE || m_operation_type == Operation.SWAP)
        {
          if (m_fills_chips_amount > 0)
          {
            m_chips_amount = m_fills_chips_amount;
            m_chips_re_amount = PromoRE;
          }
          m_rounding_sale_chips = new RoundingSaleChips(m_fills_chips_amount + PromoRE, false);
        }

      }

      //------------------------------------------------------------------------------
      // PURPOSE : Add Chip and its units on Chip Operation
      //
      //  PARAMS :
      //      - INPUT : 
      //                FeatureChips.Chips.Chip: Chip
      //                Int32: Units
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public void AddChip(FeatureChips.Chips.Chip Chip, Int32 Units)
      {
        AddChip(Chip.ChipId, Units);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Add Chip and its units on Chip Operation
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64: ChipId
      //                Int32: Units
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public void AddChip(Int64 ChipId, Int32 Units)
      {
        if (m_chips_operation.ContainsKey(ChipId))
        {
          m_chips_operation[ChipId] += Units;
        }
        else
        {
          m_chips_operation.Add(ChipId, Units);
        }
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Remove Chip on Chip Operation
      //
      //  PARAMS :
      //      - INPUT : 
      //                FeatureChips.Chips.Chip: Chip
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public void RemoveChip(FeatureChips.Chips.Chip Chip)
      {
        RemoveChip(Chip.ChipId);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Remove Chip on Chip Operation
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64: ChipId
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public void RemoveChip(Int64 ChipId)
      {
        if (m_chips_operation.ContainsKey(ChipId))
        {
          m_chips_operation.Remove(ChipId);
        }
      }


      #endregion Public Methods

      #region Private Methods

      //------------------------------------------------------------------------------
      // PURPOSE : Get Chips and its data units from chips operation
      //
      //  PARAMS :
      //      - INPUT : 
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Dictionary<FeatureChips.Chips.Chip, Int32>
      //
      //   NOTES :
      //
      private Dictionary<FeatureChips.Chips.Chip, Int32> GetChipsInfoUnits()
      {
        Dictionary<FeatureChips.Chips.Chip, Int32> _dic;

        _dic = new Dictionary<Chips.Chip, Int32>();

        foreach (KeyValuePair<Int64, Int32> _item in m_chips_operation)
        {
          _dic.Add(FeatureChips.Chips.GetChip(_item.Key), _item.Value);
        }

        return _dic;
      }

      private Boolean LoadChipsOperation(Int64 CashierSessionId, Int64 OperationId, SqlTransaction Trx)
      {
        StringBuilder _sb;

        try
        {
          _sb = new StringBuilder();

          _sb.AppendLine("   SELECT   CM.CM_CURRENCY_ISO_CODE ");
          _sb.AppendLine("          , CM.CM_CAGE_CURRENCY_TYPE ");
          _sb.AppendLine("          , CM.CM_CHIP_ID ");
          _sb.AppendLine("          , CASE ");
          _sb.AppendLine("               WHEN (CM.CM_CURRENCY_DENOMINATION IS NULL OR CM.CM_CURRENCY_DENOMINATION = 0) AND CM.ADD_AMOUNT = 0 THEN CM.SUB_AMOUNT ");
          _sb.AppendLine("               WHEN (CM.CM_CURRENCY_DENOMINATION IS NULL  OR CM.CM_CURRENCY_DENOMINATION = 0) AND CM.SUB_AMOUNT = 0 THEN CM.ADD_AMOUNT ");
          _sb.AppendLine("               WHEN CM.ADD_AMOUNT = 0 THEN (CM.SUB_AMOUNT / CM.CM_CURRENCY_DENOMINATION) ");
          _sb.AppendLine("               WHEN CM.SUB_AMOUNT = 0 THEN (CM.ADD_AMOUNT / CM.CM_CURRENCY_DENOMINATION) ");
          _sb.AppendLine("               ELSE NULL ");
          _sb.AppendLine("           END AS UNITS ");
          _sb.AppendLine("          , CM.CM_CURRENCY_DENOMINATION ");

          _sb.AppendLine("     FROM ( ");
          _sb.AppendLine("            SELECT   SUM(CM_ADD_AMOUNT) AS ADD_AMOUNT ");
          _sb.AppendLine("                   , SUM(CM_SUB_AMOUNT) AS SUB_AMOUNT ");
          _sb.AppendLine("                   , SUM(CM_INITIAL_BALANCE) AS INITIAL_BALANCE ");
          _sb.AppendLine("                   , CM_CURRENCY_DENOMINATION ");
          _sb.AppendLine("                   , CM_CAGE_CURRENCY_TYPE ");
          _sb.AppendLine("                   , CM_CURRENCY_ISO_CODE ");
          _sb.AppendLine("                   , CM_CHIP_ID ");
          _sb.AppendLine("              FROM   CASHIER_MOVEMENTS WITH(INDEX(IX_cm_operation_id)) ");
          _sb.AppendLine("             INNER   JOIN CHIPS ON  CH_CHIP_ID = CM_CHIP_ID ");
          _sb.AppendLine("             WHERE   CM_OPERATION_ID      = @pOperationId ");
          _sb.AppendLine("               AND   CM_SESSION_ID        = @pSessionId ");
          _sb.AppendLine("               AND   CH_DENOMINATION      = CM_CURRENCY_DENOMINATION ");
          _sb.AppendLine("               AND   CH_ALLOWED           = 1 ");
          _sb.AppendLine("               AND   CM_TYPE              IN  ( @pFillIn, @pChipsPurchase, @pChipsChangeIn, @pFillOut, @pChipsSale, @pChipsChangeOut) ");
          _sb.AppendLine("            GROUP BY CM_CURRENCY_ISO_CODE, CM_CAGE_CURRENCY_TYPE, CM_CHIP_ID, CM_CURRENCY_DENOMINATION ");
          _sb.AppendLine("          ) AS CM ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
            _sql_cmd.Parameters.Add("@pFillIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_IN;
            _sql_cmd.Parameters.Add("@pChipsPurchase", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_PURCHASE;
            _sql_cmd.Parameters.Add("@pChipsChangeIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_CHANGE_IN;
            _sql_cmd.Parameters.Add("@pFillOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_OUT;
            _sql_cmd.Parameters.Add("@pChipsSale", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE;
            _sql_cmd.Parameters.Add("@pChipsChangeOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_CHANGE_OUT;
            _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.Int).Value = OperationId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                this.IsoCode = _reader.GetString(0);
                this.CurrencyExchangeType = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_reader.GetInt32(1));

                // Add chip
                this.AddChip(_reader.GetInt64(2), (Int32)_reader.GetDecimal(3));

                if (this.CurrencyExchangeType == CurrencyExchangeType.CASINO_CHIP_COLOR)
                {
                  this.ChipsAmount += _reader.GetDecimal(3);
                }
                else
                {
                  this.ChipsAmount += _reader.GetDecimal(3) * _reader.GetDecimal(4);
                }
              }
            }
          }

          return true;
        }
        catch (SqlException _sql_ex)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return false;
      }

      #endregion Private Methods

    }

    public static class Chips
    {
      #region Members

      private static Chips.Dictionary m_chips = Chips.Dictionary.Create((Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1));

      #endregion Members

      #region Properties

      #endregion Properties

      #region Contructors

      #endregion Contructors

      #region Public Methods

      //------------------------------------------------------------------------------
      // PURPOSE : Create Chips Dictionaru
      //
      //  PARAMS :
      //      - INPUT : 
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public static void Create(Cage.ShowDenominationsMode DenominationsMode)
      {
        m_chips = Chips.Dictionary.Create(DenominationsMode);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Remove Chip on Chip Operation
      //
      //  PARAMS :
      //      - INPUT : 
      //                SqlTransaction: Trx
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public static void Create(SqlTransaction Trx)
      {
        Chips.Dictionary.Create(Trx);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Get Chip data
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64: ChipId
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                FeatureChips.Chips.Chip
      //
      //   NOTES :
      //
      public static Chip GetChip(Int64 ChipId)
      {
        try
        {
          return m_chips[ChipId];
        }
        catch
        {
          return null;
        }
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Clear all chips stock
      //
      //  PARAMS :
      //      - INPUT : 
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public static void ClearStock()
      {
        foreach (KeyValuePair<Int64, Chip> _item in m_chips)
        {
          _item.Value.Stock = 0;
        }
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Get Chip data from list of chips id
      //
      //  PARAMS :
      //      - INPUT : 
      //                List<Int64>: ChipsId
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Dictionary<Int64, Chip>
      //
      //   NOTES :
      //
      public static Dictionary<Int64, Chip> GetChips(List<Int64> ChipsId)
      {
        Dictionary<Int64, Chip> _chips;
        Chip _chip;

        _chips = new Dictionary<long, Chip>();

        foreach (Int64 _chip_id in ChipsId)
        {
          if (m_chips.ContainsKey(_chip_id))
          {
            _chip = m_chips[_chip_id];
            _chips.Add(_chip.ChipId, _chip);
          }
        }

        return _chips;
      }

      //------------------------------------------------------------------------------
      // PURPOSE : validate if chip exists on Chips Dictionary
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64: ChipId
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Boolean: true if the chips exists
      //
      //   NOTES :
      //
      public static Boolean ContainsKey(Int64 ChipId)
      {
        try
        {
          return m_chips.ContainsKey(ChipId);
        }
        catch
        {
          return false;
        }
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Gets all the chips by chips type (CurrencyExchangeType)
      //
      //  PARAMS :
      //      - INPUT : 
      //                CurrencyExchangeType: Type
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Dictionary<Int64, Chip>
      //
      //   NOTES :
      //
      public static SortedDictionary<Int64, Chip> GetChipsByType(CurrencyExchangeType Type)
      {
        SortedDictionary<Int64, Chip> _chips;

        _chips = new SortedDictionary<Int64, Chip>(new FeatureChips.Chips.Chip.SortComparer());

        foreach (KeyValuePair<Int64, Chip> _chip_item in m_chips)
        {
          if (_chip_item.Value.Type == FeatureChips.ConvertToChipType(Type))
          {
            _chips.Add(_chip_item.Key, _chip_item.Value);
          }
        }

        return _chips;
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Gets all the chips
      //
      //  PARAMS :
      //      - INPUT : 
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Dictionary<Int64, Chips.Chip>
      //
      //   NOTES :
      //
      public static Dictionary<Int64, Chips.Chip> GetChips()
      {
        return m_chips;
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Read chips stock for a SessionId
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64: SessionId
      //                SqlTransaction: Trx
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Boolean: if stock can be readed
      //
      //   NOTES :
      //
      public static Boolean ReadStock(Int64 SessionId, SqlTransaction Trx)
      {
        // DLL 13-JAN-2014: Only read stock when is not gaming table
        if (FeatureChips.IsChipsStockControlEnabled && !GamingTablesSessions.GamingTableInfo.IsGamingTable ||
          !FeatureChips.IsChipsSaleByTicket)
        {
          if (!Chips.GetStock(SessionId, Trx))
          {
            return false;
          }
        }

        return true;
      }

      #endregion Public Methods

      #region Private Methods

      //------------------------------------------------------------------------------
      // PURPOSE : Get chips stock for a SessionId
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64: SessionId
      //                SqlTransaction: Trx
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Boolean: if stock can be readed
      //
      //   NOTES :
      //
      private static Boolean GetStock(Int64 SessionId, SqlTransaction Trx)
      {
        return GetStock(SessionId, 0, Trx);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Get chips stock for a SessionId and OperationId
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64: SessionId
      //                Int64: OperationId,
      //                SqlTransaction: Trx
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Boolean: if stock can be readed
      //
      //   NOTES :
      //
      private static Boolean GetStock(Int64 SessionId, Int64 OperationId, SqlTransaction Trx)
      {
        StringBuilder _sb;
        DataTable _movements;
        Decimal _denomination;
        Int32 _units;
        Int64 _chip_id;
        String _iso_code;
        CASHIER_MOVEMENT _cashier_movement;
        Boolean _chips_sale_mode;
        Chip _chip;
        Cage.ShowDenominationsMode _denominations_mode;

        _chips_sale_mode = GeneralParam.GetBoolean("GamingTables", "Cashier.Mode");
        _denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);

        if (m_chips == null || m_chips.Count == 0)
        {
          Chips.Dictionary.Create(_denominations_mode);
        }

        Chips.ClearStock();

        try
        {
          _sb = new StringBuilder();

          _sb.AppendLine("   SELECT   CM.CM_TYPE ");
          _sb.AppendLine("          , CM.CM_CURRENCY_DENOMINATION ");
          _sb.AppendLine("          , CASE ");
          _sb.AppendLine("               WHEN (CM.CM_CURRENCY_DENOMINATION IS NULL OR CM.CM_CURRENCY_DENOMINATION = 0) AND CM.ADD_AMOUNT = 0 THEN CM.SUB_AMOUNT ");
          _sb.AppendLine("               WHEN (CM.CM_CURRENCY_DENOMINATION IS NULL  OR CM.CM_CURRENCY_DENOMINATION = 0) AND CM.SUB_AMOUNT = 0 THEN CM.ADD_AMOUNT ");
          _sb.AppendLine("               WHEN CM.ADD_AMOUNT = 0 THEN (CM.SUB_AMOUNT / CM.CM_CURRENCY_DENOMINATION) ");
          _sb.AppendLine("               WHEN CM.SUB_AMOUNT = 0 THEN (CM.ADD_AMOUNT / CM.CM_CURRENCY_DENOMINATION) ");
          _sb.AppendLine("               ELSE NULL ");
          _sb.AppendLine("           END AS UNITS ");
          _sb.AppendLine("          , CM.CM_CHIP_ID ");
          _sb.AppendLine("          , CM.CM_CURRENCY_ISO_CODE ");
          _sb.AppendLine("     FROM ( ");
          _sb.AppendLine("            SELECT   SUM(CM_ADD_AMOUNT) AS ADD_AMOUNT ");
          _sb.AppendLine("                   , SUM(CM_SUB_AMOUNT) AS SUB_AMOUNT ");
          _sb.AppendLine("                   , SUM(CM_INITIAL_BALANCE) AS INITIAL_BALANCE ");
          _sb.AppendLine("                   , CM_TYPE ");
          _sb.AppendLine("                   , CM_CURRENCY_DENOMINATION ");
          _sb.AppendLine("                   , CM_CHIP_ID ");
          _sb.AppendLine("                   , CM_CURRENCY_ISO_CODE ");

          if (OperationId > 0)
          {
            _sb.AppendLine("              FROM   CASHIER_MOVEMENTS WITH(INDEX(IX_cm_operation_id)) ");
          }
          else
          {
            _sb.AppendLine("              FROM   CASHIER_MOVEMENTS WITH(INDEX(IX_cm_session_id)) ");
          }

          _sb.AppendLine("             INNER   JOIN CHIPS ON  CM_CHIP_ID = CH_CHIP_ID ");

          if (OperationId > 0)
          {
            _sb.AppendLine("             WHERE   CM_OPERATION_ID      =  @pOperationId ");
            _sb.AppendLine("               AND   CM_SESSION_ID        = @pSessionId ");
          }
          else
          {
            _sb.AppendLine("             WHERE   CM_SESSION_ID        = @pSessionId ");
          }

          _sb.AppendLine("               AND   CH_ALLOWED           = 1 ");
          _sb.AppendLine("               AND   CM_TYPE              IN  ( @pFillIn, @pChipsPurchase, @pChipsChangeIn, @pFillOut, @pChipsSale, @pChipsChangeOut) ");

          _sb.AppendLine("            GROUP BY CM_TYPE, CM_CURRENCY_DENOMINATION, CM_CHIP_ID, CM_CURRENCY_ISO_CODE ");
          _sb.AppendLine("          ) AS CM ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
            _cmd.Parameters.Add("@pFillIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_IN;
            _cmd.Parameters.Add("@pChipsPurchase", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_PURCHASE;
            _cmd.Parameters.Add("@pChipsChangeIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_CHANGE_IN;
            _cmd.Parameters.Add("@pFillOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_OUT;
            _cmd.Parameters.Add("@pChipsSale", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE;
            _cmd.Parameters.Add("@pChipsChangeOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_CHANGE_OUT;
            if (OperationId > 0)
            {
              _cmd.Parameters.Add("@pOperationId", SqlDbType.Int).Value = OperationId;
            }

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _movements = new DataTable();
              _sql_da.Fill(_movements);
            }
          }

          foreach (DataRow _row in _movements.Rows)
          {
            _cashier_movement = (CASHIER_MOVEMENT)_row[0];
            // TODO: no seria necesario
            _denomination = (Decimal)_row[1];
            _units = (Int32)(Decimal)_row[2];
            _chip_id = (Int64)_row[3];
            // TODO: no seria necesario
            _iso_code = (String)_row[4];

            _chip = Chips.GetChip(_chip_id);

            if (_chip == null)
            {
              continue;
            }

            switch (_cashier_movement)
            {
              case CASHIER_MOVEMENT.CAGE_FILLER_IN:
              case CASHIER_MOVEMENT.CHIPS_PURCHASE:
              case CASHIER_MOVEMENT.CHIPS_CHANGE_IN:
                _chip.Stock += _units;
                break;

              case CASHIER_MOVEMENT.CAGE_FILLER_OUT:
              case CASHIER_MOVEMENT.CHIPS_CHANGE_OUT:
                _chip.Stock -= _units;
                break;

              case CASHIER_MOVEMENT.CHIPS_SALE:
                if (!_chips_sale_mode)
                {
                  _chip.Stock -= _units;
                }
                break;

              default:
                break;
            }
          }

          return true;
        }
        catch (SqlException _sql_ex)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return false;
      }

      #endregion Private Methods

      private class Dictionary : Dictionary<Int64, Chips.Chip>
      {

        #region Constructors

        public Dictionary()
          : base()
        {
        }

        #endregion Constructors

        #region Public Methods

        //------------------------------------------------------------------------------
        // PURPOSE : Create Chips Dictionary
        //
        //  PARAMS :
        //      - INPUT : 
        //                
        //      - OUTPUT : 
        //                
        //
        // RETURNS :
        //                Dictionary
        //
        //   NOTES :
        //
        public static Dictionary Create(Cage.ShowDenominationsMode DenominationsMode)
        {
          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              return LoadChips(DenominationsMode, _db_trx.SqlTransaction);
            }
          }
          catch
          {
            return null;
          }
        }

        //------------------------------------------------------------------------------
        // PURPOSE : Create Chips Dictionary
        //
        //  PARAMS :
        //      - INPUT : 
        //                SqlTransaction: Trx
        //                
        //      - OUTPUT : 
        //                
        //
        // RETURNS :
        //                Dictionary
        //
        //   NOTES :
        //
        public static Dictionary Create(SqlTransaction Trx)
        {
          Cage.ShowDenominationsMode _denominations_mode;
          try
          {
            _denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);
            return LoadChips(_denominations_mode, Trx);
          }
          catch
          {
            return null;
          }
        }

        #endregion Public Methods

        #region Private Methods

        //------------------------------------------------------------------------------
        // PURPOSE : Load Chips data from DB
        //
        //  PARAMS :
        //      - INPUT : 
        //                SqlTransaction: Trx
        //                
        //      - OUTPUT : 
        //                
        //
        // RETURNS :
        //                Dictionary
        //
        //   NOTES :
        //
        private static Dictionary LoadChips(Cage.ShowDenominationsMode DenominationsMode, SqlTransaction Trx)
        {
          Dictionary _dic;
          StringBuilder _sb;

          try
          {
            _dic = new Dictionary();
            _sb = new StringBuilder();

            _sb.AppendLine("SELECT   CH_CHIP_ID ");
            _sb.AppendLine("       , CH_ISO_CODE ");
            _sb.AppendLine("       , CH_DENOMINATION ");
            _sb.AppendLine("       , CH_ALLOWED ");
            _sb.AppendLine("       , ISNULL(CH_COLOR, 0) ");
            _sb.AppendLine("       , CH_NAME ");
            _sb.AppendLine("       , CH_DRAWING ");
            _sb.AppendLine("       , CH_CHIP_TYPE ");
            _sb.AppendLine("  FROM   CHIPS ");
            _sb.AppendLine(" WHERE   CH_ALLOWED = 1 ");

            if (DenominationsMode == Cage.ShowDenominationsMode.HideAllDenominations)
            {
              _sb.AppendLine("AND ( CH_CHIP_ID IN (");

              _sb.AppendLine("                    SELECT MIN(B.CH_CHIP_ID) ");
              _sb.AppendLine("                      FROM CHIPS AS B  ");
              _sb.AppendLine("                      INNER JOIN CHIPS_SETS_CHIPS AS J ON B.CH_CHIP_ID= J.CSC_CHIP_ID ");
              _sb.AppendLine("                      INNER JOIN ( SELECT MIN(C.CH_DENOMINATION) AS DENOMINATION, C.CH_ISO_CODE, C.CH_CHIP_TYPE ");
              _sb.AppendLine("                              FROM CHIPS AS C  ");
              _sb.AppendLine("                              INNER JOIN CHIPS_SETS_CHIPS AS D ON C.CH_CHIP_ID= D.CSC_CHIP_ID ");
              _sb.AppendLine("                              INNER JOIN CHIPS_SETS AS E ON E.CHS_CHIP_SET_ID = D.CSC_SET_ID ");
              _sb.AppendLine("                              WHERE (C.CH_DENOMINATION >= 1 OR C.CH_ISO_CODE = '" + Cage.CHIPS_COLOR + "') ");
              _sb.AppendLine("                              AND E.CHS_ALLOWED = 1  ");
              _sb.AppendLine("                              GROUP BY C.CH_ISO_CODE, C.CH_CHIP_TYPE ");
              _sb.AppendLine("                              ) AS H ON H.CH_ISO_CODE = B.CH_ISO_CODE AND H.CH_CHIP_TYPE = B.CH_CHIP_TYPE AND H.DENOMINATION = B.CH_DENOMINATION ");
              _sb.AppendLine("                      GROUP BY B.CH_ISO_CODE, B.CH_CHIP_TYPE, B.CH_DENOMINATION ");

              _sb.AppendLine(") )");

            }

            _sb.AppendLine(" ORDER   BY CH_CHIP_ID ASC ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
            {
              using (SqlDataReader _reader = _cmd.ExecuteReader())
              {
                while (_reader.Read())
                {
                  // Create chip
                  Chip _chip = new Chip();

                  _chip.ChipId = _reader.GetInt64(0);
                  _chip.IsoCode = _reader.GetString(1);

                  if (_reader.IsDBNull(2))
                  {
                    _chip.Denomination = 0;
                  }
                  else
                  {
                    if (DenominationsMode == Cage.ShowDenominationsMode.HideAllDenominations)
                    {
                      _chip.Denomination = 1;
                    }
                    else
                    {
                      _chip.Denomination = _reader.GetDecimal(2);
                    }

                  }
                  _chip.Allowed = _reader.GetBoolean(3);

                  if (_reader.IsDBNull(4))
                  {
                    _chip.Color = 0;
                  }
                  else
                  {
                    _chip.Color = _reader.GetInt32(4);
                  }

                  if (_reader.IsDBNull(5))
                  {
                    _chip.Name = String.Empty;
                  }
                  else
                  {
                    _chip.Name = _reader.GetString(5);
                  }

                  if (_reader.IsDBNull(6))
                  {
                    _chip.Name = String.Empty;
                  }
                  else
                  {
                    _chip.Drawing = _reader.GetString(6);
                  }

                  if (_reader.IsDBNull(7))
                  {
                    _chip.Type = ChipType.RE;
                  }
                  else
                  {
                    _chip.Type = (ChipType)_reader.GetInt32(7);
                  }

                  _dic.Add(_chip.ChipId, _chip);
                }
              }
            }
            return _dic;
          }
          catch (SqlException _sql_ex)
          {
            Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          return null;
        }

        #endregion Private Methods
      }

      public class Chip
      {
        #region Members

        private Int64 m_chip_id;
        private String m_iso_code;
        private Decimal m_denomination;
        private Boolean m_allowed;
        private Int64 m_color;
        private String m_name;
        private String m_drawing;
        private Int32 m_stock;
        private ChipType m_type;

        #endregion Members

        #region Properties

        public Int64 ChipId
        {
          get { return m_chip_id; }
          set { m_chip_id = value; }
        }

        public String IsoCode
        {
          get { return m_iso_code; }
          set { m_iso_code = value; }
        }

        public Decimal Denomination
        {
          get { return m_denomination; }
          set { m_denomination = value; }
        }

        public Boolean Allowed
        {
          get { return m_allowed; }
          set { m_allowed = value; }
        }

        public Int64 Color
        {
          get { return m_color; }
          set { m_color = value; }
        }

        public String Name
        {
          get { return m_name; }
          set { m_name = value; }
        }

        public String Drawing
        {
          get { return m_drawing; }
          set { m_drawing = value; }
        }
        public Int32 Stock
        {
          get { return m_stock; }
          set { m_stock = value; }
        }

        public ChipType Type
        {
          get { return m_type; }
          set { m_type = value; }
        }

        #endregion Properties

        #region Contructors

        //------------------------------------------------------------------------------
        // PURPOSE : Constructor
        //
        //  PARAMS :
        //      - INPUT : 
        //                
        //      - OUTPUT : 
        //                
        //
        // RETURNS :
        //
        //   NOTES :
        //
        public Chip()
        {
        }

        //------------------------------------------------------------------------------
        // PURPOSE : Constructor
        //
        //  PARAMS :
        //      - INPUT : 
        //                Int64 ChipId
        //                String ISOCode
        //                Decimal Denomination
        //                Int64 Color
        //                String Name
        //                String Drawing
        //                CurrencyExchangeType Type
        //                Boolean Allowed
        //                Int32 Stock
        //                
        //      - OUTPUT : 
        //                
        //
        // RETURNS :
        //
        //   NOTES :
        //
        public Chip(Int64 ChipId, String ISOCode, Decimal Denomination, Int64 Color, String Name, String Drawing, ChipType Type, Boolean Allowed, Int32 Stock)
        {
          this.m_chip_id = ChipId;
          this.m_iso_code = ISOCode;
          this.m_denomination = Denomination;
          this.m_color = Color;
          this.m_name = Name;
          this.m_drawing = Drawing;
          this.m_type = Type;
          this.m_allowed = Allowed;
          this.m_stock = Stock;
        }

        //------------------------------------------------------------------------------
        // PURPOSE : Constructor
        //
        //  PARAMS :
        //      - INPUT : 
        //                Int64 ChipId
        //                String ISOCode
        //                Decimal Denomination
        //                Int64 Color
        //                String Name
        //                String Drawing
        //                CurrencyExchangeType Type
        //                Boolean Allowed
        //                
        //      - OUTPUT : 
        //                
        //
        // RETURNS :
        //
        //   NOTES :
        //
        public Chip(Int64 ChipId, String ISOCode, Decimal Denomination, Int64 Color, String Name, String Drawing, ChipType Type, Boolean Allowed)
        {
          this.m_chip_id = ChipId;
          this.m_iso_code = ISOCode;
          this.m_denomination = Denomination;
          this.m_color = Color;
          this.m_name = Name;
          this.m_drawing = Drawing;
          this.m_type = Type;
          this.m_allowed = Allowed;
          this.m_stock = 0;
        }
        #endregion Contructors

        #region Public Methods

        //public Boolean UpdateStock(Int32 Value)
        //{
        //  if (this.Stock + Value >= 0)
        //  {
        //    this.Stock += Value;

        //    return true;
        //  }

        //  return false;
        //}

        #endregion Public Methods

        public class SortComparer : IComparer<Int64>
        {
          // PURPOSE: Implements Compare method to sort Chip objects
          //
          // PARAMS:
          //     - INPUT:
          //           - Chip X: First element to compare
          //           - Chip Y: Second element to compare
          //
          // RETURNS:
          //       0: If both objects are equals
          //      -1: If x is less than y
          //       1: If x is greater than y
          //
          public Int32 Compare(Int64 X, Int64 Y)
          {
            Int32 _comparer_value;
            String _iso_code_national;
            Chip _chip_x;
            Chip _chip_y;

            ChipsSets.ChipSet _chip_set_x;
            ChipsSets.ChipSet _chip_set_y;

            _chip_x = Chips.GetChip(X);
            _chip_y = Chips.GetChip(Y);
            _chip_set_x = ChipsSets.GetChipsSetByChip(_chip_x);
            _chip_set_y = ChipsSets.GetChipsSetByChip(_chip_y);

            _iso_code_national = CurrencyExchange.GetNationalCurrency();
            _comparer_value = _chip_x.IsoCode.CompareTo(_chip_y.IsoCode);

            if (_comparer_value == 0)
            {
              // Compare Type
              _comparer_value = _chip_x.Type.CompareTo(_chip_y.Type);

              if (_comparer_value == 0)
              {
                // Compare chip set
                _comparer_value = _chip_set_x.ChipSetId.CompareTo(_chip_set_y.ChipSetId);
                if (_comparer_value == 0)
                {
                  // Compare Denomination DESC
                  _comparer_value = _chip_x.Denomination.CompareTo(_chip_y.Denomination);

                  if (_comparer_value == 0)
                  {
                    return 1;
                  }
                  else if (_chip_x.Denomination > _chip_y.Denomination)
                  {
                    return -1;
                  }
                  else
                  {
                    return 1;
                  }
                }
                else
                {
                  return _comparer_value;
                }
              }
              else
              {
                return _comparer_value;
              }
            }

            if (_chip_x.IsoCode == _iso_code_national)
            {
              return -1;
            }

            if (_chip_y.IsoCode == _iso_code_national)
            {
              return 1;
            }

            return _comparer_value;
          }
        }

      }
    }

    public static class ChipsSets
    {
      #region Members

      private static Int64? m_gaming_table_id = null;

      private static ChipsSets.Dictionary m_chips_sets = ChipsSets.Dictionary.Create(GamingTableId);

      #endregion Members

      #region Properties

      public static Int64? GamingTableId
      {
        get
        {
          return m_gaming_table_id;
        }
        set
        {
          if (value != m_gaming_table_id || value == 0 || value == null || m_gaming_table_id == 0 || m_gaming_table_id == null)
          {
            m_chips_sets = ChipsSets.Dictionary.Create(value);
          }

          m_gaming_table_id = value;

        }
      }

      #endregion Properties

      #region Public Methods

      //------------------------------------------------------------------------------
      // PURPOSE : Create Chips Dictionaru
      //
      //  PARAMS :
      //      - INPUT : 
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public static void Create()
      {
        m_chips_sets = ChipsSets.Dictionary.Create(GamingTableId);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Force refresh ChipsSets Dictionary
      //
      //  PARAMS :
      //      - INPUT : 
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public static void ForceRefresh()
      {
        m_chips_sets = ChipsSets.Dictionary.Create(GamingTableId);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Get chip set data by ChipsSetId
      //
      //  PARAMS :
      //      - INPUT : 
      //                Int64 ChipSetId
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                ChipSet
      //
      //   NOTES :
      //
      public static ChipSet GetSet(Int64 ChipSetId)
      {
        //ReloadChipsAndSets();

        return m_chips_sets[ChipSetId];
      }

      public static ChipSet GetChipsSetByChip(Int64 ChipId)
      {
        Chips.Chip _chip;

        _chip = Chips.GetChip(ChipId);

        if (_chip != null)
        {
          return GetChipsSetByChip(_chip);
        }

        return null;
      }

      public static ChipSet GetChipsSetByChip(Chips.Chip Chip)
      {
        foreach (KeyValuePair<Int64, ChipSet> _chip_set in m_chips_sets)
        {
          if (_chip_set.Value.Type == Chip.Type)
          {
            foreach (KeyValuePair<Int64, Chips.Chip> _chip in _chip_set.Value.GetChips())
            {
              if (_chip.Key == Chip.ChipId)
              {
                return _chip_set.Value;
              }
            }
          }
        }
        return null;
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Get chip sets data enabled
      //
      //  PARAMS :
      //      - INPUT : 
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Dictionary<Int64, ChipSet>
      //
      //   NOTES :
      //
      public static Dictionary<Int64, ChipSet> GetChipsSetsEnabled()
      {
        Dictionary<Int64, ChipsSets.ChipSet> _chips_sets_enabled;

        _chips_sets_enabled = new Dictionary<Int64, ChipSet>();

        // ReloadChipsAndSets();

        if (m_chips_sets != null)
        {
          foreach (KeyValuePair<Int64, ChipSet> _set in m_chips_sets)
          {
            if (IsChipsTypeEnabledGP(_set.Value.Type))
            {
              _chips_sets_enabled.Add(_set.Key, _set.Value);
            }
          }
        }

        return _chips_sets_enabled;
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Gets all the chips by chips type (CurrencyExchangeType)
      //
      //  PARAMS :
      //      - INPUT : 
      //                CurrencyExchangeType: Type
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                DataTable: table with "Cage.CageAmountsInitTable()" format
      //
      //   NOTES :
      //
      public static DataTable GetChipsByTypeToCageAmountTable(CurrencyExchangeType Type, String ISOCode, Boolean HideDenominations)
      {
        return GetChipsByTypeToCageAmountTable(Type, ISOCode, null, HideDenominations);
      }

      public static DataTable GetChipsByTypeToCageAmountTable(CurrencyExchangeType Type, String ISOCode, Int64? GamingTable, Boolean HideDenominations)
      {
        DataRow _row;
        DataTable _table;
        DataView _sorted_view;

        _table = Cage.CageAmountsInitTable();

        GamingTableId = GamingTable;

        //ReloadChipsAndSets();

        foreach (KeyValuePair<Int64, ChipSet> _set in m_chips_sets)
        {
          foreach (KeyValuePair<Int64, FeatureChips.Chips.Chip> _chip_item in _set.Value.GetChips())
          {
            if (_chip_item.Value.Type == FeatureChips.ConvertToChipType(Type) && ISOCode == _chip_item.Value.IsoCode)
            {
              _row = _table.NewRow();

              _row["COLOR"] = _chip_item.Value.Color;
              _row["ISO_CODE"] = _chip_item.Value.IsoCode;
              _row["DENOMINATION"] = _chip_item.Value.Denomination;
              _row["ALLOWED"] = _chip_item.Value.Allowed;
              //_row["CURRENCY_TYPE"] = "X02";
              _row["CAGE_CURRENCY_TYPE_ENUM"] = (Int32)_chip_item.Value.Type;
              _row["CHIP_ID"] = _chip_item.Value.ChipId;
              _row["CHIP_NAME"] = _chip_item.Value.Name;
              _row["CHIP_DRAW"] = _chip_item.Value.Drawing;
              _row["CHIP_SET_ID"] = _set.Key;

              _table.Rows.Add(_row);
            }
          }
        }

        _sorted_view = _table.DefaultView;
        _sorted_view.Sort = "ISO_CODE DESC, CURRENCY_TYPE DESC, CHIP_SET_ID ASC, DENOMINATION DESC, CHIP_ID ASC";

        return _sorted_view.ToTable();

      }

      public static SortedDictionary<Int64, Chips.Chip> GetChipsByTypeAndOperation(ChipsOperation.Operation ChipsOperation, CurrencyExchangeType Type, String ISOCode, Int64? GamingTable)
      {
        SortedDictionary<Int64, Chips.Chip> _chips;

        _chips = new SortedDictionary<Int64, Chips.Chip>(new FeatureChips.Chips.Chip.SortComparer());

        GamingTableId = GamingTable;

        //ReloadChipsAndSets();

        foreach (KeyValuePair<Int64, ChipSet> _set in m_chips_sets)
        {
          if (AllowedChipOperationBySet(ChipsOperation, _set.Value, Type, ISOCode, GamingTableId))
          {
            foreach (KeyValuePair<Int64, FeatureChips.Chips.Chip> _chip_item in _set.Value.GetChips())
            {
              if (_chip_item.Value.Type == FeatureChips.ConvertToChipType(Type) && ISOCode == _chip_item.Value.IsoCode)
              {
                _chips.Add(_chip_item.Key, _chip_item.Value);
              }
            }
          }
        }

        return _chips;

      }

      public static DataTable GetCurrenciesAccepted(Int64? GamingTableId)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            return GetCurrenciesAccepted(GamingTableId, _db_trx.SqlTransaction);
          }
        }
        catch
        {
          return null;
        }
      } // GetCurrenciesAccepted

      public static DataTable GetCurrenciesAccepted(Int64? GamingTableId, SqlTransaction Trx)
      {
        DataTable _dt;
        StringBuilder _sb;

        _dt = new DataTable();

        try
        {
          _sb = new StringBuilder();

          _sb.AppendLine("     SELECT   DISTINCT CE_CURRENCY_ISO_CODE AS ISO_CODE  ");
          _sb.AppendLine("            , ISNULL(CE_CURRENCY_ORDER, 0) AS ORDEN      ");
          _sb.AppendLine("            , ISNULL(GTB_MIN_BET, 0) AS MIN_BET          ");
          _sb.AppendLine("            , ISNULL(GTB_MAX_BET, 0) AS MAX_BET          ");
          _sb.AppendLine("       FROM   GAMING_TABLE_CHIPS_SETS                    ");
          _sb.AppendLine(" INNER JOIN   CHIPS_SETS                                 ");
          _sb.AppendLine("         ON   CHS_CHIP_SET_ID = GTCS_CHIPS_SET_ID        ");
          _sb.AppendLine(" INNER JOIN   CURRENCY_EXCHANGE                          ");
          _sb.AppendLine("         ON   CE_CURRENCY_ISO_CODE = CHS_ISO_CODE        ");
          _sb.AppendLine("        AND   CE_TYPE = 0                                ");
          _sb.AppendLine("  LEFT JOIN   GAMING_TABLE_BET                           ");
          _sb.AppendLine("         ON   GTB_GAMING_TABLE_ID = GTCS_GAMING_TABLE_ID ");
          _sb.AppendLine("        AND   GTB_ISO_CODE = CHS_ISO_CODE                ");
          _sb.AppendLine("      WHERE   CHS_ALLOWED = 1                            ");

          if (GamingTableId.HasValue)
          {
            _sb.AppendLine("        AND   GTCS_GAMING_TABLE_ID = @pGamingTableId     ");
          }

          _sb.AppendLine("   ORDER BY   ISNULL(CE_CURRENCY_ORDER, 0)               ");
          _sb.AppendLine("            , CE_CURRENCY_ISO_CODE                       ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            if (GamingTableId.HasValue)
            {
              _cmd.Parameters.Add("@pGamingTableId", SqlDbType.BigInt).Value = GamingTableId.Value;
            }

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _da.Fill(_dt);
            }
          }
        }
        catch
        {
          return null;
        }

        return _dt;

      } // GetCurrenciesAccepted

      #endregion Public Methods

      public class Dictionary : Dictionary<Int64, ChipsSets.ChipSet>
      {

        #region Constructors
        public Dictionary()
          : base()
        {
        }

        #endregion Constructors

        #region Public Methods

        //------------------------------------------------------------------------------
        // PURPOSE : Create chips sets
        //
        //  PARAMS :
        //      - INPUT : 
        //                
        //      - OUTPUT : 
        //                
        //
        // RETURNS :
        //                Dictionary
        //
        //   NOTES :
        //
        public static Dictionary Create(Int64? GamingTableId)
        {
          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              return LoadChipsSets(_db_trx.SqlTransaction, GamingTableId);
            }
          }
          catch
          {
            return null;
          }
        }

        #endregion Public Methods

        #region Private Methods

        //------------------------------------------------------------------------------
        // PURPOSE : Load chips set from DB
        //
        //  PARAMS :
        //      - INPUT : 
        //                SqlTransaction: Trx
        //                
        //      - OUTPUT : 
        //                
        //
        // RETURNS :
        //                Dictionary
        //
        //   NOTES :
        //
        private static Dictionary LoadChipsSets(SqlTransaction Trx)
        {
          return LoadChipsSets(Trx, 0);
        }

        private static Dictionary LoadChipsSets(SqlTransaction Trx, Int64? GamingTableId)
        {
          StringBuilder _sb;
          ChipSet _chipset;
          Dictionary _dic;

          try
          {
            _chipset = new ChipSet();
            _dic = new Dictionary();
            _sb = new StringBuilder();

            _sb.AppendLine("SELECT   CHS_CHIP_SET_ID ");
            _sb.AppendLine("       , CHS_ISO_CODE ");
            _sb.AppendLine("       , CHS_NAME ");
            _sb.AppendLine("       , CHS_ALLOWED ");
            _sb.AppendLine("       , CHS_CHIP_TYPE ");
            _sb.AppendLine("       , CSC_CHIP_ID ");
            _sb.AppendLine("       , CHS_ALLOWED_OPERATIONS_FLAGS ");

            _sb.AppendLine("  FROM   CHIPS_SETS ");
            _sb.AppendLine(" INNER   JOIN CHIPS_SETS_CHIPS ON CSC_SET_ID = CHS_CHIP_SET_ID ");

            if (GamingTableId.HasValue && GamingTableId.Value > 0)
            {
              _sb.AppendLine(" INNER   JOIN GAMING_TABLE_CHIPS_SETS ON GTCS_CHIPS_SET_ID = CHS_CHIP_SET_ID ");
            }

            _sb.AppendLine(" WHERE   CHS_ALLOWED = 1 ");

            if (GamingTableId.HasValue && GamingTableId.Value > 0)
            {
              _sb.AppendLine(" AND GTCS_GAMING_TABLE_ID = @pGamingTableId ");
            }

            _sb.AppendLine(" ORDER   BY CHS_CHIP_SET_ID ASC ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
            {
              if (GamingTableId.HasValue && GamingTableId.Value > 0)
              {
                _cmd.Parameters.Add("@pGamingTableId", SqlDbType.BigInt).Value = GamingTableId.Value;
              }

              using (SqlDataReader _reader = _cmd.ExecuteReader())
              {
                while (_reader.Read())
                {
                  if (_chipset == null || _chipset.ChipSetId != _reader.GetInt64(0))
                  {
                    // Create mew group
                    _chipset = new ChipSet();
                    _chipset.ChipSetId = _reader.GetInt64(0);
                    _chipset.ISOCode = _reader.GetString(1);
                    _chipset.Name = _reader.GetString(2);
                    _chipset.Allowed = _reader.GetBoolean(3);
                    _chipset.Type = (FeatureChips.ChipType)_reader.GetInt32(4);
                    _chipset.AllowedOperations = _reader.GetInt32(6);

                    _chipset.Chips = new List<long>();

                    _dic.Add(_chipset.ChipSetId, _chipset);
                  }
                  // Add chips Id
                  // TODO: cambiar a bigint
                  _chipset.Chips.Add(_reader.GetInt32(5));

                }
              }
            }

            return _dic;
          }
          catch (SqlException _sql_ex)
          {
            Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          return null;
        }

        #endregion Private Methods

      }

      public class ChipSet
      {

        #region Members

        private Int64 m_chip_set_id;
        private List<Int64> m_chips;
        private String m_iso_code;
        private String m_name;
        private Boolean m_allowed;
        private ChipType m_type; // Se ha de poner el tipo
        private Int32 m_allowed_operations;

        #endregion Members

        #region Enums

        public enum AllowedSetOperations
        {
          PURCHASE = 0x00000001,
          SALE = 0x00000002,
        };

        #endregion Enums

        #region Properties

        public Int64 ChipSetId
        {
          get { return m_chip_set_id; }
          set { m_chip_set_id = value; }
        }

        public String ISOCode
        {
          get { return m_iso_code; }
          set { m_iso_code = value; }
        }

        public String Name
        {
          get { return m_name; }
          set { m_name = value; }
        }

        public Boolean Allowed
        {
          get { return m_allowed; }
          set { m_allowed = value; }
        }

        public FeatureChips.ChipType Type
        {
          get { return m_type; }
          set { m_type = value; }
        }

        public List<Int64> Chips
        {
          get { return m_chips; }
          set { m_chips = value; }
        }

        public Int32 AllowedOperations
        {
          get
          {
            return m_allowed_operations;
          }

          set
          {
            m_allowed_operations = value;
          }
        }

        public Boolean AllowedPurchase
        {
          get
          {
            return AllowedOperation(AllowedSetOperations.PURCHASE);
          }
        }

        public Boolean AllowedSale
        {
          get
          {
            return AllowedOperation(AllowedSetOperations.SALE);
          }
        }


        #endregion Properties

        #region Public Methods

        public Boolean AllowedOperation(AllowedSetOperations AllowedSetOperation)
        {
          return (m_allowed_operations & (Int32)AllowedSetOperation) == (Int32)AllowedSetOperation;
        }

        //------------------------------------------------------------------------------
        // PURPOSE : Get all chips
        //
        //  PARAMS :
        //      - INPUT : 
        //                
        //      - OUTPUT : 
        //                
        //
        // RETURNS :
        //                Dictionary<Int64, Chip>
        //
        //   NOTES :
        //
        public Dictionary<Int64, FeatureChips.Chips.Chip> GetChips()
        {
          return FeatureChips.Chips.GetChips(this.m_chips);
        }

        #endregion Public Methods
      }
    }

    public class ChipsSplits
    {
      private Decimal m_redeemable;
      private Decimal m_promo_redeemable;
      private Currency m_cash_in_split1;
      private Currency m_cash_in_split2;
      private Currency m_cash_in_tax_split1;
      private Currency m_cash_in_tax_split2;
      private Currency m_tax_custody;

      private Decimal m_amount_to_sale;
      private Boolean m_is_integrated_operation;
      private Currency m_chip_prize;
      private Currency m_chips_re_amount;
      private Currency m_amount_to_pay;

      public Decimal Redeemable
      {
        get { return m_redeemable; }
        set { m_redeemable = value; }
      }
      public Decimal PromoRedeemable
      {
        get { return m_promo_redeemable; }
        set { m_promo_redeemable = value; }
      }
      public Currency CashInSplit1
      {
        get { return m_cash_in_split1; }
        set { m_cash_in_split1 = value; }
      }
      public Currency CashInSplit2
      {
        get { return m_cash_in_split2; }
        set { m_cash_in_split2 = value; }
      }
      public Currency CashInTaxSplit1
      {
        get { return m_cash_in_tax_split1; }
        set { m_cash_in_tax_split1 = value; }
      }
      public Currency CashInTaxSplit2
      {
        get { return m_cash_in_tax_split2; }
        set { m_cash_in_tax_split2 = value; }
      }
      public Currency TaxCustody
      {
        get { return m_tax_custody; }
        set { m_tax_custody = value; }
      }

      public Decimal AmountToSale
      {
        get { return m_amount_to_sale; }
        set { m_amount_to_sale = value; }
      }
      public Boolean IsIntegratedOperation
      {
        get { return m_is_integrated_operation; }
        set { m_is_integrated_operation = value; }
      }
      public Currency ChipPrize
      {
        get { return m_chip_prize; }
        set { m_chip_prize = value; }
      }
      public Currency ChipsREAmount
      {
        get { return m_chips_re_amount; }
        set { m_chips_re_amount = value; }
      }
      public Currency AmountToPay
      {
        get { return m_amount_to_pay; }
        set { m_amount_to_pay = value; }
      }

    }

  }

  public class CageDenominationsItems
  {
    public class CageDenominationsDictionary : Dictionary<CageDenominationKey, CageDenominationsItem>
    {
      //------------------------------------------------------------------------------
      // PURPOSE : Add CageDenomination by Key
      //
      //  PARAMS :
      //      - INPUT : 
      //                CageDenominationKey: Key
      //                CageDenominationsItem: Value
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public new void Add(CageDenominationKey Key, CageDenominationsItem Value)
      {
        base.Add(Key, Value);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Add CageDenomination by Key
      //
      //  PARAMS :
      //      - INPUT : 
      //                String: ISOCode
      //                CurrencyExchangeType: Type
      //                CUR: Value
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //
      //   NOTES :
      //
      public void Add(String ISOCode, CageCurrencyType Type, CageDenominationsItem Value)
      {
        base.Add(new CageDenominationKey(ISOCode, Type), Value);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Validate if exists a key by ISOCode and CurrencyExchangeType
      //
      //  PARAMS :
      //      - INPUT : 
      //                String: ISOCode
      //                CurrencyExchangeType: Type
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                Boolean: true if exists
      //
      //   NOTES :
      //
      public Boolean ContainsKey(String ISOCode, CageCurrencyType Type)
      {
        if (Type == CageCurrencyType.Coin)
        {
          Type = CageCurrencyType.Bill;
        }

        return base.ContainsKey(new CageDenominationKey(ISOCode, Type));
      }

      public Boolean ContainsKey(String ISOCode, CurrencyExchangeType Type)
      {
        try
        {
          return base.ContainsKey(new CageDenominationKey(ISOCode, FeatureChips.ConvertToCageCurrencyType(Type)));
        }
        catch
        {
          return false;
        }

      }

      //------------------------------------------------------------------------------
      // PURPOSE : Get value by ISOCode and CurrencyExchangeType
      //
      //  PARAMS :
      //      - INPUT : 
      //                String: ISOCode
      //                CurrencyExchangeType: Type
      //                
      //      - OUTPUT : 
      //                
      //
      // RETURNS :
      //                CageDenominationsItem
      //
      //   NOTES :
      //
      public CageDenominationsItem GetValue(String ISOCode, CageCurrencyType Type)
      {
        if (Type == CageCurrencyType.Coin)
        {
          Type = CageCurrencyType.Bill;
        }

        return base[new CageDenominationKey(ISOCode, Type)];
      }

      public CageDenominationsItem this[String ISOCode, CageCurrencyType Type]
      {
        get
        {
          if (Type == CageCurrencyType.Coin)
          {
            Type = CageCurrencyType.Bill;
          }

          return base[new CageDenominationKey(ISOCode, Type)];
        }
        set
        {
          if (Type == CageCurrencyType.Coin)
          {
            Type = CageCurrencyType.Bill;
          }

          base[new CageDenominationKey(ISOCode, Type)] = value;
        }
      }

      public CageDenominationsItem this[String ISOCode, CurrencyExchangeType Type]
      {
        get
        {
          try
          {
            return base[new CageDenominationKey(ISOCode, FeatureChips.ConvertToCageCurrencyType(Type))];
          }
          catch
          {
            throw new Exception("Type not found");
          }


        }
        set
        {
          try
          {
            base[new CageDenominationKey(ISOCode, FeatureChips.ConvertToCageCurrencyType(Type))] = value;
          }
          catch
          {
            throw new Exception("Type not found");
          }
        }
      }
    }

    public struct CageDenominationKey
    {
      public String ISOCode;
      public CageCurrencyType Type;

      public CageDenominationKey(String ISOCode, CageCurrencyType Type)
      {
        if (Type == CageCurrencyType.Coin)
        {
          Type = CageCurrencyType.Bill;
        }

        this.ISOCode = ISOCode;
        this.Type = Type;
      }

      public CageDenominationKey(String ISOCode, CurrencyExchangeType Type)
      {
        this.ISOCode = ISOCode;
        this.Type = FeatureChips.ConvertToCageCurrencyType(Type);
      }
    }

    public struct CageDenominationsItem
    {
      public DataTable ItemAmounts;
      //      public Dictionary<Int64, FeatureChips.Chips.Chip> ChipsAmounts;
      public Int32 TotalUnits;
      public Decimal TotalAmount;
      public Decimal TotalTips;
      public Boolean AllowedFillOut;
      public Boolean HasTicketsTito;
    }
  }
}
