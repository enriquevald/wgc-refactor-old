//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonDraws.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: SSC
// 
// CREATION DATE: 30-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAY-2012 SSC    First release.
// 24-MAY-2013 HBB    Fixed Bug #797: PromoBOX: Gifts of Draws: The filters of draws aren't used
// 24-MAY-2013 HBB    Fixed Bug #798: Gifts of Draws: The filters of draws aren't used
// 27-NOV-2013 DLL    Numbers separated per day instead per period
// 14-APR-2014 DLL    Added new functionality: VIP Account
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 29-JAN-2016 FOS    Product Backlog Item 8470:Floor Dual Currency: Promotions And Raffle
// 24-FEB-2016 FAV    Fixed Bug 9057: Exception in Log
// 26-FEB-2016 RAB    Bug 10020:TITO: It is not possible to get lottery tickets bearing available drawings
// 01-FEB-2017 JML    Fixed Bug 23938: Print coupons gives more by day
// 23-FEB-2018 FJC    Fixed Bug 31732:WIGOS-7370 [Ticket #11400] Antara - Folios de sorteos no asignados al cliente
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
namespace WSI.Common
{
  public class DrawNumbers
  {
    DrawTicket m_draw;
    DateTime m_awarded_day;
    // Real Pending Numbers are the numbers that the player has rights to have.
    // But it's possible that there are less numbers available in the draw (this is Pending Numbers).
    Int64 m_pending_numbers;
    Int64 m_real_pending_numbers;
    Int64 m_total_numbers;
    Int64 m_total_numbers_per_points;

    public DrawNumbers(DrawTicket Draw, DateTime AwardedDay, Int64 PendingNumbers, Int64 RealPendingNumbers, Int64 TotalAwardedNumbers, Int64 TotalNumbersPerPoints)
    {
      m_draw = Draw;
      m_awarded_day = AwardedDay;
      m_pending_numbers = PendingNumbers;
      m_real_pending_numbers = RealPendingNumbers;
      m_total_numbers = TotalAwardedNumbers;
      m_total_numbers_per_points = TotalNumbersPerPoints;
    }

    public void Update(DrawTicket Draw, DateTime AwardedDay, Int64 PendingNumbers, Int64 RealPendingNumbers, Int64 TotalAwardedNumbers, Int64 TotalNumbersPerPoints)
    {
      m_draw = Draw;
      m_awarded_day = AwardedDay;
      m_pending_numbers = PendingNumbers;
      m_real_pending_numbers = RealPendingNumbers;
      m_total_numbers = TotalAwardedNumbers;
      m_total_numbers_per_points = TotalNumbersPerPoints;
    }

    // Sum two DrawNumbers
    public void SumValues(DrawNumbers DrawNumber)
    {
      m_pending_numbers += DrawNumber.PendingNumbers;
      m_real_pending_numbers += DrawNumber.RealPendingNumbers;
      m_total_numbers += DrawNumber.TotalAwardedNumbers;
      m_total_numbers_per_points += DrawNumber.TotalNumbersPerPoints;
    }

    public DrawTicket Draw
    {
      get { return m_draw; }
    }

    public DateTime AwardedDay
    {
      get { return m_awarded_day; }
    }

    public Int64 PendingNumbers
    {
      get { return m_pending_numbers; }
    }

    public Int64 RealPendingNumbers
    {
      get { return m_real_pending_numbers; }
    }

    public Int64 TotalAwardedNumbers
    {
      get { return m_total_numbers; }
    }

    public Int64 TotalNumbersPerPoints
    {
      get { return m_total_numbers_per_points; }
    }
  } // DrawNumbers

  public class DrawNumberList : List<DrawNumbers>
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Returns the number of Pending Numbers in the DrawNumberList.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Int64
    // 
    //   NOTES:
    public Int64 NumPendingNumbers
    {
      get
      {
        Int64 _num_pending;

        _num_pending = 0;
        foreach (DrawNumbers _numbers in this)
        {
          _num_pending += _numbers.PendingNumbers;
        }

        return _num_pending;
      }
    } // NumPendingNumbers

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the number of Real Pending Numbers in the DrawNumberList.
    //          Real are the numbers that the player has rights to have.
    //          But it's possible that there are less numbers available in the draw (this is Pending Numbers).
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Int64
    // 
    //   NOTES:
    public Int64 NumRealPendingNumbers
    {
      get
      {
        Int64 _num_pending;

        _num_pending = 0;
        foreach (DrawNumbers _numbers in this)
        {
          _num_pending += _numbers.RealPendingNumbers;
        }

        return _num_pending;
      }
    } // NumRealPendingNumbers

    public Int64 TotalNumbers(Int64 DrawId, DateTime AwardedDay)
    {
      foreach (DrawNumbers _draw_number in this)
      {
        // DLL 28-NOV-2013 added AwardedDay condition
        if (_draw_number.Draw.DrawId == DrawId && _draw_number.AwardedDay == AwardedDay)
        {
          return _draw_number.TotalAwardedNumbers;
        }
      }

      return 0;
    } // TotalNumbers

    public Int64 TotalNumbersPerPoints(Int64 DrawId, DateTime AwardedDay)
    {
      foreach (DrawNumbers _draw_number in this)
      {
        // DLL 28-NOV-2013 added AwardedDay condition
        if (_draw_number.Draw.DrawId == DrawId && _draw_number.AwardedDay == AwardedDay)
        {
          return _draw_number.TotalNumbersPerPoints;
        }
      }

      return 0;
    } // TotalNumbersPerPoints

    public DrawNumbers Update(DrawTicket Draw, DateTime AwardedDay, Int64 PendingNumbers, Int64 RealPendingNumbers, Int64 TotalAwardedNumbers, Int64 TotalNumbersPerPoints)
    {
      DrawNumbers _new_draw_number;

      foreach (DrawNumbers _draw_number in this)
      {
        // DLL 28-NOV-2013 added AwardedDay condition
        if (_draw_number.Draw.DrawId == Draw.DrawId && _draw_number.AwardedDay == AwardedDay)
        {
          _draw_number.Update(Draw, AwardedDay, PendingNumbers, RealPendingNumbers, TotalAwardedNumbers, TotalNumbersPerPoints);

          return _draw_number;
        }
      }

      _new_draw_number = new DrawNumbers(Draw, AwardedDay, PendingNumbers, RealPendingNumbers, TotalAwardedNumbers, TotalNumbersPerPoints);

      this.Add(_new_draw_number);

      return _new_draw_number;
    } // Update

    public Boolean Get(DrawNumbers DrawNumber, out DrawNumbers Element)
    {
      Element = null;

      foreach (DrawNumbers _draw_number in this)
      {
        if (_draw_number.Draw.DrawId == DrawNumber.Draw.DrawId)
        {
          Element = _draw_number;

          return true;
        }
      }

      return false;
    } // Get

    public new Boolean Contains(DrawNumbers DrawNumber)
    {
      foreach (DrawNumbers _draw_number in this)
      {
        if (_draw_number.Draw.DrawId == DrawNumber.Draw.DrawId)
        {
          return true;
        }
      }

      return false;
    } // Contains

    public Boolean Contains(DrawTicket Draw)
    {
      foreach (DrawNumbers _draw_number in this)
      {
        if (_draw_number.Draw.DrawId == Draw.DrawId)
        {
          return true;
        }
      }

      return false;
    } // Contains

    public Boolean Contains(DrawTicket Draw, DateTime AwardedDay)
    {
      foreach (DrawNumbers _draw_number in this)
      {
        // DLL 28-NOV-2013 added AwardedDay condition
        if (_draw_number.Draw.DrawId == Draw.DrawId && _draw_number.AwardedDay == AwardedDay)
        {
          return true;
        }
      }

      return false;
    } // Contains

    public Boolean Remove(Int64 DrawId)
    {
      DrawNumbers _draw_to_remove;

      _draw_to_remove = null;

      foreach (DrawNumbers _draw_number in this)
      {
        if (_draw_number.Draw.DrawId == DrawId)
        {
          _draw_to_remove = _draw_number;

          break;
        }
      }

      if (_draw_to_remove != null)
      {
        return this.Remove(_draw_to_remove);
      }

      return false;
    } // Remove

    // Sum all DrawNumber grouped by Draw. We need only a summatory for each Draw
    public DrawNumberList GroupByDraw()
    {
      DrawNumberList _list_by_draw;
      DrawNumbers _element;
      DateTime _today;

      _today = Misc.TodayOpening();
      _list_by_draw = new DrawNumberList();

      foreach (DrawNumbers _draw_number in this)
      {
        if (!_list_by_draw.Get(_draw_number, out _element))
        {
          _element = new DrawNumbers(_draw_number.Draw, _today, _draw_number.PendingNumbers, _draw_number.RealPendingNumbers,
                                     _draw_number.TotalAwardedNumbers, _draw_number.TotalNumbersPerPoints);
          _list_by_draw.Add(_element);

          continue;
        }

        _element.SumValues(_draw_number);
      }

      return _list_by_draw;

    } // GroupByDraw

  } // DrawNumberList

  // New class for numbers per day. Gruped all variables to calculate numbers for each day
  public class NumbersPerDay
  {
    Currency m_total_played;
    Currency m_redeemable_played;
    Currency m_total_spent;
    Currency m_redeemable_spent;
    Int64 m_real_numbers;
    Int64 m_num_numbers;
    Currency m_used_cash_in;

    public NumbersPerDay()
    {
      m_total_played = 0;
      m_redeemable_played = 0;
      m_total_spent = 0;
      m_redeemable_spent = 0;
      m_real_numbers = 0;
      m_num_numbers = 0;
      m_used_cash_in = 0;
    }

    public Currency TotalPlayed
    {
      get { return m_total_played; }
      set { m_total_played = value; }
    }

    public Currency RedeemablePlayed
    {
      get { return m_redeemable_played; }
      set { m_redeemable_played = value; }
    }

    public Currency TotalSpent
    {
      get { return m_total_spent; }
      set { m_total_spent = value; }
    }

    public Currency RedeemableSpent
    {
      get { return m_redeemable_spent; }
      set { m_redeemable_spent = value; }
    }

    public Int64 RealNumbers
    {
      get { return m_real_numbers; }
      set { m_real_numbers = value; }
    }

    public Int64 NumNumbers
    {
      get { return m_num_numbers; }
      set { m_num_numbers = value; }
    }

    public Currency UsedCashIn
    {
      get { return m_used_cash_in; }
      set { m_used_cash_in = value; }
    }

  } // NumbersPerDay

  public class Draws
  {

    private const Int32 DEFAULT_MAX_TICKETS_PER_OPERATION = 100;

    //------------------------------------------------------------------------------
    // PURPOSE: Generate all the tickets for all the draws available for the account.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - Card
    //          - PointsToSpent
    //          - DrawId
    //          - OnlyCheck
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - AwardedNumbersPerDraw
    //          - SpentPoints
    //          - TicketList
    //
    // RETURNS:
    //      - DrawTicketLogic.GetTicketCodes
    // 
    //   NOTES:
    public static DrawTicketLogic.GetTicketCodes GenerateTicketsByPoints(Int64 OperationId,
                                                                         CardData Card,
                                                                         Decimal PointsToSpent,
                                                                         Int64 DrawId,
                                                                         Boolean OnlyCheck,
                                                                         SqlTransaction SqlTrx,
                                                                         out DrawNumberList AwardedNumbersPerDraw,
                                                                         out Points SpentPoints,
                                                                         out List<DrawTicket> TicketList)
    {
      List<DrawTicket> _draws;
      DrawTicket _draw;
      List<DrawTicket> _tickets;
      DrawNumberList _ticket_draw_numbers;
      Int64 _previous_numbers;
      Int64 _num_numbers;
      Int64 _total_numbers;
      Int64 _num_tickets;
      DateTime _dt_awarded_day;

      AwardedNumbersPerDraw = new DrawNumberList();
      TicketList = new List<DrawTicket>();
      SpentPoints = 0;

      try
      {
        _ticket_draw_numbers = new DrawNumberList();

        _tickets = new List<DrawTicket>();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          SqlTransaction _sql_trx = _db_trx.SqlTransaction;

          if (!OnlyCheck)
          {
            _sql_trx = SqlTrx;
          }

          // Get Previously generated numbers for today
          if (!DrawTicketLogic.GetAccountDrawNumbers(Card.AccountId, ref AwardedNumbersPerDraw, true, _sql_trx))
          {
            return DrawTicketLogic.GetTicketCodes.ERROR;
          }

          // Get Draw indicated by DrawId (only one is returned)
          _draws = DrawTicketLogic.LockApplicableDraws(Card.PlayerTracking.CardLevel, DrawId, _sql_trx);

          if (OnlyCheck)
          {
            _db_trx.Rollback();
          }

          if (_draws.Count <= 0)
          {
            return DrawTicketLogic.GetTicketCodes.NO_DRAW;
          }

          _draw = _draws[0];

          if (_draw.NumberPoints <= 0)
          {
            return DrawTicketLogic.GetTicketCodes.NO_DRAW;
          }

          // HBB 15-MAY-2013: Filter by Level
          if (!Misc.IsLevelAllowed(Card.PlayerTracking.CardLevel, _draw.LevelFilter))
          {
            return DrawTicketLogic.GetTicketCodes.NO_DRAW;
          }

          //  HBB & RCI 23-MAY-2013: Filter by gender
          if (!Misc.IsGenderAllowed(Card.PlayerTracking.HolderGender, _draw.GenderFilter))
          {
            return DrawTicketLogic.GetTicketCodes.NO_DRAW;
          }
		  
		      //  DLL 14-APR-2014: Filter by VIP
          if (!Misc.IsVIP(Card.PlayerTracking.HolderIsVIP, _draw.IsVIP))
          {
            return DrawTicketLogic.GetTicketCodes.NO_DRAW;
          }
		  
          _dt_awarded_day = Misc.TodayOpening();

          // How many numbers?
          _previous_numbers = AwardedNumbersPerDraw.TotalNumbers(_draw.DrawId, _dt_awarded_day);
          _num_numbers = _previous_numbers + (Int64)(PointsToSpent / _draw.NumberPoints);

          // - Limit per Day and Player
          if (_draw.LimitPerDayAndPlayer < Int32.MaxValue)
          {
            if (_previous_numbers >= _draw.LimitPerDayAndPlayer)
            {
              return DrawTicketLogic.GetTicketCodes.LIMIT_REACHED;
            }
          }

          _num_numbers = Math.Min(_num_numbers, _draw.LimitPerDayAndPlayer);
          _total_numbers = Math.Max(_num_numbers, _previous_numbers); // Previouly 'printed' numbers are greater than the current amount: --> Limit changed 

          // Pending to generate
          _num_numbers -= _previous_numbers;
          _num_numbers = Math.Max(0, _num_numbers);

          // - Limit per Draw        
          _num_numbers = Math.Min(_num_numbers, _draw.MaxNumbersAvailable);

          if (_num_numbers == 0)
          {
            return DrawTicketLogic.GetTicketCodes.DRAW_MAX_REACHED;
          }
          AwardedNumbersPerDraw.Update(_draw, _dt_awarded_day, _num_numbers, _num_numbers, _total_numbers, 0);

          _num_tickets = (_num_numbers + _draw.MaxNumbersPerVoucher - 1) / _draw.MaxNumbersPerVoucher;

          SpentPoints = _num_numbers * _draw.NumberPoints;

          while (_num_numbers > 0)
          {
            Int64 _count;

            _num_tickets = (_num_numbers + _draw.MaxNumbersPerVoucher - 1) / _draw.MaxNumbersPerVoucher;
            _count = _num_numbers / _num_tickets;
            _ticket_draw_numbers.Add(new DrawNumbers(_draw, _dt_awarded_day, _count, _count, _total_numbers, 0));
            _num_numbers -= _count;
          }

          if (_ticket_draw_numbers.Count == 0)
          {
            return DrawTicketLogic.GetTicketCodes.NO_DRAW;
          }

          if (OnlyCheck)
          {
            return DrawTicketLogic.GetTicketCodes.OK;
          }


          foreach (DrawNumbers _dn in _ticket_draw_numbers)
          {
            DrawTicket _draw_ticket;

            _draw_ticket = null;

            if (!CreateTicket(_dn.Draw, _dn.AwardedDay, (Int32)_dn.PendingNumbers, Card, true, OperationId, ref _draw_ticket, _sql_trx))
            {
              Log.Error(string.Format("GenerateTicketsByPoints() ==> CreateTicket(). OperationId: {0}; AccountId: {1} ", OperationId, Card.AccountId));

              return DrawTicketLogic.GetTicketCodes.ERROR;
            }

            if (_draw_ticket.CreationCode == DrawTicketLogic.GetTicketCodes.OK)
            {
              _tickets.Add(_draw_ticket);
            }
          }

          TicketList = _tickets;
        }

        return DrawTicketLogic.GetTicketCodes.OK;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        AwardedNumbersPerDraw = new DrawNumberList();
      }

      return DrawTicketLogic.GetTicketCodes.ERROR;
    } // GenerateTicketsByPoints

    //------------------------------------------------------------------------------
    // PURPOSE: Generate all the tickets for all the draws available for the account.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardData Card
    //          - Boolean OnlyCheck
    //          - DrawNumberList DrawsToGenerate
    //          - Int64 SessionId
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT:
    //          - DrawNumberList RealAwardedNumbersPerDraw
    //          - List<DrawTicket> DrawTicketsList
    //          - Int64 OperationId
    //
    // RETURNS:
    //      - True: Generation was correctly. False: Otherwise.
    // 
    //   NOTES:
    public static Boolean CommonGenerateTickets(CardData Card,
                                                Boolean OnlyCheck,
                                                DrawNumberList DrawsToGenerate,
                                                Int64 SessionId,
                                                SqlTransaction SqlTrx,
                                                out DrawNumberList AwardedNumbersPerDraw,
                                                out List<DrawTicket> DrawTicketsList,
                                                out Int64 OperationId)
    {

      List<DrawTicket> _draws;
      DataTable _cash_in;
      DataTable _spent_by_provider_hour;
      DrawNumberList _ticket_draw_numbers;
      Boolean _account_is_anonymous;
      DrawNumberList _awarded_numbers_per_draw_and_day;
      Int32 _max_tickets_per_operation;

      Dictionary<DateTime, NumbersPerDay> _dic_numbers_per_day;
      NumbersPerDay _numbers_per_day;
      DateTime _opening_date;

      AwardedNumbersPerDraw = new DrawNumberList();
      DrawTicketsList = new List<DrawTicket>();
      OperationId = 0;

      _awarded_numbers_per_draw_and_day = new DrawNumberList();
      _account_is_anonymous = (String.IsNullOrEmpty(Card.PlayerTracking.HolderName.Trim()));

      try
      {
        _ticket_draw_numbers = new DrawNumberList();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          SqlTransaction _sql_trx = _db_trx.SqlTransaction;

          if (!OnlyCheck)
          {
            _sql_trx = SqlTrx;
          }

          if (!GetSpentGroupByProviderHour(Card.AccountId, OnlyCheck, out _spent_by_provider_hour, out _cash_in, _sql_trx))
          {
              return false;
            }

          // Get Previously generated numbers in the period grouped by day (can be more than one day)
          // This is used to calculate the pending numbers
          if (!DrawTicketLogic.GetAccountDrawNumbers(Card.AccountId, ref _awarded_numbers_per_draw_and_day, false, _sql_trx))
          {
            return false;
          }

          // Get All Opened Draws (excluding "By Points")
          _draws = DrawTicketLogic.LockApplicableDraws(Card.PlayerTracking.CardLevel, 0, _sql_trx);

          if (OnlyCheck)
          {
            _db_trx.Rollback(); 
          }

          if (DrawsToGenerate != null)
          {
            DrawNumberList _real_awarded_numbers_per_draw;
            List<DrawTicket> _real_draws;

            // RCI 16-JUN-2011: Work only with the draws in the DrawsToGenerate list:
            //                  - Intersect draws from DrawsToGenerate and _period_awarded_numbers_per_draw.
            //                  - Intersect draws from DrawsToGenerate and _draws.

            // List _period_awarded_numbers_per_draw
            _real_awarded_numbers_per_draw = new DrawNumberList();
            foreach (DrawNumbers _draw_number in _awarded_numbers_per_draw_and_day)
            {
              if (DrawsToGenerate.Contains(_draw_number))
              {
                _real_awarded_numbers_per_draw.Add(_draw_number);
              }
            }
            _awarded_numbers_per_draw_and_day = _real_awarded_numbers_per_draw;

            // List _draws
            _real_draws = new List<DrawTicket>();
            foreach (DrawTicket _draw_ticket in _draws)
            {
              if (DrawsToGenerate.Contains(_draw_ticket))
              {
                _real_draws.Add(_draw_ticket);
              }
            }
            _draws = _real_draws;
          } // if (DrawsToGenerate != null)

          foreach (DrawTicket _draw in _draws)
          {
            Int64 _previous_numbers;
            Int64 _numbers_per_points;
            Int64 _total_numbers;
            Int64 _num_tickets;
            Int64 _today_previous_numbers;
            Int64 _today_numbers_per_points;
            DateTime _dt_opening;

            // HBB 15-MAY-2013: Filter by Level
            if (!Misc.IsLevelAllowed(Card.PlayerTracking.CardLevel, _draw.LevelFilter))
            {
              continue;
            }

            //  HBB & RCI 23-MAY-2013: Filter by gender
            if (!Misc.IsGenderAllowed(Card.PlayerTracking.HolderGender, _draw.GenderFilter))
            {
              continue;
            }
			
			      //  DLL 14-APR-2014: Filter by VIP
            if (!Misc.IsVIP(Card.PlayerTracking.HolderIsVIP, _draw.IsVIP))
            {
              continue;
            }

            _dic_numbers_per_day = new Dictionary<DateTime, NumbersPerDay>();
            _numbers_per_day = new NumbersPerDay();

            if (_draw.DrawType == DrawType.BY_CASH_IN)
            {
              // RCI 12-JUL-2011: Now, draws of type CASH_IN can have offers.
              GetDrawParametersByCashIn(Card.PlayerTracking.HolderGender,
                                        _draw.ProviderList, _draw.DrawOfferList,
                                        _spent_by_provider_hour, _cash_in,
                                        out _dic_numbers_per_day);
            }
            else if (_draw.DrawType == DrawType.BY_CASH_IN_POSTERIORI)
            {
              // Do nothing
            }
            else
            {
              GetDrawParameters(Card.PlayerTracking.HolderGender,
                                _draw.ProviderList, _draw.DrawOfferList,
                                _spent_by_provider_hour,
                                out _dic_numbers_per_day);
            }

            // How many numbers?
            switch (_draw.DrawType)
            {
              case DrawType.BY_CASH_IN:
                {
                  Currency _used_cash_in;
                  Currency _cash_in_amount;
                  Decimal _cash_in_offer;

                  Boolean _only_acum;

                  _used_cash_in = 0;
                  foreach (DataRow _op_cash_in in _cash_in.Rows)
                  {
                    _cash_in_amount = (Decimal)_op_cash_in["AMOUNT"];
                    _cash_in_offer = (Decimal)_op_cash_in["OFFER"];
                    _opening_date = Misc.Opening((DateTime)_op_cash_in["DATETIME"]);

                    //Select the numbers for the opening date in each cash in
                    if (_dic_numbers_per_day.ContainsKey(_opening_date))
                    {
                      _numbers_per_day = _dic_numbers_per_day[_opening_date];
                    }
                    else
                    {
                      _numbers_per_day = new NumbersPerDay();
                    }

                    // Check constrains: Minimum CashIn, Played, Spent
                    // Note: used cash in will be 0 till some numbers are awarded
                    if (_cash_in_amount >= _draw.MinCashIn)
                    {
                      Boolean _apply;

                      _apply = false;
                      _only_acum = false;

                      if (_draw.MinPlayedPct == 0 && _draw.MinSpentPct == 0)
                      {
                        _apply = true;
                      }
                      else
                      {
                        if (_draw.FirstCashInConstrained && _numbers_per_day.UsedCashIn == 0)
                        {
                          // if _used_cash_in == 0 --> First time on enter
                          _only_acum = true;
                        }
                        else
                        {
                          if (_draw.MinPlayedPct > 0 && _numbers_per_day.RedeemablePlayed >= _draw.MinPlayedPct * _numbers_per_day.UsedCashIn / 100.0m)
                          {
                            _apply = true;
                          }
                          if (_draw.MinSpentPct > 0 && _numbers_per_day.RedeemableSpent >= _draw.MinSpentPct * _numbers_per_day.UsedCashIn / 100.0m)
                          {
                            _apply = true;
                          }
                        }
                      }

                      if (_apply)
                      {
                        Int64 _real_numbers;

                        _real_numbers = (Int64)(_cash_in_amount / _draw.NumberPrice);
                        if (_real_numbers > 0)
                        {
                          if (_draw.LimitPerOperation > 0)
                          {
                            // Max tickets per operation 
                            _numbers_per_day.NumNumbers += Math.Min((Int64)(_real_numbers * _cash_in_offer), _draw.LimitPerOperation);
                          }
                          else
                          {
                            // Numbers for this 'cash in' operation
                            _numbers_per_day.NumNumbers += (Int64)(_real_numbers * _cash_in_offer);
                          }
                          // Cash in used to generate numbers ...
                          // AJQ & RCI 23-DEC-2011: Compute the 'real used cash-in', if we use the 'cash-in' of the operation we are against the big 'cash-in'
                          // MinCashIn = 500 & Price     = 500, CashIn = 900 --> 1 number, but only '500' of the '900' were used to generate numbers 
                          _numbers_per_day.UsedCashIn += _real_numbers * _draw.NumberPrice;
                        }
                      }
                      else if (_only_acum)
                      {
                        // Cash in used to generate numbers ...
                        _numbers_per_day.UsedCashIn += _cash_in_amount;
                      }
                    }

                    _dic_numbers_per_day[_opening_date] = _numbers_per_day;
                  }
                }
                break;

              case DrawType.BY_CASH_IN_POSTERIORI:
                {
                  //DLL
                  DataTable _dt_cash_posteriori;
                  _dt_cash_posteriori = GetGroupByOperations(Card.AccountId, _draw);
                  _dic_numbers_per_day = GetNumbersCashInPosteriori(_draw, Card.PlayerTracking, _dt_cash_posteriori);
                }
                break;

              case DrawType.BY_PLAYED_CREDIT:
                foreach (KeyValuePair<DateTime, NumbersPerDay> _num_per_day in _dic_numbers_per_day)
                {
                  _num_per_day.Value.NumNumbers = (Int64)(_num_per_day.Value.TotalPlayed / _draw.NumberPrice);
                }
                break;

              case DrawType.BY_REDEEMABLE_SPENT:
                foreach (KeyValuePair<DateTime, NumbersPerDay> _num_per_day in _dic_numbers_per_day)
                {
                  _num_per_day.Value.NumNumbers = (Int64)(_num_per_day.Value.RedeemableSpent / _draw.NumberPrice);
                }
                break;

              case DrawType.BY_TOTAL_SPENT:
                foreach (KeyValuePair<DateTime, NumbersPerDay> _num_per_day in _dic_numbers_per_day)
                {
                  _num_per_day.Value.NumNumbers = (Int64)(_num_per_day.Value.TotalSpent / _draw.NumberPrice);
                }
                break;

              case DrawType.BY_POINTS:
              default:
                break;

            }  // switch (_draw.DrawType)


            foreach (KeyValuePair<DateTime, NumbersPerDay> _num_per_day in _dic_numbers_per_day)
            {
              _dt_opening = _num_per_day.Key;
              // _previous_numbers include numbers per points.
              _previous_numbers = _awarded_numbers_per_draw_and_day.TotalNumbers(_draw.DrawId, _dt_opening);
              _today_previous_numbers = _previous_numbers;
              _numbers_per_points = _awarded_numbers_per_draw_and_day.TotalNumbersPerPoints(_draw.DrawId, _dt_opening);
              // Now, _previous_numbers DOESN'T include numbers per points.
              _previous_numbers -= _numbers_per_points;

              // Pending to generate
              _num_per_day.Value.NumNumbers = Math.Max(0, _num_per_day.Value.NumNumbers - _previous_numbers);

              if (_draw.LimitPerDayAndPlayer < Int32.MaxValue)
              {
                if (_today_previous_numbers >= _draw.LimitPerDayAndPlayer)
                {
                  // Limit reached.
                  _num_per_day.Value.NumNumbers = 0;
                }
                else
                {
                  // Here, LimitPerDayAndPlayer is always greater than _today_previous_numbers.
                  _num_per_day.Value.NumNumbers = Math.Min(_num_per_day.Value.NumNumbers, _draw.LimitPerDayAndPlayer - _today_previous_numbers);
                }
              }

              // total numbers in the working day.
              _total_numbers = _today_previous_numbers + _num_per_day.Value.NumNumbers;

              // - Limit per Draw
              // Keep the real num pending numbers. We need it for the form frm_available_draws.
              _num_per_day.Value.RealNumbers = _num_per_day.Value.NumNumbers;
              _num_per_day.Value.NumNumbers = Math.Min(_num_per_day.Value.NumNumbers, _draw.MaxNumbersAvailable);

              _today_numbers_per_points = _awarded_numbers_per_draw_and_day.TotalNumbersPerPoints(_draw.DrawId, _dt_opening);

              if (_awarded_numbers_per_draw_and_day.Contains(_draw, _dt_opening))
              {
                _awarded_numbers_per_draw_and_day.Update(_draw, _dt_opening, _num_per_day.Value.NumNumbers, _num_per_day.Value.RealNumbers, _total_numbers, _today_numbers_per_points);
              }
              else
              {
                _awarded_numbers_per_draw_and_day.Add(new DrawNumbers(_draw, _dt_opening, _num_per_day.Value.NumNumbers, _num_per_day.Value.RealNumbers, _total_numbers, _today_numbers_per_points));
              }

              if (_num_per_day.Value.NumNumbers == 0)
              {
                continue;
              }

              while (_num_per_day.Value.NumNumbers > 0)
              {
                Int64 _count;

                _num_tickets = (_num_per_day.Value.NumNumbers + _draw.MaxNumbersPerVoucher - 1) / _draw.MaxNumbersPerVoucher;
                _count = _num_per_day.Value.NumNumbers / _num_tickets;
                _ticket_draw_numbers.Add(new DrawNumbers(_draw, _dt_opening, _count, _count, _total_numbers, 0));
                _num_per_day.Value.NumNumbers -= _count;
              }
            }
          } // foreach _draw

          AwardedNumbersPerDraw = _awarded_numbers_per_draw_and_day.GroupByDraw();

          if (OnlyCheck)
          {
            return true;
          }

          if (_ticket_draw_numbers.Count == 0)
          {
            // No tickets to be printed
            return true;
          }

          // Generate new operation id for DRAW_TICKET.
          Operations.DB_InsertOperation(OperationCode.DRAW_TICKET, Card.AccountId, SessionId,
                                        0, 0, 0, 0, 0, 0, string.Empty, out OperationId, _sql_trx);

          _max_tickets_per_operation = GeneralParam.GetInt32("Cashier.DrawTicket", "MaxTicketsPerOperation", DEFAULT_MAX_TICKETS_PER_OPERATION);
          _max_tickets_per_operation = Math.Min(_max_tickets_per_operation, DEFAULT_MAX_TICKETS_PER_OPERATION);

          foreach (DrawNumbers _dn in _ticket_draw_numbers)
          {
            DrawTicket _draw_ticket;

            _draw_ticket = null;

            if (!CreateTicket(_dn.Draw, _dn.AwardedDay, (Int32)_dn.PendingNumbers, Card, false, OperationId, ref _draw_ticket, _sql_trx))
            { 
              Log.Error(string.Format("CommonGenerateTickets() ==> CreateTicket(). OperationId: {0}; AccountId: {1} ", OperationId, Card.AccountId));

              return false;
            }

            if (_draw_ticket.CreationCode == DrawTicketLogic.GetTicketCodes.OK)
            {
              DrawTicketsList.Add(_draw_ticket);
            }

            if (DrawTicketsList.Count >= _max_tickets_per_operation)
            {
              break;
            }
          }
          _db_trx.Commit(); // if (!OnlyCheck) this commit has no effect.
                            // if (Onlycheck == TRUE) we do a return before arrive here.
          
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        AwardedNumbersPerDraw = new DrawNumberList();

        return false;
      }

      return true;

    } // CommonGenerateTickets

    //------------------------------------------------------------------------------
    // PURPOSE: Get the draw parameters depending on the AccountGender, the draw provider list and the hour offers.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 AccountGender
    //          - ProviderList ProviderList
    //          - DrawOfferList DrawHourOffers
    //          - DataTable ParamsByProvider
    //
    //      - OUTPUT:
    //          - Dictionary DicNumbersDay
    //
    // RETURNS:
    //      - True: Parameters retrieved correctly. False: Otherwise.
    // 
    //   NOTES:
    private static void GetDrawParameters(Int32 AccountGender,
                                          ProviderList ProviderList,
                                          DrawOfferList DrawOfferList,
                                          DataTable ParamsByProvider,
                                          out Dictionary<DateTime, NumbersPerDay> DicNumbersPerDay)
    {
      String _provider_id;
      Decimal _factor_offer;
      DateTime _date_opening;
      NumbersPerDay _num_per_day;

      //DLL 28-NOV-2013: Initialize all values in dictionary with 0
      DicNumbersPerDay = new Dictionary<DateTime, NumbersPerDay>();

      foreach (DataRow _provider in ParamsByProvider.Rows)
      {
        _provider_id = (String)_provider["TE_PROVIDER_ID"];
        if (!ProviderList.Contains(_provider_id.ToUpper()))
        {
          continue;
        }

        _factor_offer = DrawOfferList.GetFactor(AccountGender, (DateTime)_provider["STARTED"], (DateTime)_provider["FINISHED"]);

        _date_opening = Misc.Opening((DateTime)_provider["FINISHED"]);

        if (DicNumbersPerDay.ContainsKey(_date_opening))
        {
          _num_per_day = DicNumbersPerDay[_date_opening];
        }
        else
        {
          _num_per_day = new NumbersPerDay();
        }

        _num_per_day.TotalPlayed += _factor_offer * (Decimal)_provider["TOTAL_PLAYED"];
        _num_per_day.RedeemablePlayed += _factor_offer * (Decimal)_provider["REDEEMABLE_PLAYED"];
        _num_per_day.TotalSpent += _factor_offer * (Decimal)_provider["TOTAL_SPENT"];
        _num_per_day.RedeemableSpent += _factor_offer * (Decimal)_provider["REDEEMABLE_SPENT"];

        DicNumbersPerDay[_date_opening] = _num_per_day;
      }

    } // GetDrawParameters

    //------------------------------------------------------------------------------
    // PURPOSE: Get the draw parameters for the CASH_IN type depending on the AccountGender,
    //          the draw provider list and the hour offers.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 AccountGender
    //          - ProviderList ProviderList
    //          - DrawOfferList DrawHourOffers
    //          - DataTable ParamsByProvider
    //          - DataTable AllCashIn
    //
    //      - OUTPUT:
    //          - Dictionary DicNumbersDay
    //
    // 
    //   NOTES:
    private static void GetDrawParametersByCashIn(Int32 AccountGender,
                                                  ProviderList ProviderList,
                                                  DrawOfferList DrawOfferList,
                                                  DataTable ParamsByProvider,
                                                  DataTable AllCashIn,
                                                  out Dictionary<DateTime, NumbersPerDay> DicNumbersPerDay)
    {
      String _provider_id;
      Decimal _factor_offer;
      DateTime _date_opening;
      NumbersPerDay _num_per_day;

      //DLL 28-NOV-2013: Initialize all values in dictionary with 0
      DicNumbersPerDay = new Dictionary<DateTime, NumbersPerDay>();

      foreach (DataRow _provider in ParamsByProvider.Rows)
      {
        _provider_id = (String)_provider["TE_PROVIDER_ID"];

        if (!ProviderList.Contains(_provider_id.ToUpper()))
        {
          continue;
        }

        _date_opening = Misc.Opening((DateTime)_provider["STARTED"]);

        if (DicNumbersPerDay.ContainsKey(_date_opening))
        {
          _num_per_day = DicNumbersPerDay[_date_opening];
        }
        else
        {
          _num_per_day = new NumbersPerDay();
        }

        _num_per_day.TotalPlayed += (Decimal)_provider["TOTAL_PLAYED"];
        _num_per_day.RedeemablePlayed += (Decimal)_provider["REDEEMABLE_PLAYED"];
        _num_per_day.TotalSpent += (Decimal)_provider["TOTAL_SPENT"];
        _num_per_day.RedeemableSpent += (Decimal)_provider["REDEEMABLE_SPENT"];

        DicNumbersPerDay[_date_opening] = _num_per_day;
      }

      foreach (DataRow _cash_in in AllCashIn.Rows)
      {
        _factor_offer = DrawOfferList.GetFactor(AccountGender, (DateTime)_cash_in["DATETIME"], (DateTime)_cash_in["DATETIME"]);
        _cash_in["OFFER"] = _factor_offer;
      }
    } // GetDrawParametersByCashIn

    //------------------------------------------------------------------------------
    // PURPOSE: Get the draw parameters for the account grouped by provider and hour.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int64 AccountId
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT:
    //          - DataTable ParamsByProvider
    //          - DataTable AllCashIn
    //
    // RETURNS:
    //      - True: Parameters retrieved correctly. False: Otherwise.
    // 
    //   NOTES:
    public static Boolean GetSpentGroupByProviderHour(Int64 AccountId,
                                                      Boolean OnlyCheck,
                                                       out DataTable ParamsByProvider,
                                                       out DataTable AllCashIn,
                                                       SqlTransaction SqlTrx)
    {
      StringBuilder _sql_txt;
      Int32 _hours_before;
      DateTime _lower_limit_datetime;
      DataSet _ds_play_sessions;
      DataTable _wrong_play_sesions;
      StringBuilder _description;

      ParamsByProvider = new DataTable("PARAMS_BY_PROVIDER");
      AllCashIn = new DataTable("CASH_IN");
      _wrong_play_sesions = new DataTable("WRONG_PLAY_SESIONS");

      _ds_play_sessions = new DataSet();

      try
      {
        _hours_before = GeneralParam.GetInt32("Cashier", "Draws.HoursBeforeTodayOpening");

        // Round to opening date. F.ex: If the GP says 8 hours, we go to the opening hour.
        _lower_limit_datetime = Misc.Opening(Misc.TodayOpening().AddHours(-_hours_before));

        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("EXECUTE GetSpentGroupByTerminal");
        _sql_txt.AppendLine("        @pAccountId");
        _sql_txt.AppendLine("       ,@pLowerLimitDatetime");
        _sql_txt.AppendLine("       ,@pPlaySessionOpened");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pLowerLimitDatetime", SqlDbType.DateTime).Value = _lower_limit_datetime;
          _sql_cmd.Parameters.Add("@pPlaySessionOpened", SqlDbType.Int).Value = 0;  // WCP_PlaySessionStatus.Opened = 0

          using (SqlDataAdapter _sql_adap = new SqlDataAdapter(_sql_cmd))
          {
            _sql_adap.Fill(_ds_play_sessions);
            ParamsByProvider = _ds_play_sessions.Tables[0];
            _wrong_play_sesions = _ds_play_sessions.Tables[1];
          }

          Int64 _terminal_id;
          String _terminal_name;
          String _user_name;
          String _cashier_info;

          _cashier_info = String.Empty;
          _terminal_id = WSI.Common.CommonCashierInformation.TerminalId;
          _terminal_name = WSI.Common.CommonCashierInformation.TerminalName;
          _user_name = WSI.Common.CommonCashierInformation.UserName;

          _cashier_info = _terminal_name + "@" + _user_name;

          if (!OnlyCheck)
    {
            foreach (DataRow _row in _wrong_play_sesions.Rows)
      {
              //PS_ACCOUNT_ID 
              //PS_PLAY_SESSION_ID 
              //TE_PROVIDER_ID 
              //TE_TERMINAL_ID 
              //PS_STARTED              
              //PS_FINISHED             
              //TOTAL_PLAYED       
              //REDEEMABLE_PLAYED   
              //TOTAL_SPENT    
              //REDEEMABLE_SPENT 
              //PS_INITIAL_BALANCE 
              //PS_FINAL_BALANCE 
              //PS_WON_AMOUNT 
              //PS_PLAYED_COUNT 
              //UNBALANCE 
              //AVG_BET

              _description = new StringBuilder();
              _description.AppendLine(Resource.String("STR_PLAY_SESSION_DISCARDED_TO_GIVE_DRAW_NUMBERS").Replace("@p0", _row["PS_PLAY_SESSION_ID"].ToString()));
              //_description.AppendLine("Play session discarded to give draw numbers: @p0".Replace("@p0", _row["PS_PLAY_SESSION_ID"].ToString()));
              _description.AppendLine("   Account id:          @p0".Replace("@p0", _row["PS_ACCOUNT_ID"].ToString()));
              _description.AppendLine("   Provider:            @p0".Replace("@p0", _row["TE_PROVIDER_ID"].ToString()));
              _description.AppendLine("   Terminal id:         @p0".Replace("@p0", _row["TE_TERMINAL_ID"].ToString()));
              _description.AppendLine("   Play session star:   @p0".Replace("@p0", _row["PS_STARTED"].ToString()));
              _description.AppendLine("   Play session finish: @p0".Replace("@p0", _row["PS_FINISHED"].ToString()));
              _description.AppendLine("   Total played:        @p0".Replace("@p0", _row["TOTAL_PLAYED"].ToString()));
              _description.AppendLine("   Redeemable played:   @p0".Replace("@p0", _row["REDEEMABLE_PLAYED"].ToString()));
              _description.AppendLine("   Total spent:         @p0".Replace("@p0", _row["TOTAL_SPENT"].ToString()));
              _description.AppendLine("   Redeemable spent:    @p0".Replace("@p0", _row["REDEEMABLE_SPENT"].ToString()));
              _description.AppendLine("   Initial balance:     @p0".Replace("@p0", _row["PS_INITIAL_BALANCE"].ToString()));
              _description.AppendLine("   Final balance:       @p0".Replace("@p0", _row["PS_FINAL_BALANCE"].ToString()));
              _description.AppendLine("   Won amount:          @p0".Replace("@p0", _row["PS_WON_AMOUNT"].ToString()));
              _description.AppendLine("   Played count:        @p0".Replace("@p0", _row["PS_PLAYED_COUNT"].ToString()));

              if ((Decimal)_row["UNBALANCE"] > 0)
        {
                _description.AppendLine("   Unbalance:           @p0".Replace("@p0", _row["UNBALANCE"].ToString()));
        }
              if ((Decimal)_row["AVG_BET"] > 0)
         {
                _description.AppendLine("   Avg bet:             @p0".Replace("@p0", _row["AVG_BET"].ToString()));
       }
              Log.Warning(_description.ToString());

              if (!Alarm.Register(AlarmSourceCode.Cashier,
                                  _terminal_id,
                                  _cashier_info,
                                  (UInt32)AlarmCode.User_PlaSessionDiscardedToGiveDrawNumbers,
                                  _description.ToString(),
                                  AlarmSeverity.Warning,
                                  DateTime.MinValue,
                                  SqlTrx))
        {
          return false;
        }
            }
          }
        }

        Accounts.GetCashInFromDate(AccountId, _lower_limit_datetime, out AllCashIn, SqlTrx);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // GetSpentGroupByProviderHour

    //------------------------------------------------------------------------------
    // PURPOSE: Returns a new instance of DrawTicket for the indicated Numbers of the indicated Draw
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Draw
    //          - NumNumbers
    //          - Card
    //          - OperationId
    //          - PerPoints
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - new instance with the ticket information if it's inserted
    //      - Null if the ticket cannot be inserted
    // 
    //   NOTES:
    public static Boolean CreateTicket(DrawTicket Draw,
                                           DateTime AwardedDay,
                                           Int32 NumNumbers,
                                           CardData Card,
                                           Boolean PerPoints,
                                           Int64 OperationId,
                                           ref DrawTicket DrawTicket,
                                           SqlTransaction SqlTrx)
    {
      //DrawTicket DrawTicket;
      StringBuilder _sql_txt;
      SqlParameter _sql_parameter;
      Int64 _max_number;

      DrawTicket = new DrawTicket();

      DrawTicket.CreationCode = DrawTicketLogic.GetTicketCodes.ERROR;

      DrawTicket.CreationDate = WGDB.Now;
      DrawTicket.DrawId = Draw.DrawId;
      DrawTicket.DrawType = Draw.DrawType;
      DrawTicket.AccountId = Card.AccountId;
      DrawTicket.FirstDrawNum = 0;
      DrawTicket.LastDrawNum = 0;
      DrawTicket.HolderName = Card.PlayerTracking.HolderName;
      DrawTicket.TrackData = Card.TrackData;
      DrawTicket.DrawName = Draw.DrawName;
      DrawTicket.Card = Card;

      DrawTicket.LimitPerDayAndPlayer = Draw.LimitPerDayAndPlayer;
      DrawTicket.MaxNumber = Draw.MaxNumber;

      DrawTicket.Detail1 = Draw.Detail1;
      DrawTicket.Detail2 = Draw.Detail2;
      DrawTicket.Detail3 = Draw.Detail3;
      DrawTicket.Footer = Draw.Footer;
      DrawTicket.Header = Draw.Header;

      // RCI 21-DEC-2011: Bingo format flag
      DrawTicket.HasBingoFormat = Draw.HasBingoFormat;

      try
      {
        // Increase the 'last number' according to the number of numbers.
        // If last_number > 0, it means the draw is on.
        DrawTicket.CreationCode = DrawTicketLogic.GetTicketCodes.LAST_NUMBER_ERROR;

        try
        {
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("UPDATE   DRAWS ");
          _sql_txt.AppendLine("   SET   DR_LAST_NUMBER = CASE WHEN DR_LAST_NUMBER = -1 ");
          _sql_txt.AppendLine("                               THEN DR_INITIAL_NUMBER - 1 ");
          _sql_txt.AppendLine("                               ELSE DR_LAST_NUMBER END + @NumNumbers ");
          _sql_txt.AppendLine(" WHERE   DR_ID          = @DrawId ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@NumNumbers", SqlDbType.BigInt).Value = NumNumbers;
            _sql_cmd.Parameters.Add("@DrawId", SqlDbType.BigInt).Value = DrawTicket.DrawId;

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Error("CreateTicket. Cannot update DRAWS.DR_LAST_NUMBER with ID + " + DrawTicket.DrawId + " .");
              
              return false;
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          return false;
        }

        // Get the numbers for the draw.
        DrawTicket.CreationCode = DrawTicketLogic.GetTicketCodes.NUMBERS_ERROR;
        _max_number = Int64.MaxValue;

        try
        {
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("SELECT   DR_MAX_NUMBER ");
          _sql_txt.AppendLine("       , DR_LAST_NUMBER ");
          _sql_txt.AppendLine("  FROM   DRAWS ");
          _sql_txt.AppendLine(" WHERE   DR_ID = @DrawId ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@DrawId", SqlDbType.BigInt).Value = DrawTicket.DrawId;

            using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
            {
              if (_sql_reader.Read())
              {
                _max_number = _sql_reader.GetInt64(0);
                DrawTicket.LastDrawNum = _sql_reader.GetInt64(1);
                DrawTicket.FirstDrawNum = DrawTicket.LastDrawNum - NumNumbers + 1;
              }
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          return false;
        }

        if (DrawTicket.LastDrawNum > _max_number)
        {
          DrawTicket.CreationCode = DrawTicketLogic.GetTicketCodes.DRAW_MAX_REACHED;

          return false;
        }

        // Insert the 'ticket'
        // DLL 28-NOV-2013: insert DT_AWARDED_DAY
        DrawTicket.CreationCode = DrawTicketLogic.GetTicketCodes.TICKET_ERROR;

        try
        {
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("INSERT INTO   DRAW_TICKETS ( DT_CREATED ");
          _sql_txt.AppendLine("                           , DT_DRAW_ID ");
          _sql_txt.AppendLine("                           , DT_ACCOUNT_ID ");
          _sql_txt.AppendLine("                           , DT_FIRST_NUMBER ");
          _sql_txt.AppendLine("                           , DT_LAST_NUMBER ");
          _sql_txt.AppendLine("                           , DT_PER_POINTS ");
          _sql_txt.AppendLine("                           , DT_AWARDED_DAY ");
          _sql_txt.AppendLine("                           ) ");
          _sql_txt.AppendLine("                   VALUES  ( @CreationDate ");
          _sql_txt.AppendLine("                           , @DrawId ");
          _sql_txt.AppendLine("                           , @AccountId ");
          _sql_txt.AppendLine("                           , @FirstNumber ");
          _sql_txt.AppendLine("                           , @LastNumber ");
          _sql_txt.AppendLine("                           , @PerPoints ");
          _sql_txt.AppendLine("                           , @AwardedDay ");
          _sql_txt.AppendLine("                           ) ");
          _sql_txt.AppendLine("   SET       @TicketNumber = SCOPE_IDENTITY()");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@CreationDate", SqlDbType.DateTime).Value = DrawTicket.CreationDate;
            _sql_cmd.Parameters.Add("@DrawId", SqlDbType.BigInt).Value = DrawTicket.DrawId;
            _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = DrawTicket.AccountId;
            _sql_cmd.Parameters.Add("@FirstNumber", SqlDbType.BigInt).Value = DrawTicket.FirstDrawNum;
            _sql_cmd.Parameters.Add("@LastNumber", SqlDbType.BigInt).Value = DrawTicket.LastDrawNum;
            _sql_cmd.Parameters.Add("@PerPoints", SqlDbType.Bit).Value = PerPoints;
            _sql_cmd.Parameters.Add("@AwardedDay", SqlDbType.DateTime).Value = AwardedDay;

            _sql_parameter = _sql_cmd.Parameters.Add("@TicketNumber", SqlDbType.BigInt);
            _sql_parameter.Direction = ParameterDirection.Output;

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Error("DrawTickets. DRAW_TICKETS cannot be inserted");

              return false;
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          return false;
        }

        DrawTicket.TicketNumber = (Int64)_sql_parameter.Value;

        if (!DrawTicketLogic.VoucherGeneration(DrawTicket, SqlTrx))
        {
          Log.Error("CreateTicket()==> VoucherGeneration(). TicketNumber:  + " + DrawTicket.TicketNumber + " .");

          return false;
        }

        if (!DrawTicket.Voucher.Save(OperationId, SqlTrx))
        {
          Log.Error("CreateTicket()==> Voucher.Save(). OperationId:  + " + OperationId + " .");

          return false;
        }

        //DrawTicket.CreationCode = DrawTicketLogic.GetTicketCodes.ERROR;

        if (!DrawTicketLogic.SaveVoucherId(DrawTicket, SqlTrx))
        {
          //return DrawTicket;
          Log.Error("CreateTicket()==> Voucher.SaveVoucherId(). OperationId:  + " + OperationId + " .");

          return false;
        }

        // SSC 06-JUN-2012: Moved insert movement account (CommonInsertCardMovement) and create cashier movement (CreateCashierMovement)
        // to GenerateTickets method (in cashier) and Request method. Here CreationCode is OK.
        DrawTicket.CreationCode = DrawTicketLogic.GetTicketCodes.OK;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CreateTicket

    public static DataTable GetGroupByOperations(Int64 CardId, DrawTicket Draw)
    {
      const String TYPE_RECHARGE = "R";
      const String TYPE_PLAYS = "P";

      DataTable _dt_oper;
      StringBuilder _str;
      Int32 _hours_before;
      DateTime _lower_limit_datetime;
      DataTable _dt_tracking;
      DataRow _dr;

      _dt_oper = new DataTable();
      _str = new System.Text.StringBuilder();

      _str.AppendLine(" SELECT * FROM ( ");

      _str.AppendLine(" SELECT   PS_FINISHED [DATETIME] ");
      _str.AppendLine("        , 0 RECHARGE ");
      _str.AppendLine("        , PS_PLAYED_AMOUNT PLAYED ");
      _str.AppendLine("        , CASE WHEN PS_PLAYED_AMOUNT > PS_WON_AMOUNT THEN PS_PLAYED_AMOUNT - PS_WON_AMOUNT ELSE 0 END SPENT ");
      _str.AppendLine("        , TE_PROVIDER_ID PROVIDER ");
      _str.AppendLine("        , @TYPE_PLAYS [TYPE] ");
      _str.AppendLine("   FROM   PLAY_SESSIONS INNER JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID ");
      _str.AppendLine("  WHERE   PS_ACCOUNT_ID = @ACCOUNT_ID ");
      _str.AppendLine("    AND   PS_FINISHED  >= @INI_DATE ");
      _str.AppendLine("    AND   PS_STATUS    <> @PLAYSESSIONOPENED ");

      _str.AppendLine(" UNION ");

      _str.AppendLine(" SELECT   AO_DATETIME [DATETIME] ");
      _str.AppendLine("        , AO_AMOUNT RECHARGE ");
      _str.AppendLine("        , 0 PLAYED ");
      _str.AppendLine("        , 0 SPEND ");
      _str.AppendLine("        , NULL PROVIDER ");
      _str.AppendLine("        , @TYPE_RECHARGE [TYPE] ");
      _str.AppendLine("   FROM   ACCOUNT_OPERATIONS ");
      _str.AppendLine("  WHERE   AO_ACCOUNT_ID = @ACCOUNT_ID ");
      _str.AppendLine("    AND   AO_DATETIME  >= @INI_DATE ");
      _str.AppendLine("    AND   AO_CODE      IN ( @pOpCashIn, @pOpCashInPromo, @pMBCashIn, @pMBCashInPromo, @pNACashIn, @pNACashInPromo ) ");
      // RCI 15-NOV-2013: Only take in account Operations with UNDO_STATUS = None
      _str.AppendLine("    AND   ISNULL(AO_UNDO_STATUS, @pOperationUndoNone) = @pOperationUndoNone ");
      _str.AppendLine("          ) AS X ");

      _str.AppendLine("  WHERE ( RECHARGE >= @MIN_RECHARGE AND [TYPE] = @TYPE_RECHARGE) ");
      _str.AppendLine("          OR [TYPE] = @TYPE_PLAYS  ");
      _str.AppendLine("ORDER BY [DATETIME] ");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_str.ToString(), _db_trx.SqlTransaction.Connection))
        {
          _hours_before = GeneralParam.GetInt32("Cashier", "Draws.HoursBeforeTodayOpening");

          // Round to opening date. F.ex: If the GP says 8 hours, we go to the opening hour.
          _lower_limit_datetime = Misc.Opening(Misc.TodayOpening().AddHours(-_hours_before));

          _sql_cmd.Transaction = _db_trx.SqlTransaction;
          _sql_cmd.Parameters.Add("@ACCOUNT_ID", SqlDbType.BigInt).Value = CardId;
          _sql_cmd.Parameters.Add("@INI_DATE", SqlDbType.DateTime).Value = _lower_limit_datetime;
          _sql_cmd.Parameters.Add("@pOpCashIn", SqlDbType.Int).Value = (Int32)OperationCode.CASH_IN;
          _sql_cmd.Parameters.Add("@pOpCashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.PROMOTION;
          _sql_cmd.Parameters.Add("@pMBCashIn", SqlDbType.Int).Value = (Int32)OperationCode.MB_CASH_IN;
          _sql_cmd.Parameters.Add("@pMBCashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.MB_PROMOTION;
          //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
          _sql_cmd.Parameters.Add("@pNACashIn", SqlDbType.Int).Value = (Int32)OperationCode.NA_CASH_IN;
          _sql_cmd.Parameters.Add("@pNACashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.NA_PROMOTION;
          _sql_cmd.Parameters.Add("@MIN_RECHARGE", SqlDbType.Decimal).Value = Draw.MinCashIn;
          _sql_cmd.Parameters.Add("@PLAYSESSIONOPENED", SqlDbType.Int).Value = 0;
          _sql_cmd.Parameters.Add("@TYPE_RECHARGE", SqlDbType.NVarChar).Value = TYPE_RECHARGE;
          _sql_cmd.Parameters.Add("@TYPE_PLAYS", SqlDbType.NVarChar).Value = TYPE_PLAYS;
          _sql_cmd.Parameters.Add("@pOperationUndoNone", SqlDbType.Int).Value = (Int32)UndoStatus.None;

          using (SqlDataAdapter _sql_adap = new SqlDataAdapter(_sql_cmd))
          {
            _sql_adap.Fill(_dt_oper);
          }
        }
      }

      _dt_tracking = new DataTable();
      _dt_tracking.Columns.Add("RECHARGE", Type.GetType("System.Decimal"));
      _dt_tracking.Columns.Add("PLAYED", Type.GetType("System.Decimal"));
      _dt_tracking.Columns.Add("SPENT", Type.GetType("System.Decimal"));
      _dt_tracking.Columns.Add("DATETIME", Type.GetType("System.DateTime"));

      _dr = null;

      foreach (DataRow _row in _dt_oper.Rows)
      {
        if ((String)_row["TYPE"] == TYPE_RECHARGE)
        {
          _dr = _dt_tracking.NewRow();
          _dr["RECHARGE"] = _row["RECHARGE"];
          _dr["PLAYED"] = 0;
          _dr["SPENT"] = 0;
          _dr["DATETIME"] = _row["DATETIME"];
          _dt_tracking.Rows.Add(_dr);
        }

        if ((String)_row["TYPE"] == TYPE_PLAYS)
        {
          if (_dt_tracking.Rows.Count > 0 && Draw.ProviderList.Contains(((String)_row["PROVIDER"]).ToUpper()))
          {
            _dr["PLAYED"] = (Decimal)_dr["PLAYED"] + (Decimal)_row["PLAYED"];
            _dr["SPENT"] = (Decimal)_dr["SPENT"] + (Decimal)_row["SPENT"];
          }
        }
      }

      return _dt_tracking;
    }

    public static Dictionary<DateTime, NumbersPerDay> GetNumbersCashInPosteriori(DrawTicket Draw, CardData.PlayerTrackingData Card, DataTable DtTracking)
    {
      Decimal _min_played;
      Decimal _min_spent;
      Int64 _numbers;
      Decimal _cash_in_offer;
      DateTime _opening_day;
      Dictionary<DateTime, NumbersPerDay> _dic_numbers_per_day;
      NumbersPerDay _num_per_day;

      _dic_numbers_per_day = new Dictionary<DateTime, NumbersPerDay>();
      _min_played = Draw.MinPlayedPct * Draw.MinCashIn / 100.0m;
      _min_spent = Draw.MinSpentPct * Draw.MinCashIn / 100.0m;

      foreach (DataRow _row in DtTracking.Rows)
      {
        //DLL 28-NOV-2013: Separate all movements per opening day
        _opening_day = Misc.Opening((DateTime)_row["DATETIME"]);

        _cash_in_offer = Draw.DrawOfferList.GetFactor(Card.HolderGender,
                                                      (DateTime)_row["DATETIME"],
                                                      (DateTime)_row["DATETIME"]);

        if ((Draw.MinPlayedPct > 0 && (Decimal)_row["PLAYED"] >= _min_played)
            || (Draw.MinSpentPct > 0 && (Decimal)_row["SPENT"] >= _min_spent))
        {

          if (_dic_numbers_per_day.ContainsKey(_opening_day))
          {
            _num_per_day = _dic_numbers_per_day[_opening_day];
          }
          else
          {
            _num_per_day = new NumbersPerDay();
          }

          _numbers = (Int64)((Decimal)_row["RECHARGE"] / Draw.NumberPrice);
          // Apply offer
          _numbers = (Int64)(_numbers * _cash_in_offer);

          if (Draw.LimitPerOperation > 0)
          {
            // Max tickets per operation 
            _numbers = Math.Min(_numbers, Draw.LimitPerOperation);
          }

          // Numbers for this 'cash in' operation
          _num_per_day.NumNumbers += _numbers;

          _dic_numbers_per_day[_opening_day] = _num_per_day;
        }
      }

      return _dic_numbers_per_day;
    } // GetNumbersCashInPosteriori

  }  // Draws

}
