﻿namespace WSI.Labels
{
  partial class PrintLabelSettingsDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label3 = new System.Windows.Forms.Label();
      this.UD_lbl_width = new System.Windows.Forms.NumericUpDown();
      this.UD_lbl_height = new System.Windows.Forms.NumericUpDown();
      this.UD_rows = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.UD_cols = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.label16 = new System.Windows.Forms.Label();
      this.UD_padding = new System.Windows.Forms.NumericUpDown();
      this.label17 = new System.Windows.Forms.Label();
      this.label18 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.cmb_paper_size = new System.Windows.Forms.ComboBox();
      this.lbl_paper_size = new System.Windows.Forms.Label();
      this.cmb_paper_source = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.cmb_printer = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.UD_oy = new System.Windows.Forms.NumericUpDown();
      this.label11 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.label13 = new System.Windows.Forms.Label();
      this.label15 = new System.Windows.Forms.Label();
      this.label14 = new System.Windows.Forms.Label();
      this.UD_ox = new System.Windows.Forms.NumericUpDown();
      this.btn_cancel = new System.Windows.Forms.Button();
      this.btn_accept = new System.Windows.Forms.Button();
      this.btn_default = new System.Windows.Forms.Button();
      this.label19 = new System.Windows.Forms.Label();
      this.UD_copies = new System.Windows.Forms.NumericUpDown();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      ((System.ComponentModel.ISupportInitialize)(this.UD_lbl_width)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_lbl_height)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_rows)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_cols)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_padding)).BeginInit();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.UD_oy)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_ox)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_copies)).BeginInit();
      this.groupBox1.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(20, 139);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(57, 13);
      this.label3.TabIndex = 3;
      this.label3.Text = "Label size:";
      // 
      // UD_lbl_width
      // 
      this.UD_lbl_width.DecimalPlaces = 1;
      this.UD_lbl_width.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
      this.UD_lbl_width.Location = new System.Drawing.Point(115, 137);
      this.UD_lbl_width.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.UD_lbl_width.Minimum = new decimal(new int[] {
            38,
            0,
            0,
            0});
      this.UD_lbl_width.Name = "UD_lbl_width";
      this.UD_lbl_width.Size = new System.Drawing.Size(59, 20);
      this.UD_lbl_width.TabIndex = 3;
      this.UD_lbl_width.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.UD_lbl_width.Value = new decimal(new int[] {
            105,
            0,
            0,
            0});
      // 
      // UD_lbl_height
      // 
      this.UD_lbl_height.DecimalPlaces = 1;
      this.UD_lbl_height.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
      this.UD_lbl_height.Location = new System.Drawing.Point(204, 137);
      this.UD_lbl_height.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.UD_lbl_height.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
      this.UD_lbl_height.Name = "UD_lbl_height";
      this.UD_lbl_height.Size = new System.Drawing.Size(59, 20);
      this.UD_lbl_height.TabIndex = 4;
      this.UD_lbl_height.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.UD_lbl_height.Value = new decimal(new int[] {
            99,
            0,
            0,
            0});
      // 
      // UD_rows
      // 
      this.UD_rows.Location = new System.Drawing.Point(205, 85);
      this.UD_rows.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
      this.UD_rows.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.UD_rows.Name = "UD_rows";
      this.UD_rows.Size = new System.Drawing.Size(59, 20);
      this.UD_rows.TabIndex = 2;
      this.UD_rows.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.UD_rows.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(180, 141);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(18, 13);
      this.label6.TabIndex = 10;
      this.label6.Text = " x ";
      // 
      // UD_cols
      // 
      this.UD_cols.Location = new System.Drawing.Point(116, 85);
      this.UD_cols.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
      this.UD_cols.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.UD_cols.Name = "UD_cols";
      this.UD_cols.Size = new System.Drawing.Size(59, 20);
      this.UD_cols.TabIndex = 1;
      this.UD_cols.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.UD_cols.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(181, 87);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(18, 13);
      this.label7.TabIndex = 13;
      this.label7.Text = " x ";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(202, 69);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(34, 13);
      this.label4.TabIndex = 14;
      this.label4.Text = "Rows";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(113, 69);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(47, 13);
      this.label8.TabIndex = 15;
      this.label8.Text = "Columns";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(201, 121);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(38, 13);
      this.label9.TabIndex = 17;
      this.label9.Text = "Height";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(112, 121);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(35, 13);
      this.label10.TabIndex = 16;
      this.label10.Text = "Width";
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Location = new System.Drawing.Point(20, 87);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(69, 13);
      this.label16.TabIndex = 10;
      this.label16.Text = "Sheet layout:";
      // 
      // UD_padding
      // 
      this.UD_padding.DecimalPlaces = 1;
      this.UD_padding.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
      this.UD_padding.Location = new System.Drawing.Point(358, 137);
      this.UD_padding.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
      this.UD_padding.Name = "UD_padding";
      this.UD_padding.Size = new System.Drawing.Size(59, 20);
      this.UD_padding.TabIndex = 5;
      this.UD_padding.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.UD_padding.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Location = new System.Drawing.Point(308, 141);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(46, 13);
      this.label17.TabIndex = 26;
      this.label17.Text = "Padding";
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Location = new System.Drawing.Point(423, 141);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(15, 13);
      this.label18.TabIndex = 28;
      this.label18.Text = "%";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(269, 141);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(26, 13);
      this.label5.TabIndex = 29;
      this.label5.Text = "mm.";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.cmb_paper_size);
      this.groupBox2.Controls.Add(this.lbl_paper_size);
      this.groupBox2.Controls.Add(this.UD_rows);
      this.groupBox2.Controls.Add(this.label5);
      this.groupBox2.Controls.Add(this.label3);
      this.groupBox2.Controls.Add(this.label18);
      this.groupBox2.Controls.Add(this.UD_lbl_width);
      this.groupBox2.Controls.Add(this.UD_padding);
      this.groupBox2.Controls.Add(this.UD_lbl_height);
      this.groupBox2.Controls.Add(this.label17);
      this.groupBox2.Controls.Add(this.label6);
      this.groupBox2.Controls.Add(this.label16);
      this.groupBox2.Controls.Add(this.UD_cols);
      this.groupBox2.Controls.Add(this.label7);
      this.groupBox2.Controls.Add(this.label4);
      this.groupBox2.Controls.Add(this.label8);
      this.groupBox2.Controls.Add(this.label10);
      this.groupBox2.Controls.Add(this.label9);
      this.groupBox2.Location = new System.Drawing.Point(9, 179);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(465, 167);
      this.groupBox2.TabIndex = 1;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Dimensions";
      // 
      // cmb_paper_size
      // 
      this.cmb_paper_size.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_paper_size.Enabled = false;
      this.cmb_paper_size.FormattingEnabled = true;
      this.cmb_paper_size.Location = new System.Drawing.Point(116, 30);
      this.cmb_paper_size.Name = "cmb_paper_size";
      this.cmb_paper_size.Size = new System.Drawing.Size(321, 21);
      this.cmb_paper_size.TabIndex = 0;
      // 
      // lbl_paper_size
      // 
      this.lbl_paper_size.Location = new System.Drawing.Point(20, 33);
      this.lbl_paper_size.Name = "lbl_paper_size";
      this.lbl_paper_size.Size = new System.Drawing.Size(90, 13);
      this.lbl_paper_size.TabIndex = 43;
      this.lbl_paper_size.Text = "Paper Size:";
      // 
      // cmb_paper_source
      // 
      this.cmb_paper_source.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_paper_source.FormattingEnabled = true;
      this.cmb_paper_source.Location = new System.Drawing.Point(113, 68);
      this.cmb_paper_source.Name = "cmb_paper_source";
      this.cmb_paper_source.Size = new System.Drawing.Size(321, 21);
      this.cmb_paper_source.TabIndex = 1;
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(17, 71);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(90, 13);
      this.label2.TabIndex = 41;
      this.label2.Text = "Paper Source:";
      // 
      // cmb_printer
      // 
      this.cmb_printer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_printer.FormattingEnabled = true;
      this.cmb_printer.Location = new System.Drawing.Point(113, 31);
      this.cmb_printer.Name = "cmb_printer";
      this.cmb_printer.Size = new System.Drawing.Size(321, 21);
      this.cmb_printer.TabIndex = 0;
      this.cmb_printer.SelectedIndexChanged += new System.EventHandler(this.cmb_printer_SelectedIndexChanged);
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(17, 34);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(90, 13);
      this.label1.TabIndex = 39;
      this.label1.Text = "Name:";
      // 
      // UD_oy
      // 
      this.UD_oy.DecimalPlaces = 1;
      this.UD_oy.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
      this.UD_oy.Location = new System.Drawing.Point(202, 115);
      this.UD_oy.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
      this.UD_oy.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            -2147483648});
      this.UD_oy.Name = "UD_oy";
      this.UD_oy.Size = new System.Drawing.Size(59, 20);
      this.UD_oy.TabIndex = 3;
      this.UD_oy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.UD_oy.Value = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(199, 97);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(26, 13);
      this.label11.TabIndex = 37;
      this.label11.Text = "Top";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(110, 97);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(25, 13);
      this.label12.TabIndex = 36;
      this.label12.Text = "Left";
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(178, 119);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(18, 13);
      this.label13.TabIndex = 35;
      this.label13.Text = " x ";
      // 
      // label15
      // 
      this.label15.Location = new System.Drawing.Point(17, 119);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(90, 13);
      this.label15.TabIndex = 32;
      this.label15.Text = "Print Offset:";
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(267, 119);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(26, 13);
      this.label14.TabIndex = 34;
      this.label14.Text = "mm.";
      // 
      // UD_ox
      // 
      this.UD_ox.DecimalPlaces = 1;
      this.UD_ox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
      this.UD_ox.Location = new System.Drawing.Point(113, 115);
      this.UD_ox.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
      this.UD_ox.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            -2147483648});
      this.UD_ox.Name = "UD_ox";
      this.UD_ox.Size = new System.Drawing.Size(59, 20);
      this.UD_ox.TabIndex = 2;
      this.UD_ox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.UD_ox.Value = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
      // 
      // btn_cancel
      // 
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.Location = new System.Drawing.Point(367, 403);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(110, 29);
      this.btn_cancel.TabIndex = 4;
      this.btn_cancel.Text = "Close";
      this.btn_cancel.UseVisualStyleBackColor = true;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_accept
      // 
      this.btn_accept.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btn_accept.Location = new System.Drawing.Point(251, 403);
      this.btn_accept.Name = "btn_accept";
      this.btn_accept.Size = new System.Drawing.Size(110, 29);
      this.btn_accept.TabIndex = 3;
      this.btn_accept.Text = "Ok";
      this.btn_accept.UseVisualStyleBackColor = true;
      this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click);
      // 
      // btn_default
      // 
      this.btn_default.Location = new System.Drawing.Point(12, 403);
      this.btn_default.Name = "btn_default";
      this.btn_default.Size = new System.Drawing.Size(110, 29);
      this.btn_default.TabIndex = 5;
      this.btn_default.Text = "Default";
      this.btn_default.UseVisualStyleBackColor = true;
      this.btn_default.Click += new System.EventHandler(this.btn_default_Click);
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Location = new System.Drawing.Point(6, 21);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(93, 13);
      this.label19.TabIndex = 43;
      this.label19.Text = "Number of copies:";
      // 
      // UD_copies
      // 
      this.UD_copies.Location = new System.Drawing.Point(102, 19);
      this.UD_copies.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
      this.UD_copies.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.UD_copies.Name = "UD_copies";
      this.UD_copies.Size = new System.Drawing.Size(59, 20);
      this.UD_copies.TabIndex = 0;
      this.UD_copies.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.UD_copies.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.UD_ox);
      this.groupBox1.Controls.Add(this.cmb_paper_source);
      this.groupBox1.Controls.Add(this.label14);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.label15);
      this.groupBox1.Controls.Add(this.cmb_printer);
      this.groupBox1.Controls.Add(this.label13);
      this.groupBox1.Controls.Add(this.label12);
      this.groupBox1.Controls.Add(this.UD_oy);
      this.groupBox1.Controls.Add(this.label11);
      this.groupBox1.Location = new System.Drawing.Point(12, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(465, 157);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Printer";
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.label19);
      this.groupBox3.Controls.Add(this.UD_copies);
      this.groupBox3.Location = new System.Drawing.Point(12, 352);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(465, 45);
      this.groupBox3.TabIndex = 2;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Copies";
      // 
      // PrintLabelSettingsDialog
      // 
      this.AcceptButton = this.btn_accept;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(486, 451);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.btn_default);
      this.Controls.Add(this.btn_accept);
      this.Controls.Add(this.btn_cancel);
      this.Controls.Add(this.groupBox2);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "PrintLabelSettingsDialog";
      this.Text = "Print Label Preferences ...";
      this.Load += new System.EventHandler(this.PrintLabelSettingsDialog_Load);
      ((System.ComponentModel.ISupportInitialize)(this.UD_lbl_width)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_lbl_height)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_rows)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_cols)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_padding)).EndInit();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.UD_oy)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_ox)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.UD_copies)).EndInit();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown UD_lbl_width;
    private System.Windows.Forms.NumericUpDown UD_lbl_height;
    private System.Windows.Forms.NumericUpDown UD_rows;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown UD_cols;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.NumericUpDown UD_padding;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.ComboBox cmb_paper_source;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.ComboBox cmb_printer;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown UD_oy;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.NumericUpDown UD_ox;
    private System.Windows.Forms.Button btn_cancel;
    private System.Windows.Forms.Button btn_accept;
    private System.Windows.Forms.Button btn_default;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.NumericUpDown UD_copies;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.ComboBox cmb_paper_size;
    private System.Windows.Forms.Label lbl_paper_size;
  }
}