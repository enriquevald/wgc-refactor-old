//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Groups.cs
// 
//   DESCRIPTION: Class to manage groups
// 
//        AUTHOR: Rub�n Brea
// 
// CREATION DATE: 25-JUN-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-JUN-2013 RBG    Initial version
// 27-MAR-2014 MPO    Performance improved for terminal generations
// 04-DEC-2014 DHA    Added groups personalization
// 27-MAR-2014 DCS    Update exploit with the new development of cloned terminals
// 17-AUG-2015 YNM    Fixed Bug TFS-3580: added Terminal_id to m_elements from uc_terminals_group_filter
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public partial class Groups
  {

    #region CONSTANTS

    public const Int64 ID_ZONE_NO_SMOKING = 0;
    public const Int64 ID_ZONE_SMOKING = 1;
    public const Int64 ID_ROOT_ELEMENT = -1;

    public const String ELEMENTS_COLUMN_MASTER_ID = "MASTER_ID";
    public const String ELEMENTS_COLUMN_ID = "ID";
    public const String ELEMENTS_COLUMN_TYPE = "TYPE";
    public const String ELEMENTS_COLUMN_NAME = "NAME";
    public const String ELEMENTS_COLUMN_NODE_TYPE = "NODE_TYPE";
    public const String ELEMENTS_COLUMN_ENABLED = "ENABLED";
    public const String ELEMENTS_COLUMN_PARENT_ID = "PARENT_ID";
    public const String ELEMENTS_COLUMN_PARENT_TYPE = "PARENT_TYPE";
    public const String ELEMENTS_COLUMN_TERMINAL_ID = "TERMINAL_ID";

    public const String GROUP_COLUMN_ID = "GROUP_ID";
    public const String GROUP_COLUMN_ENABLED = "GROUP_ENABLED";

    public enum GROUP_NODE_ELEMENT
    {
      PROVIDERS = 1, // ROOT
      PROVIDER = 2,

      ZONES = 3,     // ROOT
      ZONE = 4,
      AREAS = 5,
      BANKS = 6,

      GROUPS = 7,    // ROOT
      GROUP = 8,

      ALL = 9,       // ROOT

      TERMINALS = 20,
    }

    public enum TERMINAL_SEARCH
    {
      ALL = 1,
      BY_PROVIDER = 2,
      BY_BANK = 3,
      FILTERED = 4
    }

    #endregion

    #region CLASS

    public class INPUT_GROUP_PARAMS
    {
      public Int64 ParentId;
      public GROUP_ELEMENT_TYPE ParentType;
      public Int64 ElementId;
      public GROUP_ELEMENT_TYPE ElementType;
      public Boolean ElementEnabled;

      public INPUT_GROUP_PARAMS()
      {
        ElementId = 0;
        ElementType = 0;
        ElementEnabled = false;
      }

    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Get root elements.
    //
    //  PARAMS:
    //      - INPUT:
    //          - ContentType: GROUP_CONTENT_TYPE
    //      - OUTPUT:
    //          - Elements: DataTable
    //
    // RETURNS: Boolean
    //
    public static Boolean GetRootElements(GROUP_CONTENT_TYPE ContentType, out DataTable Elements)
    {
      return GetRootElements(ContentType, out Elements,  null);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get root elements.
    //
    //  PARAMS:
    //      - INPUT:
    //          - ContentType: GROUP_CONTENT_TYPE
    //      - OUTPUT:
    //          - Elements: DataTable
    //
    // RETURNS: Boolean
    //
    public static Boolean GetRootElements(GROUP_CONTENT_TYPE ContentType, out DataTable Elements, List<Groups.GROUP_NODE_ELEMENT> ShowCustomNodes)
    {
      DataTable _dt_aux;

      Elements = new DataTable();

      try
      {
        switch (ContentType)
        {
          case GROUP_CONTENT_TYPE.TERMINALS:

            if (ShowCustomNodes == null || ShowCustomNodes.Count == 0 || ShowCustomNodes.Contains(GROUP_NODE_ELEMENT.PROVIDER))
            {
              CreateRoot(GROUP_NODE_ELEMENT.PROVIDER, out _dt_aux);
              Elements.Merge(_dt_aux);
            }

            if (ShowCustomNodes == null || ShowCustomNodes.Count == 0 || ShowCustomNodes.Contains(GROUP_NODE_ELEMENT.ZONE))
            {
              CreateRoot(GROUP_NODE_ELEMENT.ZONE, out _dt_aux);
              Elements.Merge(_dt_aux);
            }

            if (ShowCustomNodes == null || ShowCustomNodes.Count == 0 || ShowCustomNodes.Contains(GROUP_NODE_ELEMENT.GROUP))
            {
              CreateRoot(GROUP_NODE_ELEMENT.GROUP, out _dt_aux);
              Elements.Merge(_dt_aux);
            }

            if (ShowCustomNodes == null || ShowCustomNodes.Count == 0 || ShowCustomNodes.Contains(GROUP_NODE_ELEMENT.ALL))
            {
              CreateRoot(GROUP_NODE_ELEMENT.ALL, out _dt_aux);
              Elements.Merge(_dt_aux);
            }

            break;

          case GROUP_CONTENT_TYPE.CLIENTS:
          default:

            return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    } // GetRootElements

    //------------------------------------------------------------------------------
    // PURPOSE: Get a terminal distribution of a specific type.
    //
    //  PARAMS:
    //      - INPUT:
    //          - ElementType: Common.GROUP_ELEMENT_TYPE
    //          - ElementId: Int32
    //      - OUTPUT:
    //          - DtElements: DataTable
    //
    // RETURNS:
    //
    public static Boolean GetElements(GROUP_NODE_ELEMENT ElementType, Int32 ElementId, out DataTable DtElements)
    {
      StringBuilder _sb;
      DataTable _dt_aux;

      DtElements = CreateElementTable(false);
      DtElements.PrimaryKey = null;      
      
      _dt_aux = null;
      _sb = null;

      //ELEMENTS_COLUMN_TYPE = "TYPE";
      //ELEMENTS_COLUMN_NAME = "NAME";
      //ELEMENTS_COLUMN_ID = "ID";
      //ELEMENTS_COLUMN_ENABLED = "ENABLED";
      //ELEMENTS_COLUMN_PARENT_ID = "PARENT_ID";
      //ELEMENTS_COLUMN_PARENT_TYPE = "PARENT_TYPE";

      switch (ElementType)
      {
        case GROUP_NODE_ELEMENT.GROUP:

          _sb = new StringBuilder();
          _sb.AppendLine("  SELECT   GR_ID           AS " + ELEMENTS_COLUMN_ID);
          _sb.AppendLine("         , GR_NAME         AS " + ELEMENTS_COLUMN_NAME);
          _sb.AppendLine("         , @pTypeGroup     AS " + ELEMENTS_COLUMN_TYPE);
          _sb.AppendLine("         , @pNodeTypeGroup AS " + ELEMENTS_COLUMN_NODE_TYPE);
          _sb.AppendLine("         , GR_ENABLED      AS " + ELEMENTS_COLUMN_ENABLED);
          _sb.AppendLine("         , 0               AS " + ELEMENTS_COLUMN_PARENT_ID);
          _sb.AppendLine("         , 0               AS " + ELEMENTS_COLUMN_TERMINAL_ID);
          _sb.AppendLine("    FROM   GROUPS ");
          _sb.AppendLine("ORDER BY   1 DESC ");

          break;

        case GROUP_NODE_ELEMENT.GROUPS:

          _sb = new StringBuilder();
          _sb.AppendLine("  SELECT   GE_ELEMENT_ID   AS " + ELEMENTS_COLUMN_ID);
          _sb.AppendLine("         , GE_ELEMENT_TYPE AS " + ELEMENTS_COLUMN_TYPE);
          _sb.AppendLine("         , CASE ");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 0 THEN @pAll");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 1 THEN (SELECT GR_NAME FROM GROUPS    WHERE GR_ID          = GE_ELEMENT_ID)");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 2 THEN (SELECT PV_NAME FROM PROVIDERS WHERE PV_ID          = GE_ELEMENT_ID)");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 3 THEN CASE WHEN ge_element_id = 0 THEN @pNoSmoking ELSE @pSmoking END");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 4 THEN (SELECT AR_NAME FROM AREAS     WHERE AR_AREA_ID     = GE_ELEMENT_ID)");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 5 THEN (SELECT BK_NAME FROM BANKS     WHERE BK_BANK_ID     = GE_ELEMENT_ID)");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 6 THEN (SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID = GE_ELEMENT_ID)");
          _sb.AppendLine("           END AS " + ELEMENTS_COLUMN_NAME);
          _sb.AppendLine("         , CASE ");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 0 THEN @pNodeTypeAll");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 1 THEN @pNodeTypeGroup");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 2 THEN @pNodeTypeProvider");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 3 THEN @pNodeTypeZone");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 4 THEN @pNodeTypeArea");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 5 THEN @pNodeTypeBank");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 6 THEN @pNodeTypeTerm");
          _sb.AppendLine("           END AS " + ELEMENTS_COLUMN_NODE_TYPE);
          _sb.AppendLine("         , CASE ");
          _sb.AppendLine("               WHEN ge_element_type = 1 THEN (SELECT GR_ENABLED FROM GROUPS WHERE GR_ID = ge_element_id) ");
          _sb.AppendLine("               ELSE 1 ");
          _sb.AppendLine("           END AS " + ELEMENTS_COLUMN_ENABLED);
          _sb.AppendLine("         , @pElementId  AS " + ELEMENTS_COLUMN_PARENT_ID);
          _sb.AppendLine("         , CASE ");
          _sb.AppendLine("               WHEN GE_ELEMENT_TYPE = 6 THEN (SELECT TLC_TERMINAL_ID FROM TERMINALS_LAST_CHANGED   ");
          _sb.AppendLine("                                               WHERE TLC_MASTER_ID = GE_ELEMENT_ID)");
          _sb.AppendLine("               ELSE 0");
          _sb.AppendLine("         END  AS " + ELEMENTS_COLUMN_TERMINAL_ID);
          _sb.AppendLine("    FROM   GROUP_ELEMENTS");
          _sb.AppendLine("   WHERE   GE_GROUP_ID = @pElementId ");
          _sb.AppendLine("ORDER BY   1 DESC");

          break;

        case GROUP_NODE_ELEMENT.PROVIDER:

          _sb = new StringBuilder();
          _sb.AppendLine("  SELECT   PV_ID              AS " + ELEMENTS_COLUMN_ID);
          _sb.AppendLine("         , PV_NAME            AS " + ELEMENTS_COLUMN_NAME);
          _sb.AppendLine("         , @pTypeProvider     AS " + ELEMENTS_COLUMN_TYPE);
          _sb.AppendLine("         , @pNodeTypeProvider AS " + ELEMENTS_COLUMN_NODE_TYPE);
          _sb.AppendLine("         , 1                  AS " + ELEMENTS_COLUMN_ENABLED);
          _sb.AppendLine("         , 0                  AS " + ELEMENTS_COLUMN_PARENT_ID);
          _sb.AppendLine("         , 0               AS " + ELEMENTS_COLUMN_TERMINAL_ID);
          _sb.AppendLine("    FROM   PROVIDERS ");
          _sb.AppendLine("ORDER BY   1 DESC ");

          break;

        case GROUP_NODE_ELEMENT.PROVIDERS:

          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   TE_MASTER_ID AS " + ELEMENTS_COLUMN_ID);
          _sb.AppendLine("      ,   TE_BASE_NAME        AS " + ELEMENTS_COLUMN_NAME);
          _sb.AppendLine("      ,   @pTypeTerm     AS " + ELEMENTS_COLUMN_TYPE);
          _sb.AppendLine("      ,   @pNodeTypeTerm AS " + ELEMENTS_COLUMN_NODE_TYPE);
          _sb.AppendLine("      ,   1              AS " + ELEMENTS_COLUMN_ENABLED);
          _sb.AppendLine("      ,   @pElementId    AS " + ELEMENTS_COLUMN_PARENT_ID);
          _sb.AppendLine("      ,   TE_TERMINAL_ID AS " + ELEMENTS_COLUMN_TERMINAL_ID);
          _sb.AppendLine("   FROM   TERMINALS_LAST_CHANGED ");
          _sb.AppendLine(" INNER JOIN   TERMINALS  ");
          _sb.AppendLine("         ON   TLC_MASTER_ID = TE_MASTER_ID ");
          _sb.AppendLine("  WHERE   TE_TYPE = 1 ");
          _sb.AppendLine("    AND   ISNULL(TE_SERVER_ID, 0) =  0");
          //_sb.AppendLine("    AND   TE_BLOCKED = 0");
          _sb.AppendLine("    AND   TE_STATUS  IN (0, 1) ");
          _sb.AppendLine("    AND   TE_TERMINAL_TYPE IN ( " + Misc.GamingTerminalTypeListToString(false) + " ) ");
          _sb.AppendLine("    AND   TE_PROV_ID = @pElementId ");
          

          break;

        case GROUP_NODE_ELEMENT.ZONE:

          CreateRoot(GROUP_NODE_ELEMENT.ZONES, out _dt_aux);

          break;

        case GROUP_NODE_ELEMENT.ZONES:

          _sb = new StringBuilder();
          _sb.AppendLine("  SELECT   AR_AREA_ID     AS " + ELEMENTS_COLUMN_ID);
          _sb.AppendLine("         , @pTypeArea     AS " + ELEMENTS_COLUMN_TYPE);
          _sb.AppendLine("         , AR_NAME        AS " + ELEMENTS_COLUMN_NAME);
          _sb.AppendLine("         , @pNodeTypeArea AS " + ELEMENTS_COLUMN_NODE_TYPE);
          _sb.AppendLine("         , 1              AS " + ELEMENTS_COLUMN_ENABLED);
          _sb.AppendLine("         , 0              AS " + ELEMENTS_COLUMN_PARENT_ID);
          _sb.AppendLine("         , 0               AS " + ELEMENTS_COLUMN_TERMINAL_ID);
          _sb.AppendLine("    FROM   AREAS ");
          _sb.AppendLine("   WHERE   AR_SMOKING = @pElementId ");
          _sb.AppendLine("ORDER BY   1 DESC");

          break;

        case GROUP_NODE_ELEMENT.AREAS:

          _sb = new StringBuilder();
          _sb.AppendLine("  SELECT   BK_BANK_ID      AS " + ELEMENTS_COLUMN_ID);
          _sb.AppendLine("         , @pTypeBank      AS " + ELEMENTS_COLUMN_TYPE);
          _sb.AppendLine("         , BK_NAME         AS " + ELEMENTS_COLUMN_NAME);
          _sb.AppendLine("         , @pNodeTypeBank  AS " + ELEMENTS_COLUMN_NODE_TYPE);
          _sb.AppendLine("         , 1               AS " + ELEMENTS_COLUMN_ENABLED);
          _sb.AppendLine("         , @pElementId     AS " + ELEMENTS_COLUMN_PARENT_ID);
          _sb.AppendLine("         , 0               AS " + ELEMENTS_COLUMN_TERMINAL_ID);
          _sb.AppendLine("    FROM   BANKS ");
          _sb.AppendLine("   WHERE   BK_AREA_ID = @pElementId ");
          _sb.AppendLine("ORDER BY   1 DESC");

          break;

        case GROUP_NODE_ELEMENT.BANKS:

          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   TE_MASTER_ID AS " + ELEMENTS_COLUMN_ID);
          _sb.AppendLine("        , @pTypeTerm     AS " + ELEMENTS_COLUMN_TYPE);
          _sb.AppendLine("        , TE_BASE_NAME        AS " + ELEMENTS_COLUMN_NAME);
          _sb.AppendLine("        , @pNodeTypeTerm AS " + ELEMENTS_COLUMN_NODE_TYPE);
          _sb.AppendLine("        , 1              AS " + ELEMENTS_COLUMN_ENABLED);
          _sb.AppendLine("        , @pElementId    AS " + ELEMENTS_COLUMN_PARENT_ID);
          _sb.AppendLine("      ,   TE_TERMINAL_ID AS " + ELEMENTS_COLUMN_TERMINAL_ID);
          _sb.AppendLine("   FROM   TERMINALS_LAST_CHANGED ");
          _sb.AppendLine("  INNER JOIN   TERMINALS  ");
          _sb.AppendLine("         ON   TLC_MASTER_ID = TE_MASTER_ID ");
          _sb.AppendLine("  WHERE   TE_TYPE = 1 ");
          _sb.AppendLine("    AND   ISNULL(TE_SERVER_ID, 0) =  0");
//          _sb.AppendLine("    AND   TE_BLOCKED = 0");
          _sb.AppendLine("    AND   TE_STATUS  IN (0, 1) ");
          _sb.AppendLine("    AND   TE_TERMINAL_TYPE IN ( " + Misc.GamingTerminalTypeListToString(false) + " ) ");
          _sb.AppendLine("    AND   TE_BANK_ID = @pElementId ");

          break;
        case GROUP_NODE_ELEMENT.ALL:

          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   TE_MASTER_ID AS " + ELEMENTS_COLUMN_ID);
          _sb.AppendLine("        , @pTypeTerm     AS " + ELEMENTS_COLUMN_TYPE);
          _sb.AppendLine("        , TE_BASE_NAME        AS " + ELEMENTS_COLUMN_NAME);
          _sb.AppendLine("        , @pNodeTypeTerm AS " + ELEMENTS_COLUMN_NODE_TYPE);
          _sb.AppendLine("        , 1              AS " + ELEMENTS_COLUMN_ENABLED);
          _sb.AppendLine("        , 0              AS " + ELEMENTS_COLUMN_PARENT_ID);
          _sb.AppendLine("      ,   TE_TERMINAL_ID AS " + ELEMENTS_COLUMN_TERMINAL_ID);
          _sb.AppendLine("   FROM   TERMINALS_LAST_CHANGED ");
          _sb.AppendLine("  INNER JOIN   TERMINALS  ");
          _sb.AppendLine("         ON   TLC_MASTER_ID = TE_MASTER_ID ");
          _sb.AppendLine("  WHERE   TE_TYPE = 1 ");
          _sb.AppendLine("    AND   ISNULL(TE_SERVER_ID, 0) =  0");
//          _sb.AppendLine("    AND   TE_BLOCKED = 0");
          _sb.AppendLine("    AND   TE_STATUS  IN (0, 1) ");
          _sb.AppendLine("    AND   TE_TERMINAL_TYPE IN ( " + Misc.GamingTerminalTypeListToString(false) + " ) ");

          break;

        case GROUP_NODE_ELEMENT.TERMINALS:
        default:

          _dt_aux = new DataTable();

          break;
      }

      if (_sb != null)
      {
        if (!AddElements(_sb, ElementId, ElementType, out _dt_aux))
        {
          return false;
        }
      }

      if (_dt_aux != null)
      {
        DtElements.Merge(_dt_aux);
      }

      return true;
    } // GetElements

    //------------------------------------------------------------------------------
    // PURPOSE: Get a list of terminals with parent information.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Params INPUT_GROUP_PARAMS: : List of information of the terminals
    //          - Trx: SqlTransaction
    //      - OUTPUT:
    //          - DtElements: DataTable
    //
    // RETURNS:
    //
    public static Boolean GetTerminalElements(INPUT_GROUP_PARAMS Params, out DataTable DtTerminals, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtTerminals = CreateElementTable(false);

      //ELEMENTS_COLUMN_TYPE = "TYPE";
      //ELEMENTS_COLUMN_NAME = "NAME";
      //ELEMENTS_COLUMN_ID = "ID";
      //ELEMENTS_COLUMN_ENABLED = "ENABLED";
      //ELEMENTS_COLUMN_PARENT_ID = "PARENT_ID";
      //ELEMENTS_COLUMN_PARENT_TYPE = "PARENT_TYPE";

      try
      {

        _sb.AppendLine(" SELECT   @pParentId     AS " + ELEMENTS_COLUMN_PARENT_ID);
        _sb.AppendLine("        , @pParentType   AS " + ELEMENTS_COLUMN_PARENT_TYPE);
        _sb.AppendLine("        , @pEnabled      AS " + ELEMENTS_COLUMN_ENABLED);
        _sb.AppendLine("        , TE_MASTER_ID   AS " + ELEMENTS_COLUMN_ID);
        _sb.AppendLine("        , TE_BASE_NAME   AS " + ELEMENTS_COLUMN_NAME);
        _sb.AppendLine("        , 6              AS " + ELEMENTS_COLUMN_TYPE); // GROUP_ELEMENT_TYPE.TERM    
        _sb.AppendLine("        , TE_TERMINAL_ID   AS " + ELEMENTS_COLUMN_TERMINAL_ID);
        _sb.AppendLine("       FROM   TERMINALS_LAST_CHANGED ");
        _sb.AppendLine(" INNER JOIN   TERMINALS  ");
        _sb.AppendLine("         ON   TLC_MASTER_ID = TE_MASTER_ID ");
        _sb.AppendLine("      WHERE   TE_TYPE = 1  ");
        _sb.AppendLine("        AND   ISNULL(TE_SERVER_ID, 0) = 0 ");
        //_sb.AppendLine("    AND   TE_BLOCKED = 0");
        _sb.AppendLine("        AND   TE_STATUS  IN (0, 1) ");
        _sb.AppendLine("        AND   TE_TERMINAL_TYPE IN ( " + Misc.GamingTerminalTypeListToString(false) + " ) ");

        switch (Params.ElementType)
        {
          case GROUP_ELEMENT_TYPE.PROV:
            _sb.AppendLine("    AND TE_PROV_ID = @pElementId");
            break;
          case GROUP_ELEMENT_TYPE.ZONE:
            _sb.AppendLine("    AND TE_BANK_ID IN (SELECT BK_BANK_ID FROM AREAS INNER JOIN BANKS ON AR_AREA_ID = BK_AREA_ID WHERE AR_SMOKING = CASE WHEN (@pElementId = 0) THEN 0 ELSE 1 END)");
            break;
          case GROUP_ELEMENT_TYPE.AREA:
            _sb.AppendLine("    AND TE_BANK_ID IN (SELECT BK_BANK_ID FROM BANKS WHERE BK_AREA_ID = @pElementId)");
            break;
          case GROUP_ELEMENT_TYPE.BANK:
            _sb.AppendLine("    AND TE_BANK_ID = @pElementId");
            break;
          case GROUP_ELEMENT_TYPE.TERM:
            _sb.AppendLine("    AND TLC_MASTER_ID = @pElementId");
            break;
          case GROUP_ELEMENT_TYPE.ALL:
            break;
          case GROUP_ELEMENT_TYPE.QUERY:
          case GROUP_ELEMENT_TYPE.GROUP:
          default:
            return false;
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pElementId", SqlDbType.Int).Value = Params.ElementId;
          _sql_cmd.Parameters.Add("@pParentId", SqlDbType.Int).Value = Params.ParentId;
          _sql_cmd.Parameters.Add("@pParentType", SqlDbType.Int).Value = Params.ParentType;
          _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Params.ElementEnabled;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtTerminals);
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetTerminalElements

    //------------------------------------------------------------------------------
    // PURPOSE: Get the provider id.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Name: String
    //          - Trx: SqlTransaction
    //      - OUTPUT:
    //          - Id: Int32
    //
    // RETURNS: String
    //
    public static Boolean GetProviderId(String Name, out Int32 Id, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _pv_id;

      _sb = new StringBuilder();
      Id = 0;
      _pv_id = null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT PV_ID FROM PROVIDERS WHERE PV_NAME = @pName");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Name;
          _pv_id = _sql_cmd.ExecuteScalar();

          if (_pv_id == null)
          {
            return false;
          }

          if (DBNull.Value.Equals(_pv_id))
          {
            return false;
          }

          Id = (Int32)_pv_id;
        }
        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetProviderId

    //------------------------------------------------------------------------------
    // PURPOSE: Get the list of terminals produced by the element indicated in params
    //
    //  PARAMS:
    //      - INPUT:
    //          - Params INPUT_GROUP_PARAMS: List of information about the element to exploit
    //          - Trx SqlTransaction
    //      - OUTPUT:
    //          - DtElements DataTable 
    // 
    // RETURNS: String
    //
    public static Boolean GetExploitElements(INPUT_GROUP_PARAMS Params, out DataTable DtElements, SqlTransaction Trx)
    {
      DataTable _dt_aux;
      StringBuilder _sb;

      DtElements = null;
      _sb = new StringBuilder();

      try
      {
        switch (Params.ElementType)
        {
          case GROUP_ELEMENT_TYPE.GROUP:

            //ELEMENTS_COLUMN_TYPE = "TYPE";
            //ELEMENTS_COLUMN_NAME = "NAME";
            //ELEMENTS_COLUMN_ID = "ID";
            //ELEMENTS_COLUMN_ENABLED = "ENABLED";
            //ELEMENTS_COLUMN_PARENT_ID = "PARENT_ID";
            //ELEMENTS_COLUMN_PARENT_TYPE = "PARENT_TYPE";

            _sb.AppendLine(" SELECT   GE_ELEMENT_ID   AS " + ELEMENTS_COLUMN_ID);
            _sb.AppendLine("        , GE_ELEMENT_TYPE AS " + ELEMENTS_COLUMN_TYPE);
            _sb.AppendLine("   FROM   GROUP_ELEMENTS");
            _sb.AppendLine("  WHERE   GE_GROUP_ID = @pElementId ");
            _sb.AppendLine("ORDER BY  GE_ELEMENT_ID ");
            _sb.AppendLine("        , GE_ELEMENT_TYPE ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
            {
              _sql_cmd.Parameters.Add("@pElementId", SqlDbType.BigInt).Value = Params.ElementId;
              _dt_aux = new DataTable();

              using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
              {
                _da.Fill(_dt_aux);
              }

              DtElements = CreateElementTable(false);

              foreach (DataRow _row in _dt_aux.Rows)
              {
                INPUT_GROUP_PARAMS _params;

                _params = new INPUT_GROUP_PARAMS();

                _params.ElementId = (Int64)_row[ELEMENTS_COLUMN_ID];
                _params.ElementType = (GROUP_ELEMENT_TYPE)_row[ELEMENTS_COLUMN_TYPE];

                if (Params.ParentId == 0)
                {
                  _params.ParentId = _params.ElementId;
                  _params.ParentType = _params.ElementType;
                }
                else
                {
                  _params.ParentId = Params.ParentId;
                  _params.ParentType = Params.ParentType;
                }

                if (!GetExploitElements(_params, out _dt_aux, Trx))
                {
                  return false;
                }

                DtElements.Merge(_dt_aux);
              }
            }

            break;
          case GROUP_ELEMENT_TYPE.PROV:
          case GROUP_ELEMENT_TYPE.ZONE:
          case GROUP_ELEMENT_TYPE.AREA:
          case GROUP_ELEMENT_TYPE.BANK:
          case GROUP_ELEMENT_TYPE.TERM:
          case GROUP_ELEMENT_TYPE.ALL:

            return GetTerminalElements(Params, out DtElements, Trx);

          default:
          case GROUP_ELEMENT_TYPE.QUERY:

            return false;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetExploitElements

    public static Boolean GetExploitElements(INPUT_GROUP_PARAMS Params, out DataTable DtElements)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetExploitElements(Params, out DtElements, _db_trx.SqlTransaction);
      }
    } // GetExploitElements

    //------------------------------------------------------------------------------
    // PURPOSE: Get conversion from GROUP_ELEMENT_TYPE to GROUP_NODE_ELEMENT.
    //
    //  PARAMS:
    //      - INPUT:
    //          - ElementType: Common.GROUP_ELEMENT_TYPE
    //          - ElementId: Int32
    //      - OUTPUT:
    //
    // RETURNS: GROUP_NODE_ELEMENT
    //
    public static GROUP_NODE_ELEMENT GroupConvert(GROUP_ELEMENT_TYPE ElementType, Int32 ElementId)
    {
      GROUP_NODE_ELEMENT _type;

      switch (ElementType)
      {
        case GROUP_ELEMENT_TYPE.GROUP:
          _type = (ElementId == ID_ROOT_ELEMENT) ? GROUP_NODE_ELEMENT.GROUP : GROUP_NODE_ELEMENT.GROUPS;
          break;

        case GROUP_ELEMENT_TYPE.PROV:
          _type = (ElementId == ID_ROOT_ELEMENT) ? GROUP_NODE_ELEMENT.PROVIDER : GROUP_NODE_ELEMENT.PROVIDERS;
          break;

        case GROUP_ELEMENT_TYPE.ZONE:
          _type = (ElementId == ID_ROOT_ELEMENT) ? GROUP_NODE_ELEMENT.ZONE : GROUP_NODE_ELEMENT.ZONES;
          break;

        case GROUP_ELEMENT_TYPE.AREA:
          _type = GROUP_NODE_ELEMENT.AREAS;
          break;

        case GROUP_ELEMENT_TYPE.BANK:
          _type = GROUP_NODE_ELEMENT.BANKS;
          break;

        case GROUP_ELEMENT_TYPE.TERM:
          _type = GROUP_NODE_ELEMENT.TERMINALS;
          break;

        case GROUP_ELEMENT_TYPE.ALL:
          _type = GROUP_NODE_ELEMENT.ALL;
          break;

        default:
          _type = 0;
          break;
      }

      return _type;
    } // GroupConvert

    //------------------------------------------------------------------------------
    // PURPOSE: Get conversion from GROUP_NODE_ELEMENT to GROUP_ELEMENT_TYPE.
    //
    //  PARAMS:
    //      - INPUT:
    //          - ElementType: Common.GROUP_ELEMENT_TYPE
    //      - OUTPUT:
    //
    // RETURNS: GROUP_NODE_ELEMENT
    //
    public static GROUP_ELEMENT_TYPE GroupConvert(GROUP_NODE_ELEMENT ElementType)
    {
      GROUP_ELEMENT_TYPE _type;

      switch (ElementType)
      {
        case GROUP_NODE_ELEMENT.GROUP:
        case GROUP_NODE_ELEMENT.GROUPS:
          _type = GROUP_ELEMENT_TYPE.GROUP;
          break;

        case GROUP_NODE_ELEMENT.PROVIDER:
        case GROUP_NODE_ELEMENT.PROVIDERS:
          _type = GROUP_ELEMENT_TYPE.PROV;
          break;

        case GROUP_NODE_ELEMENT.ZONE:
        case GROUP_NODE_ELEMENT.ZONES:
          _type = GROUP_ELEMENT_TYPE.ZONE;
          break;

        case GROUP_NODE_ELEMENT.AREAS:
          _type = GROUP_ELEMENT_TYPE.AREA;
          break;

        case GROUP_NODE_ELEMENT.BANKS:
          _type = GROUP_ELEMENT_TYPE.BANK;
          break;

        case GROUP_NODE_ELEMENT.TERMINALS:
          _type = GROUP_ELEMENT_TYPE.TERM;
          break;

        case GROUP_NODE_ELEMENT.ALL:
          _type = GROUP_ELEMENT_TYPE.ALL;
          break;

        default:
          _type = 0;
          break;
      }

      return _type;
    } // GroupConvert

    //------------------------------------------------------------------------------
    // PURPOSE: Create the table's structure.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //          - DtElements: DataTable
    //
    // RETURNS:
    //
    public static DataTable CreateElementTable(Boolean MasterIdAsPK)
    {
      DataTable DtElements = new DataTable();

      DtElements.Columns.Add(ELEMENTS_COLUMN_MASTER_ID, Type.GetType("System.Int64"));
      DtElements.Columns.Add(ELEMENTS_COLUMN_ID, Type.GetType("System.Int64"));
      DtElements.Columns.Add(ELEMENTS_COLUMN_TYPE, Type.GetType("System.Int32"));
      DtElements.Columns.Add(ELEMENTS_COLUMN_NODE_TYPE, Type.GetType("System.Int32"));      
      DtElements.Columns.Add(ELEMENTS_COLUMN_NAME, Type.GetType("System.String"));
      DtElements.Columns.Add(ELEMENTS_COLUMN_ENABLED, Type.GetType("System.Boolean"));

      DtElements.Columns.Add(ELEMENTS_COLUMN_PARENT_ID, Type.GetType("System.Int64"));
      DtElements.Columns.Add(ELEMENTS_COLUMN_PARENT_TYPE, Type.GetType("System.Int32"));
      DtElements.Columns.Add(ELEMENTS_COLUMN_TERMINAL_ID, Type.GetType("System.Int32"));

      if (MasterIdAsPK)
      {
        DtElements.PrimaryKey = new DataColumn[4] { DtElements.Columns[ELEMENTS_COLUMN_MASTER_ID], DtElements.Columns[ELEMENTS_COLUMN_ID], DtElements.Columns[ELEMENTS_COLUMN_TYPE], DtElements.Columns[ELEMENTS_COLUMN_PARENT_ID] };
      }
      else
      {
        DtElements.PrimaryKey = new DataColumn[3] { DtElements.Columns[ELEMENTS_COLUMN_ID], DtElements.Columns[ELEMENTS_COLUMN_TYPE], DtElements.Columns[ELEMENTS_COLUMN_PARENT_ID] };
      }
      return DtElements;
    } // CreateTable

    ////------------------------------------------------------------------------------
    //// PURPOSE: Create the table's structure.
    ////
    ////  PARAMS:
    ////      - INPUT:
    ////          - IfName: Boolean
    ////      - OUTPUT:
    ////          - DtElements: DataTable
    ////
    //// RETURNS:
    ////
    //public static DataTable CreateTerminalTable()
    //{
    //  DataTable DtTerminals = new DataTable();

    //  DtTerminals.Columns.Add(TERMINALS_COLUMN_PID, Type.GetType("System.Int64"));
    //  DtTerminals.Columns.Add(TERMINALS_COLUMN_PTYPE, Type.GetType("System.String"));
    //  DtTerminals.Columns.Add(TERMINALS_COLUMN_PENABLED, Type.GetType("System.Boolean"));
    //  DtTerminals.Columns.Add(TERMINALS_COLUMN_ID, Type.GetType("System.Int32"));
    //  DtTerminals.Columns.Add(TERMINALS_COLUMN_NAME, Type.GetType("System.String"));

    //  DtTerminals.PrimaryKey = new DataColumn[1] { DtTerminals.Columns[TERMINALS_COLUMN_PID]};

    //  return DtTerminals;
    //} // CreateTable

    //------------------------------------------------------------------------------
    // PURPOSE: Create the table's structure.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //          - DtElements: DataTable
    //
    // RETURNS:
    //
    //public static DataTable CreateGroupsTable()
    //{
    //  DataTable DtGroups = new DataTable();

    //  DtGroups.Columns.Add(GROUP_COLUMN_ID, Type.GetType("System.Int64"));
    //  DtGroups.Columns.Add(GROUP_COLUMN_ENABLED, Type.GetType("System.Boolean"));

    //  return DtGroups;
    //} // CreateGroupsTable

    //------------------------------------------------------------------------------
    // PURPOSE: Get elements from database.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sb: StringBuilder
    //          - ElementId: Int32
    //          - DbType: SqlDbType
    //          - ElementType: Common.GROUP_ELEMENT_TYPE
    //      - OUTPUT:
    //          - DtElements: DataTable
    //
    // RETURNS: Boolean
    //
    public static Boolean AddElements(StringBuilder QuerySelect, Int64 ElementId, GROUP_NODE_ELEMENT ElementType, out DataTable DtElements)
    {
      DtElements = CreateElementTable(false);
      DtElements.PrimaryKey = null;
      

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(QuerySelect.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            switch (ElementType)
            {
              case GROUP_NODE_ELEMENT.GROUP:
                _sql_cmd.Parameters.Add("@pNodeTypeGroup", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.GROUPS;
                _sql_cmd.Parameters.Add("@pTypeGroup", SqlDbType.Int).Value = GROUP_ELEMENT_TYPE.GROUP;
                break;

              case GROUP_NODE_ELEMENT.GROUPS:
                _sql_cmd.Parameters.Add("@pAll", SqlDbType.NVarChar).Value = Resource.String("STR_GROUPS_ALL");
                _sql_cmd.Parameters.Add("@pNoSmoking", SqlDbType.NVarChar).Value = Resource.String("STR_GROUPS_NO_SMOKING");
                _sql_cmd.Parameters.Add("@pSmoking", SqlDbType.NVarChar).Value = Resource.String("STR_GROUPS_SMOKING");
                _sql_cmd.Parameters.Add("@pNodeTypeAll", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.ALL;
                _sql_cmd.Parameters.Add("@pNodeTypeGroup", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.GROUPS;
                _sql_cmd.Parameters.Add("@pNodeTypeProvider", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.PROVIDERS;
                _sql_cmd.Parameters.Add("@pNodeTypeZone", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.ZONES;
                _sql_cmd.Parameters.Add("@pNodeTypeArea", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.AREAS;
                _sql_cmd.Parameters.Add("@pNodeTypeBank", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.BANKS;
                _sql_cmd.Parameters.Add("@pNodeTypeTerm", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.TERMINALS;
                _sql_cmd.Parameters.Add("@pElementId", SqlDbType.BigInt).Value = ElementId;
                break;

              case GROUP_NODE_ELEMENT.PROVIDER:
                _sql_cmd.Parameters.Add("@pNodeTypeProvider", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.PROVIDERS;
                _sql_cmd.Parameters.Add("@pTypeProvider", SqlDbType.Int).Value = GROUP_ELEMENT_TYPE.PROV; 
                break;

              case GROUP_NODE_ELEMENT.PROVIDERS:
                _sql_cmd.Parameters.Add("@pNodeTypeTerm", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.TERMINALS;
                _sql_cmd.Parameters.Add("@pTypeTerm", SqlDbType.Int).Value = GROUP_ELEMENT_TYPE.TERM; 
                _sql_cmd.Parameters.Add("@pElementId", SqlDbType.BigInt).Value = ElementId;
                break;

              case GROUP_NODE_ELEMENT.ZONE:
                _sql_cmd.Parameters.Add("@pNodeTypeZone", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.ZONES;
                break;

              case GROUP_NODE_ELEMENT.ZONES:
                _sql_cmd.Parameters.Add("@pNodeTypeArea", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.AREAS;
                _sql_cmd.Parameters.Add("@pTypeArea", SqlDbType.Int).Value = GROUP_ELEMENT_TYPE.AREA;
                _sql_cmd.Parameters.Add("@pElementId", SqlDbType.BigInt).Value = ElementId;
                break;

              case GROUP_NODE_ELEMENT.AREAS:
                _sql_cmd.Parameters.Add("@pNodeTypeBank", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.BANKS;
                _sql_cmd.Parameters.Add("@pTypeBank", SqlDbType.Int).Value = GROUP_ELEMENT_TYPE.BANK;
                _sql_cmd.Parameters.Add("@pElementId", SqlDbType.BigInt).Value = ElementId;
                break;

              case GROUP_NODE_ELEMENT.BANKS:
                _sql_cmd.Parameters.Add("@pNodeTypeTerm", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.TERMINALS;
                _sql_cmd.Parameters.Add("@pTypeTerm", SqlDbType.Int).Value = GROUP_ELEMENT_TYPE.TERM;
                _sql_cmd.Parameters.Add("@pElementId", SqlDbType.BigInt).Value = ElementId;
                break;

              case GROUP_NODE_ELEMENT.ALL:
                _sql_cmd.Parameters.Add("@pNodeTypeTerm", SqlDbType.Int).Value = GROUP_NODE_ELEMENT.TERMINALS;
                _sql_cmd.Parameters.Add("@pTypeTerm", SqlDbType.Int).Value = GROUP_ELEMENT_TYPE.TERM;
                break;

              case GROUP_NODE_ELEMENT.TERMINALS:
              default:
                return false;
            }

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
            {
              _da.Fill(DtElements);
            }
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // AddElements

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Get root elements.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //          - DtElements: DataTable
    //
    // RETURNS:
    //
    private static void CreateRoot(GROUP_NODE_ELEMENT RootNode, out DataTable Elements)
    {
      DataRow _dr;
      Elements = CreateElementTable(false);

      //ELEMENTS_COLUMN_ID
      //ELEMENTS_COLUMN_TYPE
      //ELEMENTS_COLUMN_NAME
      //ELEMENTS_COLUMN_ENABLED
      //ELEMENTS_COLUMN_PARENT_ID
      //ELEMENTS_COLUMN_PARENT_TYPE
      //ELEMENTS_COLUMN_TERMINAL_ID

      switch (RootNode)
      {
        case GROUP_NODE_ELEMENT.PROVIDER:

          _dr = Elements.NewRow();
          _dr[ELEMENTS_COLUMN_ID] = ID_ROOT_ELEMENT;
          _dr[ELEMENTS_COLUMN_TYPE] = GROUP_ELEMENT_TYPE.PROV;
          _dr[ELEMENTS_COLUMN_NODE_TYPE] = GROUP_NODE_ELEMENT.PROVIDER;
          _dr[ELEMENTS_COLUMN_NAME] = Resource.String("STR_GROUPS_PROVIDER");
          _dr[ELEMENTS_COLUMN_ENABLED] = true;
          _dr[ELEMENTS_COLUMN_PARENT_ID] = 0;
          _dr[ELEMENTS_COLUMN_PARENT_TYPE] = 0;
          _dr[ELEMENTS_COLUMN_TERMINAL_ID] = 0;
          Elements.Rows.Add(_dr);

          //Elements.Rows.Add(GROUP_NODE_ELEMENT.PROVIDER, Resource.String("STR_GROUPS_PROVIDER"), ID_ROOT_ELEMENT, true);

          break;

        case GROUP_NODE_ELEMENT.ZONE:

          _dr = Elements.NewRow();
          _dr[ELEMENTS_COLUMN_ID] = ID_ROOT_ELEMENT;
          _dr[ELEMENTS_COLUMN_TYPE] = GROUP_ELEMENT_TYPE.ZONE;
          _dr[ELEMENTS_COLUMN_NODE_TYPE] = GROUP_NODE_ELEMENT.ZONE; 
          _dr[ELEMENTS_COLUMN_NAME] = Resource.String("STR_GROUPS_ZONE");
          _dr[ELEMENTS_COLUMN_ENABLED] = true;
          _dr[ELEMENTS_COLUMN_PARENT_ID] = 0;
          _dr[ELEMENTS_COLUMN_PARENT_TYPE] = 0;
          _dr[ELEMENTS_COLUMN_TERMINAL_ID] = 0;
          Elements.Rows.Add(_dr);

          //Elements.Rows.Add(GROUP_NODE_ELEMENT.ZONE, Resource.String("STR_GROUPS_ZONE"), ID_ROOT_ELEMENT, true);

          break;

        case GROUP_NODE_ELEMENT.ZONES:

          _dr = Elements.NewRow();
          _dr[ELEMENTS_COLUMN_ID] = ID_ZONE_NO_SMOKING;
          _dr[ELEMENTS_COLUMN_TYPE] = GROUP_ELEMENT_TYPE.ZONE;
          _dr[ELEMENTS_COLUMN_NODE_TYPE] = GROUP_NODE_ELEMENT.ZONES; 
          _dr[ELEMENTS_COLUMN_NAME] = Resource.String("STR_GROUPS_NO_SMOKING");
          _dr[ELEMENTS_COLUMN_ENABLED] = true;
          _dr[ELEMENTS_COLUMN_PARENT_ID] = 0;
          _dr[ELEMENTS_COLUMN_PARENT_TYPE] = 0;
          _dr[ELEMENTS_COLUMN_TERMINAL_ID] = 0;
          Elements.Rows.Add(_dr);

          _dr = Elements.NewRow();
          _dr[ELEMENTS_COLUMN_ID] = ID_ZONE_SMOKING;
          _dr[ELEMENTS_COLUMN_TYPE] = GROUP_ELEMENT_TYPE.ZONE;
          _dr[ELEMENTS_COLUMN_NODE_TYPE] = GROUP_NODE_ELEMENT.ZONES; 
          _dr[ELEMENTS_COLUMN_NAME] = Resource.String("STR_GROUPS_SMOKING");
          _dr[ELEMENTS_COLUMN_ENABLED] = true;
          _dr[ELEMENTS_COLUMN_PARENT_ID] = 0;
          _dr[ELEMENTS_COLUMN_PARENT_TYPE] = 0;
          _dr[ELEMENTS_COLUMN_TERMINAL_ID] = 0;
          Elements.Rows.Add(_dr);

          //Elements.Rows.Add(GROUP_NODE_ELEMENT.ZONES, Resource.String("STR_GROUPS_NO_SMOKING"), ID_ZONE_NO_SMOKING, true);
          //Elements.Rows.Add(GROUP_NODE_ELEMENT.ZONES, Resource.String("STR_GROUPS_SMOKING"), ID_ZONE_SMOKING, true);

          break;

        case GROUP_NODE_ELEMENT.GROUP:

          _dr = Elements.NewRow();
          _dr[ELEMENTS_COLUMN_ID] = ID_ROOT_ELEMENT;
          _dr[ELEMENTS_COLUMN_TYPE] = GROUP_ELEMENT_TYPE.GROUP;
          _dr[ELEMENTS_COLUMN_NODE_TYPE] = GROUP_NODE_ELEMENT.GROUP;
          _dr[ELEMENTS_COLUMN_NAME] = Resource.String("STR_GROUPS_ROOT");
          _dr[ELEMENTS_COLUMN_ENABLED] = true;
          _dr[ELEMENTS_COLUMN_PARENT_ID] = 0;
          _dr[ELEMENTS_COLUMN_PARENT_TYPE] = 0;
          _dr[ELEMENTS_COLUMN_TERMINAL_ID] = 0;
          Elements.Rows.Add(_dr);

          //Elements.Rows.Add(GROUP_NODE_ELEMENT.GROUP, Resource.String("STR_GROUPS_ROOT"), ID_ROOT_ELEMENT, true);

          break;

        case GROUP_NODE_ELEMENT.ALL:

          _dr = Elements.NewRow();
          _dr[ELEMENTS_COLUMN_ID] = ID_ROOT_ELEMENT;
          _dr[ELEMENTS_COLUMN_TYPE] = GROUP_ELEMENT_TYPE.ALL;
          _dr[ELEMENTS_COLUMN_NODE_TYPE] = GROUP_NODE_ELEMENT.ALL;
          _dr[ELEMENTS_COLUMN_NAME] = Resource.String("STR_GROUPS_ALL");
          _dr[ELEMENTS_COLUMN_ENABLED] = true;
          _dr[ELEMENTS_COLUMN_PARENT_ID] = 0;
          _dr[ELEMENTS_COLUMN_PARENT_TYPE] = 0;
          _dr[ELEMENTS_COLUMN_TERMINAL_ID] = 0;
          Elements.Rows.Add(_dr);

          //Elements.Rows.Add(GROUP_NODE_ELEMENT.ALL, Resource.String("STR_GROUPS_ALL"), ID_ROOT_ELEMENT, true);

          break;

        default:
          break;
      }
    } // CreateRoot

    #endregion

  }
}
