//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AccountPromotions.cs
// 
//   DESCRIPTION: Class to manage Account Promotions 
// 
//        AUTHOR: Joaquim Cid
// 
// CREATION DATE: 05-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUN-2012 JCM    First release.
// 17-AUG-2012 MPO    Added AddPromotionPreassignedToAccount 
//                    Modified AddPromotionToAccount: Parameter that it is the status of account promotion
// 17-AUG-2012 DDM    Modified expiration in AddPromotionToAccount.
// 21-AUG-2012 DDM    Modifications for audit from promotion reports
// 27-AUG-2012 MPO    Modified AddPromotionPreassignedToAccount: Parameter Holder level
// 19-SEP-2012 DDM    Added function Trx_SetRedeemableCostOfPromotionNRE.
// 27-SEP-2012 MPO    FixedBug in InsertAccountPromotions: Update operation id for preassigned promotions
// 28-SEP-2012 DDM    FixedBug in Trx_SetRedeemableCostOfPromotionNRE added redeemed status in the Query 
//                    for obtain in the last activated promotion. 
// 02-OCT-2012 DDM    FixedBug in ChangeStatusPromotion: Added StringBuilder to log the exception. 
// 04-OCT-2012 DDM    FixedBug in InsertAccountPromotions: Modified Insert and added pm_category_id
// 10-APR-2013 QMP & RCI   Prize Coupon promotions are not inserted into ACCOUNT_PROMOTIONS table but must work as before.
// 12-APR-2013 DHA    Recalculate the Promotion Expiration, it depends on the configuration of Expiration Limit
// 16-APR-2013 RCI & DHA   Fixed Bug #721: Periodic and Awarded promotions doesn't activate
// 19-APR-2013 QMP & RCI   Fixed Bug #737: Preassigned promotion cannot be applied
// 02-JUL-2013 DLL    Added Currency Exchange support
// 26-SEP-2013 DHA   Fixed Bug #WIG-227: CashDeskDraw in mode Play Session has not to collect taxes
// 04-OCT-2013 DDM & DHA    Fixed Bug #WIG-262: Error cancelling UNR promotions.
// 07-OCT-2013 RCI   Fixed Bug #WIG-266: Can't remove NR2 Promotion
// 15-OCT-2013 ICS   Added Undo Operation support
// 06-NOV-2013 ICS   Fixed Bug WIG-389: Error undoing operations with redeemable promotion credit spent
// 25-MAR-2014 LEM & DRV Fixed Bug WIGOSTITO-1171: The status of a promotion it's not updated for RE and NR credit gifts 
// 19-OCT-2015 MPO   TFS - Product Backlog Item 5419: Kernel: Expiration promotions by day
// 20-OCT-2015 ETP   Product Backlog Item 4690 Removed call to general param TITO.TITOMode
// 23-NOV-2015 FOS   Bug 6771:Payment tickets & handpays
// 06-JAN-2016 SGB   Backlog Item 7910: Change column AC_POINTS to bucket 1.
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 13-JAN-2015 JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 24-FEB-2016 RAB    Product Backlog Item 8257:Multiple buckets: Canje de buckets NR
// 09-MAR-2016 SMN    Bug 10451:No se pueden transferir los cr�ditos de Buckets
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 13-OCT-2016 LTC    Bug 19012: Craw on Gaming tables: won amount is not given as gaming chips
// 04-NOV-2016 JBC    Bug 19012: Craw on Gaming tables: won amount is not given as gaming chips
// 09-NOV-2016 ETP    Fixed Bug 20160: Duplicated concept when not participating in GamingTables cashdeskdraw
// 10-NOV-2016 ATB    Bug 20147:Televisa: Requested changes from Televisa for "Cashier movements"
// 14-FEB-2017 SDS    Bug  24334: Cajero - Cashless: No se permite cancelar promociones de recarga.
// 21-MAR-2017 FAV    PBI 25735:Third TAX - Movement reports
// 29-MAR-2017 FAV    PBI 25956:Third TAX - Cash desk draw and UNR TAX Configuration
// 06-JUL-2017 JML    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 13-OCT-2017 DHA    Bug 30232:WIGOS-5646 [Ticket #8958] Error on expiration date for preassigned promo
// 04-DIC-2017 FJC    WIGOS-6985 Terminal Draw when "Loser" type does not add the "Courtesy prize" to player account (TFS: Bug 31036:WIGOS-6985 Terminal Draw when "Loser" type does not add the "Courtesy prize" to player account)
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace WSI.Common
{
  public class AccountPromotion : Promotion.TYPE_PROMOTION_DATA
  {
    #region ENUMS
    private enum ActionToPerform
    {
      Expire = 1,
      Cancel = 2,
      CancelByPlayer = 3,
      CancelByCashier = 4,
      UndoOperation = 5,
    }

    public enum PrizeType
    {
      None = 0,
      InKind1_TaxesFromAccount = 1,
      InKind2_TaxesFromSite = 2,
    }
    #endregion

    #region CONSTANTS

    public const int SQL_COLUMN_ACP_UNIQUE_ID = 0;
    public const int SQL_COLUMN_ACP_OPERATION_ID = 1;
    public const int SQL_COLUMN_ACP_ACCOUNT_ID = 2;
    public const int SQL_COLUMN_ACP_PROMO_ID = 3;
    public const int SQL_COLUMN_ACP_CREDIT_TYPE = 4;
    public const int SQL_COLUMN_ACP_BALANCE = 5;
    public const int SQL_COLUMN_ACP_ACCOUNT_LEVEL = 6;
    public const int SQL_COLUMN_ACP_PROMO_TYPE = 7;
    public const int SQL_COLUMN_ACP_PROMO_NAME = 8;
    public const int SQL_COLUMN_ACP_PROMO_DATE = 9;
    public const int SQL_COLUMN_ACP_EXPIRATION = 10;
    public const int SQL_COLUMN_ACP_WITHHOLD = 11;
    public const int SQL_COLUMN_ACP_WONLOCK = 12;
    public const int SQL_COLUMN_ACP_STATUS = 13;
    public const int SQL_COLUMN_ACP_RECOMMENDED_ACCOUNT_ID = 14;
    public const int SQL_COLUMN_ACP_DETAILS = 15;
    public const int SQL_COLUMN_ACP_CREATED = 16;
    public const int SQL_COLUMN_ACP_CASH_IN = 17;
    public const int SQL_COLUMN_ACP_POINTS = 18;

    public const int SQL_COLUMN_ACP_PRIZE_TYPE = 19;
    public const int SQL_COLUMN_ACP_PRIZE_GROSS = 20;
    public const int SQL_COLUMN_ACP_PRIZE_TAX1 = 21;
    public const int SQL_COLUMN_ACP_PRIZE_TAX2 = 22;
    public const int SQL_COLUMN_ACP_PRIZE_TAX3 = 23;
    public const int SQL_COLUMN_ACP_PROMOGAME_ID = 24;
    public const int SQL_COLUMN_ACP_PYRAMIDAL_DIST = 25;

    #endregion

    public delegate Boolean PromotionAction(DataRow Promotion,
                                            AccountMovementsTable AccountMovementsTable,
                                            CashierMovementsTable CashierMovementsTable,
                                            String Details,
                                            out MultiPromos.PromoBalance PromoBalance,
                                            SqlTransaction Trx);

    //------------------------------------------------------------------------------
    // PURPOSE: Set Flag Last Execution Promotion
    //
    //  PARAMS:
    //      
    //      - INPUT:
    //          - PromotionId
    //          - LastExecuted
    //          - Transaction     : DB Transaction
    //      - OUTPUT:
    //          - Result          : True correctly executed 
    //                              False otherwise
    //
    public static Boolean Lock(Int64 PromotionId, DateTime LastExecuted, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Boolean _is_last_executed_null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    UPDATE   PROMOTIONS");
        // Don't change LAST_EXECUTED here. Only when the promotion will be applied.
        _sb.AppendLine("       SET   PM_LAST_EXECUTED = PM_LAST_EXECUTED ");
        _sb.AppendLine("     WHERE   PM_PROMOTION_ID  = @pPromotionId ");

        _is_last_executed_null = LastExecuted.Equals(DateTime.MinValue);

        if (_is_last_executed_null)
        {
          _sb.AppendLine("         AND PM_LAST_EXECUTED IS NULL ");
        }
        else
        {
          _sb.AppendLine("         AND PM_LAST_EXECUTED = @pLastExecuted");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = PromotionId;

          if (!_is_last_executed_null)
          {
            _sql_cmd.Parameters.Add("@pLastExecuted", SqlDbType.DateTime).Value = LastExecuted;
          }

          _sql_cmd.CommandTimeout = 1;     // 1 second

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //Lock

    ////------------------------------------------------------------------------------
    //// PURPOSE: Calculates the total amount spent of player with Providers in Promotion.ProviderList
    //// 
    ////  PARAMS:
    ////      - INPUT:
    ////        - AccountId     : Account Id to filter
    ////        - PlayedPerAccountAndProvider: Table with columns Player, Provider, amount reedem spent
    ////
    ////      - OUTPUT:
    ////
    //// RETURNS: Amount Reedemeable money spent with Providers in Promotion.ProviderList
    //// 
    public static void AccountProviderTableSumUp(Int64 AccountId, Int32 AccountCol, ProviderList Provider, Int32 ProviderCol, DataTable PlayedPerAccountAndProvider, Int32 SumCol, out Currency Result)
    {
      Result = 0;

      try
      {
        foreach (DataRow _amount_acc_prov in PlayedPerAccountAndProvider.Rows)
        {
          if (AccountId != (Int64)_amount_acc_prov[AccountCol])
          {
            continue;
          }

          // Provider = Null --> All Providers
          if (Provider != null && !Provider.Contains((String)_amount_acc_prov[ProviderCol]))
          {
            continue;
          }

          Result += (Decimal)_amount_acc_prov[SumCol];
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Result = 0;
      }
    } //AccountProviderTableSumUp

    //------------------------------------------------------------------------------
    // PURPOSE: Calculates the total amount spent of player with Providers in Promotion.ProviderList
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AccountId     : Account Id to filter
    //        - PlayedPerAccountAndProvider: Table with columns Player, Provider, amount reedem spent
    //
    //      - OUTPUT:
    //
    // RETURNS: Amount Reedemeable money spent with Providers in Promotion.ProviderList
    // 
    public static void AccountProviderTableSumUp(Int64 AccountId, Int32 AccountCol, ProviderList Provider, Int32 ProviderCol, DataTable PlayedPerAccountAndProvider, Int32[] SumCol, out Object[] Result)
    {
      Result = new Object[SumCol.Length];

      try
      {
        foreach (DataRow _amount_acc_prov in PlayedPerAccountAndProvider.Rows)
        {
          // Account Id = -1 --> SUM All Accounts
          if (AccountId != -1 && AccountId != (Int64)_amount_acc_prov[AccountCol])
          {
            continue;
          }

          // Provider = Null --> All Providers
          if (Provider != null && !Provider.Contains((String)_amount_acc_prov[ProviderCol]))
          {
            continue;
          }

          foreach (Int32 _col in SumCol)
          {
            switch (_amount_acc_prov[_col].GetType().ToString())
            {
              case "System.Int32":
                Result[_col] = (Int32)Result[_col] + (Int32)_amount_acc_prov[_col];
                //Result[_col] += (Int32)_amount_acc_prov[_col];
                break;

              case "System.Int64":
                Result[_col] = (Int64)Result[_col] + (Int64)_amount_acc_prov[_col];
                break;

              case "System.Decimal":
                Result[_col] = (Decimal)Result[_col] + (Decimal)_amount_acc_prov[_col];
                break;

              case "WSI.Common.Currency":
                Result[_col] = (Currency)Result[_col] + (Currency)_amount_acc_prov[_col];
                break;

              default:
                break;
            }
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Result = new Object[SumCol.Length];
      }
    }    //AccountProviderTableSumUp

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate played total from table 
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public Currency CalculatePlayedByProvider(Int64 AccountId, DataTable PlayedPerAccountAndProvider)
    {
      Currency _result;
      ProviderList Provider;      // Si es null, els considera com si fosin tots
      Provider = null;

      AccountProviderTableSumUp(AccountId, 0, Provider, 1, PlayedPerAccountAndProvider, 3, out _result);

      return _result;
    } // CalculatePlayedByProvider 

    // PURPOSE: Get current cancelable promotions
    //
    //  PARAMS:
    //      - INPUT:
    //         
    //
    //      - OUTPUT:
    //      
    //      - RETURN: List current cancelable promotions
    //
    public static Boolean GetCancelablePromotions(long AccountId, out DataTable CancelablePromos)
    {
      int _cancelable_interval; // minutes
      StringBuilder _sb;

      _cancelable_interval = GeneralParam.GetInt32("ManualPromotions", "TimeToCancel", 0);

      CancelablePromos = new DataTable();

      try
      {
        _sb = new StringBuilder();
        // NR Part
        _sb.AppendLine("SELECT   CAST(NULL AS VARBINARY) AS IMAGE                         ");
        _sb.AppendLine("       , ACP_UNIQUE_ID                                            ");
        _sb.AppendLine("       , ACP_PROMO_NAME                                           ");
        _sb.AppendLine("       , ACP_INI_WONLOCK                                          ");
        _sb.AppendLine("       , ACP_CREDIT_TYPE                                          ");
        _sb.AppendLine("       , ACP_PROMO_DATE                                           ");
        _sb.AppendLine("       , ''                                                       ");
        _sb.AppendLine("       , ACP_INI_BALANCE                                          ");
        _sb.AppendLine("       , ''                                                       ");
        _sb.AppendLine("       , ACP_BALANCE                                              ");
        _sb.AppendLine("       , ACP_STATUS                                               ");
        _sb.AppendLine("       , ACP_PROMO_ID                                             ");
        _sb.AppendLine("       , ACP_PROMO_TYPE                                           ");
        _sb.AppendLine("       , ACP_ACTIVATION                                           ");
        _sb.AppendLine("       , ACP_PRIZE_TYPE                                           ");
        _sb.AppendLine("       , ACP_PRIZE_GROSS                                          ");
        _sb.AppendLine("       , ACP_PRIZE_TAX1                                           ");
        _sb.AppendLine("       , ACP_PRIZE_TAX2                                           ");
        _sb.AppendLine("       , ACP_PRIZE_TAX3                                           ");
        _sb.AppendLine("  FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status))   ");
        _sb.AppendLine("WHERE    ACP_ACCOUNT_ID   = @pAccountId                           ");
        _sb.AppendLine("   AND   ACP_STATUS       = @pStatusActive                        ");
        _sb.AppendLine("   AND   ACP_ACTIVATION  >= DATEADD(mi, -(@pCancelableMinutes), GETDATE()) ");
        _sb.AppendLine("   AND   ACP_PLAYED = 0                                              ");
        _sb.AppendLine("   AND   ACP_PLAY_SESSION_ID IS NULL                                 ");
        _sb.AppendLine("   AND   ACP_CREDIT_TYPE     IN ( @pCreditTypeNR1, @pCreditTypeNR2 ) ");
        _sb.AppendLine("   AND   ACP_PROMO_TYPE      IN ( @pPromoManual, @pPromoPreassigned, @pPromoPointsNR, @pPromoDrawWinnerNR, @pPromoDrawLoserNR, @pPromoDrawWinnerUNR, @pPromoDrawWinnerREinKind ) ");
        _sb.AppendLine("UNION                                                             ");
        // RE & Points Part
        _sb.AppendLine("SELECT   CAST(NULL AS VARBINARY) AS IMAGE                         ");
        _sb.AppendLine("       , ACP_UNIQUE_ID                                            ");
        _sb.AppendLine("       , ACP_PROMO_NAME                                           ");
        _sb.AppendLine("       , ACP_INI_WONLOCK                                          ");
        _sb.AppendLine("       , ACP_CREDIT_TYPE                                          ");
        _sb.AppendLine("       , ACP_PROMO_DATE                                           ");
        _sb.AppendLine("       , ''                                                       ");
        _sb.AppendLine("       , ACP_INI_BALANCE                                          ");
        _sb.AppendLine("       , ''                                                       ");
        _sb.AppendLine("       , ACP_BALANCE                                              ");
        _sb.AppendLine("       , ACP_STATUS                                               ");
        _sb.AppendLine("       , ACP_PROMO_ID                                             ");
        _sb.AppendLine("       , ACP_PROMO_TYPE                                           ");
        _sb.AppendLine("       , ACP_ACTIVATION                                           ");
        _sb.AppendLine("       , ACP_PRIZE_TYPE                                           ");
        _sb.AppendLine("       , ACP_PRIZE_GROSS                                          ");
        _sb.AppendLine("       , ACP_PRIZE_TAX1                                           ");
        _sb.AppendLine("       , ACP_PRIZE_TAX2                                           ");
        _sb.AppendLine("       , ACP_PRIZE_TAX3                                           ");
        _sb.AppendLine("  FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status))   ");
        _sb.AppendLine("INNER JOIN ACCOUNTS ON ACCOUNT_PROMOTIONS.ACP_ACCOUNT_ID = ACCOUNTS.AC_ACCOUNT_ID        ");
        _sb.AppendLine(" WHERE   ACP_ACCOUNT_ID    = @pAccountId                                                 ");
        _sb.AppendLine("   AND   ACP_STATUS        = @pStatusRedeemed                                            ");
        _sb.AppendLine("   AND   ACP_ACTIVATION   >= DATEADD(mi, -(@pCancelableMinutes), GETDATE())              ");
        _sb.AppendLine("   AND  ((ACP_CREDIT_TYPE  = @pCreditTypeRE AND ACP_INI_BALANCE <= AC_PROMO_RE_BALANCE ) ");
        _sb.AppendLine("   OR    (ACP_CREDIT_TYPE  = @pCreditTypePoints AND ACP_INI_BALANCE <= DBO.GETBUCKETVALUE(@pBucketId, ACCOUNTS.AC_ACCOUNT_ID) )  ) ");
        _sb.AppendLine("   AND   ACP_PROMO_TYPE in (@pPromoManual, @pPromoPreassigned, @pPromoPointsRE )        ");
        _sb.AppendLine("ORDER BY ACP_UNIQUE_ID DESC                                    ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
            _sql_cmd.Parameters.Add("@pCancelableMinutes", SqlDbType.Int).Value = _cancelable_interval;
            _sql_cmd.Parameters.Add("@pStatusRedeemed", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.REDEEMED;
            _sql_cmd.Parameters.Add("@pCreditTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
            _sql_cmd.Parameters.Add("@pCreditTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
            _sql_cmd.Parameters.Add("@pCreditTypePoints", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.POINT;
            _sql_cmd.Parameters.Add("@pCreditTypeRE", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE;
            _sql_cmd.Parameters.Add("@pPromoManual", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.MANUAL;
            _sql_cmd.Parameters.Add("@pPromoPreassigned", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.PREASSIGNED;
            _sql_cmd.Parameters.Add("@pPromoPointsNR", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE;
            _sql_cmd.Parameters.Add("@pPromoPointsRE", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE;
            _sql_cmd.Parameters.Add("@pPromoDrawWinnerNR", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR;
            _sql_cmd.Parameters.Add("@pPromoDrawWinnerUNR", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND;
            _sql_cmd.Parameters.Add("@pPromoDrawWinnerREinKind", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND;
            _sql_cmd.Parameters.Add("@pPromoDrawLoserNR", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR;
            _sql_cmd.Parameters.Add("@pBucketId", SqlDbType.Int).Value = (Int32)Buckets.BucketId.RedemptionPoints;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(CancelablePromos);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCancelablePromotions

    //------------------------------------------------------------------------------
    // PURPOSE: Get current enabled promotions
    //
    //  PARAMS:
    //      - INPUT:
    //          
    //
    //      - OUTPUT:
    //      
    //      - RETURN: List current enabled promotion of type AccountPromotions
    //
    public static Boolean GetPendingPeriodicPromotions(out List<AccountPromotion> PendingPromos)
    {
      StringBuilder _sb;
      AccountPromotion _promo;

      try
      {
        PendingPromos = new List<AccountPromotion>();

        _sb = new StringBuilder();
        _sb.AppendLine("DECLARE @Now AS DATETIME ");
        _sb.AppendLine();
        _sb.AppendLine("SET @Now = GETDATE() ");
        _sb.AppendLine();
        _sb.AppendLine("SELECT   PM_PROMOTION_ID ");
        _sb.AppendLine("       , PM_LAST_EXECUTED ");
        _sb.AppendLine("  FROM   PROMOTIONS ");
        _sb.AppendLine(" WHERE   PM_ENABLED = 1");
        _sb.AppendLine("   AND   PM_TYPE IN (@pPeriodicBirthday, @pPeriodicLevel, @pPeriodicLevelAndMinPlayed) ");
        _sb.AppendLine("   AND   ( PM_DATE_START <= @Now AND @Now  < PM_DATE_FINISH )");
        _sb.AppendLine("   AND   ( ISNULL(PM_NEXT_EXECUTION, @Now ) <= @Now  ) ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pPeriodicBirthday", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY;
            _sql_cmd.Parameters.Add("@pPeriodicLevel", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL;
            _sql_cmd.Parameters.Add("@pPeriodicLevelAndMinPlayed", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED;

            using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
            {
              while (_sql_rd.Read())
              {
                _promo = new AccountPromotion();
                _promo.id = _sql_rd.GetInt64(0);
                _promo.last_executed = _sql_rd.IsDBNull(1) ? DateTime.MinValue : _sql_rd.GetDateTime(1);

                PendingPromos.Add(_promo);
              }
            }
          }
        } // DB_TRX

        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        PendingPromos = new List<AccountPromotion>();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        PendingPromos = new List<AccountPromotion>();
      }

      return false;
    } // GetPendingPromotions

    //------------------------------------------------------------------------------
    // PURPOSE : Apply promotions
    //
    //  PARAMS :
    //      - INPUT :
    //          - Accounts 
    //          - PlayedPerAccountAndProvider
    //          - SqlTrx
    //  
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Promotion was applied. False: Otherwise.
    public Boolean Apply(DataRow[] Accounts, DataTable PlayedPerAccountAndProvider, Boolean IsPendingDrawPromotion, SqlTransaction SqlTrx)
    {
      Currency _filtered_played;
      Currency _awarded_amount;
      Decimal _units;
      DataTable _account_promotions;
      Boolean _awarded;
      Int64 _account_id;
      Int32 _account_level;

      //create schema      
      _account_promotions = CreateAccountPromotionTable();

      foreach (DataRow _account in Accounts)
      {
        _awarded_amount = this.min_played_reward;
        _awarded = true;
        _account_id = (Int64)(_account[0]);
        _account_level = (Int32)(_account[1]);

        if (this.played != 0 || this.played_reward != 0 || this.min_played != 0)
        {
          // Played on selected providers
          AccountProviderTableSumUp(_account_id, 0, this.provider_list, 1, PlayedPerAccountAndProvider, 2, out _filtered_played);

          if (this.played_reward < 0 && this.played < 0)
          {
            // PCT
            _awarded_amount += Math.Round(_filtered_played * this.played_reward / this.played, 2);
          }
          else if (this.played_reward > 0 && this.played > 0)
          {
            _units = Math.Floor(_filtered_played / this.played);
            _awarded_amount += _units * this.played_reward;
          }

          _awarded = _awarded && (_filtered_played >= this.min_played);
        }

        if (_awarded_amount <= 0)
        {
          continue;
        }

        if (!AddPromotionToAccount(0, _account_id, _account_level, 0, 0, _awarded, _awarded_amount, this, _account_promotions))
        {
          return false;
        }
      } // foreach

      if (InsertAccountPromotions(_account_promotions, false, IsPendingDrawPromotion, SqlTrx))
      {
        return true;
      }

      return false;
    } // Apply

    //------------------------------------------------------------------------------
    // PURPOSE :  Create schema to the ACCOUNT_PROMOTIONS table 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS : Return datatable with schema
    //
    public static DataTable CreateAccountPromotionTable()
    {
      DataTable _dt;
      DataColumn _col;

      _dt = new DataTable();

      _col = _dt.Columns.Add("ACP_UNIQUE_ID", Type.GetType("System.Int64"));
      _col.AutoIncrement = true;
      _col.AutoIncrementSeed = -1;
      _col.AutoIncrementStep = -1;

      _dt.Columns.Add("ACP_OPERATION_ID", Type.GetType("System.Int64"));
      _dt.Columns.Add("ACP_ACCOUNT_ID", Type.GetType("System.Int64"));
      _dt.Columns.Add("ACP_PROMO_ID", Type.GetType("System.Int64"));
      _dt.Columns.Add("ACP_CREDIT_TYPE", Type.GetType("System.Int32"));
      _dt.Columns.Add("ACP_BALANCE", Type.GetType("System.Decimal"));
      _dt.Columns.Add("ACP_ACCOUNT_LEVEL", Type.GetType("System.Int32"));
      _dt.Columns.Add("ACP_PROMO_TYPE", Type.GetType("System.Int32"));
      _dt.Columns.Add("ACP_PROMO_NAME", Type.GetType("System.String"));
      _dt.Columns.Add("ACP_PROMO_DATE", Type.GetType("System.DateTime"));
      _dt.Columns.Add("ACP_EXPIRATION", Type.GetType("System.DateTime"));
      _dt.Columns.Add("ACP_WITHHOLD", Type.GetType("System.Decimal"));
      _dt.Columns.Add("ACP_WONLOCK", Type.GetType("System.Decimal"));
      _dt.Columns.Add("ACP_STATUS", Type.GetType("System.Int32")).DefaultValue = 0;
      _dt.Columns.Add("ACP_RECOMMENDED_ACCOUNT_ID", Type.GetType("System.Int64"));
      _dt.Columns.Add("ACP_DETAILS", Type.GetType("System.String"));
      _dt.Columns.Add("ACP_CREATED", Type.GetType("System.DateTime"));
      _dt.Columns.Add("ACP_CASH_IN", Type.GetType("System.Decimal"));
      _dt.Columns.Add("ACP_POINTS", Type.GetType("System.Decimal"));

      // TODO: FOR UNR
      _dt.Columns.Add("ACP_PRIZE_TYPE", Type.GetType("System.Int32"));
      _dt.Columns.Add("ACP_PRIZE_GROSS", Type.GetType("System.Decimal"));
      _dt.Columns.Add("ACP_PRIZE_TAX1", Type.GetType("System.Decimal"));
      _dt.Columns.Add("ACP_PRIZE_TAX2", Type.GetType("System.Decimal"));
      _dt.Columns.Add("ACP_PRIZE_TAX3", Type.GetType("System.Decimal"));

      // RLO ADD PROMOGAMEID
      _dt.Columns.Add("PM_PROMOGAME_ID", Type.GetType("System.Int64"));
      _dt.Columns.Add("PM_PYRAMIDAL_DIST", Type.GetType("System.String"));

      _dt.PrimaryKey = new DataColumn[] { _dt.Columns["ACP_UNIQUE_ID"] };

      return _dt;
    } // CreateAccountPromotionTable

    public static Boolean InsertPromotionToAccount(Int64 OperationId,
                                                   Int64 AccountId,
                                                   Int32 AccountLevel,
                                                   Currency TotalRecharged,
                                                   Points TotalPoints,
                                                   Boolean Awarded,
                                                   Currency AwardedAmount,
                                                   AccountPromotion AccountPromotion,
                                                   AccountPromotion Main_AccountPromotion,
                                                   DataTable DtAccountsPromo)
    {
      return AddPromotionToAccount(OperationId,
                                   AccountId,
                                   AccountLevel,
                                   TotalRecharged,
                                   TotalPoints,
                                   Awarded ? ACCOUNT_PROMO_STATUS.AWARDED : ACCOUNT_PROMO_STATUS.NOT_AWARDED,
                                   AwardedAmount,
                                   AccountPromotion,
                                   Main_AccountPromotion,
                                   DtAccountsPromo,
                                   true);
    } // InsertPromotionToAccount

    public static Boolean AddPromotionToAccount(Int64 OperationId,
                                                Int64 AccountId,
                                                Int32 AccountLevel,
                                                Currency TotalRecharged,
                                                Points TotalPoints,
                                                Boolean Awarded,
                                                Currency AwardedAmount,
                                                AccountPromotion AccountPromotion,
                                                DataTable DtAccountsPromo)
    {
      return AddPromotionToAccount(OperationId,
                                   AccountId,
                                   AccountLevel,
                                   TotalRecharged,
                                   TotalPoints,
                                   Awarded ? ACCOUNT_PROMO_STATUS.AWARDED : ACCOUNT_PROMO_STATUS.NOT_AWARDED,
                                   AwardedAmount,
                                   AccountPromotion,
                                   AccountPromotion,
                                   DtAccountsPromo,
                                   false);
    } // AddPromotionToAccount

    public static Boolean AddPromotionPreassignedToAccount(Int64 AccountId,
                                                           Int32 AccountLevel,
                                                           Currency AwardedAmount,
                                                           AccountPromotion AccountPromotion,
                                                           DataTable DtAccountsPromo)
    {
      return AddPromotionToAccount(0,
                                   AccountId,
                                   AccountLevel,
                                   0,
                                   0,
                                   ACCOUNT_PROMO_STATUS.PREASSIGNED,
                                   AwardedAmount,
                                   AccountPromotion,
                                   null,
                                   DtAccountsPromo,
                                   false);
    } // AddPromotionToAccount


    private static ACCOUNT_PROMO_CREDIT_TYPE ConditionallyConvertNonRedeemableCreditType(ACCOUNT_PROMO_CREDIT_TYPE CreditType)
    {
      Int32 _convert;


      // NR1 To NR2/NR3
      if (CreditType == ACCOUNT_PROMO_CREDIT_TYPE.NR1)
      {
        _convert = GeneralParam.GetInt32("Cashier", "Promotion.NR1AsNR");

        switch (_convert)
        {
          case 2:
            CreditType = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
            break;

          case 3:
            CreditType = ACCOUNT_PROMO_CREDIT_TYPE.NR3;
            break;

          default: break;
        }
      }

      // NR To UNR
      switch (CreditType)
      {
        case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
          _convert = GeneralParam.GetInt32("Cashier", "Promotion.NR1AsUNR");
          break;

        case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
          _convert = GeneralParam.GetInt32("Cashier", "Promotion.NR2AsUNR");
          break;

        case ACCOUNT_PROMO_CREDIT_TYPE.NR3:
          _convert = GeneralParam.GetInt32("Cashier", "Promotion.NR3AsUNR");
          break;

        default:
          return CreditType;
      }

      switch (_convert)
      {
        case 1:
          return ACCOUNT_PROMO_CREDIT_TYPE.UNR1;

        case 2:
          return ACCOUNT_PROMO_CREDIT_TYPE.UNR2;

        case 0:
        default:
          return CreditType;
      }
    }

    private static ACCOUNT_PROMO_CREDIT_TYPE ConvertNonRedeemableToNR1orNR2(ACCOUNT_PROMO_CREDIT_TYPE CreditType)
    {
      // NR To UNR
      switch (CreditType)
      {
        case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
        case ACCOUNT_PROMO_CREDIT_TYPE.NR3:
        case ACCOUNT_PROMO_CREDIT_TYPE.UNR1:
          return ACCOUNT_PROMO_CREDIT_TYPE.NR1;

        case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
        case ACCOUNT_PROMO_CREDIT_TYPE.UNR2:
          return ACCOUNT_PROMO_CREDIT_TYPE.NR2;

        default:
          return CreditType;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Add row values
    //
    //  PARAMS :
    //      - INPUT: 
    //            - AccountId
    //            - AccountLevel
    //            - Awarded
    //            - Balance
    //            - AccountPromotion
    //            - CreditType
    //            - DtAccountsPromo
    //            - InsertFirst
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static Boolean AddPromotionToAccount(Int64 OperationId,
                                                 Int64 AccountId,
                                                 Int32 AccountLevel,
                                                 Currency TotalRecharged,
                                                 Points TotalPoints,
                                                 ACCOUNT_PROMO_STATUS PromoStatus,
                                                 Currency AwardedAmount,
                                                 AccountPromotion AccountPromotion,
                                                 AccountPromotion Main_AccountPromotion,
                                                 DataTable DtAccountsPromo,
                                                 Boolean InsertFirst)
    {
      DataRow _account_promo;
      DateTime _created;
      DateTime _expiration;
      DateTime _today;
      DateTime _tomorrow;
      TimeSpan _diff;
      ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
      Decimal _max_UNR;
      Boolean _apply_tax;

      _credit_type = AccountPromotion.credit_type;
      _apply_tax = AccountPromotion.apply_tax;

      try
      {
        _created = WGDB.Now;
        _today = Misc.TodayOpening();

        _expiration = _created.AddHours(24);

        switch (AccountPromotion.expiration_type)
        {
          case Promotion.EXPIRATION_TYPE.EXPIRATION_TYPE_DAYS:
            if (AccountPromotion.expiration_value > 0)
            {
              _tomorrow = _today.AddDays(1);

              _diff = _tomorrow - _created;

              if (AccountPromotion.expiration_value == 1 && _diff.TotalHours <= 1)
              {
                _expiration = _tomorrow.AddDays(AccountPromotion.expiration_value);
              }
              else
              {
                _expiration = _today.AddDays(AccountPromotion.expiration_value);
              }
            }
            break;

          case Promotion.EXPIRATION_TYPE.EXPIRATION_TYPE_HOURS:
            if (AccountPromotion.expiration_value > 0)
            {
              _expiration = _created.AddHours(AccountPromotion.expiration_value);
            }
            break;

          case Promotion.EXPIRATION_TYPE.EXPIRATION_TYPE_NOT_SET:
            break;
        }

        if (AccountPromotion.expiration_value == 0)
        {
          _expiration = DateTime.MaxValue;
        }
        // DHA 12-APR-2013: In case that 'expiration_limit' is not null, 'must_expire' is set with 'expiration_limit' value
        else if (AccountPromotion.expiration_limit != DateTime.MinValue)
        {
          // DDM 17-AUG-2012: If not is periodical promotion, the expiration will be the minimum between AccountPromotion.must_expire and _expiration
          _expiration = (_expiration > AccountPromotion.must_expire) ? AccountPromotion.must_expire : _expiration;
        }

        _account_promo = DtAccountsPromo.NewRow();

        _account_promo[SQL_COLUMN_ACP_ACCOUNT_ID] = AccountId;
        _account_promo[SQL_COLUMN_ACP_ACCOUNT_LEVEL] = AccountLevel;
        _account_promo[SQL_COLUMN_ACP_PROMO_TYPE] = AccountPromotion.type;
        _account_promo[SQL_COLUMN_ACP_PROMO_ID] = AccountPromotion.id;
        _account_promo[SQL_COLUMN_ACP_PROMO_NAME] = AccountPromotion.name;
        _account_promo[SQL_COLUMN_ACP_OPERATION_ID] = OperationId;
        _account_promo[SQL_COLUMN_ACP_CREDIT_TYPE] = (Int32)_credit_type;
        _account_promo[SQL_COLUMN_ACP_BALANCE] = (Decimal)AwardedAmount;
        _account_promo[SQL_COLUMN_ACP_WITHHOLD] = 0;
        _account_promo[SQL_COLUMN_ACP_WONLOCK] = DBNull.Value;

        _account_promo[SQL_COLUMN_ACP_CASH_IN] = DBNull.Value;
        _account_promo[SQL_COLUMN_ACP_POINTS] = DBNull.Value;
        _account_promo[SQL_COLUMN_ACP_PRIZE_TYPE] = 0;

        switch (AccountPromotion.type)
        {
          case Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON:
          case Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON:
          // DDM & AJQ, 05-SEP-2013 CashDeskDraw
          case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR:
          case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE:
          case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR:
          case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE:
          case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND:
          case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND:

          case Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE:
          // JML 14-DEC-2015
          case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND:
            if (TotalRecharged <= 0)
            {
              Log.Warning("AddPromotionToAccount: Promotion per recharge without recharge! AccountId: " + AccountId.ToString() + ", PromotionId: " +
                          AccountPromotion.id.ToString() + ", Recharge: " + TotalRecharged.ToString());

              return false;
            }

            _account_promo[SQL_COLUMN_ACP_CASH_IN] = (Decimal)TotalRecharged;
            break;

          case Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE:
          case Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE:
            if (TotalPoints <= 0)
            {
              Log.Warning("AddPromotionToAccount: Promotion per points without points! AccountId: " + AccountId.ToString() + ", PromotionId: " +
                          AccountPromotion.id.ToString() + ", Points: " + TotalPoints.ToString());

              return false;
            }

            // Spent points
            _account_promo[SQL_COLUMN_ACP_POINTS] = (Decimal)TotalPoints;
            break;

          case Promotion.PROMOTION_TYPE.MANUAL:
            if (AccountPromotion.min_cash_in > 0 || AccountPromotion.cash_in > 0)
            {
              // required recharge
              _account_promo[SQL_COLUMN_ACP_CASH_IN] = (Decimal)TotalRecharged;
            }
            break;

          case Promotion.PROMOTION_TYPE.UNKNOWN:
          case Promotion.PROMOTION_TYPE.AUTOMATIC_PER_PRIZE:
          case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL:
          case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED:
          case Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY:
          case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM:
          case Promotion.PROMOTION_TYPE.EXTERNAL_REDEEM:
          case Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT:
          case Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT_AND_RECHARGE:
          default:
            break;
        }

        _credit_type = ConditionallyConvertNonRedeemableCreditType(_credit_type);
        if (AccountPromotion.type != Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN &&
            (_credit_type == ACCOUNT_PROMO_CREDIT_TYPE.UNR1 || _credit_type == ACCOUNT_PROMO_CREDIT_TYPE.UNR2))
        {
          Currency _gross;
          Currency _tax1;
          Currency _tax2;
          Currency _tax3;

          if (!CashDeskDrawBusinessLogic.AddPrizeUNR(false, AwardedAmount, out _gross, out _tax1, out _tax2, out _tax3))
          {
            return false;
          }

          _max_UNR = GeneralParam.GetDecimal("Cashier", "Promotion.UNR.MaxAwarded", 10000);

          if ((Decimal)_gross > _max_UNR && _max_UNR > 0)
          {
            Log.Error(String.Format("AddPromotionToAccount. The Awarded UNR {0} is greater than the maximum {1} ", _gross.ToString(), _max_UNR.ToString()));

            return false;
          }

          _account_promo[SQL_COLUMN_ACP_PRIZE_GROSS] = (Decimal)_gross;
          _account_promo[SQL_COLUMN_ACP_PRIZE_TAX1] = (Decimal)_tax1;
          _account_promo[SQL_COLUMN_ACP_PRIZE_TAX2] = (Decimal)_tax2;
          _account_promo[SQL_COLUMN_ACP_PRIZE_TAX3] = (Decimal)_tax3;

          if (AccountPromotion.type == Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND)
          {
            if (!GeneralParam.GetBoolean("Cashier", "Promotions.UNR.TaxesToAccount.CancelOnRedeem", true))
            {
              // DHA 29-OCT-2013: promotion not apply to redeem (taxes from account)
              _account_promo[SQL_COLUMN_ACP_CASH_IN] = DBNull.Value;
            }

            _account_promo[SQL_COLUMN_ACP_PRIZE_TYPE] = PrizeType.InKind1_TaxesFromAccount;
          }
          else
          {
            if (!GeneralParam.GetBoolean("Cashier", "Promotions.UNR.TaxesToSite.CancelOnRedeem", true))
            {
              // DHA 29-OCT-2013: promotion not apply to redeem (taxes from site)
              _account_promo[SQL_COLUMN_ACP_CASH_IN] = DBNull.Value;
            }

            _account_promo[SQL_COLUMN_ACP_PRIZE_TYPE] = PrizeType.InKind2_TaxesFromSite;
          }
        }
        else if (Main_AccountPromotion != null && Main_AccountPromotion.enabled && Main_AccountPromotion.apply_tax 
                 && Main_AccountPromotion.type == Promotion.PROMOTION_TYPE.MANUAL)
        {
        }
        else if (AccountPromotion.type != Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN &&
                (_credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND))
        {
          Currency _gross;
          Currency _tax1;
          Currency _tax2;
          Currency _tax3;

          if (!CashDeskDrawBusinessLogic.AddPrizeUNR(false, AwardedAmount, out _gross, out _tax1, out _tax2, out _tax3))
          {
            return false;
          }

          _account_promo[SQL_COLUMN_ACP_PRIZE_GROSS] = (Decimal)_gross;
          _account_promo[SQL_COLUMN_ACP_PRIZE_TAX1] = (Decimal)_tax1;
          _account_promo[SQL_COLUMN_ACP_PRIZE_TAX2] = (Decimal)_tax2;
          _account_promo[SQL_COLUMN_ACP_PRIZE_TAX3] = (Decimal)_tax3;

          if (AccountPromotion.type == Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND)
          {
            if (!GeneralParam.GetBoolean("Cashier", "Promotions.UNR.TaxesToAccount.CancelOnRedeem", true))
            {
              // Promotion not apply to redeem (taxes from account)
              _account_promo[SQL_COLUMN_ACP_CASH_IN] = DBNull.Value;
            }
            _account_promo[SQL_COLUMN_ACP_PRIZE_TYPE] = PrizeType.InKind1_TaxesFromAccount;
          }
          else
          {
            if (!GeneralParam.GetBoolean("Cashier", "Promotions.UNR.TaxesToSite.CancelOnRedeem", true))
            {
              // Promotion not apply to redeem (taxes from site)
              _account_promo[SQL_COLUMN_ACP_CASH_IN] = DBNull.Value;
            }
            _account_promo[SQL_COLUMN_ACP_PRIZE_TYPE] = PrizeType.InKind2_TaxesFromSite;
          }
        }

        // WITHHOLD depends on the credit type
        //    - NR1:     WITHHOLD = BALANCE.
        //    - Others:  WITHHOLD = 0.     

        switch (_credit_type)
        {
          case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
            _account_promo[SQL_COLUMN_ACP_WITHHOLD] = (Decimal)AwardedAmount;
            if (AccountPromotion.won_lock_enabled)
            {
              Decimal _lock;

              if (AccountPromotion.won_lock < 0)
              {
                return false;
              }

              _lock = Math.Round((1 + AccountPromotion.won_lock) * AwardedAmount, 2, MidpointRounding.AwayFromZero);
              // When 0, add 0.0001m (can't be rounded!) ---> The lock is broken when something is won
              _lock += AccountPromotion.won_lock == 0 ? 0.0001m : 0;

              _account_promo[SQL_COLUMN_ACP_WONLOCK] = _lock;
            }
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
          case ACCOUNT_PROMO_CREDIT_TYPE.UNR1:
          case ACCOUNT_PROMO_CREDIT_TYPE.UNR2:
          case ACCOUNT_PROMO_CREDIT_TYPE.NR3:
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:
            if (AccountPromotion.type != Promotion.PROMOTION_TYPE.PREASSIGNED)
            {
              _expiration = DateTime.MaxValue;
            }

            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
          default:
            Log.Warning("AddPromotionToAccount: Credit type unknown: " + _credit_type.ToString() + ", AccountId: " + AccountId.ToString() + ", PromotionId: " +
                        AccountPromotion.id.ToString() + ", AccountPromotionBalance: " + AwardedAmount.ToString());

            return false;
        }

        _credit_type = ConvertNonRedeemableToNR1orNR2(_credit_type);
        _account_promo[SQL_COLUMN_ACP_CREDIT_TYPE] = (Int32)_credit_type;
        _account_promo[SQL_COLUMN_ACP_STATUS] = PromoStatus;
        _account_promo[SQL_COLUMN_ACP_CREATED] = _created;

        _account_promo[SQL_COLUMN_ACP_PROMOGAME_ID] = AccountPromotion.PromoGameId;
        _account_promo[SQL_COLUMN_ACP_PYRAMIDAL_DIST] = AccountPromotion.PyramidalDist;

        if (_expiration == DateTime.MaxValue)
        {
          _account_promo[SQL_COLUMN_ACP_EXPIRATION] = DBNull.Value;
        }
        else
        {
          _account_promo[SQL_COLUMN_ACP_EXPIRATION] = _expiration;
        }

        _account_promo[SQL_COLUMN_ACP_PROMO_DATE] = AccountPromotion.actual_promo_date;

        if (InsertFirst)
        {
          DtAccountsPromo.Rows.InsertAt(_account_promo, 0);
        }
        else
        {
          DtAccountsPromo.Rows.Add(_account_promo);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // AddAccountPromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Account_Promotions
    //
    //  PARAMS :
    //      - INPUT:
    //            AccountsPromo
    //            SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Inserted ok. False: Otherwise.
    public static Boolean InsertAccountPromotions(DataTable AccountsPromotions, Boolean OnlyInsertPreassignedPromotions, SqlTransaction SqlTrx)
    {
      return InsertAccountPromotions(AccountsPromotions, OnlyInsertPreassignedPromotions, false, SqlTrx);
    } // InsertAccountPromotions
    public static Boolean InsertAccountPromotions(DataTable AccountsPromotions, Boolean OnlyInsertPreassignedPromotions, Boolean IsPromoGamePromotion, SqlTransaction SqlTrx)
    {
      //Insert in accountPromotions
      StringBuilder _sb;
      Int32 _nr;
      SqlParameter _param;
      DataRow[] _account_promotions;
      DataRow[] _ap_prize_coupon;
      Int32 _ap_prize_coupon_length;

      try
      {
        _sb = new StringBuilder();
        _ap_prize_coupon_length = 0;

        if (OnlyInsertPreassignedPromotions &&  !IsPromoGamePromotion)
        {
          // Select PREASSIGNED Promotions
          _account_promotions = AccountsPromotions.Select("ACP_PROMO_TYPE = " + (Int32)Promotion.PROMOTION_TYPE.PREASSIGNED);
        }
        else if (IsPromoGamePromotion) // PromoGames
        {
          // Get PromoGame Account Promotions with next status Array
          _account_promotions = SetNextStatus_PromoGamePromotions(AccountsPromotions);
        }
        else
        {
          // DDM 13-AUG-2012: If promotions are preassigned, NO INSERT!!
          // MPO 17-AUG-2012: Parse preassigned promotions
          if (!ReadPreassignedPromotionsUniqueId(AccountsPromotions, SqlTrx))
          {
            return false;
          }

          //
          // Update operation id for preassigned promotions
          //
          _account_promotions = AccountsPromotions.Select("ACP_PROMO_TYPE = " + (Int32)Promotion.PROMOTION_TYPE.PREASSIGNED);

          if (_account_promotions.Length > 0)
          {
            foreach (DataRow _row in _account_promotions)
            {
              _row.AcceptChanges();
              _row.SetModified();
            }

            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNT_PROMOTIONS");
            _sb.AppendLine("   SET   ACP_OPERATION_ID = @pOperationId ");
            _sb.AppendLine(" WHERE   ACP_UNIQUE_ID    = @pUniqueId ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "ACP_OPERATION_ID";
              _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "ACP_UNIQUE_ID";

              using (SqlDataAdapter _sql_da = new SqlDataAdapter())
              {
                _sql_da.UpdateCommand = _sql_cmd;
                _sql_da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                _sql_da.UpdateBatchSize = 500;
                _sql_da.ContinueUpdateOnError = true;

                _nr = _sql_da.Update(_account_promotions);

                if (_nr != _account_promotions.Length)
                {
                  return false;
                }
              }
            }
          }

          _account_promotions = AccountsPromotions.Select("     ACP_PROMO_TYPE <> " + (Int32)Promotion.PROMOTION_TYPE.PREASSIGNED +
                                                          " AND ACP_STATUS     <> " + (Int32)ACCOUNT_PROMO_STATUS.PREASSIGNED);

          if (_account_promotions.Length == 0)
          {
            return true;
          }

          // QMP 10-APR-2013: Do not insert Prize Coupon promotion into ACCOUNT_PROMOTIONS
          //                  Accept changes for PrizeCoupon promotions and they won't be inserted.
          _ap_prize_coupon = AccountsPromotions.Select(" ACP_PROMO_TYPE = " + (Int32)Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON);
          foreach (DataRow _row in _ap_prize_coupon)
          {
            _row.AcceptChanges();
          }

          _ap_prize_coupon_length = _ap_prize_coupon.Length;
        }

        _sb.Length = 0;
        _sb.AppendLine("INSERT INTO   ACCOUNT_PROMOTIONS  ");
        _sb.AppendLine("            ( ACP_CREATED         ");
        _sb.AppendLine("            , ACP_ACCOUNT_ID      ");
        _sb.AppendLine("            , ACP_ACCOUNT_LEVEL   ");
        _sb.AppendLine("            , ACP_PROMO_TYPE      ");
        _sb.AppendLine("            , ACP_PROMO_ID        ");
        _sb.AppendLine("            , ACP_PROMO_NAME      ");
        _sb.AppendLine("            , ACP_OPERATION_ID    ");
        _sb.AppendLine("            , ACP_CREDIT_TYPE     ");
        _sb.AppendLine("            , ACP_EXPIRATION      ");
        _sb.AppendLine("            , ACP_INI_BALANCE     ");
        _sb.AppendLine("            , ACP_INI_WITHHOLD    ");
        _sb.AppendLine("            , ACP_INI_WONLOCK     ");
        _sb.AppendLine("            , ACP_BALANCE         ");
        _sb.AppendLine("            , ACP_WITHHOLD        ");
        _sb.AppendLine("            , ACP_WONLOCK         ");
        _sb.AppendLine("            , ACP_PLAYED          ");
        _sb.AppendLine("            , ACP_WON             ");
        _sb.AppendLine("            , ACP_STATUS          ");
        _sb.AppendLine("            , ACP_UPDATED         ");
        _sb.AppendLine("            , ACP_RECOMMENDED_ACCOUNT_ID ");
        _sb.AppendLine("            , ACP_DETAILS                ");
        _sb.AppendLine("            , ACP_PROMO_DATE             ");
        _sb.AppendLine("            , ACP_CASH_IN                ");
        _sb.AppendLine("            , ACP_POINTS                 ");
        _sb.AppendLine("            , ACP_PROMO_CATEGORY_ID     ");
        _sb.AppendLine("            , ACP_PRIZE_TYPE  ");
        _sb.AppendLine("            , ACP_PRIZE_GROSS ");
        _sb.AppendLine("            , ACP_PRIZE_TAX1  ");
        _sb.AppendLine("            , ACP_PRIZE_TAX2  ");
        _sb.AppendLine("            , ACP_PRIZE_TAX3 ) ");
        _sb.AppendLine(" SELECT      @Created                   ");
        _sb.AppendLine("            , @AccountId                 ");
        _sb.AppendLine("            , @AccountLevel              ");
        _sb.AppendLine("            , @PromoType                 ");
        _sb.AppendLine("            , @PromoId                   ");
        _sb.AppendLine("            , @PromoName                 ");
        _sb.AppendLine("            , @OperationId               ");
        _sb.AppendLine("            , @CreditType                ");
        _sb.AppendLine("            , @Expiration                ");
        _sb.AppendLine("            , @Balance                   ");
        _sb.AppendLine("            , @Withhold                  ");
        _sb.AppendLine("            , @Wonlock                   ");
        _sb.AppendLine("            , @Balance                   ");
        _sb.AppendLine("            , @Withhold                  ");
        _sb.AppendLine("            , @Wonlock                   ");
        _sb.AppendLine("            , @Played                    ");
        _sb.AppendLine("            , @Won                       ");
        _sb.AppendLine("            , @Status                    ");
        _sb.AppendLine("            , GETDATE()                  ");
        _sb.AppendLine("            , @RecommendedAccountId      ");
        _sb.AppendLine("            , @Details                   ");
        _sb.AppendLine("            , @Date                      ");
        _sb.AppendLine("            , @CashIn                    ");
        _sb.AppendLine("            , @Points                    ");
        _sb.AppendLine("            , PM_CATEGORY_ID            ");
        _sb.AppendLine("            , ISNULL(@PrizeType, 0) ");
        _sb.AppendLine("            , @PrizeGross ");
        _sb.AppendLine("            , @PrizeTax1  ");
        _sb.AppendLine("            , @PrizeTax2  ");
        _sb.AppendLine("            , @PrizeTax3  ");
        _sb.AppendLine("    FROM      PROMOTIONS  ");
        _sb.AppendLine("   WHERE      PM_PROMOTION_ID = @PromoId ");

        _sb.AppendLine("SET @pUniqueId = SCOPE_IDENTITY()");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@Created", SqlDbType.DateTime).SourceColumn = "ACP_CREATED";
          _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).SourceColumn = "ACP_ACCOUNT_ID";
          _sql_cmd.Parameters.Add("@AccountLevel", SqlDbType.Int).SourceColumn = "ACP_ACCOUNT_LEVEL";
          _sql_cmd.Parameters.Add("@PromoType", SqlDbType.Int).SourceColumn = "ACP_PROMO_TYPE";
          _sql_cmd.Parameters.Add("@PromoId", SqlDbType.BigInt).SourceColumn = "ACP_PROMO_ID";
          _sql_cmd.Parameters.Add("@PromoName", SqlDbType.NVarChar).SourceColumn = "ACP_PROMO_NAME";
          _sql_cmd.Parameters.Add("@OperationId", SqlDbType.BigInt).SourceColumn = "ACP_OPERATION_ID";
          _sql_cmd.Parameters.Add("@CreditType", SqlDbType.Int).SourceColumn = "ACP_CREDIT_TYPE";
          _sql_cmd.Parameters.Add("@Expiration", SqlDbType.DateTime).SourceColumn = "ACP_EXPIRATION";
          _sql_cmd.Parameters.Add("@IniBalance", SqlDbType.Money).SourceColumn = "ACP_BALANCE";
          _sql_cmd.Parameters.Add("@Balance", SqlDbType.Money).SourceColumn = "ACP_BALANCE";
          _sql_cmd.Parameters.Add("@IniWithhold", SqlDbType.Money).SourceColumn = "ACP_WITHHOLD";
          _sql_cmd.Parameters.Add("@Withhold", SqlDbType.Money).SourceColumn = "ACP_WITHHOLD";
          _sql_cmd.Parameters.Add("@IniWonlock", SqlDbType.Money).SourceColumn = "ACP_WONLOCK";
          _sql_cmd.Parameters.Add("@Wonlock", SqlDbType.Money).SourceColumn = "ACP_WONLOCK";
          _sql_cmd.Parameters.Add("@Played", SqlDbType.Money).Value = 0;
          _sql_cmd.Parameters.Add("@Won", SqlDbType.Money).Value = 0;
          _sql_cmd.Parameters.Add("@Status", SqlDbType.Int).SourceColumn = "ACP_STATUS";
          _sql_cmd.Parameters.Add("@RecommendedAccountId", SqlDbType.BigInt).SourceColumn = "ACP_RECOMMENDED_ACCOUNT_ID";
          _sql_cmd.Parameters.Add("@Details", SqlDbType.NVarChar).SourceColumn = "ACP_DETAILS";
          _sql_cmd.Parameters.Add("@Date", SqlDbType.DateTime).SourceColumn = "ACP_PROMO_DATE";
          _sql_cmd.Parameters.Add("@CashIn", SqlDbType.Money).SourceColumn = "ACP_CASH_IN";
          _sql_cmd.Parameters.Add("@Points", SqlDbType.Money).SourceColumn = "ACP_POINTS";
          // UNR
          _sql_cmd.Parameters.Add("@PrizeType", SqlDbType.Int).SourceColumn = "ACP_PRIZE_TYPE";
          _sql_cmd.Parameters.Add("@PrizeGross", SqlDbType.Money).SourceColumn = "ACP_PRIZE_GROSS";
          _sql_cmd.Parameters.Add("@PrizeTax1", SqlDbType.Money).SourceColumn = "ACP_PRIZE_TAX1";
          _sql_cmd.Parameters.Add("@PrizeTax2", SqlDbType.Money).SourceColumn = "ACP_PRIZE_TAX2";
          _sql_cmd.Parameters.Add("@PrizeTax3", SqlDbType.Money).SourceColumn = "ACP_PRIZE_TAX3";

          _param = _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _param.SourceColumn = "ACP_UNIQUE_ID";
          _param.Direction = ParameterDirection.Output;

          _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;

            _nr = _sql_da.Update(_account_promotions);

            // Take in account the prize coupon promotions that are not inserted.
            if (_nr == _account_promotions.Length - _ap_prize_coupon_length)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertAccountPromotions

    /// <summary>
    /// Set next status on PromoGame Promotions
    /// </summary>
    /// <param name="AccountsPromotions"></param>
    /// <returns></returns>
    private static DataRow[] SetNextStatus_PromoGamePromotions(DataTable AccountsPromotions)
    {
      foreach (DataRow _row in AccountsPromotions.Select())
      {
        // Check if is PromoGame promotion
        if ((Int64)_row["PM_PROMOGAME_ID"] <= TerminalDraw.TerminalDraw.WELCOME_DRAW_ID)
        {
          continue;
        }

        // Set next status
        switch ((Promotion.PROMOTION_TYPE)_row["ACP_PROMO_TYPE"])
        {
          case Promotion.PROMOTION_TYPE.PREASSIGNED:
          case Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY:
          case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL:
          case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED:
            _row["ACP_STATUS"] = (Int32)ACCOUNT_PROMO_STATUS.PENDING_PLAYER;
            break;

          default:
            _row["ACP_STATUS"] = (Int32)ACCOUNT_PROMO_STATUS.PENDING_DRAW;
            break;
        }                
      }
      
      return AccountsPromotions.Select();
    } // SetNextStatus_PromoGamePromotions

    private static Boolean ReadPreassignedPromotionsUniqueId(DataTable AccountsPromotions, SqlTransaction SqlTrx)
    {
      Object _unique_id;
      StringBuilder _sql_txt;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("  SELECT   TOP 1 ACP_UNIQUE_ID ");
        _sql_txt.AppendLine("    FROM   ACCOUNT_PROMOTIONS ");
        _sql_txt.AppendLine("   WHERE   ACP_PROMO_ID   = @pPromoId ");
        _sql_txt.AppendLine("     AND   ACP_STATUS     = @pStatusPreassigned ");
        _sql_txt.AppendLine("     AND   ACP_ACCOUNT_ID = @pAccountId ");
        _sql_txt.AppendLine("     AND   ACP_BALANCE    = @pBalance ");
        _sql_txt.AppendLine("     AND   GETDATE() < ISNULL(ACP_EXPIRATION, GETDATE()) ");
        _sql_txt.AppendLine("ORDER BY   ACP_UNIQUE_ID ASC ");

        foreach (DataRow _row_acp in AccountsPromotions.Rows)
        {
          if (!_row_acp["ACP_PROMO_TYPE"].Equals((Int32)Promotion.PROMOTION_TYPE.PREASSIGNED))
          {
            continue;
          }

          _row_acp["ACP_STATUS"] = (Int32)ACCOUNT_PROMO_STATUS.PREASSIGNED;

          using (SqlCommand _cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = _row_acp["ACP_PROMO_ID"];
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _row_acp["ACP_ACCOUNT_ID"];
            _cmd.Parameters.Add("@pBalance", SqlDbType.Decimal).Value = _row_acp["ACP_BALANCE"];
            _cmd.Parameters.Add("@pStatusPreassigned", SqlDbType.Int).Value = _row_acp["ACP_STATUS"];

            _unique_id = _cmd.ExecuteScalar();

            if (_unique_id == null)
            {
              return false;
            }

            if (_unique_id == DBNull.Value)
            {
              return false;
            }

            _row_acp["ACP_UNIQUE_ID"] = _unique_id;
          }
        }

        return true;
      }
      catch
      { ; }

      return false;
    } // ParsePreassignedPromotions

    //------------------------------------------------------------------------------
    // PURPOSE : Filter only accounts that they can apply the promotion
    //
    //  PARAMS :
    //      - INPUT : 
    //            - Accounts
    //            - PreviouslyAwarded : Datatable with the promotions previously awarded
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataRow with accounts
    public DataRow[] FilterAccounts(DataTable Accounts, DataTable PreviouslyAwarded)
    {
      Int32 _account_level;
      DateTime _today;
      DataRow[] _filtered_accounts;
      Int32 _idx_account;

      _today = Misc.TodayOpening();

      _idx_account = 0;
      _filtered_accounts = new DataRow[Accounts.Rows.Count];
      // Account indexs
      //  0 --> AC_ACCOUNT_ID
      //  1 --> AC_HOLDER_LEVEL
      //  2 --> AC_HOLDER_BIRTH_DATE
      foreach (DataRow _account in Accounts.Rows)
      {
        _account_level = (Int32)_account[1];
        if ((this.level_filter & (1 << _account_level)) == 0)
        {
          continue;
        }

        // per birthday
        if (this.type == Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY)
        {
          if (_account.IsNull(2))
          {
            continue;
          }

          if (!Misc.IsClientBirthday(_today, (DateTime)_account[2]))
          {
            continue;
          }

          // Check birthday promotion awarded in less than one year
          if (PreviouslyAwarded.Select("ACP_ACCOUNT_ID = " + _account[0]).Length > 0)
          {
            continue;
          }
        }

        _filtered_accounts[_idx_account] = _account;
        _idx_account += 1;
      } // foreach

      Array.Resize(ref _filtered_accounts, _idx_account);

      return _filtered_accounts;
    } // FilterAccounts

    //------------------------------------------------------------------------------
    // PURPOSE : Update next_execution field in the Promotions Table
    //
    //  PARAMS :
    //      - INPUT:
    //            - PromotionId
    //            - PromotionType
    //            - ActualPromoDate
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Updated ok. False: Otherwise.
    public static Boolean UpdateNextExecution(Int64 PromotionId,
                                              Promotion.PROMOTION_TYPE PromotionType,
                                              DateTime ActualPromoDate,
                                              Boolean PromoApplied,
                                              SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DateTime _next_execution;

      _next_execution = ActualPromoDate;

      try
      {
        switch (PromotionType)
        {
          case Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY:
            _next_execution = _next_execution.AddDays(1);
            break;

          default:
            do
            {
              _next_execution = _next_execution.AddMonths(1);
            } while (ActualPromoDate.Day != _next_execution.Day);

            break;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   PROMOTIONS  ");
        _sb.AppendLine("    SET   PM_NEXT_EXECUTION  = @pNextExecution ");
        if (PromoApplied)
        {
          _sb.AppendLine("        , PM_LAST_EXECUTED   = @pLastExecuted ");
        }
        _sb.AppendLine("  WHERE   PM_PROMOTION_ID    = @pPromotionId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = PromotionId;
          _sql_cmd.Parameters.Add("@pNextExecution", SqlDbType.DateTime).Value = _next_execution;
          _sql_cmd.Parameters.Add("@pLastExecuted", SqlDbType.DateTime).Value = ActualPromoDate;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateNextExecution

    //------------------------------------------------------------------------------
    // PURPOSE : Check if promotions is apply
    //
    //  PARAMS :
    //      - INPUT:
    //            PromotionType
    //            LastExecuted
    //            Now
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If have to apply. False: Otherwise.
    public static Boolean HaveToApply(Promotion.PROMOTION_TYPE PromotionType, DateTime LastExecuted, DateTime Now)
    {
      DateTime _today;
      DateTime _last;

      if (LastExecuted == DateTime.MinValue)
      {
        return true;
      }

      _today = Misc.Opening(Now);
      _last = Misc.Opening(LastExecuted);

      if (_last >= _today)
      {
        return false;
      }

      switch (PromotionType)
      {
        case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL:
        case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED:

          _last = new DateTime(_last.Year, _last.Month, _today.Day, _today.Hour, _today.Minute, 0);

          return (_last < _today);

        case Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY:
          return true;

        default:
          return false;
      }
    } // HaveToApply

    //------------------------------------------------------------------------------
    // PURPOSE : Get promotions awarded
    //
    //  PARAMS :
    //      - INPUT:
    //            - IniDate
    //            - FinDate
    //            - PromotionType
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Return datatable with promotions awarded
    public static DataTable GetPreviouslyAwarded(DateTime IniDate,
                                                 DateTime FinDate,
                                                 Int64 PromoId,
                                                 SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _previously_awarded;

      _previously_awarded = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   ACP_ACCOUNT_ID                    ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_promo_id_promo_date)) ");
        _sb.AppendLine("  WHERE   ACP_PROMO_ID = @pPromoId          ");
        _sb.AppendLine("    AND   ACP_PROMO_DATE >= @pIniDate       ");
        _sb.AppendLine("    AND   ACP_PROMO_DATE <  @pFinDate       ");
        //_sb.AppendLine("    AND   ACP_STATUS     = @pPromotionStatus");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pIniDate", SqlDbType.DateTime).Value = IniDate;
          _sql_cmd.Parameters.Add("@pFinDate", SqlDbType.DateTime).Value = FinDate;
          _sql_cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromoId;
          //_sql_cmd.Parameters.Add("@pPromotionStatus", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.AWARDED;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_previously_awarded);

            return _previously_awarded;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _previously_awarded;
    } // GetPreviouslyAwarded

    // LTC 13-OCT-2016
    private static Boolean Trx_ActivatePromotion(DataRow AccountPromotion,
                                                 AccountMovementsTable AccountMovements,
                                                 CashierMovementsTable CashierMovements,
                                                 String Details,  // NOT USED BUT MUST BE DEFINED
                                                 out MultiPromos.PromoBalance PromoBalance,
                                                 SqlTransaction SqlTrx)
    {
      return Trx_ActivatePromotion(AccountPromotion, AccountMovements, CashierMovements, false, Details, out PromoBalance, SqlTrx, CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Activate promotion and updated balance to the account.
    //
    //  PARAMS :
    //      - INPUT:
    //            - AccountPromotion
    //            - AccountMovements
    //            - CashierMovements
    //            - Details
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - Balance
    //
    // RETURNS :
    //      - True: Updated ok. False: Otherwise.
    private static Boolean Trx_ActivatePromotion(DataRow AccountPromotion,
                                                 AccountMovementsTable AccountMovements,
                                                 CashierMovementsTable CashierMovements,
                                                 Boolean AddPromotionToAccountBalance,
                                                 String Details,  // NOT USED BUT MUST BE DEFINED
                                                 out MultiPromos.PromoBalance PromoBalance,
                                                 SqlTransaction SqlTrx,
                                                 CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskDraw = CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00, // LTC 13-OCT-2016
                                                 Boolean IsPendingTerminalDraw = false)

    {
      Decimal _promo_balance;
      Int64 _unique_id;
      Int64 _account_id;
      Int64 _promotion_id;
      Int64 _operation_id;
      StringBuilder _sb;
      ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
      MovementType _movement_type_account;
      CASHIER_MOVEMENT _movement_type_cashier;
      ACCOUNT_PROMO_STATUS _new_status;
      MultiPromos.AccountBalance _ini_bal;
      MultiPromos.AccountBalance _fin_bal;
      Decimal _ini_points;
      Decimal _fin_points;
      Decimal _ini_balance;
      Decimal _fin_balance;
      Promotion.PROMOTION_TYPE _promo_type;
      String _promo_name;
      Int64 _dummy_ps;
      DataTable _awarded_flags;
      Int64 _user_id;
      Boolean _is_prize_coupon;
      PrizeType _prize_type;
      Decimal _prize_gross;
      Decimal _prize_tax1;
      Decimal _prize_tax2;
      Decimal _prize_tax3;
      MultiPromos.AccountBalance _account_balance;

      PromoBalance = MultiPromos.PromoBalance.Zero;
      _prize_type = PrizeType.None;
      _prize_gross = 0;
      _prize_tax1 = 0;
      _prize_tax2 = 0;
      _prize_tax3 = 0;

      try
      {
        // 1 - Lock account.
        _account_id = (Int64)AccountPromotion[SQL_COLUMN_ACP_ACCOUNT_ID];
        _unique_id = (Int64)AccountPromotion[SQL_COLUMN_ACP_UNIQUE_ID];

        if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, MultiPromos.AccountBalance.Zero, out _ini_bal, out _ini_points, out _dummy_ps, SqlTrx))
        {
          return false;
        }

        // RCI & DHA 16-APR-2013: Check existence of the ACP_PROMO_TYPE column.
        _is_prize_coupon = false;
        if (AccountPromotion.Table.Columns.Contains("ACP_PROMO_TYPE") && !AccountPromotion.IsNull("ACP_PROMO_TYPE"))
        {
          Promotion.PROMOTION_TYPE _promo_type_aux;

          _promo_type_aux = (Promotion.PROMOTION_TYPE)AccountPromotion["ACP_PROMO_TYPE"];
          _is_prize_coupon = (_promo_type_aux == Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON);
        }

        if (_is_prize_coupon)
        {
          _promo_balance = (Decimal)AccountPromotion[SQL_COLUMN_ACP_BALANCE];
          _promo_balance = Math.Max(0, _promo_balance);
          _credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)AccountPromotion[SQL_COLUMN_ACP_CREDIT_TYPE];
          _promotion_id = (Int64)AccountPromotion[SQL_COLUMN_ACP_PROMO_ID];
          _operation_id = (Int64)AccountPromotion[SQL_COLUMN_ACP_OPERATION_ID];
          _new_status = (ACCOUNT_PROMO_STATUS)AccountPromotion[SQL_COLUMN_ACP_STATUS];
          _promo_type = (Promotion.PROMOTION_TYPE)AccountPromotion[SQL_COLUMN_ACP_PROMO_TYPE];

          // ATB 10-NOV-2016
          // In case of Televisa, and the promotion is "Raffle Result", it only shows the string
          if (Misc.IsVoucherModeTV() && (Promotion.PROMOTION_TYPE)AccountPromotion[SQL_COLUMN_ACP_PROMO_TYPE] == Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE)
          {
            _promo_name = Resource.String("STR_PROMOTION_RAFFLE_RESULT");
          }
          else
          {
            _promo_name = (String)AccountPromotion[SQL_COLUMN_ACP_PROMO_NAME];
          }
        }
        else
        {
          // 2 - Activate promotion.
          try
          {
            _sb = new StringBuilder();
            _sb.AppendLine("  DECLARE @Now AS DATETIME    ");

            _sb.AppendLine("      SET @Now = GETDATE()    ");
            _sb.AppendLine("   UPDATE  ACCOUNT_PROMOTIONS ");
            _sb.AppendLine("      SET                     ");

            // Only whe is not PlayDraw promotion
            if (!IsPendingTerminalDraw)
            {
              _sb.AppendLine("         ACP_STATUS     = (CASE WHEN ACP_EXPIRATION <= @Now                               ");
              _sb.AppendLine("                                THEN @pStatusExpired                                      ");
              _sb.AppendLine("                                ELSE (CASE WHEN ACP_CREDIT_TYPE IN ( @pCreditRedeemable   ");
              _sb.AppendLine("                                                                   , @pCreditPoint      ) ");
              _sb.AppendLine("                                           THEN @pStatusRedeemed                          ");
              _sb.AppendLine("                                           ELSE @pStatusActive                            ");
              _sb.AppendLine("                                      END)                                                ");
              _sb.AppendLine("                           END)                                                           ");
              _sb.AppendLine("       ,                                                                                  ");
            }
            
            _sb.AppendLine("           ACP_ACTIVATION = (CASE WHEN ACP_EXPIRATION <= @Now ");
            _sb.AppendLine("                                  THEN NULL                   ");
            _sb.AppendLine("                                  ELSE @Now                   ");
            _sb.AppendLine("                             END)                             ");
            _sb.AppendLine("         , ACP_UPDATED    = @Now                              ");

            if (AccountPromotion.Table.Columns.Contains("ACP_PROMO_TYPE") && (Promotion.PROMOTION_TYPE)AccountPromotion[SQL_COLUMN_ACP_PROMO_TYPE] == Promotion.PROMOTION_TYPE.PREASSIGNED)
            {
              _sb.AppendLine("         , ACP_EXPIRATION    = @pExpirationDate     ");
            }


            _sb.AppendLine("  OUTPUT   INSERTED.ACP_BALANCE                               "); // 0
            _sb.AppendLine("         , INSERTED.ACP_CREDIT_TYPE                           "); // 1
            _sb.AppendLine("         , INSERTED.ACP_PROMO_ID                              "); // 2
            _sb.AppendLine("         , INSERTED.ACP_OPERATION_ID                          "); // 3
            _sb.AppendLine("         , INSERTED.ACP_STATUS                                "); // 4
            _sb.AppendLine("         , INSERTED.ACP_PROMO_TYPE                            "); // 5
            _sb.AppendLine("         , INSERTED.ACP_PROMO_NAME                            "); // 6
            // UNR
            _sb.AppendLine("         , ISNULL (INSERTED.ACP_PRIZE_TYPE,  0)               "); // 7
            _sb.AppendLine("         , ISNULL (INSERTED.ACP_PRIZE_GROSS, 0)               "); // 8
            _sb.AppendLine("         , ISNULL (INSERTED.ACP_PRIZE_TAX1,  0)               "); // 9
            _sb.AppendLine("         , ISNULL (INSERTED.ACP_PRIZE_TAX2,  0)               "); // 10
            _sb.AppendLine("         , ISNULL (INSERTED.ACP_PRIZE_TAX3,  0)               "); // 11

            _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions))                          ");
            _sb.AppendLine("   WHERE   ACP_UNIQUE_ID  = @pUniqueId                                                    ");
            _sb.AppendLine("     AND   ACP_STATUS     in (@pStatusAwarded, @pStatusPreAssigned, @pStatusPendingDraw)  ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pStatusAwarded", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.AWARDED;
              _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
              _sql_cmd.Parameters.Add("@pStatusExpired", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.EXPIRED;
              _sql_cmd.Parameters.Add("@pStatusRedeemed", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.REDEEMED;
              _sql_cmd.Parameters.Add("@pStatusPreAssigned", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.PREASSIGNED;
              _sql_cmd.Parameters.Add("@pStatusPendingDraw", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.PENDING_DRAW;
              _sql_cmd.Parameters.Add("@pCreditRedeemable", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE;
              _sql_cmd.Parameters.Add("@pCreditPoint", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.POINT;
              _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _unique_id;

              if (AccountPromotion.Table.Columns.Contains("ACP_PROMO_TYPE") && (Promotion.PROMOTION_TYPE)AccountPromotion[SQL_COLUMN_ACP_PROMO_TYPE] == Promotion.PROMOTION_TYPE.PREASSIGNED)
              {
                _sql_cmd.Parameters.Add("@pExpirationDate", SqlDbType.DateTime).Value = AccountPromotion[SQL_COLUMN_ACP_EXPIRATION];
              }

              using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
              {
                if (!_reader.Read())
                {
                  Log.Warning("Trx_ActivatePromotion: Promotion not found.  UniqueId: " + _unique_id.ToString() + ", AccountId: " + _account_id.ToString());

                  return false;
                }

                _promo_balance = _reader.GetDecimal(0);
                _promo_balance = Math.Max(0, _promo_balance);
                _credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)_reader.GetInt32(1);
                _promotion_id = _reader.GetInt64(2);
                _operation_id = _reader.GetInt64(3);


                _new_status = (ACCOUNT_PROMO_STATUS)_reader.GetInt32(4);
                _promo_type = (Promotion.PROMOTION_TYPE)_reader.GetInt32(5);

                // ATB 10-NOV-2016
                // In case of Televisa, and the promotion is "Raffle Result", it only shows the string
                if (Misc.IsVoucherModeTV() && _promo_type == Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE)
                {
                  _promo_name = Resource.String("STR_PROMOTION_RAFFLE_RESULT");
                }
                else
                {
                  _promo_name = _reader.GetString(6);
                }

                // UNR
                _prize_type = (PrizeType)_reader.GetInt32(7);
                if (_prize_type != PrizeType.None)
                {
                  _prize_gross = _reader.GetDecimal(8);
                  _prize_tax1 = _reader.GetDecimal(9);
                  _prize_tax2 = _reader.GetDecimal(10);
                  _prize_tax3 = _reader.GetDecimal(11);
                }
              }
            }
          }
          catch (SqlException _sql_ex)
          {
            Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");

            return false;
          }
        }

        if (_promo_balance == 0)
        {
          return true;
        }

        _ini_balance = _ini_bal.TotalBalance;

        switch (_credit_type)
        {
          case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
          case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
            PromoBalance.Balance.PromoNotRedeemable = _promo_balance;
            _movement_type_account = MovementType.PromotionNotRedeemable;
            _movement_type_cashier = CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE;
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
            PromoBalance.Balance.PromoRedeemable = _promo_balance;
            _movement_type_account = (_promo_type == Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON) ? MovementType.PrizeCoupon : MovementType.PromotionRedeemable;
            _movement_type_cashier = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE;
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:
            PromoBalance.Balance.PromoRedeemable = _promo_balance;
            _movement_type_account = MovementType.PromotionRedeemable;
            _movement_type_cashier = (_prize_type == PrizeType.InKind1_TaxesFromAccount) ? CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS : CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS;
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
            PromoBalance.Points = _promo_balance;
            _movement_type_account = MovementType.PromotionPoint;
            _movement_type_cashier = CASHIER_MOVEMENT.PROMOTION_POINT;
            _ini_balance = _ini_points;
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
          default:
            Log.Warning("Trx_ActivatePromotion: Credit type unknown: " + _credit_type.ToString() + ", UniqueId: " + _unique_id.ToString() + " OperationId: " + _operation_id.ToString() +
                        ", AccountId: " + _account_id.ToString() + ", PromotionId: " + _promotion_id.ToString() + ", AccountPromotionBalance: " + _promo_balance.ToString());

            return false;
        }

        if (_new_status == ACCOUNT_PROMO_STATUS.EXPIRED)
        {
          return true;
        }

        // LTC 13-OCT-2016
        //if (CashDeskDraw == CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_01 && CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws(Convert.ToInt16(CashDeskDraw)))
        //{
        //  PromoBalance.Balance.Redeemable += _promo_balance;
        //  PromoBalance.Balance.PromoRedeemable -= _promo_balance;
        //}

        // 
        _account_balance = PromoBalance.Balance;

        // In Promotions with related PromoGame won't add promotion credit to account.        
        if (Misc.IsPromoGamesEnabled() && !AddPromotionToAccountBalance)
        {
          _account_balance = MultiPromos.AccountBalance.Zero;
        }

        //  3 - Update Accounts. PromoBalance.Balance
        if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, _account_balance, PromoBalance.Points, out _fin_bal, out _fin_points, SqlTrx))
        {
          return false;
        }

        switch (_credit_type)
        {
          case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
          case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
            if (_ini_bal.TotalNotRedeemable == 0)
            {
              if (!MultiPromos.Trx_AccountPromotionCost(_account_id, ACCOUNTS_PROMO_COST.SET_ON_FIRST_NR_PROMOTION, _ini_bal.TotalRedeemable, SqlTrx))
              {
                return false;
              }
            }

            _fin_balance = _fin_bal.TotalBalance;
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:
            if (!MultiPromos.Trx_AccountPromotionCost(_account_id, ACCOUNTS_PROMO_COST.UPDATE_ON_RECHARGE, PromoBalance.Balance.PromoRedeemable, SqlTrx))
            {
              return false;
            }

            _fin_balance = _fin_bal.TotalBalance;
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
            _fin_balance = _fin_points;
            break;

          default:
            //Can't be. It's checked in the previous switch.
            return false;
        }

        //  4 - Account and Cashier movements.
        if (_promo_type != Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN 
            && (!AccountPromotion.Table.Columns.Contains("ACP_PROMOGAME_ID") || AccountPromotion[SQL_COLUMN_ACP_PROMOGAME_ID] == DBNull.Value))
        {
          if (AccountMovements != null)
          {
            AccountMovements.Add(_operation_id, _account_id, _movement_type_account, _ini_balance, 0, _promo_balance, _fin_balance, _promo_name);
          }

          if (CashierMovements != null && _prize_type == PrizeType.None)
          {
            CashierMovements.Add(_operation_id, _movement_type_cashier, _promo_balance, _account_id, "", _promo_name);
          }
        }

        if (_promo_type == Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND)
        {
          switch (_prize_type)
          {
            case PrizeType.InKind2_TaxesFromSite:
            case PrizeType.InKind1_TaxesFromAccount:
              {
                if (!CashDeskDrawBusinessLogic.GenerateMovementsReInKind(_operation_id, _account_id,
                                                                         _prize_type, _prize_gross, _prize_tax1, _prize_tax2, _prize_tax3,
                                                                         AccountMovements, CashierMovements, SqlTrx))
                {
                  Log.Warning("GenerateMovementsReInKind failed. Credit type: " + _credit_type.ToString() + ", UniqueId: " + _unique_id.ToString() + " OperationId: " + _operation_id.ToString() +
                              ", AccountId: " + _account_id.ToString() + ", PromotionId: " + _promotion_id.ToString() + ", AccountPromotionBalance: " + _promo_balance.ToString());
                  return false;
                }
              }
              break;

            case PrizeType.None:
              // Do nothing;
              break;

            default:
              Log.Warning("Unknown PrizeType: " + _prize_type.ToString() + ". Credit type: " + _credit_type.ToString() + ", UniqueId: " + _unique_id.ToString() + " OperationId: " + _operation_id.ToString() +
                          ", AccountId: " + _account_id.ToString() + ", PromotionId: " + _promotion_id.ToString() + ", AccountPromotionBalance: " + _promo_balance.ToString());
              return false;
          }
        }
        else
        {
          switch (_prize_type)
          {
            case PrizeType.InKind2_TaxesFromSite:
            case PrizeType.InKind1_TaxesFromAccount:
              {
                if (!CashDeskDrawBusinessLogic.GenerateMovementsUNR(_operation_id, _account_id,
                                                                    _prize_type, _prize_gross, _prize_tax1, _prize_tax2, _prize_tax3,
                                                                    AccountMovements, CashierMovements, SqlTrx))
                {
                  Log.Warning("GenerateMovementsUNR failed. Credit type: " + _credit_type.ToString() + ", UniqueId: " + _unique_id.ToString() + " OperationId: " + _operation_id.ToString() +
                              ", AccountId: " + _account_id.ToString() + ", PromotionId: " + _promotion_id.ToString() + ", AccountPromotionBalance: " + _promo_balance.ToString());
                  return false;
                }
              }
              break;

            case PrizeType.None:
              // Do nothing;
              break;

            default:
              Log.Warning("Unknown PrizeType: " + _prize_type.ToString() + ". Credit type: " + _credit_type.ToString() + ", UniqueId: " + _unique_id.ToString() + " OperationId: " + _operation_id.ToString() +
                          ", AccountId: " + _account_id.ToString() + ", PromotionId: " + _promotion_id.ToString() + ", AccountPromotionBalance: " + _promo_balance.ToString());
              return false;
          }
        }

        // 5 - Awarded flags
        if (Promotion.ReadAwardedPromotionFlags(_promotion_id, SqlTrx, out _awarded_flags))
        {
          _user_id = (CashierMovements != null && CashierMovements.CashierSessionInfo != null) ? CashierMovements.CashierSessionInfo.AuthorizedByUserId : -1;
          Promotion.AddFlagsToAccount(_awarded_flags, _account_id, _promotion_id, _user_id, SqlTrx);
        }
        else
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_ActivatePromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Activate promotions and updated balance to the accounts.
    //
    //  PARAMS :
    //      - INPUT:
    //            - AccountPromotions
    //            - AccountMovements
    //            - CashierMovements
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Updated ok. False: Otherwise.
    public static Boolean Trx_ActivatePromotionsOnAccount(DataTable AccountPromotions,
                                                          AccountMovementsTable AccountMovements,
                                                          CashierMovementsTable CashierMovements,
                                                          SqlTransaction SqlTrx,
                                                          CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskDraw = CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00) // LTC 13-OCT-2016
    {
      return Trx_ActivatePromotionsOnAccount(AccountPromotions, AccountMovements, CashierMovements, false, SqlTrx, CashDeskDraw);
    }
    public static Boolean Trx_ActivatePromotionsOnAccount(DataTable AccountPromotions,
                                                          AccountMovementsTable AccountMovements,
                                                          CashierMovementsTable CashierMovements,
                                                          Boolean AddPromotionToAccountBalance,
                                                          SqlTransaction SqlTrx,
                                                          CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskDraw = CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00, // LTC 13-OCT-2016
                                                          Boolean IsPlayDrawOption = false)

    {
      MultiPromos.PromoBalance _promo_balance;

      foreach (DataRow _promo in AccountPromotions.Rows)
      {
        if (!Trx_ActivatePromotion(_promo, AccountMovements, CashierMovements, AddPromotionToAccountBalance, null, out _promo_balance, SqlTrx, CashDeskDraw, IsPlayDrawOption))
        {
          return false;
        }
      }

      return true;
    } // Trx_ActivatePromotionsOnAccount

    //------------------------------------------------------------------------------
    // PURPOSE : Perform an action to each Promotion of the table AccountPromotions.
    //           Commit for each Promotion.
    //
    //  PARAMS :
    //      - INPUT:
    //            - PromotionAction
    //            - AccountPromotions
    //
    //      - OUTPUT :
    //            - TotalBalance
    //            - AccountPromotionsUpdated
    //
    // RETURNS :
    //      - Number of applied promotions.
    public static Int32 Trx_PromotionApplySystemAction(PromotionAction PromotionAction,
                                                       DataTable AccountPromotions,
                                                       String Details,
                                                       out MultiPromos.PromoBalance TotalBalance,
                                                       out DataTable AccountPromotionsUpdated)
    {
      CashierSessionInfo _session;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      Int32 _num_applied;
      MultiPromos.PromoBalance _promo_balance;

      TotalBalance = MultiPromos.PromoBalance.Zero;
      _num_applied = 0;
      AccountPromotionsUpdated = CreateAccountPromotionTable();

      foreach (DataRow _promo in AccountPromotions.Rows)
      {
        try
        {
          _session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
          if (_session == null)
          {
            break;
          }

          _account_movements = new AccountMovementsTable(_session);
          _cashier_movements = new CashierMovementsTable(_session);

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!PromotionAction(_promo, _account_movements, _cashier_movements, Details, out _promo_balance, _db_trx.SqlTransaction))
            {
              continue;
            }

            // If not save Account or Cashier movements, rollback. Proceed to the next promotion.
            if (!_account_movements.Save(_db_trx.SqlTransaction))
            {
              continue;
            }

            if (!_cashier_movements.Save(_db_trx.SqlTransaction))
            {
              continue;
            }

            _db_trx.Commit();

            // DDM 21-AUG-2012: Update the balance with the returned value
            AccountPromotionsUpdated.Rows.Add(_promo.ItemArray);
            TotalBalance += _promo_balance;
            _num_applied++;

          } // DB_TRX
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // foreach

      return _num_applied;

    } // Trx_PromotionApplySystemAction

    //------------------------------------------------------------------------------
    // PURPOSE : Perform an action to each Promotion of the table AccountPromotions.
    //           Transaction control is from outside.
    //
    //  PARAMS :
    //      - INPUT:
    //            - PromotionAction
    //            - AccountPromotions
    //            - Trx
    //
    //      - OUTPUT :
    //            - TotalBalance
    //
    // RETURNS :
    //      - Number of applied promotions
    public static Int32 Trx_PromotionApplySystemAction(PromotionAction PromotionAction,
                                                       DataTable AccountPromotions,
                                                       Boolean InsertMovements,
                                                       out MultiPromos.PromoBalance TotalBalance,
                                                       SqlTransaction Trx)
    {
      CashierSessionInfo _session;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      Int32 _num_applied;
      MultiPromos.PromoBalance _promo_balance;

      TotalBalance = MultiPromos.PromoBalance.Zero;
      _num_applied = 0;

      try
      {
        _account_movements = null;
        _cashier_movements = null;

        if (InsertMovements)
        {
          _session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);

          if (_session == null)
          {
            return 0;
          }

          _account_movements = new AccountMovementsTable(_session);
          _cashier_movements = new CashierMovementsTable(_session);
        }

        foreach (DataRow _promo in AccountPromotions.Rows)
        {
          try
          {
            if (!PromotionAction(_promo, _account_movements, _cashier_movements, null, out _promo_balance, Trx))
            {
              continue;
            }

            TotalBalance += _promo_balance;
            _num_applied++;

          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        } // foreach

        if (InsertMovements)
        {
          // If not save Account or Cashier movements, rollback. Proceed to the next promotion.
          if (!_account_movements.Save(Trx))
          {
            return 0;
          }

          if (!_cashier_movements.Save(Trx))
          {
            return 0;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _num_applied;

    } // Trx_PromotionApplySystemAction

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate and save the cost of promotion.
    //
    //  PARAMS :
    //      - INPUT:
    //            - AccountId
    //            - AccountPromotions
    //            - BalanceRERefund
    //            - BalanceREAfterRefund
    //            - Trx
    //
    //      - OUTPUT :
    //            - None
    //
    // RETURNS :
    //      -  Updated ok. False: Otherwise. 
    public static Boolean Trx_SetRedeemableCostOfPromotionNRE(Int64 AccountId,
                                                              DataTable AccountPromotions,
                                                              Decimal BalanceRERefund,
                                                              Decimal BalanceREAfterRefund,
                                                              SqlTransaction Trx)
    {
      StringBuilder _sb;
      Decimal _redeemable_cost;
      Decimal _promo_ini_re_bal;
      DataRow[] _promotions;
      Int64 _acp_unique_id;

      try
      {
        // 1 - Get Initial redeemable.
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   AC_PROMO_INI_RE_BALANCE ");
        _sb.AppendLine("   FROM   ACCOUNTS ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId");

        _promo_ini_re_bal = 0;

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
          {
            if (!_sql_rd.Read())
            {
              return false;
            }

            if (_sql_rd.IsDBNull(0))
            {
              return true;
            }

            _promo_ini_re_bal = _sql_rd.GetDecimal(0);
          }
        }

        // 2 - Update ACCOUNTS
        if (!MultiPromos.Trx_AccountPromotionCost(AccountId, ACCOUNTS_PROMO_COST.UPDATE_ON_REDEEM, BalanceRERefund, Trx))
        {
          return false;
        }

        if (!MultiPromos.Trx_AccountPromotionCost(AccountId, ACCOUNTS_PROMO_COST.RESET_ON_REDEEM, BalanceREAfterRefund, Trx))
        {
          return false;
        }

        // 3 - Update cost promotion in ACCOUNT_PROMOTIONS
        if (BalanceRERefund > _promo_ini_re_bal)
        {
          _redeemable_cost = BalanceRERefund - _promo_ini_re_bal;
        }
        else
        {
          _redeemable_cost = 0;
        }

        // There is not RE cost, account_promotions not be update
        if (_redeemable_cost <= 0)
        {
          return true;
        }

        List<Int32> _credits;
        _credits = new List<int>();
        _credits.Add((Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR1);
        _credits.Add((Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR2);

        _acp_unique_id = 0;
        if (AccountPromotions.Rows.Count > 0)
        {
          foreach (Int32 _credit in _credits)
          {
            _promotions = AccountPromotions.Select("ACP_CREDIT_TYPE = " + _credit + " AND ACP_PLAYED > 0 ", "ACP_ACTIVATION DESC");
            if (_promotions.Length >= 1)
            {
              _acp_unique_id = (Int64)_promotions[0]["ACP_UNIQUE_ID"];

              break;
            }
          }
        }

        if (_acp_unique_id == 0)
        {
          // When there are not NR promotions, the cost is assigned to last activated promotion 
          _sb.Length = 0;
          _sb.AppendLine(" SELECT   TOP 1 ACP_UNIQUE_ID ");
          _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status)) ");
          _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID   = @pAccountId ");
          _sb.AppendLine("    AND   ACP_STATUS      IN ( @pStatusRedeemed, @pStatusExpired, @pStatusCancelled, @pStatusCancelledByPlayer)");
          _sb.AppendLine("    AND   ACP_CREDIT_TYPE IN (@pNR1, @pNR2)");
          _sb.AppendLine("    AND   ACP_ACTIVATION  IS NOT NULL ");
          _sb.AppendLine("    AND   ACP_PLAYED      > 0 ");
          _sb.AppendLine("  ORDER   BY ACP_ACTIVATION DESC ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

            _sql_cmd.Parameters.Add("@pNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
            _sql_cmd.Parameters.Add("@pNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
            _sql_cmd.Parameters.Add("@pStatusExpired", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.EXPIRED;
            _sql_cmd.Parameters.Add("@pStatusCancelled", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.CANCELLED;
            _sql_cmd.Parameters.Add("@pStatusCancelledByPlayer", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.CANCELLED_BY_PLAYER;
            _sql_cmd.Parameters.Add("@pStatusRedeemed", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.REDEEMED;

            using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
            {
              if (!_sql_rd.Read())
              {
                // there are not promotions! 
                return true;
              }
              _acp_unique_id = _sql_rd.GetInt64(0);
            }
          }
        }

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_REDEEMABLE_COST     = ISNULL(ACP_REDEEMABLE_COST, 0) + @pRedeembleCost ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID           = @pUniqueId ");
        _sb.AppendLine("    AND   ACP_ACCOUNT_ID          = @pAccountId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _acp_unique_id;
          _sql_cmd.Parameters.Add("@pRedeembleCost", SqlDbType.Money).Value = _redeemable_cost;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          return (_sql_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get Account_promotions according with status.
    //
    //  PARAMS :
    //      - INPUT:
    //            - PromoId
    //            - Status
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Return datatable with account_promotions pending the activated
    public static DataTable GetAccountPromotionsByStatus(Int64 PromoId, ACCOUNT_PROMO_STATUS Status)
    {
      StringBuilder _sb;
      DataTable _account_promotions;

      _account_promotions = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
        _sb.AppendLine("        , ACP_OPERATION_ID    ");
        _sb.AppendLine("        , ACP_ACCOUNT_ID      ");
        _sb.AppendLine("        , ACP_PROMO_ID        ");
        _sb.AppendLine("        , ACP_CREDIT_TYPE     ");
        _sb.AppendLine("        , ACP_BALANCE         ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_status_promo_id)) ");
        _sb.AppendLine("  WHERE   ACP_STATUS = @pStatus ");

        //If promotion id is 0, query is for all promotions
        if (PromoId != 0)
        {
          _sb.AppendLine("    AND   ACP_PROMO_ID = @pPromoId");
        }

        _sb.AppendLine("  ORDER   BY  ACP_ACCOUNT_ID, ACP_PROMO_ID ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

            if (PromoId != 0)
            {
              _sql_cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromoId;
            }

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_account_promotions);

              return _account_promotions;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _account_promotions;
    } // GetAccountPromotionsByStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Activate awarded promotion
    //
    //  PARAMS :
    //      - INPUT:
    //            - PromoId: If its value is 0, all promotions will be activated
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True if all indicated promotions have been activated, false otherwise.
    public static Boolean ActivateAwardedPromotion(Int64 PromoId)
    {
      DataTable _awarded_promotions;
      MultiPromos.PromoBalance _total_balance;
      DataTable _dummy_account_promotions;

      _awarded_promotions = AccountPromotion.GetAccountPromotionsByStatus(PromoId, ACCOUNT_PROMO_STATUS.AWARDED);

      return (Trx_PromotionApplySystemAction(Trx_ActivatePromotion, _awarded_promotions, null, out _total_balance, out _dummy_account_promotions) == _awarded_promotions.Rows.Count);

    } // ActivateAwardedPromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Accounts promotion expiration management
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True if all indicated promotions have been expired, false otherwise.
    public static Boolean AccountsPromotionExpiration()
    {
      StringBuilder _sb;
      DataTable _account_promos;
      MultiPromos.PromoBalance _total_balance;
      DataTable _dummy_account_promotions;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   ACP_UNIQUE_ID                   ");
        _sb.AppendLine("       , ACP_ACCOUNT_ID                  ");
        _sb.AppendLine("       , ACP_BALANCE                     ");
        _sb.AppendLine("  FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_status_promo_id)) ");
        _sb.AppendLine(" WHERE   ACP_STATUS           = @pStatusActive ");
        // Don't filter the promotions that are in session. We ensure that the account is not in session later.
        //_sb.AppendLine("   AND   ACP_PLAY_SESSION_ID IS NULL     ");
        _sb.AppendLine("   AND   ACP_EXPIRATION      <= GETDATE()");
        _sb.AppendLine("   AND   ACP_BALANCE          > 0        ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _account_promos = new DataTable();
              _db_trx.Fill(_sql_da, _account_promos);
            }
          }
        }

        return (Trx_PromotionApplySystemAction(Trx_ExpirePromotion, _account_promos, null, out _total_balance, out _dummy_account_promotions) == _account_promos.Rows.Count);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // AccountsPromotionExpiration

    //------------------------------------------------------------------------------
    // PURPOSE : Promotions expiration or cancel management
    //
    //  PARAMS :
    //      - INPUT :
    //          - Details
    //          - AccountPromotions Datatable must contain at least account_id and acp_unique_id
    //  
    //      - OUTPUT :
    //          - AccountPromotionsUpdated 
    // RETURNS :
    //      - The number of cancelled promotions
    //
    public static Int32 AccountsPromotionCancelation(String Details,
                                                     DataTable AccountPromotions,
                                                     out DataTable AccountPromotionsUpdated)
    {
      MultiPromos.PromoBalance _total_balance;

      return Trx_PromotionApplySystemAction(Trx_CancelPromotion, AccountPromotions, Details, out _total_balance, out AccountPromotionsUpdated);

    } // AccountsPromotionCancelation

    //------------------------------------------------------------------------------
    // PURPOSE : Change the status from promotions
    //
    //  PARAMS :
    //      - INPUT :    
    //          - Details
    //          - Promotions must contain at least account_id and acp_unique_id    
    //          - NewStatus
    //
    //      - OUTPUT :
    //          - NumChanged
    //          - AccountPromotionsUpdated
    //          
    // RETURNS :
    //      - The number of changed promotions
    //  
    public static Int32 ChangeStatusPromotions(String Details,
                                               DataTable AccountPromotions,
                                               ACCOUNT_PROMO_STATUS NewStatus,
                                               out DataTable AccountPromotionsUpdated)
    {
      StringBuilder _sb;
      StringBuilder _sb_error;
      Int32 _num_changed;
      Decimal _promo_balance;

      _num_changed = 0;
      AccountPromotionsUpdated = CreateAccountPromotionTable();

      try
      {
        _sb_error = new StringBuilder();

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS");
        _sb.AppendLine("    SET   ACP_STATUS    = @pNewStatus");
        _sb.AppendLine("        , ACP_DETAILS   = (CASE WHEN ACP_DETAILS IS NULL THEN '' ELSE ACP_DETAILS + ' ' END) + @pDetails ");
        _sb.AppendLine(" OUTPUT   INSERTED.ACP_BALANCE  ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID = @pUniqueId ");
        _sb.AppendLine("    AND   ACP_STATUS    = @pOldStatus");

        foreach (DataRow _promo in AccountPromotions.Rows)
        {
          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
              {
                _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = NewStatus;
                _cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = _promo[SQL_COLUMN_ACP_STATUS];
                _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _promo[SQL_COLUMN_ACP_UNIQUE_ID];
                _cmd.Parameters.Add("@pDetails", SqlDbType.NVarChar, 100).Value = Details;

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                  if (!_reader.Read())
                  {
                    _sb_error.Length = 0;
                    _sb_error.AppendFormat("Promotion not changed status - Detected on ChangeStatusPromotions ");
                    _sb_error.AppendLine();
                    _sb_error.AppendFormat(" - AccountId: {0}, ", _promo[SQL_COLUMN_ACP_ACCOUNT_ID]);
                    _sb_error.AppendLine();
                    _sb_error.AppendFormat(" - PromotionUniqueId: {0} ", _promo[SQL_COLUMN_ACP_UNIQUE_ID]);
                    _sb_error.AppendLine();

                    Log.Warning(_sb_error.ToString());

                    continue;
                  }

                  _promo_balance = _reader.GetDecimal(0);
                } //SqlDataReader
              } //SqlCommand

              _db_trx.Commit();

              _promo[SQL_COLUMN_ACP_BALANCE] = _promo_balance;
              AccountPromotionsUpdated.Rows.Add(_promo.ItemArray);
              _num_changed++;
            }// DB_TRX
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }// foreach
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _num_changed;
    } // ChangeStatusPromotions

    //------------------------------------------------------------------------------
    // PURPOSE : Perform an action to a promotion: Expiration, Cancellation, Cancellation by Player, etc.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Action
    //          - AccountPromotion Datarow must contain at least account_id and acp_unique_id
    //          - AccountMovements
    //          - CashierMovements
    //          - Details
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - PromoBalance
    //
    // RETURNS :
    //  
    //   NOTES : True if ok, false otherwise
    private static Boolean Trx_PerformActionToPromotion(ActionToPerform Action,
                                                        DataRow AccountPromotion,
                                                        AccountMovementsTable AccountMovements,
                                                        CashierMovementsTable CashierMovements,
                                                        String Details,
                                                        out MultiPromos.PromoBalance PromoBalance,
                                                        SqlTransaction SqlTrx)
    {
      return Trx_PerformActionToPromotion(Action, AccountPromotion, AccountMovements, CashierMovements, Details, 0, out PromoBalance, SqlTrx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Perform an action to a promotion: Expiration, Cancellation, Cancellation by Player, etc.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Action
    //          - AccountPromotion Datarow must contain at least account_id and acp_unique_id
    //          - AccountMovements
    //          - CashierMovements
    //          - Details
    //          - OperationId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - PromoBalance
    //
    // RETURNS :
    //  
    //   NOTES : True if ok, false otherwise
    private static Boolean Trx_PerformActionToPromotion(ActionToPerform Action,
                                                        DataRow AccountPromotion,
                                                        AccountMovementsTable AccountMovements,
                                                        CashierMovementsTable CashierMovements,
                                                        String Details,
                                                        Int64 OperationId,
                                                        out MultiPromos.PromoBalance PromoBalance,
                                                        SqlTransaction SqlTrx)
    {
      Decimal _previous_promo_balance;
      Decimal _promo_balance;
      Int64 _account_id;
      Int64 _unique_id;
      Int64 _play_session_id;
      MovementType _movement_type_account;
      CASHIER_MOVEMENT _movement_type_cashier;
      ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
      StringBuilder _sb;
      ACCOUNT_PROMO_STATUS _old_status;
      MultiPromos.AccountBalance _ini_bal;
      MultiPromos.AccountBalance _fin_bal;
      Decimal _ini_points;
      Decimal _fin_points;
      Decimal _ini_balance;
      Decimal _fin_balance;
      Boolean _is_prize_coupon;
      Decimal _sub_amount;
      Decimal _add_amount;
      String _promo_name;
      Promotion.PROMOTION_TYPE _promo_type;
      PrizeType _prize_type;

      PromoBalance = MultiPromos.PromoBalance.Zero;
      _promo_type = Promotion.PROMOTION_TYPE.UNKNOWN;
      _prize_type = PrizeType.None;

      _sb = new StringBuilder();

      try
      {
        //  1 - Lock Account.      
        _account_id = (Int64)AccountPromotion["ACP_ACCOUNT_ID"];
        _unique_id = (Int64)AccountPromotion["ACP_UNIQUE_ID"];

        if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, MultiPromos.AccountBalance.Zero, out _ini_bal, out _ini_points, out _play_session_id, SqlTrx))
        {
          return false;
        }

        // If there current_play_session, nothing to perfom.
        if (_play_session_id != 0)
        {
          return false;
        }

        // Ensure that the account that is not in session, have all its promotions not in session.
        if (!MultiPromos.Trx_UnlinkPromotionsInSession(_account_id, 0, 0, SqlTrx))
        {
          return false;
        }

        //  2 - Perform action to the promotions
        // QMP 10-APR-2013: Prize Coupon Promotion is not inserted into ACCOUNT_PROMOTIONS
        _is_prize_coupon = false;
        if (AccountPromotion.Table.Columns.Contains("ACP_PROMO_TYPE") && !AccountPromotion.IsNull("ACP_PROMO_TYPE"))
        {
          _promo_type = (Promotion.PROMOTION_TYPE)AccountPromotion["ACP_PROMO_TYPE"];
          _is_prize_coupon = (_promo_type == Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON);
        }

        if (_is_prize_coupon)
        {
          _promo_balance = (Decimal)AccountPromotion["ACP_BALANCE"];
          _promo_balance = Math.Max(0, _promo_balance);
          _credit_type = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE;
          _old_status = ACCOUNT_PROMO_STATUS.REDEEMED;
        }
        else
        {
          try
          {
            using (SqlCommand _sql_cmd = new SqlCommand("", SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
              _sql_cmd.Parameters.Add("@pStatusExpired", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.EXPIRED;
              _sql_cmd.Parameters.Add("@pStatusAwarded", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.AWARDED;
              _sql_cmd.Parameters.Add("@pStatusRedeemed", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.REDEEMED;
              _sql_cmd.Parameters.Add("@pStatusActiveCanceled", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.CANCELLED;
              _sql_cmd.Parameters.Add("@pStatusAwardedCanceled", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.CANCELLED_NOT_ADDED;
              _sql_cmd.Parameters.Add("@pStatusCancelledByPlayer", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.CANCELLED_BY_PLAYER;
              _sql_cmd.Parameters.Add("@pStatusPreassigned", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.PREASSIGNED;
              _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _unique_id;

              switch (Action)
              {
                case ActionToPerform.Expire:
                  _sb.Length = 0;
                  _sb.AppendLine("  UPDATE   ACCOUNT_PROMOTIONS           ");
                  _sb.AppendLine("     SET   ACP_STATUS     = @pStatusExpired ");
                  _sb.AppendLine("         , ACP_UPDATED    = GETDATE()   ");
                  _sb.AppendLine("  OUTPUT   INSERTED.ACP_BALANCE         "); // 0
                  _sb.AppendLine("         , INSERTED.ACP_CREDIT_TYPE     "); // 1
                  _sb.AppendLine("         , DELETED.ACP_STATUS           "); // 2
                  _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
                  _sb.AppendLine("   WHERE   ACP_UNIQUE_ID  = @pUniqueId  ");
                  _sb.AppendLine("     AND   ACP_STATUS     = @pStatusActive ");
                  _sb.AppendLine("     AND   ACP_PLAY_SESSION_ID IS NULL  ");
                  break;

                case ActionToPerform.Cancel:
                  _sb.Length = 0;
                  _sb.AppendLine("  UPDATE   ACCOUNT_PROMOTIONS           ");
                  _sb.AppendLine("     SET   ACP_STATUS     = (CASE WHEN ACP_STATUS = @pStatusActive ");
                  _sb.AppendLine("                                  THEN @pStatusActiveCanceled ");
                  _sb.AppendLine("                                  ELSE (CASE WHEN ACP_STATUS IN (@pStatusAwarded, @pStatusPreassigned) ");
                  _sb.AppendLine("                                             THEN @pStatusAwardedCanceled ");
                  _sb.AppendLine("                                        END)  ");
                  _sb.AppendLine("                             END)             ");
                  _sb.AppendLine("         , ACP_UPDATED    = GETDATE()         ");

                  if (!String.IsNullOrEmpty(Details))
                  {
                    _sb.AppendLine("         , ACP_DETAILS    = (CASE WHEN ACP_DETAILS IS NULL THEN '' ELSE ACP_DETAILS + ' ' END) + @pDetails ");

                    _sql_cmd.Parameters.Add("@pDetails", SqlDbType.NVarChar, 100).Value = Details;
                  }

                  _sb.AppendLine("  OUTPUT   INSERTED.ACP_BALANCE         "); // 0
                  _sb.AppendLine("         , INSERTED.ACP_CREDIT_TYPE     "); // 1
                  _sb.AppendLine("         , DELETED.ACP_STATUS           "); // 2
                  _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
                  _sb.AppendLine("   WHERE   ACP_UNIQUE_ID  = @pUniqueId  ");
                  _sb.AppendLine("     AND   ACP_STATUS     IN (@pStatusAwarded, @pStatusActive, @pStatusPreassigned) ");
                  _sb.AppendLine("     AND   ACP_PLAY_SESSION_ID IS NULL  ");
                  break;

                case ActionToPerform.CancelByCashier:
                case ActionToPerform.UndoOperation:
                  _sb.Length = 0;
                  _sb.AppendLine("  UPDATE   ACCOUNT_PROMOTIONS           ");
                  _sb.AppendLine("     SET   ACP_STATUS     = (CASE WHEN ACP_STATUS = @pStatusActive ");
                  _sb.AppendLine("                                  THEN @pStatusActiveCanceled ");
                  _sb.AppendLine("                                  ELSE (CASE WHEN ACP_STATUS IN (@pStatusAwarded, @pStatusPreassigned, @pStatusRedeemed ) ");
                  _sb.AppendLine("                                             THEN @pStatusAwardedCanceled");
                  
                  if (TITO.Utils.IsTitoMode())
                  {
                    _sb.AppendLine("                                           ELSE (CASE WHEN ACP_STATUS IN (@pStatusCancelledByPlayer) ");
                    _sb.AppendLine("                                                      THEN @pStatusActiveCanceled ");
                    _sb.AppendLine("                                                 END) ");
                  }

                  _sb.AppendLine("                                        END) ");
                  _sb.AppendLine("                             END) ");
                  _sb.AppendLine("         , ACP_UPDATED    = GETDATE()   ");

                  if (!String.IsNullOrEmpty(Details))
                  {
                    _sb.AppendLine("         , ACP_DETAILS    = (CASE WHEN ACP_DETAILS IS NULL THEN '' ELSE ACP_DETAILS + ' ' END) + @pDetails ");

                    _sql_cmd.Parameters.Add("@pDetails", SqlDbType.NVarChar, 100).Value = Details;
                  }

                  _sb.AppendLine("  OUTPUT   INSERTED.ACP_BALANCE         "); // 0
                  _sb.AppendLine("         , INSERTED.ACP_CREDIT_TYPE     "); // 1
                  _sb.AppendLine("         , DELETED.ACP_STATUS           "); // 2

                  if (Action == ActionToPerform.UndoOperation)
                  {
                    _sb.AppendLine("         , ISNULL (DELETED.ACP_PRIZE_TYPE,  0) ");   // 3
                  }

                  _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
                  _sb.AppendLine("   WHERE   ACP_UNIQUE_ID  = @pUniqueId  ");
                  // _sb.AppendLine("     AND   ACP_STATUS     IN (@pStatusAwarded, @pStatusActive, @pStatusPreassigned) ");
                  _sb.AppendLine("     AND   ACP_PLAY_SESSION_ID IS NULL  ");
                  break;

                case ActionToPerform.CancelByPlayer:
                  _sb.Length = 0;
                  _sb.AppendLine("  UPDATE   ACCOUNT_PROMOTIONS           ");
                  _sb.AppendLine("     SET   ACP_STATUS     = @pStatusCancelledByPlayer ");
                  _sb.AppendLine("         , ACP_UPDATED    = GETDATE()   ");
                  _sb.AppendLine("  OUTPUT   INSERTED.ACP_BALANCE         "); // 0
                  _sb.AppendLine("         , INSERTED.ACP_CREDIT_TYPE     "); // 1
                  _sb.AppendLine("         , DELETED.ACP_STATUS           "); // 2
                  _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
                  _sb.AppendLine("   WHERE   ACP_UNIQUE_ID  = @pUniqueId  ");

                  if (Misc.IsWassMode() || WSI.Common.TITO.Utils.IsTitoMode())
                  {
                    _sb.AppendLine("     AND   ACP_STATUS     IN (@pStatusAwarded, @pStatusActive, @pStatusRedeemed, @pStatusPreassigned) ");
                  }
                  else
                  {
                    _sb.AppendLine("     AND   ACP_STATUS     IN (@pStatusAwarded, @pStatusActive, @pStatusRedeemed) ");
                  }

                  _sb.AppendLine("     AND   ACP_PLAY_SESSION_ID IS NULL  ");
                  break;

                default:
                  _sb.Length = 0;
                  _sb.AppendFormat("Unexpected action: {0}", Action.ToString());
                  _sb.AppendLine();
                  _sb.AppendFormat(" - AccountId: {0}, Balance: {1}", _account_id.ToString(), _ini_bal.ToString());
                  _sb.AppendLine();
                  _sb.AppendFormat(" - PromotionUniqueId: {0} ", _unique_id.ToString());
                  _sb.AppendLine();

                  Log.Error(_sb.ToString());

                  return false;
              }

              _sql_cmd.CommandText = _sb.ToString();

              using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
              {
                if (!_reader.Read())
                {
                  _sb.Length = 0;
                  _sb.AppendFormat("Promotion not found (Status/InSession) - Detected on action: {0}", Action.ToString());
                  _sb.AppendLine();
                  _sb.AppendFormat(" - AccountId: {0}, Balance: {1}", _account_id.ToString(), _ini_bal.ToString());
                  _sb.AppendLine();
                  _sb.AppendFormat(" - PromotionUniqueId: {0} ", _unique_id.ToString());
                  _sb.AppendLine();

                  Log.Warning(_sb.ToString());

                  return false;
                }

                _promo_balance = _reader.GetDecimal(0);
                _credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)_reader.GetInt32(1);
                _old_status = (ACCOUNT_PROMO_STATUS)_reader.GetInt32(2);

                if (Action == ActionToPerform.UndoOperation)
                {
                  _prize_type = (PrizeType)_reader.GetInt32(3);
                }

                // updated balance for audit from promotion reports
                AccountPromotion["ACP_BALANCE"] = _promo_balance;

                if (_promo_balance == 0 || _old_status == ACCOUNT_PROMO_STATUS.AWARDED || _old_status == ACCOUNT_PROMO_STATUS.PREASSIGNED)
                {
                  // Nothing to perform.
                  return true;
                }
              }
            }
          }
          catch (SqlException _sql_ex)
          {
            _sb.Length = 0;
            _sb.AppendFormat("Sql Exception - Detected on action: {0}", Action.ToString());
            _sb.AppendLine();
            _sb.AppendFormat(" - AccountId: {0}, Balance: {1}", _account_id.ToString(), _ini_bal.ToString());
            _sb.AppendLine();
            _sb.AppendFormat(" - PromotionUniqueId: {0} ", _unique_id.ToString());
            _sb.AppendLine();
            _sb.AppendFormat(" - SqlException Number: {0}", _sql_ex.Number.ToString());
            _sb.AppendLine();
            _sb.AppendFormat(" - SqlException Message: {0}", _sql_ex.Message);
            _sb.AppendLine();

            Log.Error(_sb.ToString());

            return false;
          }
        }

        _ini_balance = _ini_bal.TotalBalance;

        switch (_credit_type)
        {
          case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
          case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
            PromoBalance.Balance.PromoNotRedeemable = _promo_balance;
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
            PromoBalance.Balance.PromoRedeemable = _promo_balance;
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
            PromoBalance.Points = _promo_balance;
            _ini_balance = _ini_points;
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
          default:
            {
              _sb.Length = 0;
              _sb.AppendFormat("Unknown Credit Type - Detected on action: {0}", Action.ToString());
              _sb.AppendLine();
              _sb.AppendFormat(" - AccountId: {0}, Balance: {1}", _account_id.ToString(), _ini_bal.ToString());
              _sb.AppendLine();
              _sb.AppendFormat(" - PromotionUniqueId: {0}, CreditType: {1},  Balance: {2}", _unique_id.ToString(), _credit_type.ToString(), _promo_balance.ToString());
              _sb.AppendLine();

              Log.Error(_sb.ToString());
            }
            return false;
        }

        _previous_promo_balance = _promo_balance;

        // Check Balance.
        if (PromoBalance.Balance.Redeemable > 0                                         // Redeemable greater than expected -> PromoBalance.Balance can't have any 'Redeemable'
            || PromoBalance.Balance.PromoRedeemable > _ini_bal.PromoRedeemable          // PromoRedeemable greater than expected
            || PromoBalance.Balance.PromoNotRedeemable > _ini_bal.PromoNotRedeemable    // PromoNotRedeemable greater than expected
            || PromoBalance.Points > _ini_points)                                       // Points greater than expected
        {
          _sb.Length = 0;
          _sb.AppendFormat("Account Balance / Promo Balance - Action: {0}", Action.ToString());
          _sb.AppendLine();
          _sb.AppendFormat(" - AccountId: {0}, Balance: {1}", _account_id.ToString(), _ini_bal.ToString());
          _sb.AppendLine();
          _sb.AppendFormat(" - PromotionUniqueId: {0}, CreditType: {1},  Balance: {2}", _unique_id.ToString(), _credit_type.ToString(), PromoBalance.ToString());
          _sb.AppendLine();

          // Update with minimum value
          PromoBalance.Balance.Redeemable = 0;
          PromoBalance.Balance.PromoRedeemable = Math.Min(_ini_bal.PromoRedeemable, PromoBalance.Balance.PromoRedeemable);
          PromoBalance.Balance.PromoNotRedeemable = Math.Min(_ini_bal.PromoNotRedeemable, PromoBalance.Balance.PromoNotRedeemable);
          PromoBalance.Points = Math.Min(_ini_points, PromoBalance.Points);

          _sb.AppendFormat(" - Promotion balance changed to: {0}", PromoBalance.ToString());
          _sb.AppendLine();

          Log.Warning(_sb.ToString());

          // Update _promo_balance with the modified value.
          switch (_credit_type)
          {
            case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
            case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
              _promo_balance = PromoBalance.Balance.PromoNotRedeemable;
              break;

            case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
              _promo_balance = PromoBalance.Balance.PromoRedeemable;
              break;

            case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
              _promo_balance = PromoBalance.Points;
              break;
          }
        }

        if (_previous_promo_balance != _promo_balance)
        {
          if (!_is_prize_coupon)
          {
            // UPDATE ACP_BALANCE with the new _promo_balance.
            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNT_PROMOTIONS ");
            _sb.AppendLine("   SET   ACP_BALANCE   = @pNewBalance ");

            if (Action == ActionToPerform.CancelByPlayer && _promo_balance == 0)
            {
              // RCI & AJQ 05-SEP-2012: Restore old status because no balance has been cancelled.
              _sb.AppendLine("       , ACP_STATUS    = @pOldStatus ");
            }

            _sb.AppendLine(" WHERE   ACP_UNIQUE_ID = @pUniqueId ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _cmd.Parameters.Add("@pNewBalance", SqlDbType.Money).Value = _promo_balance;
              _cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = _old_status;
              _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _unique_id;

              if (_cmd.ExecuteNonQuery() != 1)
              {
                return false;
              }
            }
          }

          // updated balance for audit from promotion reports
          AccountPromotion["ACP_BALANCE"] = _promo_balance;
        }

        if (_promo_balance == 0)
        {
          // The promotion balance now is 0.
          return true;
        }

        //  3 - Update Accounts.
        if (Action == ActionToPerform.UndoOperation && _credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)
        {
          _fin_bal = _ini_bal;
          _fin_points = _ini_points;
        }
        else
        {
          if (!MultiPromos.Trx_UpdateAccountBalance(_account_id,
                                                    MultiPromos.AccountBalance.Negate(PromoBalance.Balance),
                                                    Decimal.Negate(PromoBalance.Points),
                                                    out _fin_bal, out _fin_points, SqlTrx))
          {
            return false;
          }
        }

        _sub_amount = _promo_balance;
        _add_amount = 0;
        _fin_balance = _fin_bal.TotalBalance;

        //  4 - Account and Cashier movements.        
        switch (Action)
        {
          case ActionToPerform.Expire:
            _movement_type_cashier = CASHIER_MOVEMENT.NOT_REDEEMABLE_EXPIRED;
            switch (_credit_type)
            {
              case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                _movement_type_account = MovementType.CreditsNotRedeemable2Expired;
                break;

              case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                _movement_type_account = MovementType.CreditsNotRedeemableExpired;
                break;

              case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
              case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
              case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
              default:
                _sb.Length = 0;
                _sb.AppendFormat("Unexpected Credit Type - Detected on action: {0}", Action.ToString());
                _sb.AppendLine();
                _sb.AppendFormat(" - AccountId: {0}, Balance: {1}", _account_id.ToString(), _ini_bal.ToString());
                _sb.AppendLine();
                _sb.AppendFormat(" - PromotionUniqueId: {0}, CreditType: {1},  Balance: {2}", _unique_id.ToString(), _credit_type.ToString(), PromoBalance.ToString());
                _sb.AppendLine();

                Log.Error(_sb.ToString());

                return false;
            }
            break;

          case ActionToPerform.Cancel:
          case ActionToPerform.CancelByCashier:
            switch (_credit_type)
            {
              case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                _movement_type_cashier = CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT;
                _movement_type_account = MovementType.CancelPromotionPoint;
                _fin_balance = _fin_points;
                break;

              case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                _movement_type_cashier = CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE;
                _movement_type_account = MovementType.CancelPromotionRedeemable;
                break;

              case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
              case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
              case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
              default:
                _movement_type_cashier = CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE;
                _movement_type_account = MovementType.CancelNotRedeemable;
                break;
            }

            break;

          case ActionToPerform.CancelByPlayer:
            // Don't add movements here.
            // Only one total movement will be inserted in the DB_CardCreditRedeem().
            return true;

          case ActionToPerform.UndoOperation:
            // Cashier Movements in negative.
            _promo_balance = -_promo_balance;
            // Account Movements add/sub columns exchanged.
            _sub_amount = 0;
            _add_amount = _promo_balance;

            switch (_credit_type)
            {
              case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                _movement_type_cashier = CASHIER_MOVEMENT.PROMOTION_POINT;
                _movement_type_account = MovementType.PromotionPoint;
                _fin_balance = _fin_points;
                break;

              case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                _movement_type_cashier = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE;
                _movement_type_account = MovementType.PromotionRedeemable;
                break;

              case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
              case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                switch (_prize_type)
                {
                  case PrizeType.None:
                  default:
                    _movement_type_cashier = CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE;
                    break;

                  case PrizeType.InKind1_TaxesFromAccount:
                    _movement_type_cashier = CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS;
                    break;

                  case PrizeType.InKind2_TaxesFromSite:
                    _movement_type_cashier = CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS;
                    break;
                }

                _movement_type_account = MovementType.PromotionNotRedeemable;
                break;

              case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:
                switch (_prize_type)
                {
                  case PrizeType.None:
                  default:
                    _movement_type_cashier = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE;
                    break;

                  case PrizeType.InKind1_TaxesFromAccount:
                    _movement_type_cashier = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS;
                    break;

                  case PrizeType.InKind2_TaxesFromSite:
                    _movement_type_cashier = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS;
                    break;
                }

                _movement_type_account = MovementType.PromotionRedeemable;
                break;

              case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
              default:
                _movement_type_cashier = CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE;
                _movement_type_account = MovementType.PromotionNotRedeemable;
                break;
            }

            break;

          default:
            _sb.Length = 0;
            _sb.AppendFormat("Unexpected action: {0}", Action.ToString());
            _sb.AppendLine();
            _sb.AppendFormat(" - AccountId: {0}, Balance: {1}", _account_id.ToString(), _ini_bal.ToString());
            _sb.AppendLine();
            _sb.AppendFormat(" - PromotionUniqueId: {0} ", _unique_id.ToString());
            _sb.AppendLine();

            Log.Error(_sb.ToString());

            return false;
        }

        _promo_name = String.Empty;

        if (AccountPromotion.Table.Columns.Contains("ACP_PROMO_NAME"))
        {
          _promo_name = (String)AccountPromotion["ACP_PROMO_NAME"];
        }

        if (Action == ActionToPerform.UndoOperation &&
           (_promo_type == Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN || // Non visible --> Not inserted
            _credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)) // Don't insert when it's undo operation with redeemable promotion.
        {
          // Don't insert movements
        }
        else
        {
          if (AccountMovements != null)
          {
            AccountMovements.Add(OperationId, _account_id, _movement_type_account, _ini_balance, _sub_amount, _add_amount, _fin_balance, _promo_name);
          }

          if (CashierMovements != null)
          {
            CashierMovements.Add(OperationId, _movement_type_cashier, _promo_balance, _account_id, "", _promo_name);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_PerformActionToPromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Promotion expiration
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountPromotion Datarow must contain at least account_id and acp_unique_id
    //          - AccountMovements
    //          - CashierMovements
    //          - Details
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - PromoBalance
    //
    // RETURNS :
    //  
    //   NOTES : True if Expire promotions, false otherwise
    public static Boolean Trx_ExpirePromotion(DataRow AccountPromotion,
                                              AccountMovementsTable AccountMovementsTable,
                                              CashierMovementsTable CashierMovementsTable,
                                              String Details,
                                              out MultiPromos.PromoBalance PromoBalance,
                                              SqlTransaction Trx)
    {
      return Trx_PerformActionToPromotion(ActionToPerform.Expire, AccountPromotion, AccountMovementsTable, CashierMovementsTable, Details, out PromoBalance, Trx);
    } // Trx_ExpirePromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Promotion cancellation management
    //
    //  PARAMS :
    //      - INPUT :    
    //          - AccountPromotion Datarow must contain at least account_id and acp_unique_id
    //          - AccountMovements
    //          - CashierMovements
    //          - Details
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - PromoBalance
    //
    // RETURNS :
    //  
    //   NOTES : True if Expire promotions, false otherwise
    public static Boolean Trx_CancelPromotion(DataRow AccountPromotion,
                                              AccountMovementsTable AccountMovementsTable,
                                              CashierMovementsTable CashierMovementsTable,
                                              String Details,
                                              out MultiPromos.PromoBalance PromoBalance,
                                              SqlTransaction Trx)
    {
      return Trx_PerformActionToPromotion(ActionToPerform.Cancel, AccountPromotion, AccountMovementsTable, CashierMovementsTable, Details, out PromoBalance, Trx);
    } // Trx_CancelPromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Promotion Undo management
    //
    //  PARAMS :
    //      - INPUT :    
    //          - AccountPromotion Datarow must contain at least account_id and acp_unique_id
    //          - AccountMovements
    //          - CashierMovements
    //          - Details
    //          - OperationId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - PromoBalance
    //
    // RETURNS :
    //  
    //   NOTES : True if Expire promotions, false otherwise
    public static Boolean Trx_UndoPromotion(DataRow AccountPromotion,
                                            AccountMovementsTable AccountMovementsTable,
                                            CashierMovementsTable CashierMovementsTable,
                                            String Details,
                                            Int64 OperationId,
                                            out MultiPromos.PromoBalance PromoBalance,
                                            SqlTransaction Trx)
    {
      return Trx_PerformActionToPromotion(ActionToPerform.UndoOperation, AccountPromotion, AccountMovementsTable, CashierMovementsTable, Details, OperationId, out PromoBalance, Trx);
    } // Trx_CancelPromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Promotion cancellation by player
    //
    //  PARAMS :
    //      - INPUT :    
    //          - AccountPromotion Datarow must contain at least account_id and acp_unique_id
    //          - AccountMovements
    //          - CashierMovements
    //          - Details
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - PromoBalance
    //
    // RETURNS :
    //  
    //   NOTES : True if Expire promotions, false otherwise
    public static Boolean Trx_CancelByPlayerPromotion(DataRow AccountPromotion,
                                                      AccountMovementsTable AccountMovementsTable,
                                                      CashierMovementsTable CashierMovementsTable,
                                                      String Details,
                                                      out MultiPromos.PromoBalance PromoBalance,
                                                      SqlTransaction Trx)
    {
      return Trx_PerformActionToPromotion(ActionToPerform.CancelByPlayer, AccountPromotion, AccountMovementsTable, CashierMovementsTable, Details, out PromoBalance, Trx);
    } // Trx_CancelByPlayerPromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Promotion cancellation by player
    //
    //  PARAMS :
    //      - INPUT :    
    //          - AccountPromotion Datarow must contain at least account_id and acp_unique_id
    //          - AccountMovements
    //          - CashierMovements
    //          - Details
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - PromoBalance
    //
    // RETURNS :
    //  
    //   NOTES : True if Expire promotions, false otherwise
    public static Boolean Trx_CancelByCashierPromotion(DataRow AccountPromotion,
                                                      AccountMovementsTable AccountMovementsTable,
                                                      CashierMovementsTable CashierMovementsTable,
                                                      String Details,
                                                      out MultiPromos.PromoBalance PromoBalance,
                                                      SqlTransaction Trx)
    {
      return Trx_PerformActionToPromotion(ActionToPerform.CancelByCashier, AccountPromotion, AccountMovementsTable, CashierMovementsTable, Details, out PromoBalance, Trx);
    } // Trx_CancelByPlayerPromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Get the text according of balance and credit type
    //
    //  PARAMS:
    //      - INPUT:
    //        - Balance
    //        - CreditType 
    //
    //      - OUTPUT:    
    //        - None
    // RETURNS:
    //      - Return string with format 
    //   NOTES:
    //
    public static String GetFormatBalance(Decimal Balance, ACCOUNT_PROMO_CREDIT_TYPE CreditType)
    {
      String _str_credit_type;
      String _str_credit;

      switch (CreditType)
      {
        case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
        case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
        case ACCOUNT_PROMO_CREDIT_TYPE.NR3:
          _str_credit_type = "NR";
          _str_credit = ((Currency)Balance).ToString();
          break;

        case ACCOUNT_PROMO_CREDIT_TYPE.UNR1:
        case ACCOUNT_PROMO_CREDIT_TYPE.UNR2:
          _str_credit_type = "UNR";
          _str_credit = ((Currency)Balance).ToString(false);  // Don't show currency symbol
          break;

        case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
          _str_credit_type = "RE";
          _str_credit = ((Currency)Balance).ToString();
          break;

        case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:
          _str_credit_type = "URE";
          _str_credit = ((Currency)Balance).ToString();
          break;

        case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
          _str_credit_type = "PT";
          _str_credit = ((Points)Balance).ToString();
          break;

        case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
        default:
          return "";
      }

      return String.Format("{0} {1}", _str_credit, _str_credit_type);
    }

    private static Boolean GetGiftInstanceId(Int64 AccountPromotionId, out Int64 GiftInstanceId, SqlTransaction Trx)
    {
      Object _obj;
      StringBuilder _sb;

      GiftInstanceId = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   GIN_GIFT_INSTANCE_ID");
        _sb.AppendLine("  FROM   GIFT_INSTANCES      ");
        _sb.AppendLine(" WHERE   GIN_DATA_01 = @pAccountPromotionId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountPromotionId", SqlDbType.BigInt).Value = AccountPromotionId;
          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            GiftInstanceId = (Int64)_obj;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Trx_CancelPromotion(DataTable AccountPromotions,
                                              CardData CardData,
                                              CashierSessionInfo Cashiersession,
                                              out ArrayList Voucher,
                                              out String Message,
                                              SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int64 _operation_id;
      GiftInstance _gift_instance;
      GiftInstance.GIFT_INSTANCE_MSG _msg;
      MultiPromos.PromoBalance _promo_canceled;
      MultiPromos.AccountBalance _ini_balance;
      Object _unique_id;
      Int64 _account_promotion_id;
      Int64 _gift_instance_id;
      Promotion.PROMOTION_TYPE _promotion_type;
      ACCOUNT_PROMO_CREDIT_TYPE _promotion_credit_type;
      TYPE_SPLITS _splits;
      AccountMovementsTable _ac_mov_table;
      CashierMovementsTable _cm_mov_table;
      Int32 _int_value;
      String _error_msg;

      Message = Resource.String("STR_PROMOTION_ERROR_MSG_CANT_CANCEL");
      Voucher = new ArrayList();

      try
      {
        if (AccountPromotions == null || AccountPromotions.Rows.Count < 1)
        {
          return false;
        }

        _promotion_credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)AccountPromotions.Rows[0][AccountPromotion.SQL_COLUMN_ACP_CREDIT_TYPE];
        _account_promotion_id = (Int64)AccountPromotions.Rows[0][AccountPromotion.SQL_COLUMN_ACP_UNIQUE_ID];
        _promotion_type = (Promotion.PROMOTION_TYPE)AccountPromotions.Rows[0][AccountPromotion.SQL_COLUMN_ACP_PROMO_TYPE];

        if (_promotion_type == Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE || _promotion_type == Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE)
        {
          // GIFT PROMOTIONS 
          if (!AccountPromotion.GetGiftInstanceId(_account_promotion_id, out _gift_instance_id, Trx))
          {
            return false;
          }

          _gift_instance = new GiftInstance();
          _gift_instance.DB_ReadInstaceAndGift(_gift_instance_id, Trx);

          if (!GiftInstance.CancelInstance(_gift_instance, Cashiersession, out Voucher, out _msg, Trx))
          {
            System.Windows.Forms.MessageBoxIcon _icon;
            String _caption;

            _gift_instance.GetMessage(_msg, out Message, out _caption, out _icon);

            return false;
          }
        }
        else
        {
          // No gift promotions 
          Int64 _play_session_id;
          Decimal _ini_points;

          // Lock account
          if (!MultiPromos.Trx_UpdateAccountBalance(CardData.AccountId,
                                                    MultiPromos.AccountBalance.Zero,
                                                    out _ini_balance, out _ini_points, out _play_session_id, Trx))
          {
            return false;
          }

          if (!Split.ReadSplitParameters(out _splits))
          {
            return false;
          }

          // Insert Operation before movements. Need to pass OperationId to DB_InsertCardMovement().
          if (!Operations.DB_InsertOperation(OperationCode.PROMOTION, CardData.AccountId, Cashiersession.CashierSessionId,
                                             0,
                                             0, // promo id
                                             0, // currency ammount
                                             0, // currency NR
                                             0, // Operation Data
                                             0,
                                             string.Empty,
                                             out _operation_id,
                                             out _error_msg,
                                             Trx))
          {
            Log.Error("DB_CardCreditRedeem. Error llamando a DB_InsertOperation. CardId: " + CardData.AccountId);

            return false;
          }

          _sb = new StringBuilder();
          _sb.Length = 0;
          _sb.AppendLine("  SELECT   ACP_UNIQUE_ID ");
          _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions))  ");
          _sb.AppendLine("   WHERE   ACP_UNIQUE_ID  = @pUniqueId                            ");
          _sb.AppendLine("     AND   ACP_STATUS     IN (@pStatusActive, @pStatusRedeemed)   ");
          _sb.AppendLine("     AND   ACP_PLAY_SESSION_ID IS NULL                            ");
          _sb.AppendLine("     AND   ACP_PLAYED = 0                                         ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            Object _obj;

            _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _account_promotion_id;
            _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
            _cmd.Parameters.Add("@pStatusRedeemed", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.REDEEMED;

            _obj = _cmd.ExecuteScalar();
            if (_obj == null || _obj == DBNull.Value)
            {
              return false;
            }
            _unique_id = (Int64)_obj;
          }

          _ac_mov_table = new AccountMovementsTable(Cashiersession);
          _cm_mov_table = new CashierMovementsTable(Cashiersession);

          if (!AccountPromotion.Trx_CancelByCashierPromotion(AccountPromotions.Rows[0], _ac_mov_table, _cm_mov_table, null, out _promo_canceled, Trx))
          {
            return false;
          }

          if (!Operations.Trx_SetOperationBalance(_operation_id, _promo_canceled.Balance, Trx))
          {
            return false;
          }

          if (!_ac_mov_table.Save(Trx))
          {
            return false;
          }

          if (!_cm_mov_table.Save(Trx))
          {
            return false;
          }

          if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier.Voucher", "VoucherOnPromotionCancelled"), out _int_value))
          {
            _int_value = 0;
          }

          if (_int_value > 0)
          {
            if (!VoucherBuilder.PromoLost(CardData.VoucherAccountInfo()
                                        , CardData.AccountId
                                        , _splits, (_int_value == 1)
                                        , AccountPromotions
                                        , false
                                        , _operation_id
                                        , PrintMode.Print
                                        , Trx
                                        , out Voucher))
            {
              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }


    public static Boolean SetExpirationLimitPreAsignedPromoOnCreation(DataTable AccountPromotions, DateTime DateFinish)
    {
      try
      {
        if (AccountPromotions != null && AccountPromotions.Rows.Count > 0)
        {
          foreach (DataRow _promo in AccountPromotions.Rows)
          {
            if ((Promotion.PROMOTION_TYPE)_promo[SQL_COLUMN_ACP_PROMO_TYPE] == Promotion.PROMOTION_TYPE.PREASSIGNED)
            {
              _promo[SQL_COLUMN_ACP_EXPIRATION] = DateFinish;
            }
          }
        }
      }
      catch
      {
        return false;
      }

      return true;
    }

    public DateTime GetExpirationDateFromAccountPromotion(Int64 PromotionId, Int64 AccountId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.GetExpirationDateFromAccountPromotion(_db_trx.SqlTransaction, PromotionId, AccountId);
      }
    }

    private DateTime GetExpirationDateFromAccountPromotion(SqlTransaction Trx, Int64 PromotionId, Int64 AccountId)
    {
      StringBuilder _sb;
      DateTime _expiration_date = WGDB.MinDate;
      Object _obj;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT   ISNULL(ACP_EXPIRATION, PM_DATE_FINISH) AS EXPIRATION_DATE                   ");
        _sb.AppendLine("      FROM   ACCOUNT_PROMOTIONS                                                          ");
        _sb.AppendLine(" LEFT JOIN   PROMOTIONS ON ACP_PROMO_ID = PM_PROMOTION_ID                                ");
        _sb.AppendLine("     WHERE   ACP_PROMO_ID = @pPromotionId                                                ");
        _sb.AppendLine("       AND   ACP_ACCOUNT_ID = @pAccountId                                                ");
        _sb.AppendLine("       AND   ACP_STATUS IN (@pStatusPendingPlayer, @pStatusBought, @pStatusPendingDraw)  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPromotionId"          , SqlDbType.BigInt).Value = PromotionId;
          _cmd.Parameters.Add("@pAccountId"            , SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusPendingPlayer"  , SqlDbType.BigInt).Value = ACCOUNT_PROMO_STATUS.PENDING_PLAYER;
          _cmd.Parameters.Add("@pStatusBought"         , SqlDbType.BigInt).Value = ACCOUNT_PROMO_STATUS.BOUGHT;
          _cmd.Parameters.Add("@pStatusPendingDraw"    , SqlDbType.BigInt).Value = ACCOUNT_PROMO_STATUS.PENDING_DRAW;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _expiration_date = (DateTime)_obj;

            return _expiration_date;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return _expiration_date;
    }

    /// <summary>
    /// Link AccountPromotion with TerminalDraw
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="PromotionId"></param>
    /// <param name="OperationId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean RelateAccountPromotionWithTerminalDraw(Int64 AccountId, Int64 PromotionId, Int64 OperationId, SqlTransaction SqlTrx)
    {
      return this.DB_RelateAccountPromotionWithTerminalDraw(AccountId, PromotionId, OperationId, SqlTrx);
    } // RelateAccountPromotionWithTerminalDraw

    /// <summary>
    /// Link AccountPromotion with TerminalDrawRecharge
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="PromotionId"></param>
    /// <param name="OperationId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_RelateAccountPromotionWithTerminalDraw(Int64 AccountId, Int64 PromotionId, Int64 OperationId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_RelatePeriodicPromotionWithTerminalDraw_Query(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPromotionId", SqlDbType.Int).Value         = PromotionId;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value        = AccountId;
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value      = OperationId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.BigInt).Value           = ACCOUNT_PROMO_STATUS.PENDING_PLAYER;
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.BigInt).Value        = ACCOUNT_PROMO_STATUS.PENDING_DRAW;
          _cmd.Parameters.Add("@pStatusPreAsigned", SqlDbType.BigInt).Value = ACCOUNT_PROMO_STATUS.PREASSIGNED;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_RelateAccountPromotionWithTerminalDraw().");
        Log.Exception(_ex);
  }

      return false;
    } // DB_RelateAccountPromotionWithTerminalDraw

    /// <summary>
    /// PeriodicPromotion Query Update (Relation with TerminalDrawRecharge)
    /// </summary>
    /// <returns></returns>
    private String DB_RelatePeriodicPromotionWithTerminalDraw_Query()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      // Gets last Periodic promotion
      _sb.AppendLine("   DECLARE  @pUniqueId AS BIGINT                 ");
      _sb.AppendLine("    SELECT  TOP (1) @pUniqueId = ACP_UNIQUE_ID   ");
      _sb.AppendLine("      FROM  ACCOUNT_PROMOTIONS                   ");
      _sb.AppendLine("     WHERE  ACP_PROMO_TYPE IN (4,5,6,9)          ");
      _sb.AppendLine("       AND  ACP_ACCOUNT_ID = @pAccountId         ");
      _sb.AppendLine("       AND  ACP_PROMO_ID   = @pPromotionId       ");
      _sb.AppendLine("       AND  (ACP_STATUS     = @pStatus           ");
      _sb.AppendLine("        OR  ACP_STATUS     = @pStatusPreAsigned) ");
      _sb.AppendLine("  ORDER BY  ACP_UNIQUE_ID DESC                   ");
                                                                       
      // Update last Periodic promotion                                
      _sb.AppendLine("  UPDATE    ACCOUNT_PROMOTIONS                   ");
      _sb.AppendLine("     SET    ACP_OPERATION_ID  = @pOperationId    ");
      _sb.AppendLine("          , ACP_STATUS        = @pNewStatus      ");
      _sb.AppendLine("   WHERE    ACP_UNIQUE_ID     = @pUniqueId       ");
      
      return _sb.ToString();

    } // DB_RelatePeriodicPromotionWithTerminalDraw_Query

  } // AccountPromotion
}