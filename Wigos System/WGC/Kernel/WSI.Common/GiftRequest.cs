//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GiftRequest.cs
// 
//   DESCRIPTION: GiftRequest class
// 
//        AUTHOR: Susana Sams�
// 
// CREATION DATE: 23-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAY-2012 SSC    First version.
// 13-JUN-2012 SSC    Added new movements for Gifts/Points
// 06-JUL-2012 JAB    Unify the above process in a single function (not redeemable credit control).
// 21-SEP-2012 MPO    Added feature: cancel gift instance.
// 07-FEB-2013 JMM    Account points redeem status checking added.
// 13-FEB-2013 HBB & MPO   Added functionality of one cashier session for each PromoBOX.
// 10-MAY-2013 LEM    Fixed Bug# 780: Unexpected error on gift cancelation
// 10-MAY-2013 JMA    Changed queries and other features to limit the amount of gifts redemptions
// 24-MAY-2013 LEM    New parameters AccountMovementsTable and CashierMovementsTable in GiftInstance.CommonRequest and used CashierSessionId instead of CashierSessionInfo
//                    Modified GiftRequest.ProcessRequestGift to create AccountMovementsTable and CashierMovementsTable with TerminalName.
// 27-MAY-2013 DHA    Fixed Bug #789: Draw names not correctly inserted from PromoBox
// 17-SEP-2013 LEM    Get gift points cost according account level (Modified Gift.DB_GiftData())
// 06-NOV-2013 JPJ & DDM   Fixed Bug# 384:  No allowed to cancel gift 
// 09-DEC-2013 LJM    Fixed Bug #465: For credit gifts, only one deliver is counted on each pack.
// 17-JAN-2014 JBP    Added GIFT_INSTANCE_MSG.PRINTER_PRINT_FAIL
//                    Added GIFT_INSTANCE_MSG.PRINTER_IS_NOT_READY
// 18-FEB-2014 JPJ    Defect WIG-639 Draws can not be given as gift
// 25-MAR-2014 LEM & DRV Fixed Bug WIGOSTITO-1171: The status of a promotion it's not updated for RE and NR credit gifts
//                       Fixed Bug WIGOSTITO-1172: It allows to cancel a gift ticket that has been played
// 19-SEP-2014 SGB & RCI Fixed Bug #WIG-1231 Check if exceeds the allowable limit amount.
// 04-NOV-2014 XCD    Removed condition card in session when LCD request a gift.
// 13-NOV-2014 JMM    Fixed Bug WIG-1675: LCD can't redeem points when cage is not enabled
// 26-FEB-2015 AMF    Fixed Bug WIG-2100: Show terminal data
// 17-JUL-2015 YNM    Fixed Bug WIG-2330: Promobox collection error 
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 11-JAN-2016 SGB    Backlog Item 7910: Change column AC_POINTS to bucket 1.
// 19-FEB-2016 RAB    Product Backlog Item 9740:Cajero: impresi�n del n�mero de c�dula del cliente en los vouchers de sorteos
// 01-MAR-2016 FGB    Product Backlog Item 9781: Multiple Buckets: Refactorizaci�n
// 02-AGO-2016 JRC    PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
// 12-SEP-2016 ETP    PBI 17561: Print Tito Ticket in Promobox Gifts.
// 14-OCT-2016 AVZ    PBI 18701: Pending Gifts.
// 13-JUL-2017 RGR    Fixed Bug 28592:WIGOS-3609 The users that buy and cancel a gift limited to a 1 by gaming day are not able to buy again the same gift
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Collections;
using WSI.Common.TITO;

namespace WSI.Common
{

  public static class GiftRequest
  {
    private static GiftInstance m_gift_instance;

    public static Boolean ProcessRequestGift(Int64 GiftId, Int64 DrawId, Int64 AccountId, Decimal GiftPoints,
                                             Int32 GiftNumUnits, Int32 TerminalId, SqlTransaction SqlTrx,
                                             out Decimal PlayerPoints, out Decimal PlayerBalance, out ArrayList VoucherList,
                                            out WSI.Common.Gift.GIFT_MSG ErrMsg, String TerminalName, TerminalTypes TerminalType, out Ticket TicketTito)
    {
      CardData _card_data;
      Gift _gift_to_request;
      Decimal _points_to_spent;
      String _message;
      String _caption;
      MessageBoxIcon _icon;
      CashierSessionInfo _cashier_session;
      GiftInstance.GIFT_INSTANCE_MSG _msg_gift_instance;
      Gift.GIFT_MSG _msg_gift;
      AccountMovementsTable _ac_mov_table;
      CashierMovementsTable _cm_mov_table;
      GU_USER_TYPE _gu_user_type;

      //Inicializations
      PlayerPoints = 0;
      PlayerBalance = 0;
      VoucherList = new ArrayList();

      TicketTito = null;

      _points_to_spent = 0;
      ErrMsg = Gift.GIFT_MSG.UNKNOWN;
      try
      {
        //Get Card Data
        _card_data = new CardData();
        CardData.DB_CardGetAllData(AccountId, _card_data);

        if (_card_data.PointsStatus != ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED)
        {
          ErrMsg = WSI.Common.Gift.GIFT_MSG.PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS;
          return false;
        }

        //Get gift
        _gift_to_request = new Gift();
        // 18-FEB-2014 JPJ    Defect WIG-639 Draws can not be given as gift
        if (DrawId == 0)
        {
          if (!_gift_to_request.DB_GiftData(GiftId, _card_data.AccountId))
          {
            return false;
          }
        }

        //_gift_to_request.GiftNumUnits = GiftNumUnits;

        if (DrawId != 0)
        {
          _gift_to_request.DB_DrawData(DrawId);
          _gift_to_request.DrawId = DrawId;
          _gift_to_request.Type = GIFT_TYPE.DRAW_NUMBERS;
          _gift_to_request.Points = GiftPoints;
        }
        else
        {
          if (_gift_to_request.Points != GiftPoints)
          {
            ErrMsg = WSI.Common.Gift.GIFT_MSG.NOT_ENOUGH_POINTS;

            return false;
          }
        }

        //Gift instance
        m_gift_instance = new GiftInstance(_gift_to_request, _card_data);
        m_gift_instance.IsCashier = false;

        // Gift request limits
        m_gift_instance.DB_GetNumOfAvailableGifts();

        //Gift Balance
        _points_to_spent = GiftPoints * GiftNumUnits;
        m_gift_instance.GiftBalance(_points_to_spent);

        // Check again that the gift can be delivered (stock issue)
        if (!m_gift_instance.CheckGiftRequest(false, out _msg_gift_instance, out _msg_gift))
        {
          if (_msg_gift_instance != GiftInstance.GIFT_INSTANCE_MSG.UNKNOWN)
          {
            m_gift_instance.GetMessage(_msg_gift_instance, out _message, out _caption, out _icon);
          }
          else if (_msg_gift != Gift.GIFT_MSG.UNKNOWN)
          {
            _gift_to_request.GetMessage(_msg_gift, _card_data, out _message);
          }
          ErrMsg = _msg_gift;
          return false;
        }

        // Open System WKT Cashier Session if it doesn't exist
        switch (TerminalType)
        {
          case TerminalTypes.PROMOBOX:
          case TerminalTypes.WIN_UP:
            {
              _gu_user_type = Misc.GetUserTypePromobox();
            }
            break;
          case TerminalTypes.SAS_HOST:
            {
              _gu_user_type = Misc.IsNoteAcceptorEnabled() ? GU_USER_TYPE.SYS_ACCEPTOR : GU_USER_TYPE.SYS_SYSTEM;
            }
            break;
          default:
            {
              Log.Message(String.Format("ProcessRequestGift: Invalid TerminalType ({0})", TerminalType));

              return false;
            }
        }

        _cashier_session = Cashier.GetSystemCashierSessionInfo(_gu_user_type, SqlTrx, TerminalName);
        _ac_mov_table = new AccountMovementsTable(_cashier_session, _gu_user_type == GU_USER_TYPE.SYS_ACCEPTOR ? TerminalId : 0, TerminalName);
        _cm_mov_table = new CashierMovementsTable(_cashier_session, TerminalName);

        GiftInstance.GIFT_INSTANCE_MSG _err_msg;

        if (!m_gift_instance.CommonRequest(_cashier_session.CashierSessionId, _ac_mov_table, _cm_mov_table, TerminalType, SqlTrx, out VoucherList, out _err_msg, out TicketTito))
        {
          // Process the selected gift request
          m_gift_instance.GetMessage(_err_msg, out _message, out _caption, out _icon);

          return false;
        }

        //PlayerPoints and PlayerBalance
        PlayerPoints = (Decimal)m_gift_instance.RelatedCard.PlayerTracking.CurrentPoints - m_gift_instance.SpentPoints;
        PlayerBalance = (Decimal)m_gift_instance.RelatedCard.CurrentBalance + m_gift_instance.NotRedeemableCredits;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Retrieves the redeem not redeem credit that can request today the witholder
    //
    //  PARAMS :
    //      - INPUT :
    //          - Account ID
    //          - Sql Transaction
    //      - OUTPUT :
    //          - MaxRedeemable     : Returns -1 if there ara no limit to spend
    //          - MaxNotRedeemable  : Returns -1 if there ara no limit to spend
    // RETURNS :
    //          - true: data was retrieved successfully
    //          - false: data was retrieved unsuccessfully
    // 
    //   NOTES : If no limit is defined, then return -1.
    public static Boolean MaxBuyableCredits(CardData Card, out Currency MaxRedeemable, out Currency MaxNotRedeemable, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Currency _reedem_today;
      Currency _non_reedem_today;
      Decimal _gp_reedem_daily_limit;
      Decimal _gp_non_reedem_daily_limit;
      Decimal _gp_non_reedem_max_balance;
      DateTime _today;

      Decimal _global_limit;
      Decimal _daily_limit;

      MaxRedeemable = Decimal.MaxValue;
      MaxNotRedeemable = Decimal.MaxValue;

      _daily_limit = Decimal.MaxValue;
      _global_limit = Decimal.MaxValue;

      try
      {
        // Read the max limit of NRC that a player can have in the account
        if (!Decimal.TryParse(GeneralParam.Value("Gifts.NotRedeemableCredits", "BalanceLimit"), out _gp_non_reedem_max_balance))
        {
          _gp_non_reedem_max_balance = 0;
        }

        // Read the max limit of NRC that a player can earn in a day
        if (!Decimal.TryParse(GeneralParam.Value("Gifts.NotRedeemableCredits", "DailyLimit"), out _gp_non_reedem_daily_limit))
        {
          _gp_non_reedem_daily_limit = 0;
        }

        // Read the max limit of RC that a player can earn in a day
        if (!Decimal.TryParse(GeneralParam.Value("Gifts.RedeemableCredits", "DailyLimit"), out _gp_reedem_daily_limit))
        {
          _gp_reedem_daily_limit = 0;
        }

        _reedem_today = 0;
        _non_reedem_today = 0;

        if (_gp_non_reedem_max_balance > 0 || _gp_non_reedem_daily_limit > 0 || _gp_reedem_daily_limit > 0)
        {
          _today = Misc.TodayOpening();
          _sb = new StringBuilder();

          _sb.Length = 0;
          _sb.AppendLine("  SELECT   GIN_GIFT_TYPE                                               ");
          _sb.AppendLine("     		,  SUM (GIN_SPENT_POINTS * GIN_CONVERSION_TO_NRC / GIN_POINTS) ");
          _sb.AppendLine("    FROM   GIFT_INSTANCES                                              ");
          _sb.AppendLine("   WHERE   GIN_ACCOUNT_ID	    =     @pAccountId                        ");
          _sb.AppendLine("     AND   GIN_REQUESTED	   >=     @pStartTime                        ");
          _sb.AppendLine("     AND   GIN_REQUESTED	    <     @pEndTime                          ");
          _sb.AppendLine("     AND ( GIN_GIFT_TYPE	    =     @pGiftTypeNRC                      ");
          _sb.AppendLine("      OR   GIN_GIFT_TYPE	    =     @pGiftTypeRC   )                   ");
          _sb.AppendLine("  GROUP BY GIN_GIFT_TYPE	  	                                         ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Card.AccountId;
            _sql_cmd.Parameters.Add("@pStartTime", SqlDbType.DateTime).Value = _today;
            _sql_cmd.Parameters.Add("@pEndTime", SqlDbType.DateTime).Value = _today.AddDays(1);
            _sql_cmd.Parameters.Add("@pGiftTypeNRC", SqlDbType.Int).Value = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
            _sql_cmd.Parameters.Add("@pGiftTypeRC", SqlDbType.Int).Value = GIFT_TYPE.REDEEMABLE_CREDIT;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                if (_reader.GetInt32(0) == (Int32)GIFT_TYPE.REDEEMABLE_CREDIT)
                {
                  _reedem_today = _reader.GetDecimal(1);
                }
                else
                {
                  _non_reedem_today = _reader.GetDecimal(1);
                }
              }
            }
          }

          if (_gp_reedem_daily_limit > 0)
          {
            MaxRedeemable = Math.Max(0, _gp_reedem_daily_limit - _reedem_today);
          }

          if (_gp_non_reedem_daily_limit > 0)
          {
            _daily_limit = Math.Max(0, _gp_non_reedem_daily_limit - _non_reedem_today);
          }
          if (_gp_non_reedem_max_balance > 0)
          {
            _global_limit = Math.Max(0, _gp_non_reedem_max_balance - Card.AccountBalance.TotalNotRedeemable);
          }
        }

        MaxNotRedeemable = Math.Min(_daily_limit, _global_limit);

        return true;
      }
      catch
      {
        MaxRedeemable = 0;
        MaxNotRedeemable = 0;
      }

      return false;
    } //MaxBuyableCredits



  } // GiftRequest

  //------------------------------------------------------------------------------
  // CLASS : LoyaltyProgram
  // 
  // NOTES :

  public static class LoyaltyProgram
  {
    private const Int32 MAX_LEVELS = 5;  // 0 - not used, 1 ... MAX_LEVELS-1

    public static String LevelName(Int32 LevelId)
    {
      if (LevelId < 0 || LevelId >= MAX_LEVELS)
      {
        return "* " + LevelId.ToString() + " *";
      }

      if (LevelId == 0)
      {
        return "---";
      }

      return WSI.Common.Misc.ReadGeneralParams("PlayerTracking",
                                               "Level" + LevelId.ToString("00") + ".Name");
    }

  } // Class LoyaltyProgram

  //------------------------------------------------------------------------------
  // CLASS : Gift
  // 
  // NOTES :

  public class Gift
  {
    public enum GIFT_MSG       // Gift messages
    {
      UNKNOWN = 0,
      CARD_ERROR = 1,
      NOT_ENOUGH_POINTS = 2,
      NOT_AVAILABLE = 3,
      NOT_ENOUGH_STOCK = 4,
      CARD_HAS_NON_REDEEMABLE = 5,
      PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS = 6,
      GIFT_REDEEM_LIMIT_EXCEEDED = 7
    };

    public const String FORMAT_POINTS = "#,##0";
    public const String FORMAT_POINTS_DEC = "#,##0.00";

    private Int64 m_gift_id;
    private String m_gift_name;
    private GIFT_TYPE m_type;
    private Decimal m_points;
    private Decimal m_conversion_nrc;

    private Int64 m_draw_id;

    private Int32 m_current_stock;

    private Int32 m_request_counter;
    private Int32 m_delivery_counter;
    private Boolean m_available;
    private Boolean m_gift_data_ok;

    private Int32 m_account_daily_limit;
    private Int32 m_account_monthly_limit;
    private Int32 m_global_daily_limit;
    private Int32 m_global_monthly_limit;
    private Int64 m_promotion_id;

    public Gift()
    {
      ClearGift();
    }

    public Gift(DataRow Row)
    {
      ClearGift();

      m_gift_id = (Int64)Row["GI_GIFT_ID"];
      m_gift_name = (String)Row["GI_NAME"];
      m_type = (GIFT_TYPE)Row["GI_TYPE"];
      m_points = (Decimal)Row["GI_POINTS"];
      m_conversion_nrc = (Decimal)Row["GI_NON_REDEEMABLE"];
      m_draw_id = (Int64)Row["GI_DRAW_ID"];

      m_current_stock = 1;
      m_request_counter = 0;
      m_delivery_counter = 0;
      m_available = true;

      m_account_daily_limit = (Int32)Row["GI_ACCOUNT_DAILY_LIMIT"];
      m_account_monthly_limit = (Int32)Row["GI_ACCOUNT_MONTHLY_LIMIT"];
      m_global_daily_limit = (Int32)Row["GI_GLOBAL_DAILY_LIMIT"];
      m_global_monthly_limit = (Int32)Row["GI_GLOBAL_MONTHLY_LIMIT"];

      m_gift_data_ok = true;
    }

    public Int64 GiftId
    {
      get { return m_gift_id; }
    }

    public Int64 DrawId
    {
      get { return m_draw_id; }
      set { m_draw_id = value; }
    }

    public String GiftName
    {
      get { return m_gift_name; }
      set { m_gift_name = value; }
    }

    public Decimal Points
    {
      get { return m_points; }
      set { m_points = value; }
    }

    public Decimal ConversionNRC
    {
      get { return m_conversion_nrc; }
      set { m_conversion_nrc = value; }
    }

    public Boolean Available
    {
      get { return m_available; }
      set { m_available = value; }
    }

    public Int32 Stock
    {
      get { return m_current_stock; }
      set { m_current_stock = value; }
    }

    //public Int32 GiftNumUnits
    //{
    //  get { return m_gift_num_units; }
    //  set { m_gift_num_units = value; }
    //}

    public Int32 RequestCounter
    {
      get { return m_request_counter; }
      set { m_request_counter = value; }
    }

    public Int32 DeliveryCounter
    {
      get { return m_delivery_counter; }
      set { m_delivery_counter = value; }
    }

    public Boolean Ok
    {
      get { return m_gift_data_ok; }
      set { m_gift_data_ok = value; }
    }

    public GIFT_TYPE Type
    {
      get { return m_type; }
      set { m_type = value; }
    }

    public Int32 AccountDailyLimit
    {
      get { return m_account_daily_limit; }
      set { m_account_daily_limit = value; }
    }

    public Int32 AccountMonthlyLimit
    {
      get { return m_account_monthly_limit; }
      set { m_account_monthly_limit = value; }
    }

    public Int32 GlobalDailyLimit
    {
      get { return m_global_daily_limit; }
      set { m_global_daily_limit = value; }
    }

    public Int32 GlobalMonthlyLimit
    {
      get { return m_global_monthly_limit; }
      set { m_global_monthly_limit = value; }
    }

    public Int64 PromotionId
    {
      get { return m_promotion_id; }
      set { m_promotion_id = value; }
    }

    public String TypeName
    {
      get
      {
        switch (m_type)
        {
          case GIFT_TYPE.OBJECT:
            return Resource.String("STR_FRM_ACCOUNT_POINTS_009");

          case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
            return Resource.String("STR_FRM_ACCOUNT_POINTS_010");

          case GIFT_TYPE.REDEEMABLE_CREDIT:
            return Resource.String("STR_FRM_ACCOUNT_POINTS_022");

          case GIFT_TYPE.DRAW_NUMBERS:
            return Resource.String("STR_FRM_ACCOUNT_POINTS_015");

          case GIFT_TYPE.SERVICES:
            return Resource.String("STR_FRM_ACCOUNT_POINTS_021");

          default:
            return "";
        }
      }
    }

    private void ClearGift()
    {
      m_gift_id = 0;
      m_gift_name = "";
      Type = GIFT_TYPE.UNKNOWN;
      m_points = 0;
      m_conversion_nrc = 0;
      m_current_stock = 0;
      m_request_counter = 0;
      m_delivery_counter = 0;
      m_available = false;
      m_gift_data_ok = false;
      m_account_daily_limit = 0;
      m_account_monthly_limit = 0;
      m_global_daily_limit = 0;
      m_global_monthly_limit = 0;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Retrieves the selected gift data from GIFTS table
    //
    //  PARAMS :
    //      - INPUT :
    //          - GiftId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES :

    public Boolean DB_GiftData(Int64 GiftId, Int64 AccountId)
    {
      String _sql_txt;

      try
      {
        _sql_txt = "SELECT   GI_NAME " +
                          " ,( " +
                             " CASE ( " +
                                   " SELECT   AC_HOLDER_LEVEL " +
                                     " FROM   ACCOUNTS " +
                                    " WHERE   AC_ACCOUNT_ID = " + AccountId +
                                  " ) " +
                              " WHEN 1 THEN GI_POINTS_LEVEL1 " +
                              " WHEN 2 THEN GI_POINTS_LEVEL2 " +
                              " WHEN 3 THEN GI_POINTS_LEVEL3 " +
                              " WHEN 4 THEN GI_POINTS_LEVEL4 " +
                              " ELSE 0 " +
                              " END " +
                           " ) AS GI_POINTS " +
                   "       , ISNULL (GI_CONVERSION_TO_NRC, 0.0)" +
                   "       , GI_AVAILABLE " +
                   "       , GI_TYPE " +
                   "       , GI_CURRENT_STOCK " +
                   "       , GI_REQUEST_COUNTER " +
                   "       , GI_DELIVERY_COUNTER" +
                   "       , ISNULL (GI_ACCOUNT_DAILY_LIMIT   , 0) GI_ACCOUNT_DAILY_LIMIT   " +
                   "       , ISNULL (GI_ACCOUNT_MONTHLY_LIMIT , 0) GI_ACCOUNT_MONTHLY_LIMIT " +
                   "       , ISNULL (GI_GLOBAL_DAILY_LIMIT    , 0) GI_GLOBAL_DAILY_LIMIT    " +
                   "       , ISNULL (GI_GLOBAL_MONTHLY_LIMIT  , 0) GI_GLOBAL_MONTHLY_LIMIT  " +
                   "       , ISNULL ((SELECT PM_PROMOTION_ID FROM PROMOTIONS WHERE PM_POINTS_TO_CREDITS_ID = GI_POINTS_TO_CREDITS_ID ), 0) as PROMOTION_ID" +
                   "  FROM   GIFTS " +
                   " WHERE   GI_GIFT_ID = @GiftId";

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@GiftId", SqlDbType.BigInt).Value = GiftId;

            using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
            {
              if (!_sql_reader.Read())
              {
                return false;
              }

              m_gift_id = GiftId;
              m_gift_name = (String)_sql_reader.GetSqlString(0);
              m_points = (Decimal)_sql_reader.GetSqlMoney(1);
              m_conversion_nrc = (Decimal)_sql_reader.GetSqlMoney(2);
              m_available = (Boolean)_sql_reader.GetSqlBoolean(3);
              Type = (GIFT_TYPE)_sql_reader[4];
              m_current_stock = (Int32)_sql_reader.GetSqlInt32(5);
              m_request_counter = (Int32)_sql_reader.GetSqlInt32(6);
              m_delivery_counter = (Int32)_sql_reader.GetSqlInt32(7);

              m_account_daily_limit = (Int32)_sql_reader.GetSqlInt32(8);
              m_account_monthly_limit = (Int32)_sql_reader.GetSqlInt32(9);
              m_global_daily_limit = (Int32)_sql_reader.GetSqlInt32(10);
              m_global_monthly_limit = (Int32)_sql_reader.GetSqlInt32(11);
              m_promotion_id = (Int64)_sql_reader.GetSqlInt64(12);
            }

            if (m_type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT || m_type == GIFT_TYPE.REDEEMABLE_CREDIT)
            {
              if (m_promotion_id == 0)
              {
                Log.Error("GiftRequest.DB_GiftData. Gift: " + m_gift_id.ToString() + "-" + m_gift_name);

                return false;
              }
            }

            m_gift_data_ok = true;

            return true;
          }
        }
      } // try

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_GiftData

    //------------------------------------------------------------------------------
    // PURPOSE : Retrieves the selected draw data from DRAWS table
    //
    //  PARAMS :
    //      - INPUT :
    //          - DrawId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES :

    public Boolean DB_DrawData(Int64 DrawId)
    {
      String _sql_txt;

      try
      {
        _sql_txt = "SELECT   '" + Resource.String("STR_RAFFLE_TITLE") + "' + DR_NAME  As DR_NAME" +
                     "  FROM   DRAWS " +
                     " WHERE   DR_ID = @DrawId";

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@DrawId", SqlDbType.BigInt).Value = DrawId;

            using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
            {
              if (!_sql_reader.Read())
              {
                return false;
              }
              m_gift_name = (String)_sql_reader.GetSqlString(0);

              m_gift_data_ok = true;

              return true;
            }
          }
        }
      } // try

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_DrawData

    //------------------------------------------------------------------------------
    // PURPOSE : Function to manage all the messages related to Gifts' basic operations
    //
    //  PARAMS :
    //      - INPUT :
    //          - MsgType
    //          - Card
    //
    //      - OUTPUT :
    //          - Message
    //
    // RETURNS :
    // 
    //   NOTES :

    public bool GetMessage(GIFT_MSG MsgType, CardData Card, out String Message)
    {
      string[] _message_params = { "", "", "", "", "", "" };
      bool _result;
      string _card_name = "";

      _result = true;
      Message = "";

      if (Card != null)
      {
        if (Card.PlayerTracking.HolderName != "")
        {
          _card_name = Card.AccountId.ToString() + " - " + Card.PlayerTracking.HolderName;
        }
        else
        {
          _card_name = Card.AccountId.ToString();
        }
      }

      switch (MsgType)
      {
        case GIFT_MSG.CARD_ERROR:
        case GIFT_MSG.NOT_ENOUGH_POINTS:
        case GIFT_MSG.NOT_AVAILABLE:
        case GIFT_MSG.NOT_ENOUGH_STOCK:
        case GIFT_MSG.CARD_HAS_NON_REDEEMABLE:
        case GIFT_MSG.PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS:
        case GIFT_MSG.GIFT_REDEEM_LIMIT_EXCEEDED:
          _message_params[0] = _card_name;
          _message_params[1] = GiftName;

          switch (MsgType)
          {
            case GIFT_MSG.NOT_ENOUGH_POINTS:
              Message = Resource.String("STR_FRM_ACCOUNT_POINTS_012", _message_params);
              break;

            case GIFT_MSG.NOT_AVAILABLE:
              Message = Resource.String("STR_FRM_ACCOUNT_POINTS_013", _message_params);
              break;

            case GIFT_MSG.NOT_ENOUGH_STOCK:
              // "No se puede realizar la petici�n del regalo @p1 a cargo de los puntos la cuenta @p0:\r\n\r\n    - No hay existencias en stock.\r\n\r\n"
              Message = Resource.String("STR_FRM_ACCOUNT_POINTS_018", _message_params);
              break;

            case GIFT_MSG.CARD_HAS_NON_REDEEMABLE:
              if (this.m_type == GIFT_TYPE.REDEEMABLE_CREDIT)
              {
                Message = Resource.String("STR_FRM_ACCOUNT_POINTS_023", _message_params);
              }
              else
              {
                Message = Resource.String("STR_FRM_ACCOUNT_POINTS_019", _message_params);
              }
              break;
            case GIFT_MSG.PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS:
              // "Su cuenta no puede redimir puntos."
              Message = Resource.String("STR_FRM_ACCOUNT_POINTS_030", _message_params);
              break;
            case GIFT_MSG.GIFT_REDEEM_LIMIT_EXCEEDED:
              Message = Resource.String("STR_FRM_ACCOUNT_POINTS_031", _message_params);
              break;
            case GIFT_MSG.CARD_ERROR:
            default:
              Message = Resource.String("STR_FRM_ACCOUNT_POINTS_011", _message_params);
              break;
          }

          Message = Message.Replace("\\r\\n", "\r\n");

          break;

        default:
          _result = false;
          break;
      }

      return _result;
    } // GiftMsg

    //------------------------------------------------------------------------------
    // PURPOSE : Checks whether the gift can be requested in charge to the account 
    //           represented by the received card.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CardToCheck
    //          - IsCashier
    //
    //      - OUTPUT :
    //          - GiftMsg
    //
    // RETURNS :
    // 
    //   NOTES :
    public Boolean CheckGiftRequest(CardData CardToCheck, Boolean IsCashier, out GIFT_MSG GiftMsg)
    {
      //TYPE_BALANCE_PARTS_OUTPUT _balance_parts;

      // Requisites
      //    - Card must not be in use
      //    - NOT_REDEEMABLE_CREDIT gift : Card must not have non-redeemable balance 
      //    - REDEEMABLE_CREDIT gift : Card must not have non-redeemable balance 
      //    - Related account must have enough points to request the gift
      //    - Gift must be available
      //    - OBJECT gift : Gift must be in stock 
      //    - DRAW NUMBERS gift : 
      //    - SERVICES gift :

      GiftMsg = new GIFT_MSG();

      try
      {
        //    - Card must not be in use -> Except on LCD Display in SAS Host // || CardToCheck.IsLoggedIn
        if (CardToCheck.IsLocked)
        {
          // The account is in use
          GiftMsg = Gift.GIFT_MSG.CARD_ERROR;

          return false;
        }

        //    - Related account must have enough points to request the gift
        if (Points > CardToCheck.PlayerTracking.CurrentPoints)
        {
          // Not enough points
          GiftMsg = Gift.GIFT_MSG.NOT_ENOUGH_POINTS;

          return false;
        }

        //    - Gift must be available
        if (!Available)
        {
          // Not available
          GiftMsg = Gift.GIFT_MSG.NOT_AVAILABLE;

          return false;
        }

        // Gift redeem limit must to me exceeded

        //    - NOT_REDEEMABLE_CREDIT gift : Card must not have non-redeemable balance if in General Params is 
        //                                   OnlyWhenTotalNotRedeemableBalanceIsZero enabled or
        //                                   OnlyWhenManuallyAddedNotRedeemableBalanceIsZero enabled
        //                                   for Manual or Gifts non redeem recharges

        if (Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT)
        {
          // JAB 06-JUL-2012: Unify the above process in a single function (not redeemable credit control).
          if (!Accounts.CheckAddNotRedeemable(CardToCheck.AccountId, out GiftMsg))
          {

            return false;
          }
        } //end if GIFT_TYPE.NOT_REDEEMABLE_CREDIT 

        //    - OBJECT GIFT : Gift must be in stock 
        if (Type == GIFT_TYPE.OBJECT)
        {
          if (Stock < 1 || Stock <= RequestCounter)
          {
            // No stock available
            GiftMsg = Gift.GIFT_MSG.NOT_ENOUGH_STOCK;
            return false;
          }

          ////SSC 06-JUNY-2012: NumUnits must be <= Stock
          //if (!IsCashier && GiftNumUnits > Stock)
          //{
          //  // TODO: Define message
          //  GiftMsg = Gift.GIFT_MSG.NOT_ENOUGH_STOCK;

          //  return false;
          //}
        }

        // Gift can be requested
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Gift.CheckGiftRequest");
        Log.Exception(_ex);

        return false;
      }

    } // CheckGiftRequest

  } // Class Gift

  //------------------------------------------------------------------------------
  // CLASS : GiftInstance
  // 
  // NOTES :

  public class GiftInstance
  {
    #region Constants

    public enum GIFT_INSTANCE_MSG       // Gift Instance messages
    {
      UNKNOWN = 0,
      DELIVERY_ERROR = 1,
      GIFT_EXPIRED = 2,
      NOT_ENOUGH_POINTS_TO_CONVERT_NRC = 3,
      REQUEST_ERROR = 4,
      GIFT_NOT_FOUND = 5,
      DRAW_NOT_AVAILABLE = 6,
      DRAW_NUMBERS_NOT_AVAILABLE = 7,
      DRAW_NUMBERS_NOT_AVAILABLE_ACCOUNT = 8,
      DB_ERROR = 9,
      CANCEL_CARD_ERROR = 10,
      CANCEL_GIFT_ERROR = 11,
      CANCEL_STATUS_ERROR = 12,
      CANCEL_PROMOTION_NOT_FOUND = 13,
      CANCEL_NOT_ENOUGH_CREDIT = 14,
      STATUS_ERROR = 15,
      PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS = 16,
      NOT_CONNECTED_TO_MULTISITE = 17,
      REDEEM_LIMIT_EXCEEDED = 18,
      AML_NO_IDENTIFICATION_LIMIT_EXCEEDED = 19,
      PRINTER_PRINT_FAIL = 20,
      PRINTER_IS_NOT_READY = 21,
      TITO_TICKET_NOT_VALID = 22,
      RECHARGE_LIMIT_EXCEEDED = 23,
    };

    public const String FORMAT_POINTS = "#,##0";

    #endregion Constants

    #region Members

    public Barcode m_voucher_number;
    public Int64 m_account_id;
    public Int64 m_gift_id;
    public GIFT_REQUEST_STATUS m_status;

    private Int64 m_gift_instance_id;
    private Int64 m_op_request_id;
    private Int64 m_op_delivery_id;
    private Boolean m_gift_delivered;
    private CardData m_card;
    private Gift m_gift;
    private Int32 m_num_units;
    private Decimal m_spent_points;
    private Currency m_not_redeemable_credits;  // Not redeemable credits amount
    private Boolean m_gift_ok;
    private DateTime m_datetime_requested;
    private DateTime m_datetime_expiration;
    private DateTime m_datetime_delivery;
    private Int64 m_draw_id;
    private Decimal m_draw_number_points_price;   // Price (in points) of every number of the draw
    private Int32 m_draw_numbers_available;     // Maximum draw numbers available for the account
    private Decimal m_draw_max_points_to_spend;   // Maximum amount of points that can be spent for the account
    private DrawTicket m_draw_ticket;                // To show the draw ticket voucher
    private Boolean m_is_cashier;
    protected Int64 m_promotion_unique_id;
    private Int32 m_num_available;

    #endregion Members

    private void ClearGiftInstance()
    {
      m_voucher_number = null;
      m_gift_instance_id = 0;
      m_op_request_id = 0;
      m_op_delivery_id = 0;
      m_account_id = 0;
      m_card = null;
      m_gift = null;
      m_gift_id = 0;
      m_num_units = 0;
      m_spent_points = 0;
      m_not_redeemable_credits = 0;
      m_gift_ok = false;
      m_gift_delivered = false;
      m_draw_id = 0;
      m_draw_number_points_price = 0;
      m_draw_numbers_available = 0;
      m_draw_max_points_to_spend = 0;
      m_draw_ticket = null;
      m_num_available = 0;

      return;
    } // ClearGiftInstance

    public GiftInstance()
    {
      ClearGiftInstance();
    } // GiftInstance

    public GiftInstance(Gift GiftItem, CardData CardToUse)
    {
      ClearGiftInstance();

      m_gift_id = GiftItem.GiftId;

      m_gift = new Gift();
      m_gift = GiftItem;
      if (m_gift.Type == GIFT_TYPE.DRAW_NUMBERS)
      {
        m_draw_id = m_gift.DrawId;
      }

      m_card = new CardData();
      m_card = CardToUse;

      return;
    } // GiftInstance

    public Int64 InstanceId
    {
      get { return m_gift_instance_id; }
      set { m_gift_instance_id = value; }
    }

    public Int64 RequestOperationId
    {
      get { return m_op_request_id; }
      set { m_op_request_id = value; }
    }

    public Int64 DeliveryOperationId
    {
      get { return m_op_delivery_id; }
      set { m_op_delivery_id = value; }
    }

    public DateTime RequestDateTime
    {
      get { return m_datetime_requested; }
      set { m_datetime_requested = value; }
    }

    public DateTime DeliveryDateTime
    {
      get { return m_datetime_delivery; }
      set { m_datetime_delivery = value; }
    }

    public Boolean Delivered
    {
      get { return m_gift_delivered; }
      set { m_gift_delivered = value; }
    }

    public DateTime ExpirationDateTime
    {
      get { return m_datetime_expiration; }
      set { m_datetime_expiration = value; }
    }

    public Boolean VoucherNumberOk
    {
      get { return (m_voucher_number.ExternalCode != ""); }
      //set { }
    }

    public Boolean GiftOk
    {
      get { return m_gift_ok; }
      set { m_gift_ok = value; }
    }

    public Decimal SpentPoints
    {
      get { return m_spent_points; }
      set { m_spent_points = value; }
    }

    public Int64 DrawId
    {
      get { return m_draw_id; }
      set { m_draw_id = value; }
    }

    public Decimal DrawNumberPointsPrice
    {
      get { return m_draw_number_points_price; }
      set { m_draw_number_points_price = value; }
    }

    public Decimal DrawMaxPointsToSpend
    {
      get { return m_draw_max_points_to_spend; }
      set { m_draw_max_points_to_spend = value; }
    }

    public DrawTicket DrawTicket
    {
      get { return m_draw_ticket; }
      set { m_draw_ticket = value; }
    }

    public Int32 NumUnits
    {
      get { return m_num_units; }
      set { m_num_units = value; }
    }

    public Int32 NumAvailable
    {
      get { return m_num_available; }
    }

    public Currency NotRedeemableCredits
    {
      get { return m_not_redeemable_credits; }
      set { m_not_redeemable_credits = value; }
    }

    public Gift RelatedGift
    {
      get { return m_gift; }
      set { m_gift = value; }
    }

    public CardData RelatedCard
    {
      get { return m_card; }
      set { m_card = value; }
    }

    public Boolean IsCashier
    {
      get { return m_is_cashier; }
      set { m_is_cashier = value; }
    }

    public GIFT_REQUEST_STATUS Status
    {
      get { return m_status; }
      set { m_status = value; }
    }

    public Boolean DB_ReadInstaceAndGift(Int64 InstanceId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GIN_GIFT_INSTANCE_ID ");
        _sb.AppendLine("        , GIN_ACCOUNT_ID ");
        _sb.AppendLine("        , GIN_GIFT_ID ");
        _sb.AppendLine("        , GIN_SPENT_POINTS ");
        _sb.AppendLine("        , GIN_REQUESTED ");
        _sb.AppendLine("        , GIN_DELIVERED ");
        _sb.AppendLine("        , GIN_EXPIRATION ");
        _sb.AppendLine("        , GIN_REQUEST_STATUS ");
        _sb.AppendLine("        , GIN_NUM_ITEMS ");
        _sb.AppendLine("        , GIN_DATA_01 ");
        _sb.AppendLine("        , GIN_CONVERSION_TO_NRC ");
        _sb.AppendLine("        , GIN_POINTS ");
        _sb.AppendLine("        , GIN_OPER_REQUEST_ID ");
        _sb.AppendLine("   FROM   GIFT_INSTANCES ");
        _sb.AppendLine("  WHERE   GIN_GIFT_INSTANCE_ID = @pGiftInstanceId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGiftInstanceId", SqlDbType.BigInt).Value = InstanceId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {

            if (!_reader.Read())
            {
              return false;
            }

            m_gift_instance_id = _reader.GetInt64(0);
            m_account_id = _reader.GetInt64(1);
            m_gift_id = _reader.GetInt64(2);
            m_gift = new Gift();
            if (!m_gift.DB_GiftData(m_gift_id, m_account_id))
            {
              return false;
            }
            m_spent_points = _reader.GetDecimal(3);
            m_datetime_requested = _reader.GetDateTime(4);

            if (_reader.IsDBNull(5))
            {
              m_datetime_delivery = DateTime.MinValue;
            }
            else
            {
              m_datetime_delivery = _reader.GetDateTime(5);
            }

            m_datetime_expiration = _reader.GetDateTime(6);
            m_status = (GIFT_REQUEST_STATUS)_reader.GetInt32(7);
            m_num_units = _reader.GetInt32(8);

            switch (m_gift.Type)
            {
              case GIFT_TYPE.REDEEMABLE_CREDIT:
              case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
                m_promotion_unique_id = _reader.IsDBNull(9) ? 0 : _reader.GetInt64(9);
                // GIN_SPENT_POINTS * GIN_CONVERSION_TO_NRC / GIN_POINTS                
                m_not_redeemable_credits = (m_spent_points * (_reader.IsDBNull(10) ? 0 : _reader.GetDecimal(10))) / _reader.GetDecimal(11);
                break;

              case GIFT_TYPE.DRAW_NUMBERS:
                DrawId = _reader.IsDBNull(9) ? _reader.GetInt64(9) : 0;
                break;

              default:
                break;
            }

            m_op_request_id = _reader.GetInt64(12);

          }

        }

      }
      catch
      {

        return false;

      }

      return true;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Adds a gift request to the GIFT_INSTANCES table
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: data was inserted successfully
    //      - false: data was not inserted
    // 
    //   NOTES :

    public Boolean DB_GiftInstanceRequest(SqlTransaction SqlTrx)
    {
      SqlParameter _sql_parameter;
      String _sql_txt;
      Int32 _num_rows_updated = 0;
      Currency _aux_currency1;
      Int32 _gift_expiration_days;

      try
      {
        //    - Create the gift instance
        _sql_txt = "INSERT INTO GIFT_INSTANCES ( GIN_OPER_REQUEST_ID"
                                              + ", GIN_OPER_DELIVERY_ID"
                                              + ", GIN_ACCOUNT_ID"
                                              + ", GIN_GIFT_ID"
                                              + ", GIN_GIFT_NAME"
                                              + ", GIN_GIFT_TYPE"
                                              + ", GIN_POINTS"
                                              + ", GIN_CONVERSION_TO_NRC"
                                              + ", GIN_SPENT_POINTS"
                                              + ", GIN_REQUESTED"
                                              + ", GIN_EXPIRATION"
                                              + ", GIN_DELIVERED"
                                              + ", GIN_REQUEST_STATUS"
                                              + ", GIN_NUM_ITEMS"
                                              + ", GIN_DATA_01"
                                              + ")"
                                      + " VALUES ( @OperRequestId"
                                              + ", NULL"
                                              + ", @AccountId"
                                              + ", @GiftId"
                                              + ", @GiftName"
                                              + ", @GiftType"
                                              + ", @Points"
                                              + ", @ConversionToNrc"
                                              + ", @SpentPoints"
                                              + ", GETDATE ()"
                                              + ", GETDATE () + @ExpirationDays"
                                              + ", CASE WHEN (@GiftType = @GiftTypeNRC OR @GiftType = @GiftTypeRC OR @GiftType = @GiftTypeDrawNumbers) THEN GETDATE () ELSE NULL END"
                                              + ", @RequestStatus"
                                              + ", @NumItems"
                                              + ", @ExtraData01"
                                              + ")"
                                         + " SET @GiftInstanceId = SCOPE_IDENTITY()"
                                                ;

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@OperRequestId", SqlDbType.BigInt).Value = m_op_request_id;
          _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = m_card.AccountId;
          _sql_cmd.Parameters.Add("@GiftId", SqlDbType.BigInt).Value = m_gift.GiftId;
          _sql_cmd.Parameters.Add("@GiftName", SqlDbType.NVarChar, 50).Value = m_gift.GiftName;
          _sql_cmd.Parameters.Add("@GiftType", SqlDbType.BigInt).Value = m_gift.Type;
          _sql_cmd.Parameters.Add("@GiftTypeNRC", SqlDbType.Int).Value = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
          _sql_cmd.Parameters.Add("@GiftTypeRC", SqlDbType.Int).Value = GIFT_TYPE.REDEEMABLE_CREDIT;
          _sql_cmd.Parameters.Add("@GiftTypeDrawNumbers", SqlDbType.Int).Value = GIFT_TYPE.DRAW_NUMBERS;
          _aux_currency1 = m_gift.Points;
          _sql_cmd.Parameters.Add("@Points", SqlDbType.Money).Value = _aux_currency1.SqlMoney;
          _sql_cmd.Parameters.Add("@ConversionToNrc", SqlDbType.Money).Value = m_gift.ConversionNRC;
          _aux_currency1 = SpentPoints;
          _sql_cmd.Parameters.Add("@SpentPoints", SqlDbType.Money).Value = _aux_currency1.SqlMoney;
          _gift_expiration_days = Convert.ToInt32(WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "GiftExpirationDays"));
          _sql_cmd.Parameters.Add("@ExpirationDays", SqlDbType.Int).Value = _gift_expiration_days;
          _sql_cmd.Parameters.Add("@NumItems", SqlDbType.Int).Value = NumUnits;

          switch (m_gift.Type)
          {
            case GIFT_TYPE.OBJECT:
              _sql_cmd.Parameters.Add("@RequestStatus", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.PENDING;
              _sql_cmd.Parameters.Add("@ExtraData01", SqlDbType.BigInt).Value = 0;
              break;

            case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
            case GIFT_TYPE.REDEEMABLE_CREDIT:
              _sql_cmd.Parameters.Add("@RequestStatus", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.DELIVERED;
              _sql_cmd.Parameters.Add("@ExtraData01", SqlDbType.BigInt).Value = m_promotion_unique_id;
              break;

            case GIFT_TYPE.DRAW_NUMBERS:
              _sql_cmd.Parameters.Add("@RequestStatus", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.DELIVERED;
              _sql_cmd.Parameters.Add("@ExtraData01", SqlDbType.BigInt).Value = DrawId;        // Draw Id
              break;

            case GIFT_TYPE.SERVICES:
              _sql_cmd.Parameters.Add("@RequestStatus", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.PENDING;
              _sql_cmd.Parameters.Add("@ExtraData01", SqlDbType.BigInt).Value = 0;
              break;
            default:
              break;
          }

          _sql_parameter = _sql_cmd.Parameters.Add("@GiftInstanceId", SqlDbType.BigInt);
          _sql_parameter.Direction = ParameterDirection.Output;

          _num_rows_updated = _sql_cmd.ExecuteNonQuery();
        }

        if (_num_rows_updated != 1)
        {
          string _message;

          _message = "GiftInstance.Request. GIFT_INSTANCES INSERT. Operation Id: " + m_op_request_id.ToString()
                                                                   + ", Card Id: " + m_card.AccountId.ToString()
                                                                      + ", Gift: " + m_gift.GiftId.ToString() + "-" + m_gift.GiftName
                                                                    + ", Points: " + SpentPoints.ToString(Gift.FORMAT_POINTS)
                                                                                   ;

          throw new Exception(_message);
        }

        m_gift_instance_id = (Int64)_sql_parameter.Value;

        return true;
      } // try

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_GiftInstanceRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Checks specific conditions to request a draw numbers gift
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - Message: If returns false, Message has the reason for the error.
    //
    // RETURNS :
    //      - True : There is an available draw and the acount can request numbers 
    //      - False : No available draw or the account can not get more tickets from a draw
    // 
    //   NOTES :

    public Boolean CheckDrawNumbersGift(out GIFT_INSTANCE_MSG Message)
    {
      DrawNumberList _draw_numbers;
      List<DrawTicket> _dtl_dummy;
      Int64 _pending;
      Points _spent_points;
      DrawTicketLogic.GetTicketCodes _rc;

      _pending = 0;

      Message = GiftInstance.GIFT_INSTANCE_MSG.UNKNOWN;

      try
      {
        _rc = DrawTicketLogic.GetTicketCodes.ERROR;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _rc = Draws.GenerateTicketsByPoints(0, RelatedCard, RelatedCard.PlayerTracking.TruncatedPoints,
                                              m_draw_id, true, _db_trx.SqlTransaction, out _draw_numbers, out _spent_points, out _dtl_dummy);
        }

        if (_rc != DrawTicketLogic.GetTicketCodes.OK)
        {
          switch (_rc)
          {
            case DrawTicketLogic.GetTicketCodes.LIMIT_REACHED:
              Message = GiftInstance.GIFT_INSTANCE_MSG.DRAW_NUMBERS_NOT_AVAILABLE_ACCOUNT;
              break;

            case DrawTicketLogic.GetTicketCodes.DRAW_MAX_REACHED:
              Message = GiftInstance.GIFT_INSTANCE_MSG.DRAW_NUMBERS_NOT_AVAILABLE;
              break;

            default:
              Message = GiftInstance.GIFT_INSTANCE_MSG.DRAW_NOT_AVAILABLE;
              break;
          }

          return false;
        }

        foreach (DrawNumbers _dn in _draw_numbers)
        {
          _pending += _dn.PendingNumbers;
        }

        if (NumUnits > _pending)
        {
          if (_pending == 0)
          {
            // NO gift can be requested
            Message = GiftInstance.GIFT_INSTANCE_MSG.DRAW_NUMBERS_NOT_AVAILABLE;

            return false;
          }

          // Straight request the remaining draw numbers: 
          //    - No warning message
          //    - Recalculate the number of units and the points to spend

          NumUnits = (Int32)_pending;
          SpentPoints = _spent_points;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        //m_draw_id = m_draw_id;
        m_draw_number_points_price = RelatedGift.Points;    // _draw_number_points_price;
        m_draw_numbers_available = (Int32)_pending;
        m_draw_max_points_to_spend = m_draw_number_points_price * m_draw_numbers_available;
      }
    } // CheckDrawNumbersGift

    //------------------------------------------------------------------------------
    // PURPOSE : Calculates the account figures after selecting the current gift and 
    //           spending an amount of points.
    //
    //  PARAMS :
    //      - INPUT :
    //          - PointsToSpend : Amount of points to spent and it should only be 
    //                            considered when the gift type is NOT_REDEEMABLE_CREDIT.
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true : Figures were calculated successfully
    //      - false : An error occurred calculating the figures (not enough points to 
    //                get the selected gift).
    // 
    //   NOTES :

    public Boolean GiftBalance(Decimal PointsToSpend)
    {
      Decimal _acc_points;
      Decimal _num_times;

      NumUnits = 0;
      SpentPoints = 0;
      NotRedeemableCredits = 0;

      _acc_points = m_card.PlayerTracking.TruncatedPoints;

      if (PointsToSpend > _acc_points)
      {
        // Not enough points balance to get the gift
        return false;
      }

      switch (m_gift.Type)
      {
        case GIFT_TYPE.OBJECT:
          // Gift = Object
          if (IsCashier)
          {
            NumUnits = 1;
            SpentPoints = m_gift.Points;
          }
          else
          {
            _num_times = Math.Truncate(PointsToSpend / m_gift.Points);
            if (NumAvailable > 0)
            {
              _num_times = Math.Min(_num_times, NumAvailable);
            }
            NumUnits = (Int32)_num_times;
            SpentPoints = PointsToSpend;
          }

          NotRedeemableCredits = 0;
          break;

        case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
        case GIFT_TYPE.REDEEMABLE_CREDIT:
          // Gift = Non Redeemable Credit
          // Gift = Redeemable Credit
          _num_times = Math.Truncate(PointsToSpend / m_gift.Points);
          if (NumAvailable > 0)
          {
            _num_times = Math.Min(_num_times, NumAvailable);
          }
          if (_num_times < 1)
          {
            return false;
          }

          NumUnits = (Int32)_num_times;
          SpentPoints = _num_times * m_gift.Points;
          NotRedeemableCredits = _num_times * m_gift.ConversionNRC;
          break;

        case GIFT_TYPE.DRAW_NUMBERS:
          // The value of DrawMaxPointsToSpend is set in routine CheckGiftRequest().
          // - From Cashier, DrawMaxPointsToSpend has a correct value. It's called CheckGiftRequest, GetBalance and again CheckGiftRequest.
          // - From PromoBOX, DrawMaxPointsToSpend is 0. It's only called GetBalance and again CheckGiftRequest.
          if (DrawMaxPointsToSpend == 0)
          {
            DrawMaxPointsToSpend = PointsToSpend;
          }

          _num_times = Math.Truncate(Math.Min(PointsToSpend, DrawMaxPointsToSpend) / m_gift.Points);
          if (NumAvailable > 0)
          {
            _num_times = Math.Min(_num_times, NumAvailable);
          }
          if (_num_times < 1)
          {
            return false;
          }

          NumUnits = (Int32)_num_times;
          SpentPoints = _num_times * m_gift.Points;
          NotRedeemableCredits = 0;
          break;

        case GIFT_TYPE.SERVICES:
          // Gift = Service
          if (IsCashier)
          {
            NumUnits = 1;
            SpentPoints = m_gift.Points;
          }
          else
          {
            _num_times = Math.Truncate(PointsToSpend / m_gift.Points);
            if (NumAvailable > 0)
            {
              _num_times = Math.Min(_num_times, NumAvailable);
            }
            NumUnits = (Int32)_num_times;
            SpentPoints = PointsToSpend;
          }

          NotRedeemableCredits = 0;
          break;

        default:
          return false;
        //break;
      }

      if (_acc_points < SpentPoints)
      {
        return false;
      }

      return true;
    } // GiftBalance

    //------------------------------------------------------------------------------
    // PURPOSE : Check the gift request the instant before requesting the gift
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public Boolean CheckGiftRequest(Boolean IsCashier, out GiftInstance.GIFT_INSTANCE_MSG MsgGiftInstance, out Gift.GIFT_MSG MsgGift)
    {
      Boolean _check_draw_numbers;
      String _message;
      String _caption;
      MessageBoxIcon _msg_icon;

      Int32 _account_day_tot;
      Int32 _account_month_tot;
      Int32 _global_day_tot;
      Int32 _global_month_tot;


      MsgGift = Gift.GIFT_MSG.UNKNOWN;
      MsgGiftInstance = GIFT_INSTANCE_MSG.UNKNOWN;

      // Reassign the gift id to force reading the gift's data from the DB
      // RCI 15-JUN-2011: Set property for GiftId doesn't exist anymore. Use DB_GiftData() to read gift's data from the DB.
      //RelatedGift.GiftId = RelatedGift.GiftId;

      // DRAW NUMBERS gift : 
      //        - Check whether there is a draw available
      // 18-FEB-2014 JPJ Defect WIG-639 Draws can not be given as gift
      if (RelatedGift.Type == GIFT_TYPE.DRAW_NUMBERS)
      {
        _check_draw_numbers = CheckDrawNumbersGift(out MsgGiftInstance);

        if (!_check_draw_numbers)
        {
          GetMessage(MsgGiftInstance, out _message, out _caption, out _msg_icon);
          // TODO: Log del error??
        }

        return _check_draw_numbers;
      }

      // LJM 30-JAN-2014: If anything goes wrong, we don't continue with the promotion
      if (!RelatedGift.DB_GiftData(RelatedGift.GiftId, RelatedCard.AccountId))
      {

        return false;
      }

      // TODO Check Max Redeemable/NotRedeemable Credits per Points
      if (RelatedGift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT || RelatedGift.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
      {
        Currency _max_redeem;
        Currency _max_no_redeem;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          //Check the max NRC and RC for the user
          if (!GiftRequest.MaxBuyableCredits(RelatedCard, out _max_redeem, out _max_no_redeem, _db_trx.SqlTransaction))
          {
            Log.Error("CheckGiftRequest->MaxBuyableCredits: false - AccountId: " + RelatedCard.AccountId.ToString() + ".");

            return false;
          }
        }

        if (RelatedGift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT)
        {
          if (_max_no_redeem == 0 || this.NotRedeemableCredits > _max_no_redeem)
          {
            // TODO: Msg de l�mite alcanzado.
            MsgGift = Gift.GIFT_MSG.NOT_AVAILABLE;

            return false;
          }
        }

        if (RelatedGift.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
        {
          if (_max_redeem == 0 || this.NotRedeemableCredits > _max_redeem)
          {
            // TODO: Msg de l�mite alcanzado.
            MsgGift = Gift.GIFT_MSG.NOT_AVAILABLE;

            return false;
          }
        }
      }

      // RCI & JRM 24-MAY-2013: Moved the Stock check from Gift.CheckGiftRequest to GiftInstance.CheckGiftRequest.
      if (RelatedGift.Type == GIFT_TYPE.OBJECT)
      {
        // There is enough stock
        if (RelatedGift.Stock > 0 && RelatedGift.Stock > RelatedGift.RequestCounter)
        {
          // Check if the requested units exceeds the stock
          // NumUnits is for now always 1. But the check exists for future functionallity.
          if (NumUnits > RelatedGift.Stock - RelatedGift.RequestCounter)
          {
            // TODO: Define message
            MsgGift = Gift.GIFT_MSG.NOT_ENOUGH_STOCK;

            return false;
          }
        }
      }

      // Check if limit has exceeded on number or redeambable gifts
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!CheckGiftRequestedLimit(NumUnits, RelatedGift, RelatedCard.AccountId, _db_trx.SqlTransaction,
                                     out _account_day_tot, out _account_month_tot, out _global_day_tot, out _global_month_tot))
        {
          MsgGift = Gift.GIFT_MSG.GIFT_REDEEM_LIMIT_EXCEEDED;

          return false;
        }
      }

      return RelatedGift.CheckGiftRequest(RelatedCard, IsCashier, out MsgGift);
      // TODO: Log del error _msg_gift ???

    } // CheckGiftRequest


    //------------------------------------------------------------------------------
    // PURPOSE : Checks that gifts redeemed do not exceed the redeem limit on a custumer or global level
    //
    //  PARAMS :
    //      - INPUT :
    //          - pGift
    //          - pAccountId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - AccountDayTot
    //          - AccountMonthTot
    //          - GlobalDayTot
    //          - GlobalMonthTot
    //
    // RETURNS :
    //          - Boolean
    //   NOTES :

    //
    public Boolean CheckGiftRequestedLimit(Int32 GiftRequestCount, Gift Gift, Int64 AccountId, SqlTransaction Trx,
                                           out Int32 AccountDayTot, out Int32 AccountMonthTot, out Int32 GlobalDayTot, out Int32 GlobalMonthTot)
    {
      StringBuilder _sb;
      Boolean _test_result = false;
      Int32 _account_day_tot = 0;
      Int32 _account_month_tot = 0;
      Int32 _global_day_tot = 0;
      Int32 _global_month_tot = 0;
      DateTime _today;
      DateTime _first_day_of_month;
      String _sql_txt;

      try
      {
        _today = Misc.TodayOpening();
        _first_day_of_month = new DateTime(_today.Year, _today.Month, 1, _today.Hour, _today.Minute, _today.Second);
        _sb = new StringBuilder();

        _sql_txt = "SELECT   ISNULL(SUM(CASE WHEN GIN_REQUESTED >= @PeriodStartTime AND GIN_ACCOUNT_ID = @AccountId THEN GIN_NUM_ITEMS ELSE 0 END), 0) AS ACC_DAY      " +
                   "       , ISNULL(SUM(CASE WHEN GIN_ACCOUNT_ID = @AccountId								                        THEN GIN_NUM_ITEMS ELSE 0 END), 0) AS ACC_MONTH    " +
                   "       , ISNULL(SUM(CASE WHEN GIN_REQUESTED >= @PeriodStartTime                                 THEN GIN_NUM_ITEMS ELSE 0 END), 0) AS GLOBAL_DAY   " +
                   "       , ISNULL(SUM(GIN_NUM_ITEMS), 0)																						                                                 AS GLOBAL_MONTH " +
                   "  FROM   GIFT_INSTANCES                                                                                                                 " +
                   " WHERE   GIN_REQUESTED >= @FirstDayOfMonth                                                                                              " +
                   "   AND   GIN_GIFT_ID    = @GiftId AND GIN_REQUEST_STATUS <> @CancelStatus                                                                                                      ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@PeriodStartTime", SqlDbType.DateTime).Value = _today;
          _sql_cmd.Parameters.Add("@GiftId", SqlDbType.Int).Value = Gift.GiftId;
          _sql_cmd.Parameters.Add("@FirstDayOfMonth", SqlDbType.DateTime).Value = _first_day_of_month;
          _sql_cmd.Parameters.Add("@CancelStatus", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.CANCELED;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _account_day_tot = _reader.GetInt32(0);
              _account_month_tot = _reader.GetInt32(1);
              _global_day_tot = _reader.GetInt32(2);
              _global_month_tot = _reader.GetInt32(3);
            }
          }
        }
      }
      catch
      {
        _test_result = false;
        _account_day_tot = 0;
        _account_month_tot = 0;
        _global_day_tot = 0;
        _global_month_tot = 0;
      }
      finally
      {
        AccountDayTot = _account_day_tot;
        AccountMonthTot = _account_month_tot;
        GlobalDayTot = _global_day_tot;
        GlobalMonthTot = _global_month_tot;
      }

      if (((_account_day_tot + GiftRequestCount) <= Gift.AccountDailyLimit || Gift.AccountDailyLimit == 0) &&
          ((_account_month_tot + GiftRequestCount) <= Gift.AccountMonthlyLimit || Gift.AccountMonthlyLimit == 0) &&
          ((_global_day_tot + GiftRequestCount) <= Gift.GlobalDailyLimit || Gift.GlobalDailyLimit == 0) &&
          ((_global_month_tot + GiftRequestCount) <= Gift.GlobalMonthlyLimit || Gift.GlobalMonthlyLimit == 0))
      {
        _test_result = true;
      }

      return _test_result;
    } //  CheckGiftRequestedLimit

    //------------------------------------------------------------------------------
    // PURPOSE : Calculates the exact number of available gifts for this giftInstance according to the gift redeem limits and 
    // gifts that have already been redeemed
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public void DB_GetNumOfAvailableGifts()
    {
      // Assumptions:
      // 1) All gifts tested here were available to the customer at the time of opening this view
      Int32 _account_day_gift_tot;
      Int32 _account_month_gift_tot;
      Int32 _global_day_gift_tot;
      Int32 _global_month_gift_tot;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        // Get existing account and global totals for gifts already redeemed during both the period and the month
        this.CheckGiftRequestedLimit(0, this.RelatedGift, this.RelatedCard.AccountId, _db_trx.SqlTransaction,
                                     out _account_day_gift_tot, out _account_month_gift_tot, out _global_day_gift_tot, out _global_month_gift_tot);
      }

      GetNumOfAvailableGifts(_account_day_gift_tot, _account_month_gift_tot, _global_day_gift_tot, _global_month_gift_tot);

    } // DB_GetNumOfAvailableGifts

    //------------------------------------------------------------------------------
    // PURPOSE : Calculates the exact number of available gifts for this giftInstance according to the gift redeem limits
    //
    //  PARAMS :
    //      - INPUT :
    //            - AccountDayRequestedGifts
    //            - AccountMonthRequestedGifts
    //            - GlobalDayRequestedGifts
    //            - GlobalMonthRequestedGifts
    //      - OUTPUT :
    //
    //
    // RETURNS :

    public void GetNumOfAvailableGifts(Int32 AccountDayRequestedGifts, Int32 AccountMonthRequestedGifts, Int32 GlobalDayRequestedGifts, Int32 GlobalMonthRequestedGifts)
    {
      Int32 _available_gifts;

      Int32 _account_day_available;
      Int32 _account_month_available;
      Int32 _global_day_available;
      Int32 _global_month_available;

      _global_month_available = this.RelatedGift.GlobalMonthlyLimit > 0 ? (this.RelatedGift.GlobalMonthlyLimit - GlobalMonthRequestedGifts) : Int32.MaxValue;
      _global_day_available = this.RelatedGift.GlobalDailyLimit > 0 ? (this.RelatedGift.GlobalDailyLimit - GlobalDayRequestedGifts) : Int32.MaxValue;
      _account_month_available = this.RelatedGift.AccountMonthlyLimit > 0 ? (this.RelatedGift.AccountMonthlyLimit - AccountMonthRequestedGifts) : Int32.MaxValue;
      _account_day_available = this.RelatedGift.AccountDailyLimit > 0 ? (this.RelatedGift.AccountDailyLimit - AccountDayRequestedGifts) : Int32.MaxValue;

      _available_gifts = Math.Min(_global_month_available, _global_day_available);
      _available_gifts = Math.Min(_available_gifts, _account_month_available);
      _available_gifts = Math.Min(_available_gifts, _account_day_available);

      m_num_available = _available_gifts;

    } // GetNumOfAvailableGifts

    //------------------------------------------------------------------------------
    // PURPOSE : Updates into the DB all the data required to request a gift
    //
    //  PARAMS :
    //      - INPUT :
    //          - CashierSession
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - VoucherList
    //
    // RETURNS :
    // 
    //   NOTES :

    public Boolean CommonRequest(Int64 CashierSessionId,
                                 AccountMovementsTable AccountMovementsTable,
                                 CashierMovementsTable CashierMovementsTable,
                                 TerminalTypes TerminalType,
                                 SqlTransaction SqlTrx,
                                 out ArrayList VoucherList,
                                 out GIFT_INSTANCE_MSG Msg,
                                 out Ticket TitoTicket)
    {
      SqlCommand _sql_command_acc;
      SqlCommand _sql_command_gift;
      String _sql_query;
      Int64 _operation_data;
      Int32 _num_rows_updated;
      Currency _aux_currency1;
      OperationCode _operation_code;
      Int64 _promotion_id;
      Int32 _gift_expiration_days;
      Currency _won_lock;
      List<DrawTicket> _ticket_list;
      Int64 _operation_id;
      ArrayList _voucher_list_aux;

      Promotion.PROMOTION_TYPE _promo_type;
      AccountPromotion _promotion;
      DataTable _promotions;
      Int32 _int_value;
      Boolean _credit_awarded;
      RechargeInputParameters _idata = null;
      RechargeOutputParameters _odata = null;
      Currency _money_as_cashin;
      Boolean _include_card_document;

      Boolean _create_ticket;
      Boolean _need_print_redeemable;
      Boolean _need_print_no_redeemable;
      Currency _tito_ticket_amount;
      TITO_TICKET_TYPE _tito_ticket_type;
      CASHIER_MOVEMENT _cashier_movement;
      ParamsTicketOperation _params_ticket_operation;
      Ticket _tito_ticket;
      Boolean _is_tito_re_promobox_promotion;

      TitoTicket = null;
      _create_ticket = false;

      _ticket_list = new List<DrawTicket>();
      VoucherList = new ArrayList();

      m_promotion_unique_id = 0;
      _operation_id = 0;
      _credit_awarded = false;
      _include_card_document = false;

      Msg = GIFT_INSTANCE_MSG.REQUEST_ERROR;

      try
      {
        // AJQ 19-MAR-2013 - Avoid Point Redeem
        if (m_card.PointsStatus != ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED)
        {
          return false;
        }

        if (GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
        {
          int _connected_to_multisite;

          _connected_to_multisite = 0;

          try
          {
            // Check Connected
            StringBuilder _sb;
            _sb = new StringBuilder();
            _sb.AppendLine("SELECT   COUNT(*)");
            _sb.AppendLine("  FROM 	 WWP_STATUS");
            _sb.AppendLine(" WHERE 	 WWP_TYPE   = 1");
            _sb.AppendLine("   AND   WWP_STATUS = 1");
            _sb.AppendLine("   AND 	 DATEDIFF(SECOND, WWP_LAST_SENT_MSG, GETDATE()) < 180");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _connected_to_multisite = (int)_cmd.ExecuteScalar();
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          if (_connected_to_multisite <= 0)
          {
            Msg = GIFT_INSTANCE_MSG.NOT_CONNECTED_TO_MULTISITE;

            return false;
          }
        }

        // Steps
        //    - Set the Operation
        //    - Set the Promotion
        //    - Insert the Operation according to the Promotion
        //    - Card movement: Points transfer 
        //    - Only when gift exchanges points to NRCs ( Gift = NR CREDIT )
        //        - Card movement: NR Credits transfer
        //        - Update the account's NRCs
        //    - Only when gift exchanges points to Draw Numbers ( Gift = DRAW NUMBERS )
        //        - Draw Numbers request
        //    - Gift Counters
        //        - Increase gift request counter ( Gift = OBJECT )
        //        - Increase gift delivery counter ( Gift = NR CREDIT | DRAW NUMBERS )
        //    - Update the account's points
        //    - Generate the gift voucher

        //    - Set the Operation
        //          - GIFT_REQUEST : when exchanging points to objects
        //          - GIFT_DELIVERY : when exchanging points to not redeemable credits | draw numbers

        _won_lock = 0;

        _idata = new RechargeInputParameters();
        _idata.NoMoney = false;
        _idata.GenerateVouchers = false;
        _money_as_cashin = 0;

        switch (m_gift.Type)
        {
          case GIFT_TYPE.OBJECT:
            _operation_code = OperationCode.GIFT_REQUEST;
            _operation_data = 0;
            break;

          case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
            _operation_code = OperationCode.GIFT_DELIVERY;
            _operation_data = 0;
            _credit_awarded = true;
            break;

          case GIFT_TYPE.REDEEMABLE_CREDIT:
            _operation_code = OperationCode.GIFT_DELIVERY;

            _operation_data = 0;
            _won_lock = -0.0001m;
            _credit_awarded = true;

            _is_tito_re_promobox_promotion = GeneralParam.GetBoolean("WigosKiosk", "Promotions.RE.PrintTITOTicket", false) && 
                WSI.Common.TITO.Utils.IsTitoMode() && TerminalType == TerminalTypes.PROMOBOX;

            if (GeneralParam.GetBoolean("Gifts.RedeemableCredits", "AsCashIn") && !_is_tito_re_promobox_promotion)
            {
              _operation_code = OperationCode.GIFT_REDEEMABLE_AS_CASHIN;

              _idata.NoMoney = true;
              _idata.GenerateVouchers = true;
              _money_as_cashin = NotRedeemableCredits;
            }
            break;

          case GIFT_TYPE.DRAW_NUMBERS:
            _operation_code = OperationCode.GIFT_DRAW_TICKET;
            _operation_data = DrawId;         // Operation Data = Draw id
            // RAB 19-FEB-2016: ShowDocumentId = 0 --> Hide client document.
            //                  ShowDocumentId = 1 --> Show client document.
            _include_card_document = GeneralParam.GetBoolean("Cashier.DrawTicket", "ShowDocumentId", false);
            break;

          case GIFT_TYPE.SERVICES:
            _operation_code = OperationCode.GIFT_REQUEST;
            _operation_data = 0;
            break;

          default:
            {
              string _message;

              _message = "GiftInstance.Request. Wrong Gift Type = " + m_gift.Type.ToString();

              throw new Exception(_message);
            }
          //break;
        }

        //    - Set the Promotion
        //          - Get the player tracking's promotion id when gift is NRC or RedeemCredit

        _promotions = AccountPromotion.CreateAccountPromotionTable();

        _promotion = null;
        _promotion_id = 0;

        if (m_gift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT ||
            m_gift.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
        {
          if (_idata.NoMoney)
          {
            // Do nothing
          }
          else
          {
            _promo_type = (m_gift.Type == GIFT_TYPE.REDEEMABLE_CREDIT) ? Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE : Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE;
            _promotion = new AccountPromotion(); ;

            //    - Get the player tracking's promotion id
            if (!Promotion.ReadPromotionData(SqlTrx, m_gift.PromotionId, _promotion))
            {
              Log.Error("GiftInstance.Request. ReadPromotionData. AccountId: " + m_card.AccountId.ToString()
                                                                  + ", Gift: " + m_gift.GiftId.ToString() + "-" + m_gift.GiftName);

              return false;
            }

            _promotion_id = _promotion.id;
          }
        }

        //    - Insert the Operation according to the Promotion

        if (!Operations.DB_InsertOperation(_operation_code,
                                            m_card.AccountId,       // AccountId
                                            CashierSessionId,       // CashierSessionId
                                            0,                      // MbAccountId
                                            SpentPoints,            // Amount = POINTS
                                            _promotion_id,          // PromotionId
                                            NotRedeemableCredits,   // NonRedeemable
                                            _won_lock,              // NonRedeemableWonLock
                                            _operation_data,        // OperationData 
                                            0,
                                            string.Empty,
                                            out _operation_id,      // OperationId
                                            SqlTrx))
        {
          string _message;

          _message = "GiftInstance.Request. DB_InsertOperation Operation Code: " + _operation_code.ToString()
                                                                 + ", Card Id: " + m_card.AccountId.ToString()
                                                                    + ", Gift: " + m_gift.GiftId.ToString() + "-" + m_gift.GiftName
                                                                  + ", Points: " + SpentPoints.ToString(Gift.FORMAT_POINTS)
                                                                                 ;
          throw new Exception(_message);
        }

        //    - Card movement: Points transfer 
        AccountMovementsTable.Add(_operation_id, m_card.AccountId, MovementType.PointsToGiftRequest,
                                  (Decimal)m_card.PlayerTracking.CurrentPoints, SpentPoints, 0,
                                  (Decimal)m_card.PlayerTracking.CurrentPoints - SpentPoints);

        //    - Only when gift exchanges points to NRCs ( Gift = NR CREDIT )
        //        - Card movement: NR Credits transfer
        //        - Update the account's NRCs

        _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE;
        _tito_ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
        _tito_ticket_amount = 0;
        _params_ticket_operation = new ParamsTicketOperation();

        if (m_gift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT
          || m_gift.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
        {


          _need_print_redeemable = false;
          _need_print_no_redeemable = false;
          if (_idata.NoMoney)
          {
            // Do nothing
          }
          else
          {

            if (WSI.Common.TITO.Utils.IsTitoMode() && TerminalType == TerminalTypes.PROMOBOX)
            {
              _need_print_redeemable = m_gift.Type == GIFT_TYPE.REDEEMABLE_CREDIT
                    && GeneralParam.GetBoolean("WigosKiosk", "Promotions.RE.PrintTITOTicket", false);

              _need_print_no_redeemable = m_gift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT
                    && GeneralParam.GetBoolean("WigosKiosk", "Promotions.NR.PrintTITOTicket", false);

            }

            if (!AccountPromotion.AddPromotionToAccount(_operation_id, m_card.AccountId, m_card.PlayerTracking.CardLevel, 0,
                                                        this.SpentPoints, true, NotRedeemableCredits, _promotion, _promotions))
            {
              Log.Error("GiftInstance.Request. AddPromotionToAccount. AccountId: " + m_card.AccountId.ToString()
                                                                      + ", Gift: " + m_gift.GiftId.ToString() + "-" + m_gift.GiftName);

              return false;
            }

          }

          if (_need_print_no_redeemable)
          {
            _create_ticket = true;
            _tito_ticket_amount = NotRedeemableCredits;
            _tito_ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
            _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE;
          }

          if (_need_print_redeemable)
          {
            _create_ticket = true;
            _tito_ticket_amount = NotRedeemableCredits;
            _tito_ticket_type = TITO_TICKET_TYPE.PROMO_REDEEM;
            _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE;
          }


          if (_create_ticket)
          {
            CardData _card;

            _card = new CardData();

            _card.TrackData = m_card.TrackData;
            _card.AccountId = m_card.AccountId;

            _params_ticket_operation.out_promotion_id = _promotion.id;
            _params_ticket_operation.out_cash_amount = _tito_ticket_amount;
            _params_ticket_operation.ticket_type = _tito_ticket_type;
            _params_ticket_operation.in_account = _card;
            _params_ticket_operation.session_info = CashierMovementsTable.CashierSessionInfo;
            _params_ticket_operation.out_operation_id = _operation_id;
            _params_ticket_operation.cashier_terminal_id = CashierMovementsTable.CashierSessionInfo.TerminalId;
            _params_ticket_operation.terminal_types = TerminalType;
            _params_ticket_operation.terminal_id = CashierMovementsTable.CashierSessionInfo.TerminalId;
            _params_ticket_operation.in_cash_amt_0 = _tito_ticket_amount;
            _params_ticket_operation.in_cash_cur_0 = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

            //_params_ticket_operation.out_account_promo_id = OutputParameters.AccountPromotionId;
            if (!CashierMovementsTable.Add(_operation_id, _cashier_movement, _tito_ticket_amount, _card.AccountId, _card.TrackData))
            {
              Log.Error("AccountAddCredit. Error Add cashier movement for TITO ticket printed from PromoBOX.");

              return false;
            }

          }

          _idata.AccountId = m_card.AccountId;
          _idata.AccountLevel = m_card.PlayerTracking.CardLevel;
          _idata.ExternalTrackData = m_card.TrackData;
          _idata.VoucherAccountInfo = m_card.VoucherAccountInfo();
          // From gifts, can't make the client pay the card, so always indicate CardPaid = true.
          _idata.CardPaid = true;
          _idata.CardPrice = 0;

          if (!Accounts.DB_Recharge(_operation_id, _idata, _money_as_cashin, _promotions, _promotion, AccountMovementsTable, CashierMovementsTable, SqlTrx, out _odata))
          {
            switch (_odata.ErrorCode)
            {
              case RECHARGE_ERROR_CODE.ERROR_GENERIC:
              default:
                Msg = GIFT_INSTANCE_MSG.REQUEST_ERROR;
                break;

              case RECHARGE_ERROR_CODE.ERROR_AML_RECHARGE_NOT_AUTHORIZED:
                Msg = GIFT_INSTANCE_MSG.AML_NO_IDENTIFICATION_LIMIT_EXCEEDED;
                break;

              case RECHARGE_ERROR_CODE.ERROR_RECHARGE_LIMIT_EXCEEDED:
                Msg = GIFT_INSTANCE_MSG.RECHARGE_LIMIT_EXCEEDED;
                break;
            }

            Log.Error("GiftInstance.Request. DB_Recharge. AccountId: " + m_card.AccountId.ToString()
                                                          + ", Gift: " + m_gift.GiftId.ToString() + "-" + m_gift.GiftName);

            return false;
          }

          if (_idata.NoMoney)
          {
            // Do nothing
          }
          else
          {
            if (_promotions.Rows.Count == 1)
            {
              m_promotion_unique_id = (Int64)_promotions.Rows[0]["ACP_UNIQUE_ID"];
            }
          }
        }

        //    - Only when gift exchanges points to Draw Numbers ( Gift = DRAW NUMBERS )
        //        - Draw Numbers request
        if (m_gift.Type == GIFT_TYPE.DRAW_NUMBERS)
        {
          DrawNumberList _dn_dummy;
          Points _spent_points;
          DrawTicketLogic.GetTicketCodes _rc;

          _rc = Draws.GenerateTicketsByPoints(_operation_id, m_card, SpentPoints, DrawId, false, SqlTrx,
                                              out _dn_dummy, out _spent_points, out _ticket_list);

          if (_rc != DrawTicketLogic.GetTicketCodes.OK || _spent_points <= 0)
          {
            string _message;

            _message = "No draw tickets can be generated. SpentPoints: " + SpentPoints.ToString();

            throw new Exception(_message);
          }

          SpentPoints = _spent_points;

          //Insert only account_movements
          foreach (DrawTicket _ticket in _ticket_list)
          {
            AccountMovementsTable.Add(_operation_id, m_card.AccountId, MovementType.DrawTicketPrint, m_card.CurrentBalance, 0, 0, m_card.CurrentBalance);
            CashierMovementsTable.Add(_operation_id, CASHIER_MOVEMENT.DRAW_TICKET_PRINT, 0, m_card.AccountId, m_card.TrackData);

            VoucherList.Add(_ticket.Voucher);
          }
        }

        //    - Gift Counters
        //        - Increase gift request counter ( Gift = OBJECT )
        //        - Increase gift request counter (without stock control) ( Gift = SERVICES )
        //        - Increase gift delivery counter ( Gift = NR CREDIT | REDEEM CREDIT )

        if (m_gift.Type == GIFT_TYPE.OBJECT)
        {
          // Gift = OBJECT
          //    - Increase gift request counter 

          _sql_query = "UPDATE   GIFTS                                        " +
                       "   SET   GI_REQUEST_COUNTER = GI_REQUEST_COUNTER + @pNumUnits  " +
                       " WHERE   GI_GIFT_ID = @GiftId                         " +
                       "   AND   GI_CURRENT_STOCK > 0                         " +
                       "   AND   GI_CURRENT_STOCK > GI_REQUEST_COUNTER        ";
        }
        else if (m_gift.Type == GIFT_TYPE.SERVICES)
        {
          // Gift = SERVICES
          //    - Increase gift request counter (without stock control)

          _sql_query = "UPDATE   GIFTS                                        " +
                       "   SET   GI_REQUEST_COUNTER = GI_REQUEST_COUNTER + @pNumUnits  " +
                       " WHERE   GI_GIFT_ID = @GiftId                         ";
        }
        else if (m_gift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT
               || m_gift.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
        {
          // Gift = NR CREDIT | REDEEM CREDIT
          //    - Increase gift delivery counter 

          _sql_query = "UPDATE   GIFTS                                          " +
                       "   SET   GI_DELIVERY_COUNTER = GI_DELIVERY_COUNTER + @pNumUnits  " +
                       " WHERE   GI_GIFT_ID = @GiftId                           ";
        }
        else
        {
          _sql_query = "";
        }


        if (!String.IsNullOrEmpty(_sql_query))
        {
          using (_sql_command_gift = new SqlCommand(_sql_query, SqlTrx.Connection, SqlTrx))
          {
            _sql_command_gift.Parameters.Add("@GiftId", SqlDbType.BigInt).Value = m_gift.GiftId;
            _sql_command_gift.Parameters.Add("@pNumUnits", SqlDbType.Int).Value = NumUnits;


            _num_rows_updated = _sql_command_gift.ExecuteNonQuery();
          }

          if (_num_rows_updated != 1)
          {
            string _message;

            _message = "GiftInstance.Request. GIFT's stock or counters NOT updated.";

            throw new Exception(_message);
          }
        }

        //    - Create the gift instance
        this.m_op_request_id = _operation_id;

        if (!DB_GiftInstanceRequest(SqlTrx))
        {
          string _message;

          _message = "GiftInstance.Request. DB_GiftInstanceRequest. Operation: " + _operation_code.ToString()
                                                            + ", Operation Id: " + _operation_id.ToString()
                                                                 + ", Card Id: " + m_card.AccountId.ToString()
                                                                    + ", Gift: " + m_gift.GiftId.ToString() + "-" + m_gift.GiftName
                                                                  + ", Points: " + SpentPoints.ToString(Gift.FORMAT_POINTS)
                                                                                 ;

          throw new Exception(_message);
        }

        _sql_query = UpdateCustomerBucketFromDraw(SqlTrx, _sql_query, out _sql_command_acc, out _num_rows_updated, out _aux_currency1);

        if (_num_rows_updated != 2) // CUSTOMER_BUCKETS and ACCOUNTS
        {
          string _message;

          _message = "GiftInstance.Request. ACCOUNT points + NRC not updated. Points: " + _aux_currency1.ToString();

          throw new Exception(_message);
        }

        //    - Update the Bucket History
        if (!BucketsUpdate.UpdateCustomerBucket(m_card.AccountId, Buckets.BucketId.RedemptionPoints, null, SpentPoints, Buckets.BucketOperation.SUB_GENERATED, false, true, SqlTrx))
        {
          return false;
        }

        //    - Generate the gift voucher
        Barcode _voucher_number;

        // Get site Id
        if (CardData.ConfiguredSiteId == -1)
        {
          Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out CardData.ConfiguredSiteId);
        }

        _voucher_number = Barcode.Create(BarcodeType.Gift, InstanceId, CardData.ConfiguredSiteId);

        _gift_expiration_days = Convert.ToInt32(WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "GiftExpirationDays"));

        _voucher_list_aux = VoucherBuilder.GiftRequest(_operation_id,
                                                       m_card.VoucherAccountInfo(_include_card_document), // Account Info 
                                                       m_card.PlayerTracking.TruncatedPoints,  // Initial Points Balance
                                                       SpentPoints,                            // Spent Points
                                                       m_gift.Type,                            // Gift Type
                                                       m_gift.GiftName,                        // Gift Name
                                                       _voucher_number.ExternalCode,           // Gift Number
                                                       NotRedeemableCredits,                   // Not redeemable credits
                                                       NumUnits,                               // Number of gift units
                                                       WGDB.Now.AddDays(_gift_expiration_days),
                                                       PrintMode.Print,
                                                       SqlTrx);

        if (_idata.NoMoney)
        {
          // Do nothing
        }
        else
        {
          if (_credit_awarded && _promotions != null && _promotions.Rows.Count > 0)
          {
            ArrayList _voucher_list_promos;

            if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier.Voucher", "VoucherOnPromotionAwarded"), out _int_value))
            {
              _int_value = 0;
            }

            if (_int_value > 0)
            {
              TYPE_SPLITS _splits;

              if (!Split.ReadSplitParameters(out _splits))
              {
                return false;
              }

              if (!VoucherBuilder.PromoLost(m_card.VoucherAccountInfo(),
                                            m_card.AccountId,
                                            _splits,
                                            (_int_value == 1),
                                            _promotions,
                                            true,
                                            _operation_id,
                                            PrintMode.Print,
                                            SqlTrx,
                                            out _voucher_list_promos))
              {
                return false;
              }

              foreach (Voucher _voucher in _voucher_list_promos)
              {
                _voucher_list_aux.Add(_voucher);
              }
            }
          }
        }

        if (!AccountMovementsTable.Save(SqlTrx))
        {
          Log.Error("GiftInstance.Request. AccountsMovements.Save. AccountId: " + m_card.AccountId.ToString()
                                                                   + ", Gift: " + m_gift.GiftId.ToString() + "-" + m_gift.GiftName);

          return false;
        }

        if (!CashierMovementsTable.Save(SqlTrx))
        {
          Log.Error("GiftInstance.Request. CashierMovements.Save. AccountId: " + m_card.AccountId.ToString()
                                                                  + ", Gift: " + m_gift.GiftId.ToString() + "-" + m_gift.GiftName);

          return false;
        }

        foreach (Voucher _voucher in _voucher_list_aux)
        {
          VoucherList.Add(_voucher);
        }

        if (m_gift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT ||
            m_gift.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
        {
          if (_idata.NoMoney)
          {
            foreach (Voucher _voucher in _odata.VoucherList)
            {
              VoucherList.Add(_voucher);
            }
          }
        }

        if (_create_ticket)
        {

          if (!WSI.Common.Cashier.Common_CreateTicket(_params_ticket_operation, true, out _tito_ticket, SqlTrx))
          {
            Log.Error("AccountAddCredit: Common_createTicket. Error creating promotional ticket.");

            return false;
          }

          TitoTicket = _tito_ticket;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CommonRequest

    private string UpdateCustomerBucketFromDraw(SqlTransaction SqlTrx, String _sql_query, out SqlCommand _sql_command_acc, out Int32 _num_rows_updated, out Currency _aux_currency1)
    {
      //Update the customer bucket
      _sql_query = " UPDATE   CUSTOMER_BUCKET                       " +
                   "    SET   CBU_VALUE = CBU_VALUE - @PointsAmount " +
                   "  WHERE   CBU_BUCKET_ID = @pBucketId            " +
                   "    AND   CBU_CUSTOMER_ID = @AccountId          " +
                   "    AND   CBU_VALUE >= @PointsAmount            " +
                   "                                                " +
                   "IF   @@ROWCOUNT = 1                             " +
                   "BEGIN                                           " +
                   " UPDATE   ACCOUNTS                              " +
                   "    SET   AC_LAST_ACTIVITY = AC_LAST_ACTIVITY   " +
                   "  WHERE   AC_ACCOUNT_ID = @AccountId            " +
                   "END                                             ";

      _aux_currency1 = SpentPoints;

      using (_sql_command_acc = new SqlCommand(_sql_query, SqlTrx.Connection, SqlTrx))
      {
        _sql_command_acc.Parameters.Add("@PointsAmount", SqlDbType.Money).Value = _aux_currency1.SqlMoney;
        _sql_command_acc.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = m_card.AccountId;
        _sql_command_acc.Parameters.Add("@pBucketId", SqlDbType.Int).Value = (Int32)Buckets.BucketId.RedemptionPoints;

        _num_rows_updated = _sql_command_acc.ExecuteNonQuery();
      }
      return _sql_query;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Function to manage all the messages related to Gift Instance
    //
    //  PARAMS :
    //      - INPUT :
    //          - MsgType
    //
    //      - OUTPUT :
    //          - Message
    //          - Caption
    //          - MsgIcon
    //
    // RETURNS :
    // 
    //   NOTES :

    public bool GetMessage(GIFT_INSTANCE_MSG MsgType, out String Message, out String Caption, out MessageBoxIcon MsgIcon)
    {
      string[] _message_params = { "", "", "", "", "", "" };
      string _message_id;
      bool _result;
      string _card_name = "";
      PrinterStatus _status;

      _result = true;

      Message = "";
      Caption = "";
      MsgIcon = new MessageBoxIcon();

      if (m_card != null)
      {
        if (m_card.PlayerTracking.HolderName != "")
        {
          _card_name = m_card.AccountId.ToString() + " - " + m_card.PlayerTracking.HolderName;
        }
        else
        {
          _card_name = m_card.AccountId.ToString();
        }
      }

      switch (MsgType)
      {
        case GIFT_INSTANCE_MSG.GIFT_EXPIRED:
        case GIFT_INSTANCE_MSG.DELIVERY_ERROR:
        case GIFT_INSTANCE_MSG.NOT_ENOUGH_POINTS_TO_CONVERT_NRC:
        case GIFT_INSTANCE_MSG.STATUS_ERROR:
        case GIFT_INSTANCE_MSG.NOT_CONNECTED_TO_MULTISITE:
          _message_params[0] = _card_name;
          _message_params[1] = m_gift.GiftName;

          switch (MsgType)
          {
            case GIFT_INSTANCE_MSG.NOT_ENOUGH_POINTS_TO_CONVERT_NRC:
              // No se puede realizar la petici�n del regalo @p1 con cargo a los puntos de la cuenta @p0:    - Los puntos a convertir no son suficientes para solicitar el regalo."
              _message_id = "STR_FRM_ACCOUNT_POINTS_016";
              break;

            case GIFT_INSTANCE_MSG.GIFT_EXPIRED:
              // "No se puede realizar la entrega del regalo @p1 de la cuenta @p0:    - El tiempo para canjearlo ha terminado."
              _message_id = "STR_UC_GIFT_DELIVERY_018";
              break;

            case GIFT_INSTANCE_MSG.STATUS_ERROR:
              // No se puede realizar la entrega del regalo @p1 de la cuenta @p0:\r\n\r\n    - El regalo est� cancelado.\r\n\r\n
              _message_id = "STR_UC_GIFT_DELIVERY_024";
              break;

            case GIFT_INSTANCE_MSG.NOT_CONNECTED_TO_MULTISITE:
              _message_id = "STR_UC_GIFT_DELIVERY_025";
              break;
            case GIFT_INSTANCE_MSG.DELIVERY_ERROR:
            default:
              // "No se puede realizar la entrega del regalo @p1 de la cuenta @p0:    - Se ha producido un error interno."
              _message_id = "STR_UC_GIFT_DELIVERY_017";
              break;
          }

          Message = Resource.String(_message_id, _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_APP_GEN_MSG_WARNING");
          break;

        case GIFT_INSTANCE_MSG.GIFT_NOT_FOUND:
          // "El regalo no se encuentra en el sistema."
          _message_id = "STR_UC_GIFT_DELIVERY_009";

          Message = Resource.String(_message_id, _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_APP_GEN_MSG_WARNING");
          break;

        case GIFT_INSTANCE_MSG.DRAW_NOT_AVAILABLE:
          // "No se puede solicitar el regalo @p0 ya que no hay sorteos disponibles en el sistema."
          _message_id = "STR_UC_GIFT_DELIVERY_020";
          _message_params[0] = m_gift.GiftName;

          Message = Resource.String(_message_id, _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_APP_GEN_MSG_WARNING");
          break;

        case GIFT_INSTANCE_MSG.DRAW_NUMBERS_NOT_AVAILABLE_ACCOUNT:
          // La cuenta @p0 no puede solicitar m�s n�meros del sorteo seleccionado en el d�a de hoy.
          _message_id = "STR_UC_GIFT_DELIVERY_021";
          _message_params[0] = _card_name;

          Message = Resource.String(_message_id, _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_APP_GEN_MSG_WARNING");
          break;

        case GIFT_INSTANCE_MSG.DRAW_NUMBERS_NOT_AVAILABLE:
          // No se pueden solicitar m�s n�meros del sorteo seleccionado. Ya se han asignado todos los n�meros.
          _message_id = "STR_UC_GIFT_DELIVERY_022";

          Message = Resource.String(_message_id, _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_APP_GEN_MSG_WARNING");
          break;

        case GIFT_INSTANCE_MSG.REQUEST_ERROR:
          _message_params[0] = _card_name;
          _message_params[1] = m_gift.GiftName;

          // No se puede realizar la petici�n del regalo @p1 a cargo de los puntos la cuenta @p0\r\n\r\n</value> </data>
          Message = Resource.String("STR_FRM_ACCOUNT_POINTS_017", _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Error;
          Caption = Resource.String("STR_APP_GEN_MSG_ERROR");

          Log.Error("GiftInstance.Register: " + Message);
          break;

        case GIFT_INSTANCE_MSG.DB_ERROR:
          _message_params[0] = m_gift.GiftName;

          // Se ha producido un error inesperado, no se puede realizar la operaci�n para el regalo @p0.
          Message = Resource.String("STR_CANCEL_GIFT_001", _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_APP_GEN_MSG_WARNING");

          break;

        case GIFT_INSTANCE_MSG.CANCEL_CARD_ERROR:
          _message_params[0] = m_gift.GiftName;

          // No se puede realizar la cancelaci�n del regalo @p0: - La cuenta esta en uso.
          Message = Resource.String("STR_CANCEL_GIFT_002", _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_FRM_ACCOUNT_POINTS_027");

          break;

        case GIFT_INSTANCE_MSG.CANCEL_GIFT_ERROR:
          _message_params[0] = m_gift.GiftName;

          // No se puede realizar la cancelaci�n del regalo @p0: - El tipo de regalo no es el correcto.
          Message = Resource.String("STR_CANCEL_GIFT_003", _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_FRM_ACCOUNT_POINTS_027");

          break;

        case GIFT_INSTANCE_MSG.CANCEL_STATUS_ERROR:
          _message_params[0] = m_gift.GiftName;

          // No se puede realizar la cancelaci�n del regalo @p0: - El estado no es el correcto.
          Message = Resource.String("STR_CANCEL_GIFT_004", _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_FRM_ACCOUNT_POINTS_027");

          break;

        case GIFT_INSTANCE_MSG.CANCEL_PROMOTION_NOT_FOUND:
          _message_params[0] = m_gift.GiftName;

          // No se puede realizar la cancelaci�n del regalo @p0: - No hay suficiente cr�dito en la cuenta.
          Message = Resource.String("STR_CANCEL_GIFT_006", _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_FRM_ACCOUNT_POINTS_027");

          break;

        case GIFT_INSTANCE_MSG.REDEEM_LIMIT_EXCEEDED:
          _message_params[0] = m_gift.GiftName;
          Message = Resource.String("STR_UC_GIFT_DELIVERY_026", _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_APP_GEN_MSG_WARNING");

          break;

        case GIFT_INSTANCE_MSG.CANCEL_NOT_ENOUGH_CREDIT:
          _message_params[0] = m_gift.GiftName;

          // No se puede realizar la cancelaci�n del regalo @p0: - No hay suficiente cr�dito en la cuenta.
          Message = Resource.String("STR_CANCEL_GIFT_006", _message_params);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_APP_GEN_MSG_WARNING");

          break;

        case GIFT_INSTANCE_MSG.PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS:
          // El jugador no puede solicitar regalos.
          Message = Resource.String("STR_FRM_ACCOUNT_POINTS_REDEEM_NOT_ALLOWED");

          MsgIcon = MessageBoxIcon.Warning;
          Caption = Resource.String("STR_APP_GEN_MSG_WARNING");

          break;

        case GIFT_INSTANCE_MSG.AML_NO_IDENTIFICATION_LIMIT_EXCEEDED:

          // No se pueden cambiar los puntos por cr�dito redimible a causa de la ley antilavado.
          Message = Resource.String("STR_FRM_ACCOUNT_POINTS_032");
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Error;
          Caption = Resource.String("STR_APP_GEN_MSG_ERROR");

          break;

        case GIFT_INSTANCE_MSG.RECHARGE_LIMIT_EXCEEDED:

          // No se puede canjear el regalo. El monto redimible supera el m�ximo permitido.
          Message = Resource.String("STR_FRM_ACCOUNT_POINTS_033");
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Error;
          Caption = Resource.String("STR_APP_GEN_MSG_ERROR");

          break;

        case GIFT_INSTANCE_MSG.PRINTER_PRINT_FAIL:
        case GIFT_INSTANCE_MSG.PRINTER_IS_NOT_READY:

          TitoPrinter.GetStatus(out _status, out Message);

          Message = TitoPrinter.GetMsgError(_status);
          MsgIcon = MessageBoxIcon.Error;
          Caption = Resource.String("STR_APP_GEN_MSG_ERROR");

          if (MsgType == GIFT_INSTANCE_MSG.PRINTER_PRINT_FAIL)
          {
            Message += "\r\n" + Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_CANCEL_OPERATION");
          }
          break;
        case GIFT_INSTANCE_MSG.TITO_TICKET_NOT_VALID:

          // Gift @p0 cannot be canceled: \r\n\r\n    - The associated TITO ticket is no longer valid.
          Message = Resource.String("STR_CANCEL_GIFT_007", m_gift.GiftName);
          Message = Message.Replace("\\r\\n", "\r\n");

          MsgIcon = MessageBoxIcon.Error;
          Caption = Resource.String("STR_APP_GEN_MSG_ERROR");

          break;

        default:
          _result = false;
          break;
      }

      return _result;
    } // Message

    // PURPOSE: Process to cancel gift instances
    //
    //  PARAMS:
    //     - INPUT:
    //           - InstanceToCancel:
    //           - CashierSession:
    //           - VoucherList:
    //           - InstanceMsg:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if is correct the cancellation

    public static Boolean CancelInstance(GiftInstance InstanceToCancel,
                                         CashierSessionInfo CashierSession,
                                         out ArrayList VoucherList,
                                         out GIFT_INSTANCE_MSG InstanceMsg,
                                         SqlTransaction Trx)
    {

      // - Check gift / account / instance
      //        * The account isn't logged or locked
      //        * The type of gift isn't draw
      //        * Time to cancel
      //        * The state of the instance
      // - Create account movement
      // - Cancel promotion (if type of GIFT is NR or RE)
      //        * Check - Have enough credit the promotion?
      // - Update instance status
      // - Update gift counters
      // - Check the credit of account
      // - Update account points

      StringBuilder _sb;

      Gift _gift;
      CardData _card_data;
      AccountMovementsTable _ac_mov_table;
      CashierMovementsTable _cm_mov_table;
      DataTable _promotions;
      OperationCode _operation_code;
      Int64 _operation_data;
      Int64 _promotion_id;
      Int64 _operation_id;
      Decimal _spent_points;
      Currency _credits_to_cancel;
      Decimal _won_lock;
      Boolean _cancel_credit;

      Int64 _play_session_id;
      MultiPromos.AccountBalance _ini_bal;
      MultiPromos.AccountBalance _fin_bal;
      MultiPromos.PromoBalance _promo_canceled;
      Decimal _fin_points;
      Decimal _ini_points;

      ArrayList _voucher_list_aux;
      Int32 _int_value;
      Int32 _cancelable_interval; // minutes

      _cancelable_interval = GeneralParam.GetInt32("Gifts", "TimeToCancel", 0);

      InstanceMsg = GIFT_INSTANCE_MSG.UNKNOWN;
      VoucherList = new ArrayList();
      _gift = InstanceToCancel.m_gift;

      _ac_mov_table = new AccountMovementsTable(CashierSession);
      _cm_mov_table = new CashierMovementsTable(CashierSession);

      _operation_code = OperationCode.CANCEL_GIFT_INSTANCE;
      _operation_data = 0;
      _promotion_id = 0;
      _operation_id = 0;
      _won_lock = 0;
      _cancel_credit = false;
      _voucher_list_aux = null;
      _promotions = null;

      _spent_points = InstanceToCancel.m_spent_points;
      _credits_to_cancel = InstanceToCancel.m_not_redeemable_credits;

      _sb = new StringBuilder();

      switch (_gift.Type)
      {
        case GIFT_TYPE.OBJECT:
        case GIFT_TYPE.DRAW_NUMBERS:
        case GIFT_TYPE.SERVICES:
          break;
        case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
        case GIFT_TYPE.REDEEMABLE_CREDIT:
          _cancel_credit = true;
          break;
        case GIFT_TYPE.UNKNOWN:
        default:
          InstanceMsg = GIFT_INSTANCE_MSG.GIFT_NOT_FOUND;

          return false;
      }

      try
      {

        // Lock account
        if (!MultiPromos.Trx_UpdateAccountBalance(InstanceToCancel.m_account_id,
                                                  MultiPromos.AccountBalance.Zero,
                                                  out _ini_bal, out _ini_points, out _play_session_id, Trx))
        {
          InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
          Log.Error("GiftRequest.CancelInstance: Trx_UpdateAccountBalance AccountId: " + InstanceToCancel.m_account_id.ToString());

          return false;
        }

        // Get card data
        _card_data = new CardData();
        if (!CardData.DB_CardGetAllData(InstanceToCancel.m_account_id, _card_data))
        {
          InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;

          return false;
        }

        //
        // Check account
        //

        //    - Card must not be in use
        if (_card_data.IsLocked || _card_data.IsLoggedIn)
        {
          // The account is in use
          InstanceMsg = GIFT_INSTANCE_MSG.CANCEL_CARD_ERROR;

          return false;
        }

        //
        // Check gift
        //

        if (_gift.Type == GIFT_TYPE.DRAW_NUMBERS)
        {
          InstanceMsg = GIFT_INSTANCE_MSG.CANCEL_GIFT_ERROR;

          return false;
        }

        //
        // Time to cancel
        //
        if (WGDB.Now > InstanceToCancel.RequestDateTime.AddMinutes(_cancelable_interval))
        {
          InstanceMsg = GIFT_INSTANCE_MSG.CANCEL_STATUS_ERROR;

          return false;
        }


        //
        // Check instance
        //

        if (!(InstanceToCancel.Status == GIFT_REQUEST_STATUS.PENDING ||
             (InstanceToCancel.Status == GIFT_REQUEST_STATUS.DELIVERED && (_gift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT || _gift.Type == GIFT_TYPE.REDEEMABLE_CREDIT))))
        {
          InstanceMsg = GIFT_INSTANCE_MSG.CANCEL_STATUS_ERROR;

          return false;
        }

        //
        // Create account movement
        //

        if (_cancel_credit)
        {
          if (_gift.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
          {
            _won_lock = -0.0001m;
          }
        }

        if (!Operations.DB_InsertOperation(_operation_code,
                                           _card_data.AccountId,            // AccountId
                                           CashierSession.CashierSessionId, // CashierSessionId
                                           0,                               // MbAccountId
                                           _spent_points,                   // Amount = POINTS
                                           _gift.PromotionId,               // PromotionId
                                           _credits_to_cancel,              // NonRedeemable/Redeemable
                                           _won_lock,                       // NonRedeemableWonLock
                                           _operation_data,                 // OperationData 
                                           0,
                                           string.Empty,
                                           out _operation_id,               // OperationId
                                           Trx))
        {
          InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
          Log.Error("GiftRequest.CancelInstance: DB_InsertOperation AccountId: " + _card_data.AccountId.ToString());

          return false;
        }

        //    - Card movement: Cancel points subtracted
        _ac_mov_table.Add(_operation_id, _card_data.AccountId, MovementType.CancelGiftInstance,
                          (Decimal)_card_data.PlayerTracking.CurrentPoints, 0, _spent_points,
                          (Decimal)_card_data.PlayerTracking.CurrentPoints + _spent_points);


        //
        // Cancel promotion (if type of GIFT is NR or RE)
        //

        if (_cancel_credit)
        {
          if (Utils.IsTitoMode())
          {
            if (!CancelGiftTicketPromo(InstanceToCancel, CashierSession.TerminalId))
            {
              InstanceMsg = GIFT_INSTANCE_MSG.TITO_TICKET_NOT_VALID;
              return false;
            }
          }

          DataRow _pm_dr;
          String _acp_promo_name;
          Decimal _acp_balance;
          Int32 _acp_credit_type;
          Decimal _acp_played;
          Int32 _acp_promo_type;

          //
          // Check if exists the unique id
          //

          _sb.Length = 0;
          _sb.AppendLine("  SELECT   ACP_PROMO_NAME, ACP_BALANCE, ACP_CREDIT_TYPE, ACP_PLAYED, ACP_PROMO_TYPE "); // Used for print promotion lost
          _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
          _sb.AppendLine("   WHERE   ACP_UNIQUE_ID  = @pUniqueId  ");
          if (TITO.Utils.IsTitoMode())
          {
            _sb.AppendLine("     AND   ACP_STATUS     IN (@pStatusAwarded, @pStatusActive, @pStatusRedeemed, @pStatusCancelledByPlayer) ");
          }
          else
          {
            _sb.AppendLine("     AND   ACP_STATUS     IN (@pStatusAwarded, @pStatusActive, @pStatusRedeemed) ");
          }
          _sb.AppendLine("     AND   ACP_PLAY_SESSION_ID IS NULL  ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = InstanceToCancel.m_promotion_unique_id;
            _cmd.Parameters.Add("@pStatusAwarded", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.AWARDED;
            _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
            _cmd.Parameters.Add("@pStatusRedeemed", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.REDEEMED;
            _cmd.Parameters.Add("@pStatusCancelledByPlayer", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.CANCELLED_BY_PLAYER;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                InstanceMsg = GIFT_INSTANCE_MSG.CANCEL_PROMOTION_NOT_FOUND;

                return false;
              }
              _acp_promo_name = _reader.GetString(0);
              _acp_balance = _reader.GetDecimal(1);
              _acp_credit_type = _reader.GetInt32(2);
              _acp_played = _reader.GetDecimal(3);
              _acp_promo_type = _reader.GetInt32(4);
            }

          }

          if (_acp_played > 0 || _acp_balance != _credits_to_cancel)
          {
            InstanceMsg = GIFT_INSTANCE_MSG.CANCEL_NOT_ENOUGH_CREDIT;

            return false;
          }

          _promotions = AccountPromotion.CreateAccountPromotionTable();
          _pm_dr = _promotions.NewRow();
          _pm_dr["ACP_UNIQUE_ID"] = InstanceToCancel.m_promotion_unique_id;
          _pm_dr["ACP_ACCOUNT_ID"] = InstanceToCancel.m_account_id;
          _pm_dr["ACP_PROMO_ID"] = _promotion_id;
          _pm_dr["ACP_PROMO_NAME"] = _acp_promo_name;
          _pm_dr["ACP_PROMO_TYPE"] = _acp_promo_type;
          _pm_dr["ACP_BALANCE"] = _acp_balance;
          _pm_dr["ACP_CREDIT_TYPE"] = _acp_credit_type;

          _promotions.Rows.Add(_pm_dr);

          if (!AccountPromotion.Trx_CancelByCashierPromotion(_pm_dr, _ac_mov_table, _cm_mov_table, null, out _promo_canceled, Trx))
          {
            InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
            Log.Error("GiftRequest.CancelInstance: Trx_CancelByCashierPromotion AccountId: " + _card_data.AccountId.ToString());

            return false;
          }

          //
          // Check - Have enough credit the promotion?
          //
          if (!TITO.Utils.IsTitoMode())
          {
            switch (_gift.Type)
            {
              case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
                if (_promo_canceled.Balance.PromoNotRedeemable != _credits_to_cancel)
                {
                  InstanceMsg = GIFT_INSTANCE_MSG.CANCEL_NOT_ENOUGH_CREDIT;

                  return false;
                }
                break;

              case GIFT_TYPE.REDEEMABLE_CREDIT:
                if (_promo_canceled.Balance.PromoRedeemable != _credits_to_cancel)
                {
                  InstanceMsg = GIFT_INSTANCE_MSG.CANCEL_NOT_ENOUGH_CREDIT;

                  return false;
                }
                break;

              default:
                InstanceMsg = GIFT_INSTANCE_MSG.GIFT_NOT_FOUND;

                return false;
            }

            if (!Operations.Trx_SetOperationBalance(_operation_id, _promo_canceled.Balance, Trx))
            {
              InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
              Log.Error("GiftRequest.CancelInstance: Trx_SetOperationBalance AccountId: " + _card_data.AccountId.ToString());

              return false;
            }
          }


        }

        //
        // Update instance status
        //

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   GIFT_INSTANCES ");
        _sb.AppendLine("    SET   GIN_REQUEST_STATUS   = @pStatusCanceled ");
        _sb.AppendLine("  WHERE   GIN_GIFT_INSTANCE_ID = @pInstanceId ");
        _sb.AppendLine("    AND   GIN_GIFT_TYPE        = @pType ");
        _sb.AppendLine("    AND   GIN_EXPIRATION       > GETDATE() ");
        _sb.AppendLine("    AND   ( GIN_REQUEST_STATUS  = @pStatusPending ");
        _sb.AppendLine("     OR   ( GIN_REQUEST_STATUS  = @pStatusDelivered  AND GIN_GIFT_TYPE IN (@pGiftTypeNR,@pGiftTypeRE) )) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {

          _cmd.Parameters.Add("@pInstanceId", SqlDbType.BigInt).Value = InstanceToCancel.m_gift_instance_id;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = _gift.Type;
          _cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.PENDING;
          _cmd.Parameters.Add("@pStatusDelivered", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.DELIVERED;
          _cmd.Parameters.Add("@pStatusCanceled", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.CANCELED;
          _cmd.Parameters.Add("@pGiftTypeNR", SqlDbType.Int).Value = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
          _cmd.Parameters.Add("@pGiftTypeRE", SqlDbType.Int).Value = GIFT_TYPE.REDEEMABLE_CREDIT;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
            Log.Error("GiftRequest.CancelInstance: Update GiftInstance status, Gift: " + _gift.GiftId.ToString() + "-" + _gift.GiftName);


            return false;
          }

        }

        //
        // Update gift counters
        //

        //    - Gift Counters
        //        - Decrease gift request counter ( Gift = OBJECT )
        //        - Decrease gift request counter (without stock control) ( Gift = SERVICES )
        //        - Decrease gift delivery counter ( Gift = NR CREDIT | REDEEM CREDIT )

        _sb.Length = 0;
        switch (_gift.Type)
        {
          case GIFT_TYPE.OBJECT:
            _sb.AppendLine(" UPDATE   GIFTS ");
            _sb.AppendLine("    SET   GI_REQUEST_COUNTER = CASE WHEN GI_REQUEST_COUNTER > @pNumUnits THEN GI_REQUEST_COUNTER - @pNumUnits ELSE 0 END  ");
            _sb.AppendLine("  WHERE   GI_GIFT_ID = @pGiftId ");

            break;
          case GIFT_TYPE.REDEEMABLE_CREDIT:
          case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
            _sb.AppendLine(" UPDATE   GIFTS ");
            _sb.AppendLine("    SET   GI_DELIVERY_COUNTER = CASE WHEN GI_DELIVERY_COUNTER > @pNumUnits THEN GI_DELIVERY_COUNTER - @pNumUnits ELSE 0 END  ");
            _sb.AppendLine("  WHERE   GI_GIFT_ID = @pGiftId ");

            break;
          case GIFT_TYPE.SERVICES:
            _sb.AppendLine(" UPDATE   GIFTS ");
            _sb.AppendLine("    SET   GI_REQUEST_COUNTER = CASE WHEN GI_REQUEST_COUNTER > @pNumUnits THEN GI_REQUEST_COUNTER - @pNumUnits ELSE 0 END  ");
            _sb.AppendLine("  WHERE   GI_GIFT_ID = @pGiftId ");

            break;
          case GIFT_TYPE.DRAW_NUMBERS:
          case GIFT_TYPE.UNKNOWN:
          default:

            return false;
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {

          _cmd.Parameters.Add("@pGiftId", SqlDbType.BigInt).Value = _gift.GiftId;
          _cmd.Parameters.Add("@pNumUnits", SqlDbType.Int).Value = InstanceToCancel.NumUnits;


          if (_cmd.ExecuteNonQuery() != 1)
          {
            InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
            Log.Error("GiftRequest.CancelInstance: Update gift counters: Gift: " + _gift.GiftId.ToString() + "-" + _gift.GiftName);

            return false;
          }
        }

        //
        // Update account points
        //

        if (!MultiPromos.Trx_UpdateAccountBalance(InstanceToCancel.m_account_id,
                                                  MultiPromos.AccountBalance.Zero,
                                                  InstanceToCancel.SpentPoints,
                                                  out _fin_bal, out _fin_points, Trx))
        {
          InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
          Log.Error("GiftRequest.CancelInstance: Trx_UpdateAccountBalance AccountId: " + _card_data.AccountId.ToString()
                                                                          + ", Gift: " + _gift.GiftId.ToString() + "-" + _gift.GiftName);

          return false;
        }

        //
        // Generate vouchers
        //
        _voucher_list_aux = VoucherBuilder.GiftCancellation(_operation_id,
                                                            _card_data.VoucherAccountInfo(),
                                                            _card_data.PlayerTracking.TruncatedPoints,
                                                            InstanceToCancel.SpentPoints,
                                                            _gift.Type,
                                                            "",                 // Gift Number
                                                            _gift.GiftName,     // Gift Name
                                                            _credits_to_cancel,
                                                            InstanceToCancel.NumUnits,
                                                            PrintMode.Print,
                                                            Trx);

        if (_cancel_credit && _promotions != null && _promotions.Rows.Count > 0)
        {

          ArrayList _voucher_list_promos;

          if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier.Voucher", "VoucherOnPromotionCancelled"), out _int_value))
          {
            _int_value = 0;
          }

          if (_int_value > 0)
          {
            TYPE_SPLITS _splits;

            if (!Split.ReadSplitParameters(out _splits))
            {
              return false;
            }

            if (!VoucherBuilder.PromoLost(_card_data.VoucherAccountInfo(),
                                          _card_data.AccountId,
                                          _splits,
                                          (_int_value == 1),
                                          _promotions,
                                          false,
                                          _operation_id,
                                          PrintMode.Print,
                                          Trx,
                                          out _voucher_list_promos))
            {
              Log.Error("GiftRequest.CancelInstance: VoucherBuilder.PromoLost AccountId: " + _card_data.AccountId.ToString()
                                                                              + ", Gift: " + _gift.GiftId.ToString() + "-" + _gift.GiftName);

              return false;
            }

            foreach (Voucher _voucher in _voucher_list_promos)
            {
              _voucher_list_aux.Add(_voucher);
            }
          }

        }

        //
        // Save movement
        //

        if (!_ac_mov_table.Save(Trx))
        {
          InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
          Log.Error("GiftRequest.CancelInstance: AccountsMovements.Save AccountId: " + _card_data.AccountId.ToString()
                                                                        + ", Gift: " + _gift.GiftId.ToString() + "-" + _gift.GiftName);

          return false;
        }

        if (!_cm_mov_table.Save(Trx))
        {
          InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
          Log.Error("GiftRequest.CancelInstance: CashierMovements.Save. AccountId: " + _card_data.AccountId.ToString()
                                                                        + ", Gift: " + _gift.GiftId.ToString() + "-" + _gift.GiftName);

          return false;
        }

        //
        // 
        //

        VoucherList = _voucher_list_aux;

      }
      catch (Exception _ex)
      {
        InstanceMsg = GIFT_INSTANCE_MSG.DB_ERROR;
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get cancellable gift 
    //
    //  PARAMS :
    //      - INPUT :
    //          - MsgType
    //
    //      - OUTPUT :
    //          - Message
    //          - Caption
    //          - MsgIcon
    //
    // RETURNS :
    //      - DataTable: 
    //   NOTES :

    public static DataTable GetCancellableGift(Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _dt;
      Int32 _cancelable_interval; // minutes

      _dt = new DataTable();

      _cancelable_interval = GeneralParam.GetInt32("Gifts", "TimeToCancel", 0);

      try
      {

        _sb = new StringBuilder();

        _sb.AppendLine("  SELECT   TOP 3 ");
        _sb.AppendLine("           GIN_GIFT_ID ");
        _sb.AppendLine("         , GIN_GIFT_TYPE ");
        _sb.AppendLine("         , GIN_GIFT_NAME ");
        _sb.AppendLine("         , GIN_SPENT_POINTS ");
        _sb.AppendLine("         , GIN_CONVERSION_TO_NRC ");
        _sb.AppendLine("         , 0 GI_DRAW_ID ");
        _sb.AppendLine("         , GIN_GIFT_INSTANCE_ID ");
        _sb.AppendLine("         , 0 GI_ACCOUNT_DAILY_LIMIT  ");
        _sb.AppendLine("         , 0 GI_ACCOUNT_MONTHLY_LIMIT");

        _sb.AppendLine("         , 0 GI_GLOBAL_DAILY_LIMIT   ");
        _sb.AppendLine("         , 0 GI_GLOBAL_MONTHLY_LIMIT ");
        _sb.AppendLine("         , GIFTS.GI_DESCRIPTION ");
        _sb.AppendLine("         , GIFTS.GI_SMALL_RESOURCE_ID ");
        _sb.AppendLine("         , GIFTS.GI_LARGE_RESOURCE_ID ");
        _sb.AppendLine("         , GIFT_INSTANCES.GIN_REQUESTED ");
        _sb.AppendLine("         , GIFT_INSTANCES.GIN_NUM_ITEMS ");
        _sb.AppendLine("    FROM   GIFT_INSTANCES  ");
        _sb.AppendLine("    INNER JOIN   GIFTS ON  GIFT_INSTANCES.GIN_GIFT_ID = GIFTS.GI_GIFT_ID ");
        _sb.AppendLine("   WHERE   GIN_EXPIRATION > GETDATE() ");
        _sb.AppendLine("     AND  ( ( GIN_REQUEST_STATUS  = @pStatusPending AND GIN_GIFT_TYPE IN (@pGiftTypeObject,@pGiftTypeServices) )");
        _sb.AppendLine("            OR  ( GIN_REQUEST_STATUS  = @pStatusDelivered  AND GIN_GIFT_TYPE IN (@pGiftTypeNRC,@pGiftTypeRC) ");

        //  //LEM 11-12-2013: Redeemeble and Not Redeemeble Credit are not cancelable if ticket is not valid
        if (Utils.IsTitoMode())
        {
          _sb.AppendLine("               AND (SELECT TI_STATUS  FROM TICKETS WITH (INDEX (IX_ti_transaction_id_terminal_id_validation_number)) WHERE TI_TRANSACTION_ID = GIN_OPER_REQUEST_ID) = 0   ");
        }
        else
        {
          _sb.AppendLine("               AND GIN_DATA_01 > 0     ");

        }

        _sb.AppendLine("          )");
        _sb.AppendLine(")    AND   ( GIN_ACCOUNT_ID = @pAccountId ) ");
        _sb.AppendLine("     AND   GIN_REQUESTED  >= DATEADD(MI, -(@pCancelableMinutes), GETDATE()) ");
        _sb.AppendLine("ORDER BY   GIN_REQUESTED DESC ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = (Int32)GIFT_REQUEST_STATUS.PENDING;
          _cmd.Parameters.Add("@pStatusDelivered", SqlDbType.Int).Value = (Int32)GIFT_REQUEST_STATUS.DELIVERED;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pGiftTypeObject", SqlDbType.Int).Value = GIFT_TYPE.OBJECT;
          _cmd.Parameters.Add("@pGiftTypeNRC", SqlDbType.Int).Value = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
          _cmd.Parameters.Add("@pGiftTypeRC", SqlDbType.Int).Value = GIFT_TYPE.REDEEMABLE_CREDIT;
          _cmd.Parameters.Add("@pGiftTypeServices", SqlDbType.Int).Value = GIFT_TYPE.SERVICES;
          _cmd.Parameters.Add("@pCancelableMinutes", SqlDbType.Int).Value = _cancelable_interval;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            _dt.Load(_reader);
          }

        }

      }
      catch
      {
        _dt = new DataTable();
      }

      return _dt;

    } // GetCancellableGift

    //------------------------------------------------------------------------------
    // PURPOSE : Get pending gift 
    //
    //  PARAMS :
    //      - INPUT :
    //          - Account Id
    //          - Sql Transaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable: 
    //   NOTES :
    public static DataTable GetPendingGifts(Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _dt;

      _dt = new DataTable();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("  SELECT ");
        _sb.AppendLine("           GIN_GIFT_ID ");
        _sb.AppendLine("         , GIN_GIFT_TYPE ");
        _sb.AppendLine("         , GIN_GIFT_NAME ");
        _sb.AppendLine("         , GIN_SPENT_POINTS ");
        _sb.AppendLine("         , GIN_CONVERSION_TO_NRC ");
        _sb.AppendLine("         , 0 GI_DRAW_ID ");
        _sb.AppendLine("         , GIN_GIFT_INSTANCE_ID ");
        _sb.AppendLine("         , 0 GI_ACCOUNT_DAILY_LIMIT  ");
        _sb.AppendLine("         , 0 GI_ACCOUNT_MONTHLY_LIMIT");

        _sb.AppendLine("         , 0 GI_GLOBAL_DAILY_LIMIT   ");
        _sb.AppendLine("         , 0 GI_GLOBAL_MONTHLY_LIMIT ");
        _sb.AppendLine("         , GIFTS.GI_DESCRIPTION ");
        _sb.AppendLine("         , GIFTS.GI_SMALL_RESOURCE_ID ");
        _sb.AppendLine("         , GIFTS.GI_LARGE_RESOURCE_ID ");
        _sb.AppendLine("         , GIFT_INSTANCES.GIN_REQUESTED ");
        _sb.AppendLine("         , GIFT_INSTANCES.GIN_NUM_ITEMS ");
        _sb.AppendLine("         , CASHIER_VOUCHERS.CV_HTML AS VOUCHER ");
        _sb.AppendLine("    FROM   GIFT_INSTANCES  ");
        _sb.AppendLine("    INNER JOIN   GIFTS ON  GIFT_INSTANCES.GIN_GIFT_ID = GIFTS.GI_GIFT_ID ");
        _sb.AppendLine("    INNER JOIN   CASHIER_VOUCHERS ON  GIFT_INSTANCES.GIN_OPER_REQUEST_ID = CASHIER_VOUCHERS.CV_OPERATION_ID ");
        _sb.AppendLine("   WHERE   GIN_EXPIRATION > GETDATE() ");
        _sb.AppendLine("     AND  ( ( GIN_REQUEST_STATUS  = @pStatusPending AND GIN_GIFT_TYPE IN (@pGiftTypeObject,@pGiftTypeServices) )");
        _sb.AppendLine("            OR  ( GIN_REQUEST_STATUS  = @pStatusDelivered  AND GIN_GIFT_TYPE IN (@pGiftTypeNRC,@pGiftTypeRC) ");

        //  //LEM 11-12-2013: Redeemeble and Not Redeemeble Credit are not cancelable if ticket is not valid
        if (Utils.IsTitoMode())
        {
          _sb.AppendLine("               AND (SELECT TI_STATUS  FROM TICKETS WITH (INDEX (IX_ti_transaction_id_terminal_id_validation_number)) WHERE TI_TRANSACTION_ID = GIN_OPER_REQUEST_ID) = 0   ");
        }
        else
        {
          _sb.AppendLine("               AND GIN_DATA_01 > 0     ");

        }

        _sb.AppendLine("          )");
        _sb.AppendLine(")    AND   ( GIN_ACCOUNT_ID = @pAccountId ) ");
        _sb.AppendLine("ORDER BY   GIN_REQUESTED DESC ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = (Int32)GIFT_REQUEST_STATUS.PENDING;
          _cmd.Parameters.Add("@pStatusDelivered", SqlDbType.Int).Value = (Int32)GIFT_REQUEST_STATUS.DELIVERED;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pGiftTypeObject", SqlDbType.Int).Value = GIFT_TYPE.OBJECT;
          _cmd.Parameters.Add("@pGiftTypeNRC", SqlDbType.Int).Value = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
          _cmd.Parameters.Add("@pGiftTypeRC", SqlDbType.Int).Value = GIFT_TYPE.REDEEMABLE_CREDIT;
          _cmd.Parameters.Add("@pGiftTypeServices", SqlDbType.Int).Value = GIFT_TYPE.SERVICES;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            _dt.Load(_reader);
          }
        }
      }
      catch
      {
        _dt = new DataTable();
      }

      return _dt;

    } // GetPendingGifts

    public static Boolean CancelGiftTicketPromo(GiftInstance InstanceToCancel, Int32 TerminalId)
    {
      List<Int64> _ticket_list;
      Ticket _ticket;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);

      if (!Utils.IsTitoMode())
      {
        return false;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Ticket.DB_GetTicketsFromOperation(InstanceToCancel.RequestOperationId, out _ticket_list, _db_trx.SqlTransaction))
          {
            return false;
          }

          if (_ticket_list.Count != 1)
          {
            return false;
          }

          _ticket = Ticket.LoadTicket(_ticket_list[0], _db_trx.SqlTransaction, false);

          if (_allow_pay_tickets_pending_print)
          {
             if (_ticket.Status != TITO_TICKET_STATUS.VALID && _ticket.Status != TITO_TICKET_STATUS.PENDING_PRINT)
              {
                return false;
              }         
          }
          else
          {
            if (_ticket.Status != TITO_TICKET_STATUS.VALID)
            {
              return false;
            }    
          }


          if (!Ticket.Cashier_TicketDiscard(_ticket, TerminalId, InstanceToCancel.m_account_id, _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }
  }
}





