//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Cashier.cs
// 
//   DESCRIPTION: Class to manage an Administrative session of a Cashier
// 
//        AUTHOR: Miquel Beltran
// 
// CREATION DATE: 08-SEP-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-SEP-2010 MBF    First release.
// 25-OCT-2011 MPO    Add field total cash out of company a and b ([a|b]_total_cash_out)
// 26-OCT-2011 MPO    Add property "MobileBankHolderName" and it set the name of mobile bank
// 26-OCT-2011 AJQ    New class CashierInformation to keep different data on every thread (WCP process MB request in parallel).
// 16-NOV-2011 MPO    bug fixed: In RegisterCashierInformation It search by current ManagedThreadId when it should be it have multiple instances
// 16-FEB-2012 RCI    Added function GetCoverCouponPct().
// 17-FEB-2012 RCI    In routine ReadCashierSessionData(), sum the new NonRedeemable2(CoverCoupon) amount in the same variable as promotion Non Redeemable.
// 29-FEB-2012 JMM    In routine ReadCashierSessionData() the non redeemable amount is computed from CASHIER_MOVEMENTS instead MB_MOVEMENTS.
// 21-MAR-2012 SSC    Added function ReadTerminalId(SqlTransaction SqlTrx, String TerminalName).
// 19-ABR-2012 SSC    Added Operation code: COMMON_OPERATION_CODE_CHANGE_STACKER
// 22-MAY-2012 ACC    Add Mobile bank Deposit Amount in TYPE_CASHIER_SESSION_STATS and fill. 
//                    Add in switch (ReadCashierSessionData) mobile bank movements: CreditNonRedeemable, CreditCoverCoupon and CreditNonRedeemable2.
// 24-MAY-2012 JMM    Handle no dates on ReadCashierSessionData's DATE_RANGE filtering
// 07-JUN-2012 SSC    Added function IsSessionOpen().
// 11-JUN-2012 SSC    Modified GetSystemUser method to get system user by type parameter (Note Acceptor or Kiosk)
// 11-JUN-2012 SSC    Moved GetOrOpenSystemCashierSession(), DB_MobileBankInsertCashierMovement() and DB_MobileBankGetCashierInfo() methods to WSI.Common.Cashier
// 13-JUN-2012 SSC    Added new movements for Gifts/Points
// 20-JUN-2012 RCI    Added an static method to return an instance of the class CommonCashierSessionInfo.
// 07-JUL-2012 JAB    Added member variables in "TYPE_CASHIER_SESSION_STATS" (get the value "result_cashier_only_a" to show in frm_statistic_providers.vb)
// 09-JUL-2012 ACC    Add Cancel Promo and Expiration movements.
// 07-AUG-2012 JCM    Add Cashier / MB Sales Limit
// 03-OCT-2012 ACC    Moved EnsureMobileBankInCashierSession() and UpdateMBOnOpenCashierSession methods to WSI.Common.Cashier
// 13-FEB-2013 HBB & MPO    Added functionality of one cashier session for each PromoBOX.
// 22-FEB-2013 ICS    Moved from CashierBusinessLogic.cs:
//                    - UpdateSessionBalance
//                    - CashierSessionClose
// 26-APR-2013 HBB & ICS    Added functionality of one cashier session for each SYS-Acceptor.
// 03-MAY-2013 QMP    Added new cashier session statuses OPEN_PENDING and PENDING_CLOSING and related functionality.
// 22-MAY-2013 ICS    Modified 'ReadTerminalId' to prevent the creation of unnecessary records when changes an acceptor's name.
// 23-MAY-2013 QMP    Added WCPCashierSessionPendingClosing
// 24-MAY-2013 LEM    Modified CashierSessionPendingClosing to use the CashierMovementsTable constructor with TerminalName. Not used MBTerminalName as movement detail.
// 28-MAY-2013 JMM    Modified WCPCashierSessionPendingClosing to return a MB_CASHIER_SESSION_CLOSE_STATUS instead a boolean
// 16-JUL-2013 LEM    Overloaded DB_InsertCashierMovement with new parameter TerminalName
// 19-JUL-2013 LEM    Overloaded ReadCashierSessionData to use CashierMovementsFilter.SESSIONS_FROM_MOVEMENTS
// 24-JUL-2013 DLL    Add currency exchange support
// 12-AUG-2013 DRV    Added an static method that returns de OSK mode saved on the database
// 21-AUG-2013 DLL    Fixed bug WIG-142: Alarms when mismatch collected and balance are different
// 27-AUG-2013 RMS    Removed unnecessary parameters in CashDeskClose functions ( SessionId and OperationId )
// 29-AUG-2013 JCOR   Added AuthorizedByUserId AuthorizedByUserName.
// 30-AUG-2013 FBA    Added parameters to include Recharge for Points in CashDeskClose and CashDeskStatus vouchers. Fixed Bug WIG-173
// 02-SEP-2013 NMR    Added case for TITO ticket reprinting
// 03-SEP-2013 RCI    Fixed Bug WIG-183: Recharge for points: invalid cashier movements
// 10-SEP-2013 DLL    Add Check support
// 25-SEP-2013 NMR    Add case for TITO tickets movements
// 27-SEP-2013 ICS    Currency dictionaries have been changed into sorted dictionaries.
// 03-OCT-2013 NMR    Cleaned TITO warnings.
// 11-OCT-2013 AJQ & DDM     UNR's Taxes
// 14-OCT-2013 LEM    Include Tickets info in ReadCashierSessionData if TITOMode activated
// 16-OCT-2013 HBB    Added functionality of one cashier session for each machine in TITO Mode
// 22-OCT-2013 LEM    Unify saving account and cashier movements using SharedMovementsTables clases. Deleted DB_InsertCashierMovement
// 29-OCT-2013 LEM    TITO movements don't modify total amount cashier session data
// 12-NOV-2013 JML    Fixed Bug WIG-403: Recharge failed to reach the sales limit.
// 22-NOV-2013 SMN    Read CashierSession data from historicized tables
// 29-NOV-2013 NMR    Removed use of TI_XXX_CASHIER_SESSION
// 19-DEC-2013 LJM    Added a dictionary for Chips Sales and Chips Purchases
// 29-JAN-2014 RCI    Added CashIn tax support.
// 29-JAN-2014 DLL    Added game_profit for gaming tables cashier 
// 05-FEB-2014 DHA    Added New Movements TITO_TicketRedeemJackpot, TITO_TicketRedeemHandpay
// 05-MAR-2014 JCOR   Added PENDING_TRANSFER.
// 06-MAR-2014 AMF    Fixed Bug WIG-701: Not create mobile bank users when cash sessions are global(ICS 18-MAR-2014: It's not a defect, it's a feature!)
// 11-MAR-2014 RMS    Move FillCashierSessionFromDT to CSReports.CashierSessionsReports.
// 13-MAR-2014 RCI & JCA    Fixed Bug WIG-723
// 18-MAR-2014 JCOR   Added COMMON_OPERATION_CODE_CARD_ABANDONED for TITO.
// 18-MAR-2014 JPJ    Added filter per site/s
// 25-APR-2014 DHA    Added Tickets TITO detail
// 03-JUL-2014 JMM    Added Technician card events
// 25-JUL-2014 DLL    Chips are moved to a separate table
// 08-AUG-2014 RMS    Added supoort for Specific Bank Card Types
// 18-SEP-2014 RMS    Added support for cash advance
// 01-OCT-2014 JBP    Added ReadTerminalId without Transaction & null TerminalName now is controlled. 
// 27-NOV-2014 JBC    Fixed Bug WIG-1762: Close MB Session
// 15-DEC-2014 JBC, DLL, JPJ    New Company B commission movements
// 15-APR-2015 JBC & RMS    Cannot recharge when cashier session is out of gaming day
// 21-APR-2015 JBC & RMS    Fixed Bug WIG-2235: Error on stacker change (GamingDay).
// 27-MAY-2015 DCS    Added new multisite multicurrency filter and necessary changes to operate in multicurrency mode
// 17-JUL-2015 YNM    Fixed Bug WIG-2330: Promobox collection error 
// 24-AUG-2015 FAV    TFS-3257: Transfer between cashier sessions. 
// 27-AUG-2015 DHA    Generate movements/alarms for cash over/short only when is cashier session not integrated
// 29-OCT-2015 FAV    PBI 4695: Added the GetCashierSessionFromTerminal method to support for Gaming Hall
// 19-NOV-2015 ECP    Backlog Item 6463 Garcia River-09 Cash Report Resume & Closing Cash Voucer - Include Tickets Tito
// 27-NOV-2015 SGB    Product Backlog Item 4713: Change functions & structs from Cashier.TITO.BusinessLogic
// 18-DEC-2015 JMV    Product Backlog Item 7706:Salones(2): Resumen de caja espec�fico para Salones
// 13-JAN-2016 JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 28-JAN-2016 FOS    Product Backlog Item 7885: Add currency combo floor dual currency
// 28-JAN-2016 DHA    Product Backlog Item 8351:Floor Dual Currency: Movimientos de tickets TITO en M.N.
// 01-JAN-2016 FAV    Added new method GetCashierSessionById
// 02-FEB-2016 JML    Product Backlog Item 8352:Floor Dual Currency: Ticket Offline
// 09-FEB-2016 DHA    Product Backlog Item 9042: Floor Dual Currency
// 16-FEB-2016 FAV    Fixed Bug 9347, Total machine tickets voided
// 03-MAR-2016 JML    Product Backlog Item 10085:Winpot - Tax Provisions: Include movement reports
// 09-MAR-2016 EOR    Product Backlog Item 10235:TITA: Reporte de tickets
// 04-APR-2016 DHA    Product Backlog Item 9756: added chips (RE, NR and Color)
// 19-APR-2016 RGR    Product Backlog Item 11297: Codere - Tax Custody: Reports and cash movements and account
// 27-APR-2016 RGR    Product Backlog Item 11298: Codere - Tax Custody: Modify reports GUI Income and Return
// 22-MAR-2016 YNM    Bug 10759: Reception error paying with currency
// 02-MAY-2016 ETP    Product Backlog Item 12653: Multidivisa new functions for check last cashout amount and last operation amount.
// 03-MAY-2016 FAV    Product Backlog Item 12653:Cajero: Currency exchange for multiple denominations (Mallorca)
// 27-MAY-2016 YNM    PBI 13371:CountR - Review changes
// 06-JUN-2016 FAV    Fixed Bug 14126, "Operaciones con Tarjeta Bancaria" shows always a value equal to 0.
// 05-JUL-2016 ETP    PBI 15060: Cambio de multidivisa: Identificar al cliente en cualquier operaci�n con divisas
// 09-JUN-2016 DHA    Product Backlog Item 13402: Rolling & Fixed closing stock
// 09-JUN-2016 DHA    Product Backlog Item 14498: open cash and deposit in the same transaction
// 19-JUL-2016 JMV    PBI 15551: TPV Televisa: Uso del cajero como banco
// 09-AUG-2016 FGB    PBI 15985: Sistema Exped: Cashier check if the payment can de done
// 15-JUL-2016 JBP    Included GetCashierInfoBySession from FileCollection2 (Before GetCashierInfo).
// 10-AUG-2016 ETP    Fixed bug 16475: Add description to alarm.
// 19-AUG-2016 FJC    Fixed Bug 16884:CountR: error en los env�os de dinero a CountR recien creados.
// 17-AUG-2016 RAB    Product Backlog Item 15914: Gaming tables (Phase 5): Order summary box and voucher currency
// 24-AGO-2016 FJC    Bug 15896:CountR: En las sesiones de kiosko no se tiene en cuenta los tickets, por tanto no cuadra la diferencia (faltante y sobrante)
// 19-SEP-2016 EOR    PBI 17468: Televisa - Service Charge: Cashier Movements
// 26-SEP-2016 FAV    Bug 18127: Shows the message "Se sugiere pago con cheque" when 'BankCardRechargeVSCash.Pct' GP is 0 
// 27-SEP-2016 ETP    Bug 16475: Errors in zitro machines.
// 03-OCT-2016 RGR    PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
// 04-OCT-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 11-OCT-2016 ETP    Fixed Bug 18901:Multisite: Errors in cash summary in multisite mode.
// 02-NOV-2016 OVS    Fixed Bug 19938: CountR: Validaci�n tickets TITO Cajero
// 27-SEP-2016 JMM    PBI 18229:WCP - Proceso de la alarma Ticket Amount Not Multiple of Machine Denomination
// 28-NOV-2016 JMV    Bug 21029:Refund: pop-up "Payment by check" is always displayed 
// 28-NOV-2016 PDM    PBI: 20404:TITO Ticket: Validation Number
// 30-NOV-2016 FOS    PBI 19197: Reopen cashier sessions and gambling tables sessions
// 15-DEC-2016 FGB    Bug 5602:Error en el Log al cerrar una mesa desde el Gui
// 10-JAN-2017 RAB    Bug 22352: Not can close a GamingTable
// 16-JAN-2017 DHA    Bug 21855: cashier too slow on total withdrawal
// 26-JAN-2017 JML    Bug 23480:WigosGUI: Errors in cashier sessions report on balance, delivered and difference.
// 16-JAN-2016 DHA    Bug 21855: cashier too slow on total withdrawal
// 19-JAN-2017 RGR    PBI 22282: Winpot - Split cash summary
// 13-FEB-2017 JML    Fixed Bug 24422: Cashier: Incorrect withdrawal and closing with bank card
// 08-MAR-2017 FAV    PBI 25268: Third TAX - Payment Voucher
// 09-MAR-2017 FGB    PBI 25268: Third TAX - Payment Voucher
// 15-MAR-2017  ETP    WIGOS- 109: Add marker information to cashier forms.
// 08-FEB-2017 FAV    PBI 25268:Third TAX - Payment Voucher
// 23-FEB-2017 RGR    PBI 22282: Winpot - Split cash summary
// 28-MAR-2017 XGJ    Bug 26302:Voucher de Cierre de Caja-Empresa B: no refleja correctamente las cancelaciones
// 16-MAY-2017 DHA    Bug 27482:WIGOS-2090 Voucher is not printed when you open a gaming table session.
// 08-JUN-2017 XGJ    WIGOS-2752 FBM. Create two new events (Handpay - Cancel Credits, Handpay - Jackpot)
// 20-JUN-2017 EOR    Bug 28292: WIGOS-2979 The report "Resumen de sesion de caja" is not displaying the field "Report con tarjeta bancaria" 
// 11-JUL-2017 LTC    Bug 28644:WIGOS-3468 Cashier.Withdrawal � BankCardRechargeVSCash.Pct --> Doesn't apply properly
// 19-JUL-2017 LTC    Bug 28644:WIGOS-3468 Cashier.Withdrawal � BankCardRechargeVSCash.Pct --> Doesn't apply properly
// 25-JUL-2017 DHA    Bug 28929:WIGOS-3103 Some witholding documents are not being generated
// 29-AGU-2017 DHA    Bug 29477:WIGOS-4770 [Ticket #8392] Culiac�n - POS Desclnectado
// 22-SEP-2017 RAB    Bug 29823: WIGOS-4984 [Ticket #8492] Gambling Table out of working day - Exception v03.006.0023
// 19-OCT-2017 JML    Fixed Bug 30321:WIGOS-5924 [Ticket #9450] F�rmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesi�n de caja
// 29-NOV-2017 RAB    Bug 30942:WIGOS-6509 PinPad: wrong cashier movements when some vouchers are withdrawn
// 07-FEB-2018 DHA    Bug 31436:WIGOS-8001 [Ticket #12150] Todos: Error Reporte de Sesi�n de Caja Tarjeta Bancaria/PinPad
// 01-MAR-2018 EOR    Bug 31765: WIGOS-8491 Closing Cash message gets error when cancelling message when closing unbalance 
// 26-JUN-2018 FOS    Bug 33335: WIGOS-13162 Gambling tables: Credit card movement is not showed correctly when a close partial withdraw has been done.
// 28-JUN-2018 EAV    Bug 33404: WIGOS-13258 Gambling tables: after reopen gambling table session with integrated cashier shows something movements not expected with 0 
// 13-JUL-2018 AGS    Bug 33616:WIGOS-13496 Win Recycling Voucher Numbers
// 16-JUL-2018 AGS    Bug 33634:WIGOS-13459 [Ticket #15502] No permite cerrar sesi�n de caja de FVIDA desde el TPV
//--------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Collections;
using WSI.Common.CountR;
using WSI.Common.TITO;

namespace WSI.Common
{

  public struct ParamsTicketOperation
  {
    public TITO_TICKET_TYPE ticket_type;        // the type of final ticket
    public Currency out_cash_amount;            // final amount to pay national currency
    public CardData in_account;                 // la cuenta a la que se asociar� el movimiento
    public Int64 out_promotion_id;              // promotion Id; selected during Amount input
    public Int64 out_account_promo_id;          // Account promo ID
    public Currency in_cash_amt_0;              // initial amount to pay ticket currency
    public String in_cash_cur_0;                // initial ticket currency
    public Currency in_cash_amount;             // initial amount to pay national currency
    public Currency in_cash_amt_1;              // final amount to pay national currency
    public String in_cash_cur_1;                // final national currency
    public Currency max_devolution;             // max devolution amount
    public System.Windows.Forms.Form in_window_parent;               // Parent form
    public Int64 out_operation_id;              // operation Id; saved during Amount input
    public CashierSessionInfo session_info;     // current cashier session info
    public Boolean check_redeem;                // check paymentorder; out param of Amount input
    public Int64 terminal_id;                   // terminal id, used for manual handpays
    public WithholdData withhold;               // for redeem, if withhold is necessary
    public PaymentOrder payment_order;          // for redeem, if payment order is checked
    public Currency cash_redeem_total_paid;     // the amount of the cash_redeem.TotalPaid saved in routine WithholdAndPaymentOrderDialog()
    public List<VoucherValidationNumber> validation_numbers;     // list of validations numbers(string) and each amount(Currency) to redeem
    public Boolean is_apply_tax;                // inform if ticket apply taxes on total_paid or not
    public Int64 AccountOperationReasonId;
    public String AccountOperationComment;
    public Int64 cashier_terminal_id;
    public TerminalTypes terminal_types;
    public Boolean is_swap;
    public TITO_TERMINAL_TYPE tito_terminal_types;
   }

  public struct OutParamsTicketOperation
  {
    public Boolean CreaditAllowedReached;
    public ArrayList VoucherList;
    public List<Ticket> TicketList;
    public Int64 operation_id;
  }

  public static class CommonCashierInformation
  {

    public static CashierSessionInfo CashierSessionInfo()
    {
      CashierSessionInfo _csi;

      _csi = new CashierSessionInfo();

      _csi.CashierSessionId = CommonCashierInformation.SessionId;
      _csi.TerminalId = (Int32)CommonCashierInformation.TerminalId;
      _csi.UserId = CommonCashierInformation.UserId;
      _csi.TerminalName = CommonCashierInformation.TerminalName;
      _csi.UserName = CommonCashierInformation.UserName;
      _csi.AuthorizedByUserId = CommonCashierInformation.AuthorizedByUserId;
      _csi.AuthorizedByUserName = CommonCashierInformation.AuthorizedByUserName;
      _csi.GamingDay = CommonCashierInformation.GamingDay;
      _csi.IsMobileBank = false;

      return _csi;
    } // CashierSessionInfo

    private class CashierInformation
    {
      private static List<CashierInformation> m_list = new List<CashierInformation>();
      private static Boolean m_multiple_instances = true;

      private int m_owner_thread_id;
      public Int64 SessionId;
      public Int32 TerminalId;
      public String TerminalName;
      public Int32 UserId;
      public String UserName;
      public Int64 MobileBankId;
      public String MobileBankUserName;
      //29-AUG-2013 JCOR   Added AuthorizedByUserId AuthorizedByUserName.
      public Int32 AuthorizedByUserId;
      public String AuthorizedByUserName;
      public DateTime GamingDay;

      public static void SetUnique()
      {
        m_multiple_instances = false;
      }

      public static void RegisterCashierInformation(Int64 SessionId, Int32 UserId, String UserName, Int32 TerminalId, String TerminalName, Int64 MobileBankId, String MobileBankUserName, DateTime GamingDay)
      {
        CashierInformation _new_ci;

        _new_ci = new CashierInformation();

        _new_ci.SessionId = SessionId;
        _new_ci.TerminalId = TerminalId;
        _new_ci.TerminalName = TerminalName;
        _new_ci.UserId = UserId;
        _new_ci.UserName = UserName;
        _new_ci.MobileBankId = MobileBankId;
        _new_ci.MobileBankUserName = MobileBankUserName;
        _new_ci.AuthorizedByUserId = UserId;
        _new_ci.AuthorizedByUserName = UserName;
        _new_ci.GamingDay = GamingDay;

        RegisterCashierInformation(_new_ci);
      }

      public static void RegisterCashierInformation(CashierInformation NewCashierInformation)
      {
        CashierInformation _old_ci;
        int _thread_id;

        _thread_id = m_multiple_instances ? System.Threading.Thread.CurrentThread.ManagedThreadId : 0;

        NewCashierInformation.m_owner_thread_id = _thread_id;

        _old_ci = null;

        lock (m_list)
        {
          foreach (CashierInformation _ci in m_list)
          {
            if (_ci.m_owner_thread_id == _thread_id)
            {
              _old_ci = _ci;

              break;
            }
          }

          if (_old_ci != null)
          {
            m_list.Remove(_old_ci);
          }

          m_list.Add(NewCashierInformation);
        }
      }

      public static CashierInformation CurrentCashierInformation
      {
        get
        {
          int _thread_id;

          _thread_id = m_multiple_instances ? System.Threading.Thread.CurrentThread.ManagedThreadId : 0;

          lock (m_list)
          {
            foreach (CashierInformation _ci in m_list)
            {
              if (_ci.m_owner_thread_id == _thread_id)
              {
                return _ci;
              }
            }
          }

          return new CashierInformation();
          }
        }
      }

    public static Int64 TerminalId
    {
      get { return CashierInformation.CurrentCashierInformation.TerminalId; }
    }

    public static String TerminalName
    {
      get { return CashierInformation.CurrentCashierInformation.TerminalName; }
    }

    public static Int32 UserId
    {
      get { return CashierInformation.CurrentCashierInformation.UserId; }
    }

    public static Int64 SessionId
    {
      get { return CashierInformation.CurrentCashierInformation.SessionId; }
    }

    public static String MobileBankUserName
    {
      get { return CashierInformation.CurrentCashierInformation.MobileBankUserName; }
    }

    public static Int64 MobileBankId
    {
      get { return CashierInformation.CurrentCashierInformation.MobileBankId; }
    }

    public static DateTime GamingDay
    {
      get { return CashierInformation.CurrentCashierInformation.GamingDay; }
    }

    public static void SetUserSession(Int64 SessionId)
    {
      CashierInformation _ci;
      _ci = CashierInformation.CurrentCashierInformation;
      _ci.SessionId = SessionId;
      CashierInformation.RegisterCashierInformation(_ci);
    }

    public static string UserName
    {
      get { return CashierInformation.CurrentCashierInformation.UserName; }
    }

    //29-AUG-2013 JCOR   Added AuthorizedByUserId AuthorizedByUserName.
    public static string AuthorizedByUserName
    {
      set { CashierInformation.CurrentCashierInformation.AuthorizedByUserName = value; }
      get { return CashierInformation.CurrentCashierInformation.AuthorizedByUserName; }
    }

    public static Int32 AuthorizedByUserId
    {
      set { CashierInformation.CurrentCashierInformation.AuthorizedByUserId = value; }
      get { return CashierInformation.CurrentCashierInformation.AuthorizedByUserId; }
    }

    // ICS 31-MAY-2013: Must be used only for Cashier and GUI processes
    public static void SetUnique()
    {
      CashierInformation.SetUnique();
    }

    public static void SetCashierInformation(Int64 SessionId, Int32 UserId, String UserName, Int32 TerminalId, String TerminalName)
    {
      CashierInformation.RegisterCashierInformation(SessionId, UserId, UserName, TerminalId, TerminalName, 0, null, DateTime.MinValue);
    }

    public static void SetCashierInformation(Int64 SessionId, Int32 UserId, String UserName, Int32 TerminalId, String TerminalName, DateTime GamingDay)
    {
      CashierInformation.RegisterCashierInformation(SessionId, UserId, UserName, TerminalId, TerminalName, 0, null, GamingDay);
    }

    public static void SetMobileBankInformation(Int64 SessionId, Int32 UserId, String UserName, Int32 TerminalId, String TerminalName, Int64 MobileBankId, String MobileBankUserName, DateTime GamingDay)
    {
      CashierInformation.RegisterCashierInformation(SessionId, UserId, UserName, TerminalId, TerminalName, MobileBankId, MobileBankUserName, GamingDay);
    }

    /// <summary>
    /// Set cashier information with GUI user and CountR terminal
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="UserId"></param>
    /// <param name="UserName"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="Csi"></param>
    /// <returns></returns>
    public static Boolean SetCashierInformation_CageUserAndCountRTerminal(Int64 CashierSessionId, Int32 UserId, String UserName, SqlTransaction SqlTrx, out CashierSessionInfo Csi)
    {
      String _terminal_name;
      Int32 _terminal_id;

      Csi = null;
      if (!Misc.IsCountREnabled())
      {
        return false;
      }

      Cashier.GetCashierSessionById(CashierSessionId, out Csi, SqlTrx);

      if (Csi.TerminalId == 0)
      {
        return false;
      }

      _terminal_name = Csi.TerminalName;
      _terminal_id = Csi.TerminalId;

      Csi.AuthorizedByUserId = Csi.UserId;
      Csi.AuthorizedByUserName = Csi.UserName;

      // Set the information as the following:
      //  - Machine: is the CountR.
      //  - User: is the GUI user, not the CountR user.
      CommonCashierInformation.SetCashierInformation(CashierSessionId, UserId, UserName, _terminal_id, _terminal_name);

      // Set session information
      Csi = CashierSessionInfo();
      Csi.UserType = GU_USER_TYPE.USER;

      return true;
    }

    /// <summary>
    /// Set cashier information with GUI user
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="UserId"></param>
    /// <param name="UserName"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="Csi"></param>
    /// <returns></returns>
    public static Boolean SetCashierInformation_GUIUser(Int64 CashierSessionId, Int32 UserId, String UserName, SqlTransaction SqlTrx, out CashierSessionInfo Csi)
    {
      String _terminal_name;
      Int32 _terminal_id;

      Csi = null;
      _terminal_name = Environment.MachineName;
      _terminal_id = Cashier.ReadTerminalId(SqlTrx, _terminal_name, GU_USER_TYPE.USER);

      if (_terminal_id == 0)
      {
        return false;
      }

      // Set the information as the following:
      //  - Machine: is the PC where the GUI is executed.
      //  - User: is the GUI user, not the Cashier user.
      CommonCashierInformation.SetCashierInformation(CashierSessionId, UserId, UserName, _terminal_id, _terminal_name);

      // Set session information
      Csi = CashierSessionInfo();
      Csi.UserType = GU_USER_TYPE.USER;

      return true;
    } // SetCashierInformation_GUIUser
  }

  public class CashierMovementsInfo
  {
    #region Properties
    public Int64 MovementId { get; set; }
    public Int64 SessionId { get; set; }
    public Int32 CashierId { get; set; }
    public Int32 UserlId { get; set; }
    public DateTime Date { get; set; }
    public CASHIER_MOVEMENT Type { get; set; }
    public Int32 MoneyType { get; set; }
    public Int64? AccountMovementId { get; set; }
    public Decimal InitialBalance { get; set; }
    public Decimal SubAmount { get; set; }
    public Decimal AddAmount { get; set; }
    public Decimal FinalBalance { get; set; }
    public String UserName { get; set; }
    public String CashierName { get; set; }
    public String TrackData { get; set; }
    public Int64? AccountId { get; set; }
    public Int64? OperationId { get; set; }
    public String Details { get; set; }
    public String CurrencyIsoCode { get; set; }
    public Decimal? AuxAmount { get; set; }
    public Int32? UndoStatus { get; set; }
    public Decimal? CurrencyDenomination { get; set; }
    public Int64? GamingTableSessionId { get; set; }
    public Int64? ChipId { get; set; }
    public CageCurrencyType? CageCurrencyType { get; set; }
    public Int32? ChipsSaleDenomination { get; set; }
    public Int64? RelatedId { get; set; }

    #endregion
  }

  public class Cashier
  {
    private const Int32 MB_RECORDS_BATCH_SIZE = 100;

    static public Decimal m_nr2_pct_random_value = 0;

    public enum SalesLimitType
    {
      Cashier = 1,
      MobileBank = 2,
    }

    public enum CASHIER_MISMATCH
    {
      None = 0,
      Extra = 1,
      Missing = 2
    }

    // RMS 11-MAR-2014 : Converted to class
    public class TYPE_CASHIER_SESSION_STATS
    {

      //public TYPE_CASHIER_SESSION_STATS()
      //{
      //}

      // EXAMPLE:

      //Dep�sitos------------------ $ 5000
      //Retiros-------------------- $    0

      //Entradas
      //  Recarga -----------------  $1000
      //  Uso de Sala -------------  $ 100
      //  Tarjetas ----------------  $  50
      //Total --------------------- $ 1150

      //Salidas
      //  Devoluci�n - Recarga  ---  $ 500
      //  Devoluci�n - Uso de Sala   $  50
      //  Devoluci�n - Tarjetas ---  $   0
      //  Pago Premios          ---  $1188
      //Total --------------------- $ 1738

      //Total en Caja: ------------ $ 4412

      //MBF 27-JAN-2011
      //Entregado: ---------------- $ 4410
      //Diferencia: --------------- $   -2

      //   Total en Caja ---------- $ 4412
      //   Dep�sitos--------------- $ 5000  
      //   Retiros----------------- $    0
      //   Impuestos sobre premios  $  $12
      //   Pendiente Bancos M�viles $    0
      //Resultado de Caja --------- $ -600

      //  Premios ------------------ $1200
      //  Impuestos sobre premios --   $12
      //  Pago Premios ------------- $1188

      //  No Redimible ------------- $ 000
      //  Promoci�n a Redimible ---- $ 000

      //  Pagos Manuales    
      //    Pago ------------------- $ 100   
      //    Anulaci�n -------------- $  25 
      //  Total -------------------- $  75

      public Currency deposits;
      public Currency withdraws;
      public Currency payment_orders;  // Amount of redeems on Cheque

      public Currency a_total_in;
      public Currency b_total_in;
      public Currency cards_usage;
      public Currency cards_usage_a;
      public Currency cards_usage_b;
      public Currency total_cash_in;
      public Currency total_cash_in_aux;
      public Currency total_cash_in_only_a;
      public Currency participation_in_draw;

      public Currency a_total_dev;
      public Currency b_total_dev;
      public Currency b_total_dev_only_a;
      public Currency a_total_cash_out;
      public Currency b_total_cash_out;
      public Currency refund_cards_usage;
      public Currency a_refund_cards_usage;
      public Currency b_refund_cards_usage;
      public Currency prize_payment;
      public Currency total_cash_out;
      public Currency total_cash_out_only_a;

      // ICS 04-JUL-2013: Modified to have collected amount for each currency
      public SortedDictionary<CurrencyIsoType, Decimal> collected;       // Key: ISO Code & Type, Value: Amount
      public SortedDictionary<CurrencyIsoType, Decimal> collected_diff;

      // RMS 08-AUG-2014
      public Currency bank_card_amount_only_a;
      public Currency bank_card_amount_only_b;
      public Currency total_bank_card;
      public Currency total_bank_card_with_cash_advance;

      public Currency bank_credit_card_amount_only_a;
      public Currency bank_credit_card_amount_only_b;
      public Currency total_bank_credit_card;
      public Currency bank_debit_card_amount_only_a;
      public Currency bank_debit_card_amount_only_b;
      public Currency total_bank_debit_card;

      public Currency currency_exchange_amount_only_a;
      public Currency currency_exchange_amount_only_b;
      public Currency total_currency_exchange;

      // JML 20-JUL-2015: Short/Over amounts
      // public SortedDictionary<CurrencyIsoType, Decimal> collected_currency;
      public SortedDictionary<CurrencyIsoType, Decimal> short_currency;
      public SortedDictionary<CurrencyIsoType, Decimal> over_currency;
      public Boolean show_short_over;

      // DLL 10-SEP-2013: Add check cash balance
      public Currency check_amount_only_a;
      public Currency check_amount_only_b;
      public Currency total_check;

      // ICS 04-JUL-2013: Add currencies cash balance
      public Dictionary<CurrencyIsoType, Decimal> currencies_balance;

      // DLL 05-AUG-2013: Add currencies cash balance
      public SortedDictionary<CurrencyIsoType, Decimal> fill_in_balance;
      public SortedDictionary<CurrencyIsoType, Decimal> fill_out_balance;

      // ICS 18-SEP-2013: Add currencies cash in balance
      public SortedDictionary<CurrencyIsoType, Decimal> currency_cash_in;
      public SortedDictionary<CurrencyIsoType, Decimal> currency_cash_in_exchange;
      public SortedDictionary<CurrencyIsoType, Decimal> currency_cash_in_commissions;
      public SortedDictionary<CurrencyIsoType, Decimal> currency_game_profit;

      // FAV 03-MAY-2016 Currency exchange (Multiple denominations)
      public SortedDictionary<CurrencyIsoType, Decimal> currency_exchange_cash_in;
      public SortedDictionary<CurrencyIsoType, Decimal> currency_exchange_cash_out;
      public Currency cash_in_currency_exchange;
      public Currency cash_out_currency_exchange;

      public Currency cash_net; // RCI 29-JAN-2013
      public Currency cash_net_only_a; // RCI 29-JAN-2013
      public Currency total_in_cashier;
      public Currency total_in_cashier_only_a;
      public Currency prize_taxes;
      public Currency tax_returning_on_prize_coupon;
      // RRB 10-SEP-2012
      public Currency decimal_rounding;
      public Currency service_charge;
      public String prize_taxes_1_name;
      public String prize_taxes_2_name;
      public String prize_taxes_3_name;
      public Currency prize_taxes_1;
      public Currency prize_taxes_2;
      public Currency prize_taxes_3;
      public Currency mb_pending;
      public Currency mb_deposit_amount;
      public Currency mb_total_balance_lost;

      public Currency result_cashier;
      public Currency result_cashier_only_a;

      public Currency prize_gross;
      public Currency prize_gross_other;

      public Currency promo_re;
      public Currency promo_nr;
      public Currency promo_nr_to_re;

      // ACC 09-JUL-2012 Add Cancel Promo movements.
      public Currency cancel_promo_nr;
      public Currency cancel_promo_re;

      // ACC 09-JUL-2012 Add Expiration movements.
      public Currency redeemable_dev_expired;
      public Currency redeemable_prize_expired;
      public Currency not_redeemable_expired;

      // AJQ 23-AUG-2013, Points as CashIn: Movement description: Recharge for points
      public Currency points_as_cashin_only_a;
      public Currency points_as_cashin_only_b;
      public Currency points_as_cashin_prize;
      public Currency points_as_cashin;

      public Currency handpay_payment;
      public Currency handpay_canceled;

      public Currency final_balance;
      public Currency final_balance_only_a;
      public Currency final_balance_only_b;

      public String split_a_company_name;
      public String split_a_name;
      public String split_a_tax_name;
      public Decimal split_a_tax_pct;
      public Decimal cash_in_tax_pct;

      public Boolean split_enabled_b;

      public String split_b_company_name;
      public String split_b_name;
      public String split_b_tax_name;
      public Decimal split_b_tax_pct;

      public Currency report_a_total_in_gross;
      public Currency report_a_total_in_net;
      public Currency report_a_total_in_taxes;
      public Currency report_b_total_in_dev_net;
      public Currency report_b_total_in_dev_taxes;
      public Currency report_a_total_dev_gross;
      public Currency report_a_total_dev_net;
      public Currency report_a_total_dev_taxes;
      public Currency report_a_base_taxes;
      public Currency report_a_taxes;
      public Currency report_prize_gross;
      public Currency report_prize_taxes_1;
      public Currency report_prize_taxes_2;
      public Currency report_prize_taxes_3;
      public Decimal report_prize_tax_1_pct;
      public Decimal report_prize_tax_2_pct;
      public Decimal report_prize_tax_3_pct;
      public Currency report_a_total_in_cashier;
      public Currency report_b_total_in_cashier;
      public Currency report_total_in_cashier;
      public Currency report_funds_non_redeemable_promotion;

      // HBB 19-SEP-2013 TITO: adding Tickets fields
      public Int32 tickets_created_count;
      public Int32 tickets_canceled_or_redeemed_count;
      public Currency tickets_canceled_or_redeemed_amount;
      public Currency tickets_offline_amount;

      // JPJ 28-JAN-2014 TITO:
      public Currency tickets_created_amount_redeemable_handpay;
      public Currency tickets_created_amount_redeemable_jackpot;

      public Currency tickets_cancelled_redeemable_handpay;
      public Currency handpay_tito_hold;
      public Currency tickets_cancelled_redeemable_jackpot;

      // EOR 09-MAR-2016
      public Currency tickets_swap_chips;

      // DHA 25-APR-2014
      public Currency tickets_cashier_printed_cashable;
      public Currency tickets_cashier_printed_promo_redeemable;
      public Currency tickets_cashier_printed_promo_no_redeemable;
      public Currency tickets_machine_printed_cashable;
      public Currency tickets_machine_printed_promo_no_redeemable;
      public Currency tickets_cashier_paid_cashable;
      public Currency tickets_cashier_paid_promo_redeemable;
      public Currency tickets_machine_played_cashable;
      public Currency tickets_machine_played_promo_redeemable;
      public Currency tickets_machine_played_promo_no_redeemable;
      public Currency tickets_cashier_expired_redeemed;

      //JBC 21-FEB-2014
      public Currency tickets_expired_promo_redeemable;
      public Currency tickets_expired_promo_no_redeemable;
      public Currency tickets_expired_redeemable;

      public TYPE_LIABILITIES liabilities_from;
      public TYPE_LIABILITIES liabilities_to;

      // AJQ & DDM 11-OCT-2013, UNR's
      public Currency unr_k1_gross;
      public Currency unr_k1_tax1;
      public Currency unr_k1_tax2;
      public Currency unr_k1_tax3;
      public Currency unr_k2_gross;
      public Currency unr_k2_tax1;
      public Currency unr_k2_tax2;
      public Currency unr_k2_tax3;

      // JML 22-DEC-2015, RE In Kind
      public Currency ure_k1_gross;
      public Currency ure_k1_tax1;
      public Currency ure_k1_tax2;
      public Currency ure_k1_tax3;
      public Currency ure_k2_gross;
      public Currency ure_k2_tax1;
      public Currency ure_k2_tax2;
      public Currency ure_k2_tax3;

      // LA 29-FEB-2016
      public Currency prize_taxes_1_pr_re;
      public Currency prize_taxes_2_pr_re;
      public Currency prize_taxes_3_pr_re;

      // 
      public Currency total_tax1;
      public Currency total_tax2;
      public Currency total_tax3;

      // RCI 02-JAN-2014
      public Currency cash_in_tax_split1;
      public Currency cash_in_tax_split2;

      // RGR 18-APR-2016
      public Currency tax_custody;

      // RGR 26-APR-2016
      public Currency tax_custody_return;

      // JML 03-MAR-2016
      public Currency tax_provisions;

      // DLL 27-JAN-2014
      public Currency national_game_profit;

      public Decimal chips_purchase_total;
      public Decimal chips_sale_total;
      public Currency chips_sale_register_total;

      public SortedDictionary<CurrencyIsoType, Decimal> chips_purchase;
      public SortedDictionary<CurrencyIsoType, Decimal> chips_sale;

      //JBC 04-DEC-2014
      public Dictionary<Split_Taxes_Amount, Split_Tax_Amount> split_tax_amounts;

      // RMS 04-MAR-2014

      public Currency total_taxes
      {
        get
        {
          return total_tax1 + total_tax2 + total_tax3;
        }
      }

      public Currency commissions_currency_exchange;
      public Currency commissions_bank_card;
      public Currency company_b_commissions_bank_card;
      public Currency company_b_commissions_debit_card;
      public Currency company_b_commissions_credit_card;
      public Currency total_commission_bank_card;
      public Currency commissions_check;
      public Currency company_b_commissions_check;
      public Currency total_commission_check;
      public Currency company_b_commissions_currency_exchange;
      public Currency total_commission_currency_exchange;
      public Currency total_credit_line_marker;
      public Currency total_credit_line_payback;

      // JMV 01-JUL-2016
      public Currency cash_promos;

      public Currency total_banck_card_commission_a
      {
        get
        {
          return commissions_bank_card + cash_advance_debit_card_comissions + cash_advance_credit_card_comissions + cash_advance_generic_card_comissions;
        }
      }

      public Currency total_commission
      {
        get
        {
          return total_commission_check + total_commission_bank_card + total_commission_currency_exchange + total_cash_advance_comissions;
        }
      }

      public Currency total_commission_a
      {
        get
        {
          return commissions_currency_exchange + commissions_bank_card + commissions_check +
                cash_advance_credit_card_comissions + cash_advance_debit_card_comissions + cash_advance_generic_card_comissions +
                cash_advance_check_comissions;
        }
      }

      public Currency total_commission_b
      {
        get
        {
          return company_b_commissions_currency_exchange + company_b_commissions_bank_card + company_b_commissions_debit_card + company_b_commissions_credit_card +
                 company_b_commissions_check + company_b_cash_advance_check_comissions + total_cash_advance_bank_card_commission_b;
        }
      }

      public Currency total_balance_bank_card_a
      {
        get
        {
          return bank_card_amount_only_a + cash_advance_credit_card + cash_advance_credit_card_comissions +
                 cash_advance_debit_card + cash_advance_debit_card_comissions + cash_advance_generic_card + cash_advance_generic_card_comissions;
        }
      }

      public Currency total_balance_bank_card_b
      {
        get
        {
          return bank_card_amount_only_b + total_cash_advance_bank_card_commission_b;
        }
      }

      public Currency check_fill_out;

      public Currency check_result
      {
        get
        {
          return check_amount_only_a + check_amount_only_b - check_fill_out;
        }
      }

      public Currency chips_balance
      {
        get
        {
          return chips_purchase_total - chips_sale_total;
        }
      }

      // SMN 16-SEP-2014
      public Currency concepts_input;
      public Currency concepts_output;
      public SortedDictionary<CurrencyIsoType, Decimal> concepts_currency_input;
      public SortedDictionary<CurrencyIsoType, Decimal> concepts_currency_output;

      // YNM 25-JAN-2016
      public Currency entrances_input;
      public Currency entrances_input_currency;
      public SortedDictionary<CurrencyIsoType, Decimal> entrances_currency_input;
      public Currency entrances_input_card;
      public Currency entrances_input_debit_card;
      public Currency entrances_input_credit_card;
      public Currency entrances_input_check;

      // FAV 24-AUG-2015
      public Currency session_transfers_input;
      public Currency session_transfers_output;
      public SortedDictionary<CurrencyIsoType, Decimal> session_transfers_currency_input;
      public SortedDictionary<CurrencyIsoType, Decimal> session_transfers_currency_output;

      // RMS 18-SEP-2014
      public Currency cash_advance_credit_card;
      public Currency cash_advance_debit_card;
      public Currency cash_advance_generic_card;
      public Currency cash_advance_check;
      public Currency cash_advance_credit_card_comissions;
      public Currency cash_advance_debit_card_comissions;
      public Currency cash_advance_generic_card_comissions;
      public Currency cash_advance_check_comissions;
      public Currency company_b_cash_advance_credit_card_comissions;
      public Currency company_b_cash_advance_debit_card_comissions;
      public Currency company_b_cash_advance_generic_card_comissions;
      public Currency company_b_cash_advance_check_comissions;

      public Currency total_cash_advance_bank_credit_card
      {
        get
        {
          return cash_advance_credit_card + cash_advance_credit_card_comissions + company_b_cash_advance_credit_card_comissions;
        }
      }

      public Currency total_cash_advance_bank_debit_card
      {
        get
        {
          return cash_advance_debit_card + cash_advance_debit_card_comissions + company_b_cash_advance_debit_card_comissions;
        }
      }

      public Currency total_cash_advance_bank_card_commission_a
      {
        get
        {
          return cash_advance_generic_card_comissions +
                 cash_advance_debit_card_comissions +
                 cash_advance_credit_card_comissions;
        }
      }

      public Currency total_cash_advance_bank_card_commission_b
      {
        get
        {
          return company_b_cash_advance_credit_card_comissions +
                 company_b_cash_advance_debit_card_comissions +
                 company_b_cash_advance_generic_card_comissions;
        }
      }

      public Currency total_cash_advance_bank_card_comissions
      {
        get
        {
          return total_cash_advance_bank_card_commission_a +
                 total_cash_advance_bank_card_commission_b;
        }
      }

      public Currency total_cash_advance_bank_card
      {
        get
        {
          return cash_advance_generic_card + cash_advance_debit_card + cash_advance_credit_card + total_cash_advance_bank_card_comissions;
        }
      }

      public Currency total_cash_advance_comissions
      {
        get
        {
          return cash_advance_check_comissions + company_b_cash_advance_check_comissions + total_cash_advance_bank_card_comissions;
        }
      }

      public Currency total_cash_advance
      {
        get
        {
          return cash_advance_check + cash_advance_credit_card + cash_advance_debit_card + cash_advance_generic_card;
        }
      }


      //JML 06-JUL-2015
      public Currency bank_card_cash_in_card_deposit_split1;
      public Currency bank_credit_card_cash_in_card_deposit_split1;
      public Currency bank_debit_card_cash_in_card_deposit_split1;
      public Currency check_cash_in_card_deposit_split1;
      public Currency currency_cash_in_card_deposit_split1;

      public Currency bank_card_cash_in_card_deposit_split2;
      public Currency bank_credit_card_cash_in_card_deposit_split2;
      public Currency bank_debit_card_cash_in_card_deposit_split2;
      public Currency check_cash_in_card_deposit_split2;
      public Currency currency_cash_in_card_deposit_split2;

      public Currency total_bank_card_cash_in_card_deposit
      {
        get
        {
          return bank_card_cash_in_card_deposit_split1 + bank_card_cash_in_card_deposit_split2;
        }
      }

      public Currency total_bank_credit_card_cash_in_card_deposit
      {
        get
        {
          return bank_credit_card_cash_in_card_deposit_split1 + bank_credit_card_cash_in_card_deposit_split2;
        }
      }

      public Currency total_bank_debit_card_cash_in_card_deposit
      {
        get
        {
          return bank_debit_card_cash_in_card_deposit_split1 + bank_debit_card_cash_in_card_deposit_split2;
        }
      }

      public Currency total_check_cash_in_card_deposit
      {
        get
        {
          return check_cash_in_card_deposit_split1 + check_cash_in_card_deposit_split2;
        }
      }

      public Currency total_currency_cash_in_card_deposit
      {
        get
        {
          return currency_cash_in_card_deposit_split1 + currency_cash_in_card_deposit_split2;
        }
      }

      public SortedDictionary<CurrencyIsoType, Decimal> currency_cash_in_card_deposit;

      //JBC 22-OCT-2014
      public Currency total_cash_over;

      public Decimal pct_CC;

      // ECP 19-NOV-2015
      public Currency tickets_created_amount;
      public Currency tickets_cancelled_amount;
      public Int32 tickets_cancelled_count;
      public Currency tickets_played_machine_amount;
      public Currency tickets_created_machine_amount;
      public Int32 tickets_played_machine_count;
      public Int32 tickets_created_machine_count;
      public Int32 tickets_created_redeemable_handpay_count;
      public Int32 tickets_created_redeemable_jackpot_count;
      public Int32 tickets_cancelled_redeemable_handpay_count;
      public Int32 tickets_cancelled_redeemable_jackpot_count;

      public Int32 tickets_offline_count;

      public Int32 tickets_online_dicarded_count;

      public Int32 tickets_cashier_printed_cashable_count;
      public Int32 tickets_cashier_printed_promo_redeemable_count;
      public Int32 tickets_cashier_printed_promo_no_redeemable_count;
      public Int32 tickets_machine_printed_cashable_count;
      public Int32 tickets_machine_printed_promo_no_redeemable_count;
      public Int32 tickets_cashier_paid_cashable_count;
      public Int32 tickets_cashier_paid_promo_redeemable_count;
      public Int32 tickets_machine_played_cashable_count;
      public Int32 tickets_machine_played_promo_redeemable_count;
      public Int32 tickets_machine_played_promo_no_redeemable_count;
      public Int32 tickets_cashier_expired_redeemed_count;
      // EOR 20-MAY-2016
      public Int32 tickets_swap_chips_count;

      public Int32 tickets_expired_promo_redeemable_count;
      public Int32 tickets_expired_promo_no_redeemable_count;
      public Int32 tickets_expired_redeemable_count;

      // JMV 18-DEC-2015
      public Currency refill_amount_to_cage;
      public Currency collect_amount_to_cage;
      public Currency refill_amount_to_cashier;
      public Currency collect_amount_to_cashier;
      public SortedDictionary<CurrencyIsoType, Decimal> refill_currency_amount_to_cage;
      public SortedDictionary<CurrencyIsoType, Decimal> collect_currency_amount_to_cage;
      public SortedDictionary<CurrencyIsoType, Decimal> refill_currency_amount_to_cashier;
      public SortedDictionary<CurrencyIsoType, Decimal> collect_currency_amount_to_cashier;

      // FAV 16-FEB-2016
      public Currency total_machine_ticket_voided;

      // EOR 22-SEP-2016
      public Currency service_charge_a;
      public Currency service_charge_b;

      // XGJ  28-MAR-2017
      public Currency total_in_b { get; set; }

      // EOR 20-JUN-2017
      public Int32 vouchers_delivered;
      public Int32 vouchers_no_delivered;
      public Int32 total_vouchers_number
      {
        get
        {
          return vouchers_delivered + vouchers_no_delivered;
        }
      }
      public Currency vouchers_delivered_amount;
      public Currency vouchers_no_delivered_amount;
      public Currency total_vouchers_amount
      {
        get
        {
          return vouchers_delivered_amount + vouchers_no_delivered_amount;
        }
      }

      public ClosingStocks ClosingStock;

      public Boolean m_cashier_session_has_pinpad_operations;

      public Boolean MsgUnbalanceAccept;

      public Currency current_card_withdrawal_total { get; set; }

    };

    public struct TYPE_LIABILITIES
    {
      public DateTime datetime;
      public Boolean has_data;

      public Currency re_balance;
      public Currency promo_re_balance;
      public Currency promo_nr_balance;

      // For future use.
      public Currency in_session_re_to_gm;
      public Currency in_session_promo_re_to_gm;
      public Currency in_session_promo_nr_to_gm;
      public Currency points;
      public Int32 num_accounts;
      public Int32 num_accounts_in_session;
    }

    static public void ReadHoldvsWinCashierData(DateTime dtFrom, DateTime dtTo, SqlTransaction Trx, ref TYPE_MAILING_HOLDVSWIN_CASHIER HoldvsWinCashierData)
    {
      Currency _sum_inputs;
      Decimal _pc_daily_win;
      TYPE_CASHIER_SESSION_STATS _cashier_session_stats;

      _sum_inputs = 0;
      _pc_daily_win = 0;
      _cashier_session_stats = new TYPE_CASHIER_SESSION_STATS();

      Cashier.ReadCashierSessionData(Trx, dtFrom, dtTo, ref _cashier_session_stats);
      HoldvsWinCashierData.cashier_tickets_created = _cashier_session_stats.tickets_cashier_printed_cashable;
      HoldvsWinCashierData.cashier_cash_promos = _cashier_session_stats.cash_promos;
      HoldvsWinCashierData.cashier_tickets_paid = _cashier_session_stats.tickets_cashier_paid_cashable;
      HoldvsWinCashierData.cashier_handpays_paid = _cashier_session_stats.handpay_payment - _cashier_session_stats.handpay_canceled;

      _sum_inputs = HoldvsWinCashierData.cashier_bill_in + HoldvsWinCashierData.cashier_tickets_created + HoldvsWinCashierData.cashier_cash_promos;
      if (_sum_inputs == 0)
      {
        _sum_inputs = 1;
      }
      _pc_daily_win = HoldvsWinCashierData.cashier_daily_netwin / _sum_inputs;

      HoldvsWinCashierData.cashier_netwin_percent = _pc_daily_win.ToString("N2") + "%";
    }

    static public bool ReadHoldvsWinData(DateTime dtIni, SqlTransaction Trx, ref TYPE_MAILING_HOLDVSWIN HoldvsWinData)
    {
      String _sql_str;

      Decimal _egm_cash_netwin_redimible_percent;
      Decimal _egm_total_hold_percent;
      Decimal _egm_cash_total_in_redimible;
      Decimal _egm_cash_netwin_percent;
      Currency _sum_inputs;
      Decimal _pc_daily_win;

      _sum_inputs = 0;
      _pc_daily_win = 0;

      try
      {
        _sql_str = "TITO_HoldvsWin";

        using (SqlCommand _sql_command = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pIniDate", SqlDbType.DateTime).Value = dtIni;
          _sql_command.Parameters.Add("@pGroupByOption", SqlDbType.Int).Value = 2;
          _sql_command.Parameters.Add("@pOrderGroup0", SqlDbType.Int).Value = 0;
          _sql_command.CommandType = CommandType.StoredProcedure;

          using (SqlDataReader _reader = _sql_command.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            // 0 - Coin in
            // 1 - TotalWon
            // 2 - Hold
            // 3 - BillIn
            // 4 - CashableTicketIn
            // 5 - RestrictedTicketIn
            // 6 - NonRestrictedTicketIn
            // 7 - CashableTicketOut
            // 8 - RestrictedTicketOut
            // 9 - Jackpots
            // 10 - HandPaidCancelledCredits
            // 11 - TotalIn
            // 12 - TotalOut
            // 13 - TotalWin
            // 14 - TotalReIn
            // 15 - TotalReOut
            // 16 - TotalReWin


            _egm_cash_netwin_redimible_percent = 0;
            _egm_cash_total_in_redimible = 0;
            _egm_total_hold_percent = 0;
            _egm_cash_netwin_percent = 0;

            HoldvsWinData.egm_cash_total_in = _reader.GetDecimal(11);

            HoldvsWinData.egm_cash_total_out = _reader.GetDecimal(12);
            HoldvsWinData.egm_cash_netwin = HoldvsWinData.egm_cash_total_in - HoldvsWinData.egm_cash_total_out;
            if (HoldvsWinData.egm_cash_total_in != 0)
            {
              _egm_cash_netwin_percent = (HoldvsWinData.egm_cash_netwin / HoldvsWinData.egm_cash_total_in) * 100;
            }
            HoldvsWinData.egm_cash_netwin_percent = _egm_cash_netwin_percent.ToString("N2") + "%";

            HoldvsWinData.egm_cash_bill_in = HoldvsWinData.cashier_data.cashier_bill_in = _reader.GetDecimal(3);
            HoldvsWinData.egm_cash_ticket_in_redimible = _reader.GetDecimal(4);
            HoldvsWinData.egm_cash_ticket_out_redimible = _reader.GetDecimal(7);
            HoldvsWinData.egm_cash_jackpots = _reader.GetDecimal(9);
            HoldvsWinData.egm_cash_handpays = _reader.GetDecimal(10);
            HoldvsWinData.egm_cash_netwin_redimible = _reader.GetDecimal(16);
            _egm_cash_total_in_redimible = _reader.GetDecimal(14);
            if (_egm_cash_total_in_redimible != 0)
            {
              _egm_cash_netwin_redimible_percent = (HoldvsWinData.egm_cash_netwin_redimible / _egm_cash_total_in_redimible) * 100;
            }
            HoldvsWinData.egm_cash_netwin_redimible_percent = _egm_cash_netwin_redimible_percent.ToString("N2") + "%";

            HoldvsWinData.egm_total_coin_in = _reader.GetDecimal(0);
            HoldvsWinData.egm_total_total_won = _reader.GetDecimal(1);
            HoldvsWinData.egm_total_hold = _reader.GetDecimal(2);

            if (HoldvsWinData.egm_total_coin_in != 0)
            {
              _egm_total_hold_percent = (HoldvsWinData.egm_total_hold / HoldvsWinData.egm_total_coin_in) * 100;
            }
            HoldvsWinData.egm_total_hold_percent = _egm_total_hold_percent.ToString("N2") + "%";

            HoldvsWinData.cashier_data.cashier_daily_netwin = HoldvsWinData.egm_cash_bill_in + HoldvsWinData.cashier_data.cashier_tickets_created + HoldvsWinData.cashier_data.cashier_cash_promos - (HoldvsWinData.cashier_data.cashier_tickets_paid + HoldvsWinData.cashier_data.cashier_handpays_paid);

            _sum_inputs = HoldvsWinData.cashier_data.cashier_bill_in + HoldvsWinData.cashier_data.cashier_tickets_created + HoldvsWinData.cashier_data.cashier_cash_promos;

            if (_sum_inputs != 0)
            {
              _pc_daily_win = HoldvsWinData.cashier_data.cashier_daily_netwin / _sum_inputs * 100;
            }

            HoldvsWinData.cashier_data.cashier_netwin_percent = _pc_daily_win.ToString("N2") + "%";

            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Hold vs Win data:" + ex.ToString());
      }

      return false;
    }

    static public bool ReadHoldvsWinReceptionData(SqlTransaction Trx, ref TYPE_MAILING_HOLDVSWIN_RECEPTION ReceptionData)
    {
      String _sql_str;

      try
      {
        _sql_str = "TITO_HoldvsWin_Reception";

        using (SqlCommand _sql_command = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _sql_command.CommandType = CommandType.StoredProcedure;

          using (SqlDataReader _reader = _sql_command.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            ReceptionData.Enabled = (_reader.GetInt32(0) == 1);
            ReceptionData.Visits = _reader.GetInt32(2);

            ReceptionData.VisitsLevel0 = _reader.GetInt32(7);
            ReceptionData.VisitsLevel1 = _reader.GetInt32(8);
            ReceptionData.VisitsLevel2 = _reader.GetInt32(9);
            ReceptionData.VisitsLevel3 = _reader.GetInt32(10);
            ReceptionData.VisitsLevel4 = _reader.GetInt32(11);
            ReceptionData.VisitsLevel5 = _reader.GetInt32(12);

            ReceptionData.VisitsAnonymous = _reader.GetInt32(13);
            ReceptionData.VisitsMale = _reader.GetInt32(14);
            ReceptionData.VisitsFemale = _reader.GetInt32(15);

            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading reception data:" + ex.ToString());
      }

      return false;
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, String SessionsQuery, ref TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.SESSION_ID_BY_QUERY,
                             0, SessionsQuery, DateTime.MinValue, DateTime.MaxValue, "", ref CashierSessionData, string.Empty);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, String SessionsQuery, ref TYPE_CASHIER_SESSION_STATS CashierSessionData, String FloorDualCurrencyIsoCode)
    {
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.SESSION_ID_BY_QUERY,
                             0, SessionsQuery, DateTime.MinValue, DateTime.MaxValue, "", false, "", null, ref CashierSessionData, FloorDualCurrencyIsoCode);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, Int64 CurrentSessionId, ref TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.SESSION_ID,
                             CurrentSessionId, "", DateTime.MinValue, DateTime.MaxValue, "", ref CashierSessionData, String.Empty);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, DateTime DateFrom, DateTime DateTo, ref TYPE_CASHIER_SESSION_STATS CashierSessionData, string FloorDualCurrencyIsoCode)
    {
      if (DateTo == DateTime.MinValue)
      {
        DateTo = DateTime.MaxValue;
      }
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.DATE_RANGE,
                             0, "", DateFrom, DateTo, "",
                            false, "", null, ref CashierSessionData, FloorDualCurrencyIsoCode);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, DateTime DateFrom, DateTime DateTo, ref TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      if (DateTo == DateTime.MinValue)
      {
        DateTo = DateTime.MaxValue;
      }
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.DATE_RANGE,
                             0, "", DateFrom, DateTo, "",
                            false, "", null, ref CashierSessionData, string.Empty);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, DateTime DateFrom, DateTime DateTo, String SiteId, Boolean HideChipsAndCurrenciesConcepts, String IsoCode, DataTable CurrenciesTable, ref TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      if (DateTo == DateTime.MinValue)
      {
        DateTo = DateTime.MaxValue;
      }
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.DATE_RANGE,
                             0, "", DateFrom, DateTo, SiteId,
                            HideChipsAndCurrenciesConcepts, IsoCode, CurrenciesTable, ref CashierSessionData, string.Empty);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, Int64 CurrentSessionId, DateTime DateFrom, DateTime DateTo, Boolean HideChipsAndCurrenciesConcepts, DataTable CurrenciesTable, String TerminalIsoCode, ref TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      if (DateTo == DateTime.MinValue)
      {
        DateTo = DateTime.MaxValue;
      }
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.SESSION_ID,
                             CurrentSessionId, "", DateFrom, DateTo, "",
                            HideChipsAndCurrenciesConcepts, "", CurrenciesTable, ref CashierSessionData, TerminalIsoCode);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, String SessionsQuery, DateTime DateFrom, DateTime DateTo, ref TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      if (DateTo == DateTime.MinValue)
      {
        DateTo = DateTime.MaxValue;
      }
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.SESSIONS_FROM_MOVEMENTS,
                             0, SessionsQuery, DateFrom, DateTo, "", ref CashierSessionData, String.Empty);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, String SessionsQuery, DateTime DateFrom, DateTime DateTo, ref TYPE_CASHIER_SESSION_STATS CashierSessionData, String TerminalIsoCode)
    {
      if (DateTo == DateTime.MinValue)
      {
        DateTo = DateTime.MaxValue;
      }
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.SESSIONS_FROM_MOVEMENTS,
                             0, SessionsQuery, DateFrom, DateTo, "", ref CashierSessionData, TerminalIsoCode);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, Int64 CurrentSessionId, DateTime DateFrom, DateTime DateTo, ref TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      if (DateTo == DateTime.MinValue)
      {
        DateTo = DateTime.MaxValue;
      }
      ReadCashierSessionData(SQLTransaction, CashierMovementsFilter.SESSIONS_FROM_MOVEMENTS,
                            CurrentSessionId, "", DateFrom, DateTo, "",
                            false, "", null, ref CashierSessionData, string.Empty);
    }

    static public void ReadCashierSessionData(SqlTransaction SQLTransaction, CashierMovementsFilter ReadType, String SesionsQuery, DateTime DateFrom, DateTime DateTo, ref TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      ReadCashierSessionData(SQLTransaction, ReadType,
                            0, SesionsQuery, DateFrom, DateTo, "",
                            false, "", null, ref CashierSessionData, string.Empty);
    }

    static private void ReadCashierSessionData(SqlTransaction SQLTransaction,
                                           CashierMovementsFilter ReadType,
                                           Int64 CurrentSessionId,
                                           String SessionsQuery,
                                           DateTime DateFrom,
                                           DateTime DateTo,
                                           String SiteId,
                                           ref TYPE_CASHIER_SESSION_STATS CashierSessionData,
                                           String FloorDualCurrencyIsoCode)
    {
      ReadCashierSessionData(SQLTransaction, ReadType,
                            CurrentSessionId, SessionsQuery, DateFrom, DateTo, SiteId,
                            false, "", null, ref CashierSessionData, FloorDualCurrencyIsoCode);
    }

    static private void ReadCashierSessionData(SqlTransaction SQLTransaction,
                                               CashierMovementsFilter ReadType,
                                               Int64 CurrentSessionId,
                                               String SessionsQuery,
                                               DateTime DateFrom,
                                               DateTime DateTo,
                                               String SiteId,
                                               Boolean HideChipsAndCurrenciesConcepts,
                                               String IsoCode,
                                               DataTable CurrenciesTable,
                                               ref TYPE_CASHIER_SESSION_STATS CashierSessionData,
                                               String FloorDualCurrencyIsoCode)
    {
      DataTable _dt_cashier_movements_grouped;
      TYPE_SPLITS _split_data;
      String _str_tax;
      Decimal _aux_pct;
      String _query_filter;

      _query_filter = String.Empty;

      if (Split.ReadSplitParameters(out _split_data))
      {
        CashierSessionData.split_a_company_name = _split_data.company_a.company_name;
        CashierSessionData.split_a_name = _split_data.company_a.name;
        CashierSessionData.split_a_tax_name = _split_data.company_a.tax_name;
        CashierSessionData.split_a_tax_pct = _split_data.company_a.tax_pct;
        CashierSessionData.split_enabled_b = _split_data.enabled_b;
        CashierSessionData.split_b_company_name = _split_data.company_b.company_name;
        CashierSessionData.split_b_name = _split_data.company_b.name;
        CashierSessionData.split_b_tax_name = _split_data.company_b.tax_name;
        CashierSessionData.split_b_tax_pct = _split_data.company_b.tax_pct;
      }

      CashierSessionData.cash_in_tax_pct = GeneralParam.GetDecimal("Cashier", "CashInTax.Pct");

      //Taxes name
      CashierSessionData.prize_taxes_1_name = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name", SQLTransaction);
      CashierSessionData.prize_taxes_2_name = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name", SQLTransaction);
      CashierSessionData.prize_taxes_3_name = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name", SQLTransaction);

      //Taxes percentage
      _str_tax = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Pct", SQLTransaction);
      if (!String.IsNullOrEmpty(_str_tax))
      {
        Decimal.TryParse(Format.DBNumberToLocalNumber(_str_tax), out _aux_pct);
        CashierSessionData.report_prize_tax_1_pct = _aux_pct;
      }
      _str_tax = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Pct", SQLTransaction);
      if (!String.IsNullOrEmpty(_str_tax))
      {
        Decimal.TryParse(Format.DBNumberToLocalNumber(_str_tax), out _aux_pct);
        CashierSessionData.report_prize_tax_2_pct = _aux_pct;
      }
      _str_tax = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Pct", SQLTransaction);
      if (!String.IsNullOrEmpty(_str_tax))
      {
        Decimal.TryParse(Format.DBNumberToLocalNumber(_str_tax), out _aux_pct);
        CashierSessionData.report_prize_tax_3_pct = _aux_pct;
      }

      using (SqlCommand sql_command = new SqlCommand())
      {
        sql_command.CommandTimeout = Mailing.SQL_COMMAND_TIMEOUT;

        // Pseudo column 'SUB_TYPE' is introduced to distinguish movements
        // coming from each part of the union

        // AJQ 05-NOV-2012: Added the hint to force the index
        //                  Added "Union ALL" because all the registers are different (subtype)

        // ICS 04-JUL-2013: Added Initial, commissions and currency code columns for currency exchange movements

        switch (ReadType)
        {
          case CashierMovementsFilter.SESSION_ID:
            sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.NVarChar).Value = CurrentSessionId.ToString();
            sql_command.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DBNull.Value;
            sql_command.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DBNull.Value;

            break;

          case CashierMovementsFilter.SESSION_ID_BY_QUERY:
            sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.NVarChar).Value = SessionsQuery;
            sql_command.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DBNull.Value;
            sql_command.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DBNull.Value;

            break;

          case CashierMovementsFilter.DATE_RANGE:
          case CashierMovementsFilter.SESSIONS_FROM_MOVEMENTS:
          case CashierMovementsFilter.GAMING_DAY_RANGE:

            if (!GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
            {
              if (ReadType == CashierMovementsFilter.GAMING_DAY_RANGE || ReadType == CashierMovementsFilter.SESSIONS_FROM_MOVEMENTS)
              {
                if (String.IsNullOrEmpty(SessionsQuery))
                {
                  sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.NVarChar).Value = CurrentSessionId.ToString();
                }
                else
                {
                  sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.NVarChar).Value = SessionsQuery;
                }
              }
              else
              {
                sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.NVarChar).Value = DBNull.Value;
              }
            }
            else
            {
              if (SiteId == String.Empty)
              {
                sql_command.Parameters.Add("@pSiteId", SqlDbType.NVarChar).Value = DBNull.Value;
              }
              else
              {
                sql_command.Parameters.Add("@pSiteId", SqlDbType.NVarChar).Value = SiteId;
              }
            }

            if (ReadType != CashierMovementsFilter.GAMING_DAY_RANGE && DateFrom != DateTime.MinValue)
            {
              sql_command.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom;
            }
            else
            {
              sql_command.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DBNull.Value;
            }

            if (ReadType != CashierMovementsFilter.GAMING_DAY_RANGE && DateTo != DateTime.MaxValue)
            {
              sql_command.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateTo;
            }
            else
            {
              sql_command.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DBNull.Value;
            }

            ReadLiabilityData(SiteId, DateFrom, ref CashierSessionData.liabilities_from, CurrenciesTable, HideChipsAndCurrenciesConcepts, SQLTransaction);
            ReadLiabilityData(SiteId, DateTo, ref CashierSessionData.liabilities_to, CurrenciesTable, HideChipsAndCurrenciesConcepts, SQLTransaction);

            break;
        }

        if (!GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
        {
          sql_command.CommandText = "CashierMovementsGrouped_Read";
        }
        else
        {
          sql_command.CommandText = "MultiSite_CashierMovementsGroupedByHour";
        }

        sql_command.CommandType = CommandType.StoredProcedure;
        sql_command.CommandTimeout = 900; // 15 minutes
        sql_command.Connection = SQLTransaction.Connection;
        sql_command.Transaction = SQLTransaction;

        _dt_cashier_movements_grouped = new DataTable("CASHIER_MOVEMENTS_GROUPED");

        using (SqlDataAdapter _da = new SqlDataAdapter(sql_command))
        {
          _da.Fill(_dt_cashier_movements_grouped);
        }
      }

      if (HideChipsAndCurrenciesConcepts)
      {
        ArrayList _colum_names_to_apply_rate;

        _colum_names_to_apply_rate = new ArrayList();

        _colum_names_to_apply_rate.Add("CM_SUB_AMOUNT");
        _colum_names_to_apply_rate.Add("CM_ADD_AMOUNT");
        _colum_names_to_apply_rate.Add("CM_AUX_AMOUNT");
        _colum_names_to_apply_rate.Add("CM_INITIAL_BALANCE");
        _colum_names_to_apply_rate.Add("CM_FINAL_BALANCE");

        ApplyExchangeRatesToDtatable(_dt_cashier_movements_grouped, CurrenciesTable, _colum_names_to_apply_rate, out _dt_cashier_movements_grouped);
      }

      // DHA 09-FEB-2016: Floor Dual Currency filter movements
      if (!string.IsNullOrEmpty(FloorDualCurrencyIsoCode) && FloorDualCurrencyIsoCode != CurrencyExchange.GetNationalCurrency())
      {
        _query_filter = "CM_TYPE NOT IN (" + (Int32)CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1 + ", " + (Int32)CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2 + ")";
      }
      else
      {
        _query_filter = "CM_TYPE NOT IN (" + (Int32)CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY + ")";
      }

      if (_dt_cashier_movements_grouped.Rows.Count > 0)
      {
        if (!GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
        {
          CashierSessionData.m_cashier_session_has_pinpad_operations = PinPad.PinPadTransactions.GetCashierSessionHasPinPadOperation(CurrentSessionId, SessionsQuery, SQLTransaction);
        }
        CSReports.CashierSessionsReports.FillCashierSessionDataFromDt(_dt_cashier_movements_grouped.Select(_query_filter), ref CashierSessionData);
      }

      // LEM 03-DIC-2013: Commented code because this info are now inside FillCashierSessionDataFromDt
      //if (TITO.Utils.IsTitoMode)
      //{
      //  ReadTicketsBySession(SQLTransaction, CurrentSessionId, DateFrom, DateTo, ref CashierSessionData);
      //}

      //  EOR 20-JUN-2017
      if (Misc.IsBankCardCloseCashEnabled())
      {
        Withdrawal.GetPinpanMovementsSummaryCashDesk(CurrentSessionId, DateFrom, DateTo, ref CashierSessionData);
      }

      return;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Applying the conversion factor at tabel cashier movements passed by parameter
    // 
    //  PARAMS:
    //      - INPUT:
    //              - OriginalCashierMovements: Table with cashier movements
    //              - CurrenciesTable: Table with exchange rates
    //              - ColumsNameToApplyRate: Array with name columns to apply the conversion
    //
    //      - OUTPUT: 
    //              - ConvertedCashierMovements: Table with cashier movements calculated
    //
    // RETURNS: True: All okey. False: Otherwise.
    // 
    //   NOTES:
    //
    static private Boolean ApplyExchangeRatesToDtatable(DataTable OriginalCashierMovements, DataTable CurrenciesTable, ArrayList ColumsNameToApplyRate, out DataTable ConvertedCashierMovements)
    {
      DataTable _site_currencies_iso_code;
      DataTable _temp_currencies_table;
      DataSet _ds_currencies_tables;
      DataRelation _tables_relations;

      ConvertedCashierMovements = new DataTable();

      if (OriginalCashierMovements.Rows.Count <= 0 || CurrenciesTable.Rows.Count <= 0 || ColumsNameToApplyRate.Count <= 0)
      {
        return false;
      }

      if (!CurrencyMultisite.GetSiteCurrencies(out _site_currencies_iso_code))
      {
        return false;
      }

      _ds_currencies_tables = new DataSet();

      CurrenciesTable.TableName = "EXCHANGE_CURRENCIES";
      OriginalCashierMovements.TableName = "CASHIER_MOVEMENTS";
      _site_currencies_iso_code.TableName = "SITE_CURRENCIES";

      _ds_currencies_tables.Tables.Add(OriginalCashierMovements);
      _ds_currencies_tables.Tables.Add(_site_currencies_iso_code);

      _temp_currencies_table = CurrenciesTable.Copy();
      _ds_currencies_tables.Tables.Add(_temp_currencies_table);

      // DCS 25-JUN-2015
      // Because of in table CASHIER_MOVEMENTS the movements are saved with original currency (foreign currency) but the amount are converted 
      // to national currency, is necesary a new relation between movement's site and between that site and his currency average
      // Since we can not translate the foreign currency from site in the multisite.

      // That relation and his calculated column is used for move field average from table "EXCHANGE_CURRENCIES" to table "SITE_CURRENCIES"
      _tables_relations = new DataRelation("ISO_CODE", _ds_currencies_tables.Tables["EXCHANGE_CURRENCIES"].Columns["ER_ISO_CODE"], _ds_currencies_tables.Tables["SITE_CURRENCIES"].Columns["SC_ISO_CODE"], false);
      _ds_currencies_tables.Relations.Add(_tables_relations);
      _ds_currencies_tables.Tables["SITE_CURRENCIES"].Columns.Add("AVERAGE", Type.GetType("System.Decimal"), "ISNULL(Parent(ISO_CODE).AVERAGE,1)");

      // These relations and his calculated columns are used for apply exchange rates
      // That relation is used for get average from table "SITE_CURRENCIES" 
      _tables_relations = new DataRelation("SITE_ID", _ds_currencies_tables.Tables["SITE_CURRENCIES"].Columns["SC_SITE_ID"], _ds_currencies_tables.Tables["CASHIER_MOVEMENTS"].Columns["CM_SITE_ID"], false);
      _ds_currencies_tables.Relations.Add(_tables_relations);

      foreach (String _column_name in ColumsNameToApplyRate)
      {
        // These calculated columns are used for apply exchange rates with the change that we have previously
        _ds_currencies_tables.Tables["CASHIER_MOVEMENTS"].Columns[_column_name].ColumnName = _column_name + "_ORIGINAL";
        _ds_currencies_tables.Tables["CASHIER_MOVEMENTS"].AcceptChanges();
        _ds_currencies_tables.Tables["CASHIER_MOVEMENTS"].Columns.Add(_column_name, Type.GetType(_ds_currencies_tables.Tables["CASHIER_MOVEMENTS"].Columns[_column_name + "_ORIGINAL"].DataType.FullName), _column_name + "_ORIGINAL * Parent(SITE_ID).AVERAGE");
      }

      ConvertedCashierMovements.Load(_ds_currencies_tables.Tables["CASHIER_MOVEMENTS"].CreateDataReader());

      return true;
    }

    // PURPOSE: Get the Liability data from a date
    //          Will return the liability data by hour
    //  PARAMS:
    //      - INPUT:
    //          - SiteId
    //          - Date
    //          - Trx
    //
    //      - OUTPUT:
    //          - Liability
    // RETURNS: 
    // 
    static private bool ReadLiabilityData(String SiteId,
                                          DateTime Date,
                                          ref TYPE_LIABILITIES Liability,
                                          DataTable CurrenciesTable,
                                          Boolean HideChipsAndCurrenciesConcepts,
                                          SqlTransaction Trx)
    {
      StringBuilder _sb;
      DateTime _date;
      DataTable _dt_cashier_movements_grouped;
      SqlDataAdapter _da;

      Liability.has_data = false;

      try
      {
        if (Date == DateTime.MinValue || Date == DateTime.MaxValue)
        {
          return true;
        }

        _date = new DateTime(Date.Year, Date.Month, Date.Day, Date.Hour, 0, 0);

        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   CM_SUB_AMOUNT "); // RE_BALANCE || POINTS
        _sb.AppendLine("       , CM_ADD_AMOUNT "); // PROMO_RE_BALANCE
        _sb.AppendLine("       , CM_AUX_AMOUNT "); // PROMO_NR_BALANCE
        _sb.AppendLine("       , CM_TYPE ");
        _sb.AppendLine("       , CM_INITIAL_BALANCE ");
        _sb.AppendLine("       , CM_FINAL_BALANCE ");

        if (GeneralParam.GetBoolean("MultiSite", "IsCenter", false) & CurrencyMultisite.IsEnabledMultiCurrency())
        {
          _sb.AppendLine("     , CM_SITE_ID ");
        }

        _sb.AppendLine("  FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR ");
        _sb.AppendLine(" WHERE   ");

        if (GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
        {
          _sb.AppendLine(String.Format("         CM_SITE_ID in ({0}) AND ", SiteId));
        }

        _sb.AppendLine("         CM_DATE = @pDate ");
        _sb.AppendLine("   AND   CM_TYPE IN (@pTypeAccounts, @pTypeAccountsInSession, @pTypeTicket) ");
        _sb.AppendLine("   AND   CM_SUB_TYPE = 2 "); //0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = _date;
          _cmd.Parameters.Add("@pTypeAccounts", SqlDbType.Int).Value = (Int32)LIABILITY_MOVEMENTS.ACCOUNT;
          _cmd.Parameters.Add("@pTypeAccountsInSession", SqlDbType.Int).Value = (Int32)LIABILITY_MOVEMENTS.ACCOUNT_IN_SESSION;
          _cmd.Parameters.Add("@pTypeTicket", SqlDbType.Int).Value = (Int32)LIABILITY_MOVEMENTS.TICKET;

          _da = new SqlDataAdapter(_cmd);
          _dt_cashier_movements_grouped = new DataTable("CASHIER_MOVEMENTS_GROUPED");
          _da.Fill(_dt_cashier_movements_grouped);

          if (HideChipsAndCurrenciesConcepts)
          {
            ArrayList _colum_names_to_apply_rate;

            _colum_names_to_apply_rate = new ArrayList();

            _colum_names_to_apply_rate.Add("CM_SUB_AMOUNT");
            _colum_names_to_apply_rate.Add("CM_ADD_AMOUNT");
            _colum_names_to_apply_rate.Add("CM_AUX_AMOUNT");
            _colum_names_to_apply_rate.Add("CM_INITIAL_BALANCE");
            _colum_names_to_apply_rate.Add("CM_FINAL_BALANCE");

            ApplyExchangeRatesToDtatable(_dt_cashier_movements_grouped, CurrenciesTable, _colum_names_to_apply_rate, out _dt_cashier_movements_grouped);
          }

          Liability.datetime = _date;
          Liability.re_balance = 0;
          Liability.promo_re_balance = 0;
          Liability.promo_nr_balance = 0;
          Liability.in_session_re_to_gm = 0;
          Liability.in_session_promo_re_to_gm = 0;
          Liability.in_session_promo_nr_to_gm = 0;

          foreach (DataRow _dr in _dt_cashier_movements_grouped.Rows)
          {
            switch ((LIABILITY_MOVEMENTS)_dr["CM_TYPE"])
            {
              case LIABILITY_MOVEMENTS.ACCOUNT:
              case LIABILITY_MOVEMENTS.TICKET:
                Liability.re_balance += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                Liability.promo_re_balance += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                Liability.promo_nr_balance += Convert.ToDecimal(_dr["CM_AUX_AMOUNT"]);
                Liability.points = Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                Liability.num_accounts = Convert.ToInt32(_dr["CM_FINAL_BALANCE"]);
                break;

              case LIABILITY_MOVEMENTS.ACCOUNT_IN_SESSION:
                Liability.re_balance += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                Liability.promo_re_balance += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                Liability.promo_nr_balance += Convert.ToDecimal(_dr["CM_AUX_AMOUNT"]);

                Liability.in_session_re_to_gm = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                Liability.in_session_promo_re_to_gm = Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                Liability.in_session_promo_nr_to_gm = Convert.ToDecimal(_dr["CM_AUX_AMOUNT"]);
                Liability.num_accounts_in_session = Convert.ToInt32(_dr["CM_FINAL_BALANCE"]);
                break;

              default:
                break;
            }

            Liability.has_data = true;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }  // ReadLiabilityData

    //------------------------------------------------------------------------------
    // PURPOSE: Get the cashier terminal id from a specific terminal name.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SqlTrx
    //          - TerminalName
    //          - UserType
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public static Int32 ReadTerminalId(String TerminalName, GU_USER_TYPE UserType)
    {
      Int32 _terminal_id;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _terminal_id = ReadTerminalId(_db_trx.SqlTransaction, TerminalName, UserType);
        }

        return _terminal_id;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return 0;
    }

    public static Int32 ReadTerminalId(SqlTransaction SqlTrx, String TerminalName, GU_USER_TYPE UserType)
    {
      return ReadTerminalId(SqlTrx, TerminalName, UserType, true);
    }

    public static Int32 ReadTerminalId(SqlTransaction SqlTrx, String TerminalName, GU_USER_TYPE UserType, Boolean UseNewTrx)
    {
      Int32 _terminal_id;

      return ReadTerminalId(SqlTrx, TerminalName, UserType, UseNewTrx, out _terminal_id);
    }

    public static Int32 ReadTerminalId(SqlTransaction SqlTrx, String TerminalName, GU_USER_TYPE UserType, Boolean UseNewTrx, out Int32 TerminalId)
    {
      StringBuilder _sb;
      SqlParameter _sql_cashier_terminal_id;
      SqlParameter _sql_terminal_id;
      String _table_terminal;
      String _col_terminal_id;
      String _col_terminal_name;
      String _col_cashier_id;
      Boolean _has_relation_with_terminal;
      SqlTransaction _trx;
      DB_TRX _db_trx;

      _db_trx = null;

      TerminalId = 0;

      try
      {
        if (String.IsNullOrEmpty(TerminalName))
        {
          return 0;
        }

        switch (UserType)
        {
          case GU_USER_TYPE.USER:
          case GU_USER_TYPE.SYS_SYSTEM:
          default:
            _table_terminal = "";
            _col_terminal_id = "";
            _col_terminal_name = "";
            _col_cashier_id = "";
            _has_relation_with_terminal = false;
            break;

          case GU_USER_TYPE.SYS_ACCEPTOR:
          case GU_USER_TYPE.SYS_PROMOBOX:
          case GU_USER_TYPE.SYS_TITO:
            _table_terminal = "TERMINALS";
            _col_terminal_id = "TE_TERMINAL_ID";
            _col_terminal_name = "TE_NAME";
            _col_cashier_id = "CT_TERMINAL_ID";
            _has_relation_with_terminal = true;
            break;

          case GU_USER_TYPE.SYS_GAMING_TABLE:
            _table_terminal = "GAMING_TABLES";
            _col_terminal_id = "GT_GAMING_TABLE_ID";
            _col_terminal_name = "GT_NAME";
            _col_cashier_id = "CT_GAMING_TABLE_ID";
            _has_relation_with_terminal = true;
            break;

          case GU_USER_TYPE.SYS_REDEMPTION:
            _table_terminal = "COUNTR";
            _col_terminal_id = "CR_COUNTR_ID";
            _col_terminal_name = "CR_NAME";
            _col_cashier_id = "CT_COUNTR_ID";
            _has_relation_with_terminal = true;
            break;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CT_CASHIER_ID    ");
        _sb.AppendLine(String.Format("        , ISNULL({0}, 0) ", _has_relation_with_terminal ? _col_cashier_id : "NULL"));
        _sb.AppendLine("   FROM   CASHIER_TERMINALS ");
        _sb.AppendLine("  WHERE   CT_NAME = @pTerminalName ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = TerminalName;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              TerminalId = _reader.GetInt32(1);

              // Return CashierTerminalId
              return _reader.GetInt32(0);
            }
          }
        }

        // Clear StringBuilder
        _sb.Length = 0;

        if (!_has_relation_with_terminal)
        {
          _sb.AppendLine(" INSERT INTO CASHIER_TERMINALS ( CT_NAME ) VALUES ( @pTerminalName ) ");
          _sb.AppendLine(" SET @pCashierTerminalId = SCOPE_IDENTITY() ");
          _sb.AppendLine(" SET @pTerminalId = 0 ");
        }
        else
        {
          _sb.AppendLine(" UPDATE   CASHIER_TERMINALS ");
          _sb.AppendLine("    SET   CT_NAME = @pTerminalName ");
          _sb.AppendLine("   FROM   CASHIER_TERMINALS ");
          _sb.AppendLine(String.Format("  INNER   JOIN {0} ON {1} = {2} ", _table_terminal, _col_terminal_id, _col_cashier_id));
          _sb.AppendLine(String.Format("  WHERE   {0} = @pTerminalName ", _col_terminal_name));
          _sb.AppendLine("  ");
          _sb.AppendLine(" IF @@ROWCOUNT > 0 ");
          _sb.AppendLine(" BEGIN ");
          _sb.AppendLine("    SELECT   @pCashierTerminalId = CT_CASHIER_ID ");
          _sb.AppendLine(String.Format("           , @pTerminalId        = {0} ", _col_cashier_id));
          _sb.AppendLine("      FROM   CASHIER_TERMINALS ");
          _sb.AppendLine("     WHERE   CT_NAME = @pTerminalName ");
          _sb.AppendLine(" END ");
          _sb.AppendLine(" ELSE ");
          _sb.AppendLine(" BEGIN ");
          _sb.AppendLine(String.Format("    SELECT   @pTerminalId = {0} ", _col_terminal_id));
          _sb.AppendLine(String.Format("      FROM   {0} ", _table_terminal));
          _sb.AppendLine(String.Format("     WHERE   {0} = @pTerminalName ", _col_terminal_name));
          _sb.AppendLine("  ");
          _sb.AppendLine(String.Format("    INSERT INTO   CASHIER_TERMINALS ( CT_NAME, {0} ) ", _col_cashier_id));
          _sb.AppendLine("         VALUES   ( @pTerminalName, @pTerminalId ) ");
          _sb.AppendLine("            SET   @pCashierTerminalId = SCOPE_IDENTITY() ");
          _sb.AppendLine(" END ");
        }

        if (UseNewTrx)
        {
          _db_trx = new DB_TRX();
          _trx = _db_trx.SqlTransaction;
        }
        else
        {
          _trx = SqlTrx;
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.Connection, _trx))
        {
          _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = TerminalName;

          _sql_cashier_terminal_id = _cmd.Parameters.Add("@pCashierTerminalId", SqlDbType.Int);
          _sql_cashier_terminal_id.Direction = ParameterDirection.Output;

          _sql_terminal_id = _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int);
          _sql_terminal_id.Direction = ParameterDirection.Output;

          _cmd.ExecuteNonQuery();
        }

        if (UseNewTrx)
        {
          _db_trx.Commit();
        }

        if (_sql_terminal_id.Value != null && _sql_terminal_id.Value != DBNull.Value)
        {
          TerminalId = (Int32)_sql_terminal_id.Value;
        }

        return (Int32)_sql_cashier_terminal_id.Value;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return 0;
      }
      finally
      {
        if (_db_trx != null)
        {
          _db_trx.Dispose();
        }
      }

    }// ReadTerminalId

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current On Screen Keyboard mode configuration
    //
    //  PARAMS:
    //      - INPUT:
    //      - OUTPUT:
    // RETURNS: 
    //      - true: HideMode Enabled
    //      - false: HideMode Disable or Error ocurred
    //        
    //   NOTES :
    //
    public static Boolean ReadOSKMode()
    {
      Boolean _osk_mode;
      StringBuilder _sb;
      Object _obj;

      _osk_mode = false;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   CT_HIDE_OSK              ");
          _sb.AppendLine("  FROM   CASHIER_TERMINALS        ");
          _sb.AppendLine(" WHERE   CT_NAME = @pTerminalName ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = Environment.MachineName;
            _obj = _db_trx.ExecuteScalar(_cmd);
          }
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          _osk_mode = (Boolean)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _osk_mode = false;
      }

      return _osk_mode;
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Update On Screen Keyboard mode configuration
    //
    //  PARAMS:
    //      - INPUT:
    //           -Mode
    //
    //      - OUTPUT:
    // RETURNS: 
    //      - true: Update Successful
    //      - false: Error Updating
    //        
    //   NOTES :
    //
    public static Boolean UpdateOSKMode(Boolean Mode)
    {
      StringBuilder _sb;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("UPDATE   CASHIER_TERMINALS            ");
          _sb.AppendLine("   SET   CT_HIDE_OSK = @pOSKDisabled  ");
          _sb.AppendLine(" WHERE   CT_NAME     = @pTerminalName ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pOSKDisabled", SqlDbType.Bit).Value = Mode;
            _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = Environment.MachineName;

            if (_db_trx.ExecuteNonQuery(_cmd) != 1)
            {
              return false;
            }
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Get the current PinPad enabled status
    /// </summary>
    /// <returns></returns>
    public static Boolean ReadPinPadEnabled()
    {
      Boolean _pinpad_enabled;
      StringBuilder _sb;
      Object _obj;

      _pinpad_enabled = false;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   CT_PINPAD_ENABLED        ");
          _sb.AppendLine("  FROM   CASHIER_TERMINALS        ");
          _sb.AppendLine(" WHERE   CT_NAME = @pTerminalName ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = Environment.MachineName;
            _obj = _db_trx.ExecuteScalar(_cmd);
          }
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          _pinpad_enabled = (Boolean)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _pinpad_enabled = false;
      }

      return _pinpad_enabled;
    } // ReadPinPadEnabled

    /// <summary>
    /// Update PinPad enabled status
    /// </summary>
    /// <param name="Mode"></param>
    /// <returns></returns>
    public static Boolean UpdatePinPadEnabled(Boolean Mode)
    {
      StringBuilder _sb;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("UPDATE   CASHIER_TERMINALS                   ");
          _sb.AppendLine("   SET   CT_PINPAD_ENABLED = @pPinPadEnabled ");
          _sb.AppendLine(" WHERE   CT_NAME           = @pTerminalName  ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pPinPadEnabled", SqlDbType.Bit).Value = Mode;
            _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = Environment.MachineName;
            if (_db_trx.ExecuteNonQuery(_cmd) != 1)
            {
              return false;
            }
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdatePinPadEnabled

    public static Boolean IsTerminal3GS(long AccountId)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Is Terminal 3GS? and has non redemable?
          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   count(*)                                  ");
          _sb.AppendLine("   FROM   TERMINALS_3GS, ACCOUNTS                   ");
          _sb.AppendLine("  WHERE   T3GS_TERMINAL_ID = AC_CURRENT_TERMINAL_ID ");
          _sb.AppendLine("    AND   AC_ACCOUNT_ID    = @pAccount_id           ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pAccount_id", SqlDbType.BigInt).Value = AccountId;

            _obj = _cmd.ExecuteScalar();
          }
        }

        if (_obj != null)
        {
          if ((Int32)_obj == 0)
          {
            // Current terminal is not 3GS
            return false;
          }
          else
          {
            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Get the HostId 
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <returns></returns>
    public static Int32 GetHostId(Int32 TerminalId)
    {
      Int32 _hostId;
      StringBuilder _sb;
      Object _obj;

      _hostId = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   TE_TITO_HOST_ID               ");
          _sb.AppendLine("   FROM   TERMINALS                     ");
          _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
            _obj = _db_trx.ExecuteScalar(_cmd);
          }
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          _hostId = (Int32)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _hostId;
    } // GetHostId


    #region Enums Events Operations

    public enum EnumEventType
    {
      WCP_EVENT_TYPE_DEVICE_STATUS = 1,
      WCP_EVENT_TYPE_OPERATION = 2,
    }

    public enum EnumDeviceCode
    {
      COMMON_DEVICE_CODE_NO_DEVICE = 0,
      COMMON_DEVICE_CODE_PRINTER = 1,
      COMMON_DEVICE_CODE_NOTE_ACCEPTOR = 2,
      COMMON_DEVICE_CODE_SMARTCARD_READER = 3,
      COMMON_DEVICE_CODE_CUSTOMER_DISPLAY = 4,
      COMMON_DEVICE_CODE_COIN_ACCEPTOR = 5,
      COMMON_DEVICE_CODE_UPPER_DISPLAY = 6,
      COMMON_DEVICE_CODE_BAR_CODE_READER = 7,
      COMMON_DEVICE_CODE_INTRUSION = 8,
      COMMON_DEVICE_CODE_UPS = 9,
      //sle: Added or Removed Devices Codes 18/11/09
      //COMMON_DEVICE_CODE_LKAS_SOFTWARE_VERSION = 10,
      //COMMON_DEVICE_CODE_LKT_SOFTWARE_VERSION = 11,
      //COMMON_DEVICE_CODE_FLASH_1 = 12,
      //COMMON_DEVICE_CODE_FLASH_2 = 13,
      //COMMON_DEVICE_CODE_KIOSK = 14,
      COMMON_DEVICE_CODE_MODULE_IO = 15,
      //COMMON_DEVICE_CODE_AGENCY_CONNECTION = 16,

      COMMON_DEVICE_CODE_DISPLAY_MAIN = 17,
      COMMON_DEVICE_CODE_DISPLAY_TOP = 18,
      COMMON_DEVICE_CODE_TOUCH_PAD = 19,
      COMMON_DEVICE_CODE_KEYBOARD = 20,

      COMMON_DEVICE_CODE_UNICASH_KIT_LCD = 21,
      COMMON_DEVICE_CODE_UNICASH_KIT_KEYPAD = 22,
      COMMON_DEVICE_CODE_UNICASH_KIT_CARD_READER = 23,
      COMMON_DEVICE_CODE_UNICASH_KIT_BOARD = 24
    }

    public enum EnumOperationCode
    {
      COMMON_OPERATION_CODE_NO_OPERATION = 0,
      COMMON_OPERATION_CODE_START = 1,
      COMMON_OPERATION_CODE_SHUTDOWN = 2,
      COMMON_OPERATION_CODE_RESTART = 3,
      COMMON_OPERATION_CODE_TUNE_SCREEN = 4,
      COMMON_OPERATION_CODE_LAUNCH_EXPLORER = 5,
      COMMON_OPERATION_CODE_ASSIGN_KIOSK = 6,
      COMMON_OPERATION_CODE_DEASSIGN_KIOSK = 7,
      COMMON_OPERATION_CODE_UNLOCK_DOOR = 8,
      COMMON_OPERATION_CODE_LOGIN = 9,
      COMMON_OPERATION_CODE_LOGOUT = 10,
      COMMON_OPERATION_CODE_DISPLAY_SETTINGS = 11,
      COMMON_OPERATION_CODE_DOOR_OPENED = 12,
      COMMON_OPERATION_CODE_DOOR_CLOSED = 13,
      COMMON_OPERATION_CODE_BLOCK_KIOSK = 14,
      COMMON_OPERATION_CODE_UNBLOCK_KIOSK = 15,
      COMMON_OPERATION_CODE_JACKPOT_WON = 16,
      COMMON_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE = 17,
      COMMON_OPERATION_CODE_CALL_ATTENDANT = 18,
      COMMON_OPERATION_CODE_TEST_MODE_ENTER = 19,
      COMMON_OPERATION_CODE_TEST_MODE_LEAVE = 20,
      COMMON_OPERATION_CODE_LOGIN_ERROR = 21,
      COMMON_OPERATION_CODE_ATTENDANT_MENU_ENTER = 22,
      COMMON_OPERATION_CODE_ATTENDANT_MENU_EXIT = 23,
      COMMON_OPERATION_CODE_OPERATOR_MENU_ENTER = 24,
      COMMON_OPERATION_CODE_OPERATOR_MENU_EXIT = 25,
      COMMON_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED = 26,
      COMMON_OPERATION_CODE_HANDPAY_REQUESTED = 27,
      COMMON_OPERATION_CODE_HANDPAY_RESET = 28,
      COMMON_OPERATION_CODE_CARD_ABANDONED = 29,
      COMMON_OPERATION_CODE_SECURITY_CASHOUT_CENTS = 30,
      COMMON_OPERATION_CODE_TERMINAL_STATUS_CHANGE = 31,
      COMMON_OPERATION_CODE_EVENT = 32,
      COMMON_OPERATION_CODE_CHANGE_STACKER = 33,
      COMMON_OPERATION_CODE_SECURITY_CASHOUT_UNITS = 34,
      COMMON_OPERATION_CODE_GAME_FATAL_ERROR = 35,
      COMMON_OPERATION_CODE_PENDING_TRANSFER_IN = 36,
      COMMON_OPERATION_CODE_PENDING_TRANSFER_OUT = 37,
      COMMON_OPERATION_CODE_TECH_OFFLINE_INSERTED = 38,
      COMMON_OPERATION_CODE_TECH_OFFLINE_REMOVED = 39,
      COMMON_OPERATION_CODE_MEMORY_CORRUPTION_ONE = 40,
      COMMON_OPERATION_CODE_MEMORY_CORRUPTION_BOTH = 41,

      COMMON_OPERATION_CODE_TITO_WRONG_PRINTED_TICKET_ID = 45,
      COMMON_OPERATION_CODE_TITO_CANT_READ_TICKET_INFO = 46,

      COMMON_OPERATION_CODE_AFT_NOT_AUTHORIZED = 47,

      // OPERATION ID 48..64 reserved for Wigos Center
      COMMON_OPERATION_CODE_USER_BLOCKED = 65,
      COMMON_OPERATION_CODE_LOCK_AND_AFT_AMOUNT_MISMATCH = 66,
      COMMON_OPERATION_CODE_TICKET_NOT_EVEN_MULTIPLE_MACHINE_DENOM = 67,
      COMMON_OPERATION_CODE_HANDPAY_CANCEL_CREDITS = 68,
      COMMON_OPERATION_CODE_HAMDPAY_JACKPOT = 69,

      COMMON_OPERATION_CODE_BILL_IN_LIMIT_NOTE_ACCEPTOR_LOCK = 70,
      COMMON_OPERATION_CODE_BILL_IN_LIMIT_EGM_LOCK = 71,
      COMMON_OPERATION_CODE_BILL_IN_LIMIT_HANDPAY_LOCK = 72,
    }

    public enum EnumEventSeverity
    {
      COMMON_SEVERITY_OK = 0,
      COMMON_SEVERITY_WARNING = 1,
      COMMON_SEVERITY_ERROR = 2,
    }
    public enum EnumDeviceStatus
    {
      //COMMON
      COMMON_DEVICE_STATUS_UNKNOWN = 0,
      COMMON_DEVICE_STATUS_OK = 1,
      COMMON_DEVICE_STATUS_ERROR = 2,
      COMMON_DEVICE_STATUS_NOT_INSTALLED = 100,
      // Printer status
      COMMON_DEVICE_STATUS_PRINTER_OFF = 3,
      COMMON_DEVICE_STATUS_PRINTER_READY = 4,
      COMMON_DEVICE_STATUS_PRINTER_NOT_READY = 5,
      COMMON_DEVICE_STATUS_PRINTER_PAPER_OUT = 6,
      COMMON_DEVICE_STATUS_PRINTER_PAPER_LOW = 7,
      COMMON_DEVICE_STATUS_PRINTER_OFFLINE = 8,
      // Note acceptor status
      COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_JAM = 3,
      COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR = 4,
      COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL = 5,
      COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN = 6,
      // Ups status
      COMMON_DEVICE_STATUS_UPS_NO_AC = 3,
      COMMON_DEVICE_STATUS_UPS_BATTERY_LOW = 4,
      COMMON_DEVICE_STATUS_UPS_OVERLOAD = 5,
      COMMON_DEVICE_STATUS_UPS_OFF = 6,
      COMMON_DEVICE_STATUS_UPS_BATTERY_FAIL = 7,
      // Smartcard status
      COMMON_DEVICE_STATUS_SMARTCARD_REMOVED = 3,
      COMMON_DEVICE_STATUS_SMARTCARD_INSERTED = 4,
      // Coin acceptor status
      COMMON_DEVICE_STATUS_COIN_ACCEPTOR_JAM = 3,
      COMMON_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR = 4,
      COMMON_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL = 5,
    }

    #endregion Enums Events Operations

    //------------------------------------------------------------------------------
    // PURPOSE : Get event description.
    //
    //  PARAMS :
    //      - INPUT :
    //          - EventType
    //          - DeviceCode
    //          - DeviceStatus
    //          - EventSeverity
    //          - OperationCode
    //          - OperationData
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Description.
    //
    public static String EvOpGetDescription(EnumEventType EventType,
                                            EnumDeviceCode DeviceCode,
                                            EnumDeviceStatus DeviceStatus,
                                            EnumEventSeverity EventSeverity,
                                            EnumOperationCode OperationCode,
                                            Object OperationData,
                                            Int64 TechnicalAccoundID,
                                            Int16 TechMode,
                                            Int64 AccountId = 0,
                                            String Account_name = "",
                                            DateTime Ps_started = new DateTime())
    {
      String event_description;
      event_description = "";

      try
      {

        switch (EventType)
        {
          case EnumEventType.WCP_EVENT_TYPE_DEVICE_STATUS:
            switch (EventSeverity)
            {
              case EnumEventSeverity.COMMON_SEVERITY_OK:
                event_description += Resource.String("STR_FRM_CONTAINER_SEVERITY_OK");
                if (event_description == " ")
                {
                  event_description = "";
                }
                break;
              case EnumEventSeverity.COMMON_SEVERITY_WARNING:
                event_description += Resource.String("STR_FRM_CONTAINER_SEVERITY_WARNING") + ". ";
                break;
              case EnumEventSeverity.COMMON_SEVERITY_ERROR:
                event_description += Resource.String("STR_FRM_CONTAINER_SEVERITY_ERROR") + ". ";
                break;
            }

            switch (DeviceCode)
            {
              case EnumDeviceCode.COMMON_DEVICE_CODE_PRINTER:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_PRINTER") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_PRINTER_OFF:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_PRINTER_OFFLINE:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_PRINTER_OFFLINE"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_PRINTER_NOT_READY:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_PRINTER_NOT_READY"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_PRINTER_READY:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_PRINTER_READY"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_PRINTER_PAPER_OUT:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_PRINTER_PAPER_OUT"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_PRINTER_PAPER_LOW:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_PRINTER_PAPER_LOW"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_NOTE_ACCEPTOR:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_NOTE_ACCEPTOR") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_JAM:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOTE_ACCEPTOR_JAM"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_SMARTCARD_READER:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_SMARTCARD_READER") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_CUSTOMER_DISPLAY:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_CUSTOMER_DISPLAY") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_UPPER_DISPLAY:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_UPPER_DISPLAY") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_BAR_CODE_READER:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_BAR_CODE_READER") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_UPS:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_UPS") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UPS_NO_AC:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UPS_NO_AC"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UPS_BATTERY_LOW:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UPS_BATTERY_LOW"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UPS_OVERLOAD:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UPS_OVERLOAD"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UPS_OFF:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UPS_OFF"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UPS_BATTERY_FAIL:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UPS_BATTERY_FAIL"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              //case EnumDeviceCode.COMMON_DEVICE_CODE_LKAS_SOFTWARE_VERSION:
              //  event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_LKAS_SOFTWARE_VERSION") + ". ";
              //  switch (DeviceStatus)
              //  {
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK");  break;
              //  }
              //  break;

              //case EnumDeviceCode.COMMON_DEVICE_CODE_LKT_SOFTWARE_VERSION:
              //  event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_LKT_SOFTWARE_VERSION") + ". ";
              //  switch (DeviceStatus)
              //  {
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK");  break;
              //  }
              //  break;

              //case EnumDeviceCode.COMMON_DEVICE_CODE_FLASH_1:
              //  event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_FLASH_1") + ". ";
              //  switch (DeviceStatus)
              //  {
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK");  break;
              //  }
              //  break;

              //case EnumDeviceCode.COMMON_DEVICE_CODE_FLASH_2:
              //  event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_FLASH_2") + ". ";
              //  switch (DeviceStatus)
              //  {
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK");  break;
              //  }
              //  break;

              //case EnumDeviceCode.COMMON_DEVICE_CODE_KIOSK:
              //  event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_KIOSK") + ". ";
              //  switch (DeviceStatus)
              //  {
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_KIOSK_ACTIVE:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_KIOSK_ACTIVE");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_KIOSK_NOT_ACTIVE:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_KIOSK_NOT_ACTIVE");  break;
              //  }
              //  break;

              //case EnumDeviceCode.COMMON_DEVICE_CODE_AGENCY_CONNECTION:
              //  event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_AGENCY_CONNECTION") + ". ";
              //  switch (DeviceStatus)
              //  {
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_CONNECTED:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_CONNECTED");  break;
              //    case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_CONNECTED:
              //      event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_CONNECTED");  break;
              //  }
              //  break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_MODULE_IO:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_MODULE_IO") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_UNICASH_KIT_LCD:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_UNICASH_KIT_LCD") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_UNICASH_KIT_KEYPAD:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_UNICASH_KIT_KEYPAD") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_UNICASH_KIT_CARD_READER:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_UNICASH_KIT_CARD_READER") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_UNICASH_KIT_BOARD:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_UNICASH_KIT_BOARD") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_COIN_ACCEPTOR:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_COIN_ACCEPTOR") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_COIN_ACCEPTOR_JAM:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOTE_ACCEPTOR_JAM"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_INTRUSION:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_INTRUSION") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_INTURSION_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_DISPLAY_MAIN:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_DISPLAY_MAIN") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_DISPLAY_TOP:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_DISPLAY_TOP") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_TOUCH_PAD:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_TOUCH_PAD") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

              case EnumDeviceCode.COMMON_DEVICE_CODE_KEYBOARD:
                event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_CODE_KEYBOARD") + ". ";
                switch (DeviceStatus)
                {
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_ERROR:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_ERROR"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_UNKNOWN:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_UNKNOWN"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_NOT_INSTALLED:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_NOT_INSTALLED"); break;
                  case EnumDeviceStatus.COMMON_DEVICE_STATUS_OK:
                    event_description += Resource.String("STR_FRM_CONTAINER_DEVICE_STATUS_OK"); break;
                }
                break;

            }
            break;
          case EnumEventType.WCP_EVENT_TYPE_OPERATION:
            switch (OperationCode)
            {
              case EnumOperationCode.COMMON_OPERATION_CODE_START:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_STARTED");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_SHUTDOWN:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_SHUTDOWN");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_RESTART:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_RESTART");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_LAUNCH_EXPLORER:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_LAUNCH_EXPLORER");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_TUNE_SCREEN:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TUNE_SCREEN");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_ASSIGN_KIOSK:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_ASSIGN_KIOSK", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_DEASSIGN_KIOSK:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_DEASSIGN_KIOSK", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_UNLOCK_DOOR:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_UNLOCK_DOOR");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_LOGIN:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_LOGIN", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_LOGOUT:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_LOGOUT", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_DISPLAY_SETTINGS:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_DISPLAY_SETTINGS");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_DOOR_OPENED:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_DOOR_OPENED", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_DOOR_CLOSED:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_DOOR_CLOSED", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_BLOCK_KIOSK:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_BLOCK_KIOSK");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_UNBLOCK_KIOSK:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_UNBLOCK_KIOSK");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_JACKPOT_WON:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_JACKPOT_WON", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_KIOSK_BLOCKED_HIGH_PRIZE", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_CALL_ATTENDANT:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_CALL_ATTENDANT");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_TEST_MODE_ENTER:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TEST_MODE_ENTER");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_TEST_MODE_LEAVE:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TEST_MODE_LEAVE");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_LOGIN_ERROR:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_LOGIN_ERROR");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_ATTENDANT_MENU_ENTER:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_ATTENDANT_MENU_ENTER");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_ATTENDANT_MENU_EXIT:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_ATTENDANT_MENU_EXIT");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_OPERATOR_MENU_ENTER:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_OPERATOR_MENU_ENTER");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_OPERATOR_MENU_EXIT:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_OPERATOR_MENU_EXIT");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_CONFIG_SETTINGS_MODIFIED");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_HANDPAY_REQUESTED:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_HANDPAY_REQUESTED", Decimal.Parse(OperationData.ToString()) / 100);
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_HANDPAY_RESET:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_HANDPAY_RESET", Decimal.Parse(OperationData.ToString()) / 100);
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_CARD_ABANDONED:
                if (TITO.Utils.IsTitoMode())
                {
                  event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TITO_CARD_ABANDONED", OperationData.ToString());
                }
                else
                {
                  event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_CARD_ABANDONED", OperationData.ToString());
                }
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_SECURITY_CASHOUT_CENTS:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_SECURITY_CASHOUT", Decimal.Parse(OperationData.ToString()) / 100);
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_SECURITY_CASHOUT_UNITS:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_SECURITY_CASHOUT", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_EVENT:
                UInt32 _code;
                UInt32.TryParse(OperationData.ToString(), out _code);
                event_description += Resource.String("STR_EA_0x" + _code.ToString("X").PadLeft(8, '0'));
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_CHANGE_STACKER:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_STACKER_CHANGED", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_GAME_FATAL_ERROR:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_GAME_FATAL_ERROR", OperationData.ToString());
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_PENDING_TRANSFER_IN:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PENDING_TRANSFER_IN", Decimal.Parse(OperationData.ToString()) / 100);
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_PENDING_TRANSFER_OUT:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PENDING_TRANSFER_OUT", Decimal.Parse(OperationData.ToString()) / 100);
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_TECH_OFFLINE_INSERTED:
                if (Decimal.Parse(OperationData.ToString()) == 0)
                {
                  event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TECH_OFFLINE_INSERTED");
                }
                else
                {
                  event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TECH_OFFLINE_INSERTED_WITH_CARD");
                }
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_TECH_OFFLINE_REMOVED:
                event_description += Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TECH_OFFLINE_REMOVED");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_MEMORY_CORRUPTION_ONE:
                event_description += Resource.String("STR_ALARM_MEMORY_CORRUPTION_ONE");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_MEMORY_CORRUPTION_BOTH:
                event_description += Resource.String("STR_ALARM_MEMORY_CORRUPTION_BOTH");
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_TITO_WRONG_PRINTED_TICKET_ID:
                event_description += String.Format(Resource.String("STR_ALARM_TITO_WRONG_PRINTED_TICKET_ID"), TechnicalAccoundID);
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_TITO_CANT_READ_TICKET_INFO:
                {
                  string _str_data;

                  _str_data = String.Format("Ticket: {0} - Amount: {1} - ValidationType: {2}", TechnicalAccoundID, OperationData.ToString(), TechMode);

                  event_description += String.Format(Resource.String("STR_ALARM_TITO_CANT_READ_TICKET_INFO"), _str_data);
                }
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_AFT_NOT_AUTHORIZED:
                {
                  string _in_out;

                  _in_out = "UNKNOWN";

                  switch (TechMode)
                  {
                    case 0:
                      _in_out = "ToEgm";
                      break;
                    case 1:
                      _in_out = "FromEgm";
                      break;

                    default:
                      break;
                  }

                  event_description += String.Format(Resource.String("STR_ALARM_AFT_NOT_AUTHORIZED"), Decimal.Parse(OperationData.ToString()) / 100, _in_out, TechnicalAccoundID,
                                                     AccountId.ToString(), Account_name, Ps_started.ToString());
                }
                break;
              case EnumOperationCode.COMMON_OPERATION_CODE_LOCK_AND_AFT_AMOUNT_MISMATCH:
                {
                  event_description += String.Format(Resource.String("STR_ALARM_LOCK_AFT_MISMATCH"), Decimal.Parse(OperationData.ToString()) / 100);
                }
                break;

              case EnumOperationCode.COMMON_OPERATION_CODE_TICKET_NOT_EVEN_MULTIPLE_MACHINE_DENOM:
                {
                  event_description += String.Format(Resource.String("STR_ALARM_TICKET_NOT_EVEN_MULTIPLE_MACHINE_DENOM"), Decimal.Parse(OperationData.ToString()) / 100);
                }
                break;

              case EnumOperationCode.COMMON_OPERATION_CODE_BILL_IN_LIMIT_NOTE_ACCEPTOR_LOCK:
                {
                  event_description += String.Format(Resource.String("STR_ALARM_BILL_IN_LIMIT_EXCEEDED_NOTE_ACCEPTOR_LOCK"), (Currency)Decimal.Parse((OperationData).ToString()));
                }
                break;

              case EnumOperationCode.COMMON_OPERATION_CODE_BILL_IN_LIMIT_EGM_LOCK:
                {
                  event_description += String.Format(Resource.String("STR_ALARM_BILL_IN_LIMIT_EXCEEDEDT_EGM_LOCK"), (Currency)Decimal.Parse((OperationData).ToString()));
                }
                break;

              case EnumOperationCode.COMMON_OPERATION_CODE_BILL_IN_LIMIT_HANDPAY_LOCK:
                {
                  event_description += String.Format(Resource.String("STR_ALARM_BILL_IN_LIMIT_EXCEEDED_HANDPAY_LOCK"), (Currency)Decimal.Parse((OperationData).ToString()));
                }
                break;
            }
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Cashier.EvOpGetDescription EventType: {0} | DeviceCode: {1} | DeviceStatus: {2} | OperationCode: {3} OperationData: {4} {5}", EventType, DeviceCode, DeviceStatus, OperationCode, OperationData, OperationData.GetType()));
        Log.Error("Cashier.EvOpGetDescription " + _ex.Message);
      }
      return event_description;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the value of the GeneralParam Cashier.CoverCouponPct. Deprecated (must be deleted)
    // 
    //  PARAMS:
    //      - INPUT:
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Decimal
    // 
    public static Decimal GetCoverCouponPct()
    {
      String _cover_coupon_pct;
      Decimal _cover_coupon_pct_value;

      _cover_coupon_pct_value = 0;

      try
      {
        _cover_coupon_pct = Misc.ReadGeneralParams("Cashier", "CoverCouponPct");

        if (_cover_coupon_pct != null)
        {
          _cover_coupon_pct_value = Convert.ToDecimal(Format.DBNumberToLocalNumber(_cover_coupon_pct));
        }
      }
      catch
      {
        // Catch any wrong formatted value
      }

      return _cover_coupon_pct_value;
    } // GetCoverCouponPct

    //------------------------------------------------------------------------------
    // PURPOSE: Clear NR2 for this operation
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Decimal
    // 
    public static void ClearNR2()
    {
      m_nr2_pct_random_value = 0;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Not RedeemableToAdd from Cover Coupon
    // 
    //  PARAMS:
    //      - INPUT:
    //          AccountId
    //          RedeemableToAdd
    //
    //      - OUTPUT:
    //          Nr2Value
    //          Nr2Pct
    //
    // RETURNS: 
    //      - Decimal
    // 
    public static Boolean GetNR2(Int64 AccountId, Decimal RedeemableToAdd, out Decimal Nr2Value, out Decimal Nr2Pct)
    {
      ////  * NEW CODE
      String _str_value;
      Boolean _allow_add_cover_coupon;

      Decimal _range_ini;
      Decimal _range_fin;
      Int32 _range_ini_int;
      Int32 _range_fin_int;
      Int32 _result_range_rnd;

      Random _random_value;

      Nr2Pct = 0;
      Nr2Value = 0;
      _allow_add_cover_coupon = false;

      try
      {

        if (GeneralParam.GetBoolean("Cashier.CoverCoupon", "Enabled"))
        {
          //Check if allows only one CouponCover by day.
          if (GeneralParam.GetBoolean("Cashier.CoverCoupon", "OnlyFirstDailyRecharge") && (AccountId > 0))
          {
            if (Accounts.SumTodayCashIn(AccountId) == 0) // There's no recharges so we can allow cover coupon
            {
              _allow_add_cover_coupon = true;
            }
          }
          else //if not, allows one cover coupon by recharge.
          {
            _allow_add_cover_coupon = true;
          }

          if (_allow_add_cover_coupon == true)
          {
            //Allows Cover Coupon

            //Get GP RangePercentage (by value (only one) )
            Nr2Pct = GeneralParam.GetDecimal("Cashier.CoverCoupon", "RangePercentage", -1);

            if (Nr2Pct <= 0 || Nr2Pct > 100)
            {
              if (m_nr2_pct_random_value == 0)
              {
                //Get GP RangePercentage (by range)
                GeneralParam.GetDecimalRange("Cashier.CoverCoupon", "RangePercentage", out _range_ini, out _range_fin, -1, -1);
                if ((_range_ini > 0 && _range_fin <= 100) && (_range_ini <= _range_fin))
                {
                  //Range OK. Generate Random for getting value
                  _range_ini = _range_ini * 10000;
                  _range_fin = _range_fin * 10000;

                  _range_ini_int = Convert.ToInt32(_range_ini);
                  _range_fin_int = Convert.ToInt32(_range_fin);

                  //Get value from Random
                  _random_value = new Random(Environment.TickCount);
                  _result_range_rnd = _random_value.Next(_range_ini_int, _range_fin_int);

                  Nr2Pct = Convert.ToDecimal(_result_range_rnd) / 10000;
                }
                else
                {
                  //Error 
                  return false;
                }
              }
              else
              {
                Nr2Pct = m_nr2_pct_random_value;
              }
            }
            if (Nr2Pct > 0 && Nr2Pct <= 100)
            {
              //Calculate NR coverCoupon with % from GP (Nr2Pct) to obtain Not redimible amount (Nr2Value)
              _str_value = Nr2Pct.ToString();
              Nr2Pct = Math.Max(0, Convert.ToDecimal(Format.DBNumberToLocalNumber(_str_value)));
              Nr2Value = Math.Truncate(RedeemableToAdd * Nr2Pct) / 100;
            }
          }

          m_nr2_pct_random_value = Nr2Pct;

          return true;
        }
        else
        {
          //GP Cashier.CoverCoupon disabled
          return true;
        }
      }

      catch
      {
        // Catch any wrong formatted value
      }

      return false;

      ////  * OLD CODE
      ////////String _str_value;

      ////////Nr2Pct = 0;
      ////////Nr2Value = 0;

      ////////try
      ////////{
      ////////  _str_value = Misc.ReadGeneralParams("Cashier", "CoverCouponPct");
      ////////  if (!String.IsNullOrEmpty(_str_value))
      ////////  {
      ////////    Nr2Pct = Math.Max(0, Convert.ToDecimal(Format.DBNumberToLocalNumber(_str_value)));
      ////////    Nr2Value = Math.Truncate(RedeemableToAdd * Nr2Pct) / 100;

      ////////    return true;
      ////////  }
      ////////}


      ////////catch
      ////////{
      ////////  // Catch any wrong formatted value
      ////////}

      ////////return false;
    } // GetNR2

    //------------------------------------------------------------------------------
    // PURPOSE: Perform the complete Close Cash process
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CashierSessionId
    //          - TerminalId
    //          - TerminalName
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - None
    // 
    //   NOTES:
    //    This process involves the following steps:
    //    - Update mobile bank records associated to the Cashier Session (this update blocks 
    //      any attempt to perform additional movements)
    //    - Update cashier session status and closing timestamp
    //    - Update cashier session balance
    //    - Generate cashier session closing movement
    //    - Generate close session voucher
    //
    public static Boolean CashierSessionCloseById(Int64 CashierSessionId, Int32 TerminalId, String TerminalName, SqlTransaction Trx)
    {
      String _sql_txt;

      Currency _collected_amount_value;

      //Currency _final_balance;
      Cashier.TYPE_CASHIER_SESSION_STATS _close_session_stats;
      ArrayList _voucher_list;
      SortedDictionary<CurrencyIsoType, Decimal> _collected_amount;

      CurrencyIsoType _iso_type;

      Int32 _user_id;
      String _user_name;
      String _national_currency;
      Decimal _collected_diff;
      Decimal _collected;

      // Initializations
      _close_session_stats = new Cashier.TYPE_CASHIER_SESSION_STATS();
      _national_currency = CurrencyExchange.GetNationalCurrency();
      _collected_amount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      _collected_amount_value = 0;

      _iso_type = new CurrencyIsoType();
      _iso_type.IsoCode = _national_currency;
      _iso_type.Type = CurrencyExchangeType.CURRENCY;
      _collected_amount.Add(_iso_type, 0);

      _user_id = -1;
      _user_name = "";

      try
      {
        _sql_txt = "SELECT   GU_USER_ID                         " +
                  "        , GU_USERNAME                        " +
                  "        , CS_STATUS                          " +
                  "        , CS_BALANCE                         " +
                  "   FROM   CASHIER_SESSIONS                   " +
                  "          INNER JOIN GUI_USERS               " +
                  "          ON GU_USER_ID = CS_USER_ID         " +
                  "  WHERE   CS_SESSION_ID = @pCashierSessionId ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = CashierSessionId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            if (_reader.GetInt32(2) == (Int32)CASHIER_SESSION_STATUS.CLOSED)
            {
              return true;
            }

            _user_id = _reader.GetInt32(0);
            _user_name = _reader.GetString(1);
            _collected_amount[_iso_type] = _reader.GetDecimal(3);

            _collected_amount_value = _reader.GetDecimal(3);
          }
        }

        // AJQ 08-SEP-2007  
        //  - The balance is read from the database instead of the label
        //  - Update the status to close

        _sql_txt = " UPDATE   CASHIER_SESSIONS                             " +
                   "    SET   CS_CLOSING_DATE      = GETDATE()             " +
                   "        , CS_STATUS            = @pStatusClosed        " +
                   "        , CS_COLLECTED_AMOUNT  = CS_BALANCE            " +
                   "  WHERE   CS_SESSION_ID        = @pSessionId           " +
                   "    AND   CS_STATUS           IN ( @pStatusOpen        " +
                   "                                 , @pStatusOpenPending " +
                   "                                 , @pStatusPending )   ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.CLOSED;
          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          _sql_cmd.Parameters.Add("@pStatusOpenPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN_PENDING;
          _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING;

          _sql_cmd.ExecuteNonQuery();
        }

        Cashier.ReadCashierSessionData(Trx, CashierSessionId, ref _close_session_stats);

        CommonCashierInformation.SetCashierInformation(CashierSessionId, _user_id, _user_name, TerminalId, TerminalName);

        // Insert voucher.
        _close_session_stats.collected = _collected_amount;

        // ICS 04-JUL-2013: Addapted for currency exchange functionality
        foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency_balance in _close_session_stats.currencies_balance)
        {
          _collected = _collected_amount.ContainsKey(_currency_balance.Key) ? _collected_amount[_currency_balance.Key] : 0;

          _collected_diff = _collected - _currency_balance.Value;
          _close_session_stats.collected_diff.Add(_currency_balance.Key, _collected_diff);
        }

        _voucher_list = VoucherBuilder.CashDeskClose(_close_session_stats,
                                                     0,   //OperationId 
                                                     PrintMode.Print,
                                                     Trx,
                                                     Cage.IsGamingTable(CashierSessionId));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // CashierSessionCloseById

    #region MobileBankAndGUIUserTypes

    public static MB_USER_TYPE GetMobileBankUserType(GU_USER_TYPE UserType)
    {
      Boolean _is_acceptor;

      return GetMobileBankUserType(UserType, out _is_acceptor);
    }

    public static MB_USER_TYPE GetMobileBankUserType(TerminalTypes TerminalType)
    {
      MB_USER_TYPE _mb_user_type;
      GU_USER_TYPE _gui_user_type;

      if (!GetMobileBankAndGUIUserType(TerminalType, out _mb_user_type, out _gui_user_type))
      {
        return MB_USER_TYPE.NOT_ASSIGNED;
      }

      return _mb_user_type;
    }

    public static Boolean GetMobileBankAndGUIUserType(TerminalTypes TerminalType, out MB_USER_TYPE MbUserType, out GU_USER_TYPE GuiUserType)
    {
      switch (TerminalType)
      {
        case TerminalTypes.SAS_HOST:
        case TerminalTypes.T3GS:
          MbUserType = TITO.Utils.IsTitoMode() ? MB_USER_TYPE.SYS_TITO : MB_USER_TYPE.SYS_ACCEPTOR;
          GuiUserType = TITO.Utils.IsTitoMode() ? GU_USER_TYPE.SYS_TITO : GU_USER_TYPE.SYS_ACCEPTOR;
          return true;

        case TerminalTypes.PROMOBOX:
          MbUserType = MB_USER_TYPE.SYS_PROMOBOX;
          GuiUserType = Misc.GetUserTypePromobox();
          return true;

        default:
          MbUserType = MB_USER_TYPE.SYS_ACCEPTOR;
          GuiUserType = GU_USER_TYPE.SYS_ACCEPTOR;
          return false;
      }
    }

    public static MB_USER_TYPE GetMobileBankUserType(GU_USER_TYPE UserType, out Boolean IsAcceptor)
    {
      MB_USER_TYPE _mb_user_type;
      Boolean _is_system_user;

      GetGUIUserInfo(UserType, out _mb_user_type, out IsAcceptor, out _is_system_user);

      return _mb_user_type;

    } // GetMobileBankUserType

    public static void GetGUIUserInfo(GU_USER_TYPE UserType, out MB_USER_TYPE MbUserType, out Boolean IsAcceptor, out Boolean IsSystemUser)
    {
      switch (UserType)
      {
        case GU_USER_TYPE.SYS_ACCEPTOR:
          MbUserType = MB_USER_TYPE.SYS_ACCEPTOR;
          IsAcceptor = true;
          IsSystemUser = true;
          break;

        case GU_USER_TYPE.SYS_PROMOBOX:
          MbUserType = MB_USER_TYPE.SYS_PROMOBOX;
          IsAcceptor = true;
          IsSystemUser = true;
          break;

        case GU_USER_TYPE.SYS_TITO:
          MbUserType = MB_USER_TYPE.SYS_TITO;
          IsAcceptor = true;
          IsSystemUser = true;
          break;

        case GU_USER_TYPE.SYS_SYSTEM:
        case GU_USER_TYPE.SYS_REDEMPTION:
        case GU_USER_TYPE.SYS_GAMING_TABLE:
          MbUserType = MB_USER_TYPE.NOT_ASSIGNED;
          IsAcceptor = false;
          IsSystemUser = true;
          break;

        default:
          MbUserType = MB_USER_TYPE.NOT_ASSIGNED;
          IsAcceptor = false;
          IsSystemUser = false;
          break;
      }
    }

    public static GU_USER_TYPE GetGUIUserType(MB_USER_TYPE MBUserType)
    {
      String _user_name;

      return GetGUIUserType(MBUserType, out _user_name);
    }

    public static GU_USER_TYPE GetGUIUserType(MB_USER_TYPE MBUserType, out String UserName)
    {
      Boolean _is_acceptor;

      return GetGUIUserType(MBUserType, out UserName, out _is_acceptor);
    }

    public static GU_USER_TYPE GetGUIUserType(MB_USER_TYPE MBUserType, out Boolean IsAcceptor)
    {
      String _user_name;

      return GetGUIUserType(MBUserType, out _user_name, out IsAcceptor);
    }

    public static Boolean MobileBankIsAcceptor(MB_USER_TYPE MBUserType)
    {
      Boolean _is_acceptor;
      String _user_name;

      GetGUIUserType(MBUserType, out _user_name, out _is_acceptor);

      return _is_acceptor;
    }

    public static GU_USER_TYPE GetGUIUserType(MB_USER_TYPE MBUserType, out String UserName, out Boolean IsAcceptor)
    {
      switch (MBUserType)
      {
        case MB_USER_TYPE.SYS_ACCEPTOR:
          UserName = "SYS-Validator";
          IsAcceptor = true;
          return GU_USER_TYPE.SYS_ACCEPTOR;

        case MB_USER_TYPE.SYS_PROMOBOX:
          UserName = "SYS-PromoBox";
          IsAcceptor = true;
          return Misc.GetUserTypePromobox();

        case MB_USER_TYPE.SYS_TITO:
          UserName = "SYS-TITO";
          IsAcceptor = true;
          return GU_USER_TYPE.SYS_TITO;

        case MB_USER_TYPE.PERSONAL:
        case MB_USER_TYPE.ANONYMOUS:
          UserName = "";
          IsAcceptor = false;
          return GU_USER_TYPE.USER;

        default:
          UserName = "";
          IsAcceptor = false;
          return GU_USER_TYPE.NOT_ASSIGNED;
      }
    } // GetGUIUserType

    #endregion MobileBankAndGUIUserTypes

    #region SystemCashierSession

    public static CashierSessionInfo GetSystemCashierSessionInfo(GU_USER_TYPE UserType)
    {
      CashierSessionInfo _session;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _session = GetSystemCashierSessionInfo(UserType, _db_trx.SqlTransaction, "");
          if (_session != null)
          {
            _db_trx.Commit();

            return _session;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;
    } // GetSystemCashierSessionInfo

    public static CashierSessionInfo GetSystemCashierSessionInfo(GU_USER_TYPE UserType, SqlTransaction SqlTrx)
    {
      return GetSystemCashierSessionInfo(UserType, SqlTrx, "");
    } // GetSystemCashierSessionInfo

    public static CashierSessionInfo GetSystemCashierSessionInfo(GU_USER_TYPE UserType, SqlTransaction SqlTrx, String TerminalName)
    {
      Boolean _is_new;
      VoucherCashDeskOpen _voucher;

      return GetSystemCashierSessionInfo(UserType, SqlTrx, TerminalName, out _is_new, out _voucher);
    } // GetSystemCashierSessionInfo

    public static CashierSessionInfo GetSystemCashierSessionInfo(GU_USER_TYPE UserType, SqlTransaction SqlTrx, String TerminalName, out Boolean IsNewSession)
    {
      VoucherCashDeskOpen _voucher;

      return GetSystemCashierSessionInfo(UserType, SqlTrx, TerminalName, out IsNewSession, out _voucher);
    } // GetSystemCashierSessionInfo

    public static CashierSessionInfo GetSystemCashierSessionInfo(GU_USER_TYPE UserType, SqlTransaction SqlTrx, String TerminalName,
                                                                 out Boolean IsNewSession, out VoucherCashDeskOpen Voucher)
    {
      Int64 _session_id;
      CashierSessionInfo _session;
      MB_USER_TYPE _mb_user_type;
      Boolean _is_acceptor;
      Boolean _is_system_user;
      
      IsNewSession = false;
      Voucher = null;

      GetGUIUserInfo(UserType, out _mb_user_type, out _is_acceptor, out _is_system_user);

      if (_is_acceptor)
      {
        String _dummy_mb_track_data;
        Int64 _dummy_mb_account_id;

        if (!Cashier.GetOrOpenSystemCashierSession(UserType, TerminalName, out _session_id, out IsNewSession, SqlTrx))
        {
          return null;
        }

        if (!Cashier.EnsureMobileBankInCashierSession(_mb_user_type, TerminalName, _session_id, out _dummy_mb_account_id, out _dummy_mb_track_data, SqlTrx))
        {
          return null;
        }
      }
      else if (_is_system_user)
      {
        if (!GetOrOpenSystemCashierSession(UserType, TerminalName, out _session_id, out IsNewSession, out Voucher, SqlTrx))
        {
          return null;
        }
      }
      else
      {
        return null;
      }

      _session = CommonCashierInformation.CashierSessionInfo();
      _session.UserType = UserType;
      _session.IsMobileBank = false;

      return _session;

    }

    public static Boolean GetCashierTerminalIdFromCountR(Int32 CountRId, out Int32 CashierTerminalId, SqlTransaction Trx)
    {
      String _sql_txt;

      _sql_txt = " SELECT   CT_CASHIER_ID              " +
                 "   FROM   CASHIER_TERMINALS          " +
                 "  WHERE   CT_COUNTR_ID = @pCountRId  " +
                 "    AND   CT_COUNTR_ID IS NOT NULL   ";
      CashierTerminalId = -1;

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pCountRId", SqlDbType.Int).Value = CountRId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              CashierTerminalId = _reader.GetInt32(0);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
    public static Boolean GetCountRIdFromCashierTerminal(Int32 CashierTerminalId, out Int32 CountRId, SqlTransaction Trx)
    {
      String _sql_txt;

      _sql_txt = " SELECT  CT_COUNTR_ID                                 " +
                "   FROM   CASHIER_TERMINALS                            " +
                "  WHERE   CT_CASHIER_ID = @pCashierId                  " +
                "   AND    CT_COUNTR_ID IS NOT NULL                     ";

      using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
      {
        _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CashierTerminalId;

        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (!_reader.Read())
          {
            CountRId = -1;

            return false;
          }

          CountRId = _reader.GetInt32(0);
        }
      }
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current System cashier session if exists.
    //          If not exists, open a new system cashier session.
    //
    //  PARAMS:
    //      - INPUT:
    //          - GU_USER_TYPE UserType
    //          - String TerminalName
    //          - SqlTransaction Trx
    //
    //      - OUTPUT:
    //          - Int64 NewCashierSessionId 
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    //
    public static Boolean GetOrOpenSystemCashierSession(GU_USER_TYPE UserType,
                                                        String TerminalName,
                                                        out Int64 NewCashierSessionId,
                                                        SqlTransaction Trx)
    {
      Boolean _is_new;
      VoucherCashDeskOpen _voucher;

      return GetOrOpenSystemCashierSession(UserType, TerminalName, out NewCashierSessionId, out _is_new, out _voucher, Trx);
    }

    public static Boolean GetOrOpenSystemCashierSession(GU_USER_TYPE UserType,
                                                        String TerminalName,
                                                        out Int64 NewCashierSessionId,
                                                        out Boolean IsNewSession,
                                                        SqlTransaction Trx)
    {
      VoucherCashDeskOpen _voucher;

      return GetOrOpenSystemCashierSession(UserType, TerminalName, out NewCashierSessionId, out IsNewSession, out _voucher, Trx);
    }


    public static Boolean GetOrOpenSystemCashierSession(GU_USER_TYPE UserType,
                                                        String TerminalName,
                                                        out Int64 NewCashierSessionId,
                                                        out Boolean IsNewSession,
                                                        out VoucherCashDeskOpen Voucher,
                                                        SqlTransaction Trx)
    {
      String _sql_txt;
      SqlParameter _sql_parameter;
      VoucherCashDeskOpen _voucher;
      Int32 _num_rows_created;
      Int64 _session_id;
      String _name;
      Decimal _dec_value;
      Int32 _cashier_terminal_id;
      Int32 _terminal_id;
      String _terminal_name;
      Int32 _user_id;
      String _user_name;

      Boolean _cashier_sessions_by_terminal;
      Int64 _mb_account_id;
      String _mb_track_data;
      CASHIER_SESSION_STATUS _cashier_session_status;
      Boolean _session_by_terminal;
      MB_USER_TYPE _mb_user_type;
      Boolean _session_found;
      Boolean _is_acceptor;
      DateTime _gaming_day;
      DateTime _now, _next_opening_time;
      Int32 _gp_min_before_closing;

      VoucherCashDeskOpenBody _voucher_body;
      CurrencyIsoType _iso_type;

      CashierMovementsTable _cashier_mov_table;
      Terminal.TerminalInfo _terminal_info;

      // Initializations
      NewCashierSessionId = 0;
      IsNewSession = false;
      _gaming_day = DateTime.MinValue;
      Voucher = null;

      try
      {
        // Get the current cashier session type (Global / By Terminal) for this user type
        // (only applies to GU_USER_TYPE.SYS_ACCEPTOR and GU_USER_TYPE.SYS_PROMOBOX)
        _cashier_sessions_by_terminal = CashierSessionsByTerminal(UserType);

        if (_cashier_sessions_by_terminal)
        {
          _terminal_name = TerminalName;
        }
        else
        {
          _terminal_name = Environment.MachineName;
        }

        _cashier_terminal_id = Cashier.ReadTerminalId(Trx, _terminal_name, UserType, true, out _terminal_id);

        if (_cashier_terminal_id == 0)
        {
          Log.Error(String.Format("GetOrOpenSystemCashiserSession.ReadTerminalId: Error getting the Terminal Id. Terminal Name = {0}", _terminal_name));

          return false;
        }

        //If User System doesn't exist, error
        if (!Cashier.GetSystemUser(UserType, Trx, out _user_id, out _user_name))
        {
          Log.Error("GetOrOpenSystemCashierSession. Error getting the System User.");

          return false;
        }

        _mb_user_type = GetMobileBankUserType(UserType, out _is_acceptor);

        if (_is_acceptor)
        {
          if (!Cashier.ReadMobileBankNoteUser(Trx, _mb_user_type, out _mb_account_id, out _mb_track_data, out _session_id, TerminalName))
          {
            Log.Error("ReadMobileBank. Error reading Mobile Bank Card. Account type: System.");

            return false;
          }

          _session_found = false;
          _cashier_session_status = CASHIER_SESSION_STATUS.CLOSED;
          _session_by_terminal = false;

          if (_session_id > 0)
          {
            _sql_txt = " SELECT   CS_STATUS                                    " +
                       "        , CS_SESSION_BY_TERMINAL                       " +
                       "        , CS_GAMING_DAY                                " +
                       "   FROM   CASHIER_SESSIONS                             " +
                       "  WHERE   CS_SESSION_ID = @pCashierSessionId           ";

            using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
            {
              _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = _session_id;

              using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
              {
                if (_reader.Read())
                {
                  _session_found = true;
                  _cashier_session_status = (CASHIER_SESSION_STATUS)_reader.GetInt32(0);
                  _session_by_terminal = _reader.GetBoolean(1);
                  _gaming_day = _reader.GetDateTime(2);
                }
              }
            }

            if (_session_found)
            {
              if (_session_by_terminal == _cashier_sessions_by_terminal)
              {
                if (_cashier_session_status == CASHIER_SESSION_STATUS.OPEN
                || (_cashier_session_status == CASHIER_SESSION_STATUS.OPEN_PENDING && !_cashier_sessions_by_terminal))
                {
                  CommonCashierInformation.SetCashierInformation(_session_id, _user_id, _user_name, _cashier_terminal_id, _terminal_name, _gaming_day);

                  NewCashierSessionId = _session_id;

                  return true;
                }
              }
              else
              {
                String _prev_session_terminal_name;
                Int32 _prev_session_terminal_id;

                if (_cashier_session_status == CASHIER_SESSION_STATUS.OPEN_PENDING)
                {
                  if (_cashier_sessions_by_terminal)
                  {
                    _prev_session_terminal_name = Environment.MachineName;
                  }
                  else
                  {
                    _prev_session_terminal_name = TerminalName;
                  }
                  _prev_session_terminal_id = Cashier.ReadTerminalId(Trx, _prev_session_terminal_name, UserType);

                  if (_prev_session_terminal_id == 0)
                  {
                    Log.Error(String.Format("GetOrOpenSystemCashierSession.ReadTerminalId: Error getting the Terminal Id. Terminal Name = {0}", _prev_session_terminal_name));

                    return false;
                  }

                  CommonCashierInformation.SetCashierInformation(_session_id, _user_id, _user_name, _prev_session_terminal_id, _prev_session_terminal_name);

                  // Mark orphan sessions as PENDING_CLOSING
                  if (!CashierSessionPendingClosing(_mb_account_id, TerminalName, Trx))
                  {
                    return false;
                  }
                }
              }
            }
          }
        }

        // Search for an open session
        _sql_txt = " SELECT   CS_SESSION_ID                                " +
                   "        , CS_GAMING_DAY                                " +
                   "   FROM   CASHIER_SESSIONS                             " +
                   "  WHERE   CS_STATUS  = @pStatusOpen                    " +
                   "    AND   CS_USER_ID = @pUserId                        " +
                   "    AND   CS_SESSION_BY_TERMINAL = @pSessionByTerminal ";

        if (_cashier_sessions_by_terminal)
        {
          _sql_txt += " AND   CS_CASHIER_ID = @pCashierId ";
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;
          _sql_cmd.Parameters.Add("@pSessionByTerminal", SqlDbType.Bit).Value = _cashier_sessions_by_terminal;
          _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = _cashier_terminal_id;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _session_id = _reader.GetInt64(0);
              _gaming_day = _reader.GetDateTime(1);

              CommonCashierInformation.SetCashierInformation(_session_id, _user_id, _user_name, _cashier_terminal_id, _terminal_name, _gaming_day);
              NewCashierSessionId = _session_id;

              return true;
            }
          }
        }

        //GamingDay session open
        _now = WGDB.Now;
        _next_opening_time = Misc.TodayOpening().AddDays(1);
        _gp_min_before_closing = GeneralParam.GetInt32("GamingDay", "CashOpening.Select.MinutesBeforeTodayOpening");

        if (_now >= _next_opening_time.AddMinutes(-_gp_min_before_closing))
        {
          _gaming_day = _next_opening_time;
        }
        else
        {
          _gaming_day = Misc.TodayOpening();
        }

        // Create new cashier session
        _sql_txt = "INSERT INTO   CASHIER_SESSIONS               " +
                  "             ( CS_NAME                        " +
                  "             , CS_USER_ID                     " +
                  "             , CS_CASHIER_ID                  " +
                  "             , CS_TAX_A_PCT                   " +
                  "             , CS_TAX_B_PCT                   " +
                  "             , CS_SESSION_BY_TERMINAL         " +
                  "             , CS_GAMING_DAY                  " +
                  "             )                                " +
                  "      VALUES                                  " +
                  "             ( @pName                         " +             // session name
                  "             , @pUserId                       " +             // session_user_id
                  "             , @pTerminalId                   " +             // session_terminal_id
                  "             , @pTax_a_pct                    " +             //
                  "             , @pTax_b_pct                    " +
                  "             , @pSessionByTerminal            " +
                  "             , @pGamingDay                    " +
                  "             )                                " +             //
                  "         SET   @pSessionId = SCOPE_IDENTITY() ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          // Format Session Name
          _name = _user_name + " " + WGDB.Now.ToString("dd/MMM/yyyy HH:mm");
          _name = _name.ToUpper();

          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = _name;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;

          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _cashier_terminal_id;

          _dec_value = GeneralParam.GetDecimal("Cashier", "Split.A.Tax.Pct", -1);
          if (_dec_value >= 0)
          {
            _sql_cmd.Parameters.Add("@pTax_a_pct", SqlDbType.Decimal).Value = _dec_value;
          }

          _dec_value = GeneralParam.GetDecimal("Cashier", "Split.B.Tax.Pct", -1);
          if (_dec_value >= 0)
          {
            _sql_cmd.Parameters.Add("@pTax_b_pct", SqlDbType.Decimal).Value = _dec_value;
          }

          _sql_cmd.Parameters.Add("@pSessionByTerminal", SqlDbType.Bit).Value = _cashier_sessions_by_terminal;
          _sql_cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = _gaming_day;

          _sql_parameter = _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt);
          _sql_parameter.Direction = ParameterDirection.Output;
          _num_rows_created = _sql_cmd.ExecuteNonQuery();

          if (_num_rows_created != 1)
          {
            Log.Error("GetOrOpenSystemCashierSession. Error creating new session");

            return false;
          }
        }

        // store newly created session id
        _session_id = (Int64)_sql_parameter.Value;
        NewCashierSessionId = _session_id;
        IsNewSession = true;

        // Insert Cashier Movement
        CommonCashierInformation.SetCashierInformation(_session_id, _user_id, _user_name, _cashier_terminal_id, _terminal_name, _gaming_day);

        _cashier_mov_table = new CashierMovementsTable(CommonCashierInformation.CashierSessionInfo());

        // DHA 01-FEB-2016: Dual Currency, add ISO Code to open session movement
        if (Misc.IsFloorDualCurrencyEnabled() && UserType == GU_USER_TYPE.SYS_TITO)
        {
          Terminal.Trx_GetTerminalInfo(_terminal_id, out _terminal_info, Trx);

          if (String.IsNullOrEmpty(_terminal_info.CurrencyCode))
          {
            _terminal_info.CurrencyCode = CurrencyExchange.GetNationalCurrency();
          }

          _cashier_mov_table.Add(0, CASHIER_MOVEMENT.OPEN_SESSION, 0, 0, "", "", _terminal_info.CurrencyCode);
        }
        else
        {
          _cashier_mov_table.Add(0, CASHIER_MOVEMENT.OPEN_SESSION, 0, 0, "");
        }

        if (!_cashier_mov_table.Save(Trx))
        {
          Log.Error("GetOrOpenSystemCashierSession. Error saving cashier movements");

          return false;
        }

        //DLL 26-SEP-2013: Modify Open voucher
        _iso_type = new CurrencyIsoType();
        _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
        _iso_type.Type = CurrencyExchangeType.CURRENCY;

        // - Create voucher
        _voucher = new VoucherCashDeskOpen(CASHIER_MOVEMENT.OPEN_SESSION, PrintMode.Print, Trx);

        _voucher_body = new VoucherCashDeskOpenBody(CASHIER_MOVEMENT.OPEN_SESSION, _iso_type, 0, 0);
        _voucher.SetParameterValue("@VOUCHER_CASH_DESK_OPEN_BODY", _voucher_body.VoucherHTML);
        _voucher.AddString("VOUCHER_CASH_DESK_OPEN_BODY", "");

        // - Save voucher
        if (!_voucher.Save(Trx))
        {
          Log.Error("GetOrOpenSystemCashierSession. Error saving voucher. Session id: " + _session_id.ToString());

          return false;
        }

        Voucher = _voucher;

        //// ICS 14-MAR-2014, Mustn't link system cashier sessions with cage
        //if (UserType != GU_USER_TYPE.SYS_SYSTEM)
        //{
        //  // AJQ 07-MAR-2014, Always try to link the cashier session with cage
        //  if (!Cage.LinkTerminalWithCageSession(CommonCashierInformation.CashierSessionInfo(), Trx))
        //  {
        //    Log.Error("Cage.LinkTerminalWithCageSession failed.");

        //    return false;
        //  }
        //}

        // RCI & JCA 12-MAR-2014: If it's a PromoBOX or BillAcceptor and Cage is enabled, a money collection for the CashierSession must be created.
        //                        It's mandatory that the cashier sessions are configured by terminal.
        //                        If they are global cashier sessions, _terminal_id will be 0, and DB_CreateMoneyCollection() return false.
        if ((UserType == GU_USER_TYPE.SYS_PROMOBOX || UserType == GU_USER_TYPE.SYS_ACCEPTOR) && Cage.IsCageEnabled())
        {
          Int64 _dummy_money_collection_id;

          if (!TITO.MoneyCollection.DB_CreateTerminalMoneyCollection(_terminal_id, 0, 0, NewCashierSessionId, out _dummy_money_collection_id, Trx))
          {
            Log.Error("GetOrOpenSystemCashierSession. DB_CreateMoneyCollection failed.");
            if (_terminal_id == 0)
            {
              Log.Error("GetOrOpenSystemCashierSession: USER_TYPE: " + UserType.ToString() + ", Global cashier sessions not supported when cage is enabled.");
            }

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        // Logger ...
        Log.Exception(_ex);

        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current System cashier session type (global or by terminal).
    //
    //  PARAMS:
    //      - INPUT:
    //          - GU_USER_TYPE UserType
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    //
    public static Boolean CashierSessionsByTerminal(GU_USER_TYPE UserType)
    {
      switch (UserType)
      {
        case GU_USER_TYPE.SYS_ACCEPTOR:
        case GU_USER_TYPE.SYS_TITO:
          return GeneralParam.GetBoolean("SasHost", "NoteAcceptor.CashierSessionsByTerminal");

        case GU_USER_TYPE.SYS_PROMOBOX:
          return GeneralParam.GetBoolean("WigosKiosk", "NoteAcceptor.CashierSessionsByTerminal");

        case GU_USER_TYPE.SYS_REDEMPTION:
        case GU_USER_TYPE.SYS_GAMING_TABLE:
          // When a GamingTable has a virtual cashier, the cashier sessions are always by gaming table.
          return true;

        default:
          return false;
      }
    } // CashierSessionsByTerminal

    #endregion SystemCashierSession

    //------------------------------------------------------------------------------
    // PURPOSE: mobile bank get cashier information
    //
    //  PARAMS:
    //      - INPUT:
    //          - CashierSessionId
    //
    //      - OUTPUT:
    //          - UserId
    //          - UserName
    //          - TerminalId
    //          - TerminalName
    //          - CashierBalance 
    //
    // RETURNS: 
    //        
    //   
    public static Boolean DB_ReadOpenedCashierSession(Int64 SessionId
                                                 , SqlTransaction Trx
                                                 , out Int32 UserId
                                                 , out String UserName
                                                 , out Int32 TerminalId
                                                 , out String TerminalName
                                                 , out Currency CashierBalance
                                                 , out DateTime GamingDay)
    {
      String _sql_str;

      UserId = 0;
      UserName = "";
      TerminalId = 0;
      TerminalName = "";
      CashierBalance = 0;
      GamingDay = DateTime.MinValue;

      try
      {
        _sql_str = " SELECT cs_user_id                                     " +
                   "      , gu_username                                    " +
                   "      , cs_cashier_id                                  " +
                   "      , ct_name                                        " +
                   "      , cs_balance                                     " +
                   "      , cs_gaming_day                                  " +
                   "   FROM cashier_sessions, gui_users, cashier_terminals " +
                   "  WHERE cs_session_id  = @CashierSessionID             " +
                   "    AND cs_cashier_id  = ct_cashier_id                 " +
                   "    AND cs_user_id     = gu_user_id                    " +
                   "    AND cs_status     IN (@pStatusOpen,                " +
                   "                          @pStatusOpenPending)         ";

        using (SqlCommand _sql_command = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@CashierSessionID", SqlDbType.BigInt, 8, "cs_session_id").Value = SessionId;
          _sql_command.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          _sql_command.Parameters.Add("@pStatusOpenPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN_PENDING;

          using (SqlDataReader _reader = _sql_command.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            UserId = (Int32)_reader[0];
            UserName = (String)_reader[1];
            TerminalId = (Int32)_reader[2];
            TerminalName = (String)_reader[3];
            CashierBalance = _reader.GetSqlMoney(4);
            GamingDay = _reader.GetDateTime(5);

            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Cashier Session. cs_session_id: " + SessionId.ToString());
      }

      return false;
    } // DB_ReadOpenedCashierSession

    //------------------------------------------------------------------------------
    // PURPOSE: Get the system user. Must be only ONE system user.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - UserType
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - UserId
    //          - UserName
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //      - SSC 11-JUN-2012: Modified GetSystemUser method to get system user by type parameter (Note Acceptor or Kiosk)
    public static Boolean GetSystemUser(GU_USER_TYPE UserType, SqlTransaction SqlTrx, out Int32 UserId, out String UserName)
    {
      String _sql_txt;

      UserId = -1;
      UserName = "";

      try
      {
        _sql_txt = " SELECT   GU_USER_ID " +
                   "        , GU_USERNAME " +
                   "   FROM   GUI_USERS " +
                   "  WHERE   GU_USER_TYPE = @pUserType ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserType", SqlDbType.Int).Value = UserType;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            // Return the first one found. Must be ONLY ONE.
            if (_reader.Read())
            {
              UserId = _reader.GetInt32(0);
              UserName = _reader.GetString(1);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetSystemUser

    //------------------------------------------------------------------------------
    // PURPOSE: Read MobileBank for Note Acceptor
    //
    //  PARAMS:
    //      - INPUT:
    //          - Trx
    //
    //      - OUTPUT:
    //          - MbAccountId
    //          - MbTrackData
    //          - CashierSessionId
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static Boolean ReadMobileBankNoteUser(SqlTransaction Trx,
                                                 MB_USER_TYPE MobileBankType,
                                                 out Int64 MbAccountId,
                                                 out String MbTrackData,
                                                 out Int64 CashierSessionId,
                                                 String TerminalName)
    {
      GU_USER_TYPE _gu_user_type;
      String _holder_name;
      Int32 _terminal_id;
      Boolean _global_mb;

      MbAccountId = 0;
      MbTrackData = "";
      CashierSessionId = 0;

      _gu_user_type = GetGUIUserType(MobileBankType, out _holder_name);

      // ICS & QMP 18-MAR-2014: This variable indicates if we must use global mobile bank or terminal mobile bank
      // It's used in GUI when it isn't collection by terminal
      _global_mb = String.IsNullOrEmpty(TerminalName);
      _terminal_id = 0;

      if (!_global_mb)
      {
        Terminal.TerminalInfo _terminal_info;

        if (!Terminal.Trx_GetTerminalInfo(TerminalName, out _terminal_info, Trx))
        {
          return false;
        }

        _terminal_id = _terminal_info.TerminalId;
      }

      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine(" DECLARE @pin                    AS INT");
        _sb.AppendLine(" DECLARE @mb_account_id          AS BIGINT ");
        _sb.AppendLine(" DECLARE @track_data             AS NVARCHAR(20) ");
        _sb.AppendLine(" DECLARE @mb_cashier_session_id  AS BIGINT ");

        _sb.AppendLine(" SELECT   TOP 1 ");
        _sb.AppendLine("          @mb_account_id         = MB_ACCOUNT_ID ");
        _sb.AppendLine("        , @track_data            = ISNULL(MB_TRACK_DATA, '') ");
        _sb.AppendLine("        , @mb_cashier_session_id = ISNULL(MB_CASHIER_SESSION_ID, 0) ");
        _sb.AppendLine("   FROM   MOBILE_BANKS ");
        _sb.AppendLine("  WHERE ( MB_TERMINAL_ID         = @pTerminalId OR ( MB_TERMINAL_ID IS NULL AND @pTerminalId IS NULL ) ) ");
        _sb.AppendLine("    AND   MB_ACCOUNT_TYPE        = @pAcceptorType ");
        _sb.AppendLine(" ORDER BY MB_ACCOUNT_ID DESC ");

        _sb.AppendLine(" IF @mb_account_id IS NULL ");
        _sb.AppendLine(" BEGIN ");
        _sb.AppendLine("   SET @pin = ROUND((9999 - 1000) * RAND() + 1000, 0) ");

        _sb.AppendLine("   INSERT INTO   MOBILE_BANKS ");
        _sb.AppendLine("               ( MB_BLOCKED ");
        _sb.AppendLine("               , MB_BALANCE ");
        _sb.AppendLine("               , MB_ACCOUNT_TYPE ");
        _sb.AppendLine("               , MB_PIN ");
        _sb.AppendLine("               , MB_HOLDER_NAME ");
        _sb.AppendLine("               , MB_TERMINAL_ID ");
        _sb.AppendLine("               ) ");
        _sb.AppendLine("        VALUES ( 0 ");
        _sb.AppendLine("               , 0 ");
        _sb.AppendLine("               , @pAcceptorType ");
        _sb.AppendLine("               , CAST(@pin AS NVARCHAR) ");
        _sb.AppendLine("               , @pHolderName ");
        _sb.AppendLine("               , @pTerminalId ");
        _sb.AppendLine("               ) ");

        _sb.AppendLine("   SET @mb_account_id         = SCOPE_IDENTITY() ");
        _sb.AppendLine("   SET @track_data            = RIGHT('0000000000000' + CAST(@mb_account_id AS NVARCHAR), 13) ");
        _sb.AppendLine("   SET @mb_cashier_session_id = 0 ");

        _sb.AppendLine("   UPDATE   MOBILE_BANKS ");
        _sb.AppendLine("      SET   MB_TRACK_DATA = @track_data ");
        _sb.AppendLine("    WHERE   MB_ACCOUNT_ID = @mb_account_id ");
        _sb.AppendLine(" END ");

        _sb.AppendLine(" SELECT @mb_account_id, @track_data, @mb_cashier_session_id ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pMBAccountType", SqlDbType.Int).Value = MobileBankType;
          _sql_cmd.Parameters.Add("@pHolderName", SqlDbType.VarChar).Value = _holder_name;
          if (_global_mb)
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal_id;
          }
          _sql_cmd.Parameters.Add("@pAcceptorType", SqlDbType.SmallInt).Value = MobileBankType;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              MbAccountId = _reader.GetInt64(0);
              MbTrackData = _reader.GetString(1);
              CashierSessionId = _reader.GetInt64(2);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadMobileBankNoteUser

    //------------------------------------------------------------------------------
    // PURPOSE: Ensure the System Mobile Bank used for Note Acceptor has an opened cashier session.
    //          Returns the data for the System Mobile Bank
    //
    //  PARAMS:
    //      - INPUT:
    //          - Trx
    //
    //      - OUTPUT:
    //          - MbAccountId
    //          - MbTrackData
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static Boolean EnsureMobileBankInCashierSession(MB_USER_TYPE MbUserType,
                                                           String TerminalName,
                                                           Int64 SessionId,
                                                           out Int64 MbAccountId,
                                                           out String MbTrackData,
                                                           SqlTransaction SqlTrx)
    {
      Int64 _mb_cashier_session_id;
      GU_USER_TYPE _gu_user_type;

      MbAccountId = 0;
      MbTrackData = "";

      if (SessionId == 0)
      {
        return false;
      }

      try
      {
        _gu_user_type = GetGUIUserType(MbUserType);

        //  Get mobile bank information
        if (!Cashier.ReadMobileBankNoteUser(SqlTrx, MbUserType, out MbAccountId, out MbTrackData, out _mb_cashier_session_id, TerminalName))
        {
          Log.Error("ReadMobileBank. Error reading Mobile Bank Card. Account type: System.");

          return false;
        }

        //  If cashier session has changed, update mobile bank cashier session Id
        if (SessionId != _mb_cashier_session_id)
        {
          if (!UpdateMBOnOpenCashierSession(MbAccountId, MbUserType, MbTrackData, SessionId, SqlTrx))
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // EnsureMobileBankInCashierSession

    //------------------------------------------------------------------------------
    // PURPOSE: Change a mobile bank account cashier session id.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - MbAccountId
    //          - MbTrackData
    //          - CashierSessionId
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //    
    public static Boolean UpdateMBOnOpenCashierSession(Int64 MbAccountId,
                                                       MB_USER_TYPE MbUserType,
                                                       String MbTrackData,
                                                       Int64 CashierSessionId,
                                                       SqlTransaction Trx)
    {
      //Int32 _num_rows_updated;
      String _sql_txt;
      VoucherMBCardChange _voucher;
      Boolean _voucher_saved;
      Decimal _mb_balance;
      String _mb_holder_name;

      // Initializations
      _voucher_saved = false;
      _mb_holder_name = "";

      // Steps:
      // 1. Update card data.
      // 2. Insert voucher.

      try
      {
        // Get Sql Connection 
        // 1. Update card data
        _sql_txt = "UPDATE   MOBILE_BANKS                               " +
                   "   SET   MB_CASHIER_SESSION_ID = @pCashierSessionId " +
                   "OUTPUT   INSERTED.MB_BALANCE     AS BALANCE         " +
                   "       , INSERTED.MB_HOLDER_NAME AS HOLDER_NAME     " +
                   " WHERE   MB_ACCOUNT_ID         = @pAccountId        ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = MbAccountId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Error("UpdateMobileBank. Card not updated. Card Id:" + MbAccountId.ToString());

              return false;
            }

            _mb_balance = _reader.GetDecimal(0);      // Balance

            if (!_reader.IsDBNull(1))
            {
              _mb_holder_name = _reader.GetString(1); // Holder Name
            }
          }

          if (MobileBankIsAcceptor(MbUserType))
          {
            return true;
          }

          // 2. Insert voucher. Ignore for SYS-ACCEPTOR and SYS-PROMOBOX

          // 2.1 Create voucher        
          MBCardData _mb_card_data;
          _mb_card_data = new MBCardData();
          if (!MobileBank.DB_MBCardGetAllData(MbAccountId, _mb_card_data, null))
          {
            Log.Error("UpdateMobileBank. MB card not be loaded. Card Id:" + MbAccountId.ToString());
            return false;
          }

          _voucher = new VoucherMBCardChange(PrintMode.Print, _mb_card_data, Trx);

          // 2.2 Build voucher data
          _voucher.AddCurrency("VOUCHER_MBCARD_CHANGE_INITIAL", _mb_balance);
          _voucher.AddCurrency("VOUCHER_MBCARD_CHANGE_AMOUNT", 0);
          _voucher.AddCurrency("VOUCHER_MBCARD_CHANGE_FINAL", _mb_balance);

          // 2.3 Save voucher
          _voucher_saved = _voucher.Save(Trx);

          if (!_voucher_saved)
          {
            Log.Error("UpdateMBOnOpenCashierSession. Error saving voucher." + MbAccountId.ToString());

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateMBOnOpenCashierSession

    //------------------------------------------------------------------------------
    // PURPOSE: Check if exist cashier session
    //
    //  PARAMS:
    //      - INPUT:
    //          - SessionId
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - SessionId
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static Boolean ExistCashierTerminalSessionOpen(Int32 TerminalId, out Int64 SessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;
      Boolean _return;

      SessionId = 0;
      _return = false;

      try
      {
        // Search for an open session
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   CS_SESSION_ID                 ");
        _sb.AppendLine("   FROM   CASHIER_SESSIONS              ");
        _sb.AppendLine("  INNER   JOIN GUI_USERS ON GU_USER_ID  = CS_USER_ID ");
        _sb.AppendLine("  WHERE   CS_STATUS       = @pStatusOpen  ");
        _sb.AppendLine("    AND   CS_CASHIER_ID   = @pCashierId   ");
        _sb.AppendLine("    AND   GU_USER_TYPE IN ( @pUserUserType, @pGamingTableUserType) ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pUserUserType", SqlDbType.Int).Value = GU_USER_TYPE.USER;
          _sql_cmd.Parameters.Add("@pGamingTableUserType", SqlDbType.Int).Value = GU_USER_TYPE.SYS_GAMING_TABLE;

          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            SessionId = (Int64)_obj;
            _return = true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _return;
    } //ExistCashierTerminalSessionOpen

    //------------------------------------------------------------------------------
    // PURPOSE: Check if cashier session is open
    //
    //  PARAMS:
    //      - INPUT:
    //          - SessionId
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - true: cashier session is open
    //      - false: cashier session is closed
    //        
    //   NOTES :
    //
    public static Boolean IsSessionOpen(Int64 SessionId, SqlTransaction SqlTrx)
    {
      String _sql_txt;
      Object _obj;

      try
      {
        _sql_txt = "SELECT   1                             " +
                   "  FROM   CASHIER_SESSIONS              " +
                   " WHERE   CS_SESSION_ID  = @pSessionId  " +
                   "   AND   CS_STATUS      = @pStatusOpen ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          _obj = _sql_cmd.ExecuteScalar();
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } //IsSessionOpen

    //------------------------------------------------------------------------------
    // PURPOSE: Add amount of sales, to add on CS_TOTAL_SOLD ... 
    //
    //  PARAMS:
    //      - INPUT:
    //          - SessionId
    //          - AmountToAdd
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - true: cashier session is open
    //      - false: error occurred
    //        
    //   NOTES :
    //
    public static Boolean UpdateSessionTotalSold(Int64 CashierSessionId, Int32 UserId, SalesLimitType SalesLimitType, Currency SoldAmount, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   CASHIER_SESSIONS                   ");

        if (SalesLimitType == Cashier.SalesLimitType.Cashier)
        {
          _sb.AppendLine("   SET   CS_TOTAL_SOLD       = ISNULL(CS_TOTAL_SOLD, 0) + @pSoldAmount  ");
        }
        else
        {
          _sb.AppendLine("   SET   CS_MB_TOTAL_SOLD    = ISNULL(CS_MB_TOTAL_SOLD, 0) + @pSoldAmount  ");
        }

        _sb.AppendLine(" WHERE   CS_SESSION_ID       = @pSessionId  ");
        _sb.AppendLine("   AND   CS_USER_ID          = @pUserId     ");
        _sb.AppendLine("   AND   CS_STATUS           = @pStatusOpen ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSoldAmount", SqlDbType.Money).Value = SoldAmount.SqlMoney;
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

          return (_sql_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the Limit efective of Sales from a Cashier User or Cashier MobileBanks
    //
    //  PARAMS:
    //      - INPUT:
    //          - UserId: Id of User Cashier
    //          - LimitUserCashierUserMb: True retrieve the limit from a Cashier User
    //                                    False retrieve the limit from MB of Cashier User
    //      - OUTPUT:
    //          - LimitUser: Value of credit limit
    // RETURNS: 
    //      - true: 
    //      - false: error occurred
    //        
    //   NOTES :
    //
    public static Boolean SalesLimitEfective(Int32 UserId, SalesLimitType UserCashierUserMb, out Currency LimitUser)
    {
      Currency _gp_user_max_limit;
      Currency _user_efective_sales_limit;
      Decimal _gp_value;
      StringBuilder _sb;
      Object _obj;
      String _column;
      String _gp_group_key;

      LimitUser = 0;

      try
      {
        _sb = new StringBuilder();

        // Get Value of Sale Limit if defined in DB
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _column = UserCashierUserMb == SalesLimitType.Cashier ? " GU_SALES_LIMIT " : " GU_MB_SALES_LIMIT ";

          _sb.Length = 0;
          _sb.AppendLine("SELECT " + _column + "             ");
          _sb.AppendLine("  FROM   GUI_USERS                    ");
          _sb.AppendLine(" WHERE   GU_USER_ID = @pUserId        ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
            _obj = _sql_cmd.ExecuteScalar();
          }
        }

        _gp_group_key = (UserCashierUserMb == SalesLimitType.Cashier ? "Cashier" : "MobileBank");

        // Get Value of Sale Limit in GP if not defined in USER_DB
        if (_obj != null && _obj != DBNull.Value)
        {
          _user_efective_sales_limit = (Decimal)_obj;
        }
        else
        {
          _user_efective_sales_limit = 0;
          if (Decimal.TryParse(GeneralParam.Value(_gp_group_key, "SalesLimit.DefaultValue"), out _gp_value))
          {
            _user_efective_sales_limit = Math.Max(0, _gp_value);
          }
        }

        // Get Max Value of Sale Limit
        _gp_user_max_limit = Decimal.MaxValue;
        if (Decimal.TryParse(GeneralParam.Value(_gp_group_key, "SalesLimit.MaxValue"), out _gp_value))
        {
          if (_gp_value > 0)
          {
            _gp_user_max_limit = _gp_value;
          }
        }

        LimitUser = Math.Min(_gp_user_max_limit, _user_efective_sales_limit);

        if (LimitUser <= 0)
        {
          Log.Warning(String.Format("SalesLimit for {0} is zero.", _gp_group_key));
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Updates the cashier session limit of sales
    //
    //  PARAMS:
    //      - INPUT:
    //          - UserId          : User Cashier
    //          - CashierSesionId : Cashier Sesion
    //          - CreditSaleLimit : The new value of Sales Limit
    //          - UserCashierUserMb: If limit is for Cashier or Mobile Bank
    //          - SqlTrx : SQL Transaction
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - true: updated correctly
    //      - false: error occurred
    //        
    //   NOTES :
    //
    public static Boolean SetNewSalesLimit(Int32 UserId, Int64 CashierSesionId, Currency SalesLimit, SalesLimitType SalesLimitType, SqlTransaction SqlTrx)
    {
      String _column;
      StringBuilder _sb;

      try
      {
        SalesLimit = Math.Max(0, SalesLimit);

        _column = (SalesLimitType == SalesLimitType.Cashier ? " CS_SALES_LIMIT " : " CS_MB_SALES_LIMIT ");

        // UPDATE CS_SALE_LIMIT BD
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   CASHIER_SESSIONS                   ");
        _sb.AppendLine("  SET " + _column + "  = @pSalesLimit       ");
        _sb.AppendLine(" WHERE   CS_SESSION_ID = @pSessionId        ");
        _sb.AppendLine("   AND   CS_USER_ID    = @pUserId           ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSalesLimit", SqlDbType.Money).Value = SalesLimit.SqlMoney;
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSesionId;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          return (_sql_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch
      {
        Log.Error("UpdateSalesLimitCS. Error updating Cashier Sesion Limit Sales. SessionId: " + CashierSesionId.ToString());
      }

      return false;
    } // SetNewSalesLimit

    //------------------------------------------------------------------------------
    // PURPOSE: Check the current sales limitation from user cashier, updates the limitation on the cashier session opened, 
    //          and returns the allowed credit sales limitation after complete the Sale of CreditSale amount
    //
    //  PARAMS:
    //      - INPUT:
    //          - UserId:           Id of User Cashier
    //          - CashierSesionId:  Cashier Sesion Id
    //          - CreditSale:       Amount of credit of current Sale
    //          - SesionCaja:       True  returns the limit from a Cashier User
    //                              False returns the limit from MB of Cashier User
    //      - OUTPUT:
    //          - RemainCreditToSale: Value of credit limit
    // RETURNS: 
    //      - true: 
    //      - false: error occurred
    //        
    //   NOTES :
    //      - JML 12-NOV-2013: Overloaded function to return the old maximum sales limit (Necessary for bug WIG-403)
    //
    public static Boolean GetRemainingSalesLimit(Int32 UserId, Int64 CashierSesionId, SalesLimitType SalesLimitType, out Currency RemainingSalesLimit)
    {
      Currency _old_max_sales_limit;
      Currency _new_max_sales_limit;

      return GetRemainingSalesLimit(UserId, CashierSesionId, SalesLimitType, out RemainingSalesLimit, out _new_max_sales_limit, out _old_max_sales_limit);
    }

    public static Boolean GetRemainingSalesLimit(Int32 UserId, Int64 CashierSesionId, SalesLimitType SalesLimitType, out Currency RemainingSalesLimit, out Currency NewMaxSalesLimit)
    {
      Currency _old_max_sales_limit;

      return GetRemainingSalesLimit(UserId, CashierSesionId, SalesLimitType, out RemainingSalesLimit, out NewMaxSalesLimit, out _old_max_sales_limit);
    }

    public static Boolean GetRemainingSalesLimit(Int32 UserId, Int64 CashierSesionId, SalesLimitType SalesLimitType,
                                                 out Currency RemainingSalesLimit, out Currency NewMaxSalesLimit, out Currency OldMaxSalesLimit)
    {
      // Get current Sales Limit of Current Cashier o Current User
      Decimal _cs_total_sold;    // Sesion Total Sold
      Object _obj;
      StringBuilder _sb;
      String _column;
      Currency _assigned_credit;
      Currency _disposed_limit;

      RemainingSalesLimit = -1;
      NewMaxSalesLimit = -1;
      OldMaxSalesLimit = -1;

      _assigned_credit = 0;

      try
      {
        _sb = new StringBuilder();

        //Get Current Total Sales 
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _column = (SalesLimitType == SalesLimitType.Cashier ? " CS_TOTAL_SOLD " : " CS_MB_TOTAL_SOLD ");

          _sb.Length = 0;
          _sb.AppendLine("SELECT   " + _column + "              ");
          _sb.AppendLine("  FROM   CASHIER_SESSIONS             ");
          _sb.AppendLine(" WHERE   CS_SESSION_ID = @pSessionId  ");
          _sb.AppendLine("   AND   CS_USER_ID    = @pUserId     ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSesionId;
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

            _obj = _db_trx.ExecuteScalar(_cmd);
          }

          if (_obj != null && _obj != DBNull.Value)
          {
            _cs_total_sold = (Decimal)_obj;
          }
          else
          {
            _cs_total_sold = 0;
          }

          // Get the Limit Sales effective (GP or UserDB)
          if (!SalesLimitEfective(UserId, SalesLimitType, out OldMaxSalesLimit))
          {
            return false;
          }

          // If sales of MB, get all assigned credit
          if (SalesLimitType == SalesLimitType.MobileBank)
          {
            _sb.Length = 0;
            _sb.AppendLine("SELECT   SUM (MB_BALANCE + MB_PENDING_CASH)     ");
            _sb.AppendLine("  FROM   MOBILE_BANKS                           ");
            _sb.AppendLine(" WHERE   MB_CASHIER_SESSION_ID = @pSessionId    ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSesionId;

              _obj = _db_trx.ExecuteScalar(_cmd);
            }

            if (_obj != null && _obj != DBNull.Value)
            {
              _assigned_credit = (Decimal)_obj;
            }
          }
        }

        _disposed_limit = _cs_total_sold + _assigned_credit;
        // Get the Max between Current Sales and Total Sold or Extended Limits
        NewMaxSalesLimit = Math.Max(OldMaxSalesLimit, _disposed_limit);
        // Get Total after the current sale
        RemainingSalesLimit = Math.Max(0, NewMaxSalesLimit - _disposed_limit);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetRemainingSalesLimit

    //------------------------------------------------------------------------------
    // PURPOSE: Updates the credit Sold on Cashier Operation Credit Sales
    //
    //  PARAMS:
    //      - INPUT:
    //          - UserId: Id of User Cashier
    //          - CashierSesionId: Cashier Sesion Id
    //          - CreditSale: Amount of credit to check if reach the sale limitation
    //          - SesionCaja: True retrieve the limit from a Cashier User
    //                                    False retrieve the limit from MB of Cashier User
    //      - OUTPUT:
    //          - RemainCreditAfterSale: Value of credit limit
    // RETURNS: 
    //      - true: 
    //      - false: error occurred
    //        
    //   NOTES :
    //
    public static Boolean UpdateSalesLimit(Int32 UserId, Int64 CashierSessionId, Currency SoldAmount, SqlTransaction SqlTrx,
                                           out Boolean SalesLimitReached,
                                           out Currency RemainingSalesLimit)
    {
      Boolean _sales_limit_enabled;
      Currency _max_sales_limit;
      Currency _remaining_sales_limit;
      Int32 _gp_value;
      Currency _old_max_sales_limit;

      RemainingSalesLimit = 0;
      SalesLimitReached = false;

      try
      {
        if (!Int32.TryParse(GeneralParam.Value("Cashier", "SalesLimit.Enabled"), out _gp_value))
        {
          _gp_value = 0;
        }

        _sales_limit_enabled = (_gp_value == 1);

        if (!_sales_limit_enabled)
        {
          return true;
        }

        if (!Cashier.GetRemainingSalesLimit(UserId, CashierSessionId, Cashier.SalesLimitType.Cashier,
                                            out _remaining_sales_limit, out _max_sales_limit, out _old_max_sales_limit))
        {
          Log.Error("AllowCurrentCashierSale. Error reading Cashier Sesion Limit Sales. SessionId: " + CashierSessionId.ToString());

          return false;
        }

        if (_max_sales_limit != _old_max_sales_limit)
        {
          // JML 12-NOV-2013: Update new sales limit in a separate Trx, only if it has change.
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!Cashier.SetNewSalesLimit(UserId, CashierSessionId, _max_sales_limit, Cashier.SalesLimitType.Cashier, _db_trx.SqlTransaction))
            {
              Log.Error("AllowCurrentCashierSale. Error updating Cashier Sesion Limit Sales. SessionId: " + CashierSessionId.ToString());

              return false;
            }

            _db_trx.Commit();
          }
        }

        RemainingSalesLimit = _remaining_sales_limit;

        if (SoldAmount > _remaining_sales_limit)
        {
          SalesLimitReached = true;
        }
        else
        {
          SalesLimitReached = false;
          RemainingSalesLimit -= SoldAmount;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateSalesLimit

    public static Currency UpdateSessionBalance(SqlTransaction SQLTransaction, Int64 CurrentSessionId, Currency Increment)
    {
      return UpdateSessionBalance(SQLTransaction, CurrentSessionId, Increment, "", null);
    }

    // TODO: out ini & final_balance. Return boolean indicating ok or error.
    public static Currency UpdateSessionBalance(SqlTransaction SQLTransaction, Int64 CurrentSessionId, Currency Increment, String CurrencyCode, CurrencyExchangeType? Type)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      // RCI 08-AUG-2011: Don't do a try/catch.

      // RCI 08-AUG-2011: Can only UPDATE if CS_STATUS = Opened

      // DLL 05-AUG-2013: Skip select. change update with output parameter

      // DLL 22-AUG-2013: If not exists insert information 
      if (CurrencyCode == "")
      {
        _sb.AppendLine(" UPDATE  CASHIER_SESSIONS ");
        _sb.AppendLine("    SET  CS_BALANCE        = CS_BALANCE + @p1 ");
        _sb.AppendLine(" OUTPUT  INSERTED.CS_BALANCE ");
        _sb.AppendLine("  WHERE  CS_SESSION_ID     = @p2 ");
        _sb.AppendLine("    AND  CS_STATUS         = @pStatusOpen ");
        _sb.AppendLine("    AND  (CS_BALANCE + @p1 >= 0 ");
        _sb.AppendLine("          OR CS_USER_ID IN (SELECT GU_USER_ID FROM GUI_USERS WHERE GU_USER_TYPE = 6))");
      }
      else
      {
        _sb.AppendLine(" IF EXISTS ( SELECT   1  ");
        _sb.AppendLine("               FROM   CASHIER_SESSIONS_BY_CURRENCY ");
        _sb.AppendLine("              WHERE   CSC_SESSION_ID = @p2 ");
        _sb.AppendLine("                AND   CSC_ISO_CODE   = @pIsoCode ");
        _sb.AppendLine("                AND   CSC_TYPE       = @p3) ");
        _sb.AppendLine(" UPDATE   CASHIER_SESSIONS_BY_CURRENCY  ");
        _sb.AppendLine("     SET  CSC_BALANCE        = CSC_BALANCE + @p1 ");
        _sb.AppendLine("  OUTPUT  INSERTED.CSC_BALANCE ");
        _sb.AppendLine("   WHERE  CSC_SESSION_ID     = @p2 ");
        _sb.AppendLine("     AND  CSC_BALANCE + @p1 >= 0 ");
        _sb.AppendLine("     AND  CSC_TYPE = @p3  ");
        _sb.AppendLine("     AND  CSC_ISO_CODE = @pIsoCode  ");
        _sb.AppendLine("     AND  EXISTS (  ");
        _sb.AppendLine("                   SELECT CS_SESSION_ID ");
        _sb.AppendLine("                     FROM CASHIER_SESSIONS ");
        _sb.AppendLine("                    WHERE CS_SESSION_ID  =   @p2 ");
        _sb.AppendLine("                      AND CS_STATUS      = @pStatusOpen ) ");
        _sb.AppendLine("  ELSE ");
        _sb.AppendLine("     INSERT INTO   CASHIER_SESSIONS_BY_CURRENCY ");
        _sb.AppendLine("                 ( CSC_SESSION_ID ");
        _sb.AppendLine("                 , CSC_ISO_CODE ");
        _sb.AppendLine("                 , CSC_TYPE ");
        _sb.AppendLine("                 , CSC_BALANCE ");
        _sb.AppendLine("                 , CSC_COLLECTED  ");
        _sb.AppendLine("                 ) ");
        _sb.AppendLine("          OUTPUT   INSERTED.CSC_BALANCE ");
        _sb.AppendLine("          VALUES    ");
        _sb.AppendLine("                 ( @p2 ");
        _sb.AppendLine("                 , @pIsoCode ");
        _sb.AppendLine("                 , @p3 ");
        _sb.AppendLine("                 , @p1 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 ) ");
      }

      using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), SQLTransaction.Connection, SQLTransaction))
      {
        sql_command.Parameters.Add("@p1", SqlDbType.Money, 8, "cs_balance").Value = Increment.SqlMoney;
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "cs_session_id").Value = CurrentSessionId;
        sql_command.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

        if (CurrencyCode != "")
        {
          sql_command.Parameters.Add("@pIsoCode", SqlDbType.VarChar).Value = CurrencyCode;
          sql_command.Parameters.Add("@p3", SqlDbType.Int).Value = Type;
        }

        // RCI 08-AUG-2011: If not updated, throw an exception. Can't continue!
        using (SqlDataReader _reader = sql_command.ExecuteReader())
        {
          if (!_reader.Read())
          {
            throw new Exception("UpdateSessionBalance: Can't update Cashier Session Id " + CurrentSessionId + ". It is not open.");
          }

          return _reader.GetDecimal(0);
        }
      }
      //if (sql_command.ExecuteNonQuery() != 1)
      //{
      //  throw new Exception("UpdateSessionBalance: Can't update Cashier Session Id " + CurrentSessionId + ". It is not open.");
      //}

      //sql_str = "SELECT cs_balance           " +
      //          " FROM  cashier_sessions     " +
      //          " WHERE cs_session_id  = @p3 ";

      //sql_command = new SqlCommand(sql_str);
      //sql_command.Connection = SQLTransaction.Connection;
      //sql_command.Transaction = SQLTransaction;


      //sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "cs_session_id").Value = CurrentSessionId;

      //balance = (Decimal)sql_command.ExecuteScalar();

      //return balance;
    }

    public static CASHIER_SESSION_CLOSE_STATUS CashierSessionClose(Currency CollectedAmount,
                                                                   CashierSessionInfo SessionInfo,
                                                                   Int32 CashierUserId,
                                                                   out ArrayList VoucherList,
                                                                   SqlTransaction Trx)
    {
      return CashierSessionClose(CollectedAmount, SessionInfo, CashierUserId, false, out VoucherList, Trx);
    }

    public static CASHIER_SESSION_CLOSE_STATUS CashierSessionClose(Currency CollectedAmount,
                                                                   CashierSessionInfo SessionInfo,
                                                                   Int32 CashierUserId,
                                                                   Boolean IsCountR,
                                                                   out ArrayList VoucherList,
                                                                   SqlTransaction Trx)
    {
      String _national_currency;
      TYPE_CASHIER_SESSION_STATS _session_stats = new TYPE_CASHIER_SESSION_STATS();
      CurrencyIsoType _iso_type;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      Cashier.ReadCashierSessionData(Trx, SessionInfo.CashierSessionId, ref _session_stats);

      _iso_type = new CurrencyIsoType();
      _iso_type.IsoCode = _national_currency;
      _iso_type.Type = CurrencyExchangeType.CURRENCY;

      _session_stats.collected.Add(_iso_type, (Decimal)CollectedAmount);
      _session_stats.collected_diff.Add(_iso_type, (Decimal)CollectedAmount - Math.Abs(_session_stats.final_balance));

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _balance in _session_stats.currencies_balance)
      {
        _session_stats.collected.Add(_balance.Key, (Decimal)CollectedAmount);
        _session_stats.collected_diff.Add(_balance.Key, (Decimal)CollectedAmount - _balance.Value);
      }

      return CashierSessionClose(_session_stats, SessionInfo, CashierUserId, IsCountR, out VoucherList, Trx, 0);
    }

    public static CASHIER_SESSION_CLOSE_STATUS CashierSessionClose(TYPE_CASHIER_SESSION_STATS SessionStats,
                                                                   CashierSessionInfo SessionInfo,
                                                                   Int32 CashierUserId,
                                                                   out ArrayList VoucherList,
                                                                   SqlTransaction Trx,
                                                                   Int64 OperationId)
    {
      return CashierSessionClose(SessionStats, SessionInfo, CashierUserId, false, out VoucherList, Trx, OperationId);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Perform the complete Close Cash process
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - None
    // NOTES:
    //    CashierUserId is the user that open the cash session.
    //    SessionInfo.UserId is the user that close the cash session.
    //     - When closing from GUI, they differ.
    //     - When closing from Cashier, they are the same.
    //
    public static CASHIER_SESSION_CLOSE_STATUS CashierSessionClose(TYPE_CASHIER_SESSION_STATS SessionStats,
                                                                   CashierSessionInfo SessionInfo,
                                                                   Int32 CashierUserId,
                                                                   Boolean IsCountR,
                                                                   out ArrayList VoucherList,
                                                                   SqlTransaction Trx,
                                                                   Int64 OperationId)
    {
      StringBuilder _sql_str;
      Currency[] card_balance;
      Int64[] updated_acct_id;
      Currency final_balance;
      CashierMovementsTable _cashier_movements;
      Int32 _num_sessions_opened;
      Currency _collected;
      Currency _national_collected;
      String _national_currency;
      CurrencyIsoType _iso_type;
      String _description;
      String _source_name;
      AlarmSeverity _severity;
      String _message;
      CurrencyExchange _currency_exchange;
      Int64 _operation_id;

      VoucherList = new ArrayList();
      _sql_str = new StringBuilder();
      _iso_type = new CurrencyIsoType();
      _currency_exchange = new CurrencyExchange();
      _operation_id = OperationId;

      try
      {
        updated_acct_id = new Int64[MB_RECORDS_BATCH_SIZE];
        card_balance = new Currency[MB_RECORDS_BATCH_SIZE];
        _num_sessions_opened = 0;

        // RCI 08-AUG-2011: Check if user has already closed his/her cashier session.
        _sql_str.AppendLine(" SELECT   COUNT(*)                  ");
        _sql_str.AppendLine("   FROM   CASHIER_SESSIONS          ");
        _sql_str.AppendLine("  WHERE   CS_USER_ID = @pUserId     ");
        _sql_str.AppendLine("    AND   CS_STATUS  = @pStatusOpen ");

        using (SqlCommand _cmd = new SqlCommand(_sql_str.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CashierUserId;
          _cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

          _num_sessions_opened = (Int32)_cmd.ExecuteScalar();
        }

        if (_num_sessions_opened == 0)
        {
          return CASHIER_SESSION_CLOSE_STATUS.ALREADY_CLOSED;
        }

        if (!MobileBank.DB_MBCloseAllCashSessions(SessionInfo, CashierUserId, Trx))
        {
          return CASHIER_SESSION_CLOSE_STATUS.ERROR;
        }

        // AJQ 08-SEP-2007  
        //  - The balance is read from the database instead of the label
        //  - The Close Session Movement is inserted with the final amount
        //  - Update the status to close

        if (SessionInfo.CashierSessionId == 0)
        {
          Log.Error("Cashier.CashierSessionClose: Error close cashier session. Cashier session id -> " + SessionInfo.CashierSessionId);
          SessionInfo = CommonCashierInformation.CashierSessionInfo();
          return CASHIER_SESSION_CLOSE_STATUS.ERROR;
        }

        final_balance = UpdateSessionBalance(Trx, SessionInfo.CashierSessionId, 0);

        _national_currency = CurrencyExchange.GetNationalCurrency();
        _iso_type.IsoCode = _national_currency;
        _iso_type.Type = CurrencyExchangeType.CURRENCY;

        _national_collected = SessionStats.collected.ContainsKey(_iso_type) ? SessionStats.collected[_iso_type] : 0;

        _cashier_movements = new CashierMovementsTable(SessionInfo);
        _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CLOSE_SESSION, final_balance, 0, "");

        foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency_collected in SessionStats.collected)
        {
          if ((!SessionStats.currencies_balance.ContainsKey(_currency_collected.Key))
            //Bug 23760: Session close: withdrawal at close is always $0
              && (!_currency_collected.Key.IsNationalCurrency(_national_currency))) //Don't add national currency
          {
            SessionStats.currencies_balance.Add(_currency_collected.Key, 0);
          }
        }

        foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency_balance in SessionStats.currencies_balance)
        {
          _collected = SessionStats.collected.ContainsKey(_currency_balance.Key) ? SessionStats.collected[_currency_balance.Key] : 0;

          // If the n� voucher is the same (pinpad) update collected amount wiht the total amount of bank card (Close session)
          if (SessionStats.m_cashier_session_has_pinpad_operations && _currency_balance.Key.Type == CurrencyExchangeType.CARD && _collected == Withdrawal.Count)
          {
            _collected = Withdrawal.AmountTotal;
          }

          SetCashierSessionsByCurrency(SessionInfo.CashierSessionId, _currency_balance.Key.IsoCode, _currency_balance.Key.Type, _currency_balance.Value,
                                       _collected, Trx);
          switch (_currency_balance.Key.Type)
          {
            case CurrencyExchangeType.CURRENCY:
              if (_currency_balance.Value != 0)
              _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CLOSE_SESSION, _currency_balance.Value, 0, "", "", _currency_balance.Key.IsoCode);
              break;
            case CurrencyExchangeType.CASINOCHIP:
            case CurrencyExchangeType.CASINO_CHIP_RE:
              if (_currency_balance.Value != 0)
              _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CLOSE_SESSION, _currency_balance.Value, 0, "", "", _currency_balance.Key.IsoCode, 0, 0, 0, -1, CurrencyExchangeType.CASINO_CHIP_RE, CageCurrencyType.ChipsRedimible, null);
              break;
            case CurrencyExchangeType.CASINO_CHIP_NRE:
              if (_currency_balance.Value != 0)
              _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CLOSE_SESSION, _currency_balance.Value, 0, "", "", _currency_balance.Key.IsoCode, 0, 0, 0, -1, CurrencyExchangeType.CASINO_CHIP_NRE, CageCurrencyType.ChipsNoRedimible, null);
              break;
            case CurrencyExchangeType.CASINO_CHIP_COLOR:
              if (_currency_balance.Value != 0)
              _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CLOSE_SESSION, _currency_balance.Value, 0, "", "", _currency_balance.Key.IsoCode, 0, 0, 0, -1, CurrencyExchangeType.CASINO_CHIP_COLOR, CageCurrencyType.ChipsColor, null);
              break;
            case CurrencyExchangeType.CHECK:
              if (_currency_balance.Value != 0)
              _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CLOSE_SESSION_CHECK, _currency_balance.Value, 0, "", "", _currency_balance.Key.IsoCode, 0, 0, 0, -1, CurrencyExchangeType.CHECK, null, null);
              break;
            case CurrencyExchangeType.CARD:
              if (!SessionStats.m_cashier_session_has_pinpad_operations)
              {
                _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD, _currency_balance.Value, 0, "", "", _currency_balance.Key.IsoCode, Cage.BANK_CARD_CODE, 0, 0, -1, CurrencyExchangeType.CARD, FeatureChips.ConvertToCageCurrencyType(CurrencyExchangeType.CARD), null);
              }
              break;
            default:
              _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CLOSE_SESSION, _currency_balance.Value, 0, "", "", _currency_balance.Key.IsoCode);
              break;
          }
        }

        // DHA: only generate movements for cash over/short when cashier session desk is not integrated
        if (SessionInfo.UserType == GU_USER_TYPE.USER
           && !Cage.IsGamingTable(SessionInfo.CashierSessionId)
           && !IsCountR)
        {
          foreach (KeyValuePair<CurrencyIsoType, Decimal> _diff in SessionStats.collected_diff)
          {
            Int32 _pay_type;
            _pay_type = 0;

            switch (_diff.Key.Type)
            {
              case CurrencyExchangeType.CHECK:
                _pay_type = Cage.CHECK_CODE;
                break;
              case CurrencyExchangeType.CARD:
                _pay_type = Cage.BANK_CARD_CODE;
                break;
            }

            if (_diff.Value < 0)
            {
              // DHA 10-OCT-2014: added new movement if there is cash short on closing cashier session
              _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CASH_CLOSING_SHORT, Math.Abs(_diff.Value), 0, "", "", _diff.Key.IsoCode, _pay_type, 0, 0, -1, _diff.Key.Type, FeatureChips.ConvertToCageCurrencyType(_diff.Key.Type), null);
            }
            else if (_diff.Value > 0)
            {
              // DHA 10-OCT-2014: added new movement if there is cash over on closing cashier session
              _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.CASH_CLOSING_OVER, _diff.Value, 0, "", "", _diff.Key.IsoCode, _pay_type, 0, 0, -1, _diff.Key.Type, FeatureChips.ConvertToCageCurrencyType(_diff.Key.Type), null);
            }
          }
        }

        if (!_cashier_movements.Save(Trx))
        {
          return CASHIER_SESSION_CLOSE_STATUS.ERROR;
        }

        _sql_str = new StringBuilder();
        _sql_str.AppendLine(" UPDATE   CASHIER_SESSIONS                      ");
        _sql_str.AppendLine("    SET   CS_CLOSING_DATE     = GETDATE()       ");
        _sql_str.AppendLine("        , CS_STATUS           = @pStatusClosed  ");
        _sql_str.AppendLine("        , CS_COLLECTED_AMOUNT = @p2             ");
        _sql_str.AppendLine("  WHERE   CS_SESSION_ID       = @p1             ");
        _sql_str.AppendLine("    AND   CS_STATUS           = @pStatusOpen    ");

        using (SqlCommand _cmd = new SqlCommand(_sql_str.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@p1", SqlDbType.BigInt).Value = SessionInfo.CashierSessionId;
          _cmd.Parameters.Add("@p2", SqlDbType.Money).Value = _national_collected.SqlMoney;
          _cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.CLOSED;
          _cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

          _cmd.ExecuteNonQuery();
        }

        if (final_balance != SessionStats.final_balance)
        {
          Log.Warning("CashierSessionClose. Cashier Balance discrepancy for session id: " + SessionInfo.CashierSessionId.ToString()
                    + ". In screen (" + final_balance.ToString() + ") vs Calculated (" + SessionStats.final_balance.ToString() + ").");
        }

        // Insert voucher.

        //// - Create voucher
        //voucher = new VoucherCashDeskClose(CASHIER_MOVEMENT.CLOSE_SESSION, PrintMode.Print, sql_trx);

        //// TODO: Load data on voucher have to be unified

        //// - Build voucher specific data (amounts)
        //voucher.SetupCashCloseVoucher(close_session_stats);

        //// - Save voucher
        //voucher_saved = voucher.Save(sql_trx);

        VoucherList = VoucherBuilder.CashDeskClose(SessionStats,
                                                   OperationId,
                                                   PrintMode.Print,
                                                   Trx,
                                                   Cage.IsGamingTable(SessionInfo.CashierSessionId));

        //    Missing from Mobile Banks
        if (SessionStats.mb_total_balance_lost != 0)
        {
          _source_name = SessionInfo.UserName + "@" + SessionInfo.TerminalName;

          if (SessionInfo.UserName != SessionInfo.AuthorizedByUserName)
          {
            _source_name = _source_name + " - " + Resource.String("STR_VOUCHER_CARD_HANDPAY_SIGN_AUTHORIZED_BY") + ": " + SessionInfo.AuthorizedByUserName;
          }

          _description = Resource.String("STR_EA_CASH_DESK_CLOSE_MB_DIFF_MISSING", ((Currency)(-SessionStats.mb_total_balance_lost)).ToString());

          //Recording the alarm using the Alarm.Register
          Alarm.Register(AlarmSourceCode.User, SessionInfo.UserId, _source_name, (UInt32)AlarmCode.User_WrongDelivered,
                         _description, AlarmSeverity.Warning, DateTime.MinValue, Trx);
        }

        //    Missing from Cashier

        // DHA: only generate alarms for cash over/short when cashier session desk is not integrated
        if (SessionInfo.UserType == GU_USER_TYPE.USER
           && !Cage.IsGamingTable(SessionInfo.CashierSessionId)
           && !IsCountR)
        {
          foreach (KeyValuePair<CurrencyIsoType, Decimal> _diff in SessionStats.collected_diff)
          {
            if (_diff.Value != 0)
            {
              // HBB 01-AUG-2012: Checking if the amount of money expected and delivered are equals

              // RRB 11-OCT-2012: Add who authorized if is different from the cashier
              _source_name = SessionInfo.UserName + "@" + SessionInfo.TerminalName;

              if (SessionInfo.UserName != SessionInfo.AuthorizedByUserName)
              {
                _source_name = _source_name + " - " + Resource.String("STR_VOUCHER_CARD_HANDPAY_SIGN_AUTHORIZED_BY") + ": " + SessionInfo.AuthorizedByUserName;
              }

              switch (_diff.Key.Type)
              {
                case CurrencyExchangeType.CURRENCY:
                  _message = ((Currency)_diff.Value).ToString();
                  if (_diff.Key.IsoCode != _national_currency)
                  {
                    _message = Currency.Format(_diff.Value, _diff.Key.IsoCode);
                  }
                  break;
                case CurrencyExchangeType.CARD:
                case CurrencyExchangeType.CHECK:
                  CurrencyExchange.ReadCurrencyExchange(_diff.Key.Type, _diff.Key.IsoCode, out _currency_exchange, Trx);
                  _message = _currency_exchange.Description + " " + ((Currency)_diff.Value).ToString();
                  break;
                default:
                  _message = ((Currency)_diff.Value).ToString();
                  break;
              }

              _description = Resource.String(Alarm.ResourceId(AlarmCode.User_WrongDelivered), _message);
              if (_diff.Value < 0)
              {
                _severity = AlarmSeverity.Warning;
              }
              else
              {
                _severity = AlarmSeverity.Info;
              }

              //Recording the alarm using the Alarm.Register
              Alarm.Register(AlarmSourceCode.User, SessionInfo.UserId, _source_name, (UInt32)AlarmCode.User_WrongDelivered,
                             _description, _severity, DateTime.MinValue, Trx);
            }
          }
        }

        return CASHIER_SESSION_CLOSE_STATUS.OK;
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
        return CASHIER_SESSION_CLOSE_STATUS.ERROR;
      }
    }

    public static void SetCashierSessionsByCurrency(Int64 CashierSessionId, String IsoCode, CurrencyExchangeType Type, Currency Balance,
                                                    Currency Collected, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Decimal _collected_diff;

      // ICS 04-JUL-2013: Addapted for currency exchange functionality
      if (CurrencyExchange.GetNationalCurrency() == IsoCode && Type == CurrencyExchangeType.CURRENCY)
      {
        return;
      }

      _collected_diff = 0;
      if (Collected > 0)
      {
        _collected_diff = Collected - Balance;
        Balance = 0;
      }

      // DLL 14-AUG-2013: Optimize query
      _sb = new StringBuilder();
      _sb.AppendLine(" IF EXISTS ( SELECT   1  ");
      _sb.AppendLine("               FROM   CASHIER_SESSIONS_BY_CURRENCY ");
      _sb.AppendLine("              WHERE   CSC_SESSION_ID = @pSessionId ");
      _sb.AppendLine("                AND   CSC_ISO_CODE   = @pIsoCode ");
      _sb.AppendLine("                AND   CSC_TYPE       = @pType) ");
      _sb.AppendLine("    UPDATE   CASHIER_SESSIONS_BY_CURRENCY ");
      _sb.AppendLine("       SET   CSC_COLLECTED  = @pCollected ");
      _sb.AppendLine("     WHERE   CSC_SESSION_ID = @pSessionId ");
      _sb.AppendLine("       AND   CSC_ISO_CODE   = @pIsoCode ");
      _sb.AppendLine("       AND   CSC_TYPE       = @pType ");
      _sb.AppendLine("  ELSE ");
      _sb.AppendLine("     INSERT INTO   CASHIER_SESSIONS_BY_CURRENCY ");
      _sb.AppendLine("                 ( CSC_SESSION_ID ");
      _sb.AppendLine("                 , CSC_ISO_CODE ");
      _sb.AppendLine("                 , CSC_TYPE ");
      _sb.AppendLine("                 , CSC_BALANCE ");
      _sb.AppendLine("                 , CSC_COLLECTED  ");
      _sb.AppendLine("                 ) ");
      _sb.AppendLine("          VALUES    ");
      _sb.AppendLine("                 ( @pSessionId ");
      _sb.AppendLine("                 , @pIsoCode ");
      _sb.AppendLine("                 , @pType ");
      _sb.AppendLine("                 , @pBalance ");
      _sb.AppendLine("                 , @pCollected ");
      _sb.AppendLine("                 ) ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
        _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IsoCode;
        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Type;
        _cmd.Parameters.Add("@pBalance", SqlDbType.Money).Value = Balance.SqlMoney;
        _cmd.Parameters.Add("@pCollected", SqlDbType.Money).Value = Collected.SqlMoney;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error(String.Format("SetCashierSessionsByCurrency: Insert or Update CASHIER_SESSIONS_BY_CURRENCY failed. SessionId: {0}", CashierSessionId));

          return;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Unassigns a cashier session from a mobile bank terminal.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int64 SessionId
    //          - Int64 MobileBankId
    //          - SqlTransaction Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    //
    public static Boolean UnassignMBCashierSession(Int64 SessionId, Int64 MobileBankId, SqlTransaction Trx)
    {
      StringBuilder _sql_query;

      try
      {
        _sql_query = new StringBuilder();

        _sql_query.AppendLine(" UPDATE   MOBILE_BANKS                        ");
        _sql_query.AppendLine("    SET   MB_CASHIER_SESSION_ID = NULL        ");
        _sql_query.AppendLine("  WHERE   MB_CASHIER_SESSION_ID = @pSessionId ");

        if (MobileBankId != -1)
        {
          _sql_query.AppendLine("    AND   MB_ACCOUNT_ID = @pMobileBankId      ");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
          _sql_cmd.Parameters.Add("@pMobileBankId", SqlDbType.BigInt).Value = MobileBankId;

          _sql_cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UnassignCashierSession

    public static Boolean UnassignMBCashierSession(Int64 SessionId, SqlTransaction Trx)
    {
      return UnassignMBCashierSession(SessionId, -1, Trx);
    } // UnassignCashierSession

    //------------------------------------------------------------------------------
    // PURPOSE: Marks a cashier session as pending closing for a terminal.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int64 MobileBankId
    //          - String MBTerminalName
    //          - SqlTransaction Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    //
    public static Boolean CashierSessionPendingClosing(Int64 MobileBankId,
                                                       String MBTerminalName,
                                                       SqlTransaction Trx, Int64 OperationId = 0)
    {
      StringBuilder _sql_query;
      CashierSessionInfo _session_info;
      CashierMovementsTable _cashier_movements;

      try
      {
        _session_info = CommonCashierInformation.CashierSessionInfo();

        // Unassign cashier session from MB
        if (!Cashier.UnassignMBCashierSession(_session_info.CashierSessionId, MobileBankId, Trx))
        {
          return false;
        }

        // Check if other MB are still using the session and 
        // update status accordingly
        _sql_query = new StringBuilder();
        _sql_query.AppendLine(" DECLARE @count as INT                                  ");

        _sql_query.AppendLine(" SELECT   @count                = COUNT(*)              ");
        _sql_query.AppendLine("   FROM   MOBILE_BANKS                                  ");
        _sql_query.AppendLine("  WHERE   MB_CASHIER_SESSION_ID = @pSessionId           ");

        _sql_query.AppendLine(" UPDATE   CASHIER_SESSIONS                              ");
        _sql_query.AppendLine("    SET   CS_STATUS     = CASE WHEN @count = 0          ");
        _sql_query.AppendLine("                               THEN @pStatusPending     ");
        _sql_query.AppendLine("                               ELSE @pStatusOpenPending ");
        _sql_query.AppendLine("                               END                      ");
        _sql_query.AppendLine("  WHERE   CS_SESSION_ID = @pSessionId                   ");
        _sql_query.AppendLine("    AND   CS_STATUS    <> @pStatusClosed                ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = _session_info.CashierSessionId;
          _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING;
          _sql_cmd.Parameters.Add("@pStatusOpenPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN_PENDING;
          _sql_cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.CLOSED;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        // Insert cashier movement
        _cashier_movements = new CashierMovementsTable(_session_info, MBTerminalName);
        _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.CASH_PENDING_CLOSING, 0, 0, "");

        if (!_cashier_movements.Save(Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // CashierSessionPendingClosing

    //------------------------------------------------------------------------------
    // PURPOSE: Marks a cashier session as pending closing for a terminal.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int32 TerminalId
    //          - String TerminalName
    //          - SqlTransaction Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    //
    public static MB_CASHIER_SESSION_CLOSE_STATUS WCPCashierSessionPendingClosing(Int32 TerminalId, SqlTransaction Trx, Int64 OperationId = 0)
    {
      Int64 _session_id;
      Int64 _mb_id;
      CashierSessionInfo _session_info;
      Int32 _user_id;
      String _user_name;
      Int32 _cashier_id;
      String _cashier_name;
      Currency _final_movement_balance;
      MB_USER_TYPE _mb_user_type;
      WSI.Common.Terminal.TerminalInfo _terminal_info;
      DateTime _dummy_gaming_day;

      try
      {
        //If User System doesn't exist, error
        if (!WSI.Common.Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, Trx))
        {
          return MB_CASHIER_SESSION_CLOSE_STATUS.ERROR;
        }

        _mb_user_type = GetMobileBankUserType(_terminal_info.TerminalType);
        if (_mb_user_type == MB_USER_TYPE.NOT_ASSIGNED)
        {
          return MB_CASHIER_SESSION_CLOSE_STATUS.ERROR;
        }

        StringBuilder _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   MB_ACCOUNT_ID ");
        _sb.AppendLine("        , MB_CASHIER_SESSION_ID ");
        _sb.AppendLine("   FROM   MOBILE_BANKS ");
        _sb.AppendLine("  WHERE   MB_TERMINAL_ID  = @pTerminalId ");
        _sb.AppendLine("    AND   MB_ACCOUNT_TYPE = @pAccountType ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pAccountType", SqlDbType.Int).Value = _mb_user_type;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return MB_CASHIER_SESSION_CLOSE_STATUS.NO_SESSION_ASSIGNED;
            }

            _mb_id = _reader.GetInt64(0);

            if (_reader.IsDBNull(1))
            {
              return MB_CASHIER_SESSION_CLOSE_STATUS.NO_SESSION_ASSIGNED;
            }

            _session_id = _reader.GetInt64(1);
          }
        }

        _session_info = CommonCashierInformation.CashierSessionInfo();

        if (!Common.Cashier.DB_ReadOpenedCashierSession(_session_id, Trx, out _user_id, out _user_name, out _cashier_id, out _cashier_name, out _final_movement_balance, out _dummy_gaming_day))
        {
          return MB_CASHIER_SESSION_CLOSE_STATUS.ERROR;
        }

        CommonCashierInformation.SetCashierInformation(_session_id, _user_id, _user_name, _cashier_id, _cashier_name);

        try
        {
          if (!Cashier.CashierSessionPendingClosing(_mb_id, _terminal_info.Name, Trx, OperationId))
          {
            return MB_CASHIER_SESSION_CLOSE_STATUS.ERROR;
          }

          if (Common.TITO.Utils.IsTitoMode() && Misc.SystemMode() != SYSTEM_MODE.GAMINGHALL)
          {
            // Create cage movement
            if (!TITO.TITO_ChangeStacker.InsertCageMovement(CommonCashierInformation.CashierSessionInfo(), Trx))
            {
              Log.Error("WCPCashierSessionPendingClosing: Error inserting cage movement. Session id -> " + _session_id);
            }
          }
        }
        finally
        {
          CommonCashierInformation.SetCashierInformation(_session_info.CashierSessionId, _session_info.UserId, _session_info.UserName,
                                                         _session_info.TerminalId, _session_info.TerminalName);
        }

        return MB_CASHIER_SESSION_CLOSE_STATUS.OK;

      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
        Log.Error("WCPCashierSessionPendingClosing: Error. Session id -> " + TerminalId);
      }
      return MB_CASHIER_SESSION_CLOSE_STATUS.ERROR;

    } // WCPCashierSessionPendingClosing

    public class CashierSessionData
    {
      public String Cashier_Name
      {
        get
        {
          return cashier_name;
        }
        set
        {
          cashier_name = value;
        }
      }

      private String cashier_name;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Cashier information related to param session id.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int64 CashierSessionId
    //          
    //      - OUTPUT:
    //          - CashierOutputParams OutputParams
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    //
    public static Boolean GetCashierSessionData(Int64 CashierSessionId, out CashierSessionData SessionData)
    {
      SessionData = new CashierSessionData();

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          StringBuilder _sb = new StringBuilder();

          _sb.AppendLine("SELECT   CS_NAME");
          _sb.AppendLine("  FROM   CASHIER_SESSIONS");
          _sb.AppendLine(" WHERE   CS_SESSION_ID = @pCashierSessionId");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                // CS_NAME
                SessionData.Cashier_Name = _reader.IsDBNull(0) ? string.Empty : _reader.GetString(0);

                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetCashierInformation

    /// <summary>
    /// Get cashier information by session id.
    /// </summary>
    /// <param name="SessionId"></param>
    /// <param name="Trx"></param>
    /// <param name="UserId"></param>
    /// <returns></returns>
    public static Boolean GetCashierInfoBySession(Int64 SessionId, SqlTransaction Trx, out Int32 UserId)
    {
      String _user_name;
      Int32 _terminal_id;
      String _terminal_name;
      Currency _cashier_balance;

      return GetCashierInfoBySession(SessionId, Trx, out UserId, out _user_name, out _terminal_id, out _terminal_name, out _cashier_balance);
    } // GetCashierInfoBySession

    /// <summary>
    /// Get cashier information by session id.
    /// </summary>
    /// <param name="SessionId"></param>
    /// <param name="Trx"></param>
    /// <param name="UserId"></param>
    /// <param name="UserName"></param>
    /// <param name="TerminalId"></param>
    /// <param name="TerminalName"></param>
    /// <param name="CashierBalance"></param>
    /// <returns></returns>
    public static Boolean GetCashierInfoBySession(Int64 SessionId, SqlTransaction Trx, out Int32 UserId, out String UserName,
                                                  out Int32 TerminalId, out String TerminalName, out Currency CashierBalance)
    {
      StringBuilder _sql_str;

      UserId = 0;
      UserName = "";
      TerminalId = 0;
      TerminalName = "";
      CashierBalance = 0;

      try
      {
        _sql_str = new StringBuilder();
        _sql_str.AppendLine(" SELECT   TOP 1                               ");
        _sql_str.AppendLine("          CS_USER_ID                          ");
        _sql_str.AppendLine("        , GU_USERNAME                         ");
        _sql_str.AppendLine("        , CS_CASHIER_ID                       ");
        _sql_str.AppendLine("        , CT_NAME                             ");
        _sql_str.AppendLine("        , CS_BALANCE                          ");
        _sql_str.AppendLine("   FROM   CASHIER_SESSIONS                    ");
        _sql_str.AppendLine("        , GUI_USERS                           ");
        _sql_str.AppendLine("        , CASHIER_TERMINALS                   ");
        _sql_str.AppendLine("  WHERE   CS_SESSION_ID = @pCashierSessionID  ");
        _sql_str.AppendLine("    AND   CS_CASHIER_ID = CT_CASHIER_ID       ");
        _sql_str.AppendLine("    AND   CS_USER_ID    = GU_USER_ID          ");

        using (SqlCommand _sql_command = new SqlCommand(_sql_str.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pCashierSessionID", SqlDbType.BigInt).Value = SessionId;

          using (SqlDataReader _reader = _sql_command.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            UserId = _reader.GetInt32(0);
            UserName = _reader.GetString(1);
            TerminalId = _reader.GetInt32(2);
            TerminalName = _reader.GetString(3);
            CashierBalance = _reader.GetSqlMoney(4);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // GetCashierInfoBySession

    //------------------------------------------------------------------------------
    // PURPOSE: Get a CashierSessionInfo by UserId, Status and CashierId
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - UserId
    //          - Status
    //          
    //      - OUTPUT:
    //          -  CashierSessionInfo CashierSessionInfo
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    public static Boolean GetCashierSessionFromTerminal(Int32 TerminalId, Int32 UserId, CASHIER_SESSION_STATUS Status, out CashierSessionInfo CashierSessionInfo, SqlTransaction Trx)
    {
      StringBuilder _sb = new StringBuilder();
      CashierSessionInfo = new CashierSessionInfo();

      try
      {
        _sb.AppendLine("   SELECT    CS.CS_SESSION_ID                                                          ");
        _sb.AppendLine("           , CT.CT_CASHIER_ID                                                          ");
        _sb.AppendLine("           , CS.CS_USER_ID                                                             ");
        _sb.AppendLine("           , T.TE_NAME                                                                 ");
        _sb.AppendLine("           , GU.GU_USERNAME                                                            ");
        _sb.AppendLine("           , CS.CS_GAMING_DAY                                                          ");
        _sb.AppendLine("      FROM   TERMINALS T                                                               ");
        _sb.AppendLine("INNER JOIN   CASHIER_TERMINALS CT                                                      ");
        _sb.AppendLine("        ON   T.TE_TERMINAL_ID = CT.CT_TERMINAL_ID                                      ");
        _sb.AppendLine("INNER JOIN   CASHIER_SESSIONS CS  WITH(INDEX(IX_cs_user_id_status_cashier_id))         ");
        _sb.AppendLine("        ON   CT.CT_CASHIER_ID = CS.CS_CASHIER_ID                                       ");
        _sb.AppendLine("INNER JOIN   GUI_USERS GU                                                              ");
        _sb.AppendLine("        ON   CS.CS_USER_ID = GU.GU_USER_ID                                             ");
        _sb.AppendLine("     WHERE   CS_USER_ID = @pUserId                                                     ");
        _sb.AppendLine("       AND   CS_STATUS = @pStatus                                                      ");
        _sb.AppendLine("       AND   T.TE_TERMINAL_ID = @pTerminalId                                           ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)Status;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              CashierSessionInfo = new CashierSessionInfo()
              {
                CashierSessionId = (Int64)_reader[0],
                TerminalId = (Int32)_reader[1], // The Name is incorrect because the value is the CashierId
                UserId = (Int32)_reader[2],
                TerminalName = (String)_reader[3],
                UserName = (String)_reader[4],
                GamingDay = (DateTime)_reader[5],
              };

              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // GeCashierSessionByTerminalId

    //------------------------------------------------------------------------------
    // PURPOSE: Get a CashierSessionInfo by UserId, Status and CashierId
    //
    //  PARAMS:
    //      - INPUT:
    //          - CountRId
    //          - UserId
    //          - Status
    //          
    //      - OUTPUT:
    //          -  CashierSessionInfo CashierSessionInfo
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    public static Boolean GetCashierSessionFromCountR(Int32 CountRId, Int32 UserId, CASHIER_SESSION_STATUS Status, out CashierSessionInfo CashierSessionInfo, SqlTransaction Trx)
    {
      StringBuilder _sb = new StringBuilder();
      CashierSessionInfo = new CashierSessionInfo();

      try
      {
        _sb.AppendLine("   SELECT    CS.CS_SESSION_ID                                                          ");
        _sb.AppendLine("           , CT.CT_CASHIER_ID                                                          ");
        _sb.AppendLine("           , CS.CS_USER_ID                                                             ");
        _sb.AppendLine("           , CR.CR_NAME                                                                 ");
        _sb.AppendLine("           , GU.GU_USERNAME                                                            ");
        _sb.AppendLine("           , CS.CS_GAMING_DAY                                                          ");
        _sb.AppendLine("      FROM   COUNTR CR                                                               ");
        _sb.AppendLine("INNER JOIN   CASHIER_TERMINALS CT                                                      ");
        _sb.AppendLine("        ON   CR.CR_COUNTR_ID = CT.CT_COUNTR_ID                                            ");
        _sb.AppendLine("INNER JOIN   CASHIER_SESSIONS CS  WITH(INDEX(IX_cs_user_id_status_cashier_id))         ");
        _sb.AppendLine("        ON   CT.CT_CASHIER_ID = CS.CS_CASHIER_ID                                       ");
        _sb.AppendLine("INNER JOIN   GUI_USERS GU                                                              ");
        _sb.AppendLine("        ON   CS.CS_USER_ID = GU.GU_USER_ID                                             ");
        _sb.AppendLine("     WHERE   CS_USER_ID = @pUserId                                                     ");
        _sb.AppendLine("       AND   CS_STATUS = @pStatus                                                      ");
        _sb.AppendLine("       AND   CR.CR_COUNTR_ID = @pCountRId                                           ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)Status;
          _cmd.Parameters.Add("@pCountRId", SqlDbType.Int).Value = CountRId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              CashierSessionInfo = new CashierSessionInfo()
              {
                CashierSessionId = (Int64)_reader[0],
                TerminalId = (Int32)_reader[1],
                UserId = (Int32)_reader[2],
                TerminalName = (String)_reader[3],
                UserName = (String)_reader[4],
                GamingDay = (DateTime)_reader[5],
                AuthorizedByUserName = String.Empty,
                AuthorizedByUserId = 0,
              };

              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // GeCashierSessionByTerminalId

    //------------------------------------------------------------------------------
    // PURPOSE: Execute the ticket creation method in common Transaction
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //        - UpdateAccount
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //        - Ticket
    //
    //  RETURNS:
    //        - TRUE: all is OK
    //        - FALSE: something went wrong
    //
    public static Boolean Common_CreateTicket(ParamsTicketOperation Params, Boolean UpdateAccount, out Ticket Ticket, SqlTransaction SqlTrx)
    {
      return Common_CreateTicket(Params, UpdateAccount, 0, out Ticket, SqlTrx);
    } // Common_CreateTicket

    public static Boolean Common_CreateTicket(ParamsTicketOperation Params, Boolean UpdateAccount, Decimal InitialBalance, out Ticket Ticket, SqlTransaction SqlTrx)
    {
      CashierSessionInfo _session_info;
      CashierMovementsTable _cashier_movements;
      AccountMovementsTable _account_movements;

      Ticket = Common_CreateTicketData(Params, SqlTrx);

      if (Ticket == null
        || Ticket.ValidationNumber <= 0)
      {
        return false;
      }

      _session_info = Params.session_info;
      _cashier_movements = new CashierMovementsTable(_session_info);
      _account_movements = new AccountMovementsTable(_session_info);

      if (!Ticket.DB_CreateTicket(SqlTrx, Params.in_account.TrackData, UpdateAccount, InitialBalance, Params.is_swap, _cashier_movements, _account_movements))
      {
        return false;
      }

      if (!_account_movements.Save(SqlTrx))
      {
        return false;
      }

      if (Params.terminal_types != TerminalTypes.PROMOBOX)
      {
        if (!_cashier_movements.Save(SqlTrx))
        {
          return false;
        }
      }


      return true;
    } // Common_CreateTicket

    //------------------------------------------------------------------------------
    // PURPOSE: Internal function to create ticket structure from ParamsTicketOperation
    //          Some properties are setted to its default values
    //          The Validation Number is created From Cashier-Terminal data
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //        - Ticket: nw ticket structure
    //
    public static Ticket Common_CreateTicketData(ParamsTicketOperation Params, SqlTransaction SqlTrx)
    {
      Ticket _ticket;
      Int32 _expiration_days;
      DateTime _expiration_datetime;
      Int64 _sequence;
      Boolean _is_countR;

      _ticket = new Ticket();
      _is_countR = (Params.tito_terminal_types == TITO_TERMINAL_TYPE.COUNTR);

      if (_is_countR)
      {
        // Set validation number and get sequence. 
        if (!CountR_GetValidationNumberAndSequence(Params, ref _ticket, out _sequence))
        {
          return null;
        }
      }
      else
      {
        _ticket.ValidationNumber = Common_ComposeValidationNumber(out _sequence, SqlTrx, (Int32)Params.cashier_terminal_id);
      }

      // DDM & DHA --> MachineTicketNumber are the last 4 digits from the Sequence
      _ticket.MachineTicketNumber = (Int32)_sequence % 10000;

      if (_ticket.ValidationNumber > 0)
      {
        _ticket.Amount = Params.out_cash_amount;
        _ticket.TicketType = Params.ticket_type;
        _ticket.Status = TITO_TICKET_STATUS.VALID;
        _ticket.ValidationType = TITO_VALIDATION_TYPE.SYSTEM;
        _ticket.CreatedTerminalId = (Int32)Params.cashier_terminal_id;
        _ticket.CreatedTerminalType = (_is_countR) ? (Int32)TITO_TERMINAL_TYPE.COUNTR : (Int32)TITO_TERMINAL_TYPE.CASHIER;
        _ticket.CreatedDateTime = WGDB.Now;
        _ticket.CreatedAccountID = Params.in_account.AccountId;
        _ticket.PromotionID = Params.out_promotion_id;
        _ticket.AccountPromoId = Params.out_account_promo_id;
        _ticket.TransactionId = Params.out_operation_id;
        _ticket.Amt_0 = Params.in_cash_amt_0;
        _ticket.Cur_0 = Params.in_cash_cur_0;
        _ticket.Amt_1 = Params.in_cash_amt_1;
        _ticket.Cur_1 = Params.in_cash_cur_1;

        if (Params.terminal_id != 0)
        {
          _ticket.CreatedTerminalId = (Int32)Params.terminal_id;
          _ticket.CreatedTerminalType = (Int32)TITO_TERMINAL_TYPE.TERMINAL;
        }

        _expiration_days = WSI.Common.TITO.Utils.DefaultTicketExpiration(_ticket.TicketType);
        _ticket.ExpirationDateTime = _ticket.CreatedDateTime.AddDays(_expiration_days);

        if (_ticket.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
        {
          if (Promotion.GetExpirationDate(SqlTrx, _ticket.PromotionID, out _expiration_datetime))
          {
            _ticket.ExpirationDateTime = _expiration_datetime;
          }
          else
          {
            _ticket.ExpirationDateTime = _ticket.CreatedDateTime.AddDays(_expiration_days);
          }
        }
      }

      return _ticket;
    } // CreateTicketData

    //------------------------------------------------------------------------------
    // PURPOSE: Comporse ValidationNumber for new Tickets, related to CASHIER-TERMINAL
    //
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //        - Int64 or 0
    //
    public static Int64 Common_ComposeValidationNumber(out Int64 Sequence, SqlTransaction SqlTrx, Int32 CashierTerminalId)
    {
      Int32 _machine_id;
      Int32 _cashier_terminal_id;
      Int64 _low_part;

      Sequence = 0;
      try
      {
        _cashier_terminal_id = CashierTerminalId;

        if (!DB_LoadParamsForValidationNumber(_cashier_terminal_id, out _machine_id, out Sequence, SqlTrx))
        {
          return 0;
        }

        SystemValidationNumber _tito = new SystemValidationNumber();
        _low_part = _tito.ValidationNumber(true, _machine_id, (Int32)Sequence);

        return _low_part;
        }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return 0;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns values for ValidationNumber creation, in Cashier Terminal case
    //          Result will be two numbers: MachineId and SequenceId
    //          Operation is enclosed in an Isolated transaction
    // 
    //  PARAMS:
    //      - INPUT:
    //        - CashierTerminalId: ID of cahsier terminal
    //        - SqlTransaction: common transaction in all Create ticket operation
    //
    //      - OUTPUT:
    //        - CashierId: value of CT_CASHIER_ID column
    //        - SequenceId: value of CT_SEQUENCE_ID column + 1
    //
    //  RETURNS:
    //      - True or false
    //
    public static Boolean DB_LoadParamsForValidationNumber(Int32 CashierTerminalId, out Int32 MachineId, out Int64 SequenceId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      MachineId = 0;
      SequenceId = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   CASHIER_TERMINALS                               ");
        _sb.AppendLine("    SET   CT_SEQUENCE_ID = CASE WHEN (ISNULL(CT_SEQUENCE_ID, 0) + 1) > @pMaxSequence THEN 1  ");
        _sb.AppendLine("                           ELSE  ISNULL(CT_SEQUENCE_ID, 0) + 1 END");
        _sb.AppendLine(" OUTPUT   INSERTED.CT_CASHIER_ID  'CashierId'             ");
        _sb.AppendLine("        , INSERTED.CT_SEQUENCE_ID 'SequenceId'            ");
        _sb.AppendLine("  WHERE   CT_CASHIER_ID = @pCashierTerminalId             ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pCashierTerminalId", SqlDbType.Int).Value = CashierTerminalId;
          _sql_cmd.Parameters.Add("@pMaxSequence", SqlDbType.BigInt).Value = Ticket.MAX_CASHIER_SEQUENCE_ID;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            MachineId = _reader.GetInt32(0);
            SequenceId = _reader.GetInt64(1);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // DB_LoadTicketType

    //------------------------------------------------------------------------------
    // PURPOSE: Get Cashier information related to Operation Id.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int64 OperationId
    //          
    //      - OUTPUT:
    //          - CashierOutputParams OutputParams
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    public static Boolean GetCashierNameFromCashierMovements(Int64 OperationId, CASHIER_MOVEMENT CashierMovemenType, out String CashierName)
    {
      StringBuilder _sb;
      CashierName = String.Empty;
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("   SELECT    CM_SESSION_ID                                       ");
          _sb.AppendLine("          ,  CM_TYPE                                             ");
          _sb.AppendLine("          ,  CM_CASHIER_NAME                                     ");
          _sb.AppendLine("     FROM    CASHIER_MOVEMENTS WITH(INDEX(IX_cm_operation_id))   ");
          _sb.AppendLine("    WHERE    CM_OPERATION_ID = @OperationId                      ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@OperationId", SqlDbType.BigInt).Value = OperationId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                if ((CASHIER_MOVEMENT)_reader[1] == CashierMovemenType)
                {
                  CashierName = _reader.IsDBNull(2) ? String.Empty : (String)_reader[2];

                  return true;
                }
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCashierSessionDataByOperationId

    //------------------------------------------------------------------------------
    // PURPOSE: Get a CashierSessionInfo by Id
    //
    //  PARAMS:
    //      - INPUT:
    //          - CashierSessionId
    //          
    //      - OUTPUT:
    //          -  CashierSessionInfo 
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    public static Boolean GetCashierSessionById(Int64 CashierSessionId, out CashierSessionInfo CashierSessionInfo, SqlTransaction Trx)
    {
      StringBuilder _sb = new StringBuilder();
      CashierSessionInfo = new CashierSessionInfo();

      try
      {
        _sb.AppendLine("   SELECT    CS.CS_SESSION_ID                             ");
        _sb.AppendLine("           , CS.CS_CASHIER_ID                             ");
        _sb.AppendLine("           , CS.CS_USER_ID                                ");
        _sb.AppendLine("           , CT.CT_NAME                                   ");
        _sb.AppendLine("           , GU.GU_USERNAME                               ");
        _sb.AppendLine("           , CS.CS_GAMING_DAY                             ");
        _sb.AppendLine("      FROM   CASHIER_SESSIONS CS                          ");
        _sb.AppendLine("INNER JOIN   GUI_USERS GU                                 ");
        _sb.AppendLine("        ON   CS.CS_USER_ID = GU.GU_USER_ID                ");
        _sb.AppendLine("INNER JOIN   CASHIER_TERMINALS CT                         ");
        _sb.AppendLine("        ON   CT.CT_CASHIER_ID = CS.CS_CASHIER_ID          ");
        _sb.AppendLine("     WHERE   CS.CS_SESSION_ID = @pSessionId               ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              CashierSessionInfo = new CashierSessionInfo()
              {
                CashierSessionId = (Int64)_reader[0],
                TerminalId = (Int32)_reader[1], // Is the CashierId
                UserId = (Int32)_reader[2],
                TerminalName = (String)_reader[3], // Is the cashier name
                UserName = (String)_reader[4],
                GamingDay = (DateTime)_reader[5],
              };

              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // GetCashierSessionById

    //------------------------------------------------------------------------------
    // PURPOSE: Get last CashOut from Terminal 
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - Seconds
    //          
    //      - OUTPUT:
    //          -  
    //
    // RETURNS: Decimal amount of the last cash out 
    public static Decimal GetLastCashOutFromCashier(Int64 TerminalId, Int32 Seconds)
    {
      StringBuilder _sb;
      Decimal _amount;

      _amount = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   TOP 1 ISNULL(CM_SUB_AMOUNT,0)                  ");
          _sb.AppendLine("  FROM   CASHIER_MOVEMENTS                              ");
          _sb.AppendLine("  WHERE  CM_DATE >= @pMovementDate                      ");
          _sb.AppendLine("    AND  CM_TYPE = @pMovementType                       ");

          if (TerminalId != 0)
          {
            _sb.AppendLine("    AND  CM_CASHIER_ID = @pCashierId                    ");
          }

          _sb.AppendLine("ORDER BY CM_DATE DESC                                   ");


          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMovementDate", SqlDbType.DateTime).Value = WGDB.Now.AddSeconds(-Seconds);
            _sql_cmd.Parameters.Add("@pMovementType", SqlDbType.Int).Value = (Int32)CASHIER_MOVEMENT.CASH_OUT;

            if (TerminalId != 0)
            {
              _sql_cmd.Parameters.Add("@pCashierID", SqlDbType.Int).Value = TerminalId;
            }

            Object _obj;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              return (Decimal)_obj;
            }

          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _amount;
    } //GetLastCashOutFromCashier


    //------------------------------------------------------------------------------
    // PURPOSE: Get sum of last operations amount
    //
    //  PARAMS:
    //      - INPUT:
    //          - MovementIdList 
    //          - AccountID
    //          - ISOCode
    //          
    //      - OUTPUT:
    //          
    // RETURNS: Decimal amount of the last operations of the gaming day.
    public static Decimal GetLastOperationsAmount(List<CASHIER_MOVEMENT> MovementIdList, Int64 AccountId, String IsoCode = "")
    {
      StringBuilder _sb;
      Decimal _amount;
      String _str_movement_id;

      _amount = 0;

      if (MovementIdList.Count == 0)
      {
        Log.Error("GetAddSubOperation: No Movement defined");
        return _amount;
      }

      _str_movement_id = string.Join(",", MovementIdList.ConvertAll(x => ((Int32)x).ToString())
                                     .ToArray());

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT  SUM (CASE CM_TYPE                                       ");

          foreach (CASHIER_MOVEMENT _movement_id in MovementIdList)
          {
            Int32 _i_movement_id = (Int32)_movement_id;
            switch (_movement_id)
            {
              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY:
              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
                _sb.AppendFormat("   WHEN {0} THEN CM_INITIAL_BALANCE  ", _i_movement_id);
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY:
                _sb.AppendFormat("   WHEN {0} THEN -CM_INITIAL_BALANCE ", _i_movement_id);
                break;

              default:
                Log.Warning("GetAddSubOperation: Operation not defined");
                break;
            }

            _sb.AppendLine("");
          }

          _sb.AppendLine("	END ) AS Amount                                            ");
          _sb.AppendLine("FROM CASHIER_MOVEMENTS                                       ");
          _sb.AppendFormat("WHERE CM_TYPE IN ({0})", _str_movement_id);
          _sb.AppendLine("");

          _sb.AppendLine(" AND  CM_DATE >= @pMovementDate ");

          if (AccountId != 0)
          {
            _sb.AppendLine(" AND  CM_ACCOUNT_ID = @pAccountId ");
          }

          if (!String.IsNullOrEmpty(IsoCode))
          {
            _sb.AppendLine(" AND  CM_CURRENCY_ISO_CODE = @pMovementISOCODE ");
          }

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMovementDate", SqlDbType.DateTime).Value = Misc.TodayOpening();

            if (AccountId != 0)
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            }

            if (!String.IsNullOrEmpty(IsoCode))
            {
              _sql_cmd.Parameters.Add("@pMovementISOCODE", SqlDbType.NVarChar).Value = IsoCode;
            }

            Object _obj;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              return (Decimal)_obj > 0 ? (Decimal)_obj : 0;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _amount;
    } //GetLastCashOutFromCashier

    //------------------------------------------------------------------------------
    // PURPOSE: Get last operation Iso Code 
    //
    //  PARAMS:
    //      - INPUT:
    //          - MovementIdList 
    //          - AccountID
    //          
    //      - OUTPUT:
    //          
    // RETURNS:Iso code of last operation
    public static String GetLastOperationIsoCode(List<CASHIER_MOVEMENT> MovementIdList, Int64 AccountId)
    {
      StringBuilder _sb;
      String _iso_code;
      String _str_movement_id;

      _str_movement_id = string.Join(",", MovementIdList.ConvertAll(x => ((Int32)x).ToString())
                                     .ToArray());

      _iso_code = String.Empty;

      if (MovementIdList.Count == 0)
      {
        Log.Error("GetLastOperationIsoCode: No Movement defined");
        return String.Empty;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT  TOP 1 CM_CURRENCY_ISO_CODE     ");
          _sb.AppendLine("FROM CASHIER_MOVEMENTS                 ");
          _sb.AppendFormat("WHERE CM_TYPE IN ({0})", _str_movement_id);
          _sb.AppendLine("  AND CM_DATE >= @pMovementDate        ");

          if (AccountId != 0)
          {
            _sb.AppendLine("  AND CM_ACCOUNT_ID = @pAccountId      ");
          }

          _sb.AppendLine("ORDER BY CM_DATE DESC                  ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMovementDate", SqlDbType.DateTime).Value = Misc.TodayOpening();

            if (AccountId != 0)
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            }

            Object _obj;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              return (String)_obj;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _iso_code;
    } //GetLastCashOutFromCashier

    //------------------------------------------------------------------------------
    // PURPOSE: Get last withdrawal date of given AccountId
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountID
    //          
    //      - OUTPUT: 
    //          - Last withdrawal date
    //          
    // RETURNS: Operation succesful or not
    public static bool GetLastWithdrawalDate(Int64 AccountId, out DateTime DateLastRefund)
    {
      StringBuilder _sb;
      DateLastRefund = DateTime.MinValue;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          
          // LTC 2017-JUL-10
          _sb.AppendLine(@"  

                          DECLARE @DATE AS DATETIME             

                          SELECT TOP 1 @DATE = CM_DATE
                          FROM CASHIER_MOVEMENTS
                          WHERE CM_TYPE = 5     ");

          if (AccountId > 0)
          {
            _sb.AppendLine("  AND CM_ACCOUNT_ID = @pAccountId ");
          }

          _sb.AppendLine(@"

                          ORDER BY CM_DATE DESC
    
                          IF @DATE IS NULL
                          BEGIN
                              SELECT TOP 1 @DATE = CM_DATE
                              FROM CASHIER_MOVEMENTS    
                          ");

                          if (AccountId > 0)
                          {
                            _sb.AppendLine("  WHERE CM_ACCOUNT_ID = @pAccountId ");
                          }

          _sb.AppendLine(@"
                         
                          ORDER BY CM_DATE ASC
                          END

                          IF @DATE IS NULL
                          BEGIN
                            SELECT @DATE = CONVERT(DATETIME,0)
                          END

                          SELECT @DATE

                          ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (AccountId > 0)
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            }

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                DateLastRefund = _reader.GetDateTime(0);

                return true;
              }
            }
          }
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } //GetLastWithdrawalDate

    //------------------------------------------------------------------------------
    // PURPOSE: Get movements amount since given date
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountID
    //          - MovementTypesList
    //          - LastWithdrawalDate
    //          
    // RETURNS: Movements amount
    public static Decimal GetMovementLastAmount(Int64 AccountId, List<Int32> MovementTypesList, DateTime FromDate)
    {
      StringBuilder _sb;
      Decimal _total_movs_amount;
      Int32 _count;
      Boolean _fist_element;
      String _str_where;

      _total_movs_amount = 0;
      _count = 1;
      _fist_element = true;
      _str_where = String.Empty;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT ISNULL(SUM(CM_ADD_AMOUNT) - SUM(CM_SUB_AMOUNT), 0) ");
          _sb.AppendLine("FROM CASHIER_MOVEMENTS ");
          _sb.AppendLine("WHERE CM_ACCOUNT_ID = @pAccountId ");
          _sb.AppendLine("  AND CM_DATE > @pDate ");

          foreach (Int32 _movement_id in MovementTypesList)
          {
            if (_fist_element)
            {
              _str_where = " AND (CM_TYPE = @pMovementType" + _count.ToString();
              _fist_element = false;
            }
            else
            {
              _str_where += " OR CM_TYPE = @pMovementType" + _count.ToString();
            }

            _count++;
          }

          // Do we have to close the paranthesis?
          if (MovementTypesList.Count > 0)
          {
            _str_where += ")";
          }

          _sb.AppendLine(_str_where);

          _count = 1;
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = FromDate;

            foreach (Int32 _movement_id_2 in MovementTypesList)
            {
              _sql_cmd.Parameters.Add("@pMovementType" + _count.ToString(), SqlDbType.Int).Value = _movement_id_2;
              _count++;
            }

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                _total_movs_amount = _reader.GetDecimal(0);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _total_movs_amount;
    } //GetMovementLastAmount

    //------------------------------------------------------------------------------
    // PURPOSE: Determines if withdrawal warning should be shown or not
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountID
    //          
    // RETURNS: if a warning should be shown or not
    // LTC 2017-JUL-19
    public static bool ShowWithdrawalWarning(Int64 AccountId, Decimal Total_withdrawal = 0)
    {
      DateTime _date_last_refund;
      Decimal _total_bank_card_recharge_amount;
      Decimal _bank_recharge_pct;
      Decimal _bank_card_recharge_vs_cash_pct;
      List<Int32> _cash_in_movements_list;
      List<Int32> _card_recharge_movements_list;

      _cash_in_movements_list = new List<Int32>();
      _card_recharge_movements_list = new List<Int32>();

      // LTC 2017-JUL-11
      _card_recharge_movements_list.Add((int)CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1);
      _card_recharge_movements_list.Add((int)CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1);
      _card_recharge_movements_list.Add((int)CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1);

      _card_recharge_movements_list.Add((int)CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1);
      _card_recharge_movements_list.Add((int)CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1);
      _card_recharge_movements_list.Add((int)CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1);

      _bank_card_recharge_vs_cash_pct = GeneralParam.GetDecimal("Cashier.Withdrawal", "BankCardRechargeVSCash.Pct", 0);

      if (_bank_card_recharge_vs_cash_pct == 0)
      {
        return false;
      }

      if (GetLastWithdrawalDate(AccountId, out _date_last_refund))
      {
        _total_bank_card_recharge_amount = 0;

        _bank_recharge_pct = 0;

        _total_bank_card_recharge_amount = GetMovementLastAmount(AccountId, _card_recharge_movements_list, _date_last_refund);
        if (Total_withdrawal > 0)
        {
          _bank_recharge_pct = (_total_bank_card_recharge_amount / Total_withdrawal) * 100;
        }

        return (_bank_recharge_pct >= _bank_card_recharge_vs_cash_pct);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get validation number and sequence 
    //
    //  PARAMS:
    //      - INPUT:
    //          - Params 
    //          - Ticket
    //          - Sequence
    //          
    //      - OUTPUT:
    //          
    // RETURNS:
    private static Boolean CountR_GetValidationNumberAndSequence(ParamsTicketOperation Params, ref Ticket Ticket, out Int64 Sequence)
    {
      Int64 _sequence_aux;

      Sequence = 0;

      //YNM -> CountR generated number
      if (Params.validation_numbers == null || Params.validation_numbers.Count != 1)
      {
        return false;
      }

      Ticket.ValidationNumber = Params.validation_numbers[0].ValidationNumber;
      Sequence = Params.validation_numbers[0].SequenceId;



      return true;
    } // CountR_GetValidationNumberAndSequence
  }
}