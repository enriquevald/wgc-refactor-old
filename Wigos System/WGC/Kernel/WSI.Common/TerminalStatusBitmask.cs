//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : TerminalStatusBitmask.cs
// 
//   DESCRIPTION : Class to contain information regarding status of a terminal
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JAN-2014 ICS    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class TerminalStatusBitmask
  {
    #region Enums

    public enum WCP_TerminalEvent
    {
      BillIn = 0,
      TicketOut = 1,
      TicketIn = 2,
      ChangeStacker = 3,
    }

    public enum BITMASK_TYPE
    {
      None = 0,
      Door = 1,
      Bill = 2,
      Printer = 3,
      EGM = 4,
      Plays = 5,
      Jackpot = 6,
    }

    [Flags]
    public enum DOOR_FLAG
    {
      NONE = 0,                       // 000000
      BELLY_DOOR = 1,                 // 000001
      CARD_CAGE = 2,                  // 000010
      CASHBOX_DOOR = 4,               // 000100
      DROP_DOOR = 8,                  // 001000
      SLOT_DOOR = 16,                 // 010000
    }

    [Flags]
    public enum BILL_FLAG
    {
      NONE = 0,                       // 000000
      BILL_JAM = 1,                   // 000001
      HARD_FAILURE = 2,               // 000010
      COUNTERFEIT_EXCEEDED = 4,       // 000100
    }

    [Flags]
    public enum PRINTER_FLAG
    {
      NONE = 0,                       // 000000
      CARRIAGE_JAM = 1,               // 000001
      COMM_ERROR = 2,                 // 000010
      PAPER_LOW = 4,                  // 000100
      PAPER_OUT_ERROR = 8,            // 001000
      POWER_ERROR = 16,               // 010000
      REPLACE_RIBBON = 32,            // 100000
    }

    [Flags]
    public enum EGM_FLAG
    {
      NONE = 0,                       // 000000
      PART_EPROM_ERROR = 1,           // 000001
      CMOS_RAM_ERROR = 2,             // 000010
      EPROM_ERROR = 4,                // 000100
      LOW_BACKUP_BATTERY = 8,         // 001000
    }

    [Flags]
    public enum PLAYS_FLAG
    {
      NONE = 0,                       // 000000
      PLAYS_SECOND = 1,               // 000001
      CENTS_SECOND = 2,               // 000010
    }

    [Flags]
    public enum JACKPOT_FLAG
    {
      NONE = 0,                       // 000000
      JACKPOTS = 1,                   // 000001
    }

    #endregion

    #region Private Methods

    // PURPOSE : Set flag into a bitmask
    //
    //  PARAMS :
    //      - INPUT :
    //          - Current bitmask
    //          - Flag
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - New bitmask value
    //
    private static Int32 SetFlag(Int32 CurrentBitmask, Int32 Flag)
    {
      return (CurrentBitmask | Flag);
    }

    // PURPOSE : Unset flag into a bitmask
    //
    //  PARAMS :
    //      - INPUT :
    //          - Current bitmask
    //          - Flag
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - New bitmask value
    //
    private static Int32 UnsetFlag(Int32 CurrentBitmask, Int32 Flag)
    {
      return (CurrentBitmask & (~Flag));
    }

    // PURPOSE : Set flag value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - DB_column
    //          - TerminalId
    //          - Flag
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    private static Boolean DB_SetFlag(String DBColumn, Int32 TerminalId, Int32 Flag, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE   TERMINAL_STATUS ");
      _sb.AppendLine("    SET   " + DBColumn + " = " + DBColumn + " | @pFlag ");
      _sb.AppendLine("  WHERE   TS_TERMINAL_ID = @pTerminalId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
        _cmd.Parameters.Add("@pFlag", SqlDbType.Int).Value = Flag;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error("DB_GetBitmask: Error setting " + DBColumn + " status of Terminal: " + TerminalId);
          return false;
        }
      }
      return true;
    }

    // PURPOSE : Unset flag value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - DB_column
    //          - TerminalId
    //          - Flag
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    private static Boolean DB_UnsetFlag(String DBColumn, Int32 TerminalId, Int32 Flag, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE   TERMINAL_STATUS ");
      _sb.AppendLine("    SET   " + DBColumn + " = " + DBColumn + " & (~@pFlag) ");
      _sb.AppendLine("  WHERE   TS_TERMINAL_ID = @pTerminalId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
        _cmd.Parameters.Add("@pFlag", SqlDbType.Int).Value = Flag;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error("DB_GetBitmask: Error setting " + DBColumn + " status of Terminal: " + TerminalId);
          return false;
        }
      }
      return true;
    }

    // PURPOSE : Get a bitmask value from DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - DB_column
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could read the value successfully
    //      - False: Otherwise
    //
    private static Boolean DB_GetBitmask(String DBColumn, Int32 TerminalId, out Int32 Bitmask, SqlTransaction Trx)
    {
      {
        StringBuilder _sb;

        Bitmask = 0;

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   ISNULL(" + DBColumn + ", 0) ");
        _sb.AppendLine("   FROM   TERMINAL_STATUS ");
        _sb.AppendLine("  WHERE   TS_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Error("DB_GetBitmask: Error getting " + DBColumn + " status of Terminal: " + TerminalId);
              return false;
            }

            Bitmask = _reader.GetInt32(0);
          }
        }
        return true;
      }
    }

    // PURPOSE : Increase terminal stacker counter
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - ResetCounter: If it's set, the counter is reset to zero
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True: If it has been updated successfully.
    //          - False: Otherwise
    //
    private static Boolean IncreaseTerminalStacker(Int32 TerminalId, Boolean ResetCounter, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS (  SELECT   1  ");
        _sb.AppendLine("              FROM   TERMINAL_STATUS ");
        _sb.AppendLine("              WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("           )  ");
        _sb.AppendLine("      UPDATE  TERMINAL_STATUS  ");
        if (ResetCounter)
        {
          _sb.AppendLine("       SET  TS_STACKER_COUNTER = 0");
        }
        else
        {
          _sb.AppendLine("         SET  TS_STACKER_COUNTER = ISNULL(TS_STACKER_COUNTER, 0) + 1");
        }
        _sb.AppendLine("       WHERE  TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        ELSE  ");
        _sb.AppendLine(" INSERT INTO TERMINAL_STATUS ");
        _sb.AppendLine("            ( TS_TERMINAL_ID ");
        _sb.AppendLine("            , TS_STACKER_COUNTER ");
        _sb.AppendLine("            ) ");
        _sb.AppendLine("      VALUES ");
        _sb.AppendLine("            ( @pTerminalId ");
        if (ResetCounter)
        {
          _sb.AppendLine("          , 0");
        }
        else
        {
          _sb.AppendLine("          , 1");
        }
        _sb.AppendLine("            ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("UpdateTerminalStacker: Error updating Stacker counter of Terminal: " + TerminalId);
            return false;
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IncreaseTerminalStacker

    #endregion

    #region Public Methods

    // PURPOSE : Check if a flag is set in bitmask
    //
    //  PARAMS :
    //      - INPUT :
    //          - Current bitmask
    //          - Flag
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If the flag is activated
    //      - False: If the flag is not activated
    //
    public static Boolean IsFlagActived(Int32 CurrentBitmask, Int32 Flag)
    {
      return ((CurrentBitmask & Flag) == Flag);
    }

    // PURPOSE : Update a bitmask value
    //
    //  PARAMS :
    //      - INPUT :
    //          - Current bitmask
    //          - Flag
    //          - New flag status
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - New bitmask value
    //
    public static Int32 UpdateBitmask(Int32 CurrentBitmask, Int32 Flag, Boolean FlagStatus)
    {
      if (FlagStatus)
      {
        return SetFlag(CurrentBitmask, Flag);
      }
      else
      {
        return UnsetFlag(CurrentBitmask, Flag);
      }
    }

    // PURPOSE : Get a bitmask value from DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - Bitmask Type
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could read the value successfully
    //      - False: Otherwise
    //
    public static Boolean DB_GetBitmask(BITMASK_TYPE Type, Int32 TerminalId, out Int32 Bitmask, SqlTransaction Trx)
    {
      Bitmask = 0;

      switch(Type)
      {
        case BITMASK_TYPE.Door:
          return DB_GetBitmask("TS_DOOR_BITMASK", TerminalId, out Bitmask, Trx);          
        case BITMASK_TYPE.Bill:
          return DB_GetBitmask("TS_BILL_BITMASK", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.Printer:
          return DB_GetBitmask("TS_PRINTER_BITMASK", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.EGM:
          return DB_GetBitmask("TS_EGM_BITMASK", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.Plays:
          return DB_GetBitmask("TS_PLAYS_BITMASK", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.Jackpot:
          return DB_GetBitmask("TS_JACKPOT_BITMASK", TerminalId, out Bitmask, Trx);
        default:
          return false;
      }
    }

    // PURPOSE : Set flag value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - Bitmask Type
    //          - TerminalId
    //          - Flag
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    public static Boolean DB_SetFlag(BITMASK_TYPE Type, Int32 TerminalId, Int32 Flag, SqlTransaction Trx)
    {
      switch (Type)
      {
        case BITMASK_TYPE.Door:
          return DB_SetFlag("TS_DOOR_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Bill:
          return DB_SetFlag("TS_BILL_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Printer:
          return DB_SetFlag("TS_PRINTER_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.EGM:
          return DB_SetFlag("TS_EGM_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Plays:
          return DB_SetFlag("TS_PALYS_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Jackpot:
          return DB_SetFlag("TS_JACKPOT_BITMASK", TerminalId, Flag, Trx);
        default:
          return false;
      }
    }

    // PURPOSE : Unset flag value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - Bitmask Type
    //          - TerminalId
    //          - Flag
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    public static Boolean DB_UnsetFlag(BITMASK_TYPE Type, Int32 TerminalId, Int32 Flag, SqlTransaction Trx)
    {
      switch (Type)
      {
        case BITMASK_TYPE.Door:
          return DB_UnsetFlag("TS_DOOR_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Bill:
          return DB_UnsetFlag("TS_BILL_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Printer:
          return DB_UnsetFlag("TS_PRINTER_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.EGM:
          return DB_UnsetFlag("TS_EGM_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Plays:
          return DB_UnsetFlag("TS_PALYS_BITMASK", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Jackpot:
          return DB_UnsetFlag("TS_JACKPOT_BITMASK", TerminalId, Flag, Trx);
        default:
          return false;
      }
    }

    // PURPOSE : Sets terminal status values in terminal_status table
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - TerminalEvent
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True: If it has been updated successfully.
    //          - False: Otherwise
    //
    public static Boolean SetTerminalStatus(Int32 TerminalId, WCP_TerminalEvent Event, SqlTransaction Trx)
    {
      String _column;
      Int32 _flag;

      _column = String.Empty;
      _flag = 0;

      switch (Event)
      {
        case WCP_TerminalEvent.BillIn:
          _flag += (Int32)TerminalStatusBitmask.BILL_FLAG.HARD_FAILURE;
          _flag += (Int32)TerminalStatusBitmask.BILL_FLAG.BILL_JAM;
          if (!DB_UnsetFlag(BITMASK_TYPE.Bill, TerminalId, _flag, Trx))
          {
            Log.Error("SetTerminalStatus: Error unsetting bitmask for WCP_TerminalEvent: " + Event);
            return false;
          }
          if (!IncreaseTerminalStacker(TerminalId, false, Trx))
          {
            return false;
          }
          break;

        case WCP_TerminalEvent.TicketOut:
          _flag += (Int32)TerminalStatusBitmask.PRINTER_FLAG.COMM_ERROR;
          _flag += (Int32)TerminalStatusBitmask.PRINTER_FLAG.CARRIAGE_JAM;
          _flag += (Int32)TerminalStatusBitmask.PRINTER_FLAG.PAPER_OUT_ERROR;
          _flag += (Int32)TerminalStatusBitmask.PRINTER_FLAG.REPLACE_RIBBON;
          if (!DB_UnsetFlag(BITMASK_TYPE.Printer, TerminalId, _flag, Trx))
          {
            Log.Error("SetTerminalStatus: Error unsetting bitmask for WCP_TerminalEvent: " + Event);
            return false;
          }
          break;

        case WCP_TerminalEvent.TicketIn:
          return IncreaseTerminalStacker(TerminalId, false, Trx);

        case WCP_TerminalEvent.ChangeStacker:
          return IncreaseTerminalStacker(TerminalId, true, Trx);

        default:
          Log.Error("SetTerminalStatus: Unknown WCP_TerminalEvent: " + Event);
          return false;

      } // switch

      return true;
    } // SetTerminalStatus

    // PURPOSE : Set Terminal Status.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataRow Row
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True: If the status has been updated successfully.
    //          - False: Otherwise.
    //
    public static Boolean SetTerminalStatus(Int32 TerminalId, UInt16 SasEventCode, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Boolean _value;
      String _db_column;

      _db_column = String.Empty;
      _value = false;

      switch (SasEventCode)
      {
        // DOOR EVENTS
        case 0x0011:
        case 0x0013:
        case 0x0015:
        case 0x0019:
        case 0x001D:
        case 0x0012:
        case 0x0014:
        case 0x0016:
        case 0x001A:
        case 0x001E:
          switch (SasEventCode)
          {
            case 0x0011: // Slot door was opened
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.SLOT_DOOR, Trx);
            case 0x0013: // Drop door was opened
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.DROP_DOOR, Trx);
            case 0x0015: // Card cage was opened
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.CARD_CAGE, Trx);
            case 0x0019: // Cashbox door was opened
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.CASHBOX_DOOR, Trx);
            case 0x001D: // Belly door was opened
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.BELLY_DOOR, Trx);
            case 0x0012: // Slot door was closed        
              return TerminalStatusBitmask.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.SLOT_DOOR, Trx);
            case 0x0014: // Drop door was closed
              return TerminalStatusBitmask.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.DROP_DOOR, Trx);
            case 0x0016: // Card cage was closed
              return TerminalStatusBitmask.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.CARD_CAGE, Trx);
            case 0x001A: // Cashbox door was closed
              return TerminalStatusBitmask.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.CASHBOX_DOOR, Trx);
            case 0x001E: // Belly door was closed
              return TerminalStatusBitmask.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusBitmask.DOOR_FLAG.BELLY_DOOR, Trx);
          } // End door switch
          break;

        // BILL EVENTS
        case 0x0028:
        case 0x0029:
          if (SasEventCode == 0x0028) // Bill jam
          {
            return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Bill, TerminalId, (Int32)TerminalStatusBitmask.BILL_FLAG.BILL_JAM, Trx);
          }

          if (SasEventCode == 0x0029) // Bill acceptor hardware failure
          {
            return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Bill, TerminalId, (Int32)TerminalStatusBitmask.BILL_FLAG.HARD_FAILURE, Trx);
          }
          break;

        // PRINTER EVENTS
        case 0x0060:
        case 0x0061:
        case 0x0075:
        case 0x0078:
        case 0x0074:
          switch (SasEventCode)
          {
            case 0x0060: // Printer communication error
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusBitmask.PRINTER_FLAG.COMM_ERROR, Trx);
            case 0x0061: // Printer paper out error
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusBitmask.PRINTER_FLAG.PAPER_OUT_ERROR, Trx);
            case 0x0075: // Printer power off
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusBitmask.PRINTER_FLAG.POWER_ERROR, Trx);
            case 0x0078: // Printer carriage jammed
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusBitmask.PRINTER_FLAG.CARRIAGE_JAM, Trx);
            case 0x0076: // Printer power on
              return TerminalStatusBitmask.DB_UnsetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusBitmask.PRINTER_FLAG.POWER_ERROR, Trx);
            case 0x0074: // Printer paper low
              return TerminalStatusBitmask.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusBitmask.PRINTER_FLAG.PAPER_LOW, Trx);
          } // End printer switch
          break;

        case 0x0101: // SAS disconnected
          _value = true;
          _db_column = "TS_SAS_HOST_ERROR";
          break;
        case 0x0102: // SAS connected
          _value = false;
          _db_column = "TS_SAS_HOST_ERROR";
          break;
        default:
          Log.Error("SetTerminalStatus: Unknown SasEventCode: " + SasEventCode);
          return false;
      } // end switch

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS (  SELECT   1  ");
        _sb.AppendLine("              FROM   TERMINAL_STATUS ");
        _sb.AppendLine("              WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("           )  ");
        _sb.AppendLine("      UPDATE  TERMINAL_STATUS  ");
        _sb.AppendLine("         SET  " + _db_column + "  = @pValue ");
        _sb.AppendLine("       WHERE  TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        ELSE  ");
        _sb.AppendLine(" INSERT INTO TERMINAL_STATUS ");
        _sb.AppendLine("            ( TS_TERMINAL_ID ");
        _sb.AppendLine("            , " + _db_column);
        _sb.AppendLine("            ) ");
        _sb.AppendLine("      VALUES ");
        _sb.AppendLine("            ( @pTerminalId ");
        _sb.AppendLine("            , @pValue ");
        _sb.AppendLine("            ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pValue", SqlDbType.Bit).Value = _value;
          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("SetTerminalStatus: Error setting " + _db_column + " = " + _value.ToString() + " to Terminal: " + TerminalId);
            return false;
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SetTerminalStatus

    #endregion
  }
}
