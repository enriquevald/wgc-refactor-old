//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IMetersGroup.cs 
// 
//   DESCRIPTION: IMetersGroup class
//  
//        AUTHOR: David Hern�ndez & Jos� Martinez
// 
// CREATION DATE: 08-APR-2015
// 
// REVISION HISTORY:
// 
// Date        Author      Description
// ----------- ----------- -----------------------------------------------------
// 08-APR-2015 DHA & JML   First version.
// 04-MAY-2015 DHA & JML   Fase 3.
// 29-OCT-2017 LTC         [WIGOS-6134] Multisite Terminal meter adjustment
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public interface ITerminalMeterGroup
  {
    DataTable MetersData
    {
      get;
      set;
    }
    DataTable HistoryMetersData
    {
      get;
      set;
    }
    List<Meter> Meters
    {
      get;
    }

    Boolean ReadMeters(Int64[] Terminals, DateTime Workday);
    TerminalMeterGroup.ResultCode UpdateMeters();

    MeterRelationsBD MeterRelationDB
    {
      get;
    }

    Boolean Recalculation();

    // LTC  29-OCT-2017
    String SiteId { get; set; }

  } // ITerminalMeterGroup

} // WSI.Common 


