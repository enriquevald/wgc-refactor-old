﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashierSessionTransfer.cs
// 
//   DESCRIPTION: It has classes to insert, update or get records (cashier_session_transfer table) about
//                the transfer between two cashier sessions
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 17-AUG-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-AUG-2015 FAV    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{

  /// <summary>
  /// Entity class: CashierSessionTransferInfo
  /// </summary>
  public class CashierSessionTransferInfo : ICloneable
  {
    public Int64 CashierSessionTransferId { get; set; }
    public Int64 SessionSourceId { get; set; }
    public Int64 SessionTargeId { get; set; }
    public CashierSessionTransferStatus Status { get; set; }
    public Int64 OperationSourceId { get; set; }
    public Int64? OperationTargetId { get; set; }
    public DateTime DateTime { get; set; }
    public String DenominationDetailXML { get; set; }
    public SortedDictionary<CurrencyIsoType, Decimal> DenominationDetail { get; set; }

    public object Clone()
    {
      return this.MemberwiseClone();
    }
  }

  /// <summary>
  /// Business class: CashierSessionTransfer
  /// </summary>
  internal static class CashierSessionTransfer
  {

    /// <summary>
    /// Insert CashierSessionTransferInfo into DB (table cashier_session_transfer)
    /// </summary>
    /// <param name="CashierSessionTransferInfo"></param>
    /// <param name="CashierSessionTransferId"></param>
    /// <param name="Trx"></param>
    /// <returns>true all works / false error</returns>
    public static Boolean InsertTransfer(CashierSessionTransferInfo CashierSessionTransferInfo, out Int64 CashierSessionTransferId, SqlTransaction Trx)
    {
      CashierSessionTransferDAO _dao;

      CashierSessionTransferId = 0;

      if (CashierSessionTransferInfo == null || CashierSessionTransferInfo.CashierSessionTransferId > 0)
      {
        Log.Error("The CashierSessionTransferInfo parameter is not valid");

        return false;
      }

      _dao = new CashierSessionTransferDAO();

      return _dao.DB_InsertCashierSessionTransfer(CashierSessionTransferInfo, out CashierSessionTransferId, Trx);
    }

    /// <summary>
    /// Update CashierSessionTransferInfo into DB (table cashier_session_transfer)
    /// </summary>
    /// <param name="CashierSessionTransferInfo"></param>
    /// <param name="Trx">true all works / false error</param>
    /// <returns></returns>
    public static Boolean UpdateTransfer(CashierSessionTransferInfo CashierSessionTransferInfo, SqlTransaction Trx)
    {
      CashierSessionTransferDAO _dao;

      if (CashierSessionTransferInfo == null || CashierSessionTransferInfo.CashierSessionTransferId <= 0)
      {
        Log.Error("The CashierSessionTransferInfo parameter is not valid");

        return false;
      }

      _dao = new CashierSessionTransferDAO();

      return _dao.DB_UpdateCashierSessionTransfer(CashierSessionTransferInfo, Trx);
    }

    /// <summary>
    /// Get CashierSessionTransferInfo list with status='sent' that was sent to a Target session, from DB (table cashier_session_transfer) 
    /// </summary>
    /// <param name="TargetSession"></param>
    /// <param name="Trx"></param>
    /// <returns>It is not null if all works / NULL if there is an error</returns>
    public static List<CashierSessionTransferInfo> GetTransferSentListToTargetSession(CashierSessionInfo TargetSession, SqlTransaction Trx)
    { 
      List<CashierSessionTransferInfo> _list = null;
      
      CashierSessionTransferDAO _dao = new CashierSessionTransferDAO();
      _dao.DB_GetCashierSessionTransferListByTargetAndStatus(TargetSession, CashierSessionTransferStatus.Sent,
                                                          out _list, Trx);

      return _list;
    }

    /// <summary>
    /// Get CashierSessionTransferInfo object by ID from DB (table cashier_session_transfer) 
    /// </summary>
    /// <param name="CashierSessionTransferId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static CashierSessionTransferInfo GetTransferById(Int64 CashierSessionTransferId, SqlTransaction Trx)
    {
      CashierSessionTransferInfo _transfer = null;

      CashierSessionTransferDAO _dao = new CashierSessionTransferDAO();
      _dao.DB_GetCashierSessionTransferById(CashierSessionTransferId, out _transfer, Trx);
      
      return _transfer;
    }

  }

  /// <summary>
  /// Internal data access class: CashierSessionTransferDAO
  /// </summary>
  internal class CashierSessionTransferDAO
  {

    #region Attributes
    
    private const int CONNECTION_EXCEPTION_TIME_OUT = -2;
    
    #endregion

    #region Public Methods

    public Boolean DB_InsertCashierSessionTransfer(CashierSessionTransferInfo CashierSessionTransferInfo, out Int64 CashierSessionTransferId, SqlTransaction Trx)
    {
      Int32 _rows_affected;
      StringBuilder _sb;
      SqlParameter _output_param;

      CashierSessionTransferId = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO CASHIER_SESSION_TRANSFER (CST_SESSION_SOURCE_ID ");
        _sb.AppendLine("                          , CST_SESSION_TARGET_ID            ");
        _sb.AppendLine("                          , CST_STATUS                       ");
        _sb.AppendLine("                          , CST_OPERATION_SOURCE_ID          ");
        _sb.AppendLine("                          , CST_OPERATION_TARGET_ID          ");
        _sb.AppendLine("                          , CST_DATE                         ");
        _sb.AppendLine("                          , CST_DENOMINATION_DETAIL          ");
        _sb.AppendLine("                          )                                  ");
        _sb.AppendLine("                    VALUES (@pSessionSourceID                ");
        _sb.AppendLine("                          , @pSessionTargetID                ");
        _sb.AppendLine("                          , @pStatus                         ");
        _sb.AppendLine("                          , @pOperationSourceID              ");
        _sb.AppendLine("                          , @pOperationTargetID              ");
        _sb.AppendLine("                          , @pDate                           ");
        _sb.AppendLine("                          , @pDenominationDetail             ");
        _sb.AppendLine("                          )                                  ");
        _sb.AppendLine(" SET @pTransferId = SCOPE_IDENTITY()                         ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionSourceID", SqlDbType.BigInt).Value = CashierSessionTransferInfo.SessionSourceId;
          _cmd.Parameters.Add("@pSessionTargetID", SqlDbType.BigInt).Value = CashierSessionTransferInfo.SessionTargeId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)CashierSessionTransferInfo.Status;
          _cmd.Parameters.Add("@pOperationSourceID", SqlDbType.BigInt).Value = CashierSessionTransferInfo.OperationSourceId;
          _cmd.Parameters.Add("@pOperationTargetID", SqlDbType.BigInt).Value =  (object) CashierSessionTransferInfo.OperationTargetId ?? DBNull.Value;
          _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = CashierSessionTransferInfo.DateTime;
          _cmd.Parameters.Add("@pDenominationDetail", SqlDbType.Xml).Value = CashierSessionTransferInfo.DenominationDetailXML;

          _output_param = new SqlParameter("@pTransferId", SqlDbType.BigInt);
          _output_param.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_output_param);

          _rows_affected = _cmd.ExecuteNonQuery();

          if (_rows_affected == 1)
          {
            CashierSessionTransferId = (Int64)_output_param.Value;

            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public Boolean DB_UpdateCashierSessionTransfer(CashierSessionTransferInfo CashierSessionTransferInfo, SqlTransaction Trx)
    {
      Int32 _rows_affected;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   CASHIER_SESSION_TRANSFER");
        _sb.AppendLine("   SET   CST_SESSION_SOURCE_ID = @pSessionSourceID");
        _sb.AppendLine("       , CST_SESSION_TARGET_ID = @pSessionTargetID");
        _sb.AppendLine("       , CST_STATUS = @pStatus");
        _sb.AppendLine("       , CST_OPERATION_SOURCE_ID = @pOperationSourceID");
        _sb.AppendLine("       , CST_OPERATION_TARGET_ID = @pOperationTargetID");
        _sb.AppendLine("       , CST_DATE = @pDate");
        _sb.AppendLine("       , CST_DENOMINATION_DETAIL = @pDenominationDetail");
        _sb.AppendLine(" WHERE   CST_TRANSFER_ID = @pTransferId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionSourceID", SqlDbType.BigInt).Value = CashierSessionTransferInfo.SessionSourceId;
          _cmd.Parameters.Add("@pSessionTargetID", SqlDbType.BigInt).Value = CashierSessionTransferInfo.SessionTargeId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)CashierSessionTransferInfo.Status;
          _cmd.Parameters.Add("@pOperationSourceID", SqlDbType.BigInt).Value = CashierSessionTransferInfo.OperationSourceId;
          _cmd.Parameters.Add("@pOperationTargetID", SqlDbType.BigInt).Value = CashierSessionTransferInfo.OperationTargetId;
          _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = CashierSessionTransferInfo.DateTime;
          _cmd.Parameters.Add("@pDenominationDetail", SqlDbType.Xml).Value = CashierSessionTransferInfo.DenominationDetailXML;
          _cmd.Parameters.Add("@pTransferId", SqlDbType.BigInt).Value = CashierSessionTransferInfo.CashierSessionTransferId;

          _rows_affected = _cmd.ExecuteNonQuery();

          return (_rows_affected == 1);
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public Boolean DB_GetCashierSessionTransferById(Int64 CashierSessionTransferId, out CashierSessionTransferInfo CashierSessionTransferInfo, SqlTransaction Trx)
    {
      StringBuilder _sb;
      CashierSessionTransferInfo = null;
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   CST_SESSION_SOURCE_ID");
        _sb.AppendLine("       , CST_SESSION_TARGET_ID");
        _sb.AppendLine("       , CST_STATUS");
        _sb.AppendLine("       , CST_OPERATION_SOURCE_ID");
        _sb.AppendLine("       , CST_OPERATION_TARGET_ID");
        _sb.AppendLine("       , CST_DATE");
        _sb.AppendLine("       , CST_DENOMINATION_DETAIL");
        _sb.AppendLine("  FROM   CASHIER_SESSION_TRANSFER");
        _sb.AppendLine(" WHERE   CST_TRANSFER_ID = @pTransferId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTransferId", SqlDbType.BigInt).Value = CashierSessionTransferId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              CashierSessionTransferInfo = new CashierSessionTransferInfo()
              {
                SessionSourceId = (Int64)_reader[0],
                SessionTargeId = (Int64)_reader[1],
                Status = (CashierSessionTransferStatus)_reader[2],
                OperationSourceId = (Int64)_reader[3],
                OperationTargetId = _reader.IsDBNull(4) ? (long?)null : (Int64)_reader[4],
                DateTime = _reader.GetDateTime(5),
                DenominationDetailXML = (String)_reader[6],
                DenominationDetail = XMLDictionary.XMLToSortedDictionary((String)_reader[6]),
                CashierSessionTransferId = CashierSessionTransferId,
              };

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public Boolean DB_GetCashierSessionTransferListByTargetAndStatus(CashierSessionInfo CashierSession, CashierSessionTransferStatus Status,
                   out List<CashierSessionTransferInfo> CashierSessionTransferList, SqlTransaction Trx)
    {
      StringBuilder _sb;
      CashierSessionTransferList = null;
      List<CashierSessionTransferInfo> _list = new List<CashierSessionTransferInfo>();
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   CST_TRANSFER_ID");
        _sb.AppendLine("       , CST_SESSION_SOURCE_ID");
        _sb.AppendLine("       , CST_SESSION_TARGET_ID");
        _sb.AppendLine("       , CST_STATUS");
        _sb.AppendLine("       , CST_OPERATION_SOURCE_ID");
        _sb.AppendLine("       , CST_OPERATION_TARGET_ID");
        _sb.AppendLine("       , CST_DATE");
        _sb.AppendLine("       , CST_DENOMINATION_DETAIL");
        _sb.AppendLine("  FROM   CASHIER_SESSION_TRANSFER");
        _sb.AppendLine(" WHERE   CST_SESSION_TARGET_ID = @pSessionTargetId");
        _sb.AppendLine("   AND   CST_STATUS = @pStatus");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionTargetId", SqlDbType.BigInt).Value = CashierSession.CashierSessionId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)Status;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              CashierSessionTransferInfo _transfer_cashier_info = new CashierSessionTransferInfo()
              {
                CashierSessionTransferId = (Int64)_reader[0],
                SessionSourceId = (Int64)_reader[1],
                SessionTargeId = (Int64)_reader[2],
                Status = (CashierSessionTransferStatus)_reader[3],
                OperationSourceId = (Int64)_reader[4],
                OperationTargetId = (_reader.IsDBNull(5) ? (long?)null : (Int64)_reader[5]),
                DateTime = _reader.GetDateTime(6),
                DenominationDetailXML = (String)_reader[7],
                DenominationDetail = XMLDictionary.XMLToSortedDictionary((String)_reader[7])
              };

              _list.Add(_transfer_cashier_info);
            }

            CashierSessionTransferList = _list;

            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public Boolean DB_SetCashierSessionTransferStatus(Int64 CashierSessionTransferId, CashierSessionTransferStatus Status, SqlTransaction Trx)
    {
      Int32 _rows_affected;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    UPDATE   CASHIER_SESSION_TRANSFER");
        _sb.AppendLine("       SET   CST_STATUS = @Status");
        _sb.AppendLine("     WHERE   CST_TRANSFER_ID = @pTransferId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@Status", SqlDbType.Int).Value = (int)Status;
          _cmd.Parameters.Add("@pTransferId", SqlDbType.BigInt).Value = CashierSessionTransferId;

          _rows_affected = _cmd.ExecuteNonQuery();

          return (_rows_affected == 1);
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion

  }

}
