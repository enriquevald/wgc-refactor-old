﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WSI.Common.Utilities.Extensions
{
  public static class DataTableExtensions
  {
    /// <summary>
    /// Delete rows by filter
    /// </summary>
    /// <param name="table"></param>
    /// <param name="filter"></param>
    /// <returns></returns>
    public static DataTable Delete(this DataTable table, string filter)
    {
      table.Select(filter).Delete();
      return table;
    }
    public static void Delete(this IEnumerable<DataRow> rows)
    {
      foreach (var row in rows)
        row.Delete();
    }
  }
}
