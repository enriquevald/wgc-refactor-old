﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.Utilities.Extensions
{
  public static class StringExtensions
  {
    public static bool IsNullOrWhiteSpace(this string value)
    {
      if (value == null) 
        return true;

      return string.IsNullOrEmpty(value.Trim());
    }

    public static bool IsNumber(this object value)
    {
      return value is sbyte
              || value is byte
              || value is short
              || value is ushort
              || value is int
              || value is uint
              || value is long
              || value is ulong
              || value is float
              || value is double
              || value is decimal;
    }
  }
}
