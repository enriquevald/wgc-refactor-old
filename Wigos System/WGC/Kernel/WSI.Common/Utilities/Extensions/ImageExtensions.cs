﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : ImageExtensions.cs
// 
//   DESCRIPTION : Extensions Methods for the Image class
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-ENE-2016 AVZ    First release. Product Backlog Item 6648:Visitas / Recepción: Pantalla de introducción de datos del Customer / Cliente - Task 7096: Integración escaneo de documentos
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace WSI.Common.Utilities.Extensions
{
  public static class ImageExtensions
  {
	  /// <summary>
	  /// Convert to byte array
	  /// </summary>
	  /// <param name="Image"></param>
	  /// <returns></returns>
	  public static byte[] ToByteArray(this Image Image)
	  {
		  ImageConverter _image_converter = new ImageConverter();

		  var img = Image.Clone();
		  try
		  {
			  return (byte[]) _image_converter.ConvertTo(img, typeof (byte[]));
		  }
		  catch
		  {
				return new byte[]{};
		  }
	  }
  }
}
