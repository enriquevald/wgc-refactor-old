﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : HttpWebResponseExtensions.cs
// 
//   DESCRIPTION : Extensions Methods for the HttpWebResponse class
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace WSI.Common.Utilities.Extensions
{
  public static class HttpWebResponseExtensions
  {
    /// <summary>
    /// Read response data to XML document
    /// </summary>
    /// <param name="Response"></param>
    /// <returns></returns>
    public static XmlDocument Read(this HttpWebResponse Response)
    {
      XmlDocument _response_ml = new XmlDocument();
      using (var _body_stream = Response.GetResponseStream())
      using (var _stream_reader = new StreamReader(_body_stream, Encoding.UTF8))
      {
        _response_ml.LoadXml(_stream_reader.ReadToEnd());
      }

      return _response_ml;
    }
  }
}