﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : EnumExtensions.cs
// 
//   DESCRIPTION : Extensions Methods for the Enum class
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
// 11-FEB-2015 AVZ    Product Backlog Item 9046:Visitas / Recepción: BlackLists
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using WSI.Common.Utilities.Attributes.Enums;

namespace WSI.Common.Utilities.Extensions
{
  public static class EnumExtensions
  {
    /// <summary>
    /// Convert to string
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string ToValueString(this Enum value)
    {
      return value.ToString("D");
    }

    /// <summary>
    /// Get description by resource key
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string GetDescriptionResource(this Enum value)
    {
      Attribute _attribute = null;
      try
      {
        _attribute = Attribute.GetCustomAttribute(GetMemberInfo(value), typeof (DescriptionResourceAttribute));
      }
      catch
      {
        _attribute = null;
      }
      if (_attribute == null)
          return value.ToValueString();
        else
          return ((DescriptionResourceAttribute) _attribute).Description;
    }

    private static MemberInfo GetMemberInfo(Enum value)
    {
      return value.GetType().GetField(Enum.GetName(value.GetType(), value));
    }
  }
}
