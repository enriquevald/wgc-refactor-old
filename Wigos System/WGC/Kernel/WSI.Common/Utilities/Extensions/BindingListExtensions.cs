﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace WSI.Common.Utilities.Extensions
{
  public static class BindingListExtensions
  {
    /// <summary>
    /// Find an item by a property name and a value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="List"></param>
    /// <param name="PropertyName"></param>
    /// <param name="ValueToFind"></param>
    /// <returns></returns>
    public static T Find<T>(this BindingList<T> List, string PropertyName, object ValueToFind)
    {
      if(ValueToFind == null)
        return default(T);

      Type _type = typeof(T);

      PropertyDescriptorCollection _properties = TypeDescriptor.GetProperties(_type);
      PropertyDescriptor _property = _properties.Find(PropertyName, true);

      if (_property == null)
        return default(T);

      PropertyInfo _prop_info = _type.GetProperty(_property.Name);
      
      T item;
      
      for(int i = 0; i < List.Count; i++)
      {
        item = (T)List[i];
        if (_prop_info.GetValue(item, null).Equals(ValueToFind))
          return item;
      }

      return default(T);
    }
  }
}
