﻿//-----------------------------------------------------------------
// Copyright © 2010 Win Systems Ltd.
//-------------------------------------------------------------------

// MODULE NAME:   Mixrosoft Office Information
// DESCRIPTION:   Office Info
// AUTHOR:        Luis Tenorio LTC
// CREATION DATE: 20-JUN-2016

// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 20-JUN-2012  LTC    Initial version
// 20-JUN-2016  LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence.
//....................................................................

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop;
using Microsoft.Win32;

namespace WSI.Common.Utilities.Extensions
{
  public static class OfficeInfo
  {
    public static string GetOfficeExcelVersion()
    {
      try
      {
        string sVersion = string.Empty;
        Microsoft.Office.Interop.Excel.Application appVersion = new Microsoft.Office.Interop.Excel.Application();
        appVersion.Visible = false;
        switch (appVersion.Version.ToString())
        {
          case "7.0":
            sVersion = "95";
            break;
          case "8.0":
            sVersion = "97";
            break;
          case "9.0":
            sVersion = "2000";
            break;
          case "10.0":
            sVersion = "2002";
            break;
          case "11.0":
            sVersion = "2003";
            break;
          case "12.0":
            sVersion = "2007";
            break;
          case "14.0":
            sVersion = "2010";
            break;
          case "15.0":
            sVersion = "2013";
            break;
          default:
            sVersion = "Too Old!";
            break;
        }

        return sVersion;
      }
      catch (Exception)
      {
        return "0";
      }
    }

    public enum MSOfficeApplications
    {
      Access,
      Excel,
      Word
    }

    public static bool IsInstalled(MSOfficeApplications app)
    {
      var keyName = String.Empty;
      switch (app)
      {
        case MSOfficeApplications.Access:
          keyName = "Access.Application";
          break;
        case MSOfficeApplications.Excel:
          keyName = "Excel.Application";
          break;
        case MSOfficeApplications.Word:
          keyName = "Word.Application";
          break;
        default:
          keyName = "";
          break;
      }

      RegistryKey key = Registry.ClassesRoot;
      RegistryKey subKey = key.OpenSubKey(keyName);
      return !(subKey == null);

    }

  }
}
