﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace WSI.Common.Utilities.Extensions
{
  public static class ByteArrayExtensions
  {
    /// <summary>
    /// Convert to image
    /// </summary>
    /// <param name="Array"></param>
    /// <returns></returns>
    public static Image ToImage(this byte[] Array)
    {
      return (Bitmap)((new ImageConverter()).ConvertFrom(Array));
    }
  }
}
