﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : WebRequestHelper.cs
// 
//   DESCRIPTION : Helper to manage a request to an Uri
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace WSI.Common.Utilities
{
  public static class WebRequestHelper
  {
    /// <summary>
    /// Create web request POST
    /// </summary>
    /// <param name="UriAddress"></param>
    /// <param name="Document"></param>
    /// <returns></returns>
    public static WebRequest CreateWebRequestPOST(string UriAddress, XmlDocument Document)
    {
      var web_request = WebRequest.Create(UriAddress);
      web_request.Method = "POST";
      web_request.ContentType = "text/xml; charset=UTF-8";
      web_request.Credentials = CredentialCache.DefaultCredentials;
      web_request.Proxy = null;
      SetRequestBody(web_request, Document.InnerXml);

      return web_request;
    }

    private static void SetRequestBody(WebRequest WebRequest, string Body)
    {
      byte[] _buffer;

      _buffer = Encoding.UTF8.GetBytes(Body);
      WebRequest.ContentLength = _buffer.Length;
      using (Stream _request_stream = WebRequest.GetRequestStream())
      {
        _request_stream.Write(_buffer, 0, _buffer.Length);
      }
    }
  }
}
