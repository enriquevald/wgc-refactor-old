﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : DescriptionResourceAttribute.cs
// 
//   DESCRIPTION : Class Attribute to use localization in enums
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-FEB-2015 AVZ    Product Backlog Item 9046:Visitas / Recepción: BlackLists
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.Utilities.Attributes.Enums
{ 
  [AttributeUsage(AttributeTargets.Field)]
  public class DescriptionResourceAttribute : Attribute
  {
    private string m_key;

    public DescriptionResourceAttribute(string key)
    {
      m_key = key;
    }

    public string Description
    {
      get
      {
        return Resource.String(m_key);
      }
    }
  }
}
