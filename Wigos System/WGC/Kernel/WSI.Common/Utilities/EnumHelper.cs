﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : EnumHelper.cs
// 
//   DESCRIPTION : Helper to enums
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-FEB-2015 AVZ    Product Backlog Item 9046:Visitas / Recepción: BlackLists
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using WSI.Common.Utilities.Extensions;

namespace WSI.Common.Utilities
{
  public static class EnumHelper
  {
    public static IList ToList<T>() where T : IConvertible
    {
      var list = new List<KeyValuePair<Enum,string>>();
      Array _enum_values = Enum.GetValues(typeof(T));

      foreach (Enum _value in _enum_values)
        list.Add(new KeyValuePair<Enum, string>(_value, _value.GetDescriptionResource()));
        
      return list;
    }
    public static Dictionary<string, Enum> ToDictionary<T>() where T : IConvertible
    {
      var list = new Dictionary<string,Enum>();
      Array _enum_values = Enum.GetValues(typeof(T));

      foreach (Enum _value in _enum_values)
        list.Add( _value.GetDescriptionResource().ToUpperInvariant(),_value);

      return list;
    }

    public static Dictionary<Enum,string> ToDictionary2<T>() where T : IConvertible
    {
      var list = new Dictionary<Enum,string>();
      Array _enum_values = Enum.GetValues(typeof(T));

      foreach (Enum _value in _enum_values)
        list.Add(_value, _value.GetDescriptionResource());

      return list;
    }
  }
}
