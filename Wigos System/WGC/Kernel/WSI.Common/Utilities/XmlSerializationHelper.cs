﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : XmlSerializationHelper.cs
// 
//   DESCRIPTION : Helper used to Serialize objects to xml and Deserialize xml to objects
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
// 29-NOV-2016 ETP    Fixed Bug 21111: Error in fill gaming tables.
// 12-AUG-2016 RGR    Add  CleanSerialize<T>(T obj) remove xml tag
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace WSI.Common.Utilities
{
  public class XmlSerializationHelper
  {
    /// <summary>
    /// Serialize an object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Obj"></param>
    /// <returns></returns>
    public static string Serialize<T>(T Obj)
    {
      XmlSerializer _serializer = new XmlSerializer(typeof(T));
      XmlSerializerNamespaces _name_spaces = new XmlSerializerNamespaces();

      using (StringWriter writer = new Utf8StringWriter())
      {
        _name_spaces.Add("", "");
        _serializer.Serialize(writer, Obj, _name_spaces);

        return writer.ToString();
      }
    }

    /// <summary>
    /// Clean Serialize an object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Obj"></param>
    /// <returns></returns>
    internal static string CleanSerialize<T>(T obj)
    {
      var _emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
      var _serializer = new XmlSerializer(obj.GetType());
      var _settings = new XmlWriterSettings();
      _settings.Indent = true;
      _settings.OmitXmlDeclaration = true;

      using (var stream = new StringWriter())
      using (var writer = XmlWriter.Create(stream, _settings))
      {
        _serializer.Serialize(writer, obj, _emptyNamepsaces);
        return stream.ToString();
      }
    }

    /// <summary>
    /// Deserialize xml to object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Xml"></param>
    /// <returns></returns>
    public static T Deserialize<T>(string Xml)
    {
      XmlSerializer _serializer = new XmlSerializer(typeof(T));
      using (MemoryStream _mem_stream = new MemoryStream(Encoding.UTF8.GetBytes(Xml)))
      {
        return (T)_serializer.Deserialize(_mem_stream);
      }
    }

  
  }
}
