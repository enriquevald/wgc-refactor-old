﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Utf8StringWriter.cs
// 
//   DESCRIPTION : Utf8StringWriter
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WSI.Common.Utilities
{
  public class Utf8StringWriter : StringWriter
  {
    public override Encoding Encoding
    {
      get { return Encoding.UTF8; }
    }
  } // Utf8StringWriter
}
