﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : ZImage.cs
// 
//   DESCRIPTION : Class to concatenate two images
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2017 RGR    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace WSI.Common.Utilities
{
  public class ZImage
  {
    #region Const
    private static int ZHeight = 20;
    #endregion

    #region Public Methods
    public static void Concat(string fileImageOne, string fileImageTwo)
    {
      Image _imageOne;
      Image _imageTwo;
      Image _img;
      Graphics _g;

      int _width;
      int _height;

      using (_imageOne = Image.FromStream(new MemoryStream(File.ReadAllBytes(fileImageOne))))
      {
        using (_imageTwo = Image.FromStream(new MemoryStream(File.ReadAllBytes(fileImageTwo))))
        {
          _width = _imageOne.Width >= _imageTwo.Width ? _imageOne.Width : _imageTwo.Width;
          _height = _imageOne.Height + _imageTwo.Height + ZHeight;

          using (_img = new Bitmap(_width, _height, PixelFormat.Format32bppArgb))
          {
            using (_g = Graphics.FromImage(_img))
            {
              _g.Clear(SystemColors.AppWorkspace);

              _g.DrawImage(_imageOne, new Rectangle(new Point(), _imageOne.Size),
             new Rectangle(new Point(), _imageOne.Size), GraphicsUnit.Pixel);
              _g.DrawImage(_imageTwo, new Rectangle(new Point(0, _imageOne.Height + ZHeight), _imageTwo.Size),
                  new Rectangle(new Point(), _imageTwo.Size), GraphicsUnit.Pixel);
            }
            _img.Save(fileImageOne, ImageFormat.Jpeg);
          }
        }
      }
    }
    #endregion
  }
}

