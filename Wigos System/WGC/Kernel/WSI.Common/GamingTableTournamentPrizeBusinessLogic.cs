﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GamingTablesTournamentPrizeBusinessLogic.cs
// 
//   DESCRIPTION: Gaming Tables Tournaments prize business logic
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 24-APR-2017
// 
// REVISION HISTORY:
// 
// Date         Author  Description
// -----------  ------  ----------------------------------------------------------
// 24-APR-2017  FGB     WIGOS-493: Gaming table tournament - Tournament payout to winner (single player)
// -----------  ------  ----------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingTablesTournaments
{
  public enum TOURNAMENT_PRIZE_PAYMENT_ERROR_CODE
  {
    OK = 0,
    ERROR_GENERIC = 1,
    TOURNAMENT_NOT_FOUND = 2,
  }

  public class PrizePaymentInputParameters
  {
    public Int64 TournamentId;
    public CardData Card;
    public Currency PrizeAmount;
  }

  public class PrizePaymentOutputParameters
  {
    public ArrayList VoucherList = new ArrayList();
    public TOURNAMENT_PRIZE_PAYMENT_ERROR_CODE ErrorCode = TOURNAMENT_PRIZE_PAYMENT_ERROR_CODE.OK;
    public Int64? OperationId = null;
    public String ErrorMessage = String.Empty;
  }

  public class GamingTableTournamentPrizeBusinessLogic
  {
    /// <summary>
    /// Makes a payment fro a tournament prize 
    /// </summary>
    /// <param name="inputParams"></param>
    /// <param name="outputParams"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean PayTournamentPrize(PrizePaymentInputParameters inputParams, ref PrizePaymentOutputParameters outputParams, SqlTransaction SqlTrx)
    {
      Boolean _result;

      outputParams.ErrorMessage = String.Empty;
      outputParams.ErrorCode = TOURNAMENT_PRIZE_PAYMENT_ERROR_CODE.OK;

      //TODO:ZZZZ - (FGB): 2017-04-26: MUST MAKE PAYMENT

      //Promotion.PROMOTION_TYPE _promotion_type;
      //MakePayments _make_payment;
      //_make_payment = new MakePayments(_card_data, this, _prize_selected.PrizeAmount, null);
      //_promotion_type = GetPromotionType(TournamentPrizePaymentMethod);

      //if (!_make_payment.MakePayment(_promotion_type, out MessageError, out IsWarning))
      //{
      //  return false;
      //}

      if (!UpdateTournamentPrizeStatus(inputParams, TOURNAMENT_REGISTRATION_STATUS.PAID, ref outputParams, SqlTrx))
      {
        return false;
      }

      _result = (outputParams.ErrorCode == TOURNAMENT_PRIZE_PAYMENT_ERROR_CODE.OK);

      return _result;
    }

    /// <summary>
    /// Update tournament prize status
    /// </summary>
    /// <param name="inputParams"></param>
    /// <param name="NewStatus"></param>
    /// <param name="outputParams"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean UpdateTournamentPrizeStatus(PrizePaymentInputParameters inputParams, TOURNAMENT_REGISTRATION_STATUS NewStatus, ref PrizePaymentOutputParameters outputParams, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(GetSQLTournamentPrizeUpdateStatus(), SqlTrx.Connection, SqlTrx))
        {
          //Set parameters values
          SetSQLParametersTournamentPrizeStatusChange(_sql_cmd, inputParams, NewStatus);

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            outputParams.ErrorMessage = String.Empty;
            outputParams.ErrorCode = TOURNAMENT_PRIZE_PAYMENT_ERROR_CODE.OK;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("UpdateTournamentPrizeStatus -> {0}", _ex.Message));
        Log.Exception(_ex);

        outputParams.ErrorMessage = _ex.Message;
        outputParams.ErrorCode = TOURNAMENT_PRIZE_PAYMENT_ERROR_CODE.ERROR_GENERIC;
      }

      return false;
    }

    /// <summary>
    /// Return all the tournaments prizes that it status is in the StatusToShow list.
    /// </summary>
    /// <param name="StatusToShow"></param>
    /// <returns></returns>
    public static List<GamingTableTournamentPrizeForDisplay> GetTournamentPrizeList(List<TOURNAMENT_REGISTRATION_STATUS> StatusToShow)
    {
      TOURNAMENT_REGISTRATION_STATUS _tournament_registration_status_id;

      List<GamingTableTournamentPrizeForDisplay> _tournament_prize_list;
      _tournament_prize_list = new List<GamingTableTournamentPrizeForDisplay>();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(GetSQLTournamentPrizeList(StatusToShow), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          //Set status parameters values
          SetSQLParametersTournamentPaymentStatusToFilter(_sql_cmd, StatusToShow);

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _tournament_registration_status_id = (TOURNAMENT_REGISTRATION_STATUS)_reader.GetInt32(_reader.GetOrdinal("GTTP_PAYMENT_STATUS"));

              GamingTableTournamentPrizeForDisplay _tournament_prize_info = new GamingTableTournamentPrizeForDisplay()
              {
                TournamentId = _reader.GetInt64(_reader.GetOrdinal("GTT_TOURNAMENT_ID")),
                TournamentName = _reader.GetString(_reader.GetOrdinal("GTT_TOURNAMENT_NAME")),

                Ranking = _reader.GetInt32(_reader.GetOrdinal("GTTP_TOURNAMENT_RANKING")),
                AccountId = _reader.GetInt64(_reader.GetOrdinal("GTTP_ACCOUNT_ID")),
                AccountName = _reader.GetString(_reader.GetOrdinal("AC_HOLDER_NAME")),

                TournamentRegistrationStatusId = _tournament_registration_status_id,
                TournamentRegistrationStatusDescription = TournamentDB.GetTournamentRegistrationStatusDescription(_tournament_registration_status_id),

                Prize = _reader.GetDecimal(_reader.GetOrdinal("GTTP_PRIZE")),
              };

              _tournament_prize_list.Add(_tournament_prize_info);
            }
          }
        }
      }

      //Sort the list
      IComparer<GamingTableTournamentPrizeForDisplay> _comparer = new GamingTableTournamentPrizeOrderingClass("TournamentName", SortOrder.Ascending);
      _tournament_prize_list.Sort(_comparer);

      return _tournament_prize_list;
    }

    /// <summary>
    /// Get SQL to obtain tournament prize list
    /// </summary>
    /// <param name="StatusToShow"></param>
    /// <returns></returns>
    private static String GetSQLTournamentPrizeList(List<TOURNAMENT_REGISTRATION_STATUS> StatusToShow)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   GTT_TOURNAMENT_ID                                                        ");
      _sb.AppendLine("       , GTT_TOURNAMENT_NAME                                                      ");
      _sb.AppendLine("       , GTTP_TOURNAMENT_RANKING                                                  ");
      _sb.AppendLine("       , GTTP_ACCOUNT_ID                                                          ");
      _sb.AppendLine("       , ISNULL(AC_HOLDER_NAME, '')  as AC_HOLDER_NAME                            ");
      _sb.AppendLine("       , GTTP_PAYMENT_STATUS                                                      ");
      _sb.AppendLine("       , GTTP_PRIZE                                                               ");
      _sb.AppendLine(" FROM    GAMING_TABLES_TOURNAMENT_PLAYERS                                         ");
      _sb.AppendLine(" JOIN    GAMING_TABLES_TOURNAMENTS    ON GTT_TOURNAMENT_ID = GTTP_TOURNAMENT_ID   ");
      _sb.AppendLine(" LEFT OUTER JOIN  ACCOUNTS  ON AC_ACCOUNT_ID = GTTP_ACCOUNT_ID                    ");
      _sb.AppendLine(GetSQLWhereTournamentPaymentStatusToFilter(StatusToShow));

      return _sb.ToString();
    }

    /// <summary>
    /// Update tournament prize status
    /// </summary>
    /// <returns></returns>
    private static String GetSQLTournamentPrizeUpdateStatus()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE   GAMING_TABLES_TOURNAMENT_PLAYERS        ");
      _sb.AppendLine("    SET   GTTP_PAYMENT_STATUS = @pNewStatus       ");
      _sb.AppendLine("      ,   GTTP_LAST_UPDATE_DATE = GETDATE()       ");
      _sb.AppendLine("  WHERE   GTTP_TOURNAMENT_ID = @pTournamentId     ");
      _sb.AppendLine("    AND   GTTP_ACCOUNT_ID = @pAccountId           ");

      return _sb.ToString();
    }

    /// <summary>
    /// Return SQL filter for StatusToShow list
    /// </summary>
    /// <param name="StatusToShow"></param>
    /// <returns></returns>
    private static string GetSQLWhereTournamentPaymentStatusToFilter(List<TOURNAMENT_REGISTRATION_STATUS> StatusToShow)
    {
      if ((StatusToShow == null) || (StatusToShow.Count == 0))
      {
        return String.Empty;
      }

      StringBuilder _sb_filter;
      _sb_filter = new StringBuilder();
      _sb_filter.AppendLine(String.Format("WHERE    GTTP_PAYMENT_STATUS in ({0})", String.Join(", ", StatusToShow.ConvertAll(x => "@p" + x.ToString()).ToArray())));

      return _sb_filter.ToString();
    }

    /// <summary>
    /// Set status parameters values
    /// </summary>
    /// <param name="SqlCmd"></param>
    /// <param name="StatusToShow"></param>
    private static void SetSQLParametersTournamentPaymentStatusToFilter(SqlCommand SqlCmd, List<TOURNAMENT_REGISTRATION_STATUS> StatusToShow)
    {
      if (SqlCmd == null)
      {
        return;
      }

      if ((StatusToShow == null) || (StatusToShow.Count == 0))
      {
        return;
      }

      foreach (TOURNAMENT_REGISTRATION_STATUS _status in StatusToShow)
      {
        SqlCmd.Parameters.Add("@p" + _status.ToString(), SqlDbType.Int).Value = (int)_status;
      }
    }

    /// <summary>
    /// Set status parameters values for changing status of a tournament prize
    /// </summary>
    /// <param name="SqlCmd"></param>
    /// <param name="inputParams"></param>
    /// <param name="NewStatus"></param>
    private static void SetSQLParametersTournamentPrizeStatusChange(SqlCommand SqlCmd, PrizePaymentInputParameters inputParams, TOURNAMENT_REGISTRATION_STATUS NewStatus)
    {
      if (SqlCmd == null)
      {
        return;
      }

      if (inputParams == null)
      {
        return;
      }

      SqlCmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = (int)NewStatus;
      SqlCmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = inputParams.TournamentId;
      SqlCmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = inputParams.Card.AccountId;
    }

    /// <summary>
    /// Generate the voucher for the tournament prize payment
    /// </summary>
    /// <param name="inputParams"></param>
    /// <param name="outputParams"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean GenerateVoucherTournamentPrizePayment(PrizePaymentInputParameters inputParams, ref PrizePaymentOutputParameters outputParams, SqlTransaction SqlTrx)
    {
      VoucherTournamentPrizePayment _voucher_payment;

      TournamentDB _tournament;
      _tournament = new TournamentDB();

      if (_tournament.ReadTournament(inputParams.TournamentId) != TOURNAMENT_OPERATION_RESULT.OK)
      {
        outputParams.ErrorCode = TOURNAMENT_PRIZE_PAYMENT_ERROR_CODE.TOURNAMENT_NOT_FOUND;
        outputParams.ErrorMessage = "Tournament not found";

        return false;
      }

      _voucher_payment = new VoucherTournamentPrizePayment(inputParams.Card.VoucherAccountInfo(),
                                                           inputParams.Card.AccountId,
                                                           _tournament,
                                                           PrintMode.Print,
                                                           SqlTrx);

      //Add voucher to output
      outputParams.VoucherList.Add(_voucher_payment);
      outputParams.ErrorCode = TOURNAMENT_PRIZE_PAYMENT_ERROR_CODE.OK;
      outputParams.ErrorMessage = String.Empty;

      return true;
    }
  }
}
