﻿using System.Collections.Generic;

namespace WSI.Common.Entities.General
{
  public static class SelectableOptionHelper
  {
    /// <summary>
    ///   helper routine that sets the value of an ISelectable type field in the model to the matching item in teh colleciton
    ///   provided
    /// </summary>
    /// <typeparam name="TU"></typeparam>
    /// <param name="List"></param>
    /// <param name="ItemId"></param>
    /// <param name="BlankValue"></param>
    /// <returns></returns>
    public static TU GetSelectableOption<TU>(List<TU> List, int ItemId, string BlankValue) where TU : ISelectableOption, new()
    {
      TU _option;
      _option = new TU { Id = ItemId, Name = BlankValue };

      if (List != null)
      {
        if (ItemId > List[0].Id)
        {
          List<TU> _found = List.FindAll(x => x.Id == ItemId);
          if (_found.Count == 1)
            _option = _found[0];
        }
        else
        {
          _option = List[0];
        }
      }

      return _option;
    }
  }
}