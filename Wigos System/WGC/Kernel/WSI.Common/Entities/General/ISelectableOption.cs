﻿namespace WSI.Common.Entities.General
{
  public interface ISelectableOption
  {
    int Id { get; set; }
    string Name { get; set; }
  }
}
