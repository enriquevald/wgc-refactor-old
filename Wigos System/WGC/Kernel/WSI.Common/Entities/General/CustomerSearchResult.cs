//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
//  
//   MODULE NAME : CustomerSearchResults.cs
// 
//   DESCRIPTION : 
// 
// REVISION HISTORY :
// 
// Date        Author  Description
// ----------- ------  ----------------------------------------------------------
// 25-FEB-2016  SA     Initial version?
// 20-SEP-2016  JRC    PBI 17677:Visitas / Recepción: MEJORAS II - Ref. 48 - Reportes recepción - separar columna Nombre (GUI)using System;

using System;
using System.ComponentModel;
using WSI.Common.Entrances;

namespace WSI.Common.Entities.General
{
  /// <summary>
  ///   cusomer search result type
  /// </summary>
  public class CustomerSearchResult : INotifyPropertyChanged
  {
    private BlackListResultInfo blackListResultInfo;

    public long AccountNumber { get; set; }
    public string Name { get; set; }
    public string Lastname { get; set; }
    public string Document { get; set; }
    public DateTime BirthDate { get; set; }

    public BlackListResultInfo BlackListResultInfo
    {
      get { return blackListResultInfo; }
      set
      {
        if (value != blackListResultInfo)
        {
          blackListResultInfo = value;
          NotifyPropertyChanged("blackListResultInfo");
        }
      }
    }

    #region INotifyPropertyChanged Members

    public event PropertyChangedEventHandler PropertyChanged;

    #endregion
    private void NotifyPropertyChanged(string info)
    {
      if (PropertyChanged != null)
      {
        PropertyChanged(this, new PropertyChangedEventArgs(info));
      }
    }
  }
}
