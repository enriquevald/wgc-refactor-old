namespace WSI.Common.Entities.General
{
  public class InvalidField
  {
    public string Id { get; set; }
    public string Reason { get; set; }
  }
}
