using System;
using System.ComponentModel;

namespace WSI.Common.Entities.General
{
  public class ScannedDocument : INotifyPropertyChanged
  {
    private string documentTypeDescription;

    private DateTime? expiration;

    private DateTime scannedDate;

    public int DocumentTypeId { get; set; }

    public string DocumentTypeDescription
    {
      get { return documentTypeDescription; }
      set
      {
        documentTypeDescription = value;
        NotifyPropertyChanged("DocumentTypeDescription");
      }
    }

    public byte[] Image { get; set; }

    public DateTime? Expiration
    {
      get { return expiration; }
      set
      {
        if (value != expiration)
        {
          expiration = value;
          NotifyPropertyChanged("Expiration");
        }
      }
    }

    public DateTime ScannedDate
    {
      get { return scannedDate; }
      set
      {
        if (value != scannedDate)
        {
          scannedDate = value;
          NotifyPropertyChanged("ScannedDate");
        }
      }
    }

    public bool IsExpired
    {
      get
      {
        if (!Expiration.HasValue)
        {
          return false;
        }

        return WGDB.Now.Date > Expiration.Value.Date;
      }
    }
    #region INotifyPropertyChanged Members

    public event PropertyChangedEventHandler PropertyChanged;

    #endregion
    private void NotifyPropertyChanged(string info)
    {
      if (PropertyChanged != null)
      {
        PropertyChanged(this, new PropertyChangedEventArgs(info));
      }
    }
  }
}
