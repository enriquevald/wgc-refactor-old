namespace WSI.Common.Entities.General
{
  public class Gender : ISelectableOption
  {
    #region ISelectableOption Members

    public int Id { get; set; }
    public string Name { get; set; }

    #endregion
  }
}
