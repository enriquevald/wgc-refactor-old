//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : ScanImage.cs
// 
//   DESCRIPTION : Class to manage image scanning from a scanner device
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAY-2013 TJG    First release.
// 12-MAY-2017 RGR    PBI 27131: Implementation of the Duplex Scan in the Cashier
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Drawing;
using WSI.Common.Utilities;
//using System.Drawing;

namespace WSI.Common
{
  public static class ScanImage
  {
    internal const String SCAN_FOLDER_TEMP = ".\\Scan\\Temp";
    internal const String SCAN_FOLDER_OUT = ".\\Scan\\Output";
    internal const String SCAN_FILE_TYPE = "JPG";
    internal const String SCAN_FILE_FILTER = "*." + SCAN_FILE_TYPE;
    internal const String SCAN_TOOL_PATH = ".\\";
    internal const String SCAN_TOOL_NAME = "Scan.bat";
    internal const String SCAN_CONFIG_NAME = "ScanConfig.bat";
    internal const int DEFAULT_SCAN_TIMEOUT_DEVICE = 60 * 1000;    // miliseconds
    internal const int DEFAULT_SCAN_TIMEOUT_COPY = 5 * 1000;     // miliseconds
    internal const int DEFAULT_SCAN_TIMEOUT_COPY_IMAGE2 = 500;  
    internal const string DUPLEX_NAME = "-00002";

    private enum ScanStateMachine
    {
      NONE = 0
    ,
      CHECKING = 1
    ,
      SCANNING = 2
    ,
      COPY_FILE = 3
    , FINISHED = 4
    }

    public enum ScanProcessStatus
    {
      NONE = 0
    ,
      IN_PROGRESS = 1
    ,
      ERROR = 2
    , OK = 3
    }

    private static String m_temp_folder = SCAN_FOLDER_TEMP;
    private static String m_output_folder = SCAN_FOLDER_OUT;
    private static String m_file_name;
    private static int m_timeout_device;
    private static int m_timeout_copy;
    private static ScanStateMachine m_scan_state = ScanStateMachine.NONE;
    private static Boolean m_scan_result = false;

    //------------------------------------------------------------------------------
    // PURPOSE : Clears the scan folder and setups to start a new scan
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private static void ClearFolder()
    {
      // Steps
      //      - Create scan working folders
      //      - Clear the scan folder

      try
      {
        if (m_scan_state != ScanStateMachine.NONE
          && m_scan_state != ScanStateMachine.FINISHED)
        {
          // Scan already in progress
          return;
        }

        //    - Create scan working folders
        if (!Directory.Exists(m_temp_folder))
        {
          Directory.CreateDirectory(m_temp_folder);
        }

        if (!Directory.Exists(m_output_folder))
        {
          Directory.CreateDirectory(m_output_folder);
        }

        //    - Clear the scan folder
        //      Remove image files from temp folder

        string[] _files = Directory.GetFiles(m_temp_folder, SCAN_FILE_FILTER);

        foreach (string _file_name in _files)
        {
          File.Delete(_file_name);
        }
      }
      catch
      {
      }
    } // ClearFolder

    //------------------------------------------------------------------------------
    // PURPOSE : Scan file name support
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :


    private static string TempFileName()
    {
      return Path.Combine(m_temp_folder, m_file_name + "." + SCAN_FILE_TYPE);
    }

    private static string TempFileName2()
    {
      return string.Format(@"{0}\{1}{2}.{3}", m_temp_folder, m_file_name, DUPLEX_NAME, SCAN_FILE_TYPE);
    }

    private static string OutputFileName()
    {
      return Path.Combine(m_output_folder, m_file_name + "." + SCAN_FILE_TYPE);
    }
 
    //------------------------------------------------------------------------------
    // PURPOSE : Check if cmdTwain files exist
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static bool CheckCmdTwainFiles()
    {
      String[] _cmdTwain_files = { "Scan.bat" };
      String _cmdTwain_path;

      _cmdTwain_path = SCAN_TOOL_PATH;

      foreach (String _current_file in _cmdTwain_files)
      {
        if (!File.Exists(_cmdTwain_path + _current_file))
        {
          return false;
        }
      }

      return true;
    } // CheckCmdTwainFiles

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the scanning status once is finished 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - OutPutFileName: Relative path to the output image file
    //
    // RETURNS :
    //
    //   NOTES :
    private static bool GetScanStatus(out String OutPutFileName, ref Semaphore ScanSemaphore)
    {
      ScanProcessStatus _res;

      OutPutFileName = null;

      _res = ScanProcessStatus.NONE;

      ScanSemaphore.WaitOne(DEFAULT_SCAN_TIMEOUT_DEVICE);

      _res = Status(out OutPutFileName);

      if (_res == ScanProcessStatus.OK && OutPutFileName != null && OutPutFileName != "")
      {
        return true;
      }

      return false;

    } // GetScanStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Thread that starts the scan 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private static void ScanThread(ref Semaphore ScanSemaphore)
    {
      int _num_tries;
      int _max_tries;
      int _try_interval;
      ProcessStartInfo _start_info;
      FileStream _file_stream;
      StreamReader _file_stream_reader;
      Boolean _file_copied;
      Process _process;

      try
      {
        // Check if cmdTwain files exist
        if (!CheckCmdTwainFiles())
        {
          throw new Exception("CmdTwain files not found");
        }

        m_scan_state = ScanStateMachine.SCANNING;

        // Prepare the process to be launched
        _start_info = new System.Diagnostics.ProcessStartInfo();
        _start_info.Arguments = TempFileName(); // + TempFileName();
        _start_info.FileName = SCAN_TOOL_PATH + SCAN_TOOL_NAME;
        _start_info.CreateNoWindow = true;
        _start_info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal; // System.Diagnostics.ProcessWindowStyle.Hidden;
        _start_info.RedirectStandardOutput = true;
        _start_info.UseShellExecute = false;
        _start_info.RedirectStandardOutput = true;

        _process = new System.Diagnostics.Process();
        _process.StartInfo = _start_info;
        _process.Start();

        if (!_process.HasExited)
        {
          // Wait for scanning process
          if (!_process.WaitForExit(m_timeout_device))
          {
            // Process has not finished => Timeout 
          }
          else
          {
            if (_process.ExitCode != 0)
            {
              // CmdTwain didn't finish properly
            }
            else
            {
              // Allow some time to finish file generation and then copy 

              // Copy generated image to output folder
              if (!Directory.Exists(m_output_folder))
              {
                Directory.CreateDirectory(m_output_folder);
              }

              _file_copied = false;

              // Allow som time to CmdTwain to complete generate the scanned file
              _try_interval = 200;        // Check it out every 200ms
              _max_tries = Math.Min(m_timeout_copy / _try_interval, 1);

              for (_num_tries = 0; _num_tries < _max_tries; _num_tries++)
              {
                // Check whether file is available and CmdTwain has released it
                try
                {
                  _file_stream = new FileStream(TempFileName(), FileMode.Open, FileAccess.ReadWrite);
                  _file_stream_reader = new StreamReader(_file_stream);

                  _file_stream_reader.Close();
                  _file_stream.Close();

                  _file_copied = true;

                  // Break out from the endless loop
                  break;
                }
                //catch ( FileNotFoundException )
                //{
                //  break;
                //}
                catch (IOException)
                {
                  // Sleep for a while before trying
                  Thread.Sleep(_try_interval);
                } // end try
              } // for

              if (_file_copied)
              {
                try
                {
                  m_scan_state = ScanStateMachine.COPY_FILE;

                  if (File.Exists(TempFileName()))
                  {
                    Thread.Sleep(DEFAULT_SCAN_TIMEOUT_COPY_IMAGE2);
                    if (File.Exists(TempFileName2()))
                    {
                      ZImage.Concat(TempFileName(), TempFileName2());
                      File.Delete(TempFileName2());
                    }

                    // Copy and overwrite destination file

                    File.Copy(TempFileName(), OutputFileName(), true);
                    File.Delete(TempFileName());

                    m_scan_result = true;
                  }
                }
                //catch ( IOException )
                catch (Exception _ex)
                {
                  Log.Exception(_ex);
                } // end try
              }   // if - _file_copied
            }     // if - m_process.ExitCode 
          }       // if - m_process.WaitForExit

          KillCmdTwain();

          _process.Close();

          m_scan_state = ScanStateMachine.FINISHED;

          // Scanning finished.  Release semaphore to let read the output file.
          ScanSemaphore.Release();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        KillCmdTwain();

        m_scan_state = ScanStateMachine.FINISHED;
      }
    } // ScanThread

    public static void ConfigureScanner()
    {
      ProcessStartInfo _start_info;
      Process _process;

      try
      {
        KillCmdTwain();

        // Prepare the process to be launched
        _start_info = new System.Diagnostics.ProcessStartInfo();
        _start_info.FileName = SCAN_TOOL_PATH + SCAN_CONFIG_NAME;
        _start_info.CreateNoWindow = true;
        _start_info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal; // System.Diagnostics.ProcessWindowStyle.Hidden;
        _start_info.RedirectStandardOutput = true;
        _start_info.UseShellExecute = false;
        _start_info.RedirectStandardOutput = true;

        _process = new System.Diagnostics.Process();
        _process.StartInfo = _start_info;
        _process.Start();

        if (!_process.HasExited)
        {
          // Wait for scanning process
          if (!_process.WaitForExit(40000))
          {
            // Process has not finished => Timeout 
          }
          else
          {
            if (_process.ExitCode != 0)
            {
              // CmdTwain didn't finish properly
            }
            else
            {
            }
          } // end else
        } // end if
      } // end try
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        KillCmdTwain();
      }
    } // ConfigureScanner

    //------------------------------------------------------------------------------
    // PURPOSE : Method that starts the thread that starts the scan 
    //
    //  PARAMS :
    //      - INPUT :
    //          - FileName
    //          - Timeout
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static Boolean Start(String FileName, out String OutPutFileName)
    {
      return Start(FileName, out OutPutFileName, 0);
    }

    public static Boolean Start(String FileName, out String OutPutFileName, int Timeout)
    {
      Thread _thread;
      Semaphore _scan_semaphore;

      // Steps
      //      - 

      OutPutFileName = null;
    
      _scan_semaphore = new Semaphore(0, 1);

      try
      {
        if (m_scan_state != ScanStateMachine.NONE
          && m_scan_state != ScanStateMachine.FINISHED)
        {
          // Scan already in progress
          return false;
        }

        ClearFolder();

        m_scan_state = ScanStateMachine.CHECKING;
        m_scan_result = false;

        KillCmdTwain();

        // CmdTwain tool doesn't handle that names properly => Remove blanks from file name
        m_file_name = FileName.Replace(" ", "");

        if (m_file_name.Length < 1)
        {
          // Invalid filename
          return false;
        }

        if (Timeout == 0)
        {
          m_timeout_device = DEFAULT_SCAN_TIMEOUT_DEVICE;
          m_timeout_copy = DEFAULT_SCAN_TIMEOUT_COPY;
        }
        else
        {
          m_timeout_device = Timeout;
          m_timeout_copy = DEFAULT_SCAN_TIMEOUT_COPY;
        }

        // Launch scan thread    
        _thread = new Thread(new ThreadStart(delegate { ScanThread(ref _scan_semaphore); }));
        _thread.Start();

        // Get the scan status and exit
        return GetScanStatus(out OutPutFileName, ref _scan_semaphore);
      }
      catch
      {
        m_scan_state = ScanStateMachine.FINISHED;
        m_scan_result = false;

        return false;
      }
    } // Start

    //------------------------------------------------------------------------------
    // PURPOSE : Method that lets know the state of a scan 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static ScanProcessStatus Status(out String FileName)
    {
      FileName = string.Empty;    
      try
      {
        switch (m_scan_state)
        {
          case ScanStateMachine.NONE:
            return ScanProcessStatus.NONE;

          case ScanStateMachine.FINISHED:
            if (m_scan_result)
            {
              FileName = OutputFileName();             
              return ScanProcessStatus.OK;
            }

            return ScanProcessStatus.ERROR;

          default:
            return ScanProcessStatus.IN_PROGRESS;
        }
      }
      catch
      {
        return ScanProcessStatus.NONE;
      }
    } // Status

    //------------------------------------------------------------------------------
    // PURPOSE : Kills CmdTwain process tree
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private static void KillCmdTwain()
    {
      Process[] _process_list;

      // Steps
      //      - Kill scan2bmps
      //      - Kill bmp2jpg
      //      - Kill cmdtwain

      try
      {
        //    - Kill scan2bmps
        _process_list = Process.GetProcessesByName("scan2bmps");

        foreach (Process _process in _process_list)
        {
          _process.Kill();
        }

        //    - Kill bmp2jpg
        _process_list = Process.GetProcessesByName("bmp2jpg");

        foreach (Process _process in _process_list)
        {
          _process.Kill();
        }

        //    - Kill CmdTwain
        _process_list = Process.GetProcessesByName("cmdtwain");

        foreach (Process _process in _process_list)
        {
          _process.Kill();
        }
      }
      catch
      {
      }
    } // KillCmdTwain

    public static Boolean CropImage(Image Img,
                                    RectangleF Rect,
                                    Boolean ToGray,
                                    Int32 Quality,
                                    out Image Target)
    {
      Rectangle _rect_convert;
      System.Drawing.Imaging.ImageCodecInfo _image_codec_info;
      System.Drawing.Imaging.Encoder _encoder;
      System.Drawing.Imaging.EncoderParameter _encoder_parameter;
      System.Drawing.Imaging.EncoderParameters _encoder_parameters;
      Bitmap _img_aux;

      Target = null;

      if (Rect.Height == 0 || Rect.Width == 0)
      {
        Target = Img;

        return true;
      }

      try
      {
        _rect_convert = new Rectangle();
        _rect_convert.Width = (Int32)(Rect.Width / 10 * WSI.Common.TemplatePDF.CM_TO_INCH * Img.HorizontalResolution);
        _rect_convert.Height = (Int32)(Rect.Height / 10 * WSI.Common.TemplatePDF.CM_TO_INCH * Img.VerticalResolution);
        _rect_convert.X = (Int32)(Rect.X / 10 * WSI.Common.TemplatePDF.CM_TO_INCH * Img.HorizontalResolution);
        _rect_convert.Y = (Int32)(Rect.Y / 10 * WSI.Common.TemplatePDF.CM_TO_INCH * Img.HorizontalResolution);

        _img_aux = new Bitmap(_rect_convert.Width, _rect_convert.Height, Img.PixelFormat);
        _img_aux.SetResolution(Img.HorizontalResolution, Img.VerticalResolution);

        //create some image attributes
        System.Drawing.Imaging.ImageAttributes _attribs;
        _attribs = new System.Drawing.Imaging.ImageAttributes();
        if (ToGray)
        {
          //create the grayscale ColorMatrix
          System.Drawing.Imaging.ColorMatrix _color_matrix = new System.Drawing.Imaging.ColorMatrix(
                 new float[][] 
          {
             new float[] {.3f, .3f, .3f, 0, 0},
             new float[] {.59f, .59f, .59f, 0, 0},
             new float[] {.11f, .11f, .11f, 0, 0},
             new float[] {0, 0, 0, 1, 0},
             new float[] {0, 0, 0, 0, 1}
          });
          //set the color matrix attribute
          _attribs.SetColorMatrix(_color_matrix);
        }

        using (Graphics _graph = Graphics.FromImage(_img_aux))
        {
          _graph.DrawImage(Img, new Rectangle(0, 0, _rect_convert.Width, _rect_convert.Height), _rect_convert.X, _rect_convert.Y, _rect_convert.Width, _rect_convert.Height, GraphicsUnit.Pixel, _attribs);
        }

        _image_codec_info = GetEncoderInfo(System.Drawing.Imaging.ImageFormat.Jpeg);
        _encoder = System.Drawing.Imaging.Encoder.Quality;
        _encoder_parameters = new System.Drawing.Imaging.EncoderParameters(1);
        _encoder_parameter = new System.Drawing.Imaging.EncoderParameter(_encoder, (Int32)Quality);
        _encoder_parameters.Param[0] = _encoder_parameter;

        using (MemoryStream _mm = new MemoryStream())
        {
          _img_aux.Save(_mm, _image_codec_info, _encoder_parameters);
          Target = Image.FromStream(new MemoryStream(_mm.ToArray()));
        }

        _img_aux.Dispose();

        return true;
      }
      catch (Exception _ex)
      {
        Target = null;
        Log.Message("TemplatePDF.CropImage:" + _ex.Message);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Build a RectangleF object with a values passed in a string 
    //
    //  PARAMS:
    //      - INPUT: String RectangleValues
    //
    //      - OUTPUT: RectangleF RectangleToCrop
    //
    // RETURNS:
    //          - Boolean: true if it can build the rectangle object
    // 
    //   NOTES:
    //
    public static Boolean GetRectangleFromValues(String DocumentType, out RectangleF RectangleToCrop)
    {
      RectangleToCrop = new RectangleF();

      try
      {
        String _document_type_values;
        String[] _rectangle_values;

        //Obtain area and postion values
        _document_type_values = GeneralParam.GetString("Cashier.Scan", String.Format("Document.{0}.XYWH", DocumentType.PadLeft(3, '0')));

        if (String.IsNullOrEmpty(_document_type_values))
        {
          return true;
        }

        // Obtain values
        _rectangle_values = _document_type_values.Split(',');

        // Check for the four values needed
        if (_rectangle_values.Length == 4)
        {
          List<float> _float_values_list;
          float _float_value;

          _float_values_list = new List<float>();

          foreach (String _value in _rectangle_values)
          {
            if (!float.TryParse(_value,
                                System.Globalization.NumberStyles.Any,
                                System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat,
                                out _float_value))
            {
              return false;
            }
            _float_values_list.Add(_float_value);
          }

          RectangleToCrop = new RectangleF(_float_values_list[0], _float_values_list[1], _float_values_list[2], _float_values_list[3]);

          if (RectangleToCrop.Height == 0 || RectangleToCrop.Width == 0)
          {
            RectangleToCrop = new RectangleF();
          }

          return true;
        }
      }
      catch (Exception Ex)
      {
        Log.Error(String.Format(" ScanImage: Getting the cutting values - '{0}'", Ex.Message));
      }

      return false;

    }//GetRectangleFromValues

    private static System.Drawing.Imaging.ImageCodecInfo GetEncoderInfo(System.Drawing.Imaging.ImageFormat format)
    {
      System.Drawing.Imaging.ImageCodecInfo[] _encoders;

      _encoders = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();

      foreach (System.Drawing.Imaging.ImageCodecInfo _encoder in _encoders)
      {
        if (_encoder.FormatID == format.Guid)
        {
          return _encoder;
        }
      }

      return null;
    }

  }
} // class ScanImage
