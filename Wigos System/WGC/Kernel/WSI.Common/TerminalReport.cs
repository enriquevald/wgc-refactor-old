//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalReport.cs
// 
//   DESCRIPTION: Report module for terminals data.
// 
//        AUTHOR: Luis Mesa
// 
// CREATION DATE: 15-SEP-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-SEP-2014 LEM    First version.
// 06-OCT-2014 LEM    Reload on demand.
// 28-JAN-2015 ANM    Add id floor column in provider report type
// 09-MAY-2016 RAB    PBI 12627:AFIP Client: Recovery account denomination
// 24-MAY-2016 RAB    PBI 13462:Review Sprint 24 (Accounting Denomination)
// 12-JUL-2016 RAB    Bug 15251:AFIP - GUI: Historical of counters does not correctly report initial, Final, increase
// 14-JUL-2016 RAB    Bug 15593: GUI-in printing code QR and blocking of machines not works the list of terminals
// 23-JAN-2017 RAB    Bug 23260:estadistica grouped by terminal displays incorrect game
// 07-FEB-2017 RAB    Bug 24283: Reports without properly informing the account denomination
// 09-FEB-2017 AMF    Bug 23488:Ocupación de maquinas no exporta correctamente a Excel
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Threading;

namespace WSI.Common
{
  public static class TerminalReport
  {
    #region Members
    private static DataTable m_terminal_data = new DataTable();
    private static Int32 WAIT_REFRESH_TIME = 60 * 60 * 1000; //1 hour
    private static Boolean m_is_center;
    private static AutoResetEvent m_terminals_changed = new AutoResetEvent(true);
    #endregion

    #region Constants

    private const String MAP_COL_TERMINAL_ID = "te_terminal_id";
    private const String MAP_COL_SITE_ID = "te_site_id";
    private const String MAP_COL_PROVIDER_NAME = "pv_name";
    private const String MAP_COL_TERMINAL_NAME = "te_name";
    private const String MAP_COL_FLOOR_ID = "te_floor_id";
    private const String MAP_COL_AREA = "ar_name";
    private const String MAP_COL_BANK = "bk_name";
    private const String MAP_COL_POSITION = "te_position";
    private const String MAP_COL_ACCOUNT_DENOMINATION = "te_sas_accounting_denom";
    private const String MAP_COL_TERMINAL_ISO_CODE = "te_iso_code";

    private const Int32 WIDTH_COL_PROVIDER = 2000;
    private const Int32 WIDTH_COL_TERMINAL = 2200;
    private const Int32 WIDTH_COL_FLOOR = 1300;
    private const Int32 WIDTH_COL_AREA = 1900;
    private const Int32 WIDTH_COL_BANK = 1400;
    private const Int32 WIDTH_COL_POSITION = 1000;
    private const Int32 WIDTH_COL_ACCOUNT_DENOMINATION = 2300;

    private static String STR_PROVIDER = Resource.String("STR_TERMINAL_REPORT_PROVIDER");
    private static String STR_TERMINAL = Resource.String("STR_TERMINAL_REPORT_TERMINAL");
    private static String STR_FLOOR = Resource.String("STR_MAILING_TERMINALS_WITHOUT_ACTIVITY_COLUMN_05");
    private static String STR_AREA = Resource.String("STR_TERMINAL_REPORT_AREA");
    private static String STR_BANK = Resource.String("STR_TERMINAL_REPORT_BANK");
    private static String STR_POSITION = Resource.String("STR_TERMINAL_REPORT_POSITION");
    private static String STR_NAME = Resource.String("STR_TERMINAL_REPORT_NAME");
    private static String STR_ACCOUNT_DENOMINATION = Resource.String("STR_TERMINAL_REPORT_ACCOUNT_DENOMINATION");

    private const Int32 NUM_COLUMNS_PROVIDER = 4;
    private const Int32 NUM_COLUMNS_LOCATION = 3;

    private const String AUDIT_NONE_STRING = "---";

    #endregion

    #region Privates

    private static Boolean LoadDBData(SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _data;

      m_is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter");

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   terminals.*, pv_name, bk_name, bk_area_id, ar_name ");
        _sb.AppendLine("           , (select te_name from terminals where te_server_id = te_terminal_id) as te_server_name ");
        _sb.AppendLine("      FROM   terminals                                          ");
        _sb.AppendLine(" LEFT JOIN   providers  ON te_prov_id = pv_id                   " + (m_is_center ? " AND pv_site_id = 0 " : ""));
        _sb.AppendLine(" LEFT JOIN   banks      ON te_bank_id = bk_bank_id              " + (m_is_center ? " AND bk_site_id = te_site_id " : ""));
        _sb.AppendLine(" LEFT JOIN   areas      ON bk_area_id = ar_area_id              " + (m_is_center ? " AND bk_site_id = ar_site_id " : ""));

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _data = new DataTable();
            _da.Fill(_data);
          }
        }

        m_terminal_data = _data;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    private static Boolean Refresh()
    {
      Boolean _loaded;

      _loaded = false;
      using (DB_TRX _db_trx = new DB_TRX())
      {
        _loaded = LoadDBData(_db_trx.SqlTransaction);
      }

      return _loaded;
    }

    private static void TerminalReportThread()
    {
      Int32 _wait;

      _wait = WAIT_REFRESH_TIME;

      while (true)
      {
        try
        {
          m_terminals_changed.WaitOne(_wait);

          if (!WGDB.Initialized || WGDB.ConnectionState != ConnectionState.Open)
          {
            _wait = 500;

            continue;
          }

          if (!TerminalReport.Refresh())
          {
            _wait = 10 * 1000;

            Log.Warning("Refresh TerminalReport failed!");

            continue;
          }

          _wait = WAIT_REFRESH_TIME;
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          _wait = 10 * 1000;
        }
      }
    }

    #endregion

    #region Publics

    public static Int32 WaitMiliseconds
    {
      get
      {
        return WAIT_REFRESH_TIME;
      }
      set
      {
        WAIT_REFRESH_TIME = value;
      }
    }

    public static Boolean Init()
    {
      Thread _thread = null;

      try
      {
        _thread = new Thread(new ThreadStart(TerminalReportThread));
        _thread.Start();

        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static void ForceRefresh()
    {
      m_terminals_changed.Set();
    }

    ////------------------------------------------------------------------------------
    //// PURPOSE: Return list of columns settings for report.
    ////          
    ////  PARAMS:
    ////      - INPUT:
    ////        - Type        : Report type group
    ////
    //// RETURNS: 
    ////        - List<ColumnSettings>: Column settings list depends ReportType  
    //// 
    public static List<ColumnSettings> GetColumnStyles(ReportType Type)
    {
      List<ColumnSettings> _styles;

      _styles = new List<ColumnSettings>();

      foreach (ReportType _type in Enum.GetValues(typeof(ReportType)))
      {
        if (_type != ReportType.Full)
        {
          switch ((ReportType)(Type & _type))
          {
            case ReportType.Provider:
              _styles.Add(new ColumnSettings(WIDTH_COL_PROVIDER, STR_PROVIDER, STR_PROVIDER, ColumnAlign.LeftCenter, MAP_COL_PROVIDER_NAME, ReportColumn.Provider));
              _styles.Add(new ColumnSettings(WIDTH_COL_TERMINAL, STR_TERMINAL, STR_NAME, ColumnAlign.LeftCenter, MAP_COL_TERMINAL_NAME, ReportColumn.Terminal));
              _styles.Add(new ColumnSettings(WIDTH_COL_FLOOR, STR_FLOOR, STR_FLOOR, ColumnAlign.LeftCenter, MAP_COL_FLOOR_ID, ReportColumn.Floor));
              _styles.Add(new ColumnSettings(WIDTH_COL_ACCOUNT_DENOMINATION, STR_ACCOUNT_DENOMINATION, STR_ACCOUNT_DENOMINATION, ColumnAlign.RightCenter, MAP_COL_ACCOUNT_DENOMINATION, ReportColumn.AccountDenomination));
              break;

            case ReportType.Location:
              _styles.Add(new ColumnSettings(WIDTH_COL_AREA, STR_AREA, STR_AREA, ColumnAlign.LeftCenter, MAP_COL_AREA, ReportColumn.Area));
              _styles.Add(new ColumnSettings(WIDTH_COL_BANK, STR_BANK, STR_BANK, ColumnAlign.LeftCenter, MAP_COL_BANK, ReportColumn.Bank));
              _styles.Add(new ColumnSettings(WIDTH_COL_POSITION, STR_POSITION, STR_POSITION, ColumnAlign.RightCenter, MAP_COL_POSITION, ReportColumn.Position));
              break;

            default:
              break;
          }
        }
      }

      return _styles;
    }

    ////------------------------------------------------------------------------------
    //// PURPOSE: Return data column for TerminalId.
    ////          
    ////  PARAMS:
    ////      - INPUT:
    ////        - TerminalId  : Target terminal
    ////        - ColumnName  : Column name
    ////
    //// RETURNS: 
    ////        - Object: Data object for ColumnName  
    ////
    public static Object GetReportData(Int32 TerminalId, ReportColumn Column)
    {
      return GetReportData(0, TerminalId, Column);
    }

    public static Object GetReportData(Int32 SiteId, Int32 TerminalId, ReportColumn Column)
    {
      String _filter;
      DataRow[] _rows;

      if (m_terminal_data == null || m_terminal_data.Rows.Count == 0)
      {
        return null;
      }

      try
      {
        _filter = MAP_COL_TERMINAL_ID + " = " + TerminalId + (m_is_center ? " AND " + MAP_COL_SITE_ID + " = " + SiteId : "");
        _rows = m_terminal_data.Select(_filter);

        if (_rows != null && _rows.Length > 0)
        {
          return _rows[0][ColumnMapName(Column)];
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;
    }

    ////------------------------------------------------------------------------------
    //// PURPOSE: Return list of data for TerminalId.
    ////          
    ////  PARAMS:
    ////      - INPUT:
    ////        - TerminalId  : Target terminal
    ////        - Type        : Report type group
    ////
    //// RETURNS: 
    ////        - List<Object>: Contains data depends ReportType  
    ////
    public static List<Object> GetReportDataList(Int32 TerminalId, ReportType Type)
    {
      return GetReportDataList(0, TerminalId, Type);
    }

    public static List<Object> GetReportDataList(Int32 SiteId, Int32 TerminalId, ReportType Type)
    {
      String _filter;
      DataRow[] _rows;
      DataRow _terminal_data;
      List<Object> _values;

      _values = new List<Object>();

      if (m_terminal_data == null || m_terminal_data.Rows.Count == 0)
      {
        return _values;
      }

      try
      {
        _filter = MAP_COL_TERMINAL_ID + " = " + TerminalId + (m_is_center ? " AND " + MAP_COL_SITE_ID + " = " + SiteId : "");
        _rows = m_terminal_data.Select(_filter);

        if (_rows != null && _rows.Length > 0)
        {
          _terminal_data = _rows[0];

          foreach (ReportType _type in Enum.GetValues(typeof(ReportType)))
          {
            if (_type != ReportType.Full)
            {
              switch ((ReportType)(Type & _type))
              {
                case ReportType.Provider:
                  _values.Add(_terminal_data[MAP_COL_PROVIDER_NAME]);
                  _values.Add(_terminal_data[MAP_COL_TERMINAL_NAME]);
                  _values.Add(_terminal_data[MAP_COL_FLOOR_ID] != DBNull.Value ? _terminal_data[MAP_COL_FLOOR_ID] : "");

                  String _account_denomination = AUDIT_NONE_STRING;
                  if (_terminal_data[MAP_COL_ACCOUNT_DENOMINATION] != DBNull.Value && Convert.ToDecimal(_terminal_data[MAP_COL_ACCOUNT_DENOMINATION]) > 0)
                  {
                    _account_denomination = String.Format("{0:N2}", _terminal_data[MAP_COL_ACCOUNT_DENOMINATION]) + " " + _terminal_data[MAP_COL_TERMINAL_ISO_CODE];
                  }
                  _values.Add(_account_denomination);

                  break;

                case ReportType.Location:
                  _values.Add(_terminal_data[MAP_COL_AREA]);
                  _values.Add(_terminal_data[MAP_COL_BANK]);

                  if (_terminal_data[MAP_COL_POSITION] != null && _terminal_data[MAP_COL_POSITION] != DBNull.Value)
                  {
                    _values.Add(String.Format("{0:#,#}", _terminal_data[MAP_COL_POSITION]));
                  }
                  else
                  {
                    _values.Add(_terminal_data[MAP_COL_POSITION]);
                  }
                  break;

                default:
                  break;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _values;
    }

    ////------------------------------------------------------------------------------
    //// PURPOSE: Append/Add terminals data columns for ReportType to SourceTable.
    ////          
    ////  PARAMS:
    ////      - INPUT:
    ////        - SourceTable : Original data source. Can be null.
    ////        - Type        : Report type group
    ////
    //// RETURNS: 
    ////        - Boolean: True if every is ok. False otherwise.  
    //// 
    public static Boolean GetReportDataTable(DataTable SourceTable, ReportType Type)
    {
      Boolean _no_source;
      DataRow _row;
      DataTable _source_copy;

      if (m_terminal_data == null || m_terminal_data.Rows.Count == 0)
      {
        return false;
      }

      _source_copy = null;
      _no_source = (SourceTable == null);

      if (_no_source)
      {
        SourceTable = new DataTable();
        SourceTable.Columns.Add(MAP_COL_TERMINAL_ID);
        foreach (DataRow _dr in m_terminal_data.Rows)
        {
          SourceTable.Rows.Add(_dr[MAP_COL_TERMINAL_ID]);
          if (m_is_center)
          {
            SourceTable.Rows.Add(_dr[MAP_COL_SITE_ID]);
          }
        }
      }
      else
      {
        if (!SourceTable.Columns.Contains(MAP_COL_TERMINAL_ID) || (m_is_center && !SourceTable.Columns.Contains(MAP_COL_SITE_ID)))
        {
          return false;
        }

        _source_copy = SourceTable.Copy();
      }

      try
      {
        foreach (ReportType _type in Enum.GetValues(typeof(ReportType)))
        {
          if (_type != ReportType.Full)
          {
            switch ((ReportType)(Type & _type))
            {
              case ReportType.Provider:
                SourceTable.Columns.Add(MAP_COL_PROVIDER_NAME);
                SourceTable.Columns.Add(MAP_COL_TERMINAL_NAME);
                SourceTable.Columns.Add(MAP_COL_FLOOR_ID);
                SourceTable.Columns.Add(MAP_COL_ACCOUNT_DENOMINATION);

                if (_no_source)
                {
                  for (Int32 i = 0; i < m_terminal_data.Rows.Count; i++)
                  {
                    SourceTable.Rows[i][MAP_COL_PROVIDER_NAME] = m_terminal_data.Rows[i][MAP_COL_PROVIDER_NAME] != DBNull.Value ? m_terminal_data.Rows[i][MAP_COL_PROVIDER_NAME] : "";
                    SourceTable.Rows[i][MAP_COL_TERMINAL_NAME] = m_terminal_data.Rows[i][MAP_COL_TERMINAL_NAME] != DBNull.Value ? m_terminal_data.Rows[i][MAP_COL_TERMINAL_NAME] : "";
                    SourceTable.Rows[i][MAP_COL_FLOOR_ID] = m_terminal_data.Rows[i][MAP_COL_FLOOR_ID] != DBNull.Value ? m_terminal_data.Rows[i][MAP_COL_FLOOR_ID] : "";

                    String _account_denomination = AUDIT_NONE_STRING;
                    if (m_terminal_data.Rows[i][MAP_COL_ACCOUNT_DENOMINATION] != DBNull.Value && Convert.ToDecimal(m_terminal_data.Rows[i][MAP_COL_ACCOUNT_DENOMINATION]) > 0)
                    {
                      _account_denomination = String.Format("{0:N2}", m_terminal_data.Rows[i][MAP_COL_ACCOUNT_DENOMINATION]) + " " + m_terminal_data.Rows[i][MAP_COL_TERMINAL_ISO_CODE + 10];
                    }

                    SourceTable.Rows[i][MAP_COL_ACCOUNT_DENOMINATION] = _account_denomination;
                  }
                }
                else
                {
                  foreach (DataRow _dr in SourceTable.Rows)
                  {
                    _row = null;
                    if (_dr[MAP_COL_TERMINAL_ID] != null && _dr[MAP_COL_TERMINAL_ID] != DBNull.Value)
                    {
                      if (m_is_center)
                      {
                        if (_dr[MAP_COL_SITE_ID] != null && _dr[MAP_COL_SITE_ID] != DBNull.Value)
                        {
                          _row = m_terminal_data.Select(MAP_COL_TERMINAL_ID + " = " + _dr[MAP_COL_TERMINAL_ID] + " AND " + MAP_COL_SITE_ID + " = " + _dr[MAP_COL_SITE_ID])[0];
                        }
                      }
                      else
                      {
                        _row = m_terminal_data.Select(MAP_COL_TERMINAL_ID + " = " + _dr[MAP_COL_TERMINAL_ID])[0];
                      }
                    }

                    if (_row != null)
                    {
                      _dr[MAP_COL_PROVIDER_NAME] = _row[MAP_COL_PROVIDER_NAME] != DBNull.Value ? _row[MAP_COL_PROVIDER_NAME] : "";
                      _dr[MAP_COL_TERMINAL_NAME] = _row[MAP_COL_TERMINAL_NAME] != DBNull.Value ? _row[MAP_COL_TERMINAL_NAME] : "";
                      _dr[MAP_COL_FLOOR_ID] = _row[MAP_COL_FLOOR_ID] != DBNull.Value ? _row[MAP_COL_FLOOR_ID] : "";

                      String _account_denomination = AUDIT_NONE_STRING;
                      if (_row[MAP_COL_ACCOUNT_DENOMINATION] != DBNull.Value && Convert.ToDecimal(_row[MAP_COL_ACCOUNT_DENOMINATION]) > 0)
                      {
                        _account_denomination = String.Format("{0:N2}", _row[MAP_COL_ACCOUNT_DENOMINATION]) + " " + _row[MAP_COL_TERMINAL_ISO_CODE];
                      }

                      _dr[MAP_COL_ACCOUNT_DENOMINATION] = _account_denomination;
                    }
                  }
                }
                break;

              case ReportType.Location:
                SourceTable.Columns.Add(MAP_COL_AREA);
                SourceTable.Columns.Add(MAP_COL_BANK);
                SourceTable.Columns.Add(MAP_COL_POSITION);

                if (_no_source)
                {
                  for (Int32 i = 0; i < m_terminal_data.Rows.Count; i++)
                  {
                    SourceTable.Rows[i][MAP_COL_AREA] = m_terminal_data.Rows[i][MAP_COL_AREA] != DBNull.Value ? m_terminal_data.Rows[i][MAP_COL_AREA] : "";
                    SourceTable.Rows[i][MAP_COL_BANK] = m_terminal_data.Rows[i][MAP_COL_BANK] != DBNull.Value ? m_terminal_data.Rows[i][MAP_COL_BANK] : "";
                    SourceTable.Rows[i][MAP_COL_POSITION] = m_terminal_data.Rows[i][MAP_COL_POSITION] != DBNull.Value ? m_terminal_data.Rows[i][MAP_COL_POSITION] : "";
                  }
                }
                else
                {
                  foreach (DataRow _dr in SourceTable.Rows)
                  {
                    _row = null;
                    if (_dr[MAP_COL_TERMINAL_ID] != null && _dr[MAP_COL_TERMINAL_ID] != DBNull.Value)
                    {
                      if (m_is_center)
                      {
                        if (_dr[MAP_COL_SITE_ID] != null && _dr[MAP_COL_SITE_ID] != DBNull.Value)
                        {
                          _row = m_terminal_data.Select(MAP_COL_TERMINAL_ID + " = " + _dr[MAP_COL_TERMINAL_ID] + " AND " + MAP_COL_SITE_ID + " = " + _dr[MAP_COL_SITE_ID])[0];
                        }
                      }
                      else
                      {
                        _row = m_terminal_data.Select(MAP_COL_TERMINAL_ID + " = " + _dr[MAP_COL_TERMINAL_ID])[0];
                      }
                    }

                    if (_row != null)
                    {
                      _dr[MAP_COL_AREA] = _row[MAP_COL_AREA] != DBNull.Value ? _row[MAP_COL_AREA] : "";
                      _dr[MAP_COL_BANK] = _row[MAP_COL_BANK] != DBNull.Value ? _row[MAP_COL_BANK] : "";
                      _dr[MAP_COL_POSITION] = _row[MAP_COL_POSITION] != DBNull.Value ? _row[MAP_COL_POSITION] : "";
                    }
                  }
                }
                break;

              default:
                break;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        SourceTable = _source_copy;

        return false;
      }
    }

    ////------------------------------------------------------------------------------
    //// PURPOSE: Return number of columns for report.
    ////          
    ////  PARAMS:
    ////      - INPUT:
    ////        - Type : Report type group
    ////
    //// RETURNS: 
    ////        - Int32: Number of columns to include into terminal report depends ReportType
    ////
    public static Int32 NumColumns(ReportType Type)
    {
      Int32 _columns;

      _columns = 0;

      foreach (ReportType _type in Enum.GetValues(typeof(ReportType)))
      {
        if (_type != ReportType.Full)
        {
          switch ((ReportType)(Type & _type))
          {
            case ReportType.Provider:
              _columns += NUM_COLUMNS_PROVIDER;
              break;

            case ReportType.Location:
              _columns += NUM_COLUMNS_LOCATION;
              break;

            default:
              break;
          }
        }
      }

      return _columns;
    }

    public static String ColumnMapName(ReportColumn Column)
    {
      switch (Column)
      {
        case ReportColumn.Provider:
          return MAP_COL_PROVIDER_NAME;

        case ReportColumn.Terminal:
          return MAP_COL_TERMINAL_NAME;

        case ReportColumn.Floor:
          return MAP_COL_FLOOR_ID;

        case ReportColumn.AccountDenomination:
          return MAP_COL_ACCOUNT_DENOMINATION;

        case ReportColumn.Area:
          return MAP_COL_AREA;

        case ReportColumn.Bank:
          return MAP_COL_BANK;

        case ReportColumn.Position:
          return MAP_COL_POSITION;

        default:
          return "";
      }
    }

    #endregion
  }

  public enum ReportType
  {
    Provider = 1,  //2^0
    Location = 2,  //2^1
    Full = 3,      //It must be the sum of the above items
    None = 4
  }

  public enum ReportColumn
  {
    Provider = 1,
    Terminal = 2,
    Floor = 4,
    Area = 8,
    Bank = 16,
    Position = 32,
    AccountDenomination = 64,
  }

  public enum ColumnAlign
  {
    Left = 1,
    Right = 2,
    Center = 3,
    LeftCenter = 4,
    RightCenter = 5,
    CenterCenter = 6,
    LeftBottom = 7,
    RightBottom = 8,
    CenterBottom = 9,
  }

  public struct ColumnSettings
  {
    public Int32 Width;
    public String Header0;
    public String Header1;
    public String MappingName;
    public ColumnAlign Alignment;
    public ReportColumn Column;

    public ColumnSettings(Int32 Width, String Header0, String Header1, ColumnAlign Alignment, String MappingName, ReportColumn Column)
    {
      this.Width = Width;
      this.Header0 = Header0;
      this.Header1 = Header1;
      this.MappingName = MappingName;
      this.Alignment = Alignment;
      this.Column = Column;
    }
  }
}
