//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PaymentOrder.cs
// 
//   DESCRIPTION: Class to save and generate a PaymentOrder document.
// 
//        AUTHOR: Joaquim Cid
// 
// CREATION DATE: 21-JAN-2013
// 
// REVISION HISTORY:
// 
// Date         Author Description
// -----------  ------ ----------------------------------------------------------
// 21-JAN-2013  JCM    First release.
// 08-MAR-2017  FGB    PBI 25268: Third TAX - Payment Voucher
// 09-OCT-2017  JML    PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using WSI.Common;

namespace WSI.Common
{
  public class PaymentOrder
  {
    private const Int32 TEXTWIDTH = 54;
    private const Int32 INDENTED = 21;
    private const String MARGIN = "                     ";
    public const String DOC_TYPE_SEPARATOR = ",";
    public Int64 AccountId;
    public String HolderId1;
    public String HolderId2;
    public String HolderId1Type;
    public String HolderId2Type;
    public String HolderName1;
    public String HolderName2;
    public String HolderName3;
    public String HolderName4;

    public DateTime OrderDate;
    public DateTime ApplicationDate;
    public String EffectiveDays;
    public String PaymentType;

    public Int32 BankId;

    public String BankName;
    public String BankCustomerNumber;
    public String BankAccountNumber;

    public Currency AccountBalance;
    public TYPE_CASH_REDEEM CashRedeem;

    public CurrencyExchangeSubType CheckType;

    public Currency TotalPayment;
    public Currency CashPayment;
    public Currency CheckPayment;

    public Int64 DocumentId;

    public String AuthorizedTitle1;
    public String AuthorizedName1;
    public String AuthorizedTitle2;
    public String AuthorizedName2;

    public String HolderFullName
    {
      get
      {
        return CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, HolderName3, HolderName4, HolderName1, HolderName2);
      }
    }

    public PaymentOrder()
    {
      AccountId = 0;
      HolderId1 = "";
      HolderId2 = "";
      HolderId1Type = "";
      HolderId2Type = "";
      HolderName1 = "";
      HolderName2 = "";
      HolderName3 = "";
      HolderName4 = "";

      OrderDate = DateTime.MinValue;
      ApplicationDate = DateTime.MaxValue;
      EffectiveDays = "";
      PaymentType = "";

      BankId = 0;

      BankName = "";
      BankCustomerNumber = "";
      BankAccountNumber = "";

      AccountBalance = 0;
      CashRedeem = new TYPE_CASH_REDEEM();

      CheckType = CurrencyExchangeSubType.CHECK;

      TotalPayment = 0;
      CashPayment = 0;
      CheckPayment = 0;
      DocumentId = 0;
    }

    public static Boolean IsValidApplicationDate(DateTime ApplyDate, out DateTime DateFrom, out DateTime DateTo)
    {
      DateTime _now;

      _now = WGDB.Now;

      DateFrom = new DateTime(_now.Year, _now.Month, _now.Day);
      DateTo = DateFrom.AddYears(1);

      return (ApplyDate >= DateFrom && ApplyDate <= DateTo);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Payment Order are enabled or dissabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Payment Order are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsEnabled()
    {
      return GeneralParam.GetBoolean("PaymentOrder", "Enabled", false);
    } // IsEnabled

    //------------------------------------------------------------------------------
    // PURPOSE: Generation of Document Payment Order are enabled or dissabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Generation of Document Payment Order are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsGenerateDocumentEnabled(Boolean ForceRefresh)
    {
      Boolean _generate_document;
      GeneralParam.Dictionary _general_params;

      _generate_document = GeneralParam.GetBoolean("PaymentOrder", "GenerateDocument", true);

      if (ForceRefresh)
      {
        _general_params = GeneralParam.Dictionary.Create();

        _generate_document = _general_params.GetBoolean("PaymentOrder", "GenerateDocument", true);
      }

      return IsEnabled() && _generate_document;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generation of Document Payment Order are enabled or dissabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Generation of Document Payment Order are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsGenerateDocumentEnabled()
    {
      return IsEnabled() && GeneralParam.GetBoolean("PaymentOrder", "GenerateDocument", true);
    } // IsGenerateDocumentEnabled

    //------------------------------------------------------------------------------
    // PURPOSE: Get Minimun ampount for create a Payment Order
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Payment Order are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Currency MinAmount()
    {
      return GeneralParam.GetCurrency("PaymentOrder", "MinAmount", 0);
    } // MinAmount

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts a payment order into the Data Base
    // 
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //    TRUE : if everything goes ok
    //   FALSE : if the number of updated rows != 1
    public static bool DB_Save(Int64 OperationId,
                               PaymentOrder OrderPayment,
                               SqlTransaction SqlTx)
    {
      String _sql_str;
      Int32 _num_updated_rows;
      Int32 _gp_update_account;

      try
      {
        // Add the major prize
        _sql_str = " INSERT INTO   ACCOUNT_PAYMENT_ORDERS  ( " +
                   "               APO_OPERATION_ID          " +
                   "             , APO_ACCOUNT_ID            " +
                   "             , APO_DATETIME              " +
                   "             , APO_PLAYER_NAME1          " +
                   "             , APO_PLAYER_NAME2          " +
                   "             , APO_PLAYER_NAME3          " +
                   "             , APO_PLAYER_NAME4          " +
                   "             , APO_PLAYER_DOC_TYPE1      " +
                   "             , APO_PLAYER_DOC_ID1        " +
                   "             , APO_PLAYER_DOC_TYPE2      " +
                   "             , APO_PLAYER_DOC_ID2        " +
                   "             , APO_EFFECTIVE_DAYS        " +
                   "             , APO_APPLICATION_DATE      " +
                   "             , APO_PAYMENT_TYPE          " +
                   "             , APO_BANK_ID               " +
                   "             , APO_BANK_NAME             " +
                   "             , APO_BANK_CUSTOMER_NUMBER  " +
                   "             , APO_BANK_ACCOUNT_NUMBER   " +
                   "             , APO_ACCOUNT_BALANCE       " +
                   "             , APO_RETURN_BALANCE        " +
                   "             , APO_PRIZE                 " +
                   "             , APO_TAX1                  " +
                   "             , APO_TAX2                  " +
                   "             , APO_TAX3                  " +
                   "             , APO_TOTAL_PAYMENT         " +
                   "             , APO_CASH_PAYMENT          " +
                   "             , APO_CHECK_PAYMENT         " +
                   "             , APO_AUTHORIZED_BY_TITLE1  " +
                   "             , APO_AUTHORIZED_BY_NAME1   " +
                   "             , APO_AUTHORIZED_BY_TITLE2  " +
                   "             , APO_AUTHORIZED_BY_NAME2   " +
                   "             , APO_DOCUMENT_ID           " +
                   "             , APO_PAYMENT_SUBTYPE )     " +
                   "     VALUES       (                      " +
                   "               @OperationId              " +
                   "             , @AccountId                " +
                   "             , @Datetime                 " +
                   "             , @PlayerName1              " +
                   "             , @PlayerName2              " +
                   "             , @PlayerName3              " +
                   "             , @PlayerName4              " +
                   "             , @PlayerTypeDoc1           " +
                   "             , @PlayerDocId1             " +
                   "             , @PlayerTypeDoc2           " +
                   "             , @PlayerDocId2             " +
                   "             , @EffectiveDays            " +
                   "             , @ApplicationDate          " +
                   "             , @TypePayment              " +
                   "             , @BankId                   " +
                   "             , @BankName                 " +
                   "             , @BankCustomerNumber       " +
                   "             , @BankAccountNumber        " +
                   "             , @AccountBalance           " +
                   "             , @ReturnBalance            " +
                   "             , @Prize                    " +
                   "             , @Tax1                     " +
                   "             , @Tax2                     " +
                   "             , @Tax3                     " +
                   "             , @TotalPayment             " +
                   "             , @CashPayment              " +
                   "             , @CheckPayment             " +
                   "             , @AuthorizedTitle1         " +
                   "             , @AuthorizedName1          " +
                   "             , @AuthorizedTitle2         " +
                   "             , @AuthorizedName2          " +
                   "             , @DocumentId               " +
                   "             , @pCheckType  )            ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, SqlTx.Connection, SqlTx))
        {
          _sql_cmd.Parameters.Add("@OperationId", SqlDbType.BigInt).Value = OperationId;
          _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = OrderPayment.AccountId;
          _sql_cmd.Parameters.Add("@Datetime", SqlDbType.DateTime).Value = OrderPayment.OrderDate;
          _sql_cmd.Parameters.Add("@PlayerName1", SqlDbType.NVarChar, 50).Value = OrderPayment.HolderName1;
          _sql_cmd.Parameters.Add("@PlayerName2", SqlDbType.NVarChar, 50).Value = OrderPayment.HolderName2;
          _sql_cmd.Parameters.Add("@PlayerName3", SqlDbType.NVarChar, 50).Value = OrderPayment.HolderName3;
          _sql_cmd.Parameters.Add("@PlayerName4", SqlDbType.NVarChar, 50).Value = OrderPayment.HolderName4;
          _sql_cmd.Parameters.Add("@PlayerTypeDoc1", SqlDbType.NVarChar, 20).Value = OrderPayment.HolderId1Type;
          _sql_cmd.Parameters.Add("@PlayerDocId1", SqlDbType.NVarChar, 20).Value = OrderPayment.HolderId1;
          _sql_cmd.Parameters.Add("@PlayerTypeDoc2", SqlDbType.NVarChar, 20).Value = OrderPayment.HolderId2Type == null ? "" : OrderPayment.HolderId2Type;
          _sql_cmd.Parameters.Add("@PlayerDocId2", SqlDbType.NVarChar, 20).Value = OrderPayment.HolderId2 == null ? "" : OrderPayment.HolderId2;
          _sql_cmd.Parameters.Add("@EffectiveDays", SqlDbType.NVarChar).Value = OrderPayment.EffectiveDays;
          _sql_cmd.Parameters.Add("@ApplicationDate", SqlDbType.DateTime).Value = OrderPayment.ApplicationDate;
          _sql_cmd.Parameters.Add("@TypePayment", SqlDbType.NVarChar).Value = OrderPayment.PaymentType;
          _sql_cmd.Parameters.Add("@BankId", SqlDbType.Int).Value = OrderPayment.BankId;
          _sql_cmd.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = OrderPayment.BankName;
          _sql_cmd.Parameters.Add("@BankCustomerNumber", SqlDbType.NVarChar).Value = OrderPayment.BankCustomerNumber;
          _sql_cmd.Parameters.Add("@BankAccountNumber", SqlDbType.NVarChar).Value = OrderPayment.BankAccountNumber;

          _sql_cmd.Parameters.Add("@AccountBalance", SqlDbType.Money).Value = OrderPayment.AccountBalance.SqlMoney;
          _sql_cmd.Parameters.Add("@ReturnBalance", SqlDbType.Money).Value = OrderPayment.CashRedeem.TotalDevolution.SqlMoney;
          _sql_cmd.Parameters.Add("@Prize", SqlDbType.Money).Value = OrderPayment.CashRedeem.prize.SqlMoney;
          _sql_cmd.Parameters.Add("@Tax1", SqlDbType.Money).Value = OrderPayment.CashRedeem.tax1.SqlMoney;
          _sql_cmd.Parameters.Add("@Tax2", SqlDbType.Money).Value = OrderPayment.CashRedeem.tax2.SqlMoney;
          _sql_cmd.Parameters.Add("@Tax3", SqlDbType.Money).Value = OrderPayment.CashRedeem.tax3.SqlMoney;

          _sql_cmd.Parameters.Add("@pCheckType", SqlDbType.Int).Value = OrderPayment.CheckType;
          _sql_cmd.Parameters.Add("@TotalPayment", SqlDbType.Money).Value = OrderPayment.TotalPayment.SqlMoney;
          _sql_cmd.Parameters.Add("@CashPayment", SqlDbType.Money).Value = OrderPayment.CashPayment.SqlMoney;
          _sql_cmd.Parameters.Add("@CheckPayment", SqlDbType.Money).Value = OrderPayment.CheckPayment.SqlMoney;

          _sql_cmd.Parameters.Add("@AuthorizedTitle1", SqlDbType.NVarChar).Value = OrderPayment.AuthorizedTitle1 == null ? "" : OrderPayment.AuthorizedTitle1;
          _sql_cmd.Parameters.Add("@AuthorizedName1", SqlDbType.NVarChar).Value = OrderPayment.AuthorizedName1 == null ? "" : OrderPayment.AuthorizedName1;
          _sql_cmd.Parameters.Add("@AuthorizedTitle2", SqlDbType.NVarChar).Value = OrderPayment.AuthorizedTitle2 == null ? "" : OrderPayment.AuthorizedTitle2;
          _sql_cmd.Parameters.Add("@AuthorizedName2", SqlDbType.NVarChar).Value = OrderPayment.AuthorizedName2 == null ? "" : OrderPayment.AuthorizedName2;

          _sql_cmd.Parameters.Add("@DocumentId", SqlDbType.BigInt).Value = OrderPayment.DocumentId;

          _num_updated_rows = _sql_cmd.ExecuteNonQuery();

          if (_num_updated_rows != 1)
          {
            Log.Error("DB_SavePaymentOrder. Op= " + OperationId.ToString() + " Card=" + OrderPayment.AccountId.ToString());

            return false;
          }
        }

        // Update Account when the account is "personal" and G.P. UpdateAccount is true.
        Int32.TryParse(GeneralParam.Value("PaymentOrder", "UpdateAccount"), out _gp_update_account);
        if (_gp_update_account == 0)
        {
          return true;
        }

        _sql_str = " UPDATE   ACCOUNTS                       "
                 + "   SET   AC_HOLDER_NAME  = @pFullName    "
                 + "       , AC_HOLDER_NAME1 = @pHolderName1 "
                 + "       , AC_HOLDER_NAME2 = @pHolderName2 "
                 + "       , AC_HOLDER_NAME3 = @pHolderName3 "
                 + "       , AC_HOLDER_NAME4 = @pHolderName4 "
                 + " WHERE   AC_ACCOUNT_ID   = @pAccountId   "
                 + "   AND   AC_HOLDER_LEVEL <> 0            ";


        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, SqlTx.Connection, SqlTx))
        {
          _sql_cmd.Parameters.Add("@pFullName", SqlDbType.NVarChar, 200).Value = OrderPayment.HolderFullName;
          _sql_cmd.Parameters.Add("@pHolderName1", SqlDbType.NVarChar, 50).Value = OrderPayment.HolderName1;
          _sql_cmd.Parameters.Add("@pHolderName2", SqlDbType.NVarChar, 50).Value = OrderPayment.HolderName2;
          _sql_cmd.Parameters.Add("@pHolderName3", SqlDbType.NVarChar, 50).Value = OrderPayment.HolderName3;
          _sql_cmd.Parameters.Add("@pHolderName4", SqlDbType.NVarChar, 50).Value = OrderPayment.HolderName4;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = OrderPayment.AccountId;

          // Don't check the numbers of rows updated. Can be 0 if account is anonymous.
          _sql_cmd.ExecuteNonQuery();

        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      finally
      {
      }
    } // DB_Save

    //------------------------------------------------------------------------------
    // PURPOSE : Generates the payment order document related to the operation 
    //           and saves it into ACCOUNT_PAYMENT_ORDERS
    // 
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Id of the operation that identifies the payment order
    //          - SqlTx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    public static bool DB_CreatePaymentOrderDoc(Int64 OperationId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      MemoryStream _ms_pdf;
      String _str;
      String _pdf_filename;
      DocumentList _doc_list;
      Byte[] _logo;

      PaymentOrder _payment_order;

      if (OperationId < 1)
      {
        return false;
      }

      _sb = new StringBuilder();
      _ms_pdf = null;
      _payment_order = new PaymentOrder();
      _doc_list = null;
      _logo = null;

      try
      {
        _sb.AppendLine("SELECT   APO_DATETIME ");
        _sb.AppendLine("       , APO_ACCOUNT_ID ");
        _sb.AppendLine("       , APO_PLAYER_NAME1 ");
        _sb.AppendLine("       , APO_PLAYER_NAME2 ");
        _sb.AppendLine("       , APO_PLAYER_NAME3 ");
        _sb.AppendLine("       , APO_PLAYER_NAME4 ");
        _sb.AppendLine("       , APO_PLAYER_DOC_TYPE1 ");
        _sb.AppendLine("       , APO_PLAYER_DOC_ID1 ");
        _sb.AppendLine("       , APO_PLAYER_DOC_TYPE2 ");
        _sb.AppendLine("       , APO_PLAYER_DOC_ID2 ");
        _sb.AppendLine("       , APO_CHECK_PAYMENT ");
        _sb.AppendLine("       , APO_EFFECTIVE_DAYS ");
        _sb.AppendLine("       , APO_APPLICATION_DATE ");
        _sb.AppendLine("       , APO_PAYMENT_TYPE ");
        _sb.AppendLine("       , APO_BANK_NAME ");
        _sb.AppendLine("       , APO_BANK_CUSTOMER_NUMBER ");
        _sb.AppendLine("       , APO_BANK_ACCOUNT_NUMBER ");
        _sb.AppendLine("       , APO_ACCOUNT_BALANCE ");
        _sb.AppendLine("       , APO_RETURN_BALANCE ");
        _sb.AppendLine("       , APO_PRIZE ");
        _sb.AppendLine("       , APO_TAX1 ");
        _sb.AppendLine("       , APO_TAX2 ");
        _sb.AppendLine("       , APO_TAX3 ");
        _sb.AppendLine("       , APO_TOTAL_PAYMENT ");
        _sb.AppendLine("       , APO_CASH_PAYMENT ");

        _sb.AppendLine("       , APO_AUTHORIZED_BY_TITLE1 ");
        _sb.AppendLine("       , APO_AUTHORIZED_BY_NAME1 ");
        _sb.AppendLine("       , APO_AUTHORIZED_BY_TITLE2 ");
        _sb.AppendLine("       , APO_AUTHORIZED_BY_NAME2 ");
        _sb.AppendLine("       , APO_DOCUMENT_ID ");
        _sb.AppendLine("       , APO_PAYMENT_SUBTYPE ");
        _sb.AppendLine("  FROM   ACCOUNT_PAYMENT_ORDERS ");
        _sb.AppendLine(" WHERE   APO_OPERATION_ID = @pOperationId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pOperationId", System.Data.SqlDbType.BigInt).Value = OperationId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            _payment_order.OrderDate = (DateTime)_reader["APO_DATETIME"];
            _payment_order.AccountId = (Int64)_reader["APO_ACCOUNT_ID"];
            _payment_order.HolderName1 = (String)_reader["APO_PLAYER_NAME1"];
            _payment_order.HolderName2 = (_reader["APO_PLAYER_NAME2"] == DBNull.Value) ? "" : (String)_reader["APO_PLAYER_NAME2"];
            _payment_order.HolderName3 = (_reader["APO_PLAYER_NAME3"] == DBNull.Value) ? "" : (String)_reader["APO_PLAYER_NAME3"];
            _payment_order.HolderName4 = (_reader["APO_PLAYER_NAME4"] == DBNull.Value) ? "" : (String)_reader["APO_PLAYER_NAME4"];
            _payment_order.HolderId1Type = (String)_reader["APO_PLAYER_DOC_TYPE1"];
            _payment_order.HolderId1 = (String)_reader["APO_PLAYER_DOC_ID1"];
            _payment_order.HolderId2Type = (String)_reader["APO_PLAYER_DOC_TYPE2"];
            _payment_order.HolderId2 = (String)_reader["APO_PLAYER_DOC_ID2"];
            _payment_order.CheckPayment = (Decimal)_reader["APO_CHECK_PAYMENT"];
            _payment_order.EffectiveDays = (String)_reader["APO_EFFECTIVE_DAYS"];
            _payment_order.ApplicationDate = (DateTime)_reader["APO_APPLICATION_DATE"];
            _payment_order.PaymentType = (String)_reader["APO_PAYMENT_TYPE"];
            _payment_order.BankName = (_reader["APO_BANK_NAME"] == DBNull.Value) ? "" : (String)_reader["APO_BANK_NAME"];
            _payment_order.BankCustomerNumber = (_reader["APO_BANK_CUSTOMER_NUMBER"] == DBNull.Value) ? "" : (String)_reader["APO_BANK_CUSTOMER_NUMBER"];
            _payment_order.BankAccountNumber = (_reader["APO_BANK_ACCOUNT_NUMBER"] == DBNull.Value) ? "" : (String)_reader["APO_BANK_ACCOUNT_NUMBER"];

            _payment_order.AccountBalance = (Decimal)_reader["APO_ACCOUNT_BALANCE"];
            _payment_order.CashRedeem.devolution = (Decimal)_reader["APO_RETURN_BALANCE"];

            _payment_order.CashRedeem.prize = (Decimal)_reader["APO_PRIZE"];
            _payment_order.CashRedeem.tax1 = (_reader["APO_TAX1"] == DBNull.Value) ? 0 : (Decimal)_reader["APO_TAX1"];
            _payment_order.CashRedeem.tax2 = (_reader["APO_TAX2"] == DBNull.Value) ? 0 : (Decimal)_reader["APO_TAX2"];
            _payment_order.CashRedeem.tax3 = (_reader["APO_TAX3"] == DBNull.Value) ? 0 : (Decimal)_reader["APO_TAX3"];

            //FGB 2017-03-21: Get tax percentage (We get the current values, because we don't know the values when the row was created)
            _payment_order.CashRedeem.tax1_pct = GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.1.Pct") / 100;
            _payment_order.CashRedeem.tax2_pct = GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.2.Pct") / 100;
            _payment_order.CashRedeem.tax3_pct = GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.3.Pct") / 100;

            _payment_order.TotalPayment = (Decimal)_reader["APO_TOTAL_PAYMENT"];
            _payment_order.CashPayment = (Decimal)_reader["APO_CASH_PAYMENT"];

            _payment_order.AuthorizedTitle1 = (_reader["APO_AUTHORIZED_BY_TITLE1"] == DBNull.Value) ? "" : (String)_reader["APO_AUTHORIZED_BY_TITLE1"];
            _payment_order.AuthorizedName1 = (_reader["APO_AUTHORIZED_BY_NAME1"] == DBNull.Value) ? "" : (String)_reader["APO_AUTHORIZED_BY_NAME1"];
            _payment_order.AuthorizedTitle2 = (_reader["APO_AUTHORIZED_BY_TITLE2"] == DBNull.Value) ? "" : (String)_reader["APO_AUTHORIZED_BY_TITLE2"];
            _payment_order.AuthorizedName2 = (_reader["APO_AUTHORIZED_BY_NAME2"] == DBNull.Value) ? "" : (String)_reader["APO_AUTHORIZED_BY_NAME2"];

            _payment_order.DocumentId = (_reader["APO_DOCUMENT_ID"] == DBNull.Value) ? 0 : (Int64)_reader["APO_DOCUMENT_ID"];
            _payment_order.CheckType = (_reader["APO_PAYMENT_SUBTYPE"] == DBNull.Value) ? CurrencyExchangeSubType.CHECK : (CurrencyExchangeSubType)_reader["APO_PAYMENT_SUBTYPE"];
          }
        }

        TableDocuments.Load(DOCUMENT_TYPE.PAYMENT_ORDER_LOGO, out _doc_list, SqlTrx);
        if (_doc_list.Count > 0)
        {
          _logo = _doc_list[0].Content;
        }

        //
        // Create a pdf file 
        //
        if (!CashierDocs.CreateDocumentPaymentOrder(_payment_order, _logo, out _ms_pdf))
        {
          _str = String.Format("CashierDocs.CreateDocumentPaymentOrder OpId={0}", OperationId.ToString());
          Log.Error(_str);

          return false;
        }

        if (_ms_pdf == null)
        {
          _str = String.Format("DB_CreatePaymentOrderDoc.CreateDocument OpId={0}", OperationId.ToString());
          Log.Error(_str);

          return false;
        }

        //Save documents
        _pdf_filename = GeneratePDFFilename(_payment_order);
        _doc_list = new DocumentList();
        _doc_list.Add(new ReadOnlyDocument(_pdf_filename, _ms_pdf));

        if (!TableDocuments.Save(ref _payment_order.DocumentId, DOCUMENT_TYPE.PAYMENT_ORDER, _doc_list, SqlTrx))
        {
          return false;
        }

        _sb.Length = 0;
        _sb.AppendLine("UPDATE   ACCOUNT_PAYMENT_ORDERS ");
        _sb.AppendLine("   SET   APO_DOCUMENT_ID = @pDocumentId ");
        _sb.AppendLine(" WHERE   APO_OPERATION_ID = @pOperationId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pOperationId", System.Data.SqlDbType.BigInt).Value = OperationId;
          _cmd.Parameters.Add("@pDocumentId", System.Data.SqlDbType.BigInt).Value = _payment_order.DocumentId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("DB_CreatePaymentOrderDoc.UPDATE OpId=" + OperationId.ToString());

            return false;
          }
        }

        return true;
      }
      catch
      {
      }
      finally
      {
        if (_ms_pdf != null && _ms_pdf.CanRead)
        {
          _ms_pdf.Close();
        }
      }

      return false;

    } // DB_CreatePaymentOrderDoc

    /// <summary>
    /// Generates filename for PDF
    /// </summary>
    /// <param name="paymentOrder"></param>
    /// <returns></returns>
    private static String GeneratePDFFilename(PaymentOrder paymentOrder)
    {
      String _base_name;

      _base_name = "PaymentOrder_" + paymentOrder.AccountId.ToString() + "-" + paymentOrder.OrderDate.Year.ToString() + paymentOrder.OrderDate.Month.ToString("00") + paymentOrder.OrderDate.Day.ToString("00");

      return String.Format("{0}.PDF", _base_name);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the bank accounts
    // 
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //    TRUE : if everything goes ok
    //   FALSE : if throws an exception
    public static Boolean BanksAccounts(Boolean OnlyEnabled, out DataTable BankAccountInfo, SqlTransaction Trx)
    {
      //DataTable _table;
      String _sql;
      BankAccountInfo = new DataTable();

      try
      {
        _sql = " SELECT   BA_ACCOUNT_ID	 " +
               "        , BA_BANK_NAME " +
               "        , BA_DESCRIPTION " +
               "        , BA_EFFECTIVE_DAYS " +
               "        , BA_CUSTOMER_NUMBER " +
               "        , BA_ACCOUNT_NUMBER " +
               "        , BA_METHOD_PAYMENT " +
               "   FROM   BANK_ACCOUNTS  ";
        if (OnlyEnabled)
        {
          _sql += " WHERE BA_ENABLED	= 1 ";
        }

        using (SqlCommand _cmd = new SqlCommand(_sql, Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _da.Fill(BankAccountInfo);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // BanksAccounts

    //------------------------------------------------------------------------------
    // PURPOSE : Set an specific format to the text supplied
    // 
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          the formatted string
    public static String FormatText(String Text)
    {
      String _new_string;
      String[] _words;
      Int32 _total_len;

      _new_string = "";
      _total_len = 0;

      _words = Text.Trim().Split(' ');
      foreach (String _word in _words)
      {
        if (_word.Length + _total_len == TEXTWIDTH)//if the new word(_word) fills the line, the blank space is not added
        {
          _new_string += _word;
          _total_len += _word.Length;
        }

        else
          if (_word.Length + _total_len < TEXTWIDTH)
          {
            _new_string += _word + " ";
            _total_len += _word.Length + 1;
          }

          else
          {
            _new_string += Environment.NewLine + MARGIN + _word + ' ';//agregarlinea
            _total_len = _word.Length;
          }
      }

      return _new_string;
    } //FormatText
  }
}
