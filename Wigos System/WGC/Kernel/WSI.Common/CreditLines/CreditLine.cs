﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CreditLines.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Credit lines
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 16-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAR-2017 ETP    First release (Header).
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

namespace WSI.Common.CreditLines
{
  #region " Enum "

  public enum StatusDefinition
  {
    None = 0,
    PendingApproval = 1,
    Approved = 2,
    NotApproved = 3,
    Suspended = 4,
    Expired = 5,
    All = 99
  }

  public enum ResultGetMarker
  {
    OK = 0,
    NOT_APPROVED = 1,
    NOT_SUFICIENT_AMOUNT_AVAIBLE = 2,
    NEED_MORE_PERMISSIONS = 3,
    KO = 4
  }

  #endregion " Enum "

  public class CreditLine
  {
    #region " Properties "

    public Int64 Id { get; set; }
    public Int64 AccountId { get; set; }
    public StatusDefinition Status { get; set; }
    public Currency ExtensionAmount { get; set; } //TTO
    public Currency LimitAmount { get; set; } //Base Credit Amount
    public Currency SpentAmount { get; set; }
    public Boolean Exists { get; set; }

    public Currency AvailableTTOAmount
    {
      get { return LimitAmount + ExtensionAmount - SpentAmount; }
    }

    public Currency AvailableAmountNoTTO
    {
      get { return LimitAmount - SpentAmount; }
    }

    public Currency TotalAmount // Base Credit And TTO
    {
      get { return LimitAmount + ExtensionAmount; }
    }

    public Currency AvailableAmount
    {
      get { return LimitAmount - SpentAmount; }
    }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Get Credit Line by account Id.
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_GetByAccountId(Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      this.LimitAmount = 0;
      this.ExtensionAmount = 0;
      this.SpentAmount = 0;
      this.Status = 0;
      this.Exists = false;

      if (!GeneralParam.GetBoolean("CreditLine", "Enabled", false))
      {
        return true;
      }
      try
      {
        // New Ticket: unimplemented. Not used at the moment
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   CL_ID                       ");
        _sb.AppendLine("           , CL_LIMIT_AMOUNT             ");
        _sb.AppendLine("           , CL_TTO_AMOUNT               ");
        _sb.AppendLine("           , CL_SPENT_AMOUNT             ");
        _sb.AppendLine("           , CL_STATUS                   ");
        _sb.AppendLine("      FROM   CREDIT_LINES                ");
        _sb.AppendLine("     WHERE   CL_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              this.AccountId = AccountId;
              this.Id = (Int64)_reader["CL_ID"];
              this.LimitAmount = (Decimal)_reader["CL_LIMIT_AMOUNT"];
              this.ExtensionAmount = (Decimal)_reader["CL_TTO_AMOUNT"];
              this.SpentAmount = (Decimal)_reader["CL_SPENT_AMOUNT"];
              this.Status = (StatusDefinition)_reader["CL_STATUS"];
              this.Exists = true;

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CreditLines.DB_GetByAccountId -> AccountId: {0}", AccountId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetByAccountId

    /// <summary>
    /// Update spent and maybe last payback of credit line
    /// </summary>
    /// <param name="TypeMovement"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_Update(CreditLineMovements.CreditLineMovementType TypeMovement, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    UPDATE   CREDIT_LINES                         ");
        _sb.AppendLine("       SET   CL_SPENT_AMOUNT = @pSpentAmount      ");
        _sb.AppendLine("           , CL_UPDATE       =  @pCurrentDateTime ");
       
        if (TypeMovement == CreditLineMovements.CreditLineMovementType.Payback)
        {
          _sb.AppendLine("        , CL_LAST_PAYBACK = @pCurrentDateTime ");
        }

        _sb.AppendLine("     WHERE   CL_ACCOUNT_ID   = @pAccountId      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = this.AccountId;
          _cmd.Parameters.Add("@pSpentAmount", SqlDbType.Money).Value = this.SpentAmount.SqlMoney;
          _cmd.Parameters.Add("@pCurrentDateTime", SqlDbType.DateTime).Value = WGDB.Now;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CreditLines.DB_Update -> AccountId: {0}", this.AccountId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Update

    /// <summary>
    /// Update spent
    /// </summary>
    /// <param name="Amount"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean UpdateSpent(Currency Amount, SqlTransaction SqlTrx)
    {
      this.SpentAmount = Amount;
      return this.DB_Update(CreditLines.CreditLineMovements.CreditLineMovementType.UpdateSpent, SqlTrx);
    } // UpdateSpent

    /// <summary>
    /// Check if there are movements of credit line
    /// </summary>
    /// <param name="TableMovements"></param>
    /// <returns></returns>
    public Boolean HasGetMarker(DataTable TableMovements)
    {
      return (TableMovements.Select(string.Format(" AM_TYPE = '{0}' ", (Int32)MovementType.CreditLineGetMarker)).Length > 0);
    } // HasGetMarker

    /// <summary>
    /// Get a marker
    /// </summary>
    /// <param name="Amount"></param>
    /// <returns></returns>
    public ResultGetMarker CheckGetMarker(Currency Amount)
    {

      //Check status CredtiLine is approved
      if (Status != StatusDefinition.Approved)
      {
        return ResultGetMarker.NOT_APPROVED;
      }

      //Check available amount
      if (AvailableAmount < Amount)
      {
        //Check available amount with TTO - need specific permission
        if (AvailableTTOAmount < Amount)
        {
          return ResultGetMarker.NOT_SUFICIENT_AMOUNT_AVAIBLE;
        }
        else
        {
          return ResultGetMarker.NEED_MORE_PERMISSIONS;
        }
      }

      return ResultGetMarker.OK;
    } // CheckGetMarker

    /// <summary>
    /// Clone object
    /// </summary>
    /// <returns></returns>
    public CreditLine Clone()
    {
      CreditLine _credit_line;
      _credit_line = new CreditLine();

      _credit_line.Id = Id;
      _credit_line.AccountId = AccountId;
      _credit_line.Status = Status;
      _credit_line.LimitAmount = LimitAmount;
      _credit_line.ExtensionAmount = ExtensionAmount;
      _credit_line.SpentAmount = SpentAmount;
      _credit_line.Exists = Exists;

      return _credit_line;
    } // Clone


    /// <summary>
    /// Override To String for Audit.
    /// </summary>
    /// <returns></returns>
    public override String ToString()
    {
      return SpentAmount.ToString();
    } // ToString

    /// <summary>
    /// Override Equals for audit.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(System.Object obj)
    {
      // If parameter is null return false.
      if (obj == null)
      {
        return false;
      }

      // If parameter cannot be cast to CreditLine return false.
      CreditLine p = obj as CreditLine;
      if ((System.Object)p == null)
      {
        return false;
      }

      // Return true if the fields match:
      return (Id == p.Id) && (AccountId == p.AccountId) && (Exists == p.Exists) && (Status == p.Status) && SpentAmount == p.SpentAmount && LimitAmount == p.LimitAmount && ExtensionAmount == p.ExtensionAmount;
    } // Equals

    /// <summary>
    /// Override Equals for audit.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return base.GetHashCode();
    } // GetHashCode

    #region " Static Methods "

    /// <summary>
    /// Get number of signers of credit line
    /// </summary>
    /// <returns></returns>
    public static Int32 GetNumSigners()
    {
      return GeneralParam.GetInt32("CreditLine", "NumSigners", 0);
    } // GetNumSigners

    /// <summary>
    /// Return enum to String of the enum
    /// </summary>
    /// <returns>Staus</returns>
    public static String StatusToString(StatusDefinition Status)
    {
      String _status;
      _status = "";

      switch (Status)
      {
        case StatusDefinition.Approved:
          _status = Resource.String("STR_CREDIT_LINE_APPROVED");
          break;

        case StatusDefinition.Expired:
          _status = Resource.String("STR_CREDIT_LINE_EXPIRED");
          break;

        case StatusDefinition.NotApproved:
          _status = Resource.String("STR_CREDIT_LINE_NOT_APPROVED");
          break;

        case StatusDefinition.PendingApproval:
          _status = Resource.String("STR_CREDIT_LINE_PENDING_APPROVED");
          break;

        case StatusDefinition.Suspended:
          _status = Resource.String("STR_CREDIT_LINE_SUSPENDED");
          break;

        default:
          _status = "Not defined";
          Log.Warning("Status not Defined");
          break;
      }

      return _status;
    } // StatusToString

    /// <summary>
    /// Return xml for credit line movement
    /// </summary>
    /// <param name="TypeMovement"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    public static String GetXMLMovementCreditLine(CreditLineMovements.CreditLineMovementType TypeMovement, String Value)
    {

      XmlDocument xmlDoc = new XmlDocument();
      XmlElement rootNode;

      try
      {
        rootNode = xmlDoc.CreateElement("creditLine");
        xmlDoc.AppendChild(rootNode);

        switch (TypeMovement)
        {
          case CreditLineMovements.CreditLineMovementType.GetMarker:
          case CreditLineMovements.CreditLineMovementType.GetMarkerCardReplacement:
          case CreditLineMovements.CreditLineMovementType.GetMarkerChips:
            AddChildNode(xmlDoc, "getMarker", Value);
            break;
          case CreditLineMovements.CreditLineMovementType.Payback:
            AddChildNode(xmlDoc, "payback", Value);
            break;
        }

        return xmlDoc.OuterXml;
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return String.Empty;

    } // GetXMLChangeCreditLine

    /// <summary>
    /// Create movement for credit line
    /// </summary>
    /// <param name="CardData"></param>
    /// <param name="AddSubAmount"></param>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="OperationId"></param>
    /// <returns></returns>
    public static Boolean DoMovementsCreditLine(CardData CardData, Currency AddSubAmount, CashierSessionInfo CashierSessionInfo, 
                                                CreditLineMovements.CreditLineMovementType TypeMovement, CurrencyExchangeResult CurrencyResult, 
                                                SqlTransaction Trx, out Int64 OperationId)
    {

      CreditLineActions _credit_line_actions;

      OperationId = 0;

      try
      {

        if (!CreateCreditLineActions(CardData, AddSubAmount, CashierSessionInfo, TypeMovement, CurrencyResult, out _credit_line_actions))
        {
          return false;
        }

        if (CreditLinesBusinessLogic.CreateMovements(_credit_line_actions, CashierSessionInfo, Trx, out OperationId))
        {

          switch (TypeMovement)
          {
            case CreditLineMovements.CreditLineMovementType.GetMarker:
            case CreditLineMovements.CreditLineMovementType.GetMarkerChips:
            case CreditLineMovements.CreditLineMovementType.GetMarkerCardReplacement:
              CardData.CreditLineData.SpentAmount = CardData.CreditLineData.SpentAmount + AddSubAmount.Decimal;
              break;
            case CreditLineMovements.CreditLineMovementType.Payback:
              CardData.CreditLineData.SpentAmount = CardData.CreditLineData.SpentAmount - AddSubAmount.Decimal;
              break;
          }

          if (!CardData.CreditLineData.DB_Update(TypeMovement, Trx))
          {
            return false;
          }

          return true;

        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CreditLines.DoMovementsCreditLine -> AccountId: {0}", CardData.AccountId));
        Log.Exception(_ex);
      }

      return false;
    } // DoMovementsCreditLine

    /// <summary>
    /// Create credit lines actions
    /// </summary>
    /// <param name="CardData"></param>
    /// <param name="AddSubAmount"></param>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="TypeMovement"></param>
    /// <param name="CurrencyResult"></param>
    /// <param name="_credit_line_actions"></param>
    /// <returns></returns>
    public static Boolean CreateCreditLineActions(CardData CardData, Currency AddSubAmount, CashierSessionInfo CashierSessionInfo, 
                                                  CreditLineMovements.CreditLineMovementType TypeMovement, CurrencyExchangeResult CurrencyResult, 
                                                  out CreditLineActions _credit_line_actions)
    {
      CreditLineAction _credit_line_action;
       
        _credit_line_actions = new CreditLineActions();

      try
      {                
        _credit_line_action = new CreditLineAction();

        _credit_line_action.CreationUser = CashierSessionInfo.UserName;
        _credit_line_action.MovementType = TypeMovement;
        _credit_line_action.OldValue = String.Empty;
        _credit_line_action.NewValue = CreditLine.GetXMLMovementCreditLine(TypeMovement, String.Format("{0:0.00}", AddSubAmount.Decimal));

        _credit_line_actions.Actions = new List<CreditLineAction>();
        _credit_line_actions.Actions.Add(_credit_line_action);
        _credit_line_actions.AccountId = CardData.AccountId;

        switch (TypeMovement)
        {
          case CreditLineMovements.CreditLineMovementType.GetMarker:
            _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.GetMarker;
            _credit_line_actions.AvailableAmount = CardData.AccountBalance.TotalBalance;
            _credit_line_actions.FinalAmount = CardData.AccountBalance.TotalBalance + AddSubAmount.Decimal;

            break;
          case CreditLineMovements.CreditLineMovementType.GetMarkerChips:
            _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.GetMarkerChips;
            _credit_line_actions.AvailableAmount = CardData.AccountBalance.TotalBalance;
            _credit_line_actions.FinalAmount = CardData.AccountBalance.TotalBalance + AddSubAmount.Decimal;

            break;
          case CreditLineMovements.CreditLineMovementType.GetMarkerCardReplacement:
            _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.GetMarkerCardReplacement;
            _credit_line_actions.AvailableAmount = CardData.AccountBalance.TotalBalance;
            _credit_line_actions.FinalAmount = CardData.AccountBalance.TotalBalance + AddSubAmount.Decimal;

            break;
          case CreditLineMovements.CreditLineMovementType.Payback:
            _credit_line_actions.Action = CreditLinesBusinessLogic.ActionType.Payback;
            _credit_line_actions.AvailableAmount = CardData.CreditLineData.SpentAmount;
            _credit_line_actions.FinalAmount = CardData.CreditLineData.SpentAmount - AddSubAmount.Decimal;
            _credit_line_actions.CurrencyExchange = CurrencyResult;
            break;
        }        
        _credit_line_actions.AddSubAmount = AddSubAmount.Decimal;
        _credit_line_actions.CardTrackData = CardData.TrackData;
        _credit_line_actions.CreditLineId = CardData.CreditLineData.Id;

            return true;
          
        }
      catch (Exception _ex)
      {
        Log.Error("CreditLines.CreateCreditLineActions");
        Log.Exception(_ex);
      }

      return false;

    } // CreateCreditLineActions

    /// <summary>
    /// Expire credit lines by expiration date and status
    /// </summary>
    /// <returns></returns>
    public static Boolean ExpireApprovedCreditLines()
    {
      StringBuilder _sb;

      try
      {
        // Enabled functionality
        if (!GeneralParam.GetBoolean("CreditLine", "Enabled", false))
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("    UPDATE   CREDIT_LINES                   ");
        _sb.AppendLine("       SET   CL_STATUS  = @pStatusExpired   ");
        _sb.AppendLine("           , CL_UPDATE  = @pCurrentDateTime ");
        _sb.AppendLine("     WHERE   CL_STATUS  = @pStatusApproved  ");
        _sb.AppendLine("       AND   CL_EXPIRED < @pCurrentDateTime ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pStatusExpired", SqlDbType.Int).Value = StatusDefinition.Expired;
            _cmd.Parameters.Add("@pCurrentDateTime", SqlDbType.DateTime).Value = WGDB.Now;
            _cmd.Parameters.Add("@pStatusApproved", SqlDbType.Int).Value = StatusDefinition.Approved;

            _cmd.ExecuteNonQuery();

            _db_trx.Commit();
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("CreditLines.ExpireApprovedCreditLines");
        Log.Exception(_ex);
      }

      return false;
    } // ExpireApprovedCreditLines

    #endregion " Static Methods "

    #endregion " Public Methods "

    #region " Private Methods "
    /// <summary>
    /// Add child in xml for credit line movement
    /// </summary>
    /// <param name="xml"></param>
    /// <param name="nodeName"></param>
    /// <param name="value"></param>
    private static void AddChildNode(XmlDocument xml, string nodeName, string value)
    {
      XmlNode rootNode;
      XmlNode _element;


      try
      {
        rootNode = xml.SelectSingleNode("//creditLine");
        _element = xml.CreateElement(nodeName);
        rootNode.AppendChild(_element);
        _element.AppendChild(xml.CreateTextNode(value));

      }
      catch (Exception _ex)
      {
        throw _ex;
      }

    } // AddChildNode
    #endregion

  }
}
