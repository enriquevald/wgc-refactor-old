﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CreditLinesBusinessLogic.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Credit lines
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 15-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-MAR-2017 AMF    First release (Header).
// 21-NOV-2017 RAB    Bug 30826:WIGOS-6607 Credit lines: missing vouchers related to card substitution using Credit lines
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

namespace WSI.Common.CreditLines
{
  public class CreditLinesBusinessLogic
  {
    #region " Properties "

    #endregion " Properties "

    #region " Enums "

    public enum ActionType
    {
      CreateCreditLine,
      UpdateCreditLine,
      ApprovedCreditLine,
      NotApprovedCreditLine,
      SuspendedCreditLine,
      ExpiredCreditLine,
      GetMarker,
      Payback,
      GetMarkerCardReplacement,
      GetMarkerChips,
    }

    #endregion " Enums "

    #region " Public Methods "

    /// <summary>
    /// Action type to string
    /// </summary>
    /// <param name="Action"></param>
    /// <returns></returns>
    public static string ActionToString(ActionType Action)
    {
      String _action_str;
      _action_str = String.Empty;
      switch (Action)
      {
        case ActionType.CreateCreditLine:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_CREATION");
          break;

        case ActionType.UpdateCreditLine:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_UPDATE");
          break;

        case ActionType.ApprovedCreditLine:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_APPROVED");
          break;

        case ActionType.NotApprovedCreditLine:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_NOT_APPROVED");
          break;

        case ActionType.SuspendedCreditLine:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_SUSPEND");
          break;

        case ActionType.ExpiredCreditLine:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_EXPIRE");
          break;

        case ActionType.GetMarker:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_GET_MARKER");
          break;

        case ActionType.Payback:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_PAYBACK");
          break;

        case ActionType.GetMarkerCardReplacement:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_CARD_REPLACEMENT");
          break;
        default:
          Log.Warning("ActionToString. Not defined");
          _action_str = String.Format("Not Defined ({0})", Action.ToString());
          break;
      }

      return _action_str;
    } // ActionToString

    /// <summary>
    /// Action enum to string
    /// </summary>
    /// <returns></returns>
    public static Array ActionEnumToList()
    {
      return ActionType.GetValues(typeof(ActionType));
    } // ActionEnumToList

    /// <summary>
    /// Create account, cashier and credit line movements
    /// </summary>
    /// <param name="Actions"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean CreateMovements(CreditLineActions Actions, SqlTransaction Trx)
    {
      CashierSessionInfo _cashier_session;
      Int64 _operation_id;

      if (!GetRelatedCashierSessionInfo(out _cashier_session))
      {
        return false;
      }

      return CreateMovements(Actions, _cashier_session, Trx, out _operation_id);
    } // CreateMovements

    /// <summary>
    /// Create account, cashier and credit line movements
    /// </summary>
    /// <param name="Actions"></param>
    /// <param name="CashierSession"></param>
    /// <param name="Trx"></param>
    /// <param name="OperationId"></param>
    /// <returns></returns>
    public static Boolean CreateMovements(CreditLineActions Actions, CashierSessionInfo CashierSession, SqlTransaction Trx, out Int64 OperationId)
    {

      OperationId = 0;

      if (!InsertCreditLineOperation(Actions.AccountId, CashierSession, Actions.AddSubAmount, Actions.Action, Trx, out OperationId))
      {
        return false;
      }

      // Only Marker and Payback have cashier movements
      if (Actions.Action == ActionType.GetMarker || Actions.Action == ActionType.GetMarkerChips || Actions.Action == ActionType.GetMarkerCardReplacement || Actions.Action == ActionType.Payback)
      {
        if (!CreateCashierMovementsByActionType(Actions, CashierSession, OperationId, Trx))
        {
          return false;
        }
      }

      // All have account and credit line movements
      if (!CreateAccountMovementsByActionType(Actions, CashierSession, OperationId, Trx))
      {
        return false;
      }

      if (!CreateCreditLineMovementsByActionType(Actions, OperationId, Trx))
      {
        return false;
      }

      return true;
    } // CreateMovements

    /// <summary>
    /// Create credit line movements by action type
    /// </summary>
    /// <param name="Actions"></param>
    /// <param name="OperationId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean CreateCreditLineMovementsByActionType(CreditLineActions Actions, Int64 OperationId, SqlTransaction Trx)
    {

      foreach (CreditLineAction Movement in Actions.Actions)
      {
        if (!CreditLineMovements.CreateCreditLineMovement(Movement, Actions.CreditLineId, OperationId, Trx))
        {
          return false;
        }
      }

      return true;
    } // CreateCreditLineMovementsByActionType
    
    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Get related CashierSessionInfo for movements created from GUI
    /// </summary>
    /// <param name="CashierSession"></param>
    /// <returns></returns>
    private static Boolean GetRelatedCashierSessionInfo(out CashierSessionInfo CashierSession)
    {
      CashierSession = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);

      if (CashierSession == null)
      {
        return false;
      }

      return true;
    } // GetRelatedCashierSessionInfo

    /// <summary>
    /// Insert credit line operation to related to the movements
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="CashierSession"></param>
    /// <param name="Trx"></param>
    /// <param name="OperationId"></param>
    /// <returns></returns>
    private static Boolean InsertCreditLineOperation(Int64 AccountId, CashierSessionInfo CashierSession, Currency Amount, CreditLinesBusinessLogic.ActionType ActionType, SqlTransaction Trx, out Int64 OperationId)
    {

      OperationCode _operation_code;

      switch (ActionType)
      {
        case ActionType.GetMarker:
          _operation_code = OperationCode.CASH_IN;
          break;
        case ActionType.GetMarkerChips:
          _operation_code = OperationCode.CHIPS_SALE_WITH_RECHARGE;
          break;
        case ActionType.GetMarkerCardReplacement:
          _operation_code = OperationCode.CARD_REPLACEMENT;
          break;
        default:
          _operation_code = OperationCode.CREDIT_LINE;
          break;
      }

      if (!Operations.DB_InsertOperation(_operation_code,                 // Operation Code
                                         AccountId,                       // AccountId
                                         CashierSession.CashierSessionId, // CashierSessionId
                                         0,                               // MbAccountId
                                         Amount,                          // Amount
                                         0,                               // PromotionId
                                         0,                               // NonRedeemable
                                         0,                               // NonRedeemableWonLock
                                         0,                               // OperationData 
                                         0,                               // AccountOperationReasonId
                                         String.Empty,                    // Reason
                                         out OperationId,                 // OperationId
                                         Trx))
      {
        return false;
      }

      return true;
    } // InsertCreditLineOperation

    /// <summary>
    /// Create account movements by action type
    /// </summary>
    /// <param name="Actions"></param>
    /// <param name="CashierSession"></param>
    /// <param name="OperationId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean CreateAccountMovementsByActionType(CreditLineActions Actions, CashierSessionInfo CashierSession, Int64 OperationId, SqlTransaction Trx)
    {
      AccountMovementsTable _account_movements_table;

      _account_movements_table = new AccountMovementsTable(CashierSession);

      switch (Actions.Action)
      {
        case ActionType.CreateCreditLine:
          _account_movements_table.Add(OperationId, Actions.AccountId, MovementType.CreditLineCreate, 0, 0, Actions.AddSubAmount, Actions.AddSubAmount);
          break;

        case ActionType.UpdateCreditLine:
          _account_movements_table.Add(OperationId, Actions.AccountId, MovementType.CreditLineUpdate, 0, 0, 0, 0);
          break;

        case ActionType.ApprovedCreditLine:
          _account_movements_table.Add(OperationId, Actions.AccountId, MovementType.CreditLineApproved, 0, 0, 0, 0);
          break;

        case ActionType.NotApprovedCreditLine:
          _account_movements_table.Add(OperationId, Actions.AccountId, MovementType.CreditLineNotApproved, 0, 0, 0, 0);
          break;

        case ActionType.SuspendedCreditLine:
          _account_movements_table.Add(OperationId, Actions.AccountId, MovementType.CreditLineSuspended, 0, 0, 0, 0);
          break;

        case ActionType.ExpiredCreditLine:
          _account_movements_table.Add(OperationId, Actions.AccountId, MovementType.CreditLineExpired, 0, 0, 0, 0);
          break;

        case ActionType.GetMarker:
        case ActionType.GetMarkerChips:
          _account_movements_table.Add(OperationId, Actions.AccountId, MovementType.CreditLineGetMarker, Actions.AvailableAmount, 0, Actions.AddSubAmount, Actions.FinalAmount);
          break;

        case ActionType.GetMarkerCardReplacement:
          _account_movements_table.Add(OperationId, Actions.AccountId, MovementType.CreditLineGetMarkerCardReplacement, Actions.AvailableAmount, 0, Actions.AddSubAmount, Actions.AvailableAmount);
          break;

        case ActionType.Payback:
          _account_movements_table.Add(OperationId, Actions.AccountId, MovementType.CreditLinePayback, Actions.AvailableAmount, Actions.AddSubAmount, 0, Actions.FinalAmount);
          break;
      }

      if (!_account_movements_table.Save(Trx))
      {
        return false;
      }

      return true;
    } // CreateAccountMovementsByActionType

    /// <summary>
    /// Create cashier movements by action type
    /// </summary>
    /// <param name="Actions"></param>
    /// <param name="CashierSession"></param>
    /// <param name="OperationId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean CreateCashierMovementsByActionType(CreditLineActions Actions, CashierSessionInfo CashierSession, Int64 OperationId, SqlTransaction Trx)
    {
      CashierMovementsTable _cashier_movements_table;

      _cashier_movements_table = new CashierMovementsTable(CashierSession);

      switch (Actions.Action)
      {
        case ActionType.GetMarker:
        case ActionType.GetMarkerChips:
          _cashier_movements_table.Add(OperationId, CASHIER_MOVEMENT.CREDIT_LINE_MARKER, Actions.AddSubAmount, Actions.AccountId, Actions.CardTrackData);
          break;

        case ActionType.GetMarkerCardReplacement:
          _cashier_movements_table.Add(OperationId, CASHIER_MOVEMENT.CREDIT_LINE_MARKER_CARD_REPLACEMENT, Actions.AddSubAmount, Actions.AccountId, Actions.CardTrackData);
          break;

        case ActionType.Payback:
          _cashier_movements_table.Add(OperationId, CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK, Actions.AddSubAmount, Actions.AccountId, Actions.CardTrackData);
          if (Actions.CurrencyExchange.InType != CurrencyExchangeType.CURRENCY  || Actions.CurrencyExchange.InCurrencyCode != CurrencyExchange.GetNationalCurrency())
          {
            _cashier_movements_table.AddExchangeCreditLine(OperationId, Actions.AccountId, Actions.CardTrackData, Actions.CurrencyExchange, Actions.CurrencyExchange.InAmount);
          }
          break;
      }

      if (!_cashier_movements_table.Save(Trx))
      {
        return false;
      }

      return true;
    } // CreateCashierMovementsByActionType
    
    #endregion " Private Methods "

  } // CreditLinesBusinessLogic

  public class CreditLineActions
  {
    #region " Properties "

    public CreditLinesBusinessLogic.ActionType Action { get; set; }
    public List<CreditLineAction> Actions { get; set; }
    public Int64 CreditLineId { get; set; }
    public Int64 AccountId { get; set; }
    public String CardTrackData { get; set; }
    public Decimal AvailableAmount { get; set; }
    public Decimal FinalAmount { get; set; }
    public Decimal AddSubAmount { get; set; }
    public CurrencyExchangeResult CurrencyExchange { get; set; }

    #endregion " Properties "

  } // CreditLineActions

  public class CreditLineAction
  {
    #region " Properties "

    public CreditLineMovements.CreditLineMovementType MovementType { get; set; }
    public String OldValue { get; set; }
    public String NewValue { get; set; }
    public String CreationUser { get; set; }

    #endregion " Properties "

  } // CreditLineAction

} // CreditLines
