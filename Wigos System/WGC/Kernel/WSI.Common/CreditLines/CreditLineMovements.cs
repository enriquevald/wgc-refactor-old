﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CreditLineMovements.cs
// 
//   DESCRIPTION: Credit lines movements
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 16-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAR-2017 AMF    First release (Header).
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.CreditLines
{
  public class CreditLineMovements
  {
    #region " Enums "

    public enum CreditLineMovementType
    {
      Create = 0,
      UpdateLimit = 1,
      UpdateExpiration = 2,
      UpdateTTO = 3,
      UpdateIBAN = 4,
      UpdateSigners = 5,
      UpdateStatus = 6,
      GetMarker = 7,
      Payback = 8,
      GetMarkerCardReplacement = 9,
      UpdateSpent = 10,
      GetMarkerChips = 11
    }

    #endregion " Enums "

    #region " Public Methods "

    /// <summary>
    /// Movement type to string
    /// </summary>
    /// <param name="MoventType"></param>
    /// <returns></returns>
    public static string MovementTypeToString(CreditLineMovementType MoventType)
    {
      String _action_str;
      _action_str = String.Empty;
      switch (MoventType)
      {
        case CreditLineMovementType.Create:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_CREATION");
          break;

        case CreditLineMovementType.UpdateLimit:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_UPDATE_LIMIT");
          break;

        case CreditLineMovementType.UpdateExpiration:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_UPDATE_EXPIRATION");
          break;

        case CreditLineMovementType.UpdateTTO:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_UPDATE_TTO");
          break;

        case CreditLineMovementType.UpdateIBAN:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_UPDATE_IBAN");
          break;

        case CreditLineMovementType.UpdateSigners:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_UPDATE_SIGNERS");
          break;

        case CreditLineMovementType.UpdateStatus:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_UPDATE_STATUS");
          break;
        case CreditLineMovementType.GetMarker:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_GET_MARKER");
          break;
        case CreditLineMovementType.Payback:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_PAYBACK");
          break;

        case CreditLineMovementType.GetMarkerCardReplacement:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_CARD_REPLACEMENT");
          break;
        case CreditLineMovementType.GetMarkerChips:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_CHIPS_MARK");
          break;
        case CreditLineMovementType.UpdateSpent:
          _action_str = Resource.String("STR_FRM_ACTION_CREDITLINE_SPENT");
          break;
        default:
          Log.Warning("MovementtypeToString. Not defined");
          _action_str = String.Format("Not Defined ({0})", MoventType.ToString());
          break;
      }

      return _action_str;
    } // MovementTypeToString

    /// <summary>
    /// List of movement type
    /// </summary>
    /// <returns></returns>
    public static Array MovementTypeEnumToList()
    {
      return CreditLineMovementType.GetValues(typeof(CreditLineMovementType));
    } // MovementTypeEnumToList

    /// <summary>
    /// Insert credit line movements
    /// </summary>
    /// <param name="Movement"></param>
    /// <param name="CreditLineId"></param>
    /// <param name="OperationId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean CreateCreditLineMovement(CreditLineAction Action, Int64 CreditLineId, Int64 OperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   CREDIT_LINE_MOVEMENTS ");
        _sb.AppendLine("             ( CLM_CREDIT_LINE_ID    ");
        _sb.AppendLine("             , CLM_OPERATION_ID      ");
        _sb.AppendLine("             , CLM_TYPE              ");
        _sb.AppendLine("             , CLM_OLD_VALUE         ");
        _sb.AppendLine("             , CLM_NEW_VALUE         ");
        _sb.AppendLine("             , CLM_CREATION          ");
        _sb.AppendLine("             , CLM_CREATION_USER )   ");
        _sb.AppendLine("      VALUES ( @pCreditLineId        ");
        _sb.AppendLine("             , @pOperationId         ");
        _sb.AppendLine("             , @pType                ");
        _sb.AppendLine("             , @pOldValue            ");
        _sb.AppendLine("             , @pNewValue            ");
        _sb.AppendLine("             , @pCreation            ");
        _sb.AppendLine("             , @pCreationUser )      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCreditLineId", SqlDbType.BigInt).Value = CreditLineId;
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Action.MovementType;
          _cmd.Parameters.Add("@pOldValue", SqlDbType.Xml).Value = Action.OldValue;
          _cmd.Parameters.Add("@pNewValue", SqlDbType.Xml).Value = Action.NewValue;
          _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pCreationUser", SqlDbType.NVarChar, 50).Value = Action.CreationUser;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Message("Error: CreditLineMovements.CreateCreditLineMovement");
        Log.Exception(_ex);
      }

      return false;
    } // CreateCreditLineMovement

    #endregion " Public Methods "

  } // CreditLineMovements
} // CreditLines
