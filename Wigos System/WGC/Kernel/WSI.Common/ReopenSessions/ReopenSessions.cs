﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ReopenSessions.cs
// 
//   DESCRIPTION: Abstract class with basic methods to reopen a Cashier session or a Gambling table session
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 25-OCT-2016
// 
// REVISION HISTORY:
// 
// Date         Author  Description
// -----------  ------  ----------------------------------------------------------
// 25-OCT-2016  FOS     First release.
// 11-JAN-2017  FGB     Bug 22357: Reabrir Caja: Al intentar reabrir caja aparece un pop-up con error de sesión inválida. La NLS sobre la sesión de caja no se reconoc
// 26-JAN-2017  FGB     Bug 23393: Cannot reopen closed cashier sessions
// 02-FEB-2017  ATB     Bug 23992: Reopen cash: It does not allows to close the cage session
// 06-FEB-2017  DHA     Bug 24243: Error reopen gaming tables when chips sleeps on table
// 20-FEB-2017  JML     PBI 24378: Reabrir sesiones de caja y mesas de juego: rehacer reapertura y corrección de Bugs
// 29-MAY-2017  JML     Bug 27735: WIGOS-2373 - Cashier. Expeption is displayed when Close cashier session on gaming table with integrated cashier after reopened.
// 12-JUL-2017  DHA     Bug 28655:WIGOS-3546 "Reabrir caja" is not printing a voucher
// 25-APR-2018  AGS     Bug 32439: WIGOS-10410 Cashier session cannot be closed
// 30-APR-2018  JML     Fixed Bug 32484:WIGOS-10534 Cage stock amount and denominations are incorrect in database
// 05-JUL-2018  AGS     Bug 33467:WIGOS 13002 - Reopening a cashier session is not updating the cage amounts for Provisionables
//------------------------------------------------------------------------------
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.ReopenSessions
{
  public enum EnumSessionReopenType
  {
    REOPEN_CASHIER_STATUS = 0,
    REOPEN_GAMBLING_TABLE = 1,
  }

  public enum EnumSessionCanReopenCheckStatus
  {
    SESSION_CAN_BE_REOPENED = 0,
    INVALID_SESSION = 1,
    CAGE_SESSION_IS_CLOSED = 2,
    CASHIER_SESSION_IS_OPEN = 3,
    ALREADY_EXISTS_CLOSED_COLLECTION_MOVEMENTS = 4,
    CURRENT_USER_DID_NOT_CLOSE_THE_SESSION = 5,
    TIME_TO_REOPEN_SESSION_HAS_EXPIRED = 6,
    GAMING_DAY_HAS_CHANGED = 7,
    DROPBOX_MOVEMENT_IS_NOT_PENDING = 8,
    DROPBOX_MOVEMENT_IS_NOT_COLLECTED = 9,
    CAGE_MOVEMENT_IS_NOT_COLLECTED = 10,
    CAGE_MOVEMENT_IS_NOT_PENDING = 11,
    INTEGRATED_CASHIER_IS_CHANGE = 12,
  }

  public abstract class ReopenSessions
  {
    #region Public Methods

    /// <summary>
    /// Return the reopen type description (implemented in the descendants classes)
    /// </summary>
    /// <returns></returns>
    public abstract String GetReopenTypeDescription();

    /// <summary>
    /// Max time to reopen the session (implemented in the descendants classes)
    /// </summary>
    /// <returns></returns>
    protected abstract Int32 MaxAllowedMinutesToReopenSession();

    /// <summary>
    /// Function that recovery the id of the last closed operation
    /// </summary>

    public virtual OperationUndo.InputUndoOperation GetInputUndoOperation(Int64 OperationId, CashierSessionInfo CashierSessionInfo, SqlTransaction SqlTrx)
    {
      OperationUndo.InputUndoOperation _input_undo_operation;
      AccountOperations.Operation _account_operation;
      OperationUndo.UndoError _error;

      _input_undo_operation = new OperationUndo.InputUndoOperation();
      _input_undo_operation.CardData = new CardData();
      _input_undo_operation.CashierSessionInfo = CashierSessionInfo;
      _input_undo_operation.CodeOperation = OperationCode.REOPEN_OPERATION;
      _input_undo_operation.GenerateVoucherForUndo = true;
      _input_undo_operation.OperationId = 0;
      _input_undo_operation.OperationIdForUndo = OperationId;

      OperationUndo.IsCloseCashierSessionReversible(_input_undo_operation, SqlTrx, out _account_operation, out _error);

      return _input_undo_operation;
    }

    /// <summary>
    /// Function to Undo the concepts that generates the operation of closing session
    /// </summary>
    public virtual bool UndoCageMovement(OperationUndo.InputUndoOperation InputUndoOperation, Boolean HasDropBox, Boolean TrxOk, SqlTransaction SqlTrx)
    {
      if (TrxOk)
      {
        TrxOk = UndoSessionCageMovementStatus(InputUndoOperation, HasDropBox, SqlTrx);
      }
      return TrxOk;
    }

    /// <summary>
    /// Function that makes the operation undo.
    /// </summary>
    public virtual bool UndoOperation(OperationUndo.InputUndoOperation InputUndoOperation, Boolean TrxOk, SqlTransaction SqlTrx, out ArrayList VoucherList)
    {
      if (TrxOk)
      {
        TrxOk = OperationUndo.UndoOperation(InputUndoOperation, false, out VoucherList, SqlTrx);
        return TrxOk;
      }

      VoucherList = new ArrayList();

      return false;
    }

    /// <summary>
    /// Function to insert an alarm in the system.
    /// </summary>
    public virtual void InsertAlarm(String SourceName, UInt32 AlarmCode, String Description, SqlTransaction SqlTrx)
    {
      Alarm.Register(AlarmSourceCode.Cashier, 0, SourceName, AlarmCode, Description, AlarmSeverity.Info, WGDB.Now, SqlTrx);
    }

    /// <summary>
    /// Function that Reopen the cashier session
    /// </summary>
    public virtual bool OpenSession(CashierSessionInfo CashierSessionInfo, SqlTransaction SqlTrx)
    {
      Int64 _operation_id;

      if (CashierSessionOpen(CashierSessionInfo, SqlTrx, out _operation_id))
      {
        return true;
      }

      return false;
    }

    /// <summary>
    /// Function that reopen a gamingTable session
    /// </summary>
    public virtual bool ReOpenCashierSession(Int64 CashierSessionId, Boolean TrxOk, SqlTransaction SqlTrx)
    {
      if (TrxOk)
      {
        TrxOk = ReOpenCashierSessionMovement(CashierSessionId, SqlTrx);
      }

      return TrxOk;
    }

    /// <summary>
    /// Set collected to 0 for cashier session by currency
    /// </summary>
    public virtual bool ClearCollectedCashierSessionByCurrency(Int64 CashierSessionId, Boolean TrxOk, SqlTransaction SqlTrx)
    {
      if (TrxOk)
      {
        TrxOk = ClearCashierSessionByCurrencyMovements(CashierSessionId, SqlTrx);
      }

      return TrxOk;
    }

    /// <summary>
    /// Function that recover the last OperationId
    /// </summary>
    public Int64 GetLastOperationId(Int64 UserId, Int64 GamingTableId, SqlTransaction SqlTrx)
    {
      return BaseGetLastOperationId(UserId, GamingTableId, SqlTrx);
    }

    /// <summary>
    /// Function that recover the last Cashier session Information.
    /// </summary>
    public CashierSessionInfo GetLastCashierSessionInfo(Int64 UserId, Int64 TerminalId, SqlTransaction SqlTrx)
    {
      return BaseGetLastCashierSessionInfo(UserId, TerminalId, SqlTrx);
    }

    /// <summary>
    /// Function that returns the last session by User.
    /// </summary>
    public static Int64 GetSessionIdByUser(Int64 UserId, SqlTransaction SqlTrx)
    {
      Int64 _session_id;
      StringBuilder _sb;

      _session_id = 0;
      _sb = new StringBuilder();

      _sb.AppendLine(" SELECT    ISNULL(MAX(CS_SESSION_ID), 0) ");
      _sb.AppendLine("   FROM    CASHIER_SESSIONS              ");
      _sb.AppendLine("  WHERE    CS_USER_ID = @pUserId         ");

      using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        sql_command.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = UserId;

        using (SqlDataReader _reader = sql_command.ExecuteReader())
        {
          if (_reader.Read())
          {
            _session_id = _reader.GetInt64(0);
          }
        }
      }

      return _session_id;
    }

    /// <summary>
    /// Fuction to Obtain the stock in the last operation.
    /// </summary>
    public void GetCageStockLastOperation(Int64 OperationId, out DataTable CurrenciesTable, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      CurrenciesTable = new DataTable();

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   CM_CHIP_ID                       ");
      _sb.AppendLine("       , CM_INITIAL_BALANCE               ");
      _sb.AppendLine("       , CM_SUB_AMOUNT                    ");
      _sb.AppendLine("       , CM_ADD_AMOUNT                    ");
      _sb.AppendLine("       , CM_FINAL_BALANCE                 ");
      _sb.AppendLine("       , CM_AUX_AMOUNT                    ");
      _sb.AppendLine("       , CM_CURRENCY_DENOMINATION         ");
      _sb.AppendLine("       , CM_CAGE_CURRENCY_TYPE            ");
      _sb.AppendLine("       , CM_CURRENCY_ISO_CODE             ");
      _sb.AppendLine("  FROM   CASHIER_MOVEMENTS                ");
      _sb.AppendLine(" WHERE   CM_OPERATION_ID = @pOperationId  ");
      _sb.AppendLine("   AND   CM_TYPE = @pCageCloseSession     ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _cmd.Parameters.Add("@pCageCloseSession", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION;

        using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
        {
          _sql_da.Fill(CurrenciesTable);
        }
      }
    }

    /// <summary>
    /// Function to Update the status of the session cage movements.
    /// </summary>
    public static bool UndoSessionCageMovementStatus(OperationUndo.InputUndoOperation InputUndoOperation, Boolean HasDropBox, SqlTransaction SqlTrx)
    {
      Int64 _operation_id;
      DataTable _cage_movements;
      Int64 _user_id;
      EnumSessionCanReopenCheckStatus _can_reopen_check_status;
      Int64 _cage_movement_id;
      Boolean _trx_ok;
      Int64 _cashier_session_id;

      _user_id = InputUndoOperation.CashierSessionInfo.UserId;
      _operation_id = InputUndoOperation.OperationIdForUndo;
      _cashier_session_id = InputUndoOperation.CashierSessionInfo.CashierSessionId;
      _trx_ok = true;

      //If there is no Cage enabled then exit
      // FGB: 26-JAN-2017: Bug 23393: Cannot reopen closed cashier sessions
      if (!Cage.IsCageEnabled())
      {
        return true;
      }

      // Get movements to UNDO
      if (!GetUndoSessionCageMovements(_operation_id, out _cage_movements, SqlTrx))
      {
        return false;
      }

      // Check movements to UNDO
      if (!CheckUndoSessionCageMovements(InputUndoOperation, _cage_movements, out _can_reopen_check_status, SqlTrx))
      {
        return false;
      }

      foreach (DataRow _row in _cage_movements.Rows)
      {
        _cage_movement_id = (Int64)_row["CGM_MOVEMENT_ID"];

        Cage.DeleteFromPendingMovement(_cage_movement_id, SqlTrx);

        _trx_ok = Cage.UpdateCageMovementStatus(_cage_movement_id, Cage.CageStatus.CanceledUncollectedCashierRetirement, _user_id, _cashier_session_id, SqlTrx);

        if (_trx_ok && ((Cage.IsCageAutoMode() && Cage.IsCageAutoModeCageStock())
          || (_can_reopen_check_status == EnumSessionCanReopenCheckStatus.SESSION_CAN_BE_REOPENED)))
        {
          _trx_ok = Cage.UpdateCageStock(_cage_movement_id, SqlTrx);

          if (_trx_ok)
          {
            _trx_ok = CageMeters.UndoCageMeters(_cage_movement_id, false, SqlTrx);
          }
        }
      }

      return _trx_ok;
    }

    private static bool GetUndoSessionCageMovements(Int64 OperationId, out DataTable CageMovements, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      CageMovements = new DataTable();

      _sb = new StringBuilder();

      _sb.AppendLine(" SELECT   DISTINCT CGM.CGM_MOVEMENT_ID, CGM.CGM_STATUS, CGM.CGM_TYPE");
      _sb.AppendLine("   FROM   CASHIER_MOVEMENTS AS CM");
      _sb.AppendLine("  INNER   JOIN CAGE_CASHIER_MOVEMENT_RELATION AS CCMR ");
      _sb.AppendLine("       ON CM.CM_MOVEMENT_ID = CCMR.CM_MOVEMENT_ID");
      _sb.AppendLine("  INNER   JOIN CAGE_MOVEMENTS AS CGM ");
      _sb.AppendLine("       ON CCMR.CGM_MOVEMENT_ID = CGM.CGM_MOVEMENT_ID ");
      _sb.AppendLine("  WHERE   CM.CM_OPERATION_ID = @pOperationId AND CGM.CGM_STATUS IN (@pStatusOK, @pStatusSent)");
      _sb.AppendLine("  ORDER BY CGM.CGM_MOVEMENT_ID DESC");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {

          _cmd.Parameters.Add("@pStatusSent", SqlDbType.Int).Value = WSI.Common.Cage.CageStatus.Sent;
          _cmd.Parameters.Add("@pStatusOK", SqlDbType.Int).Value = WSI.Common.Cage.CageStatus.OK;

          _cmd.Parameters.Add("@pFromGamingTableDropbox", SqlDbType.Int).Value = WSI.Common.Cage.CageOperationType.FromGamingTableDropbox;
          _cmd.Parameters.Add("@pFromGamingTableDropboxWithExpected", SqlDbType.Int).Value = WSI.Common.Cage.CageOperationType.FromGamingTableDropboxWithExpected;

          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(CageMovements);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;

    }

    private static bool CheckUndoSessionCageMovements(OperationUndo.InputUndoOperation InputUndoOperation, DataTable CageMovements, out EnumSessionCanReopenCheckStatus CanReopenCheckStatus, SqlTransaction sqlTrx)
    {
      Boolean _is_integrated_cashier;
      Boolean _is_integrated_cashier_session;

      GamingTablesSessions.GetGamingTableInfo(InputUndoOperation.CashierSessionInfo.TerminalId, sqlTrx);

      CanReopenCheckStatus = EnumSessionCanReopenCheckStatus.SESSION_CAN_BE_REOPENED;

      _is_integrated_cashier = GamingTablesSessions.GamingTableInfo.IsGamingTable;

      // Is an user cashier session
      if (InputUndoOperation.CashierSessionInfo.UserType == GU_USER_TYPE.USER)
      {
        // If there is a gaming table -> is integrated
        _is_integrated_cashier_session = Cage.IsGamingTable(InputUndoOperation.CashierSessionInfo.CashierSessionId);
      }
      else
      {
        // In case the cashier is openen with user Sys-gaming-table the session is not integrated
        _is_integrated_cashier_session = InputUndoOperation.CashierSessionInfo.UserType != GU_USER_TYPE.SYS_GAMING_TABLE;
      }

      if (_is_integrated_cashier && !_is_integrated_cashier_session)
      {
        Log.Error(Resource.String("STR_REOPEN_SESSION_ERROR_SESSION_CAN_NOT_BE_REOPENED", Resource.String("STR_REOPEN_SESSION_TYPE_GAMBLING_TABLE_SESSION")));
        CanReopenCheckStatus = EnumSessionCanReopenCheckStatus.INTEGRATED_CASHIER_IS_CHANGE;
        return false;
      }
      if (!_is_integrated_cashier && _is_integrated_cashier_session)
      {
        Log.Error(Resource.String("STR_REOPEN_SESSION_ERROR_SESSION_CAN_NOT_BE_REOPENED", Resource.String("STR_REOPEN_SESSION_TYPE_GAMBLING_TABLE_SESSION")));
        CanReopenCheckStatus = EnumSessionCanReopenCheckStatus.INTEGRATED_CASHIER_IS_CHANGE;
        return false;
      }

      foreach (DataRow _row_movement in CageMovements.Rows)
      {

        if (Cage.IsCageAutoMode())
        {

          if ((Cage.CageOperationType)_row_movement["CGM_TYPE"] == Cage.CageOperationType.FromGamingTableDropbox
            && (Cage.CageStatus)_row_movement["CGM_STATUS"] != Cage.CageStatus.Sent)
          {
            Log.Error(Resource.String("STR_REOPEN_DROPBOX_MOVEMENT_IS_NOT_PENDING"));
            CanReopenCheckStatus = EnumSessionCanReopenCheckStatus.DROPBOX_MOVEMENT_IS_NOT_PENDING;
            return false;
          }

          if ((Cage.CageOperationType)_row_movement["CGM_TYPE"] == Cage.CageOperationType.FromGamingTableDropboxWithExpected
            && (Cage.CageStatus)_row_movement["CGM_STATUS"] != Cage.CageStatus.OK)
          {
            Log.Error(Resource.String("STR_REOPEN_DROPBOX_MOVEMENT_IS_NOT_COLLECTED"));
            CanReopenCheckStatus = EnumSessionCanReopenCheckStatus.DROPBOX_MOVEMENT_IS_NOT_COLLECTED;
            return false;
          }

          if (((Cage.CageOperationType)_row_movement["CGM_TYPE"] == Cage.CageOperationType.FromCashierClosing
                || (Cage.CageOperationType)_row_movement["CGM_TYPE"] == Cage.CageOperationType.FromGamingTableClosing)
             && (Cage.CageStatus)_row_movement["CGM_STATUS"] != Cage.CageStatus.OK)
          {
            Log.Error(Resource.String("STR_REOPEN_CAGE_MOVEMENT_IS_NOT_COLLECTED"));
            CanReopenCheckStatus = EnumSessionCanReopenCheckStatus.CAGE_MOVEMENT_IS_NOT_COLLECTED;
            return false;
          }

        }
        else
        {
          if ((Cage.CageStatus)_row_movement["CGM_STATUS"] != Cage.CageStatus.Sent)
          {
            Log.Error(Resource.String("STR_REOPEN_CAGE_MOVEMENT_IS_NOT_PENDING"));
            CanReopenCheckStatus = EnumSessionCanReopenCheckStatus.CAGE_MOVEMENT_IS_NOT_PENDING;
            return false;
          }
        }

        _row_movement.AcceptChanges();
        _row_movement.SetModified();
      }

      return true;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Function to open a cashier session.
    /// </summary>
    private Boolean CashierSessionOpen(CashierSessionInfo CashierSessionInfo, SqlTransaction SqlTrx, out Int64 OperationId)
    {
      String _sql_str;

      _sql_str = string.Empty;
      OperationId = 0;

      // Update the cashier status with the opening amount and the local time
      try
      {
        // Create movement for opened session
        CashierMovementsTable _cm_mov_table;
        _cm_mov_table = new CashierMovementsTable(CashierSessionInfo);

        if (!Operations.DB_InsertOperation(OperationCode.REOPEN_OPERATION, 0, CashierSessionInfo.CashierSessionId, 0,
                                          0, 0, 0, 0, 0, string.Empty, out OperationId, SqlTrx))
        {
          Log.Error("CashierSessionOpen: Error saving cashier movement. SessionId: " + CashierSessionInfo.CashierSessionId + ".");

          return false;
        }

        // Create movement for reopen session
        _cm_mov_table.Add(OperationId, CASHIER_MOVEMENT.REOPEN_CASHIER, 0, 0, "");

        // Save movements
        if (!_cm_mov_table.Save(SqlTrx))
        {
          Log.Error("CashierSessionOpen: Error saving cashier movement. SessionId: " + CashierSessionInfo.CashierSessionId + ".");

          return false;
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Function that updates the status of a cashier session.
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean ReOpenCashierSessionMovement(Int64 CashierSessionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   CASHIER_SESSIONS               ");
        _sb.AppendLine("     SET   CS_CLOSING_DATE = NULL         ");
        _sb.AppendLine("         , CS_STATUS       = @pStatusOpen ");
        _sb.AppendLine("   WHERE   CS_SESSION_ID   = @pSessionId  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("ReOpenCashierSessionMovement. Update Row. CashierSessionId: " + CashierSessionId.ToString());

            return false;
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Function that updates the collected to 0 for cashier session by currency
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean ClearCashierSessionByCurrencyMovements(Int64 CashierSessionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   CASHIER_SESSIONS_BY_CURRENCY    ");
        _sb.AppendLine("     SET   CSC_COLLECTED = 0               ");
        _sb.AppendLine("   WHERE   CSC_SESSION_ID   = @pSessionId  ");

        _sb.AppendLine("  UPDATE   CASHIER_SESSIONS    ");
        _sb.AppendLine("     SET   CS_COLLECTED_AMOUNT = 0               ");
        _sb.AppendLine("   WHERE   CS_SESSION_ID   = @pSessionId  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Get closing date from cashier session
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public DateTime GetCashierSessionClosingDate(Int64 CashierSessionId, SqlTransaction Trx)
    {
      DateTime _closing_date;
      StringBuilder _sb;

      _closing_date = DateTime.MinValue;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT  CS_CLOSING_DATE             ");
      _sb.AppendLine("    FROM  CASHIER_SESSIONS            ");
      _sb.AppendLine("   WHERE  CS_SESSION_ID = @pSessionId ");

      using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _sql_command.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;

        using (SqlDataReader _reader = _sql_command.ExecuteReader())
        {
          if (_reader.Read())
          {
            _closing_date = _reader.GetDateTime(0);
          }
        }
      }

      return _closing_date;
    }

    /// <summary>
    /// Get the identifier of the last operation.
    /// </summary>
    private Int64 BaseGetLastOperationId(Int64 UserId, Int64 GamingTableSessionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int64 _operation_id;

      _operation_id = 0;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT TOP 1   CM_OPERATION_ID       ");
      _sb.AppendLine("          FROM   CASHIER_MOVEMENTS     ");
      _sb.AppendLine("         WHERE   CM_USER_ID = @pUserId ");

      if (GamingTableSessionId > 0)
      {
        _sb.AppendLine("         AND   CM_SESSION_ID = @pSessionId ");
      }

      _sb.AppendLine("      ORDER BY   CM_MOVEMENT_ID DESC   ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = UserId;

        if (GamingTableSessionId > 0)
        {
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = GamingTableSessionId;
        }

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            _operation_id = _reader.GetInt64(0);
          }
        }
      }

      return _operation_id;
    }

    /// <summary>
    /// Function to get the last cashier session info.
    /// </summary>
    private CashierSessionInfo BaseGetLastCashierSessionInfo(Int64 UserId, Int64 TerminalId, SqlTransaction SqlTrx)
    {
      CashierSessionInfo _session_info;
      StringBuilder _sb;

      _session_info = new CashierSessionInfo();

      _sb = new StringBuilder();

      _sb.AppendLine("  SELECT TOP 1    CS_SESSION_ID                      ");
      _sb.AppendLine("                , CS_CASHIER_ID                      ");
      _sb.AppendLine("                , CS_USER_ID                         ");
      _sb.AppendLine("                , CT_NAME                            ");
      _sb.AppendLine("                , GU_USERNAME                        ");
      _sb.AppendLine("                , ''                                 ");
      _sb.AppendLine("                , 0                                  ");
      _sb.AppendLine("                , GU_USER_TYPE                       ");
      _sb.AppendLine("                , CS_GAMING_DAY                      ");
      _sb.AppendLine("          FROM   CASHIER_SESSIONS                    ");
      _sb.AppendLine("    INNER JOIN   CASHIER_TERMINALS                   ");
      _sb.AppendLine("            ON   CS_CASHIER_ID = CT_CASHIER_ID       ");
      _sb.AppendLine("    INNER JOIN   GUI_USERS                           ");
      _sb.AppendLine("            ON   CS_USER_ID = GU_USER_ID             ");
      _sb.AppendLine("         WHERE   CS_USER_ID = @pUserId               ");

      if (TerminalId > 0)
      {
        _sb.AppendLine("         AND   CS_CASHIER_ID = @pCashierId         ");
      }

      _sb.AppendLine("      ORDER BY   CS_SESSION_ID DESC                  ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = UserId;

        if (TerminalId > 0)
        {
          _cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = TerminalId;
        }

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            _session_info.CashierSessionId = _reader.GetInt64(0);
            _session_info.TerminalId = _reader.GetInt32(1);
            _session_info.UserId = _reader.GetInt32(2);
            _session_info.TerminalName = _reader.GetString(3);
            _session_info.UserName = _reader.GetString(4);
            _session_info.IsMobileBank = false;//_reader.GetBoolean(6);
            _session_info.AuthorizedByUserId = 0;
            _session_info.AuthorizedByUserName = string.Empty;
            _session_info.UserType = (GU_USER_TYPE)_reader.GetInt16(7);
            _session_info.GamingDay = _reader.GetDateTime(8);
          }
        }
      }

      return _session_info;
    }

    /// <summary>
    ///  Function to prepare the values to update the stock
    /// </summary>
    /// <returns></returns>
    public Boolean UpdateCageStockProcess(DataTable Currencies, SqlTransaction SqlTrx)
    {
      Int64 _denomination;
      Int64 _amount;
      Int64 _type;
      Int64 _quantity;
      Int64 _chip_id;
      Boolean _update_stock;

      _denomination = 0;
      _amount = 0;
      _type = 0;
      _quantity = 0;
      _chip_id = 0;

      foreach (DataRow row in Currencies.Rows)
      {
        _type = Convert.ToInt64(row["CM_CAGE_CURRENCY_TYPE"]);
        _chip_id = Convert.ToInt64(row["CM_CHIP_ID"]);

        if (_type != (Int64)WSI.Common.FeatureChips.ChipType.COLOR)
        {
          _denomination = Convert.ToInt64(row["CM_CURRENCY_DENOMINATION"]);
          _amount = Convert.ToInt64(row["CM_INITIAL_BALANCE"]);
          _quantity = _amount / _denomination;
        }
        else
        {
          _quantity = Convert.ToInt64(row["CM_AUX_AMOUNT"]);
        }

        _update_stock = UpdateCagestock(_quantity, _chip_id, SqlTrx);

        if (!_update_stock)
        {
          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// Function tha update the values of the stock
    /// </summary>
    private Boolean UpdateCagestock(Int64 Quantity, Int64 ChipId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("  UPDATE  CHIPS_STOCK                                      ");
      _sb.AppendLine("     SET  CHSK_QUANTITY   = (CHSK_QUANTITY - @pQuantity)   ");
      _sb.AppendLine("   WHERE  CHSK_CHIP_ID    = @pChipID                       ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pChipID", SqlDbType.BigInt).Value = ChipId;
        _cmd.Parameters.Add("@pQuantity", SqlDbType.BigInt).Value = Quantity;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          return false;
        }

        return true;
      }
    }

    /// <summary>
    /// Checks if the Cashier Session can be reopened
    /// </summary>
    /// <param name="CashierSessionsInfo"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="MessageError"></param>
    /// <returns></returns>
    public Boolean CheckIfCanReopenCashierSession(CashierSessionInfo CashierSessionsInfo, OperationUndo.InputUndoOperation InputUndoOperation, SqlTransaction SqlTrx, out String MessageError)
    {
      Boolean _result_ok;
      EnumSessionCanReopenCheckStatus _check_reopen_error;

      MessageError = String.Empty;

      _check_reopen_error = CanReopenCashierSession(CashierSessionsInfo, InputUndoOperation, SqlTrx);
      _result_ok = (_check_reopen_error == EnumSessionCanReopenCheckStatus.SESSION_CAN_BE_REOPENED);

      if (!_result_ok)
      {
        MessageError = GetReopenCheckErrorMessage(_check_reopen_error);
      }

      return _result_ok;
    }

    /// <summary>
    /// Returns the validation to reopen session error message
    /// </summary>
    /// <param name="CheckError"></param>
    /// <returns></returns>
    protected String GetReopenCheckErrorMessage(EnumSessionCanReopenCheckStatus CheckError)
    {
      String _error_message;
      String _reopen_type;

      _error_message = String.Empty;
      _reopen_type = GetReopenTypeDescription();

      switch (CheckError)
      {
        case EnumSessionCanReopenCheckStatus.SESSION_CAN_BE_REOPENED:
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_SESSION_CAN_BE_REOPENED", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.INVALID_SESSION:
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_INVALID_SESSION", _reopen_type, _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.CAGE_SESSION_IS_CLOSED:
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_CAGE_SESSION_IS_CLOSED", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.CASHIER_SESSION_IS_OPEN:
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_CASHIER_SESSION_IS_OPEN", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.ALREADY_EXISTS_CLOSED_COLLECTION_MOVEMENTS:
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_EXISTS_CLOSED_COLLECTION_MOVEMENTS", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.CURRENT_USER_DID_NOT_CLOSE_THE_SESSION:
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_CURRENT_USER_DID_NOT_CLOSE_THE_SESSION", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.TIME_TO_REOPEN_SESSION_HAS_EXPIRED:
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_TIME_TO_REOPEN_SESSION_HAS_EXPIRED", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.GAMING_DAY_HAS_CHANGED:
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_NOT_IN_GAMING_DAY", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.DROPBOX_MOVEMENT_IS_NOT_PENDING:
          _error_message = Resource.String("STR_REOPEN_DROPBOX_MOVEMENT_IS_NOT_PENDING", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.DROPBOX_MOVEMENT_IS_NOT_COLLECTED:
          _error_message = Resource.String("STR_REOPEN_DROPBOX_MOVEMENT_IS_NOT_COLLECTED", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.CAGE_MOVEMENT_IS_NOT_COLLECTED:
          _error_message = Resource.String("STR_REOPEN_CAGE_MOVEMENT_IS_NOT_COLLECTED", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.CAGE_MOVEMENT_IS_NOT_PENDING:
          _error_message = Resource.String("STR_REOPEN_CAGE_MOVEMENT_IS_NOT_PENDING", _reopen_type);

          break;
        case EnumSessionCanReopenCheckStatus.INTEGRATED_CASHIER_IS_CHANGE:
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_SESSION_CAN_NOT_BE_REOPENED", Resource.String("STR_REOPEN_SESSION_TYPE_GAMBLING_TABLE_SESSION"));

          break;
        default:

          throw new ArgumentException("Invalid EnumSessionCanReopenCheckStatus: '" + CheckError + "'.");
      }

      return _error_message;
    }

    /// <summary>
    /// Can reopen cashier session
    /// </summary>
    /// <returns></returns>
    protected EnumSessionCanReopenCheckStatus CanReopenCashierSession(CashierSessionInfo SessionInfo, OperationUndo.InputUndoOperation InputUndoOperation, SqlTransaction SqlTrx)
    {
      DataTable _dt;
      EnumSessionCanReopenCheckStatus _can_reopen_check_status;

      if (SessionInfo == null)
      {
        return EnumSessionCanReopenCheckStatus.INVALID_SESSION;
      }

      //Cashier session is open
      if (IsCashierSessionOpen(SessionInfo, SqlTrx))
      {
        return EnumSessionCanReopenCheckStatus.CASHIER_SESSION_IS_OPEN;
      }

      //Cage session is closed
      if (!IsCageSessionOpen(SessionInfo, SqlTrx))
      {
        return EnumSessionCanReopenCheckStatus.CAGE_SESSION_IS_CLOSED;
      }

      //Cashier session has collected movements
      if (HasCageSessionCollectedMovements(SessionInfo, SqlTrx))
      {
        return EnumSessionCanReopenCheckStatus.ALREADY_EXISTS_CLOSED_COLLECTION_MOVEMENTS;
      }

      //Is current user the same that closed the session
      if (!DidActiveUserCloseTheSessionToReopen(SessionInfo))
      {
        return EnumSessionCanReopenCheckStatus.CURRENT_USER_DID_NOT_CLOSE_THE_SESSION;
      }

      //We have a limited interval to reopen the cashier session
      if (!CanReopenByTime(SessionInfo, SqlTrx))
      {
        return EnumSessionCanReopenCheckStatus.TIME_TO_REOPEN_SESSION_HAS_EXPIRED;
      }

      if (!IsSameGamingDay(SessionInfo))
      {
        return EnumSessionCanReopenCheckStatus.GAMING_DAY_HAS_CHANGED;
      }

      if (GetUndoSessionCageMovements(InputUndoOperation.OperationIdForUndo, out _dt, SqlTrx))
      {
        if (!CheckUndoSessionCageMovements(InputUndoOperation, _dt, out _can_reopen_check_status, SqlTrx))
        {
          return _can_reopen_check_status;
        }
      }
      else
      {
        return EnumSessionCanReopenCheckStatus.INVALID_SESSION;
      }

      //If arrives here, it is everything OK and whe cashier session can be reopened.
      return EnumSessionCanReopenCheckStatus.SESSION_CAN_BE_REOPENED;
    }

    /// <summary>
    /// Is Cage session open
    /// </summary>
    /// <param name="SessionInfo"></param>
    /// <returns></returns>
    private Boolean IsCageSessionOpen(CashierSessionInfo SessionInfo, SqlTransaction SqlTrx)
    {
      Int64 _cage_session_id;

      //Cage is not enabled
      if (!Cage.IsCageEnabled())
      {
        return true;
      }

      //Exists Cage Session for Cashier Session
      if (Cage.GetCageSessionIdFromCashierSessionId(SessionInfo.CashierSessionId, out _cage_session_id, SqlTrx))
      {
        //Cage session is open
        return (Cage.IsCageSessionOpen(_cage_session_id, SqlTrx));
      }
      return true;
    }

    /// <summary>
    /// Is the cashier session open
    /// </summary>
    /// <param name="SessionInfo"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean IsCashierSessionOpen(CashierSessionInfo SessionInfo, SqlTransaction SqlTrx)
    {
      return (Cage.IsCashierSessionOpen(SessionInfo.CashierSessionId, SqlTrx));
    }

    /// <summary>
    /// Is same gaming day
    /// </summary>
    /// <param name="SessionInfo"></param>
    /// <returns></returns>
    private Boolean IsSameGamingDay(CashierSessionInfo SessionInfo)
    {
      DateTime _current_gaming_day;

      if (!Misc.IsGamingDayEnabled())
      {
        return true;
      }

      _current_gaming_day = Misc.TodayOpening();

      return (_current_gaming_day.Equals(SessionInfo.GamingDay));
    }

    /// <summary>
    /// Has cage session collected movements
    /// </summary>
    /// <returns></returns>
    private Boolean HasCageSessionCollectedMovements(CashierSessionInfo SessionInfo, SqlTransaction SqlTrx)
    {
      if (Cage.IsCageAutoMode())
      {
        return false;
      }

      return Cage.ExistsCollectedCageMovementsForCashierSession(SessionInfo, SqlTrx);
    }

    /// <summary>
    /// Did user close the cashier session
    /// </summary>
    /// <param name="SessionInfo"></param>
    /// <returns></returns>
    private Boolean DidActiveUserCloseTheSessionToReopen(CashierSessionInfo SessionInfo)
    {
      Int32 _current_user;

      _current_user = CommonCashierInformation.UserId;

      return (_current_user == SessionInfo.UserId);
    }

    /// <summary>
    /// Can reopen the cashier session by time
    /// </summary>
    /// <returns></returns>
    private Boolean CanReopenByTime(CashierSessionInfo SessionInfo, SqlTransaction SqlTrx)
    {
      Int32 _max_allowed_minutes_to_reopensession;
      DateTime _session_closed_time;
      DateTime _limit_time_to_reopen_session;
      DateTime _now;

      _max_allowed_minutes_to_reopensession = MaxAllowedMinutesToReopenSession();

      if (_max_allowed_minutes_to_reopensession == 0)
      {
        return true; //there is no time limit to reopen
      }

      _session_closed_time = GetCashierSessionClosingDate(SessionInfo.CashierSessionId, SqlTrx);
      _limit_time_to_reopen_session = _session_closed_time.AddMinutes(_max_allowed_minutes_to_reopensession);
      _now = WGDB.Now; //get current time

      return (_limit_time_to_reopen_session >= _now);
    }

    #endregion
  }
}