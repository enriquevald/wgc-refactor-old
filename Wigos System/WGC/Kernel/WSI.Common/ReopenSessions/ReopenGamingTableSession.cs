﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ReopenCashierSession.cs
// 
//   DESCRIPTION: Class to ReOpen a gaming table session, implements ReopenSessions class
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 25-OCT-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-OCT-2016 FOS    First release.
// 31-JAN-2017 DPC    Bug 22357: Reabrir Caja: Al intentar reabrir mesa aparece un pop-up con error de sesión inválida. La NLS sobre la sesión de caja no se reconoce.
// 03-FEB-2017 FGB    Bug 24132: There is no general param for "GamingTables - ReopenSession.MaxAllowedMinutes"
// 20-FEB-2017 JML    PBI 24378:Reabrir sesiones de caja y mesas de juego: rehacer reapertura y corrección de Bugs
// 12-JUL-2017 DHA    Bug 28655:WIGOS-3546 "Reabrir caja" is not printing a voucher
// 20-JUN-2018 MS     PBI 33220 [WIGOS-13070] When reopening a gambling table, the 'Gambling table session' does not show the previous state.
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.ReopenSessions
{
  public class ReopenGamingTableSession : ReopenSessions
  {
    private const String RESOURCE_REOPEN_TYPE_DESCRIPTION = "STR_REOPEN_SESSION_TYPE_GAMBLING_TABLE_SESSION";
    private const String RESOURCE_REOPEN = "STR_REOPEN_SESSION";

    private const String GP_GROUP_KEY = "GamingTables";
    private const String GP_SUBJECT_KEY = "ReopenSession.MaxAllowedMinutes";
    private const Int32 GP_DEFAULT_VALUE = 15;

    /// <summary>
    /// Reopen type description
    /// </summary>
    /// <returns></returns>
    public override String GetReopenTypeDescription()
    {
      return Resource.String(RESOURCE_REOPEN_TYPE_DESCRIPTION);
    }

    /// <summary>
    /// Max time to reopen the session
    /// </summary>
    /// <returns></returns>
    protected override Int32 MaxAllowedMinutesToReopenSession()
    {
      return GeneralParam.GetInt32(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_DEFAULT_VALUE);
    }

    /// <summary>
    /// Function to Undo all the Operations when a gamingTable session closed, and after reopen the table
    /// </summary>
    /// <returns>true: Reopen the gamingTable session/ false: The gamingTable remains closed<</returns>
    public Boolean Undo(CashierSessionInfo CashierSessionInfo, GamingTable GamingTable, SqlTransaction SqlTrx, out String MessageError, out ArrayList VoucherList)
    {
      Boolean _trx_ok;
      Int64 _gaming_table_session_id;
      GamingTablesSessions m_gaming_table_session;
      Currency _collected_amount;
      CashierSessionInfo _cashier_session_info;
      Int64 _operation_id;
      Boolean _has_dropBox;
      OperationUndo.InputUndoOperation _input_undo_operation;

      _operation_id = 0;
      _trx_ok = true;
      _gaming_table_session_id = 0;
      _collected_amount = 0;
      _has_dropBox = false;
      VoucherList = null;

      _gaming_table_session_id = GetGamingTableSessionId(GamingTable.GamingTableId, SqlTrx);
      GamingTablesSessions.GetSession(out m_gaming_table_session, _gaming_table_session_id, SqlTrx);
      _operation_id = base.GetLastOperationId(CashierSessionInfo.AuthorizedByUserId, m_gaming_table_session.CashierSessionId, SqlTrx);
      _cashier_session_info = base.GetLastCashierSessionInfo(CashierSessionInfo.UserId, CashierSessionInfo.TerminalId, SqlTrx);

      _cashier_session_info.AuthorizedByUserId = CashierSessionInfo.AuthorizedByUserId;
      _cashier_session_info.AuthorizedByUserName = CashierSessionInfo.AuthorizedByUserName;


      _input_undo_operation = base.GetInputUndoOperation(_operation_id, _cashier_session_info, SqlTrx);

      CashierSessionInfo.CashierSessionId = _cashier_session_info.CashierSessionId;
      CashierSessionInfo.GamingDay = _cashier_session_info.GamingDay;

      //Check if gaming table session can be reopen
      if (!CheckIfCanReopenCashierSession(CashierSessionInfo, _input_undo_operation, SqlTrx, out MessageError))
      {
        return false;
      }

      if (!GamingTablesSessions.GetGamingTableInfo(Convert.ToInt32(GamingTable.CashierId), SqlTrx))
      {
        //If error then show generic message
        MessageError = Resource.String("STR_REOPEN_SESSION_ERROR_SESSION_CAN_NOT_BE_REOPENED", Resource.String("STR_REOPEN_SESSION_TYPE_GAMBLING_TABLE_SESSION"));

        return false;
      }

      _has_dropBox = GamingTablesSessions.GamingTableInfo.HasDropbox;

      //Reopen
      _trx_ok = base.ReOpenCashierSession(m_gaming_table_session.CashierSessionId, _trx_ok, SqlTrx);

      _trx_ok = base.OpenSession(_input_undo_operation.CashierSessionInfo, SqlTrx);

      //Undo Conceptos
      _trx_ok = base.UndoCageMovement(_input_undo_operation, _has_dropBox, _trx_ok, SqlTrx);

      //if (Cage.IsCageAutoMode())
      //{
      //  DataTable _currencies_table;

      //  //Undo Stock de Bóveda
      //  base.GetCageStockLastOperation(_operation_id, out _currencies_table, SqlTrx);

      //  _trx_ok = base.UpdateCageStockProcess(_currencies_table, SqlTrx);
      //}

      //Undo Operations CashierMovements
      _trx_ok = base.UndoOperation(_input_undo_operation, _trx_ok, SqlTrx, out VoucherList);

      if (_trx_ok)
      {
        //Get collectedAmount values
        _collected_amount = m_gaming_table_session.CollectedAmount;

        _collected_amount = _collected_amount * -1;

        //Update Gaming_Tables_Session table
        UpdateGamingTablesSessions(_gaming_table_session_id, _collected_amount, SqlTrx);

        // Undo cashier session by currency
        _trx_ok = base.ClearCollectedCashierSessionByCurrency(m_gaming_table_session.CashierSessionId, _trx_ok, SqlTrx);

        //Generar Alarma
        base.InsertAlarm(_input_undo_operation.CashierSessionInfo.UserName + "@" + _input_undo_operation.CashierSessionInfo.TerminalName, (UInt32)AlarmCode.User_CashierSessionReopened, Resource.String(RESOURCE_REOPEN), SqlTrx);

      }

      if ((!_trx_ok) && (String.IsNullOrEmpty(MessageError)))
      {
        //If there is no message error then show generic message
        MessageError = Resource.String("STR_REOPEN_SESSION_ERROR_SESSION_CAN_NOT_BE_REOPENED", Resource.String("STR_REOPEN_SESSION_TYPE_GAMBLING_TABLE_SESSION"));
      }

      return _trx_ok;
    }

    /// <summary>
    /// Function that returns the TableSessionId active for a gamingTable
    /// </summary>
    public Int64 GetGamingTableSessionId(Int64 GamingTableId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int64 _gaming_table_session_id;

      _gaming_table_session_id = 0;

      _sb = new StringBuilder();

      _sb.AppendLine("   SELECT   GTS_GAMING_TABLE_SESSION_ID");
      _sb.AppendLine("     FROM   GAMING_TABLES_SESSIONS");
      _sb.AppendLine("    WHERE   GTS_GAMING_TABLE_ID = @pGamingTableId");
      _sb.AppendLine(" ORDER BY   GTS_GAMING_TABLE_SESSION_ID DESC");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pGamingTableId", SqlDbType.BigInt).Value = GamingTableId;

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            _gaming_table_session_id = _reader.GetInt64(0);
          }
        }
      }

      return _gaming_table_session_id;
    }

    /// <summary>
    /// Function that update te values when reopen a gamingTableSession
    /// </summary>
    private Boolean UpdateGamingTablesSessions(Int64 GamingTableSessionId, Currency CollectedAmount, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      if (!GetTotalCashSentToTable(GamingTableSessionId, out CollectedAmount ,SqlTrx))
      {
        return false;
      }

      //Reset the totals that have not been collected because the table re-opened

      _sb.AppendLine("UPDATE       GAMING_TABLES_SESSIONS");
      _sb.AppendLine("   SET       GTS_TIPS = 0");
      _sb.AppendLine("           , GTS_CLIENT_VISITS = 0");
      _sb.AppendLine("           , GTS_FINAL_CHIPS_AMOUNT = 0");
      _sb.AppendLine("           , GTS_COLLECTED_AMOUNT = @collectedAmount");
      _sb.AppendLine(" WHERE       GTS_GAMING_TABLE_SESSION_ID = @gamingTableSessionId");

      //Reset Currency Amounts to Amount sent from Cage

      _sb.AppendLine("UPDATE       GAMING_TABLES_SESSIONS_BY_CURRENCY");
      _sb.AppendLine("   SET       GTSC_COLLECTED_AMOUNT = @collectedAmount");
      _sb.AppendLine("           , GTSC_COLLECTED_AMOUNT_CONVERTED = @collectedAmount");
      _sb.AppendLine(" WHERE       GTSC_GAMING_TABLE_SESSION_ID = @gamingTableSessionId AND ");
      _sb.AppendLine("             GTSC_TYPE = @cageCurrencyType");

      //Reset Final Chips Amount because Table Reopened and we don't know the final amount yet

      _sb.AppendLine("UPDATE       GAMING_TABLES_SESSIONS_BY_CURRENCY");
      _sb.AppendLine("   SET       GTSC_FINAL_CHIPS_AMOUNT = 0");
      _sb.AppendLine(" WHERE       GTSC_GAMING_TABLE_SESSION_ID = @gamingTableSessionId");

      using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        sql_command.Parameters.Add("@gamingTableSessionId", SqlDbType.BigInt).Value = GamingTableSessionId;
        sql_command.Parameters.Add("@collectedAmount", SqlDbType.Money).Value = CollectedAmount.SqlMoney;
        sql_command.Parameters.Add("@cageCurrencyType", SqlDbType.Int).Value = CageCurrencyType.Bill;

        return (sql_command.ExecuteNonQuery() == 1);
      }
    }

    private Boolean GetTotalCashSentToTable(Int64 GamingTableSessionID, out Currency CurrencySentfromCage, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      CurrencySentfromCage = 0;

      _sb.AppendLine("SELECT       SUM(CM_ADD_AMOUNT)                                      ");
      _sb.AppendLine("  FROM       GAMING_TABLES_SESSIONS                                  ");
      _sb.AppendLine("INNER JOIN   CASHIER_MOVEMENTS                                       ");
      _sb.AppendLine("    ON       GTS_CASHIER_SESSION_ID = CM_SESSION_ID                  ");
      _sb.AppendLine(" WHERE       CM_TYPE = @movementType                                 ");
      _sb.AppendLine("             AND GTS_GAMING_TABLE_SESSION_ID = @gamingTableSessionId ");
      _sb.AppendLine("             AND CM_CAGE_CURRENCY_TYPE = @cageCurrencyType           ");

      using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        sql_command.Parameters.Add("@gamingTableSessionId", SqlDbType.BigInt).Value = GamingTableSessionID;
        sql_command.Parameters.Add("@movementType", SqlDbType.Int).Value = CASHIER_MOVEMENT.FILLER_IN;
        sql_command.Parameters.Add("@cageCurrencyType", SqlDbType.Int).Value = CageCurrencyType.Bill;

        var _rv = sql_command.ExecuteScalar();

        if (_rv == null)
        {
          return false;
        }
        else
        {
          CurrencySentfromCage = Convert.ToDecimal(_rv);
          CurrencySentfromCage = CurrencySentfromCage * -1;
        }

      }

      return true;

    }
  }
}
