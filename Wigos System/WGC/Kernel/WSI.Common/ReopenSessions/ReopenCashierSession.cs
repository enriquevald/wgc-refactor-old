﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ReopenCashierSession.cs
// 
//   DESCRIPTION: Class to ReOpen a cashier session, implements ReopenSessions class
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 25-OCT-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-OCT-2016 FOS    First release .
// 20-FEB-2017 JML    PBI 24378:Reabrir sesiones de caja y mesas de juego: rehacer reapertura y corrección de Bugs
// 12-JUL-2017 DHA    Bug 28655:WIGOS-3546 "Reabrir caja" is not printing a voucher
// 03-AUG-2017 EOR    Bug 29187: WIGOS-3944 Is not possible close a cash session by bankcard once the session was reopened
// 01-SEP-2017 ETP    WIGOS-3817 The cancellation of a withdrawal with credit card is not working properly
//------------------------------------------------------------------------------
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.ReopenSessions
{
  public class ReopenCashierSession : ReopenSessions
  {
    private const String RESOURCE_REOPEN_TYPE_DESCRIPTION = "STR_REOPEN_SESSION_TYPE_CASHIER_SESSION";
    private const String RESOURCE_REOPEN = "STR_REOPEN_SESSION";

    private const String GP_GROUP_KEY = "Cashier";
    private const String GP_SUBJECT_KEY = "ReopenSession.MaxAllowedMinutes";
    private const Int32 GP_DEFAULT_VALUE = 15;

    #region Public Methods

    /// <summary>
    /// Reopen type description
    /// </summary>
    /// <returns></returns>
    public override String GetReopenTypeDescription()
    {
      return Resource.String(RESOURCE_REOPEN_TYPE_DESCRIPTION);
    }

    /// <summary>
    /// Max time to reopen the session
    /// </summary>
    /// <returns></returns>
    protected override Int32 MaxAllowedMinutesToReopenSession()
    {
      return GeneralParam.GetInt32(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_DEFAULT_VALUE);
    }

    /// <summary>
    /// Function to Undo all Operations when we close a cashier session, and after reopen the same session
    /// </summary>
    /// <param name="CashierSessionsInfo"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="MessageError"></param>
    /// <returns>true: Reopen the session/ false: The session remains closed</returns>
    public Boolean Undo(CashierSessionInfo CashierSessionsInfo, SqlTransaction SqlTrx, out String MessageError, out ArrayList VoucherList)
    {
      Boolean _trx_ok;
      Int64 _operation_id;
      OperationUndo.InputUndoOperation _input_undo_operation;

      _trx_ok = true;
      _operation_id = 0;
      VoucherList = new ArrayList();
      MessageError = String.Empty;
      

      _operation_id = base.GetLastOperationId(CashierSessionsInfo.UserId, 0, SqlTrx);

      //Collected Movement
      _input_undo_operation = base.GetInputUndoOperation(_operation_id, CashierSessionsInfo, SqlTrx);

      //Check if cashier session can be reopen
      if (!CheckIfCanReopenCashierSession(CashierSessionsInfo, _input_undo_operation, SqlTrx, out MessageError))
      {
        return false;
      }

      //Reopen
      _trx_ok = base.ReOpenCashierSession(CashierSessionsInfo.CashierSessionId, _trx_ok, SqlTrx);

      _trx_ok = base.OpenSession(_input_undo_operation.CashierSessionInfo, SqlTrx);

      //Undo Conceptos
      _trx_ok = base.UndoCageMovement(_input_undo_operation, false, _trx_ok, SqlTrx);

      //if (Cage.IsCageAutoMode())
      //{
      //  DataTable _currencies_table;

      //  //Undo Stock de Bóveda
      //  base.GetCageStockLastOperation(_operation_id, out _currencies_table, SqlTrx);

      //  _trx_ok = base.UpdateCageStockProcess(_currencies_table, SqlTrx);
      //}

      //Undo Operations 
      _trx_ok = base.UndoOperation(_input_undo_operation, _trx_ok, SqlTrx, out VoucherList);

      // Undo cashier session by currency
      _trx_ok = base.ClearCollectedCashierSessionByCurrency(CashierSessionsInfo.CashierSessionId, _trx_ok, SqlTrx);

      // Delete conciliate Vouchers Pinpad
      _trx_ok = Withdrawal.DeleteReconciliationMovements(_input_undo_operation.OperationIdForUndo, _trx_ok, SqlTrx);

      //Alarm
      base.InsertAlarm(CashierSessionsInfo.UserName + "@" + CashierSessionsInfo.TerminalName, (UInt32)AlarmCode.User_CashierSessionReopened, Resource.String(RESOURCE_REOPEN), SqlTrx);

      if ((!_trx_ok) && (String.IsNullOrEmpty(MessageError)))
      {
        //If there is no error message then show generic message
        MessageError = Resource.String("STR_REOPEN_SESSION_ERROR_SESSION_CAN_NOT_BE_REOPENED", Resource.String("STR_REOPEN_SESSION_TYPE_CASHIER_SESSION"));
      }

      return _trx_ok;
    }

    #endregion
  }
}
