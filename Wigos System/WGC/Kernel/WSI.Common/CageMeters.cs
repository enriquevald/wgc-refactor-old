//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CageMeters.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. CageMeters
//
//        AUTHOR: Sergi Mart�nez
// 
// CREATION DATE: 17-SEP-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-SEP-2014 SMN    Initial version.
// 10-MAR-2015 YNM    Fixed Bug WIG-2137: Stock on Close Session is different that Current Stock.
// 15-SEP-2015 DCS    Backlog Item 3707 - Coin Collection
// 07-DEC-2015 ETP    BUG FIXED 7146 Incorrect amount for federal and ICS tax
// 29-MAR-2016 ETP    BUG FIXED 10918: Incorrect cancel amount for Cage session meters.
// 22-ABR-2016 JML    Product Backlog Item 9758:Gaming Tables (Fase 1): General cage report
// 27-APR-2016 DHA    Product Backlog Item 10825: added chips types to cage concepts
// 28-ABR-2016 JML    Product Backlog Item 10825:Gaming Tables (Fase 1): Items, tickets & chips (Cage)
// 28-SEP-2016 DHA    PBI 17747: cancel gaming tables operations
// 25-NOV-2016 DHA    Bug 20560: added tips on closing sesions (cage concepts)
// 08-DIC-2016 RGR    Bug 18295: Cage: The general report is not showing the correct data when a withdrawal is canceled
// 08-FEB-2017 DHA    PBI 24378: Reopen cashier/gambling tables sesions
// 07-SEP-2017 EOR    Bug 29654: WIGOS-4980 Accounting - Not redeemed chips denomination are shown on unclaimed form
// 05-JUL-2018 AGS    Bug 33467:WIGOS 13002 - Reopening a cashier session is not updating the cage amounts for Provisionables
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI;
using WSI.Common;
using System.Collections;

namespace WSI.Common
{
  public class CageMeters
  {
    #region Enums
    public enum CageSystemSourceTarget
    {
      System = 0,
      Cashiers = 10,
      GamingTables = 11,
      Terminals = 12,
    }

    public enum CageSourceTargetCashierAction
    {
      None = -1, // null
      CashierInput = 0,
      CashierOutput = 1,
    }

    public enum CageSourceTargetType
    {
      CageInput = 0,
      CageOutput = 1,
      CageInputOutput = 2,
    }

    public enum UpdateCageMetersOperationType
    {
      Set = 0,
      Increment = 1
    }

    public enum UpdateCageMetersgetGetSessionMode
    {
      FromParameter = 0,
      LastOpened = 1,
      FirstOpened = 2
    }

    public enum CreateCageMetersOption
    {
      CageMeters_CageSessionMeters = 0,         // Create meter in cage_meters and cage_session_meters table
      CageMeters = 1,                           // Create meter only in cage_meters table
      CageSessionMeters = 2,                    // Create meter only in cage_session_meters table
      CageMeters_CageAllOpenSessionMeters = 3,  // Create meter in cage_meters and cage_session_meters table for all opened cage sessions
    }

    public enum CageConceptId
    {
      Global = 0,
      ChipsCirculating = 1,
      TaxState = 2,
      TaxFederal = 3,
      ProgresiveProvision = 4,
      TargetDeposits = 5,
      Tips = 6,
      //Faltante = 7,
      //Sobrante = 8,
      Tickets = 9,
      Bills = 10,
      Coins = 11,
      TaxIEJC = 12,
      SafeKeeping = 20,
      Unclaimed = 21,

      // "ConceptId" = 1000
      // Reservado a partir del 1000 (Conceptos personalizados que se pueden crear en el GUI)
    }

    public enum CageConceptType
    {
      SystemCalculated = 0,
      UserCalculated = 1,
    } // CageConceptType

    #endregion //Enums

    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Update table CAGE_METERS & CAGE_SESSION_METERS
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :ValueOut
    //
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES :
    // 

    public static Boolean UpdateCageMeters(Decimal ValueIn
                                         , Decimal ValueOut
                                         , Int64? CageSessionId
                                         , Int64 SurceTargetId
                                         , Int64 ConceptId
                                         , String IsoCode
                                         , CageCurrencyType CageCurrencyType
                                         , UpdateCageMetersOperationType OperationType
                                         , UpdateCageMetersgetGetSessionMode SessionGetMode
                                         , out Decimal ResultMeters
                                         , out Decimal ResultSessionMeters
                                         , SqlTransaction Trx)
    {
      ResultMeters = 0;
      ResultSessionMeters = 0;

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("CageUpdateMeter", Trx.Connection, Trx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;

          _sql_cmd.Parameters.Add("@pValueIn", SqlDbType.Money).Value = ValueIn;
          _sql_cmd.Parameters.Add("@pValueOut", SqlDbType.Money).Value = ValueOut;

          if (CageSessionId == null)
          {
            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CageSessionId;
          }
          _sql_cmd.Parameters.Add("@pConceptId", SqlDbType.BigInt).Value = ConceptId;
          _sql_cmd.Parameters.Add("@pSourceTagetId", SqlDbType.BigInt).Value = SurceTargetId;
          _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IsoCode;
          _sql_cmd.Parameters.Add("@pCageCurrencyType", SqlDbType.Int).Value = CageCurrencyType;

          _sql_cmd.Parameters.Add("@pOperation", SqlDbType.Int).Value = OperationType;
          _sql_cmd.Parameters.Add("@pSessionGetMode", SqlDbType.Int).Value = SessionGetMode;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Error(String.Format("WSI.Common.Cage.UpdateCageMeters. Can't update table CAGE_METERS. Description:{0}, IsoCode:{1}, OperationType:{2}, ValueIn:{3}, ValueOut:{4}", ConceptId, IsoCode, OperationType, ValueIn, ValueOut));

              ResultMeters = 0;
              ResultSessionMeters = 0;

              return false;
            }

            ResultMeters = _reader.GetDecimal(0);
            ResultSessionMeters = _reader.GetDecimal(1);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateCageMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update table CAGE_METERS & CAGE_SESSION_METERS
    //
    //  PARAMS :
    //      - INPUT :
    //          - Group: String
    //          - Subject: String
    //          - Value: Decimal
    //          - OperationType: UpdateCageSystemMeters
    //
    //      - OUTPUT :ValueOut
    //          - ValueOut: Decimal
    //
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES :
    // 
    public static Boolean CashierSessionClose_UpdateCageSystemMeters(Int64 CashierSessionId, SqlTransaction Trx)
    {
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("CageUpdateSystemMeters", Trx.Connection, Trx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;

          _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_cmd.Parameters.Add("@pIsClosingCashierSession", SqlDbType.Bit).Value = true;

          _sql_cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // CashierSessionClose_UpdateCageSystemMeters

    public static Boolean CashierSessionReopen_UpdateCageSystemMeters(Int64 CashierSessionId, SqlTransaction Trx)
    {
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("CageUpdateSystemMeters", Trx.Connection, Trx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;

          _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_cmd.Parameters.Add("@pIsClosingCashierSession", SqlDbType.Bit).Value = false;

          _sql_cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // CashierSessionReopen_UpdateCageSystemMeters

    public static Boolean CreateCageMeters(Int64 SourceTargetId, Int64 ConceptId, Int64 CageSessionId, CreateCageMetersOption CreateMetersOption, SqlTransaction Trx)
    {
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("CageCreateMeters", Trx.Connection, Trx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;

          _sql_cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = SourceTargetId;
          _sql_cmd.Parameters.Add("@pConceptId", SqlDbType.BigInt).Value = ConceptId;
          _sql_cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = CageSessionId;
          _sql_cmd.Parameters.Add("@CreateMetersOption", SqlDbType.Int).Value = (Int32)CreateMetersOption;

          _sql_cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // UpdateCageSystemMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update table Cage Meters from cashier movements close
    //
    //  PARAMS :
    //      - INPUT :
    //            - CashierSessionId
    //            - Trx

    //      
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES : 
    // 
    public static Boolean CashierSessionClose_UpdateCageMeters(Int64 CashierSessionId, out DataTable ConceptsCashierMovements, SqlTransaction Trx)
    {
      Decimal _out_result_meters;
      Decimal _out_result_session_meters;
      Boolean _result;
      Int32 _concept_id;
      Int32 _cm_type;
      Int32 _idx_movements;
      DataRow _dr_movement;
      Int64 _cage_session_id;
      Boolean _cage_session_id_null;

      ConceptsCashierMovements = new DataTable();

      if (!GetCashierConceptMovements(CashierSessionId, out ConceptsCashierMovements, Trx))
      {
        return false;
      }

      try
      {
        _cage_session_id_null = false;
        if (!Cage.GetCageSessionIdFromCashierSessionId(CashierSessionId, out _cage_session_id))
        {
          _cage_session_id_null = true;
        }

        _result = true;
        _idx_movements = 0;
        while (_result && _idx_movements < ConceptsCashierMovements.Rows.Count)
        {
          _dr_movement = ConceptsCashierMovements.Rows[_idx_movements];

          _cm_type = (Int32)_dr_movement["CM_TYPE"];
          if (_cm_type < (Int32)CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT)
          {
            _concept_id = _cm_type - (Int32)CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN;
          }
          else
          {
            _concept_id = _cm_type - (Int32)CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT;
          }



          _result = UpdateCageMeters((Decimal)_dr_movement["CM_ADD_AMOUNT"]
                                   , (Decimal)_dr_movement["CM_SUB_AMOUNT"]
                                   , _cage_session_id_null ? null : (Int64?)_cage_session_id
                                   , (Int32)CageSystemSourceTarget.Cashiers
                                   , (Int64)_concept_id
                                   , (String)_dr_movement["CM_CURRENCY_ISO_CODE"]
                                   , (CageCurrencyType)_dr_movement["CM_CAGE_CURRENCY_TYPE"]
                                   , UpdateCageMetersOperationType.Increment
                                   , UpdateCageMetersgetGetSessionMode.LastOpened
                                   , out _out_result_meters
                                   , out _out_result_session_meters
                                   , Trx);
          _idx_movements++;
        }

        return _result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateCageMeters_CashierSessionClose


    public static Boolean UpdateGlobalCageMeters_CashierSessionClose(Int64 CashierSessionId, CageDenominationsItems.CageDenominationsDictionary CageAmounts, SqlTransaction Trx)
    {
      Boolean _result = true;
      Decimal _out_result_meters;
      Decimal _out_result_session_meters;
      Int64 _cage_session_id;
      Boolean _cage_session_id_null;

      try
      {
        _cage_session_id_null = false;
        if (!Cage.GetCageSessionIdFromCashierSessionId(CashierSessionId, out _cage_session_id))
        {
          _cage_session_id_null = true;
        }

        if (CageAmounts == null)
        {
          return true;
        }

        foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_amount in CageAmounts)
        {
          _result = UpdateCageMeters((Decimal)_cur_amount.Value.TotalAmount
                                         , (Decimal)0
                                         , _cage_session_id_null ? null : (Int64?)_cage_session_id
                                         , (Int32)CageSystemSourceTarget.Cashiers
                                         , 0
                                         , _cur_amount.Key.ISOCode
                                         , _cur_amount.Key.Type // TODO JML 22-Abr-2016: check Cage currency type
                                         , UpdateCageMetersOperationType.Increment
                                         , UpdateCageMetersgetGetSessionMode.LastOpened
                                         , out _out_result_meters
                                         , out _out_result_session_meters
                                         , Trx);


        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }//UpdateGlobalCageMeters_CashierSessionClose

    //------------------------------------------------------------------------------
    // PURPOSE : Get a relation between a Cage source target and a Cage concept
    //
    //  PARAMS :
    //      - INPUT :
    //            - ConceptId
    //            - SourceTargetId
    //            - Type
    //            - OnlyNationalCurrency
    //            - CashierAction
    //            - Enabled
    //            - PriceFactor
    //            - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES :
    // 
    public static Boolean CreateCageSourceTargetConcepts(Int64 ConceptId
                                                       , Int64 SourceTargetId
                                                       , CageSourceTargetType Type
                                                       , Boolean OnlyNationalCurrency
                                                       , CageSourceTargetCashierAction CashierAction
                                                       , Boolean Enabled
                                                       , Decimal PriceFactor
                                                       , SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("     INSERT INTO   CAGE_SOURCE_TARGET_CONCEPTS ");
        _sb.AppendLine("                 ( CSTC_CONCEPT_ID ");
        _sb.AppendLine("                 , CSTC_SOURCE_TARGET_ID ");
        _sb.AppendLine("                 , CSTC_TYPE ");
        _sb.AppendLine("                 , CSTC_ONLY_NATIONAL_CURRENCY ");
        _sb.AppendLine("                 , CSTC_CASHIER_ACTION ");
        _sb.AppendLine("                 , CSTC_ENABLED ");
        _sb.AppendLine("                 , CSTC_PRICE_FACTOR) ");
        _sb.AppendLine("          VALUES ");
        _sb.AppendLine("                 ( @ConceptId ");
        _sb.AppendLine("                 , @SourceTargetId ");
        _sb.AppendLine("                 , @Type ");
        _sb.AppendLine("                 , @OnlyNationalCurrency ");
        _sb.AppendLine("                 , @CashierAction ");
        _sb.AppendLine("                 , @Enabled ");
        _sb.AppendLine("                 , @PriceFactor) ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@ConceptId", SqlDbType.BigInt).Value = ConceptId;
          _sql_cmd.Parameters.Add("@SourceTargetId", SqlDbType.BigInt).Value = SourceTargetId;
          _sql_cmd.Parameters.Add("@Type", SqlDbType.Int).Value = (Int32)Type;
          _sql_cmd.Parameters.Add("@OnlyNationalCurrency", SqlDbType.Bit).Value = OnlyNationalCurrency;
          _sql_cmd.Parameters.Add("@Enabled", SqlDbType.Bit).Value = Enabled;
          _sql_cmd.Parameters.Add("@PriceFactor", SqlDbType.Decimal).Value = PriceFactor;

          if (CashierAction == CageSourceTargetCashierAction.None)
          {
            _sql_cmd.Parameters.Add("@CashierAction", SqlDbType.Int).Value = System.DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@CashierAction", SqlDbType.Int).Value = (Int32)CashierAction;
          }


          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("CreateCageSourceTargetConcepts. Cage Meters relation has not been created. ConceptId={0} SourceTargetId={1} Type={2}", ConceptId, SourceTargetId, Type));

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CreateCageSourceTargetConcepts

    //------------------------------------------------------------------------------
    // PURPOSE : Get cashier movements whose type belongs to cage concepts.
    //
    //  PARAMS :
    //      - INPUT :
    //            - CashierSessionId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - DtCashierMovements
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES :
    // 
    public static Boolean GetCashierConceptMovements(Int64 CashierSessionId, out DataTable DtCashierMovements, SqlTransaction SqlTrx)
    {
      StringBuilder _sql_sb;

      DtCashierMovements = new DataTable();

      try
      {
        _sql_sb = new StringBuilder();

        _sql_sb.AppendLine(" SELECT  CM_TYPE ");
        _sql_sb.AppendLine("	     , CM_CURRENCY_ISO_CODE ");
        _sql_sb.AppendLine("	     , CM_CAGE_CURRENCY_TYPE ");
        _sql_sb.AppendLine("	     , SUM(CM_SUB_AMOUNT) CM_SUB_AMOUNT ");
        _sql_sb.AppendLine("	     , SUM(CM_ADD_AMOUNT) CM_ADD_AMOUNT ");
        _sql_sb.AppendLine("    FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID ");
        _sql_sb.AppendLine("   WHERE CM_SESSION_ID = @pCashierSessionId ");
        _sql_sb.AppendLine("     AND (  (CM_TYPE > @pCashierByConceptIn AND CM_TYPE < @pCashierByConceptOut) ");
        _sql_sb.AppendLine("          OR(CM_TYPE > @pCashierByConceptOut AND CM_TYPE <= @pCashierByConceptLast)) ");
        _sql_sb.AppendLine("GROUP BY CM_TYPE, CM_CURRENCY_ISO_CODE, CM_CAGE_CURRENCY_TYPE ");

        using (SqlCommand _cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCashierByConceptOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT;
          _cmd.Parameters.Add("@pCashierByConceptIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN;
          _cmd.Parameters.Add("@pCashierByConceptLast", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_LAST;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(DtCashierMovements);

          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCashierConceptMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Get all items from cage_source_target_concepts table
    //
    //  PARAMS :
    //      - INPUT :
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - DtCageSourceTargetConcepts
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES :
    // 
    public static Boolean GetCageSourceTargetConcepts(out DataTable DtCageSourceTargetConcepts, SqlTransaction SqlTrx)
    {
      StringBuilder _sql_sb;

      DtCageSourceTargetConcepts = new DataTable();

      try
      {
        _sql_sb = new StringBuilder();

        _sql_sb.AppendLine(" SELECT  CSTC_CONCEPT_ID ");
        _sql_sb.AppendLine(" 	    ,  CSTC_SOURCE_TARGET_ID ");
        _sql_sb.AppendLine(" 	    ,  CSTC_TYPE ");
        _sql_sb.AppendLine(" 	    ,  CSTC_ONLY_NATIONAL_CURRENCY ");
        _sql_sb.AppendLine(" 	    ,  CSTC_CASHIER_ACTION ");
        _sql_sb.AppendLine(" 	    ,  ISNULL(CSTC_ENABLED, 0) CSTC_ENABLED ");
        _sql_sb.AppendLine(" 	    ,  CSTC_PRICE_FACTOR ");
        _sql_sb.AppendLine("   FROM  CAGE_SOURCE_TARGET_CONCEPTS ");

        using (SqlCommand _cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(DtCageSourceTargetConcepts);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCageSourceTargetConcepts

    //------------------------------------------------------------------------------
    // PURPOSE : Create a Cage Meters and Cage Session Meters foreach cage_source_target_concepts
    //
    //  PARAMS :
    //      - INPUT :
    //            - CageSessionId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - DtCageSourceTargetConcepts
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES :
    // 
    public static Boolean UpdateAndCreateCageSessionMeters(SqlTransaction SqlTrx)
    {
      DataTable _dt_cage_source_target_concepts;
      Boolean _result;
      DataRow _dr;
      Int32 _idx;

      _dt_cage_source_target_concepts = new DataTable();
      _result = true;
      _idx = 0;

      try
      {
        // Get all items from cage_source_target_concepts table
        if (!GetCageSourceTargetConcepts(out _dt_cage_source_target_concepts, SqlTrx))
        {
          return false;
        }

        while (_result && _idx < _dt_cage_source_target_concepts.Rows.Count)
        {
          _dr = _dt_cage_source_target_concepts.Rows[_idx];
          _result = CreateCageMeters((Int64)_dr["CSTC_SOURCE_TARGET_ID"]
                                   , (Int64)_dr["CSTC_CONCEPT_ID"]
                                   , 0
                                   , CreateCageMetersOption.CageMeters_CageAllOpenSessionMeters
                                   , SqlTrx);
          _idx++;
        }

        return _result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Create a Cage Session Meters foreach cage_source_target_concepts
    //
    //  PARAMS :
    //      - INPUT :
    //            - CageSessionId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - DtCageSourceTargetConcepts
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES :
    // 
    public static Boolean OpenCage_CreateCageSessionMeters(Int64 CageSessionId, SqlTransaction SqlTrx)
    {
      DataTable _dt_cage_source_target_concepts;
      Boolean _result;
      DataRow _dr;
      Int32 _idx;

      _dt_cage_source_target_concepts = new DataTable();
      _result = true;
      _idx = 0;

      try
      {
        // Get all items from cage_source_target_concepts table
        if (!GetCageSourceTargetConcepts(out _dt_cage_source_target_concepts, SqlTrx))
        {
          return false;
        }

        while (_result && _idx < _dt_cage_source_target_concepts.Rows.Count)
        {
          _dr = _dt_cage_source_target_concepts.Rows[_idx];
          _result = CreateCageMeters((Int64)_dr["CSTC_SOURCE_TARGET_ID"]
                                   , (Int64)_dr["CSTC_CONCEPT_ID"]
                                   , CageSessionId
                                   , CreateCageMetersOption.CageSessionMeters
                                   , SqlTrx);
          _idx++;
        }

        return _result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean DeleteCageSourceTargetConcepts(Int64 ConceptId, SqlTransaction Trx)
    {
      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine("DELETE FROM   CAGE_SOURCE_TARGET_CONCEPTS");
        _sb.AppendLine("      WHERE   CSTC_SOURCE_TARGET_ID = @pConcept_Id");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pConcept_Id", SqlDbType.BigInt).Value = ConceptId;
          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DeleteCageSourceTargetConcepts

    public static Boolean DeleteCageMeters(Int64 ConceptId, SqlTransaction Trx)
    {
      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine("DELETE FROM   CAGE_METERS                       ");
        _sb.AppendLine("      WHERE   CM_SOURCE_TARGET_ID = @pConcept_Id");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pConcept_Id", SqlDbType.BigInt).Value = ConceptId;
          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DeleteCageMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Return value tip from chips
    //
    //  PARAMS :
    //      - INPUT :
    //          - CageAmounts: CageAmountsDictionary
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Tip: Decimal
    //
    //   NOTES :
    //  
    public static SortedDictionary<CurrencyIsoType, Decimal> GetTipOfChips(DataTable ConceptsCashierMovements)
    {
      DataRow[] _rows_tips;
      Int32 _movement_type;
      SortedDictionary<CurrencyIsoType, Decimal> _tips_currency;
      Decimal _amount;
      CurrencyIsoType _currency_iso_type;

      _tips_currency = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _movement_type = (Int32)CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN + (Int32)CageMeters.CageConceptId.Tips;

      _rows_tips = ConceptsCashierMovements.Select("CM_TYPE = " + _movement_type);

      foreach (DataRow _row in _rows_tips)
      {
        _currency_iso_type = new CurrencyIsoType((String)_row[1], (CurrencyExchangeType)_row[2]);
        _amount = (Decimal)_row[4];

        if (_tips_currency.ContainsKey(_currency_iso_type))
        {
          _tips_currency[_currency_iso_type] += _amount;
        }
        else
        {
          _tips_currency.Add(_currency_iso_type, _amount);
        }
      }

      return _tips_currency;
    } // GetTipOfChips

    public static Decimal CalculateTips(SortedDictionary<CurrencyIsoType, Decimal> Tips)
    {
      Decimal _total_tips;
      String _national_currency;
      CurrencyExchange _currency_exchange;
      CurrencyExchangeResult _exchange_result;

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
      _total_tips = 0;

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _tip in Tips)
      {
        if(FeatureChips.IsChipsType(_tip.Key.Type) && _tip.Key.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          continue;
        }

        if (_tip.Key.IsoCode == _national_currency)
        {
          _total_tips += _tip.Value;
        }
        else
        {
          _currency_exchange = new CurrencyExchange();
          _exchange_result = new CurrencyExchangeResult();

          using (DB_TRX _db_trx = new DB_TRX())
          {
            CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, _tip.Key.IsoCode, out _currency_exchange, _db_trx.SqlTransaction);
          }
          _currency_exchange.ApplyExchange(_tip.Value, out  _exchange_result);
          _total_tips += _exchange_result.GrossAmount;
        }
      }

      return _total_tips;
    } // CalculateTips

    public static Boolean ExistConceptsMovements(Int64 CashierSessionId, String IsoCode, CageCurrencyType CageCurrencyType, out Boolean Exists, SqlTransaction SqlTrx)
    {
      Exists = false;

      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT  COUNT(*) ");
        _sb.AppendLine("         FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID ");
        _sb.AppendLine("        WHERE CM_SESSION_ID = @pCashierSessionId ");
        _sb.AppendLine("          AND CM_TYPE >= @pCashierByConceptIn ");
        _sb.AppendLine("          AND CM_TYPE <= @pCashierByConceptLast ");
        _sb.AppendLine("          AND CM_CURRENCY_ISO_CODE = @pIsoCode ");
        _sb.AppendLine("          AND CM_CAGE_CURRENCY_TYPE = @pCageCurrencyType ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_cmd.Parameters.Add("@pCashierByConceptIn", SqlDbType.BigInt).Value = CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN;
          _sql_cmd.Parameters.Add("@pCashierByConceptLast", SqlDbType.BigInt).Value = CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_LAST;
          _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IsoCode;
          _sql_cmd.Parameters.Add("@pCageCurrencyType", SqlDbType.Int).Value = CageCurrencyType;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              Exists = _reader.GetInt32(0) > 0;

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static bool CreateItemsMovementDetails(long movementId, CageDenominationsItems.CageDenominationsDictionary cageAmounts, SqlTransaction sqlTrx)
    {
      if (!ExecuteDeleteCageConceptMovementDetail(movementId, sqlTrx))
      {
        return false;
      }

      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _currency in cageAmounts)
      {
        if (_currency.Value.TotalAmount > 0)
        {
          if (!ExecuteInsertCageConceptMovementDetail(movementId, _currency.Value.TotalAmount, _currency.Key.ISOCode,
                                                     _currency.Key.Type, 0, sqlTrx))
          {
            return false;
          }
        }
      }

      return true;
    }

    public static Boolean CreateItemsMovementDetails(Int64 MovementId, DataTable ItemsValuesTable, DataTable TotalTable, DataTable CurrenciesAcepted, SqlTransaction Trx)
    {
      return CageMeters.CreateItemsMovementDetails(MovementId, ItemsValuesTable, TotalTable, CurrenciesAcepted, 0, Trx);
    }

    public static Boolean CreateItemsMovementDetails(Int64 MovementId, DataTable ItemsValuesTable, DataTable TotalTable, DataTable CurrenciesAcepted, Decimal TicketsAmount, SqlTransaction Trx)
    {

      Decimal _decimal;
      String _iso_code;
      CurrencyExchangeType _currency_exchange_type;
      Dictionary<CurrencyIsoType, Decimal> _global_dictionary;
      CurrencyIsoType _national_currency;
      CurrencyIsoType _currency_iso_type;
      CageCurrencyType _cage_currency_type;

      _national_currency = new CurrencyIsoType(CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CURRENCY);
      _currency_iso_type = new CurrencyIsoType(CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CURRENCY);
      _cage_currency_type = CageCurrencyType.Bill;

      if (!ExecuteDeleteCageConceptMovementDetail(MovementId, Trx))
      {
        return false;
      }


      _global_dictionary = new Dictionary<CurrencyIsoType, decimal>();

      foreach (DataRow _row in TotalTable.Rows)
      {
        _iso_code = _row[0].ToString();
        _currency_exchange_type = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row[2]);
        _decimal = 0;

        try
        {
          if (!String.IsNullOrEmpty(_row[11].ToString()))
          {
            _decimal = Format.ParseCurrency(_row[11].ToString());
          }

          if ((_decimal > 0)
            && ((_iso_code != Cage.CHIPS_ISO_CODE && !FeatureChips.IsChipsType(_currency_exchange_type))
            || (Format.ParseCurrency(_row[1].ToString()) != -100)))
          {
            if (_global_dictionary.ContainsKey(new CurrencyIsoType(_iso_code, _currency_exchange_type)))
            {
              _global_dictionary[new CurrencyIsoType(_iso_code, _currency_exchange_type)] = _global_dictionary[new CurrencyIsoType(_iso_code, _currency_exchange_type)] + _decimal;
            }
            else
            {
              _global_dictionary.Add(new CurrencyIsoType(_iso_code, _currency_exchange_type), _decimal);
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Message("Cage.CreateItemsMovementDetails: cannot insert data " + _ex);
          return false;
        }
      }

      if (_global_dictionary.ContainsKey(_national_currency))
      {
        _global_dictionary[_national_currency] = _global_dictionary[_national_currency] + TicketsAmount;
      }
      else
      {
        _global_dictionary.Add(_national_currency, TicketsAmount);
      }

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in _global_dictionary)
      {
        if (!ExecuteInsertCageConceptMovementDetail(MovementId, _currency.Value, _currency.Key.IsoCode,
                                                    FeatureChips.ConvertToCageCurrencyType(_currency.Key.Type), 0, Trx))
        {
          return false;
        }
      }

      foreach (DataRow _row in ItemsValuesTable.Rows)
      {
        _iso_code = _row["CONCEPT_ISO_CODE"].ToString();
        _cage_currency_type = (CageCurrencyType)_row["CONCEPT_CAGE_CURRENCY_TYPE"];

        if (Format.ParseCurrency(_row["CONCEPT_AMOUNT"].ToString()) != 0)
        {
          _decimal = Format.ParseCurrency(_row["CONCEPT_AMOUNT"].ToString());

          if (!ExecuteInsertCageConceptMovementDetail(MovementId, _decimal, _iso_code,
                                                    _cage_currency_type, Int64.Parse(_row[0].ToString()), Trx))
          {
            return false;
          }
        }
      }

      return true;

    } // CreateItemsMovementDetails

    public static DataTable GetItemsTableFromMovementsDetails(Int64 MovementId, DataTable ItemsValuesTable, DataTable CurrenciesAccepted)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return CageMeters.GetItemsTableFromMovementsDetails(MovementId, ItemsValuesTable, CurrenciesAccepted, _db_trx.SqlTransaction);
      }
    }

    public static DataTable GetItemsTableFromMovementsDetails(Int64 MovementId, DataTable ItemsValuesTable, DataTable CurrenciesAccepted, SqlTransaction SqlTrx)
    {
      DataTable _items_details;
      DataTable _return_table;
      DataRow[] _row_detail;
      String _iso_code;
      Int32 _iso_type;

      _return_table = ItemsValuesTable;

      _items_details = new DataTable();

      if (!CageMeters.GetCageMovementDetailsInfo(MovementId, out _items_details, SqlTrx))
      {
        return _return_table;
      }

      try
      {
        foreach (DataRow _dr in _return_table.Rows)
        {
          _iso_code = _dr["CONCEPT_ISO_CODE"].ToString();
          _iso_type = (Int32)_dr["CONCEPT_CAGE_CURRENCY_TYPE"];

          _row_detail = _items_details.Select("CCMD_CONCEPT_ID = " + _dr["CONCEPT_ID"].ToString() + " AND CCMD_ISO_CODE = '" + _iso_code + "' AND CCMD_CAGE_CURRENCY_TYPE = " + _iso_type.ToString() + " ", "CCMD_CONCEPT_ID ASC");

          foreach (DataRow _dr_details in _row_detail)
          {
            _dr["CONCEPT_AMOUNT"] = (Decimal)_dr_details["CCMD_VALUE"];
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _return_table;

    } // GetItemsTableFromMovementsDetails

    //------------------------------------------------------------------------------
    // PURPOSE : Undo CageMeters and CageSessionMeters by CageMovementId
    //
    //  PARAMS :
    //      - INPUT :
    //          - CageMovementId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES :
    //  
    public static Boolean UndoCageMeters(Int64 CageMovementId, Boolean OnlyUndoGlobal, SqlTransaction SqlTrx)
    {
      Int64 _cage_session_id;
      Int64 _source_target_id;
      Int32 _type; // CageOperationType
      Int64 _cashier_session_id;
      DataTable _cage_movement_details;
      Decimal _out_result_meters;
      Decimal _out_result_session_meters;
      Boolean _result;
      Int32 _concept_id;
      Int32 _cm_type;
      Int32 _idx_movements;
      DataRow _dr_movement;

      if (!GetCageMovementInfo(CageMovementId, out _cage_session_id, out _source_target_id, out _type, out _cashier_session_id, SqlTrx))
      {
        return false;
      }

      if (!GetCashierConceptMovements(_cashier_session_id, out _cage_movement_details, SqlTrx))
      {
        return false;
      }

      try
      {
        _result = true;
        _idx_movements = 0;

        CashierSessionReopen_UpdateCageSystemMeters(_cashier_session_id, SqlTrx);

        while (_result && _idx_movements < _cage_movement_details.Rows.Count)
        {
          _dr_movement = _cage_movement_details.Rows[_idx_movements];

          _cm_type = (Int32)_dr_movement["CM_TYPE"];
          if (_cm_type < (Int32)CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT)
          {
            _concept_id = _cm_type - (Int32)CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN;
          }
          else
          {
            _concept_id = _cm_type - (Int32)CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT;
          }



          _result = UpdateCageMeters(((Decimal)_dr_movement["CM_ADD_AMOUNT"])*-1
                                   , ((Decimal)_dr_movement["CM_SUB_AMOUNT"])*-1
                                   , (Int64)_cage_session_id
                                   , (Int32)CageSystemSourceTarget.Cashiers
                                   , (Int64)_concept_id
                                   , (String)_dr_movement["CM_CURRENCY_ISO_CODE"]
                                   , (CageCurrencyType)_dr_movement["CM_CAGE_CURRENCY_TYPE"]
                                   , UpdateCageMetersOperationType.Increment
                                   , UpdateCageMetersgetGetSessionMode.LastOpened
                                   , out _out_result_meters
                                   , out _out_result_session_meters
                                   , SqlTrx);
          _idx_movements++;
        }
       }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return true;
    } // UndoCageMeters

    #endregion //Public Methods

    #region Private Methods

    private static Boolean GetCageMovementInfo(Int64 CageMovementId
                                              , out Int64 CageSessionId
                                              , out Int64 SourceTargetId
                                              , out Int32 Type
                                              , out Int64 CashierSessionId
                                              , SqlTransaction SqlTrx)
    {
      CageSessionId = 0;
      SourceTargetId = 0;
      Type = 0;
      CashierSessionId = 0;

      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT  CGM_CAGE_SESSION_ID ");
        _sb.AppendLine("          , CGM_TYPE ");
        _sb.AppendLine("          , CGM_SOURCE_TARGET_ID ");
        _sb.AppendLine("          , CGM_CASHIER_SESSION_ID ");
        _sb.AppendLine("   FROM  CAGE_MOVEMENTS ");
        _sb.AppendLine("  WHERE  CGM_MOVEMENT_ID = @pMovementId ");
        _sb.AppendLine("    AND  CGM_TYPE IN(@pToCashier,@pToCustom,@pToGamingTable,@pToTerminal,@pFromCashier,@pFromCashierClosing,@pFromCustom,@pFromGamingTable,@pFromGamingTableClosing,@pFromTerminal, @FromGamingTableDropbox, @FromGamingTableDropboxWithExpected) ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = CageMovementId;

          _sql_cmd.Parameters.Add("@pToCashier", SqlDbType.BigInt).Value = Cage.CageOperationType.ToCashier;
          _sql_cmd.Parameters.Add("@pToCustom", SqlDbType.BigInt).Value = Cage.CageOperationType.ToCustom;
          _sql_cmd.Parameters.Add("@pToGamingTable", SqlDbType.BigInt).Value = Cage.CageOperationType.ToGamingTable;
          _sql_cmd.Parameters.Add("@pToTerminal", SqlDbType.BigInt).Value = Cage.CageOperationType.ToTerminal;
          _sql_cmd.Parameters.Add("@pFromCashier", SqlDbType.BigInt).Value = Cage.CageOperationType.FromCashier;
          _sql_cmd.Parameters.Add("@pFromCashierClosing", SqlDbType.BigInt).Value = Cage.CageOperationType.FromCashierClosing;
          _sql_cmd.Parameters.Add("@pFromCustom", SqlDbType.BigInt).Value = Cage.CageOperationType.FromCustom;
          _sql_cmd.Parameters.Add("@pFromGamingTable", SqlDbType.BigInt).Value = Cage.CageOperationType.FromGamingTable;
          _sql_cmd.Parameters.Add("@pFromGamingTableClosing", SqlDbType.BigInt).Value = Cage.CageOperationType.FromGamingTableClosing;
          _sql_cmd.Parameters.Add("@FromGamingTableDropbox", SqlDbType.BigInt).Value = Cage.CageOperationType.FromGamingTableDropbox;
          _sql_cmd.Parameters.Add("@FromGamingTableDropboxWithExpected", SqlDbType.BigInt).Value = Cage.CageOperationType.FromGamingTableDropboxWithExpected;
          _sql_cmd.Parameters.Add("@pFromTerminal", SqlDbType.BigInt).Value = Cage.CageOperationType.FromTerminal;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              CageSessionId = _reader.GetInt64(0);
              Type = _reader.GetInt32(1);
              CashierSessionId = _reader.GetInt64(3);

              switch ((Cage.CageOperationType)Type)
              {
                case Cage.CageOperationType.FromCashier:
                case Cage.CageOperationType.FromCashierClosing:
                case Cage.CageOperationType.ToCashier:
                  SourceTargetId = (Int32)CageMeters.CageSystemSourceTarget.Cashiers;
                  break;
                case Cage.CageOperationType.ToGamingTable:
                case Cage.CageOperationType.FromGamingTable:
                case Cage.CageOperationType.FromGamingTableDropbox:
                case Cage.CageOperationType.FromGamingTableDropboxWithExpected:
                case Cage.CageOperationType.FromGamingTableClosing:
                  SourceTargetId = (Int32)CageMeters.CageSystemSourceTarget.GamingTables;
                  break;
                case Cage.CageOperationType.ToTerminal:
                case Cage.CageOperationType.FromTerminal:
                  SourceTargetId = (Int32)CageMeters.CageSystemSourceTarget.Terminals;
                  break;
                default: SourceTargetId = _reader.GetInt64(2); break;

              }
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GetCageMovement

    public static Boolean GetCageMovementDetailsInfo(Int64 CageMovementId
                                          , out DataTable CageMovementDetails
                                          , SqlTransaction SqlTrx)
    {
      CageMovementDetails = new DataTable();
      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT  SUM(CCMD_VALUE) AS CCMD_VALUE   ");
        _sb.AppendLine("            , CCMD_ISO_CODE                ");
        _sb.AppendLine("            , CCMD_CAGE_CURRENCY_TYPE      ");
        _sb.AppendLine("            , CCMD_CONCEPT_ID              ");
        _sb.AppendLine("     FROM  CAGE_CONCEPT_MOVEMENT_DETAIL    ");
        _sb.AppendLine("    WHERE  CCMD_MOVEMENT_ID = @pMovementId ");
        _sb.AppendLine(" GROUP BY  CCMD_ISO_CODE, CCMD_CAGE_CURRENCY_TYPE, CCMD_CONCEPT_ID  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = CageMovementId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(CageMovementDetails);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GetCageMovement


    //------------------------------------------------------------------------------
    // PURPOSE : Check if a concept is in use:
    //            1) it has a meter with value witch is diferent to zero
    //             or
    //            2) it has a cashier movement.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ConceptId
    //
    //      - OUTPUT :
    //          - IsConceptInUse
    //
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    //   NOTES :
    //  
    public static Boolean IsConceptInUse(Int64 ConceptId, out Boolean IsConceptInUse)
    {
      IsConceptInUse = false;

      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine(" DECLARE @Exists AS BIT ");
        _sb.AppendLine(" SET @Exists = 0 ");

        _sb.AppendLine(" SELECT top 1 @Exists = 1 ");
        _sb.AppendLine("   FROM CAGE_SESSION_METERS ");
        _sb.AppendLine("  WHERE CSM_CONCEPT_ID = @pConceptId ");
        _sb.AppendLine("    AND (CSM_VALUE <> 0 OR CSM_VALUE_IN <> 0 OR CSM_VALUE_OUT <> 0) ");

        _sb.AppendLine(" IF @Exists = 0 BEGIN ");
        _sb.AppendLine("     IF EXISTS(SELECT 1 FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_SOURCE_TARGET_ID = @pSourceTargetCashier) BEGIN ");
        _sb.AppendLine("         SELECT TOP 1 @Exists = 1 ");
        _sb.AppendLine("           FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID ");
        _sb.AppendLine("          WHERE CM_TYPE in (1000000 + @pConceptId, 2000000 + @pConceptId)");
        _sb.AppendLine("     END ");
        _sb.AppendLine(" END ");

        _sb.AppendLine(" SELECT @Exists ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pConceptId", SqlDbType.BigInt).Value = ConceptId;
            _sql_cmd.Parameters.Add("@pSourceTargetCashier", SqlDbType.BigInt).Value = CageMeters.CageSystemSourceTarget.Cashiers;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                IsConceptInUse = (_reader.GetInt32(0) == 1);
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsConceptInUse

    public static Boolean CageSessionClose_SaveStock(Int64 CageSessionId, SqlTransaction SqlTrx)
    {
      try
      {
        Int32 _res;

        using (SqlCommand _sql_cmd = new SqlCommand("CageStockOnCloseSession", SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;
          _sql_cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = CageSessionId;

          _res = _sql_cmd.ExecuteNonQuery();

          if (_res < 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    public static DataTable CageSessionGetStockTable(Int64 CageSessionId, SqlTransaction SqlTrx)
    {
      DataTable _dt_res;
      _dt_res = new DataTable();

      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine("   DECLARE @List VARCHAR(MAX)                                                                                                       ");
        _sb.AppendLine("   SELECT @List = CGS_CAGE_STOCK FROM CAGE_SESSIONS WHERE CGS_CAGE_SESSION_ID = @pCageSessionId                                     ");

        _sb.AppendLine("   IF @List IS NOT NULL                                                                                                             ");
        _sb.AppendLine("   BEGIN                                                                                                                            ");
        _sb.AppendLine("    SELECT SST_VALUE AS STOCK_ROW INTO #TMP_STOCK_ROWS FROM [SplitStringIntoTable] (@List, '|', 1)                                  ");

        _sb.AppendLine("    SELECT REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 4), '|', '.') AS CGS_ISO_CODE,                        ");
        _sb.AppendLine("           CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 3), '|', '.') AS MONEY) AS CGS_DENOMINATION,     ");
        _sb.AppendLine("           CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 2), '|', '.') AS DECIMAL(18,2)) AS CGS_QUANTITY, ");
        _sb.AppendLine("           CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 1), '|', '.') AS INT) AS CGS_CAGE_CURRENCY_TYPE  ");
        _sb.AppendLine("      INTO #TMP_STOCK                                                                                                               ");
        _sb.AppendLine("      FROM #TMP_STOCK_ROWS                                                                                                          ");

        _sb.AppendLine("    SELECT * FROM #TMP_STOCK                                                                                                        ");
        _sb.AppendLine("  ORDER BY CGS_ISO_CODE                                                                                                             ");
        _sb.AppendLine("         , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION                                                                    ");
        _sb.AppendLine("                ELSE CGS_DENOMINATION * (-100000) END                                                                               ");

        _sb.AppendLine("    DROP TABLE #TMP_STOCK_ROWS                                                                                                      ");
        _sb.AppendLine("    DROP TABLE #TMP_STOCK                                                                                                           ");
        _sb.AppendLine("   END                                                                                                                              ");


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = CageSessionId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_dt_res);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_res;
    }

    private static bool ExecuteInsertCageConceptMovementDetail(long movementID, decimal currency, string isoCode, CageCurrencyType currencyType, long conceptID, SqlTransaction sqlTrx)
    {
      StringBuilder _sb_movement_detail;
      bool _success;

      _success = true;
      _sb_movement_detail = new StringBuilder();
      _sb_movement_detail.AppendLine("   INSERT INTO   CAGE_CONCEPT_MOVEMENT_DETAIL   ");
      _sb_movement_detail.AppendLine("               ( CCMD_MOVEMENT_ID               ");
      _sb_movement_detail.AppendLine("               , CCMD_VALUE                     ");
      _sb_movement_detail.AppendLine("               , CCMD_ISO_CODE                  ");
      _sb_movement_detail.AppendLine("               , CCMD_CAGE_CURRENCY_TYPE        ");
      _sb_movement_detail.AppendLine("               , CCMD_CONCEPT_ID                ");
      _sb_movement_detail.AppendLine("               )                                ");
      _sb_movement_detail.AppendLine("        VALUES                                  ");
      _sb_movement_detail.AppendLine("               ( @pMovementId                   ");
      _sb_movement_detail.AppendLine("               , @pValue                        ");
      _sb_movement_detail.AppendLine("               , @pIsoCode                      ");
      _sb_movement_detail.AppendLine("               , @pIsoType                      ");
      _sb_movement_detail.AppendLine("               , @ConceptId                     ");
      _sb_movement_detail.AppendLine("               )                                ");


      using (SqlCommand _sql_cmd_update = new SqlCommand(_sb_movement_detail.ToString(), sqlTrx.Connection, sqlTrx))
      {
        _sql_cmd_update.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = movementID;
        _sql_cmd_update.Parameters.Add("@pValue", SqlDbType.Money).Value = currency;
        _sql_cmd_update.Parameters.Add("@pIsoCode", SqlDbType.NVarChar, 3).Value = isoCode;
        _sql_cmd_update.Parameters.Add("@pIsoType", SqlDbType.Int).Value = currencyType;
        _sql_cmd_update.Parameters.Add("@ConceptId", SqlDbType.BigInt).Value = conceptID;
        try
        {
          if (_sql_cmd_update.ExecuteNonQuery() < 1)
          {
            // Not all data has been inserted
            Log.Message("Cage.CreateItemsMovementDetails: Not all data has been updated");
            _success = false;
          }
        }
        catch (Exception ex)
        {
          Log.Message("Cage.CreateItemsMovementDetails: Not all data has been updated");
          Log.Message(ex.Message);
          _success = false;
        }
      }

      return _success;
    }

    private static bool ExecuteDeleteCageConceptMovementDetail(long movementId, SqlTransaction sqlTrx)
    {
      StringBuilder _sb_delete;
      bool _success;

      _sb_delete = new StringBuilder();
      _sb_delete.AppendLine("      DELETE FROM CAGE_CONCEPT_MOVEMENT_DETAIL ");
      _sb_delete.AppendLine("            WHERE CCMD_MOVEMENT_ID = @pMovementId ");

      using (SqlCommand _sql_cmd_update = new SqlCommand(_sb_delete.ToString(), sqlTrx.Connection, sqlTrx))
      {
        _sql_cmd_update.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = movementId;

        try
        {
          _sql_cmd_update.ExecuteNonQuery();
          _success = true;
        }
        catch (Exception _ex)
        {
          Log.Message("Cage.CreateItemsMovementDetails: cannot delete data " + _ex);
          _success = false;
        }
      }

      return _success;
    }

    #endregion // Private Methods
  } // class CageMeters
}
