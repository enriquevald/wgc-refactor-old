//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Reports.cs
// 
//   DESCRIPTION: Reports Class
//
//        AUTHOR: DDM
// 
// CREATION DATE: 27-APR-2012
// 
// REVISION HISTORY:
// 
// Date         Author Description
// -----------  ------ ----------------------------------------------------------
// 27-APR-2012  DDM    First release.
// 07-MAY-2012  JCM    Only save segmentation XML report, if there aren't open sessions
// 09-MAY-2012  JCM    Fixed Bug #295: Generate DataTable only if there are not open sessions and is necessary 
// 23-JAN-2013  DDM    Fixed Bug #470.
// 16-JUN-2014  RMS    published GetLiteralsFromSegmentationAgroups() (Layout)
// 11-JUL-2014  XIT    Added MultiLanguage support
// 28-JAN-2015  DCS    In TITO mode anonymous accounts don't displayed
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace WSI.Common
{
  public static class Reports
  {  

    #region Constants
    
    // constants for Segmentation Report.
    public const String SR_SUBTABLE_NAME_HOLDER_LEVEL = "HOLDER_LEVEL";
    public const String SR_SUBTABLE_NAME_GAME_TYPE = "GAME_TYPE";
    public const String SR_SUBTABLE_NAME_HOLDER_GENDER = "HOLDER_GENDER";
    public const String SR_SUBTABLE_NAME_YEARS_OLD = "YEARS_OLD";
    public const String SR_SUBTABLE_NAME_ADT_GROUP = "ADT_GROUP";

    public const String SR_SQL_COLUMN_TD_AGROUP_CODE = "TD_AGROUP";
    public const String SR_SQL_COLUMN_TD_AGROUP_TYPE = "TD_TABLE";
    public const String SR_SQL_COLUMN_TD_AFORO = "TD_AFORO";
    public const String SR_SQL_COLUMN_TD_THEORETICAL_HOLD = "TD_THEORETICAL_HOLD";
    public const String SR_SQL_COLUMN_TD_VISITS = "TD_VISITS";
    public const String SR_SQL_COLUMN_TD_ADT = "TD_ADT";
    public const String SR_SQL_COLUMN_TD_AGROUP_NAME = "TD_AGROUP_NAME";
    public const Int32 SEGMENTATION_REPORT_NAME = 0;

    #endregion // Constants

    #region Structs

    public class AgroupNamesFromSegmentation
    {
      public Dictionary<Int32, String> holder_level = new Dictionary<Int32, String>();
      public Dictionary<Int32, String> game_type = new Dictionary<Int32, String>();
      public Dictionary<Int32, String> holder_gender = new Dictionary<Int32, String>();
      public Dictionary<Int32, String> years_old = new Dictionary<Int32, String>();
      public Dictionary<Int32, String> adt_group = new Dictionary<Int32, String>();
    };

    #endregion // Structs

    #region Public Methods

    // PURPOSE : Get level names
    //
    //  PARAMS :
    //     - INPUT:
    //           - SqlTrx
    //          
    //     - OUTPUT:
    //           - None
    //
    // RETURNS :
    //     - Datatable with the level names
    public static DataTable GetLevelNames(SqlTransaction SqlTrx)
    {
      String _sql_txt;
      DataTable _dt;

      try
      {
        _sql_txt = "SELECT   CAST(SUBSTRING(GP_SUBJECT_KEY, 6, 2) AS INT) TD_LEVEL" +
                   "       , GP_KEY_VALUE " +
                   "  FROM   GENERAL_PARAMS " +
                   " WHERE   GP_GROUP_KEY      = 'PlayerTracking' " +
                   "   AND   GP_SUBJECT_KEY LIKE 'Level%.Name' ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {
          _dt = new DataTable();

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.SelectCommand = _sql_cmd;
            _sql_da.Fill(_dt);

            return _dt;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    } // GetLevelNames

    // PURPOSE : Get Game names
    //
    //  PARAMS :
    //     - INPUT:
    //           - None
    //          
    //     - OUTPUT:
    //           - None
    //
    // RETURNS :
    //     - Datatable with the game names
    public static DataTable GetGameNames(SqlTransaction SqlTrx)
    {
      String _sql_txt;
      DataTable _dt;

      try
      {
        _sql_txt = "SELECT   GT_GAME_TYPE" +
                   "       , GT_NAME " +
                   "  FROM   GAME_TYPES " +
                   "  WHERE  GT_LANGUAGE_ID = @pLanguageId";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pLanguageId", SqlDbType.Int).Value = Resource.LanguageId;
          _dt = new DataTable();

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.SelectCommand = _sql_cmd;
            _sql_da.Fill(_dt);

            return _dt;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    } // GetGameNames
       
    // PURPOSE : Return Datatable with the segmentation report, 
    //           saves this in BD ( SaveTableXML ) if there aren't  
    //           open play sessions
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateMonth: Month of the Query
    //          - AgroupNamesFromSegmentation: AgroupNames
    //          - CheckExistReport:  Boolean for check if exist the report in BD. ( True: check. False: not  check)
    //          - GetReportWithOpenSessions:  Indicates if has to obtain (generate) the report although there are open play sessions.
    //
    //      - OUTPUT :
    //          - DtRSegmentationMonth: Datatable with report Segmentation from the new month 
    //          - ReportExistsInDB: True Report is saved
    //                              False Report is not saved (generated previously, open play sessions or error)
    // RETURNS :
    //      - Boolean: True is ok
    //                 False is not ok
    public static Boolean SetReportSegmentationFromNewMonth(DateTime DateMonth, 
                                                            AgroupNamesFromSegmentation AgroupNames,
                                                            Boolean CheckReport, 
                                                            Boolean GetReportWithOpenSessions,     
                                                            out DataTable DtRSegmentationMonth,
                                                            out Boolean ReportExistsInDB,
                                                            out Int32 CurrentPlaySessions)
    {
      DataTable _dt_rs;
      String _sql_txt;
      DataSet _ds;
      Int32 _idx;

      CurrentPlaySessions = 0;
      DtRSegmentationMonth = new DataTable();
      ReportExistsInDB = false;
      
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // If error, exit
          if (!ExistsOpenedSessions(DateMonth, out CurrentPlaySessions))
          {
            ReportExistsInDB = false;

            return false;
          }

          // If there are open play sessions and is not necessary to generate the segmentation report, exit
          // In WCP Service, there is no need to genereate the report, because is not going to be saved.
          // In GUI, although there are open play sessions, it is needed to generate the report.

          if (CurrentPlaySessions != 0 && !GetReportWithOpenSessions)
          {
            ReportExistsInDB = false;

            return true;
          }

          // Loop of two attempts to save the report in BD and get the Datatable of Segmentation Report.           
          for (_idx = 0; _idx < 2; _idx++)
          {
            _dt_rs = new DataTable();

            if (CheckReport)
            {
              // Check if exist the report
              if (CheckExistReport(SEGMENTATION_REPORT_NAME, DateMonth, _db_trx.SqlTransaction, out _dt_rs))
              {
                DtRSegmentationMonth = _dt_rs;
                ReportExistsInDB = true;

                return true;
              }
            }
            
            // Get SQL Query Segmentation report
            _sql_txt = GetQuerySegmentationReport();

            using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pIniDate", SqlDbType.DateTime).Value = DateMonth;
              _sql_cmd.Parameters.Add("@pAcTypeVirtualUser", SqlDbType.Int).Value = WSI.Common.AccountType.ACCOUNT_VIRTUAL_TERMINAL;
              _sql_cmd.Parameters.Add("@pAcTypeVirtualCashier", SqlDbType.Int).Value = WSI.Common.AccountType.ACCOUNT_VIRTUAL_CASHIER;             
              
              using (SqlDataAdapter _sql_da = new SqlDataAdapter())
              {
                _sql_da.SelectCommand = _sql_cmd;
                _sql_da.Fill(_dt_rs);
              }
            }

            // Specific format to segmentation report.
            _dt_rs = TableFormat(_dt_rs, AgroupNames);
            _dt_rs.TableName = "T_" + DateMonth.Year + "_" + DateMonth.Month.ToString("00");

            if (CurrentPlaySessions != 0)
            {
              DtRSegmentationMonth = _dt_rs;
              ReportExistsInDB = false;

              return true;
            }

            _ds = new DataSet();
            // Add table to local dataset to save it as XML
            _ds.Tables.Add(_dt_rs);
            // Save XML in the BD 
            if (SaveTableXML(_ds.GetXml(), DateMonth, SEGMENTATION_REPORT_NAME, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();
              _ds.Tables.Remove(_dt_rs);

              DtRSegmentationMonth = _dt_rs;
              ReportExistsInDB = true;

              return true;
            }

            // Has not saved the INSERT of report. 
            // We Check if exist the report (CheckExistReport)
            CheckReport = true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);        
      }

      ReportExistsInDB = false;

      return false;
    } // SetReportSegmentationFromNewMonth

    // PURPOSE : Function that save tables it as XML in BD
    //
    //  PARAMS :
    //     - INPUT:
    //           - Xml: Table it as XML
    //           - ReportDate: Date from report (Date query)
    //           - ReportName: Report name
    //           - SqlTrx
    //     - OUTPUT:
    //           - None
    //
    // RETURNS :
    //     - Boolean
    public static Boolean SaveTableXML(String Xml, 
                                       DateTime ReportDate, 
                                       Int32 ReportName, 
                                       SqlTransaction SqlTrx)
    {
      string _sql_txt = null;

      _sql_txt = "INSERT INTO   REPORTS " +
                            " ( REP_TYPE " +
                            " , REP_DATE " +
                            " , REP_DATA " +
                            " ) " +
                     " VALUES " +
                            " ( @pReportName" +
                            " , @pReportDate" +
                            " , @pXml)";

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pReportName", SqlDbType.Int).Value = ReportName;
          _sql_cmd.Parameters.Add("@pReportDate", SqlDbType.DateTime).Value = ReportDate.Date;
          _sql_cmd.Parameters.Add("@pXml", SqlDbType.Xml).Value = Xml;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false; 
    } // SaveTableXml

    // PURPOSE : Function that checks if exist report
    //
    //  PARAMS :
    //     - INPUT:
    //           - ReportName: Report name
    //           - DateCheck: Date to check
    //           - SqlTrx
    //          
    //     - OUTPUT:
    //           - DtRSegmentationMonth: Datatable report
    //
    // RETURNS :
    //     - True, if exist. False, if not exist        
    public static Boolean CheckExistReport(Int32 ReportType,
                                           DateTime DateFromCheck,
                                           SqlTransaction SqlTrx,
                                           out DataTable DtRSegmentationMonth)
    {
      String _sql_txt;
      DataSet _ds_report;
      Object _obj;

      DtRSegmentationMonth = new DataTable();
      
      try
      {
        _sql_txt = " SELECT   REP_DATA " +
                   "   FROM   REPORTS" +
                   "  WHERE   REP_TYPE  = @pReportType " +
                   "    AND   REP_DATE >= @pDateFromCheck  " +
                   "    AND   REP_DATE  < @pDateToCheck ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {          
          _sql_cmd.Parameters.Add("@pReportType", SqlDbType.Int).Value = ReportType;
          _sql_cmd.Parameters.Add("@pDateFromCheck", SqlDbType.DateTime).Value = DateFromCheck.Date;
          _sql_cmd.Parameters.Add("@pDateToCheck", SqlDbType.DateTime).Value = DateFromCheck.Date.AddDays(1);

          _obj = _sql_cmd.ExecuteScalar();
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          _ds_report = new DataSet();
          Misc.DataSetFromXml(_ds_report, (String)_obj, XmlReadMode.Auto);
          DtRSegmentationMonth = _ds_report.Tables[0];
          DtRSegmentationMonth.TableName = "T_" + DateFromCheck.Year + "_" + DateFromCheck.Month.ToString("00");
          _ds_report.Tables.Remove(DtRSegmentationMonth);

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // CheckExistReport
    
    // PURPOSE : Function to check if exist open play sessions 
    //
    //  PARAMS :
    //     - INPUT:
    //           - DateCheck: Initial date for check
    //     - OUTPUT:
    //           - ExistsOpenSessions: Return if exists any openned sessions
    //                                 between [DateCheck to (DateCheck + 1 month)]
    //
    // RETURNS :
    //     - True, if Ok. False, if Error  
    private static Boolean ExistsOpenedSessions(DateTime DateMonth, out Int32 OpenSessions)
    {
      String _sql_txt;

      OpenSessions = 0;
      
      try
      {
        _sql_txt = "SELECT   COUNT(*) " +
                     "FROM   PLAY_SESSIONS " +
                    "WHERE   PS_STATUS = 0 " +               //WCP_PlaySessionStatus.Opened = 0
                      "AND   PS_STARTED >= @pDateStarted " +
                      "AND   PS_STARTED  < DATEADD (MONTH, 1, @pDateStarted) " +
                      "AND   DATEDIFF(HOUR, PS_STARTED, GETDATE()) < 24 ";

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pDateStarted", SqlDbType.DateTime).Value = DateMonth;
            OpenSessions = ((Int32)_db_trx.ExecuteScalar(_sql_cmd));
            
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // ExistsOpenedSessions

    // PURPOSE : Function that saves segmentation report
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateMonth:  Month of the Query and month that will saves in the table from reports
    //          - ReportExistsInDb: Return True if is saved on this execution or previously exists the month report
    //                                     False if is not stored in DB
    //      - OUTPUT : 
    //          - None
    //
    // RETURNS :
    //      - Boolean: True is ok
    //                 False is not ok
    public static Boolean SegmentationReportSave(DateTime DateMonth, out Boolean ReportExistsInDb, out Int32 CurrentPlaySessions)
    {
      DataTable _dt_rs;
      Reports.AgroupNamesFromSegmentation _literals;
      ReportExistsInDb = false;
      CurrentPlaySessions = 0;
      try
      {
        _literals = Reports.GetLiteralsFromSegmentationAgroups();

        return Reports.SetReportSegmentationFromNewMonth(DateMonth, _literals, true, false, out _dt_rs, out ReportExistsInDb, out CurrentPlaySessions);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    // PURPOSE : Function to delete reports generated on the future
    //
    //  PARAMS :
    //      - INPUT :
    //          - None 
    //          
    //      - OUTPUT : 
    //          - None
    //
    // RETURNS :
    //          - True: correctly executed 
    //          - False: otherwise
    public static Boolean DeleteReportsGeneratedOnTheFuture()
    {
      DateTime _now;
      StringBuilder _sb;

      try
      {
        _now = WGDB.Now.Date;
        _sb = new StringBuilder();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb.AppendLine("DELETE                     ");
          _sb.AppendLine("  FROM   REPORTS           ");
          _sb.AppendLine(" WHERE   REP_DATE >= @pDate");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = _now.Date;
            _db_trx.ExecuteNonQuery(_sql_cmd);
          }

          _db_trx.Commit();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }


    #endregion // Public Methods

    #region Private Methods

    // PURPOSE : Function that formats table
    //
    //  PARAMS :
    //     - INPUT:
    //           - DtRSegmentationMonth: Datatable without format
    //           - AgroupNames: Agroup name 
    //     - OUTPUT:
    //           - None
    //
    // RETURNS :
    //     - DataTable with format
    private static DataTable TableFormat(DataTable DtRSegmentationMonth,
                                         AgroupNamesFromSegmentation AgroupNames)
    {
      DataTable _dt_result = null;
      DataRow _datarow = null;
      DataRow[] _datarows_sel = null;
      ArrayList _al_table_agroups_name_order;
      ArrayList _al_table_agroups_count;
      Int32 _i;

      _i = 0;

      // DEFINE  SUBTABLES
      // sets order from Subtables   
      _al_table_agroups_name_order = new ArrayList();
      _al_table_agroups_name_order.Add(SR_SUBTABLE_NAME_HOLDER_LEVEL);
      _al_table_agroups_name_order.Add(SR_SUBTABLE_NAME_GAME_TYPE);
      _al_table_agroups_name_order.Add(SR_SUBTABLE_NAME_HOLDER_GENDER);
      _al_table_agroups_name_order.Add(SR_SUBTABLE_NAME_YEARS_OLD);
      _al_table_agroups_name_order.Add(SR_SUBTABLE_NAME_ADT_GROUP);

      _al_table_agroups_count = new ArrayList();
      _al_table_agroups_count.Add(AgroupNames.holder_level.Count);
      _al_table_agroups_count.Add(AgroupNames.game_type.Count);
      _al_table_agroups_count.Add(AgroupNames.holder_gender.Count);
      _al_table_agroups_count.Add(AgroupNames.years_old.Count);
      _al_table_agroups_count.Add(AgroupNames.adt_group.Count);

      DtRSegmentationMonth.Columns.Add(SR_SQL_COLUMN_TD_AGROUP_NAME, Type.GetType("System.String"));
      _dt_result = DtRSegmentationMonth.Clone();

      // Define the table to save in XML:
      //   - Decode field TD_AGROUP_NAME
      //   - Add line empty
      //   - Add lines empty between tables (Title and subtotal)
      //   - Add a new row with 0 when doesn't exists register

      foreach (String _table_name in _al_table_agroups_name_order.ToArray())
      {
        if (_i != 0)
        {
          _datarow = _dt_result.NewRow();
          _datarow[SR_SQL_COLUMN_TD_AGROUP_NAME] = "EMPTY";
          _dt_result.Rows.Add(_datarow.ItemArray);
        }

        // Add line empty for Title
        _datarow = _dt_result.NewRow();
        _datarow[SR_SQL_COLUMN_TD_AGROUP_NAME] = _table_name + "_TITLE";
        _dt_result.Rows.Add(_datarow.ItemArray);
        
        for (Int32 _j = 0; _j <= (Int32)_al_table_agroups_count[_i] - 1; _j++)
        {
          _datarows_sel = DtRSegmentationMonth.Select(SR_SQL_COLUMN_TD_AGROUP_TYPE + " = '" + _table_name + "' AND " + SR_SQL_COLUMN_TD_AGROUP_CODE + " = " + _j);

          if (_datarows_sel.Length == 1)
          {
            _datarow = _datarows_sel[0];
          }
          else
          {
            _datarow = _dt_result.NewRow();
            _datarow[SR_SQL_COLUMN_TD_AGROUP_TYPE] = _table_name;
            _datarow[SR_SQL_COLUMN_TD_AGROUP_CODE] = _j;
            _datarow[SR_SQL_COLUMN_TD_AFORO] = 0;
            _datarow[SR_SQL_COLUMN_TD_THEORETICAL_HOLD] = 0;
            _datarow[SR_SQL_COLUMN_TD_VISITS] = 0;
            _datarow[SR_SQL_COLUMN_TD_ADT] = 0;
          }

          _datarow[SR_SQL_COLUMN_TD_AGROUP_NAME] = GetAgroupName(_table_name, _j, AgroupNames);
          _dt_result.Rows.Add(_datarow.ItemArray);

        }
        // Add line empty for Total
        _datarow = _dt_result.NewRow();
        _datarow[SR_SQL_COLUMN_TD_AGROUP_NAME] = "TOTAL";
        _datarow[SR_SQL_COLUMN_TD_AGROUP_TYPE] = _table_name;
        _dt_result.Rows.Add(_datarow.ItemArray);
        _i += 1;
      }

      return _dt_result;
    }// TableFormat

    // PURPOSE : Function that return the agroup name
    //
    //  PARAMS :
    //     - INPUT:
    //           - TableName: Table name from Agroup
    //           - Agrup: Code agroup
    //           - AgroupNames: Struct with all literals  
    //     - OUTPUT:
    //           - None
    //
    // RETURNS :
    //     - Agroup Name
    private static String GetAgroupName(String TableName,
                                        Int32 Agrup,
                                        AgroupNamesFromSegmentation AgroupNames)
    {
      switch (TableName)
      {
        case SR_SUBTABLE_NAME_HOLDER_LEVEL:
          // HOLDER LEVEL
          if (AgroupNames.holder_level.ContainsKey(Agrup))
          {
            return AgroupNames.holder_level[Agrup];
          }
          break;

        case SR_SUBTABLE_NAME_GAME_TYPE:
          // GAME TYPE
          if (AgroupNames.game_type.ContainsKey(Agrup))
          {
            return AgroupNames.game_type[Agrup];
          }
          break;

        case SR_SUBTABLE_NAME_HOLDER_GENDER:
          // GENDER
          if (AgroupNames.holder_gender.ContainsKey(Agrup))
          {
            return AgroupNames.holder_gender[Agrup];
          }
          break;

        case SR_SUBTABLE_NAME_YEARS_OLD:
          // YEARS OLD
          if (AgroupNames.years_old.ContainsKey(Agrup))
          {
            return AgroupNames.years_old[Agrup];
          }
          break;

        case SR_SUBTABLE_NAME_ADT_GROUP:
          // ADT GROUP     
          if (AgroupNames.adt_group.ContainsKey(Agrup))
          {
            return AgroupNames.adt_group[Agrup];
          }
          break;
      }
      return "";
    } // GetAgroupName

    // PURPOSE : Return SQL Query to get the segmentation report 
    //
    //  PARAMS :
    //      - INPUT :
    //          - none
    //          
    //      - OUTPUT :
    //
    //          - none
    // RETURNS :
    //      - String: SQL Query
    //
    private static String GetQuerySegmentationReport()
    {
      StringBuilder _sql_sb;

      _sql_sb = new StringBuilder();

      _sql_sb.AppendLine("DECLARE @IniDate  as DateTime                                                                                                                                     ");
      _sql_sb.AppendLine("DECLARE @FinDate  as DateTime                                                                                                                                     ");
      _sql_sb.AppendLine("DECLARE @DefaultHold as Money                                                                                                                                     ");
      _sql_sb.AppendLine("DECLARE @AcTypeVirtualUser as Integer                                                                                                                                     ");
      _sql_sb.AppendLine("DECLARE @AcTypeVirtualCashier as Integer                                                                                                                                     ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("SET @IniDate   = @pIniDate                                                                                                                                        ");
      _sql_sb.AppendLine("SET @FinDate   = DATEADD (MONTH, 1, @IniDate)                                                                                                                     ");
      _sql_sb.AppendLine("SET @AcTypeVirtualUser = @pAcTypeVirtualUser                                                                                                                           ");
      _sql_sb.AppendLine("SET @AcTypeVirtualCashier = @pAcTypeVirtualCashier                                                                                                                           ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("SET @DefaultHold = (SELECT   1 - CAST (GP_KEY_VALUE AS MONEY) / 100                                                                                               ");
      _sql_sb.AppendLine("                      FROM   GENERAL_PARAMS                                                                                                                       ");
      _sql_sb.AppendLine("                     WHERE   GP_GROUP_KEY = 'PlayerTracking'                                                                                                      ");
      _sql_sb.AppendLine("                       AND   GP_SUBJECT_KEY = 'TerminalDefaultPayout')                                                                                            ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("--------------------------------------------------------------------------                                                                                        ");
      _sql_sb.AppendLine("-- TEMPORARY TABLES                                                                                                                                               ");
      _sql_sb.AppendLine("--------------------------------------------------------------------------                                                                                        ");
      _sql_sb.AppendLine("--                                                                                                                                                                ");
      _sql_sb.AppendLine("-- #ACCOUNT_DAY                                                                                                                                                   ");
      _sql_sb.AppendLine("--                                                                                                                                                                ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("SELECT   PS_ACCOUNT_ID                                                                                                                                            ");
      _sql_sb.AppendLine("       , DATEADD (DAY, DATEDIFF (HOUR, @IniDate, PS_STARTED) / 24, @IniDate) DAY                                                                                  ");
      _sql_sb.AppendLine("       , SUM (PS_PLAYED_AMOUNT * ISNULL(TE_THEORETICAL_HOLD, @DefaultHold)) THEORETICAL_HOLD                                                                      ");
      _sql_sb.AppendLine("       , SUM (CASE WHEN TE_GAME_TYPE = 0 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_0                                                                   ");
      _sql_sb.AppendLine("       , SUM (CASE WHEN TE_GAME_TYPE = 1 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_1                                                                   ");
      _sql_sb.AppendLine("       , SUM (CASE WHEN TE_GAME_TYPE = 2 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_2                                                                   ");
      _sql_sb.AppendLine("       , SUM (CASE WHEN TE_GAME_TYPE = 3 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_3                                                                   ");
      _sql_sb.AppendLine("       , SUM (CASE WHEN TE_GAME_TYPE = 4 THEN PS_PLAYED_COUNT ELSE 0 END) TD_GAME_TYPE_PLAYED_4                                                                   ");
      _sql_sb.AppendLine("  INTO   #ACCOUNT_DAY                                                                                                                                             ");
      _sql_sb.AppendLine("  FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_started))                                                                                                                                           ");
      _sql_sb.AppendLine(" INNER   JOIN TERMINALS   ON  PS_TERMINAL_ID = TE_TERMINAL_ID                                                                                                     ");
      if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        _sql_sb.AppendLine("INNER   JOIN ACCOUNTS   ON PS_ACCOUNT_ID = AC_ACCOUNT_ID                                                                                                        ");
      }
      _sql_sb.AppendLine(" WHERE   PS_STARTED >= @IniDate                                                                                                                                   ");
      _sql_sb.AppendLine("   AND   PS_STARTED <  @FinDate                                                                                                                                   ");
      if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        _sql_sb.AppendLine(" AND   AC_TYPE NOT IN (@AcTypeVirtualUser,@AcTypeVirtualCashier)                                                                                                ");
      }
      _sql_sb.AppendLine(" GROUP   BY PS_ACCOUNT_ID,   DATEADD (DAY, DATEDIFF (HOUR, @IniDate, PS_STARTED) / 24, @IniDate)                                                                  ");
      _sql_sb.AppendLine("ORDER BY PS_ACCOUNT_ID, DAY                                                                                                                                       ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("--                                                                                                                                                                ");
      _sql_sb.AppendLine("-- #ACCOUNT_VISITS1                                                                                                                                               ");
      _sql_sb.AppendLine("--                                                                                                                                                                ");
      _sql_sb.AppendLine("SELECT   PS_ACCOUNT_ID                                                                                                                                            ");
      _sql_sb.AppendLine("       , SUM(1) AS PS_VISITS                                                                                                                                      ");
      _sql_sb.AppendLine("       , SUM(THEORETICAL_HOLD) AS THEORETICAL_HOLD                                                                                                                ");
      _sql_sb.AppendLine("       , SUM(THEORETICAL_HOLD)/SUM(1) AS ADT_VALUE                                                                                                                ");
      _sql_sb.AppendLine("       , SUM(TD_GAME_TYPE_PLAYED_0) AS TD_GAME_TYPE_PLAYED_0                                                                                                      ");
      _sql_sb.AppendLine("       , SUM(TD_GAME_TYPE_PLAYED_1) AS TD_GAME_TYPE_PLAYED_1                                                                                                      ");
      _sql_sb.AppendLine("       , SUM(TD_GAME_TYPE_PLAYED_2) AS TD_GAME_TYPE_PLAYED_2                                                                                                      ");
      _sql_sb.AppendLine("       , SUM(TD_GAME_TYPE_PLAYED_3) AS TD_GAME_TYPE_PLAYED_3                                                                                                      ");
      _sql_sb.AppendLine("       , SUM(TD_GAME_TYPE_PLAYED_4) AS TD_GAME_TYPE_PLAYED_4                                                                                                      ");
      _sql_sb.AppendLine("  INTO   #ACCOUNT_VISITS1                                                                                                                                         ");
      _sql_sb.AppendLine("  FROM   #ACCOUNT_DAY                                                                                                                                             ");
      _sql_sb.AppendLine("GROUP BY PS_ACCOUNT_ID                                                                                                                                            ");
      _sql_sb.AppendLine("ORDER BY PS_ACCOUNT_ID                                                                                                                                            ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("--                                                                                                                                                                ");
      _sql_sb.AppendLine("-- #ACCOUNT_VISITS                                                                                                                                                ");
      _sql_sb.AppendLine("--                                                                                                                                                                ");
      _sql_sb.AppendLine("SELECT   PS_ACCOUNT_ID                                                                                                                                            ");
      _sql_sb.AppendLine("       , PS_VISITS                                                                                                                                                ");
      _sql_sb.AppendLine("       , AC_HOLDER_LEVEL                                                                                                                                          ");
      _sql_sb.AppendLine("       , CASE WHEN (TD_GAME_TYPE_PLAYED_0 = 0 AND TD_GAME_TYPE_PLAYED_1  = 0                                                                                      ");
      _sql_sb.AppendLine("                                              AND TD_GAME_TYPE_PLAYED_2  = 0                                                                                      ");
      _sql_sb.AppendLine("                                              AND TD_GAME_TYPE_PLAYED_3  = 0                                                                                      ");
      _sql_sb.AppendLine("                                              AND TD_GAME_TYPE_PLAYED_4  = 0) THEN 0 ELSE                                                                         ");
      _sql_sb.AppendLine("           CASE WHEN (TD_GAME_TYPE_PLAYED_1 >= TD_GAME_TYPE_PLAYED_0 AND TD_GAME_TYPE_PLAYED_1 >= TD_GAME_TYPE_PLAYED_2                                           ");
      _sql_sb.AppendLine("                                                                     AND TD_GAME_TYPE_PLAYED_1 >= TD_GAME_TYPE_PLAYED_3                                           ");
      _sql_sb.AppendLine("                                                                     AND TD_GAME_TYPE_PLAYED_1 >= TD_GAME_TYPE_PLAYED_4) THEN 1 ELSE                              ");
      _sql_sb.AppendLine("           CASE WHEN (TD_GAME_TYPE_PLAYED_2 >= TD_GAME_TYPE_PLAYED_0 AND TD_GAME_TYPE_PLAYED_2 >= TD_GAME_TYPE_PLAYED_1                                           ");
      _sql_sb.AppendLine("                                                                     AND TD_GAME_TYPE_PLAYED_2 >= TD_GAME_TYPE_PLAYED_3                                           ");
      _sql_sb.AppendLine("                                                                     AND TD_GAME_TYPE_PLAYED_2 >= TD_GAME_TYPE_PLAYED_4) THEN 2 ELSE                              ");
      _sql_sb.AppendLine("           CASE WHEN (TD_GAME_TYPE_PLAYED_3 >= TD_GAME_TYPE_PLAYED_0 AND TD_GAME_TYPE_PLAYED_3 >= TD_GAME_TYPE_PLAYED_1                                           ");
      _sql_sb.AppendLine("                                                                     AND TD_GAME_TYPE_PLAYED_3 >= TD_GAME_TYPE_PLAYED_2                                           ");
      _sql_sb.AppendLine("                                                                     AND TD_GAME_TYPE_PLAYED_3 >= TD_GAME_TYPE_PLAYED_4) THEN 3 ELSE                              ");
      _sql_sb.AppendLine("           CASE WHEN (TD_GAME_TYPE_PLAYED_4 >= TD_GAME_TYPE_PLAYED_0 AND TD_GAME_TYPE_PLAYED_4 >= TD_GAME_TYPE_PLAYED_1                                           ");
      _sql_sb.AppendLine("                                                                     AND TD_GAME_TYPE_PLAYED_4 >= TD_GAME_TYPE_PLAYED_2                                           ");
      _sql_sb.AppendLine("                                                                     AND TD_GAME_TYPE_PLAYED_4 >= TD_GAME_TYPE_PLAYED_3) THEN 4 ELSE                              ");
      _sql_sb.AppendLine("                                                                                                                              0 END END END END END AS GAME_TYPE  ");
      _sql_sb.AppendLine("       , AC_HOLDER_GENDER                                                                                                                                         ");
      _sql_sb.AppendLine("       , CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) IS NULL) THEN 0 ELSE                                                                       ");
      _sql_sb.AppendLine("          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 18*12) THEN 1 ELSE                                                                      ");
      _sql_sb.AppendLine("          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 25*12) THEN 2 ELSE                                                                      ");
      _sql_sb.AppendLine("          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 35*12) THEN 3 ELSE                                                                      ");
      _sql_sb.AppendLine("          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 45*12) THEN 4 ELSE                                                                      ");
      _sql_sb.AppendLine("          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 55*12) THEN 5 ELSE                                                                      ");
      _sql_sb.AppendLine("          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 65*12) THEN 6 ELSE                                                                      ");
      _sql_sb.AppendLine("          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 75*12) THEN 7 ELSE                                                                      ");
      _sql_sb.AppendLine("          CASE WHEN (DATEDIFF( month, AC_HOLDER_BIRTH_DATE,  @IniDate ) < 85*12) THEN 8 ELSE                                                                      ");
      _sql_sb.AppendLine("                                                                                        9 END END END END END END END END END AS YEARS_OLD                        ");
      _sql_sb.AppendLine("       , CASE WHEN (ADT_VALUE <   50) THEN 0 ELSE                                                                                                                 ");
      _sql_sb.AppendLine("          CASE WHEN (ADT_VALUE <  100) THEN 1 ELSE                                                                                                                ");
      _sql_sb.AppendLine("          CASE WHEN (ADT_VALUE <  150) THEN 2 ELSE                                                                                                                ");
      _sql_sb.AppendLine("          CASE WHEN (ADT_VALUE <  250) THEN 3 ELSE                                                                                                                ");
      _sql_sb.AppendLine("          CASE WHEN (ADT_VALUE <  400) THEN 4 ELSE                                                                                                                ");
      _sql_sb.AppendLine("          CASE WHEN (ADT_VALUE < 1000) THEN 5 ELSE                                                                                                                ");
      _sql_sb.AppendLine("                                            6 END END END END END END AS ADT_GROUP                                                                                ");
      _sql_sb.AppendLine("       , THEORETICAL_HOLD                                                                                                                                         ");
      _sql_sb.AppendLine("       , ADT_VALUE                                                                                                                                                ");
      _sql_sb.AppendLine("  INTO   #ACCOUNT_VISITS                                                                                                                                          ");
      _sql_sb.AppendLine("  FROM   #ACCOUNT_VISITS1                                                                                                                                         ");
      _sql_sb.AppendLine(" INNER   JOIN ACCOUNTS    ON PS_ACCOUNT_ID = AC_ACCOUNT_ID                                                                                                        ");
      _sql_sb.AppendLine("ORDER BY PS_ACCOUNT_ID                                                                                                                                            ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("--------------------------------------------------------------------------                                                                                        ");
      _sql_sb.AppendLine("-- AFORO, THEORETICAL, VISITS, ADT                                                                                                                                ");
      _sql_sb.AppendLine("--------------------------------------------------------------------------                                                                                        ");
      _sql_sb.AppendLine("SELECT * FROM (                                                                                                                                                   ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  -- HOLDER_LEVEL                                                                                                                                                 ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  SELECT                                                                                                                                                          ");
      _sql_sb.AppendLine("          'HOLDER_LEVEL' AS TD_TABLE                                                                                                                              ");
      _sql_sb.AppendLine("         , AC_HOLDER_LEVEL TD_AGROUP                                                                                                                              ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("         , COUNT(PS_VISITS)  TD_AFORO                                                                                                                             ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD                                                                                                              ");
      _sql_sb.AppendLine("         , SUM(PS_VISITS) TD_VISITS                                                                                                                               ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT                                                                                                          ");
      _sql_sb.AppendLine("  FROM     #ACCOUNT_VISITS                                                                                                                                        ");
      _sql_sb.AppendLine("  GROUP BY AC_HOLDER_LEVEL                                                                                                                                        ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("  UNION ALL                                                                                                                                                       ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  -- GAME_TYPE                                                                                                                                                    ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  SELECT                                                                                                                                                          ");
      _sql_sb.AppendLine("           'GAME_TYPE' AS TD_TABLE                                                                                                                                ");
      _sql_sb.AppendLine("         , GAME_TYPE AS TD_AGROUP                                                                                                                                 ");
      _sql_sb.AppendLine("         , COUNT(PS_VISITS)  TD_AFORO                                                                                                                             ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD                                                                                                              ");
      _sql_sb.AppendLine("         , SUM(PS_VISITS) TD_VISITS                                                                                                                               ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT                                                                                                          ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("    FROM   #ACCOUNT_VISITS                                                                                                                                        ");
      _sql_sb.AppendLine("  GROUP BY GAME_TYPE                                                                                                                                              ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("  UNION ALL                                                                                                                                                       ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  --  HOLDER_GENDER                                                                                                                                               ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  SELECT                                                                                                                                                          ");
      _sql_sb.AppendLine("           'HOLDER_GENDER' AS TD_TABLE                                                                                                                            ");
      _sql_sb.AppendLine("         , ISNULL(AC_HOLDER_GENDER,0) TD_AGROUP                                                                                                                   ");
      _sql_sb.AppendLine("         , COUNT(PS_VISITS)  TD_AFORO                                                                                                                             ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD                                                                                                              ");
      _sql_sb.AppendLine("         , SUM(PS_VISITS) TD_VISITS                                                                                                                               ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT                                                                                                          ");
      _sql_sb.AppendLine("    FROM   #ACCOUNT_VISITS                                                                                                                                        ");
      _sql_sb.AppendLine("  GROUP BY AC_HOLDER_GENDER                                                                                                                                       ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("  UNION ALL                                                                                                                                                       ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  --  YEARS_OLD                                                                                                                                                   ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  SELECT                                                                                                                                                          ");
      _sql_sb.AppendLine("           'YEARS_OLD' AS TD_TABLE                                                                                                                                ");
      _sql_sb.AppendLine("         , YEARS_OLD TD_AGROUP                                                                                                                                    ");
      _sql_sb.AppendLine("         , COUNT(PS_VISITS)  TD_AFORO                                                                                                                             ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD                                                                                                              ");
      _sql_sb.AppendLine("         , SUM(PS_VISITS) TD_VISITS                                                                                                                               ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT                                                                                                          ");
      _sql_sb.AppendLine("    FROM   #ACCOUNT_VISITS                                                                                                                                        ");
      _sql_sb.AppendLine("  GROUP BY YEARS_OLD                                                                                                                                              ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("  UNION ALL                                                                                                                                                       ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  --  ADT_GROUP                                                                                                                                                   ");
      _sql_sb.AppendLine("  --                                                                                                                                                              ");
      _sql_sb.AppendLine("  SELECT                                                                                                                                                          ");
      _sql_sb.AppendLine("           'ADT_GROUP' AS TD_TABLE                                                                                                                                ");
      _sql_sb.AppendLine("         ,  ADT_GROUP TD_AGROUP                                                                                                                                      ");
      _sql_sb.AppendLine("         , COUNT(PS_VISITS)  TD_AFORO                                                                                                                                ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) TD_THEORETICAL_HOLD                                                                                                                 ");
      _sql_sb.AppendLine("         , SUM(PS_VISITS) TD_VISITS                                                                                                                                  ");
      _sql_sb.AppendLine("         , SUM(THEORETICAL_HOLD) / SUM(PS_VISITS) TD_ADT                                                                                                             ");
      _sql_sb.AppendLine("    FROM   #ACCOUNT_VISITS                                                                                                                                        ");
      _sql_sb.AppendLine("  GROUP BY ADT_GROUP                                                                                                                                              ");
      _sql_sb.AppendLine(") X                                                                                                                                                               ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("                                                                                                                                                                  ");
      _sql_sb.AppendLine("--                                                                                                                                                                ");
      _sql_sb.AppendLine("-- DROP TABLES                                                                                                                                                    ");
      _sql_sb.AppendLine("--                                                                                                                                                                ");
      _sql_sb.AppendLine("DROP TABLE #ACCOUNT_DAY                                                                                                                                           ");
      _sql_sb.AppendLine("DROP TABLE #ACCOUNT_VISITS                                                                                                                                        ");
      _sql_sb.AppendLine("DROP TABLE #ACCOUNT_VISITS1                                                                                                                                       ");

      return _sql_sb.ToString();

    } // GetQuerySegmentationReport

    // PURPOSE : Return of the struct with the literals from agroups the segmentation
    //
    //  PARAMS :
    //      - INPUT :
    //          - none
    //          
    //      - OUTPUT :
    //          - none
    //
    // RETURNS :
    //      - AgroupNamesFromSegmentation 
    //
    public static AgroupNamesFromSegmentation GetLiteralsFromSegmentationAgroups()
    {
      DataTable _dt_level_names;
      DataTable _dt_games_names;
      
      using (DB_TRX _db_trx = new DB_TRX())
      {
        _dt_level_names = GetLevelNames(_db_trx.SqlTransaction);
        _dt_games_names = GetGameNames(_db_trx.SqlTransaction);
      }

      AgroupNamesFromSegmentation _agroup_names = new AgroupNamesFromSegmentation();

      // holder level
      _agroup_names.holder_level.Add(0, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_00"));// anónimo

      foreach (DataRow _drow_level in _dt_level_names.Rows)
      {
        _agroup_names.holder_level.Add((Int32)_drow_level[0], (String)_drow_level[1]);
      }

      // game type
      foreach (DataRow _drow_game in _dt_games_names.Rows)
      {
        _agroup_names.game_type.Add((Int32)_drow_game[0], (String)_drow_game[1]);
      }

      // holder gender
      _agroup_names.holder_gender.Add(0, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_00"));     // anónimo
      _agroup_names.holder_gender.Add(1, Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE"));   // men
      _agroup_names.holder_gender.Add(2, Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE")); // woman

      // years old
      _agroup_names.years_old.Add(0, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_00")); // anónimo 
      _agroup_names.years_old.Add(1, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_01")); // menor 18
      _agroup_names.years_old.Add(2, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_02")); // 18-24
      _agroup_names.years_old.Add(3, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_03")); // 25-34
      _agroup_names.years_old.Add(4, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_04")); // 35-44
      _agroup_names.years_old.Add(5, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_05")); // 45-54
      _agroup_names.years_old.Add(6, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_06")); // 55-64
      _agroup_names.years_old.Add(7, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_07")); // 65-74
      _agroup_names.years_old.Add(8, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_08")); // 75-84
      _agroup_names.years_old.Add(9, Resource.String("STR_SEGMENTATION_REPORT_YEARS_OLD_09")); // 85 o mas.      

       // adt group
      _agroup_names.adt_group.Add(0, Resource.String("STR_SEGMENTATION_REPORT_ADT_GROUP_00")); // $0-49
      _agroup_names.adt_group.Add(1, Resource.String("STR_SEGMENTATION_REPORT_ADT_GROUP_01")); // $50-99
      _agroup_names.adt_group.Add(2, Resource.String("STR_SEGMENTATION_REPORT_ADT_GROUP_02")); // $100-149
      _agroup_names.adt_group.Add(3, Resource.String("STR_SEGMENTATION_REPORT_ADT_GROUP_03")); // $150-249
      _agroup_names.adt_group.Add(4, Resource.String("STR_SEGMENTATION_REPORT_ADT_GROUP_04")); // $250-399
      _agroup_names.adt_group.Add(5, Resource.String("STR_SEGMENTATION_REPORT_ADT_GROUP_05")); // $400-999
      _agroup_names.adt_group.Add(6, Resource.String("STR_SEGMENTATION_REPORT_ADT_GROUP_06")); // $1,000 o more

      return _agroup_names;
    } // GetLiteralsFromSegmentationAgroups
    #endregion // Private Methods

  }
}
