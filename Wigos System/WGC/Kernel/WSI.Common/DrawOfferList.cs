//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DrawOfferList.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 02-MAY-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-MAY-2011 RCI    First release.
// 22-JUN-2012 JCM    Moved IsTimeInRange to Common.Schedule
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace WSI.Common
{
  public class DrawOffer
  {
    internal String m_key;
    internal Int32 m_n;
    internal Int32 m_m;
    internal DrawGenderFilter m_gender_filter;
    internal Int32 m_weekday;
    internal DateTime m_day;
    internal Int32 m_time_from;
    internal Int32 m_time_to;

    public String Key
    {
      get { return m_key; }
    }
    public Int32 N
    {
      get { return m_n; }
    }
    public Int32 M
    {
      get { return m_m; }
    }
    public DrawGenderFilter GenderFilter
    {
      get { return m_gender_filter; }
    }
    public Int32 Weekday
    {
      get { return m_weekday; }
    }
    public DateTime Day
    {
      get { return m_day; }
    }
    public Int32 TimeFrom
    {
      get { return m_time_from; }
    }
    public Int32 TimeTo
    {
      get { return m_time_to; }
    }

  } // DrawOffer

  public class DrawOfferList
  {
    DataTable m_offer = null;
    DataSet m_ds = null;

    # region Constructors

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor for a DrawOfferList instance
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public DrawOfferList()
    {
      Init();
    } // DrawOfferList

    #endregion // Constructors

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Add a new draw offer.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 N
    //          - Int32 M
    //          - DrawGenderFilter GenderFilter
    //          - Int32 Weekday
    //          - DateTime Day
    //          - Int32 TimeFrom
    //          - Int32 TimeTo
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String: the Key representing the draw offer.
    // 
    public String AddOffer(Int32 N, Int32 M, DrawGenderFilter GenderFilter, Int32 Weekday, DateTime Day, Int32 TimeFrom, Int32 TimeTo)
    {
      DataRow _dr;
      String _key;

      _dr = m_offer.NewRow();
      _dr["N"] = N;
      _dr["M"] = M;
      _dr["GenderFilter"] = GenderFilter;
      _dr["Weekday"] = Weekday;
      _dr["Day"] = Day.ToString("yyyy-MM-dd");
      _dr["TimeFrom"] = TimeFrom;
      _dr["TimeTo"] = TimeTo;

      _key = "";
      if (Weekday == 0)
      {
        _key += Day.ToString("yyyyMMdd");
      }
      else
      {
        _key += Weekday.ToString("000");
      }
      _key += "-" + TimeFrom.ToString("00") + "-" + TimeTo.ToString("00");

      if (GenderFilter != DrawGenderFilter.ALL)
      {
        _key += "-" + (Int32)GenderFilter;
      }

      _dr["Key"] = _key;

      if (SeekOffer(_key) != null)
      {
        return null;
      }
      m_offer.Rows.Add(_dr);

      return _key;
    } // AddOffer

    //------------------------------------------------------------------------------
    // PURPOSE: Remove a draw offer.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Key
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void RemoveOffer(String Key)
    {
      DataRow _dr;

      _dr = SeekOffer(Key);
      if (_dr != null)
      {
        _dr.Delete();
        m_offer.AcceptChanges();
      }
    } // RemoveOffer

    //------------------------------------------------------------------------------
    // PURPOSE: Get the draw offer in the index position of the internal DataTable.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Index
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DrawOffer
    // 
    public DrawOffer GetOffer(Int32 Index)
    {
      DataRow _dr;
      DrawOffer _draw_offer;

      if (Index >= m_offer.Rows.Count || Index < 0)
      {
        return null;
      }

      _dr = m_offer.Rows[Index];
      _draw_offer = new DrawOffer();

      _draw_offer.m_key = (String)_dr["Key"];
      _draw_offer.m_n = (Int32)_dr["N"];
      _draw_offer.m_m = (Int32)_dr["M"];
      _draw_offer.m_gender_filter = (DrawGenderFilter)_dr["GenderFilter"];
      _draw_offer.m_weekday = (Int32)_dr["Weekday"];
      _draw_offer.m_day = DateTimeParse((String)_dr["Day"]);
      _draw_offer.m_time_from = (Int32)_dr["TimeFrom"];
      _draw_offer.m_time_to = (Int32)_dr["TimeTo"];

      return _draw_offer;
    } // GetOffer

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if there are offers for the gender specified.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DrawGenderFilter GenderFilter
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean
    // 
    public Boolean ExistGenderOffers(DrawGenderFilter GenderFilter)
    {
      foreach (DataRow _dr in m_offer.Rows)
      {
        if ((DrawGenderFilter)_dr["GenderFilter"] == GenderFilter)
        {
          return true;
        }
      }

      return false;
    } // ExistGenderOffers

    //------------------------------------------------------------------------------
    // PURPOSE: Get the number of elements in the internal DataTable.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Int32
    // 
    public Int32 Count
    {
      get { return m_offer.Rows.Count; }
    } // Count

    //------------------------------------------------------------------------------
    // PURPOSE: Get a XML representation of the draw offer list.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    public String ToXml()
    {
      if (m_offer.Rows.Count == 0)
      {
        return null;
      }
      return Misc.DataSetToXml(m_ds, XmlWriteMode.IgnoreSchema);
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE: Read a XML representation of the draw offer list and store it into the DataSet.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void FromXml(String Xml)
    {
      m_offer.Clear();
      if (Xml == null)
      {
        return;
      }
      Misc.DataSetFromXml(m_ds, Xml, XmlReadMode.IgnoreSchema);
    } // FromXml

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Init the properties of the class.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void Init()
    {
      DrawGenderFilter _gender_filter_dummy;
      DataColumn _col;

      _gender_filter_dummy = DrawGenderFilter.ALL;

      m_ds = new DataSet("DrawOfferList");

      m_offer = new DataTable("DrawOffer");
      m_offer.Columns.Add("Key", Type.GetType("System.String"));
      m_offer.Columns.Add("N", Type.GetType("System.Int32"));
      m_offer.Columns.Add("M", Type.GetType("System.Int32"));
      _col = m_offer.Columns.Add("GenderFilter", _gender_filter_dummy.GetType());
      _col.DefaultValue = DrawGenderFilter.ALL;
      _col.AllowDBNull = false;
      m_offer.Columns.Add("Weekday", Type.GetType("System.Int32"));
      m_offer.Columns.Add("Day", Type.GetType("System.String"));
      m_offer.Columns.Add("TimeFrom", Type.GetType("System.Int32"));
      m_offer.Columns.Add("TimeTo", Type.GetType("System.Int32"));
      m_offer.PrimaryKey = new DataColumn[1] { m_offer.Columns["Key"] };

      m_ds.Tables.Add(m_offer);
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Search for the offer key in the draw offer list.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Key
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DataRow: If found return the DataRow.
    // 
    private DataRow SeekOffer(String Key)
    {
      foreach (DataRow _dr in m_offer.Rows)
      {
        if (Key.Equals((String)_dr["Key"]))
        {
          return _dr;
        }
      }
      return null;
    } // SeekOffer

    #endregion // Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the maximum offer factor that applies between Start and End datetimes.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 AccountGender
    //          - DateTime Start
    //          - DateTime End
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Decimal
    // 
    public Decimal GetFactor(Int32 AccountGender, DateTime Start, DateTime End)
    {
      Decimal _factor;
      DateTime _start;
      DateTime _end;
      
      _start = new DateTime(Start.Year, Start.Month, Start.Day, Start.Hour, 0, 0);
      _end = new DateTime(End.Year, End.Month, End.Day, End.Hour, 0, 0);
      
      _factor = -1;
      while (_start <= _end)
      {
        _factor = Math.Max(_factor, GetFactor(AccountGender, _start));
        _start = _start.AddHours(1);
      }

      if (_factor < 0)
      {
        _factor = 1;
      }

      return _factor;
    } // GetFactor

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the latest offer factor (from all the available offers) that applies in DateTime
    //          according to the AccountGender.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 AccountGender
    //          - DateTime DateTime
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Decimal
    // 
    private Decimal GetFactor(Int32 AccountGender, DateTime DateTime)
    {
      Int32 _today_mask;
      Int32 _day_mask;

      Decimal _factor_offer_day;
      Decimal _factor_offer_weekday;

      DateTime _today;
      Int32 _h_from;
      Int32 _h_to;
      Int32 _offer_n;
      Int32 _offer_m;
      DateTime _offer_day;
      Boolean _found_day;

      _factor_offer_weekday = 1;
      _factor_offer_day = 1;
      _found_day = false;

      _today = Common.Misc.Opening(DateTime);
      _today = _today.Date;
      _today_mask = (1 << (Int32)_today.DayOfWeek);

      foreach (DataRow _dr in m_offer.Rows)
      {
        _day_mask = (Int32)_dr["Weekday"];
        _h_from = (Int32)_dr["TimeFrom"];
        _h_to = (Int32)_dr["TimeTo"];
        _offer_n = (Int32)_dr["N"];
        _offer_m = (Int32)_dr["M"];

        // RCI 07-JUL-2011: Filter by gender.
        switch ((DrawGenderFilter)_dr["GenderFilter"])
        {
          case DrawGenderFilter.ALL:
          default:
            // Do nothing
            break;

          case DrawGenderFilter.MEN_ONLY:
            if (AccountGender != 1)
            {
              continue;
            }
            break;

          case DrawGenderFilter.WOMEN_ONLY:
            if (AccountGender != 2)
            {
              continue;
            }
            break;
        }

        if (_day_mask == 0)
        {
          _offer_day = DateTimeParse((String)_dr["Day"]);

          // Day
          if (_today == _offer_day)
          {
            if (ScheduleDay.IsTimeInRange(DateTime.TimeOfDay, _h_from, _h_to))
            {
              _factor_offer_day = (_offer_m == 0) ? 0 : (Decimal)_offer_n / (Decimal)_offer_m;
              _found_day = true;
            }
          }
        }
        else
        {
          // Weekday
          if ((_today_mask & _day_mask) == _today_mask)
          {
            if (ScheduleDay.IsTimeInRange(DateTime.TimeOfDay, _h_from, _h_to))
            {
              _factor_offer_weekday = (_offer_m == 0) ? 0 : (Decimal)_offer_n / (Decimal)_offer_m;
            }
          }
        }
      }

      // Day offers have priority over weekday offers.
      return _found_day ? _factor_offer_day : _factor_offer_weekday;
    } // GetFactor

    //------------------------------------------------------------------------------
    // PURPOSE: Parse an String to DateTime (only the Date part).
    //          The String can be like "yyyy-MM-dd" or like "yyyy-MM-ddThh:mm:ss+GMT".
    //
    //  PARAMS:
    //      - INPUT:
    //          - String StrDate
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DateTime
    //
    private DateTime DateTimeParse(String StrDate)
    {
      String[] _parts;

      _parts = StrDate.Split('T');
      return DateTime.ParseExact(_parts[0], "yyyy-MM-dd", null);
    } // DateTimeParse

  } // DrawOfferList

} // WSI.Common
