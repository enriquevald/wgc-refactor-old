﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GamingTablesTournaments.cs
// 
//   DESCRIPTION: Gaming Tables Tournaments namespace
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 04-APR-2017
// 
// REVISION HISTORY:
// 
// Date         Author  Description
// -----------  ------  ----------------------------------------------------------
// 04-APR-2017  JMM     First version.
// 05-APR-2017  FGB     WIGOS-636: Gaming table tournament - Cashier tournaments list to register and enroll
// 21-APR-2017  ATB     PBI 26954: Gaming table tournament - Tournament parameters
// 24-APR-2017  FAV     PBI 26949: Gaming table tournament - Review changes
// 26-APR-2017  FAV     PBI 26952: Gaming table tournament - Tournament Start/End expiration job
// 27-APR-2017  ATB     PBI 26950: Gaming table tournament - Prize pyramid
// -----------  ------  ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

namespace WSI.Common.GamingTablesTournaments
{
  public enum TOURNAMENT_OPERATION_RESULT
  {
    OK = 1,
    NOT_FOUND = 2,

    ERROR = 999,
  }

  /// <summary>
  /// Gaming Tables Tournament status
  /// </summary>
  public enum TOURNAMENT_STATUS
  {
    READY = 1,
    INSCRIPTION = 2,
    WAITING = 3,
    STARTED = 4,
    FINISHED = 5,
  }

  /// <summary>
  /// Gaming Tables Tournaments configuration
  /// </summary>
  public static class TournamentConfig
  {
    #region Properties
    public static Boolean AdmissionCharge
    {
      get
      {
        return (GeneralParam.GetBoolean("GamingTablesTournaments", "Admission.Charge"));
      }
    }

    public static String ExpirationDays
    {
      get
      {
        return (GeneralParam.GetString("GamingTablesTournaments", "Prize.ExpirationDays"));
      }
    }

    public static Boolean IsFeaturesGamingTablesTournamentsEnabled
    {
      get
      {
        return (GeneralParam.GetBoolean("Features", "GamingTablesTournaments", false));
      }
    }

    public static Boolean IsGamingTablesTournamentsEnabled
    {
      get
      {
        return (IsFeaturesGamingTablesTournamentsEnabled && GeneralParam.GetBoolean("GamingTablesTournaments", "Enabled", false));
      }
    }

    public static String VoucherHeader
    {
      get
      {
        return (GeneralParam.GetString("GamingTablesTournaments", "Voucher.Header"));
      }
    }

    public static String VoucherFooter
    {
      get
      {
        return (GeneralParam.GetString("GamingTablesTournaments", "Voucher.Footer"));
      }
    }
    #endregion

  }

  /// <summary>
  /// Tournament's configuration class
  /// </summary>
  public class TournamentGeneralParams
  {
    #region Attributes
    private Boolean m_enabled;
    private String m_expiration_days;
    private Boolean m_admission_charge;
    private String m_header;
    private String m_footer;
    #endregion

    #region Properties
    public Boolean Enabled
    {
      get { return this.m_enabled; }
      set { this.m_enabled = value; }
    }
    public String ExpirationDays
    {
      get { return this.m_expiration_days; }
      set { this.m_expiration_days = value; }
    }
    public Boolean AdmissionCharge
    {
      get { return this.m_admission_charge; }
      set { this.m_admission_charge = value; }
    }
    public String Header
    {
      get { return this.m_header; }
      set { this.m_header = value; }
    }
    public String Footer
    {
      get { return this.m_footer; }
      set { this.m_footer = value; }
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Constructor
    /// </summary>
    public TournamentGeneralParams()
    {
    }

    /// <summary>
    /// Read data from the general params in memory
    /// </summary>
    public void DB_GeneralParam_Read()
    {
      this.Enabled = ReadGeneralParam("Enabled") == "1";
      this.ExpirationDays = ReadGeneralParam("Prize.ExpirationDays");
      this.AdmissionCharge = ReadGeneralParam("Admission.Charge") == "1";
      this.Header = ReadGeneralParam("Voucher.Header");
      this.Footer = ReadGeneralParam("Voucher.Footer");
    }

    /// <summary>
    /// Updating method for the general params
    /// </summary>
    public bool DB_GeneralParam_Update(string GroupKey, string SubjectKey, string NewValue, SqlTransaction Trx)
    {
      return GeneralParam.GenerateGeneralParam(GroupKey, SubjectKey, NewValue, Trx);
    }
    #endregion

    #region Private Methods
    private string ReadGeneralParam(String SubjectKey)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
      _sb.AppendLine("   FROM   GENERAL_PARAMS ");
      _sb.AppendLine("  WHERE   GP_GROUP_KEY  = 'GamingTablesTournaments' ");
      _sb.AppendLine("    AND   GP_SUBJECT_KEY = '" + SubjectKey + "'");


      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return String.Empty;
            }
            return _reader.GetString(0);

          }
        }
      }
    }
    #endregion

  }

  /// <summary>
  /// TournamentInfo class
  /// </summary>
  public class TournamentInfo
  {
    #region Attributes
    private Int64 tournament_id;
    private TOURNAMENT_STATUS tournament_status;
    private String tournament_name;
    private Boolean admission_increment;
    private Decimal fixed_prize_amount;
    private Decimal increment_prize_amount;
    private Decimal total_prize_amount;
    private Decimal admission_amount;
    private Int32 rebuy;
    private Decimal rebuy_amount;
    private Decimal rebuy_pctj_contribution;
    private DateTime event_start_date;
    private DateTime event_finish_date;
    private Int32 game_rounds;
    private DateTime inscription_start_date;
    private DateTime inscription_finish_date;
    private Int32 game_type;
    private String rules;
    private Int32 capacity;
    private Int32 seats;
    private Boolean enabled;
    private List<TournamentsGameTableInfo> available_tables;
    #endregion

    #region Structs
    public struct TournamentsGameTableInfo
    {
      #region Attributes
      private Int32 id;
      private String code;
      private String name;
      private Int32 game_type;
      private Int32 num_seats;
      #endregion

      #region "Property"

      public Int32 Id
      {
        get { return id; }
        set { id = value; }
      }

      public String Code
      {
        get { return code; }
        set { code = value; }
      }

      public String Name
      {
        get { return name; }
        set { name = value; }
      }

      public Int32 GameType
      {
        get { return game_type; }
        set { game_type = value; }
      }

      public Int32 NumSeats
      {
        get { return num_seats; }
        set { num_seats = value; }
      }

      #endregion

      #region Public Methods
      /// <summary>
      /// Exports a list of TournamentsGameTableInfo to a XML
      /// </summary>
      /// <param name="StatusId"></param>
      /// <returns></returns>
      public static String ToXml(List<TournamentsGameTableInfo> GamingTablesList)
      {
        XmlDocument _xml_document;
        XmlNode _root_node;
        XmlNode _table_node;
        XmlAttribute _attribute;

        _xml_document = new XmlDocument();
        _root_node = _xml_document.CreateElement("GAMING_TABLES");

        foreach (TournamentsGameTableInfo _element in GamingTablesList)
        {
          _table_node = _xml_document.CreateElement("TABLE");

          _attribute = _table_node.OwnerDocument.CreateAttribute("ID");
          _attribute.Value = _element.id.ToString();
          _table_node.Attributes.Append(_attribute);

          _attribute = _table_node.OwnerDocument.CreateAttribute("CODE");
          _attribute.Value = _element.code;
          _table_node.Attributes.Append(_attribute);

          _attribute = _table_node.OwnerDocument.CreateAttribute("GAME_TYPE");
          _attribute.Value = _element.game_type.ToString();
          _table_node.Attributes.Append(_attribute);

          _attribute = _table_node.OwnerDocument.CreateAttribute("NAME");
          _attribute.Value = _element.name;
          _table_node.Attributes.Append(_attribute);

          _attribute = _table_node.OwnerDocument.CreateAttribute("NUM_SEATS");
          _attribute.Value = _element.num_seats.ToString();
          _table_node.Attributes.Append(_attribute);

          _root_node.AppendChild(_table_node);
        }

        _xml_document.AppendChild(_root_node);

        return _xml_document.OuterXml;
      }

      /// <summary>
      /// Parses a xml string to a list of TournamentsGameTableInfo
      /// </summary>
      /// <param name="StatusId"></param>
      /// <returns></returns>
      public static List<TournamentsGameTableInfo> LoadXML(String GamingTablesXML)
      {
        TournamentsGameTableInfo _type_game_table;
        List<TournamentsGameTableInfo> _gaming_tables_list;
        XmlDocument _xml_document;

        _xml_document = new XmlDocument();
        _xml_document.LoadXml(GamingTablesXML);

        _gaming_tables_list = new List<TournamentsGameTableInfo>();
        _gaming_tables_list.Clear();

        foreach (XmlNode _game in _xml_document.SelectSingleNode("GAMING_TABLES").SelectNodes("TABLE"))
        {
          _type_game_table = new TournamentsGameTableInfo();
          Int32.TryParse(_game.Attributes["ID"].Value, out _type_game_table.id);
          _type_game_table.code = _game.Attributes["CODE"].Value;
          _type_game_table.name = _game.Attributes["NAME"].Value;
          Int32.TryParse(_game.Attributes["GAME_TYPE"].Value, out _type_game_table.game_type);
          Int32.TryParse(_game.Attributes["NUM_SEATS"].Value, out _type_game_table.num_seats);

          _gaming_tables_list.Add(_type_game_table);
        }

        return _gaming_tables_list;
      }

      public static Boolean FindGamingTable(List<TournamentsGameTableInfo> GamingTablesList, Int32 GamingTableId)
      {
        foreach (TournamentsGameTableInfo _table_item in GamingTablesList)
        {
          if (_table_item.Id == GamingTableId)
          {
            return true;
          }
        }

        return false;
      }

      #endregion

    }
    #endregion


    #region "Property"

    public Int64 TournamentId
    {
      get { return tournament_id; }
      set { tournament_id = value; }
    }

    public TOURNAMENT_STATUS TournamentStatus
    {
      get { return tournament_status; }
      set { tournament_status = value; }
    }

    public String TournamentName
    {
      get { return tournament_name; }
      set { tournament_name = value; }
    }

    public Boolean AdmissionIncrement
    {
      get { return admission_increment; }
      set { admission_increment = value; }
    }

    public Decimal FixedPrizeAmount
    {
      get { return fixed_prize_amount; }
      set { fixed_prize_amount = value; }
    }

    public Decimal IncrementPrizeAmount
    {
      get { return increment_prize_amount; }
      set { increment_prize_amount = value; }
    }

    public Decimal TotalPrizeAmount
    {
      get { return total_prize_amount; }
      set { total_prize_amount = value; }
    }

    public Decimal AdmissionAmount
    {
      get { return admission_amount; }
      set { admission_amount = value; }
    }

    public Int32 Rebuy
    {
      get { return rebuy; }
      set { rebuy = value; }
    }

    public Decimal RebuyAmount
    {
      get { return rebuy_amount; }
      set { rebuy_amount = value; }
    }

    public Decimal RebuyPctjContribution
    {
      get { return rebuy_pctj_contribution; }
      set { rebuy_pctj_contribution = value; }
    }

    public DateTime EventStartDate
    {
      get { return event_start_date; }
      set { event_start_date = value; }
    }

    public DateTime EventFinishDate
    {
      get { return event_finish_date; }
      set { event_finish_date = value; }
    }

    public Int32 GameRounds
    {
      get { return game_rounds; }
      set { game_rounds = value; }
    }

    public DateTime InscriptionStartDate
    {
      get { return inscription_start_date; }
      set { inscription_start_date = value; }
    }

    public DateTime InscriptionFinishDate
    {
      get { return inscription_finish_date; }
      set { inscription_finish_date = value; }
    }

    public Int32 GameType
    {
      get { return game_type; }
      set { game_type = value; }
    }

    public String Rules
    {
      get { return rules; }
      set { rules = value; }
    }

    public Int32 Capacity
    {
      get { return capacity; }
      set { capacity = value; }
    }

    public Int32 Seats
    {
      get { return seats; }
      set { seats = value; }
    }

    public Boolean Enabled
    {
      get { return enabled; }
      set { enabled = value; }
    }

    public List<TournamentsGameTableInfo> AvailableTables
    {
      get { return available_tables; }
      set { available_tables = value; }
    }

    #endregion

    #region "Constructor"

    public TournamentInfo()
    {
      this.TournamentId = 0;
      this.TournamentStatus = 0;
      this.TournamentName = "";
      this.AdmissionIncrement = false;
      this.FixedPrizeAmount = 0;
      this.IncrementPrizeAmount = 0;
      this.TotalPrizeAmount = 0;
      this.AdmissionAmount = 0;
      this.Rebuy = 0;
      this.RebuyAmount = 0;
      this.RebuyPctjContribution = 0;
      this.GameType = 0;
      this.Rules = "";
      this.Capacity = 0;
      this.Seats = 0;
      this.Enabled = true;
      this.AvailableTables = new List<TournamentsGameTableInfo>();
    }

    #endregion

    #region "Public Methods"
    public Decimal GetTotalEntryAmount()
    {
      return AdmissionAmount;
    }

    /// <summary>
    /// Returns the description for a tournament status
    /// </summary>
    /// <param name="StatusId"></param>
    /// <returns></returns>
    public static String GetTournamentStatusDescription(TOURNAMENT_STATUS StatusId)
    {
      switch (StatusId)
      {
        case TOURNAMENT_STATUS.READY:
          return Resource.String("STR_TOURNAMENT_STATUS_READY");

        case TOURNAMENT_STATUS.INSCRIPTION:
          return Resource.String("STR_TOURNAMENT_STATUS_INSCRIPTION");

        case TOURNAMENT_STATUS.WAITING:
          return Resource.String("STR_TOURNAMENT_STATUS_WAITING");

        case TOURNAMENT_STATUS.STARTED:
          return Resource.String("STR_TOURNAMENT_STATUS_STARTED");

        case TOURNAMENT_STATUS.FINISHED:
          return Resource.String("STR_TOURNAMENT_STATUS_FINISHED");

        default:
          return Resource.String("STR_TOURNAMENT_STATUS_UNKNOWN");
      }
    }

    /// <summary>
    /// Returns the description for a tournament registration status
    /// </summary>
    /// <param name="StatusId"></param>
    /// <returns></returns>
    public static String GetTournamentRegistrationStatusDescription(TOURNAMENT_REGISTRATION_STATUS StatusId)
    {
      switch (StatusId)
      {
        case TOURNAMENT_REGISTRATION_STATUS.ENROLLED:
          return Resource.String("STR_TOURNAMENT_REGISTRATION_STATUS_INSCRIPTION");

        case TOURNAMENT_REGISTRATION_STATUS.ATTENDED:
          return Resource.String("STR_TOURNAMENT_REGISTRATION_STATUS_ATTENDED");

        case TOURNAMENT_REGISTRATION_STATUS.NOT_AWARDED:
          return Resource.String("STR_TOURNAMENT_REGISTRATION_STATUS_NOT_AWARDED");

        case TOURNAMENT_REGISTRATION_STATUS.AWARDED:
          return Resource.String("STR_TOURNAMENT_REGISTRATION_STATUS_AWARDED");

        case TOURNAMENT_REGISTRATION_STATUS.PAID:
          return Resource.String("STR_TOURNAMENT_REGISTRATION_STATUS_PAID");

        case TOURNAMENT_REGISTRATION_STATUS.EXPIRED:
          return Resource.String("STR_TOURNAMENT_REGISTRATION_STATUS_EXPIRED");

        default:
          return Resource.String("STR_TOURNAMENT_REGISTRATION_STATUS_UNKNOWN");
      }
    }

    public class TournamentBreakdownPrizes
    {
      public struct Prize
      {
        private Int32 id;
        private Int32 ranking;
        private decimal prize_percentage;

        #region " Property "
        public Int32 TournamentId
        {
          get { return id; }
          set { this.id = value; }
        }

        public Int32 Ranking
        {
          get { return this.ranking; }
          set { this.ranking = value; }
        }

        public decimal PrizePercentage
        {
          get { return this.prize_percentage; }
          set { this.prize_percentage = value; }
        }
        #endregion
      }
      public List<Prize> tournamentBreakdownPrizes;
      public Int32 TournamentId;

      public TOURNAMENT_OPERATION_RESULT ReadTournamentPrizes(Int32 TournamentId)
      {
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   GTTP_TOURNAMENT_ID                   ");
        _sb.AppendLine("       , GTTP_RANKING                         ");
        _sb.AppendLine("       , GTTP_PRIZE_PERCENTAGE                ");
        _sb.AppendLine(" FROM    GAMING_TABLES_TOURNAMENTS_PRIZES     ");
        _sb.AppendLine("WHERE    GTTP_TOURNAMENT_ID = @pTournamentId  ");

        this.TournamentId = TournamentId;
        this.tournamentBreakdownPrizes = new List<Prize>();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = TournamentId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {

              DataTable dt = new DataTable();
              dt.Load(_reader);

              foreach (DataRow dr in dt.Rows)
              {
                Prize prize = new Prize();
                prize.TournamentId = Int32.Parse(dr.ItemArray[0].ToString());
                prize.Ranking = Int32.Parse(dr.ItemArray[1].ToString());
                prize.PrizePercentage = Int32.Parse(dr.ItemArray[2].ToString());

                tournamentBreakdownPrizes.Add(prize);
              }

              //while (dt._reader.Read())
              //{
              //  Prize prize = new Prize();
              //  prize.TournamentId = _reader.GetInt32(_reader.GetOrdinal("GTTP_TOURNAMENT_ID"));
              //  prize.Ranking = _reader.GetInt32(_reader.GetOrdinal("GTTP_RANKING"));
              //  prize.PrizePercentage = _reader.GetInt32(_reader.GetOrdinal("GTTP_PRIZE_PERCENTAGE"));

              //  tournamentBreakdownPrizes.Add(prize);

              //}
              return TOURNAMENT_OPERATION_RESULT.OK;
            }
          }
        }

        return TOURNAMENT_OPERATION_RESULT.ERROR;

      } // ReadTournament

      public TOURNAMENT_OPERATION_RESULT InsertTournamentPrizes()
      {
        //Deleting the previous tournament prizes
        DeleteTournamentPrizes();

        //Inserting the new prizes
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine("INSERT INTO   GAMING_TABLES_TOURNAMENTS_PRIZES  ");
        _sb.AppendLine("          (   GTTP_TOURNAMENT_ID                ");
        _sb.AppendLine("            , GTTP_RANKING                      ");
        _sb.AppendLine("            , GTTP_PRIZE_PERCENTAGE             ");
        _sb.AppendLine("          )                                     ");
        _sb.AppendLine(" VALUES   (   @pTournamentId                    ");
        _sb.AppendLine("            , @pRanking                         ");
        _sb.AppendLine("            , @pPrizePercentage                 ");
        _sb.AppendLine("          )                                     ");


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt);
            _sql_cmd.Parameters.Add("@pRanking", SqlDbType.Int);
            _sql_cmd.Parameters.Add("@pPrizePercentage", SqlDbType.Decimal, 100);

            foreach (TournamentInfo.TournamentBreakdownPrizes.Prize prize in this.tournamentBreakdownPrizes)
            {
              _sql_cmd.Parameters["@pTournamentId"].Value = prize.TournamentId;
              _sql_cmd.Parameters["@pRanking"].Value = prize.Ranking;
              _sql_cmd.Parameters["@pPrizePercentage"].Value = prize.PrizePercentage;

              _sql_cmd.ExecuteNonQuery();
            }
            _db_trx.Commit();

            //_db_trx.Rollback();
            //return TOURNAMENT_OPERATION_RESULT.ERROR;
          }
        }
        return TOURNAMENT_OPERATION_RESULT.OK;

      } // InsertTournament

      private TOURNAMENT_OPERATION_RESULT DeleteTournamentPrizes()
      {
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine("DELETE   GAMING_TABLES_TOURNAMENTS_PRIZES    ");
        _sb.AppendLine(" WHERE   GTTP_TOURNAMENT_ID = @pTournamentId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = this.TournamentId;

            int deleted = _sql_cmd.ExecuteNonQuery();
            if (deleted > 0)
            {
              _db_trx.Commit();

              return TOURNAMENT_OPERATION_RESULT.OK;
            }

            _db_trx.Rollback();
          }
        }

        return TOURNAMENT_OPERATION_RESULT.ERROR;

      } // DeleteTournament
    }
    #endregion

  }

  /// <summary>
  /// TournamentInfoForDisplay class
  /// </summary>
  public class TournamentInfoForDisplay
  {
    #region Attributes
    private Int64 tournament_id;
    private TOURNAMENT_STATUS tournament_status;
    private String tournament_name;
    private DateTime inscription_start_date;
    private DateTime inscription_finish_date;
    private DateTime event_start_date;
    private DateTime event_finish_date;
    private Int32 registered_players;
    private Int32 tournament_capacity;
    private Int32 tournament_rebuys;
    #endregion

    #region "Property"

    public Int64 TournamentId
    {
      get { return tournament_id; }
      set { tournament_id = value; }
    }

    public TOURNAMENT_STATUS TournamentStatus
    {
      get { return tournament_status; }
      set { tournament_status = value; }
    }

    public String TournamentStatusDescription
    {
      get { return TournamentDB.GetTournamentStatusDescription(TournamentStatus); }
    }

    public String TournamentName
    {
      get { return tournament_name; }
      set { tournament_name = value; }
    }

    public DateTime StartDate
    {
      get
      {
        if (TournamentStatus == TOURNAMENT_STATUS.INSCRIPTION)
        {
          return inscription_start_date;
        }
        else
        {
          return event_start_date;
        }
      }
    }

    public DateTime FinishDate
    {
      get
      {
        if (TournamentStatus == TOURNAMENT_STATUS.INSCRIPTION)
        {
          return inscription_finish_date;
        }
        else
        {
          return event_finish_date;
        }
      }
    }

    public DateTime EventStartDate
    {
      //get { return event_start_date; }
      set { event_start_date = value; }
    }

    public DateTime EventFinishDate
    {
      //get { return event_finish_date; }
      set { event_finish_date = value; }
    }

    public DateTime InscriptionStartDate
    {
      //get { return inscription_start_date; }
      set { inscription_start_date = value; }
    }

    public DateTime InscriptionFinishDate
    {
      //get { return inscription_finish_date; }
      set { inscription_finish_date = value; }
    }

    public Int32 RegisteredPlayers
    {
      get { return registered_players; }
      set { registered_players = value; }
    }

    public Int32 TournamentCapacity
    {
      get { return tournament_capacity; }
      set { tournament_capacity = value; }
    }

    public Int32 TournamentRebuys
    {
      get { return tournament_rebuys; }
      set { tournament_rebuys = value; }
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Return list of tournaments that it status is in the StatusToShow list.
    /// </summary>
    /// <param name="StatusToShow"></param>
    /// <returns></returns>
    public static List<TournamentInfoForDisplay> GetTournamentList(List<TOURNAMENT_STATUS> StatusToShow)
    {
      List<TournamentInfoForDisplay> _tournament_list;
      _tournament_list = new List<TournamentInfoForDisplay>();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(GetSQLTournamentList(StatusToShow), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          //Set status parameters values
          TournamentInfoForDisplay.SetSQLParametersTournamentStatusToFilter(_sql_cmd, StatusToShow);

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              TournamentInfoForDisplay _tournamen_display_info = new TournamentInfoForDisplay()
              {
                TournamentId = _reader.GetInt64(_reader.GetOrdinal("GTT_TOURNAMENT_ID")),
                TournamentStatus = (TOURNAMENT_STATUS)_reader.GetInt32(_reader.GetOrdinal("GTT_TOURNAMENT_STATUS")),
                TournamentName = _reader.GetString(_reader.GetOrdinal("GTT_TOURNAMENT_NAME")),
                InscriptionStartDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_INSCRIPTION_START_DATE")),
                InscriptionFinishDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_INSCRIPTION_FINISH_DATE")),
                EventStartDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_EVENT_START_DATE")),
                EventFinishDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_EVENT_FINISH_DATE")),
                RegisteredPlayers = _reader.GetInt32(_reader.GetOrdinal("REGISTERED_PLAYERS")),
                TournamentCapacity = _reader.GetInt32(_reader.GetOrdinal("GTT_CAPACITY")),
                TournamentRebuys = _reader.GetInt32(_reader.GetOrdinal("GTT_REBUY"))
              };

              _tournament_list.Add(_tournamen_display_info);
            }
          }
        }
      }

      return _tournament_list;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Return SQL filter for StatusToShow list
    /// </summary>
    /// <param name="StatusToShow"></param>
    /// <returns></returns>
    private static string GetSQLWhereTournamentStatusToFilter(List<TOURNAMENT_STATUS> StatusToShow)
    {
      StringBuilder _sb_filter;

      _sb_filter = new StringBuilder();
      _sb_filter.AppendLine("WHERE   GTT_ENABLED = 1");

      if ((StatusToShow != null) && (StatusToShow.Count > 0))
      {
        _sb_filter.AppendLine(String.Format("  AND   GTT_TOURNAMENT_STATUS in ({0})", string.Join(", ", StatusToShow.ConvertAll(x => "@p" + x.ToString()).ToArray())));
      }

      return _sb_filter.ToString();
    }

    /// <summary>
    /// Get SQL to obtain the tournament list
    /// </summary>
    /// <param name="StatusToShow"></param>
    /// <returns></returns>
    private static string GetSQLTournamentList(List<TOURNAMENT_STATUS> StatusToShow)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   GTT_TOURNAMENT_STATUS                              ");
      _sb.AppendLine("       , '' as GTT_TOURNAMENT_STATUS_DESC                   ");
      _sb.AppendLine("       , GTT_TOURNAMENT_ID                                  ");
      _sb.AppendLine("       , GTT_TOURNAMENT_NAME                                ");
      _sb.AppendLine("       , GTT_EVENT_START_DATE                               ");
      _sb.AppendLine("       , GTT_EVENT_FINISH_DATE                              ");
      _sb.AppendLine("       , GTT_INSCRIPTION_START_DATE                         ");
      _sb.AppendLine("       , GTT_INSCRIPTION_FINISH_DATE                        ");
      _sb.AppendLine("       , (SELECT COUNT(*)                                   ");
      _sb.AppendLine("          FROM GAMING_TABLES_TOURNAMENT_PLAYERS             ");
      _sb.AppendLine("          WHERE GTTP_TOURNAMENT_ID = GTT_TOURNAMENT_ID      ");
      _sb.AppendLine("          ) as REGISTERED_PLAYERS                           ");
      _sb.AppendLine("       , GTT_CAPACITY                                       ");
      _sb.AppendLine("       , GTT_REBUY                                          ");
      _sb.AppendLine(" FROM    GAMING_TABLES_TOURNAMENTS                          ");
      _sb.AppendLine(TournamentInfoForDisplay.GetSQLWhereTournamentStatusToFilter(StatusToShow));

      return _sb.ToString();
    }

    /// <summary>
    /// Set status parameters values
    /// </summary>
    /// <param name="sqlCmd"></param>
    /// <param name="StatusToShow"></param>
    private static void SetSQLParametersTournamentStatusToFilter(SqlCommand sqlCmd, List<TOURNAMENT_STATUS> StatusToShow)
    {
      if (StatusToShow == null)
      {
        return;
      }

      if (StatusToShow.Count == 0)
      {
        return;
      }

      if (sqlCmd == null)
      {
        return;
      }

      foreach (TOURNAMENT_STATUS _status in StatusToShow)
      {
        sqlCmd.Parameters.Add("@p" + _status.ToString(), SqlDbType.Int).Value = (int)_status;
      }
    }
    #endregion

  }

  /// <summary>
  /// TournamentDB class
  /// </summary>
  public class TournamentDB : TournamentInfo
  {
    #region Public Methods
    public TOURNAMENT_OPERATION_RESULT ReadTournament(Int64 TournamentId)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   GTT_TOURNAMENT_ID                   ");
      _sb.AppendLine("       , GTT_TOURNAMENT_STATUS               ");
      _sb.AppendLine("       , GTT_TOURNAMENT_NAME                 ");
      _sb.AppendLine("       , GTT_ADMISSION_INCREMENT             ");
      _sb.AppendLine("       , GTT_FIXED_PRIZE_AMOUNT              ");
      _sb.AppendLine("       , GTT_INCREMENT_PRIZE_AMOUNT          ");
      _sb.AppendLine("       , GTT_TOTAL_PRIZE_AMOUNT              ");
      _sb.AppendLine("       , GTT_ADMISSION_AMOUNT                ");
      _sb.AppendLine("       , GTT_REBUY                           ");
      _sb.AppendLine("       , GTT_REBUY_AMOUNT                    ");
      _sb.AppendLine("       , GTT_REBUY_PCTJ_CONTRIBUTION         ");
      _sb.AppendLine("       , GTT_EVENT_START_DATE                ");
      _sb.AppendLine("       , GTT_EVENT_FINISH_DATE               ");
      _sb.AppendLine("       , GTT_GAME_ROUNDS                     ");
      _sb.AppendLine("       , GTT_INSCRIPTION_START_DATE          ");
      _sb.AppendLine("       , GTT_INSCRIPTION_FINISH_DATE         ");
      _sb.AppendLine("       , GTT_GAME_TYPE                       ");
      _sb.AppendLine("       , GTT_RULES                           ");
      _sb.AppendLine("       , GTT_CAPACITY                        ");
      _sb.AppendLine("       , GTT_SEATS                           ");
      _sb.AppendLine("       , GTT_ENABLED                         ");
      _sb.AppendLine("       , GTT_AVAILABLE_TABLES                ");
      _sb.AppendLine(" FROM    GAMING_TABLES_TOURNAMENTS           ");
      _sb.AppendLine("WHERE    GTT_TOURNAMENT_ID = @pTournamentId  ");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = TournamentId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              this.TournamentId = _reader.GetInt64(_reader.GetOrdinal("GTT_TOURNAMENT_ID"));
              this.TournamentStatus = (TOURNAMENT_STATUS)_reader.GetInt32(_reader.GetOrdinal("GTT_TOURNAMENT_STATUS"));
              this.TournamentName = _reader.GetString(_reader.GetOrdinal("GTT_TOURNAMENT_NAME"));
              this.AdmissionIncrement = _reader.GetBoolean(_reader.GetOrdinal("GTT_ADMISSION_INCREMENT"));
              this.FixedPrizeAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_FIXED_PRIZE_AMOUNT"));
              this.IncrementPrizeAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_INCREMENT_PRIZE_AMOUNT"));
              this.TotalPrizeAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_TOTAL_PRIZE_AMOUNT"));
              this.AdmissionAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_ADMISSION_AMOUNT"));
              this.Rebuy = _reader.GetInt32(_reader.GetOrdinal("GTT_REBUY"));
              this.RebuyAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_REBUY_AMOUNT"));
              this.RebuyPctjContribution = _reader.GetDecimal(_reader.GetOrdinal("GTT_REBUY_PCTJ_CONTRIBUTION"));
              this.EventStartDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_EVENT_START_DATE"));
              this.EventFinishDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_EVENT_FINISH_DATE"));
              this.GameRounds = _reader.GetInt32(_reader.GetOrdinal("GTT_GAME_ROUNDS"));
              this.InscriptionStartDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_INSCRIPTION_START_DATE"));
              this.InscriptionFinishDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_INSCRIPTION_FINISH_DATE"));
              this.GameType = _reader.GetInt32(_reader.GetOrdinal("GTT_GAME_TYPE"));
              this.Rules = _reader.GetString(_reader.GetOrdinal("GTT_RULES"));
              this.Capacity = _reader.GetInt32(_reader.GetOrdinal("GTT_CAPACITY"));
              this.Seats = _reader.GetInt32(_reader.GetOrdinal("GTT_SEATS"));
              this.Enabled = _reader.GetBoolean(_reader.GetOrdinal("GTT_ENABLED"));

              if (_reader.IsDBNull(_reader.GetOrdinal("GTT_AVAILABLE_TABLES")))
              {
                this.AvailableTables = null;
              }
              else
              {
                this.AvailableTables = TournamentsGameTableInfo.LoadXML(_reader.GetString(_reader.GetOrdinal("GTT_AVAILABLE_TABLES")));
              }

              return TOURNAMENT_OPERATION_RESULT.OK;
            }
          }
        }
      }

      return TOURNAMENT_OPERATION_RESULT.ERROR;

    } // ReadTournament

    public TOURNAMENT_OPERATION_RESULT UpdateTournament()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("UPDATE   GAMING_TABLES_TOURNAMENTS                              ");
      _sb.AppendLine("   SET   GTT_TOURNAMENT_STATUS = @pTournamentStatus             ");
      _sb.AppendLine("       , GTT_TOURNAMENT_NAME = @pTournamentName                 ");
      _sb.AppendLine("       , GTT_ADMISSION_INCREMENT = @pAdmissionIncrement         ");
      _sb.AppendLine("       , GTT_FIXED_PRIZE_AMOUNT = @pFixedPrizeAmount            ");
      _sb.AppendLine("       , GTT_INCREMENT_PRIZE_AMOUNT = @pIncrementPrizeAmount    ");
      _sb.AppendLine("       , GTT_TOTAL_PRIZE_AMOUNT = @pTotalPrizeAmount            ");
      _sb.AppendLine("       , GTT_ADMISSION_AMOUNT = @pAdmissionAmmount              ");
      _sb.AppendLine("       , GTT_REBUY = @pRebuy                                    ");
      _sb.AppendLine("       , GTT_REBUY_AMOUNT = @pRebuyAmount                       ");
      _sb.AppendLine("       , GTT_REBUY_PCTJ_CONTRIBUTION = @pRebuyPctjContribution  ");
      _sb.AppendLine("       , GTT_EVENT_START_DATE = @pEventStartDate                ");
      _sb.AppendLine("       , GTT_EVENT_FINISH_DATE = @pEventFinishDate              ");
      _sb.AppendLine("       , GTT_GAME_ROUNDS = @pGameRounds                         ");
      _sb.AppendLine("       , GTT_INSCRIPTION_START_DATE = @pInscriptionStartDate    ");
      _sb.AppendLine("       , GTT_INSCRIPTION_FINISH_DATE = @pInscriptionFinishDate  ");
      _sb.AppendLine("       , GTT_GAME_TYPE = @pGameType                             ");
      _sb.AppendLine("       , GTT_RULES = @pRules                                    ");
      _sb.AppendLine("       , GTT_CAPACITY = @pCapacity                              ");
      _sb.AppendLine("       , GTT_SEATS = @pSeats                                    ");
      _sb.AppendLine("       , GTT_ENABLED = @pEnabled                                ");
      _sb.AppendLine("       , GTT_AVAILABLE_TABLES = @pAvailableTables               ");
      _sb.AppendLine(" WHERE   GTT_TOURNAMENT_ID = @pTournamentId                     ");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = this.TournamentId;
          _sql_cmd.Parameters.Add("@pTournamentStatus", SqlDbType.Int).Value = this.TournamentStatus;
          _sql_cmd.Parameters.Add("@pTournamentName", SqlDbType.NVarChar, 100).Value = this.TournamentName;
          _sql_cmd.Parameters.Add("@pAdmissionIncrement", SqlDbType.Bit).Value = this.AdmissionIncrement;
          _sql_cmd.Parameters.Add("@pFixedPrizeAmount", SqlDbType.Money).Value = this.FixedPrizeAmount;
          _sql_cmd.Parameters.Add("@pIncrementPrizeAmount", SqlDbType.Money).Value = this.IncrementPrizeAmount;
          _sql_cmd.Parameters.Add("@pTotalPrizeAmount", SqlDbType.Money).Value = this.TotalPrizeAmount;
          _sql_cmd.Parameters.Add("@pAdmissionAmmount", SqlDbType.Money).Value = this.AdmissionAmount;
          _sql_cmd.Parameters.Add("@pRebuy", SqlDbType.Int).Value = this.Rebuy;
          _sql_cmd.Parameters.Add("@pRebuyAmount", SqlDbType.Money).Value = this.RebuyAmount;
          _sql_cmd.Parameters.Add("@pRebuyPctjContribution", SqlDbType.Money).Value = this.RebuyPctjContribution;
          _sql_cmd.Parameters.Add("@pEventStartDate", SqlDbType.DateTime).Value = this.EventStartDate;
          _sql_cmd.Parameters.Add("@pEventFinishDate", SqlDbType.DateTime).Value = this.EventFinishDate;
          _sql_cmd.Parameters.Add("@pGameRounds", SqlDbType.Int).Value = this.GameRounds;
          _sql_cmd.Parameters.Add("@pInscriptionStartDate", SqlDbType.DateTime).Value = this.InscriptionStartDate;
          _sql_cmd.Parameters.Add("@pInscriptionFinishDate", SqlDbType.DateTime).Value = this.InscriptionFinishDate;
          _sql_cmd.Parameters.Add("@pGameType", SqlDbType.Int).Value = this.GameType;
          _sql_cmd.Parameters.Add("@pRules", SqlDbType.NVarChar, 1024).Value = this.Rules;
          _sql_cmd.Parameters.Add("@pCapacity", SqlDbType.Int).Value = this.Capacity;
          _sql_cmd.Parameters.Add("@pSeats", SqlDbType.Int).Value = this.Seats;
          _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = this.Enabled;
          _sql_cmd.Parameters.Add("@pAvailableTables", SqlDbType.Xml).Value = TournamentsGameTableInfo.ToXml(this.AvailableTables);

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            _db_trx.Commit();

            return TOURNAMENT_OPERATION_RESULT.OK;
          }

          _db_trx.Rollback();
        }
      }

      return TOURNAMENT_OPERATION_RESULT.ERROR;

    } // UpdateTournament

    public TOURNAMENT_OPERATION_RESULT InsertTournament()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("INSERT INTO   GAMING_TABLES_TOURNAMENTS    ");
      _sb.AppendLine("          (   GTT_TOURNAMENT_STATUS        ");
      _sb.AppendLine("            , GTT_TOURNAMENT_NAME          ");
      _sb.AppendLine("            , GTT_ADMISSION_INCREMENT      ");
      _sb.AppendLine("            , GTT_FIXED_PRIZE_AMOUNT       ");
      _sb.AppendLine("            , GTT_INCREMENT_PRIZE_AMOUNT   ");
      _sb.AppendLine("            , GTT_TOTAL_PRIZE_AMOUNT       ");
      _sb.AppendLine("            , GTT_ADMISSION_AMOUNT         ");
      _sb.AppendLine("            , GTT_REBUY                    ");
      _sb.AppendLine("            , GTT_REBUY_AMOUNT             ");
      _sb.AppendLine("            , GTT_REBUY_PCTJ_CONTRIBUTION  ");
      _sb.AppendLine("            , GTT_EVENT_START_DATE         ");
      _sb.AppendLine("            , GTT_EVENT_FINISH_DATE        ");
      _sb.AppendLine("            , GTT_GAME_ROUNDS              ");
      _sb.AppendLine("            , GTT_INSCRIPTION_START_DATE   ");
      _sb.AppendLine("            , GTT_INSCRIPTION_FINISH_DATE  ");
      _sb.AppendLine("            , GTT_GAME_TYPE                ");
      _sb.AppendLine("            , GTT_RULES                    ");
      _sb.AppendLine("            , GTT_CAPACITY                 ");
      _sb.AppendLine("            , GTT_SEATS                    ");
      _sb.AppendLine("            , GTT_ENABLED                  ");
      _sb.AppendLine("            , GTT_AVAILABLE_TABLES         ");
      _sb.AppendLine("          )                                ");
      _sb.AppendLine(" VALUES   (   @pTournamentStatus           ");
      _sb.AppendLine("            , @pTournamentName             ");
      _sb.AppendLine("            , @pAdmissionIncrement         ");
      _sb.AppendLine("            , @pFixedPrizeAmount           ");
      _sb.AppendLine("            , @pIncrementPrizeAmount       ");
      _sb.AppendLine("            , @pTotalPrizeAmount           ");
      _sb.AppendLine("            , @pAdmissionAmmount           ");
      _sb.AppendLine("            , @pRebuy                      ");
      _sb.AppendLine("            , @pRebuyAmount                ");
      _sb.AppendLine("            , @pRebuyPctjContribution      ");
      _sb.AppendLine("            , @pEventStartDate             ");
      _sb.AppendLine("            , @pEventFinishDate            ");
      _sb.AppendLine("            , @pGameRounds                 ");
      _sb.AppendLine("            , @pInscriptionStartDate       ");
      _sb.AppendLine("            , @pInscriptionFinishDate      ");
      _sb.AppendLine("            , @pGameType                   ");
      _sb.AppendLine("            , @pRules                      ");
      _sb.AppendLine("            , @pCapacity                   ");
      _sb.AppendLine("            , @pSeats                      ");
      _sb.AppendLine("            , @pEnabled                    ");
      _sb.AppendLine("            , @pAvailableTables            ");
      _sb.AppendLine("          )                                ");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = this.TournamentId;
          _sql_cmd.Parameters.Add("@pTournamentStatus", SqlDbType.Int).Value = this.TournamentStatus;
          _sql_cmd.Parameters.Add("@pTournamentName", SqlDbType.NVarChar, 100).Value = this.TournamentName;
          _sql_cmd.Parameters.Add("@pAdmissionIncrement", SqlDbType.Bit).Value = this.AdmissionIncrement;
          _sql_cmd.Parameters.Add("@pFixedPrizeAmount", SqlDbType.Money).Value = this.FixedPrizeAmount;
          _sql_cmd.Parameters.Add("@pIncrementPrizeAmount", SqlDbType.Money).Value = this.IncrementPrizeAmount;
          _sql_cmd.Parameters.Add("@pTotalPrizeAmount", SqlDbType.Money).Value = this.TotalPrizeAmount;
          _sql_cmd.Parameters.Add("@pAdmissionAmmount", SqlDbType.Money).Value = this.AdmissionAmount;
          _sql_cmd.Parameters.Add("@pRebuy", SqlDbType.Int).Value = this.Rebuy;
          _sql_cmd.Parameters.Add("@pRebuyAmount", SqlDbType.Money).Value = this.RebuyAmount;
          _sql_cmd.Parameters.Add("@pRebuyPctjContribution", SqlDbType.Money).Value = this.RebuyPctjContribution;
          _sql_cmd.Parameters.Add("@pEventStartDate", SqlDbType.DateTime).Value = this.EventStartDate;
          _sql_cmd.Parameters.Add("@pEventFinishDate", SqlDbType.DateTime).Value = this.EventFinishDate;
          _sql_cmd.Parameters.Add("@pGameRounds", SqlDbType.Int).Value = this.GameRounds;
          _sql_cmd.Parameters.Add("@pInscriptionStartDate", SqlDbType.DateTime).Value = this.InscriptionStartDate;
          _sql_cmd.Parameters.Add("@pInscriptionFinishDate", SqlDbType.DateTime).Value = this.InscriptionFinishDate;
          _sql_cmd.Parameters.Add("@pGameType", SqlDbType.Int).Value = this.GameType;
          _sql_cmd.Parameters.Add("@pRules", SqlDbType.NVarChar, 1024).Value = this.Rules;
          _sql_cmd.Parameters.Add("@pCapacity", SqlDbType.Int).Value = this.Capacity;
          _sql_cmd.Parameters.Add("@pSeats", SqlDbType.Int).Value = this.Seats;
          _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = this.Enabled;
          _sql_cmd.Parameters.Add("@pAvailableTables", SqlDbType.Xml).Value = TournamentsGameTableInfo.ToXml(this.AvailableTables);

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            _db_trx.Commit();

            return TOURNAMENT_OPERATION_RESULT.OK;
          }

          _db_trx.Rollback();
        }
      }

      return TOURNAMENT_OPERATION_RESULT.ERROR;

    } // InsertTournament

    public TOURNAMENT_OPERATION_RESULT DeleteTournament()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("DELETE   GAMING_TABLES_TOURNAMENTS          ");
      _sb.AppendLine(" WHERE   GTT_TOURNAMENT_ID = @pTournamentId ");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = this.TournamentId;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            _db_trx.Commit();

            return TOURNAMENT_OPERATION_RESULT.OK;
          }

          _db_trx.Rollback();
        }
      }

      return TOURNAMENT_OPERATION_RESULT.ERROR;

    } // DeleteTournament
    #endregion

  }

  /// <summary>
  /// GamingTableTournamentBusinessLogic class
  /// </summary>
  public static class GamingTableTournamentBusinessLogic
  {
    #region Public Methods
    public static Boolean RegisterPlayer(TournamentInfo TournamentInfo, Int64 AccountId, out String MessageError)
    {
      MessageError = "Aforo completo";
      return false;
    }

    public static Boolean Rebuy(TournamentInfo TournamentInfo, Int64 AccountId, out String MessageError)
    {
      MessageError = "Torneo no permite rebuys";
      return false;
    }

    public static Boolean PayPrize(TournamentInfo TournamentInfo, Int64 AccountId, out String MessageError)
    {
      MessageError = "Torneo no permite pagar premios";
      return false;
    }

    public static Boolean SetAllTournamentsToStarted(DateTime CurrentDateTime)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   GAMING_TABLES_TOURNAMENTS                           ");
        _sb.AppendLine("   SET   GTT_TOURNAMENT_STATUS = @pStartedStatus             ");
        _sb.AppendLine(" WHERE   GTT_TOURNAMENT_STATUS <>  @pFinishedStatus          ");
        _sb.AppendLine("   AND   @pCurrentDateTime >= GTT_EVENT_START_DATE           ");
        _sb.AppendLine("   AND   GTT_ENABLED = 1                                     ");


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pStartedStatus", SqlDbType.Int).Value = TOURNAMENT_STATUS.STARTED;
            _sql_cmd.Parameters.Add("@pFinishedStatus", SqlDbType.Int).Value = TOURNAMENT_STATUS.FINISHED;
            _sql_cmd.Parameters.Add("@pCurrentDateTime", SqlDbType.DateTime).Value = CurrentDateTime;
            _sql_cmd.ExecuteNonQuery();
            _db_trx.Commit();
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean SetAllTournamentsToFinished(DateTime CurrentDateTime)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   GAMING_TABLES_TOURNAMENTS                      ");
        _sb.AppendLine("   SET   GTT_TOURNAMENT_STATUS = @pFinishedStatus       ");
        _sb.AppendLine(" WHERE   GTT_TOURNAMENT_STATUS = @pStartedStatus        ");
        _sb.AppendLine("   AND   @pCurrentDateTime > GTT_EVENT_FINISH_DATE      ");
        _sb.AppendLine("   AND   GTT_ENABLED = 1                                ");


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pStartedStatus", SqlDbType.Int).Value = TOURNAMENT_STATUS.STARTED;
            _sql_cmd.Parameters.Add("@pFinishedStatus", SqlDbType.Int).Value = TOURNAMENT_STATUS.FINISHED;
            _sql_cmd.Parameters.Add("@pCurrentDateTime", SqlDbType.DateTime).Value = CurrentDateTime;
            _sql_cmd.ExecuteNonQuery();
            _db_trx.Commit();
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion

  }

  /// <summary>
  /// GamingTableTournamentListRowEnterEventArgs class. Events args for a row enter on a GamingTableTournamentList
  /// </summary>
  public class GamingTableTournamentListRowEnterEventArgs : EventArgs
  {
    #region Public Methods
    public Int64 TournamentId { get; set; }

    public GamingTableTournamentListRowEnterEventArgs(Int64 tournamentId)
    {
      TournamentId = tournamentId;
    }
    #endregion

  }

  //Events handler for a row enter on a GamingTableTournamentList
  public delegate void GamingTableTournamentListRowEnterEventHandler(object sender, GamingTableTournamentListRowEnterEventArgs e);
}
