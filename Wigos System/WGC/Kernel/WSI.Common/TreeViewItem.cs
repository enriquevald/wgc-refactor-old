﻿//-------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:       TreeViewItem
// DESCRIPTION:       Class to show object properties in a TreeView
// AUTHOR:            Ferran Ortner
// CREATION DATE:     30-MAR-2017
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 30-MAR-2017  FOS    Initial version
//--------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WSI.Common
{

  public class TreeViewItem
  {

    #region " Properties "
    
    public Int32 Id { get; set; }
    public String Description { get; set; }
    public EditSelectionItemType Type { get; set; }
    public List<TreeViewItem> Nodes { get; set; }
    public NodeStatus Status { get; set; }
    public ContextMenu ContextMenu { get; set; }

    #endregion 

    #region " Public Functions "

    /// <summary>
    /// Constructors
    /// </summary>
    public TreeViewItem()
    {
      this.Nodes = new List<TreeViewItem>();
    } // TreeViewItem
    /// <summary>
    /// 
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="Description"></param>
    /// <param name="Type"></param>
    /// <param name="Status"></param>
    public TreeViewItem ( Int32 Id
                        , String Description
                        , EditSelectionItemType Type
                        , NodeStatus Status)
    {
      this.Id           = Id;
      this.Description  = Description;
      this.Type         = Type;
      this.Status       = Status;
      this.Nodes        = new List<TreeViewItem>();
    } // TreeViewItem

    #endregion

  } // TreeViewItem
}
