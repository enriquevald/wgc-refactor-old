﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GamingTablesTournament.cs
// 
//   DESCRIPTION: Gaming Tables Tournament class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 04-APR-2017
// 
// REVISION HISTORY:
// 
// Date         Author  Description
// -----------  ------  ----------------------------------------------------------
// 04-APR-2017  JMM     First version.
// 05-APR-2017  FGB     WIGOS-636: Gaming table tournament - Cashier tournaments list to register and enroll
// 21-APR-2017  ATB     PBI 26954: Gaming table tournament - Tournament parameters
// 24-APR-2017  FAV     PBI 26949: Gaming table tournament - Review changes
// 26-APR-2017  FAV     PBI 26952: Gaming table tournament - Tournament Start/End expiration job
// 27-APR-2017  ATB     PBI 26950: Gaming table tournament - Prize pyramid
// 02-MAY-2017  ATB     PBI 26950: Gaming table tournament - Prize pyramid
// 03-MAY-2017  ATB     PBI 27204: Gaming table tournament - Modify tournament gaming table selection
// 12-MAY-2017  ATB    PBI 26467: Gaming table tournament - New and Edition
// 12-MAY-2017  FAV    PBI 26952: Gaming table tournament - Tournament Start/End expiration job
// -----------  ------  ----------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingTablesTournament
{
  /// <summary>
  /// Gaming tables tournament operation result
  /// </summary>
  public enum GAMING_TABLES_TOURNAMENT_OPERATION_RESULT
  {
    OK = 1,
    NOT_FOUND = 2,

    ERROR = 999,
  }

  /// <summary>
  /// Gaming Tables Tournament status
  /// </summary>
  public enum GAMING_TABLES_TOURNAMENT_STATUS
  {
    /// <summary>
    /// Tournament is configured and ready
    /// </summary>
    READY = 1,

    /// <summary>
    /// Tournament is in inscription period
    /// </summary>
    INSCRIPTION = 2,

    /// <summary>
    /// El torneo está esperando la fecha de inicio porque el período de inscripción está terminado
    /// </summary>
    WAITING = 3,

    /// <summary>
    /// The tournament is started
    /// </summary>
    STARTED = 4,

    /// <summary>
    /// The tournament is started and it permit inscriptions
    /// </summary>
    STARTED_INSCRIPTION = 5,

    /// <summary>
    /// The tournament is finished
    /// </summary>
    FINISHED = 6,
  }

  /// <summary>
  /// TournamentDB class
  /// </summary>
  public class GamingTablesTournamentDB : GamingTablesTournamentInfo
  {
    public Int32 TournamentID;

    #region Public Methods
    public GAMING_TABLES_TOURNAMENT_OPERATION_RESULT ReadTournament(Int64 TournamentId)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   GTT_TOURNAMENT_ID                   ");
        _sb.AppendLine("       , GTT_TOURNAMENT_STATUS               ");
        _sb.AppendLine("       , GTT_TOURNAMENT_NAME                 ");
        _sb.AppendLine("       , GTT_FIXED_PRIZE_AMOUNT              ");
        _sb.AppendLine("       , GTT_INCREMENT_PRIZE_AMOUNT          ");
        _sb.AppendLine("       , GTT_TOTAL_PRIZE_AMOUNT              ");
        _sb.AppendLine("       , GTT_ADMISSION_AMOUNT                ");
        _sb.AppendLine("       , GTT_REBUY                           ");
        _sb.AppendLine("       , GTT_REBUY_AMOUNT                    ");
        _sb.AppendLine("       , GTT_REBUY_PCTJ_CONTRIBUTION         ");
        _sb.AppendLine("       , GTT_EVENT_START_DATE                ");
        _sb.AppendLine("       , GTT_EVENT_FINISH_DATE               ");
        _sb.AppendLine("       , GTT_GAME_ROUNDS                     ");
        _sb.AppendLine("       , GTT_INSCRIPTION_START_DATE          ");
        _sb.AppendLine("       , GTT_INSCRIPTION_FINISH_DATE         ");
        _sb.AppendLine("       , GTT_GAME_TYPE                       ");
        _sb.AppendLine("       , GTT_RULES                           ");
        _sb.AppendLine("       , GTT_CAPACITY                        ");
        _sb.AppendLine("       , GTT_SEATS                           ");
        _sb.AppendLine("       , GTT_ENABLED                         ");
        _sb.AppendLine("       , GTT_AVAILABLE_TABLES                ");
        _sb.AppendLine(" FROM    GAMING_TABLES_TOURNAMENTS           ");
        _sb.AppendLine("WHERE    GTT_TOURNAMENT_ID = @pTournamentId  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = TournamentId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                this.TournamentId = _reader.GetInt64(_reader.GetOrdinal("GTT_TOURNAMENT_ID"));
                this.TournamentStatus = (GAMING_TABLES_TOURNAMENT_STATUS)_reader.GetInt32(_reader.GetOrdinal("GTT_TOURNAMENT_STATUS"));
                this.TournamentName = _reader.GetString(_reader.GetOrdinal("GTT_TOURNAMENT_NAME"));
                this.FixedPrizeAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_FIXED_PRIZE_AMOUNT"));
                this.IncrementPrizeAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_INCREMENT_PRIZE_AMOUNT"));
                this.TotalPrizeAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_TOTAL_PRIZE_AMOUNT"));
                this.AdmissionAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_ADMISSION_AMOUNT"));
                this.Rebuy = _reader.GetInt32(_reader.GetOrdinal("GTT_REBUY"));
                this.RebuyAmount = _reader.GetDecimal(_reader.GetOrdinal("GTT_REBUY_AMOUNT"));
                this.RebuyPctjContribution = _reader.GetDecimal(_reader.GetOrdinal("GTT_REBUY_PCTJ_CONTRIBUTION"));
                this.EventStartDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_EVENT_START_DATE"));
                this.EventFinishDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_EVENT_FINISH_DATE"));
                this.GameRounds = _reader.GetInt32(_reader.GetOrdinal("GTT_GAME_ROUNDS"));
                this.InscriptionStartDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_INSCRIPTION_START_DATE"));
                this.InscriptionFinishDate = _reader.GetDateTime(_reader.GetOrdinal("GTT_INSCRIPTION_FINISH_DATE"));
                this.GameType = _reader.GetInt32(_reader.GetOrdinal("GTT_GAME_TYPE"));
                this.Rules = _reader.GetString(_reader.GetOrdinal("GTT_RULES"));
                this.Capacity = _reader.GetInt32(_reader.GetOrdinal("GTT_CAPACITY"));
                this.Seats = _reader.GetInt32(_reader.GetOrdinal("GTT_SEATS"));
                this.Enabled = _reader.GetBoolean(_reader.GetOrdinal("GTT_ENABLED"));

                if (_reader.IsDBNull(_reader.GetOrdinal("GTT_AVAILABLE_TABLES")))
                {
                  this.AvailableTables = null;
                }
                else
                {
                  this.AvailableTables = GamingTablesTournamentGameTableInfo.LoadXML(_reader.GetString(_reader.GetOrdinal("GTT_AVAILABLE_TABLES")));
                }

                return GAMING_TABLES_TOURNAMENT_OPERATION_RESULT.OK;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return GAMING_TABLES_TOURNAMENT_OPERATION_RESULT.ERROR;
    } // ReadTournament

    public GAMING_TABLES_TOURNAMENT_OPERATION_RESULT UpdateTournament()
    {
      StringBuilder _sb;
     // SqlParameter _sql_parameter;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   GAMING_TABLES_TOURNAMENTS                              ");
        _sb.AppendLine("   SET   GTT_TOURNAMENT_STATUS = @pTournamentStatus             ");
        _sb.AppendLine("       , GTT_TOURNAMENT_NAME = @pTournamentName                 ");
        _sb.AppendLine("       , GTT_FIXED_PRIZE_AMOUNT = @pFixedPrizeAmount            ");
        _sb.AppendLine("       , GTT_INCREMENT_PRIZE_AMOUNT = @pIncrementPrizeAmount    ");
        _sb.AppendLine("       , GTT_TOTAL_PRIZE_AMOUNT = @pTotalPrizeAmount            ");
        _sb.AppendLine("       , GTT_ADMISSION_AMOUNT = @pAdmissionAmmount              ");
        _sb.AppendLine("       , GTT_REBUY = @pRebuy                                    ");
        _sb.AppendLine("       , GTT_REBUY_AMOUNT = @pRebuyAmount                       ");
        _sb.AppendLine("       , GTT_REBUY_PCTJ_CONTRIBUTION = @pRebuyPctjContribution  ");
        _sb.AppendLine("       , GTT_EVENT_START_DATE = @pEventStartDate                ");
        _sb.AppendLine("       , GTT_EVENT_FINISH_DATE = @pEventFinishDate              ");
        _sb.AppendLine("       , GTT_GAME_ROUNDS = @pGameRounds                         ");
        _sb.AppendLine("       , GTT_INSCRIPTION_START_DATE = @pInscriptionStartDate    ");
        _sb.AppendLine("       , GTT_INSCRIPTION_FINISH_DATE = @pInscriptionFinishDate  ");
        _sb.AppendLine("       , GTT_GAME_TYPE = @pGameType                             ");
        _sb.AppendLine("       , GTT_RULES = @pRules                                    ");
        _sb.AppendLine("       , GTT_CAPACITY = @pCapacity                              ");
        _sb.AppendLine("       , GTT_SEATS = @pSeats                                    ");
        _sb.AppendLine("       , GTT_ENABLED = @pEnabled                                ");
        _sb.AppendLine("       , GTT_AVAILABLE_TABLES = @pAvailableTables               ");
        _sb.AppendLine(" WHERE   GTT_TOURNAMENT_ID = @pTournamentId                     ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = this.TournamentId;
            _sql_cmd.Parameters.Add("@pTournamentStatus", SqlDbType.Int).Value = this.TournamentStatus;
            _sql_cmd.Parameters.Add("@pTournamentName", SqlDbType.NVarChar, 100).Value = this.TournamentName;
            _sql_cmd.Parameters.Add("@pFixedPrizeAmount", SqlDbType.Money).Value = this.FixedPrizeAmount;
            _sql_cmd.Parameters.Add("@pIncrementPrizeAmount", SqlDbType.Money).Value = this.IncrementPrizeAmount;
            _sql_cmd.Parameters.Add("@pTotalPrizeAmount", SqlDbType.Money).Value = this.TotalPrizeAmount;
            _sql_cmd.Parameters.Add("@pAdmissionAmmount", SqlDbType.Money).Value = this.AdmissionAmount;
            _sql_cmd.Parameters.Add("@pRebuy", SqlDbType.Int).Value = this.Rebuy;
            _sql_cmd.Parameters.Add("@pRebuyAmount", SqlDbType.Money).Value = this.RebuyAmount;
            _sql_cmd.Parameters.Add("@pRebuyPctjContribution", SqlDbType.Money).Value = this.RebuyPctjContribution;
            _sql_cmd.Parameters.Add("@pEventStartDate", SqlDbType.DateTime).Value = this.EventStartDate;
            _sql_cmd.Parameters.Add("@pEventFinishDate", SqlDbType.DateTime).Value = this.EventFinishDate;
            _sql_cmd.Parameters.Add("@pGameRounds", SqlDbType.Int).Value = this.GameRounds;
            _sql_cmd.Parameters.Add("@pInscriptionStartDate", SqlDbType.DateTime).Value = this.InscriptionStartDate;
            _sql_cmd.Parameters.Add("@pInscriptionFinishDate", SqlDbType.DateTime).Value = this.InscriptionFinishDate;
            _sql_cmd.Parameters.Add("@pGameType", SqlDbType.Int).Value = this.GameType;
            _sql_cmd.Parameters.Add("@pRules", SqlDbType.NVarChar, 1024).Value = this.Rules;
            _sql_cmd.Parameters.Add("@pCapacity", SqlDbType.Int).Value = this.Capacity;
            _sql_cmd.Parameters.Add("@pSeats", SqlDbType.Int).Value = this.Seats;
            _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = this.Enabled;
            _sql_cmd.Parameters.Add("@pAvailableTables", SqlDbType.Xml).Value = GamingTablesTournamentGameTableInfo.ToXml(this.AvailableTables);

            if (_sql_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return GAMING_TABLES_TOURNAMENT_OPERATION_RESULT.OK;
            }

            _db_trx.Rollback();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return GAMING_TABLES_TOURNAMENT_OPERATION_RESULT.ERROR;

    } // UpdateTournament

    public GAMING_TABLES_TOURNAMENT_OPERATION_RESULT InsertTournament()
    {
      StringBuilder _sb;
      SqlParameter _sql_parameter;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO   GAMING_TABLES_TOURNAMENTS    ");
        _sb.AppendLine("          (   GTT_TOURNAMENT_STATUS        ");
        _sb.AppendLine("            , GTT_TOURNAMENT_NAME          ");
        _sb.AppendLine("            , GTT_FIXED_PRIZE_AMOUNT       ");
        _sb.AppendLine("            , GTT_INCREMENT_PRIZE_AMOUNT   ");
        _sb.AppendLine("            , GTT_TOTAL_PRIZE_AMOUNT       ");
        _sb.AppendLine("            , GTT_ADMISSION_AMOUNT         ");
        _sb.AppendLine("            , GTT_REBUY                    ");
        _sb.AppendLine("            , GTT_REBUY_AMOUNT             ");
        _sb.AppendLine("            , GTT_REBUY_PCTJ_CONTRIBUTION  ");
        _sb.AppendLine("            , GTT_EVENT_START_DATE         ");
        _sb.AppendLine("            , GTT_EVENT_FINISH_DATE        ");
        _sb.AppendLine("            , GTT_GAME_ROUNDS              ");
        _sb.AppendLine("            , GTT_INSCRIPTION_START_DATE   ");
        _sb.AppendLine("            , GTT_INSCRIPTION_FINISH_DATE  ");
        _sb.AppendLine("            , GTT_GAME_TYPE                ");
        _sb.AppendLine("            , GTT_RULES                    ");
        _sb.AppendLine("            , GTT_CAPACITY                 ");
        _sb.AppendLine("            , GTT_SEATS                    ");
        _sb.AppendLine("            , GTT_ENABLED                  ");
        _sb.AppendLine("            , GTT_AVAILABLE_TABLES         ");
        _sb.AppendLine("          )                                ");
        _sb.AppendLine(" VALUES   (   @pTournamentStatus           ");
        _sb.AppendLine("            , @pTournamentName             ");
        _sb.AppendLine("            , @pFixedPrizeAmount           ");
        _sb.AppendLine("            , @pIncrementPrizeAmount       ");
        _sb.AppendLine("            , @pTotalPrizeAmount           ");
        _sb.AppendLine("            , @pAdmissionAmmount           ");
        _sb.AppendLine("            , @pRebuy                      ");
        _sb.AppendLine("            , @pRebuyAmount                ");
        _sb.AppendLine("            , @pRebuyPctjContribution      ");
        _sb.AppendLine("            , @pEventStartDate             ");
        _sb.AppendLine("            , @pEventFinishDate            ");
        _sb.AppendLine("            , @pGameRounds                 ");
        _sb.AppendLine("            , @pInscriptionStartDate       ");
        _sb.AppendLine("            , @pInscriptionFinishDate      ");
        _sb.AppendLine("            , @pGameType                   ");
        _sb.AppendLine("            , @pRules                      ");
        _sb.AppendLine("            , @pCapacity                   ");
        _sb.AppendLine("            , @pSeats                      ");
        _sb.AppendLine("            , @pEnabled                    ");
        _sb.AppendLine("            , @pAvailableTables            ");
        _sb.AppendLine("          )                                ");
        _sb.AppendLine(" SET   @pSessionId = SCOPE_IDENTITY()      ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = this.TournamentId;
            _sql_cmd.Parameters.Add("@pTournamentStatus", SqlDbType.Int).Value = this.TournamentStatus;
            _sql_cmd.Parameters.Add("@pTournamentName", SqlDbType.NVarChar, 100).Value = this.TournamentName;
            _sql_cmd.Parameters.Add("@pFixedPrizeAmount", SqlDbType.Money).Value = this.FixedPrizeAmount;
            _sql_cmd.Parameters.Add("@pIncrementPrizeAmount", SqlDbType.Money).Value = this.IncrementPrizeAmount;
            _sql_cmd.Parameters.Add("@pTotalPrizeAmount", SqlDbType.Money).Value = this.TotalPrizeAmount;
            _sql_cmd.Parameters.Add("@pAdmissionAmmount", SqlDbType.Money).Value = this.AdmissionAmount;
            _sql_cmd.Parameters.Add("@pRebuy", SqlDbType.Int).Value = this.Rebuy;
            _sql_cmd.Parameters.Add("@pRebuyAmount", SqlDbType.Money).Value = this.RebuyAmount;
            _sql_cmd.Parameters.Add("@pRebuyPctjContribution", SqlDbType.Money).Value = this.RebuyPctjContribution;
            _sql_cmd.Parameters.Add("@pEventStartDate", SqlDbType.DateTime).Value = this.EventStartDate;
            _sql_cmd.Parameters.Add("@pEventFinishDate", SqlDbType.DateTime).Value = this.EventFinishDate;
            _sql_cmd.Parameters.Add("@pGameRounds", SqlDbType.Int).Value = this.GameRounds;
            _sql_cmd.Parameters.Add("@pInscriptionStartDate", SqlDbType.DateTime).Value = this.InscriptionStartDate;
            _sql_cmd.Parameters.Add("@pInscriptionFinishDate", SqlDbType.DateTime).Value = this.InscriptionFinishDate;
            _sql_cmd.Parameters.Add("@pGameType", SqlDbType.Int).Value = this.GameType;
            _sql_cmd.Parameters.Add("@pRules", SqlDbType.NVarChar, 1024).Value = this.Rules;
            _sql_cmd.Parameters.Add("@pCapacity", SqlDbType.Int).Value = this.Capacity;
            _sql_cmd.Parameters.Add("@pSeats", SqlDbType.Int).Value = this.Seats;
            _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = this.Enabled;
            _sql_cmd.Parameters.Add("@pAvailableTables", SqlDbType.Xml).Value = GamingTablesTournamentGameTableInfo.ToXml(this.AvailableTables);

            _sql_parameter = _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt);
            _sql_parameter.Direction = ParameterDirection.Output;
            if (_sql_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();
              this.TournamentID = Int32.Parse(_sql_parameter.Value.ToString());
              return GAMING_TABLES_TOURNAMENT_OPERATION_RESULT.OK;
            }

            _db_trx.Rollback();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return GAMING_TABLES_TOURNAMENT_OPERATION_RESULT.ERROR;
    } // InsertTournament

    public GAMING_TABLES_TOURNAMENT_OPERATION_RESULT DeleteTournament()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("DELETE   GAMING_TABLES_TOURNAMENTS          ");
        _sb.AppendLine(" WHERE   GTT_TOURNAMENT_ID = @pTournamentId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTournamentId", SqlDbType.BigInt).Value = this.TournamentId;

            if (_sql_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return GAMING_TABLES_TOURNAMENT_OPERATION_RESULT.OK;
            }

            _db_trx.Rollback();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return GAMING_TABLES_TOURNAMENT_OPERATION_RESULT.ERROR;
    } // DeleteTournament
    #endregion
  }

  /// <summary>
  /// GamingTablesTournamentListRowEnterEventArgs class. Events args for a row enter on a GamingTablesTournamentList
  /// </summary>
  public class GamingTablesTournamentListRowEnterEventArgs : EventArgs
  {
    #region Public Methods
    public Int64 TournamentId { get; set; }

    public GamingTablesTournamentListRowEnterEventArgs(Int64 tournamentId)
    {
      TournamentId = tournamentId;
    }
    #endregion
  }

  //Events handler for a row enter on a GamingTablesTournamentList
  public delegate void GamingTablesTournamentListRowEnterEventHandler(object sender, GamingTablesTournamentListRowEnterEventArgs e);
}