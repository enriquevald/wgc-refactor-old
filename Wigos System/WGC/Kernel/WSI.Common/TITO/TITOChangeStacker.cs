//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TITO_ProcessChangeStacker.cs
// 
//   DESCRIPTION: Process change stacker
//   AUTHOR     : Javier Fernandez
//
//
//   REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-SEP-2013 JFC    First version.
// 26-NOV-2013 JRM    Refactoring, code clean up
// 30-NOV-2013 JFC    Get or create system cash session when new stacker is inserted  
// 24-JAN-2014 JPJ    Changed class from one project to another (WCP --> Common) 
// 17-FEB-2014 ICS    Added new method to create cage movements on stacker changing
// 26-FEB-2014 DHA    WIGOSTITO-1092: log message must be shown in english
// 05-MAY-2014 DRV    WIGOSTITO-1215: Alarm code change
// 27-OCT-2014 SGB    If terminal status is retired, there's no need to open new money collection and cashier session
// 05-NOV-2014 DRV    WIG-1641: Errror changing stacker when the new stacker is inserted in an other terminal
// 12-MAY-2015 DRV & DHA  WIG-2337: error when a terminal is retired and there is a stacker change
// 22-SEP-2016 FAV    Bug 17806: Mico: Collect with exception
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common.TITO
{
  public static partial class TITO_ChangeStacker
  {

    #region Constants

    public const Int32 AUDIT_CODE_TERMINALS = 9;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Stacker change in TITO mode. 
    //           Updates stackers and money_collections tables.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Terminal container
    //          - Stacker object representing new stacker
    //          - SqlTransaction Trx
    //
    //      - OUTPUT : 
    //
    // RETURNS : true if all ok, otherwise false.
    //
    public static Boolean ChangeStackerInTitoMode(Int32 TerminalId,
                                                  Stacker NewStacker,
                                                  Int64 TransactionId,
                                                  SqlTransaction Trx)
    {
      return ChangeStackerInTitoMode(TerminalId, NewStacker, TransactionId, false, Trx);
    }

    public static Boolean ChangeStackerInTitoMode(Int32 TerminalId,
                                                  Stacker NewStacker,
                                                  Int64 TransactionId,
                                                  Boolean StackerMustBeInserted,
                                                  SqlTransaction Trx)
    {

      String _error_description = String.Empty;

      return ChangeStackerInTitoMode(TerminalId, NewStacker, TransactionId, StackerMustBeInserted,
                              out _error_description, Trx);
    }

    public static Boolean ChangeStackerInTitoMode(Int32 TerminalId,
                                                  Stacker NewStacker,
                                                  Int64 TransactionId,
                                                  Boolean StackerMustBeInserted,
                                                  out String ErrorDescription,
                                                  SqlTransaction Trx)
    {
      String _alarm_description;
      MoneyCollection _money_collection_vo;
      Terminal.TerminalInfo _terminal_info;
      Boolean _assign_stacker_to_terminal;

      // "Error changing stacker."
      ErrorDescription = Resource.String("STR_ERROR_CHANGE_STACKER");

      if (TerminalId <= 0)
      {
        return false;
      }

      if (!Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, Trx))
      {
        return false;
      }

      try
      {
        // Extract old stacker
        _money_collection_vo = MoneyCollection.DB_ReadCurrentMoneyCollection(TerminalId, Trx);
        if (_money_collection_vo == null)
        {
          // Error extracting old stacker, issue alarm
          ErrorDescription = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TERMINAL_EMPTY");

          // TODO: It should defferentiate between calls from WCP or GUI
          Alarm.Register(AlarmSourceCode.TerminalWCP,
                         TerminalId,
                         "TITO_WCP",
                         (UInt32)AlarmCode.WCP_TITO_EmptyTerminal,
                         ErrorDescription,
                         AlarmSeverity.Info);
        }

        if (_money_collection_vo != null)
        {
          //
          // Close MoneyCollectionMeter
          //
          if (!MoneyCollectionMeter.CloseMoneyCollectionMeter(_money_collection_vo.CashierSessionId, Trx))
          {
            Log.Error(String.Format("MoneyCollectionMeter.CloseMoneyCollectionMeter SessionId={0}", _money_collection_vo.CashierSessionId));
            return false;
          }

          if (!_money_collection_vo.DB_ChangeStatusToPendingClosing(Trx))
          {
            Log.Error(String.Format("DB_ChangeStatusToPendingClosing Collection={0}", _money_collection_vo.CollectionId));
            return false;
          }

          if (_money_collection_vo.StackerId != 0 && (NewStacker == null || _money_collection_vo.StackerId != NewStacker.StackerId))
          {
            if (!Stacker.DB_RemoveFromTerminal(_money_collection_vo.StackerId, Trx))
            {
              Log.Error(String.Format("DB_RemoveFromTerminal OldStacker={0}", _money_collection_vo.StackerId));
              return false;
            }
          }
        }

        //
        // Insert new stacker, money collection and cashier session
        //
        if (NewStacker != null && _terminal_info.Status == TerminalStatus.ACTIVE)
        {
          ErrorDescription = null;
          _assign_stacker_to_terminal = true;

          switch (NewStacker.Status)
          {
            case TITO_STACKER_STATUS.DISABLED:
            case TITO_STACKER_STATUS.DEACTIVATED:
              _assign_stacker_to_terminal = false;
              Log.Warning(String.Format("Stacker {0} has not a valid status. No stacker will be assigned.", new object[] { TerminalId }));
              ErrorDescription = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_INSERT_STACKER_ERROR",
                                     NewStacker.StackerId.ToString());

              Alarm.Register(AlarmSourceCode.TerminalWCP,
                             TerminalId,
                             "TITO_WCP",
                             (UInt32)AlarmCode.WCP_TITO_StackerChangeFail,
                             ErrorDescription,
                             AlarmSeverity.Error);
              NewStacker = new Stacker();
              break;

            case TITO_STACKER_STATUS.ACTIVE:
              if ((NewStacker.TerminalPreassigned > 0) && (NewStacker.TerminalPreassigned != TerminalId))
              {
                // DHA 26-FEB-2014: log message must be shown in english
                Log.Warning(String.Format("Stacker {0} is preassigned to terminal {1} but was inserted in terminal {2}.", new object[] { NewStacker.StackerId, NewStacker.TerminalPreassigned, TerminalId }));

                _alarm_description = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_INSERT_STACKER_WITH_WRONG_TERMINAL_PREASSIGNED",
                                                    NewStacker.StackerId,
                                                    NewStacker.TerminalPreassigned,
                                                    TerminalId);
                Alarm.Register(AlarmSourceCode.TerminalWCP,
                               TerminalId,
                               "TITO_WCP",
                               (UInt32)AlarmCode.WCP_TITO_WrongStackerState,
                               _alarm_description,
                               AlarmSeverity.Warning);

              }
              if (NewStacker.InsertedTerminalId > 0 && NewStacker.InsertedTerminalId != TerminalId)
              {
                // DHA 26-FEB-2014: log message must be shown in english
                Log.Error(String.Format("Inserted stacker was assigned to another terminal. Stacker: {0}. Terminal: {1}.", new object[] { NewStacker.StackerId, NewStacker.InsertedTerminalId }));

                ErrorDescription = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_INSERT_STACKER_WITH_WRONG_TERMINAL_ASSIGNED",
                                                     NewStacker.StackerId,
                                                     NewStacker.InsertedTerminalId);

                Alarm.Register(AlarmSourceCode.TerminalWCP,
                               TerminalId,
                               "TITO_WCP",
                               (UInt32)AlarmCode.WCP_TITO_WrongStackerTerminal,
                               ErrorDescription,
                               AlarmSeverity.Error);
                NewStacker = new Stacker();
                _assign_stacker_to_terminal = false;
              }
              break;

            default:
              break;
          }
          if (StackerMustBeInserted && !_assign_stacker_to_terminal)
          {
            return false;
          }
          if (_assign_stacker_to_terminal)
          {
            if (!NewStacker.DB_AssignToTerminal(TerminalId, Trx))
            {
              Log.Error(String.Format("DB_AssignToTerminal Terminal={0}, NewStacker={1}", TerminalId, NewStacker.StackerId));
              return false;
            }
          }
        }
        else
        {
          if (_terminal_info.Status == TerminalStatus.ACTIVE)
          {
            NewStacker = new Stacker();
          }
          else
          {
            //SGB 27-OCT-2014: If terminal status = retired, there's no need to open new money collection and cashier session
            ErrorDescription = String.Empty;
            return true;
          }
        }

        // Here, we can assume that the cashier session associated to the terminal is not open.
        if (!InitializeStacker(TerminalId, NewStacker, TransactionId, Trx))
        {
          Log.Error("ChangeStacker error (TITO mode): InitializeStacker. Terminal Id=" + TerminalId);

          return false;
        }

        ErrorDescription = String.Empty;
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("ChangeStacker error (TITO mode)");

        return false;
      }
    }         // ChangeStackerInTitoMode

    //------------------------------------------------------------------------------
    // PURPOSE : Create new CashierSesssion, TerminalMoneyCollection and MoneyCollectionMeters for NewStacker
    //           
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - NewStacker
    //          - Trx
    //
    //      - OUTPUT : 
    //          - True if cashier session is already created and opened, or not has errors. False otherwise.
    // RETURNS : true if all ok, otherwise false.
    //
    public static Boolean InitializeStacker(Int32 TerminalId, Stacker NewStacker, Int64 TransactionId, SqlTransaction Trx)
    {
      String _terminal_name;
      CashierSessionInfo _cashier_session_info;
      Int64 _money_collection_id;
      Boolean _is_new_session;

      if (NewStacker == null)
      {
        NewStacker = new Stacker();
      }

      if (!Misc.GetTerminalName(TerminalId, out _terminal_name, Trx))
      {
        Log.Error(String.Format("GetTerminalName Terminal={0}, TerminalName={1}", TerminalId, _terminal_name));
        return false;
      }

      _cashier_session_info = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_TITO, Trx, _terminal_name, out _is_new_session);

      if (_cashier_session_info == null)
      {
        Log.Error(String.Format("Cashier.GetSystemCashierSessionInfo: TerminalName={0}", _terminal_name));
        return false;
      }

      // The cashier session already exists, so the money collection also exists. Return, we have all we need.
      if (!_is_new_session)
      {
        return true;
      }

      if (!MoneyCollection.DB_CreateTerminalMoneyCollection(TerminalId, TransactionId, NewStacker.StackerId, _cashier_session_info.CashierSessionId, out _money_collection_id, Trx))
      {
        Log.Error(String.Format("MoneyCollection.DB_CreateMoneyCollection: Terminal={0}, NewStacker={1}, CashierSession={2}, MoneyCollection={3}", TerminalId, NewStacker.StackerId, _cashier_session_info.CashierSessionId, _money_collection_id));
        return false;
      }

      if (!MoneyCollectionMeter.InitializeMoneyCollectionMeter(_cashier_session_info.CashierSessionId, _money_collection_id, TerminalId, Trx))
      {
        Log.Error(String.Format("MoneyCollectionMeter.InitializeMoneyCollectionMeter: CashierSession={0}, MoneyCollection={1}, Terminal={2}", _cashier_session_info.CashierSessionId, _money_collection_id, TerminalId));
        return false;
      }

      return true;
    } // InitializeStacker

    //------------------------------------------------------------------------------
    // PURPOSE : Stacker change in TITO mode when Terminal is not accessible. 
    //
    //  PARAMS :
    //      - INPUT :
    //          - Terminal container
    //          - Stacker object representing new stacker
    //          - SqlTransaction Trx
    //
    //      - OUTPUT : 
    //
    // RETURNS : true if all ok, otherwise false.
    //
    public static Boolean OfflineChangeStackerInTitoMode(Int32 TerminalId,
                                                         Stacker NewStacker,
                                                         SqlTransaction Trx,
                                                         Int32 UserId,
                                                         String UserName)
    {
      return OfflineChangeStackerInTitoMode(TerminalId, NewStacker, Trx, UserId, UserName, false, true);
    }
    public static Boolean OfflineChangeStackerInTitoMode(Int32 TerminalId,
                                                        Stacker NewStacker,
                                                        SqlTransaction Trx,
                                                        Int32 UserId,
                                                        String UserName,
                                                        Boolean StackerMustBeInserted,
                                                        Boolean AuditStackerChange)
    { 
      String ErrorDescrption = String.Empty;
      return OfflineChangeStackerInTitoMode(TerminalId, NewStacker, Trx,
                                              UserId, UserName, StackerMustBeInserted, AuditStackerChange, out ErrorDescrption);
    
    }

    public static Boolean OfflineChangeStackerInTitoMode(Int32 TerminalId,
                                                         Stacker NewStacker,
                                                         SqlTransaction Trx,
                                                         Int32 UserId,
                                                         String UserName,
                                                         Boolean StackerMustBeInserted,
                                                         Boolean AuditStackerChange,
                                                         out String ErrorDescription)
    {
      String _auditStr;
      _auditStr = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_STACKER_CHANGED", 0);
      ErrorDescription = String.Empty;

      try
      {
        //WSI.WCP.WCP_SASMeters.GetSasMeters(WSI.WCP.SASMeters.ENUM_METERS_TYPE.BILLS_AND_TICKETS, _tcp_client.InternalId, out _terminal_meters, _sql_trx);
        //WSI.WCP.WCP_SASMeters.NewProcessTerminalSasMeters(WSI.WCP.SASMeters.ENUM_METERS_TYPE.BILLS_AND_TICKETS, _terminal_meters, _tcp_client.InternalId, _sql_trx);

        if (MB_CASHIER_SESSION_CLOSE_STATUS.ERROR != (MB_CASHIER_SESSION_CLOSE_STATUS)Cashier.WCPCashierSessionPendingClosing(TerminalId, Trx))
        // We don't send any stacker, we are closing the actual one and leaving the terminal without one.
        {

          if (WSI.Common.TITO.TITO_ChangeStacker.ChangeStackerInTitoMode(TerminalId, NewStacker, 0, StackerMustBeInserted, out ErrorDescription, Trx))
          {
            // Reset terminal stacker counter
            if (TerminalStatusFlags.SetTerminalStatus(TerminalId, TerminalStatusFlags.WCP_TerminalEvent.ChangeStacker, Trx))
            {
              // Audit is generated due to the remove stacker action
              if (AuditStackerChange)
              {
                WSI.Common.Auditor.Audit(ENUM_GUI.WIGOS_GUI,            // GuiId
                                         UserId,                        // UserId
                                         UserName,                      // UserName
                                         Environment.MachineName,       // ComputerName
                                         AUDIT_CODE_TERMINALS,          // AuditCode
                                         16000 + 4553,                  // NlsId (GUI)
                                         _auditStr, "", "", "", "");
              }
              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }  // OfflineChangeStackerInTitoMode

    //------------------------------------------------------------------------------
    // PURPOSE : Insert a cage movement when stacker has been changed
    //
    //  PARAMS :
    //      - INPUT :
    //          - CSInfo
    //          - SqlTransaction Trx
    //
    //      - OUTPUT : 
    //
    // RETURNS : true if all ok, otherwise false.
    //
    //   NOTES : Only insert movement if cashier session had a cage session previously
    //
    public static Boolean InsertCageMovement(CashierSessionInfo CSInfo,
                                             SqlTransaction Trx)
    {
      Int64 _money_collection_id;
      Int64 _cage_session_id;
      Int64 _movement_id;

      // Get cage session id
      //if (!Cage.GetCageSessionId(CSInfo.CashierSessionId, out _cage_session_id, Trx))
      if (!Cage.GetCageSessionIdFromCashierSessionId(CSInfo.CashierSessionId, out _cage_session_id, Trx))
      {
        // There is no cage session linked to the cashier session
        return true;
      }

      // Get money collection id
      if (!MoneyCollection.GetOrCreateTerminalMoneyCollectionId(CSInfo.CashierSessionId, CSInfo.TerminalId, out _money_collection_id, Trx))
      {
        Log.Error("TITOChangeStacker.InsertCageMovement: Error getting money collection id. Cashier session id -> " + CSInfo.CashierSessionId);
        return false;
      }

      // Create cage movement
      if (!Cage.CreateCageMovement(_cage_session_id, CSInfo, Cage.CageOperationType.FromTerminal, Cage.CageStatus.Sent,
                                   _money_collection_id, out _movement_id, Trx))
      {
        Log.Error("TITOChangeStacker.InsertCageMovement: Error creating cage movement. Cashier session id -> " + CSInfo.CashierSessionId);
        return false;
      }

      // Create pending cage movement
      if (!Cage.CreateCagePendingMovement(_movement_id, Trx))
      {
        Log.Error("TITOChangeStacker.InsertCageMovement: Error creating pending cage movement. Cashier session id -> " + CSInfo.CashierSessionId);
        return false;
      }

      return true;
    }
  }
}
