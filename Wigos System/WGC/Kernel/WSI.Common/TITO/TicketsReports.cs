//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Intl.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TicketsReports.cs
// 
//   DESCRIPTION: 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-DEC-2014 DRV    First release.
// 12-JAN-2015 SGB    Modification of the 'where' to filter dates.
// 13-FEB-2015 DCS    Fixed Bug WIG-2048: Error in date filter, includes tickets matching the closing date
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common.TITO
{
  public static class TicketsReports
  {
    public const Int32 SQL_TICKET_DATE = 0;
    public const Int32 SQL_PREVIOUS_CREATED_COUNT = 1;
    public const Int32 SQL_PREVIOUS_CREATED_AMOUNT = 2;
    public const Int32 SQL_PREVIOUS_CASHED_COUNT = 3;
    public const Int32 SQL_PREVIOUS_CASHED_AMOUNT = 4;
    public const Int32 SQL_PREVIOUS_EXPIRED_COUNT = 5;
    public const Int32 SQL_PREVIOUS_EXPIRED_AMOUNT = 6;
    public const Int32 SQL_CURRENT_VALID_COUNT = 7;
    public const Int32 SQL_CURRENT_VALID_AMOUNT = 8;
    public const Int32 SQL_CURRENT_EXPIRED_COUNT = 9;
    public const Int32 SQL_CURRENT_EXPIRED_AMOUNT = 10;
    //------------------------------------------------------------------------------
    // PURPOSE : Returns a Datatable containing the ticketOutReport.
    //
    //  PARAMS :
    //      - INPUT : InitialDate: Datetime
    //                FinalDate:   Datetime
    //                Result: Out parameter Datatable
    //
    //      - OUTPUT :
    //
    // RETURNS : true-> ok false-> error
    //
    //   NOTES :
    //
    public static Boolean GetTicketOutReport(DateTime InitialDate, DateTime FinalDate, out DataTable Result)
    {
      StringBuilder _sb;
      String _base_date;

      _base_date = "01/01/2007 " + GeneralParam.GetInt32("WigosGUI", "ClosingTime").ToString("00") + ":00:00";

      Result = new DataTable();
      _sb = new StringBuilder();
      _sb.AppendLine("         SELECT   DATEADD(DAY, DATEDIFF(HOUR, '" + _base_date + "', TI.TICKET_FILTER_DATE)/24 , '" + _base_date + "')             ");
      _sb.AppendLine(" 		  	        , COUNT(CASE WHEN TI.OPERATION_TYPE = 0 THEN 1 END) AS PREVIOUS_CREATED_COUNT                                       ");
      _sb.AppendLine("                , SUM(CASE WHEN TI.OPERATION_TYPE = 0 THEN TI.ti_amount ELSE 0 END)  AS PREVIOUS_CREATED_AMOUNT                     ");
      _sb.AppendLine("                , COUNT(CASE WHEN ti_status IN (@pRedeemdedStatus, @pCancelledStatus) AND TI.OPERATION_TYPE = 2 THEN 1 END) AS PREVIOUS_CASHED_COUNT                ");
      _sb.AppendLine("                , SUM(CASE WHEN ti_status IN (@pRedeemdedStatus, @pCancelledStatus) AND TI.OPERATION_TYPE = 2 THEN ti_amount ELSE 0 END) AS PREVIOUS_CASHED_AMOUNT ");
      _sb.AppendLine("                , COUNT(CASE WHEN TI.OPERATION_TYPE = 1 THEN 1 END) AS PREVIOUS_EXPIRED_COUNT                                       ");
      _sb.AppendLine("                , SUM(CASE WHEN TI.OPERATION_TYPE = 1 THEN TI.TI_AMOUNT ELSE 0 END) AS PREVIOUS_EXPIRED_AMOUNT                      ");
      _sb.AppendLine("                , COUNT(CASE WHEN TI.OPERATION_TYPE = 3  THEN 1 END) AS CURRENT_VALID_COUNT                                         ");
      _sb.AppendLine("                , SUM(CASE WHEN TI.OPERATION_TYPE = 3 THEN TI.ti_amount ELSE 0 END) AS CURRENT_VALID_AMOUNT                         ");
      _sb.AppendLine("                , COUNT(CASE WHEN TI.OPERATION_TYPE = 4  THEN 1 END) AS CURRENT_EXPIRED                                             ");
      _sb.AppendLine("                , SUM(CASE WHEN TI.OPERATION_TYPE = 4 THEN TI.ti_amount ELSE 0 END) AS CURRENT_EXPIRED_AMOUNT                              ");
      _sb.AppendLine(" 		       FROM (                                                                                                                   ");
      _sb.AppendLine(" 	               SELECT   ti_amount                                                                                                 ");
      _sb.AppendLine(" 	                      , ti_status                                                                                                 ");
      _sb.AppendLine(" 	                      , DATEADD(day, DATEDIFF(day, 0, TI_CREATED_DATETIME), 1) AS TICKET_DATE                                     ");
      _sb.AppendLine("                        , DATEADD(day, 1, TI_CREATED_DATETIME) AS TICKET_FILTER_DATE                                                ");
      _sb.AppendLine(" 	                      , 0 as OPERATION_TYPE                                                                                       ");
      _sb.AppendLine(" 			             FROM   TICKETS                                                                                                   ");
      _sb.AppendLine(" " + GetSqlWhereCreated(InitialDate, FinalDate) + " ");
      _sb.AppendLine("              UNION ALL                                                                                                             ");
      _sb.AppendLine("                 SELECT   ti_amount                                                                                                 ");
      _sb.AppendLine(" 	                      , ti_status                                                                                                 ");
      _sb.AppendLine(" 	                      , DATEADD(day, DATEDIFF(day, 0, TI_EXPIRATION_DATETIME), 1) AS TICKET_DATE                                  ");
      _sb.AppendLine("                        , DATEADD(day, 1, TI_EXPIRATION_DATETIME) AS TICKET_FILTER_DATE                                             ");
      _sb.AppendLine(" 	                      , 1 as OPERATION_TYPE                                                                                       ");
      _sb.AppendLine(" 			             FROM   TICKETS                                                                                                   ");
      _sb.AppendLine(" " + GetSqlWhereCashed(InitialDate, FinalDate) + " ");
      _sb.AppendLine("              UNION ALL                                                                                                             ");
      _sb.AppendLine("                 SELECT   ti_amount                                                                                                 ");
      _sb.AppendLine(" 	                      , ti_status                                                                                                 ");
      _sb.AppendLine(" 	                      , DATEADD(day, DATEDIFF(day, 0, TI_LAST_ACTION_DATETIME), 1) AS TICKET_DATE                                 ");
      _sb.AppendLine("                        , DATEADD(day, 1, TI_LAST_ACTION_DATETIME) AS TICKET_FILTER_DATE                                            ");
      _sb.AppendLine(" 	                      , 2 as OPERATION_TYPE                                                                                       ");
      _sb.AppendLine(" 			             FROM   TICKETS                                                                                                   ");
      _sb.AppendLine(" " + GetSqlWhereExpired(InitialDate, FinalDate) + " ");
      _sb.AppendLine("              UNION ALL                                                                                                             ");
      _sb.AppendLine("                 SELECT   ti_amount                                                                                                 ");
      _sb.AppendLine(" 	                      , ti_status                                                                                                 ");
      _sb.AppendLine(" 	                      , DATEADD(day, DATEDIFF(day, 0, TI_CREATED_DATETIME), 0) AS TICKET_DATE                                     ");
      _sb.AppendLine("                        , TI_CREATED_DATETIME AS TICKET_FILTER_DATE                                                                 ");
      _sb.AppendLine(" 	                      , 3 as OPERATION_TYPE                                                                                       ");
      _sb.AppendLine(" 			             FROM   TICKETS                                                                                                   ");
      _sb.AppendLine(" " + GetSqlWhereNotCashed(InitialDate, FinalDate) + " ");
      _sb.AppendLine("              UNION ALL                                                                                                             ");
      _sb.AppendLine("                 SELECT   ti_amount                                                                                                 ");
      _sb.AppendLine(" 	                      , ti_status                                                                                                 ");
      _sb.AppendLine(" 	                      , DATEADD(day, DATEDIFF(day, 0, TI_EXPIRATION_DATETIME), 0) AS TICKET_DATE                                  ");
      _sb.AppendLine("                        , TI_EXPIRATION_DATETIME AS TICKET_FILTER_DATE                                                              ");
      _sb.AppendLine(" 	                      , 4 as OPERATION_TYPE                                                                                       ");
      _sb.AppendLine(" 			             FROM   TICKETS                                                                                                   ");
      _sb.AppendLine(" " + GetSqlWhereValid(InitialDate, FinalDate) + " ");
      _sb.AppendLine(" 	            ) AS TI                                                                                                               ");
      _sb.AppendLine(" " + GetSqlWhere(InitialDate, FinalDate) +" ");
      _sb.AppendLine("        GROUP BY   DATEDIFF(HOUR, '" + _base_date + "', TI.TICKET_FILTER_DATE)/24                                                  ");
      _sb.AppendLine("        ORDER BY   DATEDIFF(HOUR, '" + _base_date + "', TI.TICKET_FILTER_DATE)/24                                                  ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (InitialDate != DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pIniDateRange", SqlDbType.DateTime).Value = InitialDate;
            }

            if (FinalDate != DateTime.MaxValue)
            {
              _cmd.Parameters.Add("@pFinDateRange", SqlDbType.DateTime).Value = FinalDate;
            }
            _cmd.Parameters.Add("@pCancelledStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.CANCELED;
            _cmd.Parameters.Add("@pRedeemdedStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.REDEEMED;
            _cmd.Parameters.Add("@pExpiredStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.EXPIRED;
            _cmd.Parameters.Add("@pValidStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(Result);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    private static String GetSqlWhereCreated(DateTime IniDate, DateTime FinDate)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" WHERE ");
      }

      if (IniDate != DateTime.MinValue)
      {
        _sb.AppendLine(" TI_CREATED_DATETIME >= DATEADD(DAY, -2, @pIniDateRange) AND ");
      }

      if (FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" TI_CREATED_DATETIME < DATEADD(DAY, -1, @pFinDateRange) ");
      }
      else
      {
        _sb = _sb.Replace(" AND ", "");
      }

      return _sb.ToString();

    }

    private static String GetSqlWhereCashed(DateTime IniDate, DateTime FinDate)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" WHERE ");
      }

      if (IniDate != DateTime.MinValue)
      {
        _sb.AppendLine(" TI_EXPIRATION_DATETIME >= DATEADD(DAY, -2, @pIniDateRange) AND ");
      }

      if (FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" TI_EXPIRATION_DATETIME < DATEADD(DAY, -1, @pFinDateRange) ");
      }
      else
      {
        _sb = _sb.Replace(" AND ", "");
      }

      if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" AND ");
      }
      else
      {
        _sb.AppendLine(" WHERE ");
      }

      _sb.AppendLine(" TI_STATUS = @pExpiredStatus ");

      return _sb.ToString();

    }

    private static String GetSqlWhereExpired(DateTime IniDate, DateTime FinDate)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" WHERE ");
      }

      if (IniDate != DateTime.MinValue)
      {
        _sb.AppendLine(" TI_LAST_ACTION_DATETIME >= DATEADD(DAY, -2, @pIniDateRange) AND ");
      }

      if (FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" TI_LAST_ACTION_DATETIME < DATEADD(DAY, -1, @pFinDateRange) ");
      }
      else
      {
        _sb = _sb.Replace(" AND ", "");
      }

      return _sb.ToString();

    }

    private static String GetSqlWhereNotCashed(DateTime IniDate, DateTime FinDate)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" WHERE ");
      }

      if (IniDate != DateTime.MinValue)
      {
        _sb.AppendLine(" TI_CREATED_DATETIME >= DATEADD(DAY, -1, @pIniDateRange) AND ");
      }

      if (FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" TI_CREATED_DATETIME < @pFinDateRange ");
      }
      else
      {
        _sb = _sb.Replace(" AND ", "");
      }

      if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" AND ");
      }
      else
      {
        _sb.AppendLine(" WHERE ");
      }

      _sb.AppendLine(" ti_status = @pValidStatus ");

      return _sb.ToString();

    }

    private static String GetSqlWhereValid(DateTime IniDate, DateTime FinDate)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" WHERE ");
      }

      if (IniDate != DateTime.MinValue)
      {
        _sb.AppendLine(" TI_EXPIRATION_DATETIME >= DATEADD(DAY, -1, @pIniDateRange) AND ");
      }

      if (FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" TI_EXPIRATION_DATETIME < @pFinDateRange ");
      }
      else
      {
        _sb = _sb.Replace(" AND ", "");
      }
      
      if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" AND ");
      }
      else
      {
        _sb.AppendLine(" WHERE ");
      }

      _sb.AppendLine(" ti_status = @pExpiredStatus ");

      return _sb.ToString();

    }

    private static String GetSqlWhere(DateTime IniDate, DateTime FinDate)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" WHERE ");
      }
      else
      {
        _sb.AppendLine(" WHERE ti.TICKET_DATE is not null ");
      }

      if (IniDate != DateTime.MinValue)
      {
        _sb.AppendLine(" TI.TICKET_FILTER_DATE >= @pIniDateRange AND ");
      }

      if (FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" TI.TICKET_FILTER_DATE < @pFinDateRange");
      }
      else
      {
        _sb = _sb.Replace(" AND ", "");
      }

      return _sb.ToString();
 
    }
  }
}
