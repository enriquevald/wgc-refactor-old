//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: HandPay.cs
// 
//   DESCRIPTION: HandPay class
// 
//        AUTHOR: Joshwa Marcalle
// 
// CREATION DATE: 24-JAN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-JAN-2014 JRM    First version. 
// 28-FEB-2014 ICS    Moved functions from TITO_HandPay.cs
// 21-MAR-2014 JCOR   Fixed Bug WIG-1154: Handpay by ticket offline doesn't report correctly.
// 25-MAR-2014 JCOR   Added DB_UpdateHandPayCandidatePlaySession
// 01-APR-2014 ICS    Refactoring and code cleaning
// 04-APR-2014 AMF    Moved function DB_UpdateHandPayCandidatePlaySession to WCP_BusinessLogic
// 04-SEP-2014 JCO    Added level and progressive_id.
// 08-SEP-2014 JCO    Added hp_id.
// 15-OCT-2014 MPO    WIG-1504: The ticket jackpot is registered with incorrect movement
// 20-NOV-2014 DRV    Fixed Bug: WIG-1716: Jackpot handpay produces imbalance
// 20-ABR-2016 JML    Fixed Bug: 12154: Dual Currency - "Site Jackpot": Don't exchanged prize to machine currency
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Drawing;
using WSI.Common.TITO;

namespace WSI.Common.TITO
{
  public class HandPay
  {
    #region Enums

    public enum TICKET_HANDPAY_STATUS
    {
      NOT_FOUND = -1,
      VALID = 0,
      EXPIRED = 1,
      INCORRECT_AMOUNTS = 2,
      REDEEMED = 3
    }

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Link a handpay with a ticket.
    // 
    //  PARAMS:
    //      - INPUT:
    //              - TicketId
    //              - TerminalId
    //              - TransactionId
    //              - HandpayAmount
    //              - HandpayType
    //              - SqlTransaction
    //
    //      - OUTPUT:
    //              - RowsAffected
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean DB_UpdateHandPayTicket(Int64 TicketId,
                                             Int32 TerminalId,
                                             Int64 TransactionId,
                                             Decimal HandpayAmount,
                                             HANDPAY_TYPE HandpayType,
                                             out Int32 RowsAffected,
                                             SqlTransaction Trx)
    {
      HANDPAY_STATUS _hp_status;
      return DB_UpdateHandPayTicket(TicketId, TerminalId, TransactionId, HandpayAmount, HandpayType, out RowsAffected, out _hp_status, Trx);
    }
    public static Boolean DB_UpdateHandPayTicket(Int64 TicketId,
                                                 Int32 TerminalId,
                                                 Int64 TransactionId,
                                                 Decimal HandpayAmount,
                                                 HANDPAY_TYPE HandpayType,
                                                 out Int32 RowsAffected,
                                                 out HANDPAY_STATUS HandPayStatus,
                                                 SqlTransaction Trx)
    {
      StringBuilder _sb;

      HandPayStatus = 0;
      RowsAffected = 0;

      _sb = new StringBuilder();

      _sb.AppendLine("UPDATE   HANDPAYS                               ");
      _sb.AppendLine("   SET   HP_TICKET_ID = @pTicketID              ");
      _sb.AppendLine("OUTPUT   INSERTED.HP_STATUS                     ");
      _sb.AppendLine(" WHERE   HP_TERMINAL_ID       = @pTerminalID    ");
      _sb.AppendLine("   AND   HP_AMOUNT            = @pHandpayAmount ");
      _sb.AppendLine("   AND   HP_TYPE              = @pHandpayType   ");
      _sb.AppendLine("   AND   HP_TRANSACTION_ID    = @pTransactionID ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTicketID", SqlDbType.BigInt).Value = TicketId;
          _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pHandpayAmount", SqlDbType.Money).Value = HandpayAmount;
          _cmd.Parameters.Add("@pHandpayType", SqlDbType.Int).Value = HandpayType;
          _cmd.Parameters.Add("@pTransactionID", SqlDbType.BigInt).Value = TransactionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            RowsAffected = 0;
            while (_reader.Read())
            {
              HandPayStatus = (HANDPAY_STATUS)_reader.GetInt32(0);
              RowsAffected++;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateHandPayTicket

    //------------------------------------------------------------------------------
    // PURPOSE: Insert a new Handpay.
    // 
    //  PARAMS:
    //      - INPUT:
    //            - TicketId
    //            - TerminalId
    //            - HandPayAmount
    //            - HandPayType
    //            - TerminalName
    //            - ProviderId
    //            - LastDateTimeReported
    //            - SqlTransaction
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean DB_InsertNewHandPay(Int64 TicketId,
                                              Int32 TerminalId,
                                              Int64 TransactionId,
                                              Decimal HandPayAmount,
                                              HANDPAY_TYPE HandPayType,
                                              String TerminalName,
                                              String ProviderId,
                                              DateTime LastDateTimeReported,
                                              SqlTransaction Trx)
    {
      StringBuilder _sb;
      Decimal _sys_amt;
      String _egm_iso;
      Decimal _egm_amt;

      _sb = new StringBuilder();
      _sb.AppendLine("INSERT   INTO HANDPAYS        ");
      _sb.AppendLine("       ( HP_TERMINAL_ID       ");
      _sb.AppendLine("       , HP_DATETIME          ");
      _sb.AppendLine("       , HP_PREVIOUS_METERS   ");
      _sb.AppendLine("       , HP_AMOUNT            ");
      _sb.AppendLine("       , HP_TE_NAME           ");
      _sb.AppendLine("       , HP_TE_PROVIDER_ID    ");
      _sb.AppendLine("       , HP_TYPE              ");
      _sb.AppendLine("       , HP_TICKET_ID         ");
      _sb.AppendLine("       , HP_TRANSACTION_ID    ");
      _sb.AppendLine("       , HP_AMT0              ");
      _sb.AppendLine("       , HP_CUR0              ");
      _sb.AppendLine("       )                      ");
      _sb.AppendLine("VALUES ( @pTerminalId         ");
      _sb.AppendLine("       , GETDATE()            ");
      _sb.AppendLine("       , @pPreviousMeters     ");
      _sb.AppendLine("       , @pHandPayAmount      ");
      _sb.AppendLine("       , @pTermName           ");
      _sb.AppendLine("       , @pProviderId         ");
      _sb.AppendLine("       , @pHandpayType        ");
      _sb.AppendLine("       , @pTicketId           ");
      _sb.AppendLine("       , @pTransactionId      ");
      _sb.AppendLine("       , @pAmount0            ");
      _sb.AppendLine("       , @pCurren0            ");
      _sb.AppendLine("       )                      ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          WSI.Common.TITO.HandPay.GetFloorDualCurrency(HandPayAmount, TerminalId, out _sys_amt, out _egm_amt, out _egm_iso, Trx);

          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pPreviousMeters", SqlDbType.DateTime).Value = LastDateTimeReported;
          _cmd.Parameters.Add("@pHandPayAmount", SqlDbType.Money).Value = _sys_amt;
          _cmd.Parameters.Add("@pTermName", SqlDbType.NVarChar).Value = TerminalName;
          _cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar).Value = ProviderId;
          _cmd.Parameters.Add("@pHandpayType", SqlDbType.Int).Value = ((int)HandPayType);
          _cmd.Parameters.Add("@pAmount0", SqlDbType.Money).Value = (Decimal)_egm_amt;
          _cmd.Parameters.Add("@pCurren0", SqlDbType.NVarChar, 3).Value = _egm_iso;

          if (TicketId == 0)
          {
            _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
          }

          if (TransactionId == 0)
          {
            _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
          }

          _cmd.Parameters.Add("@pLastDatetimeReported", SqlDbType.DateTime).Value = LastDateTimeReported;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_InsertNewHandPay


    public static Boolean GetFloorDualCurrency(Decimal HandPayAmount, Int32 TerminalId, out Decimal SysAmt, out Decimal EGM_Amt, out String EGM_Cur, SqlTransaction Trx)
    {
      String _sys_iso;
      Object _obj;
      Boolean _return;

      _return = false;

      _sys_iso = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
      EGM_Cur = _sys_iso;
      EGM_Amt = HandPayAmount;
      SysAmt = HandPayAmount;

      try
      {
        if (GeneralParam.GetBoolean("FloorDualCurrency", "Enabled", false))//&& this.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.TERMINAL)
        {
          using (SqlCommand _sql_cmd_te_iso2 = new SqlCommand("SELECT TE_ISO_CODE FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId", Trx.Connection, Trx))
          {
            _sql_cmd_te_iso2.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
            _obj = _sql_cmd_te_iso2.ExecuteScalar();
            if (_obj != null && _obj != DBNull.Value)
            {
              EGM_Cur = (String)_obj;
            }
          }
          SysAmt = CurrencyExchange.GetExchange(HandPayAmount, EGM_Cur, _sys_iso, Trx);

          // HP_AMOUNT
          HandPayAmount = (Decimal)SysAmt;
          _return = true;
        }
      }
      catch
      {
        _return = false;
      }
      return _return;
    }

    public static Boolean GetAmountToMachine(MultiPromos.AccountBalance LocalAmount, Int32 TerminalId, out MultiPromos.AccountBalance EGM_Amt, SqlTransaction Trx)
    {
      String _sys_iso;
      Object _obj;
      Boolean _return;
      String _EGM_Cur;

      _return = false;

      _sys_iso = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
      _EGM_Cur = _sys_iso;
      EGM_Amt = LocalAmount;

      try
      {
        if (GeneralParam.GetBoolean("FloorDualCurrency", "Enabled", false))
        {
          using (SqlCommand _sql_cmd_te_iso2 = new SqlCommand("SELECT TE_ISO_CODE FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId", Trx.Connection, Trx))
          {
            _sql_cmd_te_iso2.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
            _obj = _sql_cmd_te_iso2.ExecuteScalar();
            if (_obj != null && _obj != DBNull.Value)
            {
              _EGM_Cur = (String)_obj;
            }
          }
          EGM_Amt = CurrencyExchange.GetExchange(LocalAmount, _sys_iso, _EGM_Cur, Trx);

          _return = true;
        }
      }
      catch
      {
        _return = false;
      }
      return _return;
    }

    public static Boolean GetAmountFromMachine(MultiPromos.AccountBalance MachineAmount, Int32 TerminalId, out MultiPromos.AccountBalance LocalAmount, SqlTransaction Trx)
    {
      String _sys_iso;
      Object _obj;
      Boolean _return;
      String _EGM_Cur;

      _return = false;

      _sys_iso = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
      _EGM_Cur = _sys_iso;
      LocalAmount = MachineAmount;

      try
      {
        if (GeneralParam.GetBoolean("FloorDualCurrency", "Enabled", false))
        {
          using (SqlCommand _sql_cmd_te_iso2 = new SqlCommand("SELECT TE_ISO_CODE FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId", Trx.Connection, Trx))
          {
            _sql_cmd_te_iso2.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
            _obj = _sql_cmd_te_iso2.ExecuteScalar();
            if (_obj != null && _obj != DBNull.Value)
            {
              _EGM_Cur = (String)_obj;
            }
          }
          LocalAmount = CurrencyExchange.GetExchange(MachineAmount, _EGM_Cur, _sys_iso, Trx);

          _return = true;
        }
      }
      catch
      {
        _return = false;
      }
      return _return;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Get HandPay id based on SequenceId.
    // 
    //  PARAMS:
    //      - INPUT:
    //              - TerminalId: Int32
    //              - TransactionId: Int64
    //              - HandpayCents: Int64
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //              - DoesHandPayExists: Boolean
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean DB_DoesHandPayExists(Int32 TerminalId,
                                               Int64 TransactionId,
                                               Int64 HandpayCents,
                                               out Boolean DoesHandPayExists,
                                               SqlTransaction Trx)
    {
      Decimal _handpay_amount;

      _handpay_amount = (Decimal)HandpayCents / 100;

      return DB_DoesHandPayExists(TerminalId, TransactionId, HANDPAY_TYPE.UNKNOWN, _handpay_amount, out DoesHandPayExists, Trx);

    } // DB_DoesHandPayExists

    //------------------------------------------------------------------------------
    // PURPOSE: Get HandPay id based on SequenceId.
    // 
    //  PARAMS:
    //      - INPUT:
    //              - TerminalId: Int32
    //              - TransactionId: Int64
    //              - HandpayType: HANDPAY_TYPE
    //              - HandpayAmount: Decimal
    //              - SqlTransaction
    //
    //      - OUTPUT:
    //              - DoesHandPayExists: Boolean
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean DB_DoesHandPayExists(Int32 TerminalId,
                                               Int64 TransactionId,
                                               HANDPAY_TYPE HandpayType,
                                               Decimal HandpayAmount,
                                                out Boolean DoesHandPayExists,
                                                SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      DoesHandPayExists = false;

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   1 ");
      _sb.AppendLine("  FROM   HANDPAYS                            ");
      _sb.AppendLine(" WHERE   HP_TERMINAL_ID       = @pTerminalID    ");
      _sb.AppendLine("   AND   HP_AMOUNT            = @pHandpayAmount ");
      _sb.AppendLine("   AND   HP_TRANSACTION_ID    = @pTransactionID ");

      if (HandpayType != HANDPAY_TYPE.UNKNOWN)
      {
        _sb.AppendLine("   AND   HP_TYPE              = @pHandpayType   ");
      }

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pHandpayAmount", SqlDbType.Money).Value = HandpayAmount;
          _cmd.Parameters.Add("@pHandpayType", SqlDbType.Int).Value = HandpayType;
          _cmd.Parameters.Add("@pTransactionID", SqlDbType.BigInt).Value = TransactionId;

          _obj = _cmd.ExecuteScalar();
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          DoesHandPayExists = true;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_DoesHandPayExists



    //------------------------------------------------------------------------------
    // PURPOSE : Check if ticket has a handpay and get parameters for the process
    // 
    //  PARAMS :
    //      - INPUT :
    //          - Ticket: The ticket data to check
    //
    //      - OUTPUT :
    //          - HandpayParams: To set the parameters for the handpay process
    //
    // RETURNS :
    //      - True : Ticket has a valid handpay
    //      - False : Ticket hasn't a valid handpay
    // 
    public static TICKET_HANDPAY_STATUS GetTicketHandpayStatus(Ticket Ticket, SqlTransaction Trx)
    {
      Handpays.HandpayRegisterOrCancelParameters _HandpayParams;

      return GetTicketHandpayStatus(Ticket, out _HandpayParams, Trx);
    }

    public static TICKET_HANDPAY_STATUS GetTicketHandpayStatus(Ticket Ticket, out Handpays.HandpayRegisterOrCancelParameters HandpayParams, SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _denomination_str;
      String _denomination_value;
      Currency _played_amount;
      Int32 _handpay_time_period;
      Int32 _handpay_cancel_time_period;

      HandpayParams = null;

      // Check that is a Jackpot, Handpay or Offline Ticket
      if (!(Ticket.TicketType == TITO_TICKET_TYPE.JACKPOT || Ticket.TicketType == TITO_TICKET_TYPE.HANDPAY || Ticket.TicketType == TITO_TICKET_TYPE.OFFLINE))
      {
        return TICKET_HANDPAY_STATUS.NOT_FOUND;
      }

      try
      {
        Handpays.GetParameters(out _handpay_time_period, out _handpay_cancel_time_period);

        // Query for the ticket and data 
        // - We check if handpay remains unpayed and the amounts are equal

        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   HP_TYPE ");                    // 0
        _sb.AppendLine("          , HP_DATETIME ");                // 1
        _sb.AppendLine("          , dbo.ApplyExchange2(ISNULL(HP_AMT0, HP_AMOUNT), ISNULL(HP_CUR0, @pNatCur), @pNatCur) AS HP_AMOUNT ");                  // 2
        _sb.AppendLine("          , HP_TERMINAL_ID ");             // 3
        _sb.AppendLine("          , TE_NAME ");                    // 4
        _sb.AppendLine("          , TE_PROVIDER_ID ");             // 5
        _sb.AppendLine("          , (CASE WHEN HP_PLAY_SESSION_ID IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END) AS HAS_PLAY_SESSION_ID");  // 6
        _sb.AppendLine("          , (CASE WHEN HP_MOVEMENT_ID IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END) AS HAS_MOVEMENT_ID");          // 7
        _sb.AppendLine("          , ISNULL (HP_LEVEL, 0) AS HP_LEVEL ");                                                                          // 8
        _sb.AppendLine("          , ISNULL (HP_PROGRESSIVE_ID, 0) AS HP_PROGRESSIVE_ID ");                                                        // 9
        _sb.AppendLine("          , HP_ID ");                      // 10
        _sb.AppendLine("          , isNull(HP_AMT0, HP_AMOUNT) AS HP_AMT0 ");   // 11
        _sb.AppendLine("          , isNull(HP_CUR0, @pNatCur) AS HP_CUR0 ");          // 12
        _sb.AppendLine("          , HP_AMT1 ");                                 // 13
        _sb.AppendLine("          , HP_CUR1 ");                                 // 14 
        _sb.AppendLine("     FROM   HANDPAYS ");
        _sb.AppendLine("LEFT JOIN   TERMINALS ON HP_TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine("    WHERE   HP_TICKET_ID = @pTicketId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = Ticket.TicketID;
          _sql_cmd.Parameters.Add("@pNatCur", SqlDbType.NVarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              // Prepare the parameters for the handpay process
              HandpayParams = new Handpays.HandpayRegisterOrCancelParameters();

              HandpayParams.CancelHandpay = false;
              HandpayParams.IsTicket = true;

              HandpayParams.HandpayType = (HANDPAY_TYPE)_reader.GetInt32(_reader.GetOrdinal("HP_TYPE")); // Type
              HandpayParams.HandpayDate = _reader.GetDateTime(_reader.GetOrdinal("HP_DATETIME")); // Date
              HandpayParams.HandpayAmount = _reader.GetDecimal(_reader.GetOrdinal("HP_AMOUNT")); // Amount

              HandpayParams.TerminalId = _reader.GetInt32(_reader.GetOrdinal("HP_TERMINAL_ID")); // Terminal Id;
              HandpayParams.TerminalName = _reader.GetString(_reader.GetOrdinal("TE_NAME")); // Terminal Name
              HandpayParams.TerminalProvider = _reader.GetString(_reader.GetOrdinal("TE_PROVIDER_ID")); // Terminal Provider;
              HandpayParams.Level = _reader.GetInt32(_reader.GetOrdinal("HP_LEVEL")); // Level.
              HandpayParams.ProgressiveId = _reader.GetInt64(_reader.GetOrdinal("HP_PROGRESSIVE_ID")); // Progressive Id.
              HandpayParams.HandpayId = _reader.GetInt64(_reader.GetOrdinal("HP_ID")); // Handpay Id.

              HandpayParams.TaxBaseAmount = HandpayParams.HandpayAmount; // The Tax amount calculated should be 100% of the handpay amount.

              HandpayParams.HandpayAmt0 = _reader.GetDecimal(_reader.GetOrdinal("HP_AMT0")); // Amt 0
              HandpayParams.HandpayCur0 = _reader.GetString(_reader.GetOrdinal("HP_CUR0")); // Cur 0
              if (!_reader.IsDBNull(13))
              {
                HandpayParams.HandpayAmt1 = _reader.GetDecimal(_reader.GetOrdinal("HP_AMT1")); // Amt 1
              }
              if (!_reader.IsDBNull(14))
              {
                HandpayParams.HandpayCur1 = _reader.GetString(_reader.GetOrdinal("HP_CUR1")); // Cur 1
              }

              // Check if ticket amount and handpay amount matches
              if (HandpayParams.HandpayAmt0 != Ticket.Amt_0)
              {
                return TICKET_HANDPAY_STATUS.INCORRECT_AMOUNTS;
              }

              // Check if it has play_session and movement id
              if (_reader.GetBoolean(_reader.GetOrdinal("HAS_PLAY_SESSION_ID")) && _reader.GetBoolean(_reader.GetOrdinal("HAS_MOVEMENT_ID")))
              {
                return TICKET_HANDPAY_STATUS.REDEEMED;
              }

              HandpayParams.SiteJackpotAwardedOnTerminalId = Ticket.CreatedTerminalId;
            }
          }
        }

        // Query for PlaySession Amount of the handpay
        if (!GetHandpaySessionPlayedAmount(Ticket.CreatedAccountID
                                         , Ticket.CreatedPlaySessionId
                                         , out _played_amount
                                         , out _denomination_str
                                         , out _denomination_value
                                         , Trx))
        {
          return TICKET_HANDPAY_STATUS.NOT_FOUND;
        }

        HandpayParams.SessionPlayedAmount = _played_amount;
        HandpayParams.DenominationStr = _denomination_str;
        HandpayParams.DenominationValue = _denomination_value;
        HandpayParams.TicketStatus = Ticket.Status;

        return TICKET_HANDPAY_STATUS.VALID;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return TICKET_HANDPAY_STATUS.NOT_FOUND;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Set playsession id when handpay is registered
    // 
    //  PARAMS :
    //      - INPUT :
    //          - Ticket Id
    //          - PlaySession Id
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True : Ticket has been updated correctly
    //      - False : Otherwise
    // 
    public static Boolean DB_SetTicketPlaySessionId(Int64 TicketId, Int64 PlaySessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE TICKETS ");
      _sb.AppendLine("    SET  TI_CREATED_PLAY_SESSION_ID = @pPlaySessionId ");
      _sb.AppendLine("  WHERE  TI_TICKET_ID = @pTicketId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
        _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          return false;
        }
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : It changes the ticket id of a handpay. It's used for tickets offline and ticket reprint.
    // 
    //  PARAMS :
    //      - INPUT :
    //          - Old Ticket Id
    //          - New Ticket Id
    //          - Transaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True : Handpay has been updated correctly
    //      - False : Otherwise
    // 
    public static Boolean DB_ReplaceHandPayTicketId(Int64 OldTicketId, Int64 NewTicketId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE  HANDPAYS ");
      _sb.AppendLine("    SET  HP_TICKET_ID = @pNewTicketId ");
      _sb.AppendLine("  WHERE  HP_TICKET_ID = @pOldTicketId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOldTicketId", SqlDbType.BigInt).Value = OldTicketId;
        _cmd.Parameters.Add("@pNewTicketId", SqlDbType.BigInt).Value = NewTicketId;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          return false;
        }
      }

      return true;
    } // DB_SwapHandPayTicketId

    //------------------------------------------------------------------------------
    // PURPOSE : It changes the ticket play Session id. It's used for tickets offline.
    // 
    //  PARAMS :
    //      - INPUT :
    //          - Old Ticket Id
    //          - New Ticket Id
    //          - Transaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True : Ticket has been updated correctly
    //      - False : Otherwise
    // 
    public static Boolean DB_ReplaceTicketPlaySessionId(Int64 OldTicketId, Int64 NewTicketId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE  TICKETS ");
      _sb.AppendLine("    SET  TI_CREATED_PLAY_SESSION_ID = ");
      _sb.AppendLine("        ( SELECT TI_CREATED_PLAY_SESSION_ID ");
      _sb.AppendLine("          FROM TICKETS WHERE TI_TICKET_ID = @pOldTicketId )");
      _sb.AppendLine("  WHERE  TI_TICKET_ID = @pNewTicketId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOldTicketId", SqlDbType.BigInt).Value = OldTicketId;
        _cmd.Parameters.Add("@pNewTicketId", SqlDbType.BigInt).Value = NewTicketId;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          return false;
        }
      }

      return true;
    } // DB_SwapTicketPlaySessionId

    //------------------------------------------------------------------------------
    // PURPOSE: Asks if there is a handpay less than 5 minutes ago. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True:  There is a handpay less than 5 minutes ago. 
    //      - False: There isn't a handpay less than 5 minutes ago. 
    //
    public static Boolean ExistsSuspectedHandPay(Int32 TerminalId,
                                                  Decimal HandpayAmount,
                                                  HANDPAY_TYPE HandpayType,
                                                  Int64 TransactionId,
                                                  SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT         HP_TERMINAL_ID                                      ");
      _sb.AppendLine("  FROM         HANDPAYS                                            ");
      _sb.AppendLine(" WHERE         HP_AMOUNT            =   @pHandpayAmount            ");
      _sb.AppendLine("   AND         HP_TYPE              =   @pHandpayType              ");
      _sb.AppendLine("   AND         HP_TRANSACTION_ID    <>  @pTransactionID            ");
      _sb.AppendLine("   AND         HP_TERMINAL_ID       =   @pTerminalID               ");
      _sb.AppendLine("   AND         DATEDIFF (SECOND, HP_DATETIME, GETDATE ()) <= 300   ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pHandpayAmount", SqlDbType.BigInt).Value = HandpayAmount;
          _cmd.Parameters.Add("@pHandpayType", SqlDbType.Int).Value = HandpayType;
          _cmd.Parameters.Add("@pTransactionID", SqlDbType.BigInt).Value = TransactionId;
          _cmd.Parameters.Add("@pTerminalID", SqlDbType.BigInt).Value = TerminalId;

          _obj = _cmd.ExecuteScalar();
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get PlayedAmount for a Handpay play session. 
    // 
    //  PARAMS:
    //      - INPUT:
    //            - AccountId
    //            - PlaySessionId
    //            - SqlTransaction
    //
    //      - OUTPUT:
    //            - PlayedAmount
    //            - DenominationStr
    //            - DenominationValue
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean GetHandpaySessionPlayedAmount(Int64 AccountId,
                                                        Int64 PlaySessionId,
                                                        out Currency PlayedAmount,
                                                        out String DenominationStr,
                                                        out String DenominationValue,
                                                        SqlTransaction Trx)
    {
      String _sql_txt;
      Currency _cu_denomination;

      PlayedAmount = 0;
      DenominationStr = "";
      DenominationValue = "";

      try
      {
        _sql_txt = " SELECT   PS_PLAY_SESSION_ID " +
                   "        , PS_PLAYED_AMOUNT " +
                   "        , TE_DENOMINATION " +
                   "        , TE_MULTI_DENOMINATION " +
                   "   FROM   PLAY_SESSIONS, TERMINALS " +
                   "  WHERE   PS_PLAY_SESSION_ID  = @PlaySessionId " +
                   "    AND   TE_TERMINAL_ID = PS_TERMINAL_ID";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@PlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return false;
            }
            if (_sql_reader.IsDBNull(2))
            {
              DenominationStr = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_MULTIDENOMINATION");

              if (!_sql_reader.IsDBNull(3))
              {
                DenominationValue = _sql_reader.GetString(3);
              }
            }
            else
            {
              _cu_denomination = _sql_reader.GetDecimal(2);

              DenominationStr = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION");
              DenominationValue = _cu_denomination.ToString();
            }

            PlayedAmount = _sql_reader.GetDecimal(1);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetHandpaySessionPlayedAmount

    public static Boolean HasHandpayTicket(Int64 HandPayId)
    {
      StringBuilder _sb;
      Object _obj;

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT HP_TICKET_ID FROM HANDPAYS WHERE HP_ID = @pHandPayId");

      using (DB_TRX _trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pHandPayId", SqlDbType.BigInt).Value = HandPayId;

          try
          {
            _obj = _sql_cmd.ExecuteScalar();

            if (_obj == null || _obj == DBNull.Value)
            {
              return false;
            }

            return true;
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }
      }

      return false;
    }

    # endregion
  }
}


