//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MoneyCollectionMeter.cs
// 
//   DESCRIPTION: Functions related with money collection meters
//
//        AUTHOR: Ruben Rodriguez
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-DEC-2013 RRR    First release.
//------------ ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common.TITO
{
  public static class MoneyCollectionMeter
  {
    public static Boolean InitializeMoneyCollectionMeter(Int64 SessionId, Int64 MoneyCollectionId, Int32 TerminalId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF NOT EXISTS(SELECT TOP 1 * FROM   MONEY_COLLECTION_METERS        ");
        _sb.AppendLine("                             WHERE   MCM_SESSION_ID = @pSessionId ) ");
        _sb.AppendLine(" INSERT INTO  MONEY_COLLECTION_METERS  ");
        _sb.AppendLine("           ( MCM_MONEY_COLLECTION_ID   ");
        _sb.AppendLine("           , MCM_SESSION_ID            ");
        _sb.AppendLine("           , MCM_TERMINAL_ID           ");
        _sb.AppendLine("           , MCM_STARTED               ");
        _sb.AppendLine("           )                           ");
        _sb.AppendLine(" VALUES     ( @pCollectionId           ");
        _sb.AppendLine("            , @pSessionId              ");
        _sb.AppendLine("            , @pTerminalId             ");
        _sb.AppendLine("            , GETDATE()                ");
        _sb.AppendLine("            )                          ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
          _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_command.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;

          // Don't check number of rows affected. INSERT can be done or not.
          _sql_command.ExecuteNonQuery();
        }

        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean CloseMoneyCollectionMeter(Int64 SessionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE  MONEY_COLLECTION_METERS       ");
        _sb.AppendLine("    SET  MCM_FINISHED = GETDATE()      ");
        _sb.AppendLine("  WHERE  MCM_SESSION_ID = @pSessionId  ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;

          if (_sql_command.ExecuteNonQuery() != 1)
          {
            Log.Warning("CloseMoneyCollectionMeter. Money Collection Meter not updated. SessionId: " + SessionId.ToString());

            return true;
          }
        }

        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

  }
}
