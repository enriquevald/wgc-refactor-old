//--------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//--------------------------------------------------------------------------
// 
//   MODULE NAME : ValidationNumberEncryptDecrypt.cs
//
//   DESCRIPTION : Secure Enhanced Validation Algorithm
//
//        AUTHOR : Javier Fernández
//
// CREATION DATE : 08-OCT-2013
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ------------------------------------------------------
// 08-OCT-2013 JFC    First release.
// 08-OCT-2013 JFC    Decrypt implementation.
// 11-OCT-2013 JFC    Transform ValidationNumberManager to static class.
// 29-OCT-2013 DRV    Added Format an unformat validation numbers functions
// 08-NOV-2013 NMR    Replaced algorithm for ValidationNumber calcul
// 14-NOV-2013 JFC    Refactoring code to add new terminology ValidationNumberSequence.
// 28-NOV-2013 JFC    (*) Added decrypt standard number methods (GetTicketCreditAmount and GetTicketTime).
// 14-NOV-2013 JFC    Added encrypt standard number method (EncryptStandard) and (*) deleted decrypt standard number methods.
// 28-NOV-2016 PDM    PBI: 20404:TITO Ticket: Validation Number
// 17-MAY-2016 JCA    PBI 27514:TITO - Validation Number - SystemId - new GP to paremetrize
// 13-JUL-2018 AGS    Bug 33616:WIGOS-13496 Win Recycling Voucher Numbers
//--------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace WSI.Common.TITO
{
  public class SystemValidationNumber
  {
    Random m_random = new Random(Misc.GetTickCount());
    SHA1 m_sha1 = SHA1.Create();

    /// <summary>
    /// Calculate Validation Number for tickets
    /// </summary>
    /// <param name="IsCashier"></param>
    /// <param name="TerminalId"></param>
    /// <param name="SequenceId"></param>
    /// <returns></returns>
    public Int64 ValidationNumber(Boolean IsCashier, Int32 TerminalId, Int64 SequenceId)
    {
      return ValidationNumber1(IsCashier, SequenceId);
    }
    static public Int64 ValidationNumber1(Boolean IsCashier, Int64 SequenceId)
    {
      int _site_id;
      DateTime _now;
      string _prefix;
      int _system_id;
      string _dummy;

      _now = WGDB.Now;
      _site_id = GeneralParam.GetInt32("Site", "Identifier", 0);
      _prefix = GeneralParam.GetString("TITO", "Tickets.SystemIdFormat", "YY");


      _prefix = _prefix.ToUpper().Trim();
      if (Int32.TryParse(_prefix, out _system_id))
      {
        _system_id = _system_id % 100;
      }
      else
      {
        _system_id = (20 + _now.Year % 10) % 100;
        if (IsCashier)
        {
          _system_id += 10;
        }
      }

      return ValidationNumberPrimeBased(_site_id, _system_id, _now, SequenceId, out _dummy);
    }


    static private Int64 ValidationNumberPrimeBased(int SiteId, int SystemId, DateTime When, Int64 SequenceId, out String StrValidationNumber)
    {
      TimeSpan _elapsed;
      String _str_num;
      Int64 _val_num;

      int _yy;
      int _hhhh;
      int _ssss;
      int _xxxx;
      int _zzzz;


      Int64 _xx;

      _yy = SystemId;

      _elapsed = When - new DateTime(2018, 1, 1, 0, 0, 0);

      _xx = (Int64)_elapsed.TotalSeconds;
      _xx *= 987658241L;
      _xx %= 99979993L;

      _hhhh = (int)(1 + (_xx % 9999)); _xx /= 9999;
      _ssss = (int)(1 + (_xx % 9999)); _xx /= 9999;

      _xx = (Int64)SequenceId;
      _xx *= 987658253L;
      _xx += (SiteId * 31622467L);
      _xx %= 99979981L;

      _xxxx = (int)(1 + (_xx % 9999)); _xx /= 9999;
      _zzzz = (int)(1 + (_xx % 9999)); _xx /= 9999;


      StrValidationNumber = _yy.ToString("00") + "-" + _hhhh.ToString("0000") + "-" + _ssss.ToString("0000") + "-" + _xxxx.ToString("0000") + "-" + _zzzz.ToString("0000");
      _str_num = _yy.ToString("00") + _hhhh.ToString("0000") + _ssss.ToString("0000") + _xxxx.ToString("0000") + _zzzz.ToString("0000");
      _val_num = Int64.Parse(_str_num);

      return _val_num;
    }
  } // SystemValidationNumber


  public static class ValidationNumberManager
  {
    #region Constants

    // Validation Number will represented in XX-digit BCD
    static readonly Int64 VALID_NUMBER_LEN16 = 9999999999999999 + 1;
    static readonly Byte STANDARD_VALIDATION_NUMBER_LENGTH = 8;
    static readonly Byte AMOUNT_LENGTH = 6;

    #endregion

    # region Public methods

    //------------------------------------------------------------------------------
    // PURPOSE : The six binary bytes composed of the gaming machine validation ID
    //           and new validation sequence number are converted by the validation
    //           algorithm into a 16-digit BCD number that includes a check digit.
    //
    //  PARAMS :
    //      - INPUT : 
    //             - Machine ID in hexadecimal format (Int32).
    //             - Sequence number in hexadecimal format (Int32).
    //
    //      - OUTPUT : 
    //             - The BCD validation number sequence in decimal format (Int64).
    // RETURNS : 
    //             - True if all is correct.
    //
    //[System.Obsolete("Use SystemValidationNumber.ValidationNumber",true)]
    //This function is discontinued because the new method to create Validation numbers do it innecessary
    //public static Boolean Encrypt(Int32 MachineId, Int32 SeqNumber, out Int64 ValidationNumberSequence)
   
    //------------------------------------------------------------------------------
    // PURPOSE : Method for standard validation number calculation.
    //
    //  PARAMS :
    //      - INPUT : 
    //             - Credit amount of cashout ticket.
    //             - Time of cashout ticket.
    //
    //      - OUTPUT : 
    //             - Decimal standard validation number sequence (Int32).
    //
    // RETURNS : 
    //             - True if all is correct.
    //
    public static Boolean EncryptStandard(Currency Amount, DateTime TicketTime, out Int64 StandardValidationNumberSequence)
    {
      Boolean _result;
      Int32 _amount;
      String _amount_string;
      String _standard_val_number_BCD_format;

      Int32 _hour;
      Int32 _minute;
      Int32 _second;

      Int32 _byte0;
      Int32 _byte1;
      Int32 _byte2;
      Int32 _credit0;
      Int32 _credit1;
      Int32 _credit2;
      Int32 _remainder;

      _standard_val_number_BCD_format = null;
      _credit0 = 0;
      _credit1 = 0;
      _credit2 = 0;

      _result = false;
      StandardValidationNumberSequence = -1;
      _amount = 0;
      _amount_string = null;

      _result = ConvertCurrencyToInt(Amount, out _amount);

      if (_result)
      {
        _amount_string = _amount.ToString("D" + AMOUNT_LENGTH);
        _result = Int32.TryParse(_amount_string.Substring(4, 2), out _credit0);

        if (_result)
        {
          _result = Int32.TryParse(_amount_string.Substring(2, 2), out _credit1);

          if (_result)
          {
            _result = Int32.TryParse(_amount_string.Substring(0, 2), out _credit2);

            if (_result)
            {
              _second = TicketTime.Second;
              _minute = TicketTime.Minute;
              _hour = TicketTime.Hour;

              _byte2 = ValidationNumberManager.Add(_credit2, _hour, out _remainder);

              _credit1 += _remainder;
              _byte1 = ValidationNumberManager.Add(_credit1, _minute, out _remainder);

              _credit0 += _remainder;
              _byte0 = ValidationNumberManager.Add(_credit0, _second, out _remainder);

              _standard_val_number_BCD_format = _byte0.ToString("D2") +
                                                _byte2.ToString("D2") +
                                                _byte1.ToString("D2") +
                                                _byte0.ToString("D2");
            }

            try
            {
              // To take only eight less significative digits
              StandardValidationNumberSequence = Convert.ToInt64(_standard_val_number_BCD_format, 16) %
                                                 (Int64)Math.Pow(10, STANDARD_VALIDATION_NUMBER_LENGTH);

              _result = true;
            }
            catch (Exception)
            {
              _result = false;
            }
          }
        }
      }

      return _result;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns machine ID and sequence number from BCD validation number sequence.
    //
    //  PARAMS :
    //      - INPUT : 
    //             - The BCD validation number sequence in decimal format (Int64).
    //
    //      - OUTPUT : 
    //             - Machine ID in hexadecimal format (Int32).
    //             - Sequence number in hexadecimal format (Int32).
    //
    // RETURNS : 
    //             - True if all is correct.
    //
    public static Boolean Decrypt(Int64 ValidationNumberSequence, out Int32 MachineId, out Int32 SeqNumber)
    {
      Boolean _result_ok;
      String[] _v7_v15_values;
      String _v7;
      String _v15;

      MachineId = -1;
      SeqNumber = -1;

      _result_ok = false;

      try
      {
        if (ValidationNumberManager.CheckDecryptInputOk(ValidationNumberSequence))
        {
          _v7_v15_values = new String[] { "00", "01", "10", "11" };
          foreach (String _idx in _v7_v15_values)
          {
            _v7 = _idx.Substring(0, 1);
            _v15 = _idx.Substring(1, 1);

            ValidationNumberManager.GetMachineAndSeqNumbers(ValidationNumberSequence, _v7, _v15, out MachineId, out SeqNumber);
            _result_ok = ValidationNumberManager.CheckDecryptResultOk(ValidationNumberSequence, MachineId, SeqNumber);

            if (_result_ok)
            {
              break;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _result_ok = false;
      }

      return _result_ok;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Calculated validation number from system id and validation number sequence
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SystemId
    //          - ValidationNumber
    //
    //      - OUTPUT:
    //          - Composed ticket Validation number
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //
    public static Int64 GetCalculatedValidationNumber(Int32 SystemId, Int64 ValidationNumberSequence)
    {
      // returns system-id shifted to 17+18 position
      return (SystemId * VALID_NUMBER_LEN16) + ValidationNumberSequence;

    } // GetCalculatedValidationNumber

    //------------------------------------------------------------------------------
    // PURPOSE: Get validation number sequence from system id and Calculated validation number
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SystemId
    //          - ValidationNumber
    //
    //      - OUTPUT:
    //          - validation number sequence
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //
    public static Int64 GetValidationNumberSequence(Int32 SystemId, Int64 ValidationNumber)
    {
      // returns system-id shifted to 17+18 position
      return ValidationNumber - (SystemId * VALID_NUMBER_LEN16);

    } // GetValidationNumberSequence

    //------------------------------------------------------------------------------
    // PURPOSE: Get String with validation number from validation type and validation number sequence
    // 
    //  PARAMS:
    //      - INPUT:
    //          - validation number
    //          - validation type
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - formated validation number
    // 
    //   NOTES:
    //
    public static String FormatValidationNumber(Int64 ValidationNumberSequence, Int32 ValidationType, Boolean HideNumber)
    {
      String _hidden_8_mask;
      String _hidden_18_mask;
      String _result;
      StringBuilder _result_edit;
      Int32 _showed_numbers;
      Int32 _hidden_numbers;
      Int32 _aux;

      _hidden_8_mask = "0000-0000";
      _hidden_18_mask = "00-0000-0000-0000-0000";
      _aux = 0;
      _showed_numbers = GeneralParam.GetInt32("TITO", "Tickets.ShowLastNNumbers", 0);
      _result = _hidden_8_mask;

      //Select the correct mask for each validation type
      if ((TITO_VALIDATION_TYPE)ValidationType == TITO_VALIDATION_TYPE.STANDARD)
      {
        _hidden_numbers = 8 - _showed_numbers;
      }
      else
      {
        _result = _hidden_18_mask;
        _hidden_numbers = 18 - _showed_numbers;
      }

      //Format the number and hide digits if it's needed
      if (_showed_numbers == 0 || !HideNumber)
      {
        return ValidationNumberSequence.ToString(_result);
      }
      else
      {
        _result_edit = new StringBuilder(ValidationNumberSequence.ToString(_result));

        while (_hidden_numbers > 0 && _aux < _result_edit.Length)
        {
          if (_result_edit[_aux] != '-')
          {
            _result_edit[_aux] = '*';
            _hidden_numbers--;
          }
          _aux++;
        }
        return _result_edit.ToString();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: from a string with the formatted validation number, returns an integer with the number, and the kind of validation
    // 
    //  PARAMS:
    //      - INPUT:
    //          - validation number
    //          - 
    //
    //      - OUTPUT:
    //          - validation type
    //
    // RETURNS:
    //          - validation number sequence in integer format
    // 
    //   NOTES:
    //
    public static Int64 UnFormatValidationNumber(String ValidationNumber, out TITO_VALIDATION_TYPE ValidationType)
    {
      Int64 _number;

      _number = 0;

      ValidationNumber = ValidationNumber.Replace("-", "");
      ValidationType = TITO_VALIDATION_TYPE.STANDARD;

      if (Int64.TryParse(ValidationNumber, out _number))
      {
        if (_number.ToString().Length > 8)
        {
          ValidationNumber = ValidationNumber.PadLeft(18, '0');
          if (String.Compare(ValidationNumber.Substring(0, 2), "00") == 0)
          {
            ValidationType = TITO_VALIDATION_TYPE.SECURITY_ENHANCED;
          }
          else
          {
            ValidationType = TITO_VALIDATION_TYPE.SYSTEM;
          }
        }
      }

      return _number;
    }

    # endregion

    # region Private methods

    #region Steps of encrypt and decrypt methods

    // Place the gaming machine validation ID and the sequence number in an array of 6 bytes.
    private static Byte[] GetStep1(Byte[] MachineId, Byte[] SeqNumber)
    {
      return new Byte[] 
      { 
        SeqNumber[0],
        SeqNumber[1],
        SeqNumber[2],
        MachineId[0], 
        MachineId[1], 
        MachineId[2] 
      };
    }

    // Array A gets transformed into array B.
    private static Byte[] GetStep2(Byte[] Step1)
    {
      return new Byte[]
      { 
        Step1[0],
        Step1[1],
        (Byte)(Step1[2] ^ Step1[0]),
        (Byte)(Step1[3] ^ Step1[1]),
        (Byte)(Step1[4] ^ Step1[0]),
        (Byte)(Step1[5] ^ Step1[1])
      };
    }

    // Array B gets transformed into array C.
    private static Byte[] GetStep3(Byte[] Step2)
    {
      Byte[] _crc_45;
      Byte[] _crc_23;
      Byte[] _crc_01;

      _crc_45 = ValidationNumberManager.GetCRCAsByteArray(new Byte[] { Step2[4], Step2[5] });
      _crc_23 = ValidationNumberManager.GetCRCAsByteArray(new Byte[] { Step2[2], Step2[3] });
      _crc_01 = ValidationNumberManager.GetCRCAsByteArray(new Byte[] { Step2[0], Step2[1] });

      return new Byte[]
      { 
        _crc_01[0],
        _crc_01[1],
        _crc_23[0],
        _crc_23[1],
        _crc_45[0],
        _crc_45[1]
      };
    }

    // Array C gets transformed into an array of digits N.
    private static Int32[] GetStep4(Byte[] step3)
    {
      String _bin_to_BCD_543;
      String _bin_to_BCD_210;
      String _step4_pre;

      _bin_to_BCD_543 = ((UInt32)((((0x00 << 4) | step3[5]) << 16) | ((step3[4] << 8) | step3[3]))).ToString().PadLeft(8, '0');
      _bin_to_BCD_210 = ((UInt32)((((0x00 << 4) | step3[2]) << 16) | ((step3[1] << 8) | step3[0]))).ToString().PadLeft(8, '0');

      _step4_pre = _bin_to_BCD_543 + _bin_to_BCD_210;

      return ValidationNumberManager.ConvertToIntArray(_step4_pre);
    }

    // The array of digits N gets transformed into the array of digits V.
    // The finished packed BCD validation number will be ordered with V15 as the MSB and V0 as the LSB.
    private static Int64 GetStep5(Int32[] Step4)
    {
      String[] _step4;

      Step4[8] = Step4[8] | ((ValidationNumberManager.Sum(new List<Int32>(Step4).GetRange(8, 8)) % 5) << 1);
      Step4[0] = Step4[0] | ((ValidationNumberManager.Sum(new List<Int32>(Step4).GetRange(0, 8)) % 5) << 1);

      _step4 = new String[Step4.Length];

      for (Int32 _idx = 0; _idx < _step4.Length; _idx++)
      {
        _step4[_idx] = Step4[_idx].ToString();
      }

      return Int64.Parse(String.Join("", _step4), NumberStyles.Integer);
    }

    private static void GetMachineAndSeqNumbers(Int64 ValidationNumber,
                                                String V7,
                                                String V15,
                                                out Int32 MachineId,
                                                out Int32 SeqNumber)
    {
      String _step_3;
      String _step_2;

      MachineId = -1;
      SeqNumber = -1;
      _step_3 = ValidationNumberManager.GetStep3FromValNumber(ValidationNumber, V7, V15);

      if (_step_3.Length == 12)
      {
        _step_2 = ValidationNumberManager.GetStep2FromStep3(_step_3);
        ValidationNumberManager.GetMachineIdAndSeqNumberFromStep2(_step_2, out MachineId, out SeqNumber);
      }
    }

    private static String GetStep3FromValNumber(Int64 ValidationNumber, String V7, String V15)
    {
      String _hex_number_1;
      String _hex_number_2;
      Int64 _number_1;
      Int64 _number_2;

      String _val_number_string;
      String _val_number_1;
      String _val_number_2;

      _hex_number_1 = null;
      _hex_number_2 = null;
      _number_1 = 0;
      _number_2 = 0;

      _val_number_string = ValidationNumber.ToString("D16");
      _val_number_1 = V15 + _val_number_string.Substring(1, 7);
      _val_number_2 = V7 + _val_number_string.Substring(9, 7);

      if (Int64.TryParse(_val_number_1, out _number_1))
      {
        _hex_number_1 = _number_1.ToString("X6");
      }

      if (Int64.TryParse(_val_number_2, out _number_2))
      {
        _hex_number_2 = _number_2.ToString("X6");
      }

      return _hex_number_1 + _hex_number_2;
    }

    private static String GetStep2FromStep3(String Step3)
    {
      String _result;
      Byte[] _bytes;
      Byte[] _bytes2;
      String _b1;
      String _b2;

      _result = null;
      _bytes = new Byte[Step3.Length / 2];

      for (Int32 i = 0; i < Step3.Length; i += 2)
      {
        Byte b = Byte.Parse(Step3.Substring(i, 2), NumberStyles.HexNumber);
        _bytes[i / 2] = b;
      }

      _bytes2 = null;

      for (Int32 i = 0; i < _bytes.Length; i += 2)
      {
        _bytes2 = ValidationNumberManager.GetByteArrayFromCRC(new Byte[] { _bytes[i + 1], _bytes[i] });

        if (_bytes2 != null)
        {
          _b1 = _bytes2[1].ToString("X2");
          _b2 = _bytes2[0].ToString("X2");
          _result += _b1 + _b2;
        }
        else
        {
          _result = null;
          break;
        }
      }

      return _result;
    }

    private static void GetMachineIdAndSeqNumberFromStep2(String Step2,
                                                          out Int32 MachineId,
                                                          out Int32 SeqNumber)
    {
      Byte _B2;
      Byte _B3;
      Byte _B4;
      Byte _B5;

      Byte _A0;
      Byte _A1;
      Byte _A2;
      Byte _A3;
      Byte _A4;
      Byte _A5;

      String _machine_id_string;
      String _seq_number_string;

      MachineId = -1;
      SeqNumber = -1;

      if (Step2 == null)
      {
        return;
      }

      _B2 = Byte.Parse(Step2.Substring(Step2.Length - 6, 2), NumberStyles.HexNumber);
      _B3 = Byte.Parse(Step2.Substring(Step2.Length - 8, 2), NumberStyles.HexNumber);
      _B4 = Byte.Parse(Step2.Substring(Step2.Length - 10, 2), NumberStyles.HexNumber);
      _B5 = Byte.Parse(Step2.Substring(Step2.Length - 12, 2), NumberStyles.HexNumber);

      _A0 = Byte.Parse(Step2.Substring(Step2.Length - 2, 2), NumberStyles.HexNumber);
      _A1 = Byte.Parse(Step2.Substring(Step2.Length - 4, 2), NumberStyles.HexNumber);
      _A2 = (Byte)(_B2 ^ _A0);
      _A3 = (Byte)(_B3 ^ _A1);
      _A4 = (Byte)(_B4 ^ _A0);
      _A5 = (Byte)(_B5 ^ _A1);

      _machine_id_string = _A5.ToString("X2") + _A4.ToString("X2") + _A3.ToString("X2");
      _seq_number_string = _A2.ToString("X2") + _A1.ToString("X2") + _A0.ToString("X2");

      MachineId = Int32.Parse(_machine_id_string, NumberStyles.HexNumber);
      SeqNumber = Int32.Parse(_seq_number_string, NumberStyles.HexNumber);
    }

    # endregion

    # region Utils

    private static Boolean CheckEncryptInputOk(Int32 MachineId, Int32 SeqNumber)
    {
      return MachineId >= 0 && MachineId <= 0xFFFFFF && SeqNumber >= 0 && SeqNumber <= 0xFFFFFF;
    }

    private static Boolean CheckDecryptInputOk(Int64 ValidationNumber)
    {
      return ValidationNumber >= 0 && ValidationNumber <= 9999999999999999;
    }

    private static Boolean CheckDecryptResultOk(Int64 ValidationNumber, Int32 MachineId, Int32 SeqNumber)
    {
      Int64 _calculated_val_number;

      _calculated_val_number = -1;

      SystemValidationNumber _tito = new SystemValidationNumber();
      _calculated_val_number = _tito.ValidationNumber(true, MachineId, (Int32)SeqNumber);

      return ValidationNumber == _calculated_val_number;
    }

    private static Int32[] ConvertToIntArray(String Number)
    {
      List<Int32> _result;

      _result = new List<Int32>();

      foreach (Char _c in Number.ToCharArray())
      {
        _result.Add(Int32.Parse(_c.ToString()));
      }

      return _result.ToArray();
    }

    private static Int32 Sum(List<Int32> Digits)
    {
      Int32 _result;

      _result = 0;

      foreach (Int32 _idx in Digits)
      {
        _result += _idx;
      }

      return _result;
    }

    private static Byte[] GetCRCAsByteArray(Byte[] Bytes)
    {
      Int64 _crc;
      Int64 _q;
      Byte _c;

      _crc = 0;

      for (Int32 _idx = 0; _idx < Bytes.Length; _idx++)
      {
        _c = Bytes[_idx];
        _q = (_crc ^ _c) & 0x0f;
        _crc = (_crc >> 4) ^ (_q * 0x1081);
        _q = (_crc ^ (_c >> 4)) & 0xf;
        _crc = (_crc >> 4) ^ (_q * 0x1081);
      }

      return new Byte[] { (Byte)(_crc & 0xff), (Byte)(_crc >> 8) };
    }

    private static Byte[] GetByteArrayFromCRC(Byte[] Bytes)
    {
      // TODO JFC Eficiencia mejorable
      Byte[] _result;
      Byte[] _bs;
      Boolean _is_founded;
      Byte[] _byte_array;
      Byte[] _byte_2;

      _result = null;
      _bs = null;
      _is_founded = false;

      for (Int32 _idx = 0x0; _idx <= 0xffff; _idx++)
      {
        _byte_array = BitConverter.GetBytes(_idx);
        _byte_2 = new Byte[] { _byte_array[1], _byte_array[0] };
        _bs = ValidationNumberManager.GetCRCAsByteArray(_byte_2);
        _is_founded = _bs[0] == Bytes[0] && _bs[1] == Bytes[1];

        if (_is_founded)
        {
          _result = _byte_2;
          break;
        }
      }

      return _result;
    }

    private static Int32 Add(Int32 Amount1, Int32 Amount2, out Int32 Remainder)
    {
      Int32 _max;
      Int32 _total;

      _max = 100;
      _total = Amount1 + Amount2;

      Remainder = _total / _max;

      return _total % _max;
    }

    private static Boolean ConvertCurrencyToInt(Currency Amount, out Int32 AmountInt)
    {
      Boolean _result;
      Decimal _d = 0;

      _result = false;

      try
      {
        _result = Decimal.TryParse(Amount.ToString(false), out _d);
      }
      catch
      {
        _result = false;
      }

      AmountInt = (Int32)(_d * 100);

      return _result;
    }

    # endregion

    # endregion
  }
}