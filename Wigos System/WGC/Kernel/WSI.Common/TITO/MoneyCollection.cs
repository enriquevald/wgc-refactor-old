//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MoneyCollection.cs
// 
//   DESCRIPTION: MoneyCollection class
// 
//        AUTHOR: Javier Fernández
// 
// CREATION DATE: 22-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-OCT-2013 JFC    First version. 
// 27-NOV-2013 ICS    Added new functions for cashier money collections 
// 29-NOV-2013 ICS    Added functions for get/create money collections 
// 02-ENE-2014 JRM    Fixed bug WIGOSTITO-940 Log error when changing the stacker of a newly created Terminal
// 31-MAR-2014 DRV    Code refractor
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common.TITO
{
  public class MoneyCollection
  {
    # region Private fields

    private Int64 m_CollectionId;
    private DateTime m_Datetime;
    private Int32 m_TerminalId;
    private Int32 m_UserId;
    private Boolean m_UndoCollect;
    private Boolean m_MoneyLost;
    private Int64 m_CashierSessionId;
    private String m_Notes;
    private Int64 m_StackerId;
    private Int32 m_InsertedUserId;
    private DateTime m_ExtractionDatetime;
    private DateTime m_CollectionDatetime;
    private TITO_MONEY_COLLECTION_STATUS m_Status;
    private Int32 m_CashierId;
    private Decimal m_ExpectedTicketAmount;
    private Int32 m_ExpectedTicketCount;
    private Decimal m_ExpectedRETicketAmount;
    private Int32 m_ExpectedRETicketCount;
    private Decimal m_ExpectedPromoRETicketAmount;
    private Int32 m_ExpectedPromoRETicketCount;
    private Decimal m_ExpectedPromoNRTicketAmount;
    private Int32 m_ExpectedPromoNRTicketCount;

    # endregion

    #region Properties

    public Int64 CollectionId
    {
      get { return this.m_CollectionId; }
      set { this.m_CollectionId = value; }
    }

    public DateTime Datetime
    {
      get { return this.m_Datetime; }
      set { this.m_Datetime = value; }
    }

    public Int32 TerminalId
    {
      get { return this.m_TerminalId; }
      set { this.m_TerminalId = value; }
    }

    public Int32 UserId
    {
      get { return this.m_UserId; }
      set { this.m_UserId = value; }
    }

    public Boolean UndoCollect
    {
      get { return this.m_UndoCollect; }
      set { this.m_UndoCollect = value; }
    }

    public Boolean MoneyLost
    {
      get { return this.m_MoneyLost; }
      set { this.m_MoneyLost = value; }
    }

    public Int64 CashierSessionId
    {
      get { return this.m_CashierSessionId; }
      set { this.m_CashierSessionId = value; }
    }

    public String Notes
    {
      get { return this.m_Notes; }
      set { this.m_Notes = value; }
    }

    public Int64 StackerId
    {
      get { return this.m_StackerId; }
      set { this.m_StackerId = value; }
    }

    public Int32 InsertedUserId
    {
      get { return this.m_InsertedUserId; }
      set { this.m_InsertedUserId = value; }
    }

    public DateTime ExtractionDatetime
    {
      get { return this.m_ExtractionDatetime; }
      set { this.m_ExtractionDatetime = value; }
    }

    public DateTime CollectionDatetime
    {
      get { return this.m_CollectionDatetime; }
      set { this.m_CollectionDatetime = value; }
    }

    public TITO_MONEY_COLLECTION_STATUS Status
    {
      get { return this.m_Status; }
      set { this.m_Status = value; }
    }

    public Int32 CashierId
    {
      get { return this.m_CashierId; }
      set { this.m_CashierId = value; }
    }

    public Decimal ExpectedTicketAmount
    {
      get { return m_ExpectedTicketAmount; }
      set { m_ExpectedTicketAmount = value; }
    }

    public Int32 ExpectedTicketCount
    {
      get { return m_ExpectedTicketCount; }
      set { m_ExpectedTicketCount = value; }
    }

    public Decimal ExpectedRETicketAmount
    {
      get { return m_ExpectedRETicketAmount; }
      set { m_ExpectedRETicketAmount = value; }
    }

    public Int32 ExpectedRETicketCount
    {
      get { return m_ExpectedRETicketCount; }
      set { m_ExpectedRETicketCount = value; }
    }

    public Decimal ExpectedPromoRETicketAmount
    {
      get { return m_ExpectedPromoRETicketAmount; }
      set { m_ExpectedPromoRETicketAmount = value; }
    }

    public Int32 ExpectedPromoRETicketCount
    {
      get { return m_ExpectedPromoRETicketCount; }
      set { m_ExpectedPromoRETicketCount = value; }
    }

    public Decimal ExpectedPromoNRTicketAmount
    {
      get { return m_ExpectedPromoNRTicketAmount; }
      set { m_ExpectedPromoNRTicketAmount = value; }
    }


    public Int32 ExpectedPromoNRTicketCount
    {
      get { return m_ExpectedPromoNRTicketCount; }
      set { m_ExpectedPromoNRTicketCount = value; }
    }

    # endregion

    # region Constructors

    private MoneyCollection() { }

    # endregion

    # region Create methods

    public static Boolean DB_GetMoneyCollection(Int32 TerminalId,
                                                Int64 StackerId,
                                                out Int64 MoneyCollectionId,
                                                SqlTransaction Trx)
    {
      return DB_GetMoneyCollection(TerminalId, 0, StackerId, out MoneyCollectionId, Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the open money_collection_id for the specified terminal
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Terminal id
    //          - Stacker id
    //          - Transaction
    //
    //      - OUTPUT:
    //          - MoneyCollectionId
    //
    // RETURNS:
    //      - true:  Successful transaction
    //      - false: Not transaction completed
    // 
    //   NOTES:
    //
    public static Boolean DB_GetMoneyCollection(Int32 TerminalId,
                                                Int64 TransactionId,
                                                Int64 StackerId,
                                                out Int64 MoneyCollectionId,
                                                SqlTransaction Trx)
    {
      Object _obj;
      StringBuilder _sb;

      MoneyCollectionId = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   MC_COLLECTION_ID ");
        _sb.AppendLine("  FROM   MONEY_COLLECTIONS ");
        _sb.AppendLine(" WHERE   MC_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("   AND   MC_STATUS = @pStatus ");
        if (StackerId > 0)
        {
          _sb.AppendLine("   AND   MC_STACKER_ID = @pStackerId ");
        }
        if (TransactionId > 0)
        {
          _sb.AppendLine("   AND   MC_WCP_TRANSACTION_ID = @pTransactionId ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.OPEN;
          _cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = StackerId;
          _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;

          _obj = _cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            MoneyCollectionId = (Int64)_obj;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Insert new cashier collection in money_collections table
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Cashier Terminal Id
    //          - Cashier Session Id
    //          - Transaction
    //
    //      - OUTPUT:
    //          - Money Collection Id
    //
    // RETURNS:
    //      - true:  Successful transaction
    //      - false: Something went wrong
    //
    public static Boolean DB_CreateCashierMoneyCollection(Int32 CashierTerminalId,
                                                          Int64 CashierSessionId,
                                                          out Int64 MoneyCollectionId,
                                                          SqlTransaction Trx)
    {

      return DB_CreateMoneyCollection(CashierTerminalId, 0, 0, CashierSessionId, out MoneyCollectionId, TITO_TERMINAL_TYPE.CASHIER, Trx);

    } // DB_CreateCashierMoneyCollection

    //------------------------------------------------------------------------------
    // PURPOSE: Insert new row in money_collections table.  
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Terminal id
    //          - Stacker id
    //          - Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true:  Successful transaction
    //      - false: Not transaction completed
    // 
    //   NOTES:
    //
    public static Boolean DB_CreateTerminalMoneyCollection(Int32 TerminalId,
                                                           Int64 TransactionId,
                                                           Int64 StackerId,
                                                           Int64 CashierSessionId,
                                                           out Int64 MoneyCollectionId,
                                                           SqlTransaction SqlTrx)
    {
      return DB_CreateMoneyCollection(TerminalId, TransactionId, StackerId, CashierSessionId, out MoneyCollectionId, TITO_TERMINAL_TYPE.TERMINAL, SqlTrx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert new row in money_collections table.  
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Terminal id
    //          - Stacker id
    //          - Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true:  Successful transaction
    //      - false: Not transaction completed
    // 
    //   NOTES:
    //
    private static Boolean DB_CreateMoneyCollection(Int32 TerminalId,
                                                    Int64 TransactionId,
                                                    Int64 StackerId,
                                                    Int64 CashierSessionId,
                                                    out Int64 MoneyCollectionId,
                                                    TITO_TERMINAL_TYPE TerminalType,
                                                    SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _param;

      MoneyCollectionId = 0;

      try
      {
        if (TerminalId <= 0 || CashierSessionId <= 0)
        {
          return false;
        }


        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO MONEY_COLLECTIONS          ");
        if (TerminalType == TITO_TERMINAL_TYPE.TERMINAL)
        {
          _sb.AppendLine("          ( MC_TERMINAL_ID              ");
        }
        else
        {
          _sb.AppendLine("          ( MC_CASHIER_ID               ");
        }
        _sb.AppendLine("          , MC_WCP_TRANSACTION_ID       ");
        _sb.AppendLine("          , MC_STACKER_ID               ");
        _sb.AppendLine("          , MC_CASHIER_SESSION_ID       ");
        _sb.AppendLine("          , MC_DATETIME                 ");
        _sb.AppendLine("          , MC_STATUS                   ");
        _sb.AppendLine("          )                             ");
        _sb.AppendLine(" VALUES   ( @pTerminalId                ");
        _sb.AppendLine("          , @pTransactionId             ");
        _sb.AppendLine("          , @pStackerId                 ");
        _sb.AppendLine("          , @pCashierSessionId          ");
        _sb.AppendLine("          , @pDatetime                  ");
        _sb.AppendLine("          , @pStatus                    ");
        _sb.AppendLine("          )                             ");
        _sb.AppendLine(" SET @pCollectionId = SCOPE_IDENTITY()  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.OPEN;
          if (CashierSessionId > 0)
          {
            _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          }
          else
          {
            _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          if (StackerId > 0)
          {
            _cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = StackerId;
          }
          else
          {
            _cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          if (TransactionId > 0)
          {
            _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
          }
          else
          {
            _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = DBNull.Value;
          }

          _param = _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        MoneyCollectionId = (Int64)_param.Value;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    # endregion

    # region Read methods


    //------------------------------------------------------------------------------
    // PURPOSE: Populate MoneyCollection object with the current money collection data
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Terminal id
    //          - Transaction
    //
    //      - OUTPUT:
    //          - 
    //
    // RETURNS:
    //      - Populated MoneyCollection object.
    // 
    //   NOTES:
    //
    public static MoneyCollection DB_ReadCurrentMoneyCollection(Int32 TerminalId,
                                          SqlTransaction Trx)
    {
      MoneyCollection _result = new MoneyCollection();
      _result.TerminalId = TerminalId;
      if (!_result.DB_ReadMoneyCollection(Trx))
      {
        return null;
      }
      else
      {
        return _result;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Populate MoneyCollection object.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - MoneyCollection id
    //          - Transaction
    //
    //      - OUTPUT:
    //          - 
    //
    // RETURNS:
    //      - Populated MoneyCollection object.
    // 
    //   NOTES:
    //
    public static MoneyCollection DB_Read(Int64 MoneyCollectionId,
                                          SqlTransaction Trx)
    {
      MoneyCollection _result = new MoneyCollection();
      _result.CollectionId = MoneyCollectionId;
      if (!_result.DB_ReadMoneyCollection(Trx))
      {
        return null;
      }
      else
      {
        return _result;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Populate MoneyCollection object.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Transaction
    //
    //      - OUTPUT:
    //          - 
    //
    // RETURNS:
    //      - Boolean: True -> Everything OK
    //                False -> Something went wrong
    //   NOTES:
    //
    private Boolean DB_ReadMoneyCollection(SqlTransaction Trx)
    {
      SqlCommand _sql_cmd;
      SqlDataReader _reader;
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   MC_COLLECTION_ID                  ");
      _sb.AppendLine("       , MC_DATETIME                       ");
      _sb.AppendLine("       , MC_TERMINAL_ID                    ");
      _sb.AppendLine("       , MC_USER_ID                        ");
      _sb.AppendLine("       , MC_UNDO_COLLECT                   ");
      _sb.AppendLine("       , MC_MONEY_LOST                     ");
      _sb.AppendLine("       , MC_CASHIER_SESSION_ID             ");
      _sb.AppendLine("       , MC_NOTES                          ");
      _sb.AppendLine("       , MC_STACKER_ID                     ");
      _sb.AppendLine("       , MC_INSERTED_USER_ID               ");
      _sb.AppendLine("       , MC_COLLECTION_DATETIME            ");
      _sb.AppendLine("       , MC_EXTRACTION_DATETIME            ");
      _sb.AppendLine("       , MC_STATUS                         ");
      _sb.AppendLine("  FROM   MONEY_COLLECTIONS                 ");
      if (this.CollectionId != 0)
      {
        _sb.AppendLine(" WHERE   MC_COLLECTION_ID = @pCollectionId ");
      }
      else
      {
        _sb.AppendLine(" WHERE   MC_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("   AND   MC_STATUS = @pStatusOpen ");
      }

      _sql_cmd = new SqlCommand(_sb.ToString());
      _sql_cmd.Connection = Trx.Connection;
      _sql_cmd.Transaction = Trx;

      _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = this.CollectionId;
      _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = this.TerminalId;
      _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = (Int32)TITO_MONEY_COLLECTION_STATUS.OPEN;

      _reader = null;

      try
      {
        _reader = _sql_cmd.ExecuteReader();

        if (_reader.Read())
        {
          CollectionId = _reader.IsDBNull(0) ? 0 : Convert.ToInt64(_reader[0]);
          Datetime = (DateTime)_reader[1];
          TerminalId = _reader.IsDBNull(2) ? 0 : Convert.ToInt32(_reader[2]);
          UserId = _reader.IsDBNull(3) ? 0 : Convert.ToInt32(_reader[3]);
          UndoCollect = (Boolean)_reader[4];
          MoneyLost = (Boolean)_reader[5];
          CashierSessionId = _reader.IsDBNull(6) ? 0 : Convert.ToInt64(_reader[6]);
          Notes = _reader[7] as String;
          StackerId = _reader.IsDBNull(8) ? 0 : Convert.ToInt64(_reader[8]);
          InsertedUserId = _reader.IsDBNull(9) ? 0 : Convert.ToInt32(_reader[9]);

          if (!_reader.IsDBNull(10))
          {
            ExtractionDatetime = (DateTime)_reader[10];
          }
          if (!_reader.IsDBNull(11))
          {
            m_CollectionDatetime = (DateTime)_reader[11];
          }

          if (!_reader.IsDBNull(12))
          {
            Status = (TITO_MONEY_COLLECTION_STATUS)_reader[12];
          }
        }
        else
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
          _reader.Dispose();
          _reader = null;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get id or create a collection of a cashier session.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Cashier session id
    //          - Transaction
    //
    //      - OUTPUT:
    //          - Money collection id
    //
    // RETURNS:
    //      - True: If collection could be found
    //      - False: Otherwise
    //
    public static Boolean GetCashierMoneyCollectionId(Int64 CashierSessionId, out Int64 MoneyCollectionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;
      Int32 _cashier_terminal_id;

      if (!GetMoneyCollectionId(CashierSessionId, Trx, out MoneyCollectionId))
      {

        return false;
      }

      // Not found, must create new one
      if (MoneyCollectionId == 0)
      {
        // Get Cashier Terminal Id
        _sb = new StringBuilder();
        _sb.Length = 0;
        _sb.AppendLine("SELECT   CS_CASHIER_ID               ");
        _sb.AppendLine("  FROM   CASHIER_SESSIONS            ");
        _sb.AppendLine(" WHERE   CS_SESSION_ID = @pSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _obj = _cmd.ExecuteScalar();
        }

        // Cashier Terminal not found
        if (_obj == null || _obj == DBNull.Value)
        {
          return false;
        }
        _cashier_terminal_id = (Int32)_obj;

        // Create new cashier money collection
        return DB_CreateCashierMoneyCollection(_cashier_terminal_id, CashierSessionId, out MoneyCollectionId, Trx);
      }

      return true;
    }

    public static Boolean GetOrCreateTerminalMoneyCollectionId(Int64 CashierSessionId, Int32 TerminalId, out Int64 MoneyCollectionId, SqlTransaction Trx)
    {
      if (!GetMoneyCollectionId(CashierSessionId, Trx, out MoneyCollectionId))
      {

        return false;
      }
      // Not found, must create new one
      if (MoneyCollectionId == 0)
      {
        // Create new terminal money collection
        return DB_CreateTerminalMoneyCollection(TerminalId, 0, 0, CashierSessionId, out MoneyCollectionId, Trx);
      }

      return true;
    } //GetCashierMoneyCollectionId

    private static Boolean GetMoneyCollectionId(Int64 CashierSessionId, SqlTransaction Trx, out Int64 MoneyCollectionId)
    {
      StringBuilder _sb;
      Object _obj;

      MoneyCollectionId = 0;

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   MC_COLLECTION_ID                       ");
      _sb.AppendLine("  FROM   MONEY_COLLECTIONS                      ");
      _sb.AppendLine(" WHERE   MC_CASHIER_SESSION_ID = @pSessionId    ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;

          _obj = _cmd.ExecuteScalar();
        }
        if (_obj != null && _obj != DBNull.Value)
        {
          MoneyCollectionId = (Int64)_obj;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } //GetCashierMoneyCollectionId

    # endregion Select methods

    # region Update methods

    //------------------------------------------------------------------------------
    // PURPOSE: Remove stacker and update money collection
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Stacker object.
    //          - Transaction
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: stacker state was changed successfully
    //      - false: stacker state could not be changed
    // 
    //   NOTES:
    //
    public Boolean DB_ChangeStatusToPendingClosing(SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        this.ExtractionDatetime = WGDB.Now;
        this.Status = TITO_MONEY_COLLECTION_STATUS.PENDING;
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   MONEY_COLLECTIONS                             ");
        _sb.AppendLine("    SET   MC_EXTRACTION_DATETIME = @pExtractionDatetime ");
        _sb.AppendLine("        , MC_STATUS =              @pStatus             ");
        _sb.AppendLine("        , MC_LAST_UPDATED =        GETDATE()            ");
        _sb.AppendLine("  WHERE   MC_COLLECTION_ID =       @pCollectionId       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pExtractionDatetime", SqlDbType.DateTime).Value = this.ExtractionDatetime;
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = this.Status;
          _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = this.CollectionId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("DB_ChangeStatusToPendingClosing. Money Collection status not updated. CollectionId: " + CollectionId.ToString());

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Gets the number of ticket for one Collection
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Trnsaction
    //          - CollectionId
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - number of tickets
    // 
    //   NOTES:
    //
    public static int DB_CountPendingTickets(SqlTransaction Trx, Int64 CollectionId)
    {
      int _result;
      StringBuilder _sb;

      _result = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   COUNT(*) AS TOTAL_TICKETS                     ");
        _sb.AppendLine("   FROM   TICKETS                                       ");
        _sb.AppendLine("  WHERE   TI_MONEY_COLLECTION_ID = @pCollectionId       ");
        _sb.AppendLine("    AND   TI_COLLECTED_MONEY_COLLECTION   IS NULL       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = CollectionId;

          _result = (int)_sql_cmd.ExecuteScalar();

          return _result;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return _result;
    }


    public Boolean DB_UpdateExpectedValues(SqlTransaction SqlTrx)
    {
      Int32 _updated_rows;
      StringBuilder _sb;

      _updated_rows = 0;

      _sb = new StringBuilder();

      _sb.AppendLine("   UPDATE   MONEY_COLLECTIONS                                   ");
      _sb.AppendLine("   SET  MC_EXPECTED_TICKET_AMOUNT          = ISNULL(MC_EXPECTED_TICKET_AMOUNT         , 0) + @pMcExpectedTicketAmount         ");
      _sb.AppendLine(" ,      MC_EXPECTED_TICKET_COUNT           = ISNULL(MC_EXPECTED_TICKET_COUNT          , 0) + @pMcExpectedTicket_count         ");
      _sb.AppendLine(" ,      MC_EXPECTED_RE_TICKET_AMOUNT       = ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT      , 0) + @pMcExpectedReTicketAmount       ");
      _sb.AppendLine(" ,      MC_EXPECTED_RE_TICKET_COUNT        = ISNULL(MC_EXPECTED_RE_TICKET_COUNT       , 0) + @pMcExpectedReTicketCount        ");
      _sb.AppendLine(" ,      MC_EXPECTED_PROMO_RE_TICKET_AMOUNT = ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0) + @pMcExpectedPromoReTicketAmount  ");
      _sb.AppendLine(" ,      MC_EXPECTED_PROMO_RE_TICKET_COUNT  = ISNULL(MC_EXPECTED_PROMO_RE_TICKET_COUNT , 0) + @pMcExpectedPromoReTicketCount   ");
      _sb.AppendLine(" ,      MC_EXPECTED_PROMO_NR_TICKET_AMOUNT = ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0) + @pMcExpectedPromoNrTicketAmount  ");
      _sb.AppendLine(" ,      MC_EXPECTED_PROMO_NR_TICKET_COUNT  = ISNULL(MC_EXPECTED_PROMO_NR_TICKET_COUNT , 0) + @pMcExpectedPromoNrTicketCount   ");
      _sb.AppendLine(" ,      MC_LAST_UPDATED                    = GETDATE()                                                                        ");
      _sb.AppendLine("   WHERE   MC_COLLECTION_ID = @pCollectionId                        ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pMcExpectedTicketAmount", SqlDbType.Money).Value = m_ExpectedTicketAmount;
          _cmd.Parameters.Add("@pMcExpectedTicket_count", SqlDbType.Int).Value = m_ExpectedTicketCount;
          _cmd.Parameters.Add("@pMcExpectedReTicketAmount", SqlDbType.Money).Value = m_ExpectedRETicketAmount;
          _cmd.Parameters.Add("@pMcExpectedReTicketCount", SqlDbType.Int).Value = m_ExpectedRETicketCount;
          _cmd.Parameters.Add("@pMcExpectedPromoReTicketAmount", SqlDbType.Money).Value = m_ExpectedPromoRETicketAmount;
          _cmd.Parameters.Add("@pMcExpectedPromoReTicketCount", SqlDbType.Int).Value = m_ExpectedPromoRETicketCount;
          _cmd.Parameters.Add("@pMcExpectedPromoNrTicketAmount", SqlDbType.Money).Value = m_ExpectedPromoNRTicketAmount;
          _cmd.Parameters.Add("@pMcExpectedPromoNrTicketCount", SqlDbType.Int).Value = m_ExpectedPromoNRTicketCount;

          _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = m_CollectionId;

          _updated_rows = _cmd.ExecuteNonQuery();
    
          if (_updated_rows < 1)
          {
          
            return false;
          }

          return true;
        
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    # endregion

    # region Overridden methods

    //public override string ToString()
    //{
    //  StringBuilder _result;
    //  String _separator;

    //  _separator = ", ";

    //  _result = new StringBuilder();

    //  _result.Append(@"{Id: ");
    //  _result.Append(this.CollectionId);
    //  _result.Append(_separator);

    //  _result.Append("terminal: ");
    //  _result.Append(this.TerminalId);
    //  _result.Append(_separator);

    //  _result.Append("stacker: ");
    //  _result.Append(this.StackerId);
    //  _result.Append(_separator);

    //  _result.Append("status: ");
    //  _result.Append(this.Status);
    //  _result.Append(@"}");

    //  return _result.ToString();
    //}

    # endregion
  }
}
