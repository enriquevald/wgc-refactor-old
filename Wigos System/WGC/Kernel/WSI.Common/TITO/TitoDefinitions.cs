//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TitoDefinitions.cs
// 
//   DESCRIPTION: Common definitions to use in TITOMode
//
//        AUTHOR: David Rigal
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-OCT-2013 DRV    First release.
// 08-FEB-2016 RGR    Product Backlog Item 6462: Garcia River-08: Cash Promotions NR: Print Ticket TITO or assign account
// 11-APR-2016 ADI    Product Backlog Item 11471:CountR - Cash It Out: GUI - L�gica de Pago de Ticket
// 27-10-2016  FJC    PBI 19605:TITO: SAS_HOST - informar motivo de rechazo
// 22-MAR-2017 RAB    PBI 25973: MES10 Ticket validation - Dealer copy link to TITO ticket
// 26-APR-2017 ETP    PBI 26779: Added Junket error message.
//------------ ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public enum TITO_VALIDATION_TYPE
  {
    STANDARD = 0,
    SECURITY_ENHANCED = 1,
    SYSTEM = 2
  }

  // HBB 28-JUN-2013
  public enum TITO_TICKET_STATUS
  {
    VALID = 0,
    CANCELED = 1,
    REDEEMED = 2,
    EXPIRED = 3,
    DISCARDED = 4,
    PENDING_CANCEL = 5,
    PENDING_PRINT = 6,
    PAID_AFTER_EXPIRATION = 7,
    // RGR 07-03-2017
    SWAP_FOR_CHIPS = 8, 
    PENDING_PAYMENT = 9,
    PENDING_PAYMENT_AFTER_EXPIRATION = 10
  }

  // HBB 28-JUN-2013
  public enum TITO_TERMINAL_TYPE
  {
    CASHIER = 0,
    TERMINAL = 1,
    COUNTR = 2
  }

  public enum ENUM_TITO_IS_VALID_TICKET_MSG
  {
    VALID_CASHABLE = 0x0,
    VALID_RESTRICTED_PROMOTIONAL_TICKET = 0x1,
    VALID_NONRESTRICTED_PROMOTIONAL_TICKET = 0x2,
    UNABLE_TO_VALIDATE = 0x80, // In Case ValidatedState does not meet any of the known conditions, defaults to this
    NOT_A_VALID_VALIDATION_NUMBER = 0x81, //
    VALIDATION_NUMBER_NOT_IN_SYSTEM = 0x82, //
    TICKET_MARKED_PENDING_IN_SYSTEM = 0x83,
    TICKET_ALREADY_REDEEMED = 0x84,
    TICKET_EXPIRED = 0x85,
    VALIDATION_INFO_NOT_AVAILABLE = 0x86,
    TICKET_AMOUNT_DOES_NOT_MATCH_SYSTEM_AMOUNT= 0x87,
    TICKET_AMOUNT_EXCEEDS_AUTO_REDEMPTION_LIMIT = 0x88,
    TERMINAL_DOES_NOT_ALLOW_REDEMPTION = 0x89,
    REQUEST_FOR_CURRENT_TICKET_STATUS = 0XFF,
    // For Pay Tickets with the iPOD
    VALID_HANDPAY = 0x03,
    VALID_JACKPOT = 0x04,
    TICKET_OFFLINE = 0x90,
  }

  // TITO Ticket types
  public enum TITO_TICKET_TYPE
  {
    CASHABLE = 0,
    PROMO_REDEEM = 1,
    PROMO_NONREDEEM = 2,  // only playable
    HANDPAY = 3,
    JACKPOT = 4,
    OFFLINE = 5,
    CHIPS_DEALER_COPY = 6
  }

  public enum TITO_STACKER_STATUS
  {
    DISABLED = 0,
    DEACTIVATED = 1,
    ACTIVE = 2,//Active
  }

  public enum TITO_MONEY_COLLECTION_STATUS
  {
    NONE = -1,   // Nothing
    OPEN = 0,    // Into EGM
    PENDING = 1, // Pending to collect
    CLOSED_IN_IMBALANCE = 2,
    CLOSED_BALANCED = 3
  }

  public enum ENUM_TITO_OPERATIONS_RESULT
  {
    EXCEPTION = -1,
    NONE = 0,
    OK = 1,
    GENERIC_ERROR = 3,
    DATA_NOT_FOUND = 4,
    DATA_NOT_SAVED = 5,
    AUX_DATA_NOT_FOUND = 6,
    AUX_DATA_NOT_SAVED = 7,
    AUX_OPERATION_ERROR = 8,
    AUX_OPERATION_CANCELED = 9,
    TICKET_NOT_VALID = 11,
    TICKET_NOT_NULLABLE = 12,
    PRINTER_IS_NOT_READY = 13,
    PRINTER_PRINT_FAIL = 14,
    PRINTER_PRINT_STOPED = 15,
    TPV_NOT_PAID = 16,
    CREDIT_LINE_KO = 17,
    JUNKETS_KO = 18
  }

  // indicates grouping indexes for SAS meters
  // refers to tables SAS_METERS_GROUPS and SAS_METERS_CATALOG_PER_GROUP
  public enum SAS_METERS_GROUPS
  {
    IDX_SMG_NONE = 0,
    IDX_SMG_BILLS = 1,        // indice para los grupos de billetes
    IDX_SMG_TICKETS = 2,      // indice para los grupos de tickets
    IDX_SMG_METERS_REPORTED = 3,
    IDX_SMG_LAST
  }

  // RGR 22-JAN-2016
  public enum ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM
  {
    CREATE_TICKET = 0,
    ONLY_ASSIGN_PROMOTION = 1
  }


  // FJC 27-10-2016 PBI 19605:TITO: SAS_HOST - informar motivo de rechazo
  public enum ENUM_TITO_REDEEM_TICKET_TRANS_STATUS
  {
    // SAS-HOST // This part of enumerate match with TITO constants en Sas_Host.h (LKT SAS-HOST)
    SAS_REDEEM_TICKET_TRANS_STATUS_CASHABLE_REDEEMED = 0x00,
    SAS_REDEEM_TICKET_TRANS_STATUS_RESTRICTED_PROMOTIONAL_REDEEMED = 0x01,
    SAS_REDEEM_TICKET_TRANS_STATUS_NONRESTRICTED_PROMOTIONAL_REDEEMED = 0x02,
    SAS_REDEEM_TICKET_TRANS_STATUS_WAITING_POOL = 0x20,
    SAS_REDEEM_TICKET_TRANS_STATUS_PENDING = 0x40,
    SAS_REDEEM_TICKET_TRANS_STATUS_REJECTED_BY_HOST = 0x80,
    SAS_REDEEM_TICKET_TRANS_STATUS_VALIDATION_NUMBER_NOT_MATCH = 0x81,
    SAS_REDEEM_TICKET_TRANS_STATUS_NOT_VALID_TRANSFER_FUNCTION = 0x82,
    SAS_REDEEM_TICKET_TRANS_STATUS_NOT_VALID_TRANSFER_AMOUNT = 0x83,
    SAS_REDEEM_TICKET_TRANS_STATUS_AMOUNT_EXCEEDED_EGM_LIMIT = 0x84,
    SAS_REDEEM_TICKET_TRANS_STATUS_AMOUNT_NOT_MULTIPLE_DENOM = 0x85,
    SAS_REDEEM_TICKET_TRANS_STATUS_AMOUNT_NOT_MATCH_TICKET_AMOUNT = 0x86,
    SAS_REDEEM_TICKET_TRANS_STATUS_MACHINE_UNABLE_TO_ACCEPT_THIS_TRANSFER = 0x87,
    SAS_REDEEM_TICKET_TRANS_STATUS_REJECTED_DUE_TIMEOUT = 0x88,
    SAS_REDEEM_TICKET_TRANS_STATUS_REJECTED_DUE_COMM_LINK_DOWN = 0x89,
    SAS_REDEEM_TICKET_TRANS_STATUS_REDEMPTION_DISABLED = 0x8A,
    SAS_REDEEM_TICKET_TRANS_STATUS_REJECTED_DUE_VALIDATOR_FAILURE = 0x8B,
    SAS_REDEEM_TICKET_TRANS_STATUS_NOT_COMPATIBLE_WITH_CURRENT_REDEMPTION_CYCLE = 0xC0,
    SAS_REDEEM_TICKET_TRANS_STATUS_NO_VALIDATION_INFO_AVAILABLE = 0xFF,
  }
}
