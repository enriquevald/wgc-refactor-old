//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Ticket.cs
// 
//   DESCRIPTION: Ticket class
// 
//        AUTHOR: Joshwa Marcalle
// 
// CREATION DATE: 30-SEP-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-SEP-2013 JRM    First version.
// 08-OCT-2013 NMR    Fixed errors in message processing: IsValidTicket and CancelTicket
// 09-OCT-2013 JRM    Renaming variables accordig to specs. Clean up
// 10-OCT-2013 JRM    Added method to calculate validation number based on system is and valitadion number
// 26-OCT-2013 JRM    Added method to replace play session virtual account for a real one.
// 05-NOV-2013 JRM    IsValid and Cancelticket methods interact with the intermediary field TI_CANCEL_IN_PROCESS in tickets table
// 11-NOV-2013 NMR    Fixed error in DB_ReplacePlaySessionVirtualAccountForRealAccount;
//                    improvemnt of all methods
// 19-NOV-2013 NMR    Added new ticket property
// 20-NOV-2013 NMR    Deleted unused method
// 26-NOV-2013 JRM    Ticket status change now updates ti_last_action_terminal and datetime 
// 27-NOV-2013 NMR    Fixed error when replacing play-session account; another improvements;
//                    Added property TransactionId; modified control of duplicated ticktes
// 28-NOV-2013 NMR    Don't actualize account, when it is the same
// 29-NOV-2013 NMR    Removed use of TI_XXX_CASHIER_SESSION
// 05-DIC-2013 NMR    Changed typeof property amount; will contain cash in Unities (not cents)
// 10-DIC-2013 NMR    Added new properties related to play-sessions
// 24-JAN-2013 JRM    Added method to fetch tickets based on Handpays
// 14-FEB-2014 DRV    Removed isCollected property and added CollectedMoneyCollection property
// 07-MAR-2014 QMP & DHA Fixed bug WIGOSTITO-1129: Added 'terminal_type' filter to DB_GetTicketsFromOperation()
// 07-MAR-2014 QMP & DDM & DHA Fixed bug WIGOSTITO-1131: Created Cash/Account movements have the same Operation ID
// 25-APR-2014 DHA & JML Rename/Added TITO Cashier/Account Movements
// 27-NOC-2014 LRS    Add field TI_ACCOUNT_PROMOTION
// 15-DIC-2014 LRS    Add trace account id promotion
// 09-JUN-2015 MPO    Fixed Bug WIG-2420
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 14-DEC-2015 MPO    Fixed Bug 6580:Ref.14723: Tickets expired
// 28-JAN-2016 DHA    Product Backlog Item 8351:Floor Dual Currency: Movimientos de tickets TITO en M.N.
// 02-FEB-2016 JML    Product Backlog Item 8352:Floor Dual Currency: Ticket Offline
// 08-APR-2016 DHA    Fixed Bug 11591: Floor Dual Currency - error on exchange conversion in cashier movement
// 19-APR-2016 ETP    Fixed Bug 12159: PendingCancell.AllowToReedem allows to pays Non-Redeemeble Tickets.
// 04-APR-2016 RGR    Product Backlog Item 11018:UNPLANNED - Temas varios Winions - Mex Sprint 22
// 02-MAY-2016 ADI    Add Property Taxes  
// 12-JUL-2016 ESE    Fixed Bug 15218:Buy chips, validate ticket and amount is $0
// 18-JUL-2016 LTC    Bug 15672: TITO pay no valid tickets redeemable
// 05-SEP-2016 AMF    Bug 17278: Tickets No Redimibles
// 13-OCT-2016 FJC    PBI 18258:TITO: Nuevo estado "Pending Print" en tickets - Creaci�n nuevo estado
// 20-OCT-2016 XGJ    PBI 18259:TITO: Nuevo estado "Pending Print" en tickets - Mostrar nuevo estado en pantallas (GUI y Cashier)

// 02-NOV-2016 OVS    Bug 19934: CountR: Validaci�n tickets TITO Cajero
// 03-OCT-2016 FJC    PBI 19605:TITO: SAS_HOST - informar motivo de rechazo
// 18-NOV-2016 LTC    Bug 17133: TITO Ticket validation is allowed with out the corresponding GP activated
// 01-DIC-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
// 18-JAN-2017 FAV    Bug 19045: TITO, Error in Collection for tickets with Cage.automode = 1
// 22-MAR-2017 RAB    PBI 25973: MES10 Ticket validation - Dealer copy link to TITO ticket
// 23-MAR-2017 RAB    PBI 25973: MES10 Ticket validation - Dealer copy link to TITO ticket
// 03-APR-2017 RAB    PBI 26401: MES10 Ticket validation - Ticket status change to cancelled
// 18-JUL-2017 FJC    Defect WIGOS-3118 [Ticket #4699] Too many handpays being generated because of late arrival of ticket numbers
// 28-JUL-2017 DHA    Bug 29036:WIGOS-4075 Wrong amount when paying a ticket
// 16-OCT-2017 JMM    Bug 30258:WIGOS-5677 WCP is unable to mark some tickets as expired
// 15-NOV-2017 JML    Bug 30798:WIGOS-5926 [Ticket #9396] error titios promo
// 05-JUN-2018 AGS		Bug 32889:WIGOS-12142, WIGOS-12143, WIGOS-12146, WIGOS-12148 - Drop mesas
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Drawing;
using WSI.Common.TITO;
using WSI.Common.CountR;

namespace WSI.Common
{
  public class Ticket
  {
    // Constants that indicates the limit of sequences for ticket validation_number generation
    // Both limits are established according to SAS protocol
    // There is a safety margin to prevent errors when tables updates fails
    static public Int64 MAX_TERMINAL_SEQUENCE_ID = 0xFFFFFF - 10000;
    static public Int64 MAX_CASHIER_SEQUENCE_ID = 0xFFFFFF - 10000;

    public enum ENUM_PAYMENT_TYPE
    {
      Redeem = 0,
      Ticket = 1
    }

    // class internall properties
    private Int64 m_ticket_id;
    private TITO_VALIDATION_TYPE m_validation_type;
    private Int64 m_validation_number;
    private Int32 m_machine_ticket_number;
    private Decimal m_amount;
    private TITO_TICKET_STATUS m_status;
    private TITO_TICKET_TYPE m_ticket_type;
    private DateTime m_created_date_time;
    private Int32 m_created_terminal_id;
    private Int32 m_created_terminal_type;
    private DateTime m_expiration_datetime;
    private Int64 m_money_collection_id;
    private Int64 m_created_account_id;
    private Int32 m_last_action_terminal_id;
    private DateTime m_last_action_datetime;
    private Int64 m_last_action_account_id;
    private Int32 m_last_action_terminal_type;
    private Int64 m_promotion_id;
    private Int64 m_account_promotion_id;
    private Int64 m_collected_money_collection;
    private Int64 m_pool_id;
    // private Int32 m_validation_system_id;
    private Int64 m_transaction_id;
    private Int64 m_created_play_session_id;
    //  private Int64 m_canceled_play_session_id;

    // Helper variable
    //private Int32 m_affected_rows;
    //private Int32 m_valid_tickets_count;
    //private Int32 m_invalid_tickets_count;

    // aux properties used in cashier
    private String m_created_terminal_name;          // nombre del terminal de creaci�n
    // private String m_created_terminal_type_name;     // nombre del tipo de terminal de creaci�n
    private String m_last_action_terminal_name;      // nombre del terminal en d�nde se modifica
    // private String m_last_action_terminal_type_name; // nombre del tipo de terminal de modificaci�n
    private List<CountRTaxes> m_ticket_countr_taxes; // Taxas asignadas al ticket. L�gica para CountR

    private Decimal m_amt_0;
    private String m_cur_0;
    private Decimal m_amt_1;
    private String m_cur_1;

    private String m_address_1;
    private String m_address_2;

    #region Properties

    //public Int32 ValidTicketsCount
    //{
    //  get { return m_valid_tickets_count; }
    //  set { m_valid_tickets_count = value; }
    //}

    //public Int32 InvalidTicketsCount
    //{
    //  get { return m_invalid_tickets_count; }
    //  set { m_invalid_tickets_count = value; }
    //}

    //public Int32 AffectedRows
    //{
    //  get { return m_affected_rows; }
    //  set { m_affected_rows = value; }
    //}

    public Int64 TicketID
    {
      get { return m_ticket_id; }
      set { m_ticket_id = value; }
    }

    public Int64 ValidationNumber
    {
      get { return m_validation_number; }
      set { m_validation_number = value; }
    }

    public Int32 MachineTicketNumber
    {
      get { return m_machine_ticket_number; }
      set { m_machine_ticket_number = value; }
    }

    //public Int32 SystemID
    //{
    //  get { return m_validation_system_id; }
    //  set { m_validation_system_id = value; }
    //}

    public Int64 TransactionId
    {
      get { return m_transaction_id; }
      set { m_transaction_id = value; }
    }

    public Int64 CreatedPlaySessionId
    {
      get { return m_created_play_session_id; }
      set { m_created_play_session_id = value; }
    }

    //public Int64 CanceledPlaySessionId
    //{
    //  get { return m_canceled_play_session_id; }
    //  set { m_canceled_play_session_id = value; }
    //}

    public Int64 PoolID
    {
      get { return m_pool_id; }
      set { m_pool_id = value; }
    }

    public TITO_VALIDATION_TYPE ValidationType
    {
      get { return m_validation_type; }
      set { m_validation_type = value; }
    }

    public DateTime CreatedDateTime
    {
      get { return m_created_date_time; }
      set { m_created_date_time = value; }
    }

    public Int32 CreatedTerminalId
    {
      get { return m_created_terminal_id; }
      set { m_created_terminal_id = value; }
    }

    public Int32 CreatedTerminalType
    {
      get { return m_created_terminal_type; }
      set { m_created_terminal_type = value; }
    }

    public DateTime ExpirationDateTime
    {
      get { return m_expiration_datetime; }
      set { m_expiration_datetime = value; }
    }

    public Int64 MoneyCollectionID
    {
      get { return m_money_collection_id; }
      set { m_money_collection_id = value; }
    }

    public Int64 CreatedAccountID
    {
      get { return m_created_account_id; }
      set { m_created_account_id = value; }
    }

    public Int32 LastActionTerminalID
    {
      get { return m_last_action_terminal_id; }
      set { m_last_action_terminal_id = value; }
    }

    public DateTime LastActionDateTime
    {
      get { return m_last_action_datetime; }
      set { m_last_action_datetime = value; }
    }

    public Int64 LastActionAccountID
    {
      get { return m_last_action_account_id; }
      set { m_last_action_account_id = value; }
    }

    public Int32 LastActionTerminalType
    {
      get { return m_last_action_terminal_type; }
      set { m_last_action_terminal_type = value; }
    }

    public Int64 PromotionID
    {
      get { return m_promotion_id; }
      set { m_promotion_id = value; }
    }

    public Int64 AccountPromoId
    {
      get { return m_account_promotion_id; }
      set { m_account_promotion_id = value; }
    }

    public Int64 CollectedMoneyCollection
    {
      get { return m_collected_money_collection; }
      set { m_collected_money_collection = value; }
    }


    //public Int64 LastActionCashierSession
    //{
    //  get { return m_last_action_cashier_session; }
    //  set { m_last_action_cashier_session = value; }
    //}

    //public Int64 CreatedCashierSession
    //{
    //  get { return m_created_cashier_session; }
    //  set { m_created_cashier_session = value; }
    //}

    public Decimal Amount
    {
      get { return m_amount; }
      set { m_amount = value; }
    }

    public TITO_TICKET_STATUS Status
    {
      get { return m_status; }
      set { m_status = value; }
    }

    public TITO_TICKET_TYPE TicketType
    {
      get { return m_ticket_type; }
      set { m_ticket_type = value; }
    }

    public String CreatedTerminalName
    {
      get { return m_created_terminal_name; }
      set { m_created_terminal_name = value; }
    }

    //public String CreatedTerminalTypeName
    //{
    //  get { return m_created_terminal_type_name; }
    //  set { m_created_terminal_type_name = value; }
    //}

    public String LastActionTerminalName
    {
      get { return m_last_action_terminal_name; }
      set { m_last_action_terminal_name = value; }
    }

    //public String LastActionTerminalTypeName
    //{
    //  get { return m_last_action_terminal_type_name; }
    //  set { m_last_action_terminal_type_name = value; }
    //}

    public Boolean IsActive
    {
      get { return (((m_status == TITO_TICKET_STATUS.VALID && m_status == TITO_TICKET_STATUS.PENDING_PRINT) && m_collected_money_collection == 0) || this.IsPendingCancel); }
    }

    public Boolean IsPendingCancel
    {
      get
      {
        Boolean _allow_to_redeem_gp;
        _allow_to_redeem_gp = GeneralParam.GetBoolean("TITO", "PendingCancel.AllowToRedeem", false);

        return (_allow_to_redeem_gp && m_status == TITO_TICKET_STATUS.PENDING_CANCEL);
      }
    }

    public Boolean IsRedeemable
    {
      get
      {
        return (m_ticket_type == TITO_TICKET_TYPE.PROMO_REDEEM ||
                m_ticket_type == TITO_TICKET_TYPE.CASHABLE ||
                m_ticket_type == TITO_TICKET_TYPE.HANDPAY ||
                m_ticket_type == TITO_TICKET_TYPE.JACKPOT);
      }
    }

    // LTC 18-NOV-2016
    public static Boolean IsAllowToAvoid
    {
      get
      {
        Boolean _allow_to_avoid;
        _allow_to_avoid = GeneralParam.GetBoolean("TITO", "PendingCancel.AllowToVoid", false);

        return (_allow_to_avoid);
      }
    }

    public Boolean AllowedRedemption
    {
      get { return IsActive && IsRedeemable; }
    }

    public Decimal Amt_0
    {
      get { return m_amt_0; }
      set { m_amt_0 = value; }
    }

    public String Cur_0
    {
      get { return m_cur_0; }
      set { m_cur_0 = value; }
    }

    public Decimal Amt_1
    {
      get { return m_amt_1; }
      set { m_amt_1 = value; }
    }

    public String Cur_1
    {
      get { return m_cur_1; }
      set { m_cur_1 = value; }
    }

    public String Address1
    {
      get { return m_address_1; }
      set { m_address_1 = value; }
    }

    public String Address2
    {
      get { return m_address_2; }
      set { m_address_2 = value; }
    }

    public List<CountRTaxes> Taxes
    {
      get { return m_ticket_countr_taxes; }
      set { m_ticket_countr_taxes = value; }
    }

    #endregion

    public Ticket()
    {

    }

    public Boolean DB_CreateTicket(SqlTransaction SqlTrx,
                                  String CardTrackData,
                                  Boolean UpdateAccount,
                                  Decimal InitialBalance,
                                  Boolean IsSwap,
                                  CashierMovementsTable CashierMovementsTable,
                                  AccountMovementsTable AccountMovementsTable)
    {
      return DB_CreateTicket(SqlTrx, CardTrackData, UpdateAccount, ENUM_PAYMENT_TYPE.Redeem, InitialBalance, CashierMovementsTable, AccountMovementsTable, IsSwap);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: - Function for ticket creation, all types. 
    //          - Prefer use with CardTrackData, but is not necesary
    //          - Method is also used when ticket is created cloning another that is VALID; 
    //            that is the reason that some columns in BD are NULL.
    //            This is correct situation.
    //
    //  PARAMS:
    //      - INPUT:
    //        - CardTrackData: Track data for cashier movements
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //        - CashierMovementsTable: for save cashier movements if al is ok.
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //public Boolean DB_CreateTicket(SqlTransaction SqlTrx, CashierMovementsTable CashierMovementsTable, AccountMovementsTable AccountMovementsTable)
    //{
    //  return DB_CreateTicket(SqlTrx, "", false, ENUM_PAYMENT_TYPE.Redeem, 0, CashierMovementsTable, AccountMovementsTable);
    //}

    public Boolean DB_CreateTicket(SqlTransaction SqlTrx,
                                   String CardTrackData,
                                   Boolean UpdateAccount,
                                   Decimal InitialBalance,
                                   CashierMovementsTable CashierMovementsTable,
                                   AccountMovementsTable AccountMovementsTable)
    {
      return DB_CreateTicket(SqlTrx, CardTrackData, UpdateAccount, ENUM_PAYMENT_TYPE.Redeem, InitialBalance, CashierMovementsTable, AccountMovementsTable);
    }

    public Boolean DB_CreateTicket(SqlTransaction SqlTrx,
                                   String CardTrackData,
                                   Boolean UpdateAccount,
                                   ENUM_PAYMENT_TYPE m_payment_type,
                                   Decimal InitialBalance,
                                   CashierMovementsTable CashierMovements,
                                   AccountMovementsTable AccountMovements,
                   bool IsSwap = false)
    {
      String _cmd;
      SqlParameter _param_ti_id;
      SqlParameter _param_ti_amount;
      CASHIER_MOVEMENT _cashier_mov_type;
      MovementType _account_mov_type;
      MultiPromos.AccountBalance _acc_ini_balance;
      MultiPromos.AccountBalance _acc_final_balance;
      MultiPromos.AccountBalance _ticket_amount;
      Decimal _sub_balance;
      Decimal _add_balance;
      Decimal _final_balance;
      String _details;
      Int64 _transaction_id;
      Promotion.TYPE_PROMOTION_DATA m_selected_promo_data = new Promotion.TYPE_PROMOTION_DATA();
      Object _value;
      Int64 _aux_promotion_id;
      DateTime _aux_cancelation_date;
      String _terminal_iso_code;
      Decimal _amount_issue;
      SqlParameter _param_ti_validation_number;
      SqlParameter _param_ti_transaction_id;
      Int32 _time_to_check_duplicity_in_cashout;
      Boolean _pending_print_enabled;

      _cmd = String.Empty;
      _transaction_id = 0;
      _value = 0;
      _param_ti_validation_number = new SqlParameter();
      _param_ti_transaction_id = new SqlParameter();

      // 08-APR-2016 DHA: Floor Dual Currency
      _amount_issue = this.Amt_0;
      _terminal_iso_code = CurrencyExchange.GetNationalCurrency();
      _time_to_check_duplicity_in_cashout = 0;
      _pending_print_enabled = false;

      if (this.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM || this.TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
      {
        if (m_created_play_session_id > 0)
        {
          GetAccountPromotionAndExpirationDate(this.CreatedTerminalId, out _aux_promotion_id, out _aux_cancelation_date, SqlTrx);
          if (_aux_promotion_id > 0)
          {
            this.AccountPromoId = _aux_promotion_id;
            _value = _aux_promotion_id;
          }
          if (_aux_cancelation_date != DateTime.MinValue)
          {
            DateTime _aux_today;
            Int32 _cmp_datetime;

            _aux_today = Misc.TodayOpening().AddDays(1);
            _cmp_datetime = _aux_cancelation_date.CompareTo(_aux_today);
            this.ExpirationDateTime = _cmp_datetime < 0 ? _aux_today : _aux_cancelation_date;
            if (_cmp_datetime < 0)
            {
              StringBuilder _str_builder = new StringBuilder();
              _str_builder.Append("DB_CreateTicket: Expiration datetime from promotion incorrectly: ");
              _str_builder.Append(_aux_cancelation_date.ToString("u") + " replace for " + _aux_today.ToString("u"));
              _str_builder.Append(" - CreatedTerminalId: " + this.CreatedTerminalId.ToString());
              _str_builder.Append(" - PromoId: " + _aux_promotion_id.ToString());
              Log.Warning(_str_builder.ToString());
            }
          }
        }
      }

      _pending_print_enabled = GeneralParam.GetBoolean("TITO", "PendingPrint.Enabled", true);
      _time_to_check_duplicity_in_cashout = GeneralParam.GetInt32("TITO", "Tickets.TimeToCheckDuplicityInCashOut", 5, 0, 14);

      _cmd = "TITO_CreateTicket";

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_cmd, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;

          _sql_cmd.Parameters.Add("@pTicketValidationNumber", SqlDbType.BigInt).Value = this.ValidationNumber;

          _param_ti_amount = _sql_cmd.Parameters.Add("@pTicketAmount", SqlDbType.Money);
          _param_ti_amount.Value = this.Amount;
          _sql_cmd.Parameters.Add("@pTicketStatus", SqlDbType.Int).Value = this.Status;
          _sql_cmd.Parameters.Add("@pTicketStatusCheck", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.PENDING_PRINT;
          _sql_cmd.Parameters.Add("@pTicketType", SqlDbType.Int).Value = this.TicketType;
          _sql_cmd.Parameters.Add("@pTicketCreatedDateTime", SqlDbType.DateTime).Value = this.CreatedDateTime;
          _sql_cmd.Parameters.Add("@pCreatedTerminalId", SqlDbType.Int).Value = this.CreatedTerminalId;
          _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = this.CreatedTerminalType;
          _sql_cmd.Parameters.Add("@pTicketExpirationDateTime", SqlDbType.DateTime).Value = this.ExpirationDateTime;
          _sql_cmd.Parameters.Add("@pTicketCreatedAccountID", SqlDbType.BigInt).Value = this.CreatedAccountID;
          if (this.PromotionID != 0)
          {
            _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = this.PromotionID;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          _sql_cmd.Parameters.Add("@pValidationType", SqlDbType.Int).Value = (Int32)this.ValidationType;
          _sql_cmd.Parameters.Add("@pMachineTicketNumber", SqlDbType.Int).Value = this.MachineTicketNumber;

          if (this.TransactionId != 0)
          {
            _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = this.TransactionId;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = DBNull.Value;
          }

          if (this.CreatedPlaySessionId != 0)
          {
            _sql_cmd.Parameters.Add("@pCreatedPlaySessionId", SqlDbType.BigInt).Value = this.CreatedPlaySessionId;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pCreatedPlaySessionId", SqlDbType.BigInt).Value = DBNull.Value;
          }

          //get if promotion is Re o NRE
          if (m_created_play_session_id == 0)
          {
            //cashier functionality
            if (this.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM || this.TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
            {
              _value = this.AccountPromoId;
            }
          }
          _sql_cmd.Parameters.Add("@pAccountPromotion", SqlDbType.BigInt).Value = _value;

          Currency _egm_amt;
          Currency _sys_amt;
          String _egm_iso;
          String _sys_iso;
          Object _obj;

          _egm_amt = this.Amt_0;
          _egm_iso = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
          _sys_iso = _egm_iso;

          if (GeneralParam.GetBoolean("FloorDualCurrency", "Enabled", false) && this.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.TERMINAL)
          {
            using (SqlCommand _sql_cmd_te_iso2 = new SqlCommand("SELECT TE_ISO_CODE FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId", SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd_te_iso2.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = this.CreatedTerminalId;
              _obj = _sql_cmd_te_iso2.ExecuteScalar();
              if (_obj != null && _obj != DBNull.Value)
              {
                _egm_iso = (String)_obj;
                _terminal_iso_code = _egm_iso;
              }
            }

            _sys_amt = CurrencyExchange.GetExchange(_egm_amt, _egm_iso, _sys_iso, SqlTrx);

            // TI_AMOUNT
            _param_ti_amount.Value = (Decimal)_sys_amt;
            this.Amount = (Decimal)_sys_amt;

          }
          else if(this.TicketType == TITO_TICKET_TYPE.CHIPS_DEALER_COPY)
          {
            _egm_iso = this.Cur_0;
          }

          _sql_cmd.Parameters.Add("@pAmt0", SqlDbType.Money).Value = (Decimal)_egm_amt;
          _sql_cmd.Parameters.Add("@pCur0", SqlDbType.NVarChar, 3).Value = _egm_iso;

          _sql_cmd.Parameters.Add("@pPendingPrintEnabled", SqlDbType.Bit).Value = _pending_print_enabled;
          _sql_cmd.Parameters.Add("@pTimeToCheckDuplicityInCashout", SqlDbType.Int).Value = _time_to_check_duplicity_in_cashout;

          _param_ti_id = _sql_cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt);
          _param_ti_id.SourceColumn = "TI_TICKET_ID";
          _param_ti_id.Direction = ParameterDirection.Output;

          _param_ti_validation_number = _sql_cmd.Parameters.Add("@pValidationNumberOutput", SqlDbType.BigInt);
          _param_ti_validation_number.SourceColumn = "TI_VALIDATION_NUMBER";
          _param_ti_validation_number.Direction = ParameterDirection.Output;

          _param_ti_transaction_id = _sql_cmd.Parameters.Add("@pTransactionIdOutput", SqlDbType.BigInt);
          _param_ti_transaction_id.SourceColumn = "TI_TRANSACTION_ID";
          _param_ti_transaction_id.Direction = ParameterDirection.Output;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          this.TicketID = (Int64)_param_ti_id.Value;
          if (_pending_print_enabled)
          {
            this.ValidationNumber = (Int64)_param_ti_validation_number.Value;
            this.TransactionId = (Int64)_param_ti_transaction_id.Value;
          }

          _details = "";
          _sub_balance = Amount;
          _add_balance = 0;

          // DHA 25-APR-2014 modified/added TITO tickets movements
          switch (TicketType)
          {
            case TITO_TICKET_TYPE.CASHABLE:
            case TITO_TICKET_TYPE.CHIPS_DEALER_COPY:
              _ticket_amount = new MultiPromos.AccountBalance(this.Amount, 0, 0);
              if (this.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.CASHIER)
              {
                _account_mov_type = MovementType.TITO_TicketCashierPrintedCashable;
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_CASHABLE;
              }
              else if (this.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.TERMINAL)
              {
                _account_mov_type = MovementType.TITO_TicketMachinePrintedCashable;
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_CASHABLE;
              }
              else
              {
                _account_mov_type = MovementType.TITO_TicketCountRPrintedCashable;
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_COUNTR_PRINTED_CASHABLE;
              }
              break;
            case TITO_TICKET_TYPE.PROMO_REDEEM:
              _ticket_amount = new MultiPromos.AccountBalance(0, this.Amount, 0);
              _account_mov_type = MovementType.TITO_TicketCashierPrintedPromoRedeemable;
              _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE;
              break;
            case TITO_TICKET_TYPE.PROMO_NONREDEEM:
              _ticket_amount = new MultiPromos.AccountBalance(0, 0, this.Amount);
              if (this.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.CASHIER)
              {
                _account_mov_type = MovementType.TITO_TicketCashierPrintedPromoNotRedeemable;
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE;
              }
              else
              {
                _account_mov_type = MovementType.TITO_TicketMachinePrintedPromoNotRedeemable;
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE;
              }
              break;
            case TITO_TICKET_TYPE.OFFLINE:
              if (m_payment_type == ENUM_PAYMENT_TYPE.Ticket)
              {
                _ticket_amount = new MultiPromos.AccountBalance(0, 0, this.Amount);
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CREATE_PLAYABLE_OFFLINE;
              }
              else
              {
                _ticket_amount = new MultiPromos.AccountBalance(this.Amount, 0, 0);
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CREATE_CASHABLE_OFFLINE;
              }
              _account_mov_type = MovementType.TITO_TicketOffline;
              InitialBalance = 0;
              _sub_balance = 0;
              _add_balance = Amount;
              break;
            case TITO_TICKET_TYPE.HANDPAY:
              _ticket_amount = MultiPromos.AccountBalance.Zero;
              _account_mov_type = MovementType.TITO_TicketMachinePrintedHandpay;
              _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_HANDPAY;
              _details = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_HANDPAY");
              _sub_balance = 0;
              _add_balance = 0;
              break;
            case TITO_TICKET_TYPE.JACKPOT:
              _ticket_amount = MultiPromos.AccountBalance.Zero;
              _account_mov_type = MovementType.TITO_TicketMachinePrintedJackpot;
              _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_JACKPOT;
              _details = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_JACKPOT");
              _sub_balance = 0;
              _add_balance = 0;
              break;
            default:
              _ticket_amount = MultiPromos.AccountBalance.Zero;
              _cashier_mov_type = CASHIER_MOVEMENT.NOT_ASSIGNED;
              _account_mov_type = (MovementType)(-1);
              _sub_balance = 0;
              _add_balance = 0;
              break;
          }

          if (UpdateAccount)
          {
            if (!Utils.AccountBalanceOut(CreatedAccountID, _ticket_amount, AccountPromoId, out _acc_ini_balance, out _acc_final_balance, SqlTrx))
            {
              return false;
            }
            InitialBalance = _acc_ini_balance.TotalBalance;
            _final_balance = _acc_final_balance.TotalBalance;
          }
          else
          {
            _final_balance = Math.Max(0, InitialBalance - _sub_balance + _add_balance);
          }
        }

        // QMP & DHA: When terminal type is terminal, OperationId must be 0
        if ((TITO_TERMINAL_TYPE)this.CreatedTerminalType != TITO_TERMINAL_TYPE.TERMINAL)
        {
          _transaction_id = this.TransactionId;
        }

        if (Misc.IsFloorDualCurrencyEnabled() && (TITO_TERMINAL_TYPE)this.CreatedTerminalType == TITO_TERMINAL_TYPE.TERMINAL && _terminal_iso_code != CurrencyExchange.GetNationalCurrency())
        {
          CashierMovements.Add(_transaction_id, CASHIER_MOVEMENT.TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY, 0, CreatedAccountID, CardTrackData, _details, _terminal_iso_code);
          CashierMovements.SetInitialBalanceInLastMov(_amount_issue);
          CashierMovements.SetAddAmountInLastMov(Amount);
        }

        // Cashier & Account movements, are save in the call function
        AccountMovements.Add(_transaction_id, CreatedAccountID, _account_mov_type, InitialBalance, _sub_balance, _add_balance, _final_balance, _details);
        CashierMovements.Add(_transaction_id, _cashier_mov_type, Amount, CreatedAccountID, CardTrackData, _details, IsSwap);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    }


    //public Boolean DB_CreateTicketMovements(SqlTransaction SqlTrx,
    //                                         String CardTrackData,
    //                                         Boolean UpdateAccount,
    //                                         ENUM_PAYMENT_TYPE m_payment_type,
    //                                         Decimal InitialBalance,
    //                                         CashierMovementsTable CashierMovements,
    //                                         AccountMovementsTable AccountMovements)
    //{
    //  CASHIER_MOVEMENT _cashier_mov_type;
    //  MovementType _account_mov_type;
    //  MultiPromos.AccountBalance _acc_ini_balance;
    //  MultiPromos.AccountBalance _acc_final_balance;
    //  MultiPromos.AccountBalance _ticket_amount;
    //  Decimal _sub_balance;
    //  Decimal _add_balance;
    //  Decimal _final_balance;
    //  String _details;
    //  Int64 _transaction_id;

    //  _transaction_id = 0;

    //  try
    //  {
    //    _details = "";
    //    _sub_balance = Amount;
    //    _add_balance = 0;

    //    // DHA 25-APR-2014 modified/added TITO tickets movements
    //    switch (TicketType)
    //    {
    //      case TITO_TICKET_TYPE.CASHABLE:
    //        _ticket_amount = new MultiPromos.AccountBalance(this.Amount, 0, 0);
    //        if (this.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.CASHIER)
    //        {
    //          _account_mov_type = MovementType.TITO_TicketCashierPrintedCashable;
    //          _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_CASHABLE;
    //        }
    //        else
    //        {
    //          _account_mov_type = MovementType.TITO_TicketMachinePrintedCashable;
    //          _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_CASHABLE;
    //        }
    //        break;
    //      case TITO_TICKET_TYPE.PROMO_REDEEM:
    //        _ticket_amount = new MultiPromos.AccountBalance(0, this.Amount, 0);
    //        _account_mov_type = MovementType.TITO_TicketCashierPrintedPromoRedeemable;
    //        _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE;
    //        break;
    //      case TITO_TICKET_TYPE.PROMO_NONREDEEM:
    //        _ticket_amount = new MultiPromos.AccountBalance(0, 0, this.Amount);
    //        if (this.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.CASHIER)
    //        {
    //          _account_mov_type = MovementType.TITO_TicketCashierPrintedPromoNotRedeemable;
    //          _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE;
    //        }
    //        else
    //        {
    //          _account_mov_type = MovementType.TITO_TicketMachinePrintedPromoNotRedeemable;
    //          _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE;
    //        }
    //        break;
    //      case TITO_TICKET_TYPE.OFFLINE:
    //        if (m_payment_type == ENUM_PAYMENT_TYPE.Ticket)
    //        {
    //          _ticket_amount = new MultiPromos.AccountBalance(0, 0, this.Amount);
    //          _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CREATE_PLAYABLE_OFFLINE;
    //        }
    //        else
    //        {
    //          _ticket_amount = new MultiPromos.AccountBalance(this.Amount, 0, 0);
    //          _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CREATE_CASHABLE_OFFLINE;
    //        }
    //        _account_mov_type = MovementType.TITO_TicketOffline;
    //        InitialBalance = 0;
    //        _sub_balance = 0;
    //        _add_balance = Amount;
    //        break;
    //      case TITO_TICKET_TYPE.HANDPAY:
    //        _ticket_amount = MultiPromos.AccountBalance.Zero;
    //        _account_mov_type = MovementType.TITO_TicketMachinePrintedHandpay;
    //        _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_HANDPAY;
    //        _details = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_HANDPAY");
    //        _sub_balance = 0;
    //        _add_balance = 0;
    //        break;
    //      case TITO_TICKET_TYPE.JACKPOT:
    //        _ticket_amount = MultiPromos.AccountBalance.Zero;
    //        _account_mov_type = MovementType.TITO_TicketMachinePrintedJackpot;
    //        _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_JACKPOT;
    //        _details = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_JACKPOT");
    //        _sub_balance = 0;
    //        _add_balance = 0;
    //        break;
    //      default:
    //        _ticket_amount = MultiPromos.AccountBalance.Zero;
    //        _cashier_mov_type = CASHIER_MOVEMENT.NOT_ASSIGNED;
    //        _account_mov_type = (MovementType)(-1);
    //        _sub_balance = 0;
    //        _add_balance = 0;
    //        break;
    //    }

    //    if (UpdateAccount)
    //    {
    //      if (!Utils.AccountBalanceOut(CreatedAccountID, _ticket_amount, AccountPromoId, out _acc_ini_balance, out _acc_final_balance, SqlTrx))
    //      {
    //        return false;
    //      }
    //      InitialBalance = _acc_ini_balance.TotalBalance;
    //      _final_balance = _acc_final_balance.TotalBalance;
    //    }
    //    else
    //    {
    //      _final_balance = Math.Max(0, InitialBalance - _sub_balance + _add_balance);
    //    }

    //    // QMP & DHA: When terminal type is terminal, OperationId must be 0
    //    if ((TITO_TERMINAL_TYPE)this.CreatedTerminalType != TITO_TERMINAL_TYPE.TERMINAL)
    //    {
    //      _transaction_id = this.TransactionId;
    //    }

    //    // Cashier & Account movements, are save in the call function
    //    AccountMovements.Add(_transaction_id, CreatedAccountID, _account_mov_type, InitialBalance, _sub_balance, _add_balance, _final_balance, _details);
    //    CashierMovements.Add(_transaction_id, _cashier_mov_type, Amount, CreatedAccountID, CardTrackData, _details);

    //    return true;
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);
    //  }

    //  return false;

    //}  // DB_CreateTicket


    //------------------------------------------------------------------------------
    // PURPOSE: Create tickets
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Datable: TicketsCreated
    //          - SqlTransaction: SqlTrx
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //      - true: ticket was successfully created
    //      - false: error in transaction
    // 
    //   NOTES:
    //
    //public static Boolean DB_CreateTicketBatch(DataTable TicketsCreated, SqlTransaction SqlTrx)
    //{
    //  StringBuilder _sb;
    //  Int32 _nr;

    //  _sb = new StringBuilder();

    //  _sb.AppendLine(" INSERT   TICKETS                       ");
    //  _sb.AppendLine("        ( TI_VALIDATION_NUMBER          ");
    //  _sb.AppendLine("        , TI_AMOUNT                     ");
    //  _sb.AppendLine("        , TI_STATUS                     ");
    //  _sb.AppendLine("        , TI_TYPE_ID                    ");
    //  _sb.AppendLine("        , TI_CREATED_DATETIME           ");
    //  _sb.AppendLine("        , TI_CREATED_TERMINAL_ID        ");
    //  _sb.AppendLine("        , TI_CREATED_TERMINAL_TYPE      ");
    //  _sb.AppendLine("        , TI_EXPIRATION_DATETIME        ");
    //  //_sb.AppendLine("        , TI_CREATED_ACCOUNT_ID         ");
    //  ////_sb.AppendLine("        , TI_PROMOTION_ID               "); DELETE!
    //  _sb.AppendLine("        , TI_VALIDATION_TYPE            ");
    //  _sb.AppendLine("        , TI_MACHINE_NUMBER             ");
    //  _sb.AppendLine("        , TI_TRANSACTION_ID             ");
    //  //_sb.AppendLine("        , TI_CREATED_PLAY_SESSION_ID    ");
    //  _sb.AppendLine("        )                               ");
    //  _sb.AppendLine(" VALUES ( @pTicketValidationNumber      ");
    //  _sb.AppendLine("        , @pTicketAmount                ");
    //  _sb.AppendLine("        , @pTicketStatus                ");
    //  _sb.AppendLine("        , @pTicketType                  ");
    //  _sb.AppendLine("        , @pTicketCreatedDateTime       ");
    //  _sb.AppendLine("        , @pCreatedTerminalId           ");
    //  _sb.AppendLine("        , @pTerminalType                ");
    //  _sb.AppendLine("        , @pTicketExpirationDateTime    ");
    //  //_sb.AppendLine("        , @pTicketCreatedAccountID      ");
    //  ////_sb.AppendLine("        , @pPromotionId                 ");
    //  _sb.AppendLine("        , @pValidationType              ");
    //  _sb.AppendLine("        , @pMachineTicketNumber         ");
    //  _sb.AppendLine("        , @pTransactionId               ");
    //  //_sb.AppendLine("        , @pCreatedPlaySessionId        ");
    //  _sb.AppendLine("        )                               ");
    //  _sb.AppendLine("    SET   @pTicketId = SCOPE_IDENTITY() ");

    //  try
    //  {
    //    using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
    //    {
    //      _sql_cmd.Parameters.Add("@pTicketValidationNumber", SqlDbType.BigInt).SourceColumn = "ValidationNumber";
    //      _sql_cmd.Parameters.Add("@pTicketAmount", SqlDbType.Money).SourceColumn = "TicketAmount";
    //      _sql_cmd.Parameters.Add("@pTicketStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID;
    //      _sql_cmd.Parameters.Add("@pTicketType", SqlDbType.Int).SourceColumn = "TicketType";
    //      _sql_cmd.Parameters.Add("@pTicketCreatedDateTime", SqlDbType.DateTime).SourceColumn = "TicketCreated";
    //      _sql_cmd.Parameters.Add("@pCreatedTerminalId", SqlDbType.Int).SourceColumn = "TerminalId";
    //      _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = (Int32)TITO_TERMINAL_TYPE.TERMINAL;
    //      _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).SourceColumn = "TransactionId";
    //      _sql_cmd.Parameters.Add("@pTicketExpirationDateTime", SqlDbType.DateTime).SourceColumn = "TicketExpiration";//this.ExpirationDateTime;
    //      _sql_cmd.Parameters.Add("@pValidationType", SqlDbType.Int).SourceColumn = "ValidationType";
    //      _sql_cmd.Parameters.Add("@pMachineTicketNumber", SqlDbType.Int).SourceColumn = "MachineTicketNumber";
    //      _sql_cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt, 0, "TicketId").Direction = ParameterDirection.Output;


    //      //_sql_cmd.Parameters.Add("@pTicketCreatedAccountID", SqlDbType.BigInt).Value = this.CreatedAccountID;
    //      //if (this.PromotionID != 0)
    //      //{
    //      //  _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = this.PromotionID;
    //      //}
    //      //else
    //      //{
    //      //  _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = DBNull.Value;
    //      //}



    //      //if (this.CreatedPlaySessionId != 0)
    //      //{
    //      //  _sql_cmd.Parameters.Add("@pCreatedPlaySessionId", SqlDbType.BigInt).Value = this.CreatedPlaySessionId;
    //      //}
    //      //else
    //      //{
    //      //  _sql_cmd.Parameters.Add("@pCreatedPlaySessionId", SqlDbType.BigInt).Value = DBNull.Value;
    //      //}


    //      using (SqlDataAdapter _sql_da = new SqlDataAdapter())
    //      {
    //        _sql_da.InsertCommand = _sql_cmd;
    //        _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

    //        _sql_da.UpdateBatchSize = 500;
    //        _nr = _sql_da.Update(TicketsCreated);

    //        if (_nr != TicketsCreated.Rows.Count)
    //        {
    //          Log.Error("DB_CreateTicketBatch. Tickets not inserted.");

    //          return false;
    //        }
    //      }

    //      return true;
    //    }
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);

    //    return false;
    //  }

    //}  // DB_CreateTicketBatch

    //------------------------------------------------------------------------------
    // PURPOSE: Create a random out ticket for the tito simulator
    // 
    //  PARAMS:
    //      - INPUT:
    //          - ValidationNumber
    //          - ValidationType
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: 
    // 
    //   NOTES:
    //
    //public Boolean CreateRandomTicketFromSystemId(Int32 SystemId, Int32 ValidationType, Int64 SequenceId)
    //{
    //  Int64 _randomValidationNumber;
    //  Random _random;

    //  _random = new Random((int)DateTime.Now.Ticks);

    //  _randomValidationNumber = _random.Next(100000, 999999);

    //  ValidationNumber = ValidationNumberManager.GetCalculatedValidationNumber(SystemId, _randomValidationNumber);

    //  return true;
    //}

    //public static Boolean DB_DiscardTicket(Int64 TicketId, Int64 OperationId, SqlTransaction Trx)
    //{
    //  StringBuilder _sb;

    //  try
    //  {
    //    _sb = new StringBuilder();
    //    _sb.AppendLine(" UPDATE   TICKETS ");
    //    _sb.AppendLine("    SET   TI_STATUS                    = @pTicketStatusDiscarded ");
    //    _sb.AppendLine("        , TI_LAST_ACTION_DATETIME      = @pModifiedTime ");
    //    _sb.AppendLine("  WHERE   TI_TICKET_ID                 = @pTicketId ");
    //    _sb.AppendLine("    AND   TI_TRANSACTION_ID            = @pTicketOperationId ");
    //    _sb.AppendLine("    AND   TI_STATUS                    = @pTicketStatusValid ");

    //    using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
    //    {
    //      _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
    //      _cmd.Parameters.Add("@pTicketStatusValid", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID;
    //      _cmd.Parameters.Add("@pTicketStatusDiscarded", SqlDbType.Int).Value = TITO_TICKET_STATUS.DISCARDED;
    //      _cmd.Parameters.Add("@pModifiedTime", SqlDbType.DateTime).Value = WGDB.Now;
    //      _cmd.Parameters.Add("@pTicketOperationId", SqlDbType.BigInt).Value = OperationId;


    //      return (_cmd.ExecuteNonQuery() == 1);
    //    }
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);
    //  }

    //  return false;
    //} // DB_CancelTicket

    //------------------------------------------------------------------------------
    // PURPOSE: Internal function to change ticket status.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int64: TicketId
    //        - TITO_STATES: New ticket Status
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean DB_UpdateTicket(Ticket Ticket, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE   TICKETS                                             ");
      _sb.AppendLine("    SET   TI_STATUS                      = @pStatus           ");
      _sb.AppendLine("        , TI_LAST_ACTION_DATETIME        = @pDateTime         ");
      _sb.AppendLine("        , TI_LAST_ACTION_TERMINAL_ID     = @pTerminalId       ");
      _sb.AppendLine("        , TI_LAST_ACTION_TERMINAL_TYPE   = @pTerminalType     ");
      _sb.AppendLine("        , TI_LAST_ACTION_ACCOUNT_ID      = @pAccountId        ");
      _sb.AppendLine("        , TI_TRANSACTION_ID              = @pTransanctionId   ");

      // DHA 12-FEB-2014: Only updates the collectionId if it is not 0
      if (Ticket.MoneyCollectionID != 0 && (Ticket.Status == TITO_TICKET_STATUS.REDEEMED || Ticket.Status == TITO_TICKET_STATUS.DISCARDED || Ticket.Status == TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION))
      {
        _sb.AppendLine("      , TI_MONEY_COLLECTION_ID         = @pMoneyCollectionId");
      }
      if (Ticket.Status == TITO_TICKET_STATUS.REDEEMED || (Ticket.TicketType == TITO_TICKET_TYPE.CHIPS_DEALER_COPY && Ticket.Status == TITO_TICKET_STATUS.CANCELED))
      {
        _sb.AppendLine("       , TI_AMOUNT = @pAmount ");
        _sb.AppendLine("       , TI_AMT1 = @pAmt_1 ");
        _sb.AppendLine("       , TI_CUR1 = @pCur_1 ");
      }
      _sb.AppendLine("  WHERE   TI_TICKET_ID = @pTicketId                           ");

      try
      {
        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = Ticket.TicketID;
          _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = Ticket.Status;
          _sql_command.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = Ticket.LastActionDateTime;
          _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Ticket.LastActionTerminalID;
          _sql_command.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = Ticket.LastActionTerminalType;
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Ticket.LastActionAccountID;
          _sql_command.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = Ticket.MoneyCollectionID;
          _sql_command.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = Ticket.Amt_1;
          _sql_command.Parameters.Add("@pAmt_1", SqlDbType.Decimal).Value = Ticket.Amt_1;
          _sql_command.Parameters.Add("@pCur_1", SqlDbType.NVarChar).Value = Ticket.Cur_1;
          _sql_command.Parameters.Add("@pTransanctionId", SqlDbType.BigInt).Value = Ticket.TransactionId;
          if (_sql_command.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // DB_UpdateTicket

    public static Boolean Cashier_TicketDiscard(Ticket Ticket, Int32 TerminalId, Int64 AccountId, SqlTransaction SqlTrx)
    {
      if (Ticket.TicketID < 1)
      {
        return false;
      }

      Ticket.Status = TITO_TICKET_STATUS.DISCARDED;
      Ticket.LastActionDateTime = WGDB.Now;
      Ticket.LastActionTerminalID = TerminalId;
      Ticket.LastActionTerminalType = (Int32)TITO_TERMINAL_TYPE.CASHIER;
      Ticket.LastActionAccountID = AccountId;

      if (!Ticket.DB_UpdateTicket(Ticket, SqlTrx))
      {
        return false;
      }

      return true;
    }

    public static Boolean Cashier_ValidateCanceledTicket(Ticket Ticket, Int32 TerminalId, Int64 AccountId, CashierSessionInfo CashierSessionInfo, out ArrayList VoucherList, SqlTransaction SqlTrx)
    {
      CashierMovementsTable _cm_mov_table;
      VoucherTitoTicketOperation _voucher_temp;
      List<VoucherValidationNumber> _voucher_tickets;
      VoucherValidationNumber _voucher_ticket_data;
      Int64 _validation_operation_id;
      CardData _card_data;
      Currency _amount_redeemable;
      Currency _amount_non_redeemable;

      VoucherList = new ArrayList();

      if (Ticket.TicketID <= 0 || Ticket.Status != TITO_TICKET_STATUS.PENDING_CANCEL)
      {
        return false;
      }

      Ticket.Status = TITO_TICKET_STATUS.VALID;
      Ticket.LastActionDateTime = WGDB.Now;
      Ticket.LastActionTerminalID = TerminalId;
      Ticket.LastActionTerminalType = (Int32)TITO_TERMINAL_TYPE.CASHIER;
      Ticket.LastActionAccountID = AccountId;

      if (!Ticket.DB_UpdateTicket(Ticket, SqlTrx))
      {
        return false;
      }

      // Get all card data
      _card_data = new CardData();
      if (!CardData.DB_CardGetAllData(AccountId, _card_data))
      {
        return false;
      }

      if (Ticket.IsRedeemable)
      {
        _amount_redeemable = -Ticket.Amount;
        _amount_non_redeemable = 0;
      }
      else
      {
        _amount_redeemable = 0;
        _amount_non_redeemable = -Ticket.Amount;
      }

      // Insert validation operation
      if (!Operations.DB_InsertOperation(OperationCode.TITO_TICKET_VALIDATION, AccountId, CashierSessionInfo.CashierSessionId, 0, 0, _amount_redeemable, _amount_non_redeemable, 0, 0, string.Empty, out _validation_operation_id, SqlTrx))
      {
        return false;
      }

      _cm_mov_table = new CashierMovementsTable(CashierSessionInfo);
      _cm_mov_table.Add(_validation_operation_id, CASHIER_MOVEMENT.TITO_VALIDATE_TICKET, -Ticket.Amount, AccountId, _card_data.TrackData, "");
      if (!_cm_mov_table.Save(SqlTrx))
      {
        return false;
      }

      // Create voucher
      _voucher_tickets = new List<VoucherValidationNumber>();

      // Set ticket data for voucher
      _voucher_ticket_data = new VoucherValidationNumber();
      _voucher_ticket_data.ValidationNumber = Ticket.ValidationNumber;
      _voucher_ticket_data.Amount = Ticket.Amount;
      _voucher_ticket_data.ValidationType = Ticket.ValidationType;
      _voucher_ticket_data.TicketType = Ticket.TicketType;
      _voucher_tickets.Add(_voucher_ticket_data);

      _voucher_temp = new VoucherTitoTicketOperation(_card_data.VoucherAccountInfo(), OperationCode.TITO_TICKET_VALIDATION, false, _voucher_tickets, PrintMode.Print, SqlTrx);
      _voucher_temp.Save(_validation_operation_id, SqlTrx);
      VoucherList.Add(_voucher_temp);

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Mark tickets as expired. This method is meant to be ran from a worker thread
    // 
    //  PARAMS:
    //      - INPUT:
    //          - ValidationNumber
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: ticket was cancelled successfully
    //      - false: ticket could not be cancelled
    // 
    //   NOTES:
    //
    public static Boolean DB_ForceTicketCancellationByExpiration(Int64 TicketId, CashierSessionInfo _sesion, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      StringBuilder _sb_status;
      bool _allow_expire_tickets_pending_print;
      int _status;

      _sb = new StringBuilder();
      _status = 0;
      _allow_expire_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToExpire", true);

      _sb_status = new StringBuilder();
      _sb.AppendLine("SELECT TI_STATUS FROM TICKETS WHERE TI_TICKET_ID = @pTicketId");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;

          _status = (int)_cmd.ExecuteScalar();
        }
      }
      catch { }

      _sb.AppendLine(" UPDATE   TICKETS                        ");
      _sb.AppendLine("    SET   TI_STATUS                    = @pExpiredStatus  ");
      _sb.AppendLine("        , TI_LAST_ACTION_DATETIME      = GETDATE() ");
      _sb.AppendLine("        , TI_LAST_ACTION_TERMINAL_ID   = @pCashierSystemTerminal ");
      _sb.AppendLine("        , TI_LAST_ACTION_TERMINAL_TYPE = @pCashierType ");
      _sb.AppendLine("  WHERE   TI_TICKET_ID = @pTicketId ");
      _sb.AppendLine("    AND   TI_EXPIRATION_DATETIME <= GETDATE() ");
      _sb.AppendLine("    AND   TI_STATUS IN ( @pTicketStateValid , @pTicketStatePendingPrint, @pTicketStatePendingCancel) ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;

          if (_allow_expire_tickets_pending_print)
            _cmd.Parameters.Add("@pExpiredStatus", SqlDbType.Int).Value = (int)TITO_TICKET_STATUS.EXPIRED;
          else
          {
            if (_status == (int)TITO_TICKET_STATUS.PENDING_PRINT) _cmd.Parameters.Add("@pExpiredStatus", SqlDbType.Int).Value = (int)TITO_TICKET_STATUS.DISCARDED;
            else _cmd.Parameters.Add("@pExpiredStatus", SqlDbType.Int).Value = (int)TITO_TICKET_STATUS.EXPIRED;
          }
          _cmd.Parameters.Add("@pCashierSystemTerminal", SqlDbType.BigInt).Value = _sesion.TerminalId;
          _cmd.Parameters.Add("@pCashierType", SqlDbType.Int).Value = (int)TITO_TERMINAL_TYPE.CASHIER;
          _cmd.Parameters.Add("@pTicketStateValid", SqlDbType.Int).Value = (int)TITO_TICKET_STATUS.VALID;
          _cmd.Parameters.Add("@pTicketStatePendingPrint", SqlDbType.Int).Value = (int)TITO_TICKET_STATUS.PENDING_PRINT;
          _cmd.Parameters.Add("@pTicketStatePendingCancel", SqlDbType.Int).Value = (int)TITO_TICKET_STATUS.PENDING_CANCEL;

          return _cmd.ExecuteNonQuery() == 1;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_ForceTicketCancellationByExpiration 

    //------------------------------------------------------------------------------
    // PURPOSE: Mark tickets as discarded. This method is meant to be ran from a worker thread
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TicketId
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: ticket was discarded successfully
    //      - false: ticket could not be discarded
    // 
    //   NOTES:
    //
    public static Boolean DB_ForceOfflineTicketDiscard(Int64 TicketId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   TICKETS                        ");
      _sb.AppendLine("    SET   TI_STATUS    = @pDiscardedStatus  ");
      _sb.AppendLine("  WHERE   TI_TICKET_ID = @pTicketId ");
      _sb.AppendLine("    AND   (TI_STATUS    = @pTicketStateValid ");
      _sb.AppendLine("    or   TI_STATUS    = @pTicketStatePendingPrint) ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
          _cmd.Parameters.Add("@pDiscardedStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.DISCARDED;
          _cmd.Parameters.Add("@pTicketStateValid", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID;
          _cmd.Parameters.Add("@pTicketStatePendingPrint", SqlDbType.Int).Value = TITO_TICKET_STATUS.PENDING_PRINT;

          return _cmd.ExecuteNonQuery() == 1;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_ForceOfflineTicketDiscard

    //------------------------------------------------------------------------------
    // PURPOSE: Set MoneyCollection id to a Ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TicketId
    //          - MoneyCollectionId
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: ticket was updated successfully
    //      - false: ticket could not be updated
    // 
    //   NOTES:
    //
    public static Boolean DB_ForceOfflineTicketMoneyCollection(Int64 TicketId, Int64 MoneyCollectionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   TICKETS                        ");
      _sb.AppendLine("    SET   TI_MONEY_COLLECTION_ID    = @pMoneyCollectionId  ");
      _sb.AppendLine("  WHERE   TI_TICKET_ID = @pTicketId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
          _cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;

          return _cmd.ExecuteNonQuery() == 1;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_ForceOfflineTicketDiscard

    //------------------------------------------------------------------------------
    // PURPOSE: Set CollectedMoneyCollection and CageMovement id to a Ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CageMovementId
    //          - SessionInfo
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: ticket was updated successfully
    //      - false: ticket could not be updated
    // 
    //   NOTES:
    //
    public static Boolean DB_UpdateCollectedTickets(Int64 TicketId, Int64 CageMovementId, SqlTransaction _sql_trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("UPDATE   TICKETS                                                  ");
      _sb.AppendLine("   SET   TI_COLLECTED_MONEY_COLLECTION = TI_MONEY_COLLECTION_ID,  ");
      _sb.AppendLine("         TI_CAGE_MOVEMENT_ID = @pCageMovementId                   ");
      _sb.AppendLine(" WHERE   TI_TICKET_ID = @pTicketId                                ");
      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx))
        {
          _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
          _cmd.Parameters.Add("@pCageMovementId", SqlDbType.BigInt).Value = CageMovementId;

          if (_cmd.ExecuteNonQuery() > 0)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get the count of tickets
    //
    //  PARAMS :
    //      - INPUT :
    //                - NumTickets
    //                - SessionInfo
    //                - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - None
    //
    //   NOTES :
    //
    public static Boolean GetNumTicketsForSession(CashierSessionInfo SessionInfo, out Int32 NumTickets, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      NumTickets = -1;
      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   COUNT(TI_TICKET_ID) ");
      _sb.AppendLine("       FROM   TICKETS ");
      _sb.AppendLine(" INNER JOIN   MONEY_COLLECTIONS ");
      _sb.AppendLine("         ON   TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID ");
      _sb.AppendLine("      WHERE   MC_CASHIER_SESSION_ID = @pSessionId ");
      _sb.AppendLine("        AND   (TI_COLLECTED_MONEY_COLLECTION IS NULL OR TI_COLLECTED_MONEY_COLLECTION <> MC_COLLECTION_ID) ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionInfo.CashierSessionId;
          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            NumTickets = (Int32)_obj;

            return true;
          }
        }

        if (NumTickets < 0)
        {
          Log.Message("GetNumTicketsForSession: Couldn't recover the count of tickets.");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetNumTicketsForSession

    //------------------------------------------------------------------------------
    // PURPOSE: Get a List of tickets for SessionId
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SessionInfo
    //          - TicketsIdList
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: ticket was updated successfully
    //      - false: ticket could not be updated
    // 
    //   NOTES:
    //
    public static Boolean GetTicketsByCashierSessionId(CashierSessionInfo SessionInfo, out List<Int64> TicketsIdList, SqlTransaction Trx)
    {
      StringBuilder _sb;

      TicketsIdList = new List<Int64>();

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   T.TI_TICKET_ID                                  ");
      _sb.AppendLine("  FROM   MONEY_COLLECTIONS MC                            ");
      _sb.AppendLine(" INNER   JOIN TICKETS T                                  ");
      _sb.AppendLine("    ON   MC.MC_COLLECTION_ID = T.TI_MONEY_COLLECTION_ID  ");
      _sb.AppendLine(" WHERE   MC.MC_CASHIER_SESSION_ID = @pSessionId          ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionInfo.CashierSessionId;

          using (SqlDataReader _sql_rdr = _sql_cmd.ExecuteReader())
          {
            while (_sql_rdr.Read())
            {
              TicketsIdList.Add(_sql_rdr.GetInt64(0));
            }
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: It checks for existing ticket in data base.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - A not null SqlTransaction
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: ticket already exists
    //      - false: ticket does not exist
    // 
    //   NOTES:
    //
    public Boolean DB_IsTicketAlreadyCreated(SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      // FJC 07-10-2016. (PBI 18258). We change Order in Where criteria because doesn't match with order in SQL Index previously created 
      // (IX_ti_transaction_id_terminal_id_validation_number)
      _sb.AppendLine("SELECT   COUNT(*)                                     ");
      _sb.AppendLine("  FROM   TICKETS                                      ");
      _sb.AppendLine(" WHERE   TI_TRANSACTION_ID      = @pTransactionId     ");
      _sb.AppendLine("   AND   TI_CREATED_TERMINAL_ID = @pCreatedTerminalId ");
      _sb.AppendLine("   AND   TI_VALIDATION_NUMBER   = @pValidationNumber  ");


      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pCreatedTerminalId", SqlDbType.Int).Value = this.CreatedTerminalId;
        _cmd.Parameters.Add("@pValidationNumber", SqlDbType.BigInt).Value = this.ValidationNumber;
        _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = this.TransactionId;

        return (Int32)_cmd.ExecuteScalar() > 0;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: It returns all the tickets from an operation.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Operation Id
    //          - A not null SqlTransaction
    //
    //      - OUTPUT:
    //          - List of tickets
    //
    // RETURNS:
    //      - true: ticket already exists
    //      - false: ticket does not exist
    // 
    //   NOTES:
    //
    public static Boolean DB_GetTicketsFromOperation(Int64 OperationId, out List<Int64> TicketList)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_GetTicketsFromOperation(OperationId, out TicketList, _db_trx.SqlTransaction);
      }
    }
    public static Boolean DB_GetTicketsFromOperation(Int64 OperationId, out List<Int64> TicketList, SqlTransaction Trx)
    {
      StringBuilder _sb;

      TicketList = new List<Int64>();

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT TI_TICKET_ID                      ");
      _sb.AppendLine("   FROM TICKETS WITH (INDEX (IX_ti_transaction_id_terminal_id_validation_number))");
      _sb.AppendLine("  WHERE TI_TRANSACTION_ID = @pOpId          ");
      _sb.AppendLine("    AND TI_CREATED_TERMINAL_TYPE = @pTerminalType               ");

      try
      {

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOpId", SqlDbType.BigInt).Value = OperationId;
          _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TITO_TERMINAL_TYPE.CASHIER;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              TicketList.Add(_reader.GetInt64(0));
            }

            _reader.Close();
          }
        }
      }
      catch (Exception)
      {
        return false;
      }

      return true;
    }
    // --------------------------

    //------------------------------------------------------------------------------
    // PURPOSE: Load tickets related to handpay
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - Loaded ticket or NULL
    //


    //public static Boolean LoadTicketsForHandPay(Int32 TerminalId,
    //                                           Int64 TransactionId,
    //                                           Decimal HandPayAmount,
    //                                           TITO_TICKET_STATUS TicketStatus,
    //                                           TITO_TICKET_TYPE TitoTicketType,
    //                                           out DataTable TicketsTable)
    //{
    //  using (DB_TRX _db_trx = new DB_TRX())
    //  {
    //    return LoadTicketsForHandPay(TerminalId,
    //                                 TransactionId,
    //                                 HandPayAmount,
    //                                 TicketStatus,
    //                                 TitoTicketType,
    //                                 out TicketsTable,
    //                                 _db_trx.SqlTransaction);
    //  }
    //} // LoadTicketsForHandPay

    public static Boolean LoadTicketsForHandPay(Int32 TerminalId,
                                               Int64 TransactionId,
                                               Decimal HandPayAmount,
                                               TITO_TICKET_STATUS TicketStatus,
                                               TITO_TICKET_TYPE TitoTicketType,
                                               out DataTable TicketsTable,
                                               SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      TicketsTable = new DataTable();

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   TI_TICKET_ID                             ");
      _sb.AppendLine("  FROM   TICKETS WITH (INDEX (IX_ti_transaction_id_terminal_id_validation_number)) ");
      _sb.AppendLine(" WHERE   TI_CREATED_TERMINAL_ID = @pTerminalID    ");
      _sb.AppendLine("   AND   TI_TRANSACTION_ID      = @pTransactionID ");
      _sb.AppendLine("   AND   TI_AMOUNT              = @pHandpayAmount ");
      _sb.AppendLine("   AND   TI_STATUS              = @pTicketStatus  ");
      _sb.AppendLine("   AND   TI_TYPE_ID             = @pTicketType    ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pTransactionID", SqlDbType.BigInt).Value = TransactionId;
          _cmd.Parameters.Add("@pHandPayAmount", SqlDbType.Decimal).Value = HandPayAmount;
          _cmd.Parameters.Add("@pTicketStatus", SqlDbType.Int).Value = TicketStatus;
          _cmd.Parameters.Add("@pTicketType", SqlDbType.Int).Value = TitoTicketType;

          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _da.Fill(TicketsTable);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // LoadTicketsForHandPay

    // --------------------------

    //------------------------------------------------------------------------------
    // PURPOSE: Load entire ticket data, if we know its ValidationNumber
    //          Beacuse ValidationNumber isn't unique, query selects the newest
    //          ticket with that number
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - Loaded ticket or NULL
    //

    public static Ticket LoadTicket(Int64 TicketId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return LoadTicket(TicketId, _db_trx.SqlTransaction, false);
      }
    } // LoadTicket

    public static Ticket LoadTicket(Int64 TicketId, SqlTransaction SqlTrx, Boolean IsApplyTax)
    {
      Ticket _ticket;
      DataTable _tickets;
      Boolean _error;
      DataRow _row;

      _ticket = new Ticket();
      _tickets = new DataTable();
      _error = false;

      if (!LoadTicketsByCondition(" TI_TICKET_ID = " + TicketId, ref _tickets, SqlTrx) || _tickets.Rows.Count != 1)
      {
        _error = true;
      }

      if (!_error)
      {
        _row = _tickets.Rows[0];

        _ticket.TicketID = (Int64)_row["TI_TICKET_ID"];
        _ticket.ValidationNumber = (Int64)_row["TI_VALIDATION_NUMBER"];
        _ticket.ValidationType = (TITO_VALIDATION_TYPE)_row["TI_VALIDATION_TYPE"];
        _ticket.Amount = (Decimal)_row["TI_AMOUNT"];
        _ticket.Status = (TITO_TICKET_STATUS)_row["TI_STATUS"];
        _ticket.TicketType = (TITO_TICKET_TYPE)_row["TI_TYPE_ID"];
        _ticket.CreatedDateTime = (DateTime)_row["TI_CREATED_DATETIME"];
        _ticket.CreatedTerminalId = (Int32)_row["TI_CREATED_TERMINAL_ID"];
        _ticket.CreatedTerminalName = (String)_row["TERMINAL_NAME"];
        _ticket.CreatedTerminalType = (Int32)_row["TI_CREATED_TERMINAL_TYPE"];
        //  _ticket.CreatedTerminalTypeName = (String)_row["TERMINAL_TYPE_NAME"];
        _ticket.TransactionId = (Int64)_row["TI_TRANSACTION_ID"];
        _ticket.CreatedPlaySessionId = (Int64)_row["TI_CREATED_PLAY_SESSION_ID"];
        if (_row["TI_EXPIRATION_DATETIME"] != null && _row["TI_EXPIRATION_DATETIME"] != DBNull.Value)
        {
          _ticket.ExpirationDateTime = (DateTime)_row["TI_EXPIRATION_DATETIME"];
        }
        _ticket.MoneyCollectionID = (Int64)_row["MONEY_COLLECTION"];
        _ticket.CreatedAccountID = (Int64)_row["CREATED_ACCOUNT_ID"];
        _ticket.LastActionTerminalID = (Int32)_row["LAST_TERMINAL_ID"];
        _ticket.LastActionTerminalName = (String)_row["LAST_TERMINAL_NAME"];
        if (_row["TI_LAST_ACTION_DATETIME"] != null && _row["TI_LAST_ACTION_DATETIME"] != DBNull.Value)
        {
          _ticket.LastActionDateTime = (DateTime)_row["TI_LAST_ACTION_DATETIME"];
        }
        _ticket.LastActionAccountID = (Int64)_row["LAST_ACCOUNT_ID"];
        _ticket.LastActionTerminalType = (Int32)_row["LAST_TERMINAL_TYPE"];
        //   _ticket.LastActionTerminalTypeName = (String)_row["LAST_TERMINAL_TYPE_NAME"];
        _ticket.PromotionID = (Int64)_row["PROMOTION"];
        _ticket.CollectedMoneyCollection = (Int64)_row["COLLECTED"];
        _ticket.MachineTicketNumber = (Int32)_row["TI_MACHINE_NUMBER"];
        _ticket.Amt_0 = (Decimal)_row["TI_AMT0"];
        _ticket.Cur_0 = (String)_row["TI_CUR0"];
        _ticket.Amt_1 = (Decimal)_row["TI_AMT1"];
        _ticket.Cur_1 = (String)_row["TI_CUR1"];
      }

      return _ticket;
    }

    public static Boolean LoadTicketsByCondition(String Condition, ref DataTable TicketsTable, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      TicketsTable = new DataTable();

      _sb = new StringBuilder();
      _sb.AppendLine("   SELECT   TI_TICKET_ID");
      _sb.AppendLine("          , TI_VALIDATION_NUMBER");
      _sb.AppendLine("          , dbo.ApplyExchange2((CASE WHEN TI_AMT0 is null or TI_AMT0 = 0 THEN TI_AMOUNT ELSE TI_AMT0 END), ISNULL(TI_CUR0, @pNatCur), @pNatCur) as TI_AMOUNT");  //LTC 18-JUL-2016
      _sb.AppendLine("          , TI_STATUS");
      _sb.AppendLine("          , TI_TYPE_ID");
      _sb.AppendLine("          , TI_CREATED_DATETIME");
      _sb.AppendLine("          , TI_CREATED_TERMINAL_ID");
      _sb.AppendLine("          , ISNULL(CASE WHEN TI_CREATED_TERMINAL_TYPE IN (0, 2) THEN CT.CT_NAME ELSE TE.TE_NAME END, '') TERMINAL_NAME");
      _sb.AppendLine("          , TI_CREATED_TERMINAL_TYPE");
      _sb.AppendLine("          , ISNULL(TT.TT_NAME, '') TERMINAL_TYPE_NAME");
      _sb.AppendLine("          , TI_EXPIRATION_DATETIME");
      _sb.AppendLine("          , ISNULL(TI_MONEY_COLLECTION_ID, 0) MONEY_COLLECTION");
      _sb.AppendLine("          , ISNULL(TI_CREATED_ACCOUNT_ID, 0) CREATED_ACCOUNT_ID");
      _sb.AppendLine("          , ISNULL(TI_LAST_ACTION_TERMINAL_ID, 0) LAST_TERMINAL_ID");
      _sb.AppendLine("          , ISNULL(CASE WHEN TI_LAST_ACTION_TERMINAL_TYPE = 0 THEN CT2.CT_NAME ELSE TE2.TE_NAME END, '') LAST_TERMINAL_NAME");
      _sb.AppendLine("          , TI_LAST_ACTION_DATETIME");
      _sb.AppendLine("          , ISNULL(TI_LAST_ACTION_ACCOUNT_ID, 0) LAST_ACCOUNT_ID");
      _sb.AppendLine("          , ISNULL(TI_LAST_ACTION_TERMINAL_TYPE, 0) LAST_TERMINAL_TYPE");
      _sb.AppendLine("          , ISNULL(TT2.TT_NAME, '') LAST_TERMINAL_TYPE_NAME");
      _sb.AppendLine("          , ISNULL(TI_PROMOTION_ID, 0) PROMOTION");
      _sb.AppendLine("          , ISNULL(TI_COLLECTED_MONEY_COLLECTION, 0) COLLECTED");
      _sb.AppendLine("          , TI_VALIDATION_TYPE ");
      _sb.AppendLine("          , ISNULL(TI_MACHINE_NUMBER, 0) TI_MACHINE_NUMBER ");
      _sb.AppendLine("          , ISNULL(TI_TRANSACTION_ID, 0) TI_TRANSACTION_ID     ");
      _sb.AppendLine("          , ISNULL(TI_CREATED_PLAY_SESSION_ID, 0) TI_CREATED_PLAY_SESSION_ID ");
      _sb.AppendLine("          , isNull(TI_AMT0, TI_AMOUNT) as TI_AMT0 ");
      _sb.AppendLine("          , isNull(TI_CUR0, @pNatCur) as TI_CUR0 ");
      _sb.AppendLine("          , isNull(TI_AMT1, dbo.ApplyExchange2(ISNULL(TI_AMT0, TI_AMOUNT), ISNULL(TI_CUR0, @pNatCur), @pNatCur) ) as TI_AMT1 ");
      _sb.AppendLine("          , isNull(TI_CUR1, @pNatCur) as TI_CUR1 ");
      _sb.AppendLine("          , ISNULL(TI_COLLECTED, 0) TI_COLLECTED");
      _sb.AppendLine("     FROM   TICKETS TI");
      _sb.AppendLine("LEFT JOIN   TERMINALS TE ON (TE.TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID)");
      _sb.AppendLine("LEFT JOIN   CASHIER_TERMINALS CT ON (CT.CT_CASHIER_ID = TI_CREATED_TERMINAL_ID)");
      _sb.AppendLine("LEFT JOIN   TERMINAL_TYPES TT ON (TT.TT_TYPE = TI_CREATED_TERMINAL_TYPE)");
      _sb.AppendLine("LEFT JOIN   TERMINALS TE2 ON (TE2.TE_TERMINAL_ID = TI_LAST_ACTION_TERMINAL_ID)");
      _sb.AppendLine("LEFT JOIN   CASHIER_TERMINALS CT2 ON (CT2.CT_CASHIER_ID = TI_LAST_ACTION_TERMINAL_ID)");
      _sb.AppendLine("LEFT JOIN   TERMINAL_TYPES TT2 ON (TT2.TT_TYPE = TI_LAST_ACTION_TERMINAL_TYPE)");
      if (!String.IsNullOrEmpty(Condition))
      {
        _sb.AppendLine("    WHERE   " + Condition);
      }
      _sb.AppendLine(" ORDER BY   TI_CREATED_DATETIME DESC ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pNatCur", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency();
          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _da.Fill(TicketsTable);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    private void GetAccountPromotionAndExpirationDate(Int64 TerminalId, out Int64 AccountPromotionId, out DateTime ExpirationDate, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int64 _promotionid;

      _sb = new StringBuilder();
      ExpirationDate = DateTime.MinValue;
      AccountPromotionId = 0;

      _sb.AppendLine("    SELECT   TE_ACCOUNT_PROMOTION_ID                                         ");
      _sb.AppendLine("           , ACP_EXPIRATION                                                  ");
      _sb.AppendLine("           , ACP_PROMO_ID                                                    ");
      _sb.AppendLine("      FROM   TERMINALS                                                       ");
      _sb.AppendLine("INNER JOIN   ACCOUNT_PROMOTIONS ON TE_ACCOUNT_PROMOTION_ID = ACP_UNIQUE_ID   ");
      _sb.AppendLine("     WHERE   TE_TERMINAL_ID = @pCreatedTerminalId                            ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _sql_cmd.Parameters.Add("@pCreatedTerminalId", SqlDbType.BigInt).Value = TerminalId;

        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            AccountPromotionId = _reader.GetInt64(0);
            _promotionid = _reader.GetInt64(2);
            if (_reader.IsDBNull(1))
            {
              ExpirationDate = Misc.TodayOpening().AddDays(1);
              Log.Warning("ACP_PROMO_ID " + _promotionid.ToString() + " DOESNT EXPIRE, TICKET EXPIRATION SET TO 1 DAY");
            }
            else
            {
              ExpirationDate = _reader.GetDateTime(1);
            }
          }
        }
      }
    } // GetAccountPromotionAndExpirationDate


    /// <summary>
    /// Insert a record into TICKETS_AUDIT_STATUS_CHANGE
    /// </summary>
    /// <param name="ValidationNumber"></param>
    /// <param name="RejectReason"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_InsertTicketsAuditStatusChange(Int64 ValidationNumber,
                                                             ENUM_TITO_REDEEM_TICKET_TRANS_STATUS MachineStatus,
                                                             ENUM_TITO_IS_VALID_TICKET_MSG ValidateStatus,
                                                             SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" INSERT INTO TICKETS_AUDIT_STATUS_CHANGE  ");
        _sb.AppendLine("             (                            ");
        _sb.AppendLine("               TIA_INSERT_DATE            ");
        _sb.AppendLine("             , TIA_TICKET_ID              ");
        _sb.AppendLine("             , TIA_VALIDATION_NUMBER      ");
        _sb.AppendLine("             , TIA_STATUS                 ");
        _sb.AppendLine("             , TIA_REJECT_REASON_EGM      ");
        _sb.AppendLine("             , TIA_REJECT_REASON_WCP      ");
        _sb.AppendLine("             )                            ");
        _sb.AppendLine("     VALUES  (                            ");
        _sb.AppendLine("               GETDATE()                  ");
        _sb.AppendLine("             , 0                          ");
        _sb.AppendLine("             , @pTiValidationNumber       ");
        _sb.AppendLine("             , @pTiStatus                 ");
        _sb.AppendLine("             , @pMachineStatus            ");
        _sb.AppendLine("             , @pValidateStatus)          ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTiValidationNumber", SqlDbType.BigInt).Value = ValidationNumber;
          _sql_cmd.Parameters.Add("@pTiStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.DISCARDED;
          _sql_cmd.Parameters.Add("@pMachineStatus", SqlDbType.BigInt).Value = MachineStatus;
          _sql_cmd.Parameters.Add("@pValidateStatus", SqlDbType.BigInt).Value = ValidateStatus;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_InsertTicketsAuditStatusChange

    /// <summary>
    /// Update Reject Reason from a ticket
    /// </summary>
    /// <param name="TicketId"></param>
    /// <param name="MachineStatus"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_UpdateTicketRejectReason(Int64 TicketId,
                                                       ENUM_TITO_REDEEM_TICKET_TRANS_STATUS MachineStatus,
                                                       ENUM_TITO_IS_VALID_TICKET_MSG ValidateStatus,
                                                       SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("  UPDATE                                                ");
        _sb.AppendLine(" TICKETS                                                ");
        _sb.AppendLine("     SET  TI_REJECT_REASON_EGM  = @pMachineStatus       ");
        _sb.AppendLine("        , TI_REJECT_REASON_WCP  = @pValidateStatus      ");
        _sb.AppendLine("   WHERE  TI_TICKET_ID          = @pTicketId            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
          _sql_cmd.Parameters.Add("@pMachineStatus", SqlDbType.BigInt).Value = (Int64)MachineStatus;
          _sql_cmd.Parameters.Add("@pValidateStatus", SqlDbType.BigInt).Value = (Int64)ValidateStatus;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateTicketRejectReason


  }
}
