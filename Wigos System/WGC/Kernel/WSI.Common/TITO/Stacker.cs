//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Stacker.cs
// 
//   DESCRIPTION: Stacker class
// 
//        AUTHOR: Javier Fernández
// 
// CREATION DATE: 22-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-OCT-2013 JFC    First version. 
// 11-NOV-2013 NMR    Using ExecuteNonQuery instead ExecuteReader
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common.TITO
{
  public class Stacker
  {
    #region Private fields

    private Int64 m_stackerId;
    private String m_model;
    private Int32 m_inserted;
    private Int32 m_providerPreassigned;
    private Int32 m_terminalPreassigned;
    private String m_notes;
    private TITO_STACKER_STATUS m_status;

    # endregion

    #region Properties

    public Int64 StackerId
    {
      get { return this.m_stackerId; }
      set { this.m_stackerId = value; }
    }

    public String Model
    {
      get { return this.m_model; }
      set { this.m_model = value; }
    }

    public Int32 InsertedTerminalId
    {
      get { return this.m_inserted; }
      set { this.m_inserted = value; }
    }

    public Int32 ProviderPreassigned
    {
      get { return this.m_providerPreassigned; }
      set { this.m_providerPreassigned = value; }
    }

    public Int32 TerminalPreassigned
    {
      get { return this.m_terminalPreassigned; }
      set { this.m_terminalPreassigned = value; }
    }

    public String Notes
    {
      get { return this.m_notes; }
      set { this.m_notes = value; }
    }

    public TITO_STACKER_STATUS Status
    {
      get { return this.m_status; }
      set { this.m_status = value; }
    }

    # endregion

    # region Constructors
    # endregion

    # region Read methods

    //------------------------------------------------------------------------------
    // PURPOSE: Populate Stacker object.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Stacker id
    //          - Transaction
    //
    //      - OUTPUT:
    //          - 
    //
    // RETURNS:
    //      - Populated Stacker object.
    // 
    //   NOTES:
    //
    public static Stacker DB_Read(Int64 StackerId,
                         SqlTransaction Trx)
    {
      Stacker result;

      if (StackerId <= 0)
      {
        return null;
      }

      SqlCommand _sql_cmd;
      SqlDataReader _reader;
      StringBuilder _sb;

      result = null;
      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   ST_STACKER_ID               ");
      _sb.AppendLine("       , ST_MODEL                    ");
      _sb.AppendLine("       , ST_INSERTED                 ");
      _sb.AppendLine("       , ST_PROVIDER_PREASSIGNED     ");
      _sb.AppendLine("       , ST_TERMINAL_PREASSIGNED     ");
      _sb.AppendLine("       , ST_NOTES                    ");
      _sb.AppendLine("       , ST_STATUS                   ");
      _sb.AppendLine("  FROM   STACKERS                    ");
      _sb.AppendLine(" WHERE   ST_STACKER_ID = @pStackerId ");

      _sql_cmd = new SqlCommand(_sb.ToString());
      _sql_cmd.Connection = Trx.Connection;
      _sql_cmd.Transaction = Trx;

      _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = StackerId;

      _reader = null;

      try
      {
        _reader = _sql_cmd.ExecuteReader();

        if (_reader.Read())
        {
          result = new Stacker();
          result.StackerId = _reader.IsDBNull(0) ? 0 : Convert.ToInt64(_reader[0]);
          result.Model = _reader[1].ToString();
          result.InsertedTerminalId = _reader.IsDBNull(2) ? 0 : Convert.ToInt32(_reader[2]);
          result.ProviderPreassigned = _reader.IsDBNull(3) ? 0 : Convert.ToInt32(_reader[3]);
          result.TerminalPreassigned = _reader.IsDBNull(4) ? 0 : Convert.ToInt32(_reader[4]);
          result.Notes = _reader[5].ToString();
          result.Status = _reader.IsDBNull(6) ? TITO_STACKER_STATUS.DISABLED : (TITO_STACKER_STATUS)_reader[6];
        }

        return result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return null;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
          _reader.Dispose();
          _reader = null;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Populate Stacker object.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Terminal Id
    //          - Transaction
    //
    //      - OUTPUT:
    //          - 
    //
    // RETURNS:
    //      - Populated Stacker object.
    // 
    //   NOTES:
    //
    public static Stacker DB_ReadStackerInsertedInTerminal(Int32 TerminalId,
                                                           SqlTransaction Trx)
    {
      Stacker result;

      if (TerminalId <= 0)
      {
        return null;
      }

      SqlCommand _sql_cmd;
      SqlDataReader _reader;
      StringBuilder _sb;

      result = null;
      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   ST_STACKER_ID               ");
      _sb.AppendLine("       , ST_MODEL                    ");
      _sb.AppendLine("       , ST_INSERTED                 ");
      _sb.AppendLine("       , ST_PROVIDER_PREASSIGNED     ");
      _sb.AppendLine("       , ST_TERMINAL_PREASSIGNED     ");
      _sb.AppendLine("       , ST_NOTES                    ");
      _sb.AppendLine("       , ST_STATUS                   ");
      _sb.AppendLine("  FROM   STACKERS                    ");
      _sb.AppendLine(" WHERE   ST_INSERTED = @pTerminalId  ");

      _sql_cmd = new SqlCommand(_sb.ToString());
      _sql_cmd.Connection = Trx.Connection;
      _sql_cmd.Transaction = Trx;

      _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

      _reader = null;

      try
      {
        _reader = _sql_cmd.ExecuteReader();

        if (_reader.Read())
        {
          result = new Stacker();
          result.StackerId = _reader.IsDBNull(0) ? 0 : Convert.ToInt64(_reader[0]);
          result.Model = _reader[1].ToString();
          result.InsertedTerminalId = _reader.IsDBNull(2) ? 0 : Convert.ToInt32(_reader[2]);
          result.ProviderPreassigned = _reader.IsDBNull(3) ? 0 : Convert.ToInt32(_reader[3]);
          result.TerminalPreassigned = _reader.IsDBNull(4) ? 0 : Convert.ToInt32(_reader[4]);
          result.Notes = _reader[5].ToString();
          result.Status = _reader.IsDBNull(6) ? TITO_STACKER_STATUS.DISABLED : (TITO_STACKER_STATUS)_reader[6];
        }

        return result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return null;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
          _reader.Dispose();
          _reader = null;
        }
      }
    } // DB_ReadStackerInsertedInTerminal

    # endregion

    # region Update methods

    //------------------------------------------------------------------------------
    // PURPOSE: Insert stacker in a new terminal
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Terminal id from new terminal
    //          - Transaction
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: stacker state was changed successfully
    //      - false: stacker state could not be changed
    // 
    //   NOTES:
    //
    public Boolean DB_AssignToTerminal(Int32 NewTerminalId,
                              SqlTransaction SqlTrx)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   STACKERS                    ");
      _sb.AppendLine("    SET   ST_INSERTED = @pInserted    ");
      _sb.AppendLine("  WHERE   ST_STACKER_ID = @pStackerId ");

      _sql_cmd = new SqlCommand(_sb.ToString());
      _sql_cmd.Connection = SqlTrx.Connection;
      _sql_cmd.Transaction = SqlTrx;


      _sql_cmd.Parameters.Add("@pInserted", SqlDbType.Int).Value = NewTerminalId;
      _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = this.StackerId;

      this.InsertedTerminalId = NewTerminalId;

      try
      {
        return _sql_cmd.ExecuteNonQuery() > 0;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Remove stacker from terminal
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Transaction
    //
    //      - OUTPUT:
    //          -
    //
    // RETURNS:
    //      - true: stacker state was changed successfully
    //      - false: stacker state could not be changed
    // 
    //   NOTES:
    //
    public static Boolean DB_RemoveFromTerminal(Int64 PStackerId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   STACKERS                    ");
        _sb.AppendLine("    SET   ST_INSERTED =   @pInserted  ");
        _sb.AppendLine("  WHERE   ST_STACKER_ID = @pStackerId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pInserted", SqlDbType.Int).Value = DBNull.Value;
          _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = PStackerId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("DB_RemoveFromTerminal. Stacker not removed from terminal. Stacker id: " + PStackerId.ToString());

            return false;
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    # endregion

    # region Overridden methods

    public override string ToString()
    {
      StringBuilder _result;
      String _separator;
      String _terminal;

      _terminal = this.InsertedTerminalId > 0 ? this.InsertedTerminalId.ToString() : "NULL";

      _separator = ", ";

      _result = new StringBuilder();

      _result.Append(@"{Id: ");
      _result.Append(this.StackerId);
      _result.Append(_separator);

      _result.Append("terminal: ");
      _result.Append(_terminal);
      _result.Append(_separator);

      _result.Append("status: ");
      _result.Append(this.Status);
      _result.Append(@"}");

      return _result.ToString();
    }

    # endregion
  }
}
