//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: OperationUndo.cs 
// 
//   DESCRIPTION: Class with necessary methods to undo an operation
// 
//        AUTHOR: Ignasi Carre�o
// 
// CREATION DATE: 08-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ---------- ----------------------------------------------------------
// 08-OCT-2013 ICS        First version.
// 24-OCT-2013 ICS        Added restriction to don't undo operations with UNR promotions.
// 29-OCT-2013 ICS        Fixed Bug WIG-337: Undone Operations must set 'undo_status' field.
// 30-OCT-2013 ICS        Fixed Bug WIG-346: Negative cash net.
// 04-NOV-2013 ICS        Fixed Bug WIG-367: Incorrect error message undoing last operation.
// 06-NOV-2013 ICS        Fixed Bug WIG-389: Error undoing operations with redeemable promotion credit spent.
// 10-DEC-2013 LJM        Fixed Bug WIG-468: Error undoing operations on Cash Balance.
// 14-JAN-2014 LEM        Fixed Bug WIGOSTITO-973: Error undoing operations with Currency Exchange.
// 16-JAN-2014 ACM        Added Undo Chips Sale/Purchase operations.
// 24-JAN-2014 RRR        Fixed Bug WIG-543: Anulaci�n de venta de fichas no permitida si no hay efectivo en caja.
// 28-JAN-2014 LEM        Include GIFT_DELIVERY, TITO_REISSUE, TITO_OFFLINE as cancellable operations in TITOMode.
// 30-JAN-2014 RCI & ICS  Fixed Bug WIG-557: Incorrect balance in undo chips purchase with check payment.
// 31-JAN-2014 DHA        Added Undo Tickets Tito.
// 18-FEB-2014 DLL        Fixed Bug WIG-642: undo Chips purchase not check chips stock.
// 06-MAR-2014 DHA        Fixed Bug WIGOSTITO-1121: save the voucher in the operation undo.
// 06-MAR-2014 DLL        Fixed Bug WIG-702: undo Chips purchase check chips stock when is not gaming table.
// 08-APR-2014 DLL        Fixed Bug WIG-806: wrong message to undo operation with a great amount.
// 25-APR-2014 DHA & JML  Rename/Added TITO Cashier/Account Movements.
// 28-APR-2014 SMN        Fixed Bug WIG-846: Stock of chips is negative.
// 12-MAY-2014 DLL        Fixed Bug WIG-915: Imposible to undo chips purchase.
// 06-JUN-2014 JAB        Added two decimal characters in denominations fields.
// 10-JUN-2014 DLL        Fixed Bug WIG-WIG-1025: when ShowDenominations are disabled don't check stock control in chips.
// 25-JUN-2014 DLL        Fixed Bug WIGOSTITO-975: Added restriction to don't undo operations with Cash desk draw play sessions.
// 25-JUL-2014 DLL        Chips are moved to a separate table
// 08-AUG-2014 RMS        Added supoort for Specific Bank Card Types
// 14-AUG-2014 RCI        Correct initial/final balance on Card/Check undo operations.
// 18-SEP-2014 DLL        Added new movement Cash Advance
// 02-OCT-2014 JBC        Fixed Bug WIG-1391: Problem with undo operation solved.
// 09-OCT-2014 MPO        Added undo operation HANPAY for undo handpay in TITO.
// 17-OCT-2014 DRV        Fixed Bug WIGOSTITO-1255: Can't undo a Chip Sale operation whith the GP ShowDenomination with value "0"
// 21-OCT-2014 MPO        Fixed Bug WIG-1541: Incorrect handpay total because handpay movement is voided.
// 01-DIC-2014 JBC        Fixed Bug WIG-1542: Undo bank transaction Data
// 09-DIC-2014 OPC        Fixed Bug WIG-1816: base tax negative when undo last operation.
// 15-DEC-2014 JBC, DLL, JPJ    New Company B commission movements
// 17-DIC-2014 JBC        Fixed Bug WIG-1853: UndoOperations cash advance bank transaction
// 21-JAN-2015 OPC        Added new functionality for undo mb recharge.
// 21-JAN-2015 OPC        Added new functionality for undo mb deposit.
// 11-FEB-2015 OPC        Fixed Bug WIG-2033: added update balance when does an recharge from mb.
// 16-MAR-2015 DHA        Insert the operation id to exports to SPACE
// 02-APR-2015 DHA        Avoid to send Chips sale register on table operations to MultiSite
// 08-JUL-2015 DLL        Fixed Bug WIG-2554: error when undo operation in TITO
// 02-SEP-2015 FAV        Fixed Bug TFS-4075: Card cancellation: Error in cash balance
// 23-SEP-2015 YNM        Fixed Bug TFS-5610: It's imposible cancel last sale of chips from the cashier
// 11-NOV-2015 FOS        Product Backlog Item: 3709 Payment tickets & handpays
// 13-JAN-2015 JML        Product Backlog Item 6559: Cashier draw (RE in kind)
// 27-JAN-2016 RAB        Bug 5527:Error en el balance de la cuenta al cancelar una promoci�n y anular la �ltima recarga
// 15-FEB-2016 RAB        Bug 5433:�ltimos movimientos del cajero y de Wigos GUI: en la recarga y posterior anulaci�n se muestra el mismo segundo
// 22-MAR-2016 DDS        Bug 10733: Voiding operation is not correct. Related with bug 5527
// 24-MAR-2016 RAB        Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 13-APR-2016 DHA        Product Backlog Item 9950: added chips operation (sales and purchases)
// 18-APR-2016 JRC        Bug 12174: Use the already opened trx in the methos to avoid posible timeout
// 03-MAY-2016 etp        Product Backlog Item 12653: Cajero: UNDO Currency exchange for multiple denominations (Mallorca)
// 06-JUL-2016 DHA        Product Backlog Item 15064: multicurrency chips sale/purchase
// 01-AUG-2016 RAB        Product Backlog Item 15614: TPV Televisa: Undo last operation on bank card
// 07-SEP-2016 ETP        Fixed Bug 17379: Not allow void recharge if it has cashdeskdraw participation.
// 07-SEP-2016 ETP        Fixed Bug 17349: Pinpad not void recharge if it fails.
// 20-SEP-2016 FJC        Bug 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
// 21-SEP-2016 ETP        Fixed Bug 17846: Reported error messages are incorrect.
// 21-SEP-2016 ETP        Fixed Bug 17897: TPV Televisa: incorrect void of cr�dit card balance.
// 22-SEP-2016 LTC        Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 26-SEP-2016 ETP        Fixed Bug 18021: PinPad - Cashier - Canceling PinPad operations rest from cash balance.
// 27-SEP-2016 RAB        Bug 17875: TPV Televisa: Shortages of balance to cancel a credit card operation. Problems with the price of the player card
// 29-SEP-2016 FJC        Fixed Bug (Reopened) 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
// 03-OCT-2016 ETP        Fixed bug 18423: Error when canceling pin pad card payed.
// 24-OCT-2016 ESE        Fixed bug 16896: Can not undo operation if there is no cash
// 30-NOV-2016 FOS        PBI 19197: Reopen cashier sessions and gambling tables sessions
// 10-JAN-2017 RAB        Bug 11007: Error in balance in the account to cancel a promotion and/or void the last recharge
// 27-JAN-2016 JML        Fixed Bug 23692: Cashless - Sale of chips: error in integrated mode with promotion
// 23-JAN-2017 FJC        PBI 22249:Sorteo de m�quina - Participaci�n - US 2 - Anulaci�n
// 03-JAN-2017 ATB        Bug 24137:Cashier: Valid tickets can not be voided
// 08-FEB-2017 XGJ        PBI 22249:Sorteo de m�quina - Participaci�n - US 2 - Anulaci�n
// 22-FEB-2017 DPC        Bug 24975:Anulaci�n de adelanto de efectivo: no se actualiza el efectivo en caja ni las entradas con tarjeta bancaria
// 22-FEB-2017 JML        PBI 24378:Reabrir sesiones de caja y mesas de juego: rehacer reapertura y correcci�n de Bugs
// 27-MAR-2017 JML        PBI 25978:MES10 Ticket validation - Undo operation
// 29-MAR-2017 DPC        WIGOS-685: CreditLine - Undo last operation "Get Marker"
// 29-MAR-2017 RAB        PBI 25978:MES10 Ticket validation - Undo operation
// 30-MAR-2017 RAB        PBI 25978:MES10 Ticket validation - Undo operation
// 16-MAY-2017 RGR        Fixed Bug 24188: You can not cancel cash advances from other cash sessions without having made one for at least the same value
// 23-MAY-2017 DHA        PBI 27487: WIGOS-83 MES21 - Sales selection
// 03-JUL-2017 RAB        PBI 28002: WIGOS-2730 - Rounding - Undo operation
// 06-JUL-2017 JML        PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 10-JUL-2017 RAB        PBI 28002: WIGOS-2730 - Rounding - Undo operation
// 11-JUL-2017 JML        PBI 28628:WIGOS-3086 Rounding - Undo operation in consumption of remaining amount
// 30-AGU-2017 DHA        Bug 29496:WIGOS-4758 [Ticket #8367] Cancelaci�n de recarga BM V03.06.0023
// 30-AUG-2017 DPC        [WIGOS-4779]: [Ticket #8418]Issue: Reports of Missing and surplus BM (Cancellation Deposits) V03.06.0023
// 16-OCT-2017 DHA        Bug 30270:WIGOS-5199 [Ticket #8834] Cancellation in recharges and chip sales v003.006.0023
// 23-JAN-2018 DHA & AGS  Bug 31304:WIGOS-7194 [Ticket #11260] Release 03.005.141 - In cashier is allowed to cancel the withdrawal already received in the cage
// 10-MAY-2018 DHA        Bug 32655:WIGOS-11699 Error appears when undoing the last chip sell operation
// 25-JUN-2018 FOS        Bug 33297:WIGOS-13059 Gambling tables: it is allowed to reopen a gambling table with integrated cashier (fixed banking) when the collection movements have been finished.
// ----------- ---------- ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;
using System.Collections;
using WSI.Common.Junkets;

namespace WSI.Common
{
  public class OperationUndo
  {
    #region Enums

    public enum UndoError
    {
      None = 0,
      Unknown = 1,
      NoReversibleOperations = 2,
      InsuficientCash = 3,
      InsuficientBalance = 4,
      InsuficientChips = 5,
      TicketNotValid = 6,
      MBAfterDeposit = 7,
      MBAfterRecharge = 8,
      CanceledPromotions = 9,
      TicketChipsDealerCopyNotValid = 10
    }

    #endregion

    #region Class Atributes

    public static Int64 m_mbaccount_id = -1;

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Insert Undo Operation.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Operation : Original account operation
    //          - CashierSessionId : Current cashier session Id
    //          - Trx : SQL Transaction
    //
    //      - OUTPUT:
    //          - UndoOperation: New inserted undo operation
    //
    // RETURNS:
    //      - Boolean: If operation has been inserted correctly.
    private static Boolean DB_InsertUndoOperation(AccountOperations.Operation AccountOperation, Int64 CashierSessionId,
                                                  out Int64 UndoOperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _parameter;

      UndoOperationId = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO ACCOUNT_OPERATIONS ( AO_DATETIME");
        _sb.AppendLine("                               , AO_CODE");
        _sb.AppendLine("                               , AO_ACCOUNT_ID");
        _sb.AppendLine("                               , AO_CASHIER_SESSION_ID");
        _sb.AppendLine("                               , AO_MB_ACCOUNT_ID");
        _sb.AppendLine("                               , AO_PROMO_ID");
        _sb.AppendLine("                               , AO_AMOUNT");
        _sb.AppendLine("                               , AO_NON_REDEEMABLE ");
        _sb.AppendLine("                               , AO_WON_LOCK");
        _sb.AppendLine("                               , AO_OPERATION_DATA");
        _sb.AppendLine("                               , AO_NON_REDEEMABLE2 ");
        _sb.AppendLine("                               , AO_REDEEMABLE ");
        _sb.AppendLine("                               , AO_PROMO_REDEEMABLE ");
        _sb.AppendLine("                               , AO_PROMO_NOT_REDEEMABLE ");
        _sb.AppendLine("                               , AO_UNDO_STATUS ");
        _sb.AppendLine("                               , AO_UNDO_OPERATION_ID ");
        _sb.AppendLine("                               )");
        _sb.AppendLine("                       VALUES ( GETDATE()");
        _sb.AppendLine("                              , @pCode");
        _sb.AppendLine("                              , @pAccountId");
        _sb.AppendLine("                              , @pCashierSessionId");
        _sb.AppendLine("                              , @pMbAccountId");
        _sb.AppendLine("                              , @pPromoId");
        _sb.AppendLine("                              , @pAmount");
        _sb.AppendLine("                              , @pNonRedeemable");
        _sb.AppendLine("                              , @pNonRedeemableWonLock");
        _sb.AppendLine("                              , @pOperationData");
        _sb.AppendLine("                              , @pNonRedeemable2");
        _sb.AppendLine("                              , @pRedeemable");
        _sb.AppendLine("                              , @pPromoRedeemable");
        _sb.AppendLine("                              , @pPromoNotRedeemable");
        _sb.AppendLine("                              , @pUndoStatus");
        _sb.AppendLine("                              , @pUndoOperationId");
        _sb.AppendLine("                              )");
        _sb.AppendLine("SET @pOperationId = SCOPE_IDENTITY()");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = AccountOperation.Code;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountOperation.AccountId;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _cmd.Parameters.Add("@pMbAccountId", SqlDbType.BigInt).Value = AccountOperation.MbAccountId;
          _cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = AccountOperation.PromoId;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = -AccountOperation.Amount;
          _cmd.Parameters.Add("@pNonRedeemable", SqlDbType.Money).Value = -AccountOperation.NonRedemable;
          _cmd.Parameters.Add("@pNonRedeemableWonLock", SqlDbType.Money).Value = -AccountOperation.WonLock;
          _cmd.Parameters.Add("@pOperationData", SqlDbType.BigInt).Value = AccountOperation.OperationData;
          _cmd.Parameters.Add("@pNonRedeemable2", SqlDbType.Money).Value = -AccountOperation.NonRedemable2;
          _cmd.Parameters.Add("@pRedeemable", SqlDbType.Money).Value = -AccountOperation.Redeemable;
          _cmd.Parameters.Add("@pPromoRedeemable", SqlDbType.Money).Value = -AccountOperation.PromoRedeemable;
          _cmd.Parameters.Add("@pPromoNotRedeemable", SqlDbType.Money).Value = -AccountOperation.PromoNonRedeemable;
          _cmd.Parameters.Add("@pUndoStatus", SqlDbType.Int).Value = (Int32)UndoStatus.UndoOperation;
          _cmd.Parameters.Add("@pUndoOperationId", SqlDbType.BigInt).Value = AccountOperation.OperationId;

          _parameter = _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt);
          _parameter.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            UndoOperationId = (Int64)_parameter.Value;

            // DHA 16-MAR-2015
            if (!Operations.InsertPendingOperationMultiSite(UndoOperationId, AccountOperation.Code, AccountOperation.Amount, false, Trx))
            {
              return false;
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_InsertUndoOperation

    //------------------------------------------------------------------------------
    // PURPOSE: Insert negated cashier movements
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId : Old operation Id
    //          - UndoOperationID : Operation Id thats undo old operation
    //          - CashierSessionInfo : Cashier Session Info    
    //          - Trx : SQL Transaction
    //
    //      - OUTPUT:
    //          - Exchange result if movements contains currency exchange
    //
    // RETURNS:
    //      - Boolean: If cashier movements has been inserted correctly.
    //
    private static Boolean DB_InsertUndoCashierMovements(Int64 OperationId, Int64 UndoOperationId, CashierSessionInfo CashierSessionInfo, Boolean ForceUndoTPV,
                                                         out CurrencyExchangeResult ExchangeResult, out Currency TotalChips, out DataTable CashierMovements,
                                                         SqlTransaction Trx)
    {
      StringBuilder _sb;
      CASHIER_MOVEMENT _type;
      DateTime _mov_date;
      Decimal _change_rate;
      CashierMovementsTable _cashier_mov_table;
      Boolean _undo_card_to_cash;

      _sb = new StringBuilder();
      CashierMovements = new DataTable();

      ExchangeResult = new CurrencyExchangeResult();
      ExchangeResult.InType = CurrencyExchangeType.CURRENCY;
      TotalChips = 0;

      _undo_card_to_cash = Misc.GetBankCardUndoMode() == Misc.BankCardUndoMode.FROM_AMOUNT_CONCEPT && !ForceUndoTPV;

      // Read a operation cashier movements without promotions
      #region Read Operation_cashier_Movements
      _sb.Length = 0;
      _sb.AppendLine("  SELECT   *                                                                                        ");
      _sb.AppendLine("    FROM   CASHIER_MOVEMENTS WITH(INDEX(IX_cm_operation_id))                                        ");
      _sb.AppendLine("   WHERE   CM_OPERATION_ID     = @pOperationId                                                      ");
      if (TITO.Utils.IsTitoMode())
      {
        _sb.AppendLine("     AND   CM_TYPE  NOT IN (@pPromotionPoint,@pHandpayWithholding, ");
      }
      else
      {
        _sb.AppendLine("     AND   CM_TYPE  NOT IN (@pPromoNotRedeem, @pPromotionPoint, ");
      }
      _sb.AppendLine("                            @pHandpay, @pManualHandpay, ");
      _sb.AppendLine("                            @pCancelPromoNotRedeem, @pCancelPromoRedeem, @pCancelPromotionPoint,    ");
      _sb.AppendLine("                            @pPrizeInKind1Gross, @pPrizeInKind2Gross,                               ");
      _sb.AppendLine("                            @pPrizeReInKind1Gross, @pPrizeReInKind2Gross)                           ");

      _sb.AppendLine("ORDER BY   CM_MOVEMENT_ID ASC                                                                       ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _cmd.Parameters.Add("@pPromoNotRedeem", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE;
        _cmd.Parameters.Add("@pPromotionPoint", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_POINT;
        _cmd.Parameters.Add("@pPrizeInKind1Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS;
        _cmd.Parameters.Add("@pPrizeInKind2Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS;
        _cmd.Parameters.Add("@pCancelPromoNotRedeem", SqlDbType.Int).Value = CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE;

        _cmd.Parameters.Add("@pPrizeReInKind1Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS;
        _cmd.Parameters.Add("@pPrizeReInKind2Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS;

        _cmd.Parameters.Add("@pCancelPromoRedeem", SqlDbType.Int).Value = CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE;
        _cmd.Parameters.Add("@pCancelPromotionPoint", SqlDbType.Int).Value = CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE;
        _cmd.Parameters.Add("@pHandpayWithholding", SqlDbType.Int).Value = CASHIER_MOVEMENT.HANDPAY_WITHHOLDING;
        _cmd.Parameters.Add("@pHandpay", SqlDbType.Int).Value = CASHIER_MOVEMENT.HANDPAY;
        _cmd.Parameters.Add("@pManualHandpay", SqlDbType.Int).Value = CASHIER_MOVEMENT.MANUAL_HANDPAY;

        using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
        {
          _sql_da.Fill(CashierMovements);
        }
      }
      #endregion

      CashierMovements.Columns.Remove("CM_MOVEMENT_ID");
      CashierMovements.Columns.Add("CM_BALANCE_INCREMENT");
      CashierMovements.Columns.Add("CURRENCY_TYPE", typeof(Int32));

      foreach (DataRow _row in CashierMovements.Rows)
      {
        _type = (CASHIER_MOVEMENT)_row["CM_TYPE"];

        switch (_type)
        {
          #region switch type cashier movement
          case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
          // RMS 08-AUG-2014
          case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
          // DLL 15-DEC-2014
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
          // DLL 17-DEC-2014
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:
            if (_type == CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1 || _type == CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1
              || _type == CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1 || _type == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1
              || _type == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1 || _type == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1)
            {
              ExchangeResult.InType = CurrencyExchangeType.CARD;
            }
            if (_type == CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1 || _type == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1)
            {
              ExchangeResult.InType = CurrencyExchangeType.CHECK;
            }

            // RAB 01-AUG_2016
            // ETP 26-SEP-2016
            // ATB 01-DEC-2016
            // In case of undo the last card deposit action (Split1) we change the movement type
            if (_undo_card_to_cash && ExchangeResult.InType == CurrencyExchangeType.CARD)
            {
              _row["CM_TYPE"] = CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO;
            }

            ExchangeResult.Comission = (Decimal)_row["CM_SUB_AMOUNT"];
            ExchangeResult.InCurrencyCode = (String)_row["CM_CURRENCY_ISO_CODE"];
            ExchangeResult.GrossAmount = (Decimal)_row["CM_ADD_AMOUNT"];
            ExchangeResult.InAmount = (Decimal)_row["CM_INITIAL_BALANCE"];
            ExchangeResult.NetAmountSplit1 = (Decimal)_row["CM_FINAL_BALANCE"];
            ExchangeResult.NR2Amount = (Decimal)_row["CM_AUX_AMOUNT"];
            _mov_date = (DateTime)_row["CM_DATE"];
            _row["CURRENCY_TYPE"] = ExchangeResult.InType;

            // Get change rate
            if (CurrencyExchange.GetHistoricChangeRate(ExchangeResult.InCurrencyCode, ExchangeResult.InType, _mov_date, out _change_rate, Trx))
            {
              ExchangeResult.ChangeRate = _change_rate;
            }

            // Change cashier movements sign
            _row["CM_INITIAL_BALANCE"] = -(Decimal)_row["CM_INITIAL_BALANCE"];
            _row["CM_FINAL_BALANCE"] = -(Decimal)_row["CM_FINAL_BALANCE"];
            break;
          case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2:
          case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
          case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
          // RMS 08-AUG-2014
          case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
          case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
          // DLL 15-DEC-2014
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
          // DLL 17-DEC-2014
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2:

            // RAB 01-AUG_2016
            // ETP 26-SEP-2016
            // ATB 01-DEC-2016
            // In case of undo the last card deposit action (Split2) we change the movement type
            if (_undo_card_to_cash && ExchangeResult.InType == CurrencyExchangeType.CARD)
            {
              _row["CM_TYPE"] = CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2_UNDO;
            }

            ExchangeResult.NetAmountSplit2 = (Decimal)_row["CM_FINAL_BALANCE"];

            // Change cashier movements sign
            _row["CM_INITIAL_BALANCE"] = -(Decimal)_row["CM_INITIAL_BALANCE"];
            _row["CM_FINAL_BALANCE"] = -(Decimal)_row["CM_FINAL_BALANCE"];
            _row["CURRENCY_TYPE"] = ExchangeResult.InType;

            break;
          case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
            TotalChips = -(Decimal)_row["CM_ADD_AMOUNT"];
            _row["CM_INITIAL_BALANCE"] = ((Decimal)_row["CM_FINAL_BALANCE"] + (Decimal)_row["CM_ADD_AMOUNT"]);
            _row["CM_FINAL_BALANCE"] = ((Decimal)_row["CM_INITIAL_BALANCE"] - (Decimal)_row["CM_ADD_AMOUNT"]);
            _row["CURRENCY_TYPE"] = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]);
            break;

          case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE:
            TotalChips = -(Decimal)_row["CM_ADD_AMOUNT"];
            _row["CM_INITIAL_BALANCE"] = -(Decimal)_row["CM_INITIAL_BALANCE"];
            _row["CM_FINAL_BALANCE"] = -(Decimal)_row["CM_ADD_AMOUNT"];
            _row["CURRENCY_TYPE"] = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]);
            break;
          case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
            TotalChips = -(Decimal)_row["CM_SUB_AMOUNT"];
            _row["CM_INITIAL_BALANCE"] = ((Decimal)_row["CM_FINAL_BALANCE"] - (Decimal)_row["CM_SUB_AMOUNT"]);
            _row["CM_FINAL_BALANCE"] = ((Decimal)_row["CM_INITIAL_BALANCE"] + (Decimal)_row["CM_SUB_AMOUNT"]);
            _row["CURRENCY_TYPE"] = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]);
            break;

          case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE:
            TotalChips = -(Decimal)_row["CM_SUB_AMOUNT"];
            _row["CM_INITIAL_BALANCE"] = -(Decimal)_row["CM_INITIAL_BALANCE"];
            _row["CM_FINAL_BALANCE"] = (Decimal)_row["CM_SUB_AMOUNT"];
            _row["CURRENCY_TYPE"] = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]);
            break;

          case CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT:
            TotalChips = -(Decimal)_row["CM_SUB_AMOUNT"];
            _row["CM_INITIAL_BALANCE"] = ((Decimal)_row["CM_FINAL_BALANCE"] - (Decimal)_row["CM_SUB_AMOUNT"]);
            _row["CM_FINAL_BALANCE"] = ((Decimal)_row["CM_INITIAL_BALANCE"] + (Decimal)_row["CM_SUB_AMOUNT"]);
            _row["CURRENCY_TYPE"] = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]);
            break;

          case CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT:
            TotalChips = -(Decimal)_row["CM_ADD_AMOUNT"];
            _row["CM_INITIAL_BALANCE"] = ((Decimal)_row["CM_FINAL_BALANCE"] - (Decimal)_row["CM_SUB_AMOUNT"]);
            _row["CM_FINAL_BALANCE"] = ((Decimal)_row["CM_INITIAL_BALANCE"] + (Decimal)_row["CM_SUB_AMOUNT"]);
            _row["CURRENCY_TYPE"] = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]);
            break;

          case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
          // DLL 18-DEC-2014
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK:
            _row["CM_INITIAL_BALANCE"] = -(Decimal)_row["CM_INITIAL_BALANCE"];
            _row["CM_FINAL_BALANCE"] = -(Decimal)_row["CM_FINAL_BALANCE"];
            _row["CURRENCY_TYPE"] = CurrencyExchangeType.CHECK;
            break;

          case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
          case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
          case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
          // DLL 18-DEC-2014
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
            _row["CM_INITIAL_BALANCE"] = -(Decimal)_row["CM_INITIAL_BALANCE"];
            _row["CM_FINAL_BALANCE"] = -(Decimal)_row["CM_FINAL_BALANCE"];
            _row["CURRENCY_TYPE"] = CurrencyExchangeType.CARD;
            break;

          case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE:
          // DLL 18-DEC-2014
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE:
            // RAB 04-AUG-2016
            if (_undo_card_to_cash)
            {
              _row["CM_TYPE"] = CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE_UNDO;
            }

            ExchangeResult.Comission = (Decimal)_row["CM_SUB_AMOUNT"];
            ExchangeResult.InCurrencyCode = (String)_row["CM_CURRENCY_ISO_CODE"];
            ExchangeResult.GrossAmount = (Decimal)_row["CM_ADD_AMOUNT"];
            ExchangeResult.InAmount = (Decimal)_row["CM_INITIAL_BALANCE"];
            ExchangeResult.NetAmountSplit1 = (Decimal)_row["CM_FINAL_BALANCE"];
            ExchangeResult.NR2Amount = (Decimal)_row["CM_AUX_AMOUNT"];
            _mov_date = (DateTime)_row["CM_DATE"];
            _row["CURRENCY_TYPE"] = ExchangeResult.InType;

            // Get change rate
            if (CurrencyExchange.GetHistoricChangeRate(ExchangeResult.InCurrencyCode, ExchangeResult.InType, _mov_date, out _change_rate, Trx))
            {
              ExchangeResult.ChangeRate = _change_rate;
            }
            break;

          case CASHIER_MOVEMENT.CASH_ADVANCE_CASH:
          case CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER:
          case CASHIER_MOVEMENT.MB_CASH_IN:
          case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY:
          case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY:
          case CASHIER_MOVEMENT.CLOSE_SESSION:
          case CASHIER_MOVEMENT.CASH_CLOSING_SHORT:
            _row["CM_INITIAL_BALANCE"] = -(Decimal)_row["CM_INITIAL_BALANCE"];
            _row["CM_FINAL_BALANCE"] = -(Decimal)_row["CM_FINAL_BALANCE"];
            if (FeatureChips.IsCageCurrencyTypeChips((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]))
            {
              _row["CURRENCY_TYPE"] = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]);
            }
            else
            {
              _row["CURRENCY_TYPE"] = CurrencyExchangeType.CURRENCY;
            }
            break;

          case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION:
            _row["CM_ADD_AMOUNT"] = (Decimal)_row["CM_FINAL_BALANCE"];
            _row["CM_INITIAL_BALANCE"] = -(Decimal)_row["CM_INITIAL_BALANCE"];
            _row["CM_FINAL_BALANCE"] = -(Decimal)_row["CM_FINAL_BALANCE"];
            if (FeatureChips.IsCageCurrencyTypeChips((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]))
            {
              _row["CURRENCY_TYPE"] = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]);
            }
            else
            {
              _row["CURRENCY_TYPE"] = CurrencyExchangeType.CURRENCY;
            }

            break;
          case CASHIER_MOVEMENT.FILLER_OUT:
            _row["CURRENCY_TYPE"] = CurrencyExchangeType.CURRENCY;
            _row["CM_INITIAL_BALANCE"] = (Decimal)_row["CM_INITIAL_BALANCE"] - (Decimal)_row["CM_SUB_AMOUNT"];
            _row["CM_FINAL_BALANCE"] = (Decimal)_row["CM_SUB_AMOUNT"] + (Decimal)_row["CM_FINAL_BALANCE"];
            break;

          default:
            _row["CM_INITIAL_BALANCE"] = 0;
            _row["CM_FINAL_BALANCE"] = 0;

            if (FeatureChips.IsCageCurrencyTypeChips((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]))
            {
              _row["CURRENCY_TYPE"] = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CM_CAGE_CURRENCY_TYPE"]);
            }
            else
            {
              _row["CURRENCY_TYPE"] = CurrencyExchangeType.CURRENCY;
            }
            break;
          #endregion
        }

        //RAB 15-FEB-2016: Get actually datetime from movement and save in datarow.
        _row["CM_DATE"] = WGDB.Now;

        // LEM 14-JAN-2014: Is necessary to setting the Balance Increment for CashIn movements because the call to Cashier.UpdateSession Balance are removed.
        // RCI & ICS 30-JAN-2014: Fixed Bug WIG-557
        switch (_type)
        {
          #region switch 2 type cashier movement
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT:
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT:
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC:
            if (_undo_card_to_cash)
            {
              _row["CM_TYPE"] = CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_UNDO;
            }

            _row["CM_BALANCE_INCREMENT"] = -(Decimal)_row["CM_ADD_AMOUNT"];
            break;

          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
            if (_undo_card_to_cash)
            {
              _row["CM_TYPE"] = CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_UNDO;
            }

            _row["CM_BALANCE_INCREMENT"] = -(Decimal)_row["CM_ADD_AMOUNT"];
            break;

          case CASHIER_MOVEMENT.CASH_IN:
          case CASHIER_MOVEMENT.CHECK_PAYMENT:
          case CASHIER_MOVEMENT.MB_CASH_IN:
          case CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER:
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
          // ATB 01-DEC-2016
          // When the recharge's undo is in currency, the movement type remains the same
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
            _row["CM_BALANCE_INCREMENT"] = -(Decimal)_row["CM_ADD_AMOUNT"];
            break;
          case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
          case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
            if (IsFromCashier())
              _row["CM_BALANCE_INCREMENT"] = -(Decimal)_row["CM_ADD_AMOUNT"];
            break;

          case CASHIER_MOVEMENT.CASH_OUT:
          case CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO:
            _row["CM_BALANCE_INCREMENT"] = _row["CM_SUB_AMOUNT"];
            break;

          case CASHIER_MOVEMENT.FILLER_OUT:
            _row["CM_BALANCE_INCREMENT"] = _row["CM_SUB_AMOUNT"];
            break;

          default:
            _row["CM_BALANCE_INCREMENT"] = 0;
            break;
          #endregion
        }

        // OPC 09-DEC-2014: Change the aux_amount column value to negative 
        switch (_type)
        {
          #region switch 3 type cashier movement
          case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
          case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
          case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2:
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
          case CASHIER_MOVEMENT.CARD_REPLACEMENT:
          case CASHIER_MOVEMENT.HANDPAY_WITHHOLDING:
          // DHA 06-JUL-2015
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK:
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT:
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT:
          case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC:
          // DCS 06-JUL-2015
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
          case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN:

          // RAB 24-MAR-2016: Bug 10745
          case CASHIER_MOVEMENT.CARD_ASSOCIATE:
            if (!_row.IsNull("CM_AUX_AMOUNT"))
            {
              _row["CM_AUX_AMOUNT"] = -(Decimal)_row["CM_AUX_AMOUNT"];
            }
            break;

          default:
            break;
          #endregion
        }

        // Change cashier movements sign
        _row["CM_SUB_AMOUNT"] = -(Decimal)_row["CM_SUB_AMOUNT"];
        _row["CM_ADD_AMOUNT"] = -(Decimal)_row["CM_ADD_AMOUNT"];

        if (_row.IsNull("CM_CURRENCY_DENOMINATION"))
        {
          _row["CM_CURRENCY_DENOMINATION"] = 0;
        }

        if (_row.IsNull("CM_GAMING_TABLE_SESSION_ID"))
        {
          _row["CM_GAMING_TABLE_SESSION_ID"] = 0;
        }

        _row["CM_OPERATION_ID"] = UndoOperationId;

        // The row must be in state Added.
        _row.AcceptChanges();
        _row.SetAdded();
      }

      // Check if there was a currency exchange
      if (ExchangeResult.InAmount == 0)
      {
        ExchangeResult = null;
      }

      _cashier_mov_table = new CashierMovementsTable(CashierSessionInfo);
      _cashier_mov_table.DataTableToSave = CashierMovements;

      return _cashier_mov_table.Save(Trx, true);

    } // DB_InsertUndoCashierMovements

    /// <summary>
    /// Update sale chips rounding amount from DB
    /// </summary>
    /// <param name="CashierMovements">All nullable account movements</param>
    /// <param name="AccountId">I'll account for canceling</param>
    /// <param name="Trx">SQL Transaction</param>
    /// <returns></returns>
    private static Boolean UpdateUndoRoundingSaleChips(DataTable CashierMovements, Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Currency _chips_rounding;
      DataRow[] _chips_rounding_cashier_movements;

      _sb = new StringBuilder();
      _chips_rounding = Currency.Zero();
      _chips_rounding_cashier_movements = CashierMovements.Select(String.Format("CM_TYPE IN ({0}, {1})", ((Int32)CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT).ToString(),
                                                                                                         ((Int32)CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT).ToString()));

      if (_chips_rounding_cashier_movements.Length == 0)
      {
        return true;
      }

      foreach (DataRow _current_row in _chips_rounding_cashier_movements)
      {
        switch ((CASHIER_MOVEMENT)_current_row["CM_TYPE"])
        {
          case CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT:
            _chips_rounding += Format.ParseCurrency(_current_row["CM_SUB_AMOUNT"].ToString());
            break;
          case CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT:
            _chips_rounding -= Format.ParseCurrency(_current_row["CM_ADD_AMOUNT"].ToString());
            break;
        }
      }

      _sb.AppendLine("UPDATE   ACCOUNTS                                                                         ");
      _sb.AppendLine("   SET   AC_SALES_CHIPS_ROUNDING_AMOUNT = AC_SALES_CHIPS_ROUNDING_AMOUNT + @pChipRounding ");
      _sb.AppendLine(" WHERE   AC_ACCOUNT_ID                  = @pAccountId                                     ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pChipRounding", SqlDbType.Decimal).Value = _chips_rounding.Decimal;
        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

        return (_cmd.ExecuteNonQuery() > 0);
      }
    } // GetUndoRoundingSaleChips

    //------------------------------------------------------------------------------
    // PURPOSE: Insert negated cashier movements
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId : Old operation Id
    //          - UndoOperationID : Operation Id thats undo old operation
    //          - CashierSessionInfo : Cashier Session Info    
    //          - TicketUndo: ticket to avoid
    //          - Trx : SQL Transaction
    //
    //      - OUTPUT:
    //          - Exchange result if movements contains currency exchange
    //
    // RETURNS:
    //      - Boolean: If cashier movements has been inserted correctly.
    //
    private static Boolean DB_InsertUndoTITOTicketCashierMovements(Int64 OperationId, Int64 UndoOperationId, CashierSessionInfo CashierSessionInfo, Ticket TicketUndo,
                                                         out CurrencyExchangeResult ExchangeResult, out Currency TotalChips, out List<Int64> CahierMovementsIds, SqlTransaction Trx)
    {
      StringBuilder _sb;
      CASHIER_MOVEMENT _type;
      DataTable _cashier_movements;
      CashierMovementsTable _cashier_mov_table;

      _sb = new StringBuilder();
      _cashier_movements = new DataTable();
      ExchangeResult = null;
      CahierMovementsIds = new List<Int64>();
      TotalChips = 0;

      // Read a operation cashier movements without promotions
      _sb.Length = 0;
      _sb.AppendLine("  SELECT   TOP 1 *                                                                                  ");
      _sb.AppendLine("    FROM   CASHIER_MOVEMENTS WITH(INDEX(IX_cm_operation_id))                                        ");
      _sb.AppendLine("   WHERE   CM_OPERATION_ID     = @pOperationId                                                      ");
      _sb.AppendLine("     AND   CM_ADD_AMOUNT = @pAmount                                                                 ");
      _sb.AppendLine("     AND   CM_UNDO_STATUS IS NULL                                                                   ");

      if (TicketUndo.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
      {
        _sb.AppendLine("     AND   CM_TYPE = @pPromoNotRedeemable                                                         ");
      }
      if (TicketUndo.TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
      {
        _sb.AppendLine("     AND   CM_TYPE = @pPromoRedeemable                                                            ");
      }
      _sb.AppendLine("    AND   CM_ADD_AMOUNT = @pAmount                                                                  ");

      _sb.AppendLine("   UNION                                                                                            ");

      _sb.AppendLine("  SELECT   TOP 1 *                                                                                  ");
      _sb.AppendLine("    FROM   CASHIER_MOVEMENTS WITH(INDEX(IX_cm_operation_id))                                        ");
      _sb.AppendLine("   WHERE   CM_OPERATION_ID     = @pOperationId                                                      ");
      _sb.AppendLine("     AND   CM_UNDO_STATUS IS NULL                                                                   ");

      if (TicketUndo.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
      {
        _sb.AppendLine("     AND   CM_TYPE = @pTicketCreatePromoNoRedeemable                                              ");
      }
      else if (TicketUndo.TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
      {
        _sb.AppendLine("     AND   CM_TYPE = @pTicketCreatePromoRedeemable                                                ");
      }
      _sb.AppendLine("     AND   CM_ADD_AMOUNT = @pAmount                                                                 ");


      _sb.AppendLine("ORDER BY   CM_MOVEMENT_ID ASC                                                                       ");

      _cashier_movements = new DataTable();

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _cmd.Parameters.Add("@pPromoNotRedeemable", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE;
        _cmd.Parameters.Add("@pPromoRedeemable", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE;
        _cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = TicketUndo.Amount;
        _cmd.Parameters.Add("@pTicketCreatePromoRedeemable", SqlDbType.Int).Value = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE;
        _cmd.Parameters.Add("@pTicketCreatePromoNoRedeemable", SqlDbType.Int).Value = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE;

        using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
        {
          _sql_da.Fill(_cashier_movements);
        }
      }

      _cashier_movements.Columns.Add("CM_BALANCE_INCREMENT");
      _cashier_movements.Columns.Add("CURRENCY_TYPE", typeof(Int32));

      foreach (DataRow _row in _cashier_movements.Rows)
      {
        CahierMovementsIds.Add((Int64)_row["CM_MOVEMENT_ID"]);

        _type = (CASHIER_MOVEMENT)_row["CM_TYPE"];

        _row["CM_INITIAL_BALANCE"] = 0;
        _row["CM_FINAL_BALANCE"] = 0;

        // Change cashier movements sign
        _row["CM_SUB_AMOUNT"] = -(Decimal)_row["CM_SUB_AMOUNT"];
        _row["CM_ADD_AMOUNT"] = -(Decimal)_row["CM_ADD_AMOUNT"];

        // Don't touch the cashier balance here. It's updated afterwards (call to Cashier.UpdateSessionBalance).
        _row["CM_BALANCE_INCREMENT"] = 0;
        _row["CM_CURRENCY_DENOMINATION"] = 0;
        _row["CM_GAMING_TABLE_SESSION_ID"] = 0;
        _row["CM_OPERATION_ID"] = UndoOperationId;

        _row["CURRENCY_TYPE"] = 0;

        // The row must be in state Added.
        _row.AcceptChanges();
        _row.SetAdded();
      }

      _cashier_mov_table = new CashierMovementsTable(CashierSessionInfo);
      _cashier_mov_table.DataTableToSave = _cashier_movements;

      return _cashier_mov_table.Save(Trx);

    } // DB_InsertUndoTITOTicketCashierMovements


    //------------------------------------------------------------------------------
    // PURPOSE: Mark old operation movements as undone
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId : Old operation Id
    //          - UndoOperationID : Operation Id thats undo old operation
    //          - Trx : SQL Transaction
    //
    //      - OUTPUT:
    // 
    // RETURNS:
    //      - Boolean: If operation has been modified correctly.
    //
    private static Boolean DB_MarkAsUndone(Int64 OperationId, Int64 UndoOperationId, SqlTransaction Trx)
    {
      StringBuilder _sb = new StringBuilder();

      // Update original operation account operations
      _sb.AppendLine("UPDATE   ACCOUNT_OPERATIONS ");
      _sb.AppendLine("   SET   AO_UNDO_OPERATION_ID = @pUndoOperationId ");
      _sb.AppendLine("       , AO_UNDO_STATUS       = @pUndone ");
      _sb.AppendLine(" WHERE   AO_OPERATION_ID      = @pOperationId ");

      // Update original operation account movements
      _sb.AppendLine("UPDATE   ACCOUNT_MOVEMENTS ");
      _sb.AppendLine("   SET   AM_UNDO_STATUS       = @pUndone ");
      _sb.AppendLine(" WHERE   AM_OPERATION_ID      = @pOperationId ");

      // Update undo operation account movements
      _sb.AppendLine("UPDATE   ACCOUNT_MOVEMENTS ");
      _sb.AppendLine("   SET   AM_UNDO_STATUS       = @pUndoOperation ");
      _sb.AppendLine(" WHERE   AM_OPERATION_ID      = @pUndoOperationId ");

      // Update original operation cashier movements
      _sb.AppendLine("UPDATE   CASHIER_MOVEMENTS ");
      _sb.AppendLine("   SET   CM_UNDO_STATUS       = @pUndone ");
      _sb.AppendLine(" WHERE   CM_OPERATION_ID      = @pOperationId ");

      // Update undo operation cashier movements
      _sb.AppendLine("UPDATE   CASHIER_MOVEMENTS ");
      _sb.AppendLine("   SET   CM_UNDO_STATUS       = @pUndoOperation ");
      _sb.AppendLine(" WHERE   CM_OPERATION_ID      = @pUndoOperationId ");

      // Update original operation mobile bank movements
      _sb.AppendLine("UPDATE   MB_MOVEMENTS      ");
      _sb.AppendLine("   SET   MBM_UNDO_STATUS      = @pUndone ");
      _sb.AppendLine(" WHERE   MBM_OPERATION_ID     = @pOperationId ");

      // Update undo operation mobile bank movements
      _sb.AppendLine("UPDATE   MB_MOVEMENTS ");
      _sb.AppendLine("   SET   MBM_UNDO_STATUS      = @pUndoOperation ");
      _sb.AppendLine(" WHERE   MBM_OPERATION_ID     = @pUndoOperationId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _cmd.Parameters.Add("@pUndoOperationId", SqlDbType.BigInt).Value = UndoOperationId;
        _cmd.Parameters.Add("@pUndone", SqlDbType.Int).Value = (Int32)UndoStatus.Undone;
        _cmd.Parameters.Add("@pUndoOperation", SqlDbType.Int).Value = (Int32)UndoStatus.UndoOperation;

        return (_cmd.ExecuteNonQuery() > 0);
      }

    } // DB_MarkAsUndone

    private static Boolean DB_MarkTITOTicketMovementsAsUndone(Int64 UndoOperationId, SqlTransaction Trx)
    {
      StringBuilder _sb = new StringBuilder();

      // Update undo operation account movements
      _sb.AppendLine("UPDATE   ACCOUNT_MOVEMENTS                          ");
      _sb.AppendLine("   SET   AM_UNDO_STATUS       = @pUndoOperation     ");
      _sb.AppendLine(" WHERE   AM_OPERATION_ID      = @pUndoOperationId   ");

      // Update undo operation cashier movements
      _sb.AppendLine("UPDATE   CASHIER_MOVEMENTS                          ");
      _sb.AppendLine("   SET   CM_UNDO_STATUS       = @pUndoOperation     ");
      _sb.AppendLine(" WHERE   CM_OPERATION_ID      = @pUndoOperationId   ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pUndoOperationId", SqlDbType.BigInt).Value = UndoOperationId;
        _cmd.Parameters.Add("@pUndoOperation", SqlDbType.Int).Value = (Int32)UndoStatus.UndoOperation;

        return (_cmd.ExecuteNonQuery() > 0);
      }

    } // DB_MarkTicketAsUndone

    private static Boolean DB_MarkTITOTicketOldMovementsAsUndone(Int64 OperationId, List<Int64> AccountMovementsIds, List<Int64> CahierMovementsIds, SqlTransaction Trx)
    {
      StringBuilder _sb = new StringBuilder();

      // Update original operation account movements
      _sb.AppendLine("UPDATE   ACCOUNT_MOVEMENTS                          ");
      _sb.AppendLine("   SET   AM_UNDO_STATUS       = @pUndone            ");
      _sb.AppendLine(" WHERE   AM_OPERATION_ID      = @pOperationId       ");
      _sb.AppendLine("   AND   AM_MOVEMENT_ID IN ( ");
      for (Int32 _idx_am = 0; _idx_am < AccountMovementsIds.Count; _idx_am++)
      {
        if (_idx_am > 0)
        {
          _sb.AppendLine(", @pAccountMovementsId" + _idx_am);
        }
        else
        {
          _sb.AppendLine("@pAccountMovementsId" + _idx_am);
        }
      }
      _sb.AppendLine(")  ");

      // Update original operation cashier movements
      _sb.AppendLine("UPDATE   CASHIER_MOVEMENTS                          ");
      _sb.AppendLine("   SET   CM_UNDO_STATUS       = @pUndone            ");
      _sb.AppendLine(" WHERE   CM_OPERATION_ID      = @pOperationId       ");
      _sb.AppendLine("   AND   CM_MOVEMENT_ID IN (");
      for (Int32 _idx_cm = 0; _idx_cm < CahierMovementsIds.Count; _idx_cm++)
      {
        if (_idx_cm > 0)
        {
          _sb.AppendLine(", @pCashierMovementsId" + _idx_cm);
        }
        else
        {
          _sb.AppendLine("@pCashierMovementsId" + _idx_cm);
        }
      }

      _sb.AppendLine(")  ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _cmd.Parameters.Add("@pUndone", SqlDbType.Int).Value = (Int32)UndoStatus.Undone;
        for (Int32 _idx_am = 0; _idx_am < AccountMovementsIds.Count; _idx_am++)
        {
          _cmd.Parameters.Add("@pAccountMovementsId" + _idx_am, SqlDbType.BigInt).Value = AccountMovementsIds[_idx_am];
        }
        for (Int32 _idx_cm = 0; _idx_cm < CahierMovementsIds.Count; _idx_cm++)
        {
          _cmd.Parameters.Add("@pCashierMovementsId" + _idx_cm, SqlDbType.BigInt).Value = CahierMovementsIds[_idx_cm];
        }

        return (_cmd.ExecuteNonQuery() > 0);
      }

    } // DB_MarkTicketAsUndone

    //------------------------------------------------------------------------------
    // PURPOSE: Update user max devolution
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - OperationId
    //          - MaxDevolution
    //          - CardPaidUndone
    //          - Trx : SQL Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean: If max devolution has been updated correctly.
    //
    private static Boolean DB_UpdateMaxDevolution(Int64 AccountId, Int64 OperationId, Currency MaxDevolution,
                                                  Boolean CardPaidUndone, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        // Set new max devolution
        _sb.AppendLine("UPDATE   ACCOUNTS                                          ");
        _sb.AppendLine("   SET   AC_INITIAL_CASH_IN          = @pNewMaxDevolution  ");
        _sb.AppendLine("       , AC_CANCELLABLE_OPERATION_ID = CASE WHEN AC_CANCELLABLE_OPERATION_ID = @pOperationId THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END ");
        _sb.AppendLine("       , AC_LAST_ACTIVITY            = GETDATE()           ");

        // Reset card paid if it was paid in this recharge
        if (CardPaidUndone)
        {
          _sb.AppendLine("     , AC_DEPOSIT    = 0 ");
          _sb.AppendLine("     , AC_CARD_PAID  = 0 ");
        }
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID               = @pAccountId         ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pNewMaxDevolution", SqlDbType.Money).Value = MaxDevolution.SqlMoney;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateMaxDevolution

    //------------------------------------------------------------------------------
    // PURPOSE: Update GamingTableSession values
    //
    //  PARAMS:
    //      - INPUT:
    //          - CashierSessionId
    //          - CashierSessionId
    //          - OperationCode
    //          - Amount
    //          - Trx : SQL Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean: If GamingTableSession has been updated correctly.
    //
    private static Boolean UpdateGamingTableSession(Int64 CashierSessionId, Int64 GamingTableSessionId, OperationCode CodeOperation,
                                                    Currency Amount, CurrencyIsoType CurrencyIsoType, SqlTransaction Trx)
    {
      Boolean _is_chips_sale_operation;
      GamingTablesSessions _gaming_tables_session;

      switch (CodeOperation)
      {
        case OperationCode.CHIPS_SALE:
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
          _is_chips_sale_operation = true;
          break;

        case OperationCode.CHIPS_PURCHASE:
          _is_chips_sale_operation = false;
          break;

        default:
          return false;
      }

      // Load GamingTableSession from this CashierSession


      // Cashier without GamingTableSession associated
      if (!GamingTablesSessions.GetOrOpenSession(out _gaming_tables_session, CashierSessionId, Trx))
      {
        if(_gaming_tables_session == null)
        {
          return true;
        }

        // Load GamingTableSession associaed to this operation
        if (!GamingTablesSessions.GetSession(out _gaming_tables_session, GamingTableSessionId, Trx))
        {
          return false;
        }

        // External Sale 
        if (_is_chips_sale_operation)
        {
          if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.ChipsExternalSales, Amount, CurrencyIsoType, Trx))
          {
            return false;
          }
        }
        // External Purchase
        else
        {
          if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.ChipsExternalPurchase, Amount, CurrencyIsoType, Trx))
          {
            return false;
          }
        }

        // RRR 07-FEB-2014: Undo visits adding
        if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.Visits, -1, Trx))
        {
          return false;
        }
      }
      else
      {
        // Chips Sale Operation
        if (_is_chips_sale_operation)
        {
          // External
          if (_gaming_tables_session.LastExternalSaleSessionId != null)
          {
            // Update Total Sale Amount
            if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.ChipsTotalSales, Amount, CurrencyIsoType, Trx))
            {
              return false;
            }

            // Delete LastExternalSaleSessionId
            if (!_gaming_tables_session.UpdateSessionLastSessionId(GTS_UPDATE_TYPE.LastExternalSale, null, Trx))
            {
              return false;
            }

            // Load External GamingTableSession 
            if (!GamingTablesSessions.GetSession(out _gaming_tables_session, GamingTableSessionId, Trx))
            {
              return false;
            }

            // Update External Sale Amount
            if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.ChipsExternalSales, Amount, CurrencyIsoType, Trx))
            {
              return false;
            }
          }
          // Own
          else
          {
            // Update Own Sale Amount and Total Sale Amount
            if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.ChipsOwnSales, Amount, CurrencyIsoType, Trx))
            {
              return false;
            }
          }
        }
        // Chips Purchase Operation
        else
        {
          // External
          if (_gaming_tables_session.LastExternalPurchaseSessionId != null)
          {
            // Update Total Purchase Amount
            if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.ChipsTotalPurchase, Amount, CurrencyIsoType, Trx))
            {
              return false;
            }

            // Delete LastExternalPurchaseSessionId            
            if (!_gaming_tables_session.UpdateSessionLastSessionId(GTS_UPDATE_TYPE.LastExternalPurchase, null, Trx))
            {
              return false;
            }

            // Load External GamingTableSession 
            if (!GamingTablesSessions.GetSession(out _gaming_tables_session, GamingTableSessionId, Trx))
            {
              return false;
            }

            // Update External Purchase Amount            
            if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.ChipsExternalPurchase, Amount, CurrencyIsoType, Trx))
            {
              return false;
            }
          }
          // Own
          else
          {
            // Update Own Sale Amount and Total Sale Amount            
            if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.ChipsOwnPurchase, Amount, CurrencyIsoType, Trx))
            {
              return false;
            }
          }
        }

        // RRR 07-FEB-2014: Undo visits adding
        if (!_gaming_tables_session.UpdateSession(GTS_UPDATE_TYPE.Visits, -1, Trx))
        {
          return false;
        }

      }
      return true;
    } // UpdateGamingTableSession

    //------------------------------------------------------------------------------
    // PURPOSE: Check if cashier session has enough cash to undo operation
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - CashierSessionId
    //          - Trx
    //
    //      - OUTPUT:
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: True if has enough cash to undo it.
    //
    private static Boolean HasEnoughCashToUndo(Int64 OperationId, Int64 CashierSessionId,
                                               out CurrencyIsoType IsoType, out Decimal IsoAmount, SqlTransaction Trx)
    {
      StringBuilder _sb;
      CASHIER_MOVEMENT _cm_type;
      Currency _balance;
      Boolean _exchange_found;
      Boolean _is_mb_recharge;
      Boolean _is_undo_credit_card_to_cash;
      Boolean _is_national_currency;
      Boolean _is_advance;

      _sb = new StringBuilder();
      IsoType = new CurrencyIsoType();
      IsoType.IsoCode = "";
      IsoType.Type = CurrencyExchangeType.CURRENCY;
      IsoAmount = 0;
      _balance = 0;
      _exchange_found = false;
      _is_mb_recharge = false;
      _is_advance = false;

      _cm_type = CASHIER_MOVEMENT.CASH_IN;
      // Read a undo operation.
      _sb.AppendLine("  SELECT   CM_TYPE                         ");
      _sb.AppendLine("         , CM_INITIAL_BALANCE              ");
      _sb.AppendLine("         , CM_ADD_AMOUNT                   ");
      _sb.AppendLine("         , CM_CURRENCY_ISO_CODE            ");
      _sb.AppendLine("         , CM_SUB_AMOUNT                   ");
      _sb.AppendLine("    FROM   CASHIER_MOVEMENTS               ");
      _sb.AppendLine("   WHERE   CM_OPERATION_ID = @pOperationId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          while (_reader.Read() && !_exchange_found)
          {
            _cm_type = (CASHIER_MOVEMENT)_reader.GetInt32(0);

            switch (_cm_type)
            {
              case CASHIER_MOVEMENT.CASH_IN:
                IsoAmount = IsoAmount + _reader.GetDecimal(2);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
              // RAB 01-AUG-2016
              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO:
              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2_UNDO:
              // RMS 08-AUG-2014
              case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
              // DLL 15-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CARD;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
              // DLL 17-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CHECK;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              // RRR 24-JAN-2014: Check only chips price in chips sale anulation
              case CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO:
                IsoAmount = IsoAmount - _reader.GetDecimal(4);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
              // DLL 18-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CHECK;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
              // RAB 01-AUG-2016
              case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE_UNDO:
              case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD_UNDO:
              case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD_UNDO:
              case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD_UNDO:
              case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
              case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
              // DLL 18-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
                _is_advance = true;
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CARD;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE:
              // DLL 18-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_CASH:
                IsoAmount = _reader.GetDecimal(4);  // SUB_AMOUNT: Indicates the cash to substract from cashier.
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                break;

              // OPC 21-JAN-2015
              case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
              case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
                IsoAmount = IsoAmount + _reader.GetDecimal(2);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _is_mb_recharge = true;
                break;

              // OPC 21-JAN-2015
              case CASHIER_MOVEMENT.MB_CASH_IN:
                IsoAmount = IsoAmount + _reader.GetDecimal(2);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY:
                IsoAmount = _reader.GetDecimal(2);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = CurrencyExchange.GetNationalCurrency();

                break;
              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                break;
              case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CARD:
              case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD:
              case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD:
                IsoType.Type = CurrencyExchangeType.CARD;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                IsoAmount = _reader.GetDecimal(2);
                _exchange_found = true;
                break;
              case CASHIER_MOVEMENT.CREDIT_LINE_MARKER:
                IsoType.Type = CurrencyExchangeType.CREDITLINE;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                IsoAmount = _reader.GetDecimal(4);

                _exchange_found = true;
                break;
              default:
                break;
            }
          }
        }
      }

      // Not found because recharge must be greater than 0
      if (IsoAmount == 0)
      {
        return false;
      }

      if (_is_mb_recharge)
      {
        return true;
      }

      if (IsoType.Type == CurrencyExchangeType.CREDITLINE)
      {
        return true;
      }

      if (IsAdvanceMovement(_cm_type))
      {
        return true;
      }

      _is_national_currency = IsoType.IsoCode == CurrencyExchange.GetNationalCurrency() && IsoType.Type == CurrencyExchangeType.CURRENCY;

      //Si el GP fuerza una devolucion en efectivo pero se ha echo un adelato de efectivo con tarjeta bancaria se devuelve en la tarjeta bancaria 
      _is_undo_credit_card_to_cash = (Misc.GetBankCardUndoMode() == Misc.BankCardUndoMode.FROM_AMOUNT_CONCEPT && !_is_advance);

      if (_is_national_currency || _is_undo_credit_card_to_cash)
      {
        // Balance en efectivo (moneda nacional)
        _balance = Cashier.UpdateSessionBalance(Trx, CashierSessionId, 0);
      }
      else
      {
        // Balance del credito
        _balance = Cashier.UpdateSessionBalance(Trx, CashierSessionId, 0, IsoType.IsoCode, IsoType.Type);
      }

      return (_balance >= IsoAmount);

    }// HasEnoughCashToUndo

    private static bool IsAdvanceMovement(CASHIER_MOVEMENT type)
    {
      bool _isAdvance;

      switch (type)
      {
        case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE:
        case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
        case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
        case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
        case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
        case CASHIER_MOVEMENT.CASH_ADVANCE_CASH:
          _isAdvance = true;
          break;
        default:
          _isAdvance = false;
          break;
      }

      return _isAdvance;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check if tue operation is done from the cashier
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean: True if is from Cashier.
    //
    private static bool IsFromCashier()
    {
      string[] _clargs = Environment.GetCommandLineArgs();
      if (_clargs.Length == 0)
        return false;
      return _clargs[0].ToUpperInvariant().Contains("WSI.CASHIER".ToUpperInvariant());
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Check if cashier session has enough cash to undo operation
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - CashierSessionId
    //          - Trx
    //
    //      - OUTPUT:
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: True if has enough cash to undo it.
    //
    private static Boolean HasEnoughCashToUndo(AccountOperations.Operation AccountOperation, Int64 CashierSessionId,
                                               out CurrencyIsoType IsoType, out Decimal IsoAmount, SqlTransaction Trx)
    {
      StringBuilder _sb;
      CASHIER_MOVEMENT _cm_type;
      Currency _balance;
      Boolean _exchange_found;
      Boolean _is_mb_recharge;

      _sb = new StringBuilder();
      IsoType = new CurrencyIsoType();
      IsoType.IsoCode = "";
      IsoType.Type = CurrencyExchangeType.CURRENCY;
      IsoAmount = 0;
      _balance = 0;
      _exchange_found = false;
      _is_mb_recharge = false;

      // Read a undo operation.
      _sb.AppendLine("  SELECT   CM_TYPE                         ");
      _sb.AppendLine("         , CM_INITIAL_BALANCE              ");
      _sb.AppendLine("         , CM_ADD_AMOUNT                   ");
      _sb.AppendLine("         , CM_CURRENCY_ISO_CODE            ");
      _sb.AppendLine("         , CM_SUB_AMOUNT                   ");
      _sb.AppendLine("    FROM   CASHIER_MOVEMENTS               ");
      _sb.AppendLine("   WHERE   CM_OPERATION_ID = @pOperationId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = AccountOperation.OperationId;

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          while (_reader.Read() && !_exchange_found)
          {
            _cm_type = (CASHIER_MOVEMENT)_reader.GetInt32(0);

            switch (_cm_type)
            {
              case CASHIER_MOVEMENT.CASH_IN:
                IsoAmount = IsoAmount + _reader.GetDecimal(2);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
              // RMS 08-AUG-2014
              case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
              // DLL 15-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CARD;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
              // DLL 17-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CHECK;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              // RRR 24-JAN-2014: Check only chips price in chips sale anulation
              case CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO:
                IsoAmount = IsoAmount - _reader.GetDecimal(4);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
              // DLL 18-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CHECK;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
              case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
              case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
              // DLL 18-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CARD;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE:
              // DLL 18-DEC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE:
                IsoAmount = _reader.GetDecimal(1);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _exchange_found = true;
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_CASH:
                IsoAmount = _reader.GetDecimal(4);  // SUB_AMOUNT: Indicates the cash to substract from cashier.
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                break;

              // OPC 21-JAN-2015
              case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
              case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
                IsoAmount = IsoAmount + _reader.GetDecimal(2);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                _is_mb_recharge = true;
                break;

              // OPC 21-JAN-2015
              case CASHIER_MOVEMENT.MB_CASH_IN:
                IsoAmount = IsoAmount + _reader.GetDecimal(2);
                IsoType.Type = CurrencyExchangeType.CURRENCY;
                IsoType.IsoCode = _reader.IsDBNull(3) ? "" : _reader.GetString(3);
                break;

              default:
                break;
            }
          }
        }
      }

      // Not found because recharge must be greater than 0
      if (IsoAmount == 0)
      {
        return false;
      }

      if (_is_mb_recharge)
      {
        return true;
      }
      if (IsoType.IsoCode != CurrencyExchange.GetNationalCurrency())
      {
        _balance = Cashier.UpdateSessionBalance(Trx, CashierSessionId, 0, IsoType.IsoCode, IsoType.Type);
      }
      else
      {
        _balance = Cashier.UpdateSessionBalance(Trx, CashierSessionId, 0);
      }

      return (_balance >= AccountOperation.Amount);

    } // HasEnoughCashToUndo

    //------------------------------------------------------------------------------
    // PURPOSE: Check if cashier session has enough chips to undo operation
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - CashierSessionId
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean: True if has enough cash to undo it.
    //
    private static Boolean HasEnoughChipsToUndo(Currency Amount, Int64 CashierSessionId, Int64 OperationId, SqlTransaction Trx)
    {
      Decimal _total_chips;
      FeatureChips.ChipsOperation _chips_operation;

      _chips_operation = new FeatureChips.ChipsOperation(CashierSessionId, OperationId, 0, 0, CurrencyExchange.GetNationalCurrency(), CASHIER_MOVEMENT.NOT_ASSIGNED);
      _total_chips = 0;

      if (!GamingTablesSessions.GamingTableInfo.IsGamingTable)
      {
        // get chips stock for this operation
        if (!GeneralParam.GetBoolean("GamingTables", "ChipsStockControl", false))
        {
          return false;
        }

        foreach (KeyValuePair<Int64, Int32> _chip_ope in _chips_operation.ChipsUnits)
        {
          if (!FeatureChips.Chips.ContainsKey(_chip_ope.Key)
            || (FeatureChips.Chips.ContainsKey(_chip_ope.Key) && _chip_ope.Value > FeatureChips.Chips.GetChip(_chip_ope.Key).Stock))
          {

            return false;
          }
        }
      }

      if ((Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1) == Cage.ShowDenominationsMode.HideAllDenominations)
      {
        _total_chips = (Int64)Cashier.UpdateSessionBalance(Trx, CashierSessionId, 0, _chips_operation.IsoCode, _chips_operation.CurrencyExchangeType);
      }
      else
      {
        _total_chips += _chips_operation.ChipsAmount;
      }

      // Case for exchange chips
      if (_chips_operation.IsoCode != CurrencyExchange.GetNationalCurrency())
      {
        return (_chips_operation.ChipsAmount.SqlMoney.Value <= _total_chips);
      }

      return (Amount.SqlMoney.Value <= _total_chips);
    } // HasEnoughChipsToUndo

    //------------------------------------------------------------------------------
    // PURPOSE: Check for existing tickets from an Operation.
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId    
    //          - Trx
    //
    //      - OUTPUT:
    //          - TicketsList
    //
    // RETURNS:
    //      - Boolean: True if has a ticket
    //
    private static Boolean HasTickets(Int64 OperationId, out List<Int64> TicketsList, SqlTransaction Trx)
    {
      // Exist
      if (!Ticket.DB_GetTicketsFromOperation(OperationId, out TicketsList))
      {
        return false;
      }
      if (TicketsList.Count == 0)
      {
        return false;
      }

      return true;
    } // HasValidTicket

    //------------------------------------------------------------------------------
    // PURPOSE: Check if operation has any UNR promotion
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean: True if has at least one UNR promotion.
    //
    private static Boolean HasPromoUNR(Int64 OperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _count;

      _sb = new StringBuilder();

      // Read a undo operation.
      _sb.AppendLine("  SELECT   COUNT(*)                         ");
      _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_operation_id)) ");
      _sb.AppendLine("   WHERE   ACP_OPERATION_ID = @pOperationId ");
      _sb.AppendLine("     AND   ISNULL(ACP_PRIZE_TYPE, @pTypeNone) != @pTypeNone  ");
      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _cmd.Parameters.Add("@pTypeNone", SqlDbType.Int).Value = AccountPromotion.PrizeType.None;

        _count = (Int32)_cmd.ExecuteScalar();
      }

      return (_count > 0);

    } // HasPromoUNR


    //------------------------------------------------------------------------------
    // PURPOSE: Check if operation has any Cash DeskDraw PlaySession
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean: True if has at least one CashDeskDrawPlaySession.
    //
    private static Boolean HasCashDeskDrawPlaySession(Int64 OperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _count;

      _sb = new StringBuilder();

      // Read a undo operation.
      _sb.AppendLine("  SELECT   COUNT(*)                                            ");
      _sb.AppendLine("    FROM   ACCOUNT_MOVEMENTS WITH(INDEX(IX_am_operation_id))   ");
      _sb.AppendLine("   WHERE   AM_OPERATION_ID = @pOperationId                     ");
      _sb.AppendLine("     AND   AM_TYPE       in (@pType)                  ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = MovementType.CashdeskDrawPlaySession;

        _count = (Int32)_cmd.ExecuteScalar();
      }

      return (_count > 0);

    } // HasEnoughCashToUndo


    //------------------------------------------------------------------------------
    // PURPOSE: Check if operation has any Cash DeskDraw PlaySession
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean: True if has at least one CashDeskDrawPlaySession.
    //
    private static Boolean HasTerminalDeskDrawPlaySession(Int64 OperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _count;

      _sb = new StringBuilder();

      // Read a undo operation.
      _sb.AppendLine("  SELECT   COUNT(*)                                            ");
      _sb.AppendLine("    FROM   ACCOUNT_MOVEMENTS WITH(INDEX(IX_am_operation_id))   ");
      _sb.AppendLine("   WHERE   AM_OPERATION_ID = @pOperationId                     ");
      _sb.AppendLine("     AND   AM_TYPE       in (@pType, @pType2)                  ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = MovementType.StartCardSession;
        _cmd.Parameters.Add("@pType2", SqlDbType.Int).Value = MovementType.EndCardSession;

        _count = (Int32)_cmd.ExecuteScalar();
      }

      return (_count > 0);

    } // HasEnoughCashToUndo

    //------------------------------------------------------------------------------
    // PURPOSE: Get won amount in cash desk draw
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - PromoRedeemable
    //          - PromoNotRedeemable
    //
    // RETURNS:
    //      - Boolean: True if has found a desk draw.
    //
    private static Boolean GetDeskDrawWon(Int64 OperationId, out Currency PromoRedeemable, out Currency PromoNotRedeemable, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      PromoRedeemable = 0;
      PromoNotRedeemable = 0;

      // Read a undo operation.
      _sb.AppendLine("  SELECT   CD_RE_WON                        ");
      _sb.AppendLine("         , CD_NR_WON                        ");
      _sb.AppendLine("    FROM   CASHDESK_DRAWS                   ");
      _sb.AppendLine("   WHERE   CD_OPERATION_ID = @pOperationId  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            if (!_reader.IsDBNull(0))
            {
              PromoRedeemable = (Currency)_reader.GetDecimal(0);
            }

            if (!_reader.IsDBNull(1))
            {
              PromoNotRedeemable = (Currency)_reader.GetDecimal(1);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetDeskDrawWon

    //------------------------------------------------------------------------------
    // PURPOSE: Check cashier session values to undo operation
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: True if data are ok.
    //
    private static Boolean GetValuesToUndoChipsOperation(Int64 OperationId, out CurrencyIsoType IsoType, out Decimal IsoAmount, out Int64 GamingTableSessionId,
                                                         SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      GamingTableSessionId = 0;

      // Read an undo operation.
      _sb.AppendLine("  SELECT   ISNULL(CM_CURRENCY_ISO_CODE, '') AS CM_CURRENCY_ISO_CODE ");
      _sb.AppendLine("         , ISNULL(CM_GAMING_TABLE_SESSION_ID, 0) AS CM_GAMING_TABLE_SESSION_ID");
      _sb.AppendLine("         , CASE WHEN CM_TYPE = @pChipsPurchaseTotal OR CM_TYPE = @pChipsPurchaseTotalExchange ");
      _sb.AppendLine("                  THEN CM_ADD_AMOUNT ");
      _sb.AppendLine("                  ELSE CM_SUB_AMOUNT END AS AMOUNT  ");
      _sb.AppendLine("         , CM_CAGE_CURRENCY_TYPE ");
      _sb.AppendLine("         , CM_INITIAL_BALANCE");
      _sb.AppendLine("    FROM   CASHIER_MOVEMENTS ");
      _sb.AppendLine("   WHERE   CM_OPERATION_ID = @pOperationId ");
      _sb.AppendLine("     AND   CM_TYPE IN(@pChipsPurchaseTotal, @pChipsSaleTotal, @pChipsPurchaseTotalExchange, @pChipsSaleTotalExchange, @pChipsSaleRemainigAmount, @pChipsSaleConsumedRemainigAmount) ");


      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _cmd.Parameters.Add("@pChipsPurchaseTotal", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL;
        _cmd.Parameters.Add("@pChipsSaleTotal", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_TOTAL;
        _cmd.Parameters.Add("@pChipsPurchaseTotalExchange", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE;
        _cmd.Parameters.Add("@pChipsSaleTotalExchange", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE;
        _cmd.Parameters.Add("@pChipsSaleRemainigAmount", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT;
        _cmd.Parameters.Add("@pChipsSaleConsumedRemainigAmount", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT;

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            IsoType.IsoCode = _reader.GetString(0);
            IsoType.Type = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_reader.GetInt32(3));

            GamingTableSessionId = _reader.GetInt64(1);
            IsoAmount = _reader.GetDecimal(2);

            if (WSI.Common.TITO.Utils.IsTitoMode() && IsoType.IsoCode != CurrencyExchange.GetNationalCurrency())
            {
              IsoAmount = _reader.GetDecimal(4);
            }


            return true;
          }
        }
      }

      return false;
    } // GetCashierSessionValues

    private static Boolean GetOperation(Int64 AccountId,
                                        Int64 OperationId,
                                        Int64 CashierSessionId,
                                        out AccountOperations.Operation AccountOperation,
                                        OperationCode CodeOperation,
                                        Int64 MBAccountId,
                                        SqlTransaction Trx)
    {
      m_mbaccount_id = MBAccountId;
      return GetOperation(AccountId, OperationId, CashierSessionId, out AccountOperation, CodeOperation, Trx);
    } // GetOperation

    //------------------------------------------------------------------------------
    // PURPOSE: Get specific cash in/promotion, chips sale or chips purchase operation of an player account
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountOperation
    //
    // RETURNS:
    //      - Boolean: If the operation has been recovered successfully.
    //
    private static Boolean GetOperation(Int64 AccountId, Int64 OperationId, Int64 CashierSessionId, out AccountOperations.Operation AccountOperation, OperationCode CodeOperation, SqlTransaction Trx, Boolean UseTodayOpening = false)
    {
      StringBuilder _sb;
      Int32 _undoable_minutes;

      _sb = new StringBuilder();
      AccountOperation = new AccountOperations.Operation();
      _undoable_minutes = GeneralParam.GetInt32("Cashier.Undo", "Allowed.Minutes");

      if (_undoable_minutes == 0)
      {
        return false;
      }

      _sb.AppendLine(" SELECT TOP 1 AO_OPERATION_ID                    ");
      _sb.AppendLine("            , AO_ACCOUNT_ID                      ");
      _sb.AppendLine("            , AO_CODE                            ");
      _sb.AppendLine("            , AO_CASHIER_SESSION_ID              ");
      _sb.AppendLine("            , AO_MB_ACCOUNT_ID                   ");
      _sb.AppendLine("            , AO_AMOUNT                          ");
      _sb.AppendLine("            , AO_REDEEMABLE                      ");
      _sb.AppendLine("            , AO_NON_REDEEMABLE                  ");
      _sb.AppendLine("            , AO_NON_REDEEMABLE2                 ");
      _sb.AppendLine("            , AO_WON_LOCK                        ");
      _sb.AppendLine("            , AO_PROMO_ID                        ");
      _sb.AppendLine("            , AO_PROMO_REDEEMABLE                ");
      _sb.AppendLine("            , AO_PROMO_NOT_REDEEMABLE            ");
      _sb.AppendLine("            , AO_OPERATION_DATA                  ");
      _sb.AppendLine("            , AO_AUTOMATICALLY_PRINTED           ");
      _sb.AppendLine("            , AO_USER_ID                         ");
      _sb.AppendLine("            , AO_UNDO_STATUS                     ");
      _sb.AppendLine("            , AO_UNDO_OPERATION_ID               ");
      _sb.AppendLine("       FROM   ACCOUNT_OPERATIONS                 ");

      if (UseTodayOpening)
        _sb.AppendLine("      WHERE   AO_DATETIME  > @pTodayOpening");
      else
        _sb.AppendLine("      WHERE   AO_DATETIME  >= DATEADD(mi, -(@pUndoableMinutes), GETDATE()) ");

      switch (CodeOperation)
      {
        case OperationCode.HANDPAY:
          _sb.AppendLine("    AND   AO_CODE IN (@pHandpay) ");
          break;
        case OperationCode.CASH_IN:
        case OperationCode.PROMOTION:
        case OperationCode.PROMOTION_WITH_TAXES:
        case OperationCode.PRIZE_PAYOUT:
        case OperationCode.PRIZE_PAYOUT_AND_RECHARGE:
          _sb.AppendLine("    AND   AO_CODE IN (@pCashIn, @pPromotion, @pPromotionWithTaxes, @pPrizePayout, @pPrizePayoutAndRecharge ) ");
          break;
        case OperationCode.GIFT_DELIVERY:
          _sb.AppendLine("    AND   AO_CODE IN (@pGiftDelivery, @pGiftDeliveryAsCashIn) ");
          break;
        case OperationCode.TITO_REISSUE:
          _sb.AppendLine("    AND   AO_CODE = @pTitoReissue ");
          break;
        case OperationCode.TITO_OFFLINE:
          _sb.AppendLine("    AND   AO_CODE = @pTitoOffline ");
          break;
        case OperationCode.CHIPS_SALE:
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:

          _sb.AppendLine("    AND   AO_CODE IN (@pOperationCodeChipsSale, @pOperationCodeChipsSaleWithRecharge) ");

          // Virtual Account
          if (AccountId == 0)
          {
            _sb.AppendLine("  AND   AO_CASHIER_SESSION_ID = @pCashierSessionId  ");
          }
          break;
        case OperationCode.CHIPS_PURCHASE:
        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
        case OperationCode.CASH_ADVANCE:
          _sb.AppendLine("    AND   AO_CODE = @pOperationCode ");
          // Virtual Account
          if (AccountId == 0)
          {
            _sb.AppendLine("  AND   AO_CASHIER_SESSION_ID = @pCashierSessionId  ");
          }
          break;
        case OperationCode.MB_CASH_IN:
        case OperationCode.MB_DEPOSIT:
          _sb.AppendLine("    AND   AO_CODE IN(@pOperationCode, @pOperationCodeAux) ");
          _sb.AppendLine("    AND   AO_MB_ACCOUNT_ID = @pMBAccountId ");
          break;
        case OperationCode.CASH_WITHDRAWAL:
          _sb.AppendLine("    AND   AO_CODE = @pCashWithdrawalUndo");
          break;
        case OperationCode.TERMINAL_COLLECT_DROPBOX:
        case OperationCode.TERMINAL_REFILL_HOPPER:
        case OperationCode.CASHIER_COLLECT_REFILL:
        case OperationCode.CASHIER_COLLECT_REFILL_CAGE:
          _sb.AppendLine("    AND   AO_CODE = @pOperationCode ");
          _sb.AppendLine("    AND   AO_CASHIER_SESSION_ID = @pCashierSessionId  ");
          break;
        case OperationCode.CURRENCY_EXCHANGE_CHANGE:
        case OperationCode.CURRENCY_EXCHANGE_DEVOLUTION:
        case OperationCode.CUSTOMER_ENTRANCE:
          _sb.AppendLine("    AND   AO_CODE = @pOperationCode ");
          _sb.AppendLine("    AND   AO_CASHIER_SESSION_ID = @pCashierSessionId  ");
          break;
        case OperationCode.CLOSE_OPERATION:
        case OperationCode.REOPEN_OPERATION:
          _sb.AppendLine("    AND   AO_CODE = @pOperationCode ");
          break;
        default:
          return false;
      }

      // Not Virtual Account
      if (AccountId > 0)
      {
        _sb.AppendLine("      AND   AO_ACCOUNT_ID = @pAccountId  ");
      }

      if (!TITO.Utils.IsTitoMode() && CodeOperation != OperationCode.CASH_WITHDRAWAL && CodeOperation != OperationCode.CLOSE_OPERATION && CodeOperation != OperationCode.REOPEN_OPERATION)
      {
        _sb.AppendLine("      AND   AO_AMOUNT  <> 0");
      }

      if (OperationId > 0)
      {
        _sb.AppendLine("      AND   AO_OPERATION_ID = @pOperationId        ");
      }

      _sb.AppendLine("   ORDER BY  AO_OPERATION_ID DESC                ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          switch (CodeOperation)
          {
            case OperationCode.HANDPAY:
              _cmd.Parameters.Add("@pHandpay", SqlDbType.Int).Value = OperationCode.HANDPAY;
              break;
            case OperationCode.CASH_IN:
            case OperationCode.PROMOTION:
            case OperationCode.PROMOTION_WITH_TAXES:
            case OperationCode.PRIZE_PAYOUT:
            case OperationCode.PRIZE_PAYOUT_AND_RECHARGE:
              _cmd.Parameters.Add("@pCashIn", SqlDbType.Int).Value = OperationCode.CASH_IN;
              _cmd.Parameters.Add("@pPromotion", SqlDbType.Int).Value = OperationCode.PROMOTION;
              _cmd.Parameters.Add("@pPromotionWithTaxes", SqlDbType.Int).Value = OperationCode.PROMOTION_WITH_TAXES;
              _cmd.Parameters.Add("@pPrizePayout", SqlDbType.Int).Value = OperationCode.PRIZE_PAYOUT;
              _cmd.Parameters.Add("@pPrizePayoutAndRecharge", SqlDbType.Int).Value = OperationCode.PRIZE_PAYOUT_AND_RECHARGE;
              break;
            case OperationCode.GIFT_DELIVERY:
              _cmd.Parameters.Add("@pGiftDelivery", SqlDbType.Int).Value = OperationCode.GIFT_DELIVERY;
              _cmd.Parameters.Add("@pGiftDeliveryAsCashIn", SqlDbType.Int).Value = OperationCode.GIFT_REDEEMABLE_AS_CASHIN;
              break;
            case OperationCode.TITO_OFFLINE:
              _cmd.Parameters.Add("@pTitoOffline", SqlDbType.Int).Value = OperationCode.TITO_OFFLINE;
              break;
            case OperationCode.TITO_REISSUE:
              _cmd.Parameters.Add("@pTitoReissue", SqlDbType.Int).Value = OperationCode.TITO_REISSUE;
              break;
            case OperationCode.CHIPS_SALE:
            case OperationCode.CHIPS_SALE_WITH_RECHARGE:

              _cmd.Parameters.Add("@pOperationCodeChipsSale", SqlDbType.Int).Value = OperationCode.CHIPS_SALE;
              _cmd.Parameters.Add("@pOperationCodeChipsSaleWithRecharge", SqlDbType.Int).Value = OperationCode.CHIPS_SALE_WITH_RECHARGE;

              // Virtual Account
              if (AccountId == 0)
              {
                _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = CashierSessionId;
              }
              break;
            case OperationCode.CHIPS_PURCHASE:
              _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = OperationCode.CHIPS_PURCHASE;
              // Virtual Account
              if (AccountId == 0)
              {
                _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = CashierSessionId;
              }
              break;
            case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
              _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT;
              // Virtual Account
              if (AccountId == 0)
              {
                _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = CashierSessionId;
              }
              break;

            case OperationCode.CASH_ADVANCE:
              _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = OperationCode.CASH_ADVANCE;
              // Virtual Account
              if (AccountId == 0)
              {
                _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = CashierSessionId;
              }
              break;

            case OperationCode.MB_CASH_IN:
            case OperationCode.MB_DEPOSIT:
              _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = OperationCode.MB_CASH_IN;
              _cmd.Parameters.Add("@pOperationCodeAux", SqlDbType.Int).Value = OperationCode.MB_DEPOSIT;
              _cmd.Parameters.Add("@pMBAccountId", SqlDbType.BigInt).Value = m_mbaccount_id;
              break;

            case OperationCode.CASH_WITHDRAWAL:
              _cmd.Parameters.Add("@pCashWithdrawalUndo", SqlDbType.Int).Value = OperationCode.CASH_WITHDRAWAL;

              break;

            case OperationCode.TERMINAL_COLLECT_DROPBOX:
              _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = OperationCode.TERMINAL_COLLECT_DROPBOX;
              // Virtual Account
              if (AccountId == 0)
              {
                _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = CashierSessionId;
              }
              break;
            case OperationCode.TERMINAL_REFILL_HOPPER:
              _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = OperationCode.TERMINAL_REFILL_HOPPER;
              // Virtual Account
              if (AccountId == 0)
              {
                _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = CashierSessionId;
              }
              break;
            case OperationCode.CURRENCY_EXCHANGE_CHANGE:
            case OperationCode.CURRENCY_EXCHANGE_DEVOLUTION:
            case OperationCode.CUSTOMER_ENTRANCE:
              _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = CodeOperation;
              _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = CashierSessionId;
              break;
            case OperationCode.CLOSE_OPERATION:
            case OperationCode.REOPEN_OPERATION:
              _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = CodeOperation;

              break;
          }

          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
          _cmd.Parameters.Add("@pUndoableMinutes", SqlDbType.Int).Value = _undoable_minutes;
          _cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = Misc.TodayOpening();

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            AccountOperation.OperationId = _reader.GetInt64(0);
            AccountOperation.AccountId = _reader.GetInt64(1);
            AccountOperation.Code = (OperationCode)_reader.GetInt32(2);

            if (!_reader.IsDBNull(3))
            {
              AccountOperation.CashierSessionId = _reader.GetInt64(3);
            }

            if (!_reader.IsDBNull(4))
            {
              AccountOperation.MbAccountId = _reader.GetInt64(4);
            }

            if (!_reader.IsDBNull(5))
            {
              AccountOperation.Amount = _reader.GetDecimal(5);
            }
            else
            {
              AccountOperation.Amount = 0;
            }

            AccountOperation.Redeemable = _reader.GetDecimal(6);

            if (!_reader.IsDBNull(7))
            {
              AccountOperation.NonRedemable = _reader.GetDecimal(7);
            }

            if (!_reader.IsDBNull(8))
            {
              AccountOperation.NonRedemable2 = _reader.GetDecimal(8);
            }

            if (!_reader.IsDBNull(9))
            {
              AccountOperation.WonLock = _reader.GetDecimal(9);
            }

            if (!_reader.IsDBNull(10))
            {
              AccountOperation.PromoId = _reader.GetInt64(10);
            }

            AccountOperation.PromoRedeemable = _reader.GetDecimal(11);
            AccountOperation.PromoNonRedeemable = _reader.GetDecimal(12);

            if (!_reader.IsDBNull(13))
            {
              AccountOperation.OperationData = _reader.GetInt64(13);
            }

            AccountOperation.AutomaticallyPrinted = _reader.GetBoolean(14);

            if (!_reader.IsDBNull(15))
            {
              AccountOperation.UserId = _reader.GetInt64(15);
            }

            if (!_reader.IsDBNull(16))
            {
              AccountOperation.UndoStatus = (UndoStatus)_reader.GetInt32(16);
            }

            if (!_reader.IsDBNull(17))
            {
              AccountOperation.UndoOperationId = _reader.GetInt64(17);
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetOperation

    public static Boolean IsTITOReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId, OperationCode OpCode,
                                           out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
                                           out UndoError Error, out List<Int64> TicketList, SqlTransaction Trx)
    {
      Ticket _ticket;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      Error = UndoError.Unknown;
      TicketList = new List<Int64>();

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OpCode, Trx)) //CASH_In represents CAHS_IN and PROMOTION too
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.CASH_IN && AccountOperation.Code != OperationCode.PROMOTION)
        {
          if (TITO.Utils.IsTitoMode() &&
              AccountOperation.Code != OperationCode.GIFT_DELIVERY &&
              AccountOperation.Code != OperationCode.GIFT_REDEEMABLE_AS_CASHIN &&
              AccountOperation.Code != OperationCode.TITO_OFFLINE &&
              AccountOperation.Code != OperationCode.TITO_REISSUE)
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code == OperationCode.CASH_IN || AccountOperation.Code == OperationCode.PROMOTION)
        {
          if (!TITO.Utils.IsTitoMode())
          {
            // Check if account has enough redeemable to undo operation
            if (CardData.AccountBalance.TotalRedeemable <
                (AccountOperation.Redeemable + AccountOperation.PromoRedeemable))
            {
              Error = UndoError.InsuficientBalance;

              return false;
            }
          }

          if (AccountOperation.Redeemable > 0)
          {
            // Check if you have enough cash in the cash session to undo the operation
            if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
            {
              Error = UndoError.InsuficientCash;

              return false;
            }
          }

          // Check if it has no UNR promotions
          if (HasPromoUNR(AccountOperation.OperationId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }

          // Check if it has no Cash Desk Draw PlaySession
          if (HasCashDeskDrawPlaySession(AccountOperation.OperationId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
          if (HasTerminalDeskDrawPlaySession(AccountOperation.OperationId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }

        }

        if (Ticket.DB_GetTicketsFromOperation(OperationId, out TicketList, Trx))
        {
          foreach (Int64 _ticket_id in TicketList)
          {
            _ticket = Ticket.LoadTicket(_ticket_id);
            if (_allow_pay_tickets_pending_print)
            {
              if (_ticket.Status != TITO_TICKET_STATUS.VALID && _ticket.Status != TITO_TICKET_STATUS.PENDING_PRINT)
              {
                Error = UndoError.TicketNotValid;

                return false;
              }
            }
            else
            {
              if (_ticket.Status != TITO_TICKET_STATUS.VALID)
              {
                Error = UndoError.TicketNotValid;

                return false;
              }
            }
          }
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean IsTITOTicketAvoidable(CardData CardData, Int64 OperationId, Int64 CashierSessionId, OperationCode OpCode, List<Ticket> TicketsUndo,
                                           out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
                                           out UndoError Error, out Int64 TicketNoDiscardable, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      Error = UndoError.Unknown;
      TicketNoDiscardable = 0;

      try
      {
        //Check if any ticket has status played
        if (!GetTicketNoDiscardable(TicketsUndo, out TicketNoDiscardable, Trx))
        {
          Error = UndoError.TicketNotValid;

          return false;
        }

        if (TicketNoDiscardable > 0)
        {
          Error = UndoError.TicketNotValid;

          return false;
        }

        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OpCode, Trx)) //CASH_In represents CAHS_IN and PROMOTION too
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        AccountOperation.Redeemable = 0;
        AccountOperation.PromoRedeemable = 0;
        AccountOperation.PromoNonRedeemable = 0;
        AccountOperation.Amount = 0;
        AccountOperation.NonRedemable = 0;

        foreach (Ticket _ticket in TicketsUndo)
        {
          switch (_ticket.TicketType)
          {
            case TITO_TICKET_TYPE.CASHABLE:
              AccountOperation.Redeemable += _ticket.Amount;
              AccountOperation.Amount += _ticket.Amount;
              break;
            case TITO_TICKET_TYPE.PROMO_REDEEM:
              AccountOperation.PromoRedeemable += _ticket.Amount;
              AccountOperation.NonRedemable += _ticket.Amount;
              break;
            case TITO_TICKET_TYPE.PROMO_NONREDEEM:
              AccountOperation.PromoNonRedeemable += _ticket.Amount;
              AccountOperation.NonRedemable += _ticket.Amount;
              break;
          }
        }

        if (AccountOperation.Code != OperationCode.CASH_IN && AccountOperation.Code != OperationCode.PROMOTION)
        {
          if (TITO.Utils.IsTitoMode() &&
              AccountOperation.Code != OperationCode.GIFT_DELIVERY &&
              AccountOperation.Code != OperationCode.GIFT_REDEEMABLE_AS_CASHIN &&
              AccountOperation.Code != OperationCode.TITO_OFFLINE &&
              AccountOperation.Code != OperationCode.TITO_REISSUE)
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code == OperationCode.CASH_IN || AccountOperation.Code == OperationCode.PROMOTION)
        {
          if (AccountOperation.Redeemable > 0)
          {
            // TODO: REVISAR!!!
            // Check if you have enough cash in the cash session to undo the operation
            if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
            {
              Error = UndoError.InsuficientCash;

              return false;
            }

            // Check if it has no Cash Desk Draw PlaySession
            if (HasCashDeskDrawPlaySession(AccountOperation.OperationId, Trx))
            {
              Error = UndoError.NoReversibleOperations;

              return false;
            }
            if (HasTerminalDeskDrawPlaySession(AccountOperation.OperationId, Trx))
            {
              Error = UndoError.NoReversibleOperations;

              return false;
            }

          }

          // Check if it has no UNR promotions
          if (HasPromoUNR(AccountOperation.OperationId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if the tickets list has valid state to disable.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Ticket List
    //          - Trx
    //
    //      - OUTPUT:
    //          - Ticket Error
    //
    // RETURNS:
    //      - Boolean: If the operation has been successfully.
    //
    private static Boolean GetTicketNoDiscardable(List<Ticket> Tickets, out Int64 TicketNoDiscardable, SqlTransaction Trx)
    {
      StringBuilder _str_query;
      String _str_tickets;
      Object _obj;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      _str_query = new StringBuilder();
      _str_tickets = string.Empty;
      TicketNoDiscardable = 0;

      try
      {

        foreach (Ticket _ticket in Tickets)
        {
          _str_tickets += _ticket.TicketID + ",";
        }

        _str_tickets = _str_tickets.TrimEnd(new Char[] { ',' });
        //_str_tickets = _str_tickets.Substring(0, _str_tickets.Length - 1);

        _str_query.AppendLine("  SELECT   TI_VALIDATION_NUMBER");
        _str_query.AppendLine("    FROM   TICKETS");
        _str_query.AppendLine("   WHERE   TI_TICKET_ID IN ( " + _str_tickets.ToString() + " )");

        if (_allow_pay_tickets_pending_print)
        {
          _str_query.AppendLine("     AND   TI_STATUS <> @pValidStatus");
          _str_query.AppendLine("     AND   TI_STATUS <> @pPendingPrintStatus");
        }
        else
        {
          _str_query.AppendLine("     AND   TI_STATUS <> @pValidStatus");
        }

        _str_query.AppendLine("ORDER BY   TI_TICKET_ID DESC");

        using (SqlCommand _cmd = new SqlCommand(_str_query.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pValidStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID;
          if (_allow_pay_tickets_pending_print) _cmd.Parameters.Add("@pPendingPrintStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.PENDING_PRINT;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null)
          {
            TicketNoDiscardable = (Int64)_obj;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    private static Boolean IsHandpayReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId,
                                                 out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
                                                 out UndoError Error, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      Error = UndoError.Unknown;

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.HANDPAY, Trx)) //CASH_In represents CAHS_IN and PROMOTION too
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Redeemable > 0)
        {
          // Check if you have enough cash in the cash session to undo the operation
          if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
          {
            Error = UndoError.InsuficientCash;

            return false;
          }
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if specific operation of cash in/promotion can be undone
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData
    //          - CashierSessionId
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountOperation: Account Operation to be undone
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: 
    //            - True: If it can be undone
    //            - False: Otherwise
    //
    public static Boolean IsCashInReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId,
                                                 out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
                                                 out UndoError Error, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      Error = UndoError.Unknown;

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.CASH_IN, Trx)) //CASH_In represents CAHS_IN and PROMOTION too
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.CASH_IN && AccountOperation.Code != OperationCode.PROMOTION && AccountOperation.Code != OperationCode.PROMOTION_WITH_TAXES)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code == OperationCode.CASH_IN || AccountOperation.Code == OperationCode.PROMOTION || AccountOperation.Code == OperationCode.PROMOTION_WITH_TAXES)
        {
          if (!TITO.Utils.IsTitoMode())
          {
            // Check if account has enough redeemable to undo operation
            if (CardData.AccountBalance.TotalRedeemable < (AccountOperation.Redeemable + AccountOperation.PromoRedeemable))
            {
              Error = UndoError.InsuficientBalance;

              return false;
            }
          }

          if (AccountOperation.Redeemable > 0)
          {
            if (AccountOperation.Code == OperationCode.PROMOTION_WITH_TAXES)
            {

              // Check if you have enough cash in the cash session to undo the operation
              if (!HasEnoughCashToUndo(AccountOperation, CashierSessionId, out IsoType, out IsoAmount, Trx))
              {
                Error = UndoError.InsuficientCash;

                return false;
              }
            }
            else
            {

              // Check if you have enough cash in the cash session to undo the operation
              if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
              {
                Error = UndoError.InsuficientCash;

                return false;
              }
            }
          }

          if (AccountPromotion.GetAccountPromotionsByStatus(0, ACCOUNT_PROMO_STATUS.CANCELLED_NOT_ADDED).Select("ACP_OPERATION_ID = " + AccountOperation.OperationId).Length > 0)
          {
            Error = UndoError.CanceledPromotions;

            return false;
          }

          // Check if it has no UNR promotions
          if (HasPromoUNR(AccountOperation.OperationId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }

          // Check if it has no Cash Desk Draw PlaySession
          if (HasCashDeskDrawPlaySession(AccountOperation.OperationId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
          if (HasTerminalDeskDrawPlaySession(AccountOperation.OperationId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }

        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsCashInReversible

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if specific operation of chips sale can be undone
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData
    //          - CashierSessionId
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountOperation: Account Operation to be undone
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: 
    //            - True: If it can be undone
    //            - False: Otherwise
    public static Boolean IsChipsSaleReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId,
                                                 out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
                                                 out Int64 GamingTableSessionId, out List<Int64> TicketsList, out UndoError Error, SqlTransaction Trx)
    {
      CurrencyIsoType _iso_type_national_cur;
      Ticket _ticket;
      bool _allow_pay_tickets_pending_print;

      _iso_type_national_cur = new CurrencyIsoType();
      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      TicketsList = new List<Int64>();

      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      GamingTableSessionId = 0;
      Error = UndoError.Unknown;

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.CHIPS_SALE, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.CHIPS_SALE && AccountOperation.Code != OperationCode.CHIPS_SALE_WITH_RECHARGE)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code == OperationCode.CHIPS_SALE)
        {
          // Get CashierMovements Values
          if (!GetValuesToUndoChipsOperation(AccountOperation.OperationId, out IsoType, out IsoAmount, out GamingTableSessionId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }

          //Check if exists a valid ticket for this operation
          if (HasTickets(AccountOperation.OperationId, out TicketsList, Trx))
          {
            // Valid
            foreach (Int64 _ticket_id in TicketsList)
            {
              _ticket = Ticket.LoadTicket(_ticket_id);

              if (_ticket.TicketType == TITO_TICKET_TYPE.CHIPS_DEALER_COPY && _ticket.Status != TITO_TICKET_STATUS.VALID)
              {
                Error = UndoError.TicketChipsDealerCopyNotValid;
                return false;
              }

              if (_allow_pay_tickets_pending_print)
              {
                if (_ticket.Status != TITO_TICKET_STATUS.VALID && _ticket.Status != TITO_TICKET_STATUS.PENDING_PRINT)
                {
                  Error = UndoError.TicketNotValid;

                  return false;
                }
              }
              else
              {
                if (_ticket.Status != TITO_TICKET_STATUS.VALID)
                {
                  Error = UndoError.TicketNotValid;

                  return false;
                }
              }

            }
          }
        }
        else if (AccountOperation.Code == OperationCode.CHIPS_SALE_WITH_RECHARGE)
        {
          // Get CashierMovements Values. this is necessary here to obtain the GamingTableSessionId
          if (!GetValuesToUndoChipsOperation(AccountOperation.OperationId, out IsoType, out IsoAmount, out GamingTableSessionId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
          // Check if you have enough cash in the cash session to undo the operation
          if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out _iso_type_national_cur, out IsoAmount, Trx))
          {
            Error = UndoError.InsuficientCash;

            return false;
          }
          //Check if exists a valid ticket for this operation
          if (HasTickets(AccountOperation.OperationId, out TicketsList, Trx))
          {
            // Valid
            foreach (Int64 _ticket_id in TicketsList)
            {
              _ticket = Ticket.LoadTicket(_ticket_id);

              if (_ticket.TicketType == TITO_TICKET_TYPE.CHIPS_DEALER_COPY && _ticket.Status != TITO_TICKET_STATUS.VALID)
              {
                Error = UndoError.TicketChipsDealerCopyNotValid;

                return false;
              }

              if (_allow_pay_tickets_pending_print)
              {
                if (_ticket.Status != TITO_TICKET_STATUS.VALID && _ticket.Status != TITO_TICKET_STATUS.PENDING_PRINT)
                {
                  Error = UndoError.TicketNotValid;

                  return false;
                }
              }
              else
              {
                if (_ticket.Status != TITO_TICKET_STATUS.VALID)
                {
                  Error = UndoError.TicketNotValid;

                  return false;
                }
              }

            }
          }
          // LTC 22-SEP-2016
          // Check if it has no Cash Desk Draw PlaySession
          if (HasCashDeskDrawPlaySession(AccountOperation.OperationId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
          if (HasTerminalDeskDrawPlaySession(AccountOperation.OperationId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }

        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsChipsSaleReversible

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if specific operation of chips sale can be undone. This function it's only for TITO mode.
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData
    //          - CashierSessionId
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountOperation: Account Operation to be undone
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: 
    //            - True: If it can be undone
    //            - False: Otherwise
    //public static Boolean IsChipsSaleWithRechargeReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId,
    //                                             out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
    //                                             out Int64 GamingTableSessionId, out List<Int64> TicketsList, out UndoError Error, SqlTransaction Trx)
    //{
    //  Ticket _ticket;
    //  CurrencyIsoType _iso_type_national_cur;
    //  bool _allow_pay_tickets_pending_print;

    //  _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
    //  _ticket = new Ticket();
    //  AccountOperation = new AccountOperations.Operation();
    //  IsoType = new CurrencyIsoType();
    //  _iso_type_national_cur = new CurrencyIsoType();
    //  IsoAmount = 0;
    //  GamingTableSessionId = 0;
    //  Error = UndoError.Unknown;
    //  TicketsList = new List<Int64>();


    //  try
    //  {
    //    if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.CHIPS_SALE_WITH_RECHARGE, Trx))
    //    {
    //      Error = UndoError.NoReversibleOperations;

    //      return false;
    //    }

    //    if (AccountOperation.Code != OperationCode.CHIPS_SALE_WITH_RECHARGE)
    //    {
    //      Error = UndoError.NoReversibleOperations;

    //      return false;
    //    }

    //    if (AccountOperation.UndoStatus != UndoStatus.None)
    //    {
    //      Error = UndoError.NoReversibleOperations;

    //      return false;
    //    }

    //    if (AccountOperation.Code == OperationCode.CHIPS_SALE_WITH_RECHARGE)
    //    {
    //      // Get CashierMovements Values. this is necessary here to obtain the GamingTableSessionId
    //      if (!GetValuesToUndoChipsOperation(AccountOperation.OperationId, out IsoType, out IsoAmount, out GamingTableSessionId, Trx))
    //      {
    //        Error = UndoError.NoReversibleOperations;

    //        return false;
    //      }
    //      // Check if you have enough cash in the cash session to undo the operation
    //      if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out _iso_type_national_cur, out IsoAmount, Trx))
    //      {
    //        Error = UndoError.InsuficientCash;

    //        return false;
    //      }
    //      //Check if exists a valid ticket for this operation
    //      if (HasTickets(AccountOperation.OperationId, out TicketsList, Trx))
    //      {
    //        // Valid
    //        foreach (Int64 _ticket_id in TicketsList)
    //        {
    //          _ticket = Ticket.LoadTicket(_ticket_id);

    //          if (_ticket.TicketType == TITO_TICKET_TYPE.CHIPS_DEALER_COPY && _ticket.Status != TITO_TICKET_STATUS.VALID)
    //          {
    //            Error = UndoError.TicketChipsDealerCopyNotValid;

    //            return false;
    //          }

    //          if (_allow_pay_tickets_pending_print)
    //          {
    //            if (_ticket.Status != TITO_TICKET_STATUS.VALID && _ticket.Status != TITO_TICKET_STATUS.PENDING_PRINT)
    //            {
    //              Error = UndoError.TicketNotValid;

    //              return false;
    //            }
    //          }
    //          else
    //          {
    //            if (_ticket.Status != TITO_TICKET_STATUS.VALID)
    //            {
    //              Error = UndoError.TicketNotValid;

    //              return false;
    //            }
    //          }

    //        }
    //      }
    //      // LTC 22-SEP-2016
    //      // Check if it has no Cash Desk Draw PlaySession
    //      if (HasCashDeskDrawPlaySession(AccountOperation.OperationId, Trx))
    //      {
    //        Error = UndoError.NoReversibleOperations;

    //        return false;
    //      }
    //      if (HasTerminalDeskDrawPlaySession(AccountOperation.OperationId, Trx))
    //      {
    //        Error = UndoError.NoReversibleOperations;

    //        return false;
    //      }

    //    }

    //    Error = UndoError.None;

    //    return true;
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);
    //  }

    //  return false;
    //} // IsChipsSaleWithRechargeReversible

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if specific operation of chips purchase with cash out can be undone
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData
    //          - CashierSessionId
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountOperation: Account Operation to be undone
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: 
    //            - True: If it can be undone
    //            - False: Otherwise
    public static Boolean IsChipsPurchaseWithCashOutReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId,
                                                 out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
                                                 out Int64 GamingTableSessionId, out List<Int64> TicketsList, out UndoError Error, SqlTransaction Trx)
    {
      Ticket _ticket;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      _ticket = new Ticket();
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      GamingTableSessionId = 0;
      Error = UndoError.Unknown;
      TicketsList = new List<Int64>();

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code == OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT)
        {
          // Check if the cashier has enough chips to undo operation
          if (!(HasEnoughChipsToUndo(AccountOperation.Amount, CashierSessionId, AccountOperation.OperationId, Trx)))
          {
            Error = UndoError.InsuficientChips;

            return false;
          }

          if (TITO.Utils.IsTitoMode())
          {
            if (HasTickets(AccountOperation.OperationId, out TicketsList, Trx))
            {
              // Valid
              foreach (Int64 _ticket_id in TicketsList)
              {
                _ticket = Ticket.LoadTicket(_ticket_id);
                if (_allow_pay_tickets_pending_print)
                {
                  if (_ticket.Status != TITO_TICKET_STATUS.VALID && _ticket.Status != TITO_TICKET_STATUS.PENDING_PRINT)
                  {
                    Error = UndoError.TicketNotValid;

                    return false;
                  }
                }
                else
                {
                  if (_ticket.Status != TITO_TICKET_STATUS.VALID)
                  {
                    Error = UndoError.TicketNotValid;

                    return false;
                  }
                }

              }
            }
          }

          // Get CashierMovements Values
          if (!GetValuesToUndoChipsOperation(AccountOperation.OperationId, out IsoType, out IsoAmount, out GamingTableSessionId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsChipsPurchaseWithCashOutReversible


    //------------------------------------------------------------------------------
    // PURPOSE: Checks if specific operation of chips purchase can be undone
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData
    //          - CashierSessionId
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountOperation: Account Operation to be undone
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: 
    //            - True: If it can be undone
    //            - False: Otherwise
    public static Boolean IsChipsPurchaseReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId,
                                                 out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
                                                 out Int64 GamingTableSessionId, out List<Int64> TicketsList, out UndoError Error, SqlTransaction Trx)
    {
      Ticket _ticket;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      _ticket = new Ticket();
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      GamingTableSessionId = 0;
      Error = UndoError.Unknown;
      TicketsList = new List<Int64>();

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.CHIPS_PURCHASE, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.CHIPS_PURCHASE)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code == OperationCode.CHIPS_PURCHASE)
        {
          // Check if the cashier has enough chips to undo operation
          // DLL 17-FEB-2014: cashier and gaming table with cashier need to check chips balance
          if (!(HasEnoughChipsToUndo(AccountOperation.Amount, CashierSessionId, AccountOperation.OperationId, Trx)))
          {
            Error = UndoError.InsuficientChips;

            return false;
          }

          if (!TITO.Utils.IsTitoMode())
          {
            // Check if account has enough redeemable to undo operation
            if (CardData.AccountBalance.TotalRedeemable < AccountOperation.Amount)
            {
              Error = UndoError.InsuficientBalance;

              return false;
            }
          }
          // Check if exists a valid ticket for this operation
          else
          {
            if (HasTickets(AccountOperation.OperationId, out TicketsList, Trx))
            {
              // Valid
              foreach (Int64 _ticket_id in TicketsList)
              {
                _ticket = Ticket.LoadTicket(_ticket_id);
                if (_allow_pay_tickets_pending_print)
                {
                  if (_ticket.Status != TITO_TICKET_STATUS.VALID && _ticket.Status != TITO_TICKET_STATUS.PENDING_PRINT)
                  {
                    Error = UndoError.TicketNotValid;

                    return false;
                  }
                }
                else
                {
                  if (_ticket.Status != TITO_TICKET_STATUS.VALID)
                  {
                    Error = UndoError.TicketNotValid;

                    return false;
                  }
                }

              }
            }
          }

          // Get CashierMovements Values
          if (!GetValuesToUndoChipsOperation(AccountOperation.OperationId, out IsoType, out IsoAmount, out GamingTableSessionId, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsChipsPurchaseReversible

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if specific operation cash advance
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData
    //          - CashierSessionId
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountOperation: Account Operation to be undone
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: 
    //            - True: If it can be undone
    //            - False: Otherwise
    private static bool IsCashAdvanceReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId, Boolean UseTodayOpening,
                                                 out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
                                                 out Int64 GamingTableSessionId, out List<Int64> TicketsList, out UndoError Error, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      GamingTableSessionId = 0;
      Error = UndoError.Unknown;
      TicketsList = new List<Int64>();

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.CASH_ADVANCE, Trx, UseTodayOpening))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.CASH_ADVANCE)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        // Check if you have enough cash in the cash session to undo the operation
        if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
        {
          Error = UndoError.InsuficientCash;

          return false;
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsCashAdvanceReversible


    private static bool IsMBCashInReversible(MBCardData CardData,
                                             Int64 OperationId,
                                             Int64 CashierSessionId,
                                             OperationCode OpCode,
                                             out AccountOperations.Operation AccOperation,
                                             out CurrencyIsoType IsoType,
                                             out Decimal IsoAmount,
                                             out UndoError Error,
                                             SqlTransaction Trx)
    {
      AccOperation = new AccountOperations.Operation();
      Error = UndoError.Unknown;
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;

      try
      {
        if (!GetOperation(0, OperationId, CashierSessionId, out AccOperation, OpCode, CardData.CardId, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (OpCode != AccOperation.Code)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (!HasEnoughCashToUndo(AccOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
        {
          Error = UndoError.InsuficientCash;

          return false;
        }

        // Check if it has no Cash Desk Draw PlaySession
        if (HasCashDeskDrawPlaySession(AccOperation.OperationId, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsMBCashInReversible

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if specific operation about currency exchange can be undone
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData
    //          - CashierSessionId
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountOperation: Account Operation to be undone
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: 
    //            - True: If it can be undone
    //            - False: Otherwise
    private static bool IsCurrencyExchangeReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId, OperationCode OpCode,
                                                 out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount, out UndoError Error, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      Error = UndoError.Unknown;

      try
      {
        if (OpCode == OperationCode.CURRENCY_EXCHANGE_CHANGE)
        {
          // The last account operation can not be a currency refund
          if (IsCashRefundLastOperation(CardData.AccountId, CashierSessionId, Trx))
          {
            Error = UndoError.NoReversibleOperations;
            return false;
          }
        }

        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OpCode, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.CURRENCY_EXCHANGE_CHANGE && AccountOperation.Code != OperationCode.CURRENCY_EXCHANGE_DEVOLUTION)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        // Check if you have enough cash in the cash session to undo the operation
        if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
        {
          Error = UndoError.InsuficientCash;

          return false;
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsCashAdvanceReversible


    //------------------------------------------------------------------------------
    // PURPOSE: Checks if specific operation customer entrance can be undone
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData
    //          - CashierSessionId
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountOperation: Account Operation to be undone
    //          - IsoType
    //          - IsoAmount
    //
    // RETURNS:
    //      - Boolean: 
    //            - True: If it can be undone
    //            - False: Otherwise
    private static bool IsCustomerEntranceReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId, OperationCode OpCode,
                                                 out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount, out UndoError Error, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      Error = UndoError.Unknown;

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OpCode, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.CUSTOMER_ENTRANCE)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        // Check if you have enough cash in the cash session to undo the operation
        if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
        {
          Error = UndoError.InsuficientCash;

          return false;
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsCashAdvanceReversible

    public static Boolean HasEnoughCashForMobileBankUndo(Currency CurrentDisposed, Currency MovementDisposed)
    {
      return (CurrentDisposed >= MovementDisposed);
    } // HasEnoughCashForMobileBankUndo


    // 18-APR-2016 JRC
    public static bool GetLastBankMovementByOperation(Int64 OperationId,
                                                      out DataTable MBTable)
    {
      DataTable _mbtable = new DataTable();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GetLastBankMovementByOperation(OperationId, out _mbtable, _db_trx.SqlTransaction);
          MBTable = _mbtable;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      MBTable = _mbtable;

      return false;
    }

    // 18-APR-2016 JRC
    public static bool GetLastBankMovementByOperation(Int64 OperationId,
                                                      out DataTable MBTable, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      MBTable = new DataTable();

      _sb.AppendLine("SELECT   MBM_MOVEMENT_ID");
      _sb.AppendLine("       , MBM_TYPE");
      _sb.AppendLine("       , MBM_DATETIME");
      _sb.AppendLine("       , MBM_CASHIER_SESSION_ID");
      _sb.AppendLine("       , MBM_MB_ID");
      _sb.AppendLine("       , MBM_TERMINAL_ID");
      _sb.AppendLine("       , MBM_TERMINAL_NAME");
      _sb.AppendLine("       , MBM_PLAYER_ACCT_ID");
      _sb.AppendLine("       , MBM_PLAYER_TRACKDATA");
      _sb.AppendLine("       , MBM_WCP_SEQUENCE_ID");
      _sb.AppendLine("       , MBM_WCP_TRANSACTION_ID");
      _sb.AppendLine("       , MBM_ACCT_MOVEMENT_ID");
      _sb.AppendLine("       , MBM_CASHIER_ID");
      _sb.AppendLine("       , MBM_CASHIER_NAME");
      _sb.AppendLine("       , MBM_INITIAL_BALANCE");
      _sb.AppendLine("       , MBM_ADD_AMOUNT");
      _sb.AppendLine("       , MBM_SUB_AMOUNT");
      _sb.AppendLine("       , MBM_FINAL_BALANCE");
      _sb.AppendLine("       , MBM_OPERATION_ID");
      _sb.AppendLine("       , MBM_AMOUNT_01");
      _sb.AppendLine("       , MBM_AMOUNT_02");
      _sb.AppendLine("       , MBM_UNDO_STATUS");
      _sb.AppendLine("  FROM   MB_MOVEMENTS");
      _sb.AppendLine(" WHERE   MBM_OPERATION_ID = @pOperationId");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

            using (SqlDataAdapter _adapter = new SqlDataAdapter(_cmd))
            {
              _adapter.Fill(MBTable);
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetLastBankMovementByOperation

    private static bool IsWithdrawReversible(CardData CardData, InputUndoOperation Input, out AccountOperations.Operation AccountOperation, out UndoError Error, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      //IsoType = new CurrencyIsoType();
      //IsoAmount = 0;
      Error = UndoError.Unknown;
      StringBuilder _sb;
      bool _result;

      _result = true;

      if (Input.OperationIdForUndo == 0)
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP 1                     ");
        _sb.AppendLine("           AO_OPERATION_ID           ");
        _sb.AppendLine("         , AO_UNDO_STATUS            ");
        _sb.AppendLine("    FROM   ACCOUNT_OPERATIONS        ");
        _sb.AppendLine("   WHERE   AO_CODE = @OperationCode  ");

        if (Input.CashierSessionInfo != null && Input.CashierSessionInfo.CashierSessionId != 0)
        {
          _sb.AppendLine("   AND   AO_CASHIER_SESSION_ID = @CashierSessionId ");
        }

        _sb.AppendLine("ORDER BY   AO_OPERATION_ID DESC      ");

        using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          sql_command.Parameters.Add("@OperationCode", SqlDbType.BigInt).Value = OperationCode.CASH_WITHDRAWAL;

          if (Input.CashierSessionInfo != null && Input.CashierSessionInfo.CashierSessionId != 0)
          {
            sql_command.Parameters.Add("@CashierSessionId", SqlDbType.BigInt).Value = Input.CashierSessionInfo.CashierSessionId;
          }

          using (SqlDataReader _reader = sql_command.ExecuteReader())
          {
            if (_reader.Read() && _reader.GetInt64(0) > 0 && _reader.IsDBNull(1))
            {
              Input.OperationIdForUndo = _reader.GetInt64(0);
            }
            else
            {
              _result = false;
            }
          }
        }
      }

      if (_result && !GetOperation(CardData.AccountId, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out AccountOperation, OperationCode.CASH_WITHDRAWAL, Trx))
      {
        Error = UndoError.NoReversibleOperations;

        _result = false;
      }

      if (Cage.IsCageEnabled()) //Only check Cage Movements if we have it enabled.
      {
        if (!Cage.IsCageAutoMode())
        {
          Int64 _cage_movement_id;
          Cage.CageStatus _status;

          if (!Cage.GetCageMovementStatusByAccountOperation(Input.OperationIdForUndo, out _cage_movement_id, out _status, Trx))
          {
            _result = false;
          }

          if(_status == Cage.CageStatus.OK || _status == Cage.CageStatus.OkDenominationImbalance || _status == Cage.CageStatus.OkAmountImbalance)
          {
            _result = false;
          }
        }
      }

      return _result;
    }

    private static bool IsCollectInTerminalRevesible(CardData CardData, Int64 OperationId, Int64 CashierSessionId,
                                                 out AccountOperations.Operation AccountOperation, out UndoError Error, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      Error = UndoError.Unknown;

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.TERMINAL_COLLECT_DROPBOX, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.TERMINAL_COLLECT_DROPBOX)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    private static bool IsRefillInTerminalRevesible(CardData CardData, Int64 OperationId, Int64 CashierSessionId,
                                             out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType, out Decimal IsoAmount,
                                             out UndoError Error, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      Error = UndoError.Unknown;

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.CASH_ADVANCE, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.TERMINAL_REFILL_HOPPER)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        // Check if you have enough cash in the cash session to undo the operation
        if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
        {
          Error = UndoError.InsuficientCash;

          return false;
        }

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    private static bool IsCashRefundLastOperation(Int64 AccountId, Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT   TOP 1                                      ");
      _sb.AppendLine("           AO_OPERATION_ID                            ");
      _sb.AppendLine("         , AO_CODE                                    ");
      _sb.AppendLine("    FROM   ACCOUNT_OPERATIONS                         ");
      _sb.AppendLine("   WHERE   AO_CODE IN (@pCurrencyExchange, @pCurrencyExchangeDevolution) ");
      _sb.AppendLine("     AND   AO_CASHIER_SESSION_ID = @pCashierSessionId ");
      _sb.AppendLine("     AND   AO_ACCOUNT_ID = @pAccountId                ");
      _sb.AppendLine("     AND   ISNULL(AO_UNDO_STATUS, 0) = 0              ");

      _sb.AppendLine("ORDER BY   AO_OPERATION_ID DESC                       ");

      using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        sql_command.Parameters.Add("@pCurrencyExchange", SqlDbType.BigInt).Value = OperationCode.CURRENCY_EXCHANGE_CHANGE;
        sql_command.Parameters.Add("@pCurrencyExchangeDevolution", SqlDbType.BigInt).Value = OperationCode.CURRENCY_EXCHANGE_DEVOLUTION;
        sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
        sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

        using (SqlDataReader _reader = sql_command.ExecuteReader())
        {
          if (_reader.Read())
          {
            return (_reader.GetInt32(1) == (Int32)OperationCode.CURRENCY_EXCHANGE_DEVOLUTION);
          }
        }
      }

      return false;
    }

    private static bool IsCloseCashierSessionReversible(CardData CardData, InputUndoOperation Input, out AccountOperations.Operation AccountOperation, out UndoError Error, SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      Error = UndoError.Unknown;
      StringBuilder _sb;
      bool _result;

      _result = true;

      if (Input.OperationIdForUndo == 0)
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP 1                     ");
        _sb.AppendLine("           AO_OPERATION_ID           ");
        _sb.AppendLine("         , AO_UNDO_STATUS            ");
        _sb.AppendLine("    FROM   ACCOUNT_OPERATIONS        ");
        _sb.AppendLine("   WHERE   AO_CODE = @OperationCode  ");

        if (Input.CashierSessionInfo != null && Input.CashierSessionInfo.CashierSessionId != 0)
        {
          _sb.AppendLine("   AND   AO_CASHIER_SESSION_ID = @CashierSessionId ");
        }
        _sb.AppendLine("ORDER BY   AO_OPERATION_ID DESC      ");

        using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          sql_command.Parameters.Add("@OperationCode", SqlDbType.BigInt).Value = OperationCode.CLOSE_OPERATION;

          if (Input.CashierSessionInfo != null && Input.CashierSessionInfo.CashierSessionId != 0)
          {
            sql_command.Parameters.Add("@CashierSessionId", SqlDbType.BigInt).Value = Input.CashierSessionInfo.CashierSessionId;
          }

          using (SqlDataReader _reader = sql_command.ExecuteReader())
          {
            if (_reader.Read() && _reader.GetInt64(0) > 0 && _reader.IsDBNull(1))
            {
              Input.OperationIdForUndo = _reader.GetInt64(0);
            }
            else
            {
              _result = false;
            }
          }
        }
      }

      if (_result && !GetOperation(CardData.AccountId, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out AccountOperation, OperationCode.CLOSE_OPERATION, Trx, true))
      {
        Error = UndoError.NoReversibleOperations;

        _result = false;
      }

      return _result;
    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Performs all proceses to undo an operation
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData : Player CardData
    //          - OperationId : Id of the operation to be undone
    //          - CashierSessionId : Current cashier session Id
    //          - Trx : SQL Transaction
    //
    //      - OUTPUT:
    //          - VoucherList: List of vouchers to be print
    //
    // RETURNS:
    //      - Boolean: If operation has been undone correctly.
    //
    public static Boolean UndoOperation(InputUndoOperation Input, out ArrayList VoucherList, out UndoError Error)
    {
      Boolean _result_ok;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _result_ok = UndoOperation(Input, false, out VoucherList, out Error, _db_trx.SqlTransaction);

        if (_result_ok)
        {
          _db_trx.Commit();
        }
      }

      return _result_ok;
    }

    public static Boolean UndoOperation(InputUndoOperation Input, out ArrayList VoucherList)
    {
      UndoError _dummy_error = UndoError.Unknown;

      return UndoOperation(Input, out VoucherList, out _dummy_error);
    }


    public class InputUndoOperation
    {
      public OperationCode CodeOperation;
      public CardData CardData;
      public MBCardData MBCardData;
      public Int64 OperationIdForUndo;
      public CashierSessionInfo CashierSessionInfo;
      //
      public Int64 OperationId = 0; // When is 0, is created a new operation id
      public Boolean GenerateVoucherForUndo = true;
      public Boolean TransferToTerminalOperation = false;
      public Boolean UseTodayOpening = false;
      public Boolean ForceUndoTPV = false;  // When is true Undo operations are forced without looking balance.
    }

    public static Boolean UndoOperation(InputUndoOperation Input,
                                        Boolean UndoFromGUI,
                                        out ArrayList VoucherList,
                                        SqlTransaction Trx)
    {
      UndoError _dummy_error = UndoError.Unknown;

      return UndoOperation(Input, UndoFromGUI, out VoucherList, out _dummy_error, Trx);
    }


    public static Boolean UndoOperation(InputUndoOperation Input,
                                        Boolean UndoFromGUI,
                                        out ArrayList VoucherList,
                                        out UndoError Error,
                                        SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int64 _undo_operation_id;
      Decimal _max_devolution;
      MultiPromos.AccountBalance _ac_ini_balance;
      MultiPromos.AccountBalance _ac_fin_balance;
      Decimal _ini_points;
      Decimal _fin_points;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      CancellableOperation _cancellable_operation;
      List<CancellableOperation> _cancellable_operation_list;
      Currency _card_price;
      Currency _initial_balance;
      Currency _final_balance;
      Currency _initial_balance_points;
      Currency _final_balance_points;
      Currency _sub_amount;
      Currency _add_amount;
      Currency _amount;
      Currency _operation_amount;
      AccountMovementsTable _ac_mov_table;
      CashierMovementsTable _cm_mov_table;
      MultiPromos.AccountBalance _balance_undone;
      String _iso_code;
      CurrencyExchangeType _currency_type;
      DataTable _dt_account_promotions;
      MultiPromos.PromoBalance _op_promos_balance = new MultiPromos.PromoBalance();
      MultiPromos.PromoBalance _promo_balance = new MultiPromos.PromoBalance();
      TYPE_SPLITS _splits;
      AccountOperations.Operation _account_operation;
      Boolean _card_paid_undone;
      CurrencyExchangeResult _exchange_result;
      String _original_track_data;
      Int64 _original_account_id;
      Int64 _gaming_table_session_id;
      Ticket _ticket;
      List<Int64> _tickets_list;
      Currency _total_chips;
      DataTable _cashier_movements;
      Currency _devolution;
      Boolean _continue_operations;
      DataTable _table_movements;
      CardData _card_data;
      Decimal AmountToChange;
      MBMovementType UndoMovement;
      CreditLines.CreditLineMovements.CreditLineMovementType _credit_line_type;

      CreditLines.CreditLineActions _credit_line_actions;

      VoucherList = new ArrayList();
      _gaming_table_session_id = 0;
      _ticket = null;
      _tickets_list = new List<Int64>();

      _ac_ini_balance = MultiPromos.AccountBalance.Zero;
      _ini_points = 0;
      _iso_type = new CurrencyIsoType(CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CASINO_CHIP_RE);
      _cashier_movements = null;
      _max_devolution = 0;
      _devolution = 0;
      _continue_operations = true;

      Error = UndoError.Unknown;

      _card_data = new CardData();
      _iso_amount = 0;

      try
      {
        if (!Input.ForceUndoTPV)
        {
          if (Input.CodeOperation != OperationCode.MB_DEPOSIT
              && Input.CodeOperation != OperationCode.CASH_WITHDRAWAL
              && Input.CodeOperation != OperationCode.TERMINAL_REFILL_HOPPER
              && Input.CodeOperation != OperationCode.TERMINAL_COLLECT_DROPBOX
              && Input.CodeOperation != OperationCode.CASHIER_COLLECT_REFILL
                && Input.CodeOperation != OperationCode.CASHIER_COLLECT_REFILL_CAGE
                && Input.CodeOperation != OperationCode.CLOSE_OPERATION
                && Input.CodeOperation != OperationCode.REOPEN_OPERATION)
          {
            // Read account balance
            if (!MultiPromos.Trx_UpdateAccountBalance(Input.CardData.AccountId, MultiPromos.AccountBalance.Zero, 0, out _ac_ini_balance, out _ini_points,
                                                      Trx))
            {
              return false;
            }
          }

          // Check if operation is reversible
          CheckIfOperationIsReversible(Input, ref Error, Trx, ref _iso_type, ref _gaming_table_session_id, ref _tickets_list, ref _continue_operations, out _iso_amount, out _account_operation);

          if (!_continue_operations)
          {
            return false;
          }
        }
        else
        {
          if (!GetOperation(Input.CardData.AccountId, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out _account_operation, Input.CodeOperation, Trx))
          {
            Error = UndoError.NoReversibleOperations;

            return false;
          }
        }
        Error = UndoError.Unknown;

        if (Input.OperationId == 0)
        {
          // Insert undo operation in Account operations
          if (!DB_InsertUndoOperation(_account_operation, Input.CashierSessionInfo.CashierSessionId, out _undo_operation_id, Trx))
          {
            return false;
          }

          // Moved Later
          //if (Input.CodeOperation == OperationCode.MB_DEPOSIT)
          //{
          //  if (!MobileBank.DB_MBUndoDeposit(Input.MBCardData, Input.CashierSessionInfo, Input.MBCardData.DisposedCash - _iso_amount, Trx))
          //  {
          //    return false;
          //  }
          //}
        }
        else
        {
          _undo_operation_id = Input.OperationId;
        }

        _initial_balance = _ac_ini_balance.TotalBalance;
        _initial_balance_points = _ini_points;
        _final_balance_points = 0;
        _final_balance = 0;
        _sub_amount = 0;
        _add_amount = 0;
        _card_paid_undone = false;

        // Set cancellable operation information
        _cancellable_operation = SetCancellableOperationInformation(_account_operation);

        // Read a operation account movements without promotions
        ReadAccountMovementWithoutPromo(Input, Trx, _undo_operation_id, _cancellable_operation, ref _initial_balance, ref _final_balance, ref _initial_balance_points, ref _final_balance_points, ref _sub_amount, ref _add_amount, _account_operation, ref _card_paid_undone, out _sb, out _card_price, out _ac_mov_table, out _original_track_data, out _original_account_id);

        _ac_mov_table.SetAccountTrackData(_original_account_id, _original_track_data);

        // Insert new cashier movements and update cashier session values if it is necessary
        if (!DB_InsertUndoCashierMovements(Input.OperationIdForUndo, _undo_operation_id, Input.CashierSessionInfo, Input.ForceUndoTPV, out _exchange_result, out _total_chips, out _cashier_movements, Trx))
        {

          return false;
        }

        if (!UndoFromGUI && Cage.IsCageAutoMode() && Cage.IsCageAutoModeCageStock())
        {
          if (!WSI.Common.GamingHall.CageStock.UpdateUndoCageStock(_cashier_movements, Trx))
          {
            Log.Error("Error in UpdateUndoCageStock()");

            return false;
          }
        }

        // Search for operation on bank_transactions table and insert movement if is necessary
        if (!DB_InsertUndoBankTransactionMovement(Input.OperationIdForUndo, _undo_operation_id, Trx))
        {
          return false;
        }

        if (Input.CodeOperation == OperationCode.MB_CASH_IN ||
            Input.CodeOperation == OperationCode.MB_DEPOSIT)
        {
          if (!DB_InsertUndoMobileBankMovements(Input.OperationIdForUndo, _undo_operation_id, Input.CashierSessionInfo, out AmountToChange, out UndoMovement, Trx))
          {
            return false;
          }

          //MS Moved it here so we know whether undoing Excess or Shortfall

          if (Input.CodeOperation == OperationCode.MB_DEPOSIT)
          {
            if (!MobileBank.DB_MBUndoDeposit(Input.MBCardData, Input.CashierSessionInfo, AmountToChange, UndoMovement, Trx))
            //if (!MobileBank.DB_MBUndoDeposit(Input.MBCardData, Input.CashierSessionInfo, Input.MBCardData.DisposedCash - _iso_amount, UndoMovement, Trx))
            {
              return false;
            }
          }


          // Undo the MB Record Her
        }

        // Undo cashier balance

        UndoCashierBalance(_account_operation, out _amount, out _operation_amount, out _iso_code, out _currency_type);

        // Update TotalSold Value from Cashier Session

        _continue_operations = true;

        UpdateTotalSoldValue(Input, Trx, ref _amount, _account_operation, ref _continue_operations);

        if (!_continue_operations)
        {
          return false;
        }

        CheckIfWasCurrencyExchange(ref _amount, ref _iso_code, ref _currency_type, _account_operation, _exchange_result);

        //LEM 14-JAN-2014: This updating occurs when cashier movements saving (SP_ICM stored procedure). Is not necessary here.
        //// Update Cash Balance
        //Cashier.UpdateSessionBalance(Trx, CashierSessionInfo.CashierSessionId, _amount, _iso_code, _currency_type);

        _cm_mov_table = new CashierMovementsTable(Input.CashierSessionInfo);
        _balance_undone = new MultiPromos.AccountBalance();
        _dt_account_promotions = new DataTable();
        Currency _missing_redeemable = 0;
        Currency _missing_promo_redeemable = 0;
        Currency _redeemable_to_substract = 0;
        Currency _promo_redeemable_to_substract = 0;


        _continue_operations = true;

        _cancellable_operation_list = CalculateValuesOperationWithPromo(Input, Trx, _ac_ini_balance, _iso_type, _cancellable_operation, ref _balance_undone, ref _dt_account_promotions, ref _op_promos_balance, _account_operation, _gaming_table_session_id, _cashier_movements, ref _total_chips, ref _continue_operations, ref _missing_redeemable, ref _missing_promo_redeemable, ref _redeemable_to_substract, ref _promo_redeemable_to_substract);

        if (!_continue_operations)
        {
          return false;
        }


        if (!TITO.Utils.IsTitoMode() || Input.TransferToTerminalOperation)
        {
          if (_account_operation.Code != OperationCode.MB_DEPOSIT
                && _account_operation.Code != OperationCode.CASH_WITHDRAWAL
                && _account_operation.Code != OperationCode.CHIPS_SALE_WITH_RECHARGE
                && _account_operation.Code != OperationCode.CLOSE_OPERATION
                && _account_operation.Code != OperationCode.REOPEN_OPERATION) //LTC 22-SEP-2016
          {
            // Update account balance: _ac_fin_balance is not important here.
            if (!MultiPromos.Trx_UpdateAccountBalance(_account_operation.AccountId, _balance_undone, out _ac_fin_balance, Trx))
            {
              return false;
            }
            //  }
            //}

            if (WSI.Common.Misc.IsTerminalDrawEnabled() && !WSI.Common.TerminalDraw.TerminalDraw.DB_DeleteTerminalDrawRechargeByOperationId(_account_operation.OperationId, Trx))
            {
              return false;
            }
          }
        }

        // Cancel Promotions
        if (Input.CodeOperation != OperationCode.CLOSE_OPERATION && Input.CodeOperation != OperationCode.REOPEN_OPERATION)
        {
          _continue_operations = true;

          CancelPromotions(Trx, _undo_operation_id, _ac_mov_table, _cm_mov_table, _dt_account_promotions, ref _promo_balance, _account_operation, ref _continue_operations, out _ac_fin_balance, out _fin_points);

          if (!_continue_operations)
          {
            return false;
          }
        }

        if (WSI.Common.Misc.IsTerminalDrawEnabled() && !WSI.Common.TerminalDraw.TerminalDraw.DB_DeleteTerminalDrawRechargeByOperationId(_account_operation.OperationId, Trx))
        {
          return false;
        }

        // Undo chip sale with rounding
        if (!UpdateUndoRoundingSaleChips(_cashier_movements, _original_account_id, Trx))
        {
          return false;
        }

        // Save new movements
        if (!_ac_mov_table.Save(Trx))
        {
          return false;
        }
        if (!_cm_mov_table.Save(Trx))
        {
          return false;
        }

        // Mark old movements as undone
        if (!DB_MarkAsUndone(Input.OperationIdForUndo, _undo_operation_id, Trx))
        {
          return false;
        }

        // Read split parameters
        if (!Split.ReadSplitParameters(out _splits))
        {
          return false;
        }

        // Cancel Junket commision and Junket flags.
        if (!JunketsCommissionsMovements.UndoSave(Input.OperationIdForUndo, Trx))
        {
          return false;
        }

        // Set devolution amount of operation undone
        _amount = _cancellable_operation.Balance.Redeemable;

        if (!_splits.prize_coupon && _account_operation.Code != OperationCode.PROMOTION_WITH_TAXES)
        {
          _amount = _operation_amount;
        }

        // Calculate remaining devolution amount after operation undo
        _continue_operations = true;

        _max_devolution = CalculateRemainigDevolution(Input, Trx, ref _amount, _account_operation, _card_paid_undone, ref _continue_operations, ref _missing_promo_redeemable);

        if (!_continue_operations)
        {
          return false;
        }

        // Cancel tickets
        _continue_operations = true;

        CancelTickets(Input, Trx, _account_operation, ref _ticket, _tickets_list, ref _continue_operations);

        if (!_continue_operations)
        {
          _amount = _operation_amount;
        }

        // Calculate remaining devolution amount after operation undo
        _continue_operations = true;

        _max_devolution = CalculateRemainigDevolution(Input, Trx, ref _amount, _account_operation, _card_paid_undone, ref _continue_operations, ref _missing_promo_redeemable);

        if (!_continue_operations)
        {
          return false;
        }

        _table_movements = _ac_mov_table.getTable();
        CardData.DB_CardGetAllData(_account_operation.AccountId, _card_data);

        if (_card_data.CreditLineData.HasGetMarker(_table_movements))
        {
          if ((_card_data.CreditLineData.SpentAmount - _account_operation.Amount.Decimal) < 0)
          {
            return false;
          }

          if (!_card_data.CreditLineData.UpdateSpent(_card_data.CreditLineData.SpentAmount - _account_operation.Amount.Decimal, Trx))
          {
            return false;
          }

          if (Input.CodeOperation == OperationCode.CHIPS_SALE_WITH_RECHARGE)
          {
            _credit_line_type = CreditLines.CreditLineMovements.CreditLineMovementType.GetMarkerChips;
          }
          else
          {
            _credit_line_type = CreditLines.CreditLineMovements.CreditLineMovementType.GetMarker;
          }

          if (!CreditLines.CreditLine.CreateCreditLineActions(_card_data, -_account_operation.Amount.Decimal, Input.CashierSessionInfo,
                                                              _credit_line_type, null, out _credit_line_actions))
          {
            return false;
          }


          if (!CreditLines.CreditLinesBusinessLogic.CreateCreditLineMovementsByActionType(_credit_line_actions, _undo_operation_id, Trx))
          {
            return false;
          }

        }

        if (!UndoThresholdOperation(Input.OperationIdForUndo, _undo_operation_id, Trx))
        {
          return false;
        }

        // Create undo vouchers
        if (Input.GenerateVoucherForUndo)
        {
          VoucherList = VoucherBuilder.UndoOperation(Input.OperationIdForUndo, _account_operation.Code, _undo_operation_id, Input.CodeOperation, PrintMode.Print, Trx);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Function to check WasCurrencExchange
    /// </summary>
    private static void CheckIfWasCurrencyExchange(ref Currency Amount, ref String IsoCode, ref CurrencyExchangeType CurrencyType, AccountOperations.Operation AccountOperation, CurrencyExchangeResult ExchangeResult)
    {
      if (AccountOperation.Code == OperationCode.CASH_IN ||
          AccountOperation.Code == OperationCode.PROMOTION ||
          AccountOperation.Code == OperationCode.PROMOTION_WITH_TAXES ||
          AccountOperation.Code == OperationCode.CHIPS_SALE ||
          AccountOperation.Code == OperationCode.CHIPS_PURCHASE ||
          AccountOperation.Code == OperationCode.CHIPS_SALE_WITH_RECHARGE)
      {
        // Check if there was currency exchange
        if (ExchangeResult != null)
        {
          Amount = ExchangeResult.InAmount;
          IsoCode = ExchangeResult.InCurrencyCode;
          CurrencyType = ExchangeResult.InType;
        }
        Amount = -Amount;
      }
    }

    /// <summary>
    /// Function to Update TotalSold
    /// </summary>
    private static void UpdateTotalSoldValue(InputUndoOperation Input, SqlTransaction Trx, ref Currency Amount, AccountOperations.Operation AccountOperation, ref Boolean ContinueOperations)
    {
      if (AccountOperation.Code == OperationCode.CHIPS_SALE_WITH_RECHARGE ||
          AccountOperation.Code == OperationCode.GIFT_REDEEMABLE_AS_CASHIN ||
          AccountOperation.Code == OperationCode.CASH_IN ||
          AccountOperation.Code == OperationCode.PROMOTION ||
          AccountOperation.Code == OperationCode.PROMOTION_WITH_TAXES)
      {
        if (!Cashier.UpdateSessionTotalSold(Input.CashierSessionInfo.CashierSessionId, Input.CashierSessionInfo.UserId, Cashier.SalesLimitType.Cashier, -Amount, Trx))
        {
          ContinueOperations = false;
        }
      }
    }

    /// <summary>
    /// Function to make the undo operation of the cashier balance
    /// </summary>
    private static void UndoCashierBalance(AccountOperations.Operation AccountOperation, out Currency Amount, out Currency OperationAmount, out String IsoCode, out CurrencyExchangeType CurrencyType)
    {
      IsoCode = "";
      CurrencyType = CurrencyExchangeType.CURRENCY;
      OperationAmount = AccountOperation.Amount;
      Amount = OperationAmount;

      if (AccountOperation.Code == OperationCode.GIFT_REDEEMABLE_AS_CASHIN)
      {
        Amount = AccountOperation.NonRedemable;
      }
    }


    /// <summary>
    /// Function to Cancel the tiquets
    /// </summary>
    private static void CancelTickets(InputUndoOperation Input, SqlTransaction Trx, AccountOperations.Operation AccountOperation, ref Ticket Ticket, List<Int64> TicketsList, ref Boolean ContinueOperations)
    {
      if (TicketsList.Count > 0)
      {
        foreach (Int64 _ticket_id in TicketsList)
        {
          Ticket = Ticket.LoadTicket(_ticket_id);
          if (!Ticket.Cashier_TicketDiscard(Ticket, Input.CashierSessionInfo.TerminalId, AccountOperation.AccountId, Trx))
          {
            ContinueOperations = false;
          }
        }
      }
    }

    /// <summary>
    /// Function to Calculete the remainig devolution values
    /// </summary>
    private static decimal CalculateRemainigDevolution(InputUndoOperation Input, SqlTransaction Trx, ref Currency Amount, AccountOperations.Operation AccountOperation, Boolean CardPaidUndone, ref Boolean ContinueOperations, ref Currency MissingPromoRedeemable)
    {
      Decimal _max_devolution;
      _max_devolution = 0;

      // Calculate remaining devolution amount after operation undo
      if (AccountOperation.Code == OperationCode.CASH_IN ||
          AccountOperation.Code == OperationCode.PROMOTION ||
          AccountOperation.Code == OperationCode.PROMOTION_WITH_TAXES ||
          AccountOperation.Code == OperationCode.GIFT_REDEEMABLE_AS_CASHIN ||
          AccountOperation.Code == OperationCode.CHIPS_PURCHASE ||
          AccountOperation.Code == OperationCode.CHIPS_SALE_WITH_RECHARGE ||
          (AccountOperation.Code == OperationCode.MB_CASH_IN && Misc.IsTerminalDrawEnabled()))
      {
        //[XGJ][FJC][SJA] 23-MAR-2017 Fixed Bug 26082:Sorteo de M�quina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado.
        //if (!Misc.IsTerminalDrawEnabled() && Input.CodeOperation == OperationCode.CASH_IN)
        if (Input.CodeOperation == OperationCode.CASH_IN || (AccountOperation.Code == OperationCode.MB_CASH_IN && Misc.IsTerminalDrawEnabled()))
        {
          _max_devolution = Math.Max(0, Input.CardData.MaxDevolution - Amount - MissingPromoRedeemable);
        }

        // Update max devolution
        if (!DB_UpdateMaxDevolution(AccountOperation.AccountId, Input.OperationIdForUndo, _max_devolution, CardPaidUndone, Trx))
        {
          ContinueOperations = false;
        }
      }

      return _max_devolution;
    }

    /// <summary>
    /// Function to cancel the promotions
    /// </summary>
    private static void CancelPromotions(SqlTransaction Trx, Int64 UndoOperationId, AccountMovementsTable ac_MovTable, CashierMovementsTable cm_MovTable, DataTable dt_AccountPromotions, ref MultiPromos.PromoBalance PromoBalance, AccountOperations.Operation AccountOperation, ref Boolean ContinueOperations, out MultiPromos.AccountBalance ac_FinBalance, out Decimal FinPoints)
    {
      ac_FinBalance = new MultiPromos.AccountBalance();
      FinPoints = 0;

      foreach (DataRow _promotion in dt_AccountPromotions.Rows)
      {
        AccountPromotion.Trx_UndoPromotion(_promotion, ac_MovTable, cm_MovTable, "", UndoOperationId, out PromoBalance, Trx);
      }

      if (AccountOperation.Code != OperationCode.MB_DEPOSIT
          && AccountOperation.Code != OperationCode.CASH_WITHDRAWAL)
      {
        // Take the REAL final balance
        if (!MultiPromos.Trx_UpdateAccountBalance(AccountOperation.AccountId, MultiPromos.AccountBalance.Zero, 0,
                                                  out ac_FinBalance, out FinPoints, Trx))
        {
          ContinueOperations = false;
        }
      }
    }


    /// <summary>
    /// Function to get the account movement when it hasn't promotions
    /// </summary>
    private static void ReadAccountMovementWithoutPromo(InputUndoOperation Input, SqlTransaction Trx, Int64 UndoOperationId, CancellableOperation CancellableOperation, ref Currency InitialBalance, ref Currency FinalBalance, ref Currency InitialBalancePoints, ref Currency FinalBalancePoints, ref Currency SubAmount, ref Currency AddAmount, AccountOperations.Operation AccountOperation, ref Boolean CardPaidUndone, out StringBuilder _sb, out Currency CardPrice, out AccountMovementsTable ac_MovTable, out String OriginalTrackData, out Int64 OriginalAccountId)
    {
      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT   AM_MOVEMENT_ID                                                                      ");
      _sb.AppendLine("         , AM_ACCOUNT_ID                                                                       ");
      _sb.AppendLine("         , AM_TYPE                                                                             ");
      _sb.AppendLine("         , AM_SUB_AMOUNT                                                                       ");
      _sb.AppendLine("         , AM_ADD_AMOUNT                                                                       ");
      _sb.AppendLine("         , AM_TRACK_DATA                                                                       ");
      _sb.AppendLine("         , ISNULL(AM_DETAILS, '')                                                              ");
      _sb.AppendLine("    FROM   ACCOUNT_MOVEMENTS WITH(INDEX(IX_am_operation_id))                                   ");
      _sb.AppendLine("   WHERE   AM_OPERATION_ID      = @pOperationId                                                ");

      if (TITO.Utils.IsTitoMode())
      {
        _sb.AppendLine("   AND   AM_TYPE NOT IN (@PromotionPoint, @pCancelPromoNotRedeem, @pCancelPromoRedeem, @pCancelPromotionPoint)");
      }
      else
      {
        _sb.AppendLine("   AND   AM_TYPE NOT IN (@pPromoNotRedeem, @PromotionPoint, @pCancelPromoNotRedeem, @pCancelPromoRedeem, @pCancelPromotionPoint)");
      }

      _sb.AppendLine("ORDER BY   AM_MOVEMENT_ID ASC                                                                  ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = AccountOperation.OperationId;
        _cmd.Parameters.Add("@pPromoNotRedeem", SqlDbType.Int).Value = MovementType.PromotionNotRedeemable;
        _cmd.Parameters.Add("@PromotionPoint", SqlDbType.Int).Value = MovementType.PromotionPoint;
        _cmd.Parameters.Add("@pCancelPromoNotRedeem", SqlDbType.Int).Value = MovementType.CancelNotRedeemable;
        _cmd.Parameters.Add("@pCancelPromoRedeem", SqlDbType.Int).Value = MovementType.CancelPromotionRedeemable;
        _cmd.Parameters.Add("@pCancelPromotionPoint", SqlDbType.Int).Value = MovementType.CancelPromotionPoint;

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          MovementType _movement_type;
          String _details;

          ac_MovTable = new AccountMovementsTable(Input.CashierSessionInfo);
          CardPrice = 0;
          OriginalAccountId = 0;
          OriginalTrackData = "";

          while (_reader.Read())
          {
            SubAmount = _reader.GetDecimal(3);
            AddAmount = _reader.GetDecimal(4);

            _movement_type = (MovementType)_reader.GetInt32(2);

            if (String.IsNullOrEmpty(OriginalTrackData) && !_reader.IsDBNull(5))
            {
              OriginalTrackData = _reader.GetString(5);
              OriginalAccountId = _reader.GetInt64(1);
            }

            _details = _reader.GetString(6);

            switch (_movement_type)
            {
              case MovementType.PointsToGiftRequest:
              case MovementType.PointsGiftDelivery:
                FinalBalancePoints = InitialBalancePoints + SubAmount - AddAmount;
                ac_MovTable.Add(UndoOperationId, _reader.GetInt64(1), _movement_type, InitialBalancePoints, -SubAmount, -AddAmount, FinalBalancePoints, _details);
                InitialBalancePoints = FinalBalancePoints;
                break;

              case MovementType.CreditLineGetMarker:
                ac_MovTable.Add(UndoOperationId, _reader.GetInt64(1), _movement_type, InitialBalance, -SubAmount, -AddAmount, -AddAmount, _details);
                InitialBalance = FinalBalance;
                break;

              default:
                FinalBalance = InitialBalance + SubAmount - AddAmount;

                InitialCashInForChips(ref CardPrice, ref InitialBalance, ref FinalBalance, ref AddAmount, ref CardPaidUndone, _movement_type);

                ac_MovTable.Add(UndoOperationId, _reader.GetInt64(1), _movement_type, InitialBalance, -SubAmount, -AddAmount, FinalBalance, _details);
                InitialBalance = FinalBalance;
                break;
            }

            // TODO: A revisar...
            if (AccountOperation.Code == OperationCode.CASH_OUT)
            {
              switch (_movement_type)
              {
                case MovementType.CashOut:
                  CancellableOperation.Balance.PromoRedeemable += SubAmount;
                  break;

                case MovementType.Devolution:
                  CancellableOperation.Balance.Redeemable += SubAmount;
                  break;

                default:
                  break;
              }
            }

            if (AccountOperation.Code == OperationCode.CHIPS_SALE_WITH_RECHARGE)
            {
              switch (_movement_type)
              {
                case MovementType.ChipsSaleTotal:
                  CancellableOperation.Balance.Redeemable -= SubAmount;
                  break;

                case MovementType.ChipsSaleRemainingAmount:
                  CancellableOperation.Balance.Redeemable -= SubAmount;
                  break;

                case MovementType.ChipsSaleConsumedRemainingAmount:
                  CancellableOperation.Balance.Redeemable += AddAmount;
                  break;

                default:
                  break;
              }
            }
          }// end while
        }
      }
    }


    /// <summary>
    /// Function to Get/Set the initial cashin for chips
    /// </summary>
    private static void InitialCashInForChips(ref Currency CardPrice, ref Currency InitialBalance, ref Currency FinalBalance, ref Currency AddAmount, ref Boolean CardPaidUndone, MovementType MovementType)
    {
      switch (MovementType)
      {
        /* TODO: Palacio Shortcut for AC_INITIAL_CASH_IN_FOR_CHIPS:
         *   case MovementType.ChipsPurchaseWithCashOut 
         *   case MovementType.ChipsSaleWithCashIn
         */
        case MovementType.DepositIn:
        // DHA 06-JUL-2015
        case MovementType.CardDepositInCheck:
        case MovementType.CardDepositInCurrencyExchange:
        case MovementType.CardDepositInCardCredit:
        case MovementType.CardDepositInCardDebit:
        case MovementType.CardDepositInCardGeneric:
        // RCI 14-AUG-2014: Correct initial/final balance on Card/Check undo operations.
        case MovementType.HIDDEN_RechargeNotDoneWithCash:

          // Don't update final balance for these movements
          FinalBalance = InitialBalance;
          if (MovementType == MovementType.DepositIn ||
            MovementType == MovementType.CardDepositInCheck ||
            MovementType == MovementType.CardDepositInCurrencyExchange ||
            MovementType == MovementType.CardDepositInCardCredit ||
            MovementType == MovementType.CardDepositInCardDebit ||
            MovementType == MovementType.CardDepositInCardGeneric)
          {
            CardPaidUndone = true;
            CardPrice = AddAmount;
          }
          break;

        default:
          break;
      }
    }

    /// <summary>
    /// Function to Get the values operations in movements with promos
    /// </summary>
    private static List<CancellableOperation> CalculateValuesOperationWithPromo(InputUndoOperation Input, SqlTransaction Trx, MultiPromos.AccountBalance acIniBalance, CurrencyIsoType IsoType, CancellableOperation CancellableOperation, ref MultiPromos.AccountBalance BalanceUndone, ref DataTable dt_AccountPromotions, ref MultiPromos.PromoBalance OpPromosBalance, AccountOperations.Operation AccountOperation, Int64 GamingTableSessionId, DataTable CashierMovements, ref Currency TotalChips, ref Boolean ContinueOperations, ref Currency MissingRedeemable, ref Currency MissingPromoRedeemable, ref Currency RedeemableToSubstract, ref Currency PromoRedeemableToSubstract)
    {
      List<CancellableOperation> _cancellable_operation_list;
      Object _custody_operation;
      Decimal _custody_amount_operation;

      _cancellable_operation_list = new List<CancellableOperation>();

      switch (AccountOperation.Code)
      {
        case OperationCode.CASH_IN:
        case OperationCode.PROMOTION:
        case OperationCode.PROMOTION_WITH_TAXES:
        case OperationCode.CHIPS_PURCHASE:
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
        case OperationCode.MB_CASH_IN:

          _cancellable_operation_list = new List<CancellableOperation>();
          _cancellable_operation_list.Add(CancellableOperation);

          // Read a operation promotions
          CardData.GetPromotionsToBeCancelledDueToRedeem(AccountOperation.AccountId, 1, _cancellable_operation_list,
                                                         false, true, out OpPromosBalance, out dt_AccountPromotions, Trx);

          // Calculate redemable amount to substract
          RedeemableToSubstract = Math.Min(CancellableOperation.Balance.Redeemable, acIniBalance.Redeemable);

          // RAB 27-JAN-2016
          // Calculate promo redemable amount to substract
          PromoRedeemableToSubstract = Math.Min(CancellableOperation.Balance.PromoRedeemable, acIniBalance.PromoRedeemable);

          // Check if account has enought redeemable to undo, if not we must subtract it from promo_redeemable balance
          MissingRedeemable = Math.Max(0, CancellableOperation.Balance.Redeemable - acIniBalance.Redeemable);

          // Check if account has enought promo_redeemable to undo, if not we must subtract it from redeemable balance
          MissingPromoRedeemable = Math.Max(0, CancellableOperation.Balance.PromoRedeemable - acIniBalance.PromoRedeemable);

          RedeemableToSubstract += MissingRedeemable; //_missing_promo_redeemable;
          PromoRedeemableToSubstract += MissingPromoRedeemable; //_missing_redeemable;

          //_redeemable_to_substract += _missing_redeemable;
          //_promo_redeemable_to_substract = _op_promos_balance.Balance.PromoRedeemable;

          // Set balance to undone
          BalanceUndone = new MultiPromos.AccountBalance(-RedeemableToSubstract, -PromoRedeemableToSubstract, 0);

          if (AccountOperation.Code == OperationCode.CHIPS_PURCHASE)
          {
            // Update GamingTableSession values for CHIPS_PURCHASE
            if (!UpdateGamingTableSession(Input.CashierSessionInfo.CashierSessionId, GamingTableSessionId, OperationCode.CHIPS_PURCHASE, TotalChips, IsoType, Trx))
            {
              ContinueOperations = false;
            }
          }

          if (ContinueOperations && !GamingTableBusinessLogic.IsEnableManualConciliation() && AccountOperation.Code == OperationCode.CHIPS_SALE_WITH_RECHARGE)
          {
            // Update GamingTableSession for CHIPS_SALE_WITH_RECHARGE              
            if (!UpdateGamingTableSession(Input.CashierSessionInfo.CashierSessionId, GamingTableSessionId, OperationCode.CHIPS_SALE_WITH_RECHARGE, TotalChips, IsoType, Trx))
            {
              ContinueOperations = false;
            }
          }

          _custody_operation = CashierMovements.Compute("SUM(CM_ADD_AMOUNT)", "CM_TYPE=583 ");
          if (Decimal.TryParse(_custody_operation.ToString(), out _custody_amount_operation))
          {
            BalanceUndone.CustodyProvision = _custody_amount_operation;
          }
          else
          {
            BalanceUndone.CustodyProvision = 0;
          }

          break;

        case OperationCode.CASH_OUT:
        case OperationCode.CHIPS_SALE:
        case OperationCode.HANDPAY:
        case OperationCode.CLOSE_OPERATION:

          // Set balance to undone
          BalanceUndone = new MultiPromos.AccountBalance(CancellableOperation.Balance.Redeemable, CancellableOperation.Balance.PromoRedeemable, 0);

          if (!GamingTableBusinessLogic.IsEnableManualConciliation() && AccountOperation.Code == OperationCode.CHIPS_SALE)
          {
            // Update GamingTableSession values for CHIPS_SALE
            if (!UpdateGamingTableSession(Input.CashierSessionInfo.CashierSessionId, GamingTableSessionId, OperationCode.CHIPS_SALE, TotalChips, IsoType, Trx))
            {
              ContinueOperations = false;
            }
          }
          break;

        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
        default:
          break;
      }
      return _cancellable_operation_list;
    }

    /// <summary>
    /// Function to remove cancellable operaions.
    /// </summary>
    /// <param name="AccountOperation"></param>
    /// <returns></returns>
    private static CancellableOperation SetCancellableOperationInformation(AccountOperations.Operation AccountOperation)
    {
      CancellableOperation _cancellable_operation;
      _cancellable_operation = new CancellableOperation();
      _cancellable_operation.Balance.Redeemable = AccountOperation.Redeemable;
      _cancellable_operation.Balance.PromoRedeemable = AccountOperation.PromoRedeemable;
      _cancellable_operation.Balance.PromoNotRedeemable = AccountOperation.PromoNonRedeemable;
      _cancellable_operation.OperationId = AccountOperation.OperationId;
      return _cancellable_operation;
    }

    /// <summary>
    /// Function that indicates if the operation is reversible.
    /// </summary>
    private static void CheckIfOperationIsReversible(InputUndoOperation Input, ref UndoError Error, SqlTransaction Trx, ref CurrencyIsoType IsoType, ref Int64 GamingTableSessionId, ref List<Int64> TicketsList, ref Boolean ContinueOperations, out Decimal IsoAmount, out AccountOperations.Operation AccountOperation)
    {
      IsoAmount = 0;
      AccountOperation = new AccountOperations.Operation();
      Decimal _max_devolution;
      Currency _devolution;

      _max_devolution = 0;
      _devolution = 0;

      switch (Input.CodeOperation)
      {
        case OperationCode.HANDPAY:
        case OperationCode.HANDPAY_CANCELLATION:
          // MPO 09-OCT-2014: Handpay: Prepared for undo TITO handpay.
          if (!TITO.Utils.IsTitoMode())
          {
            ContinueOperations = false;
          }
          if (!IsHandpayReversible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out AccountOperation,
                                   out IsoType, out IsoAmount, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;
        case OperationCode.CASH_IN:
        case OperationCode.GIFT_DELIVERY:
        case OperationCode.TITO_OFFLINE:
        case OperationCode.TITO_REISSUE:
          if (Input.CodeOperation == OperationCode.CASH_IN)
          {
            if (Misc.IsTerminalDrawEnabled())
            {
              if (!WSI.Common.TerminalDraw.TerminalDraw.DB_GetTerminalDrawRechargesAmountByOperationId(AccountOperation.OperationId, out _devolution, Trx))
              {
                Log.Error(String.Format("Error in GetTerminalDrawRechargesAmountByOperationId(). OperationId: {0}", AccountOperation.OperationId));

                ContinueOperations = false;
              }

              //[XGJ][FJC][SJA] 23-MAR-2017 Fixed Bug 26082:Sorteo de M�quina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado.
              //_max_devolution = (_devolution != 0) ? (Input.CardData.MaxDevolution - _devolution) : 0;
              _max_devolution = Input.CardData.MaxDevolution;
            }

            if (Misc.IsTerminalDrawEnabled() && !WSI.Common.TerminalDraw.TerminalDraw.DB_DeleteTerminalDrawRechargeByOperationId(AccountOperation.OperationId, Trx))
            {
              ContinueOperations = false;
            }
          }

          if (TITO.Utils.IsTitoMode())
          {
            if (!IsTITOReversible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, Input.CodeOperation, out AccountOperation,
                               out IsoType, out IsoAmount, out Error, out TicketsList, Trx))
            {
              ContinueOperations = false;
            }
          }
          else
          {
            if (!IsCashInReversible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out AccountOperation,
                 out IsoType, out IsoAmount, out Error, Trx))
            {
              ContinueOperations = false;
            }
          }
          break;
        case OperationCode.CHIPS_SALE:
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
          if (!IsChipsSaleReversible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out AccountOperation,
                             out IsoType, out IsoAmount, out GamingTableSessionId, out TicketsList, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;

        case OperationCode.CHIPS_PURCHASE:
          if (!IsChipsPurchaseReversible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out AccountOperation,
                             out IsoType, out IsoAmount, out GamingTableSessionId, out TicketsList, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;
        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
          if (!IsChipsPurchaseWithCashOutReversible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out AccountOperation,
                              out IsoType, out IsoAmount, out GamingTableSessionId, out TicketsList, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;

        case OperationCode.CASH_ADVANCE:
          if (!IsCashAdvanceReversible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, Input.UseTodayOpening, out AccountOperation,
                              out IsoType, out IsoAmount, out GamingTableSessionId, out TicketsList, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;

        case OperationCode.MB_CASH_IN:
        case OperationCode.MB_DEPOSIT:
          if (!IsMBCashInReversible(Input.MBCardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, Input.CodeOperation,
                                    out AccountOperation, out IsoType, out IsoAmount, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;

        case OperationCode.CASH_WITHDRAWAL:
          if (!IsWithdrawReversible(Input.CardData, Input, out AccountOperation, out Error, Trx))
          {
            ContinueOperations = false;
          }

          break;

        case OperationCode.TERMINAL_COLLECT_DROPBOX:
          if (!IsCollectInTerminalRevesible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out AccountOperation, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;
        case OperationCode.TERMINAL_REFILL_HOPPER:
          if (!IsRefillInTerminalRevesible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, out AccountOperation,
                              out IsoType, out IsoAmount, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;
        case OperationCode.CURRENCY_EXCHANGE_CHANGE:
        case OperationCode.CURRENCY_EXCHANGE_DEVOLUTION:
          if (!IsCurrencyExchangeReversible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, Input.CodeOperation, out AccountOperation,
                              out IsoType, out IsoAmount, out Error, Trx))
          {
            ContinueOperations = false;
          }

          break;

        case OperationCode.CUSTOMER_ENTRANCE:
          if (!IsCustomerEntranceReversible(Input.CardData, Input.OperationIdForUndo, Input.CashierSessionInfo.CashierSessionId, Input.CodeOperation, out AccountOperation,
                              out IsoType, out IsoAmount, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;
        case OperationCode.REOPEN_OPERATION:
          if (!IsCloseCashierSessionReversible(Input.CardData, Input, out AccountOperation, out Error, Trx))
          {
            ContinueOperations = false;
          }
          break;
        default:
          ContinueOperations = false;
          break;

      }
    }


    /// <summary>
    /// PROVISIONAL
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="UndoOperationId"></param>
    /// <param name="Session"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public class MBInputParams
    {
      public Currency MB_Deposit;
      public Currency MB_CashIn;
      public Currency MB_PendingCash;
      public Currency MB_ShortFallCash;
      public Currency MB_OverCash;
      public Int64 MB_CardId;
      public Int64 MB_CashierSessionId;

      public MBInputParams()
      {
        MB_Deposit = Currency.Zero();
        MB_CashIn = Currency.Zero();
        MB_PendingCash = Currency.Zero();
        MB_ShortFallCash = Currency.Zero();
        MB_OverCash = Currency.Zero();
        MB_CardId = -1;
        MB_CashierSessionId = -1;
      }
    }

    public static Boolean DB_InsertUndoMobileBankMovements(Int64 OperationId, Int64 UndoOperationId, CashierSessionInfo Session, out Decimal AmountToChange, out MBMovementType MovementType, SqlTransaction Trx)
    {
      DataTable _mb_table;
      Int64 _mb_movement_id;
      MBMovementData _input;
      Decimal _aux_initial_value;
      MBInputParams _mb_input_params;

      _mb_table = new DataTable();
      MovementType = MBMovementType.None;
      AmountToChange = 0.0M;

      if (!GetLastBankMovementByOperation(OperationId, out _mb_table, Trx))
      {
        return false;
      }

      try
      {
        _mb_input_params = new MBInputParams();

        foreach (DataRow _row in _mb_table.Rows)
        {
          _input = new MBMovementData();

          _aux_initial_value = (Decimal)_row["MBM_INITIAL_BALANCE"];

          _row["MBM_SUB_AMOUNT"] = -(Decimal)_row["MBM_SUB_AMOUNT"];
          _row["MBM_ADD_AMOUNT"] = -(Decimal)_row["MBM_ADD_AMOUNT"];
          _row["MBM_INITIAL_BALANCE"] = _row["MBM_FINAL_BALANCE"];

          switch ((MBMovementType)_row["MBM_TYPE"])
          {
            case MBMovementType.TransferCredit:
              _row["MBM_FINAL_BALANCE"] = (Decimal)_row["MBM_FINAL_BALANCE"] - (Decimal)_row["MBM_SUB_AMOUNT"];
              _input.Amount_01 = ((Currency)(-(Decimal)_row["MBM_AMOUNT_01"]));
              _input.Amount_02 = ((Currency)(-(Decimal)_row["MBM_AMOUNT_02"]));

              break;

            case MBMovementType.DepositCash:
              _row["MBM_FINAL_BALANCE"] = _aux_initial_value.ToString();
              _mb_input_params.MB_Deposit = _mb_input_params.MB_Deposit + ((Currency)((Decimal)_row["MBM_ADD_AMOUNT"]));
              _mb_input_params.MB_CardId = (Int64)_row["MBM_MB_ID"];
              _mb_input_params.MB_CashierSessionId = (Int64)_row["MBM_CASHIER_SESSION_ID"];

              break;

            case MBMovementType.AutomaticChangeLimit:
              _row["MBM_FINAL_BALANCE"] = _aux_initial_value.ToString();
              _mb_input_params.MB_CashIn = _mb_input_params.MB_CashIn + ((Currency)((Decimal)_row["MBM_ADD_AMOUNT"]));

              break;

            case MBMovementType.CashExcess:
              _row["MBM_FINAL_BALANCE"] = _aux_initial_value.ToString();
              _mb_input_params.MB_OverCash = _mb_input_params.MB_OverCash + ((Currency)((Decimal)_row["MBM_ADD_AMOUNT"]));
              AmountToChange = _mb_input_params.MB_OverCash;
              MovementType = MBMovementType.CashExcess;

              break;

            case MBMovementType.CashShortFall:
              _row["MBM_FINAL_BALANCE"] = _aux_initial_value.ToString();
              _mb_input_params.MB_ShortFallCash = _mb_input_params.MB_ShortFallCash + ((Currency)((Decimal)_row["MBM_SUB_AMOUNT"]));
              AmountToChange = _mb_input_params.MB_ShortFallCash;
              MovementType = MBMovementType.CashShortFall;

              break;

            case MBMovementType.CloseSession:
            case MBMovementType.CloseSessionWithCashLost:
            case MBMovementType.CloseSessionWithExcess:
            case MBMovementType.CreditCoverCoupon:
            case MBMovementType.CreditNonRedeemable:
            case MBMovementType.CreditNonRedeemable2:
            case MBMovementType.ManualChangeLimit:
              // Nothing to do.
              break;

            default:

              break;
          }

          _row["MBM_OPERATION_ID"] = UndoOperationId;

          _mb_movement_id = (long)_row["MBM_MOVEMENT_ID"];

          _input.CashierId = Session.TerminalId;
          _input.CardId = (Int64)_row["MBM_MB_ID"];
          _input.Type = (MBMovementType)_row["MBM_TYPE"];
          _input.CashierSessionId = (Int64)_row["MBM_CASHIER_SESSION_ID"];
          _input.InitialBalance = ((Currency)((Decimal)_row["MBM_INITIAL_BALANCE"]));
          _input.SubAmount = ((Currency)((Decimal)_row["MBM_SUB_AMOUNT"]));
          _input.AddAmount = ((Currency)((Decimal)_row["MBM_ADD_AMOUNT"]));
          _input.FinalBalance = ((Currency)((Decimal)_row["MBM_FINAL_BALANCE"]));
          _input.CashierName = Session.TerminalName;
          _input.OperationId = (Int64)_row["MBM_OPERATION_ID"];

          if (!MobileBank.DB_InsertMBCardMovement(_input, out _mb_movement_id, Trx))
          {
            return false;
          }

          if (_input.Type == MBMovementType.TransferCredit)
          {
            if (!DB_UpdateMobileBankTransaction(_input, Trx))
            {
              return false;
            }
          }
        }

        if (_mb_input_params.MB_CardId != -1 &&
            _mb_input_params.MB_CashierSessionId != -1)
        {
          if (!DB_UpdateMobileBankTransaction(_mb_input_params, Trx))
          {
            return false;
          }
        }

        if (!DB_MarkAsUndone(OperationId, UndoOperationId, Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_InsertUndoMobileBankMovements

    private static Boolean DB_UpdateMobileBankTransaction(Object InputData, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_updated;
      Boolean _is_deposit;

      _sb = new StringBuilder();
      _is_deposit = false;

      if (InputData is MBInputParams)
      {
        _is_deposit = true;

        _sb.AppendLine("UPDATE   MOBILE_BANKS                                                       ");
        _sb.AppendLine("   SET   MB_BALANCE             =                                           ");
        _sb.AppendLine("                                ( CASE                                      ");
        _sb.AppendLine("                                  WHEN (MB_BALANCE + @pLimit < 0) THEN      ");
        _sb.AppendLine("                                     MB_BALANCE                             ");
        _sb.AppendLine("                                  ELSE                                      ");
        _sb.AppendLine("                                     MB_BALANCE + @pLimit                   ");
        _sb.AppendLine("                                  END                                       ");
        _sb.AppendLine("                                )                                           ");
        _sb.AppendLine("       , MB_DEPOSIT             = MB_DEPOSIT      + (@pAmount)              ");
        _sb.AppendLine("       , MB_PENDING_CASH        = MB_PENDING_CASH - (@pAmount - @pOverCash + @pShortFallCash) ");
        _sb.AppendLine("       , MB_SHORTFALL_CASH      = MB_SHORTFALL_CASH + @pShortFallCash       ");
        _sb.AppendLine("       , MB_OVER_CASH           = MB_OVER_CASH    + @pOverCash              ");
        _sb.AppendLine(" WHERE   MB_ACCOUNT_ID          = @pAccountId                               ");
        _sb.AppendLine("   AND   MB_CASHIER_SESSION_ID  = @pCashierSessionId                        ");
      }
      else
      {
        _sb.AppendLine("UPDATE   MOBILE_BANKS           ");
        _sb.AppendLine("   SET   MB_BALANCE             = MB_BALANCE      - @pAmount  ");
        _sb.AppendLine("       , MB_CASH_IN             = MB_CASH_IN      + @pAmount  ");
        _sb.AppendLine("       , MB_PENDING_CASH        = MB_PENDING_CASH + @pAmount  ");
        _sb.AppendLine("       , MB_ACTUAL_NUMBER_OF_RECHARGES = isNull(MB_ACTUAL_NUMBER_OF_RECHARGES, 0) - 1  ");
        _sb.AppendLine(" WHERE   MB_ACCOUNT_ID          = @pAccountId                 ");
        _sb.AppendLine("   AND   MB_CASHIER_SESSION_ID  = @pCashierSessionId          ");
      }

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          if (_is_deposit)
          {
            _sql_cmd.Parameters.Add("@pLimit", SqlDbType.Money).Value = ((MBInputParams)InputData).MB_CashIn.SqlMoney;
            _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = ((MBInputParams)InputData).MB_Deposit.SqlMoney;
            _sql_cmd.Parameters.Add("@pOverCash", SqlDbType.Money).Value = ((MBInputParams)InputData).MB_OverCash.SqlMoney;
            _sql_cmd.Parameters.Add("@pShortFallCash", SqlDbType.Money).Value = ((MBInputParams)InputData).MB_ShortFallCash.SqlMoney;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = ((MBInputParams)InputData).MB_CardId;
            _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = ((MBInputParams)InputData).MB_CashierSessionId;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = ((MBMovementData)InputData).SubAmount.SqlMoney;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = ((MBMovementData)InputData).CardId;
            _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = ((MBMovementData)InputData).CashierSessionId;
          }

          _num_rows_updated = _sql_cmd.ExecuteNonQuery();

          if (_num_rows_updated != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateMobileBankTransaction

    //------------------------------------------------------------------------------
    // PURPOSE: Performs all proceses to undo a TITO ticket (only for promotions)
    //
    //  PARAMS:
    //      - INPUT:
    //          - OperationId : Id of the operation to be undone
    //          - UndoOperationId : Id of the undo operation
    //          - Trx : SQL Transaction
    //
    // RETURNS:
    //      - Boolean: If operation has been undone correctly.
    //
    private static Boolean DB_InsertUndoBankTransactionMovement(Int64 OperationId, Int64 UndoOperationId, SqlTransaction Trx)
    {
      CurrencyExchangeResult _exchange_result;
      RechargeInputParameters _input_params;
      StringBuilder _sb;
      BankTransactionData _bt_data;
      String _track_data;

      try
      {
        _exchange_result = new CurrencyExchangeResult();
        _input_params = new RechargeInputParameters();
        _bt_data = new BankTransactionData();
        _sb = new StringBuilder();
        _track_data = string.Empty;

        //Check if operation id exists on Bank transactions table
        _sb.AppendLine("  SELECT    BT_OPERATION_ID                 ");
        _sb.AppendLine("          , BT_TYPE                         ");
        _sb.AppendLine("          , BT_DOCUMENT_NUMBER              ");
        _sb.AppendLine("          , BT_BANK_NAME                    ");
        _sb.AppendLine("          , BT_BANK_COUNTRY                 ");
        _sb.AppendLine("          , BT_HOLDER_NAME                  ");
        _sb.AppendLine("          , BT_CARD_EXPIRATION_DATE         ");
        _sb.AppendLine("          , BT_CARD_TRACK_DATA              ");
        _sb.AppendLine("          , BT_AMOUNT                       ");
        _sb.AppendLine("          , BT_ISO_CODE                     ");
        _sb.AppendLine("          , BT_TRANSACTION_TYPE             ");
        _sb.AppendLine("          , BT_EDITED                       ");
        _sb.AppendLine("          , BT_CHECK_ROUTING_NUMBER         ");
        _sb.AppendLine("          , BT_CHECK_ACCOUNT_NUMBER         ");
        _sb.AppendLine("          , BT_CHECK_DATE                   ");
        _sb.AppendLine("          , BT_COMMENTS                     ");
        _sb.AppendLine("          , BT_ACCOUNT_TRACK_DATA           ");
        _sb.AppendLine("          , BT_ACCOUNT_HOLDER_NAME          ");
        _sb.AppendLine("    FROM    BANK_TRANSACTIONS               ");
        _sb.AppendLine("    WHERE   BT_OPERATION_ID = @pOperationId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          using (SqlDataReader _sql_rd = _cmd.ExecuteReader())
          {
            //If exists, fill _exchange_result object with the info of DB
            while (_sql_rd.Read())
            {
              _bt_data = new BankTransactionData();
              _bt_data.OperationId = _sql_rd.GetInt64(0);
              _bt_data.Type = (CurrencyExchangeSubType)_sql_rd.GetInt32(1);
              _bt_data.DocumentNumber = _sql_rd[2] as String;
              _bt_data.BankName = _sql_rd[3] as String;
              _bt_data.BankCountry = _sql_rd.GetInt32(4);
              _bt_data.HolderName = _sql_rd[5] as String;
              _bt_data.ExpirationDate = _sql_rd[6] as String;
              _bt_data.TrackData = _sql_rd[7] as String;
              _bt_data.inAmount = -_sql_rd.GetDecimal(8);
              _bt_data.CurrencyCode = _sql_rd[9] as String;
              _bt_data.TransactionType = (TransactionType)_sql_rd.GetInt32(10);
              _bt_data.CardEdited = _sql_rd.GetBoolean(11);
              _bt_data.RoutingNumber = _sql_rd[12] as String;
              _bt_data.AccountNumber = _sql_rd[13] as String;
              _bt_data.CheckDate = _sql_rd.IsDBNull(14) ? DateTime.MinValue : _sql_rd.GetDateTime(14);
              _bt_data.Comments = _sql_rd[15] as String;
              _track_data = _sql_rd.GetString(16);
              _bt_data.AccountHolderName = _sql_rd[17] as String;
            }
          }
        }

        if (_bt_data.OperationId != 0)
        {
          if (_track_data == CardData.RECYCLED_TRACK_DATA)
          {
            _input_params.ExternalTrackData = _track_data;
          }
          else
          {
            //Set trackdata (toInternal) to InputParams.TrackData
            if (!CardNumber.TrackDataToExternal(out _input_params.ExternalTrackData, _track_data, (int)CardTrackData.CardsType.PLAYER))
            {

              return false;
            }
          }

          _exchange_result.GrossAmount = _bt_data.inAmount;
          _exchange_result.InCurrencyCode = _bt_data.CurrencyCode;
          _exchange_result.BankTransactionData = _bt_data;

          //Inserts movement into Bank Transactions table with the same info of the previous movement but negative amount
          if (!Accounts.DB_InsertBankTransaction(UndoOperationId, _exchange_result, _input_params.ExternalTrackData, Trx))
          {

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {

        Log.Message("DB_InsertUndoBankTransaction: " + _ex.Message);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Performs all proceses to undo a TITO ticket (only for promotions)
    //
    //  PARAMS:
    //      - INPUT:
    //          - CardData : Player CardData
    //          - OperationId : Id of the operation to be undone
    //          - CashierSessionId : Current cashier session Id
    //          - TicketsUndo: List of tickets to undo
    //          - Trx : SQL Transaction
    //
    //      - OUTPUT:
    //          - VoucherList: List of vouchers to be print
    //
    // RETURNS:
    //      - Boolean: If operation has been undone correctly.
    //
    public static Boolean UndoTITOTicket(OperationCode CodeOperation, CardData CardData, Int64 OperationId, CashierSessionInfo CashierSessionInfo, List<Ticket> TicketsUndo, out ArrayList VoucherList, out Int64 TicketNoDiscardable)
    {
      Boolean _result_ok;
      TicketNoDiscardable = 0;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _result_ok = UndoTITOTicket(CodeOperation, CardData, OperationId, CashierSessionInfo, TicketsUndo, out VoucherList, out TicketNoDiscardable, _db_trx.SqlTransaction);

        if (_result_ok)
        {

          _db_trx.Commit();
        }
      }

      return _result_ok;
    }

    public static Boolean UndoTITOTicket(OperationCode CodeOperation, CardData CardData, Int64 OperationId, CashierSessionInfo CashierSessionInfo, List<Ticket> TicketsUndo, out ArrayList VoucherList, out Int64 TicketNoDiscardable, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int64 _undo_operation_id;
      MultiPromos.AccountBalance _ac_ini_balance;
      MultiPromos.AccountBalance _ac_fin_balance;
      MultiPromos.AccountBalance _operation_balance;
      Decimal _ini_points;
      Decimal _fin_points;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      Currency _initial_balance;
      Currency _final_balance;
      Currency _initial_balance_points;
      Currency _final_balance_points;
      Currency _sub_amount;
      Currency _add_amount;
      AccountMovementsTable _ac_mov_table;
      CashierMovementsTable _cm_mov_table;
      TYPE_SPLITS _splits;
      AccountOperations.Operation _account_operation;
      UndoError _error;
      CurrencyExchangeResult _exchange_result;
      String _original_track_data;
      Int64 _original_account_id;
      Currency _total_chips;
      List<Int64> _account_movements_ids;
      List<Int64> _cahier_movements_ids;
      List<VoucherValidationNumber> _voucher_tickets;
      VoucherValidationNumber _voucher_ticket_data;

      VoucherList = new ArrayList();
      _operation_balance = new MultiPromos.AccountBalance();
      _voucher_tickets = new List<VoucherValidationNumber>();
      TicketNoDiscardable = 0;

      try
      {
        // Read account balance
        if (!MultiPromos.Trx_UpdateAccountBalance(CardData.AccountId, MultiPromos.AccountBalance.Zero, 0, out _ac_ini_balance, out _ini_points, Trx))
        {
          return false;
        }

        // Check if ticket can be discarded is reversible
        switch (CodeOperation)
        {
          case OperationCode.PROMOTION:
            if (!IsTITOTicketAvoidable(CardData, OperationId, CashierSessionInfo.CashierSessionId, CodeOperation, TicketsUndo, out _account_operation,
                               out _iso_type, out _iso_amount, out _error, out TicketNoDiscardable, Trx))
            {

              return false;
            }
            break;

          default:
            return false;
        }

        _operation_balance = new MultiPromos.AccountBalance(-_account_operation.Redeemable, -_account_operation.PromoRedeemable, -_account_operation.PromoNonRedeemable);

        // Insert undo operation
        if (!DB_InsertUndoOperation(_account_operation, CashierSessionInfo.CashierSessionId, out _undo_operation_id, Trx))
        {
          return false;
        }

        _initial_balance = _ac_ini_balance.TotalBalance;
        _initial_balance_points = _ini_points;
        _final_balance_points = 0;
        _final_balance = 0;
        _sub_amount = 0;
        _add_amount = 0;

        foreach (Ticket _ticket_undo in TicketsUndo)
        {
          _account_movements_ids = new List<Int64>();
          _cahier_movements_ids = new List<Int64>();

          // Read a operation account movements        
          _sb = new StringBuilder();
          _sb.AppendLine("  SELECT TOP 1   AM_MOVEMENT_ID                                                                ");
          _sb.AppendLine("         , AM_ACCOUNT_ID                                                                       ");
          _sb.AppendLine("         , AM_TYPE                                                                             ");
          _sb.AppendLine("         , AM_SUB_AMOUNT                                                                       ");
          _sb.AppendLine("         , AM_ADD_AMOUNT                                                                       ");
          _sb.AppendLine("         , AM_TRACK_DATA                                                                       ");
          _sb.AppendLine("         , AM_DETAILS                                                                          ");
          _sb.AppendLine("    FROM   ACCOUNT_MOVEMENTS WITH(INDEX(IX_am_operation_id))                                   ");
          _sb.AppendLine("   WHERE   AM_OPERATION_ID      = @pOperationId                                                ");
          _sb.AppendLine("     AND   AM_UNDO_STATUS IS NULL                                                              ");
          _sb.AppendLine("     AND   AM_ADD_AMOUNT        = @pAmount                                                     ");

          if (_ticket_undo.TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
          {
            _sb.AppendLine("   AND   AM_TYPE = @pPromoRedeemable                                                         ");
          }
          else if (_ticket_undo.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
          {
            _sb.AppendLine("   AND   AM_TYPE = @pPromoNotRedeemable                                                      ");
          }
          _sb.AppendLine("  UNION                                                                                        ");
          _sb.AppendLine("  SELECT TOP 1  AM_MOVEMENT_ID                                                                 ");
          _sb.AppendLine("         , AM_ACCOUNT_ID                                                                       ");
          _sb.AppendLine("         , AM_TYPE                                                                             ");
          _sb.AppendLine("         , AM_SUB_AMOUNT                                                                       ");
          _sb.AppendLine("         , AM_ADD_AMOUNT                                                                       ");
          _sb.AppendLine("         , AM_TRACK_DATA                                                                       ");
          _sb.AppendLine("         , ISNULL(AM_DETAILS, '')                                                              ");
          _sb.AppendLine("    FROM   ACCOUNT_MOVEMENTS WITH(INDEX(IX_am_operation_id))                                   ");
          _sb.AppendLine("   WHERE   AM_OPERATION_ID      = @pOperationId                                                ");
          _sb.AppendLine("     AND   AM_UNDO_STATUS IS NULL                                                              ");
          _sb.AppendLine("     AND   AM_SUB_AMOUNT        = @pAmount                                                     ");

          if (_ticket_undo.TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
          {
            _sb.AppendLine("   AND   AM_TYPE = @pTicketCreatePromoRedeemable ");
          }
          if (_ticket_undo.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
          {
            _sb.AppendLine("   AND   AM_TYPE = @pTicketCreatePromoNoRedeemable                                           ");
          }

          _sb.AppendLine("ORDER BY   AM_MOVEMENT_ID ASC                                                                  ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = _account_operation.OperationId;
            _cmd.Parameters.Add("@pPromoNotRedeemable", SqlDbType.Int).Value = MovementType.PromotionNotRedeemable;
            _cmd.Parameters.Add("@pPromoRedeemable", SqlDbType.Int).Value = MovementType.PromotionRedeemable;
            _cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = _ticket_undo.Amount;
            _cmd.Parameters.Add("@pTicketCreatePromoRedeemable", SqlDbType.Int).Value = MovementType.TITO_TicketCashierPrintedPromoRedeemable;
            _cmd.Parameters.Add("@pTicketCreatePromoNoRedeemable", SqlDbType.Int).Value = MovementType.TITO_TicketCashierPrintedPromoNotRedeemable;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              MovementType _movement_type;
              String _details;

              _ac_mov_table = new AccountMovementsTable(CashierSessionInfo);
              _original_account_id = 0;
              _original_track_data = "";

              while (_reader.Read())
              {
                _account_movements_ids.Add(_reader.GetInt64(0));
                _sub_amount = _reader.GetDecimal(3);
                _add_amount = _reader.GetDecimal(4);

                _movement_type = (MovementType)_reader.GetInt32(2);

                if (String.IsNullOrEmpty(_original_track_data) && !_reader.IsDBNull(5))
                {
                  _original_track_data = _reader.GetString(5);
                  _original_account_id = _reader.GetInt64(1);
                }

                _details = _reader.GetString(6);

                _final_balance = _initial_balance + _sub_amount - _add_amount;

                switch (_movement_type)
                {
                  // RCI 14-AUG-2014: Correct initial/final balance on Card/Check undo operations.
                  case MovementType.HIDDEN_RechargeNotDoneWithCash:

                    // Don't update final balance for these movements
                    _final_balance = _initial_balance;
                    break;

                  default:
                    break;
                }

                _ac_mov_table.Add(_undo_operation_id, _reader.GetInt64(1), _movement_type, _initial_balance, -_sub_amount, -_add_amount, _final_balance, _details);
                _initial_balance = _final_balance;

              }// end while
            }
          }

          _ac_mov_table.SetAccountTrackData(_original_account_id, _original_track_data);

          // Insert new cashier movements and update cashier session values if it is necessary
          if (!DB_InsertUndoTITOTicketCashierMovements(OperationId, _undo_operation_id, CashierSessionInfo, _ticket_undo, out _exchange_result, out _total_chips, out _cahier_movements_ids, Trx))
          {
            return false;
          }

          _cm_mov_table = new CashierMovementsTable(CashierSessionInfo);

          // Take the REAL final balance
          if (!MultiPromos.Trx_UpdateAccountBalance(_account_operation.AccountId, MultiPromos.AccountBalance.Zero, 0,
                                                    out _ac_fin_balance, out _fin_points, Trx))
          {
            return false;
          }

          // Save new movements
          if (!_ac_mov_table.Save(Trx))
          {
            return false;
          }
          if (!_cm_mov_table.Save(Trx))
          {
            return false;
          }

          // Read split parameters
          if (!Split.ReadSplitParameters(out _splits))
          {
            return false;
          }

          // Cancel tickets
          if (_ticket_undo != null)
          {
            if (!Ticket.Cashier_TicketDiscard(_ticket_undo, CashierSessionInfo.TerminalId, _account_operation.AccountId, Trx))
            {
              return false;
            }
          }

          // Mark old movements as undone
          if (!DB_MarkTITOTicketOldMovementsAsUndone(OperationId, _account_movements_ids, _cahier_movements_ids, Trx))
          {
            return false;
          }

          // Set ticket data for voucher
          _voucher_ticket_data = new VoucherValidationNumber();
          _voucher_ticket_data.ValidationNumber = _ticket_undo.ValidationNumber;
          _voucher_ticket_data.Amount = _ticket_undo.Amount;
          _voucher_ticket_data.ValidationType = _ticket_undo.ValidationType;
          _voucher_ticket_data.TicketType = _ticket_undo.TicketType;
          _voucher_tickets.Add(_voucher_ticket_data);
        }

        // Mark new movements as undone
        if (!DB_MarkTITOTicketMovementsAsUndone(_undo_operation_id, Trx))
        {
          return false;
        }

        // Update operation balance
        if (!Operations.Trx_UpdateOperationBalanceTITO(OperationId, _operation_balance, Trx))
        {
          return false;
        }

        // Create voucher
        VoucherTitoTicketOperation _voucher_temp;
        _voucher_temp = new VoucherTitoTicketOperation(CardData.VoucherAccountInfo(), OperationCode.PROMOTION, true, _voucher_tickets, PrintMode.Print, Trx);
        // DHA 06-MAR-2014: save the voucher in the operation undo
        _voucher_temp.Save(_undo_operation_id, Trx);
        VoucherList.Add(_voucher_temp);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UndoOperation

    //------------------------------------------------------------------------------
    // PURPOSE: Get last reversible cash in/promotion, chips sale or chips purchase operation of an player account
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - OperationId
    //          - CashierSessionId
    //
    //      - OUTPUT:
    //          - AccountOperation
    //          - IsoType
    //          - Isoamount              
    //          - Error
    //
    // RETURNS:
    //      - Boolean: If the operation has been recovered successfully.
    //
    public static Boolean GetLastReversibleOperation(OperationCode CodeOperation, Object CardData, Int64 CashiersessionId,
                                                     out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType,
                                                     out Decimal IsoAmount, out UndoError Error)
    {
      Int64 _dummy_gts_id;
      List<Int64> _dummy_tickets_list;

      _dummy_gts_id = 0;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        switch (CodeOperation)
        {
          case OperationCode.CASH_IN:
          case OperationCode.GIFT_DELIVERY:
          case OperationCode.TITO_OFFLINE:
          case OperationCode.TITO_REISSUE:
            if (TITO.Utils.IsTitoMode())
            {
              return IsTITOReversible((CardData)CardData, -1, CashiersessionId, CodeOperation, out AccountOperation, out IsoType, out IsoAmount, out Error, out _dummy_tickets_list, _db_trx.SqlTransaction);
            }
            else
            {
              return IsCashInReversible((CardData)CardData, -1, CashiersessionId, out AccountOperation, out IsoType, out IsoAmount, out Error, _db_trx.SqlTransaction);
            }

          case OperationCode.CHIPS_SALE:
          case OperationCode.CHIPS_SALE_WITH_RECHARGE:
            return IsChipsSaleReversible((CardData)CardData, -1, CashiersessionId, out AccountOperation, out IsoType, out IsoAmount, out _dummy_gts_id, out _dummy_tickets_list, out Error, _db_trx.SqlTransaction);

          case OperationCode.CHIPS_PURCHASE:
            return IsChipsPurchaseReversible((CardData)CardData, -1, CashiersessionId, out AccountOperation, out IsoType, out IsoAmount, out _dummy_gts_id, out _dummy_tickets_list, out Error, _db_trx.SqlTransaction);

          case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
            return IsChipsPurchaseWithCashOutReversible((CardData)CardData, -1, CashiersessionId, out AccountOperation, out IsoType, out IsoAmount, out _dummy_gts_id, out _dummy_tickets_list, out Error, _db_trx.SqlTransaction);

          case OperationCode.CASH_ADVANCE:
            return IsCashAdvanceReversible((CardData)CardData, -1, CashiersessionId, false, out AccountOperation, out IsoType, out IsoAmount, out _dummy_gts_id, out _dummy_tickets_list, out Error, _db_trx.SqlTransaction);

          case OperationCode.MB_CASH_IN:
          case OperationCode.MB_DEPOSIT:
            return IsMBCashInReversible((MBCardData)CardData, -1, CashiersessionId, CodeOperation, out AccountOperation, out IsoType, out IsoAmount, out Error, _db_trx.SqlTransaction);

          case OperationCode.CURRENCY_EXCHANGE_CHANGE:
            return IsCurrencyExchangeReversible((CardData)CardData, -1, CashiersessionId, CodeOperation, out AccountOperation, out IsoType, out IsoAmount, out Error, _db_trx.SqlTransaction);
          case OperationCode.CURRENCY_EXCHANGE_DEVOLUTION:

            return IsCurrencyExchangeReversible((CardData)CardData, -1, CashiersessionId, CodeOperation, out AccountOperation, out IsoType, out IsoAmount, out Error, _db_trx.SqlTransaction);

          default:
            AccountOperation = null;
            IsoType = null;
            IsoAmount = 0;
            Error = UndoError.NoReversibleOperations;
            return false;
        }

      }
    } // GetLastReversibleOperation

    //------------------------------------------------------------------------------
    // PURPOSE: Get error message
    //
    //  PARAMS:
    //      - INPUT:
    //          - UndoError Error
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String: Error message
    //
    public static String GetErrorMessage(UndoError Error)
    {
      String _str_message;

      switch (Error)
      {
        case OperationUndo.UndoError.NoReversibleOperations:
          _str_message = Resource.String("STR_UNDO_OPERATION_NO_REVERSIBLE");
          break;

        case OperationUndo.UndoError.InsuficientBalance:
          _str_message = Resource.String("STR_UNDO_OPERATION_NOT_ENOUGH_REDEEMABLE");
          break;

        case OperationUndo.UndoError.InsuficientCash:
          _str_message = Resource.String("STR_UNDO_OPERATION_NOT_ENOUGH_CASH");
          break;

        case OperationUndo.UndoError.InsuficientChips:
          _str_message = Resource.String("STR_UNDO_OPERATION_NOT_ENOUGH_CHIPS");
          break;

        case OperationUndo.UndoError.TicketNotValid:
          _str_message = Resource.String("STR_UNDO_OPERATION_TICKET_NOT_VALID");
          break;

        case OperationUndo.UndoError.MBAfterDeposit:
          _str_message = Resource.String("STR_MB_UNDO_RECHARGE_AFTER_DEPOSIT");
          break;

        case OperationUndo.UndoError.MBAfterRecharge:
          _str_message = Resource.String("STR_MB_UNDO_DEPOSIT_AFTER_RECHARGE");
          break;

        case OperationUndo.UndoError.CanceledPromotions:
          _str_message = Resource.String("STR_UNDO_OPERATION_CANCELED_PROMOTIONS");
          break;

        case OperationUndo.UndoError.TicketChipsDealerCopyNotValid:
          _str_message = Resource.String("STR_UNDO_OPERATION_TICKET_CHIPS_DEALER_COPY_NOT_VALID", GeneralParam.GetString("GamingTables", "Cashier.Mode.DealerCopy.Title", ""));
          break;

        default:
          _str_message = Resource.String("STR_UNDO_OPERATION_ERROR");
          break;
      }

      return _str_message.Replace("\\r\\n", "\r\n");
    } // GetErrorMessage

    //------------------------------------------------------------------------------
    // PURPOSE: Gets if there is a Withdrawal to undo
    //
    //  PARAMS:
    //      - INPUT:
    //          - InputUndoOperation Input
    //
    //      - OUTPUT:
    //          - AccountOperation
    //          - Error
    //
    // RETURNS:
    //      - String: Error message
    //
    public static Boolean IsWithdrawReversible(InputUndoOperation Input, out AccountOperations.Operation AccountOperation, out UndoError Error)
    {
      AccountOperations.Operation _account_operation;
      UndoError _error;

      AccountOperation = new AccountOperations.Operation();
      Error = UndoError.Unknown;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        return IsWithdrawReversible(Input.CardData, Input, out _account_operation, out _error, _db_trx.SqlTransaction);
      }
    }

    public static Boolean IsCloseCashierSessionReversible(InputUndoOperation Input, SqlTransaction _sql_trx, out AccountOperations.Operation AccountOperation, out UndoError Error)
    {
      AccountOperations.Operation _account_operation;
      UndoError _error;

      AccountOperation = new AccountOperations.Operation();
      Error = UndoError.Unknown;

      return IsCloseCashierSessionReversible(Input.CardData, Input, out _account_operation, out _error, _sql_trx);
    }

    /// <summary>
    /// Is gaming table tournament prize payout reversible?
    /// </summary>
    private static Boolean IsTournamentPrizePayoutReversible(CardData CardData, Int64 OperationId, Int64 CashierSessionId,
                                                             out AccountOperations.Operation AccountOperation, out CurrencyIsoType IsoType,
                                                             out Decimal IsoAmount, out UndoError Error,
                                                             SqlTransaction Trx)
    {
      AccountOperation = new AccountOperations.Operation();
      IsoType = new CurrencyIsoType();
      IsoAmount = 0;
      Error = UndoError.Unknown;

      try
      {
        if (!GetOperation(CardData.AccountId, OperationId, CashierSessionId, out AccountOperation, OperationCode.GT_TOURNAMENTS_PRIZE_PAYMENT, Trx))
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.Code != OperationCode.GT_TOURNAMENTS_PRIZE_PAYMENT)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        if (AccountOperation.UndoStatus != UndoStatus.None)
        {
          Error = UndoError.NoReversibleOperations;

          return false;
        }

        //// Check if you have enough cash in the cash session to undo the operation
        //if (!HasEnoughCashToUndo(AccountOperation.OperationId, CashierSessionId, out IsoType, out IsoAmount, Trx))
        //{
        //  Error = UndoError.InsuficientCash;
        //  return false;
        //}

        Error = UndoError.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Upadte UndoOperation of undo threshold operation
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="UndoOperationId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean UndoThresholdOperation(Int64 OperationId, Int64 UndoOperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   THRESHOLD_OPERATIONS                     ");
        _sb.AppendLine("    SET   TO_UNDO_OPERATION_ID = @pUndoOperationId ");
        _sb.AppendLine("  WHERE   TO_OPERATION_ID = @pOperationId          ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = (OperationId == 0) ? DBNull.Value : (object)OperationId;

          _cmd.Parameters.Add("@pUndoOperationId", SqlDbType.BigInt).Value = (UndoOperationId == 0) ? DBNull.Value : (object)UndoOperationId;

          if (_cmd.ExecuteNonQuery() > 1)
          {
            Log.Error("UndoThresholdOperation. "); // + m_card_data.AccountId.ToString());

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    #endregion
  }
}
