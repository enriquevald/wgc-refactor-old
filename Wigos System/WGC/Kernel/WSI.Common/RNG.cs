using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace WSI.Common
{
  public static partial class CERTIFIED_RNG
  {
    const UInt64 RANGE_31_BITS = (1L << 31);                                                            // Range of 31 bits ->  2,147,483,648 
    private static UInt64 MAX_RANGE = GeneralParam.GetUInt64("RNG", "CertifiedRange", RANGE_31_BITS);   // Ensure we are using the certified range
    private static RNGCryptoServiceProvider m_rng_csp = new RNGCryptoServiceProvider();                 // Create the RNG
    private static Byte[] m_raw_bytes = new Byte[8];                                                    // Buffer

    //
    // Test if the current selection hits the 'Jackpot'
    // Ex.: If Range equals 1000, 1 of every 1000 calls will return true.
    //
    public static Boolean Hit(UInt64 Range)
    {
      try
      {
        UInt64 _number;

        // Check Range is Less or Equal than the certified range
        if (Range > MAX_RANGE)
        {
          // Out of range --> Use the maximum allowed
          Range = MAX_RANGE;
        }

        _number = GetRandomNumber(Range);

        if (_number == (Range / 2))
        {
          // Hit!
          return true;
        }
      }
      catch
      { }

      return false;
    } // Hit

    //
    // Return a random number in the selected Range
    // Ex: Range = 1000, The random number will be betweeen 0 and 999
    //
    public static UInt64 GetRandomNumber(UInt64 Range)
    {
      UInt64 _n;
      UInt64 _m;
      UInt64 _rnd;

      _n = UInt64.MaxValue / Range;
      _m = Range * _n;

      lock (m_rng_csp)
      {
        do
        {
          m_rng_csp.GetBytes(m_raw_bytes);
          _rnd = BitConverter.ToUInt64(m_raw_bytes, 0);

        } while (_rnd >= _m);
      }

      _rnd = _rnd % Range;

      return _rnd;
    } // GetRandomNumber

    //
    // The following routine is used to avoid casting "signed" ranges to "unsigned" ones.
    //
    public static UInt64 GetRandomNumber(Int64 Range)
    {
      return GetRandomNumber((UInt64)Range);
    }
  }

}
