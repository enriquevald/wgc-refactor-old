﻿//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems Intl.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FeedbackReports.cs
// 
//   DESCRIPTION: 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-OCT-2016 DMT     First release.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common.WinUp
{
  public static class FeedbackReports
  {
    public const Int32 SQL_FEEDBACK_COLUMN_NAME = 0;
    public const Int32 SQL_FEEDBACK_COLUMN_EMAIL = 1;
    public const Int32 SQL_FEEDBACK_COLUMN_TELEPHONE = 2;
    public const Int32 SQL_FEEDBACK_COLUMN_SUBJECT = 3;
    public const Int32 SQL_FEEDBACK_COLUMN_MESSAGE = 4;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns a Datatable containing the feedbackReport.
    //
    //  PARAMS :
    //      - INPUT : InitialDate: Datetime
    //                FinalDate:   Datetime
    //                Result: Out parameter Datatable
    //
    //      - OUTPUT :
    //
    // RETURNS : true-> ok false-> error
    //
    //   NOTES :
    //
    public static Boolean GetFeedbackReport(DateTime InitialDate, DateTime FinalDate, out DataTable Result)
    {
      StringBuilder _sb;
      String _base_date;

      _base_date = "01/01/2007 " + GeneralParam.GetInt32("WigosGUI", "ClosingTime").ToString("00") + ":00:00";

      Result = new DataTable();
      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT A.AC_HOLDER_NAME AS NAME, A.AC_HOLDER_EMAIL_01 AS EMAIL, A.AC_HOLDER_PHONE_NUMBER_01 AS TELEPHONE,  MF.SUBJECT, MF.MESSAGE, MF.SEND_DATE");
      
      _sb.AppendLine(" FROM MAPP_FEEDBACK MF");
      _sb.AppendLine(" LEFT JOIN ACCOUNTS A ON MF.ACCOUNT_ID = A.AC_ACCOUNT_ID");
      _sb.AppendLine(" WHERE " + GetSqlWhereSendDate(InitialDate, FinalDate) + " ");
 
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (InitialDate != DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pIniDateRange", SqlDbType.DateTime).Value = InitialDate;
            }

            if (FinalDate != DateTime.MaxValue)
            {
              _cmd.Parameters.Add("@pFinDateRange", SqlDbType.DateTime).Value = FinalDate;
            }

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(Result);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    private static String GetSqlWhereSendDate(DateTime IniDate, DateTime FinDate)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      //if (IniDate != DateTime.MinValue || FinDate != DateTime.MaxValue)
      //{
      //  _sb.AppendLine(" AND ");
      //}

      if (IniDate != DateTime.MinValue)
      {
        _sb.AppendLine(" MF.SEND_DATE >= DATEADD(DAY, -2, @pIniDateRange) AND ");
      }

      if (FinDate != DateTime.MaxValue)
      {
        _sb.AppendLine(" MF.SEND_DATE < @pFinDateRange ");
      }
      else
      {
        _sb = _sb.Replace(" AND ", "");
      }

      return _sb.ToString();

    }

    private static String GetSqlWhereCreated()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" WHERE ");
      _sb.AppendLine(" MF.ACCOUNT_ID = A.AC_ACCOUNT_ID ");


      _sb.AppendLine("OR MF.ACCOUNT_ID = A.AC_ACCOUNT_ID ");



      return _sb.ToString();

    }
  }
}
