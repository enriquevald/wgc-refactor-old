﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwards.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Rubén Lama Ordóñez
// 
// CREATION DATE: 31-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAY-2017 RLO    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwards : JackpotBase
  {
    #region " Constants "

    // Private constants    
    private const Int32 DB_ID             = 0;
    private const Int32 DB_JACKPOT_ID     = 1;
    private const Int32 DB_TYPE           = 2;
    private const Int32 DB_AMOUNT         = 3;
    private const Int32 DB_RELATED_ID     = 4;
    private const Int32 DB_DATE_CREATION  = 5;
    
    #endregion

    #region " Properties "

    public Int32 JackpotId { get; set; }
    public Int32 Type { get; set; }
    public Decimal Amount { get; set; }
    public Int32 RelatedId { get; set; }
    public DateTime DateCreation { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's 
    /// </summary>
    public JackpotAwards()
    {
      this.Id           = DEFAULT_ID_VALUE;
      this.JackpotId    = DEFAULT_ID_VALUE;
      this.Type         = DEFAULT_INT_VALUE;
      this.Amount       = DEFAULT_DECIMAL_VALUE;
      this.RelatedId    = DEFAULT_INT_VALUE;
      this.DateCreation = WGDB.MinDate;
    } // JackpotAwards

    /// <summary>
    // Read Jackpot Award list
    /// </summary>
    /// <param name="JackpotAwardId"></param>
    /// <param name="AwardList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetJackpotAwardsByJackpotAwardId(Int32 JackpotAwardId, out List<JackpotAwardItem> AwardList, SqlTransaction SqlTrx)
    {
      return this.DB_GetJackpotAwards_Internal(JackpotAwardId, out AwardList, SqlTrx);
    } // GetJackpotAwardsByJackpotAwardId

    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific Jackpot Award Id 
    /// </summary>
    /// <param name="JackpotAwardId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotAwards_Internal(Int32 JackpotAwardId, out List<JackpotAwardItem> AwardList, SqlTransaction SqlTrx)
    {
      AwardList = new List<JackpotAwardItem>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotAwardsQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotAwardId", SqlDbType.Int).Value = JackpotAwardId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!DB_JackpotAwardsDataMapper(_sql_reader))
              {
                return false;
              }

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwards.DB_GetJackpotAwards_Internal -> JackpotAwardsId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotAwards_Internal

    /// <summary>
    /// Load Jackpot Awarded data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotAwardsDataMapper(SqlDataReader SqlReader)
    {
      try
      {
        this.Id           = SqlReader.GetInt32(DB_ID);
        this.JackpotId    = SqlReader.GetInt32(DB_JACKPOT_ID);
        this.Type         = SqlReader.GetInt32(DB_TYPE);
        this.Amount       = SqlReader.GetDecimal(DB_AMOUNT);
        this.RelatedId    = SqlReader.GetInt32(DB_RELATED_ID);
        this.DateCreation = SqlReader.GetDateTime(DB_DATE_CREATION);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotAwardsDataMapper");
        Log.Exception(_ex);
      }

      return false;
      
    } // DB_JackpotAwardsDataMapper 
   
    /// <summary>
    /// Gets Jackpot Awards select query
    /// </summary>
    /// <returns></returns>
    private String DB_JackpotAwardsQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT       JAD_ID                         "); // 0
      _sb.AppendLine("               , JS_JACKPOT_ID                  "); // 1
      _sb.AppendLine("               , JS_NAME                        "); // 2
      _sb.AppendLine("               , JA_TYPE                        "); // 3
      _sb.AppendLine("               , JAD_AMOUNT                     "); // 4
      _sb.AppendLine("               , JAD_POSITION                   "); // 5
      _sb.AppendLine("               , JAD_TERMINAL_ID                "); // 6
      _sb.AppendLine("               , JAD_ACCOUNT_ID                 "); // 7
      _sb.AppendLine("               , JAD_DATE_AWARD                 "); // 8
      _sb.AppendLine("          FROM   JACKPOTS_AWARD                 ");
      _sb.AppendLine("    INNER JOIN   JACKPOTS_AWARD_DETAIL          ");
      _sb.AppendLine("            ON   JAD_AWARD_ID = JA_ID           ");
      _sb.AppendLine("    INNER JOIN   JACKPOTS_SETTINGS              ");
      _sb.AppendLine("            ON   JA_JACKPOT_ID = JS_JACKPOT_ID  ");
      _sb.AppendLine("         WHERE   JA_ID = @pJackpotAwardId       ");
      _sb.AppendLine("      ORDER BY   JS_JACKPOT_ID, JAD_POSITION    ");

      return _sb.ToString();

    } // DB_JackpotAwardsQuery_Select  
    
    #endregion " Private Methods "

    #region " Internal Methods "
    
    /// <summary>
    /// Clone JackpotAwards
    /// </summary>
    /// <returns></returns>
    internal JackpotAwards Clone()
    {
      return new JackpotAwards()
      {
        Id            = this.Id,                    
        JackpotId     = this.JackpotId,
        Type          = this.Type,
        Amount        = this.Amount,
        RelatedId     = this.RelatedId,
        DateCreation  = this.DateCreation
      };
    } // Clone

    #endregion 
  } // JackpotAwards
}
