﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardFilterFlag.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 30-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAR-2017 JBP    First release.
// 31-MAR-2017 RLO    Add Table fields
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardFilterFlag : JackpotBase
  {
    #region " Constants "

    private const Int32 DB_ID         = 0;
    private const Int32 DB_FLAG_ID    = 1;
    private const Int32 DB_NAME       = 2;
    private const Int32 DB_COLOR      = 3;
    private const Int32 DB_TYPE       = 4;
    private const Int32 DB_FLAG_COUNT = 5;
    
    #endregion

    #region " Properties "
    
    public Int32 JackpotId { get; set; }
    public Int32 Type { get; set; }
    public Int64 FlagId { get; set; }
    public Int32 FlagCount { get; set; }
    public Int32 Color { get; set; }
    public String Name { get; set; }
    public Boolean IsDeleted { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by JackpotId
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotAwardFilterFlag()
    {
      this.Id = DEFAULT_ID_VALUE;
      
      this.LastUpdate = WGDB.MinDate;
      this.LastModification = WGDB.MinDate;
    } // JackpotAwardFilterFlag

    /// <summary>
    /// Read Jackpot Award Flags
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="JackpotAwardFilterFlagsList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetAllFlags(Int32 JackpotId, out List<JackpotAwardFilterFlag> JackpotAwardFilterFlagsList, SqlTransaction SqlTrx)
    {
      return this.DB_GetAllFlags_Internal(JackpotId, out JackpotAwardFilterFlagsList, SqlTrx);
    } // Read

    public override Boolean Save(SqlTransaction SqlTrx)
    {
      if (this.IsNew)
      {
        if (!this.IsDeleted)
        {
          return this.DB_Insert(SqlTrx);
        }

        return true;
      }

      if (this.IsModified)
      {
        if (this.IsDeleted)
        {
          return this.DB_Delete(SqlTrx);
        }

        return this.DB_Update(SqlTrx);
      }

      return true;
    } // Save

    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.JackpotId = DEFAULT_ID_VALUE;
    } // Reset

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Get data from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetAllFlags_Internal(Int32 JackpotId, out List<JackpotAwardFilterFlag> JackpotAwardFilterFlagsList, SqlTransaction SqlTrx)
    {
      JackpotAwardFilterFlag _flag;

      JackpotAwardFilterFlagsList = new List<JackpotAwardFilterFlag>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_GetAllFlagQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _flag = JackpotAwardFlagMappFromSqlReader(_sql_reader);
              _flag.JackpotId = JackpotId;

              if (_flag != null)
              {
                JackpotAwardFilterFlagsList.Add(_flag);
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardFilterFlags.DB_GetAllFlags_Internal -> JackpotId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotAwardFilterFlags_Internal

    /// <summary>
    /// Delete an specific Jackpot flag
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_Delete(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" DELETE   JACKPOTS_AWARD_FILTERS_FLAGS  ");
        _sb.AppendLine("  WHERE   JAF_ID = @pId                 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = this.Id;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardFilterFlags.DB_Delete -> JackpotId: {0}", this.JackpotId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardFilterFlags.DB_Delete -> JackpotAwardFilterFlagsId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Update

    /// <summary>
    /// Load Jackpot Award Days data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>

    /// <summary>
    /// Mapp jackpot award filter flags from sql reader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private JackpotAwardFilterFlag JackpotAwardFlagMappFromSqlReader(SqlDataReader SqlReader)
    {
      JackpotAwardFilterFlag _jackpot_flags;

      _jackpot_flags = new JackpotAwardFilterFlag();

      if (!this.DB_JackpotAwardFilterFlagDataMapper(_jackpot_flags, SqlReader))
      {
        Log.Error("JackpotContribution.JackpotAwardFlagMappFromSqlReader -> Error on load Jackpot flags");
      }

      return _jackpot_flags;
    } // JackpotAward FlagsMappFromSqlReader

    /// <summary>
    /// Load Jackpot Jackpot Award Filter Flags Group data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotAwardFilterFlagDataMapper(SqlDataReader SqlReader)
    {
      return this.DB_JackpotAwardFilterFlagDataMapper(this, SqlReader);
    } // DB_JackpotAwardFilterFlagDataMapper
    private Boolean DB_JackpotAwardFilterFlagDataMapper(JackpotAwardFilterFlag JackpotAwardFilterFlag, SqlDataReader SqlReader)
    {
      try
      {
        JackpotAwardFilterFlag.Id         = SqlReader.IsDBNull(DB_ID) ? DEFAULT_ID_VALUE : SqlReader.GetInt32(DB_ID);
        JackpotAwardFilterFlag.FlagId     = SqlReader.GetInt64(DB_FLAG_ID);
        JackpotAwardFilterFlag.Name       = SqlReader.GetString(DB_NAME);
        JackpotAwardFilterFlag.Color      = SqlReader.GetInt32(DB_COLOR);
        JackpotAwardFilterFlag.Type       = SqlReader.GetInt32(DB_TYPE);
        JackpotAwardFilterFlag.FlagCount  = SqlReader.IsDBNull(DB_FLAG_COUNT) ? DEFAULT_ID_VALUE : SqlReader.GetInt32(DB_FLAG_COUNT);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotAwardFilterFlagDataMapper");
        Log.Exception(_ex);
      }

      JackpotAwardFilterFlag = null;

      return false;

    } // DB_JackpotAwardFilterFlagDataMapper 

    /// <summary>
    /// Gets Jackpot Award Days select query
    /// </summary>
    /// <returns></returns>
    private String DB_GetAllFlagQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("  WITH ALL_FLAGS AS                                   ");
      _sb.AppendLine("  (                                                   ");
      _sb.AppendLine("    SELECT   FL_FLAG_ID AS FLAG_ID                    ");
      _sb.AppendLine("           , FL_NAME    AS FLAG_NAME                  ");
      _sb.AppendLine("           , FL_COLOR   AS FLAG_COLOR                 ");
      _sb.AppendLine("           , FL_TYPE    AS FLAG_TYPE                  ");
      _sb.AppendLine("      FROM   FLAGS                                    ");
      _sb.AppendLine("  )                                                   ");
      _sb.AppendLine("     SELECT   JAF_ID AS JACKPOT_FLAG_ID               ");
      _sb.AppendLine("            , FLAG_ID                                 ");
      _sb.AppendLine("            , FLAG_NAME                               ");
      _sb.AppendLine("            , FLAG_COLOR                              ");
      _sb.AppendLine("            , FLAG_TYPE                               ");
      _sb.AppendLine("            , ISNULL(JAF_FLAG_COUNT, 0) AS FLAG_COUNT ");
      _sb.AppendLine("       FROM   ALL_FLAGS                               ");
      _sb.AppendLine("  LEFT JOIN   JACKPOTS_AWARD_FILTERS_FLAGS AS JVJ     ");
      _sb.AppendLine("         ON   JAF_FLAG_ID    = FLAG_ID                ");
      _sb.AppendLine("        AND   JAF_TYPE       = FLAG_TYPE              ");
      _sb.AppendLine("        AND   JAF_JACKPOT_ID = @pJackpotId            ");
      _sb.AppendLine("   ORDER BY   FLAG_TYPE                               ");

      return _sb.ToString();

    } // DB_GetAllFlagQuery_Select  

    #endregion " Private Methods "

    #region " Internal Methods "

    
    /// <summary>
    /// Insert an specific Jackpot Award Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      SqlParameter _op;
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("   INSERT INTO   JACKPOTS_AWARD_FILTERS_FLAGS   ");
        _sb.AppendLine("   			       ( JAF_JACKPOT_ID                 ");
        _sb.AppendLine("   			       , JAF_FLAG_ID                    ");
        _sb.AppendLine("   			       , JAF_TYPE                       ");
        _sb.AppendLine("   			       , JAF_FLAG_COUNT )               ");
        _sb.AppendLine("        VALUES                                  ");
        _sb.AppendLine("               ( @pJackpotId                    ");
        _sb.AppendLine("   			       , @pFlagId                       ");
        _sb.AppendLine("   			       , @pType                         ");
        _sb.AppendLine("   			       , @pCount )                      ");

        _sb.AppendLine(" SET     @pId = SCOPE_IDENTITY()                  ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId" , SqlDbType.Int).Value = this.JackpotId;
          _cmd.Parameters.Add("@pFlagId"    , SqlDbType.Int).Value = this.FlagId;
          _cmd.Parameters.Add("@pType"      , SqlDbType.Int).Value = this.Type;
          _cmd.Parameters.Add("@pCount"     , SqlDbType.Int).Value = this.FlagCount;
          
          //Output parameter
          _op = new SqlParameter("@pId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardFilterFlags.DB_Insert -> JackpotId: {0}", this.JackpotId));

            return false;
          }

          this.Id = (Int32)_op.Value;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardFilterFlags.DB_Insert -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Jackpot Award Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" UPDATE   JACKPOTS_AWARD_FILTERS_FLAGS  ");
        _sb.AppendLine("    SET   JAF_JACKPOT_ID  = @pJackpotId ");
        _sb.AppendLine("        , JAF_TYPE        = @pType      ");
        _sb.AppendLine("        , JAF_FLAG_ID     = @pFlagId    ");
        _sb.AppendLine("        , JAF_FLAG_COUNT  = @pFlagCount ");
        _sb.AppendLine("  WHERE   JAF_ID          = @pId        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId"        , SqlDbType.Int).Value = this.Id;
          _cmd.Parameters.Add("@pJackpotId" , SqlDbType.Int).Value = this.JackpotId;
          _cmd.Parameters.Add("@pFlagId"    , SqlDbType.Int).Value = this.FlagId;
          _cmd.Parameters.Add("@pType"      , SqlDbType.Int).Value = this.Type;
          _cmd.Parameters.Add("@pFlagCount" , SqlDbType.Int).Value = this.FlagCount;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardFilterFlags.DB_Update -> JackpotId: {0}", this.JackpotId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
    {
        Log.Error(String.Format("JackpotAwardFilterFlags.DB_Update -> JackpotAwardFilterFlagsId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Update

    /// <summary>
    /// Clone JackpotAwardFilterFlag
    /// </summary>
    /// <returns></returns>
    internal JackpotAwardFilterFlag Clone()
    {
      return new JackpotAwardFilterFlag()
      {
        Id        = this.Id, 
        JackpotId = this.JackpotId,
        FlagId    = this.FlagId,
        FlagCount = this.FlagCount,
        Type      = this.Type,
        Color     = this.Color,
        Name      = this.Name
             
      };
    } // Clone

    #endregion

  } // JackpotAwardFilterFlag
}
