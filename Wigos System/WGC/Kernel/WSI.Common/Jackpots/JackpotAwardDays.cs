﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardDays.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 30-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAR-2017 JBP    First release.
// 31-MAR-2017 RLO    Add Table fields
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardDays : JackpotBase
  {

    #region " Constants "

    // Private constants
    private const Int32 DB_ID               = 0;
    private const Int32 DB_JACKPOT_ID       = 1;
    private const Int32 DB_MONDAY_FROM      = 2;
    private const Int32 DB_MONDAY_TO        = 3;
    private const Int32 DB_TUESDAY_FROM     = 4;
    private const Int32 DB_TUESDAY_TO       = 5;
    private const Int32 DB_WEDNESDAY_FROM   = 6;
    private const Int32 DB_WEDNESDAY_TO     = 7;
    private const Int32 DB_THURSDAY_FROM    = 8;
    private const Int32 DB_THURSDAY_TO      = 9;
    private const Int32 DB_FRIDAY_FROM      = 10;
    private const Int32 DB_FRIDAY_TO        = 11;
    private const Int32 DB_SATURDAY_FROM    = 12;
    private const Int32 DB_SATURDAY_TO      = 13;
    private const Int32 DB_SUNDAY_FROM      = 14;
    private const Int32 DB_SUNDAY_TO        = 15;
    private const Int32 DB_LAST_UPDATE      = 16;

    #endregion

    #region " Properties "
   
    public Int32 JackpotId { get; set; }
    public Int32 MondayFrom { get; set; }
    public Int32 MondayTo { get; set; }
    public Int32 TuesdayFrom { get; set; }
    public Int32 TuesdayTo { get; set; }
    public Int32 WednesdayFrom { get; set; }
    public Int32 WednesdayTo { get; set; }
    public Int32 ThursdayFrom { get; set; }
    public Int32 ThursdayTo { get; set; }
    public Int32 FridayFrom { get; set; }
    public Int32 FridayTo { get; set; }
    public Int32 SaturdayFrom { get; set; }
    public Int32 SaturdayTo { get; set; }
    public Int32 SundayFrom { get; set; }
    public Int32 SundayTo { get; set; }
    
    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by JackpotId
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotAwardDays()
    {
      this.Id               = DEFAULT_ID_VALUE;
      this.MondayFrom       = DEFAULT_TIME_FROM_VALUE;
      this.MondayTo         = DEFAULT_TIME_TO_VALUE;
      this.TuesdayFrom      = DEFAULT_TIME_FROM_VALUE;
      this.TuesdayTo        = DEFAULT_TIME_TO_VALUE;
      this.WednesdayFrom    = DEFAULT_TIME_FROM_VALUE;
      this.WednesdayTo      = DEFAULT_TIME_TO_VALUE;
      this.ThursdayFrom     = DEFAULT_TIME_FROM_VALUE;
      this.ThursdayTo       = DEFAULT_TIME_TO_VALUE;
      this.FridayFrom       = DEFAULT_TIME_FROM_VALUE;
      this.FridayTo         = DEFAULT_TIME_TO_VALUE;
      this.SaturdayFrom     = DEFAULT_TIME_FROM_VALUE;
      this.SaturdayTo       = DEFAULT_TIME_TO_VALUE;
      this.SundayFrom       = DEFAULT_TIME_FROM_VALUE;
      this.SundayTo         = DEFAULT_TIME_TO_VALUE;
      this.LastUpdate       = WGDB.MinDate;
      this.LastModification = WGDB.MinDate;

    } // JackpotAwardDays

    // Read Jackpot contribution
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.DB_GetJackpotAwardDays_Internal(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      return this.DB_GetJackpotAwardDays_Internal(JackpotId, SqlTrx);
    } // Read

    /// <summary>
    /// Hour in minutes to datetime
    /// </summary>
    /// <param name="Minutes"></param>
    /// <returns></returns>
    private DateTime HourInMinutesToDate(Int32 Minutes)
    {
      return new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day).AddMinutes(Minutes);
    } // HourInMinutesToDate

    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.JackpotId = DEFAULT_ID_VALUE;
    } // Reset
        
    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotAwardDays_Internal(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotAwardDaysQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!DB_JackpotAwardDaysDataMapper(_sql_reader))
              {
                return false;
              }

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardDays.DB_GetJackpot -> JackpotAwardDaysId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotAwardDays_Internal

    /// <summary>
    /// Load Jackpot Award Days data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotAwardDaysDataMapper(SqlDataReader SqlReader)
    {
      try
      {
        this.Id             = SqlReader.GetInt32(DB_ID);
        this.JackpotId     = SqlReader.GetInt32(DB_JACKPOT_ID);
        this.FridayFrom    = SqlReader.IsDBNull(DB_FRIDAY_FROM)    ? DEFAULT_TIME_FROM_VALUE : SqlReader.GetInt32(DB_FRIDAY_FROM);
        this.FridayTo      = SqlReader.IsDBNull(DB_FRIDAY_TO)      ? DEFAULT_TIME_TO_VALUE   : SqlReader.GetInt32(DB_FRIDAY_TO);
        this.MondayFrom    = SqlReader.IsDBNull(DB_MONDAY_FROM)    ? DEFAULT_TIME_FROM_VALUE : SqlReader.GetInt32(DB_MONDAY_FROM);
        this.MondayTo      = SqlReader.IsDBNull(DB_MONDAY_TO)      ? DEFAULT_TIME_TO_VALUE   : SqlReader.GetInt32(DB_MONDAY_TO);
        this.TuesdayFrom   = SqlReader.IsDBNull(DB_TUESDAY_FROM)   ? DEFAULT_TIME_FROM_VALUE : SqlReader.GetInt32(DB_TUESDAY_FROM);
        this.TuesdayTo     = SqlReader.IsDBNull(DB_TUESDAY_TO)     ? DEFAULT_TIME_TO_VALUE   : SqlReader.GetInt32(DB_TUESDAY_TO);
        this.WednesdayFrom = SqlReader.IsDBNull(DB_WEDNESDAY_FROM) ? DEFAULT_TIME_FROM_VALUE : SqlReader.GetInt32(DB_WEDNESDAY_FROM);
        this.WednesdayTo   = SqlReader.IsDBNull(DB_WEDNESDAY_TO)   ? DEFAULT_TIME_TO_VALUE   : SqlReader.GetInt32(DB_WEDNESDAY_TO);
        this.ThursdayFrom  = SqlReader.IsDBNull(DB_THURSDAY_FROM)  ? DEFAULT_TIME_FROM_VALUE : SqlReader.GetInt32(DB_THURSDAY_FROM);
        this.ThursdayTo    = SqlReader.IsDBNull(DB_THURSDAY_TO)    ? DEFAULT_TIME_TO_VALUE   : SqlReader.GetInt32(DB_THURSDAY_TO);
        this.SaturdayFrom  = SqlReader.IsDBNull(DB_SATURDAY_FROM)  ? DEFAULT_TIME_FROM_VALUE : SqlReader.GetInt32(DB_SATURDAY_FROM);
        this.SaturdayTo    = SqlReader.IsDBNull(DB_SATURDAY_TO)    ? DEFAULT_TIME_TO_VALUE   : SqlReader.GetInt32(DB_SATURDAY_TO);
        this.SundayFrom    = SqlReader.IsDBNull(DB_SUNDAY_FROM)    ? DEFAULT_TIME_FROM_VALUE : SqlReader.GetInt32(DB_SUNDAY_FROM);
        this.SundayTo      = SqlReader.IsDBNull(DB_SUNDAY_TO)      ? DEFAULT_TIME_TO_VALUE   : SqlReader.GetInt32(DB_SUNDAY_TO);

        // Last update
        this.LastUpdate = SqlReader.IsDBNull(DB_LAST_UPDATE) ? WGDB.MinDate : SqlReader.GetDateTime(DB_LAST_UPDATE);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotDataMapper");
        Log.Exception(_ex);
      }

      return false;
      
    } // DB_JackpotAwardDaysDataMapper 
   
    /// <summary>
    /// Gets Jackpot Award Days select query
    /// </summary>
    /// <returns></returns>
    private String DB_JackpotAwardDaysQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();      

      _sb.AppendLine("    SELECT   JSA_ID                       "); // 0
      _sb.AppendLine("           , JSA_JACKPOT_ID               "); // 1
      _sb.AppendLine("           , JSA_MONDAY_FROM              "); // 2
      _sb.AppendLine("           , JSA_MONDAY_TO                "); // 3
      _sb.AppendLine("           , JSA_TUESDAY_FROM             "); // 4
      _sb.AppendLine("           , JSA_TUESDAY_TO               "); // 5
      _sb.AppendLine("           , JSA_WEDNESDAY_FROM           "); // 6
      _sb.AppendLine("           , JSA_WEDNESDAY_TO             "); // 7
      _sb.AppendLine("           , JSA_THURSDAY_FROM            "); // 8
      _sb.AppendLine("           , JSA_THURSDAY_TO              "); // 9
      _sb.AppendLine("           , JSA_FRIDAY_FROM              "); // 10
      _sb.AppendLine("           , JSA_FRIDAY_TO                "); // 11
      _sb.AppendLine("           , JSA_SATURDAY_FROM            "); // 12
      _sb.AppendLine("           , JSA_SATURDAY_TO              "); // 13
      _sb.AppendLine("           , JSA_SUNDAY_FROM              "); // 14
      _sb.AppendLine("           , JSA_SUNDAY_TO                "); // 15
      _sb.AppendLine("           , JSA_LAST_UPDATE              "); // 16
      _sb.AppendLine("      FROM   JACKPOTS_SETTINGS_AWARDING   ");
      _sb.AppendLine("     WHERE   JSA_JACKPOT_ID = @pJackpotId ");

      return _sb.ToString();

    } // DB_JackpotAwardDaysQuery_Select  
    
    #endregion " Private Methods "

    #region " Internal Methods "

    /// <summary>
    /// Insert an specific Jackpot Award Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      SqlParameter _op;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   JACKPOTS_SETTINGS_AWARDING   ");
        _sb.AppendLine("             ( JSA_JACKPOT_ID               ");
        _sb.AppendLine("             , JSA_MONDAY_FROM              ");
        _sb.AppendLine("             , JSA_MONDAY_TO                ");
        _sb.AppendLine("             , JSA_TUESDAY_FROM             ");
        _sb.AppendLine("             , JSA_TUESDAY_TO               ");
        _sb.AppendLine("             , JSA_WEDNESDAY_FROM           ");
        _sb.AppendLine("             , JSA_WEDNESDAY_TO             ");
        _sb.AppendLine("             , JSA_THURSDAY_FROM            ");
        _sb.AppendLine("             , JSA_THURSDAY_TO              ");
        _sb.AppendLine("             , JSA_FRIDAY_FROM              ");
        _sb.AppendLine("             , JSA_FRIDAY_TO                ");
        _sb.AppendLine("             , JSA_SATURDAY_FROM            ");
        _sb.AppendLine("             , JSA_SATURDAY_TO              ");
        _sb.AppendLine("             , JSA_SUNDAY_FROM              ");
        _sb.AppendLine("             , JSA_SUNDAY_TO                ");
        _sb.AppendLine("             , JSA_LAST_UPDATE      )       ");
        _sb.AppendLine("      VALUES                                ");
        _sb.AppendLine("             ( @pJackpotId                  ");
        _sb.AppendLine("             , @pJackpotMondayFrom          ");
        _sb.AppendLine("             , @pJackpotMondayTo            ");
        _sb.AppendLine("             , @pJackpotTuesdayFrom         ");
        _sb.AppendLine("             , @pJackpotTuesdayTo           ");
        _sb.AppendLine("             , @pJackpotWednesdayFrom       ");
        _sb.AppendLine("             , @pJackpotWednesdayTo         ");
        _sb.AppendLine("             , @pJackpotThursdayFrom        ");
        _sb.AppendLine("             , @pJackpotThursdayTo          ");
        _sb.AppendLine("             , @pJackpotFridayFrom          ");
        _sb.AppendLine("             , @pJackpotFridayTo            ");
        _sb.AppendLine("             , @pJackpotSaturdayFrom        ");
        _sb.AppendLine("             , @pJackpotSaturdayTo          ");
        _sb.AppendLine("             , @pJackpotSundayFrom          ");
        _sb.AppendLine("             , @pJackpotSundayTo            ");
        _sb.AppendLine("             , @pLastUpdate           )     ");

        _sb.AppendLine(" SET     @pId = SCOPE_IDENTITY()            ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId"           , SqlDbType.Int).Value = this.JackpotId;
          _cmd.Parameters.Add("@pJackpotMondayTo"     , SqlDbType.Int).Value = this.MondayTo;
          _cmd.Parameters.Add("@pJackpotMondayFrom"   , SqlDbType.Int).Value = this.MondayFrom;
          _cmd.Parameters.Add("@pJackpotTuesdayTo"    , SqlDbType.Int).Value = this.TuesdayTo;
          _cmd.Parameters.Add("@pJackpotTuesdayFrom"  , SqlDbType.Int).Value = this.TuesdayFrom;
          _cmd.Parameters.Add("@pJackpotWednesdayTo"  , SqlDbType.Int).Value = this.WednesdayTo;
          _cmd.Parameters.Add("@pJackpotWednesdayFrom", SqlDbType.Int).Value = this.WednesdayFrom;
          _cmd.Parameters.Add("@pJackpotThursdayTo"   , SqlDbType.Int).Value = this.ThursdayTo;
          _cmd.Parameters.Add("@pJackpotThursdayFrom" , SqlDbType.Int).Value = this.ThursdayFrom;
          _cmd.Parameters.Add("@pJackpotFridayTo"     , SqlDbType.Int).Value = this.FridayTo;
          _cmd.Parameters.Add("@pJackpotFridayFrom"   , SqlDbType.Int).Value = this.FridayFrom;
          _cmd.Parameters.Add("@pJackpotSaturdayTo"   , SqlDbType.Int).Value = this.SaturdayTo;
          _cmd.Parameters.Add("@pJackpotSaturdayFrom" , SqlDbType.Int).Value = this.SaturdayFrom;
          _cmd.Parameters.Add("@pJackpotSundayTo"     , SqlDbType.Int).Value = this.SundayTo;
          _cmd.Parameters.Add("@pJackpotSundayFrom"   , SqlDbType.Int).Value = this.SundayFrom;
          
          // Last update
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = (this.LastUpdate == WGDB.MinDate) ? DBNull.Value : (Object)this.LastUpdate;

          //Output parameter
          _op = new SqlParameter("@pId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);
          
          if (_cmd.ExecuteNonQuery() != 1)
          {
            this.Id = (Int32)_op.Value;

            Log.Error(String.Format("JackpotAwardDays.DB_Insert -> JackpotId: {0}", this.JackpotId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardDays.DB_Insert -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Jackpot Award Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   JACKPOTS_SETTINGS_AWARDING                    ");
        _sb.AppendLine("    SET   JSA_JACKPOT_ID      = @pJackpotId             ");
        _sb.AppendLine("        , JSA_MONDAY_FROM     = @pJackpotMondayFrom     ");
        _sb.AppendLine("        , JSA_MONDAY_TO       = @pJackpotMondayTo       ");
        _sb.AppendLine("        , JSA_TUESDAY_FROM    = @pJackpotTuesdayFrom    ");
        _sb.AppendLine("        , JSA_TUESDAY_TO      = @pJackpotTuesdayTo      ");
        _sb.AppendLine("        , JSA_WEDNESDAY_FROM  = @pJackpotWednesdayFrom  ");
        _sb.AppendLine("        , JSA_WEDNESDAY_TO    = @pJackpotWednesdayTo    ");
        _sb.AppendLine("        , JSA_THURSDAY_FROM   = @pJackpotThursdayFrom   ");
        _sb.AppendLine("        , JSA_THURSDAY_TO     = @pJackpotThursdayTo     ");
        _sb.AppendLine("        , JSA_FRIDAY_FROM     = @pJackpotFridayFrom     ");
        _sb.AppendLine("        , JSA_FRIDAY_TO       = @pJackpotFridayTo       ");
        _sb.AppendLine("        , JSA_SATURDAY_FROM   = @pJackpotSaturdayFrom   ");
        _sb.AppendLine("        , JSA_SATURDAY_TO     = @pJackpotSaturdayTo     ");
        _sb.AppendLine("        , JSA_SUNDAY_FROM     = @pJackpotSundayFrom     ");
        _sb.AppendLine("        , JSA_SUNDAY_TO       = @pJackpotSundayTo       ");
        _sb.AppendLine("        , JSA_LAST_UPDATE     = @pLastUpdate            ");
        _sb.AppendLine("  WHERE   JSA_ID              = @pId                    ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId"                  , SqlDbType.Int).Value = this.Id;
          _cmd.Parameters.Add("@pJackpotId"           , SqlDbType.Int).Value = this.JackpotId;
          _cmd.Parameters.Add("@pJackpotMondayTo"     , SqlDbType.Int).Value = this.MondayTo;
          _cmd.Parameters.Add("@pJackpotMondayFrom"   , SqlDbType.Int).Value = this.MondayFrom;
          _cmd.Parameters.Add("@pJackpotTuesdayTo"    , SqlDbType.Int).Value = this.TuesdayTo;
          _cmd.Parameters.Add("@pJackpotTuesdayFrom"  , SqlDbType.Int).Value = this.TuesdayFrom;
          _cmd.Parameters.Add("@pJackpotWednesdayTo"  , SqlDbType.Int).Value = this.WednesdayTo;
          _cmd.Parameters.Add("@pJackpotWednesdayFrom", SqlDbType.Int).Value = this.WednesdayFrom;
          _cmd.Parameters.Add("@pJackpotThursdayTo"   , SqlDbType.Int).Value = this.ThursdayTo;
          _cmd.Parameters.Add("@pJackpotThursdayFrom" , SqlDbType.Int).Value = this.ThursdayFrom;
          _cmd.Parameters.Add("@pJackpotFridayTo"     , SqlDbType.Int).Value = this.FridayTo;
          _cmd.Parameters.Add("@pJackpotFridayFrom"   , SqlDbType.Int).Value = this.FridayFrom;
          _cmd.Parameters.Add("@pJackpotSaturdayTo"   , SqlDbType.Int).Value = this.SaturdayTo;
          _cmd.Parameters.Add("@pJackpotSaturdayFrom" , SqlDbType.Int).Value = this.SaturdayFrom;
          _cmd.Parameters.Add("@pJackpotSundayTo"     , SqlDbType.Int).Value = this.SundayTo;
          _cmd.Parameters.Add("@pJackpotSundayFrom"   , SqlDbType.Int).Value = this.SundayFrom;

          // Last update
          this.LastUpdate = this.LastModification;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardDays.DB_Update -> JackpotContributionId: {0}", this.Id));

            return false;
          }
          
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardDays.DB_Update -> JackpotAwardDaysId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Update

    /// <summary>
    /// Clone JackpotAwardDays
    /// </summary>
    /// <returns></returns>
    internal JackpotAwardDays Clone()
    {
      return new JackpotAwardDays()
      {
        Id            = this.Id,
        JackpotId     = this.JackpotId,
        MondayFrom    = this.MondayFrom,
        MondayTo      = this.MondayTo,
        TuesdayFrom   = this.TuesdayFrom,
        TuesdayTo     = this.TuesdayTo,
        WednesdayFrom = this.WednesdayFrom,
        WednesdayTo   = this.WednesdayTo,
        ThursdayFrom  = this.ThursdayFrom,
        ThursdayTo    = this.ThursdayTo,
        FridayFrom    = this.FridayFrom,
        FridayTo      = this.FridayTo,
        SaturdayFrom  = this.SaturdayFrom,
        SaturdayTo    = this.SaturdayTo,
        SundayTo      = this.SundayTo,
        SundayFrom    = this.SundayFrom,
        LastUpdate    = this.LastUpdate 
      };
    } // Clone

    #endregion 

  } // JackpotAwardDays
}
