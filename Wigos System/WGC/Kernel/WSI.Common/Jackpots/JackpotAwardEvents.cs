﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardEvents.cs
// 
//   DESCRIPTION: Jackpots award events
// 
//        AUTHOR: Mark Stansfield
// 
// CREATION DATE: 28-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAY-2017 MS     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{

  public class JackpotAwardEvents
  {

    #region " Const "

    private const Int32 m_max_retries = 2;
    private const Int32 m_correction_minutes = 10;
    private const String m_format_db_date_time = "yyyy/MM/dd HH:mm:ss";

    #endregion " Const "

    #region " Properties "

    public Jackpot Jackpot { get; set; }
    public Int32 NumberHappyHourBefore { get; set; }
    public Int32 NumberHappyHourAfter { get; set; }
    public Int32 IntervalHappyHourBefore { get; set; }
    public Int32 IntervalHappyHourAfter { get; set; }
    public Jackpot.AwardFilterAmountDistribution DistributionBefore { get; set; }
    public Jackpot.AwardFilterAmountDistribution DistributionAfter { get; set; }
    public Int32 HappyHourType { get; set; }
    public Decimal Amount
    {
      get
      {
        return m_Amount;
      }
      set
      {
        m_Amount = Math.Round(value, 8, MidpointRounding.AwayFromZero);
      }
    }
    public Decimal HHAmount
    {
      get
      {
        return m_HH_Amount;
      }
      set
      {
        m_HH_Amount = Math.Round(value, 8, MidpointRounding.AwayFromZero);
      }
    }
    public Decimal SharedAmount
    {
      get
      {
        return m_shared_amount;
      }
      set
      {
        m_shared_amount = Math.Round(value, 8, MidpointRounding.AwayFromZero);
      }
    }
    public static Boolean GP_DrawAllEGMAfterCheck
    {
      get
      {
        return GeneralParam.GetBoolean("Jackpots", "Award.DrawToAllEGMAfterRetry", true);
      }
    }
    public static Int32 GP_Award_AwaitingTime
    {
      get
      {
        return GeneralParam.GetInt32("Jackpots", "Award.AwaitingTime", 180);
      }
    }

    #endregion " Properties "

    #region " Members "

    private Int64 m_jad_award_id;
    private Int64 m_jad_award_id_hhb;
    private Int64 m_jad_award_id_hha;
    private Int64 m_jad_award_main;
    private Int64 m_jad_award_notification;
    private Int64 m_jad_award_shared;

    private Jackpot.JackpotType m_jackpot_type;
    private DateTime m_award_start_time;
    private DateTime m_notification_time;
    private DateTime m_award_main_start_time;

    private Decimal m_Amount;
    private Decimal m_HH_Amount;
    private Decimal m_shared_amount;

    #endregion " Members "

    #region " Enums "

    public enum JackpotStatusFlag
    {
      None = 0,                      // 000000
      Pending = 1,                   // 000001
      CantBeAwarded = 2,             // 000010
      Awarded = 4,                   // 000100 
      AwardedWithoutConditions = 8,  // 001000
      Cancelled = 16,                // 010000
      Done = 32                      // 100000
    }

    #endregion " Enums "

    #region " Constructors "

    /// <summary>
    /// Create new object JackpotAwardEvents
    /// </summary>
    public JackpotAwardEvents()
    {
      this.Amount = 0;
    } // JackpotAwardEvents()

    /// <summary>
    /// Create new object JackpotAwardEvents
    /// </summary>
    /// <param name="SharedID"></param>
    /// <param name="AmountShared"></param>
    public JackpotAwardEvents(Int64 SharedID, Decimal AmountShared)
    {
      this.Amount = 0;
      this.m_shared_amount = AmountShared;
      this.m_jad_award_shared = SharedID;
    } // JackpotAwardEvents()

    #endregion " Constructors "

    #region " Public methods "

    /// <summary>
    /// Award jackpots
    /// </summary>
    /// <returns></returns>
    public static Boolean Award()
    {
      Jackpot _jackpot;
      Decimal _occupancy;
      DataRow _award_current;
      DataTable _dt_AccountFlags;
      List<Jackpot> _all_jackpots;
      DataTable _dt_jackpot_awards;
      DataRow[] _dt_jackpot_awards_currents;
      List<InfoPlaySession> _list_info_play_sessions;
      
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (!GetJackpotsAwardDetailsDatatable(out _dt_jackpot_awards, _db_trx.SqlTransaction))
          {
            return false;
          }

          if (_dt_jackpot_awards.Rows.Count == 0)
          {
            return true;
          }

          _dt_jackpot_awards_currents = _dt_jackpot_awards.Select(String.Format(" JAD_DATE_AWARD < '{0}'", WGDB.Now.ToString(m_format_db_date_time)));

          if (_dt_jackpot_awards_currents.Length == 0)
          {
            return true;
          }

          if (!JackpotBusinessLogic.GetAllListForCheck(Jackpot.JackpotFilterType.All, _db_trx.SqlTransaction, out _list_info_play_sessions, out _all_jackpots, out _occupancy, out _dt_AccountFlags))
          {
            return false;
          }

          if (_all_jackpots == null)
          {
            return false;
          }

          if (_all_jackpots.Count == 0)
          {
            return true;
          }

        } // Transaction for GET

        for (int i = 0; i < _dt_jackpot_awards_currents.Length; i++)
        {
          _award_current = _dt_jackpot_awards.Rows[i];

          _jackpot = _all_jackpots.Find(_jack => _jack.Id == (Int32)_award_current["JA_JACKPOT_ID"]);

          if (JackpotBusinessLogic.IsFlagActived((Int32)_award_current["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.Cancelled)
           || JackpotBusinessLogic.IsFlagActived((Int32)_award_current["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.CantBeAwarded)
           || JackpotBusinessLogic.IsFlagActived((Int32)_award_current["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.Done))
          {
            continue;
          }
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (_jackpot == null)
            {
              Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> Jackpot not found: ID >> {0}", (Int32)_award_current["JA_JACKPOT_ID"]));

              continue;
            }

            if ((Int32)_award_current["JAD_STATUS"] == (Int32)JackpotAwardEvents.JackpotStatusFlag.Pending)
            {
              // Pending Jackpot Details to award
              if (!AwardPendingDetails(_award_current, _jackpot, _occupancy, _dt_jackpot_awards, _list_info_play_sessions, _dt_AccountFlags, _db_trx.SqlTransaction))
              {
                continue;
              }

            }
            else
            {
              // Pending Jackpot Details to finish
              if (!BonusEGMToFinish(_award_current, _db_trx.SqlTransaction))
              {
                continue;
              }
            }
          }
        } // FOR 

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.Award");
        Log.Exception(_ex);
      }

      return false;

    }
    /// <summary>
    /// StartAwardProcess
    /// </summary>
    /// <returns></returns>
    public Boolean StartAwardProcess(SqlTransaction SqlTrx)
    {
      try
      {

        //Happy Hour Config
        if (this.Jackpot.AwardPrizeConfig.HappyHour.EventType != Common.Jackpot.Jackpot.AwardHappyHourEvents.Disabled)
        {
          this.GetHappyHourConfig();
        }

        if (!CalculateStartTime())
        {
          return false;
        }

        if (!AwardProcess(SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.StartAwardProcess");
        Log.Exception(_ex);
      }

      return false;

    } // StartAwardProcess

/// <summary>
    /// Start Award Process
/// </summary>
/// <param name="Jackpot"></param>
/// <param name="Winners"></param>
/// <param name="PlaySessionInfo"></param>
/// <param name="SqlTrx"></param>
/// <param name="WinnerDetailsId"></param>
/// <param name="AmountToWinner"></param>
/// <returns></returns>
    public Boolean StartSharedAward(Jackpot Jackpot, List<InfoPlaySession> Winners, InfoPlaySession PlaySessionInfo, SqlTransaction SqlTrx, out Int64 WinnerDetailsId, out Decimal AmountToWinner)
    {
      DataTable _bulk_update;
      Int32 _pos;
      Decimal _amount;
      Int32 _new_status;
      Int64 _details_id;

      AmountToWinner = 0;
      WinnerDetailsId = -1;

      try
      {
        _bulk_update = CreateDtDetails();
        _pos = 0;
        _amount = JackpotBusinessLogic.TruncateAmount(m_shared_amount / Winners.Count, 2);

        if (_amount == 0)
        {
          return true;
        }

        _new_status = JackpotBusinessLogic.UpdateBitmask((Int32)JackpotStatusFlag.Pending, (Int32)JackpotStatusFlag.Awarded, true);

        if (Jackpot.PayoutMode == Jackpot.PayoutModeType.Manual
         || Jackpot.PayoutMode == Jackpot.PayoutModeType.Automatic)
        {
          _new_status = JackpotBusinessLogic.UpdateBitmask(_new_status, (Int32)JackpotStatusFlag.Done, true);

          if (!SaveJackpotAmountOperations(-_amount * Winners.Count, Jackpot.Id, Jackpot.JackpotType.PrizeSharing, JackpotAmountOperations.OperationStatus.Pending, JackpotAmountOperations.OperationType.Award, String.Empty, SqlTrx))
          {
            return false;
          }
        }

        foreach (InfoPlaySession _winner in Winners)
        {
          if (!DB_InsertDetails(m_jad_award_shared, _pos, _new_status, _amount, _winner.TerminalId, _winner.AccountId
                        , 0, WGDB.Now, Common.Jackpot.Jackpot.JackpotType.PrizeSharing, SqlTrx, out _details_id))
          {
            return false;
          }

          if (PlaySessionInfo != _winner)
          {
            if (!JackpotNotificationEGM.NotificationAndHandpay(Jackpot, _winner, _details_id, _amount,-1, SqlTrx))
            {
              return false;
            }
          }
          else
          {
            AmountToWinner = _amount;
            WinnerDetailsId = _details_id;
          }

          _pos++;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.StartSharedAward");
        Log.Exception(_ex);
      }

      return false;

    } // StartSharedAward

    /// <summary>
    /// Award Status to string
    /// </summary>
    /// <param name="Status"></param>
    /// <returns></returns>
    public static String AwardStatusToString(JackpotStatusFlag Status)
    {
      String _status;

      switch (Status)
	    {
		    case JackpotStatusFlag.None:
          _status = String.Empty;
         break;
        case JackpotStatusFlag.Pending:
         _status =  Resource.String("JACKPOT_AWARD_STATUS_PENDING"); 
         break;
        case JackpotStatusFlag.CantBeAwarded:
         _status = Resource.String("JACKPOT_AWARD_STATUS_CANT");
         break;
        case JackpotStatusFlag.Awarded:
         _status = Resource.String("JACKPOT_AWARD_STATUS_AWARDED");
         break;
        case JackpotStatusFlag.AwardedWithoutConditions:
         _status = Resource.String("JACKPOT_AWARD_STATUS_WITHOUT_CONDITIONS");
         break;
        case JackpotStatusFlag.Cancelled:
         _status = Resource.String("JACKPOT_AWARD_STATUS_CANCEL");
         break;
        case JackpotStatusFlag.Done:
         _status = Resource.String("JACKPOT_AWARD_STATUS_DONE");
         break;
        default:
         _status = Status.ToString();
         Log.Warning(String.Format("AwardStatusToString: Status not Defined {0}", _status));
         break;
	    }

      return _status;
    } // AwardStatusToString

    #endregion " Public methods "

    #region " Private methods "

    /// <summary>
    /// Add amount in operation table for substract to type box
    /// </summary>
    /// <param name="Amount"></param>
    /// <param name="JackpotID"></param>
    /// <param name="JackpotType"></param>
    /// <param name="Status"></param>
    /// <param name="OperationType"></param>
    /// <param name="UserName"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean SaveJackpotAmountOperations(Decimal Amount, Int32 JackpotID, Jackpot.JackpotType JackpotType, JackpotAmountOperations.OperationStatus Status,
                                                       JackpotAmountOperations.OperationType OperationType, String UserName, SqlTransaction SqlTrx)
    {

      JackpotAmountOperations _operation;

      try
      {

        if (Amount == 0)
        {
          return true;
        }

        _operation = new JackpotAmountOperations();

        _operation.Amount = Amount;
        _operation.JackpotId = JackpotID;
        _operation.JackpotType = JackpotType;
        _operation.Status = Status;
        _operation.Type = OperationType;
        _operation.UserName = UserName;

        if (!_operation.Save(SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.SaveJackpotAmountOperations");
        Log.Exception(_ex);
      }

      return false;

    } // SaveJackpotAmountOperations

    /// <summary>
    /// Get new amount to contrubution
    /// </summary>
    /// <returns></returns>
    private static Boolean GetInfoNewAmounts(Int32 JackpotId, Int64 RelatedMainId, SqlTransaction SqlTrx, out DataTable DtNewAmounts)
    {

      try
      {
        DtNewAmounts = new DataTable();

        using (SqlCommand _cmd = new SqlCommand(DB_GetJackpotsSharedMaster_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pMainRelated", SqlDbType.BigInt).Value = RelatedMainId;
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;
          _cmd.Parameters.Add("@pTypeHidden", SqlDbType.Int).Value = Jackpot.JackpotType.Hidden;
          _cmd.Parameters.Add("@pTypeShared", SqlDbType.Int).Value = Jackpot.JackpotType.PrizeSharing;
          _cmd.Parameters.Add("@pTypeMain", SqlDbType.Int).Value = Jackpot.JackpotType.Main;

          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _da.Fill(DtNewAmounts);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.GetInfoNewAmounts");
        Log.Exception(_ex);
      }

      DtNewAmounts = null;

      return false;

    } // GetSharedInfo

    /// <summary>
    /// Cancel award
    /// </summary>
    /// <param name="_dt_jackpot_awards"></param>
    /// <param name="_award_current"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean CancelAward(ref DataTable _dt_jackpot_awards, DataRow _award_current, SqlTransaction SqlTrx)
    {
      try
      {

        if (!UpdateCancelAward(ref _dt_jackpot_awards, _award_current, JackpotStatusFlag.CantBeAwarded, SqlTrx))
        {
          Log.Error(String.Format("JackpotAwardEvents.ThreadAward >> _list_info_play_sessions = 0 --> CancelAward |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)_award_current["JA_JACKPOT_ID"], _award_current["JAD_ID"]));
        }

        if (JackpotBusinessLogic.IsFlagActived((Int32)_award_current["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.CantBeAwarded))
        {
          SqlTrx.Commit();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.ThreadAward");
        Log.Exception(_ex);
      }

      return false;

    } // CancelAward

    /// <summary>
    /// DB_Jackpots_AwardInsert
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_Jackpots_AwardInsert(SqlTransaction SqlTrx, ref Boolean ContinueAward)
    {
      SqlParameter _op;
      StringBuilder _sb;
      DateTime _time;

      if (!ContinueAward && this.Amount > 0)
      {
        ContinueAward = true;
      }

      if ((this.Amount <= 0) && (this.m_jackpot_type != Jackpot.JackpotType.Notification))
      {
        // do not add entry with 0 amount
        return true;
      }

      _time = this.m_award_start_time;

      if (this.Jackpot.Type == Jackpot.AreaType.LocalMistery)
      {
        _time = WGDB.Now;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   JACKPOTS_AWARD             ");
        _sb.AppendLine("             ( JA_JACKPOT_ID              ");
        _sb.AppendLine("             , JA_TYPE                    ");
        _sb.AppendLine("             , JA_AMOUNT                  ");
        _sb.AppendLine("             , JA_RELATED_ID              ");
        _sb.AppendLine("             , JA_CREATION              ) ");
        _sb.AppendLine("      VALUES                              ");
        _sb.AppendLine("             ( @pJackpotId                ");
        _sb.AppendLine("             , @pJackpotType              ");
        _sb.AppendLine("             , @pJackpotAmount            ");
        _sb.AppendLine("             , @pRelatedId                ");
        _sb.AppendLine("             , @pCreationDate           ) ");

        _sb.AppendLine(" SET     @pId = SCOPE_IDENTITY()          ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = this.Jackpot.Id;
          _cmd.Parameters.Add("@pJackpotType", SqlDbType.Int).Value = this.m_jackpot_type;
          _cmd.Parameters.Add("@pJackpotAmount", SqlDbType.Decimal).Value = this.Amount;
          _cmd.Parameters.Add("@pRelatedId", SqlDbType.BigInt).Value = this.m_jad_award_main;
          _cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = _time;

          //Output parameter
          _op = new SqlParameter("@pId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JACKPOTS_AWARD.DB_Jackpots_AwardInsert -> JackpotId: {0}", this.Jackpot.Id));

            return false;
          }

          this.m_jad_award_id = (Int32)_op.Value;

          switch (this.m_jackpot_type)
          {
            case Jackpot.JackpotType.Main:
              this.m_jad_award_main = this.m_jad_award_id;
              break;
            case Jackpot.JackpotType.HappyHourBefore:
              this.m_jad_award_id_hhb = this.m_jad_award_id;
              break;
            case Jackpot.JackpotType.HappyHourAfter:
              this.m_jad_award_id_hha = this.m_jad_award_id;
              break;
            case Jackpot.JackpotType.Notification:
              this.m_jad_award_notification = this.m_jad_award_id;
              break;

          } //switch

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardFilters.DB_Insert -> JackpotId: {0}", this.Jackpot.Id));
        Log.Exception(_ex);
      }

      return false;

    } //DB_Jackpots_AwardInsert

    /// <summary>
    /// BuildRow
    /// </summary>
    /// <param name="BulkTable"></param>
    /// <param name="JackpotDetailsId"></param>
    /// <param name="Position"></param>
    /// <param name="Type"></param>
    /// <param name="Status"></param>
    /// <param name="Amount"></param>
    /// <param name="CreationDate"></param>
    /// <returns></returns>
    private Boolean BuildRow(ref DataTable BulkTable, Int64 JackpotDetailsId, Int32 Position, Int32 StatusFlag, Decimal Amount, Int32 TerminalId,
                             Int64 AccountId, Int32 Retries, DateTime CreationDate, Jackpot.JackpotType Type)
    {
      DataRow _workrow;

      try
      {

        Amount = JackpotBusinessLogic.TruncateAmount(Amount, 2);

        if (Amount == 0 && Type != Common.Jackpot.Jackpot.JackpotType.Notification)
        {
          return true;
        }

        _workrow = BulkTable.NewRow();

        _workrow["JAD_AWARD_ID"] = JackpotDetailsId;
        _workrow["JAD_POSITION"] = Position;
        _workrow["JAD_STATUS"] = StatusFlag;
        _workrow["JAD_AMOUNT"] = Amount;
        if (TerminalId == -1)
        {
          _workrow["JAD_TERMINAL_ID"] = DBNull.Value;
        }
        else
        {
          _workrow["JAD_TERMINAL_ID"] = TerminalId;
        }
        if (AccountId == -1)
        {
          _workrow["JAD_ACCOUNT_ID"] = DBNull.Value;
        }
        else
        {
          _workrow["JAD_ACCOUNT_ID"] = AccountId;
        }
        _workrow["JAD_RETRIES"] = Retries;
        _workrow["JAD_DATE_AWARD"] = CreationDate;

        BulkTable.Rows.Add(_workrow);

        return true;

      }
      catch (Exception)
      {

      } // BuildRow

      return false;

    } // BuildRow

    /// <summary>
    /// CreateDtMaster
    /// </summary>
    /// <returns></returns>
    private DataTable CreateDtDetails()
    {

      DataTable _temp_table;

      _temp_table = new DataTable();

      _temp_table.Columns.Add("JAD_AWARD_ID", Type.GetType("System.Int32"));
      _temp_table.Columns.Add("JAD_POSITION", Type.GetType("System.Int32"));
      _temp_table.Columns.Add("JAD_STATUS", Type.GetType("System.Int32"));
      _temp_table.Columns.Add("JAD_AMOUNT", Type.GetType("System.Decimal"));
      _temp_table.Columns.Add("JAD_TERMINAL_ID", Type.GetType("System.Int32"));
      _temp_table.Columns.Add("JAD_ACCOUNT_ID", Type.GetType("System.Int32"));
      _temp_table.Columns.Add("JAD_RETRIES", Type.GetType("System.Int32"));
      _temp_table.Columns.Add("JAD_DATE_AWARD", Type.GetType("System.DateTime"));

      return _temp_table;

    } // CreateDtDetails

    /// <summary>
    /// Create masters
    /// </summary>
    /// <param name="Sqltrx"></param>
    /// <returns></returns>
    private Boolean CreateMaster(SqlTransaction Sqltrx, out Boolean ContinueAward)
    {

      ContinueAward = false;

      try
      {
        //Insert JACKPOTS_AWARD Main
        this.m_jad_award_main = 0;
        this.m_jackpot_type = Jackpot.JackpotType.Main;
        this.Amount = Jackpot.ContributionMeters.MainAmount;
        if (!DB_Jackpots_AwardInsert(Sqltrx, ref ContinueAward))
        {
          return false;
        }

        //Insert JACKPOTS_AWARD HH before
        if (IsBefore())
        {
          this.Amount = this.HHAmount;
          this.m_jackpot_type = Jackpot.JackpotType.HappyHourBefore;

          if (this.Amount > 0)
          {
            ContinueAward = true;
          }

          if (!DB_Jackpots_AwardInsert(Sqltrx, ref ContinueAward))
          {
            return false;
          }

        }

        //Insert JACKPOTS_AWARD HH after
        if (IsAfter())
        {
          this.Amount = this.HHAmount;
          this.m_jackpot_type = Jackpot.JackpotType.HappyHourAfter;
          if (!DB_Jackpots_AwardInsert(Sqltrx, ref ContinueAward))
          {
            return false;
          }
        }

        //Insert JACKPOTS_AWARD Prize Sharing
        this.m_jackpot_type = Jackpot.JackpotType.PrizeSharing;
        this.Amount = Jackpot.ContributionMeters.SharedAmount;
        if (!DB_Jackpots_AwardInsert(Sqltrx, ref ContinueAward))
        {
          return false;
        }

        //Insert JACKPOTS_AWARD Hidden
        this.m_jackpot_type = Jackpot.JackpotType.Hidden;
        this.Amount = Jackpot.ContributionMeters.HiddenAmount;
        if (!DB_Jackpots_AwardInsert(Sqltrx, ref ContinueAward))
        {
          return false;
        }

        if (this.Jackpot.AdvNotificationEnabled && m_jad_award_main > 0)
        {
          //Insert JACKPOTS_AWARD Notification
          this.m_jackpot_type = Jackpot.JackpotType.Notification;
          this.Amount = 0.00m;

          if (!DB_Jackpots_AwardInsert(Sqltrx, ref ContinueAward))
          {
            return false;
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.CreateMaster");
        Log.Exception(_ex);
      }

      return false;

    } // CreateMaster

    /// <summary>
    /// Create happy hours
    /// </summary>
    /// <param name="Nums"></param>
    /// <param name="Happy"></param>
    /// <param name="PositionStart"></param>
    /// <param name="_bulk_update"></param>
    /// <param name="Remainder"></param>
    /// <param name="TotalRemainder"></param>
    /// <returns></returns>
    private Boolean CreateHappyHour(Int32 Nums, Jackpot.JackpotType Happy, ref Int32 PositionStart, ref DataTable _bulk_update, ref Decimal Remainder, ref Decimal TotalRemainder)
    {

      List<Decimal> _hh_amounts;
      Jackpot.AwardFilterAmountDistribution _distribution;
      Int32 _interval;
      Jackpot.JackpotType _jackpot_type;
      Int64 _jad_award_id_hh;

      try
      {

        _distribution = (Happy == Jackpot.JackpotType.HappyHourAfter ? (Jackpot.AwardFilterAmountDistribution)this.DistributionAfter : (Jackpot.AwardFilterAmountDistribution)this.DistributionBefore);
        _interval = (Happy == Jackpot.JackpotType.HappyHourAfter ? IntervalHappyHourAfter : IntervalHappyHourBefore);
        _jackpot_type = Happy;
        _jad_award_id_hh = (Happy == Jackpot.JackpotType.HappyHourAfter ? m_jad_award_id_hha : m_jad_award_id_hhb);

        _hh_amounts = CalculateHappyHours(this.Amount, Nums, _distribution, out Remainder);
        TotalRemainder += Remainder;

        if ((this.m_award_main_start_time == this.m_award_start_time) && (Happy == Jackpot.JackpotType.HappyHourAfter))
        {
          this.m_award_start_time = this.m_award_start_time.AddSeconds(_interval);
        }

        if ((Happy == Jackpot.JackpotType.HappyHourAfter) && (this.Jackpot.Type == Common.Jackpot.Jackpot.AreaType.LocalFixed))
        {
          this.m_award_start_time = this.m_award_main_start_time.AddSeconds(_interval);
        }

        foreach (Decimal _amount in _hh_amounts)
        {
          if (!BuildRow(ref _bulk_update, _jad_award_id_hh, PositionStart, JackpotBusinessLogic.UpdateBitmask((Int32)JackpotStatusFlag.None, (Int32)JackpotStatusFlag.Pending, true),
                        _amount, -1, -1, 0, this.m_award_start_time, Common.Jackpot.Jackpot.JackpotType.HappyHour))
          {
            return false;
          }

          this.m_award_start_time = this.m_award_start_time.AddSeconds(_interval);
          PositionStart += 1;
        } // foreach HHAmounts

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.CreateHappyHour");
        Log.Exception(_ex);
      }

      return false;
    } // CreateHappyHour

    /// <summary>
    /// CalculateHappyHours
    /// </summary>
    /// <param name="HappyHourAmount"></param>
    /// <param name="NumberofHappyHours"></param>
    /// <param name="AwardDistribution"></param>
    /// <param name="Remainder"></param>
    /// <returns>List<Decimal> with all Happy Hour Amounts</returns>
    /// <returns>Uses Remainder output parameter</returns>/// 
    private List<Decimal> CalculateHappyHours(Decimal HappyHourAmount, Int32 NumberofHappyHours,
      Jackpot.AwardFilterAmountDistribution AwardDistribution, out Decimal Remainder)
    {
      List<Decimal> _HappyHourList = new List<Decimal>();
      Decimal _Increment = 0.00m;
      Decimal _Total = 0.00m;

      Remainder = 0.00m;

      switch (AwardDistribution)
      {
        case Jackpot.AwardFilterAmountDistribution.Uniform:
          _Increment = Math.Round((HappyHourAmount / NumberofHappyHours), 8, MidpointRounding.AwayFromZero);

          for (int i = 1; i <= NumberofHappyHours - 1; i++)
          {
            _HappyHourList.Add(_Increment);
            _Total += _Increment;
          }

          Remainder = HappyHourAmount - _Total;
          _HappyHourList.Add(Remainder);
          break;

        case Jackpot.AwardFilterAmountDistribution.LowToHigh:
          _Increment = (HappyHourAmount * 2) / (NumberofHappyHours * (NumberofHappyHours + 1));
          _Increment = Math.Round(_Increment, 8, MidpointRounding.AwayFromZero);

          for (int i = 1; i <= NumberofHappyHours - 1; i++)
          {
            _HappyHourList.Add(Math.Round((_Increment * i), 8, MidpointRounding.AwayFromZero));
            _Total += Math.Round((_Increment * i), 8, MidpointRounding.AwayFromZero);
          }

          Remainder = HappyHourAmount - _Total;
          _HappyHourList.Add(Remainder);
          break;

        case Jackpot.AwardFilterAmountDistribution.HighToLow:
          _Increment = (HappyHourAmount * 2) / (NumberofHappyHours * (NumberofHappyHours + 1));
          _Increment = Math.Round(_Increment, 8, MidpointRounding.AwayFromZero);

          for (int i = 0; i < NumberofHappyHours - 1; i++)
          {
            _HappyHourList.Add(Math.Round((_Increment * (NumberofHappyHours - i)), 8, MidpointRounding.AwayFromZero));
            _Total += Math.Round((_Increment * (NumberofHappyHours - i)), 8, MidpointRounding.AwayFromZero);
          }

          Remainder = HappyHourAmount - _Total;
          _HappyHourList.Add(Remainder);
          break;
      }

      return _HappyHourList;

    } // CalculateHappyHours

    /// <summary>
    /// GetHappyHourConfig
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean GetHappyHourConfig()
    {

      NumberHappyHourBefore = this.Jackpot.AwardPrizeConfig.HappyHour.AwardsBefore;
      NumberHappyHourAfter = this.Jackpot.AwardPrizeConfig.HappyHour.AwardsAfter;
      DistributionBefore = this.Jackpot.AwardPrizeConfig.HappyHour.AmountDistributionBefore;
      DistributionAfter = this.Jackpot.AwardPrizeConfig.HappyHour.AmountDistributionAfter;
      HappyHourType = (Int32)this.Jackpot.AwardPrizeConfig.HappyHour.EventType;

      if (IsBefore())
      {
        IntervalHappyHourBefore = (Int32)(((Decimal)this.Jackpot.AwardPrizeConfig.HappyHour.TimeBefore / (Decimal)NumberHappyHourBefore) * 60);
      }
      else
      {
        IntervalHappyHourBefore = 0;
      }

      if (IsAfter())
      {
        IntervalHappyHourAfter = (Int32)(((Decimal)this.Jackpot.AwardPrizeConfig.HappyHour.TimeAfter / (Decimal)NumberHappyHourAfter) * 60);
      }
      else
      {
        IntervalHappyHourAfter = 0;
      }

      return true;
    } // GetHappyHourConfig

    /// <summary>
    /// Is happy hour before?
    /// </summary>
    /// <returns></returns>
    private Boolean IsBefore()
    {
      if ((this.HappyHourType == (Int32)Jackpot.AwardHappyHourEvents.BeforeJackpot) || (this.HappyHourType == (Int32)Jackpot.AwardHappyHourEvents.BothEvents))
      {
        return true;
      }

      return false;
    } // IsBefore

    /// <summary>
    /// Is happy hour after?
    /// </summary>
    /// <returns></returns>
    private Boolean IsAfter()
    {
      if ((this.HappyHourType == (Int32)Jackpot.AwardHappyHourEvents.AfterJackpot) || (this.HappyHourType == (Int32)Jackpot.AwardHappyHourEvents.BothEvents))
      {
        return true;
      }

      return false;
    } // IsAfter

    /// <summary>
    /// Award process
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean AwardProcess(SqlTransaction SqlTrx)
    {
      DataTable _bulk_update;
      Decimal _total_remainder;
      Decimal _remainder;
      Int32 _position;
      Boolean _continue_award;

      try
      {

        _total_remainder = 0.00m;
        _remainder = 0.00m;
        _position = 0;

        //If number of rows before and after then half the amount
        if (this.HappyHourType == (Int32)Jackpot.AwardHappyHourEvents.BothEvents)
        {
          this.HHAmount = Jackpot.ContributionMeters.HappyHourAmount / 2;
        }
        else
        {
          this.HHAmount = Jackpot.ContributionMeters.HappyHourAmount;
        }

        if (!CreateMaster(SqlTrx, out _continue_award))
        {
          return false;
        }

        if (!_continue_award)
        {
          return true;
        }

        // Details
        _bulk_update = CreateDtDetails();

        // Notification Time
        if (this.Jackpot.AdvNotificationEnabled)
        {
          if (Jackpot.Type == Common.Jackpot.Jackpot.AreaType.LocalMistery)
          {
            this.m_award_main_start_time = this.m_award_main_start_time.AddSeconds((NumberHappyHourBefore * IntervalHappyHourBefore) + IntervalHappyHourBefore);
          }

          this.m_notification_time = this.m_award_main_start_time.AddMinutes(-Jackpot.AdvNotificationTime);

          if (!BuildRow(ref _bulk_update, m_jad_award_notification, 0, JackpotBusinessLogic.UpdateBitmask((Int32)JackpotStatusFlag.None, (Int32)JackpotStatusFlag.Done, true),
                        0.00m, -1, -1, 0, this.m_notification_time, Common.Jackpot.Jackpot.JackpotType.Notification))
          {
            return false;
          }
        }

        this.Amount = this.HHAmount;

        if (IsBefore() && this.Amount > 0)
        {
          m_award_start_time = m_award_main_start_time.AddMinutes(-Jackpot.AwardPrizeConfig.HappyHour.TimeBefore);
          if (!CreateHappyHour(this.NumberHappyHourBefore, Jackpot.JackpotType.HappyHourBefore, ref _position, ref _bulk_update, ref _remainder, ref _total_remainder))
          {
            return false;
          }

        } //Happy Hour Before

        // Main
        if (!BuildRow(ref _bulk_update, m_jad_award_main, 0, JackpotBusinessLogic.UpdateBitmask((Int32)JackpotStatusFlag.None, (Int32)JackpotStatusFlag.Pending, true),
                      this.Jackpot.ContributionMeters.MainAmount, -1, -1, 0, this.m_award_main_start_time, Common.Jackpot.Jackpot.JackpotType.Main))
        {
          return false;
        }

        // Happy Hour After
        if (IsAfter() && this.Amount > 0)
        {
          m_award_start_time = m_award_main_start_time;
          if (!CreateHappyHour(this.NumberHappyHourAfter, Jackpot.JackpotType.HappyHourAfter, ref _position, ref _bulk_update, ref _remainder, ref _total_remainder))
          {
            return false;
          }
        } //Happy hour after

        if (!DB_InsertDataTable(_bulk_update, SqlTrx))
        {
          return false;
        }

        if (Jackpot.Type == Common.Jackpot.Jackpot.AreaType.LocalMistery)
        {
          this.Jackpot.AddNumPending(-1, SqlTrx);
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // AwardProcess

    /// <summary>
    /// Calculate start time
    /// </summary>
    /// <returns></returns>
    private Boolean CalculateStartTime()
    {
      DateTime _from;
      DateTime _to;
      Int32 _seconds_to_add;
      Int32 _minutes_to_subtract;
      TimeSpan _timeSpan;
      DateTime _now;

      try
      {
        _now = WGDB.Now;
        _minutes_to_subtract = 0;

        _seconds_to_add = 0;

        if (Jackpot.Type == Common.Jackpot.Jackpot.AreaType.LocalFixed)
        {
          if (IsBefore())
          {
            _minutes_to_subtract = this.Jackpot.AwardPrizeConfig.HappyHour.TimeBefore;
          }

          if (this.Jackpot.AdvNotificationEnabled && this.Jackpot.AdvNotificationTime > 0)
          {
            _minutes_to_subtract = Math.Max(_minutes_to_subtract, this.Jackpot.AdvNotificationTime);
          }

          if (Jackpot.TodayMinutesTo != Jackpot.TodayMinutesFrom)
          {
            _from = _now.Date.AddMinutes(Jackpot.TodayMinutesFrom);

            if (_from.Ticks < _now.Ticks)
            {
              _from = _now;
            }

            _to = _now.Date.AddMinutes(Jackpot.TodayMinutesTo - m_correction_minutes);

            if (_to.Ticks < _from.Ticks)
            {
              _to = _now.Date.AddMinutes(Jackpot.TodayMinutesTo);
            }
          }
          else
          {
            _from = _now.AddMinutes(_minutes_to_subtract);
            _to = _now.Date.AddDays(1);

            if (_from > _to)
            {
              _from = _now;
            }

          }

          _timeSpan = _to - _from;

          if((UInt64)_timeSpan.TotalSeconds > 0)
          {
            _seconds_to_add = (Int32)CERTIFIED_RNG.GetRandomNumber((UInt64)_timeSpan.TotalSeconds);
          }

         

          //Main Jackpot Time
          m_award_main_start_time = _from.AddSeconds(_seconds_to_add);

          m_award_start_time = m_award_main_start_time.AddMinutes(-_minutes_to_subtract);

        }
        else
        {
          m_award_start_time = _now;
          m_award_main_start_time = m_award_start_time;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.CalculateStartTime");
        Log.Exception(_ex);
      }

      return false;

    } // CalculateStartTime

    /// <summary>
    /// Calculate the time for await award
    /// </summary>
    /// <param name="Minutes"></param>
    /// <param name="DateCurrentAwardTicks"></param>
    /// <param name="DateNextAwardTicks"></param>
    /// <returns></returns>
    private static Boolean AwaitingTimeAward(out Int32 Minutes, Int64 DateCurrentAwardTicks, Int64 DateNextAwardTicks)
    {

      Int32 _interval;

      try
      {
        _interval = 0;

        if (DateNextAwardTicks > 0)
        {
          _interval = (Int32)TimeSpan.FromTicks(DateNextAwardTicks - DateNextAwardTicks).TotalSeconds;
        }

        Minutes = Math.Max(60, Math.Min(GP_Award_AwaitingTime, _interval));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.AwaitingTimeAward");
        Log.Exception(_ex);
      }

      Minutes = 0;

      return false;

    } // AwaitingTimeAward

    /// <summary>
    /// Grant award
    /// </summary>
    /// <param name="AwardCurrent"></param>
    /// <param name="Jackpot"></param>
    /// <param name="_list_info_play_sessions"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean GiveAward(DataRow AwardCurrent, Jackpot Jackpot, List<InfoPlaySession> _list_info_play_sessions, JackpotStatusFlag Status, SqlTransaction SqlTrx)
    {

      Int32 _new_status;
      Decimal _amount_main;
      DataTable DtNewAmounts;
      Decimal _amount_hidden;
      Decimal _amount_shared;
      Int64 _parent_id_dummy;
      Int64 _parent_id_shared;
      Int64 _winner_details_id;
      Decimal _amount_to_winner;
      List<InfoPlaySession> _play_session_awarded;

      try
      {

        _amount_shared = 0;
        _amount_to_winner = 0;
        _parent_id_shared = -1;
        _winner_details_id = -1;

        if (!JackpotBusinessLogic.GetWinners(new List<InfoPlaySession>(_list_info_play_sessions), 1, out _play_session_awarded))
        {
          return false;
        }

        if (_play_session_awarded.Count == 0)
        {
          return false;
        }

        if ((Jackpot.JackpotType)AwardCurrent["JA_TYPE"] == Jackpot.JackpotType.Main)
        {

          if (!GetInfoNewAmounts(Jackpot.Id, (Int64)AwardCurrent["JA_ID"], SqlTrx, out DtNewAmounts))
          {
            return false;
          }

          if (!GetNewAmountByType(DtNewAmounts, Common.Jackpot.Jackpot.JackpotType.Main, out _parent_id_dummy, out _amount_main))
          {
            return false;
          }

          _amount_main = JackpotBusinessLogic.TruncateAmount(_amount_main, 2);

          if (!GetNewAmountByType(DtNewAmounts, Common.Jackpot.Jackpot.JackpotType.Hidden, out _parent_id_dummy, out _amount_hidden))
          {
            return false;
          }

          _amount_hidden = JackpotBusinessLogic.TruncateAmount(_amount_hidden, 2);

          if (!GetNewAmountByType(DtNewAmounts, Common.Jackpot.Jackpot.JackpotType.PrizeSharing, out _parent_id_shared, out _amount_shared))
          {
            return false;
          }

          if (!UpdateAwardAmount(ref AwardCurrent, _amount_main, SqlTrx))
          {
            return false;
          }

        }

        _new_status = (Int32)Status;
        if (Jackpot.PayoutMode == Jackpot.PayoutModeType.Manual || Jackpot.PayoutMode == Jackpot.PayoutModeType.Automatic)
        {
          _new_status = JackpotBusinessLogic.UpdateBitmask(_new_status, (Int32)JackpotStatusFlag.Done, true);

          if (!SaveJackpotAmountOperations(-(Decimal)AwardCurrent["JAD_AMOUNT"], (Int32)AwardCurrent["JA_JACKPOT_ID"], (Jackpot.JackpotType)AwardCurrent["JA_TYPE"],
                                           JackpotAmountOperations.OperationStatus.Pending, JackpotAmountOperations.OperationType.Award, String.Empty, SqlTrx))
          {
            return false;
          }
        }

        if (!UpdateAwardedDetail(_play_session_awarded[0].TerminalId, _play_session_awarded[0].AccountId, (Int64)AwardCurrent["JAD_ID"], _new_status, SqlTrx))
        {
          return false;
        }

        if ((Jackpot.JackpotType)AwardCurrent["JA_TYPE"] == Jackpot.JackpotType.Main)
        {
          if (Jackpot.AwardPrizeConfig.PrizeSharing.Enabled)
          {
            if (!JackpotBusinessLogic.SharingPrizeGeneration(Jackpot, _play_session_awarded[0], _parent_id_shared, _amount_shared, SqlTrx, out _winner_details_id, out _amount_to_winner))
            {
              return false;
            }
          }
        }

        if (!JackpotNotificationEGM.NotificationAndHandpay(Jackpot, _play_session_awarded[0], (Int64)AwardCurrent["JAD_ID"], (Decimal)AwardCurrent["JAD_AMOUNT"] + _amount_to_winner, _winner_details_id, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.GiveAward");
        Log.Exception(_ex);
      }

      return false;
    } // GiveAward

    /// <summary>
    /// Get amount by type
    /// </summary>
    /// <param name="DtNewAmounts"></param>
    /// <param name="Type"></param>
    /// <param name="ParentID"></param>
    /// <param name="NewAmount"></param>
    /// <returns></returns>
    private static Boolean GetNewAmountByType(DataTable DtNewAmounts, Jackpot.JackpotType Type, out Int64 ParentID, out Decimal NewAmount)
    {

      DataRow[] _dr;

      NewAmount = 0;
      ParentID = -1;
      try
      {
        _dr = DtNewAmounts.Select(String.Format(" JA_TYPE = {0} ", (Int32)Type));

        if (_dr.Length == 0)
        {
          return true;
        }

        NewAmount = (Decimal)_dr[0]["JA_AMOUNT"];
        ParentID = (Int64)_dr[0]["JA_ID"];

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.GetNewAmountByType");
        Log.Exception(_ex);
      }

      return false;
    } // GetNewAmountByType

    /// <summary>
    /// Check conditions jackpot for award
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="Occupancy"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <param name="ListJackpotAwardedToday"></param>
    /// <param name="DtAccountFlags"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean CheckConditionsJackpotForAward(Jackpot Jackpot, Decimal Occupancy, ref List<InfoPlaySession> ListPlaySessionsForCheck,
                                                         List<Int32> ListJackpotAwardedToday, DataTable DtAccountFlags, SqlTransaction SqlTrx)
    {
      try
      {

        if (Jackpot.AwardConfig == null || Jackpot.AwardConfig.Filter == null || Jackpot.AwardConfig.WeekDays == null)
        {
          return false;
        }

        //Filter list anonymous and has promotions
        if (!JackpotCheckConditions.FilterListByConditions(Jackpot, ref ListPlaySessionsForCheck))
        {
          return false;
        }

        //if there aren't conditions, next jackpot
        if (ListPlaySessionsForCheck.Count == 0)
        {
          return false;
        }

        if (Jackpot.Type == Jackpot.AreaType.LocalFixed)
        {
          //Jackpots no awarded today
          if (ListJackpotAwardedToday != null && ListJackpotAwardedToday.Contains(Jackpot.Id))
          {
            ListPlaySessionsForCheck.Clear();

            return false;
          }
        }

        //ByCreationDate
        if (!JackpotCheckConditions.CheckConditionsByCreationDate(Jackpot, ref  ListPlaySessionsForCheck))
        {
          return false;
        }

        //ByGender
        if (!JackpotCheckConditions.CheckConditionsByGender(Jackpot, ref ListPlaySessionsForCheck))
        {
          return false;
        }

        //ByDateOfBirth
        if (!JackpotCheckConditions.CheckConditionsByDateOfBirth(Jackpot, ref ListPlaySessionsForCheck))
        {
          return false;
        }

        //Participating Terminals
        if (!JackpotCheckConditions.CheckConditionsByTerminals(Jackpot, ref ListPlaySessionsForCheck, SqlTrx))
        {
          return false;
        }

        //Occupancy
        if (!JackpotCheckConditions.CheckConditionsByOcupancy(Jackpot, Occupancy))
        {
          ListPlaySessionsForCheck.Clear();

          return false;
        }

        //ByFlags
        if (!JackpotCheckConditions.CheckConditionsByFlags(Jackpot, ref ListPlaySessionsForCheck, DtAccountFlags, SqlTrx))
        {
          return false;
        }

        //Minumum bet
        if (!CheckConditionsByMinBet(Jackpot, ref ListPlaySessionsForCheck))
        {
          return false;
        }

        //Level
        if (!CheckConditionsByLevel(Jackpot, ref ListPlaySessionsForCheck))
        {
          return false;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.CheckConditionsJackpotForAward");
        Log.Exception(_ex);
      }

      return false;
    } // CheckConditionsJackpotForAward

    /// <summary>
    /// Check minimum bet
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <returns></returns>
    private static Boolean CheckConditionsByMinBet(Jackpot Jackpot, ref List<InfoPlaySession> ListPlaySessionsForCheck)
    {
      try
      {
        ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.MinBet >= Jackpot.AwardConfig.Filter.MinimumBet);

        if (ListPlaySessionsForCheck.Count == 0)
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.CheckConditionsByMinBet");
        Log.Exception(_ex);
      }

      return false;
    } // CheckConditionsByMinBet

    /// <summary>
    /// Check conditions by level
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <returns></returns>
    private static Boolean CheckConditionsByLevel(Jackpot Jackpot, ref List<InfoPlaySession> ListPlaySessionsForCheck)
    {
      try
      {

        if (Jackpot.AwardPrizeConfig.CustomerTier.ByLevel)
        {
          if (Jackpot.AwardPrizeConfig.CustomerTier.Anonymous)
          {
            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => (Jackpot.AwardLevel)_condition.AccountHolderLevel == Jackpot.AwardLevel.Anonymous);
          }

          if (Jackpot.AwardPrizeConfig.CustomerTier.Level1)
          {
            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => (Jackpot.AwardLevel)_condition.AccountHolderLevel == Jackpot.AwardLevel.Level1);
          }

          if (Jackpot.AwardPrizeConfig.CustomerTier.Level2)
          {
            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => (Jackpot.AwardLevel)_condition.AccountHolderLevel == Jackpot.AwardLevel.Level2);
          }

          if (Jackpot.AwardPrizeConfig.CustomerTier.Level3)
          {
            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => (Jackpot.AwardLevel)_condition.AccountHolderLevel == Jackpot.AwardLevel.Level3);
          }

          if (Jackpot.AwardPrizeConfig.CustomerTier.Level4)
          {
            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => (Jackpot.AwardLevel)_condition.AccountHolderLevel == Jackpot.AwardLevel.Level4);
          }

          if (Jackpot.AwardPrizeConfig.CustomerTier.Vip == Jackpot.AwardCustomerVipType.NoVip)
          {
            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => !_condition.IsVip);
          }

          if (Jackpot.AwardPrizeConfig.CustomerTier.Vip == Jackpot.AwardCustomerVipType.Vip)
          {
            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.IsVip);
          }
        }

        if (ListPlaySessionsForCheck.Count == 0)
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.CheckConditionsByLevel");
        Log.Exception(_ex);
      }

      return false;
    } // CheckConditionsByLevel

    #endregion " Private methods "

    #region " DataBase methods "

    /// <summary>
    /// DB_InsertDataTable
    /// </summary>
    /// <param name="DtDetails"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_InsertDataTable(DataTable DtDetails, SqlTransaction SqlTrx)
    {

      Int32 _num_updated = 0;
      try
      {

        using (SqlCommand _sql_cmd = new SqlCommand(InsertDetailsSelect(false), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pJackpotAwardId", SqlDbType.Int).SourceColumn = "JAD_AWARD_ID";
          _sql_cmd.Parameters.Add("@pPosition", SqlDbType.Int).SourceColumn = "JAD_POSITION";
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "JAD_STATUS";
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).SourceColumn = "JAD_AMOUNT";
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "JAD_TERMINAL_ID";
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "JAD_ACCOUNT_ID";
          _sql_cmd.Parameters.Add("@pRetries", SqlDbType.Int).SourceColumn = "JAD_RETRIES";
          _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).SourceColumn = "JAD_DATE_AWARD";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;

            _num_updated = _sql_da.Update(DtDetails);

            if (_num_updated != DtDetails.Rows.Count)
            {
              Log.Message(String.Format("JackpotAwardEvents.DB_InsertDetails() - An error occurred updating into JACKPOT_AWARD_DETAIL: Rows affected <> {0}."
                , DtDetails.Rows.Count));

              return false;
            }
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_InsertDataTable

    /// <summary>
    /// Insert details
    /// </summary>
    /// <param name="JackpotDetailsId"></param>
    /// <param name="Position"></param>
    /// <param name="StatusFlag"></param>
    /// <param name="Amount"></param>
    /// <param name="TerminalId"></param>
    /// <param name="AccountId"></param>
    /// <param name="Retries"></param>
    /// <param name="CreationDate"></param>
    /// <param name="Type"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_InsertDetails(Int64 JackpotDetailsId, Int32 Position, Int32 StatusFlag, Decimal Amount, Int32 TerminalId,
                             Int64 AccountId, Int32 Retries, DateTime CreationDate, Jackpot.JackpotType Type, SqlTransaction SqlTrx, out Int64 DetailId)
    {
      SqlParameter _param_detail_id;

      DetailId = 0;

      try
      {

        using (SqlCommand _sql_cmd = new SqlCommand(InsertDetailsSelect(true), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pJackpotAwardId", SqlDbType.Int).Value = JackpotDetailsId;
          _sql_cmd.Parameters.Add("@pPosition", SqlDbType.Int).Value = Position;
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = StatusFlag;
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = Amount;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId == -1 ? DBNull.Value : (Object)TerminalId;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId == -1 ? DBNull.Value : (Object)AccountId; ;
          _sql_cmd.Parameters.Add("@pRetries", SqlDbType.Int).Value = Retries;
          _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = CreationDate;
          _param_detail_id = _sql_cmd.Parameters.Add("@pJadId", SqlDbType.BigInt);

          _param_detail_id.Direction = ParameterDirection.Output;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardEvents.DB_InsertDetails -> JackpotDetailsId: {0}", JackpotDetailsId));

            return false;
          }

          if (_param_detail_id.Value == DBNull.Value)
          {
            return false;
          }

          DetailId = (Int64)_param_detail_id.Value;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_InsertDetails


    /// <summary>
    /// Insert Details Select
    /// </summary>
    /// <param name="GetScopeIdentity"></param>
    /// <returns></returns>
    private static String InsertDetailsSelect(Boolean GetScopeIdentity)
    {

      StringBuilder _sb;


      _sb = new StringBuilder();

      _sb.AppendLine(" INSERT INTO   JACKPOTS_AWARD_DETAIL ");
      _sb.AppendLine("             ( JAD_AWARD_ID          ");
      _sb.AppendLine("             , JAD_POSITION          ");
      _sb.AppendLine("             , JAD_STATUS            ");
      _sb.AppendLine("             , JAD_AMOUNT            ");
      _sb.AppendLine("             , JAD_TERMINAL_ID       ");
      _sb.AppendLine("             , JAD_ACCOUNT_ID        ");
      _sb.AppendLine("             , JAD_RETRIES           ");
      _sb.AppendLine("             , JAD_DATE_AWARD        ");
      _sb.AppendLine("             )                       ");
      _sb.AppendLine("      VALUES                         ");
      _sb.AppendLine("             ( @pJackpotAwardId      ");
      _sb.AppendLine("             , @pPosition            ");
      _sb.AppendLine("             , @pStatus              ");
      _sb.AppendLine("             , @pAmount              ");
      _sb.AppendLine("             , @pTerminalId          ");
      _sb.AppendLine("             , @pAccountId           ");
      _sb.AppendLine("             , @pRetries             ");
      _sb.AppendLine("             , @pDate                ");
      _sb.AppendLine("             )                       ");
      if (GetScopeIdentity)
      {
        _sb.AppendLine(" SET @pJadId   = SCOPE_IDENTITY() ");
      }


      return _sb.ToString();

    } // InsertDetailsSelect 

    /// <summary>
    /// GetJackpotsAwardDetailsDatatable
    /// </summary>
    /// <param name="DtJackpotAward"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean GetJackpotsAwardDetailsDatatable(out DataTable DtJackpotAward, SqlTransaction SqlTrx)
    {

      try
      {
        if (!DB_GetJackpotsAwardDetailsDatatable(out DtJackpotAward, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.GetJackpotsAwardDetailsDatatable");
        Log.Exception(_ex);
      }

      DtJackpotAward = null;

      return false;

    } // GetJackpotsAwardDetailsDatatable

    /// <summary>
    /// UpdateAwardedDetail
    /// </summary>
    /// <param name="TerminalID"></param>
    /// <param name="AccountID"></param>
    /// <param name="AwardDetailID"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean UpdateAwardedDetail(Int32 TerminalID, Int64 AccountID, Int64 AwardDetailID, Int32 Status, SqlTransaction SqlTrx)
    {

      try
      {
        if (!DB_UpdateAwardedDetail(TerminalID, AccountID, AwardDetailID, Status, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.UpdateAwardedDetail");
        Log.Exception(_ex);
      }

      return false;

    } // UpdateAwardedDetail

    /// <summary>
    /// Update detail no awarded
    /// </summary>
    /// <param name="AwardDetailID"></param>
    /// <param name="AwaitTime"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean UpdateDetailNoAwarded(Int64 AwardDetailID, DateTime AwaitTime, SqlTransaction SqlTrx)
    {

      try
      {
        if (!DB_UpdateDetailNoAwarded(AwardDetailID, AwaitTime, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.UpdateDetailNoAwarded");
        Log.Exception(_ex);
      }

      return false;

    } // UpdateDetailNoAwarded

    /// <summary>
    /// Update award is cancelled
    /// </summary>
    /// <param name="DtAllAward"></param>
    /// <param name="Award"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean UpdateCancelAward(ref DataTable DtAllAward, DataRow Award, JackpotStatusFlag Status, SqlTransaction SqlTrx)
    {
      List<DataRow> _lst_dr_jackpot_awards_cancel;

      try
      {

        _lst_dr_jackpot_awards_cancel = new List<DataRow>();
        _lst_dr_jackpot_awards_cancel.Add(Award);

        if (!UpdateCancelAwards(ref DtAllAward, _lst_dr_jackpot_awards_cancel.ToArray(), Status, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.UpdateCancelAward");
        Log.Exception(_ex);
      }

      return false;

    } // UpdateCancelAward

    /// <summary>
    /// Update award for type is cancelled
    /// </summary>
    /// <param name="DtAllAward"></param>
    /// <param name="RelatedID"></param>
    /// <param name="Type"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean UpdateCancelAwardsType(ref DataTable DtAllAward, Int64 RelatedID, Jackpot.JackpotType Type, JackpotStatusFlag Status, SqlTransaction SqlTrx)
    {
      DataRow[] _lst_dr_jackpot_awards_cancel;

      try
      {

        if (Type != Jackpot.JackpotType.All)
        {
          _lst_dr_jackpot_awards_cancel = DtAllAward.Select(String.Format("JA_RELATED_ID = {0} AND JA_TYPE = {1}", RelatedID, (Int32)Type));
        }
        else
        {
          _lst_dr_jackpot_awards_cancel = DtAllAward.Select(String.Format("JA_RELATED_ID = {0} OR JA_ID = {0}", RelatedID));
        }

        if (!UpdateCancelAwards(ref DtAllAward, _lst_dr_jackpot_awards_cancel, Status, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.UpdateCancelAwardsType");
        Log.Exception(_ex);
      }

      return false;

    } // UpdateCancelAwardsType

    /// <summary>
    /// Update cancel awards
    /// </summary>
    /// <param name="DtAllAward"></param>
    /// <param name="LstDrJackpotAwardsCancel"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean UpdateCancelAwards(ref DataTable DtAllAward, DataRow[] LstDrJackpotAwardsCancel, JackpotStatusFlag Status, SqlTransaction SqlTrx)
    {

      DataTable _dt_award_cancel;
      DataRow[] _dr_award;

      try
      {

        _dt_award_cancel = DtAllAward.Copy();

        _dt_award_cancel.Rows.Clear();

        foreach (DataRow _award_cancel in LstDrJackpotAwardsCancel)
        {
          _dt_award_cancel.ImportRow(_award_cancel);
        }

        if (!DB_UpdateCancelAwards(_dt_award_cancel, Status, SqlTrx))
        {
          return false;
        }

        foreach (DataRow _award_cancelled in LstDrJackpotAwardsCancel)
        {
          _dr_award = DtAllAward.Select(String.Format("JAD_ID = {0}", _award_cancelled["JAD_ID"]));

          if (_dr_award.Length > 0)
          {
            _dr_award[0]["JAD_STATUS"] = Status;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.UpdateCancelAwards");
        Log.Exception(_ex);
      }

      return false;

    } // UpdateCancelAwards

    /// <summary>
    /// Update award amount
    /// </summary>
    /// <param name="DrAward"></param>
    /// <param name="NewAmountMain"></param>
    /// <param name="NewAmountHidden"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean UpdateAwardAmount(ref DataRow DrAward, Decimal NewAmountMain, SqlTransaction SqlTrx)
    {

      try
      {
        if (!DB_UpdateAwardAmount(ref DrAward, NewAmountMain, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.UpdateAwardAmount");
        Log.Exception(_ex);
      }

      return false;

    } // UpdateAwardAmount

    /// <summary>
    /// DB_GetJackpotsAwardDetailsDatatable
    /// </summary>
    /// <param name="DtJackpotAward"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_GetJackpotsAwardDetailsDatatable(out DataTable DtJackpotAward, SqlTransaction SqlTrx)
    {
      Int32 _done_cancelled;
      try
      {
        DtJackpotAward = new DataTable();
        _done_cancelled = JackpotBusinessLogic.UpdateBitmask((Int32)JackpotStatusFlag.Done, (Int32)JackpotStatusFlag.Cancelled, true);
        _done_cancelled = JackpotBusinessLogic.UpdateBitmask(_done_cancelled, (Int32)JackpotStatusFlag.CantBeAwarded, true);

        using (SqlCommand _cmd = new SqlCommand(DB_GetJackpotsAwardDetails_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = JackpotStatusFlag.Pending;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = _done_cancelled;

          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _da.Fill(DtJackpotAward);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.DB_GetJackpotsAwardDetailsDatatable");
        Log.Exception(_ex);
      }

      DtJackpotAward = null;

      return false;

    } // DB_GetJackpotsAwardDetailsDatatable

    /// <summary>
    /// DB_UpdateAwardedDetail
    /// </summary>
    /// <param name="TerminalID"></param>
    /// <param name="AccountID"></param>
    /// <param name="AwardDetailID"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_UpdateAwardedDetail(Int32 TerminalID, Int64 AccountID, Int64 AwardDetailID, Int32 Status, SqlTransaction SqlTrx)
    {

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_UpdateAwardedDetailAwarded_Update(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminal", SqlDbType.Int).Value = TerminalID;
          _cmd.Parameters.Add("@pAccount", SqlDbType.BigInt).Value = AccountID;
          _cmd.Parameters.Add("@pAwardDetail", SqlDbType.BigInt).Value = AwardDetailID;
          _cmd.Parameters.Add("@pStatus", SqlDbType.BigInt).Value = JackpotBusinessLogic.UpdateBitmask((Int32)JackpotStatusFlag.Pending, (Int32)Status, true);
          _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = WGDB.Now;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardEvents.DB_UpdateAwardedDetail -> AwardDetailID: {0}", AwardDetailID));

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.DB_UpdateAwardedDetail");
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateAwardedDetail

    /// <summary>
    /// DB_UpdateDetailNoAwarded
    /// </summary>
    /// <param name="AwardDetailID"></param>
    /// <param name="AwaitTime"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_UpdateDetailNoAwarded(Int64 AwardDetailID, DateTime AwaitTime, SqlTransaction SqlTrx)
    {

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_UpdateDetailNoAwarded_Update(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAwardDetail", SqlDbType.BigInt).Value = AwardDetailID;
          _cmd.Parameters.Add("@pAwardTime", SqlDbType.DateTime).Value = AwaitTime;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardEvents.DB_UpdateDetailNoAwarded -> AwardDetailID: {0}", AwardDetailID));

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.DB_UpdateDetailNoAwarded");
        Log.Exception(_ex);
      }

      return false;

    } // DB_UpdateWaitingTimeAward

    /// <summary>
    /// DB_UpdateCancelAwards
    /// </summary>
    /// <param name="DtAwardCancel"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_UpdateCancelAwards(DataTable DtAwardCancel, JackpotStatusFlag Status, SqlTransaction SqlTrx)
    {

      Int32 _num_updated;

      try
      {

        _num_updated = 0;

        foreach (DataRow _award_cancel in DtAwardCancel.Rows)
        {
          _award_cancel.SetModified();
        }

        using (SqlCommand _sql_cmd = new SqlCommand(DB_UpdateStatus_Update(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pID", SqlDbType.BigInt).SourceColumn = "JAD_ID";
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = JackpotBusinessLogic.UpdateBitmask((Int32)JackpotStatusFlag.Pending, (Int32)Status, true);

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.UpdateCommand = _sql_cmd;

            _num_updated = _sql_da.Update(DtAwardCancel);

            if (_num_updated != DtAwardCancel.Rows.Count)
            {
              Log.Message(String.Format("JackpotAwardEvents.DB_UpdateCancelAwards() - An error occurred updating into JACKPOT_AWARD_DETAIL: Rows affected <> {0}.", DtAwardCancel.Rows.Count));

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.DB_UpdateCancelAwards");
        Log.Exception(_ex);
      }

      return false;

    } // DB_UpdatePaid

    /// <summary>
    /// DB_UpdatePaid
    /// </summary>
    /// <param name="JackpotDetailsId"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_UpdateStatus(Int64 JackpotDetailsId, Int32 Status, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(DB_UpdateStatus_Update(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pID", SqlDbType.BigInt).Value = JackpotDetailsId;
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.UpdateCommand = _sql_cmd;

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Error(String.Format("JackpotAwardEvents.DB_UpdatePaid -> JAD_ID = {0} | JAD_STATUS: {1}", JackpotDetailsId, Status));

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.DB_UpdateCancelAwards");
        Log.Exception(_ex);
      }

      return false;

    } // DB_UpdatePaid


    /// <summary>
    /// Award pending details
    /// </summary>
    /// <param name="AwardCurrent"></param>
    /// <param name="Jackpot"></param>
    /// <param name="Occupancy"></param>
    /// <param name="DtJackpotAward"></param>
    /// <param name="ListInfoPlaySessions"></param>
    /// <param name="DtAccountFlags"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean AwardPendingDetails(DataRow AwardCurrent, Common.Jackpot.Jackpot Jackpot, Decimal Occupancy, DataTable DtJackpotAward,
                                               List<InfoPlaySession> ListInfoPlaySessions, DataTable DtAccountFlags, SqlTransaction SqlTrx)
    {

      DataRow _award_next;
      Int32 _seconds_await_award;
      Int64 _date_next_award_ticks;
      Int64 _date_current_award_ticks;
      List<InfoPlaySession> _list_info_play_sessions_filtred;

      _award_next = null;
      _date_next_award_ticks = 0;
      _date_current_award_ticks = 0;

      if (!Jackpot.Enabled)
      {
        if (!UpdateCancelAwardsType(ref DtJackpotAward, (Int64)AwardCurrent["JA_RELATED_ID"], Jackpot.JackpotType.All, JackpotStatusFlag.Cancelled, SqlTrx))
        {
          Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> CancelAwards |  Jackpot.ID >> {0} | AwardDetail.Type >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], Jackpot.JackpotType.HappyHourBefore.ToString()));

          return false;
        }

        if (JackpotBusinessLogic.IsFlagActived((Int32)AwardCurrent["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.Cancelled))
        {
          SqlTrx.Commit();

          return false;
        }
      }

      _list_info_play_sessions_filtred = new List<InfoPlaySession>(ListInfoPlaySessions);
      
      if ((Jackpot.JackpotType)AwardCurrent["JA_TYPE"] == Jackpot.JackpotType.HappyHourBefore)
      {
        if (Jackpot.AwardPrizeConfig.HappyHour.EventType == Jackpot.AwardHappyHourEvents.Disabled || Jackpot.AwardPrizeConfig.HappyHour.EventType == Jackpot.AwardHappyHourEvents.AfterJackpot)
        {
          //Cancel HH before not awarded
          if (!UpdateCancelAwardsType(ref DtJackpotAward, (Int64)AwardCurrent["JA_RELATED_ID"], Jackpot.JackpotType.HappyHourBefore, JackpotStatusFlag.Cancelled, SqlTrx))
          {
            Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> CancelAwards |  Jackpot.ID >> {0} | AwardDetail.Type >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], Jackpot.JackpotType.HappyHourBefore.ToString()));

            return false;
          }

          if (JackpotBusinessLogic.IsFlagActived((Int32)AwardCurrent["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.Cancelled))
          {
            SqlTrx.Commit();

            return false;
          }
        }
      }

      if ((Jackpot.JackpotType)AwardCurrent["JA_TYPE"] == Jackpot.JackpotType.HappyHourAfter)
      {
        if (Jackpot.AwardPrizeConfig.HappyHour.EventType == Jackpot.AwardHappyHourEvents.Disabled || Jackpot.AwardPrizeConfig.HappyHour.EventType == Jackpot.AwardHappyHourEvents.BeforeJackpot)
        {
          //Cancel HH after not awarded

          if (!UpdateCancelAwardsType(ref DtJackpotAward, (Int64)AwardCurrent["JA_RELATED_ID"], Jackpot.JackpotType.HappyHourAfter, JackpotStatusFlag.Cancelled, SqlTrx))
          {
            Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> CancelAwards |  Jackpot.ID >> {0} | AwardDetail.Type >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], Jackpot.JackpotType.HappyHourAfter.ToString()));

            return false;
          }

          if (JackpotBusinessLogic.IsFlagActived((Int32)AwardCurrent["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.Cancelled))
          {
            SqlTrx.Commit();

            return false;
          }
        }
      }

      if (CheckConditionsJackpotForAward(Jackpot, Occupancy, ref _list_info_play_sessions_filtred, null, DtAccountFlags, SqlTrx))
      {
        //Award
        if (!GiveAward(AwardCurrent, Jackpot, _list_info_play_sessions_filtred, JackpotStatusFlag.Awarded, SqlTrx))
        {
          Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> GiveAward |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));

          return false;
        }
      }
      else
      {
        //No award
        if ((Int32)AwardCurrent["JAD_RETRIES"] < m_max_retries - 1)
        {
          if (DtJackpotAward.Rows.IndexOf(AwardCurrent) + 1 < DtJackpotAward.Rows.Count)
          {
            _award_next = DtJackpotAward.Rows[DtJackpotAward.Rows.IndexOf(AwardCurrent) + 1];
          }

          _date_current_award_ticks = ((DateTime)AwardCurrent["JAD_DATE_AWARD"]).Ticks;

          if (_award_next != null)
          {
            _date_next_award_ticks = ((DateTime)_award_next["JAD_DATE_AWARD"]).Ticks;
          }

          if (!AwaitingTimeAward(out _seconds_await_award, _date_current_award_ticks, _date_next_award_ticks))
          {
            Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> AwaitingTimeAward |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));

            return false;
          }

          if (!UpdateDetailNoAwarded((Int64)AwardCurrent["JAD_ID"], ((DateTime)AwardCurrent["JAD_DATE_AWARD"]).AddSeconds(_seconds_await_award), SqlTrx))
          {
            Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> UpdateDetailNoAwarded |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));

            return false;
          }
        }

        if ((Int32)AwardCurrent["JAD_RETRIES"] == m_max_retries - 1)
        {
          if (GP_DrawAllEGMAfterCheck)
          {
            //Max retries + no play sessions
            if (ListInfoPlaySessions.Count == 0)
            {
              CancelAward(ref DtJackpotAward, AwardCurrent, SqlTrx);

              return false;
            }

            if (!JackpotBusinessLogic.FilterPlaySessionsByTerminalsAwardJackpot(Jackpot, ref ListInfoPlaySessions, SqlTrx))
            {
              Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> FilterPlaySessionsByTerminalsAwardJackpot |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));

              return false;
            }

            if (ListInfoPlaySessions.Count == 0)
            {
              CancelAward(ref DtJackpotAward, AwardCurrent, SqlTrx);

              return false;
            }

            if (!GiveAward(AwardCurrent, Jackpot, ListInfoPlaySessions, JackpotStatusFlag.AwardedWithoutConditions, SqlTrx))
            {
              Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> GiveAward |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));

              return false;
            }
          }
          else
          {
            if (!UpdateCancelAward(ref DtJackpotAward, AwardCurrent, JackpotStatusFlag.CantBeAwarded, SqlTrx))
            {
              Log.Error(String.Format("JackpotAwardEvents.ThreadAward >> Max reties --> UpdateCancelAwards |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));

              return false;
            }

            if (JackpotBusinessLogic.IsFlagActived((Int32)AwardCurrent["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.CantBeAwarded))
            {
              SqlTrx.Commit();

              return false;
            }
          }
        }
      }

      SqlTrx.Commit();

      return true;
    } // AwardPendingDetails

    /// <summary>
    /// Finish EGM Jackpot Details
    /// </summary>
    /// <param name="AwardCurrent"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean BonusEGMToFinish(DataRow AwardCurrent, SqlTransaction SqlTrx)
    {
      Int32 _jackpot_detail_status;
      Int64 _bonus_id;
      BonusTransferStatus _bonus_status;
      MultiPromos.AccountBalance _transferred;
      Int32 _target_terminal_id;
      Int64 _target_account_id;


      _jackpot_detail_status = -1;
      
      if (AwardCurrent["JAD_BONUS_ID"] == DBNull.Value)
      {
        Log.Warning(String.Format("JackpotAwardEvents.ThreadAward --> BONUS_ID is NULL |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));
        return false;
      }

      _bonus_id = (Int64)AwardCurrent["JAD_BONUS_ID"];

      if (!Bonusing.JackpotDetailsReadBonusStatus((Int64)AwardCurrent["JAD_ID"], SqlTrx,
                                     out _bonus_status, out _transferred, out _target_terminal_id, out _target_account_id))
      {
        Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> Bonusing.JackpotDetailsReadBonusStatus |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));

        return false;
      }

      if (_bonus_status == BonusTransferStatus.Confirmed)
      {
        _jackpot_detail_status = JackpotBusinessLogic.UpdateBitmask((Int32)AwardCurrent["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.Done, true);


        if (!SaveJackpotAmountOperations(-(Decimal)AwardCurrent["JAD_AMOUNT"], (Int32)AwardCurrent["JA_JACKPOT_ID"], (Jackpot.JackpotType)AwardCurrent["JA_TYPE"],
                                                  JackpotAmountOperations.OperationStatus.Pending, JackpotAmountOperations.OperationType.Award, String.Empty, SqlTrx))
        {
          Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> SaveJackpotAmountOperations |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));

          return false;
        }
      }

      if (_bonus_status == BonusTransferStatus.CanceledTimeout)
      {
        _jackpot_detail_status = JackpotBusinessLogic.UpdateBitmask((Int32)AwardCurrent["JAD_STATUS"], (Int32)JackpotAwardEvents.JackpotStatusFlag.Cancelled, true);
      }

      if (_jackpot_detail_status > 0)
      {
        if (!DB_UpdateStatus((Int64)AwardCurrent["JAD_ID"], _jackpot_detail_status, SqlTrx))
        {
          Log.Error(String.Format("JackpotAwardEvents.ThreadAward --> DB_UpdateStatus |  Jackpot.ID >> {0} | AwardDetail.ID >> {1} ", (Int32)AwardCurrent["JA_JACKPOT_ID"], AwardCurrent["JAD_ID"]));

          return false;
        }
      }

      SqlTrx.Commit();
      return true;
    } // BonusEGMToFinish

    /// <summary>
    /// DB_UpdateAwardAmount
    /// </summary>
    /// <param name="DrAward"></param>
    /// <param name="NewAmountMain"></param>
    /// <param name="NewAmountHidden"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_UpdateAwardAmount(ref DataRow DrAward, Decimal NewAmountMain, SqlTransaction SqlTrx)
    {

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(DB_UpdateAwardDetailAmount_Update(), SqlTrx.Connection, SqlTrx))
        {

          _sql_cmd.CommandText = DB_UpdateAwardDetailAmount_Update();

          _sql_cmd.Parameters.Add("@pDetailAmount", SqlDbType.Money).Value = JackpotBusinessLogic.TruncateAmount(NewAmountMain, 2);
          _sql_cmd.Parameters.Add("@pDetailID", SqlDbType.BigInt).Value = (Int64)DrAward["JAD_ID"];

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardEvents.DB_UpdateAwardAmount -> JAD_ID = {0} | JA_JACKPOT_ID: {1} | JA_TYPE = {2} ", (Int64)DrAward["JAD_ID"], (Int32)DrAward["JA_JACKPOT_ID"], (Int64)DrAward["JA_TYPE"]));

            return false;
          }

          DrAward["JA_AMOUNT"] = NewAmountMain;
          DrAward["JAD_AMOUNT"] = JackpotBusinessLogic.TruncateAmount(NewAmountMain, 2);

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.DB_UpdateAwardAmount");
        Log.Exception(_ex);
      }

      return false;

    } // DB_UpdateAwardAmount

    #region " Queries "

    /// <summary>
    /// DB_GetJackpotsAwardDetails_Select
    /// </summary>
    /// <returns></returns>
    private static String DB_GetJackpotsAwardDetails_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   JA_ID                                          ");
      _sb.AppendLine("            , JA_JACKPOT_ID                                  ");
      _sb.AppendLine("            , JA_TYPE                                        ");
      _sb.AppendLine("            , JA_AMOUNT                                      ");
      _sb.AppendLine("            , JA_RELATED_ID                                  ");
      _sb.AppendLine("            , JAD_ID                                         ");
      _sb.AppendLine("            , JAD_AWARD_ID                                   ");
      _sb.AppendLine("            , JAD_POSITION                                   ");
      _sb.AppendLine("            , JAD_STATUS                                     ");
      _sb.AppendLine("            , JAD_AMOUNT                                     ");
      _sb.AppendLine("            , JAD_TERMINAL_ID                                ");
      _sb.AppendLine("            , JAD_ACCOUNT_ID                                 ");
      _sb.AppendLine("            , JAD_RETRIES                                    ");
      _sb.AppendLine("            , JAD_DATE_AWARD                                 ");
      _sb.AppendLine("            , JAD_BONUS_ID                                   ");
      _sb.AppendLine("       FROM   JACKPOTS_AWARD                                 ");
      _sb.AppendLine(" INNER JOIN   JACKPOTS_AWARD_DETAIL  ON JA_ID = JAD_AWARD_ID ");
      _sb.AppendLine("      WHERE   ( JAD_STATUS = @pStatusPending                 ");
      _sb.AppendLine("         OR     JAD_STATUS & @pStatus      =  0      )       ");
      _sb.AppendLine("        AND   JAD_AMOUNT > 0                                 ");
      _sb.AppendLine("   ORDER BY   JA_JACKPOT_ID, JAD_DATE_AWARD                  ");

      return _sb.ToString();

    } // DB_GetJackpotsAwardDetails_Select

    /// <summary>
    /// DB_GetJackpotsSharedMaster_Select
    /// </summary>
    /// <returns></returns>
    private static String DB_GetJackpotsSharedMaster_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" SELECT   JCM_AMOUNT AS JA_AMOUNT                  ");
      _sb.AppendLine("        , 0 AS JA_TYPE                             ");
      _sb.AppendLine("        , -1 as JA_ID                              ");
      _sb.AppendLine("   FROM   JACKPOTS_CONTRIBUTION_METERS             ");
      _sb.AppendLine("  WHERE   JCM_JACKPOT_ID = @pJackpotId             ");
      _sb.AppendLine("    AND   JCM_TYPE = @pTypeMain                    ");
      _sb.AppendLine("  UNION                                            ");
      _sb.AppendLine(" SELECT   JA_AMOUNT                                ");
      _sb.AppendLine("        , JA_TYPE                                  ");
      _sb.AppendLine("        , JA_ID                                    ");
      _sb.AppendLine("   FROM   JACKPOTS_AWARD                           ");
      _sb.AppendLine("  WHERE   JA_RELATED_ID = @pMainRelated            ");
      _sb.AppendLine("    AND   JA_TYPE IN (@pTypeShared, @pTypeHidden ) ");

      return _sb.ToString();
    } // DB_GetJackpotsSharedMaster_Select

    /// <summary>
    /// DB_UpdateAwardedDetail_Update
    /// </summary>
    /// <returns></returns>
    private static String DB_UpdateAwardedDetailAwarded_Update()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   JACKPOTS_AWARD_DETAIL        ");
      _sb.AppendLine("    SET   JAD_TERMINAL_ID = @pTerminal ");
      _sb.AppendLine("        , JAD_ACCOUNT_ID = @pAccount   ");
      _sb.AppendLine("        , JAD_STATUS = @pStatus        ");
      _sb.AppendLine("        , JAD_DATE_AWARD = @pDate      ");
      _sb.AppendLine("  WHERE   JAD_ID = @pAwardDetail       ");

      return _sb.ToString();

    } // DB_UpdateAwardedDetailAwarded_Update

    /// <summary>
    /// DB_UpdateDetailNoAwarded_Update
    /// </summary>
    /// <returns></returns>
    private static String DB_UpdateDetailNoAwarded_Update()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   JACKPOTS_AWARD_DETAIL         ");
      _sb.AppendLine("    SET   JAD_DATE_AWARD = @pAwardTime  ");
      _sb.AppendLine("        , JAD_RETRIES = JAD_RETRIES + 1 ");
      _sb.AppendLine("  WHERE   JAD_ID = @pAwardDetail        ");

      return _sb.ToString();

    } // DB_UpdateDetailNoAwarded_Update

    /// <summary>
    /// DB_UpdateStatus_Update
    /// </summary>
    /// <returns></returns>
    private static String DB_UpdateStatus_Update()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   JACKPOTS_AWARD_DETAIL ");
      _sb.AppendLine("    SET   JAD_STATUS = @pStatus ");
      _sb.AppendLine("  WHERE   JAD_ID = @pID         ");

      return _sb.ToString();

    } // DB_UpdateStatus_Update

    /// <summary>
    /// DB_GetAmountAward_Select
    /// </summary>
    /// <returns></returns>
    private static String DB_GetAmountAward_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" SELECT   JCM_AMOUNT                   ");
      _sb.AppendLine("   FROM   JACKPOTS_CONTRIBUTION_METERS ");
      _sb.AppendLine("  WHERE   JCM_JACKPOT_ID = @pJackpotID ");
      _sb.AppendLine("    AND   JCM_TYPE = @pType   				 ");

      return _sb.ToString();

    } // DB_GetAmountAward_select

    /// <summary>
    /// DB_UpdateAwardAmount_Update
    /// </summary>
    /// <returns></returns>
    private static String DB_UpdateAwardAmount_Update()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   JACKPOTS_AWARD 			 ");
      _sb.AppendLine("    SET   JA_AMOUNT = @pAmount ");
      _sb.AppendLine("  WHERE   JA_ID = @pID      	 ");

      return _sb.ToString();

    } // DB_UpdateAwardAmount_Update

    /// <summary>
    /// DB_UpdateAwardDetailAmount_Update
    /// </summary>
    /// <returns></returns>
    private static String DB_UpdateAwardDetailAmount_Update()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   JACKPOTS_AWARD_DETAIL       ");
      _sb.AppendLine("    SET   JAD_AMOUNT = @pDetailAmount ");
      _sb.AppendLine("  WHERE   JAD_ID = @pDetailID 		    ");

      return _sb.ToString();

    } // DB_UpdateAwardDetailAmount_Update

    #endregion " Queries "

    #endregion " DataBase method "

  }

}
