﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardsList.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot Awards list
// 
//        AUTHOR: Rubén Lama Ordóñez
// 
// CREATION DATE: 01-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUN-2017 RLO    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardItemList : JackpotBase
  {

    #region " Properties "

    public Int32 JackpotId { get; set; }
    public Int64 AwardId { get; set; }
    public List<JackpotAwardItem> Awards { get; set; }

    #endregion " Properties "

     #region " Public Methods "
     
    /// <summary>
    /// Default Constructor
    /// </summary>
    public JackpotAwardItemList()
    {
      this.JackpotId  = DEFAULT_ID_VALUE;
      this.Awards     = new List<JackpotAwardItem>();
    } // JackpotViewerTerminalList
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="JackpotId"></param>    
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Read(Int32 JackpotAwardId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.Read(JackpotAwardId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int64 JackpotAwardId, SqlTransaction SqlTrx)
    {
      List<JackpotAwardItem> _awards;

      if (!new JackpotAwardItem().GetJackpotAwardsByJackpotAwardId(JackpotAwardId, out _awards, SqlTrx))
      {
        return false;
      }

      if (_awards.Count > 0)
      {
        this.JackpotId = _awards[0].JackpotId;
      }
      
      this.Awards     = _awards;

      return true;
    } // Read

    /// <summary>
    /// Clone JackpotAwardPrizeConfig
    /// </summary>
    /// <returns></returns>
    public JackpotAwardItemList Clone()
    {
      return new JackpotAwardItemList()
      {
        JackpotId = this.JackpotId,
        AwardId   = this.AwardId,
        Awards    = this.Awards.Clone()
      };
    } // Clone

    #endregion " Public Methods "

    #region " Private Methods "


    #endregion " Private Methods "

  } // JackpotContributionList
}
