﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotCheckConditions.cs
// 
//   DESCRIPTION: Jackpots check conditions
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 01-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUN-2017 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotCheckConditions
  {

    #region " Public methods "

    /// <summary>
    /// Check conditions of jackpots
    /// </summary>
    /// <returns></returns>
    public static Boolean CheckConditions()
    {

      Decimal _occupancy;
      DataTable DtAccountFlags;
      List<Jackpot> _all_jackpots;
      List<InfoAward> _list_jackpots_awarded_today;
      List<InfoPlaySession> _list_info_play_sessions;
      List<InfoPlaySession> _list_info_play_sessions_jackpot;

      try
      {
        _list_info_play_sessions_jackpot = new List<InfoPlaySession>();

        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (!JackpotBusinessLogic.GetAllListForCheck(Jackpot.JackpotFilterType.EnabledAndPending, _db_trx.SqlTransaction, out _list_info_play_sessions, out _all_jackpots, out _occupancy, out DtAccountFlags))
          {
            return false;
          }

          if (_all_jackpots == null)
          {
            return false;
          }

          if (_all_jackpots.Count == 0)
          {
            return true;
          }

          if (!JackpotCheckConditions.GetJackpotAwardDetailsOnDay(out _list_jackpots_awarded_today, _db_trx.SqlTransaction))
          {
            return false;
          }

          foreach (Jackpot _jackpot in _all_jackpots)
          {

            if (_jackpot.Type == Jackpot.AreaType.LocalMistery && _list_info_play_sessions.Count == 0)
            {
              continue;
            }

            _list_info_play_sessions_jackpot = new List<InfoPlaySession>(_list_info_play_sessions);

            if (!JackpotCheckConditions.CheckConditionsJackpot(_jackpot, _occupancy, ref _list_info_play_sessions_jackpot, _list_jackpots_awarded_today, DtAccountFlags, _db_trx.SqlTransaction))
            {
              continue;
            }

            // Event generation
            if (!StartAwardProcess(_jackpot, _db_trx.SqlTransaction))
            {
              return false;
            }

          }

          _db_trx.Commit();

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.CheckConditions");
        Log.Exception(_ex);
      }

      return false;

    } // CheckConditions

    /// <summary>
    /// Filter list conditions of jackpot
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <returns></returns>
    public static Boolean FilterListByConditions(Jackpot Jackpot, ref List<InfoPlaySession> ListPlaySessionsForCheck)
    {
      try
      {
        //Exlclude accounts with promotion
        if (Jackpot.AwardConfig.Filter.ExcludeAccountsWithPromo)
        {
          ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => !_condition.HasPromotion);
        }

        //Exclude anonymous accounts
        if (Jackpot.AwardConfig.Filter.ExcludeAnonymousAccount)
        {
          ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => !_condition.IsAnonymousAccount);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.FilterListByConditions");
        Log.Exception(_ex);
      }

      return false;
    } // FilterListByConditions

    /// <summary>
    /// Check conditions by creation date
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <returns></returns>
    public static Boolean CheckConditionsByCreationDate(Jackpot Jackpot, ref List<InfoPlaySession> ListPlaySessionsForCheck)
    {
      try
      {
        //By creation date - enable
        if (Jackpot.AwardConfig.Filter.ByCreationDate != Jackpot.AwardFilterCreationDate.Disabled)
        {
          //Creation date
          if (Jackpot.AwardConfig.Filter.ByCreationDate == Jackpot.AwardFilterCreationDate.Creation)
          {
            if (Jackpot.AwardConfig.Filter.JustDayOfCreation)
            {

              ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.AccountCreated.Date.Ticks == WGDB.Now.Date.Ticks);

              //Applies on creation day only
              if (ListPlaySessionsForCheck.Count == 0)
              {
                return false;
              }
            }
            else
            {
              ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.AccountCreated.AddDays(Jackpot.AwardConfig.Filter.DayOfCreationPlus).Ticks > WGDB.Now.Ticks);

              //Applies on creation day + X
              if (ListPlaySessionsForCheck.Count == 0)
              {
                return false;
              }
            }
          }

          //Account anniversary
          if (Jackpot.AwardConfig.Filter.ByCreationDate == Jackpot.AwardFilterCreationDate.CreationBirthdate)
          {
            if (Jackpot.AwardConfig.Filter.AnyDayOfMonth)
            {
              if (Jackpot.AwardConfig.Filter.CreationBetweenThis == Jackpot.DEFAULT_INT_VALUE && Jackpot.AwardConfig.Filter.CreationBetweenThat == Jackpot.DEFAULT_INT_VALUE)
              {

                ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.AccountCreated.Month == WGDB.Now.Month);

                //Applies on any day of the anniversary month
                if (ListPlaySessionsForCheck.Count == 0)
                {
                  return false;
                }
              }
              else
              {

                ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.AccountCreated.Month == WGDB.Now.Month &&
                                                                                                      (WGDB.Now.Year - _condition.AccountCreated.Year) >= Jackpot.AwardConfig.Filter.CreationBetweenThis &&
                                                                                                      (WGDB.Now.Year - _condition.AccountCreated.Year) <= Jackpot.AwardConfig.Filter.CreationBetweenThat);

                //Only applies between X and Y anniversaries
                if (ListPlaySessionsForCheck.Count == 0)
                {
                  return false;
                }



              }
            }

            if (Jackpot.AwardConfig.Filter.DayAndMonth)
            {
              if (Jackpot.AwardConfig.Filter.CreationBetweenThis == Jackpot.DEFAULT_INT_VALUE && Jackpot.AwardConfig.Filter.CreationBetweenThat == Jackpot.DEFAULT_INT_VALUE)
              {

                ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.AccountCreated.Day == WGDB.Now.Day &&
                                                                                                      _condition.AccountCreated.Month == WGDB.Now.Month &&
                                                                                                      _condition.AccountCreated.Year < WGDB.Now.Year);

                //Applies only on the day of the anniversary
                if (ListPlaySessionsForCheck.Count == 0)
                {
                  return false;
                }
              }
              else
              {

                ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.AccountCreated.Day == WGDB.Now.Day &&
                                                                                                      _condition.AccountCreated.Month == WGDB.Now.Month &&
                                                                                                      _condition.AccountCreated.Year < WGDB.Now.Year &&
                                                                                                      (WGDB.Now.Year - _condition.AccountCreated.Year) >= Jackpot.AwardConfig.Filter.CreationBetweenThis &&
                                                                                                      (WGDB.Now.Year - _condition.AccountCreated.Year) <= Jackpot.AwardConfig.Filter.CreationBetweenThat);

                //Only applies between X and Y anniversaries
                if (ListPlaySessionsForCheck.Count == 0)
                {
                  return false;
                }
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.CheckConditionsByCreationDate");
        Log.Exception(_ex);
      }

      return false;
    } // CheckConditionsByCreationDate

    /// <summary>
    /// Check conditions by gender
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <returns></returns>
    public static Boolean CheckConditionsByGender(Jackpot Jackpot, ref List<InfoPlaySession> ListPlaySessionsForCheck)
    {
      try
      {
        if (Jackpot.AwardConfig.Filter.ByGender != GENDER.UNKNOWN)
        {

          ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.AccountHolderGender == (Int32)Jackpot.AwardConfig.Filter.ByGender);

          if (ListPlaySessionsForCheck.Count == 0)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.CheckConditionsByGender");
        Log.Exception(_ex);
      }

      return false;
    } // CheckConditionsByGender

    /// <summary>
    /// Check conditions by date of birth
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <returns></returns>
    public static Boolean CheckConditionsByDateOfBirth(Jackpot Jackpot, ref List<InfoPlaySession> ListPlaySessionsForCheck)
    {
      try
      {
        //Check by date of birth enabled
        if (Jackpot.AwardConfig.Filter.ByDateOfBirth != Jackpot.AwardFilterDateOfBirth.Disabled)
        {
          if (Jackpot.AwardConfig.Filter.ByDateOfBirth == Jackpot.AwardFilterDateOfBirth.Birthday)
          {
            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.AccountHolderBirth.Month == WGDB.Now.Month);

            //Applies any day within birthday's month
            if (ListPlaySessionsForCheck.Count == 0)
            {
              return false;
            }
          }

          //Applies only on birthday
          if (Jackpot.AwardConfig.Filter.ByDateOfBirth == Jackpot.AwardFilterDateOfBirth.DayAndMonth)
          {
            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _condition.AccountHolderBirth.Day == WGDB.Now.Day && _condition.AccountHolderBirth.Month == WGDB.Now.Month);

            if (ListPlaySessionsForCheck.Count == 0)
            {
              return false;
            }
          }
        }

        //Applies to age range
        if (Jackpot.AwardConfig.Filter.AgeRange)
        {
          if (Jackpot.AwardConfig.Filter.OlderThan > Jackpot.DEFAULT_INT_VALUE && Jackpot.AwardConfig.Filter.AndSmallerThan > Jackpot.DEFAULT_INT_VALUE)
          {

            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => GetAge(WGDB.Now, _condition.AccountHolderBirth) >= Jackpot.AwardConfig.Filter.OlderThan &&
                                                                                                  GetAge(WGDB.Now, _condition.AccountHolderBirth) < Jackpot.AwardConfig.Filter.AndSmallerThan);

            //Olden than X and younger than Y
            if (ListPlaySessionsForCheck.Count == 0)
            {
              return false;
            }
          }

          if (Jackpot.AwardConfig.Filter.SmallerThan > Jackpot.DEFAULT_INT_VALUE && Jackpot.AwardConfig.Filter.OrOlderThan > Jackpot.DEFAULT_INT_VALUE)
          {

            ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => GetAge(WGDB.Now, _condition.AccountHolderBirth) < Jackpot.AwardConfig.Filter.SmallerThan ||
                                                                                        GetAge(WGDB.Now, _condition.AccountHolderBirth) >= Jackpot.AwardConfig.Filter.OrOlderThan);

            //Younger than X or older than Y
            if (ListPlaySessionsForCheck.Count == 0)
            {
              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.CheckConditionsByDateOfBirth");
        Log.Exception(_ex);
      }

      return false;
    } // CheckConditionsByDateOfBirth

    /// <summary>
    /// Check conditions by terminals
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean CheckConditionsByTerminals(Jackpot Jackpot, ref List<InfoPlaySession> ListPlaySessionsForCheck, SqlTransaction SqlTrx)
    {
      try
      {

        if (!JackpotBusinessLogic.FilterPlaySessionsByTerminalsAwardJackpot(Jackpot, ref ListPlaySessionsForCheck, SqlTrx))
        {
          return false;
        }

        if (ListPlaySessionsForCheck.Count > 0)
        {
          return true;
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.CheckConditionsByTerminals");
        Log.Exception(_ex);
      }

      return false;
    } // CheckConditionsByTerminals

    /// <summary>
    /// Check conditions by flags
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <param name="DtAccountFlags"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean CheckConditionsByFlags(Jackpot Jackpot, ref List<InfoPlaySession> ListPlaySessionsForCheck, DataTable DtAccountFlags, SqlTransaction SqlTrx)
    {

      DataRow[] _dr_flags;
      List<Int64> _list_account_remove;

      try
      {
        //If flags is not active, disabled condition
        if (Jackpot.AwardConfig.Filter.Flags.Items.FindAll(_flag => _flag.FlagCount > 0).Count == 0)
        {
          return true;
        }

        _list_account_remove = new List<Int64>();

        foreach (InfoPlaySession _play_session in ListPlaySessionsForCheck)
        {
          foreach (JackpotAwardFilterFlag _flag in Jackpot.AwardConfig.Filter.Flags.Items.FindAll(_flag => _flag.FlagCount > 0))
          {
            _dr_flags = DtAccountFlags.Select(String.Format(" AF_ACCOUNT_ID = {0} AND AF_FLAG_ID = {1} ", _play_session.AccountId, _flag.FlagId));

            if (_dr_flags.Length == 0)
            {
              _list_account_remove.Add(_play_session.AccountId);
              break;
            }

            if ((Int32)_dr_flags[0]["FLAG_COUNT"] < _flag.FlagCount)
            {
              _list_account_remove.Add(_play_session.AccountId);
              break;
            }
          }
        }

        ListPlaySessionsForCheck.RemoveAll(_ps => _list_account_remove.Contains(_ps.AccountId));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.CheckConditionsByFlags");
        Log.Exception(_ex);
      }

      return false;

    } // CheckConditionsByFlags

    /// <summary>
    /// Check conditions by occupancy
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="Occupancy"></param>
    /// <returns></returns>
    public static Boolean CheckConditionsByOcupancy(Jackpot Jackpot, Decimal Occupancy)
    {
      try
      {
        if (Jackpot.AwardConfig.Filter.MinOccupancy >= Occupancy)
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.CheckConditionsByOcupancy");
        Log.Exception(_ex);
      }

      return false;
    } // CheckConditionsByOcupancy

    #endregion " Public methods "

    #region " Private methods "

    /// <summary>
    /// Get jackpots that awarded today
    /// </summary>
    /// <param name="ListJackpotAwardedToday"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean GetJackpotAwardDetailsOnDay(out List<InfoAward> ListJackpotAwardedToday, SqlTransaction SqlTrx)
    {

      try
      {
        if (!DB_GetJackpotAwardDetailsOnDay(out ListJackpotAwardedToday, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.GetJackpotAwardDetailsOnDay");
        Log.Exception(_ex);
      }

      ListJackpotAwardedToday = null;

      return false;

    } // GetJackpotAwardDetailsOnDay

    /// <summary>
    /// Check conditions jackpot
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="Occupancy"></param>
    /// <param name="ListPlaySessionsForCheck"></param>
    /// <param name="ListJackpotAwardedToday"></param>
    /// <param name="DtAccountFlags"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean CheckConditionsJackpot(Jackpot Jackpot, Decimal Occupancy, ref List<InfoPlaySession> ListPlaySessionsForCheck,
                                                 List<InfoAward> ListJackpotAwardedToday, DataTable DtAccountFlags, SqlTransaction SqlTrx)
    {
      List<InfoAward> _awarded_today;

      try
      {

        if (Jackpot.AwardConfig == null || Jackpot.AwardConfig.Filter == null || Jackpot.AwardConfig.WeekDays == null)
        {
          return false;
        }

        if (Jackpot.Type == Jackpot.AreaType.LocalFixed)
        {
          // Not Main or not paid
          // LocalFixed only one main per day.
          _awarded_today = ListJackpotAwardedToday.FindAll(_award => _award.JackpotId == Jackpot.Id
                                                                  && (_award.JackpotType == Jackpot.JackpotType.Main
                                                                  || !(JackpotBusinessLogic.IsFlagActived(_award.Status, (Int32)JackpotAwardEvents.JackpotStatusFlag.Done))));

          //Jackpots no awarded today
          if (_awarded_today.Count > 0)
          {
            ListPlaySessionsForCheck.Clear();

            return false;
          }

        }
        else
        {
          // Mistery not paid
          _awarded_today = ListJackpotAwardedToday.FindAll(_award => _award.JackpotId == Jackpot.Id
                                                                  && !(JackpotBusinessLogic.IsFlagActived(_award.Status, (Int32)JackpotAwardEvents.JackpotStatusFlag.Done)));

          //Jackpots no awarded today or pending Award
          if (_awarded_today.Count > 0)
          {
            ListPlaySessionsForCheck.Clear();

            return false;
          }

          //Filter list anonymous and has promotions
          if (!FilterListByConditions(Jackpot, ref ListPlaySessionsForCheck))
          {
            return false;
          }

          //if there aren't conditions, next jackpot
          if (ListPlaySessionsForCheck.Count == 0)
          {
            return false;
          }

          //ByCreationDate
          if (!CheckConditionsByCreationDate(Jackpot, ref  ListPlaySessionsForCheck))
          {
            return false;
          }

          //ByGender
          if (!CheckConditionsByGender(Jackpot, ref ListPlaySessionsForCheck))
          {
            return false;
          }

          //ByDateOfBirth
          if (!CheckConditionsByDateOfBirth(Jackpot, ref ListPlaySessionsForCheck))
          {
            return false;
          }

          //Participating Terminals
          if (!CheckConditionsByTerminals(Jackpot, ref ListPlaySessionsForCheck, SqlTrx))
          {
            return false;
          }

          //Occupancy
          if (!CheckConditionsByOcupancy(Jackpot, Occupancy))
          {
            ListPlaySessionsForCheck.Clear();

            return false;
          }

          //ByFlags
          if (!CheckConditionsByFlags(Jackpot, ref ListPlaySessionsForCheck, DtAccountFlags, SqlTrx))
          {
            return false;
          }
        }

        //Days
        if (!CheckConditionsByDay(Jackpot))
        {
          ListPlaySessionsForCheck.Clear();

          return false;
        }      

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.CheckConditionsJackpot");
        Log.Exception(_ex);
      }

      return false;

    } // CheckConditionsJackpot

    /// <summary>
    /// Check conditions by day
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <returns></returns>
    private static Boolean CheckConditionsByDay(Jackpot Jackpot)
    {
      DateTime _now;
      Int32 _minutes_from;
      Int32 _minutes_to;
      Int32 _subtract_minutes;

      try
      {
        _now = WGDB.Now;
        _minutes_from = 0;
        _minutes_to = 0;
        _subtract_minutes = 0;

        // Adding Happy Hours before Main: Main should be gived in interval.
        if (Jackpot.Type == Jackpot.AreaType.LocalFixed)
        {
          if ((Jackpot.AwardPrizeConfig.HappyHour.EventType == Common.Jackpot.Jackpot.AwardHappyHourEvents.BeforeJackpot
            || Jackpot.AwardPrizeConfig.HappyHour.EventType == Common.Jackpot.Jackpot.AwardHappyHourEvents.BothEvents)
            && Jackpot.AwardPrizeConfig.HappyHour.TimeBefore > 0)
          {
            _subtract_minutes = Jackpot.AwardPrizeConfig.HappyHour.TimeBefore;
          }

          if (Jackpot.AdvNotificationEnabled && Jackpot.AdvNotificationTime > 0)
          {
            _subtract_minutes = Math.Max(_subtract_minutes, Jackpot.AdvNotificationTime);
          }
        }

        switch (_now.DayOfWeek)
        {
          case DayOfWeek.Monday:
            _minutes_from = Jackpot.AwardConfig.WeekDays.MondayFrom;
            _minutes_to = Jackpot.AwardConfig.WeekDays.MondayTo;

            break;
          case DayOfWeek.Tuesday:
            _minutes_from = Jackpot.AwardConfig.WeekDays.TuesdayFrom;
            _minutes_to = Jackpot.AwardConfig.WeekDays.TuesdayTo;

            break;
          case DayOfWeek.Wednesday:
            _minutes_from = Jackpot.AwardConfig.WeekDays.WednesdayFrom;
            _minutes_to = Jackpot.AwardConfig.WeekDays.WednesdayTo;

            break;
          case DayOfWeek.Thursday:
            _minutes_from = Jackpot.AwardConfig.WeekDays.ThursdayFrom;
            _minutes_to = Jackpot.AwardConfig.WeekDays.ThursdayTo;

            break;
          case DayOfWeek.Friday:
            _minutes_from = Jackpot.AwardConfig.WeekDays.FridayFrom;
            _minutes_to = Jackpot.AwardConfig.WeekDays.FridayTo;

            break;
          case DayOfWeek.Saturday:
            _minutes_from = Jackpot.AwardConfig.WeekDays.SaturdayFrom;
            _minutes_to = Jackpot.AwardConfig.WeekDays.SaturdayTo;

            break;
          case DayOfWeek.Sunday:
            _minutes_from = Jackpot.AwardConfig.WeekDays.SundayFrom;
            _minutes_to = Jackpot.AwardConfig.WeekDays.SundayTo;

            break;
        }

        // When day is unchecked the value is Jackpot.DEFAULT_TIME_VALUE (-1) and Jackpot is not gived that day.
        if (_minutes_from == Jackpot.DEFAULT_TIME_VALUE
         || _minutes_to == Jackpot.DEFAULT_TIME_VALUE)
        {
          return false;
        }

        Jackpot.TodayMinutesFrom = _minutes_from;
        Jackpot.TodayMinutesTo = _minutes_to;

        if (!AwardByDayAndHour(_minutes_from, _minutes_to, _now, _subtract_minutes))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.CheckConditionsByDay");
        Log.Exception(_ex);
      }

      return false;
    } //CheckConditionsByDay

    /// <summary>
    /// Check hour of day from jackpot
    /// </summary>
    /// <param name="From"></param>
    /// <param name="To"></param>
    /// <param name="Date"></param>
    /// <returns></returns>
    private static Boolean AwardByDayAndHour(Int32 From, Int32 To, DateTime Date, Int32 SubstractMinutes)
    {
      Int32 _minutes;

      _minutes = DateToMinutes(Date) + SubstractMinutes;

      _minutes = _minutes > To ? DateToMinutes(Date) : _minutes;

      if (From == To) // 24h
      {
        return true;
      }
      else if (From < To)
      {
        if (From <= _minutes && _minutes < To)   // From <= _time < To
        {
          return true;
        }
      }
      else if (From > To)
      {
        if (From <= _minutes      // _t0 <= _tt < 24h
            || _minutes < To)     // 0h  <= _tt < _t1
        {
          return true;
        }
      }

      return false;
    } // AwardByDayAndHour

    /// <summary>
    /// Convert date to minutes
    /// </summary>
    /// <param name="WorkingDate"></param>
    /// <returns></returns>
    private static Int32 DateToMinutes(DateTime WorkingDate)
    {

      int _hours_in_minutes = 0;
      int _minutes = 0;

      _hours_in_minutes = (WorkingDate.Hour * 60);
      _minutes = WorkingDate.Minute;

      return _hours_in_minutes + _minutes;

    } // DateToMinutes

    /// <summary>
    /// Get age
    /// </summary>
    /// <param name="reference"></param>
    /// <param name="birthday"></param>
    /// <returns></returns>
    private static Int32 GetAge(DateTime reference, DateTime birthday)
    {
      int age = reference.Year - birthday.Year;
      if (reference < birthday.AddYears(age)) age--;

      return age;
    } // GetAge

    /// <summary>
    /// Start process award for jackpot
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <returns></returns>
    private static Boolean StartAwardProcess(Jackpot Jackpot, SqlTransaction SqlTrx)
    {

      JackpotAwardEvents _Award_Events;

      try
      {
        _Award_Events = new JackpotAwardEvents();
        _Award_Events.Jackpot = Jackpot;

        if (!_Award_Events.StartAwardProcess(SqlTrx))
        {
          return false;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.StartAwardProcess");
        Log.Exception(_ex);
      }

      return false;
    } // StartAwardProcess

    #endregion " Private methods "

    #region " DataBase methods "

    /// <summary>
    /// Check if pending jackpots
    /// </summary>
    /// <param name="ListJackpotAwardedToday"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_GetJackpotAwardDetailsOnDay(out List<InfoAward> ListJackpotAwardedToday, SqlTransaction SqlTrx)
    {
      InfoAward _Awarded;
      Int32 _status_result;

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_GetJackpotAwardDetailsOnDay_Select(), SqlTrx.Connection, SqlTrx))
        {
          _status_result = JackpotBusinessLogic.UpdateBitmask((Int32)JackpotAwardEvents.JackpotStatusFlag.CantBeAwarded, (Int32)JackpotAwardEvents.JackpotStatusFlag.Cancelled, true);

          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = _status_result;
          
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            ListJackpotAwardedToday = new List<InfoAward>();

            while (_sql_reader.Read())
            {
              _Awarded = new InfoAward()
              {
                JackpotId = (Int32)_sql_reader["JA_JACKPOT_ID"],
                Status = (Int32)_sql_reader["JAD_STATUS"],
                JackpotType = (Jackpot.JackpotType)_sql_reader["JA_TYPE"]
              };

              ListJackpotAwardedToday.Add(_Awarded);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotCheckConditions.DB_GetJackpotAwardDetailsOnDay");
        Log.Exception(_ex);
      }

      ListJackpotAwardedToday = null;

      return false;
    } // DB_GetJackpotAwardDetailsOnDay

    #region " Queries "

    /// <summary>
    /// Get select jackpots awarded today
    /// </summary>
    /// <returns></returns>
    private static String DB_GetJackpotAwardDetailsOnDay_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   JA_JACKPOT_ID                           ");
      _sb.AppendLine("            , JA_TYPE                                 ");
      _sb.AppendLine("            , JAD_STATUS                              ");
      _sb.AppendLine("       FROM   JACKPOTS_AWARD_DETAIL                   ");
      _sb.AppendLine(" INNER JOIN   JACKPOTS_AWARD ON JA_ID = JAD_AWARD_ID  ");
      _sb.AppendLine("      WHERE   JAD_DATE_AWARD > dbo.TodayOpening(0)    ");
      _sb.AppendLine("        AND   JAD_STATUS & @pStatus = 0               ");

      return _sb.ToString();
    } // DB_GetJackpotAwardDetailsOnDay_Select

    #endregion " Queries "

    #endregion " DataBase methods "

  }
}
