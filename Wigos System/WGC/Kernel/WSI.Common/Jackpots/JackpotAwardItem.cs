﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardItem.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Rubén Lama Ordóñez
// 
// CREATION DATE: 31-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAY-2017 RLO    First release
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardItem
  {
    #region " Constants "

    // Private constants    
    private const Int32 DB_ID               = 0;
    private const Int32 DB_JACKPOT_ID       = 1;
    private const Int32 DB_NAME             = 2;
    private const Int32 DB_TYPE             = 3;
    private const Int32 DB_AMOUNT           = 4;
    private const Int32 DB_POSITION         = 5;
    private const Int32 DB_TERMINAL_ID      = 6;
    private const Int32 DB_ACCOUNT_ID       = 7;
    private const Int32 DB_DATE_AWARD       = 8;
    private const Int32 DB_STATUS           = 9;
    
    #endregion

    #region " Properties "

    public Int64 Id { get; set; }
    public Int32 JackpotId { get; set; }
    public String Name { get; set; }
    public Int32 Type { get; set; }
    public Decimal Amount { get; set; }
    public Int32 Position { get; set; }
    public Int32 TerminalId { get; set; }
    public Int64 AccountId { get; set; }
    public DateTime DateAward { get; set; }
    public Int32 Status { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's 
    /// </summary>
    public JackpotAwardItem()
    {
      this.Id         = Jackpot.DEFAULT_ID_VALUE;
      this.JackpotId  = Jackpot.DEFAULT_ID_VALUE;
      this.Name       = String.Empty;
      this.Type       = Jackpot.DEFAULT_INT_VALUE;
      this.Amount     = Jackpot.DEFAULT_DECIMAL_VALUE;
      this.Position   = Jackpot.DEFAULT_INT_VALUE;
      this.TerminalId = Jackpot.DEFAULT_INT_VALUE;
      this.AccountId  = Jackpot.DEFAULT_INT_VALUE;
      this.DateAward  = WGDB.MinDate;
      this.Status     = (Int32)JackpotAwardEvents.JackpotStatusFlag.None;
    } // JackpotAwards
    
    /// <summary>
    // Read Jackpot Award list
    /// </summary>
    /// <param name="JackpotAwardId"></param>
    /// <param name="AwardList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetJackpotAwardsByJackpotAwardId(Int64 JackpotAwardId, out List<JackpotAwardItem> AwardList, SqlTransaction SqlTrx)
    {
      return this.DB_GetJackpotAwards_Internal(JackpotAwardId, out AwardList, SqlTrx);
    } // GetJackpotAwardsByJackpotAwardId

    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific Jackpot Award Id 
    /// </summary>
    /// <param name="JackpotAwardId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotAwards_Internal(Int64 JackpotAwardId, out List<JackpotAwardItem> AwardList, SqlTransaction SqlTrx)
    {
      JackpotAwardItem _item;
            
      AwardList = new List<JackpotAwardItem>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotAwardsQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotAwardId", SqlDbType.Int).Value = JackpotAwardId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _item = new JackpotAwardItem();
              if (!DB_JackpotAwardsDataMapper(_item, _sql_reader))
              {
                return false;
              }

              AwardList.Add(_item);
            }
              return true;
            }
          }
        }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwards.DB_GetJackpotAwards_Internal -> JackpotAwardsId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotAwards_Internal

    /// <summary>
    /// Load Jackpot Awarded data from SQLReader
    /// </summary>
    /// <param name="AwardItem"></param>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotAwardsDataMapper(JackpotAwardItem AwardItem, SqlDataReader SqlReader)
    {
      try
      {
        AwardItem.Id          = SqlReader.GetInt64(DB_ID);
        AwardItem.JackpotId   = SqlReader.GetInt32(DB_JACKPOT_ID);
        AwardItem.AccountId   = SqlReader.IsDBNull(DB_ACCOUNT_ID) ? Jackpot.DEFAULT_ID_VALUE  : SqlReader.GetInt64(DB_ACCOUNT_ID);
        AwardItem.Amount      = SqlReader.GetDecimal(DB_AMOUNT); 
        AwardItem.DateAward   = SqlReader.GetDateTime(DB_DATE_AWARD);
        AwardItem.Name        = SqlReader.GetString(DB_NAME);
        AwardItem.Position    = SqlReader.GetInt32(DB_POSITION);
        AwardItem.TerminalId  = SqlReader.IsDBNull(DB_TERMINAL_ID) ? Jackpot.DEFAULT_ID_VALUE : SqlReader.GetInt32(DB_TERMINAL_ID);
        AwardItem.Type        = SqlReader.GetInt32(DB_TYPE) ;
        AwardItem.Status      = SqlReader.GetInt32(DB_STATUS);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotAwardsDataMapper");
        Log.Exception(_ex);
      }

      return false;
      
    } // DB_JackpotAwardsDataMapper 
   
    /// <summary>
    /// Gets Jackpot Awards select query
    /// </summary>
    /// <returns></returns>
    private String DB_JackpotAwardsQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("        SELECT   JAD_ID                         "); // 0
      _sb.AppendLine("               , JS_JACKPOT_ID                  "); // 1
      _sb.AppendLine("               , JS_NAME                        "); // 2
      _sb.AppendLine("               , JA_TYPE                        "); // 3
      _sb.AppendLine("               , JAD_AMOUNT                     "); // 4
      _sb.AppendLine("               , JAD_POSITION                   "); // 5
      _sb.AppendLine("               , JAD_TERMINAL_ID                "); // 6
      _sb.AppendLine("               , JAD_ACCOUNT_ID                 "); // 7
      _sb.AppendLine("               , JAD_DATE_AWARD                 "); // 8
      _sb.AppendLine("               , JAD_STATUS                     "); // 9
      _sb.AppendLine("          FROM   JACKPOTS_AWARD                 ");
      _sb.AppendLine("    INNER JOIN   JACKPOTS_AWARD_DETAIL          ");
      _sb.AppendLine("            ON   JAD_AWARD_ID = JA_ID           ");
      _sb.AppendLine("    INNER JOIN   JACKPOTS_SETTINGS              ");
      _sb.AppendLine("            ON   JA_JACKPOT_ID = JS_JACKPOT_ID  ");
      _sb.AppendLine("         WHERE   JA_ID = @pJackpotAwardId       ");
      _sb.AppendLine("      ORDER BY   JS_JACKPOT_ID, JAD_POSITION    ");

      return _sb.ToString();

    } // DB_JackpotAwardsQuery_Select  
    
    #endregion " Private Methods "

    #region " Internal Methods "
    
    /// <summary>
    /// Reset all members id
    /// </summary>
    internal void Reset()
    {
      this.Id = Jackpot.DEFAULT_ID_VALUE;
      this.JackpotId = Jackpot.DEFAULT_ID_VALUE;
    } // Reset
    
    /// <summary>
    /// Clone JackpotAwards
    /// </summary>
    /// <returns></returns>
    internal JackpotAwardItem Clone()
    {
      return new JackpotAwardItem()
      {
        Id          = this.Id,                    
        JackpotId   = this.JackpotId,
        AccountId   = this.AccountId,
        TerminalId  = this.TerminalId,
        Name        = this.Name,  
        Position    = this.Position, 
        Type        = this.Type,
        Amount      = this.Amount,
        DateAward   = this.DateAward
      };
    } // Clone

    #endregion 
  } // JackpotAwards
}
