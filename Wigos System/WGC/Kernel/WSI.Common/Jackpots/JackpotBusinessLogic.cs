﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotBusinessLogic.cs
// 
//   DESCRIPTION: Static class for manage Jackpot threads.
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 02-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-MAY-2017 ETP    First release.
// 18-JUL-2017 ETP    WIGOS-3515 New Jackpots option menu appears in wrong place and with wrong description.

//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{

  #region " Enums "

  public enum ThreadStatus
  {
    IDLE,
    CHECK,
    ERROR
  }

  public enum TypeProcess
  {
    CHECK_CONDITIONS,
    AWARD
  }

  #endregion

  public class JackpotBusinessLogic
  {

    #region " Threads "

    /// <summary>
    /// Distribute amounts
    /// </summary>
    /// <returns></returns>
    public static ThreadStatus ThreadDistribution()
    {
      return JackpotDistribution.Distribution();
    } // ThreadDistribution

    /// <summary>
    /// Check conditions for award jackpot
    /// </summary>
    /// <returns></returns>
    public static Boolean ThreadCheckConditions()
    {
      return JackpotCheckConditions.CheckConditions();

    } // ThreadCheckConditions

    /// <summary>
    /// Award process
    /// </summary>
    /// <returns></returns>
    public static Boolean ThreadAward()
    {
      return JackpotAwardEvents.Award();

    } // ThreadAward

    /// <summary>
    /// Fill meters process
    /// </summary>
    /// <returns></returns>
    public static Boolean ThreadFillMeters()
    {
      return JackpotMeters.FillMeters();

    } // ThreadFillMeters

    #endregion " Threads "

    #region " Public methods "

    /// <summary>
    /// Is Jackpots Enabled
    /// </summary>
    /// <returns></returns>
    public static Boolean IsJackpotsEnabled()
    {
      return WSI.Common.GeneralParam.GetBoolean("Jackpots", "Enabled", false);
    } // IsJackpotsEnabled

    /// <summary>
    /// Retrieve the Compensation Percentage
    /// </summary>
    /// <param name="ToCompensate"></param>
    /// <param name="Minimum"></param>
    /// <param name="Average"></param>
    /// <returns></returns>
    public static Decimal GetCompensationPct(Decimal ToCompensate, Decimal Minimum, Decimal Average)
    {
      Decimal _to_compensate;
      Decimal _to_compensate0;
      Decimal _compen_pct;

      if (Minimum <= 0)
      {
        // No minimum but there is something to be compensed ...
        if (ToCompensate > 0)
        {
          Minimum = Average * 0.5m;
        }
      }

      if (Minimum <= 0)
      {
        return 0.0m;
      }

      // To be compensated:
      _to_compensate = ToCompensate;  // Amount to be compensated
      _to_compensate += Average;      // Compensate 'Next Jackpot'
      // At the starting point:
      _to_compensate0 = Minimum;      // Minimum for next jackpot 

      _compen_pct = (Decimal)((200.0 / Math.PI) * Math.Atan((double)(_to_compensate / _to_compensate0)));
      _compen_pct = Math.Max(_compen_pct, 0);

      return _compen_pct;
    } // GetCompensationPct

    /// <summary>
    /// Determines when the Jackpot has been hit
    /// </summary>
    /// <param name="Contribution"></param>
    /// <param name="Average"></param>
    /// <param name="Accumulated"></param>
    /// <param name="Maximum"></param>
    /// <param name="ProbabilityChangeWithMax"></param>
    /// <returns></returns>
    public static Boolean JackpotHit(Decimal Contribution, Decimal Average, Decimal Accumulated, Decimal Maximum, Boolean ProbabilityChangeWithMax)
    {
      UInt64 _hits;
      Decimal _near;
      Decimal _x;

      if (Contribution <= 0)
      {
        return false;
      }

      if (Maximum <= 0)
      {
        return false;
      }

      if (Accumulated >= Maximum)
      {
        return true;
      }

      _hits = (UInt64)Math.Round(Average / Contribution, 0, MidpointRounding.AwayFromZero);

      if (ProbabilityChangeWithMax)
      {
        // Near Maximum?
        _near = Average + (Maximum - Average) * 9 / 10;
        if (Accumulated >= _near)
        {
          if (Maximum - _near <= 0)
          {
            _x = 1;
          }
          else
          {
            try
            {
              _x = (Accumulated - _near) / (Maximum - _near); // x = [0, 1]
            }
            catch
            {
              _x = 1;
            }
          }
          _x = Math.Min(_x, 1);
          _x = Math.Max(_x, 0);
          _hits = (UInt64)Math.Round((_hits * (1 - _x) * (1 - _x)), 0, MidpointRounding.AwayFromZero);
        }
      }

      // Minimum Hits = 1 -> 100.00%
      _hits = Math.Max(_hits, 1);

      return CERTIFIED_RNG.Hit(_hits);

    } // JackpotHit

    /// <summary>
    /// Get All jackpots
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="ListJackpots"></param>
    /// <returns></returns>
    public static Boolean GetAllJackpots(Jackpot.JackpotFilterType Filter, SqlTransaction SqlTrx, out List<Jackpot> ListJackpots)
    {
      Jackpot _cls_jackpot;

      try
      {
        _cls_jackpot = new Jackpot();

        ListJackpots = _cls_jackpot.GetAllJackpots(Filter, SqlTrx);

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.getAllJackpots");
        Log.Exception(_ex);
      }

      ListJackpots = null;

      return false;
    } // GetAllJackpots

    /// <summary>
    /// Get all list for check
    /// </summary>
    /// <param name="Filter"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="ListInfoPlaySessions"></param>
    /// <param name="AllJackpots"></param>
    /// <param name="Occupancy"></param>
    /// <param name="DtAccountFlags"></param>
    /// <returns></returns>
    public static Boolean GetAllListForCheck(Jackpot.JackpotFilterType Filter, SqlTransaction SqlTrx, out List<InfoPlaySession> ListInfoPlaySessions,
                                             out List<Jackpot> AllJackpots, out Decimal Occupancy, out DataTable DtAccountFlags)
    {

      try
      {

        Occupancy = 0;
        DtAccountFlags = new DataTable();
        AllJackpots = new List<Jackpot>();
        ListInfoPlaySessions = new List<InfoPlaySession>();

        if (!GetInfoPlaySessions(SqlTrx, out ListInfoPlaySessions))
        {
          return false;
        }

        if (!GetAllJackpots(Filter, SqlTrx, out AllJackpots))
        {
          return false;
        }

        if (!GetOccupancy(SqlTrx, out Occupancy))
        {
          return false;
        }

        if (!GetAccountFlags(SqlTrx, out DtAccountFlags))
        {
          return false;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.GetAllListForCheck");
        Log.Exception(_ex);
      }

      Occupancy = -1;
      AllJackpots = null;
      DtAccountFlags = null;
      ListInfoPlaySessions = null;

      return false;

    } // GetAllListForCheck

    /// <summary>
    /// Get Random winners list
    /// </summary>
    /// <param name="ListInfoPlaySessions"></param>
    /// <param name="NumWinners"></param>
    /// <param name="Winners"></param>
    /// <returns></returns>
    public static Boolean GetWinners(List<InfoPlaySession> ListInfoPlaySessions, Int32 MaxNumWinners, out List<InfoPlaySession> Winners)
    {
      Int32 _idx_lucky;
      Int32 _num_winners;
      try
      {

        Winners = new List<InfoPlaySession>();
        _num_winners = Math.Min(ListInfoPlaySessions.Count, MaxNumWinners);

        for (Int32 _idx = 0; _idx < _num_winners; _idx++)
        {
          _idx_lucky = (Int32)CERTIFIED_RNG.GetRandomNumber(ListInfoPlaySessions.Count);

          Winners.Add(ListInfoPlaySessions[_idx_lucky]);
          ListInfoPlaySessions.RemoveAt(_idx_lucky);
        }

        return true;

      }
      catch (Exception Ex)
      {
        Log.Error("JackpotBusinessLogic.GetWinners");
        Log.Exception(Ex);
      }

      Winners = null;

      return false;
    } // GetWinners

    /// <summary>
    /// Truncate aomunt with X decimals
    /// </summary>
    /// <param name="Amount"></param>
    /// <param name="NumDecimals"></param>
    /// <returns></returns>
    public static Decimal TruncateAmount(Decimal Amount, Int32 NumDecimals)
    {
      Double _decimals;

      _decimals = Math.Pow(Convert.ToDouble(10), Convert.ToDouble(NumDecimals));

      return Convert.ToDecimal(Math.Truncate(_decimals * Convert.ToDouble(Amount)) / _decimals);
    } // TruncateAmount

    /// <summary>
    /// Add status into other status
    /// </summary>
    /// <param name="CurrentBitmask"></param>
    /// <param name="Flag"></param>
    /// <param name="FlagStatus"></param>
    /// <returns></returns>
    public static Int32 UpdateBitmask(Int32 CurrentBitmask, Int32 Flag, Boolean FlagStatus)
    {
      if (FlagStatus)
      {
        return (CurrentBitmask | Flag);
      }
      else
      {
        return (CurrentBitmask & (~Flag));
      }
    } // UpdateBitmask

    /// <summary>
    /// Check if a flag is set in bitmask
    /// </summary>
    /// <param name="CurrentBitmask"></param>
    /// <param name="Flag"></param>
    /// <returns></returns>
    public static Boolean IsFlagActived(Int32 CurrentBitmask, Int32 Flag)
    {
      return ((CurrentBitmask & Flag) == Flag);
    } // IsFlagActived

    /// <summary>
    /// Get terminals by element type
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="JackpotId"></param>
    /// <param name="ExploitElementType"></param>
    /// <param name="DtContributions"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean GetTerminalsByElementType(Int64 TerminalId, Int32 JackpotId, EXPLOIT_ELEMENT_TYPE ExploitElementType,
                                                     out DataTable DtContributions, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      DtContributions = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TG_TERMINAL_ID              ");
        _sb.AppendLine("        , TG_ELEMENT_ID               ");
        _sb.AppendLine("        , TG_ELEMENT_TYPE             ");
        _sb.AppendLine("   FROM   TERMINAL_GROUPS             ");
        _sb.AppendLine("  WHERE   1=1                         ");

        if (TerminalId > -1)
        {
          _sb.AppendLine("  AND   TG_TERMINAL_ID = @pTerminalId ");
        }

        _sb.AppendLine("    AND   TG_ELEMENT_TYPE = @pType      ");

        if (TerminalId == -1)
        {
          _sb.AppendLine("    AND   TG_ELEMENT_ID = @pJackpotId ");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          if (TerminalId > -1)
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          }

          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = ExploitElementType;

          if (TerminalId == -1)
          {
            _sql_cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;
          }

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(DtContributions);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } // GetTerminalsByElementType

    /// <summary>
    /// Sharing prize Generation
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="PlaySessionInfo"></param>
    /// <param name="SharedID"></param>
    /// <param name="AmountShared"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="WinnerDetailsId"></param>
    /// <param name="AmountToWinner"></param>
    /// <returns></returns>
    public static Boolean SharingPrizeGeneration(Jackpot Jackpot, InfoPlaySession PlaySessionInfo, Int64 SharedID,
                                                 Decimal AmountShared, SqlTransaction SqlTrx, out Int64 WinnerDetailsId, out Decimal AmountToWinner)
    {
      List<InfoPlaySession> _list_info_play_sessions;
      List<InfoPlaySession> _winners;
      JackpotAwardEvents _jackpotAwardEvents;

      _jackpotAwardEvents = new JackpotAwardEvents(SharedID, AmountShared);
      AmountToWinner = 0;
      WinnerDetailsId = -1;

      // ETP 24/05/2017 Not shared amount
      if (_jackpotAwardEvents.SharedAmount == 0.0m)
      {
        return true;
      }

      if (!DB_GetSharedJackpotList(Jackpot.AwardPrizeConfig.PrizeSharing, PlaySessionInfo, SqlTrx, out _list_info_play_sessions))
      {
        return false;
      }

      if (_list_info_play_sessions == null)
      {
        return false;
      }

      _list_info_play_sessions.Add(PlaySessionInfo);

      if (!GetWinners(_list_info_play_sessions, Jackpot.AwardPrizeConfig.PrizeSharing.MaxNumWinner, out _winners))
      {
        return false;
      }

      if (_winners.Count > 0)
      {
        if (!_jackpotAwardEvents.StartSharedAward(Jackpot, _winners, PlaySessionInfo, SqlTrx, out WinnerDetailsId, out AmountToWinner))
        {
          return false;
        }
      }

      return true;
    } // Sharing prize generation.

    /// <summary>
    /// Filter lists play session by terminals award set in jackpot
    /// </summary>
    /// <returns></returns>
    public static Boolean FilterPlaySessionsByTerminalsAwardJackpot(Jackpot Jackpot, ref List<InfoPlaySession> ListPlaySessionsForCheck, SqlTransaction SqlTrx)
    {

      DataTable _dt_terminals;
      List<Int32> _terminals_id;

      try
      {

        _terminals_id = new List<Int32>();

        //Get terminals
        if (!GetTerminalsByElementType(-1, Jackpot.Id, EXPLOIT_ELEMENT_TYPE.JACKPOT_AWARDS, out _dt_terminals, SqlTrx))
        {
          return false;
        }

        if (_dt_terminals.Rows.Count == 0) return true;

        foreach (DataRow dr in _dt_terminals.Rows)
        {
          _terminals_id.Add((Int32)dr["TG_TERMINAL_ID"]);
        }

        ListPlaySessionsForCheck = ListPlaySessionsForCheck.FindAll(_condition => _terminals_id.Contains(_condition.TerminalId));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.FilterPlaySessionsByTerminalsAwardJackpot");
        Log.Exception(_ex);
      }

      return false;

    } // FilterPlaySessionsByTerminalsAwardJackpot

    #endregion " Public methods "

    #region " Private methods "

    /// <summary>
    /// Get occupancy site
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="Occupancy"></param>
    /// <returns></returns>
    private static Boolean GetOccupancy(SqlTransaction SqlTrx, out Decimal Occupancy)
    {

      try
      {
        if (!DB_GetOccupancy(out Occupancy, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.GetOccupancy");
        Log.Exception(_ex);
      }

      Occupancy = -1;

      return false;

    } // GetOccupancy

    /// <summary>
    /// Get info play sessions
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="InfoCheckCondition"></param>
    /// <returns></returns>
    private static Boolean GetInfoPlaySessions(SqlTransaction SqlTrx, out List<InfoPlaySession> InfoCheckCondition)
    {

      try
      {
        if (!DB_GetInfoPlaySessions(out InfoCheckCondition, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.GetInfoPlaySessions");
        Log.Exception(_ex);
      }


      InfoCheckCondition = null;

      return false;
    } // GetInfoPlaySessions

    /// <summary>
    /// Get info flags
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="DtAccountFlags"></param>
    /// <returns></returns>
    private static Boolean GetAccountFlags(SqlTransaction SqlTrx, out DataTable DtAccountFlags)
    {
      try
      {
        if (!DB_GetAccountFlags(out DtAccountFlags, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.GetAccountFlags");
        Log.Exception(_ex);
      }

      DtAccountFlags = null;

      return false;

    } // GetAccountFlags

    #endregion " Private methods "

    #region " Database methods "

    /// <summary>
    /// Get occupancy
    /// </summary>
    /// <param name="Occupancy"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_GetOccupancy(out Decimal Occupancy, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_GetOccupancy_Select(), SqlTrx.Connection, SqlTrx))
        {
          Occupancy = (Decimal)_cmd.ExecuteScalar();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.DB_GetOccupancy");
        Log.Exception(_ex);
      }

      Occupancy = -1;

      return false;
    } // DB_GetOccupancy

    /// <summary>
    /// Get conditions
    /// </summary>
    /// <param name="ListInfoCheckCondition"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_GetInfoPlaySessions(out List<InfoPlaySession> ListInfoCheckCondition, SqlTransaction SqlTrx)
    {

      InfoPlaySession _info_check_condition;

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_GetInfoPlaySessions_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pOpenned", SqlDbType.Int).Value = PlaySessionStatus.Opened;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            ListInfoCheckCondition = new List<InfoPlaySession>();

            while (_sql_reader.Read())
            {
              _info_check_condition = new InfoPlaySession();
              DB_InfoCheckConditionDataMapper(_info_check_condition, _sql_reader);
              ListInfoCheckCondition.Add(_info_check_condition);
            }
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.DB_GetInfoPlaySessions");
        Log.Exception(_ex);
      }

      ListInfoCheckCondition = null;

      return false;

    } // DB_GetInfoPlaySessions

    /// <summary>
    /// Get account flags
    /// </summary>
    /// <param name="DtAccountFlags"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_GetAccountFlags(out DataTable DtAccountFlags, SqlTransaction SqlTrx)
    {

      try
      {
        DtAccountFlags = new DataTable();

        using (SqlCommand _cmd = new SqlCommand(DB_GetAccountFlags_Select(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(DtAccountFlags);
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.DB_GetAccountFlags");
        Log.Exception(_ex);
      }

      DtAccountFlags = null;

      return false;

    } // DB_GetAccountFlags

    /// <summary>
    /// Get select play sessions for conditions
    /// </summary>
    /// <returns></returns>
    private static String DB_GetInfoPlaySessions_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   PS_TERMINAL_ID										                                                      ");
      _sb.AppendLine(" 	          , AC_ACCOUNT_ID							  			                                                      ");
      _sb.AppendLine(" 		        , AC_CREATED									    	                                                      ");
      _sb.AppendLine(" 		        , AC_HOLDER_BIRTH_DATE						                                                        ");
      _sb.AppendLine(" 		        , AC_HOLDER_LEVEL									                                                        ");
      _sb.AppendLine(" 		        , AC_HOLDER_GENDER									                                                      ");
      _sb.AppendLine(" 		        , CASE WHEN ISNULL (AC_IN_SESSION_PROMO_NR_TO_GM, 0) > 0 THEN 1 ELSE 0 END AS HAS_PROMO   ");
      _sb.AppendLine(" 		        , CASE WHEN AC_USER_TYPE <> 0 AND AC_HOLDER_LEVEL <> 0 THEN 0 ELSE 1 END AS IS_ANONYMOUS  ");
      _sb.AppendLine(" 		        , CASE WHEN PS_PLAYED_COUNT > 0 THEN PS_PLAYED_AMOUNT / PS_PLAYED_COUNT ELSE 0 END MINBET ");
      _sb.AppendLine(" 		        , TE_BANK_ID                                                                              ");
      _sb.AppendLine(" 		        , BK_AREA_ID                                                                              ");
      _sb.AppendLine(" 		        , ISNULL(AC_HOLDER_IS_VIP, 0) AS VIP                                                      ");
      _sb.AppendLine("       FROM   PLAY_SESSIONS 										                                                      ");
      _sb.AppendLine(" INNER JOIN   ACCOUNTS ON AC_ACCOUNT_ID = PS_ACCOUNT_ID							                                  ");
      _sb.AppendLine(" INNER JOIN   TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID							                              ");
      _sb.AppendLine(" INNER JOIN   BANKS     ON TE_BANK_ID = BK_BANK_ID        			                                      ");
      _sb.AppendLine("      WHERE   PS_STATUS = @pOpenned  					                                                        ");

      return _sb.ToString();

    } // DB_GetInfoPlaySessions_Select

    /// <summary>
    /// Get occupancy
    /// </summary>
    /// <returns></returns>
    private static String DB_GetOccupancy_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" SELECT   CASE (SELECT COUNT(*) FROM TERMINALS WHERE TE_TYPE = 1 AND TE_STATUS = 0 AND TE_TERMINAL_TYPE < 100) WHEN 0 THEN 0                                 ");
      _sb.AppendLine("          ELSE 100.0 * ( SELECT COUNT(1) FROM (SELECT DISTINCT PS_TERMINAL_ID FROM PLAY_SESSIONS WITH (INDEX (IX_PS_STATUS))  WHERE PS_STATUS = 0) PLAYING ) ");
      _sb.AppendLine("               /                                                                                                                                             ");
      _sb.AppendLine("               (SELECT COUNT(*) FROM TERMINALS WHERE TE_TYPE = 1 AND TE_STATUS = 0 AND TE_TERMINAL_TYPE < 100) END AS PCT                                    ");

      return _sb.ToString();
    } // DB_GetOccupancy_Select

    /// <summary>
    /// Get account flags
    /// </summary>
    /// <returns></returns>
    private static String DB_GetAccountFlags_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("   SELECT   AF_ACCOUNT_ID          ");
      _sb.AppendLine("          , AF_FLAG_ID             ");
      _sb.AppendLine("          , COUNT(1) AS FLAG_COUNT ");
      _sb.AppendLine("	   FROM   ACCOUNT_FLAGS          ");
      _sb.AppendLine(" GROUP BY   AF_ACCOUNT_ID          ");
      _sb.AppendLine("          , AF_FLAG_ID             ");

      return _sb.ToString();
    } // DB_GetOccupancy_Select

    /// <summary>
    /// Mapper BD to object
    /// </summary>
    /// <param name="CheckCondition"></param>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private static Boolean DB_InfoCheckConditionDataMapper(InfoPlaySession CheckCondition, SqlDataReader SqlReader)
    {
      try
      {

        CheckCondition.TerminalId = (Int32)SqlReader["PS_TERMINAL_ID"];
        CheckCondition.AccountId = (Int64)SqlReader["AC_ACCOUNT_ID"];
        CheckCondition.AccountCreated = (DateTime)SqlReader["AC_CREATED"];

        if (SqlReader["AC_HOLDER_BIRTH_DATE"] != DBNull.Value)
        {
          CheckCondition.AccountHolderBirth = (DateTime)SqlReader["AC_HOLDER_BIRTH_DATE"];
        }

        CheckCondition.AccountHolderLevel = (Int32)SqlReader["AC_HOLDER_LEVEL"];

        if (SqlReader["AC_HOLDER_GENDER"] != DBNull.Value)
        {
          CheckCondition.AccountHolderGender = (Int32)SqlReader["AC_HOLDER_GENDER"];
        }

        CheckCondition.HasPromotion = ((Int32)SqlReader["HAS_PROMO"] == 1);
        CheckCondition.IsAnonymousAccount = ((Int32)SqlReader["IS_ANONYMOUS"] == 1);
        CheckCondition.MinBet = (Decimal)SqlReader["MINBET"];
        CheckCondition.TerminalAreaId = (Int32)SqlReader["BK_AREA_ID"];
        CheckCondition.TerminalBankId = (Int32)SqlReader["TE_BANK_ID"];
        CheckCondition.IsVip = (Boolean)SqlReader["VIP"];

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_InfoCheckConditionDataMapper");
        Log.Exception(_ex);
      }

      CheckCondition = null;

      return false;

    } // DB_InfoCheckConditionDataMapper    

    /// <summary>
    /// DB_GetSharedJackpotList
    /// </summary>
    /// <param name="PrizeSharing"></param>
    /// <param name="PlaySessionInfo"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="ListInfoPlaySessions"></param>
    /// <returns></returns>
    private static Boolean DB_GetSharedJackpotList(JackpotAwardPrizeSharing PrizeSharing, InfoPlaySession PlaySessionInfo,
                                                   SqlTransaction SqlTrx, out List<InfoPlaySession> ListInfoPlaySessions)
    {
      StringBuilder _sb;
      InfoPlaySession _play_session;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("       SELECT   PS_TERMINAL_ID										                  ");
        _sb.AppendLine(" 	            , PS_ACCOUNT_ID							  			                  ");
        _sb.AppendLine("         FROM   PLAY_SESSIONS 										                  ");

        if (PrizeSharing.SameBank)
        {
          _sb.AppendLine(" INNER JOIN   TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID				");
          _sb.AppendLine("        AND   TE_BANK_ID                  = @pBankId  			      ");
        }
        else if (PrizeSharing.SameArea)
        {
          _sb.AppendLine(" INNER JOIN   TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID			  ");
          _sb.AppendLine(" INNER JOIN   BANKS     ON TE_BANK_ID     = BK_BANK_ID        		");
          _sb.AppendLine("        AND   BK_AREA_ID                  = @pAreaId              ");
        }
        else if (PrizeSharing.SameJackpot)
        {
          _sb.AppendLine(" INNER JOIN   TERMINAL_GROUPS ON TG_TERMINAL_ID = PS_TERMINAL_ID	");
          _sb.AppendLine("        AND   TG_ELEMENT_TYPE                   = @pAwardType     ");
          _sb.AppendLine("        AND   TG_ELEMENT_ID                     = @pJackpotId     ");
        }

        _sb.AppendLine("        WHERE   PS_STATUS                         = @pOpenned				");
        _sb.AppendLine("        AND   PS_ACCOUNT_ID                  != @pWinnerAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {

          if (PrizeSharing.SameBank)
          {
            _cmd.Parameters.Add("@pBankId", SqlDbType.Int).Value = PlaySessionInfo.TerminalBankId;

          }
          else if (PrizeSharing.SameArea)
          {
            _cmd.Parameters.Add("@pAreaId", SqlDbType.Int).Value = PlaySessionInfo.TerminalAreaId;

          }
          else if (PrizeSharing.SameJackpot)
          {
            _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = PrizeSharing.JackpotId;
            _cmd.Parameters.Add("@pAwardType", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.JACKPOT_AWARDS;
          }

          _cmd.Parameters.Add("@pOpenned", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pWinnerAccountId", SqlDbType.BigInt).Value = PlaySessionInfo.AccountId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            ListInfoPlaySessions = new List<InfoPlaySession>();

            while (_sql_reader.Read())
            {
              _play_session = new InfoPlaySession()
              {
                TerminalId = (Int32)_sql_reader["PS_TERMINAL_ID"],
                AccountId = (Int64)_sql_reader["PS_ACCOUNT_ID"]
              };

              ListInfoPlaySessions.Add(_play_session);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotBusinessLogic.DB_GetSharedJackpotList");
        Log.Exception(_ex);
      }

      ListInfoPlaySessions = null;

      return false;
    } // DB_GetSharedJackpotList

    #endregion " Database methods "

  }

  #region " Classes "

  public class InfoPlaySession
  {
    public Int32 TerminalId { get; set; }
    public Int64 AccountId { get; set; }
    public DateTime AccountCreated { get; set; }
    public DateTime AccountHolderBirth { get; set; }
    public Int32 AccountHolderLevel { get; set; }
    public Int32 AccountHolderGender { get; set; }
    public Boolean HasPromotion { get; set; }
    public Boolean IsAnonymousAccount { get; set; }
    public Decimal MinBet { get; set; }
    public Int32 TerminalBankId { get; set; }
    public Int32 TerminalAreaId { get; set; }
    public Boolean IsVip { get; set; }
  }

  public class InfoAward
  {
    public Int32 JackpotId;
    public Int32 Status;
    public Jackpot.JackpotType JackpotType;
  }

  #endregion " Classes "

}
