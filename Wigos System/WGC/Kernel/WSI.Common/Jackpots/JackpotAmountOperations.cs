﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAmountOperations.cs
// 
//   DESCRIPTION: Jackpots amount operations
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 25-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-MAY-2017 ETP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAmountOperations
  {

    public enum OperationType
    {
      None = 0,
      Add = 1,
      Reset = 2,
      Award = 3,
      Recalculate = 4
    }

    public enum OperationStatus
    {
      None = 0,
      Pending = 1,
      Processed = 2,
      Cancelled = 3
    }

    #region " constructor "

    /// <summary>
    /// Class constructor Init Id = -1
    /// </summary>
    /// 
    public JackpotAmountOperations()
    {
      JackpotId = -1;
      Id = -1;
    }

    #endregion " constructor "

    #region " members "

    public Int64 Id { get; set; }
    public Int32 JackpotId { get; set; }
    public Jackpot.JackpotType JackpotType { get; set; }
    public OperationType Type { get; set; }
    public OperationStatus Status { get; set; }
    public Decimal Amount { get; set; }
    public String UserName { get; set; }

    #endregion " members "

    #region " public methods "

    /// <summary>
    /// Save
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Save()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Save(_db_trx.SqlTransaction))
        {
          return false;
        }
        _db_trx.Commit();
      }

      return true;
    } // Save

    /// <summary>
    /// Save
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Save(SqlTransaction SqlTrx)
    {

      if (!DB_OperationAmount(SqlTrx))
      {
        return false;
      }

      return true;
    } // Save

    /// <summary>
    /// Get operation amounts pending
    /// </summary>
    /// <param name="AmountOperations"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_GetOperationAmounts(out List<JackpotAmountOperations> AmountOperations, SqlTransaction SqlTrx)
    {

      StringBuilder _sb;
      JackpotAmountOperations _operation;

      try
      {
        AmountOperations = new List<JackpotAmountOperations>();

        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   JAO_ID                       ");
        _sb.AppendLine("           , JAO_JACKPOT_ID               ");
        _sb.AppendLine("           , JAO_JACKPOT_TYPE             ");
        _sb.AppendLine("           , JAO_TYPE                     ");
        _sb.AppendLine("           , JAO_STATUS                   ");
        _sb.AppendLine("           , JAO_AMOUNT                   ");
        _sb.AppendLine("           , JAO_USER_NAME                ");
        _sb.AppendLine("           , JAO_CREATION                 ");
        _sb.AppendLine("           , JAO_LAST_UPDATE              ");
        _sb.AppendLine("     FROM    JACKPOTS_AMOUNT_OPERATIONS   ");
        _sb.AppendLine("    WHERE    JAO_STATUS = @pStatusPending ");
        _sb.AppendLine(" ORDER BY    JAO_CREATION   ASC           ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = OperationStatus.Pending;

          using (SqlDataReader _sql_dr = _sql_cmd.ExecuteReader())
          {
            while (_sql_dr.Read())
            {
              _operation = new JackpotAmountOperations()
              {
                Id = (Int64)_sql_dr["JAO_ID"],
                JackpotId = (Int32)_sql_dr["JAO_JACKPOT_ID"],
                JackpotType = (Jackpot.JackpotType)_sql_dr["JAO_JACKPOT_TYPE"],
                Type = (OperationType)_sql_dr["JAO_TYPE"],
                Status = (OperationStatus)_sql_dr["JAO_STATUS"],
                Amount = (Decimal)_sql_dr["JAO_AMOUNT"],
                UserName = (String)_sql_dr["JAO_USER_NAME"]
              };

              AmountOperations.Add(_operation);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("JackpotAmountOperations.DB_GetOperationAmounts() - An error occurred while getting JACKPOTS_AMOUNT_OPERATIONS");
        Log.Error(_ex.Message);
      }
      AmountOperations = null;

      return false;
    } // DB_GetOperationAmounts

    /// <summary>
    /// Recalculate amounts when change Max/Min of main, hh or shared jackpots
    /// </summary>
    /// <param name="RecalculateJackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean RecalculateJackpotAmounts(Int32 RecalculateJackpotId, SqlTransaction SqlTrx)
    {
      JackpotAmountOperations _amount_ops;

      _amount_ops = new JackpotAmountOperations();
      _amount_ops.Id = -1;
      _amount_ops.JackpotId = RecalculateJackpotId;
      _amount_ops.JackpotType = Jackpot.JackpotType.All;
      _amount_ops.Status = JackpotAmountOperations.OperationStatus.Pending;
      _amount_ops.Type = OperationType.Recalculate;
      _amount_ops.Amount = 0;
      _amount_ops.UserName = String.Empty;

      if (!_amount_ops.Save(SqlTrx))
      {
        Log.Error("JackpotAmountOperations.RecalculateJackpotAmounts");

        return false;
      }

      return true;
    } // RecalculateJackpotAmounts

    #endregion " public methods "

    #region " private methods "

    /// <summary>
    /// Insert or Update ExternalAmount
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_OperationAmount(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DateTime _update_time;

      try
      {
        _update_time = WGDB.Now;
        _sb = new StringBuilder();

        if (Id == -1)
        {
          _sb.AppendLine(" INSERT INTO   JACKPOTS_AMOUNT_OPERATIONS               ");
          _sb.AppendLine("             ( JAO_JACKPOT_ID                           ");
          _sb.AppendLine("             , JAO_JACKPOT_TYPE                         ");
          _sb.AppendLine("             , JAO_TYPE                                 ");
          _sb.AppendLine("             , JAO_STATUS                               ");
          _sb.AppendLine("             , JAO_AMOUNT                               ");
          _sb.AppendLine("             , JAO_USER_NAME                            ");
          _sb.AppendLine("             , JAO_CREATION                             ");
          _sb.AppendLine("             , JAO_LAST_UPDATE     )                    ");
          _sb.AppendLine("      VALUES                                            ");
          _sb.AppendLine("             ( @pJackpotId                              ");
          _sb.AppendLine("             , @pJackpotType                            ");
          _sb.AppendLine("             , @pType                                   ");
          _sb.AppendLine("             , @pStatus                                 ");
          _sb.AppendLine("             , @pAmount                                 ");
          _sb.AppendLine("             , @pUserName                               ");
          _sb.AppendLine("             , @pCreation                               ");
          _sb.AppendLine("             , @pLastUpdate        )                    ");
        }
        else
        {
          _sb.AppendLine("      UPDATE   JACKPOTS_AMOUNT_OPERATIONS              ");
          _sb.AppendLine("         SET   JAO_JACKPOT_ID          = @pJackpotId   ");
          _sb.AppendLine("             , JAO_JACKPOT_TYPE        = @pJackpotType ");
          _sb.AppendLine("             , JAO_TYPE                = @pType        ");
          _sb.AppendLine("             , JAO_STATUS              = @pStatus      ");
          _sb.AppendLine("             , JAO_AMOUNT              = @pAmount      ");
          _sb.AppendLine("             , JAO_USER_NAME           = @pUserName    ");
          _sb.AppendLine("             , JAO_LAST_UPDATE         = @pLastUpdate  ");
          _sb.AppendLine("       WHERE   JAO_ID                  = @pId          ");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = this.Id;
          _sql_cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = this.JackpotId;
          _sql_cmd.Parameters.Add("@pJackpotType", SqlDbType.Int).Value = this.JackpotType;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = this.Type;
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = this.Status;
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = this.Amount;
          _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar).Value = (Object)this.UserName ?? DBNull.Value;
          _sql_cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = _update_time;
          _sql_cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = _update_time;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Message("JackpotAmountOperations.DB_OperationAmount() - An error occurred inserting into JACKPOTS_AMOUNT_OPERATIONS");

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_ExternalAmount

    #endregion " private methods "
  }
}
