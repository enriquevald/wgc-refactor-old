﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: ExtensionMethods.cs
// 
//      DESCRIPTION: PlayerInfoData Extension Methods
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 04-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-APR-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.Common;

namespace WSI.Common.Jackpot
{
  public static class ExtensionMethods
  {

    #region " TreeViewItem "

    /// <summary>
    /// Convert JackpotList into a TreeViewItemList
    /// </summary>
    /// <param name="JackpotList"></param>
    /// <returns></returns>
    public static List<TreeViewItem> ToTreeViewItemList(this List<Jackpot> JackpotList)
    {
      return ToTreeViewItemList(JackpotList, null);
    }
    public static List<TreeViewItem> ToTreeViewItemList(this List<Jackpot> JackpotList, EventHandler MenuItem_OnClick)
    {
      List<TreeViewItem> _tree_view_item;

      _tree_view_item = new List<TreeViewItem>();

      if (JackpotList != null && JackpotList.Count > 0)
      {
        foreach (Jackpot _jackpot in JackpotList)
        {
          _tree_view_item.Add(_jackpot.ToTreeViewItem(MenuItem_OnClick));
        }
      }

      return _tree_view_item;

    } // ToTreeViewItemList

    public static List<TreeViewItem> ToTreeViewItemList(this List<JackpotViewer> JackpotViewerList)
    {
      List<TreeViewItem> _treeview_item;

      _treeview_item = new List<TreeViewItem>();

      if (JackpotViewerList != null && JackpotViewerList.Count > 0)
      {
        foreach (JackpotViewer _jackpot_viewer in JackpotViewerList)
        {
          _treeview_item.Add(_jackpot_viewer.ToTreeViewItem(null));
        }
      }

      return _treeview_item;
    }

    #endregion

    #region " Jackpot "

    /// <summary>
    /// Get specific jackpot by id
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <returns></returns>
    public static Jackpot GetJackpotById(this List<Jackpot> JackpotList, Int32 JackpotId)
    {
      foreach (Jackpot _jackpot in JackpotList)
      {
        if(_jackpot.Id == JackpotId)
        {
          return _jackpot;
        }
      }

      return null;
    } // GetJackpotById

    /// <summary>
    /// Set specific jackpot by id
    /// </summary>
    /// <param name="JackpotList"></param>
    /// <param name="JackpotId"></param>
    /// <param name="NewJackpotValue"></param>
    public static void SetJackpotById(this List<Jackpot> JackpotList, Int32 JackpotId, Jackpot NewJackpotValue)
    {
      Int32 _idx;

      for (_idx = 0; _idx < JackpotList.Count; _idx++)
      {
        if (JackpotList[_idx].Id == JackpotId)
        {
          JackpotList[_idx] = NewJackpotValue;
        }        
      }
    } // SetJackpotById  
 
    /// <summary>
    /// Get specific jackpot viewer by id
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <returns></returns>
    public static JackpotViewer GetJackpotViewerById(this List<JackpotViewer> JackpotViewerList, Int32 JackpotViewerId)
    {
      foreach (JackpotViewer _jackpot_viewer in JackpotViewerList)
      {
        if (_jackpot_viewer.Id == JackpotViewerId)
        {
          return _jackpot_viewer;
        }
      }

      return null;
    } // GetJackpotViewerById

    /// <summary>
    /// Set specific jackpot viewer by id
    /// </summary>
    /// <param name="JackpotViewerList"></param>
    /// <param name="JackpotViewerId"></param>
    /// <param name="NewJackpotViewerValue"></param>
    public static void SetJackpotViewerById(this List<JackpotViewer> JackpotViewerList, Int32 JackpotViewerId, JackpotViewer NewJackpotViewerValue)
    {
      Int32 _idx;

      for (_idx = 0; _idx < JackpotViewerList.Count; _idx++)
      {
        if (JackpotViewerList[_idx].Id == JackpotViewerId)
        {
          JackpotViewerList[_idx] = NewJackpotViewerValue;
        }
      }
    } // SetJackpotViewerById  

    /// <summary>
    /// Clone Jackpot list 
    /// </summary>
    /// <returns></returns>
    public static List<Jackpot> Clone(this List<Jackpot> JackpotList)
    {
      List<Jackpot> _jackpot_list = new List<Jackpot>();

      foreach (Jackpot _jackpot in JackpotList)
      {
        _jackpot_list.Add(_jackpot.Clone());
      }

      return _jackpot_list;
    } // Clone

    /// <summary>
    /// Clone JackpotContribution list 
    /// </summary>
    /// <returns></returns>
    public static Boolean IsConfigured(this List<JackpotContribution> JackpotContributionList)
    {
      foreach (JackpotContribution _contribution in JackpotContributionList)
      {
        if(!_contribution.IsConfigured)
        {
          return false;
        }
      }

      return true;
    } // IsConfigured

    /// <summary>
    /// Clone JackpotContribution list 
    /// </summary>
    /// <returns></returns>
    public static List<JackpotContribution> Clone(this List<JackpotContribution> JackpotContributionList)
    {
      List<JackpotContribution> _jackpot_contribution_list = new List<JackpotContribution>();

      foreach (JackpotContribution _jackpot_contribution in JackpotContributionList)
      {
        _jackpot_contribution_list.Add(_jackpot_contribution.Clone());
      }

      return _jackpot_contribution_list;
    } // Clone

    /// <summary>
    /// Clone Jackpotflags list 
    /// </summary>
    /// <returns></returns>
    public static List<JackpotAwardFilterFlag> Clone(this List<JackpotAwardFilterFlag> JackpotflagsList)
    {
      List<JackpotAwardFilterFlag> _jackpot_flags_list = new List<JackpotAwardFilterFlag>();

      foreach (JackpotAwardFilterFlag _jackpot_flags in JackpotflagsList)
      {
        _jackpot_flags_list.Add(_jackpot_flags.Clone());
      }

      return _jackpot_flags_list;
    } // Clone

    #endregion

    #region " JackpotViewer "


    /// <summary>
    /// Get specific Jackpot Viewer item by JackpotId
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <returns></returns>
    public static JackpotViewerItem GetJackpotViewerItemByJackpotId(this List<JackpotViewerItem> JackpotViewerList, Int32 JackpotId)
    {
      foreach (JackpotViewerItem _item in JackpotViewerList)
      {
        if (_item.JackpotId == JackpotId)
        {
          return _item;
        }
      }

      return null;
    } // GetJackpotById

    /// <summary>
    /// Get specific Jackpot Viewer item by TerminalID
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <returns></returns>
    public static JackpotViewerTerminalItem GetJackpotViewerItemByTerminalId(this List<JackpotViewerTerminalItem> JackpotViewerList, Int32 TerminalId)
    {
      foreach (JackpotViewerTerminalItem _item in JackpotViewerList)
      {
        if (_item.TerminalId == TerminalId)
        {
          return _item;
        }
      }

      return null;
    } // GetJackpotById
    
    /// <summary>
    /// Clone JackpotViewer list 
    /// </summary>
    /// <returns></returns>
    public static List<JackpotViewer> Clone(this List<JackpotViewer> JackpotViewerList)
    {
      List<JackpotViewer> _jackpot_viewer_list = new List<JackpotViewer>();

      foreach (JackpotViewer _jackpot_viewer in JackpotViewerList)
      {
        _jackpot_viewer_list.Add(_jackpot_viewer.Clone());
      }

      return _jackpot_viewer_list;
    } // Clone

    /// <summary>
    /// Clone JackpotViewerItem list 
    /// </summary>
    /// <returns></returns>
    public static List<JackpotViewerItem> Clone(this List<JackpotViewerItem> JackpotViewerItem)
    {
      List<JackpotViewerItem> _jackpot_viewer_items = new List<JackpotViewerItem>();

      if(JackpotViewerItem != null)
      {
        foreach (JackpotViewerItem _item in JackpotViewerItem)
        {
          _jackpot_viewer_items.Add(_item.Clone());
        }
      }

      return _jackpot_viewer_items;
    } // Clone

    /// <summary>
    /// Remove item from JackpotViewerItem list by JackpotId
    /// </summary>
    /// <returns></returns>
    public static void RemoveByJackpotId(this List<JackpotViewerItem> JackpotViewerItems, Int32 JackpotId)
    {
      foreach (JackpotViewerItem _item in JackpotViewerItems)
      {
        if ( _item.JackpotId == JackpotId)
        {
          JackpotViewerItems.Remove (_item);

          return;
        }
      }
    } // RemoveByJackpotId

    /// <summary>
    /// Remove item from JackpotViewerItem list by JackpotId
    /// </summary>
    /// <returns></returns>
    public static void RemoveByTerminalId(this List<JackpotViewerTerminalItem> JackpotViewerItems, Int32 TerminalId)
    {
      foreach (JackpotViewerTerminalItem _item in JackpotViewerItems)
      {
        if (_item.TerminalId == TerminalId)
        {
          JackpotViewerItems.Remove(_item);

          return;
        }
      }
    } // RemoveByJackpotId

    #endregion

    #region "JackpotViewerTerminal"

    /// <summary>
    /// Clone JackpotViewerItem list 
    /// </summary>
    /// <returns></returns>
    public static List<JackpotViewerTerminalItem> Clone(this List<JackpotViewerTerminalItem> JackpotViewerTerminalItem)
    {
      List<JackpotViewerTerminalItem> _jackpot_viewer_terminal_items = new List<JackpotViewerTerminalItem>();

      if (JackpotViewerTerminalItem != null)
      {
        foreach (JackpotViewerTerminalItem _item in JackpotViewerTerminalItem)
        {
          _jackpot_viewer_terminal_items.Add(_item.Clone());
        }
      }

      return _jackpot_viewer_terminal_items;
    } // Clone

    #endregion

    #region "JackpotAwardList"

    /// <summary>
    /// Clone JackpotAwards list 
    /// </summary>
    /// <returns></returns>
    public static List<JackpotAwardItem> Clone(this List<JackpotAwardItem> JackpotAwards)
    {
      List<JackpotAwardItem> _jackpot_awards_items = new List<JackpotAwardItem>();

      if (JackpotAwards != null)
      {
        foreach (JackpotAwardItem _item in JackpotAwards)
        {
          _jackpot_awards_items.Add(_item.Clone());
        }
      }

      return _jackpot_awards_items;
    } // Clone

    /// <summary>
    /// Read JackpotAwards list 
    /// </summary>
    /// <returns></returns>
    public static void Read(this List<JackpotAwardItemList> JackpotAwards)
    {
      if (JackpotAwards != null)
      {
        foreach (JackpotAwardItemList _item in JackpotAwards)
        {
          _item.Read(Convert.ToInt32(_item.AwardId));
        }
      }

    } // Read

    #endregion

    #region " Common "

    /// <summary>
    /// Convert int value (minutes) to datetime
    /// </summary>
    /// <param name="Minutes"></param>
    /// <returns></returns>
    public static DateTime HourInMinutesToDate(this Int32 Minutes)
    {
      return new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day).AddMinutes(Math.Max(Minutes, 0));
    } // HourInMinutesToDate

    #endregion " Common "

  }
}