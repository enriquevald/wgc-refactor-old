﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotContribution.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot Contribution Groups
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 30-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAR-2017 JBP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotContribution : JackpotBase
  {
    #region " Constants "

    // Private constants
    private const Int32 DB_ID                   = 0;
    private const Int32 DB_JACKPOT_ID           = 1;
    private const Int32 DB_NAME                 = 2;
    private const Int32 DB_TYPE                 = 3;
    private const Int32 DB_FIXED_CONTRIBUTION   = 4;
    private const Int32 DB_MONDAY               = 5;
    private const Int32 DB_TUESDAY              = 6;
    private const Int32 DB_WEDNESDAY            = 7;
    private const Int32 DB_THURSDAY             = 8;
    private const Int32 DB_FRIDAY               = 9;
    private const Int32 DB_SATURDAY             = 10;
    private const Int32 DB_SUNDAY               = 11;
    private const Int32 DB_TERMINALS            = 12;
    private const Int32 DB_LAST_UPDATE          = 13;

    // Public constants
    public const String DEFAULT_NAME_VALUE      = "1";
    public const ContributionGroupState DEFAULT_CONTRIBUTION_GROUP_STATE = ContributionGroupState.NotInUse;

    #endregion

    #region " Enums "

    private enum TYPE_JACKPOT_CONTRIBUTION_GET
    {
      ID          = 0,
      JACKPOT_ID  = 1,
      ALL_ENABLED = 2
    }

    #endregion

    #region " Properties "

    public Int32 JackpotId { get; set; }
    public String Name { get; set; }
    public ContributionType Type { get; set; }
    public Decimal FixedContribution { get; set; }
    public Decimal Monday { get; set; }
    public Decimal Tuesday { get; set; }
    public Decimal Wednesday { get; set; }
    public Decimal Thursday { get; set; }
    public Decimal Friday { get; set; }
    public Decimal Saturday { get; set; }
    public Decimal Sunday { get; set; }
    public String Terminals { get; set; }
    public ContributionGroupState ContributionState { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    #region " Constructors " 

    /// <summary>
    /// Default Constructor
    /// </summary>
    public JackpotContribution()
    {
      this.Id                 = DEFAULT_ID_VALUE;
      this.Name               = DEFAULT_NAME_VALUE;
      this.Type               = ContributionType.Fixed;
      this.FixedContribution  = DEFAULT_DECIMAL_VALUE;
      this.Monday             = DEFAULT_DECIMAL_VALUE;
      this.Tuesday            = DEFAULT_DECIMAL_VALUE;
      this.Wednesday          = DEFAULT_DECIMAL_VALUE;
      this.Thursday           = DEFAULT_DECIMAL_VALUE;
      this.Friday             = DEFAULT_DECIMAL_VALUE;
      this.Saturday           = DEFAULT_DECIMAL_VALUE;
      this.Sunday             = DEFAULT_DECIMAL_VALUE;
      this.Terminals          = String.Empty;
      this.LastUpdate         = WGDB.MinDate;
      this.LastModification   = WGDB.MinDate;

      this.ContributionState  = DEFAULT_CONTRIBUTION_GROUP_STATE;
    } // JackpotContribution
    public JackpotContribution(String Name)
    {
      this.Id         = DEFAULT_ID_VALUE;
      this.JackpotId  = DEFAULT_ID_VALUE;
      this.Name       = Name;
      this.LastUpdate = WGDB.MinDate;
      this.LastModification = WGDB.MinDate;
    } // JackpotContribution

    /// <summary>
    /// Constructor's by JackpotContributionId
    /// </summary>
    /// <param name="JackpotContributionId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotContribution(Int32 JackpotContributionId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        this.DB_GetJackpotContribution_Internal(JackpotContributionId, _db_trx.SqlTransaction);
      }
    } // JackpotContribution
    public JackpotContribution(Int32 JackpotContributionId, SqlTransaction SqlTrx)
    {
      this.DB_GetJackpotContribution_Internal(JackpotContributionId, SqlTrx);
    } // JackpotContribution

    #endregion 

    /// <summary>
    /// Get all jackpots contribution by Jackpot Id
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="JackpotContributionList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetAllJackpotContributionByJackpotId(Int32 JackpotId, out List<JackpotContribution> JackpotContributionList, SqlTransaction SqlTrx)
    {
      return DB_GetAllJackpotContribution_Internal(JackpotId, out JackpotContributionList, SqlTrx);
    } // GetAllJackpotContributionByJackpotId

    /// <summary>
    /// Get contributions of all jackpots enabled
    /// </summary>
    /// <param name="JackpotContributionList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetAllJackpotContributionEnabled(out List<JackpotContribution> JackpotContributionList, SqlTransaction SqlTrx)
    {
      return DB_GetEnabledJackpotContribution_Internal(out JackpotContributionList, SqlTrx);
    } // GetAllJackpotContributionEnabled

    /// <summary>
    /// Delete Jackpot contribution
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Delete(SqlTransaction SqlTrx)
    {
      return this.DB_Delete(SqlTrx);
    } // Delete
    
    /// <summary>
    /// Get Contribution Percentatge by type and day.
    /// </summary>
    /// <returns>Percentatge</returns>
    public Decimal GetContributionPercentatge()
    {
      DayOfWeek _day_of_week;

      if (Type == Jackpot.ContributionType.Fixed)
      {
        return this.FixedContribution;
      }

      if (Type == Jackpot.ContributionType.Weekly)
      {
        _day_of_week = Misc.TodayOpening().DayOfWeek;

        switch (_day_of_week)
        {
          case DayOfWeek.Monday:           
            return this.Monday;

          case DayOfWeek.Tuesday:            
            return this.Tuesday; 

          case DayOfWeek.Wednesday:            
            return this.Wednesday;

          case DayOfWeek.Thursday:            
            return this.Thursday;

          case DayOfWeek.Friday:            
            return this.Friday;

          case DayOfWeek.Saturday:            
            return this.Saturday;

          case DayOfWeek.Sunday:            
            return this.Sunday;
        }
      }
        
      Log.Warning("JackpotContyribution.GetContributionPercentatge Contribution not defined. Type = "+ (Int32)Type);

      return DEFAULT_INT_VALUE;
    } // GetContributionPercentatge
    
    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {

      this.JackpotId = DEFAULT_ID_VALUE;
    } // Reset

    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific JackpotContribution 
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotContribution_Internal(Int32 Id, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotContributionQuery_Select(TYPE_JACKPOT_CONTRIBUTION_GET.ID), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Id;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!DB_JackpotContributionDataMapper(_sql_reader))
              {
                return false;
              }

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContribution.DB_GetJackpotContribution_Internal -> JackpotContributionId: {0}", Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotContribution_Internal

    /// <summary>
    /// Get all JackpotContribution from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetAllJackpotContribution_Internal(Int32 JackpotId, out List<JackpotContribution> JackpotContributionList, SqlTransaction SqlTrx)
    {
      JackpotContribution _contribution;

      JackpotContributionList = new List<JackpotContribution>();
      
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotContributionQuery_Select(TYPE_JACKPOT_CONTRIBUTION_GET.JACKPOT_ID), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _contribution = JackpotContributionMappFromSqlReader(_sql_reader);

              if (_contribution != null)
              {
                JackpotContributionList.Add(_contribution);
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContribution.DB_GetAllJackpotContribution_Internal -> JackpotId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetAllJackpotContribution_Internal
    
    /// <summary>
    /// Get all JackpotContribution from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetEnabledJackpotContribution_Internal(out List<JackpotContribution> JackpotContributionList, SqlTransaction SqlTrx)
    {
      JackpotContribution _contribution;

      JackpotContributionList = new List<JackpotContribution>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotContributionQuery_Select(TYPE_JACKPOT_CONTRIBUTION_GET.ALL_ENABLED), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _contribution = JackpotContributionMappFromSqlReader(_sql_reader);

              if (_contribution != null)
              {
                JackpotContributionList.Add(_contribution);
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContribution.DB_GetAllJackpotContribution_Internal -> JackpotId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetAllJackpotContribution_Internal

    /// <summary>
    /// Mapp jackpot contribution from sql reader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private JackpotContribution JackpotContributionMappFromSqlReader(SqlDataReader SqlReader)
    {
      JackpotContribution _jackpot_contribution;

      _jackpot_contribution = new JackpotContribution();

      if (!DB_JackpotContributionDataMapper(_jackpot_contribution, SqlReader))
      {
        Log.Error(String.Format("JackpotContribution.DB_GetAllJackpotContribution_Internal -> Error on load JackpotContributionId: {0}", SqlReader.GetInt32(DB_ID)));
      }

      return _jackpot_contribution;
    } // JackpotContributionMappFromSqlReader

    /// <summary>
    /// Delete Jackpot in Database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_Delete(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" DELETE FROM   JACKPOTS_SETTINGS_CONTRIBUTION ");
        _sb.AppendLine("       WHERE   JSC_ID                 = @pId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = this.Id;
          
          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotContribution.DB_Delete -> JackpotContributionId: {0}", this.Id));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContribution.DB_Delete -> JackpotContributionId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Delete

    /// <summary>
    /// Load Jackpot Contribution Group data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotContributionDataMapper(SqlDataReader SqlReader)
    {
      return DB_JackpotContributionDataMapper(this, SqlReader);
    }
    private Boolean DB_JackpotContributionDataMapper(JackpotContribution JackpotContribution, SqlDataReader SqlReader)
    {
      try
      {
        JackpotContribution.Id                 = SqlReader.GetInt32(DB_ID);
        JackpotContribution.JackpotId          = SqlReader.GetInt32(DB_JACKPOT_ID);
        JackpotContribution.Name               = SqlReader.IsDBNull(DB_NAME)               ? String.Empty           : SqlReader.GetString(DB_NAME);
        JackpotContribution.Type               = SqlReader.IsDBNull(DB_TYPE)               ? ContributionType.Fixed : (ContributionType)SqlReader.GetInt32(DB_TYPE);
        JackpotContribution.FixedContribution  = SqlReader.IsDBNull(DB_FIXED_CONTRIBUTION) ? DEFAULT_DECIMAL_VALUE  : SqlReader.GetDecimal(DB_FIXED_CONTRIBUTION);
        JackpotContribution.Monday             = SqlReader.IsDBNull(DB_MONDAY)             ? DEFAULT_DECIMAL_VALUE  : SqlReader.GetDecimal(DB_MONDAY);
        JackpotContribution.Tuesday            = SqlReader.IsDBNull(DB_TUESDAY)            ? DEFAULT_DECIMAL_VALUE  : SqlReader.GetDecimal(DB_TUESDAY);
        JackpotContribution.Wednesday          = SqlReader.IsDBNull(DB_WEDNESDAY)          ? DEFAULT_DECIMAL_VALUE  : SqlReader.GetDecimal(DB_WEDNESDAY);
        JackpotContribution.Thursday           = SqlReader.IsDBNull(DB_THURSDAY)           ? DEFAULT_DECIMAL_VALUE  : SqlReader.GetDecimal(DB_THURSDAY);
        JackpotContribution.Friday             = SqlReader.IsDBNull(DB_FRIDAY)             ? DEFAULT_DECIMAL_VALUE  : SqlReader.GetDecimal(DB_FRIDAY);
        JackpotContribution.Saturday           = SqlReader.IsDBNull(DB_SATURDAY)           ? DEFAULT_DECIMAL_VALUE  : SqlReader.GetDecimal(DB_SATURDAY);
        JackpotContribution.Sunday             = SqlReader.IsDBNull(DB_SUNDAY)             ? DEFAULT_DECIMAL_VALUE  : SqlReader.GetDecimal(DB_SUNDAY);
        JackpotContribution.Terminals          = SqlReader.IsDBNull(DB_TERMINALS)          ? String.Empty           : SqlReader.GetString(DB_TERMINALS);
        JackpotContribution.LastUpdate         = SqlReader.IsDBNull(DB_LAST_UPDATE)        ? WGDB.MinDate           : SqlReader.GetDateTime(DB_LAST_UPDATE);
        JackpotContribution.ContributionState  = ContributionGroupState.InUse;
        
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotContributionDataMapper");
        Log.Exception(_ex);
      }

      JackpotContribution = null;

      return false;
      
    } // DB_JackpotContributionDataMapper 

    /// <summary>
    /// Gets Jackpot Contribution Group select query
    /// </summary>
    /// <param name="Type"></param>
    /// <param name="Parameter"></param>
    /// <returns></returns>
    private String DB_JackpotContributionQuery_Select(TYPE_JACKPOT_CONTRIBUTION_GET Type)
    {
      StringBuilder _sb;

      //Parameter = String.Empty;
      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   JSC_ID                         "); // 0
      _sb.AppendLine("           , JSC_JACKPOT_ID                 "); // 1
      _sb.AppendLine("           , JSC_NAME                       "); // 2
      _sb.AppendLine("           , JSC_TYPE                       "); // 3
      _sb.AppendLine("           , JSC_FIXED_CONTRIBUTION         "); // 4
      _sb.AppendLine("           , JSC_MONDAY                     "); // 5
      _sb.AppendLine("           , JSC_TUESDAY                    "); // 6
      _sb.AppendLine("           , JSC_WEDNESDAY                  "); // 7
      _sb.AppendLine("           , JSC_THURSDAY                   "); // 8
      _sb.AppendLine("           , JSC_FRIDAY                     "); // 9
      _sb.AppendLine("           , JSC_SATURDAY                   "); // 10
      _sb.AppendLine("           , JSC_SUNDAY                     "); // 11
      _sb.AppendLine("           , JSC_TERMINALS                  "); // 12
      _sb.AppendLine("           , JSC_LAST_UPDATE                "); // 13
      _sb.AppendLine("      FROM   JACKPOTS_SETTINGS_CONTRIBUTION ");
      _sb.AppendLine(GetJackpotContributionCondition(Type));
      _sb.AppendLine("  ORDER BY   JSC_NAME                       ");

      return _sb.ToString();

    } // DB_JackpotContributionQuery_Select

    /// <summary>
    /// Get where condition
    /// </summary>
    /// <param name="Type"></param>
    /// <returns></returns>
    private string GetJackpotContributionCondition(TYPE_JACKPOT_CONTRIBUTION_GET Type)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      switch (Type)
      {
        default:
        case TYPE_JACKPOT_CONTRIBUTION_GET.ID:
        {
          _sb.AppendLine("   WHERE   JSC_ID = @pId");
          break;
        }
        case TYPE_JACKPOT_CONTRIBUTION_GET.JACKPOT_ID:
        {
         _sb.AppendLine("   WHERE   JSC_JACKPOT_ID = @pJackpotId");
         break;
        }
        case TYPE_JACKPOT_CONTRIBUTION_GET.ALL_ENABLED:
        {
            _sb.AppendLine(" INNER JOIN   JACKPOTS_SETTINGS              ");
            _sb.AppendLine("         ON   JSC_JACKPOT_ID = JS_JACKPOT_ID ");
            _sb.AppendLine("        AND   JS_ENABLED = 1                 ");
          break;
        }     
      }

      return _sb.ToString();

    } // GetJackpotContributionCondition

    #endregion " Private Methods "

    #region " Internal Methods "

    
    /// <summary>
    /// Insert a specific Jackpot Contributions in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      SqlParameter _op;
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" INSERT INTO   JACKPOTS_SETTINGS_CONTRIBUTION ");
        _sb.AppendLine("             ( JSC_JACKPOT_ID                 ");
        _sb.AppendLine("             , JSC_NAME                       ");
        _sb.AppendLine("             , JSC_TYPE                       ");
        _sb.AppendLine("             , JSC_FIXED_CONTRIBUTION         ");
        _sb.AppendLine("             , JSC_MONDAY                     ");
        _sb.AppendLine("             , JSC_TUESDAY                    ");
        _sb.AppendLine("             , JSC_WEDNESDAY                  ");
        _sb.AppendLine("             , JSC_THURSDAY                   ");
        _sb.AppendLine("             , JSC_FRIDAY                     ");
        _sb.AppendLine("             , JSC_SATURDAY                   ");
        _sb.AppendLine("             , JSC_SUNDAY                     ");
        _sb.AppendLine("             , JSC_TERMINALS                  ");       
        _sb.AppendLine("             , JSC_LAST_UPDATE        )       ");           
        _sb.AppendLine("      VALUES                                  ");
        _sb.AppendLine("             ( @pJackpotId                    ");
        _sb.AppendLine("             , @pName                         ");
        _sb.AppendLine("             , @pType                         ");
        _sb.AppendLine("             , @pFixedContribution            ");
        _sb.AppendLine("             , @pMonday                       ");
        _sb.AppendLine("             , @pTuesday                      ");
        _sb.AppendLine("             , @pWednesday                    ");
        _sb.AppendLine("             , @pThursday                     ");
        _sb.AppendLine("             , @pFriday                       ");
        _sb.AppendLine("             , @pSaturday                     ");
        _sb.AppendLine("             , @pSunday                       ");
        _sb.AppendLine("             , @pTerminals                    ");
        _sb.AppendLine("             , @pLastUpdate            )      ");

        _sb.AppendLine(" SET     @pId = SCOPE_IDENTITY()              ");
        
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId"         , SqlDbType.Int     ).Value = this.JackpotId;
          _cmd.Parameters.Add("@pName"              , SqlDbType.NVarChar).Value = this.Name;
          _cmd.Parameters.Add("@pType"              , SqlDbType.Int     ).Value = this.Type;

          _cmd.Parameters.Add("@pFixedContribution" , SqlDbType.Money   ).Value = (this.FixedContribution == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.FixedContribution;
          _cmd.Parameters.Add("@pMonday"            , SqlDbType.Money   ).Value = (this.Monday            == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Monday;
          _cmd.Parameters.Add("@pTuesday"           , SqlDbType.Money   ).Value = (this.Tuesday           == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Tuesday;
          _cmd.Parameters.Add("@pWednesday"         , SqlDbType.Money   ).Value = (this.Wednesday         == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Wednesday;
          _cmd.Parameters.Add("@pThursday"          , SqlDbType.Money   ).Value = (this.Thursday          == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Thursday;
          _cmd.Parameters.Add("@pFriday"            , SqlDbType.Money   ).Value = (this.Friday            == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Friday;
          _cmd.Parameters.Add("@pSaturday"          , SqlDbType.Money   ).Value = (this.Saturday          == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Saturday;
          _cmd.Parameters.Add("@pSunday"            , SqlDbType.Money   ).Value = (this.Sunday            == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Sunday;
          _cmd.Parameters.Add("@pTerminals"         , SqlDbType.Xml     ).Value = (String.IsNullOrEmpty (this.Terminals))                    ? DBNull.Value : (Object)this.Terminals;

          // Last update (Always update in Save())
          this.LastUpdate = WGDB.Now;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;

          //Output parameter
          _op = new SqlParameter("@pId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotContribution.DB_Insert -> JackpotId: {0}", this.JackpotId));

            return false;
          }

          this.Id = (Int32)_op.Value;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContribution.DB_Insert -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update a specific Jackpot Contributions in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" UPDATE   JACKPOTS_SETTINGS_CONTRIBUTION               ");
        _sb.AppendLine("    SET                                                ");
        _sb.AppendLine("          JSC_JACKPOT_ID         = @pJackpotId         ");
        _sb.AppendLine("        , JSC_NAME               = @pName              ");
        _sb.AppendLine("        , JSC_TYPE               = @pType              ");
        _sb.AppendLine("        , JSC_FIXED_CONTRIBUTION = @pFixedContribution ");
        _sb.AppendLine("        , JSC_MONDAY             = @pMonday            ");
        _sb.AppendLine("        , JSC_TUESDAY            = @pTuesday           ");
        _sb.AppendLine("        , JSC_WEDNESDAY          = @pWednesday         ");
        _sb.AppendLine("        , JSC_THURSDAY           = @pThursday          ");
        _sb.AppendLine("        , JSC_FRIDAY             = @pFriday            ");
        _sb.AppendLine("        , JSC_SATURDAY           = @pSaturday          ");
        _sb.AppendLine("        , JSC_SUNDAY             = @pSunday            ");
        _sb.AppendLine("        , JSC_TERMINALS          = @pTerminals         ");
        _sb.AppendLine("        , JSC_LAST_UPDATE        = @pLastUpdate        ");
        _sb.AppendLine("  WHERE   JSC_ID                 = @pId                ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId"                , SqlDbType.Int     ).Value = this.Id;
          _cmd.Parameters.Add("@pJackpotId"         , SqlDbType.Int     ).Value = this.JackpotId;
          _cmd.Parameters.Add("@pName"              , SqlDbType.NVarChar).Value = this.Name;
          _cmd.Parameters.Add("@pType"              , SqlDbType.Int     ).Value = this.Type;

          _cmd.Parameters.Add("@pFixedContribution" , SqlDbType.Money   ).Value = (this.FixedContribution == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.FixedContribution;
          _cmd.Parameters.Add("@pMonday"            , SqlDbType.Money   ).Value = (this.Monday            == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Monday;
          _cmd.Parameters.Add("@pTuesday"           , SqlDbType.Money   ).Value = (this.Tuesday           == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Tuesday;
          _cmd.Parameters.Add("@pWednesday"         , SqlDbType.Money   ).Value = (this.Wednesday         == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Wednesday;
          _cmd.Parameters.Add("@pThursday"          , SqlDbType.Money   ).Value = (this.Thursday          == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Thursday;
          _cmd.Parameters.Add("@pFriday"            , SqlDbType.Money   ).Value = (this.Friday            == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Friday;
          _cmd.Parameters.Add("@pSaturday"          , SqlDbType.Money   ).Value = (this.Saturday          == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Saturday;
          _cmd.Parameters.Add("@pSunday"            , SqlDbType.Money   ).Value = (this.Sunday            == Jackpot.DEFAULT_DECIMAL_VALUE)  ? DBNull.Value : (Object)this.Sunday;
          _cmd.Parameters.Add("@pTerminals"         , SqlDbType.Xml     ).Value = (String.IsNullOrEmpty (this.Terminals))                    ? DBNull.Value : (Object)this.Terminals;

          // Last update
          this.LastUpdate = this.LastModification;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotContribution.DB_Update -> JackpotContributionId: {0}", this.Id));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContribution.DB_Update -> JackpotContributionId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Update

    /// <summary>
    /// Clone JackpotContribution
    /// </summary>
    /// <returns></returns>
    internal JackpotContribution Clone()
    {
      return new JackpotContribution()
      {
        Id                = this.Id,
        JackpotId         = this.JackpotId,
        Name              = this.Name,
        Type              = this.Type,
        FixedContribution = this.FixedContribution,
        Monday            = this.Monday,
        Tuesday           = this.Tuesday,
        Wednesday         = this.Wednesday,
        Thursday          = this.Thursday,
        Friday            = this.Friday,
        Saturday          = this.Saturday,
        Sunday            = this.Sunday,
        Terminals         = this.Terminals,
        LastUpdate        = this.LastUpdate,
        ContributionState = this.ContributionState
      };
    } // Clone

    #endregion 

  } // JackpotContribution
}
