﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardConfig.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Rubén Lama
// 
// CREATION DATE: 19-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-APR-2017 RLO    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardConfig : JackpotBase
  {
    #region " Properties "

    public JackpotAwardDays WeekDays { get; set; }
    public JackpotAwardFilter Filter { get; set; }

    public override Boolean IsConfigured
    {
      get { return (this.Filter.IsConfigured && this.WeekDays.IsConfigured); }
    }
    
    public override Boolean IsModified
    {
      get { return (this.Filter.IsModified || this.WeekDays.IsModified); }
    }

    public override DateTime LastUpdate
    {
      get { return Filter.LastUpdate; }
      set 
      { 
        this.Filter.LastUpdate   = value;
        this.WeekDays.LastUpdate = value;
      }
    }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by JackpotId
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotAwardConfig()
    {
      this.Id       = DEFAULT_ID_VALUE;
      this.WeekDays = new JackpotAwardDays();
      this.Filter   = new JackpotAwardFilter();

    } // JackpotAwardConfig

    // Read Jackpot contribution
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.DB_GetJackpotAwardConfig_Internal(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      return this.DB_GetJackpotAwardConfig_Internal(JackpotId, SqlTrx);
    } // Read

    /// <summary>
    /// Save Jackpot Award configuration
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public override Boolean Save(SqlTransaction SqlTrx)
    {
      this.WeekDays.JackpotId = this.Id;
      this.Filter.JackpotId   = this.Id;

      // Read Jackpot Award days
      if (!this.WeekDays.Save(SqlTrx))
      {
        return false;
      }

      // Read Jackpot Award filters
      if (!this.Filter.Save(SqlTrx))
      {
        return false;
      }

      return true;
    } // Save
    
    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.Filter.Reset();
      this.WeekDays.Reset();

    } // Reset
           
    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotAwardConfig_Internal(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      this.Id = JackpotId;

      this.WeekDays = new JackpotAwardDays();
      this.Filter   = new JackpotAwardFilter();

      // Read Jackpot Award days
      if (!this.WeekDays.Read(JackpotId, SqlTrx))
      {
        return false;
      }

      // Read Jackpot Award filters
      if (!this.Filter.Read(JackpotId, SqlTrx))
      {
        return false;
      }

      return true;

    } // DB_GetJackpotAwardConfig_Internal

    #endregion " Private Methods "

    #region " Internal Methods "

    /// <summary>
    /// Clone JackpotAwardConfig
    /// </summary>
    /// <returns></returns>
    internal JackpotAwardConfig Clone()
    {
      return new JackpotAwardConfig()
      {
        Id        = this.Id,
        Filter    = this.Filter.Clone(),
        WeekDays  = this.WeekDays.Clone()
      };
    } // Clone

    #endregion 

  } // JackpotAwardConfig
}
