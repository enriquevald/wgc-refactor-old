﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardFilterFlagsList.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot Contribution Groups List
// 
//        AUTHOR: Rubén Lama Ordóñez
// 
// CREATION DATE: 17-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-MAY-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardFilterFlagList : JackpotBase
  {

    #region " Properties "

    public Int32 JackpotId { get; set; }
    public List<JackpotAwardFilterFlag> Items { get; set; }

    public List<JackpotAwardFilterFlag> RelatedFlags
    {
      get { return this.FlagsToSave(); }
    }

    #endregion " Properties "

    #region " Public Methods "
     
    /// <summary>
    /// Default Constructor
    /// </summary>
    public JackpotAwardFilterFlagList()
    {
      this.Items = new List<JackpotAwardFilterFlag>();
    } // JackpotContributionList

    /// <summary>
    /// Flags to save and show in external form
    /// </summary>
    /// <param name="JackpotId"></param>    
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.Read(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      List<JackpotAwardFilterFlag> _flags;

      if (!new JackpotAwardFilterFlag().GetAllFlags(JackpotId, out _flags, SqlTrx))
      {
        return false;
      }

      this.JackpotId = JackpotId;
      this.Items     = _flags;

      return true;
    } // Read

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public List<JackpotAwardFilterFlag> FlagsToSave()
    {
      return this.FlagsToSaveInternal();
    } // FlagsToSave

    /// <summary>
    /// Reset all members id (For clonation)
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.JackpotId = Jackpot.DEFAULT_ID_VALUE;

      if (this.Items != null)
      {
        foreach (JackpotAwardFilterFlag _flags in this.Items)
        {
          _flags.Reset();
        }
      }
    } // Reset

    /// <summary>
    /// Mark all flags to deleted
    /// </summary>
    public void MarkAllToDeleted()
    {
      if (this.Items != null)
      {
        foreach (JackpotAwardFilterFlag _flag in this.Items)
        {
          _flag.FlagCount = 0;
          _flag.IsDeleted = true;
          _flag.LastModification = WGDB.Now;
        }
      }
    } // Reset 
  
    #endregion " Public Methods "

    #region " Internal Methods "

    /// <summary>
    /// Inset/Update an specific Jackpot contribution Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Save(SqlTransaction SqlTrx)
    {
      try
      {

        foreach (JackpotAwardFilterFlag _flag in this.RelatedFlags)
        {
          _flag.JackpotId = this.JackpotId;

          if (!_flag.Save(SqlTrx))
          {
            return false;
          }
          
          if (_flag.IsDeleted)
          {
            this.Items.Remove(_flag);
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardFilterFlagsList.DB_Save -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Save

    /// <summary>
    /// Clone JackpotAwardPrizeConfig
    /// </summary>
    /// <returns></returns>
    internal JackpotAwardFilterFlagList Clone()
    {
      return new JackpotAwardFilterFlagList()
      {
        JackpotId = this.JackpotId,
        Items     = this.Items.Clone()
      };
    } // Clone

    /// <summary>
    /// Flags To save internal
    /// </summary>
    /// <returns></returns>
    internal List<JackpotAwardFilterFlag> FlagsToSaveInternal()
    {
      List<JackpotAwardFilterFlag> _flags_to_save = new List<JackpotAwardFilterFlag>();

      foreach (JackpotAwardFilterFlag _flag in Items)
      {
        if (_flag.FlagCount > 0 || _flag.IsDeleted)
        {
          _flags_to_save.Add(_flag);
        }
      }

      return _flags_to_save;
    } // FlagsToSaveInternal

    #endregion

  } // JackpotContributionList
}
