﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardCustomerTier.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Ruben Lama
// 
// CREATION DATE: 28-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardCustomerTier : JackpotBase
  {
    #region " Constants "

    private const Int32 DB_ID                        = 0;
    private const Int32 DB_JACKPOT_ID                = 1;
    private const Int32 DB_AWARD_CUSTOMER_ENABLED    = 2;
    private const Int32 DB_AWARD_CUSTOMER_LEVEL_1    = 3;
    private const Int32 DB_AWARD_CUSTOMER_LEVEL_2    = 4;
    private const Int32 DB_AWARD_CUSTOMER_LEVEL_3    = 5;
    private const Int32 DB_AWARD_CUSTOMER_LEVEL_4    = 6;
    private const Int32 DB_AWARD_CUSTOMER_ANONYMOUS  = 7;
    private const Int32 DB_AWARD_CUSTOMER_VIP        = 8;
    private const Int32 DB_LAST_UPDATE               = 9;

    #endregion

    #region " Properties "

    public Int32 JackpotId { get; set; }
    public Boolean ByLevel { get; set; }
    public Boolean Level1 { get; set; }
    public Boolean Level2 { get; set; }
    public Boolean Level3 { get; set; }
    public Boolean Level4 { get; set; }
    public Boolean Anonymous { get; set; }
    public AwardCustomerVipType Vip { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by JackpotId
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotAwardCustomerTier()
    {
      this.Id               = DEFAULT_ID_VALUE;
      this.ByLevel          = false;
      this.Level1           = false;
      this.Level2           = false;
      this.Level3           = false;
      this.Level4           = false;
      this.Anonymous        = false;
      this.Vip              = AwardCustomerVipType.All;
      this.LastUpdate       = WGDB.MinDate;
      this.LastModification = WGDB.MinDate;
      
    } // JackpotAwardFilter

    // Read Jackpot Award Customer by Tier
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.Read(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      return this.DB_GetJackpotAwardCustomerTier_Internal(JackpotId, SqlTrx);
    } // Read
    
    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.JackpotId = DEFAULT_ID_VALUE;
    } // Reset

    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotAwardCustomerTier_Internal(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotAwardCustomerTierQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!DB_JackpotAwardCustomerTierDataMapper(_sql_reader))
              {
                return false;
              }

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardCustomerTier.DB_GetJackpotAwardCustomerTier_Internal -> JackpotId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotAwardCustomerTier_Internal

    /// <summary>
    /// Load Jackpot Award Customer by Tier data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotAwardCustomerTierDataMapper(SqlDataReader SqlReader)
    {
      try
      {
        this.Id         = SqlReader.GetInt32(DB_ID);
        this.JackpotId  = SqlReader.GetInt32(DB_JACKPOT_ID);
        this.ByLevel    = SqlReader.GetBoolean(DB_AWARD_CUSTOMER_ENABLED);
        this.Level1     = SqlReader.GetBoolean(DB_AWARD_CUSTOMER_LEVEL_1);
        this.Level2     = SqlReader.GetBoolean(DB_AWARD_CUSTOMER_LEVEL_2);
        this.Level3     = SqlReader.GetBoolean(DB_AWARD_CUSTOMER_LEVEL_3);
        this.Level4     = SqlReader.GetBoolean(DB_AWARD_CUSTOMER_LEVEL_4);
        this.Anonymous  = SqlReader.GetBoolean(DB_AWARD_CUSTOMER_ANONYMOUS);
        this.Vip        = (Jackpot.AwardCustomerVipType)SqlReader.GetInt32(DB_AWARD_CUSTOMER_VIP);
        this.LastUpdate = SqlReader.IsDBNull(DB_LAST_UPDATE) ? WGDB.MinDate : SqlReader.GetDateTime(DB_LAST_UPDATE);
        
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotAwardCustomerTierDataMapper");
        Log.Exception(_ex);
      }

      return false;
      
    } // DB_JackpotAwardCustomerTierDataMapper 
   
    /// <summary>
    /// Gets Jackpot Award Customer by Tier select query
    /// </summary>
    /// <returns></returns>
    private String DB_JackpotAwardCustomerTierQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   JCT_ID                           "); // 0
      _sb.AppendLine("           , JCT_JACKPOT_ID                   "); // 1
      _sb.AppendLine("           , JCT_BY_LEVEL_ENABLED             "); // 2
      _sb.AppendLine("           , JCT_LEVEL_1                      "); // 3
      _sb.AppendLine("           , JCT_LEVEL_2                      "); // 4
      _sb.AppendLine("           , JCT_LEVEL_3                      "); // 5
      _sb.AppendLine("           , JCT_LEVEL_4                      "); // 6
      _sb.AppendLine("           , JCT_ANONYMOUS                    "); // 7
      _sb.AppendLine("           , JCT_VIP                          "); // 8
      _sb.AppendLine("           , JCT_LAST_UPDATE                  "); // 9
      _sb.AppendLine("      FROM   JACKPOTS_SETTINGS_CUSTOMER_TIER  ");
      _sb.AppendLine("     WHERE   JCT_JACKPOT_ID = @pJackpotId     ");

      return _sb.ToString();

    } // DB_JackpotAwardCustomerTierQuery_Select  
    
    #endregion " Private Methods "

    #region " Internal Methods "

    /// <summary>
    /// Insert an specific Jackpot Award Customer by Tier in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      SqlParameter _op;
      StringBuilder _sb;
   
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   JACKPOTS_SETTINGS_CUSTOMER_TIER  ");
        _sb.AppendLine("             ( JCT_JACKPOT_ID                   ");
        _sb.AppendLine("             , JCT_BY_LEVEL_ENABLED             ");
        _sb.AppendLine("             , JCT_LEVEL_1                      ");
        _sb.AppendLine("             , JCT_LEVEL_2                      ");
        _sb.AppendLine("             , JCT_LEVEL_3                      ");
        _sb.AppendLine("             , JCT_LEVEL_4                      ");
        _sb.AppendLine("             , JCT_ANONYMOUS                    ");
        _sb.AppendLine("             , JCT_VIP                          ");
        _sb.AppendLine("             , JCT_LAST_UPDATE      )           ");
        _sb.AppendLine("      VALUES                                    ");
        _sb.AppendLine("             ( @pJackpotId                      ");
        _sb.AppendLine("             , @pByLevel                        ");
        _sb.AppendLine("             , @pLevel1                         ");
        _sb.AppendLine("             , @pLevel2                         ");
        _sb.AppendLine("             , @pLevel3                         ");
        _sb.AppendLine("             , @pLevel4                         ");
        _sb.AppendLine("             , @pAnonymous                      ");
        _sb.AppendLine("             , @pVip                            ");
        _sb.AppendLine("             , @pLastUpdate         )           ");

        _sb.AppendLine(" SET     @pId = SCOPE_IDENTITY()                ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId" , SqlDbType.Int).Value = this.JackpotId;
          _cmd.Parameters.Add("@pByLevel"   , SqlDbType.Int).Value = this.ByLevel;
          _cmd.Parameters.Add("@pLevel1"    , SqlDbType.Int).Value = this.Level1;
          _cmd.Parameters.Add("@pLevel2"    , SqlDbType.Int).Value = this.Level2;
          _cmd.Parameters.Add("@pLevel3"    , SqlDbType.Int).Value = this.Level3;
          _cmd.Parameters.Add("@pLevel4"    , SqlDbType.Int).Value = this.Level4;
          _cmd.Parameters.Add("@pAnonymous" , SqlDbType.Int).Value = this.Anonymous;
          _cmd.Parameters.Add("@pVip"       , SqlDbType.Int).Value = this.Vip;

          // Last update
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = (this.LastUpdate == WGDB.MinDate) ? DBNull.Value : (Object)this.LastUpdate;

          //Output parameter
          _op = new SqlParameter("@pId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);
          
          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardCustomerTier.DB_Insert -> JackpotId: {0}", this.JackpotId));

            return false;
          }

          this.Id = (Int32)_op.Value;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardCustomerTier.DB_Insert -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Jackpot Award Customer by Tier in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   JACKPOTS_SETTINGS_CUSTOMER_TIER       ");
        _sb.AppendLine("    SET   JCT_JACKPOT_ID        = @pJackpotId   ");
        _sb.AppendLine("        , JCT_BY_LEVEL_ENABLED  = @pByLevel     ");
        _sb.AppendLine("        , JCT_LEVEL_1           = @pLevel1      ");
        _sb.AppendLine("        , JCT_LEVEL_2           = @pLevel2      ");
        _sb.AppendLine("        , JCT_LEVEL_3           = @pLevel3      ");
        _sb.AppendLine("        , JCT_LEVEL_4           = @pLevel4      ");
        _sb.AppendLine("        , JCT_ANONYMOUS         = @pAnonymous   ");
        _sb.AppendLine("        , JCT_VIP               = @pVip         ");
        _sb.AppendLine("        , JCT_LAST_UPDATE       = @pLastUpdate  ");
        _sb.AppendLine("  WHERE   JCT_ID                = @pId          ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          
          _cmd.Parameters.Add("@pId"        , SqlDbType.Int).Value = this.Id;
          _cmd.Parameters.Add("@pJackpotId" , SqlDbType.Int).Value = this.JackpotId;
          _cmd.Parameters.Add("@pByLevel"   , SqlDbType.Bit).Value = this.ByLevel;
          _cmd.Parameters.Add("@pLevel1"    , SqlDbType.Bit).Value = this.Level1;
          _cmd.Parameters.Add("@pLevel2"    , SqlDbType.Bit).Value = this.Level2;
          _cmd.Parameters.Add("@pLevel3"    , SqlDbType.Bit).Value = this.Level3;
          _cmd.Parameters.Add("@pLevel4"    , SqlDbType.Bit).Value = this.Level4;
          _cmd.Parameters.Add("@pAnonymous" , SqlDbType.Bit).Value = this.Anonymous;
          _cmd.Parameters.Add("@pVip"       , SqlDbType.Int).Value = this.Vip;

          // Last update
          this.LastUpdate = this.LastModification;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;
          
          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardCustomerTier.DB_Update -> JackpotAwardCustomerTierId: {0}", this.Id));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardCustomerTier.DB_Update -> JackpotAwardCustomerTierId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Update

    /// <summary>
    /// Clone JackpotAwardCustomerTier
    /// </summary>
    /// <returns></returns>
    internal JackpotAwardCustomerTier Clone()
    {
      return new JackpotAwardCustomerTier()
      {
        Id          = this.Id,
        JackpotId   = this.JackpotId,
        ByLevel     = this.ByLevel,    
        Level1      = this.Level1,
        Level2      = this.Level2,
        Level3      = this.Level3,
        Level4      = this.Level4,
        Anonymous   = this.Anonymous,
        Vip         = this.Vip,
        LastUpdate  = this.LastUpdate
      };
    } // Clone

    #endregion 

  } // JackpotAwardCustomerByTier
}
