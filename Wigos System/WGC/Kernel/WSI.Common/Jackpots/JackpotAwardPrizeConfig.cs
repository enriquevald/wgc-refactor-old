﻿// 
//   DESCRIPTION: Jackpot award prize
// 
//        AUTHOR: Carlos Cáceres González
// 
// CREATION DATE: 02-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-APR-2017 CCG    First release.
//--

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardPrizeConfig : JackpotBase
  {

    #region " Properties "

    public Int32 JackpotId { get; set; }
    public JackpotAwardCustomerTier CustomerTier { get; set; }
    public JackpotAwardPrizeSharing PrizeSharing { get; set; }
    public JackpotAwardHappyHour HappyHour { get; set; }

    public override Boolean IsConfigured
    {
      get { return (this.HappyHour.IsConfigured && this.PrizeSharing.IsConfigured && this.CustomerTier.IsConfigured); }
    }

    public override Boolean IsModified
    {
      get { return (this.HappyHour.IsModified || this.PrizeSharing.IsModified || this.CustomerTier.IsModified); }
    }

    public override DateTime LastUpdate
    {
      get { return PrizeSharing.LastUpdate; }
      set
      {
        this.PrizeSharing.LastUpdate  = value;
        this.HappyHour.LastUpdate     = value;
        this.CustomerTier.LastUpdate  = value;
      }
    }
    
    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by JackpotId
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotAwardPrizeConfig()
    {
      this.Id           = DEFAULT_ID_VALUE;
      this.JackpotId    = DEFAULT_ID_VALUE; 
      this.CustomerTier = new JackpotAwardCustomerTier();
      this.PrizeSharing = new JackpotAwardPrizeSharing();
      this.HappyHour    = new JackpotAwardHappyHour();
    } // JackpotAwardConfig

    // Read Jackpot contribution
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.DB_GetJackpotAwardPrize_Internal(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      return this.DB_GetJackpotAwardPrize_Internal(JackpotId, SqlTrx);
    } // Read

    public override Boolean Save(SqlTransaction SqlTrx)
    {
      this.CustomerTier.JackpotId  = this.JackpotId;
      this.PrizeSharing.JackpotId  = this.JackpotId;
      this.HappyHour.JackpotId     = this.JackpotId;

      // Save Customer Tier
      if (!this.CustomerTier.Save(SqlTrx))
      {
        return false;
      }

      // Save prize sharing
      if (!this.PrizeSharing.Save(SqlTrx))
      {
        return false;
      }

      // Save Happy hour
      if (!this.HappyHour.Save(SqlTrx))
      {
        return false;
      }

      return true;
    } // Save

    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {

      this.JackpotId = Jackpot.DEFAULT_ID_VALUE;

      this.CustomerTier.Reset();
      this.HappyHour.Reset();
      this.PrizeSharing.Reset();
    } // Reset
           
    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotAwardPrize_Internal(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      this.CustomerTier  = new JackpotAwardCustomerTier();
      this.PrizeSharing  = new JackpotAwardPrizeSharing();
      this.HappyHour     = new JackpotAwardHappyHour();

      this.JackpotId     = JackpotId;

      // Read Jackpot Customer Tier
      if (!this.CustomerTier.Read(JackpotId, SqlTrx))
      {
        return false;
      }

      // Read Jackpot Award prize sharing
      if (!this.PrizeSharing.Read(JackpotId, SqlTrx))
      {
        return false;
      }

      // Read Jackpot Award happy hour
      if (!this.HappyHour.Read(JackpotId, SqlTrx))
      {
        return false;
      }

      return true;

    } // DB_GetJackpotAwardConfig_Internal

    #endregion " Private Methods "

    #region " Internal Methods "
    
    /// <summary>
    /// Clone JackpotAwardPrizeConfig
    /// </summary>
    /// <returns></returns>
    internal JackpotAwardPrizeConfig Clone()
    {
      return new JackpotAwardPrizeConfig()
      {
        JackpotId     = this.JackpotId,
        CustomerTier  = this.CustomerTier.Clone(),
        PrizeSharing  = this.PrizeSharing.Clone(),
        HappyHour     = this.HappyHour.Clone()       
      };
    } // Clone
    
    #endregion 

  } // JackpotAwardPrizeConfig
}
