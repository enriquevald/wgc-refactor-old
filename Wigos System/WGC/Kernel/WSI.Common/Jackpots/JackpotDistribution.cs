﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotDistribution.cs
// 
//   DESCRIPTION: Jackpots distribution
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 01-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUN-2017 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotDistribution
  {

    #region " Public methods "

    /// <summary>
    /// Distribute amounts
    /// </summary>
    /// <returns></returns>
    public static ThreadStatus Distribution()
    {
      List<Jackpot> _jackpot_list;
      ThreadStatus _status_operations;
      ThreadStatus _status_terminals;

      // Distribute amount operations
      _status_operations = DistributionAmountOperations(out _jackpot_list);

      // Distribute amount terminals
      _status_terminals = DistributionAmountTerminals(ref _jackpot_list);

      // Return status
      if (_status_operations == ThreadStatus.ERROR || _status_terminals == ThreadStatus.ERROR)
      {
        return ThreadStatus.ERROR;
      }
      else if (_status_operations == ThreadStatus.CHECK || _status_terminals == ThreadStatus.CHECK)
      {
        return ThreadStatus.CHECK;
      }
      else
      {
        return ThreadStatus.IDLE;
      }
    } // ThreadDistribution

    #endregion " Public methods "

    #region " Private methods "

    /// <summary>
    /// Distribute amount operations
    /// </summary>
    /// <param name="JackpotList"></param>
    /// <returns></returns>
    private static ThreadStatus DistributionAmountOperations(out List<Jackpot> JackpotList)
    {
      List<JackpotAmountOperations> _amount_list;

      JackpotList = null;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!JackpotAmountOperations.DB_GetOperationAmounts(out _amount_list, _db_trx.SqlTransaction))
        {
          Log.Error("JackpotDistribution.DistributionAmountOperations.DB_GetOperationAmounts - Error reading operation amounts.");

          return ThreadStatus.ERROR;
        }

        if (_amount_list.Count == 0)
        {
          return ThreadStatus.IDLE;
        }

        if (!JackpotBusinessLogic.GetAllJackpots(Jackpot.JackpotFilterType.All, _db_trx.SqlTransaction, out JackpotList))
        {
          Log.Error("JackpotDistribution.DistributionAmountOperations.GetAllJackpots - Error reading jackpot configuration.");

          return ThreadStatus.ERROR;
        }
      }

      foreach (JackpotAmountOperations _operation in _amount_list)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          switch (_operation.Type)
          {
            case JackpotAmountOperations.OperationType.Add:
              if (!OperationAdd(JackpotList, _operation, _db_trx.SqlTransaction))
              {
                return ThreadStatus.ERROR;
              }
              break;

            case JackpotAmountOperations.OperationType.Reset:
              if (!OperationReset(JackpotList, _operation, _db_trx.SqlTransaction))
              {
                return ThreadStatus.ERROR;
              }
              break;

            case JackpotAmountOperations.OperationType.Award:
              if (!OperationAward(JackpotList, _operation, _db_trx.SqlTransaction))
              {
                return ThreadStatus.ERROR;
              }
              break;

            case JackpotAmountOperations.OperationType.Recalculate:
              if (!OperationRecalculate(JackpotList, _operation, _db_trx.SqlTransaction))
              {
                return ThreadStatus.ERROR;
              }
              break;

          }

          if (!_operation.Save(_db_trx.SqlTransaction))
          {
            return ThreadStatus.ERROR;
          }

          _db_trx.Commit();
        }
      }

      return ThreadStatus.IDLE;
    } // DistributionAmountOperations

    /// <summary>
    /// Distribute amount terminals by percentatge
    /// </summary>
    /// <param name="JackpotList"></param>
    /// <returns></returns>
    private static ThreadStatus DistributionAmountTerminals(ref List<Jackpot> JackpotList)
    {
      DataTable _dt_processing_terminals;
      DataTable _dt_contributions;
      ThreadStatus _status;
      JackpotAmountTerminal _amount_terminal;
      JackpotContributionList _contribution_list;

      _contribution_list = new JackpotContributionList();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!JackpotAmountTerminal.DB_ReadAmountTerminals(out _dt_processing_terminals, _db_trx.SqlTransaction))
        {
          Log.Error("JackpotDistribution.DistributionAmountTerminals.DB_ReadAmountTerminals - Error reading terminal amounts.");

          return ThreadStatus.ERROR;
        }

        if (_dt_processing_terminals.Rows.Count == 0)
        {
          return ThreadStatus.IDLE;
        }

        if (!_contribution_list.GetJackpotContribution(_db_trx.SqlTransaction))
        {
          Log.Error("JackpotDistribution.DistributionAmountTerminals.GetJackpotContribution - Error reading jackpot contribution configuration.");

          return ThreadStatus.ERROR;
        }

        if (JackpotList == null)
        {
          if (!JackpotBusinessLogic.GetAllJackpots(Jackpot.JackpotFilterType.Enabled, _db_trx.SqlTransaction, out JackpotList))
          {
            Log.Error("JackpotDistribution.DistributionAmountTerminals.GetAllJackpots - Error reading jackpot configuration.");

            return ThreadStatus.ERROR;
          }
        }
        else
        {
          JackpotList = JackpotList.FindAll(_jackpot => _jackpot.Enabled);
        }
      }

      _status = ThreadStatus.CHECK;

      foreach (DataRow _dr_terminals in _dt_processing_terminals.Rows)
      {
        _amount_terminal = new JackpotAmountTerminal()
        {
          TerminalId = (Int32)_dr_terminals["JAT_TERMINAL_ID"],
          Amount = (Decimal)_dr_terminals["JAT_AMOUNT"]
        };

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (_contribution_list.Items.Count > 0)
          {
            if (!JackpotBusinessLogic.GetTerminalsByElementType(_amount_terminal.TerminalId, 0, EXPLOIT_ELEMENT_TYPE.JACKPOT_CONTRIBUTIONS, out _dt_contributions, _db_trx.SqlTransaction))
            {
              Log.Error("JackpotDistribution.DistributionAmountTerminals.GetTerminalsByElementType - Error reading. Terminal = " + _amount_terminal.TerminalId);
              _status = ThreadStatus.ERROR;
              _db_trx.Rollback();

              continue;
            }

            if (_dt_contributions.Rows.Count > 0)
            {
              if (!TerminalAmountToMeter(_amount_terminal, _dt_contributions, _contribution_list, JackpotList, _db_trx.SqlTransaction))
              {
                Log.Error("JackpotDistribution.DistributionAmountTerminals.TerminalAmountToMeter - Error reading. Terminal = " + _amount_terminal.TerminalId);
                _status = ThreadStatus.ERROR;
                _db_trx.Rollback();

                continue;
              }
            }
          }

          if (!_amount_terminal.Subtract(_db_trx.SqlTransaction))
          {
            Log.Error("JackpotDistribution.DistributionAmountTerminals._amount_terminal.Subtract - Error updating jackpots_amount_terminal. Terminal_ID = " + _amount_terminal.TerminalId);
            _status = ThreadStatus.ERROR;
            _db_trx.Rollback();

            continue;
          }

          _db_trx.Commit();
        }
      }

      return _status;
    } // DistributionAmountTerminals

    /// <summary>
    /// Add extra money
    /// </summary>
    /// <param name="JackpotList"></param>
    /// <param name="_operation"></param>
    /// <returns></returns>
    private static Boolean OperationAdd(List<Jackpot> JackpotList, JackpotAmountOperations _operation, SqlTransaction SqlTrx)
    {
      Jackpot _jackpot;

      try
      {
        _jackpot = JackpotList.Find(x => x.Id == _operation.JackpotId);

        if (_jackpot.Enabled)
        {
          // Jackpot Breakdown to meters.
          _jackpot.ContributionMeters.Jackpot = _jackpot;

          switch (_operation.JackpotType)
          {
            case Jackpot.JackpotType.Main:
              _jackpot.ContributionMeters.ContributionMainAmount = _operation.Amount;
              _jackpot.ContributionMeters.CompensationAmount = -_operation.Amount;
              break;

            case Jackpot.JackpotType.PrizeSharing:
              _jackpot.ContributionMeters.ContributionSharedAmount = _operation.Amount;
              _jackpot.ContributionMeters.CompensationAmount = -_operation.Amount;
              break;

            case Jackpot.JackpotType.HappyHour:
              _jackpot.ContributionMeters.ContributionHappyHourAmount = _operation.Amount;
              _jackpot.ContributionMeters.CompensationAmount = -_operation.Amount;
              break;
          }

          if (_jackpot.ContributionMeters.Save(true, true, SqlTrx))
          {
            _operation.Status = JackpotAmountOperations.OperationStatus.Processed;

            return true;
          }
        }
        else
        {
          _operation.Status = JackpotAmountOperations.OperationStatus.Cancelled;
          Log.Warning(String.Format("JackpotDistribution.DistributionAmountOperations.OperationAdd - Jackpot {0} Id:{1} disabled", _jackpot.Name, _jackpot.Id));

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } // OperationAdd

    /// <summary>
    /// award prize
    /// </summary>
    /// <param name="JackpotList"></param>
    /// <param name="_operation"></param>
    /// <returns></returns>
    private static Boolean OperationAward(List<Jackpot> JackpotList, JackpotAmountOperations _operation, SqlTransaction SqlTrx)
    {
      Jackpot _jackpot;

      try
      {
        _jackpot = JackpotList.Find(x => x.Id == _operation.JackpotId);

        if (_jackpot.Enabled)
        {
          // Jackpot Breakdown to meters.
          _jackpot.ContributionMeters.Jackpot = _jackpot;

          switch (_operation.JackpotType)
          {
            case Jackpot.JackpotType.Main:
              if (!InitializeMain(_jackpot, _operation.Amount))
              {
                return false;
              }
              break;

            case Jackpot.JackpotType.PrizeSharing:
              _jackpot.ContributionMeters.ContributionSharedAmount = _operation.Amount;
              break;

            case Jackpot.JackpotType.HappyHour:
            case Jackpot.JackpotType.HappyHourBefore:
            case Jackpot.JackpotType.HappyHourAfter:
              _jackpot.ContributionMeters.ContributionHappyHourAmount = _operation.Amount;
              break;
          }

          if (_jackpot.ContributionMeters.Save(true, true, SqlTrx))
          {
            _operation.Status = JackpotAmountOperations.OperationStatus.Processed;

            return true;
          }
        }
        else
        {
          _operation.Status = JackpotAmountOperations.OperationStatus.Cancelled;
          Log.Warning(String.Format("JackpotDistribution.DistributionAmountOperations.OperationAward - Jackpot {0} Id:{1} disabled", _jackpot.Name, _jackpot.Id));

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } // OperationAward

    /// <summary>
    /// Reset main, shared, HH and hidden
    /// </summary>
    /// <param name="JackpotList"></param>
    /// <param name="_operation"></param>
    /// <returns></returns>
    private static Boolean OperationReset(List<Jackpot> JackpotList, JackpotAmountOperations _operation, SqlTransaction SqlTrx)
    {
      Jackpot _jackpot;

      try
      {
        _jackpot = JackpotList.Find(x => x.Id == _operation.JackpotId);

        // Jackpot Breakdown to meters.
        _jackpot.ContributionMeters.Jackpot = _jackpot;

        _jackpot.ContributionMeters.ToCompensateAmount = _jackpot.ContributionMeters.ToCompensateAmount
                                                       - _jackpot.ContributionMeters.MainAmount
                                                       - _jackpot.ContributionMeters.HiddenAmount
                                                       - _jackpot.ContributionMeters.SharedAmount
                                                       - _jackpot.ContributionMeters.HappyHourAmount
                                                       + _jackpot.Minimum
                                                       + _jackpot.AwardPrizeConfig.PrizeSharing.Minimum
                                                       + _jackpot.AwardPrizeConfig.HappyHour.Minimum;
        _jackpot.ContributionMeters.MainAmount = _jackpot.Minimum;
        _jackpot.ContributionMeters.HiddenAmount = 0;
        _jackpot.ContributionMeters.SharedAmount = _jackpot.AwardPrizeConfig.PrizeSharing.Minimum;
        _jackpot.ContributionMeters.HappyHourAmount = _jackpot.AwardPrizeConfig.HappyHour.Minimum;

        if (_jackpot.ContributionMeters.Save(false, true, SqlTrx))
        {
          _operation.Status = JackpotAmountOperations.OperationStatus.Processed;

          return true;
        }
        else
        {
          _operation.Status = JackpotAmountOperations.OperationStatus.Cancelled;
          Log.Warning(String.Format("JackpotDistribution.DistributionAmountOperations.OperationReset - Jackpot {0} Id:{1}", _jackpot.Name, _jackpot.Id));

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } // OperationReset

    /// <summary>
    /// Recalculate amounts jackpot
    /// </summary>
    /// <param name="JackpotList"></param>
    /// <param name="_operation"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean OperationRecalculate(List<Jackpot> JackpotList, JackpotAmountOperations _operation, SqlTransaction SqlTrx)
    {
      Jackpot _jackpot;

      try
      {
        _jackpot = JackpotList.Find(x => x.Id == _operation.JackpotId);

        if (_jackpot.Enabled)
        {
          // Jackpot Breakdown to meters.
          _jackpot.ContributionMeters.Jackpot = _jackpot;

          if (_jackpot.ContributionMeters.Save(true, true, SqlTrx))
          {
            _operation.Status = JackpotAmountOperations.OperationStatus.Processed;

            return true;
          }
        }
        else
        {
          _operation.Status = JackpotAmountOperations.OperationStatus.Cancelled;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } // OperationAdd


    /// <summary>
    /// Terminal Amount To meter
    /// </summary>
    /// <param name="AmountTerminal"></param>
    /// <param name="DtAmounts"></param>
    /// <param name="Contribution"></param>
    /// <param name="JackpotList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean TerminalAmountToMeter(JackpotAmountTerminal AmountTerminal, DataTable DtAmounts, JackpotContributionList Contribution,
                                                 List<Jackpot> JackpotList, SqlTransaction SqlTrx)
    {
      List<JackpotContribution> _contribution;
      List<Int32> _jackpot_id_list;

      GetAfectedJackpots(DtAmounts, Contribution, out _jackpot_id_list, out _contribution);

      return CalculateContributionMeter(_contribution, AmountTerminal, _jackpot_id_list, JackpotList, SqlTrx);

    } // TerminalAmountToMeter

    /// <summary>
    /// Get Afected Jackpots
    /// </summary>
    /// <param name="dtAmounts"></param>
    /// <param name="Contribution"></param>
    /// <param name="JackpotIdList"></param>
    /// <param name="TerminalContributions"></param>
    private static void GetAfectedJackpots(DataTable dtAmounts, JackpotContributionList Contribution,
                                           out List<Int32> JackpotIdList, out List<JackpotContribution> TerminalContributions)
    {
      JackpotContribution _contribution;

      JackpotIdList = new List<Int32>();
      TerminalContributions = new List<JackpotContribution>();

      foreach (DataRow _dr_amounts in dtAmounts.Rows)
      {
        _contribution = Contribution.Items.Find(x => x.Id == (Int64)_dr_amounts["TG_ELEMENT_ID"]);

        if (_contribution == null)
        {
          continue;
        }

        TerminalContributions.Add(_contribution);

        if (!JackpotIdList.Exists(x => x == _contribution.JackpotId))
        {
          JackpotIdList.Add(_contribution.JackpotId);
        }

      }
    } // GetAfectedJackpots

    /// <summary>
    /// Calculate contribuiton meter
    /// </summary>
    /// <param name="TerminalContributions"></param>
    /// <param name="AmountTerminal"></param>
    /// <param name="JackpotIdList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean CalculateContributionMeter(List<JackpotContribution> TerminalContributions, JackpotAmountTerminal AmountTerminal,
                                                      List<Int32> JackpotIdList, List<Jackpot> JackpotList, SqlTransaction SqlTrx)
    {
      List<JackpotContribution> _list_jackpot;
      Decimal _contribution_pct;
      Decimal _meter_amount;
      Decimal _compensation_amount;
      JackpotMeter _jackpot_meter;
      Jackpot _jackpot;

      foreach (Int32 _jackpot_id in JackpotIdList)
      {
        _list_jackpot = TerminalContributions.FindAll(x => x.JackpotId == _jackpot_id);
        _jackpot = JackpotList.Find(x => x.Id == _jackpot_id);

        if (_list_jackpot.Count > 0)
        {
          _contribution_pct = GetJackpotAmountContribution(_jackpot_id, _list_jackpot) / 100.0m;

          if (_contribution_pct > 0)
          {

            SplitAmount(_contribution_pct * AmountTerminal.Amount, _jackpot, out _meter_amount, out _compensation_amount);

            // Jackpot to meter history.
            _jackpot_meter = new JackpotMeter()
            {
              JackpotId = _jackpot_id,
              TerminalId = AmountTerminal.TerminalId,
              Type = JackpotMeterType.ByWorkingDay,
              PlayedAmount = AmountTerminal.Amount,
              MeterAmount = _meter_amount,
              CompensationAmount = _compensation_amount
            };

            if (!_jackpot_meter.Save(SqlTrx))
            {
              return false;
            }

            // Jackpot Breakdown to meters.
            _jackpot.ContributionMeters.Jackpot = _jackpot;
            _jackpot.ContributionMeters.MeterAmount = _meter_amount;
            _jackpot.ContributionMeters.CompensationAmount = _compensation_amount;

            if (!_jackpot.ContributionMeters.Save(true, false, SqlTrx))
            {
              return false;
            }

            if (_jackpot.Type == Jackpot.AreaType.LocalMistery)
            {
              if (_jackpot.NumPending < _jackpot.MaximumNumPending)
              {
                if (JackpotBusinessLogic.JackpotHit(_jackpot.ContributionMeters.ContributionMainAmount, _jackpot.Average, _jackpot.ContributionMeters.MainAmount, _jackpot.Maximum, _jackpot.ProbabilityWithMax))
                {
                  _jackpot.AddNumPending(1, SqlTrx);
                }
              }
            }
          }
        }
      }

      return true;
    }  // CalculateContributionMeter

    /// <summary>
    /// Split TotalContribution to meter and compensation
    /// </summary>
    /// <param name="TotalContribution"></param>
    /// <param name="JackpotData"></param>
    /// <param name="Meter"></param>
    /// <param name="Compensation"></param>
    private static void SplitAmount(Decimal TotalContribution, Jackpot JackpotData, out Decimal Meter, out Decimal Compensation)
    {
      Jackpot Jackpot;
      Decimal _to_compensate_pct;

      _to_compensate_pct = JackpotData.CompensationFixedPct;

      switch (JackpotData.CompensationType)
      {
        case Jackpot.ContributionCompensationType.C0:  // C0 = Jackpot.ContributionPCT          
          break;

        case Jackpot.ContributionCompensationType.C1:  // C1 = (Min / Avg)*100 + Jackpot.ContributionPCT    
          if (JackpotData.Average > 0)
          {
            _to_compensate_pct += JackpotData.Minimum / JackpotData.Average * 100.0m;
          }
          break;

        case Jackpot.ContributionCompensationType.C2: // C2 = GetCompensationPct(ATAN) + Jackpot.ContributionPCT
          _to_compensate_pct += JackpotBusinessLogic.GetCompensationPct(JackpotData.ContributionMeters.ToCompensateAmount, JackpotData.Minimum, JackpotData.Average);
          break;

        default:
          Log.Warning(String.Format("JackpotBusinessLogic.SplitAmount - JackpotId = {0} , JackpotType = {1}", JackpotData.Id, JackpotData.CompensationType));
          break;
      }

      Compensation = Math.Round(TotalContribution * Math.Min(_to_compensate_pct, 100.0m) / 100.0m, 8, MidpointRounding.AwayFromZero);
      Meter = TotalContribution - Compensation;

    } // SplitAmount

    /// <summary>
    /// Get contribution (minimum if there is more than one)
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="ContributionListByJackpot"></param>
    /// <returns></returns>
    private static Decimal GetJackpotAmountContribution(Int32 JackpotId, List<JackpotContribution> ContributionListByJackpot)
    {
      Decimal _minimum;
      Decimal _value;

      if (ContributionListByJackpot.Count == 0)
      {
        return 0;
      }

      _minimum = ContributionListByJackpot[0].GetContributionPercentatge();

      if (ContributionListByJackpot.Count == 1)
      {
        return _minimum;
      }

      foreach (JackpotContribution _jackpot_item in ContributionListByJackpot)
      {
        _value = _jackpot_item.GetContributionPercentatge();
        if (_value < _minimum)
        {
          _minimum = _value;
        }
      }
      return _minimum;
    } // GetJackpotAmountContribution

    /// <summary>
    /// Award and initialize main jackpot
    /// </summary>
    /// <param name="InitializeJackpot"></param>
    /// <param name="Amount"></param>
    /// <returns></returns>
    private static Boolean InitializeMain(Jackpot InitializeJackpot, Decimal Amount)
    {
      Decimal _exceeded;

      try
      {
        if (InitializeJackpot.Maximum < Amount + InitializeJackpot.ContributionMeters.MainAmount + InitializeJackpot.Minimum + InitializeJackpot.ContributionMeters.HiddenAmount)
        {
          _exceeded = Amount + InitializeJackpot.ContributionMeters.MainAmount + InitializeJackpot.Minimum + InitializeJackpot.ContributionMeters.HiddenAmount - InitializeJackpot.Maximum;
          InitializeJackpot.ContributionMeters.MainAmount += Amount + InitializeJackpot.Minimum + InitializeJackpot.ContributionMeters.HiddenAmount - _exceeded;
          InitializeJackpot.ContributionMeters.HiddenAmount = _exceeded;
        }
        else
        {
          InitializeJackpot.ContributionMeters.MainAmount += Amount + InitializeJackpot.Minimum + InitializeJackpot.ContributionMeters.HiddenAmount;
          InitializeJackpot.ContributionMeters.HiddenAmount = 0;
        }

        InitializeJackpot.ContributionMeters.ToCompensateAmount += InitializeJackpot.Minimum;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } // InitializeMain

    #endregion " Private methods "

  }
}
