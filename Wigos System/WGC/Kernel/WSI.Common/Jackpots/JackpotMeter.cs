﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotMeter.cs
// 
//   DESCRIPTION: Jackpots meters history
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 28-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2017 ETP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  #region " Enums "

  public enum JackpotMeterType
  {
    None = 0,
    ByWorkingDay = 1
  }

  #endregion " Enums "

  public class JackpotMeter
  {

    #region " Constructor "

    /// <summary>
    /// Class constructor Init Id = -1
    /// </summary>
    /// 
    public JackpotMeter()
    {
      JackpotId = -1;
    } // JackpotMeter

    #endregion " Constructor "

    #region " Members "

    public Int32 JackpotId { get; set; }
    public Int32 TerminalId { get; set; }
    public JackpotMeterType Type { get; set; }
    public DateTime Date { get; set; }
    public Decimal PlayedAmount { get; set; }
    public Decimal MeterAmount { get; set; }
    public Decimal CompensationAmount { get; set; }

    #endregion " Members "

    #region " Public methods "

    /// <summary>
    /// Save meter
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Save(SqlTransaction SqlTrx)
    {

      if (!DB_InsertMeter(SqlTrx))
      {
        return false;
      }

      return true;
    } // Save


    #endregion " Public methods "
    
    #region " Private methods "

    /// <summary>
    /// Insert or Update Meter.
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_InsertMeter(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_updated;

      try
      { 
        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS ( SELECT   1                                                                  ");
        _sb.AppendLine("               FROM   JACKPOTS_METERS_HISTORY                                            ");
        _sb.AppendLine("              WHERE   JMH_JACKPOT_ID  = @pJackpotId                                      ");
        _sb.AppendLine("                AND   JMH_TERMINAL_ID = @pTerminalId                                     ");
        _sb.AppendLine("                AND   JMH_TYPE        = @pTypeJackpot                                    ");
        _sb.AppendLine("                AND   JMH_DATE        = @pWorkingDay                                     ");
        _sb.AppendLine("           )                                                                             ");
        _sb.AppendLine("      UPDATE   JACKPOTS_METERS_HISTORY                                                   ");
        _sb.AppendLine("         SET   JMH_PLAYED_AMOUNT       = JMH_PLAYED_AMOUNT + @pPlayedAmount              ");
        _sb.AppendLine("           ,   JMH_METER_AMOUNT        = JMH_METER_AMOUNT +  @pMeterAmount               ");
        _sb.AppendLine("           ,   JMH_COMPENSATION_AMOUNT = JMH_COMPENSATION_AMOUNT + @pCompensationAmount  ");
        _sb.AppendLine("           ,   JMH_LAST_UPDATE         = @pLastUpdate                                    ");
        _sb.AppendLine("       WHERE   JMH_JACKPOT_ID          = @pJackpotId                                     ");
        _sb.AppendLine("         AND   JMH_TERMINAL_ID         = @pTerminalId                                    ");
        _sb.AppendLine("         AND   JMH_TYPE                = @pTypeJackpot                                   ");
        _sb.AppendLine("         AND   JMH_DATE                = @pWorkingDay                                    ");
        _sb.AppendLine("        ELSE                                                                             ");
        _sb.AppendLine(" INSERT INTO JACKPOTS_METERS_HISTORY                                                     ");
        _sb.AppendLine("            ( JMH_JACKPOT_ID                                                             ");
        _sb.AppendLine("            , JMH_TERMINAL_ID                                                            ");
        _sb.AppendLine("            , JMH_TYPE                                                                   ");
        _sb.AppendLine("            , JMH_DATE                                                                   ");
        _sb.AppendLine("            , JMH_PLAYED_AMOUNT                                                          ");
        _sb.AppendLine("            , JMH_METER_AMOUNT                                                           ");
        _sb.AppendLine("            , JMH_COMPENSATION_AMOUNT                                                    ");
        _sb.AppendLine("            , JMH_CREATION                                                               ");
        _sb.AppendLine("            , JMH_LAST_UPDATE                                                            ");
        _sb.AppendLine("            )                                                                            ");
        _sb.AppendLine("      VALUES                                                                             ");
        _sb.AppendLine("            ( @pJackpotId                                                                ");
        _sb.AppendLine("            , @pTerminalID                                                               ");
        _sb.AppendLine("            , @pTypeJackpot                                                              ");
        _sb.AppendLine("            , @pWorkingDay                                                               ");
        _sb.AppendLine("            , @pPlayedAmount                                                             ");
        _sb.AppendLine("            , @pMeterAmount                                                              ");
        _sb.AppendLine("            , @pCompensationAmount                                                       ");
        _sb.AppendLine("            , @pCreation                                                                 ");
        _sb.AppendLine("            , @pLastUpdate                                                               ");
        _sb.AppendLine("            )                                                                            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pJackpotId", SqlDbType.BigInt).Value = this.JackpotId;
          _sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.BigInt).Value = this.TerminalId;
          _sql_cmd.Parameters.Add("@pTypeJackpot", SqlDbType.Int).Value = this.Type;
          _sql_cmd.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = WSI.Common.Misc.TodayOpening();
          _sql_cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Decimal).Value = this.PlayedAmount;
          _sql_cmd.Parameters.Add("@pMeterAmount", SqlDbType.Decimal).Value = this.MeterAmount;
          _sql_cmd.Parameters.Add("@pCompensationAmount", SqlDbType.Decimal).Value = this.CompensationAmount;
          _sql_cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = WGDB.Now;
          _sql_cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = WGDB.Now;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _num_updated = _sql_cmd.ExecuteNonQuery();

            if (_num_updated != 1)
            {
              Log.Message("JackpotMeter.DB_InsertMeter() - An error occurred inserting: Rows affected <> 1.");

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("JackpotMeter.DB_InsertMeter() - An error occurred inserting.");
        Log.Exception(_ex);
      }

      return false;
    } // DB_InsertMeter

    #endregion " Private methods "

  }
}
