﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardHappyHour.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Ruben Lama
// 
// CREATION DATE: 28-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardHappyHour : JackpotBase
  {
    #region " Constants "

    private const Int32 DB_ID                           = 0;
    private const Int32 DB_JACKPOT_ID                   = 1;
    private const Int32 DB_EVENT_TYPE                   = 2;
    private const Int32 DB_TIME_BEFORE                  = 3;
    private const Int32 DB_AWARDS_BEFORE                = 4;
    private const Int32 DB_AMOUNT_DISTRIBUTION_BEFORE   = 5;
    private const Int32 DB_TIME_AFTER                   = 6;
    private const Int32 DB_AWARDS_AFTER                 = 7;
    private const Int32 DB_AMOUNT_DISTRIBUTION_AFTER    = 8;    
    private const Int32 DB_LAST_UPDATE                  = 9;
    private const Int32 DB_MINIMUM                      = 10;
    private const Int32 DB_MAXIMUM                      = 11;

    #endregion
    
    #region " Properties "
    
    public Int32 JackpotId { get; set; }
    public AwardHappyHourEvents EventType { get; set; }
    public Int32 TimeBefore { get; set; }
    public Decimal Minimum { get; set; }
    public Decimal Maximum { get; set; }
    public Int32 AwardsBefore { get; set; }
    public AwardFilterAmountDistribution AmountDistributionBefore { get; set; }
    public Int32 TimeAfter { get; set; }
    public Int32 AwardsAfter { get; set; }
    public AwardFilterAmountDistribution AmountDistributionAfter { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by JackpotId
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotAwardHappyHour()
    {
      this.Id                       = DEFAULT_ID_VALUE;
      this.EventType                = AwardHappyHourEvents.Disabled;
      this.Minimum                  = DEFAULT_DECIMAL_VALUE;
      this.Maximum                  = DEFAULT_DECIMAL_VALUE;
      this.TimeBefore               = DEFAULT_INT_VALUE;     
      this.AwardsBefore             = DEFAULT_INT_VALUE;
      this.AmountDistributionBefore = AwardFilterAmountDistribution.Uniform;
      this.TimeAfter                = DEFAULT_INT_VALUE;
      this.AwardsAfter              = DEFAULT_INT_VALUE;      
      this.AmountDistributionAfter  = AwardFilterAmountDistribution.Uniform;     
      this.LastUpdate               = WGDB.MinDate;
      this.LastModification         = WGDB.MinDate;
      
    } // JackpotAwardHappyHour

    // Read Jackpot Award Happy Hour
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.DB_GetJackpotAwardHappyHour_Internal(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      return this.DB_GetJackpotAwardHappyHour_Internal(JackpotId, SqlTrx);
    } // Read

    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
      {
      base.Reset();
      this.JackpotId = DEFAULT_ID_VALUE;
    } // Reset
   
    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotAwardHappyHour_Internal(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotAwardHappyHourQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!DB_JackpotAwardHappyHourDataMapper(_sql_reader))
              {
                return false;
              }

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardHappyHour.DB_GetFilters -> JackpotId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotAwardHappyHour_Internal

    /// <summary>
    /// Load Jackpot Award Days data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotAwardHappyHourDataMapper(SqlDataReader SqlReader)
    {
      try
      {
        this.Id                       = SqlReader.GetInt32(DB_ID);
        this.JackpotId                = SqlReader.GetInt32(DB_JACKPOT_ID);
        this.EventType                = (AwardHappyHourEvents)SqlReader.GetInt32(DB_EVENT_TYPE);
        this.TimeBefore               = SqlReader.GetInt32(DB_TIME_BEFORE);
        this.AwardsBefore             = SqlReader.GetInt32(DB_AWARDS_BEFORE);
        this.AmountDistributionBefore = (AwardFilterAmountDistribution)SqlReader.GetInt32(DB_AMOUNT_DISTRIBUTION_BEFORE);
        this.TimeAfter                = SqlReader.GetInt32(DB_TIME_AFTER);
        this.AwardsAfter              = SqlReader.GetInt32(DB_AWARDS_AFTER);
        this.AmountDistributionAfter  = (AwardFilterAmountDistribution)SqlReader.GetInt32(DB_AMOUNT_DISTRIBUTION_AFTER);
        this.LastUpdate               = SqlReader.IsDBNull(DB_LAST_UPDATE) ? WGDB.MinDate : SqlReader.GetDateTime(DB_LAST_UPDATE);
        this.Minimum                  = SqlReader.GetDecimal(DB_MINIMUM);
        this.Maximum                  = SqlReader.GetDecimal(DB_MAXIMUM);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotAwardHappyHourDataMapper");
        Log.Exception(_ex);
      }

      return false;
      
    } // DB_JackpotAwardHappyHourDataMapper 
   
    /// <summary>
    /// Gets Jackpot  select query
    /// </summary>
    /// <returns></returns>
    private String DB_JackpotAwardHappyHourQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   JHH_ID                         "); // 0
      _sb.AppendLine("           , JHH_JACKPOT_ID                 "); // 1
      _sb.AppendLine("           , JHH_EVENT_TYPE                 "); // 2
      _sb.AppendLine("           , JHH_TIME_BEFORE                "); // 3
      _sb.AppendLine("           , JHH_AWARDS_BEFORE              "); // 4
      _sb.AppendLine("           , JHH_AMOUNT_DISTRIBUTION_BEFORE "); // 5
      _sb.AppendLine("           , JHH_TIME_AFTER                 "); // 6
      _sb.AppendLine("           , JHH_AWARDS_AFTER               "); // 7
      _sb.AppendLine("           , JHH_AMOUNT_DISTRIBUTION_AFTER  "); // 8
      _sb.AppendLine("           , JHH_LAST_UPDATE                "); // 9
      _sb.AppendLine("           , JHH_MINIMUM                    "); // 10
      _sb.AppendLine("           , JHH_MAXIMUM                    "); // 11
      _sb.AppendLine("      FROM   JACKPOTS_SETTINGS_HAPPY_HOUR   ");
      _sb.AppendLine("     WHERE   JHH_JACKPOT_ID = @pJackpotId   ");

      return _sb.ToString();

    } // DB_JackpotAwardHappyHourQuery_Select  
    
    #endregion " Private Methods "

    #region " Internal Methods "

    
    /// <summary>
    /// Insert an specific Jackpot happy hour in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      SqlParameter _op;
      StringBuilder _sb;
   
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO   JACKPOTS_SETTINGS_HAPPY_HOUR       ");
        _sb.AppendLine("             ( JHH_JACKPOT_ID                     ");
        _sb.AppendLine("             , JHH_EVENT_TYPE                     ");
        _sb.AppendLine("             , JHH_MINIMUM                        ");
        _sb.AppendLine("             , JHH_MAXIMUM                        ");
        _sb.AppendLine("             , JHH_TIME_BEFORE                    ");
        _sb.AppendLine("             , JHH_AWARDS_BEFORE                  ");
        _sb.AppendLine("             , JHH_AMOUNT_DISTRIBUTION_BEFORE     ");
        _sb.AppendLine("             , JHH_TIME_AFTER                     ");
        _sb.AppendLine("             , JHH_AWARDS_AFTER                   ");
        _sb.AppendLine("             , JHH_AMOUNT_DISTRIBUTION_AFTER      ");
        _sb.AppendLine("             , JHH_LAST_UPDATE                )   ");
        _sb.AppendLine("      VALUES                                      ");
        _sb.AppendLine("             ( @pJackpotId                        ");
        _sb.AppendLine("             , @pEventType                        ");
        _sb.AppendLine("             , @pMinimum                          ");
        _sb.AppendLine("             , @pMaximum                          ");
        _sb.AppendLine("             , @pTimeBefore                       ");
        _sb.AppendLine("             , @pAwardsBefore                     ");
        _sb.AppendLine("             , @pAmountDistributionBefore         ");
        _sb.AppendLine("             , @pTimeAfter                        ");
        _sb.AppendLine("             , @pAwardsAfter                      ");
        _sb.AppendLine("             , @pAmountDistributionAfter          ");
        _sb.AppendLine("             , @pLastUpdate                   )   ");

        _sb.AppendLine(" SET     @pId = SCOPE_IDENTITY()                  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId"                 , SqlDbType.Int).Value = this.JackpotId;
          _cmd.Parameters.Add("@pEventType"                 , SqlDbType.Int).Value = this.EventType;
          _cmd.Parameters.Add("@pMinimum  "                 , SqlDbType.Money).Value = this.Minimum;
          _cmd.Parameters.Add("@pMaximum  "                 , SqlDbType.Money).Value = this.Maximum;

          // Nullable fields
          _cmd.Parameters.Add("@pTimeBefore"                , SqlDbType.Int).Value = this.TimeBefore;
          _cmd.Parameters.Add("@pAwardsBefore"              , SqlDbType.Int).Value = this.AwardsBefore;
          _cmd.Parameters.Add("@pAmountDistributionBefore"  , SqlDbType.Int).Value = this.AmountDistributionBefore;
          _cmd.Parameters.Add("@pTimeAfter"                 , SqlDbType.Int).Value = this.TimeAfter;
          _cmd.Parameters.Add("@pAwardsAfter"               , SqlDbType.Int).Value = this.AwardsAfter;
          _cmd.Parameters.Add("@pAmountDistributionAfter"   , SqlDbType.Int).Value = this.AmountDistributionAfter;

          // Last update
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = (this.LastUpdate == WGDB.MinDate) ? DBNull.Value : (Object)this.LastUpdate;

          //Output parameter
          _op = new SqlParameter("@pId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);
          
          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardHappyHour.DB_Insert -> JackpotId: {0}", this.JackpotId));

            return false;
          }

          this.Id = (Int32)_op.Value;

          // Recalculate current meter with new max and min values if has changed.
          if (this.EventType != Jackpot.AwardHappyHourEvents.Disabled)
          {
            return JackpotAmountOperations.RecalculateJackpotAmounts(this.JackpotId, SqlTrx);
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardHappyHour.DB_Insert -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Jackpot Happy Hour Config in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _need_recalculate;
      Boolean _recalculate;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SET  @pRecalculate = 0                                              ");
        _sb.AppendLine(" SELECT  @pRecalculate = 1                                              ");
        _sb.AppendLine("  FROM   JACKPOTS_SETTINGS_HAPPY_HOUR                                   ");
        _sb.AppendLine(" WHERE ( JHH_MINIMUM                     <> @pMinimum                   ");
        _sb.AppendLine("    OR   JHH_MAXIMUM                     <> @pMaximum                   ");
        _sb.AppendLine("    OR   JHH_EVENT_TYPE                  <> @pEventType)                ");
        _sb.AppendLine("   AND   JHH_ID                           = @pId                        ");

        _sb.AppendLine(" UPDATE   JACKPOTS_SETTINGS_HAPPY_HOUR                                  ");
        _sb.AppendLine("    SET   JHH_JACKPOT_ID                  = @pJackpotId                 ");
        _sb.AppendLine("        , JHH_EVENT_TYPE                  = @pEventType                 ");
        _sb.AppendLine("        , JHH_MINIMUM                     = @pMinimum                   ");
        _sb.AppendLine("        , JHH_MAXIMUM                     = @pMaximum                   ");
        _sb.AppendLine("        , JHH_TIME_BEFORE                 = @pTimeBefore                ");
        _sb.AppendLine("        , JHH_AWARDS_BEFORE               = @pAwardsBefore              ");
        _sb.AppendLine("        , JHH_AMOUNT_DISTRIBUTION_BEFORE  = @pAmountDistributionBefore  ");
        _sb.AppendLine("        , JHH_TIME_AFTER                  = @pTimeAfter                 ");
        _sb.AppendLine("        , JHH_AWARDS_AFTER                = @pAwardsAfter               ");
        _sb.AppendLine("        , JHH_AMOUNT_DISTRIBUTION_AFTER   = @pAmountDistributionAfter   ");
        _sb.AppendLine("        , JHH_LAST_UPDATE                 = @pLastUpdate                ");
        _sb.AppendLine("  WHERE   JHH_ID                          = @pId                        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId"                        , SqlDbType.Int).Value = this.Id;
          _cmd.Parameters.Add("@pJackpotId"                 , SqlDbType.Int).Value = this.JackpotId;
          _cmd.Parameters.Add("@pEventType"                 , SqlDbType.Int).Value = this.EventType;
          _cmd.Parameters.Add("@pMinimum  "                 , SqlDbType.Money).Value = this.Minimum;
          _cmd.Parameters.Add("@pMaximum  "                 , SqlDbType.Money).Value = this.Maximum;
          
          // Nullable fields
          _cmd.Parameters.Add("@pTimeBefore"                , SqlDbType.Int).Value = this.TimeBefore;
          _cmd.Parameters.Add("@pAwardsBefore"              , SqlDbType.Int).Value = this.AwardsBefore;
          _cmd.Parameters.Add("@pAmountDistributionBefore"  , SqlDbType.Int).Value = this.AmountDistributionBefore;
          _cmd.Parameters.Add("@pTimeAfter"                 , SqlDbType.Int).Value = this.TimeAfter;
          _cmd.Parameters.Add("@pAwardsAfter"               , SqlDbType.Int).Value = this.AwardsAfter;
          _cmd.Parameters.Add("@pAmountDistributionAfter"   , SqlDbType.Int).Value = this.AmountDistributionAfter;

          // Last update
          this.LastUpdate = this.LastModification;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;

          _need_recalculate = new SqlParameter("@pRecalculate", SqlDbType.Bit);
          _need_recalculate.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_need_recalculate);

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardHappyHour.DB_Update -> JackpotAwardHappyHourId: {0}", this.Id));

            return false;
          }

          _recalculate = (Boolean)_need_recalculate.Value;

          // Recalculate amount with new max and min values if has changed.
          if (_recalculate)
          {
            return JackpotAmountOperations.RecalculateJackpotAmounts(this.JackpotId, SqlTrx);
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardHappyHour.DB_Update -> JackpotAwardHappyHourId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Update

    /// <summary>
    /// Clone JackpotAwardHappyHour
    /// </summary>
    /// <returns></returns>
    internal JackpotAwardHappyHour Clone()
    {
      return new JackpotAwardHappyHour()
      {
        Id                        = this.Id,
        JackpotId                 = this.JackpotId,
        EventType                 = this.EventType,
        Minimum                   = this.Minimum,
        Maximum                   = this.Maximum,
        TimeBefore                = this.TimeBefore,
        AwardsBefore              = this.AwardsBefore,
        AmountDistributionBefore  = this.AmountDistributionBefore,
        TimeAfter                 = this.TimeAfter,
        AwardsAfter               = this.AwardsAfter,
        AmountDistributionAfter   = this.AmountDistributionAfter,
        LastUpdate                = this.LastUpdate 
      };
    } // Clone

    #endregion 

  } // JackpotAwardHappyHour
}
