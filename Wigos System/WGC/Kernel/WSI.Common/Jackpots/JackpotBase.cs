﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotBase.cs
// 
//   DESCRIPTION: Base Class utilities for Jackpot
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 30-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAR-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace WSI.Common.Jackpot
{
  public class JackpotBase
  {
    #region " Constants "

    // Public constants
    public const Int32 DEFAULT_ID_VALUE         = -1;
    public const Int32 DEFAULT_INT_VALUE        = 0;
    public const Decimal DEFAULT_DECIMAL_VALUE  = 0;

    public const Int32 DEFAULT_TIME_VALUE       = -1;
    public const Int32 DEFAULT_TIME_FROM_VALUE  = 0;
    public const Int32 DEFAULT_TIME_TO_VALUE    = 1439;

    #endregion

    #region "Enums"

    #region " Jackpot Config "
    
    public enum JackpotType
    {
      Main = 0,
      Hidden = 1,
      PrizeSharing = 2,
      HappyHour = 3,
      ToCompensate = 4,
      Notification = 5,
      HappyHourBefore = 6,
      HappyHourAfter = 7,
      All = 8
    } // JackpotType

    public enum JackpotFilterType
    {
      All = 0,
      ByJackpotId = 1,
      Enabled = 2,
      Pending = 3,
      EnabledAndPending = 4
    } // JackpotFilterType

    public enum AreaType
    {
      LocalMistery = 0,
      LocalFixed = 1
    } // AreaType

    public enum ContributionCreditType
    {
      Redeemable = 0
    } // ContributionCreditType

    public enum ContributionType
    {
      Fixed = 0,
      Weekly = 1
    } // ContributionType

    public enum ContributionGroupState
    {
      NotInUse = 0,
      InUse = 1,
      Deleted = 2
    } // ContributionGroupState

    public enum PayoutModeType
    {
      Manual = 0,
      Automatic = 1,
      EGMBonusing = 2
    } // PayoutModeType

    public enum ContributionCompensationType
    {
      C0 = 0,
      C1 = 1,
      C2 = 2
    } // ContributionCompensationType

    #endregion " Jackpot Config "

    #region " Award Prize "
    
    public enum AwardCustomerVipType
    {
      All = 0,
      Vip = 1,
      NoVip = 2
    } // AwardCustomerVipType

    public enum AwardCustomerTierType
    {
      All = 0,
      OneByOne = 1,
      Anonymous = 2
    } // AwardCustomerTierType


    public enum AwardHappyHourEvents 
    {
      Disabled = 0,
      BeforeJackpot = 1,
      AfterJackpot = 2,
      BothEvents = 3
    } // AwardHappyHourEvents

    #endregion 

    #region " Award Filter "
    
    public enum JackpotSelectionEGMType
    {
      All = 0,                       // 000000
      Selected = 1                   // 000001
    } // JackpotSelectionEGMType

    public enum AwardFilterAmountDistribution
    {
      Uniform = 0,
      LowToHigh = 1,
      HighToLow = 2
    } // AwardFilterAmountDistribution

    public enum AwardFilterCreationDate
    {
      Disabled = 0,
      Creation = 1,
      CreationBirthdate = 2
    } // AwardFilterCreationDate

    public enum AwardFilterDateOfBirth
    {
      Disabled = 0,
      Birthday = 1, 
      DayAndMonth = 2
    } // AwardFilterDateOfBirth

    public enum AwardLevel
    {
      Anonymous = 0,
      Level1 = 1,
      Level2 = 2,
      Level3 = 3,
      Level4 = 4
    }
    
    #endregion " Award Filter "

    #region " JackpotViewer "

    public enum JackpotViewerFilterType
    {

      All                 = 0,
      ByJackpotViewerId   = 1,
      Enabled             = 2,
      EnabledAndRelated   = 3,
      ByJackpotViewerCode = 4
    } // JackpotViewerFilterType

    public enum JackpotViewerGetType
    {
      All = 0,
      OnlySpecialData = 1,
      OnlyLastAwards  = 2
    }  // JackpotViewerGetType

    #endregion

    #endregion

    #region " Properties "

    public Boolean IsNew
    {
      get { return (this.Id == DEFAULT_ID_VALUE); }
    }

    public Int32 Id  { get; set; }

    public virtual DateTime LastUpdate  { get; set; }    
    public virtual Boolean IsConfigured
    {
      get { return (this.LastUpdate > WGDB.MinDate); }
    }    

    // Last edition in GUI (Before DB_Update())
    public DateTime LastModification { get; set; }

    public virtual Boolean IsModified 
    { 
      get { return (this.LastModification > WGDB.MinDate); } 
    }
    
    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// 
    /// Save Jackpot DB Method
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public virtual Boolean Save()
    {
      using(DB_TRX _db_trx = new DB_TRX())
      {
        if (this.Save(_db_trx.SqlTransaction))
        {
          return _db_trx.Commit();
        }
      }

      return false;
    } // Save
    public virtual Boolean Save(SqlTransaction SqlTrx)
    {
      return this.DB_Save(SqlTrx);
    } // Save

    /// <summary>
    /// Reset all members id
    /// </summary>
    public virtual void Reset()
    {
      this.Id               = DEFAULT_ID_VALUE;
      this.LastModification = WGDB.MinDate;
    } // Reset
    
    #endregion " Public Methods "

    #region " Internal Methods "

    #region " DataBase Methods " 

    /// <summary>
    /// Save Jackpot DB Method
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal virtual Boolean DB_Save(SqlTransaction SqlTrx)
    {
      if (this.IsNew)
      {
        return this.DB_Insert(SqlTrx);
      }

      if (this.IsModified)
      {
        return this.DB_Update(SqlTrx);
      }

      return true;
    } // DB_Save

    /// <summary>
    /// Save special transaction properties
    /// </summary>
    /// <returns></returns>
    internal virtual Boolean DB_SaveSpecialProperties(SqlTransaction SqlTrx)
    {
      return false;
    } // DB_SaveSpecialProperties 

    /// <summary>
    /// Base Class
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal virtual Boolean DB_ReadSpecialData(Int32 Id, SqlTransaction SqlTrx)
    {
      return false;
    } // DB_ReadSpecialData
    internal virtual Boolean DB_ReadSpecialData(Int32 JackpotViewerId, JackpotViewerGetType Type, SqlTransaction SqlTrx)
    {
      return false;
    } // DB_ReadSpecialData

    /// <summary>
    /// Insert an specific Jackpot in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal virtual Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Jackpot in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal virtual Boolean DB_Update(SqlTransaction SqlTrx)
    {
      return false;
    } // DB_Update

    #endregion 

    #region " Common Methods "
    
    #endregion
    
    #endregion " Private Methods "

  } // JackpotBase
}
