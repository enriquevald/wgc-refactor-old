﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotViewerItem.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Rubén Lama
// 
// CREATION DATE: 11-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAY-2017 RLO    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotViewerItem : JackpotBase
  {
    #region " Constants "

    private const Int32 DB_ID                 = 0;
    private const Int32 DB_JACKPOT_VIEWER_ID  = 1;
    private const Int32 DB_JACKPOT_ID         = 2;
    private const Int32 DB_ORDER              = 3;
    private const Int32 DB_NAME               = 4;
    private const Int32 DB_STATUS             = 5;
    private const Int32 DB_MAIN               = 6;
    private const Int32 DB_HAPPY_HOUR         = 7;
    private const Int32 DB_PRIZE_SHARING      = 8;
    private const Int32 DB_HIDDEN             = 9;
    private const Int32 DB_ISO_CODE           = 10;
    private const Int32 DB_RELATED            = 11;
    private const Int32 DB_ENABLED            = 12;

    #endregion
    
    #region " Enums "

    public enum TYPE_JACKPOT_VIEWER_GET
    {
      All = 0,
      SameViewer = 1
    }
    
    #endregion

    #region " Properties "

    public Int32 JackpotViewerId { get; set; }
    public Int32 JackpotId { get; set; }
    public String Name { get; set; }
    public Boolean Status { get; set; }
    public Int32 Order { get; set; }
    public Decimal Main { get; set; }
    public Decimal Hidden { get; set; }
    public Decimal PrizeSharing { get; set; }
    public Decimal HappyHour { get; set; }
    public String IsoCode { get; set; }
    public Boolean IsRelated { get; set; }
    public Boolean Enabled { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by Jackpot Viewer Items
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotViewerItem()
    {
      this.Id               = DEFAULT_ID_VALUE;
      this.JackpotViewerId  = DEFAULT_ID_VALUE;
      this.JackpotId        = DEFAULT_ID_VALUE;
      this.Order            = DEFAULT_INT_VALUE;
      this.Status           = false;
      this.Main             = DEFAULT_DECIMAL_VALUE;
      this.Hidden           = DEFAULT_DECIMAL_VALUE;
      this.PrizeSharing     = DEFAULT_DECIMAL_VALUE;
      this.HappyHour        = DEFAULT_DECIMAL_VALUE;
      this.IsoCode          = String.Empty;
      this.IsRelated        = false;
      this.Enabled          = false;

    } // JackpotViewerItem

    /// <summary>
    /// Get all available items
    /// </summary>
    /// <returns></returns>
    public List<JackpotViewerItem> GetAllItemsAvailables(Int32 JackpotViewerId)
    {
       List<JackpotViewerItem> _items;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (this.GetItemsByJackpotViewerId(JackpotViewerId, TYPE_JACKPOT_VIEWER_GET.All, out _items, _db_trx.SqlTransaction))
        {
          return _items;
        }
      }

      return null;
    } // GetAllItemsAvailables

    /// <summary>
    /// Get all Jackpot Viewer items by Jackpot Viewer Id
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="JackpotViewerItemsList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetItemsByJackpotViewerId(Int32 JackpotViewerId, out List<JackpotViewerItem> JackpotViewerItemsList, SqlTransaction SqlTrx)
    {
      return this.DB_GetAllJackpotViewerItems_Internal(JackpotViewerId, TYPE_JACKPOT_VIEWER_GET.SameViewer, out JackpotViewerItemsList, SqlTrx);
    }
    public Boolean GetItemsByJackpotViewerId(Int32 JackpotViewerId, TYPE_JACKPOT_VIEWER_GET Type, out List<JackpotViewerItem> JackpotViewerItemsList, SqlTransaction SqlTrx)
    {
      return this.DB_GetAllJackpotViewerItems_Internal(JackpotViewerId, Type, out JackpotViewerItemsList, SqlTrx);
    } // GetItemsByJackpotViewerId

    public override Boolean Save(SqlTransaction SqlTrx)
    {
      // Nothing to update. (Must be delete all related items before Save item by item.
      //if (!DeleteByJackpotViewerId(this.JackpotViewerId, SqlTrx))
      //{
      //  return false;
      //}
      
      return this.DB_Insert(SqlTrx);
    } // Save

    /// <summary>
    /// Delete all items by JackpotViewerId
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DeleteByJackpotViewerId(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      return this.DB_DeleteByJackpotViewerId(JackpotViewerId, SqlTrx);
    } // DeleteByJackpotViewerId
    
    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.JackpotViewerId  = DEFAULT_ID_VALUE;
      this.JackpotId        = DEFAULT_ID_VALUE;
    } // Reset

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Get items by Jackpot Viewer Id
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="Items"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetAllJackpotViewerItems_Internal(Int32 JackpotViewerId, TYPE_JACKPOT_VIEWER_GET Type, out List<JackpotViewerItem> JackpotViewerItemsList, SqlTransaction SqlTrx)
    {
      JackpotViewerItem _item;

      JackpotViewerItemsList = new List<JackpotViewerItem>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_GetJackpotsViewerItems_Select(Type), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotViewerId", SqlDbType.Int).Value = JackpotViewerId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _item = this.JackpotViewerItemMappFromSqlReader(_sql_reader);

              if (_item != null)
              {
                _item.JackpotViewerId = JackpotViewerId;
                JackpotViewerItemsList.Add(_item);
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewerItem.DB_GetAllJAckpotViewerItems_Internal -> JackpotViewerId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetAllJackpotViewerItems_Internal

    /// <summary>
    /// Mapp jackpot contribution from sql reader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private JackpotViewerItem JackpotViewerItemMappFromSqlReader(SqlDataReader SqlReader)
    {
      JackpotViewerItem _jackpot_viewer_item;

      _jackpot_viewer_item = new JackpotViewerItem();

      if (!DB_JackpotJackpotViewerItemDataMapper(_jackpot_viewer_item, SqlReader))
      {
        Log.Error("JackpotViewerItem.JackpotViewerItemMappFromSqlReader -> Error on load.");
      }

      return _jackpot_viewer_item;
    } // JackpotContributionMappFromSqlReader

    /// <summary>
    /// Delete all items by Jackpot Viewer in database
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_DeleteByJackpotViewerId(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DELETE FROM  JACKPOTS_VIEWERS_RELATED_JACKPOT          ");
        _sb.AppendLine("       WHERE  JVJ_JACKPOT_VIEWER_ID = @pJackpotViewerId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotViewerId", SqlDbType.Int).Value = JackpotViewerId;
          _cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewerItem.DB_DeleteByJackpotViewerId -> JackpotViewerId: {0}", JackpotViewerId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_DeleteByJackpotViewerId

    /// <summary>
    /// Load Jackpot Viewer item data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotJackpotViewerItemDataMapper(JackpotViewerItem JackpotViewerItem, SqlDataReader SqlReader)
    {
      try
      {
        JackpotViewerItem.Id              = (SqlReader.IsDBNull(DB_ID) )               ? DEFAULT_ID_VALUE  : SqlReader.GetInt32(DB_ID);
        JackpotViewerItem.JackpotViewerId = (SqlReader.IsDBNull(DB_JACKPOT_VIEWER_ID)) ? DEFAULT_ID_VALUE  : SqlReader.GetInt32(DB_JACKPOT_VIEWER_ID);
        JackpotViewerItem.JackpotId       = (SqlReader.IsDBNull(DB_JACKPOT_ID))        ? DEFAULT_ID_VALUE  : SqlReader.GetInt32(DB_JACKPOT_ID);
        JackpotViewerItem.Order           = (SqlReader.IsDBNull(DB_ORDER))             ? DEFAULT_INT_VALUE : SqlReader.GetInt32(DB_ORDER);
        JackpotViewerItem.Name            = SqlReader.GetString(DB_NAME);
        JackpotViewerItem.Status          = SqlReader.GetBoolean(DB_STATUS);
        JackpotViewerItem.Main            = SqlReader.GetDecimal(DB_MAIN);
        JackpotViewerItem.HappyHour       = SqlReader.GetDecimal(DB_HAPPY_HOUR);
        JackpotViewerItem.PrizeSharing    = SqlReader.GetDecimal(DB_PRIZE_SHARING);
        JackpotViewerItem.Hidden          = SqlReader.GetDecimal(DB_HIDDEN);
        JackpotViewerItem.IsoCode         = SqlReader.GetString(DB_ISO_CODE);
        JackpotViewerItem.IsRelated       = (SqlReader.GetInt32(DB_RELATED) > 0);
        JackpotViewerItem.Enabled         = SqlReader.GetBoolean(DB_ENABLED);
                                                 
        return true;                             
      }                                          
      catch (Exception _ex)                      
      {
        Log.Error("Error on DB_JackpotJackpotViewerItemDataMapper");
        Log.Exception(_ex);
      }

      JackpotViewerItem = null;

      return false;
      
    } // DB_JackpotJackpotViewerItemDataMapper 

    /// <summary>
    /// GetJackpotsViewerItems SQl Query
    /// </summary>
    /// <returns></returns>
    private String DB_GetJackpotsViewerItems_Select(TYPE_JACKPOT_VIEWER_GET Type)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.AppendLine(" WITH         JACKPOTS AS                                                                                                    ");
      _sb.AppendLine("(                                                                                                                            ");
      _sb.AppendLine("  SELECT      JS.JS_JACKPOT_ID  AS JACKPOT_ID                                                                                ");
      _sb.AppendLine("            , JS.JS_NAME        AS JACKPOT_NAME                                                                              ");
      _sb.AppendLine("            , JS.JS_ENABLED     AS JACKPOT_STATUS                                                                            ");
      _sb.AppendLine("            , JS.JS_ISO_CODE    AS JACKPOT_ISO_CODE                                                                          ");
      _sb.AppendLine("            , ISNULL(SUM(CASE WHEN JCM.JCM_TYPE = 0 THEN ROUND(JCM.JCM_AMOUNT,2,1) ELSE 0 END ), 0) AS JACKPOT_MAIN          ");
      _sb.AppendLine("            , ISNULL(SUM(CASE WHEN JCM.JCM_TYPE = 1 THEN ROUND(JCM.JCM_AMOUNT,2,1) ELSE 0 END ), 0) AS JACKPOT_HIDDEN        ");
      _sb.AppendLine("            , ISNULL(SUM(CASE WHEN JCM.JCM_TYPE = 2 THEN ROUND(JCM.JCM_AMOUNT,2,1) ELSE 0 END ), 0) AS JACKPOT_PRIZE_SHARING ");
      _sb.AppendLine("            , ISNULL(SUM(CASE WHEN JCM.JCM_TYPE = 3 THEN ROUND(JCM.JCM_AMOUNT,2,1) ELSE 0 END ), 0) AS JACKPOT_HAPPY_HOUR    ");
      _sb.AppendLine("            , JS.JS_ENABLED     AS JACKPOT_ENABLED                                                                           ");
      _sb.AppendLine("       FROM   JACKPOTS_SETTINGS AS JS                                                                                        ");
      _sb.AppendLine("  LEFT JOIN   JACKPOTS_CONTRIBUTION_METERS AS JCM ON JCM.JCM_JACKPOT_ID = JS.JS_JACKPOT_ID                                   ");
      _sb.AppendLine("   GROUP BY   JS.JS_JACKPOT_ID, JS.JS_NAME, JS.JS_ENABLED, JS.JS_JACKPOT_ID, JS.JS_ISO_CODE                                  ");
      _sb.AppendLine(")                                                                                                                            ");
      _sb.AppendLine("     SELECT   JVJ.JVJ_ID AS ID                                                                                               ");
      _sb.AppendLine("			      , JVJ.JVJ_JACKPOT_VIEWER_ID AS JACKPOT_VIEWER_ID                                                                 ");
      _sb.AppendLine("			      , J.JACKPOT_ID                                                                                                   ");
      _sb.AppendLine("			      , JVJ.JVJ_ORDER AS JACKPOT_ORDER                                                                                 ");
      _sb.AppendLine("			      , J.JACKPOT_NAME                                                                                                 ");
      _sb.AppendLine("			      , J.JACKPOT_STATUS                                                                                               ");
      _sb.AppendLine("			      , J.JACKPOT_MAIN                                                                                                 ");
      _sb.AppendLine("			      , J.JACKPOT_HAPPY_HOUR                                                                                           ");
      _sb.AppendLine("			      , J.JACKPOT_PRIZE_SHARING                                                                                        ");
      _sb.AppendLine("			      , J.JACKPOT_HIDDEN                                                                                               ");
      _sb.AppendLine("			      , J.JACKPOT_ISO_CODE                                                                                             ");
      _sb.AppendLine("            , CASE WHEN JVJ.JVJ_JACKPOT_VIEWER_ID = @pJackpotViewerId THEN 1 ELSE 0 END AS JACKPOT_RELATED                   ");
      _sb.AppendLine("			      , J.JACKPOT_ENABLED                                                                                              ");
      _sb.AppendLine("       FROM   JACKPOTS AS J                                                                                                  ");
      _sb.AppendLine("  LEFT JOIN   JACKPOTS_VIEWERS_RELATED_JACKPOT JVJ                                                                           ");
      _sb.AppendLine("         ON   J.JACKPOT_ID = JVJ.JVJ_JACKPOT_ID                                                                              ");
      _sb.AppendLine(this.DB_GetConditionByType(Type));                                                                                      
      _sb.AppendLine("   ORDER BY   JVJ.JVJ_ORDER, J.JACKPOT_ID                                                                                    ");

      return _sb.ToString();

    } // DB_GetJackpotsViewerItems_Select
        
    private string DB_GetConditionByType(TYPE_JACKPOT_VIEWER_GET Type)
    {
      String _condition; 

      _condition = String.Empty;

      switch(Type)
      {
        case TYPE_JACKPOT_VIEWER_GET.All:
          _condition = "        AND   JVJ.JVJ_JACKPOT_VIEWER_ID = @pJackpotViewerId";
          break;

        case TYPE_JACKPOT_VIEWER_GET.SameViewer:
          _condition = "      WHERE   JVJ.JVJ_JACKPOT_VIEWER_ID = @pJackpotViewerId";
          break;
      }

      return _condition;
    } // DB_GetConditionByType

    #endregion " Private Methods "
    
    #region " Internal Methods "

    /// <summary>
    /// Insert an specific Jackpot Viewer Item in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      SqlParameter _op;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO   JACKPOTS_VIEWERS_RELATED_JACKPOT ");
        _sb.AppendLine("             ( JVJ_JACKPOT_VIEWER_ID            ");
        _sb.AppendLine("             , JVJ_JACKPOT_ID                   ");
        _sb.AppendLine("             , JVJ_ORDER             )          ");
        _sb.AppendLine("      VALUES                                    ");
        _sb.AppendLine("             ( @pJackpotViewerId                ");
        _sb.AppendLine("             , @pJackpotId                      ");
        _sb.AppendLine("             , @pOrder               )          ");

        _sb.AppendLine(" SET     @pId = SCOPE_IDENTITY()                ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotViewerId" , SqlDbType.Int).Value = this.JackpotViewerId;
          _cmd.Parameters.Add("@pJackpotId"       , SqlDbType.Int).Value = this.JackpotId;
          _cmd.Parameters.Add("@pOrder"           , SqlDbType.Int).Value = this.Order;

          //Output parameter
          _op = new SqlParameter("@pId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);
          
          if (_cmd.ExecuteNonQuery() != 1)
          {
            this.Id = (Int32)_op.Value;

            Log.Error(String.Format("JackpotViewerItem.DB_Insert -> JackpotId: {0}", this.JackpotId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewerItem.DB_Insert -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Insert
    
    /// <summary>
    /// Clone JackpotViewerItem
    /// </summary>
    /// <returns></returns>
    internal JackpotViewerItem Clone()
    {
      return new JackpotViewerItem()
      {
        Id              = this.Id,
        JackpotId       = this.JackpotId,
        JackpotViewerId = this.JackpotViewerId,
        Name            = this.Name,
        Order           = this.Order,
        Status          = this.Status,
        IsoCode         = this.IsoCode,
        IsRelated       = this.IsRelated,
        Enabled         = this.Enabled
      };
    } // Clone
    
    #endregion 

  } // JackpotViewerItem
}
