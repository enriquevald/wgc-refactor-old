﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotViewerItemsList.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot Contribution Groups List
// 
//        AUTHOR: Rubén Lama Ordóñez
// 
// CREATION DATE: 17-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-MAY-2017 RLO    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotViewerItemList : JackpotBase
  {
    
    #region " Properties "

    public Int32 JackpotViewerId { get; set; }
    public String Code { get; set; }
    public List<JackpotViewerItem> Items { get; set; }
    public JackpotAwardItemList JackpotsAwards { get; set; }
    
    #endregion " Properties "

    #region " Public Methods "
     
    /// <summary>
    /// Default Constructor
    /// </summary>
    public JackpotViewerItemList()
    {
      this.JackpotViewerId  = DEFAULT_ID_VALUE;
      this.Code             = String.Empty;
      this.Items            = new List<JackpotViewerItem>();
      this.JackpotsAwards   = new JackpotAwardItemList();
      this.LastModification = WGDB.MinDate;
    } // JackpotContributionList
    
    /// <summary>
    /// Get all Jackpot related to Jackpot Viewer
    /// </summary>
    /// <param name="JackpotViewerId"></param>    
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Read(Int32 JackpotViewerId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.Read(JackpotViewerId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      List<JackpotViewerItem> _items;

      if (!new JackpotViewerItem().GetItemsByJackpotViewerId(JackpotViewerId, out _items, SqlTrx))
      {
        return false;
      }

      this.JackpotViewerId = JackpotViewerId;
      this.Items = _items;

      return true;
    } // Read
    
    /// <summary>
    /// Reset all members id (For clonation)
    /// </summary>
    public override void Reset()
    {
      this.JackpotViewerId = DEFAULT_ID_VALUE;

      // Reset all JackpotViewerItem
      foreach (JackpotViewerItem _item in this.Items)
      {
        _item.Reset();
      }

      // Reset all JackpotAwardItem
      foreach (JackpotAwardItem _award in this.JackpotsAwards.Awards)
      {
        _award.Reset();
      }
    } // Reset

    /// <summary>
    /// Reorder all members
    /// </summary>
    public void ReorderItems()
    {
      Int32 _idx;

      for (_idx = 0; _idx < this.Items.Count; _idx++)
      {
        this.Items[_idx].Order = _idx + 1;
      }
    } // ReorderItems
    
    /// <summary>
    /// Save all contributions
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public override Boolean Save(SqlTransaction SqlTrx)
    {
      if (!this.IsModified)
      {
        return true;
      }

      // Remove all items from JackpotViewer (Only if it's update)
      if (!this.DB_InitializeJackpotViewerItems(SqlTrx))
      {
        return false;
      }

      return this.DB_Save(SqlTrx);
    } // Save

    /// <summary>
    /// Clone JackpotAwardPrizeConfig
    /// </summary>
    /// <returns></returns>
    public JackpotViewerItemList Clone()
    {
      return new JackpotViewerItemList()
      {
        JackpotViewerId = this.JackpotViewerId,
        Code            = this.Code,
        Items           = this.Items.Clone(),
        JackpotsAwards  = this.JackpotsAwards.Clone()
      };
    } // Clone

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Delete all items of JackpotViewerId
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_InitializeJackpotViewerItems(SqlTransaction SqlTrx)
    {
      if (this.IsNew)
      {
        return true;
      }

      // Delete all items
      if (!new JackpotViewerItem().DeleteByJackpotViewerId(this.JackpotViewerId, SqlTrx))
      {
        return false;
      }

      return true;
    } // DB_InitializeJackpotViewerItems 

    #endregion " Private Methods "

    #region " Internal Methods "

    /// <summary>
    /// Inset/Update an specific Jackpot contribution Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Save(SqlTransaction SqlTrx)
    {
      Int32 _idx;

      try
      {
        _idx = 0;

        // Save all items
        foreach (JackpotViewerItem _item in this.Items)
        {
          _item.Order = ++_idx;
          _item.JackpotViewerId = this.JackpotViewerId;

          if (!_item.Save(SqlTrx))
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewerItemsList.DB_Save -> JackpotId: {0}", this.JackpotViewerId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Save

    #endregion

  } // JackpotContributionList
}
