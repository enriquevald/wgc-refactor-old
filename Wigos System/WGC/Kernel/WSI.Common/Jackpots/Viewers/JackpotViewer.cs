﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotViewer.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot Viewers
// 
//        AUTHOR: Rubén Lama
// 
// CREATION DATE: 11-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAY-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace WSI.Common.Jackpot
{
  public class JackpotViewer : JackpotBase
  {

    #region " Constants "

    private const Int32 DB_JACKPOT_VIEWER_ID      = 0;
    private const Int32 DB_JACKPOT_VIEWER_NAME    = 1;
    private const Int32 DB_JACKPOT_VIEWER_CODE    = 2;
    private const Int32 DB_JACKPOT_VIEWER_ENABLED = 3;
    private const Int32 DB_LAST_UPDATE            = 4;

    private const Int32 DB_JACKPOT_AWARDS_ID      = 0;

    #endregion

    #region " Members "

    private JackpotViewerItemList m_all_jackpots_list;
    private JackpotViewerTerminalList m_all_terminal_list;
    public List<JackpotAwardItemList> JackpotAwards { get; set; }

    #endregion

    #region " Properties "
    
    public String Name { get; set; }
    public Int32 Code { get; set; }
    public Boolean Enabled { get; set; }
    public List<JackpotAwardItemList> AwardsList { get; set; }

    public JackpotViewerItemList Jackpots { get; set; }    
    public JackpotViewerItemList AllJackpotsList
    {
      get
      {
        if ( this.m_all_jackpots_list == null
          || this.m_all_jackpots_list.Items.Count == 0)
        {
          this.m_all_jackpots_list = this.SetNewItemList();
        }

        return this.m_all_jackpots_list;
      }
      set { this.m_all_jackpots_list = value; }
    }

    public JackpotViewerTerminalList Terminals { get; set; }
    public JackpotViewerTerminalList AllTerminalList
    {
      get
      {
        if ( this.m_all_terminal_list == null
          || this.m_all_terminal_list.Items.Count == 0)
        {
          this.m_all_terminal_list = this.SetNewTerminalList();
        }

        return this.m_all_terminal_list;
      }
      set { this.m_all_terminal_list = value; }
    }

    #endregion " Properties "

    #region " Public Methods "

    #region " Constructors "

    public JackpotViewer()
    {
      this.Id        = DEFAULT_ID_VALUE;
      this.Name      = String.Empty;
      this.Code      = DEFAULT_INT_VALUE;
      this.Enabled   = false;
      this.Jackpots  = new JackpotViewerItemList();
      this.Terminals = new JackpotViewerTerminalList();
    } // JackpotViewer

    public JackpotViewer(Int32 JackpotViewerId)
    {
      if (!this.DB_GetJackpotViewerById(JackpotViewerId))
      {
        Log.Error(String.Format("Error on load JackpotViewer. JackpotViewerId {0}", JackpotViewerId));
      }
    } // JackpotViewer

    public JackpotViewer(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      if (!this.DB_GetJackpotViewerById(JackpotViewerId, SqlTrx))
      {
        Log.Error(String.Format("Error on load JackpotViewer. JackpotViewerId {0}", JackpotViewerId));
      }
    } // JackpotViewer

    #endregion 

    /// <summary>
    /// Same functionality of constructor because this return boolean.
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <returns></returns>
    public Boolean Read(Int32 JackpotViewerCode)
    {
      return this.DB_GetJackpotViewerByCode(JackpotViewerCode);
    } // Read

    /// <summary>
    /// Same functionality of constructor because this return boolean.
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <returns></returns>
    public Boolean ReadAwards(Int32 JackpotViewerCode)
    {
      return this.DB_GetJackpotViewerByCode(JackpotViewerCode, JackpotViewerGetType.OnlySpecialData);
    } // ReadAwards

    /// <summary>
    /// Get all jackpot viewer list
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public List<JackpotViewer> GetAllJackpotsViewers()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.GetAllJackpotsViewers(_db_trx.SqlTransaction);
      }
    }
    public List<JackpotViewer> GetAllJackpotsViewers(SqlTransaction SqlTrx)
    {
      return this.DB_GetAllJackpotsViewers(SqlTrx);
    } // GetAllJackpotsViewers
    
    public Boolean ExistJackpotViewersEnabledAndRelatedToViewer(SqlTransaction SqlTrx)
    {
      return this.DB_ExistEnabledJackpotsViewer(SqlTrx);
    } // ExistJackpotViewersEnabledAndRelatedToViewer

    public List<JackpotViewer> GetEnabledJackpotViewers(SqlTransaction SqlTrx)
    {
      return this.DB_GetEnabledJackpotsViewer(SqlTrx);
    } // GetEnabledJackpotViewers


    /// <summary>
    /// Convert Jackpot viewer item into a TreeViewItem
    /// </summary>
    /// <param name="MenuItem_OnClick"></param>
    /// <returns></returns>
    public TreeViewItem ToTreeViewItem()
    {
      return ToTreeViewItem(null);
    } // ToTreeViewItem
    public TreeViewItem ToTreeViewItem(EventHandler MenuItem_OnClick)
    {
      return new TreeViewItem()
      {
        Id          = this.Id,
        Description = this.Name,
        Status      = GetNodeStatus(),
        Type        = EditSelectionItemType.JACKPOT_VIEWER_CONFIG,
        Nodes       = GetConfigNodes(),
        ContextMenu = null
      };
    } // ToTreeViewItem
    
    /// <summary>
    /// Get context menu of Jackpot Viewer
    /// </summary>
    /// <param name="MenuItem_OnClick"></param>
    /// <returns></returns>
    public ContextMenu GetContextMenu(EventHandler MenuItem_OnClick)
    {
      return null;
    } // GetContextMenu
    
    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.LastUpdate = WGDB.MinDate;
    } // Reset

    /// <summary>
    /// Check if code is unique in JAckpotViewers table
    /// </summary>
    /// <param name="Order"></param>
    /// <returns></returns>
    public Boolean CheckIfCodeIsUnique(Int32 Order)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.DB_CheckIfCodeIsUnique(Order, _db_trx.SqlTransaction);
      }
    } // CheckIfCodeIsUnique

    /// <summary>
    /// Clone JackpotViewer
    /// </summary>
    /// <returns></returns>
    public JackpotViewer Clone()
    {
      return this.Clone_Internal();
    } // Clone
    
    /// <summary>
    /// Return a list with included Jackpots
    /// </summary>
    /// <returns></returns>
    public JackpotViewerItemList GetAvailableJackpots()
    {
      return this.GetSpecificItems_Internal(false);
    } // GetAvailableJackpots

    /// <summary>
    /// Return a list with not included Jackpots
    /// </summary>
    /// <returns></returns>
    public JackpotViewerItemList GetIncludedJackpots()
    {
      return this.GetSpecificItems_Internal(true);
    } // GetIncluded

    /// <summary>
    /// Return a list with included Jackpots
    /// </summary>
    /// <returns></returns>
    public JackpotViewerTerminalList GetAvailableTerminals()
    {
      return this.GetSpecificTerminals_Internal(false);
    } // GetAvailableJackpots

    /// <summary>
    /// Return a list with not included Jackpots
    /// </summary>
    /// <returns></returns>
    public JackpotViewerTerminalList GetIncludedTerminals()
    {
      return this.GetSpecificTerminals_Internal(true);
    } // GetIncluded
        
    #endregion " Public Methods "

    #region " Private Methods "

    #region " DataBase Methods " 

    /// <summary>
    /// Get all jackpot Viewers list
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<JackpotViewer> DB_GetAllJackpotsViewers(SqlTransaction SqlTrx)
    {
      List<JackpotViewer> _jackpot_viewer_list;
      JackpotViewer _jackpot_viewer;

      _jackpot_viewer_list = new List<JackpotViewer>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_JackpotViewerQuery_Select(JackpotViewerFilterType.All), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _jackpot_viewer = this.JackpotViewerMappFromSqlReader(_sql_reader, SqlTrx);

              if (_jackpot_viewer != null)
              {
                _jackpot_viewer_list.Add(_jackpot_viewer);
              }
            }
          }
          
          return _jackpot_viewer_list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotViewers.DB_GetAllJackpotsViewers --> Error on read all Jackpots Viewers list");
        Log.Exception(_ex);
      }

      return null;

    } // DB_GetAllJackpotsViewers

    /// <summary>
    /// Get all jackpot Viewers list
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_ExistEnabledJackpotsViewer(SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_JackpotViewerQuery_Select(JackpotViewerFilterType.EnabledAndRelated), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            return _sql_reader.Read();            
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotViewers.DB_ExistEnabledJackpotsViewer --> Error on read Jackpots Viewers enabled");
        Log.Exception(_ex);
      }

      return false;

    } // DB_ExistEnabledJackpotsViewer

    /// <summary>
    /// Get all jackpot Viewers list
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<JackpotViewer> DB_GetEnabledJackpotsViewer(SqlTransaction SqlTrx)
    {
      List<JackpotViewer> _enabled_jackpot_viewer_list;
      JackpotViewer _jackpot_viewer;

      _enabled_jackpot_viewer_list = new List<JackpotViewer>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_JackpotViewerQuery_Select(JackpotViewerFilterType.Enabled), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while(_sql_reader.Read())
            {
              _jackpot_viewer = this.JackpotViewerMappFromSqlReader(_sql_reader, SqlTrx);

              if (_jackpot_viewer != null)
              {
                _enabled_jackpot_viewer_list.Add(_jackpot_viewer);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotViewers.DB_ExistEnabledJackpotsViewer --> Error on read Jackpots Viewers enabled");
        Log.Exception(_ex);
      }

      return _enabled_jackpot_viewer_list;
    
    } // DB_ExistEnabledJackpotsViewer

    /// <summary>
    /// Get data from specific Jackpot Viewer
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotViewerById(Int32 JackpotViewerId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_GetJackpotViewerById(JackpotViewerId, _db_trx.SqlTransaction);
      }
    } // DB_GetJackpotViewerById  
    private Boolean DB_GetJackpotViewerById(Int32 JackpotViewerId, JackpotViewerGetType GetType)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_GetJackpotViewerById(JackpotViewerId, GetType, _db_trx.SqlTransaction);
      }
    } // DB_GetJackpotViewerById  
    private Boolean DB_GetJackpotViewerById(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      return DB_GetJackpotViewerById(JackpotViewerId, JackpotViewerGetType.All, SqlTrx);
    } // DB_GetJackpotViewerById
    private Boolean DB_GetJackpotViewerById(Int32 JackpotViewerId, JackpotViewerGetType GetType, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_JackpotViewerQuery_Select(JackpotViewerFilterType.ByJackpotViewerId), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotViewerId", SqlDbType.Int).Value = JackpotViewerId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if ( _sql_reader.Read()
              && !this.DB_JackpotViewerDataMapper(_sql_reader))
            {
              return false;
            }
          }

          //Read special data 
          if (!this.DB_ReadSpecialData(JackpotViewerId, GetType, SqlTrx))
          {
            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewer.DB_GetJackpotViewerById -> JackpotViewerId: {0}", JackpotViewerId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotViewerById

    /// <summary>
    /// Get data from specific Jackpot Viewer
    /// </summary>
    /// <param name="JackpotViewerCode"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotViewerByCode(Int32 JackpotViewerCode)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_GetJackpotViewerByCode(JackpotViewerCode, _db_trx.SqlTransaction);
      }
    } // DB_GetJackpotViewerByCode  
    private Boolean DB_GetJackpotViewerByCode(Int32 JackpotViewerCode, JackpotViewerGetType GetType)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_GetJackpotViewerByCode(JackpotViewerCode, GetType, _db_trx.SqlTransaction);
      }
    } // DB_GetJackpotViewerByCode  
    private Boolean DB_GetJackpotViewerByCode(Int32 JackpotViewerCode, SqlTransaction SqlTrx)
    {
      return DB_GetJackpotViewerByCode(JackpotViewerCode, JackpotViewerGetType.All, SqlTrx);
    } // DB_GetJackpotViewerByCode
    private Boolean DB_GetJackpotViewerByCode(Int32 JackpotViewerCode, JackpotViewerGetType GetType, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_JackpotViewerQuery_Select(JackpotViewerFilterType.ByJackpotViewerCode), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotViewerCode", SqlDbType.Int).Value = JackpotViewerCode;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if ( _sql_reader.Read()
              && !this.DB_JackpotViewerDataMapper(_sql_reader))
            {
              return false;
            }
          }

          //Read special data 
          if (!this.DB_ReadSpecialData(this.Id, GetType, SqlTrx))
          {
            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewer.DB_GetJackpotViewerByCode -> JackpotViewerCode: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotViewerByCode

    /// <summary>
    /// Read special Jackpot Viewer members:
    ///   - JackpotViewerItemList
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_ReadSpecialData(Int32 JackpotViewerId, JackpotViewerGetType Type, SqlTransaction SqlTrx)
    {
      this.Jackpots       = new JackpotViewerItemList();
      this.Terminals      = new JackpotViewerTerminalList();
      this.JackpotAwards  = new List<JackpotAwardItemList>();
      
      switch (Type)
      { 
        case JackpotViewerGetType.All:
          ReadAllSpecialData(JackpotViewerId, false, SqlTrx);
          break;

        case JackpotViewerGetType.OnlySpecialData:
          ReadAllSpecialData(JackpotViewerId, true, SqlTrx);
          break;
        case JackpotViewerGetType.OnlyLastAwards:
          this.JackpotAwards = ReadLastAwards(JackpotViewerId, SqlTrx);
          break; 
      }
     
      return true;

    } // DB_ReadSpecialData

    /// <summary>
    /// Read last awards
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<JackpotAwardItemList> ReadLastAwards(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      List<JackpotAwardItemList> list = new List<JackpotAwardItemList>();
      
      this.JackpotAwards = this.GetAllJackpotsAwards(JackpotViewerId, SqlTrx);

      if (this.JackpotAwards == null)
      {
        return list;
      }

      foreach (JackpotAwardItemList _item in this.JackpotAwards)
      {
        // Read Jackpot Viewer awards
        if (_item.Read(_item.AwardId, SqlTrx))
        {
          list.Add(_item);
        }
      }
      return list;
    } // ReadLastAwards

    /// <summary>
    /// GetAllJackpotsAwards
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<JackpotAwardItemList> GetAllJackpotsAwards(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      List<JackpotAwardItemList> _list;

      _list = new List<JackpotAwardItemList>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_JackpotsAwardsQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotViewerId", SqlDbType.Int).Value = JackpotViewerId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _list.Add(new JackpotAwardItemList() { AwardId = _sql_reader.GetInt64(DB_JACKPOT_AWARDS_ID) } );
            }
          }

          return _list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("JackpotViewers.DB_GetAllJackpotsViewers --> Error on read all Jackpots Viewers list");
        Log.Exception(_ex);
      }

      return null;

    } // GetAllJackpotsAwards

    /// <summary>
    /// Readl all special data in DB_ReadSpecialData
    /// </summary>
    private void ReadAllSpecialData(Int32 JackpotViewerId, Boolean GetLastAwards, SqlTransaction SqlTrx)
    {
      // Read Jackpot Viewer Items
      if (!this.Jackpots.Read(JackpotViewerId, SqlTrx))
      {
        return;
      }

      // Read Jackpot Viewer terminal Items
      if (!this.Terminals.Read(JackpotViewerId, SqlTrx))
      {
        return;
      }

      // Read Jackpot Viewer Jackpots last awards
      if (GetLastAwards)
      {
        this.ReadLastAwards(JackpotViewerId, SqlTrx);
      }        

    } // ReadAllSpecialData
    
    /// <summary>
    /// Load Jackpot Viewer data from SQLReader
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_JackpotViewerDataMapper(SqlDataReader SqlReader)
    {
      return DB_JackpotViewerDataMapper(this, SqlReader);
    } // DB_JackpotViewerDataMapper
    private Boolean DB_JackpotViewerDataMapper(JackpotViewer JackpotViewer, SqlDataReader SqlReader)
    {
      try
      {
        JackpotViewer.Id         = SqlReader.GetInt32(DB_JACKPOT_VIEWER_ID);
        JackpotViewer.Name       = SqlReader.GetString(DB_JACKPOT_VIEWER_NAME);
        JackpotViewer.Code       = SqlReader.GetInt32(DB_JACKPOT_VIEWER_CODE);
        JackpotViewer.Enabled    = SqlReader.GetBoolean(DB_JACKPOT_VIEWER_ENABLED);                
        JackpotViewer.LastUpdate = SqlReader.GetDateTime(DB_LAST_UPDATE);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotViewerDataMapper");
        Log.Exception(_ex);
      }

      JackpotViewer = null;

      return false;

    } // DB_JackpotViewerDataMapper    

    /// <summary>
    /// Mapp jackpot from sql reader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private JackpotViewer JackpotViewerMappFromSqlReader(SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      JackpotViewer _jackpot_viewer;

      _jackpot_viewer = new JackpotViewer();

      if (!this.DB_JackpotViewerDataMapper(_jackpot_viewer, SqlReader))
      {
        Log.Error(String.Format("JackpotViewer.JackpotViewerMappFromSqlReader -> Error on load JackpotViewerId: {0}", SqlReader.GetInt32(DB_JACKPOT_VIEWER_ID)));
      }

      return _jackpot_viewer;
    } // JackpotMappFromSqlReader

    /// <summary>
    /// Gets Jackpot select query
    /// </summary>
    /// <param name="GetAllJackpots"></param>
    /// <returns></returns>
    private String DB_JackpotViewerQuery_Select(JackpotViewerFilterType Filter)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("  SELECT   JV_JACKPOT_VIEWER_ID "); // 0
      _sb.AppendLine("         , JV_NAME              "); // 1
      _sb.AppendLine("         , JV_CODE              "); // 2
      _sb.AppendLine("         , JV_ENABLED           "); // 3
      _sb.AppendLine("         , JV_LAST_UPDATE       "); // 4
      _sb.AppendLine("    FROM   JACKPOTS_VIEWERS     ");

      // Set filter
      _sb.AppendLine(DB_GetJackpotViewerFilterSQL(Filter));
      
      return _sb.ToString();

    } // DB_JackpotViewerQuery_Select  

    /// <summary>
    /// Get SQL Filter of Jackpots table
    /// </summary>
    /// <param name="Filter"></param>
    /// <returns></returns>
    private string DB_GetJackpotViewerFilterSQL(JackpotViewerFilterType Filter)
      {
      StringBuilder _sb;

      switch(Filter)
      {
        case JackpotViewerFilterType.ByJackpotViewerId:
          // By Jackpot Id
          return "     WHERE   JV_JACKPOT_VIEWER_ID = @pJackpotViewerId";

        case JackpotViewerFilterType.Enabled:
          return "     WHERE   JV_ENABLED = 1";

        case JackpotViewerFilterType.EnabledAndRelated:
        {
          // Only enabled and related JackpotsViewers
          _sb = new StringBuilder();
          _sb.AppendLine(" INNER JOIN   JACKPOTS_VIEWERS_RELATED_TERMINAL ON JVT_JACKPOT_VIEWER_ID = JV_JACKPOT_VIEWER_ID");
          _sb.AppendLine("      WHERE   JV_ENABLED = 1");

          return _sb.ToString();
        }

        case JackpotViewerFilterType.ByJackpotViewerCode:
          //By JackpotViewer code
          return "     WHERE   JV_CODE = @pJackpotViewerCode";

        case JackpotViewerFilterType.All:
        default:
          // All Jackpots
          break;
      }
      
      return String.Empty;

    } // DB_GetJackpotFilterSQL

    /// <summary>
    /// Gets Jackpot select query
    /// </summary>
    /// <param name="GetAllJackpots"></param>
    /// <returns></returns>
    private String DB_JackpotsAwardsQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   JA_ID                                         "); // 0
      _sb.AppendLine("       FROM   JACKPOTS_VIEWERS                              "); // 1
      _sb.AppendLine(" INNER JOIN   JACKPOTS_VIEWERS_RELATED_JACKPOT              "); // 2
      _sb.AppendLine("         ON   JVJ_JACKPOT_VIEWER_ID = JV_JACKPOT_VIEWER_ID  "); // 3
      _sb.AppendLine(" INNER JOIN   JACKPOTS_AWARD                                "); // 4
      _sb.AppendLine("         ON   JVJ_JACKPOT_ID        = JA_JACKPOT_ID         ");
      _sb.AppendLine("      WHERE   JV_JACKPOT_VIEWER_ID  = @pJackpotViewerId     ");

      return _sb.ToString();

    } // DB_JackpotsAwardsQuery_Select  

    /// <summary>
    /// Check if code is unique
    /// </summary>
    /// <param name="Order"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_CheckIfCodeIsUnique(Int32 Code, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" SELECT   JV_CODE                         ");
        _sb.AppendLine("   FROM   JACKPOTS_VIEWERS                ");
        _sb.AppendLine("  WHERE   JV_CODE               = @pCode  ");
        _sb.AppendLine("    AND   JV_JACKPOT_VIEWER_ID  != @pId   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId"  , SqlDbType.Int).Value = this.Id;
          _cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = Code;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            // If read, not unique. 
            return (!_sql_reader.Read());
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewer.DB_CheckIfCodeIsUnique -> JackpotViewerId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_CheckIfCodeIsUnique
                                                                                                                                    
    #endregion 

    #region " Common Methods "
    
    /// <summary>
    /// Set fixed nodes to configure single Jackpot
    /// </summary>
    /// <returns></returns>
    private List<TreeViewItem> GetConfigNodes()
    {
      return new List<TreeViewItem>()
      {
        new TreeViewItem(this.Id, Resource.String("STR_JACKPOT_VIEWER_NODE_MONITOR") , EditSelectionItemType.JACKPOT_VIEWER_MONITOR , NodeStatus.Enabled),
        new TreeViewItem(this.Id, Resource.String("STR_JACKPOT_VIEWER_NODE_TERMINAL"), EditSelectionItemType.JACKPOT_VIEWER_TERMINAL, NodeStatus.Enabled)
      };
    } // GetConfigNodes
    
    /// <summary>
    /// Translate enabled to NodeStatus
    /// </summary>
    /// <returns></returns>
    private NodeStatus GetNodeStatus()
    {
      return (this.Enabled) ? NodeStatus.Enabled : NodeStatus.Disabled;
    } // GetNodeStatus

    #endregion

    #region " JackpotViewerItemList Methods" 

    /// <summary>
    /// Get specific Jackpots 
    /// </summary>
    /// <param name="Type"></param>
    /// <returns></returns>
    private JackpotViewerItemList GetSpecificItems_Internal(Boolean IsRelated)
    {
      JackpotViewerItemList _items;

      _items = new JackpotViewerItemList();
      _items.JackpotViewerId = this.Id;

      foreach (JackpotViewerItem _item in this.AllJackpotsList.Items)
      {
        if (_item.IsRelated == IsRelated)
        {
          _items.Items.Add(_item);
        }
      }

      return _items;
    } // GetSpecificItems_Internal

    /// <summary>
    /// Get specific Jackpots 
    /// </summary>
    /// <returns></returns>
    private JackpotViewerItemList SetNewItemList()
    {
      JackpotViewerItemList _item_list;

      _item_list = new JackpotViewerItemList();

      _item_list.JackpotViewerId = this.Id;
      _item_list.Items = new JackpotViewerItem().GetAllItemsAvailables(this.Id);

      return _item_list;

    } // SetNewItemList

    #endregion

    #region "JackpotViewerTerminalList Methods"

    /// <summary>
    /// Get specific Jackpots 
    /// </summary>
    /// <param name="Type"></param>
    /// <returns></returns>
    private JackpotViewerTerminalList GetSpecificTerminals_Internal(Boolean IsRelated)
    {
      JackpotViewerTerminalList _items;

      _items = new JackpotViewerTerminalList();
      _items.JackpotViewerId = this.Id;

      foreach (JackpotViewerTerminalItem _item in this.AllTerminalList.Items)
      {
        if (_item.IsRelated == IsRelated)
        {
          _items.Items.Add(_item);
        }
      }

      return _items;
    } // GetSpecificTerminals_Internal

    /// <summary>
    /// Get specific Terminals 
    /// </summary>
    /// <returns></returns>
    private JackpotViewerTerminalList SetNewTerminalList()
    {
      JackpotViewerTerminalList _terminal_list;

      _terminal_list = new JackpotViewerTerminalList();

      _terminal_list.JackpotViewerId = this.Id;
      _terminal_list.Items = new JackpotViewerTerminalItem().GetAllITerminalAvailables(this.Id);

      return _terminal_list;

    } // SetNewTerminalList

    #endregion

    #endregion

    #region " Internal Methods "

            
    /// <summary>
    /// Save special transaction properties
    /// </summary>
    /// <returns></returns>
    internal override Boolean DB_SaveSpecialProperties(SqlTransaction SqlTrx)
    {
      if(!this.Jackpots.Save(SqlTrx))
      {
        return false;
      }

      if (!this.Terminals.Save(SqlTrx))
      {
        return false;
      }

      return true;
    } // DB_SaveSpecialProperties 
    
    /// <summary>
    /// Insert an specific Jackpot in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _op;

      _sb = new StringBuilder();

      try
      {        
        _sb.AppendLine(" INSERT INTO    JACKPOTS_VIEWERS  ");
        _sb.AppendLine("              ( JV_NAME           ");
        _sb.AppendLine("              , JV_CODE           ");
        _sb.AppendLine("              , JV_ENABLED        ");
        _sb.AppendLine("              , JV_LAST_UPDATE  ) ");
        _sb.AppendLine("      VALUES                      ");
        _sb.AppendLine("              ( @pName            ");
        _sb.AppendLine("              , @pCode            ");
        _sb.AppendLine("              , @pEnabled         ");
        _sb.AppendLine("              , @pLastUpdate    ) ");

        _sb.AppendLine(" SET  @pJackpotViewerId = SCOPE_IDENTITY()  ");
        
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {

          _cmd.Parameters.Add("@pName"        , SqlDbType.NVarChar).Value = this.Name;
          _cmd.Parameters.Add("@pCode"        , SqlDbType.Int     ).Value = this.Code;
          _cmd.Parameters.Add("@pEnabled"     , SqlDbType.Bit     ).Value = this.Enabled;

          // Last update:
          //   - In JackpotViewer main class Update when create JackpotViewer
          this.LastUpdate = WGDB.Now;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;
          
          // Output parameter
          _op = new SqlParameter("@pJackpotViewerId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);

          if (_cmd.ExecuteNonQuery() == 1)
          {
            this.Id = (Int32)_op.Value;

            this.Jackpots.JackpotViewerId  = this.Id;
            this.Terminals.JackpotViewerId = this.Id;
            
            if (!this.DB_SaveSpecialProperties(SqlTrx))
            {
              return false;
            }

            return true;
          }

          Log.Error(String.Format("JackpotViewer.DB_Insert -> Jackpot Viewer: {0}", this.Name));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewer.DB_Insert -> Jackpot Viewer: {0}", this.Name));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Jackpot Viewer in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" UPDATE   JACKPOTS_VIEWERS                                ");
        _sb.AppendLine("    SET   JV_NAME                     = @pName            ");
        _sb.AppendLine("        , JV_ENABLED                  = @pEnabled         ");
        _sb.AppendLine("        , JV_CODE                     = @pCode            ");
        _sb.AppendLine("        , JV_LAST_UPDATE              = @pLastUpdate      ");
        _sb.AppendLine("  WHERE   JV_JACKPOT_VIEWER_ID        = @pJackpotViewerId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {          
          _cmd.Parameters.Add("@pName"            , SqlDbType.NVarChar).Value = this.Name;
          _cmd.Parameters.Add("@pEnabled"         , SqlDbType.Bit     ).Value = this.Enabled;
          _cmd.Parameters.Add("@pCode"            , SqlDbType.Int     ).Value = this.Code;
          _cmd.Parameters.Add("@pJackpotViewerId" , SqlDbType.Int     ).Value = this.Id;

          // Last update:
          this.LastUpdate = this.LastModification;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            if (!this.DB_SaveSpecialProperties(SqlTrx))
            {
              return false;
            }

            return true;
          }

          Log.Error(String.Format("JackpotViewer.DB_Update -> JackpotViewerId: {0}", this.Id));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewer.DB_Update -> JackpotViewerId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Update

    /// <summary>
    /// Clone JackpotViewer Internal
    /// </summary>
    /// <returns></returns>
    internal JackpotViewer Clone_Internal()
    {
      return new JackpotViewer()
      {
        Id          = this.Id,
        Name        = this.Name,
        Code        = this.Code,
        Enabled     = this.Enabled,
        LastUpdate  = this.LastUpdate,
        Jackpots    = this.Jackpots.Clone(),
        Terminals   = this.Terminals.Clone()
      };
    } // Clone_Internal

    #endregion
        
  }
}
