﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotViewerItemParameters : JackpotBase
  {

    #region " Constants "

    //private const Int32 DB_DAYS = 0;
    //private const Int32 DB_AWARDING_START = 1;
    //private const Int32 DB_AWARDING_END = 2;
    private const Int32 DB_EXCLUDE_PROMOTIONS  = 0;
    private const Int32 DB_EXCLUDE_ANONYMOUS   = 1;
    private const Int32 DB_MIN_OCCUPATION_PCT  = 2;
    private const Int32 DB_TO_COMPENSATE       = 3;
    private const Int32 DB_SUM_MINIMUM         = 4;
    private const Int32 DB_JACKPOT_INDEX       = 5;
    private const Int32 DB_ACCUMULATED         = 6;
    private const Int32 DB_MINIMUM             = 7;
    private const Int32 DB_MAXIMUM             = 8;
    private const Int32 DB_AVERAGE             = 9;
    private const Int32 DB_NAME                = 10;
    private const Int32 DB_MINIMUM_BET         = 11;
    private const Int32 DB_CONTRIBUTION_PCT    = 12;
    private const Int32 DB_CONTR_PCT_MONDAY    = 13;
    private const Int32 DB_CONTR_PCT_TUESDAY   = 14;
    private const Int32 DB_CONTR_PCT_WEDNESDAY = 15;
    private const Int32 DB_CONTR_PCT_THURSDAY  = 16;
    private const Int32 DB_CONTR_PCT_FRIDAY    = 17;
    private const Int32 DB_CONTR_PCT_SATURDAY  = 18;
    private const Int32 DB_CONTR_PCT_SUNDAY    = 19;
    private const Int32 DB_NUM_PENDING         = 20;
    private const Int32 DB_PLAYING             = 21;
    
    #endregion

    #region " Members "

    #endregion

    #region "Properties"

    public Int32 Days { get; set; }
    public Int32 AwardingStart { get; set; }
    public Int32 AwardingEnd { get; set; }
    public Boolean ExcludePromotions { get; set; }
    public Boolean ExcludeAnonymous { get; set; }
    public Decimal MinOccupancyPct { get; set; }
    public Decimal ToCompensate { get; set; }
    public Decimal SumMinimum { get; set; }
    public Int32 JackpotIndex { get; set; }
    public Decimal Accumulated { get; set; }
    public Decimal Minimum { get; set; }
    public Decimal Maximum { get; set; }
    public Decimal Average { get; set; }
    public String Name { get; set; }
    public Decimal MinimumBet { get; set; }
    public Decimal ContributionPct { get; set; }
    public Decimal ContrPctMonday { get; set; }
    public Decimal ContrPctTuesday { get; set; }
    public Decimal ContrPctWednesday { get; set; }
    public Decimal ContrPctThursday { get; set; }
    public Decimal ContrPctFriday { get; set; }
    public Decimal ContrPctSaturday { get; set; }
    public Decimal ContrPctSunday { get; set; }
    public Int32 NumPending { get; set; }
    public Int32 Playing { get; set; }

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Constructor's by Jackpot Viewer Parameters
    /// </summary>
    public JackpotViewerItemParameters()
    {
      Days              = DEFAULT_INT_VALUE;
      AwardingStart     = DEFAULT_INT_VALUE;
      AwardingEnd       = DEFAULT_INT_VALUE;
      ExcludePromotions = false;
      ExcludeAnonymous  = false;
      MinOccupancyPct   = DEFAULT_DECIMAL_VALUE;
      ToCompensate      = DEFAULT_DECIMAL_VALUE;
      SumMinimum        = DEFAULT_DECIMAL_VALUE;
      JackpotIndex      = DEFAULT_INT_VALUE;
      Accumulated       = DEFAULT_DECIMAL_VALUE;
      Minimum           = DEFAULT_DECIMAL_VALUE;
      Average           = DEFAULT_DECIMAL_VALUE;
      Name              = String.Empty;
      MinimumBet        = DEFAULT_DECIMAL_VALUE;
      ContributionPct   = DEFAULT_DECIMAL_VALUE;
      ContrPctMonday    = DEFAULT_DECIMAL_VALUE;
      ContrPctTuesday   = DEFAULT_DECIMAL_VALUE;
      ContrPctWednesday = DEFAULT_DECIMAL_VALUE;
      ContrPctThursday  = DEFAULT_DECIMAL_VALUE; 
      ContrPctFriday    = DEFAULT_DECIMAL_VALUE;
      ContrPctSaturday  = DEFAULT_DECIMAL_VALUE;
      ContrPctSunday    = DEFAULT_DECIMAL_VALUE;
      NumPending        = DEFAULT_INT_VALUE;
      Playing           = DEFAULT_INT_VALUE;
    }

    public List<JackpotViewerItemParameters> GetJackpotViewerParametersList(int JackpotViewerId, SqlTransaction Trx)
    {
      List<JackpotViewerItemParameters> _items;
     
      if (this.DB_GetJackpotViewerParametersList(Trx, JackpotViewerId, out _items))
      {
        return _items;
      }
      
      return null;
    }

    public Decimal GetWorkindDayPct(DayOfWeek WorkingDay)
    {
      switch(WorkingDay)
      {
        case DayOfWeek.Monday:
          return ContrPctMonday;

        case DayOfWeek.Tuesday:
          return ContrPctTuesday;

        case DayOfWeek.Wednesday:
          return ContrPctWednesday;

        case DayOfWeek.Thursday:
          return ContrPctThursday;

        case DayOfWeek.Friday:
          return ContrPctFriday;

        case DayOfWeek.Saturday:
          return ContrPctSaturday;

        case DayOfWeek.Sunday:
          return ContrPctSunday;

        default:
          return 0;
      }
    }

    #endregion

    #region "Private Methods"

    private Boolean DB_GetJackpotViewerParametersList(SqlTransaction Trx, int JackpotViewerId, out List<JackpotViewerItemParameters> JackpotViewerItemParamsList)
    {
      JackpotViewerItemParamsList = new List<JackpotViewerItemParameters>();

      try
      {

        using (SqlCommand _cmd = new SqlCommand(DB_GetJackpotsViewerItemParametersList_Select(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pJackpotViewerId", SqlDbType.Int).Value = JackpotViewerId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              JackpotViewerItemParameters _jackpot_params = new JackpotViewerItemParameters();

              if (!DB_JackpotViewerItemParametersDataMapper(_jackpot_params, _sql_reader))
              {
                return false;
              }
      
              JackpotViewerItemParamsList.Add(_jackpot_params);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    private String DB_GetJackpotsViewerItemParametersList_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("WITH CONTRIBUTIONS AS (                                                                                                ");
      _sb.AppendLine("	    SELECT    JS_JACKPOT_ID														                           AS JACKPOT_ID                       ");
      _sb.AppendLine("			        , JS_MINIMUM											                                   AS MINIMUM                          ");
      _sb.AppendLine("		          , JS_MAXIMUM											                                   AS MAXIMUM                          ");
      _sb.AppendLine("		          , JS_AVERAGE											                                   AS AVERAGE                          ");
      _sb.AppendLine("		          , JS_NAME											                                       AS NAME                             ");
      _sb.AppendLine("		          , ISNULL(SUM(CASE WHEN JCM_TYPE = 4 THEN  JCM_AMOUNT ELSE 0 END), 0) AS TO_COMPENSATE                    ");
      _sb.AppendLine("		          , ISNULL(SUM(CASE WHEN JCM_TYPE = 0 THEN  JCM_AMOUNT ELSE 0 END), 0) AS ACCUMULATED                      ");
      _sb.AppendLine("		          , JS_COMPENSATION_FIXED_PCT											                     AS CONTRIBUTION_PCT                 ");
      _sb.AppendLine("              , JSC_MONDAY															                           AS CONTR_PCT_MONDAY                 ");
      _sb.AppendLine("              , JSC_TUESDAY														                             AS CONTR_PCT_TUESDAY                ");
      _sb.AppendLine("              , JSC_WEDNESDAY														                           AS CONTR_PCT_WEDNESDAY              ");
      _sb.AppendLine("              , JSC_THURSDAY														                           AS CONTR_PCT_THURSDAY               ");
      _sb.AppendLine("              , JSC_FRIDAY															                           AS CONTR_PCT_FRIDAY                 ");
      _sb.AppendLine("	            , JSC_SATURDAY														                           AS CONTR_PCT_SATURDAY               ");
      _sb.AppendLine("			        , JSC_SUNDAY															                           AS CONTR_PCT_SUNDAY                 ");			   			   			   			  			   
      _sb.AppendLine("		          , JS_NUM_PENDING														                         AS NUM_PENDING                      ");
      _sb.AppendLine("	      FROM    JACKPOTS_SETTINGS                                                                                      ");
      _sb.AppendLine("	INNER JOIN    JACKPOTS_SETTINGS_CONTRIBUTION ON JSC_JACKPOT_ID = JS_JACKPOT_ID                                       ");
      _sb.AppendLine("   LEFT JOIN    JACKPOTS_CONTRIBUTION_METERS   ON JCM_JACKPOT_ID = JS_JACKPOT_ID                                         ");
      _sb.AppendLine("       WHERE    JCM_TYPE IN (0, 4)                                                                                     ");
      _sb.AppendLine("    GROUP BY    JS_JACKPOT_ID, JS_MINIMUM, JS_MAXIMUM, JS_AVERAGE, JS_NAME, JS_COMPENSATION_FIXED_PCT, JS_NUM_PENDING, JSC_MONDAY, JSC_TUESDAY, JSC_WEDNESDAY, JSC_THURSDAY, JSC_FRIDAY, JSC_SATURDAY, JSC_SUNDAY  ");
      _sb.AppendLine(")                                                                                                                      ");
      _sb.AppendLine("      SELECT    JAF_EXCLUDE_ACCOUNTS_WITH_PROMO	 AS EXCLUDE_ACCOUNTS_WITH_PROMO                                        ");
      _sb.AppendLine("		          , JAF_EXCLUDE_ANONYMOUS_ACCOUNTS	 AS EXCLUDE_ANONYMOUS_ACCOUNTS                                         ");
      _sb.AppendLine("	            , JAF_MIN_OCCUPANCY								 AS MIN_OCCUPPANCY                                                     ");      
      _sb.AppendLine("		          , C.TO_COMPENSATE                  AS TO_COMPENSATE                                                      ");
      _sb.AppendLine("		          , C.MINIMUM											   AS SUM_MINIMUM                                                        ");
      _sb.AppendLine("		          , JVJ_ORDER											   AS JACKPOT_INDEX                                                      ");
      _sb.AppendLine("		          , C.ACCUMULATED                    AS ACCUMULATED                                                        ");
      _sb.AppendLine("		          , C.MINIMUM											   AS MINIMUM                                                            ");
      _sb.AppendLine("		          , C.MAXIMUM											   AS MAXIMUM                                                            ");
      _sb.AppendLine("		          , C.AVERAGE											   AS AVERAGE                                                            ");
      _sb.AppendLine("		          , C.NAME											     AS NAME                                                               ");
      _sb.AppendLine("		          , JAF_MINIMUM_BET									 AS MINIMUM_BET                                                        ");
      _sb.AppendLine("		          , C.CONTRIBUTION_PCT							 AS CONTRIBUTION_PCT                                                   ");
      _sb.AppendLine("              , C.CONTR_PCT_MONDAY							 AS CONTR_PCT_MONDAY                                                   ");
      _sb.AppendLine("              , C.CONTR_PCT_TUESDAY   					 AS CONTR_PCT_TUESDAY                                                  ");
      _sb.AppendLine("              , C.CONTR_PCT_WEDNESDAY						 AS CONTR_PCT_WEDNESDAY                                                ");
      _sb.AppendLine("              , C.CONTR_PCT_THURSDAY						 AS CONTR_PCT_THURSDAY                                                 ");
      _sb.AppendLine("              , C.CONTR_PCT_FRIDAY							 AS CONTR_PCT_FRIDAY                                                   ");
      _sb.AppendLine("              , C.CONTR_PCT_SATURDAY						 AS CONTR_PCT_SATURDAY                                                 ");
      _sb.AppendLine("              , C.CONTR_PCT_SUNDAY							 AS CONTR_PCT_SUNDAY                                                   ");
      _sb.AppendLine("		          , C.NUM_PENDING										 AS NUM_PENDING                                                        ");
      _sb.AppendLine("              , ( SELECT COUNT(1)                                                                                      ");      
			_sb.AppendLine("                  		  FROM  ( SELECT DISTINCT PS_TERMINAL_ID FROM PLAY_SESSIONS WITH (INDEX (IX_PS_STATUS))          ");
			_sb.AppendLine("                  INNER JOIN    TERMINAL_GROUPS ON TG_TERMINAL_ID = PS_TERMINAL_ID AND TG_ELEMENT_ID = 1               ");
			_sb.AppendLine("                  		   AND    TG_ELEMENT_TYPE = 4 WHERE PS_STATUS = 0                                                ");
			_sb.AppendLine("                  		 UNION    SELECT DISTINCT PS_TERMINAL_ID FROM PLAY_SESSIONS WITH (INDEX (IX_PS_FINISHED_STATUS)) ");
			_sb.AppendLine("                  INNER JOIN    TERMINAL_GROUPS ON TG_TERMINAL_ID = PS_TERMINAL_ID AND TG_ELEMENT_ID = 1               ");
			_sb.AppendLine("                  	  	 AND    TG_ELEMENT_TYPE = 4 WHERE PS_FINISHED >= DATEADD (MINUTE, -10, GETDATE())              ");
			_sb.AppendLine("                  		   AND    PS_STATUS <> 0 ) PLAYING ) AS PLAYING                                    ");
      _sb.AppendLine("                        FROM	  CONTRIBUTIONS                    AS C                                                  ");
      _sb.AppendLine("   LEFT JOIN    JACKPOTS_AWARD_FILTERS			     ON JAF_JACKPOT_ID = C.JACKPOT_ID                                      ");
      _sb.AppendLine("   LEFT JOIN    JACKPOTS_VIEWERS_RELATED_JACKPOT ON JVJ_JACKPOT_ID = C.JACKPOT_ID                                      ");
      _sb.AppendLine("       WHERE    JVJ_JACKPOT_VIEWER_ID = @pJackpotViewerId                                                              ");

      return _sb.ToString();
    }

    /// <summary>
    /// Load Jackpot Viewer item parameters data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotViewerItemParametersDataMapper(JackpotViewerItemParameters JackpotViewerItemParameters, SqlDataReader SqlReader)
    {
      try
      {
        JackpotViewerItemParameters.Days              = 7;//SqlReader.GetInt32(DB_ID);
        JackpotViewerItemParameters.AwardingStart     = 3600;
        JackpotViewerItemParameters.AwardingEnd       = 3420;    
        JackpotViewerItemParameters.ExcludePromotions = SqlReader.GetBoolean(DB_EXCLUDE_PROMOTIONS);
        JackpotViewerItemParameters.ExcludeAnonymous  = SqlReader.GetBoolean(DB_EXCLUDE_ANONYMOUS);     
        JackpotViewerItemParameters.MinOccupancyPct   = SqlReader.GetDecimal(DB_MIN_OCCUPATION_PCT);
        JackpotViewerItemParameters.ToCompensate      = SqlReader.GetDecimal(DB_TO_COMPENSATE);
        JackpotViewerItemParameters.SumMinimum        = SqlReader.GetDecimal(DB_SUM_MINIMUM);
        JackpotViewerItemParameters.JackpotIndex      = SqlReader.GetInt32(DB_JACKPOT_INDEX);
        JackpotViewerItemParameters.Accumulated       = SqlReader.GetDecimal(DB_ACCUMULATED);
        JackpotViewerItemParameters.Minimum           = SqlReader.GetDecimal(DB_MINIMUM);
        JackpotViewerItemParameters.Maximum           = SqlReader.GetDecimal(DB_MAXIMUM);
        JackpotViewerItemParameters.Average           = SqlReader.GetDecimal(DB_AVERAGE);
        JackpotViewerItemParameters.Name              = SqlReader.GetString(DB_NAME);
        JackpotViewerItemParameters.MinimumBet        = SqlReader.GetDecimal(DB_MINIMUM_BET);
        JackpotViewerItemParameters.ContributionPct   = SqlReader.GetDecimal(DB_CONTRIBUTION_PCT);
        JackpotViewerItemParameters.ContrPctMonday    = (SqlReader.IsDBNull(DB_CONTR_PCT_MONDAY))    ? Jackpot.DEFAULT_ID_VALUE : SqlReader.GetDecimal(DB_CONTR_PCT_MONDAY);
        JackpotViewerItemParameters.ContrPctTuesday   = (SqlReader.IsDBNull(DB_CONTR_PCT_TUESDAY))   ? Jackpot.DEFAULT_ID_VALUE : SqlReader.GetDecimal(DB_CONTR_PCT_TUESDAY);
        JackpotViewerItemParameters.ContrPctWednesday = (SqlReader.IsDBNull(DB_CONTR_PCT_WEDNESDAY)) ? Jackpot.DEFAULT_ID_VALUE : SqlReader.GetDecimal(DB_CONTR_PCT_WEDNESDAY);
        JackpotViewerItemParameters.ContrPctThursday  = (SqlReader.IsDBNull(DB_CONTR_PCT_THURSDAY))  ? Jackpot.DEFAULT_ID_VALUE : SqlReader.GetDecimal(DB_CONTR_PCT_THURSDAY);
        JackpotViewerItemParameters.ContrPctFriday    = (SqlReader.IsDBNull(DB_CONTR_PCT_FRIDAY))    ? Jackpot.DEFAULT_ID_VALUE : SqlReader.GetDecimal(DB_CONTR_PCT_FRIDAY);
        JackpotViewerItemParameters.ContrPctSaturday  = (SqlReader.IsDBNull(DB_CONTR_PCT_SATURDAY))  ? Jackpot.DEFAULT_ID_VALUE : SqlReader.GetDecimal(DB_CONTR_PCT_SATURDAY);
        JackpotViewerItemParameters.ContrPctSunday    = (SqlReader.IsDBNull(DB_CONTR_PCT_SUNDAY))    ? Jackpot.DEFAULT_ID_VALUE : SqlReader.GetDecimal(DB_CONTR_PCT_SUNDAY);
        JackpotViewerItemParameters.NumPending        = SqlReader.GetInt32(DB_NUM_PENDING);
        JackpotViewerItemParameters.Playing           = SqlReader.GetInt32(DB_PLAYING);

        return true;                
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotViewerItemParametersDataMapper");
        Log.Exception(_ex);
      }

      JackpotViewerItemParameters = null;

      return false;

    } // DB_JackpotJackpotViewerItemDataMapper 

    #endregion

  }
}
