﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotViewerItemsList.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot Viewer Terminal List
// 
//        AUTHOR: Carlos Cáceres González
// 
// CREATION DATE: 25-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-MAY-2017 CCG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotViewerTerminalList : JackpotBase
  {
    #region " Properties "

    public Int32 JackpotViewerId { get; set; }
    public List<JackpotViewerTerminalItem> Items { get; set; }
    
    #endregion " Properties "

    #region " Public Methods "
     
    /// <summary>
    /// Default Constructor
    /// </summary>
    public JackpotViewerTerminalList()
    {
      this.JackpotViewerId  = DEFAULT_ID_VALUE;
      this.Items            = new List<JackpotViewerTerminalItem>();
    } // JackpotViewerTerminalList
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="JackpotId"></param>    
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.Read(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      List<JackpotViewerTerminalItem> _items;

      if (!new JackpotViewerTerminalItem().GetTerminalsByJackpotViewerId(JackpotViewerId, out _items, SqlTrx))
      {
        return false;
      }

      this.JackpotViewerId = JackpotViewerId;
      this.Items = _items;

      return true;
    } // Read

    /// <summary>
    /// Save all contributions
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public override Boolean Save(SqlTransaction SqlTrx)
    {
      if (!this.IsModified)
      {
        return true;
      }

      // Remove all items from JackpotViewer (Only if it's update)
      if (!this.DB_InitializeJackpotViewerItems(SqlTrx))
      {
        return false;
      }

      return this.DB_Save(SqlTrx);
    } // DB_SaveJackpotViewerItems
    
    /// <summary>
    /// Reset all members id (For clonation)
    /// </summary>
    public override void Reset()
    {
      this.JackpotViewerId = DEFAULT_ID_VALUE;

      if (this.Items != null)
      {
        foreach (JackpotViewerTerminalItem _item in this.Items)
        {
          _item.Reset();
        }
      }
    } // Reset

    /// <summary>
    /// Clone JackpotAwardPrizeConfig
    /// </summary>
    /// <returns></returns>
    public JackpotViewerTerminalList Clone()
    {
      return new JackpotViewerTerminalList()
      {
        JackpotViewerId = this.JackpotViewerId,
        Items = this.Items.Clone()
      };
    } // Clone

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Delete all items of JackpotViewerId
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_InitializeJackpotViewerItems(SqlTransaction SqlTrx)
    {
      if (this.IsNew)
      {
        return true;
      }

      // Delete all items
      if (!new JackpotViewerTerminalItem().DeleteByJackpotViewerId(this.JackpotViewerId, SqlTrx))
      {
        return false;
      }

      return true;
    } // DB_InitializeJackpotViewerItems 

    #endregion " Private Methods "

    #region " Internal Methods "

    /// <summary>
    /// Inset/Update an specific Jackpot contribution Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Save(SqlTransaction SqlTrx)
    {
      try
      {
        // Save all items
        foreach (JackpotViewerTerminalItem _item in this.Items)
        {
          _item.JackpotViewerId = this.JackpotViewerId;

          if (!_item.Save(SqlTrx))
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewerTerminalList.DB_Save -> JackpotId: {0}", this.JackpotViewerId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Save

    #endregion 

  } // JackpotContributionList
}
