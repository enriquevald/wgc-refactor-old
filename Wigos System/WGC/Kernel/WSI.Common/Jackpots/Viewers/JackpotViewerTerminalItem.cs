﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotViewerItemsList.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot Viewer Terminal Item
// 
//        AUTHOR: Carlos Cáceres González
// 
// CREATION DATE: 25-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-MAY-2017 CCG    First release.
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotViewerTerminalItem : JackpotBase
  {

    #region " Constants "

    private const Int32 DB_JACKPOT_VIEWER_ID  = 0;
    private const Int32 DB_TERMINAL_VIEWER_ID = 1;   
    private const Int32 DB_TERMINAL_NAME      = 2;
    private const Int32 DB_RELATED            = 3;
    
    #endregion
    
    #region " Enums "

    public enum TYPE_JACKPOT_VIEWER_TERMINAL_GET
    {
      All = 0,
      SameViewer = 1
    }
    
    #endregion

    #region " Properties "

    public Int32 JackpotViewerId { get; set; }
    public Int32 TerminalId { get; set; }
    public String TerminalName { get; set; }
    public Boolean IsRelated { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by Jackpot Viewer Terminal Items
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotViewerTerminalItem()
    {      
      this.JackpotViewerId  = DEFAULT_ID_VALUE;
      this.TerminalId       = DEFAULT_ID_VALUE;
      this.TerminalName     = String.Empty;
      this.IsRelated        = false;

    } // JackpotViewerItem

    /// <summary>
    /// Get all available terminal items
    /// </summary>
    /// <returns></returns>
    public List<JackpotViewerTerminalItem> GetAllITerminalAvailables(Int32 JackpotViewerTerminalId)
    {
       List<JackpotViewerTerminalItem> _items;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (this.GetTerminalsByJackpotViewerId(JackpotViewerTerminalId, TYPE_JACKPOT_VIEWER_TERMINAL_GET.All, out _items, _db_trx.SqlTransaction))
        {
          return _items;
        }
      }

      return null;
    } // GetAllITerminalAvailables

    /// <summary>
    /// Get all Jackpot Viewer Terminal items by Jackpot Viewer Id
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="JackpotViewerItemsList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetTerminalsByJackpotViewerId(Int32 JackpotViewerTerminalId, out List<JackpotViewerTerminalItem> JackpotViewerTerminalItemsList, SqlTransaction SqlTrx)
    {
      return this.DB_GetAllJAckpotViewerTerminalItems_Internal(JackpotViewerTerminalId, TYPE_JACKPOT_VIEWER_TERMINAL_GET.SameViewer, out JackpotViewerTerminalItemsList, SqlTrx);
    }
    public Boolean GetTerminalsByJackpotViewerId(Int32 JackpotViewerTerminalId, TYPE_JACKPOT_VIEWER_TERMINAL_GET Type, out List<JackpotViewerTerminalItem> JackpotViewerTerminalItemsList, SqlTransaction SqlTrx)
    {
      return this.DB_GetAllJAckpotViewerTerminalItems_Internal(JackpotViewerTerminalId, Type, out JackpotViewerTerminalItemsList, SqlTrx);
    } // GetTerminalsByJackpotViewerId

    public override Boolean Save(SqlTransaction SqlTrx)
    {
      // Nothing to update. (Must be delete all related items before Save item by item.
      return this.DB_Insert(SqlTrx);
    } // Save

    /// <summary>
    /// Delete all terminal items by JackpotViewerId
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DeleteByJackpotViewerId(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      return this.DB_DeleteByJackpotViewerId(JackpotViewerId, SqlTrx);
    } // DeleteByJackpotViewerId
    
    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.JackpotViewerId  = DEFAULT_ID_VALUE;
      this.TerminalId       = DEFAULT_ID_VALUE;
    } // Reset
    
    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// /// Get terminal items by Jackpot Viewer Id
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="Type"></param>
    /// <param name="JackpotViewerTerminalItemsList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetAllJAckpotViewerTerminalItems_Internal(Int32 JackpotViewerId, TYPE_JACKPOT_VIEWER_TERMINAL_GET Type, out List<JackpotViewerTerminalItem> JackpotViewerTerminalItemsList, SqlTransaction SqlTrx)
    {
      JackpotViewerTerminalItem _item;

      JackpotViewerTerminalItemsList = new List<JackpotViewerTerminalItem>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_GetJackpotsViewerTerminalItems_Select(Type), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotViewerId", SqlDbType.Int).Value = JackpotViewerId;
          _cmd.Parameters.Add("@TerminalType", SqlDbType.Int).Value = TerminalTypes.SITE_JACKPOT;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _item = this.JackpotViewerTerminalItemMappFromSqlReader(_sql_reader);
              _item.JackpotViewerId = JackpotViewerId;

              if (_item != null)
              {
                JackpotViewerTerminalItemsList.Add(_item);
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewerTerminalItem.DB_GetAllJAckpotViewerTerminalItems_Internal -> JackpotViewerTerminalId: {0}", TerminalId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetAllJAckpotViewerTerminalItems_Internal

    /// <summary>
    /// Mapp jackpot terminal item from sql reader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private JackpotViewerTerminalItem JackpotViewerTerminalItemMappFromSqlReader(SqlDataReader SqlReader)
    {
      JackpotViewerTerminalItem _jackpot_viewer_terminal_item;

      _jackpot_viewer_terminal_item = new JackpotViewerTerminalItem();

      if (!DB_JackpotJackpotViewerTerminalItemDataMapper(_jackpot_viewer_terminal_item, SqlReader))
      {
        Log.Error("JackpotViewerTerminalItem.JackpotViewerTerminalItemMappFromSqlReader -> Error on load.");
      }

      return _jackpot_viewer_terminal_item;
    } // JackpotViewerTerminalItemMappFromSqlReader

    /// <summary>
    /// Delete all terminal items by Jackpot Viewer in database
    /// </summary>
    /// <param name="JackpotViewerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_DeleteByJackpotViewerId(Int32 JackpotViewerId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DELETE FROM  JACKPOTS_VIEWERS_RELATED_TERMINAL         ");
        _sb.AppendLine("       WHERE  JVT_JACKPOT_VIEWER_ID = @pJackpotViewerId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotViewerId", SqlDbType.Int).Value = JackpotViewerId;
          _cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewerterminalItem.DB_DeleteByJackpotViewerId -> JackpotViewerId: {0}", JackpotViewerId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_DeleteByJackpotViewerId

    /// <summary>
    /// Load Jackpot Viewer terminal item data from SQLReader
    /// </summary>
    /// <param name="JackpotViewerTerminalItem"></param>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotJackpotViewerTerminalItemDataMapper(JackpotViewerTerminalItem JackpotViewerTerminalItem, SqlDataReader SqlReader)
    {
      try
      {
        JackpotViewerTerminalItem.JackpotViewerId = (SqlReader.IsDBNull(DB_JACKPOT_VIEWER_ID))  ? DEFAULT_ID_VALUE : SqlReader.GetInt32(DB_JACKPOT_VIEWER_ID);
        JackpotViewerTerminalItem.TerminalId      = (SqlReader.IsDBNull(DB_TERMINAL_VIEWER_ID)) ? DEFAULT_ID_VALUE : SqlReader.GetInt32(DB_TERMINAL_VIEWER_ID);
        JackpotViewerTerminalItem.TerminalName    = SqlReader.GetString(DB_TERMINAL_NAME);
        JackpotViewerTerminalItem.IsRelated       = (SqlReader.GetInt32(DB_RELATED) > 0);
                                                 
        return true;                             
      }                                          
      catch (Exception _ex)                      
      {
        Log.Error("Error on DB_JackpotJackpotViewerTerminalItemDataMapper");
        Log.Exception(_ex);
      }

      JackpotViewerTerminalItem = null;

      return false;
      
    } // DB_JackpotJackpotViewerTerminalItemDataMapper 

    /// <summary>
    /// GetJackpotsViewerTerminalItems SQL Query
    /// </summary>
    /// <returns></returns>
    private String DB_GetJackpotsViewerTerminalItems_Select(TYPE_JACKPOT_VIEWER_TERMINAL_GET Type)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.AppendLine("  WITH ALL_TERMINALS AS                                                                             ");
      _sb.AppendLine("	(                                                                                                 ");
      _sb.AppendLine("	    SELECT    TE_TERMINAL_ID AS TERMINAL_ID                                                       ");
      _sb.AppendLine("				      , TE_NAME AS TERMINAL_NAME                                                            ");
      _sb.AppendLine("		    FROM    TERMINALS                                                                           ");
      _sb.AppendLine("		   WHERE    TE_TERMINAL_TYPE = @TerminalType                                                    ");
      _sb.AppendLine("	)                                                                                                 ");
      _sb.AppendLine("      SELECT    JVT.JVT_JACKPOT_VIEWER_ID AS JACKPOT_VIEWER_ID                                      ");
      _sb.AppendLine("    		      , T.TERMINAL_ID                                                                       ");
      _sb.AppendLine("    		      , T.TERMINAL_NAME                                                                     ");
      _sb.AppendLine("    		      , CASE WHEN JVT.JVT_JACKPOT_VIEWER_ID IS NULL THEN 0 ELSE 1 END AS TERMINAL_RELATED   ");
      _sb.AppendLine("        FROM    ALL_TERMINALS AS T                                                                  ");
      _sb.AppendLine("   LEFT JOIN    JACKPOTS_VIEWERS_RELATED_TERMINAL JVT ON JVT.JVT_TERMINAL_VIEWER_ID = T.TERMINAL_ID ");      
      _sb.AppendLine(this.DB_GetConditionByType(Type));
      _sb.AppendLine("    ORDER BY    TERMINAL_ID                                                                         ");

      return _sb.ToString();
    } // DB_GetJackpotsViewerTerminalItems_Select

    private string DB_GetConditionByType(TYPE_JACKPOT_VIEWER_TERMINAL_GET Type)
    {
      String _condition;

      _condition = String.Empty;

      switch (Type)
      {
        case TYPE_JACKPOT_VIEWER_TERMINAL_GET.All:
          _condition = "        AND   JVT.JVT_JACKPOT_VIEWER_ID = @pJackpotViewerId ";
          break;

        case TYPE_JACKPOT_VIEWER_TERMINAL_GET.SameViewer:
          _condition = "      WHERE   JVT.JVT_JACKPOT_VIEWER_ID = @pJackpotViewerId ";
          break;
      }

      return _condition;
    } // DB_GetConditionByType
        
    #endregion " Private Methods "
    
    #region " Internal Methods "

    /// <summary>
    /// Insert an specific Jackpot Viewer Terminal Item in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO   JACKPOTS_VIEWERS_RELATED_TERMINAL    ");
        _sb.AppendLine("             ( JVT_JACKPOT_VIEWER_ID                ");
        _sb.AppendLine("             , JVT_TERMINAL_VIEWER_ID )             ");
        _sb.AppendLine("      VALUES                                        ");
        _sb.AppendLine("             ( @pJackpotViewerId                    ");
        _sb.AppendLine("             , @TerminalId )                        ");
        _sb.AppendLine(" SET           @pJackpotViewerId = SCOPE_IDENTITY() ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotViewerId" , SqlDbType.Int).Value = this.JackpotViewerId;
          _cmd.Parameters.Add("@TerminalId"       , SqlDbType.Int).Value = this.TerminalId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotViewerTerminalItem.DB_Insert -> TerminalId: {0}", this.TerminalId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotViewerTerminalItem.DB_Insert -> TerminalId: {0}", this.TerminalId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Insert


    /// <summary>
    /// Clone JackpotViewerTerminalItem
    /// </summary>
    /// <returns></returns>
    internal JackpotViewerTerminalItem Clone()
    {
      return new JackpotViewerTerminalItem()
      {
        JackpotViewerId = this.JackpotViewerId,
        TerminalId      = this.TerminalId,
        TerminalName    = this.TerminalName
      };
    } // Clone
    
    #endregion 

  }
}
