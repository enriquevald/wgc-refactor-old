﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardPrizeSharing.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Javier Barea
// 
// CREATION DATE: 28-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardPrizeSharing : JackpotBase
  {
    #region " Constants "

    private const Int32 DB_ID                           = 0;
    private const Int32 DB_JACKPOT_ID                   = 1;
    private const Int32 DB_PRIZE_SHARING_ENABLED        = 2;
    private const Int32 DB_PRIZE_SHARING_INCLUDE_WINNER = 3;
    private const Int32 DB_PRIZE_SHARING_SAME_BANK      = 4;
    private const Int32 DB_PRIZE_SHARING_SAME_AREA      = 5;
    private const Int32 DB_PRIZE_SHARING_SAME_JACKPOT   = 6;
    private const Int32 DB_PRIZE_SHARING_MAX_NUM_WINNER = 7;
    private const Int32 DB_PRIZE_SHARING_LAST_UPDATE    = 8;
    private const Int32 DB_MINIMUM                      = 9;
    private const Int32 DB_MAXIMUM                      = 10;

    private const Int32 DEFAULT_NUM_MAX_WINNERS         = 1;

    #endregion

    #region " Properties "

    public Int32 JackpotId { get; set; }
    public Boolean Enabled { get; set; }
    public Boolean IncludeWinner { get; set; }
    public Boolean SameBank { get; set; }
    public Boolean SameArea { get; set; }
    public Boolean SameJackpot { get; set; }
    public Int32 MaxNumWinner { get; set; }
    public Decimal Minimum { get; set; } 
    public Decimal Maximum { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by JackpotId
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotAwardPrizeSharing()
    {
      this.Id               = DEFAULT_ID_VALUE;
      this.Enabled          = false;
      this.IncludeWinner    = false;
      this.SameBank         = false;
      this.SameArea         = false;
      this.SameJackpot      = true;
      this.MaxNumWinner     = DEFAULT_NUM_MAX_WINNERS;      
      this.LastUpdate       = WGDB.MinDate;
      this.LastModification = WGDB.MinDate;
      this.Minimum          = DEFAULT_DECIMAL_VALUE;
      this.Maximum          = DEFAULT_DECIMAL_VALUE;
    } // JackpotAwardPrizeSharing

    // Read Jackpot Award Prize Sharing
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.Read(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      return this.DB_GetJackpotAwardPrizeSharing_Internal(JackpotId, SqlTrx);
    } // Read
    
    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.JackpotId = DEFAULT_ID_VALUE;
    } // Reset

    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotAwardPrizeSharing_Internal(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotAwardPrizeSharingQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!DB_JackpotAwardPrizeSharingDataMapper(_sql_reader))
              {
                return false;
              }

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardPrizeSharing.DB_GetJackpotAwardPrizeSharing_Internal -> JackpotId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotAwardPrizeSharing_Internal

    /// <summary>
    /// Load Jackpot Award Prize Sharing data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotAwardPrizeSharingDataMapper(SqlDataReader SqlReader)
    {
      try
      {
        this.Id            = SqlReader.GetInt32(DB_ID);
        this.JackpotId     = SqlReader.GetInt32(DB_JACKPOT_ID);
        this.Enabled       = SqlReader.GetBoolean(DB_PRIZE_SHARING_ENABLED);
        this.IncludeWinner = SqlReader.GetBoolean(DB_PRIZE_SHARING_INCLUDE_WINNER);
        this.SameBank      = SqlReader.GetBoolean(DB_PRIZE_SHARING_SAME_BANK);
        this.SameArea      = SqlReader.GetBoolean(DB_PRIZE_SHARING_SAME_AREA);
        this.SameJackpot   = SqlReader.GetBoolean(DB_PRIZE_SHARING_SAME_JACKPOT);
        this.MaxNumWinner  = SqlReader.GetInt32(DB_PRIZE_SHARING_MAX_NUM_WINNER);
        this.Minimum       = SqlReader.GetDecimal(DB_MINIMUM);
        this.Maximum       = SqlReader.GetDecimal(DB_MAXIMUM);

        this.LastUpdate    = SqlReader.IsDBNull(DB_PRIZE_SHARING_LAST_UPDATE) ? WGDB.MinDate : SqlReader.GetDateTime(DB_PRIZE_SHARING_LAST_UPDATE);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotAwardPrizeSharingDataMapper");
        Log.Exception(_ex);
      }

      return false;
      
    } // DB_JackpotAwardPrizeSharingDataMapper 
   
    /// <summary>
    /// Gets Jackpot Award Prize Sharing select query
    /// </summary>
    /// <returns></returns>
    private String DB_JackpotAwardPrizeSharingQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   JPS_ID                           "); // 0
      _sb.AppendLine("           , JPS_JACKPOT_ID                   "); // 1
      _sb.AppendLine("           , JPS_ENABLED                      "); // 2
      _sb.AppendLine("           , JPS_INCLUDE_WINNER               "); // 3
      _sb.AppendLine("           , JPS_TO_SAME_BANK                 "); // 4
      _sb.AppendLine("           , JPS_TO_SAME_AREA                 "); // 5
      _sb.AppendLine("           , JPS_TO_SAME_JACKPOT              "); // 6
      _sb.AppendLine("           , JPS_MAX_NUM_OF_WINNERS           "); // 7
      _sb.AppendLine("           , JPS_LAST_UPDATE                  "); // 8
      _sb.AppendLine("           , JPS_MINIMUM                      "); // 9
      _sb.AppendLine("           , JPS_MAXIMUM                      "); // 10
      _sb.AppendLine("      FROM   JACKPOTS_SETTINGS_PRIZE_SHARING  ");
      _sb.AppendLine("     WHERE   JPS_JACKPOT_ID = @pJackpotId     ");

      return _sb.ToString();

    } // DB_JackpotAwardPrizeSharingQuery_Select  

    #endregion " Private Methods "

    #region " Internal Methods "
    
    /// <summary>
    /// Insert an specific Jackpot Award Prize Sharing in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      SqlParameter _op;
      StringBuilder _sb;
   
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   JACKPOTS_SETTINGS_PRIZE_SHARING  ");
        _sb.AppendLine("             ( JPS_JACKPOT_ID                   ");
        _sb.AppendLine("             , JPS_ENABLED                      ");
        _sb.AppendLine("             , JPS_MINIMUM                      ");
        _sb.AppendLine("             , JPS_MAXIMUM                      ");
        _sb.AppendLine("             , JPS_INCLUDE_WINNER               ");
        _sb.AppendLine("             , JPS_TO_SAME_BANK                 ");
        _sb.AppendLine("             , JPS_TO_SAME_AREA                 ");
        _sb.AppendLine("             , JPS_TO_SAME_JACKPOT              ");
        _sb.AppendLine("             , JPS_MAX_NUM_OF_WINNERS           ");
        _sb.AppendLine("             , JPS_LAST_UPDATE        )         ");
        _sb.AppendLine("      VALUES                                    ");
        _sb.AppendLine("             ( @pJackpotId                      ");
        _sb.AppendLine("             , @pEnabled                        ");
        _sb.AppendLine("             , @pMinimum                        ");
        _sb.AppendLine("             , @pMaximum                        ");
        _sb.AppendLine("             , @pIncludeWinner                  ");
        _sb.AppendLine("             , @pToSameBank                     ");
        _sb.AppendLine("             , @pToSameArea                     ");
        _sb.AppendLine("             , @pToSameJackpot                  ");
        _sb.AppendLine("             , @pMaxNumWinners                  ");
        _sb.AppendLine("             , @pLastUpdate    )                ");

        _sb.AppendLine(" SET     @pId = SCOPE_IDENTITY()                ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId"     , SqlDbType.Int).Value    = this.JackpotId;
          _cmd.Parameters.Add("@pEnabled"       , SqlDbType.Bit).Value    = this.Enabled;
          _cmd.Parameters.Add("@pMinimum  "     , SqlDbType.Money).Value  = this.Minimum;
          _cmd.Parameters.Add("@pMaximum  "     , SqlDbType.Money).Value  = this.Maximum;
          _cmd.Parameters.Add("@pIncludeWinner" , SqlDbType.Bit).Value    = this.IncludeWinner;
          _cmd.Parameters.Add("@pToSameBank"    , SqlDbType.Bit).Value    = this.SameBank;
          _cmd.Parameters.Add("@pToSameArea"    , SqlDbType.Bit).Value    = this.SameArea;
          _cmd.Parameters.Add("@pToSameJackpot" , SqlDbType.Bit).Value    = this.SameJackpot;
          _cmd.Parameters.Add("@pMaxNumWinners" , SqlDbType.Int).Value    = this.MaxNumWinner;

          // Last update
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = (this.LastUpdate == WGDB.MinDate) ? DBNull.Value : (Object)this.LastUpdate;

          //Output parameter
          _op = new SqlParameter("@pId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);
          
          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardPrizeSharing.DB_Insert -> JackpotId: {0}", this.JackpotId));

            return false;
          }

          this.Id = (Int32)_op.Value;

          if (this.Enabled)
          {
            return JackpotAmountOperations.RecalculateJackpotAmounts(this.JackpotId, SqlTrx);
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardPrizeSharing.DB_Insert -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Jackpot Award Prize Sharing in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _need_recalculate;
      Boolean _recalculate;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SET  @pRecalculate = 0                          ");
        _sb.AppendLine(" SELECT  @pRecalculate = 1                          ");
        _sb.AppendLine("  FROM   JACKPOTS_SETTINGS_PRIZE_SHARING            ");
        _sb.AppendLine(" WHERE ( JPS_MINIMUM               <> @pMinimum     ");
        _sb.AppendLine("    OR   JPS_MAXIMUM               <> @pMaximum     ");
        _sb.AppendLine("    OR   JPS_ENABLED               <> @pEnabled)    ");
        _sb.AppendLine("   AND   JPS_ID                     = @pId          ");


        _sb.AppendLine(" UPDATE   JACKPOTS_SETTINGS_PRIZE_SHARING           ");
        _sb.AppendLine("    SET   JPS_JACKPOT_ID          = @pJackpotId     ");
        _sb.AppendLine("        , JPS_ENABLED             = @pEnabled       ");
        _sb.AppendLine("        , JPS_MINIMUM             = @pMinimum       ");
        _sb.AppendLine("        , JPS_MAXIMUM             = @pMaximum       ");
        _sb.AppendLine("        , JPS_INCLUDE_WINNER      = @pIncludeWinner ");
        _sb.AppendLine("        , JPS_TO_SAME_BANK        = @pToSameBank    ");
        _sb.AppendLine("        , JPS_TO_SAME_AREA        = @pToSameArea    ");
        _sb.AppendLine("        , JPS_TO_SAME_JACKPOT     = @pToSameJackpot ");
        _sb.AppendLine("        , JPS_MAX_NUM_OF_WINNERS  = @pMaxNumWinners ");
        _sb.AppendLine("        , JPS_LAST_UPDATE         = @pLastUpdate    ");
        _sb.AppendLine("  WHERE   JPS_ID                  = @pId            ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId"            , SqlDbType.Int).Value    = this.Id;
          _cmd.Parameters.Add("@pJackpotId"     , SqlDbType.Int).Value    = this.JackpotId;
          _cmd.Parameters.Add("@pEnabled"       , SqlDbType.Bit).Value    = this.Enabled;
          _cmd.Parameters.Add("@pMinimum  "     , SqlDbType.Money).Value  = this.Minimum;
          _cmd.Parameters.Add("@pMaximum  "     , SqlDbType.Money).Value  = this.Maximum;
          _cmd.Parameters.Add("@pIncludeWinner" , SqlDbType.Bit).Value    = this.IncludeWinner;
          _cmd.Parameters.Add("@pToSameBank"    , SqlDbType.Bit).Value    = this.SameBank;
          _cmd.Parameters.Add("@pToSameArea"    , SqlDbType.Bit).Value    = this.SameArea;
          _cmd.Parameters.Add("@pToSameJackpot" , SqlDbType.Bit).Value    = this.SameJackpot;
          _cmd.Parameters.Add("@pMaxNumWinners" , SqlDbType.Int).Value    = this.MaxNumWinner;

          // Last update
          this.LastUpdate = this.LastModification;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;

          _need_recalculate = new SqlParameter("@pRecalculate", SqlDbType.Bit);
          _need_recalculate.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_need_recalculate);

          
          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("JackpotAwardPrizeSharing.DB_Update -> JackpotAwardPrizeSharingId: {0}", this.Id));

            return false;
          }

          _recalculate = (Boolean)_need_recalculate.Value;

          // Recalculate amount with new max and min values if has changed.
          if (_recalculate)
          {
            return JackpotAmountOperations.RecalculateJackpotAmounts(this.JackpotId, SqlTrx);
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardPrizeSharing.DB_Update -> JackpotAwardPrizeSharingId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Update

    /// <summary>
    /// Clone JackpotAwardPrizeSharing
    /// </summary>
    /// <returns></returns>
    internal JackpotAwardPrizeSharing Clone()
    {
      return new JackpotAwardPrizeSharing()
      {
        Id            = this.Id,
        JackpotId     = this.JackpotId,
        Enabled       = this.Enabled,
        Minimum       = this.Minimum,
        Maximum       = this.Maximum,
        IncludeWinner = this.IncludeWinner,
        SameBank      = this.SameBank,
        SameArea      = this.SameArea,
        SameJackpot   = this.SameJackpot,
        MaxNumWinner  = this.MaxNumWinner,
        LastUpdate    = this.LastUpdate 
      };
    } // Clone

    #endregion 

  } // JackpotAwardCustomerByTier
}
