﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotCommonMisc.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 03-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-APR-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Data.SqlClient;
namespace WSI.Common.Jackpot
{
  public class JackpotCommonMisc
  {
    #region " Enums "
        
    #endregion " Enums "

    #region " Public Methods "

    /// <summary>
    /// Converts default value to DBNull
    /// </summary>
    /// <param name="Value"></param>
    /// <param name="DefaultValue"></param>
    /// <returns></returns>
    public static Object DefaultValueToDBNull(Object Value, Object DefaultValue)
    {
      if (Value == null || Value == DefaultValue)
      {
        return DBNull.Value;
      }

      return Value;
    } // DefaultValueToDBNull

    #endregion " Public Methods "

    #region " Private Methods "

    #endregion 

  }
 
}

