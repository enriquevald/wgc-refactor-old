﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotContributionList.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot Contribution Groups List
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 30-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAR-2017 JBP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotContributionList : JackpotBase
  {

    #region " Properties "

    public Int32 JackpotId { get; set; }
    public List<JackpotContribution> Items  { get; set; }

    public override Boolean IsConfigured
    {
      get { return Items.IsConfigured(); }
    }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Default Constructor
    /// </summary>
    public JackpotContributionList()
    {
      this.JackpotId  = DEFAULT_ID_VALUE;
      this.Items      = new List<JackpotContribution>();
      this.LastUpdate = WGDB.MinDate;
      this.LastModification = WGDB.MinDate;
    } // JackpotContributionList

    /// <summary>
    /// Get all jackpot contribution enabled
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetJackpotContribution(SqlTransaction SqlTrx)
    {
      List<JackpotContribution> _contributions;

      if (!new JackpotContribution().GetAllJackpotContributionEnabled(out _contributions, SqlTrx))
      {
        return false;
      }

      this.JackpotId = DEFAULT_ID_VALUE;
      this.Items = _contributions;

      return true;
    } // GetJackpotContribution
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="JackpotId"></param>    
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.Read(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read

    public Boolean Read(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      List<JackpotContribution> _contributions;

      if (!new JackpotContribution().GetAllJackpotContributionByJackpotId(JackpotId, out _contributions, SqlTrx))
      {
        return false;
      }

      this.JackpotId = JackpotId;
      this.Items = _contributions;

      return true;

    } // Read  

    /// <summary>
    /// Save all contributions
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public override Boolean Save(SqlTransaction SqlTrx)
    {
      return this.DB_Save(SqlTrx);
    } // Save   

    public Int32 GetContributionsInUse()
    {
      Int32 _result;

      _result = 0;

      foreach (JackpotContribution _jackpot_contribution in this.Items) 
      {
        _result += (_jackpot_contribution.ContributionState == Jackpot.ContributionGroupState.InUse ? 1 : 0);
      }

      return _result;
    }

    public Boolean AddNewContribution()
    {
      Boolean _result;

      _result = false;

      for(int i = 0; i < this.Items.Count; i++)
      {
        if (this.Items[i].ContributionState == Jackpot.ContributionGroupState.NotInUse || 
            this.Items[i].ContributionState == Jackpot.ContributionGroupState.Deleted) 
        {
          this.Items[i] = new JackpotContribution(this.Items[i].Name);
          this.Items[i].ContributionState = Jackpot.ContributionGroupState.InUse;

          _result = true; 

          break;
        }        
      }

      return _result;
    } // AddNewContribution


    /// <summary>
    /// Reset all members id (For clonation)
    /// </summary>
    public override void Reset()
    {
      this.JackpotId = Jackpot.DEFAULT_ID_VALUE;

      if (this.Items != null)
      {
        foreach (JackpotContribution _contribution in this.Items)
        {
          _contribution.Reset();
        }
      }
    } // Reset

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Inset/Update an specific Jackpot contribution Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Save(SqlTransaction SqlTrx)
    {
      try
      {
        foreach (JackpotContribution _contribution in this.Items)
        {
          _contribution.JackpotId = this.JackpotId;

          switch (_contribution.ContributionState)
          {
            case Jackpot.ContributionGroupState.Deleted:
              if (_contribution.Id > 0)
              {
                return _contribution.Delete(SqlTrx);
              }
              break;

            case Jackpot.ContributionGroupState.InUse:
              if (!_contribution.Save(SqlTrx))
              {
                return false;
              }
              break;
          }       
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContributionList.DB_Save -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Save

    /// <summary>
    ///Delete all contribution gropus from a Jackpot in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_Delete(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" DELETE FROM   JACKPOTS_SETTINGS_CONTRIBUTION ");
        _sb.AppendLine("       WHERE   JSC_JACKPOT_ID   = @pJackpotId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = this.JackpotId;

          _cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContributionList.DB_Delete -> JackpotContributionId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Delete

    #endregion " Private Methods "

    #region " Internal Methods "
    
    /// <summary>
    /// Clone JackpotAwardPrizeConfig
    /// </summary>
    /// <returns></returns>
    internal JackpotContributionList Clone()
    {
      return new JackpotContributionList()
      {
        JackpotId = this.JackpotId,
        Items     = this.Items.Clone()
      };
    } // Clone

    #endregion

  } // JackpotContributionList
}