﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAwardFilter.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Ruben Lama
// 
// CREATION DATE: 18-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-APR-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAwardFilter : JackpotBase
  {
    #region " Constants "

    private const Int32 DB_ID                           = 0;
    private const Int32 DB_JACKPOT_ID                   = 1;
    private const Int32 DB_EXCLUDE_ANONYMOUS_ACCOUNTS   = 2;
    private const Int32 DB_EXCLUDE_ACCOUNTS_WITH_PROMO  = 3;
    private const Int32 DB_MIN_OCCUPANCY                = 4;
    private const Int32 DB_BY_GENDER                    = 5;
    private const Int32 DB_BY_DATE_OF_BIRTH             = 6;
    private const Int32 DB_AGE_RANGE                    = 7;
    private const Int32 DB_OLDER_THAN                   = 8;
    private const Int32 DB_AND_SMALLER_THAN             = 9;
    private const Int32 DB_SMALLER_THAN                 = 10;
    private const Int32 DB_OR_OLDER_THAN                = 11;
    private const Int32 DB_BY_CREATION_DATE             = 12;
    private const Int32 DB_JUST_DAY_OF_CREATION         = 13;
    private const Int32 DB_JUST_DAY_OF_CREATION_PLUS    = 14;
    private const Int32 DB_ANY_DAY_OF_MONTH             = 15;
    private const Int32 DB_DAY_AND_MONTH                = 16;
    private const Int32 DB_CREATION_BETWEEN_THIS        = 17;
    private const Int32 DB_CREATION_BETWEEN_THAT        = 18;
    private const Int32 DB_SELECTION_EGM_TYPE           = 19;
    private const Int32 DB_SELECTED_EGM                 = 20;
    private const Int32 DB_MINIMUM_BET                  = 21;
    private const Int32 DB_LAST_UPDATE                  = 22;

    #endregion

    #region " Properties "

    public Int32 JackpotId { get; set; }
    public GENDER ByGender { get; set; }
    public AwardFilterDateOfBirth ByDateOfBirth { get; set; }
    public Boolean AgeRange { get; set; }
    public Int32 OlderThan { get; set; }
    public Int32 AndSmallerThan { get; set; }
    public Int32 SmallerThan { get; set; }
    public Int32 OrOlderThan { get; set; }
    public AwardFilterCreationDate ByCreationDate { get; set; }
    public Boolean JustDayOfCreation { get; set; }
    public Int32 DayOfCreationPlus { get; set; }
    public Boolean AnyDayOfMonth { get; set; }
    public Boolean DayAndMonth { get; set; }
    public Int32 CreationBetweenThis { get; set; }
    public Int32 CreationBetweenThat { get; set; }
    public Boolean ExcludeAnonymousAccount { get; set; }
    public Boolean ExcludeAccountsWithPromo { get; set; }
    public Decimal MinOccupancy { get; set; }
    public String SelectedEgm { get; set; }
    public JackpotAwardFilterFlagList Flags { get; set; }
    public Decimal MinimumBet { get; set; }
    public JackpotSelectionEGMType SelectionEGMType { get; set; }    

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Constructor's by JackpotId
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    public JackpotAwardFilter()
    {
      this.Id                       = DEFAULT_ID_VALUE;
      this.ExcludeAnonymousAccount  = false;
      this.ExcludeAccountsWithPromo = false;
      this.MinOccupancy             = 0;
      this.ByGender                 = GENDER.UNKNOWN;
      this.ByDateOfBirth            = AwardFilterDateOfBirth.Disabled;
      this.AgeRange                 = false;
      this.OlderThan                = DEFAULT_INT_VALUE;
      this.AndSmallerThan           = DEFAULT_INT_VALUE;
      this.SmallerThan              = DEFAULT_INT_VALUE;
      this.OrOlderThan              = DEFAULT_INT_VALUE;
      this.ByCreationDate           = AwardFilterCreationDate.Disabled;
      this.JustDayOfCreation        = false;
      this.DayOfCreationPlus        = DEFAULT_INT_VALUE;
      this.AnyDayOfMonth            = false;
      this.DayAndMonth              = false;
      this.CreationBetweenThis      = DEFAULT_INT_VALUE;
      this.CreationBetweenThat      = DEFAULT_INT_VALUE;
      this.SelectedEgm              = String.Empty;
      this.LastUpdate               = WGDB.MinDate;
      this.LastModification         = WGDB.MinDate;
      this.Flags                    = new JackpotAwardFilterFlagList();
      this.MinimumBet               = DEFAULT_DECIMAL_VALUE;
      this.SelectionEGMType         = DEFAULT_INT_VALUE;
      
    } // JackpotAwardFilter

    // Read Jackpot Award Filter
    public Boolean Read(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.DB_GetJackpotAwardFilters_Internal(JackpotId, _db_trx.SqlTransaction);
      }
    } // Read
    public Boolean Read(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      return this.DB_GetJackpotAwardFilters_Internal(JackpotId, SqlTrx);
    } // Read
    
    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.JackpotId = DEFAULT_ID_VALUE;
    } // Reset

    #endregion " Public Methods "

    #region " Private Methods "
    
    /// <summary>
    /// Get data from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotAwardFilters_Internal(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotAwardFiltersQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if ( _sql_reader.Read()
              && !this.DB_JackpotAwardFiltersDataMapper(_sql_reader))
            {
              return false;
            }
          }
          
          // Read special data (AwardFilterFlags...)
          if (!DB_ReadSpecialData(JackpotId, SqlTrx))
          {
            return false;
        }

          return true;
      }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardFilter.DB_GetJackpotAwardFilters_Internal -> JackpotId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotAwardFilters_Internal

    /// <summary>
    /// Load Jackpot Award Days data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_JackpotAwardFiltersDataMapper(SqlDataReader SqlReader)
    {
      try
      {
        this.Id                       = SqlReader.GetInt32(DB_ID);
        this.JackpotId                = SqlReader.GetInt32(DB_JACKPOT_ID);
        this.JustDayOfCreation        = SqlReader.GetBoolean(DB_JUST_DAY_OF_CREATION);
        this.DayOfCreationPlus        = SqlReader.GetInt32(DB_JUST_DAY_OF_CREATION_PLUS);
        this.ByGender                 = (GENDER)SqlReader.GetInt32(DB_BY_GENDER);
        this.ByDateOfBirth            = (AwardFilterDateOfBirth)SqlReader.GetInt32(DB_BY_DATE_OF_BIRTH);
        this.AgeRange                 = SqlReader.GetBoolean(DB_AGE_RANGE);
        this.OlderThan                = SqlReader.GetInt32(DB_OLDER_THAN);
        this.AndSmallerThan           = SqlReader.GetInt32(DB_AND_SMALLER_THAN);
        this.SmallerThan              = SqlReader.GetInt32(DB_SMALLER_THAN);
        this.OrOlderThan              = SqlReader.GetInt32(DB_OR_OLDER_THAN);
        this.ByCreationDate           = (AwardFilterCreationDate)SqlReader.GetInt32(DB_BY_CREATION_DATE);
        this.CreationBetweenThis      = SqlReader.GetInt32(DB_CREATION_BETWEEN_THIS);
        this.CreationBetweenThat      = SqlReader.GetInt32(DB_CREATION_BETWEEN_THAT);
        this.AnyDayOfMonth            = SqlReader.GetBoolean(DB_ANY_DAY_OF_MONTH);
        this.DayAndMonth              = SqlReader.GetBoolean(DB_DAY_AND_MONTH);
        this.ExcludeAccountsWithPromo = SqlReader.GetBoolean(DB_EXCLUDE_ACCOUNTS_WITH_PROMO);
        this.ExcludeAnonymousAccount  = SqlReader.GetBoolean(DB_EXCLUDE_ANONYMOUS_ACCOUNTS);
        this.MinOccupancy             = SqlReader.GetDecimal(DB_MIN_OCCUPANCY);
        this.MinimumBet               = SqlReader.GetDecimal(DB_MINIMUM_BET);
        this.SelectedEgm              = SqlReader.IsDBNull(DB_SELECTED_EGM) ? String.Empty  : SqlReader.GetString(DB_SELECTED_EGM);
        this.LastUpdate               = SqlReader.IsDBNull(DB_LAST_UPDATE)  ? WGDB.MinDate  : SqlReader.GetDateTime(DB_LAST_UPDATE);
        this.SelectionEGMType         = SqlReader.IsDBNull(DB_SELECTION_EGM_TYPE) ? JackpotSelectionEGMType.All : (JackpotSelectionEGMType)SqlReader.GetInt32(DB_SELECTION_EGM_TYPE);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotAwardFiltersDataMapper");
        Log.Exception(_ex);
      }

      return false;
      
    } // DB_JackpotAwardFiltersDataMapper 
   
    /// <summary>
    /// Gets Jackpot Award Days select query
    /// </summary>
    /// <returns></returns>
    private String DB_JackpotAwardFiltersQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   JAF_ID                           "); // 0
      _sb.AppendLine("           , JAF_JACKPOT_ID                   "); // 1
      _sb.AppendLine("           , JAF_EXCLUDE_ANONYMOUS_ACCOUNTS   "); // 2
      _sb.AppendLine("           , JAF_EXCLUDE_ACCOUNTS_WITH_PROMO  "); // 3
      _sb.AppendLine("           , JAF_MIN_OCCUPANCY                "); // 4
      _sb.AppendLine("           , JAF_BY_GENDER                    "); // 5
      _sb.AppendLine("           , JAF_BY_DATE_OF_BIRTH             "); // 6
      _sb.AppendLine("           , JAF_AGE_RANGE                    "); // 7
      _sb.AppendLine("           , JAF_OLDER_THAN                   "); // 8
      _sb.AppendLine("           , JAF_AND_SMALLER_THAN             "); // 9
      _sb.AppendLine("           , JAF_SMALLER_THAN                 "); // 10
      _sb.AppendLine("           , JAF_OR_OLDER_THAN                "); // 11
      _sb.AppendLine("           , JAF_BY_CREATION_DATE             "); // 12
      _sb.AppendLine("           , JAF_JUST_DAY_OF_CREATION         "); // 13
      _sb.AppendLine("           , JAF_DAY_OF_CREATION_PLUS         "); // 14
      _sb.AppendLine("           , JAF_ANY_DAY_OF_MONTH             "); // 15
      _sb.AppendLine("           , JAF_DAY_AND_MONTH                "); // 16
      _sb.AppendLine("           , JAF_CREATION_BETWEEN_THIS        "); // 17
      _sb.AppendLine("           , JAF_CREATION_BETWEEN_THAT        "); // 18
      _sb.AppendLine("           , JAF_SELECTION_EGM_TYPE           "); // 19
      _sb.AppendLine("           , JAF_SELECTED_EGM                 "); // 20
      _sb.AppendLine("           , JAF_MINIMUM_BET                  "); // 21
      _sb.AppendLine("           , JAF_LAST_UPDATE                  "); // 22
      _sb.AppendLine("      FROM   JACKPOTS_AWARD_FILTERS           ");
      _sb.AppendLine("     WHERE   JAF_JACKPOT_ID = @pJackpotId     ");

      return _sb.ToString();

    } // DB_JackpotAwardFiltersQuery_Select  
    
    #endregion " Private Methods "

    #region " Internal Methods "
    
    /// <summary>
    /// Insert an specific Jackpot Award Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      SqlParameter _op;
      StringBuilder _sb;
   
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   JACKPOTS_AWARD_FILTERS             ");
        _sb.AppendLine("             ( JAF_JACKPOT_ID                     ");
        _sb.AppendLine("             , JAF_EXCLUDE_ANONYMOUS_ACCOUNTS     ");
        _sb.AppendLine("             , JAF_EXCLUDE_ACCOUNTS_WITH_PROMO    ");
        _sb.AppendLine("             , JAF_MIN_OCCUPANCY                  ");
        _sb.AppendLine("             , JAF_BY_GENDER                      ");
        _sb.AppendLine("             , JAF_BY_DATE_OF_BIRTH               ");
        _sb.AppendLine("             , JAF_AGE_RANGE                      ");
        _sb.AppendLine("             , JAF_OLDER_THAN                     ");
        _sb.AppendLine("             , JAF_AND_SMALLER_THAN               ");
        _sb.AppendLine("             , JAF_SMALLER_THAN                   ");
        _sb.AppendLine("             , JAF_OR_OLDER_THAN                  ");
        _sb.AppendLine("             , JAF_BY_CREATION_DATE               ");
        _sb.AppendLine("             , JAF_JUST_DAY_OF_CREATION           ");
        _sb.AppendLine("             , JAF_DAY_OF_CREATION_PLUS           ");
        _sb.AppendLine("             , JAF_ANY_DAY_OF_MONTH               ");
        _sb.AppendLine("             , JAF_DAY_AND_MONTH                  ");
        _sb.AppendLine("             , JAF_CREATION_BETWEEN_THIS          ");
        _sb.AppendLine("             , JAF_CREATION_BETWEEN_THAT          ");
        _sb.AppendLine("             , JAF_SELECTION_EGM_TYPE             ");
        _sb.AppendLine("             , JAF_SELECTED_EGM                   ");
        _sb.AppendLine("             , JAF_MINIMUM_BET                    ");
        _sb.AppendLine("             , JAF_LAST_UPDATE                  ) ");
        _sb.AppendLine("      VALUES                                      ");
        _sb.AppendLine("             ( @pJackpotId                        ");
        _sb.AppendLine("             , @pExcludeAnonymousAccount          ");
        _sb.AppendLine("             , @pExcludeAccountsWithPromo         ");
        _sb.AppendLine("             , @pMinOccupancy                     ");
        _sb.AppendLine("             , @pByGender                         ");
        _sb.AppendLine("             , @pByDateOfBirth                    ");
        _sb.AppendLine("             , @pAgeRange                         ");
        _sb.AppendLine("             , @pOlderThan                        ");
        _sb.AppendLine("             , @pAndSmallerThan                   ");
        _sb.AppendLine("             , @pSmallerThan                      ");
        _sb.AppendLine("             , @pOrOlderThan                      ");
        _sb.AppendLine("             , @pByCreationDate                   ");
        _sb.AppendLine("             , @pJustDayOfCreation                ");
        _sb.AppendLine("             , @pDayOfCreationPlus                ");
        _sb.AppendLine("             , @pAnyDayOfMonth                    ");
        _sb.AppendLine("             , @pDayAndMonth                      ");
        _sb.AppendLine("             , @pCreationBetweenThis              ");
        _sb.AppendLine("             , @pCreationBetweenThat              ");
        _sb.AppendLine("             , @pJackpotSelectionEGMType          ");
        _sb.AppendLine("             , @pJackpotSelectedEGM               ");
        _sb.AppendLine("             , @pMinimumBet                       ");
        _sb.AppendLine("             , @pLastUpdate                     ) ");

        _sb.AppendLine(" SET     @pId = SCOPE_IDENTITY()                  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId"               , SqlDbType.Int   ).Value = this.JackpotId;
          _cmd.Parameters.Add("@pExcludeAnonymousAccount" , SqlDbType.Bit   ).Value = this.ExcludeAnonymousAccount;
          _cmd.Parameters.Add("@pExcludeAccountsWithPromo", SqlDbType.Bit   ).Value = this.ExcludeAccountsWithPromo;
          _cmd.Parameters.Add("@pMinOccupancy"            , SqlDbType.Money ).Value = this.MinOccupancy;          
          _cmd.Parameters.Add("@pAgeRange"                , SqlDbType.Int   ).Value = this.AgeRange; 
          _cmd.Parameters.Add("@pByCreationDate"          , SqlDbType.Int   ).Value = this.ByCreationDate;
          _cmd.Parameters.Add("@pJustDayOfCreation"       , SqlDbType.Int   ).Value = this.JustDayOfCreation;
          _cmd.Parameters.Add("@pDayOfCreationPlus"       , SqlDbType.Int   ).Value = this.DayOfCreationPlus;
          _cmd.Parameters.Add("@pAnyDayOfMonth"           , SqlDbType.Int   ).Value = this.AnyDayOfMonth;
          _cmd.Parameters.Add("@pDayAndMonth"             , SqlDbType.Int   ).Value = this.DayAndMonth;
          _cmd.Parameters.Add("@pMinimumBet"              , SqlDbType.Money ).Value = this.MinimumBet;

          // Nullable fields
          _cmd.Parameters.Add("@pByGender"                , SqlDbType.Int   ).Value = this.ByGender;
          _cmd.Parameters.Add("@pByDateOfBirth"           , SqlDbType.Int   ).Value = this.ByDateOfBirth;
          _cmd.Parameters.Add("@pOlderThan"               , SqlDbType.Int   ).Value = this.OlderThan;
          _cmd.Parameters.Add("@pAndSmallerThan"          , SqlDbType.Int   ).Value = this.AndSmallerThan;
          _cmd.Parameters.Add("@pSmallerThan"             , SqlDbType.Int   ).Value = this.SmallerThan;
          _cmd.Parameters.Add("@pOrOlderThan"             , SqlDbType.Int   ).Value = this.OrOlderThan;
          _cmd.Parameters.Add("@pCreationBetweenThat"     , SqlDbType.Int   ).Value = this.CreationBetweenThat;
          _cmd.Parameters.Add("@pCreationBetweenThis"     , SqlDbType.Int   ).Value = this.CreationBetweenThis;
          _cmd.Parameters.Add("@pJackpotSelectedEGM"      , SqlDbType.Xml   ).Value = (String.IsNullOrEmpty (this.SelectedEgm)) ? DBNull.Value : (Object)this.SelectedEgm;
          _cmd.Parameters.Add("pJackpotSelectionEGMType"  , SqlDbType.Int   ).Value = this.SelectionEGMType;

          // Last update
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = (this.LastUpdate == WGDB.MinDate) ? DBNull.Value : (Object)this.LastUpdate;
   
          //Output parameter
          _op = new SqlParameter("@pId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);
          
          if (_cmd.ExecuteNonQuery() == 1)
          {
            this.Id = (Int32)_op.Value;
            this.Flags.JackpotId = this.JackpotId;

            if (!this.DB_SaveSpecialProperties(SqlTrx))
            {
            return false;
          }

          return true;
        }

          Log.Error(String.Format("JackpotAwardFilters.DB_Insert -> JackpotId: {0}", this.JackpotId));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardFilters.DB_Insert -> JackpotId: {0}", this.JackpotId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Jackpot Award Days in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   JACKPOTS_AWARD_FILTERS                                        ");
        _sb.AppendLine("    SET   JAF_JACKPOT_ID                  = @pJackpotId                 ");
        _sb.AppendLine("        , JAF_EXCLUDE_ANONYMOUS_ACCOUNTS  = @pExcludeAnonymousAccount   ");
        _sb.AppendLine("        , JAF_EXCLUDE_ACCOUNTS_WITH_PROMO = @pExcludeAccountsWithPromo  ");
        _sb.AppendLine("        , JAF_MIN_OCCUPANCY               = @pMinOccupancy              ");
        _sb.AppendLine("        , JAF_BY_GENDER                   = @pByGender                  ");
        _sb.AppendLine("        , JAF_BY_DATE_OF_BIRTH            = @pByDateOfBirth             ");
        _sb.AppendLine("        , JAF_AGE_RANGE                   = @pAgeRange                  ");
        _sb.AppendLine("        , JAF_OLDER_THAN                  = @pOlderThan                 ");
        _sb.AppendLine("        , JAF_AND_SMALLER_THAN            = @pAndSmallerThan            ");
        _sb.AppendLine("        , JAF_SMALLER_THAN                = @pSmallerThan               ");
        _sb.AppendLine("        , JAF_OR_OLDER_THAN               = @pOrOlderThan               ");
        _sb.AppendLine("        , JAF_BY_CREATION_DATE            = @pByCreationDate            ");
        _sb.AppendLine("        , JAF_JUST_DAY_OF_CREATION        = @pJustDayOfCreation         ");
        _sb.AppendLine("        , JAF_DAY_OF_CREATION_PLUS        = @pDayOfCreationPlus         ");
        _sb.AppendLine("        , JAF_ANY_DAY_OF_MONTH            = @pAnyDayOfMonth             ");
        _sb.AppendLine("        , JAF_DAY_AND_MONTH               = @pDayAndMonth               ");
        _sb.AppendLine("        , JAF_CREATION_BETWEEN_THIS       = @pCreationBetweenThis       ");
        _sb.AppendLine("        , JAF_CREATION_BETWEEN_THAT       = @pCreationBetweenThat       ");
        _sb.AppendLine("        , JAF_SELECTION_EGM_TYPE          = @pJackpotSelectionEGMType   ");
        _sb.AppendLine("        , JAF_SELECTED_EGM                = @pJackpotSelectedEGM        ");
        _sb.AppendLine("        , JAF_MINIMUM_BET                 = @pMinimumBet                ");
        _sb.AppendLine("        , JAF_LAST_UPDATE                 = @pLastUpdate                ");
        _sb.AppendLine("  WHERE   JAF_ID                          = @pId                        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId"                      , SqlDbType.Int   ).Value = this.Id;
          _cmd.Parameters.Add("@pJackpotId"               , SqlDbType.Int   ).Value = this.JackpotId;
          _cmd.Parameters.Add("@pExcludeAnonymousAccount" , SqlDbType.Bit   ).Value = this.ExcludeAnonymousAccount;
          _cmd.Parameters.Add("@pExcludeAccountsWithPromo", SqlDbType.Bit   ).Value = this.ExcludeAccountsWithPromo;
          _cmd.Parameters.Add("@pMinOccupancy"            , SqlDbType.Money ).Value = this.MinOccupancy;    
          _cmd.Parameters.Add("@pAgeRange"                , SqlDbType.Int   ).Value = this.AgeRange;
          _cmd.Parameters.Add("@pByCreationDate"          , SqlDbType.Int   ).Value = this.ByCreationDate;
          _cmd.Parameters.Add("@pJustDayOfCreation"       , SqlDbType.Int   ).Value = this.JustDayOfCreation;
          _cmd.Parameters.Add("@pDayOfCreationPlus"       , SqlDbType.Int   ).Value = this.DayOfCreationPlus;
          _cmd.Parameters.Add("@pAnyDayOfMonth"           , SqlDbType.Int   ).Value = this.AnyDayOfMonth;
          _cmd.Parameters.Add("@pDayAndMonth"             , SqlDbType.Int   ).Value = this.DayAndMonth;  
          _cmd.Parameters.Add("@pMinimumBet"              , SqlDbType.Money ).Value = this.MinimumBet;
          
          // Nullable fields
          _cmd.Parameters.Add("@pByGender"                , SqlDbType.Int   ).Value = this.ByGender;
          _cmd.Parameters.Add("@pByDateOfBirth"           , SqlDbType.Int   ).Value = this.ByDateOfBirth;
          _cmd.Parameters.Add("@pOlderThan"               , SqlDbType.Int   ).Value = this.OlderThan;
          _cmd.Parameters.Add("@pAndSmallerThan"          , SqlDbType.Int   ).Value = this.AndSmallerThan;
          _cmd.Parameters.Add("@pSmallerThan"             , SqlDbType.Int   ).Value = this.SmallerThan;
          _cmd.Parameters.Add("@pOrOlderThan"             , SqlDbType.Int   ).Value = this.OrOlderThan;
          _cmd.Parameters.Add("@pCreationBetweenThat"     , SqlDbType.Int   ).Value = this.CreationBetweenThat;
          _cmd.Parameters.Add("@pCreationBetweenThis"     , SqlDbType.Int   ).Value = this.CreationBetweenThis;
          _cmd.Parameters.Add("@pJackpotSelectedEGM"      , SqlDbType.Xml   ).Value = (String.IsNullOrEmpty (this.SelectedEgm)) ? DBNull.Value : (Object)this.SelectedEgm;
          _cmd.Parameters.Add("@pJackpotSelectionEGMType" , SqlDbType.Int   ).Value = this.SelectionEGMType;

          // Last update
          this.LastUpdate = this.LastModification;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            if (!this.DB_SaveSpecialProperties(SqlTrx))
          {
            return false;
          }

          return true;
        }

          Log.Error(String.Format("JackpotAwardFilters.DB_Update -> JackpotSettingsAwardingId: {0}", this.Id));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotAwardFilters.DB_Update -> JackpotSettingsAwardingId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Update

    /// <summary>
    /// Save special transaction properties
    /// </summary>
    /// <returns></returns>
    internal override Boolean DB_SaveSpecialProperties(SqlTransaction SqlTrx)
    {
      if (!this.Flags.Save(SqlTrx))
      {
        return false;
      }

      return true;
    } // DB_SaveSpecialProperties 
    
    /// <summary>
    /// Read special Jackpot members:
    ///   - AwardDays
    ///   - Contributions
    ///   - AwardPrizeConfig
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_ReadSpecialData(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      this.Flags = new JackpotAwardFilterFlagList();

      // Read Jackpot Contributions
      if (!this.Flags.Read(JackpotId, SqlTrx))
      {
        return false;
      }

      return true;

    } // DB_ReadSpecialData

    /// <summary>
    /// Clone JackpotAwardFilter
    /// </summary>
    internal JackpotAwardFilter Clone()
    {
      return new JackpotAwardFilter()
      {
        Id                        = this.Id,
        JackpotId                 = this.JackpotId,
        AgeRange                  = this.AgeRange,
        AndSmallerThan            = this.AndSmallerThan,
        AnyDayOfMonth             = this.AnyDayOfMonth,
        ByCreationDate            = this.ByCreationDate,
        ByDateOfBirth             = this.ByDateOfBirth,
        ByGender                  = this.ByGender,
        CreationBetweenThat       = this.CreationBetweenThat,
        CreationBetweenThis       = this.CreationBetweenThis,
        DayAndMonth               = this.DayAndMonth,
        DayOfCreationPlus         = this.DayOfCreationPlus,
        ExcludeAccountsWithPromo  = this.ExcludeAccountsWithPromo,
        ExcludeAnonymousAccount   = this.ExcludeAnonymousAccount,
        JustDayOfCreation         = this.JustDayOfCreation,
        MinOccupancy              = this.MinOccupancy,
        OlderThan                 = this.OlderThan,
        OrOlderThan               = this.OrOlderThan,
        SmallerThan               = this.SmallerThan, 
        SelectedEgm               = this.SelectedEgm,
        SelectionEGMType          = this.SelectionEGMType,
        Flags                     = this.Flags.Clone (),
        MinimumBet                = this.MinimumBet,
        LastUpdate                = this.LastUpdate
      };
    } // Clone

    
    #endregion 

  } // JackpotAwardFilter
}
