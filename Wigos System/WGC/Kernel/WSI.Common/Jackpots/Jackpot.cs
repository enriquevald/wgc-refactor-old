﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Jackpot.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpot
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 30-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAR-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace WSI.Common.Jackpot
{
  public class Jackpot : JackpotBase
  {
    #region " Constants "

    // Private constants
    private const Int32 DB_JACKPOT_ID               = 0;
    private const Int32 DB_JACKPOT_NAME             = 1;
    private const Int32 DB_ENABLED                  = 2;
    private const Int32 DB_TYPE                     = 3;
    private const Int32 DB_ISO_CODE                 = 4;
    private const Int32 DB_MINIMUM                  = 5;
    private const Int32 DB_MAXIMUM                  = 6;
    private const Int32 DB_AVERAGE                  = 7;
    private const Int32 DB_PAYOUT_MODE              = 8;
    private const Int32 DB_CREDIT_PRIZE_TYPE        = 9;
    private const Int32 DB_ADV_NOTIFICATION_ENABLED = 10;
    private const Int32 DB_ADV_NOTIFICATION_TIME    = 11;
    private const Int32 DB_ADV_NOTIFICATION_FLAG    = 12;
    private const Int32 DB_NUM_PENDING              = 13;
    private const Int32 DB_COMPENSATION_TYPE        = 14;
    private const Int32 DB_COMPENSATION_FIXED_PCT   = 15;
    private const Int32 DB_PRIZE_SHARING_PCT        = 16;
    private const Int32 DB_HIDDEN_PCT               = 17;
    private const Int32 DB_HAPPY_HOUR_PCT           = 18;
    private const Int32 DB_LAST_UPDATE              = 19;
    private const Int32 DB_MAX_NUM_PENDING          = 20;
    private const Int32 DB_PROBABILITY_WITH_MAX     = 21;

    #endregion

    #region " Members "

    JackpotContributionMeters m_contribution_meter;

    #endregion

    #region " Properties "

    public String Name { get; set; }
    public Boolean Enabled { get; set; }
    public AreaType Type { get; set; }
    public String IsoCode { get; set; }
    public Decimal Minimum { get; set; }
    public Decimal Maximum { get; set; }
    public Decimal Average { get; set; }
    public PayoutModeType PayoutMode { get; set; }
    public ContributionCreditType CreditPrizeType { get; set; }
    public Boolean AdvNotificationEnabled { get; set; }
    public Int32 AdvNotificationTime { get; set; }
    public Boolean AdvNotificationFlag { get; set; }
    public Int32 NumPending { get; set; }
    public ContributionCompensationType CompensationType { get; set; }
    public Decimal CompensationFixedPct { get; set; }
    public Decimal PrizeSharingPct { get; set; }
    public Decimal HiddenPct { get; set; }
    public Decimal HappyHourPct { get; set; }
    public Int32 TodayMinutesFrom { get; set; }
    public Int32 TodayMinutesTo { get; set; }
    public Int32 MaximumNumPending { get; set; }
    public Boolean ProbabilityWithMax { get; set; }

    public Decimal MainPct
    {
      get { return 100 - (this.PrizeSharingPct + this.HiddenPct + this.HappyHourPct); }
    }

    public override Boolean IsConfigured
    {
      get
      {
        return this.LastUpdate > WGDB.MinDate
            //&& this.m_contributions.IsConfigured // Not necessary, is configured in creation of Jackpot.
            && this.AwardConfig.IsConfigured
            && this.AwardPrizeConfig.IsConfigured;
      }
    }

    public override Boolean IsModified
    {
      get
      {
        return this.LastModification > WGDB.MinDate
          //&& this.m_contributions.IsModified // Not necessary, is configured in creation of Jackpot.
            || this.AwardConfig.IsModified
            || this.AwardPrizeConfig.IsModified;
      }
    }
        

    ////////////////////////////////////////////
    //          Special properties            //
    ////////////////////////////////////////////
    
    public JackpotAwardConfig AwardConfig { get; set; }
    public JackpotContributionList Contributions { get; set; }
    public JackpotAwardPrizeConfig AwardPrizeConfig { get; set; }

    public JackpotContributionMeters ContributionMeters
    {
      get
      { 
        if (this.m_contribution_meter == null)
        {
          this.m_contribution_meter = new JackpotContributionMeters(this.Id);
        }        
        return this.m_contribution_meter; 
      }
      set { this.m_contribution_meter = value; }
    }

    #endregion " Properties "

    #region " Public Methods "

    #region " Constructors "

    public Jackpot()
    {
      this.Id                     = DEFAULT_ID_VALUE;
      this.Name                   = String.Empty;
      this.Enabled                = false;
      this.Type                   = AreaType.LocalMistery;
      this.IsoCode                = String.Empty;
      this.Minimum                = DEFAULT_DECIMAL_VALUE;
      this.Maximum                = DEFAULT_DECIMAL_VALUE;
      this.Average                = DEFAULT_DECIMAL_VALUE;
      this.PayoutMode             = PayoutModeType.Manual;
      this.CreditPrizeType        = ContributionCreditType.Redeemable;
      this.AdvNotificationEnabled = false;
      this.AdvNotificationTime    = DEFAULT_INT_VALUE;
      this.AdvNotificationFlag    = false;
      this.NumPending             = DEFAULT_INT_VALUE;
      this.CompensationType       = ContributionCompensationType.C0;
      this.CompensationFixedPct   = DEFAULT_DECIMAL_VALUE;
      this.PrizeSharingPct        = DEFAULT_INT_VALUE;
      this.HiddenPct              = DEFAULT_INT_VALUE;
      this.HappyHourPct           = DEFAULT_INT_VALUE;
      this.ProbabilityWithMax     = false;
      this.MaximumNumPending      = 1;

      this.LastUpdate             = WGDB.MinDate;
      this.LastModification       = WGDB.MinDate;
      
      this.AwardConfig            = new JackpotAwardConfig();
      this.Contributions          = new JackpotContributionList();
      this.AwardPrizeConfig       = new JackpotAwardPrizeConfig();

    } // Jackpot

    public Jackpot(Int32 JackpotId)
    {
      if(!this.DB_GetJackpotById(JackpotId))
      {
        Log.Error(String.Format("Error on load Jackpot. JackpotId {0}", JackpotId));
      }
    } // Jackpot

    public Jackpot(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      if (!this.DB_GetJackpotById(JackpotId, SqlTrx))
      {
        Log.Error(String.Format("Error on load Jackpot. JackpotId {0}", JackpotId));
      }
    } // Jackpot

    #endregion 

    /// <summary>
    /// Get all jackpot list
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public List<Jackpot> GetAllJackpots()
    {
      return this.GetAllJackpots(JackpotFilterType.All);
    } // GetAllJackpots
    public List<Jackpot> GetAllJackpots(JackpotFilterType Filter)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.GetAllJackpots(Filter, _db_trx.SqlTransaction);
      }
    } // GetAllJackpots
    public List<Jackpot> GetAllJackpots(SqlTransaction SqlTrx)
    {
      return this.GetAllJackpots(JackpotFilterType.All, SqlTrx);
    } // GetAllJackpots
    public List<Jackpot> GetAllJackpots(JackpotFilterType Filter, SqlTransaction SqlTrx)
    {
      return this.DB_GetAllJackpots(Filter, SqlTrx);
    } // GetAllJackpots
    public Boolean GetAllJackpots(out DataTable Jackpots)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_GetAllJackpots_DataTable(out Jackpots, _db_trx.SqlTransaction);
      }
    } // GetAllJackpots

    /// <summary>
    /// Convert Jackpot item into a TreeViewItem
    /// </summary>
    /// <param name="MenuItem_OnClick"></param>
    /// <returns></returns>
    public TreeViewItem ToTreeViewItem()
    {
      return ToTreeViewItem(null);
    } // ToTreeViewItem
    public TreeViewItem ToTreeViewItem(EventHandler MenuItem_OnClick)
    {
      return new TreeViewItem()
      {
        Id          = this.Id,
        Description = this.Name,
        Status      = GetNodeStatus(),
        Type        = EditSelectionItemType.JACKPOT_CONFIG,
        Nodes       = GetConfigNodes(),
        ContextMenu = null
      };
    } // ToTreeViewItem
    
    /// <summary>
    /// Get context menu of Jackpot
    /// </summary>
    /// <param name="MenuItem_OnClick"></param>
    /// <returns></returns>
    public ContextMenu GetContextMenu(EventHandler MenuItem_OnClick)
    {
      return new ContextMenu()
      {
        Name      = this.Id.ToString(),
        MenuItems = 
        {
          GetMenuItem(ContextMenuAction.JACKPOT_CLONE , Resource.String("STR_JACKPOT_NODE_CONTEXTMENU_CLONE")           , MenuItem_OnClick),
          GetMenuItem(ContextMenuAction.JACKPOT_ADD   , Resource.String("STR_JACKPOT_NODE_CONTEXTMENU_ADD_EXTRA_MONEY") , MenuItem_OnClick),
          GetMenuItem(ContextMenuAction.JACKPOT_RESET , Resource.String("STR_JACKPOT_NODE_CONTEXTMENU_RESET")           , MenuItem_OnClick),
        }
      };
    } // GetContextMenu
    
    /// <summary>
    /// Add or substract num Pending.
    /// </summary>
    /// <param name="NumPending"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean AddNumPending(Int32 NumPending, SqlTransaction SqlTrx)
    {
      this.NumPending += NumPending;

      return this.DB_UpdateNumPending(SqlTrx);
    } // AddNumPending

    /// <summary>
    /// Add or substract num Pending.
    /// </summary>
    /// <param name="NumPending"></param>
    /// <returns></returns>
    public Boolean AddNumPending(Int32 NumPending)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        AddNumPending(NumPending, _db_trx.SqlTransaction);
        return _db_trx.Commit();
      }
    } // AddNumPending

    /// <summary>
    /// Add Notification flag
    /// </summary>
    /// <param name="NotificationFlag"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean AddNotificationFlag(Boolean NotificationFlag, SqlTransaction SqlTrx)
    {
      this.AdvNotificationFlag = NotificationFlag;

      return this.DB_UpdateNumPending(SqlTrx);
    } // AddNumpending
    
    /// <summary>
    /// Add Notification flag
    /// </summary>
    /// <param name="NumPending"></param>
    /// <returns></returns>
    public Boolean AddNotificationFlag(Boolean NotificationFlag)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        AddNotificationFlag(NotificationFlag, _db_trx.SqlTransaction);
        return _db_trx.Commit();
      }
    } // AddNumpending

    /// <summary>
    /// Reset all members id
    /// </summary>
    public override void Reset()
    {
      base.Reset();

      this.Contributions.Reset();
      this.AwardConfig.Reset();
      this.AwardPrizeConfig.Reset();
    } // Reset
    
    /// <summary>
    /// Clone Jackpot
    /// </summary>
    /// <returns></returns>
    public Jackpot Clone()
    {
      return this.Clone_Internal();
    } // Clone
    
    #endregion " Public Methods "

    #region " Internal Methods "

    #region " DataBase Methods " 

    /// <summary>
    /// Save special transaction properties
    /// </summary>
    /// <returns></returns>
    internal override Boolean DB_SaveSpecialProperties(SqlTransaction SqlTrx)
    {
      if (!this.Contributions.Save(SqlTrx))
      {
        return false;
      }

      if (!this.AwardConfig.Save(SqlTrx))
      {
        return false;
      }

      if (!this.AwardPrizeConfig.Save(SqlTrx))
      {
        return false;
      }

      return true;
    } // DB_SaveSpecialProperties 

    /// <summary>
    /// Insert an specific Jackpot in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _op;

      _sb = new StringBuilder();

      try
      {        
        _sb.AppendLine(" INSERT INTO    JACKPOTS_SETTINGS           ");
        _sb.AppendLine("              ( JS_NAME                     ");
        _sb.AppendLine("              , JS_ENABLED                  ");
        _sb.AppendLine("              , JS_TYPE                     ");
        _sb.AppendLine("              , JS_ISO_CODE                 ");
        _sb.AppendLine("              , JS_MINIMUM                  ");
        _sb.AppendLine("              , JS_MAXIMUM                  ");
        _sb.AppendLine("              , JS_AVERAGE                  ");  
        _sb.AppendLine("              , JS_PAYOUT_MODE              ");
        _sb.AppendLine("              , JS_CREDIT_PRIZE_TYPE        ");
        _sb.AppendLine("              , JS_ADV_NOTIFICATION_ENABLED ");
        _sb.AppendLine("              , JS_ADV_NOTIFICATION_TIME    ");
        _sb.AppendLine("              , JS_ADV_NOTIFICATION_FLAG    ");
        _sb.AppendLine("              , JS_NUM_PENDING              ");
        _sb.AppendLine("              , JS_MAX_NUM_PENDING          ");
        _sb.AppendLine("              , JS_PROBABILITY_WITH_MAX     ");
        _sb.AppendLine("              , JS_COMPENSATION_TYPE        ");
        _sb.AppendLine("              , JS_COMPENSATION_FIXED_PCT   ");
        _sb.AppendLine("              , JS_PRIZE_SHARING_PCT        ");
        _sb.AppendLine("              , JS_HIDDEN_PCT               ");
        _sb.AppendLine("              , JS_HAPPY_HOUR_PCT           ");
        _sb.AppendLine("              , JS_LAST_UPDATE        )     ");
        _sb.AppendLine("      VALUES                                ");
        _sb.AppendLine("              ( @pName                      ");
        _sb.AppendLine("              , @pEnabled                   ");
        _sb.AppendLine("              , @pType                      ");
        _sb.AppendLine("              , @pIsoCode                   ");
        _sb.AppendLine("              , @pMinimum                   ");
        _sb.AppendLine("              , @pMaximum                   ");
        _sb.AppendLine("              , @pAverage                   ");
        _sb.AppendLine("              , @pPayoutMode                ");
        _sb.AppendLine("              , @pCreditPrizeType           ");
        _sb.AppendLine("              , @pAdvNotificationEnabled    ");
        _sb.AppendLine("              , @pAdvNotificationTime       ");
        _sb.AppendLine("              , @pAdvNotificationFlag       ");
        _sb.AppendLine("              , @pNumPending                ");
        _sb.AppendLine("              , @pMaxNumPending             ");
        _sb.AppendLine("              , @pProbabilityWithMax        ");
        _sb.AppendLine("              , @pCompensationType          ");
        _sb.AppendLine("              , @pCompensationFixedPct      ");
        _sb.AppendLine("              , @pPrizeSharingPct           ");
        _sb.AppendLine("              , @pHiddenPct                 ");
        _sb.AppendLine("              , @pHappyHourPct              ");
        _sb.AppendLine("              , @pLastUpdate          )     ");

        _sb.AppendLine(" SET     @pJackpotId = SCOPE_IDENTITY()     ");
        
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pName"                  , SqlDbType.NVarChar).Value = this.Name;
          _cmd.Parameters.Add("@pEnabled"               , SqlDbType.Bit     ).Value = this.Enabled;
          _cmd.Parameters.Add("@pType"                  , SqlDbType.Int     ).Value = this.Type;
          _cmd.Parameters.Add("@pIsoCode"               , SqlDbType.NVarChar).Value = this.IsoCode;          
          _cmd.Parameters.Add("@pMinimum"               , SqlDbType.Money   ).Value = this.Minimum;
          _cmd.Parameters.Add("@pMaximum"               , SqlDbType.Money   ).Value = this.Maximum;
          _cmd.Parameters.Add("@pAverage"               , SqlDbType.Money   ).Value = this.Average;
          _cmd.Parameters.Add("@pPayoutMode"            , SqlDbType.Int     ).Value = this.PayoutMode;
          _cmd.Parameters.Add("@pCreditPrizeType"       , SqlDbType.Int     ).Value = this.CreditPrizeType;
          _cmd.Parameters.Add("@pAdvNotificationEnabled", SqlDbType.Bit     ).Value = this.AdvNotificationEnabled;
          _cmd.Parameters.Add("@pAdvNotificationTime"   , SqlDbType.Int     ).Value = this.AdvNotificationTime;
          _cmd.Parameters.Add("@pAdvNotificationFlag"   , SqlDbType.Bit     ).Value = this.AdvNotificationFlag;
          _cmd.Parameters.Add("@pNumPending"            , SqlDbType.Int     ).Value = this.NumPending;
          _cmd.Parameters.Add("@pMaxNumPending"         , SqlDbType.Int     ).Value = this.MaximumNumPending;
          _cmd.Parameters.Add("@pProbabilityWithMax"    , SqlDbType.Bit     ).Value = this.ProbabilityWithMax;
          _cmd.Parameters.Add("@pCompensationType"      , SqlDbType.Int     ).Value = this.CompensationType;
          _cmd.Parameters.Add("@pCompensationFixedPct"  , SqlDbType.Decimal ).Value = this.CompensationFixedPct;
          _cmd.Parameters.Add("@pPrizeSharingPct"       , SqlDbType.Money   ).Value = this.PrizeSharingPct;
          _cmd.Parameters.Add("@pHiddenPct"             , SqlDbType.Money   ).Value = this.HiddenPct;
          _cmd.Parameters.Add("@pHappyHourPct"          , SqlDbType.Money   ).Value = this.HappyHourPct;

          // Last update:
          //   - In Jackpot main class Update when create Jackpot
          this.LastUpdate = WGDB.Now;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;
          
          //Output parameter
          _op = new SqlParameter("@pJackpotId", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);

          if (_cmd.ExecuteNonQuery() == 1)
          {
            this.Id = (Int32)_op.Value;
            
            this.AwardConfig.Id             = this.Id;
            this.Contributions.JackpotId    = this.Id;
            this.AwardPrizeConfig.JackpotId = this.Id;

            if (!this.DB_SaveSpecialProperties(SqlTrx))
            {
              return false;
            }

            // Recalculate amount with max/min if jackpot enabled
            if (this.Enabled)
            {
              return JackpotAmountOperations.RecalculateJackpotAmounts(this.Id, SqlTrx);
            }

            return true;
          }

          Log.Error(String.Format("Jackpot.DB_Insert -> Jackpot: {0}", this.Name));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Jackpot.DB_Insert -> Jackpot: {0}", this.Name));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Jackpot in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_Update(SqlTransaction SqlTrx)
    {
      SqlParameter _need_recalculate;
      Boolean _recalculate;
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("    SET  @pRecalculate = 0                                        ");
        _sb.AppendLine(" SELECT  @pRecalculate = 1                                        ");
        _sb.AppendLine("  FROM   JACKPOTS_SETTINGS                                        ");
        _sb.AppendLine(" WHERE ( JS_MINIMUM                   <> @pMinimum                ");
        _sb.AppendLine("    OR   JS_MAXIMUM                   <> @pMaximum                ");
        _sb.AppendLine("    OR   JS_ENABLED                   <> @pEnabled)               ");
        _sb.AppendLine("   AND   JS_JACKPOT_ID                = @pJackpotId               ");

        _sb.AppendLine(" UPDATE   JACKPOTS_SETTINGS                                       ");
        _sb.AppendLine("    SET   JS_NAME                     = @pName                    ");
        _sb.AppendLine("        , JS_ENABLED                  = @pEnabled                 ");
        _sb.AppendLine("        , JS_TYPE                     = @pType                    ");
        _sb.AppendLine("        , JS_ISO_CODE                 = @pIsoCode                 ");
        _sb.AppendLine("        , JS_MINIMUM                  = @pMinimum                 ");
        _sb.AppendLine("        , JS_MAXIMUM                  = @pMaximum                 ");
        _sb.AppendLine("        , JS_AVERAGE                  = @pAverage                 ");
        _sb.AppendLine("        , JS_PAYOUT_MODE              = @pPayoutMode              ");
        _sb.AppendLine("        , JS_CREDIT_PRIZE_TYPE        = @pCreditPrizeType         ");
        _sb.AppendLine("        , JS_ADV_NOTIFICATION_ENABLED = @pAdvNotificationEnabled  ");
        _sb.AppendLine("        , JS_ADV_NOTIFICATION_TIME    = @pAdvNotificationTime     ");
        _sb.AppendLine("        , JS_ADV_NOTIFICATION_FLAG    = @pAdvNotificationFlag     ");
        _sb.AppendLine("        , JS_NUM_PENDING              = @pNumPending              ");
        _sb.AppendLine("        , JS_MAX_NUM_PENDING          = @pMaxNumPending           ");
        _sb.AppendLine("        , JS_PROBABILITY_WITH_MAX     = @pProbabilityWithMax      ");
        _sb.AppendLine("        , JS_COMPENSATION_TYPE        = @pCompensationType        ");
        _sb.AppendLine("        , JS_COMPENSATION_FIXED_PCT   = @pCompensationFixedPct    ");
        _sb.AppendLine("        , JS_PRIZE_SHARING_PCT        = @pPrizeSharingPct         ");
        _sb.AppendLine("        , JS_HIDDEN_PCT               = @pHiddenPct               ");
        _sb.AppendLine("        , JS_HAPPY_HOUR_PCT           = @pHappyHourPct            ");
        _sb.AppendLine("        , JS_LAST_UPDATE              = @pLastUpdate              ");
        _sb.AppendLine("  WHERE   JS_JACKPOT_ID               = @pJackpotId               ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId"             , SqlDbType.Int     ).Value = this.Id;
          _cmd.Parameters.Add("@pName"                  , SqlDbType.NVarChar).Value = this.Name;
          _cmd.Parameters.Add("@pEnabled"               , SqlDbType.Bit     ).Value = this.Enabled;
          _cmd.Parameters.Add("@pType"                  , SqlDbType.Int     ).Value = this.Type;
          _cmd.Parameters.Add("@pIsoCode"               , SqlDbType.NVarChar).Value = this.IsoCode;          
          _cmd.Parameters.Add("@pMinimum"               , SqlDbType.Money   ).Value = this.Minimum;
          _cmd.Parameters.Add("@pMaximum"               , SqlDbType.Money   ).Value = this.Maximum;
          _cmd.Parameters.Add("@pAverage"               , SqlDbType.Money   ).Value = this.Average;
          _cmd.Parameters.Add("@pPayoutMode"            , SqlDbType.Int     ).Value = this.PayoutMode;
          _cmd.Parameters.Add("@pCreditPrizeType"       , SqlDbType.Int     ).Value = this.CreditPrizeType;
          _cmd.Parameters.Add("@pAdvNotificationEnabled", SqlDbType.Bit     ).Value = this.AdvNotificationEnabled;
          _cmd.Parameters.Add("@pAdvNotificationTime"   , SqlDbType.Int     ).Value = this.AdvNotificationTime;
          _cmd.Parameters.Add("@pAdvNotificationFlag"   , SqlDbType.Bit     ).Value = this.AdvNotificationFlag;
          _cmd.Parameters.Add("@pNumPending"            , SqlDbType.Int     ).Value = this.NumPending;
          _cmd.Parameters.Add("@pMaxNumPending"         , SqlDbType.Int     ).Value = this.MaximumNumPending;
          _cmd.Parameters.Add("@pProbabilityWithMax"    , SqlDbType.Bit     ).Value = this.ProbabilityWithMax;
          _cmd.Parameters.Add("@pCompensationType"      , SqlDbType.Int     ).Value = this.CompensationType;
          _cmd.Parameters.Add("@pCompensationFixedPct"  , SqlDbType.Decimal ).Value = this.CompensationFixedPct;
          _cmd.Parameters.Add("@pPrizeSharingPct"       , SqlDbType.Money   ).Value = this.PrizeSharingPct;
          _cmd.Parameters.Add("@pHiddenPct"             , SqlDbType.Money   ).Value = this.HiddenPct;
          _cmd.Parameters.Add("@pHappyHourPct"          , SqlDbType.Money   ).Value = this.HappyHourPct;

          // Last update:
          this.LastUpdate = this.LastModification;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;
          
          _need_recalculate = new SqlParameter("@pRecalculate", SqlDbType.Bit);
          _need_recalculate.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_need_recalculate);

          if (_cmd.ExecuteNonQuery() == 1)
          {
            if (!this.DB_SaveSpecialProperties(SqlTrx))
            {
              return false;
            }

            _recalculate = (Boolean)_need_recalculate.Value;

            // Recalculate current meter with new max and min values if has changed.
            if (_recalculate)
            {
              return JackpotAmountOperations.RecalculateJackpotAmounts(this.Id, SqlTrx);
            }

            return true;
          }

          Log.Error(String.Format("Jackpot.DB_Update -> JackpotId: {0}", this.Id));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Jackpot.DB_Update -> JackpotId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Update

    /// <summary>
    /// Read special Jackpot members:
    ///   - AwardDays
    ///   - Contributions
    ///   - AwardPrizeConfig
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal override Boolean DB_ReadSpecialData(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      this.Contributions    = new JackpotContributionList();
      this.AwardConfig      = new JackpotAwardConfig();
      this.AwardPrizeConfig = new JackpotAwardPrizeConfig();
      
      // Read Jackpot Contributions
      if (!this.Contributions.Read(JackpotId, SqlTrx))
      {
        return false;
      }

      // Read Jackpot Award days
      if (!this.AwardConfig.Read(JackpotId, SqlTrx))
      {
        return false;
      }

      // Read Jackpot Award prize
      if (!this.AwardPrizeConfig.Read(JackpotId, SqlTrx))
      {
        return false;
      }

      return true;

    } // DB_ReadSpecialData

    #endregion

    #region " Private Methods "

   
    /// <summary>
    /// Get all jackpot list
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<Jackpot> DB_GetAllJackpots(JackpotFilterType Filter, SqlTransaction SqlTrx)
    {
      List<Jackpot> _jackpot_list;
      Jackpot _jackpot;

      _jackpot_list = new List<Jackpot>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotQuery_Select(Filter), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _jackpot = this.JackpotMappFromSqlReader(_sql_reader, SqlTrx);

              if (_jackpot != null)
              {
                _jackpot_list.Add(_jackpot);
              }
            }
          }
          
          // Read special data of all Jackpots. 
          if(!this.DB_ReadAllJackpotSpecialData(_jackpot_list, SqlTrx))
          {
            Log.Error("Jackpot.DB_GetAllJackpots --> Error on read all Special data of Jackpots list");
          }

          return _jackpot_list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Jackpot.DB_GetAllJackpots --> Error on read all Jackpots list");
        Log.Exception(_ex);
      }

      return null;

    } // DB_GetAllJackpots
     
    /// <summary>
    /// Get Jackpots in DataTable
    /// </summary>
    /// <param name="DtJackpots"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetAllJackpots_DataTable(out DataTable DtJackpots, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtJackpots = new DataTable();

      try
      {
        _sb.AppendLine("   SELECT   JS_JACKPOT_ID      ");
        _sb.AppendLine("          , JS_NAME            ");
        _sb.AppendLine("     FROM   JACKPOTS_SETTINGS  ");
        _sb.AppendLine(" ORDER BY   JS_NAME ASC        ");
        _sb.AppendLine("          , JS_JACKPOT_ID ASC  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _da.Fill(DtJackpots);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetAllJackpots_DataTable
    
    /// <summary>
    /// Get data from specific Jackpot 
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetJackpotById(Int32 JackpotId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_GetJackpotById(JackpotId, _db_trx.SqlTransaction);
      }
    } // DB_GetJackpotById   
    private Boolean DB_GetJackpotById(Int32 JackpotId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_JackpotQuery_Select(JackpotFilterType.ByJackpotId), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if ( _sql_reader.Read() 
              && !this.DB_JackpotDataMapper(_sql_reader))
            {
              return false;
            }
          }

          // Read special data (AwardDays, Contribution groups...)
          if(!DB_ReadSpecialData(JackpotId, SqlTrx))
          {
            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Jackpot.DB_GetJackpotById -> JackpotId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetJackpotById

    /// <summary>
    /// Read all Jackpots special data
    /// </summary>
    /// <param name="JackpotList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_ReadAllJackpotSpecialData(List<Jackpot> JackpotList, SqlTransaction SqlTrx)
    {
      foreach(Jackpot _jackpot in JackpotList)
      {
        if(!_jackpot.DB_ReadSpecialData(_jackpot.Id, SqlTrx))
        {
          return false;
        }
      }

      return true;
    } // DB_ReadAllJackpotSpecialData

    /// <summary>
    /// Update num pending of specific Jackpot in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_UpdateNumPending(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" UPDATE   JACKPOTS_SETTINGS               ");
        _sb.AppendLine("    SET   JS_NUM_PENDING  = @pNumPending  ");
        _sb.AppendLine("  WHERE   JS_JACKPOT_ID   = @pJackpotId   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = this.Id;
          _cmd.Parameters.Add("@pNumPending", SqlDbType.Int).Value = this.NumPending;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("Jackpot.DB_UpdateNumPending -> JackpotId: {0}", Id));

            return false;
          }

              return true;
          }
        }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Jackpot.DB_UpdateNumPending -> JackpotId: {0}", Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateNumPending

    /// <summary>
    /// Update num flag notification of specific Jackpot in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_UpdateNumFlagNotification(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" UPDATE   JACKPOTS_SETTINGS                                ");
        _sb.AppendLine("    SET   JS_ADV_NOTIFICATION_FLAG = @pAdvNotificationFlag ");
        _sb.AppendLine("  WHERE   JS_JACKPOT_ID            = @pJackpotId           ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = this.Id;
          _cmd.Parameters.Add("@pAdvNotificationFlag", SqlDbType.Bit).Value = this.AdvNotificationFlag;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error(String.Format("Jackpot.DB_UpdateNumFlagNotification -> JackpotId: {0}", this.Id));

            return false;
          }

              return true;
          }
        }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Jackpot.DB_UpdateNumFlagNotification -> JackpotId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;

    } // DB_UpdateNumFlagNotification

    /// <summary>
    /// Load Jackpot data from SQLReader
    /// </summary>
    /// <param name="Jackpot"></param>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_JackpotDataMapper(SqlDataReader SqlReader)
    {
      return DB_JackpotDataMapper(this, SqlReader);
    } // DB_JackpotDataMapper
    private Boolean DB_JackpotDataMapper(Jackpot Jackpot, SqlDataReader SqlReader)
    {
      try
      {
        Jackpot.Id                     = SqlReader.GetInt32(DB_JACKPOT_ID);
        Jackpot.Name                   = SqlReader.GetString(DB_JACKPOT_NAME);
        Jackpot.Enabled                = SqlReader.GetBoolean(DB_ENABLED);
        Jackpot.Type                   = (AreaType)SqlReader.GetInt32(DB_TYPE);
        Jackpot.IsoCode                = SqlReader.GetString(DB_ISO_CODE);
        Jackpot.Minimum                = SqlReader.GetDecimal(DB_MINIMUM);
        Jackpot.Maximum                = SqlReader.GetDecimal(DB_MAXIMUM);
        Jackpot.Average                = SqlReader.GetDecimal(DB_AVERAGE);
        Jackpot.PayoutMode             = (PayoutModeType)SqlReader.GetInt32(DB_PAYOUT_MODE);
        Jackpot.CreditPrizeType        = (ContributionCreditType)SqlReader.GetInt32(DB_CREDIT_PRIZE_TYPE);
        Jackpot.AdvNotificationEnabled = SqlReader.GetBoolean(DB_ADV_NOTIFICATION_ENABLED);
        Jackpot.AdvNotificationTime    = SqlReader.GetInt32(DB_ADV_NOTIFICATION_TIME);        
        Jackpot.AdvNotificationFlag    = SqlReader.GetBoolean(DB_ADV_NOTIFICATION_FLAG);
        Jackpot.NumPending             = SqlReader.GetInt32(DB_NUM_PENDING);
        Jackpot.CompensationType       = (ContributionCompensationType)SqlReader.GetInt32(DB_COMPENSATION_TYPE);
        Jackpot.CompensationFixedPct   = SqlReader.GetDecimal(DB_COMPENSATION_FIXED_PCT);
        Jackpot.PrizeSharingPct        = SqlReader.GetDecimal(DB_PRIZE_SHARING_PCT);
        Jackpot.HiddenPct              = SqlReader.GetDecimal(DB_HIDDEN_PCT);
        Jackpot.HappyHourPct           = SqlReader.GetDecimal(DB_HAPPY_HOUR_PCT);
        Jackpot.LastUpdate             = SqlReader.GetDateTime(DB_LAST_UPDATE);
        Jackpot.MaximumNumPending      = SqlReader.GetInt32(DB_MAX_NUM_PENDING);
        Jackpot.ProbabilityWithMax     = SqlReader.GetBoolean(DB_PROBABILITY_WITH_MAX);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_JackpotDataMapper");
        Log.Exception(_ex);
      }

      Jackpot = null;

      return false;

    } // DB_JackpotDataMapper    

    /// <summary>
    /// Mapp jackpot from sql reader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Jackpot JackpotMappFromSqlReader(SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      Jackpot _jackpot;

      _jackpot = new Jackpot();

      if (!this.DB_JackpotDataMapper(_jackpot, SqlReader))
      {
        Log.Error(String.Format("Jackpot.JackpotMappFromSqlReader -> Error on load JackpotId: {0}", SqlReader.GetInt32(DB_JACKPOT_ID)));
      }

      return _jackpot;
    } // JackpotMappFromSqlReader

    /// <summary>
    /// Gets Jackpot select query
    /// </summary>
    /// <param name="GetAllJackpots"></param>
    /// <returns></returns>
    private String DB_JackpotQuery_Select(JackpotFilterType Filter)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   JS_JACKPOT_ID               "); // 0
      _sb.AppendLine("           , JS_NAME                     "); // 1
      _sb.AppendLine("           , JS_ENABLED                  "); // 2
      _sb.AppendLine("           , JS_TYPE                     "); // 3
      _sb.AppendLine("           , JS_ISO_CODE                 "); // 4
      _sb.AppendLine("           , JS_MINIMUM                  "); // 5
      _sb.AppendLine("           , JS_MAXIMUM                  "); // 6
      _sb.AppendLine("           , JS_AVERAGE                  "); // 7
      _sb.AppendLine("           , JS_PAYOUT_MODE              "); // 8
      _sb.AppendLine("           , JS_CREDIT_PRIZE_TYPE        "); // 9
      _sb.AppendLine("           , JS_ADV_NOTIFICATION_ENABLED "); // 10
      _sb.AppendLine("           , JS_ADV_NOTIFICATION_TIME    "); // 11
      _sb.AppendLine("           , JS_ADV_NOTIFICATION_FLAG    "); // 12
      _sb.AppendLine("           , JS_NUM_PENDING              "); // 13
      _sb.AppendLine("           , JS_COMPENSATION_TYPE        "); // 14
      _sb.AppendLine("           , JS_COMPENSATION_FIXED_PCT   "); // 15
      _sb.AppendLine("           , JS_PRIZE_SHARING_PCT        "); // 16
      _sb.AppendLine("           , JS_HIDDEN_PCT               "); // 17
      _sb.AppendLine("           , JS_HAPPY_HOUR_PCT           "); // 18
      _sb.AppendLine("           , JS_LAST_UPDATE              "); // 19
      _sb.AppendLine("           , JS_MAX_NUM_PENDING          "); // 20
      _sb.AppendLine("           , JS_PROBABILITY_WITH_MAX     "); // 21
      _sb.AppendLine("      FROM   JACKPOTS_SETTINGS           ");

      // Set filter
      _sb.AppendLine(DB_GetJackpotFilterSQL(Filter));
            
      return _sb.ToString();

    } // DB_JackpotQuery_Select

    /// <summary>
    /// Get SQL Filter of Jackpots table
    /// </summary>
    /// <param name="Filter"></param>
    /// <returns></returns>
    private string DB_GetJackpotFilterSQL(JackpotFilterType Filter)
    {
      switch(Filter)
      {
        case JackpotFilterType.ByJackpotId:
          // By Jackpot Id
          return "     WHERE   JS_JACKPOT_ID = @pJackpotId";

        case JackpotFilterType.Enabled:
          // Only enabled Jackpots
          return "     WHERE   JS_ENABLED = 1";

        case JackpotFilterType.Pending:
          // Only with num pending Jackpots
          return "     WHERE   JS_NUM_PENDING > 0";

        case JackpotFilterType.EnabledAndPending:
          // Only enabled and with pending Jackpots
          return "     WHERE   JS_ENABLED = 1 AND JS_NUM_PENDING > 0";

        case JackpotFilterType.All:
        default:
          // All Jackpots
          break;
      }

      return String.Empty;

    } // DB_GetJackpotFilterSQL

    #endregion 

    #region " Common Methods "
    
    /// <summary>
    /// Set fixed nodes to configure single Jackpot
    /// </summary>
    /// <returns></returns>
    private List<TreeViewItem> GetConfigNodes()
    {
      return new List<TreeViewItem>()
      {
        new TreeViewItem (this.Id, Resource.String("STR_JACKPOT_NODE_AWARD_RESTRICTIONS") , EditSelectionItemType.JACKPOT_AWARD_CONFIG, GetNodeStatus()),
        new TreeViewItem (this.Id, Resource.String("STR_JACKPOT_NODE_AWARD_PRIZES")       , EditSelectionItemType.JACKPOT_AWARD_PRIZE_CONFIG, GetNodeStatus())
      };
    } // GetConfigNodes
    
    /// <summary>
    /// Translate enabled to NodeStatus
    /// </summary>
    /// <returns></returns>
    private NodeStatus GetNodeStatus()
    {
      return (this.Enabled) ? NodeStatus.Enabled : NodeStatus.Disabled;
    } // GetNodeStatus

    /// <summary>
    /// Get menú item 
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="Tag"></param>
    /// <param name="Text"></param>
    /// <param name="MenuItem_OnClick"></param>
    /// <returns></returns>
    private MenuItem GetMenuItem(ContextMenuAction Tag, String Text, EventHandler MenuItem_OnClick)
    {
      return new MenuItem(Text, MenuItem_OnClick)
      {
        Tag = Tag,
        Name = this.Id.ToString()
      };
    } // GetMenuItem

    #endregion
    
    #endregion " Private Methods "

    #region " Clone "

    /// <summary>
    /// Clone Jackpot Internal
    /// </summary>
    /// <returns></returns>
    internal Jackpot Clone_Internal()
    {
      return new Jackpot()
      {
        Id                     = this.Id,
        Average                = this.Average,
        CreditPrizeType        = this.CreditPrizeType,
        Enabled                = this.Enabled,
        IsoCode                = this.IsoCode,
        Maximum                = this.Maximum,
        Minimum                = this.Minimum,
        Name                   = this.Name,
        PayoutMode             = this.PayoutMode,
        Type                   = this.Type,
        AdvNotificationEnabled = this.AdvNotificationEnabled,
        AdvNotificationTime    = this.AdvNotificationTime,
        AdvNotificationFlag    = this.AdvNotificationFlag,
        NumPending             = this.NumPending,
        CompensationType       = this.CompensationType,
        CompensationFixedPct   = this.CompensationFixedPct,
        PrizeSharingPct        = this.PrizeSharingPct,
        HiddenPct              = this.HiddenPct,
        HappyHourPct           = this.HappyHourPct,
        LastUpdate             = this.LastUpdate,
        MaximumNumPending      = this.MaximumNumPending,
        ProbabilityWithMax     = this.ProbabilityWithMax,

        // Classes
        AwardConfig            = this.AwardConfig.Clone(),
        AwardPrizeConfig       = this.AwardPrizeConfig.Clone(),
        Contributions          = this.Contributions.Clone(),     
        ContributionMeters     = this.ContributionMeters != null ? this.ContributionMeters.Clone() : null
        
      };
    } // Clone_Internal

    #endregion " Private Methods "
  } // Jackpot
}
