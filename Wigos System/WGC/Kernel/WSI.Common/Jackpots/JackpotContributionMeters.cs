﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotContributionMeters.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Mark Stansfield
// 
// CREATION DATE: 28-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-MAY-2017 MS     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.Jackpot;

namespace WSI.Common.Jackpot
{

  public class JackpotContributionMeters
  {

    #region " Properties "

    public Jackpot Jackpot { get; set; }
    public Int32 JackpotId { get; set; }
    public Decimal MeterAmount { get; set; }
    public Decimal CompensationAmount { get; set; }

    // Values based on supplied meter amount and configuration
    public Decimal HappyHourAmount { get; set; }
    public Decimal SharedAmount { get; set; }
    public Decimal MainAmount { get; set; }
    public Decimal HiddenAmount { get; set; }

    public Decimal ContributionHappyHourAmount { get; set; }
    public Decimal ContributionSharedAmount { get; set; }
    public Decimal ContributionMainAmount { get; set; }

    public Decimal ToCompensateAmount { get; set; }

    #endregion

    #region "Constructor"

    /// <summary>
    /// Constructor for split up.
    /// </summary>
    public JackpotContributionMeters()
    {
    } // JackpotContributionMeters()

    /// <summary>
    /// Constructor for get Meters.
    /// </summary>
    /// <param name="JackpotId"></param>
    public JackpotContributionMeters(Int32 JackpotId)
    {
      this.JackpotId = JackpotId;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        DB_GetMeterById(_db_trx.SqlTransaction);
      }
    } // JackpotContributionMeters(Int32 JackpotId)

    #endregion "Constructor"

    #region "Public Methods "

    /// <summary>
    /// Save jackpot contribution meters
    /// </summary>
    /// <param name="DoPerformSplit"></param>
    /// <param name="IsAmountOperation"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Save(Boolean DoPerformSplit, Boolean IsAmountOperation, SqlTransaction SqlTrx)
    {
      if (DoPerformSplit)
      {
        if (!PerformSplit(IsAmountOperation, SqlTrx))
        {
          return false;
        }
      }

      return BulkUpdate(IsAmountOperation, SqlTrx);

    } // Save

    /// <summary>
    /// Meter Data Mapper.
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean MeterDataMapper(SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      Jackpot.JackpotType _type;
      Decimal _amount;

      try
      {
        _type = (Jackpot.JackpotType)SqlReader["JCM_TYPE"];
        _amount = (Decimal)SqlReader["JCM_AMOUNT"];

        switch (_type)
        {
          case Jackpot.JackpotType.PrizeSharing:
            this.SharedAmount = _amount;
            break;

          case Jackpot.JackpotType.Hidden:
            this.HiddenAmount = _amount;
            break;

          case Jackpot.JackpotType.HappyHour:
            this.HappyHourAmount = _amount;
            break;

          case Jackpot.JackpotType.Main:
            this.MainAmount = _amount;
            break;

          case Jackpot.JackpotType.ToCompensate:
            this.ToCompensateAmount = _amount;
            break;

          default:
            Log.Warning("JackpotContributionBreakdown.MeterDataMapper JackpotType not Defined: " + _type);
            break;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // MeterDataMapper

    /// <summary>
    /// Function that returns if the maximum configurated in jackpots is under the amount of the same jackpot
    /// </summary>
    /// <param name="JackpotType">Type of jackpot that need to check</param>
    /// <param name="MaxJackpotAmount">Maximum value to hte each kind of jackpot</param>
    /// <returns></returns>
    public Boolean IsMaxJackpotAmountUnderCurrentAmount(Jackpot.JackpotType JackpotType, Decimal MaxJackpotAmount)
    {
      switch (JackpotType)
      {
        case Jackpot.JackpotType.Main:
          if (MaxJackpotAmount < MainAmount)
          {
            return true;
          }
          break;
        case Jackpot.JackpotType.PrizeSharing:
          if (MaxJackpotAmount < SharedAmount)
          {
            return true;
          }

          break;
        case Jackpot.JackpotType.HappyHour:
          if (MaxJackpotAmount < HappyHourAmount)
          {
            return true;
          }
          break;
        default:
          break;
      }

      return false;
    }

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Bulk update
    /// </summary>
    /// <param name="IsAmountOperation"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean BulkUpdate(Boolean IsAmountOperation, SqlTransaction SqlTrx)
    {
      DataTable _bulk_update;

      _bulk_update = CreateDtContributionMeters();

      //pass the table to Buildrow and add new row in method
      _bulk_update.Rows.Add(BuildRow(ref _bulk_update, Jackpot.Id, Jackpot.JackpotType.PrizeSharing, this.SharedAmount));
      _bulk_update.Rows.Add(BuildRow(ref _bulk_update, Jackpot.Id, Jackpot.JackpotType.Hidden, this.HiddenAmount));
      _bulk_update.Rows.Add(BuildRow(ref _bulk_update, Jackpot.Id, Jackpot.JackpotType.HappyHour, this.HappyHourAmount));
      _bulk_update.Rows.Add(BuildRow(ref _bulk_update, Jackpot.Id, Jackpot.JackpotType.Main, this.MainAmount));
      _bulk_update.Rows.Add(BuildRow(ref _bulk_update, Jackpot.Id, Jackpot.JackpotType.ToCompensate, this.ToCompensateAmount));

      if (IsAmountOperation)
      {
        this.ContributionSharedAmount = 0m;
        this.ContributionHappyHourAmount = 0m;
        this.ContributionMainAmount = 0m;
      }

      if (!DB_UpdateContributionMeters(_bulk_update, SqlTrx))
      {
        return false;
      }

      return true;
    } // BulkUpdate

    /// <summary>
    /// Create datatable
    /// </summary>
    /// <returns></returns>
    private DataTable CreateDtContributionMeters()
    {
      DataTable _temp_table;

      _temp_table = new DataTable();

      _temp_table.Columns.Add("JCM_JACKPOT_ID", Type.GetType("System.Int32"));
      _temp_table.Columns.Add("JCM_TYPE", Type.GetType("System.Int32"));
      _temp_table.Columns.Add("JCM_AMOUNT", Type.GetType("System.Decimal"));

      return _temp_table;
    } //CreateDtContributionMeters

    /// <summary>
    /// BuildRow
    /// </summary>
    /// <param name="BulkTable"></param>
    /// <param name="JackpotId"></param>
    /// <param name="Type"></param>
    /// <param name="Amount"></param>
    /// <returns></returns>
    private DataRow BuildRow(ref DataTable BulkTable, Int32 JackpotId, Jackpot.JackpotType Type, Decimal Amount)
    {
      DataRow _workrow;

      _workrow = BulkTable.NewRow();

      _workrow["JCM_JACKPOT_ID"] = JackpotId;
      _workrow["JCM_TYPE"] = Type;
      _workrow["JCM_AMOUNT"] = Amount;

      return _workrow;
    } // BuildRow

    /// <summary>
    /// DB_UpdateContributionMeters
    /// </summary>
    /// <param name="DtBreakdown"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_UpdateContributionMeters(DataTable DtBreakdown, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_updated;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" IF EXISTS ( SELECT   1                                ");
        _sb.AppendLine("               FROM   JACKPOTS_CONTRIBUTION_METERS     ");
        _sb.AppendLine("              WHERE   JCM_JACKPOT_ID  = @pJackpotId    ");
        _sb.AppendLine("                AND   JCM_TYPE        = @pJackpotType) ");
        _sb.AppendLine(" BEGIN                                                 ");
        _sb.AppendLine("   UPDATE   JACKPOTS_CONTRIBUTION_METERS               ");
        _sb.AppendLine("      SET   JCM_AMOUNT     = @pAmount                  ");
        _sb.AppendLine("    WHERE   JCM_JACKPOT_ID = @pJackpotId               ");
        _sb.AppendLine("      AND   JCM_TYPE       = @pJackpotType             ");
        _sb.AppendLine(" END                                                   ");
        _sb.AppendLine(" ELSE                                                  ");
        _sb.AppendLine(" BEGIN                                                 ");
        _sb.AppendLine(" INSERT INTO   JACKPOTS_CONTRIBUTION_METERS            ");
        _sb.AppendLine("             ( JCM_JACKPOT_ID                          ");
        _sb.AppendLine("             , JCM_TYPE                                ");
        _sb.AppendLine("             , JCM_AMOUNT                              ");
        _sb.AppendLine("             )                                         ");
        _sb.AppendLine("      VALUES                                           ");
        _sb.AppendLine("             ( @pJackpotId                             ");
        _sb.AppendLine("             , @pJackpotType                           ");
        _sb.AppendLine("             , @pAmount                                ");
        _sb.AppendLine("             )                                         ");
        _sb.AppendLine(" END                                                  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).SourceColumn = "JCM_JACKPOT_ID";
          _sql_cmd.Parameters.Add("@pJackpotType", SqlDbType.Int).SourceColumn = "JCM_TYPE";
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).SourceColumn = "JCM_AMOUNT";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;

            _num_updated = _sql_da.Update(DtBreakdown);

            if (_num_updated != DtBreakdown.Rows.Count)
            {
              Log.Message(String.Format("JackpotContributionBreakdown.DB_InsertBreakdown() - An error occurred updating into JACKPOTS_CONTRIBUTION: Rows affected <> {0}.",
                          DtBreakdown.Rows.Count));

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateContributionMeters

    /// <summary>
    /// Perform split
    /// </summary>
    /// <param name="IsAmountOperation"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean PerformSplit(Boolean IsAmountOperation, SqlTransaction SqlTrx)
    {
      Decimal _hidden;
      Decimal _recalculate_amount;

      _hidden = 0;
      _recalculate_amount = 0;

      if (!IsAmountOperation)
      {
        // Calculate Current Values
        _hidden = this.MeterAmount * this.Jackpot.HiddenPct / 100.0m;
        this.ContributionSharedAmount = this.MeterAmount * this.Jackpot.PrizeSharingPct / 100.0m;
        this.ContributionHappyHourAmount = this.MeterAmount * this.Jackpot.HappyHourPct / 100.0m;
        this.HiddenAmount += _hidden;
        this.ContributionMainAmount = this.MeterAmount - (_hidden + this.ContributionSharedAmount + this.ContributionHappyHourAmount);
      }

      // Shared jackpot
      if (!RecalculateAmount(this.SharedAmount,
                             this.ContributionSharedAmount,
                             this.Jackpot.AwardPrizeConfig.PrizeSharing.Minimum,
                             this.Jackpot.AwardPrizeConfig.PrizeSharing.Maximum,
                             this.Jackpot.AwardPrizeConfig.PrizeSharing.Enabled,
                             IsAmountOperation,
                             out _recalculate_amount))
      {
        return false;
      }
      this.SharedAmount = _recalculate_amount;

      // Happy hour jackpot
      if (!RecalculateAmount(this.HappyHourAmount,
                             this.ContributionHappyHourAmount,
                             this.Jackpot.AwardPrizeConfig.HappyHour.Minimum,
                             this.Jackpot.AwardPrizeConfig.HappyHour.Maximum,
                             !(this.Jackpot.AwardPrizeConfig.HappyHour.EventType == Jackpot.AwardHappyHourEvents.Disabled),
                             IsAmountOperation,
                             out _recalculate_amount))
      {
        return false;
      }
      this.HappyHourAmount = _recalculate_amount;

      // Main jackpot
      if (!RecalculateAmount(this.MainAmount,
                             this.ContributionMainAmount,
                             this.Jackpot.Minimum,
                             this.Jackpot.Maximum,
                             true,
                             IsAmountOperation,
                             out _recalculate_amount))
      {
        return false;
      }
      this.MainAmount = _recalculate_amount;

      // ToCompensate
      this.ToCompensateAmount -= this.CompensationAmount;

      return true;
    } // PerformSplit

    /// <summary>
    /// Recalculate shared, hh, main and ToCompensate amounts
    /// </summary>
    /// <param name="MeterAmount"></param>
    /// <param name="ContributionAmount"></param>
    /// <param name="MinimumAmount"></param>
    /// <param name="MaximumAmount"></param>
    /// <param name="IsEnabled"></param>
    /// <param name="IsAmountOperation"></param>
    /// <param name="RecalculateAmount"></param>
    /// <returns></returns>
    private Boolean RecalculateAmount(Decimal MeterAmount,
                                      Decimal ContributionAmount,
                                      Decimal MinimumAmount,
                                      Decimal MaximumAmount,
                                      Boolean IsEnabled,
                                      Boolean IsAmountOperation,
                                      out Decimal RecalculateAmount)
    {
      Decimal _diff;

      _diff = 0;
      RecalculateAmount = MeterAmount;

      try
      {
        // If disabled, the amount go to ToCompensate
        if (!IsEnabled)
        {
          this.ToCompensateAmount -= MeterAmount + ContributionAmount;
          MeterAmount = 0;
        }
        else
        {
          // If The amount is minor than minimum, change to minimum
          if (MinimumAmount > MeterAmount)
          {
            this.ToCompensateAmount += (MinimumAmount - MeterAmount);
            MeterAmount = MinimumAmount + ContributionAmount;
          }
          // If The amount is greater than maximum, change to maximum
          else if (MaximumAmount < MeterAmount + ContributionAmount)
          {
            _diff = MaximumAmount - MeterAmount;
            MeterAmount += _diff;
            this.ToCompensateAmount -= (ContributionAmount - _diff);
          }
          else
          {
            MeterAmount += ContributionAmount;

            // Maybe after award or reset
            if (MeterAmount < MinimumAmount && IsAmountOperation)
            {
              _diff = MinimumAmount - MeterAmount;
              MeterAmount = MinimumAmount;
              this.ToCompensateAmount += _diff;
            }
          }
        }

        RecalculateAmount = MeterAmount;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContributionMeters.RecalculateAmount -> JackpotId: {0}", 0));
        Log.Exception(_ex);
      }

      return false;
    } // RecalculateAmount

    /// <summary>
    /// Get Meters by ID.
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetMeterById(SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_MeterQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJackpotId", SqlDbType.Int).Value = JackpotId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              if (!MeterDataMapper(_sql_reader, SqlTrx))
              {
                return false;
              }
            }
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JackpotContributionMeters.DB_GetMeterById -> JackpotId: {0}", JackpotId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetMeterById

    /// <summary>
    /// DB_MeterQuery_Select
    /// </summary>
    /// <returns></returns>
    private String DB_MeterQuery_Select()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   JCM_TYPE                     ");
      _sb.AppendLine("       , JCM_AMOUNT                   ");
      _sb.AppendLine("  FROM   JACKPOTS_CONTRIBUTION_METERS ");
      _sb.AppendLine(" WHERE   JCM_JACKPOT_ID = @pJackpotId ");

      return _sb.ToString();
    } // DB_MeterQuery_Select 

    #endregion " Private Methods "

    #region " Internal Methods "

    internal JackpotContributionMeters Clone()
    {

      return new JackpotContributionMeters()
      {
        Jackpot = this.Jackpot,
        JackpotId = this.JackpotId,
        MeterAmount = this.MeterAmount,
        HappyHourAmount = this.HappyHourAmount,
        SharedAmount = this.SharedAmount,
        MainAmount = this.MainAmount,
        HiddenAmount = this.MainAmount
      };
    } // Clone

    #endregion " Internal Methods "

  }
}
