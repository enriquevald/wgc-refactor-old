﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotAmountTerminal.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Jackpots amount terminals
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 28-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2017 AMF    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Jackpot
{
  public class JackpotAmountTerminal
  {
    #region " Properties "

    public Int32 TerminalId { get; set; }
    public Decimal Amount { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Read the registers witn amount
    /// </summary>
    /// <param name="AmountTerminals"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_ReadAmountTerminals(out DataTable AmountTerminals, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      AmountTerminals = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   JAT_TERMINAL_ID           ");
        _sb.AppendLine("       , JAT_AMOUNT                ");
        _sb.AppendLine("  FROM   JACKPOTS_AMOUNT_TERMINALS ");
        _sb.AppendLine(" WHERE   JAT_AMOUNT > 0            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(AmountTerminals);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } // DB_ReadAmountTerminals

    /// <summary>
    /// Subtract the amount terminal
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean Subtract(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   JACKPOTS_AMOUNT_TERMINALS          ");
        _sb.AppendLine("    SET   JAT_AMOUNT = JAT_AMOUNT - @pAmount ");
        _sb.AppendLine("  WHERE   JAT_TERMINAL_ID = @pTerminalId     ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = this.TerminalId;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = this.Amount;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("Error: JackpotAmountTerminal.Save");

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Message("Error: JackpotAmountTerminal.Save");
        Log.Exception(_ex);
      }

      return false;
    } // Subtract

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Save the amount terminal
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean Save(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS (SELECT   1                               ");
        _sb.AppendLine("              FROM   JACKPOTS_AMOUNT_TERMINALS       ");
        _sb.AppendLine("             WHERE   JAT_TERMINAL_ID = @pTerminalId) ");
        _sb.AppendLine(" BEGIN                                               ");
        _sb.AppendLine("   UPDATE   JACKPOTS_AMOUNT_TERMINALS                ");
        _sb.AppendLine("      SET   JAT_AMOUNT = JAT_AMOUNT + @pAmount       ");
        _sb.AppendLine("    WHERE   JAT_TERMINAL_ID = @pTerminalId           ");
        _sb.AppendLine(" END                                                 ");
        _sb.AppendLine(" ELSE                                                ");
        _sb.AppendLine(" BEGIN                                               ");
        _sb.AppendLine("   INSERT INTO   JACKPOTS_AMOUNT_TERMINALS           ");
        _sb.AppendLine("               ( JAT_TERMINAL_ID                     ");
        _sb.AppendLine("               , JAT_AMOUNT)                         ");
        _sb.AppendLine("        VALUES ( @pTerminalId                        ");
        _sb.AppendLine("               , @pAmount)                           ");
        _sb.AppendLine(" END                                                 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = this.TerminalId;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = this.Amount;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("Error: JackpotAmountTerminal.Save");

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Message("Error: JackpotAmountTerminal.Save");
        Log.Exception(_ex);
      }

      return false;
    } // Save

    #endregion " Private Methods "
  }
}
