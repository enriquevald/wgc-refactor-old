﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotContributionBreakdown.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: ENRIC TOMAS
// 
// CREATION DATE: 13-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JUN-2017 ETP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;

namespace WSI.Common.Jackpot
{

  public class JackpotNotificationEGM
  {

  #region " Public methods "
   
   /// <summary>
    /// Notification, Handpay and Bonus to EGM
   /// </summary>
   /// <param name="JackpotInfo"></param>
   /// <param name="InfoPlaySession"></param>
   /// <param name="JackpotDetailIdx"></param>
   /// <param name="Prize"></param>
   /// <param name="DetailsSharedId"></param>
   /// <param name="SqlTrx"></param>
   /// <returns></returns>
    public static Boolean NotificationAndHandpay(Jackpot JackpotInfo, InfoPlaySession InfoPlaySession, Int64 JackpotDetailIdx, Decimal Prize, Int64 DetailsSharedId, SqlTransaction SqlTrx)
    {
      try
      {
        DateTime _handpay_date;
        SiteJackpotAwarded _wcp_command;
        CardData _card_data;
        Terminal.TerminalInfo _terminal_info;
        Handpays.HandpayRegisterOrCancelParameters _input_handpay;
        Int32 _db_terminal_id;
        String _db_provider_id;
        String _db_terminal_name;


        if (Prize == 0)
        {
          return true;
        }

        if (JackpotInfo.PayoutMode == Jackpot.PayoutModeType.Manual || JackpotInfo.PayoutMode == Jackpot.PayoutModeType.Automatic)
        {

          if (!Terminal.Trx_GetTerminalInfo(InfoPlaySession.TerminalId, out _terminal_info, SqlTrx))
          {
            _terminal_info.Name = "";
          }

          if (!DB_ReadSiteTerminal(SqlTrx, out _db_terminal_id, out _db_provider_id, out _db_terminal_name))
          {
            return false;
          }

          if (!DB_InsertHandpay(JackpotInfo, _db_terminal_id, _db_provider_id, _db_terminal_name, Prize,
                                InfoPlaySession.TerminalId, InfoPlaySession.AccountId, 0, SqlTrx, out _handpay_date))
          {
            return false;
          }

          if (JackpotInfo.PayoutMode == Jackpot.PayoutModeType.Automatic)
          {
            _input_handpay = new Handpays.HandpayRegisterOrCancelParameters();
            _input_handpay.TerminalId = _db_terminal_id;
            _input_handpay.TerminalProvider = _db_provider_id;
            _input_handpay.TerminalName = _db_terminal_name;
            _input_handpay.HandpayType = HANDPAY_TYPE.SITE_JACKPOT;
            _input_handpay.HandpayAmount = Prize;
            _input_handpay.HandpayAmt0 = Prize;
            _input_handpay.HandpayCur0 = CurrencyExchange.GetNationalCurrency();
            _input_handpay.HandpayAmount = Prize;
            _input_handpay.HandpayDate = _handpay_date;
            _input_handpay.CancelHandpay = false;
            _input_handpay.SiteJackpotAwardedOnTerminalId = InfoPlaySession.TerminalId;

            if (!WSI.Common.Handpays.AutomaticallyPayHandPay(InfoPlaySession.AccountId, _input_handpay, SqlTrx))
            {
              return false;
            }
          }

          _wcp_command = new WSI.Common.SiteJackpotAwarded();

          _wcp_command.JackpotId = 0;
          _wcp_command.JackpotDatetime = _handpay_date;
          _wcp_command.JackpotLevelId = JackpotInfo.Id;
          _wcp_command.JackpotLevelName = JackpotInfo.Name;
          _wcp_command.JackpotAmountCents = (Int64)(Prize * 100);

          _wcp_command.JackpotMachineName = "";

          _wcp_command.JackpotMachineName = _terminal_info.Name;

          _card_data = new CardData();
          if (CardData.DB_CardGetAllData(InfoPlaySession.AccountId, _card_data, SqlTrx))
          {
            _wcp_command.WinnerTrackData = _card_data.TrackData;
            _wcp_command.WinnerHolderName = _card_data.PlayerTracking.HolderName3;

            _wcp_command.ParametersShowOnlyToWinner = 0;
            _wcp_command.ParametersWinnerShowTimeSeconds = GeneralParam.GetInt32("SasHost", "SiteJackpotWinnerShowTimeSeconds", 60);
            _wcp_command.ParametersNoWinnerShowTimeSeconds = GeneralParam.GetInt32("SasHost", "SiteJackpotNoWinnerShowTimeSeconds", 60);

            WSI.Common.WcpCommands.InsertWcpCommand(InfoPlaySession.TerminalId, WSI.Common.WCP_CommandCode.SiteJackpotAwarded, _wcp_command.ToXml(), SqlTrx);
          }

        } //if ( PayoutMode == Manual Or Automatic )

        if (JackpotInfo.PayoutMode == Jackpot.PayoutModeType.EGMBonusing)
        {
          MultiPromos.AccountBalance _to_transfer;
          BonusAwardInfo _info;

          _to_transfer = new MultiPromos.AccountBalance();
          _info = new BonusAwardInfo();

          _to_transfer.Redeemable = Prize;
          _to_transfer.PromoRedeemable = (Decimal)0;
          _to_transfer.PromoNotRedeemable = (Decimal)0;

          _info.SourceType = (Int32)BonusSource.SiteJackpot;
          _info.TargetTerminalId = InfoPlaySession.TerminalId;
          _info.TargetType = 1;
          _info.SourceBigInt1 = 0;
          _info.SourceBigInt2 = 0;
          _info.SourceInt1 = 0;
          _info.SourceInt2 = 0;
          _info.TargetAccountId = InfoPlaySession.AccountId;


          if (!Bonusing.JackpotDetailInsertToBonus(JackpotDetailIdx, JackpotInfo.Id, _to_transfer, _info, DetailsSharedId, SqlTrx))
          {
            return false;
          }

        } //if ( PayoutMode == EGMBonusing )

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("JackpotAwardEvents.NotificationAndHandpay");
        Log.Exception(_ex);
      }

      return false;

    } // NotificationAndHandpay




    #endregion " Public methods "

  #region " Private methods "

    /// <summary>
    /// DB_Insert Handpay
    /// </summary>
    /// <param name="JackpotInfo"></param>
    /// <param name="TerminalId"></param>
    /// <param name="ProviderId"></param>
    /// <param name="TerminalName"></param>
    /// <param name="JackpotPrize"></param>
    /// <param name="AwardedOnTerminalId"></param>
    /// <param name="AwardedToAccountId"></param>
    /// <param name="MovementId"></param>
    /// <param name="Trx"></param>
    /// <param name="HandpayDate"></param>
    /// <returns></returns>
    private static Boolean DB_InsertHandpay(Jackpot JackpotInfo, Int32 TerminalId, String ProviderId, String TerminalName,
      Decimal JackpotPrize, Int32 AwardedOnTerminalId, Int64 AwardedToAccountId, Int64 MovementId,
                                     SqlTransaction Trx, out DateTime HandpayDate)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sb;

      HandpayDate = WGDB.Now;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("INSERT INTO HANDPAYS ( HP_TERMINAL_ID                         ");
        _sb.AppendLine("                     , HP_DATETIME                            ");
        _sb.AppendLine("                     , HP_AMOUNT                              ");
        _sb.AppendLine("                     , HP_TE_NAME                             ");
        _sb.AppendLine("                     , HP_TE_PROVIDER_ID                      ");
        _sb.AppendLine("                     , HP_TYPE                                ");
        _sb.AppendLine("                     , HP_SITE_JACKPOT_INDEX                  ");
        _sb.AppendLine("                     , HP_SITE_JACKPOT_NAME                   ");
        _sb.AppendLine("                     , HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ");
        _sb.AppendLine("                     , HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID  ");
        _sb.AppendLine("                     , HP_MOVEMENT_ID                         ");
        _sb.AppendLine("                     , HP_STATUS                              ");
        _sb.AppendLine("                     , HP_PAYMENT_MODE                        ");
        _sb.AppendLine("                     , HP_AMT0                                ");
        _sb.AppendLine("                     , HP_CUR0                                ");
        _sb.AppendLine("                     )                                        ");
        _sb.AppendLine("             VALUES  ( @TerminalId                            ");
        _sb.AppendLine("                     , @Date                                  ");
        _sb.AppendLine("                     , @Amount                                ");
        _sb.AppendLine("                     , @TermName                              ");
        _sb.AppendLine("                     , @ProviderId                            ");
        _sb.AppendLine("                     , @HandpayType                           ");
        _sb.AppendLine("                     , @JackpotIdx                            ");
        _sb.AppendLine("                     , @JackpotName                           ");
        _sb.AppendLine("                     , @AwardedOnTerminalId                   ");
        _sb.AppendLine("                     , @AwardedToAccountId                    ");
        _sb.AppendLine("                     , @MovementId                            ");
        _sb.AppendLine("                     , @HpStatus                              ");
        _sb.AppendLine("                     , @HpPaymentMode                         ");
        _sb.AppendLine("                     , @Amt0                                  ");
        _sb.AppendLine("                     , @cur0                                  ");
        _sb.AppendLine("                    )                                         ");


        _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);

        _sql_cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
        _sql_cmd.Parameters.Add("@ProviderId", SqlDbType.NVarChar).Value = ProviderId;
        _sql_cmd.Parameters.Add("@TermName", SqlDbType.NVarChar).Value = TerminalName;
        _sql_cmd.Parameters.Add("@Amount", SqlDbType.Money).Value = JackpotPrize;
        _sql_cmd.Parameters.Add("@HandpayType", SqlDbType.Int).Value = ((Int32)HANDPAY_TYPE.SITE_JACKPOT);
        _sql_cmd.Parameters.Add("@JackpotIdx", SqlDbType.Int).Value = JackpotInfo.Id + 1000;
        _sql_cmd.Parameters.Add("@JackpotName", SqlDbType.NVarChar).Value = JackpotInfo.Name;
        _sql_cmd.Parameters.Add("@AwardedOnTerminalId", SqlDbType.Int).Value = AwardedOnTerminalId;
        _sql_cmd.Parameters.Add("@AwardedToAccountId", SqlDbType.BigInt).Value = AwardedToAccountId;
        _sql_cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = HandpayDate;
        _sql_cmd.Parameters.Add("@HpStatus", SqlDbType.Int).Value = MovementId == 0 ? HANDPAY_STATUS.PENDING : HANDPAY_STATUS.PAID;
        _sql_cmd.Parameters.Add("@HpPaymentMode", SqlDbType.Int).Value = ((Int32)Handpays.SiteJackpotPaymentMode);
        _sql_cmd.Parameters.Add("@MovementId", SqlDbType.BigInt).Value = MovementId == 0 ? DBNull.Value : (Object)MovementId;

        _sql_cmd.Parameters.Add("Amt0", SqlDbType.Money).Value = JackpotPrize;
        _sql_cmd.Parameters.Add("Cur0", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency();

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error("JackpotAwardEvents.JackpotAward Insert Handpay Failed!");

          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // DB_InsertHandpay



    /// <summary>
    /// DB_ReadSiteTerminal
    /// </summary>
    /// <param name="Trx"></param>
    /// <param name="TerminalId"></param>
    /// <param name="ProviderId"></param>
    /// <param name="TerminalName"></param>
    /// <returns></returns>
    private static Boolean DB_ReadSiteTerminal(SqlTransaction Trx, out Int32 TerminalId, out String ProviderId, out String TerminalName)
    {
      SqlCommand _sql_cmd;
      SqlDataReader _dr;
      StringBuilder _sb;

      _dr = null;
      TerminalId = 0;
      ProviderId = "";
      TerminalName = "";

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   TE_TERMINAL_ID                                                ");
        _sb.AppendLine("       , TE_PROVIDER_ID                                                ");
        _sb.AppendLine("       , TE_NAME                                                       ");
        _sb.AppendLine("  FROM   TERMINALS                                                     ");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = (SELECT   MIN(TE_TERMINAL_ID)                ");
        _sb.AppendLine("                             FROM   TERMINALS                          ");
        _sb.AppendLine("                            WHERE   TE_TERMINAL_TYPE = @pTerminalType) ");

        _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);
        _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.SmallInt).Value = TerminalTypes.SITE;
        _dr = _sql_cmd.ExecuteReader();

        if (!_dr.Read())
        {
          Log.Error("JackpotAwardEvents.NotificationAndHandpay (Read Terminals(Site) Failed!)");

          return false;
        }

        TerminalId = _dr.GetInt32(_dr.GetOrdinal("TE_TERMINAL_ID"));
        ProviderId = _dr.GetString(_dr.GetOrdinal("TE_PROVIDER_ID"));
        TerminalName = _dr.GetString(_dr.GetOrdinal("TE_NAME"));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_dr != null)
        {
          _dr.Close();
          _dr.Dispose();
          _dr = null;
        }
      }
    } // DB_ReadTerminals




  }

  #endregion " Private methods "

}
