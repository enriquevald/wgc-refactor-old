﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotMeter.cs
// 
//   DESCRIPTION: Jackpots meters
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 21-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-JUN-2017 AMF    First release.
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;

namespace WSI.Common.Jackpot
{
  public class JackpotMeters
  {
    #region " Enums "

    public enum ReportType
    {
      Daily,
      Weekly,
      Monthly,
      Hour
    } // ReportType

    #endregion " Enums "

    #region " Constructor "

    /// <summary>
    /// Class constructor
    /// </summary>
    /// 
    public JackpotMeters()
    {
    } // JackpotMeters

    #endregion " Constructor "

    #region " Public methods "

    /// <summary>
    /// Fill Main amount meter into jackpots_meters
    /// </summary>
    /// <returns></returns>
    public static Boolean FillMeters()
    {
      if (!DB_FillMeters())
      {
        return false;
      }

      return true;
    } // FillMeters

    /// <summary>
    /// Get jackpots meters
    /// </summary>
    /// <param name="GetReportType"></param>
    /// <param name="InitHour"></param>
    /// <param name="FilterDate"></param>
    /// <param name="FilterJackpots"></param>
    /// <returns></returns>
    public static String GetMeters(ReportType GetReportType, Int32 InitHour, String FilterDate, String FilterJackpots)
    {
      String _str_date;
      String _str_date_time;
      StringBuilder _sb_select;
      StringBuilder _sb_group;

      _str_date = SqlStatistics.GUI_FormatDateDB(new DateTime(2007, 1, 1));
      _str_date_time = SqlStatistics.GUI_FormatDateDB(new DateTime(2007, 1, 1, InitHour, 0, 0));
      _sb_select = new StringBuilder();
      _sb_group = new StringBuilder();

      _sb_select.AppendLine("     SELECT   ");

      switch (GetReportType)
      {
        case ReportType.Daily:
          _sb_select.AppendLine(String.Format("              DATEADD(DAY, DATEDIFF(HOUR, {0}, JM_DATE_TIME) / 24, {1}) AS SINCE ", _str_date, _str_date));

          _sb_group.AppendLine(String.Format("   GROUP BY   JS_JACKPOT_ID, JS_NAME, JS_ISO_CODE, DATEDIFF(HOUR, {0}, JM_DATE_TIME) / 24 ", _str_date));
          break;

        case ReportType.Weekly:
          _sb_select.AppendLine(String.Format("              DATEADD(DAY, DATEDIFF(HOUR, {0}, JM_DATE_TIME) / 24 / 7 * 7, {1}) AS SINCE ", _str_date, _str_date));

          _sb_group.AppendLine(String.Format("   GROUP BY   JS_JACKPOT_ID, JS_NAME, JS_ISO_CODE, DATEDIFF(HOUR, {0}, JM_DATE_TIME) / 24 / 7 ", _str_date));
          break;

        case ReportType.Monthly:
          _sb_select.AppendLine(String.Format("              DATEADD(MONTH, DATEDIFF(MONTH, {0}, DATEADD(HOUR, -{1}, JM_DATE_TIME)), {2}) AS SINCE ", _str_date, InitHour, _str_date));

          _sb_group.AppendLine(String.Format("   GROUP BY   JS_JACKPOT_ID, JS_NAME, JS_ISO_CODE, DATEDIFF(MONTH, {0}, DATEADD(HOUR, -{1}, JM_DATE_TIME)) ", _str_date, InitHour));
          break;

        case ReportType.Hour:
          _sb_select.AppendLine(String.Format("              DATEADD(HOUR, DATEDIFF(HOUR, {0}, JM_DATE_TIME), {1}) AS SINCE ", _str_date_time, _str_date_time));

          _sb_group.AppendLine(String.Format("   GROUP BY   JS_JACKPOT_ID, JS_NAME, JS_ISO_CODE, DATEDIFF(HOUR, {0}, JM_DATE_TIME) ", _str_date_time));
          break;
      }

      _sb_select.AppendLine("            , JS_NAME              AS NAME                ");
      _sb_select.AppendLine("            , ROUND(MIN(JM_METER_AMOUNT),2,1) AS MINIMUM  ");
      _sb_select.AppendLine("            , ROUND(MAX(JM_METER_AMOUNT),2,1) AS MAXIMUM  ");
      _sb_select.AppendLine("            , ROUND(AVG(JM_METER_AMOUNT),2,1) AS AVERAGE  ");
      _sb_select.AppendLine("            , JS_ISO_CODE          AS ISO_CODE            ");
      _sb_select.AppendLine("       FROM   JACKPOTS_METERS                             ");
      _sb_select.AppendLine(" INNER JOIN   JACKPOTS_SETTINGS                           ");
      _sb_select.AppendLine("         ON   JM_JACKPOT_ID = JS_JACKPOT_ID               ");
      _sb_select.AppendLine("      WHERE   1 = 1                                       ");

      if (!String.IsNullOrEmpty(FilterDate))
      {
        _sb_select.AppendLine(String.Format("        AND   {0} ", FilterDate));
      }

      if (!String.IsNullOrEmpty(FilterJackpots))
      {
        _sb_select.AppendLine(String.Format("        AND   JM_JACKPOT_ID IN ({0}) ", FilterJackpots));
      }

      _sb_select.Append(_sb_group.ToString());

      _sb_select.AppendLine("   ORDER BY   SINCE ASC ");

      return _sb_select.ToString();
    } // GetMeters

    #endregion " Public methods "

    #region " Private methods "

    /// <summary>
    /// Fill meters
    /// </summary>
    /// <returns></returns>
    private static Boolean DB_FillMeters()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   JACKPOTS_METERS                ");
        _sb.AppendLine("             ( JM_JACKPOT_ID                  ");
        _sb.AppendLine("             , JM_DATE_TIME                   ");
        _sb.AppendLine("             , JM_METER_AMOUNT                ");
        _sb.AppendLine("             )                                ");
        _sb.AppendLine("      SELECT   JCM_JACKPOT_ID                 ");
        _sb.AppendLine("             , @pDateTime                     ");
        _sb.AppendLine("             , JCM_AMOUNT                     ");
        _sb.AppendLine("        FROM   JACKPOTS_CONTRIBUTION_METERS   ");
        _sb.AppendLine("  INNER JOIN   JACKPOTS_SETTINGS              ");
        _sb.AppendLine("          ON   JCM_JACKPOT_ID = JS_JACKPOT_ID ");
        _sb.AppendLine("         AND   JCM_TYPE       = @pJackpotType ");
        _sb.AppendLine("         AND   JS_ENABLED     = @pEnabled     ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = WGDB.Now;
            _sql_cmd.Parameters.Add("@pJackpotType", SqlDbType.Int).Value = Jackpot.JackpotType.Main;
            _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = true;

            _sql_cmd.ExecuteNonQuery();
          }

          _db_trx.Commit();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("JackpotMeters.DB_FillMeters() - An error occurred inserting.");
        Log.Exception(_ex);
      }

      return false;
    } // DB_FillMeters

    #endregion " Private methods "
  }
}
