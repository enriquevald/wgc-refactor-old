//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CardData.cs
// 
//   DESCRIPTION: CardData class
// 
//        AUTHOR: Susana Sams�
// 
// CREATION DATE: 24-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-MAY-2012 SSC    First version.
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 18-JUN-2012 JAB    Added new fields (CurrentRedeemableBalance, CurrentNotRedeemableBalance).
// 20-JUN-2012 JAB    Added new function (CalculatePromotionsAmount).
// 21-JUN-2012 JAB    Added new function (NotRedeemableParts).
// 22-JUN-2012 RCI    Added new field (PlaySession.CashIn).
// 26-JUL-2012 RCI    Added the possibility of Cancel All Promotions when partial or total redeem: GP Cashier.XXX_Redeem.
// 09-AUG-2012 DDM    Added new function (FormatFullName) 
// 01-OCT-2012 QMP    Added new function (CheckDuplicateAccounts)
// 05-DEC-2012 RCI    Support for multiple cancellation
// 06-FEB-2013 QMP    Added new CardData class property PointsStatus
// 06-FEB-2013 LEM    Added new functions DB_UpdateDataCard and DB_UpdatePinCard
// 12-MAR-2013 MPO    _is_multisite condition in DB_CardGetAllData, DB_UpdateDataCard and DB_UpdatePinCard
// 14-MAR-2013 RCI    Use of PK in the query for the multiple cancellation of B part.
// 27-MAR-2013 LEM    Added new IsValidBirthdate and IsValidWeddingdate functions
//                    Brought function CheckIdAlreadyExists and class ControlsRequired from WSI.Cashier.frm_player_edit
//                    Create new ENUM_CARDDATA_FIELD
// 28-MAR-2013 LEM    Modified DB_CardGetAllData, DB_UpdateDataCard to add new field Twitter Account
// 08-APR-2013 QMP    Split GetPromotionsToBeCancelledDueToRedeem SQL Query in two to use the appropriate indexes.
// 10-APR-2013 QMP & RCI   Prize Coupon promotions are not inserted into ACCOUNT_PROMOTIONS table but must work as before.
// 26-APR-2013 LEM    Fixed Bug #753: Account updating don't update HolderId1 and HolderId2 when RFC or CURP are selected
// 07-MAY-2013 JBP    Added BlockDescription to CardData
//                    Modified DB_CardGetAllData (), DB_UpdateDataCard () to add new field AC_BLOCK_DESCRIPTION
// 09-MAY-2013 JBP    Rename DB_UpdateDataCard to DB_BlockUnblockCard () and some changes.
// 17-MAY-2013 MPO    Added to import column ID Site (ac_ms_created_on_site_id)
// 28-MAY-2013 NMR    Fixed incorrect property asignation
// 13-JUN-2013 ACM    Added three new documents types and new fields (holderOccupation, Beneficiary: name, id, id_type)
// 17-JUN-2013 DHA    Added General Param to disable cancellations
// 19-JUN-2013 NMR    Fixed bug #866: Prevent access to HANDPAYS table when is MultiSite
// 19-JUN-2013 NMR    Fixed bug #867: Prevent access to ACCOUNT_PROMOTIONS table when is MultiSite
// 05-JUL-2013 LEM    Added functions for managment of CardData info related to Countries, Nationalities and Federal States Entities
// 18-JUL-2013 ACM    Modified function LoadAccountDoccumets().Changed the PlayerTracking property: HolderHasBeneficiary.
// 24-JUL-2013 LEM    Modified function CheckWinner() to support notification and automatic payment of Site Jackpots.
// 14-AUG-2013 RCI    Fixed bug WIG-116: Take in account GPs Cashier.XXXRedeem - CancelAllPromotions
// 20-AUG-2013 RMS    Fixed Bug WIG-109: Created PreferredHolderId and PreferredHolderIdType to control old and updated accounts HolderId
//                                       in PlayerTrackingData.
// 28-AUG-2013 JAB    Added new camps in Customized's Voucher.
// 04-OCT-2013 DDM & DHA    Fixed Bug #WIG-262: Error cancelling UNR promotions.
// 07-OCT-2013 RCI   Fixed Bug #WIG-266: Can't remove NR2 Promotion
// 17-OCT-2013 RMS   Optimized DB_CardGetAllData removing JOIN to get the secuence for Split2 and removed unnecessary property of CancellableOperation.
// 22-OCT-2013 ACM   Language of Countries and Nationalities changed to "es".
// 25-OCT-2013 ACM   Fixed Bug #WIG-313: Ticket does't print nationality and birthplace.
// 30-OCT-2013 LEM   Deleted CommonInsertCardMovement and Ms_CommonInsertCardMovement.
// 31-OCT-2013 JAB   To set initial value "PlayerTracking.HolderBirthDateText". Now "HolderBirthText" is shown when customized ticket are printed.
// 15-NOV-2013 RCI   Fixed Bug WIG-408: An operation (recharge) is cancellable if it's not UNDONE.
// 18-NOV-2013 FBA   Fixed Bug WIG-413: Ocupations must be ordered alphabetically
// 21-NOV-2013 AMF   New comment when close session.
// 02-DEC-2013 DHA   AccountId and TrackData is hidden in voucher for virtual accounts.
// 08-JAN-2014 LJM   Refactor to make the uc_card form filling go faster.
// 15-JAN-2014 AMF   Fixed Bug WIGOSTITO-981
// 17-JAN-2014 JML   Fixed Bug WIG-521
// 03-FEB-2014 LJM   Added function DB_CardGetAccountId()
// 03-FEB-2014 RCI   CashInTax: Support to cancel recharges (but the CashIn Tax is not returned)
// 06-FEB-2014 RCI   Fixed Bug WIG-600: Can't create new accounts.
// 06-FEB-2014 RCI   Fixed Bug WIG-591: Can't access MB accounts from player accounts screen.
// 12-FEB-2014 FBA   Added class IdentificationTypes
// 18-FEB-2014 FBA   Fixed Bug WIG-641
// 21-FEB-2014 JBC   Fixed Bug WIG-657: Identification types control
// 05-MAY-2014 RMS   Fixed Bug WIG-842: Personalizaci�n de Cuentas: Al intentar personalizar una cuenta el formulario aparece en blanco. 
// 30-MAY-2014 RMS   Fixed Bug WIG-964: Cuentas: No se esta controlando el identificador de sala 
// 01-JUL-2014 AMF   Personal Card Replacement
// 11-JUL-2014 XIT   Added Multicountry support
// 14-JUL-2014 AMF   Regionalization
// 12-AUG-2014 RCI   Fixed Bug WIG-1178: Regionalization support
// 25-AUG-2014 JBC   Fixed Bug WIG-1207: Second name always empty
// 25-SEP-2014 OPC   Fixed Bug WIG-1275: Only show the enabled occupations (and also the one that has the player)
// 28-SEP-2014 RCI   Fixed Bug WIG-1324: Unexpected excepction in personalization when occupation is not set
// 28-SEP-2014 RCI   Fixed Bug WIG-1326: Can't create personalized accounts
// 01-OCT-2014 SGB   Use a SqlCommand in DB_LinkCard
// 08-OCT-2014 DHA   Added property to accumulate if a recharge is going to refund without plays
// 16-OCT-2014 MPO   Fixed WIG-1517, incorrect format name.
// 22-OCT-2014 SMN   Added BlockReason to DB_CardGetPersonalData function
// 28-OCT-2014 SMN     Added new functionality: BLOCK_REASON field can have more than one block reason.
// 29-JAN-2015 DHA   Fixed Bug WIG-1958: when cash desk draw is enabled by play session, cancellation operations are disabled
// 12-FEB-2015 YNM   Fixed Bug WIG-1087: When unblocking card, the unlocking reason was never show
// 26-FEB-2015 DRV   Fixed Bug WIG-2116: Voucher error on chips purchase with cash out
// 09-MAR-2015 DRV   Fixed Bug WIG-2140: Card is redeemed buying chips
// 18-MAR-2015 SGB   Change text anonymous client in voucher by general param
// 04-MAY-2015 YNM    Fixed Bug TFS-1160: Add birth Country and nationality on account import
// 03-AUG-2015 ETP   Fixed BUG 3496 Use of common header for card replacement.
// 12-AGO-2015 SGB   Backlog Item 3296: Add address country in account
// 25-AGO-2015 SGB   Backlog Item 3297: Check ZIP and autoselection in combobox
// 22-SEP-2015 ETP   Fixed BUG 4576: New validation for documentTypes.
// 30-SEP-2015 AMF   Fixed Bug TFS-4591: Postal Code, change adress order
// 05-OCT-2015 AMF   Fixed Bug TFS-4922: Postal Code, Federal entity depends country
// 06-OCT-2015 SGB   Fixed Bug TFS-4533: Increment caracters in HolderAddress02
// 04-NOV-2015 AVZ   Backlog Item 6145:BonoPLUS: Reserved credit - Task 6153: BonoPLUS Reserved - Added reserved into account. (Reserved)
// 11-NOV-2015 FOS   Product Backlog Item: 3709 Payment tickets & handpays
// 17-NOV-2015 DLL   Backlog Item 6505: Apply tax to all withdrawal. Without Devolution.
// 23-NOV-2015 FOS   Bug 6771:Payment tickets & handpays
// 26-NOV-2015 AMF   Backlog Item 7079: BonoPLAY.
// 09-DEC-2015 DDS   Backlog Item 7423: Detect card is possibly used as automatic cashier (Code is commented)
// 13-JAN-2015 JML   Product Backlog Item 6559: Cashier draw (RE in kind)
// 19-JAN-2016 JRC   Product Backlog Item 7909: Multiple buckets. Waking up the thread
// 06-JAN-2016 SGB   Backlog Item 7910: Change column AC_POINTS to bucket 1.
// 02-FEB-2016 AMF   Bug 8913: Reserved & Buckets in multisite
// 11-FEB-2015 AVZ    Product Backlog Item 9046:Visitas / Recepci�n: BlackLists
// 17-NOV-2015 DLL   Backlog Item 6505: Apply tax to all withdrawal. Without Devolution.
// 19-FEB-2016 RAB   Product Backlog Item 9740:Cajero: impresi�n del n�mero de c�dula del cliente en los vouchers de sorteos
// 28-APR-2016 EOR   Product Backlog Item 11296: Codere - Custody Tax: Implement Devolution Tax Custody
// 21-APR-2016 ETP   Fixed bug 11743: Mostrar buckets de credito redimible & no redimible en el cajero.
// 23-MAR-2016 GDA   Product Backlog Item 9445:Promo RE impuestos: Anular Operaci�n se modifica GetPromotionsToBeCancelledDueToRedeem para que no incluya promos RE son impuestos
// 17-JUN-2016 DLL   Fixed Bug 14671:REF.385 Cuentas: Error en la direcci�n de la cuenta cuando el Pa�s est� en blanco
// 31-OCT-2016 JML   Fixed Bug 19865: Gamming tables stock control - not displayed in the cashier
// 31-OCT-2016 JML   Fixed Bug 19866: Chips shange control - not displayed in the cashier
// 10-ENE-2017 DPC   Bug 21943:Recepci�n: Permite guardar Accounts con el mismo HolderId desde la opci�n "GUARDAR PIN"
// 12-ENE-2017 DPC   Bug 22421:Auditor�a: mensajes err�neos al imprimir datos del cliente
// 17-MAR-2017 ETP   WIGOS-110: Creditlines - Get Account Credit Line data.
// 21-MAR-2017 FAV   PBI 25735:Third TAX - Movement reports
// 20-APR-2017 JML   Fixed Bug 26807: Custody - Incorrect custody amount to be refunded
// 17-MAR-2017 ETP   WIGOS-110: Creditlines - Get Account Credit Line data.
// 23-MAY-2017 FAV   Bug 27543:Problemas de pagos manuales
// 24-MAY-2017 RGR   Fixed Bug 24542: Audit: Misinformed records when creating a new account
// 06-JUL-2017 JML   PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 26-JUL-2017 EOR   Bug 28994: WIGOS-3955 Reopen Cash - cashier error is registered when the cash session is reopened out of gaming day
// 22-DEC-2017 RAB   Bug 31145:WIGOS-7261 Cashier - Prize payment operation doesn't work. Warning mesage "No es posible realizar la operaci�n" is displayed
// 01-JAN-2018 DHA   Bug 31392:WIGOS-7261 Cashier - Prize payment operation doesn't work. Warning mesage "No es posible realizar la operaci�n" is displayed.
// 01-JAN-2018 DHA   Bug 31393:WIGOS-7793 Service charge: wrong calculation with Terminal draw
// 19-FEB-2018 EOR   Bug 31586: WIGOS-7074 [Ticket #11015] Release 03.005.0079 - Custodia - Cancelacion - Mensaje error 
// 09-MAR-2018 EOR   Bug 31854: WIGOS-8110 Wrong amount in a withdrawal having terminal and games draws enabled 
// 09-MAR-2018 EOR   Bug 31891: WIGOS-8251 Cashless error: Error transfering credit, from a card to another 
// 12-JUN-2018 DPC   Bug 33001:[WIGOS-12663]: Account Last gaming sessions doesn't show the bill in
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Drawing;
using WSI.Common.CreditLines;

namespace WSI.Common
{
  #region "Enums"
  public enum CreditRedeemSourceOperationType
  {
    DEFAULT,
    CHIPS_PURCHASE_WITH_CASHOUT,
    PAY_HANDPAY,
  };

  public enum COUNTRY_ZIP_CODE_SETTINGS
  {
    COUNTRY_LIST = 1
  };

  public enum ADDRESS_VALIDATION
  {
    MANUAL_ENTRY = 0,
    VALIDATED = 1,
    VALIDATION_SKIPPED = 2
  };
  //public enum SUSPICIOUS_CARD_USE
  //{
  //  DB_ERROR = 0,
  //  SUSPICIOUS = 1,
  //  NO_SUSPICIOUS = 2
  //}
  #endregion

  public class CardSettings
  {
    public Int32 PersonalCardReplacementPriceInPoints;
    public Int32 PersonalCardReplacementChargeAfterTimes;
    public Currency AnonymCardPrice;
    public Currency PersonalCardPrice;
    public Currency PersonalCardReplacementPrice;
    public Int32 AnonymCardRefundable;
  }

  public static class Occupations
  {
    private static DataTable m_occupations = null;
    private static int m_tick_read = 0;

    internal static DataTable CloneTable()
    {
      LoadOccupations();
      if (m_occupations != null)
      {
        return m_occupations.Copy();
      }
      return new DataTable();
    }

    private static void LoadOccupations()
    {
      try
      {
        if (m_occupations != null)
        {
          if (Misc.GetElapsedTicks(m_tick_read) < (5 * 60 * 1000))
          {
            return;
          }
        }

        using (DB_TRX _trx = new DB_TRX())
        {
          DataTable _table;
          StringBuilder _sb;

          _table = new DataTable();

          _sb = new StringBuilder();
          _sb.AppendLine("   SELECT   OC_ID                         ");
          _sb.AppendLine("          , OC_DESCRIPTION                ");
          _sb.AppendLine("          , OC_CODE                       ");
          _sb.AppendLine("          , OC_ENABLED                    ");
          _sb.AppendLine("     FROM   OCCUPATIONS                   ");
          _sb.AppendLine("    WHERE   OC_COUNTRY_ISO_CODE2 = @pCountry_iso_code ");
          _sb.AppendLine(" ORDER BY   OC_ORDER, OC_DESCRIPTION      ");

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pCountry_iso_code", SqlDbType.NVarChar).Value = Resource.CountryISOCode2;
              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
              {
                _sql_da.Fill(_table);
              }
            }
          }

          m_occupations = _table;
          m_tick_read = Misc.GetTickCount();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public static void GetOccupationInfo(Int32 Id, out String Description, out String Code)
    {
      Description = String.Empty;
      Code = String.Empty;

      LoadOccupations();

      if (m_occupations == null)
      {
        return;
      }

      DataRow[] _occupation;
      _occupation = m_occupations.Select("OC_ID = " + Id.ToString());
      if (_occupation != null && _occupation.Length > 0)
      {
        Description = (String)_occupation[0]["OC_DESCRIPTION"];
        Code = (String)_occupation[0]["OC_CODE"];
      }
    }

    public static void GetOccupationInfoByCode(String Code, out String Description, out Int32 Id)
    {
      Description = String.Empty;
      Id = 0;

      LoadOccupations();

      if (m_occupations == null)
      {
        return;
      }

      DataRow[] _occupation;
      _occupation = m_occupations.Select("OC_CODE = " + Code);
      if (_occupation != null && _occupation.Length > 0)
      {
        Description = (String)_occupation[0]["OC_DESCRIPTION"];
        Id = (Int32)_occupation[0]["OC_ID"];
      }
    }

    public static void Update(Object DataSource, Int32 HolderOccupationId, Int32 PositionToInsert)
    {
      DataTable _dt;
      DataRow[] _oc_holder_source;
      DataRow[] _oc_holder;
      DataRow _dr;
      String _column_id;
      String _column_name;

      LoadOccupations();

      if (m_occupations == null)
      {
        return;
      }

      _oc_holder_source = m_occupations.Select("OC_ID = " + HolderOccupationId.ToString());
      if (_oc_holder_source == null || _oc_holder_source.Length == 0)
      {
        // Must found!
        return;
      }

      _dt = (DataTable)DataSource;

      _column_id = "OC_ID";
      _column_name = "OC_DESCRIPTION";
      if (!_dt.Columns.Contains(_column_id))
      {
        // GUI support
        _column_id = "Id";
        _column_name = "Name";
      }
      _oc_holder = _dt.Select(_column_id + " = " + HolderOccupationId.ToString());
      if (_oc_holder != null && _oc_holder.Length > 0)
      {
        return;
      }

      _dr = _dt.NewRow();
      _dr[_column_id] = HolderOccupationId;
      _dr[_column_name] = _oc_holder_source[0]["OC_DESCRIPTION"];

      _dt.Rows.InsertAt(_dr, PositionToInsert);
    }

  }  // Occupations

  public static class Countries
  {
    private static DataTable m_countries = null;
    private static int m_tick_read = 0;

    public static Dictionary<string, string> ISO2toISO3 = new Dictionary<string, string>()
      {
        {"AX", "ALA"},
        {"AF", "AFG"},
        {"AL", "ALB"},
        {"DZ", "DZA"},
        {"AS", "ASM"},
        {"AD", "AND"},
        {"AO", "AGO"},
        {"AI", "AIA"},
        {"AQ", "ATA"},
        {"AG", "ATG"},
        {"AR", "ARG"},
        {"AM", "ARM"},
        {"AW", "ABW"},
        {"AU", "AUS"},
        {"AT", "AUT"},
        {"AZ", "AZE"},
        {"BS", "BHS"},
        {"BH", "BHR"},
        {"BD", "BGD"},
        {"BB", "BRB"},
        {"BY", "BLR"},
        {"BE", "BEL"},
        {"BZ", "BLZ"},
        {"BJ", "BEN"},
        {"BM", "BMU"},
        {"BT", "BTN"},
        {"BO", "BOL"},
        {"BA", "BIH"},
        {"BW", "BWA"},
        {"BV", "BVT"},
        {"BR", "BRA"},
        {"IO", "IOT"},
        {"BN", "BRN"},
        {"BG", "BGR"},
        {"BF", "BFA"},
        {"BI", "BDI"},
        {"KH", "KHM"},
        {"CM", "CMR"},
        {"CA", "CAN"},
        {"CV", "CPV"},
        {"KY", "CYM"},
        {"CF", "CAF"},
        {"TD", "TCD"},
        {"CL", "CHL"},
        {"CN", "CHN"},
        {"CX", "CXR"},
        {"CC", "CCK"},
        {"CO", "COL"},
        {"KM", "COM"},
        {"CD", "COD"},
        {"CG", "COG"},
        {"CK", "COK"},
        {"CR", "CRI"},
        {"CI", "CIV"},
        {"HR", "HRV"},
        {"CU", "CUB"},
        {"CY", "CYP"},
        {"CZ", "CZE"},
        {"DK", "DNK"},
        {"DJ", "DJI"},
        {"DM", "DMA"},
        {"DO", "DOM"},
        {"EC", "ECU"},
        {"EG", "EGY"},
        {"SV", "SLV"},
        {"GQ", "GNQ"},
        {"ER", "ERI"},
        {"EE", "EST"},
        {"ET", "ETH"},
        {"FK", "FLK"},
        {"FO", "FRO"},
        {"FJ", "FJI"},
        {"FI", "FIN"},
        {"FR", "FRA"},
        {"GF", "GUF"},
        {"PF", "PYF"},
        {"TF", "ATF"},
        {"GA", "GAB"},
        {"GM", "GMB"},
        {"GE", "GEO"},
        {"DE", "DEU"},
        {"GH", "GHA"},
        {"GI", "GIB"},
        {"GR", "GRC"},
        {"GL", "GRL"},
        {"GD", "GRD"},
        {"GP", "GLP"},
        {"GU", "GUM"},
        {"GT", "GTM"},
        {"GN", "GIN"},
        {"GW", "GNB"},
        {"GY", "GUY"},
        {"HT", "HTI"},
        {"HM", "HMD"},
        {"HN", "HND"},
        {"HK", "HKG"},
        {"HU", "HUN"},
        {"IS", "ISL"},
        {"IN", "IND"},
        {"ID", "IDN"},
        {"IR", "IRN"},
        {"IQ", "IRQ"},
        {"IE", "IRL"},
        {"IL", "ISR"},
        {"IT", "ITA"},
        {"JM", "JAM"},
        {"JP", "JPN"},
        {"JO", "JOR"},
        {"KZ", "KAZ"},
        {"KE", "KEN"},
        {"KI", "KIR"},
        {"KP", "PRK"},
        {"KR", "KOR"},
        {"KW", "KWT"},
        {"KG", "KGZ"},
        {"LA", "LAO"},
        {"LV", "LVA"},
        {"LB", "LBN"},
        {"LS", "LSO"},
        {"LR", "LBR"},
        {"LY", "LBY"},
        {"LI", "LIE"},
        {"LT", "LTU"},
        {"LU", "LUX"},
        {"MO", "MAC"},
        {"MK", "MKD"},
        {"MG", "MDG"},
        {"MW", "MWI"},
        {"MY", "MYS"},
        {"MV", "MDV"},
        {"ML", "MLI"},
        {"MT", "MLT"},
        {"MH", "MHL"},
        {"MQ", "MTQ"},
        {"MR", "MRT"},
        {"MU", "MUS"},
        {"YT", "MYT"},
        {"MX", "MEX"},
        {"FM", "FSM"},
        {"MD", "MDA"},
        {"MC", "MCO"},
        {"MN", "MNG"},
        {"MS", "MSR"},
        {"MA", "MAR"},
        {"MZ", "MOZ"},
        {"MM", "MMR"},
        {"NA", "NAM"},
        {"NR", "NRU"},
        {"NP", "NPL"},
        {"NL", "NLD"},
        {"AN", "ANT"},
        {"NC", "NCL"},
        {"NZ", "NZL"},
        {"NI", "NIC"},
        {"NE", "NER"},
        {"NG", "NGA"},
        {"NU", "NIU"},
        {"NF", "NFK"},
        {"MP", "MNP"},
        {"NO", "NOR"},
        {"OM", "OMN"},
        {"PK", "PAK"},
        {"PW", "PLW"},
        {"PS", "PSE"},
        {"PA", "PAN"},
        {"PG", "PNG"},
        {"PY", "PRY"},
        {"PE", "PER"},
        {"PH", "PHL"},
        {"PN", "PCN"},
        {"PL", "POL"},
        {"PT", "PRT"},
        {"PR", "PRI"},
        {"QA", "QAT"},
        {"RE", "REU"},
        {"RO", "ROU"},
        {"RU", "RUS"},
        {"RW", "RWA"},
        {"SH", "SHN"},
        {"KN", "KNA"},
        {"LC", "LCA"},
        {"PM", "SPM"},
        {"VC", "VCT"},
        {"WS", "WSM"},
        {"SM", "SMR"},
        {"ST", "STP"},
        {"SA", "SAU"},
        {"SN", "SEN"},
        {"CS", "SCG"},
        {"SC", "SYC"},
        {"SL", "SLE"},
        {"SG", "SGP"},
        {"SK", "SVK"},
        {"SI", "SVN"},
        {"SB", "SLB"},
        {"SO", "SOM"},
        {"ZA", "ZAF"},
        {"GS", "SGS"},
        {"ES", "ESP"},
        {"LK", "LKA"},
        {"SD", "SDN"},
        {"SR", "SUR"},
        {"SJ", "SJM"},
        {"SZ", "SWZ"},
        {"SE", "SWE"},
        {"CH", "CHE"},
        {"SY", "SYR"},
        {"TW", "TWN"},
        {"TJ", "TJK"},
        {"TZ", "TZA"},
        {"TH", "THA"},
        {"TL", "TLS"},
        {"TG", "TGO"},
        {"TK", "TKL"},
        {"TO", "TON"},
        {"TT", "TTO"},
        {"TN", "TUN"},
        {"TR", "TUR"},
        {"TM", "TKM"},
        {"TC", "TCA"},
        {"TV", "TUV"},
        {"UG", "UGA"},
        {"UA", "UKR"},
        {"AE", "ARE"},
        {"GB", "GBR"},
        {"US", "USA"},
        {"UM", "UMI"},
        {"UY", "URY"},
        {"UZ", "UZB"},
        {"VU", "VUT"},
        {"VA", "VAT"},
        {"VE", "VEN"},
        {"VN", "VNM"},
        {"VG", "VGB"},
        {"VI", "VIR"},
        {"WF", "WLF"},
        {"EH", "ESH"},
        {"YE", "YEM"},
        {"ZM", "ZMB"},
        {"ZW", "ZWE"},
        { "",""}
      };

    public static string ISO3toISO2(string Code)
    {
      var _codes = new Dictionary<string, string>
        {
          {"ALA", "AX"},
          {"AFG", "AF"},
          {"ALB", "AL"},
          {"DZA", "DZ"},
          {"ASM", "AS"},
          {"AND", "AD"},
          {"AGO", "AO"},
          {"AIA", "AI"},
          {"ATA", "AQ"},
          {"ATG", "AG"},
          {"ARG", "AR"},
          {"ARM", "AM"},
          {"ABW", "AW"},
          {"AUS", "AU"},
          {"AUT", "AT"},
          {"AZE", "AZ"},
          {"BHS", "BS"},
          {"BHR", "BH"},
          {"BGD", "BD"},
          {"BRB", "BB"},
          {"BLR", "BY"},
          {"BEL", "BE"},
          {"BLZ", "BZ"},
          {"BEN", "BJ"},
          {"BMU", "BM"},
          {"BTN", "BT"},
          {"BOL", "BO"},
          {"BIH", "BA"},
          {"BWA", "BW"},
          {"BVT", "BV"},
          {"BRA", "BR"},
          {"IOT", "IO"},
          {"BRN", "BN"},
          {"BGR", "BG"},
          {"BFA", "BF"},
          {"BDI", "BI"},
          {"KHM", "KH"},
          {"CMR", "CM"},
          {"CAN", "CA"},
          {"CPV", "CV"},
          {"CYM", "KY"},
          {"CAF", "CF"},
          {"TCD", "TD"},
          {"CHL", "CL"},
          {"CHN", "CN"},
          {"CXR", "CX"},
          {"CCK", "CC"},
          {"COL", "CO"},
          {"COM", "KM"},
          {"COD", "CD"},
          {"COG", "CG"},
          {"COK", "CK"},
          {"CRI", "CR"},
          {"CIV", "CI"},
          {"HRV", "HR"},
          {"CUB", "CU"},
          {"CYP", "CY"},
          {"CZE", "CZ"},
          {"DNK", "DK"},
          {"DJI", "DJ"},
          {"DMA", "DM"},
          {"DOM", "DO"},
          {"ECU", "EC"},
          {"EGY", "EG"},
          {"SLV", "SV"},
          {"GNQ", "GQ"},
          {"ERI", "ER"},
          {"EST", "EE"},
          {"ETH", "ET"},
          {"FLK", "FK"},
          {"FRO", "FO"},
          {"FJI", "FJ"},
          {"FIN", "FI"},
          {"FRA", "FR"},
          {"GUF", "GF"},
          {"PYF", "PF"},
          {"ATF", "TF"},
          {"GAB", "GA"},
          {"GMB", "GM"},
          {"GEO", "GE"},
          {"DEU", "DE"},
          {"GHA", "GH"},
          {"GIB", "GI"},
          {"GRC", "GR"},
          {"GRL", "GL"},
          {"GRD", "GD"},
          {"GLP", "GP"},
          {"GUM", "GU"},
          {"GTM", "GT"},
          {"GIN", "GN"},
          {"GNB", "GW"},
          {"GUY", "GY"},
          {"HTI", "HT"},
          {"HMD", "HM"},
          {"HND", "HN"},
          {"HKG", "HK"},
          {"HUN", "HU"},
          {"ISL", "IS"},
          {"IND", "IN"},
          {"IDN", "ID"},
          {"IRN", "IR"},
          {"IRQ", "IQ"},
          {"IRL", "IE"},
          {"ISR", "IL"},
          {"ITA", "IT"},
          {"JAM", "JM"},
          {"JPN", "JP"},
          {"JOR", "JO"},
          {"KAZ", "KZ"},
          {"KEN", "KE"},
          {"KIR", "KI"},
          {"PRK", "KP"},
          {"KOR", "KR"},
          {"KWT", "KW"},
          {"KGZ", "KG"},
          {"LAO", "LA"},
          {"LVA", "LV"},
          {"LBN", "LB"},
          {"LSO", "LS"},
          {"LBR", "LR"},
          {"LBY", "LY"},
          {"LIE", "LI"},
          {"LTU", "LT"},
          {"LUX", "LU"},
          {"MAC", "MO"},
          {"MKD", "MK"},
          {"MDG", "MG"},
          {"MWI", "MW"},
          {"MYS", "MY"},
          {"MDV", "MV"},
          {"MLI", "ML"},
          {"MLT", "MT"},
          {"MHL", "MH"},
          {"MTQ", "MQ"},
          {"MRT", "MR"},
          {"MUS", "MU"},
          {"MYT", "YT"},
          {"MEX", "MX"},
          {"FSM", "FM"},
          {"MDA", "MD"},
          {"MCO", "MC"},
          {"MNG", "MN"},
          {"MSR", "MS"},
          {"MAR", "MA"},
          {"MOZ", "MZ"},
          {"MMR", "MM"},
          {"NAM", "NA"},
          {"NRU", "NR"},
          {"NPL", "NP"},
          {"NLD", "NL"},
          {"ANT", "AN"},
          {"NCL", "NC"},
          {"NZL", "NZ"},
          {"NIC", "NI"},
          {"NER", "NE"},
          {"NGA", "NG"},
          {"NIU", "NU"},
          {"NFK", "NF"},
          {"MNP", "MP"},
          {"NOR", "NO"},
          {"OMN", "OM"},
          {"PAK", "PK"},
          {"PLW", "PW"},
          {"PSE", "PS"},
          {"PAN", "PA"},
          {"PNG", "PG"},
          {"PRY", "PY"},
          {"PER", "PE"},
          {"PHL", "PH"},
          {"PCN", "PN"},
          {"POL", "PL"},
          {"PRT", "PT"},
          {"PRI", "PR"},
          {"QAT", "QA"},
          {"REU", "RE"},
          {"ROU", "RO"},
          {"RUS", "RU"},
          {"RWA", "RW"},
          {"SHN", "SH"},
          {"KNA", "KN"},
          {"LCA", "LC"},
          {"SPM", "PM"},
          {"VCT", "VC"},
          {"WSM", "WS"},
          {"SMR", "SM"},
          {"STP", "ST"},
          {"SAU", "SA"},
          {"SEN", "SN"},
          {"SCG", "CS"},
          {"SYC", "SC"},
          {"SLE", "SL"},
          {"SGP", "SG"},
          {"SVK", "SK"},
          {"SVN", "SI"},
          {"SLB", "SB"},
          {"SOM", "SO"},
          {"ZAF", "ZA"},
          {"SGS", "GS"},
          {"ESP", "ES"},
          {"LKA", "LK"},
          {"SDN", "SD"},
          {"SUR", "SR"},
          {"SJM", "SJ"},
          {"SWZ", "SZ"},
          {"SWE", "SE"},
          {"CHE", "CH"},
          {"SYR", "SY"},
          {"TWN", "TW"},
          {"TJK", "TJ"},
          {"TZA", "TZ"},
          {"THA", "TH"},
          {"TLS", "TL"},
          {"TGO", "TG"},
          {"TKL", "TK"},
          {"TON", "TO"},
          {"TTO", "TT"},
          {"TUN", "TN"},
          {"TUR", "TR"},
          {"TKM", "TM"},
          {"TCA", "TC"},
          {"TUV", "TV"},
          {"UGA", "UG"},
          {"UKR", "UA"},
          {"ARE", "AE"},
          {"GBR", "GB"},
          {"USA", "US"},
          {"UMI", "UM"},
          {"URY", "UY"},
          {"UZB", "UZ"},
          {"VUT", "VU"},
          {"VAT", "VA"},
          {"VEN", "VE"},
          {"VNM", "VN"},
          {"VGB", "VG"},
          {"VIR", "VI"},
          {"WLF", "WF"},
          {"ESH", "EH"},
          {"YEM", "YE"},
          {"ZMB", "ZM"},
          {"ZWE", "ZW"},
          {"", ""}
        };
      if (!_codes.ContainsKey(Code))
        return "";
      return _codes[Code];
    }



    internal static DataTable CloneTable()
    {
      LoadCountries();
      if (m_countries != null)
      {
        return m_countries.Copy();
      }
      return new DataTable();
    }

    private static void LoadCountries()
    {
      try
      {
        if (m_countries != null)
        {
          if (Misc.GetElapsedTicks(m_tick_read) < (5 * 60 * 1000))
          {
            return;
          }
        }

        using (DB_TRX _trx = new DB_TRX())
        {
          DataTable _table;
          StringBuilder _sb;

          _table = new DataTable();

          _sb = new StringBuilder();
          _sb.AppendLine("   SELECT   CO_COUNTRY_ID          ");
          _sb.AppendLine("          , CO_NAME                ");
          _sb.AppendLine("          , CO_ISO2                ");
          _sb.AppendLine("     FROM   COUNTRIES              ");
          _sb.AppendLine("    WHERE   CO_LANGUAGE_ID = @pLanguageId  ");
          _sb.AppendLine(" ORDER BY   CO_NAME                ");

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pLanguageId", SqlDbType.Int).Value = Resource.LanguageId;

              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
              {
                _sql_da.Fill(_table);
              }
            }
          }

          m_countries = _table;
          m_tick_read = Misc.GetTickCount();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public static void GetCountryInfo(Int32 Id, out String Name, out String Adjective, out String Iso2)
    {
      Name = String.Empty;
      Adjective = String.Empty;
      Iso2 = String.Empty;

      LoadCountries();

      if (m_countries == null)
  {
        return;
      }

      DataRow[] _country;
      _country = m_countries.Select("CO_COUNTRY_ID = " + Id.ToString());
      if (_country != null && _country.Length > 0)
      {
        Name = (String)_country[0]["CO_NAME"];
        Adjective = Name;
        Iso2 = (String)_country[0]["CO_ISO2"];
      }
    }

    public static Boolean ISO2ToCountryId(String Iso2, out Int32 CountryId)
    {
      CountryId = 0;

      try
      {
        LoadCountries();

        if (m_countries != null)
        {
          DataRow[] _country;
          _country = m_countries.Select("CO_ISO2 = '" + Iso2.ToString() + "'");
          if (_country != null && _country.Length > 0)
          {
            CountryId = (int)_country[0]["CO_COUNTRY_ID"];

            return true;
          }
        }
      }
      catch
      { }

      return false;
    }
  }

  public static class States
    {
    private static DataTable m_states = null;
    private static int m_tick_read = 0;

    internal static DataTable CloneTable()
    {
      LoadStates();
      if (m_states != null)
      {
        return m_states.Copy();
      }
      return new DataTable();
    }

    public static Boolean GetStates(out DataTable States, String CountryISOCode)
    {
      StringBuilder _sb;
      States = new DataTable();
      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("   SELECT   FS_STATE_ID     ");
          _sb.AppendLine("          , FS_NAME         ");
          _sb.AppendLine("     FROM   FEDERAL_STATES  ");
          _sb.AppendLine("    WHERE   FS_COUNTRY_ISO_CODE2 = @pCountry_iso_code");
          _sb.AppendLine(" ORDER BY   FS_NAME         ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
            _sql_cmd.Parameters.Add("@pCountry_iso_code", SqlDbType.NVarChar).Value = CountryISOCode;
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              States = new DataTable();
              _sql_da.Fill(States);

              return true;
            }
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    }

    private static void LoadStates()
    {
      try
      {
        DataTable _table;
        if (m_states != null)
        {
          if (Misc.GetElapsedTicks(m_tick_read) < (5 * 60 * 1000))
          {
            return;
          }
        }

        if (!GetStates(out _table, Resource.CountryISOCode2))
        {
          return;
        }

        m_states = _table;
        m_tick_read = Misc.GetTickCount();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public static String StateName(Int32 StateId)
    {
      LoadStates();

      if (m_states == null)
      {
        return String.Empty;
      }

      DataRow[] _state;
      _state = m_states.Select("FS_STATE_ID = " + StateId.ToString());
      if (_state != null && _state.Length > 0)
      {
        return (String)_state[0]["FS_NAME"];
    }

      return String.Empty;
    }

    public static Int32 StateId(String StateName)
    {
      LoadStates();

      if (m_states == null)
      {
        return -1;
      }

      DataRow[] _state;
      _state = m_states.Select("FS_NAME = '" + StateName + "'");
      if (_state != null && _state.Length > 0)
      {
        return (Int32)_state[0]["FS_STATE_ID"];
      }

      return -1;
    }
  }

  public static class Zip
  {
    private static DataTable m_zip_list = null;
    private static DataTable m_zip_list_filter = null;
    private static int m_tick_read_list = 0;
    private static DataTable m_zip_setting = null;
    private static int m_tick_read_setting = 0;

    public const String SQL_COUNTRY_ISO2 = "CZL_COUNTRY_ISO2";
    public const String SQL_ZIP_CODE = "CZL_ZIP_CODE";
    public const String SQL_ID = "ID";
    public const String SQL_ADDR = "ADDR";
    public const String SQL_FEDERAL_ENTITY = "CZL_ADDR_04";
    public const String SQL_DELEGATION = "CZL_ADDR_03";
    public const String SQL_COLONY = "COLONY";
    public const String SQL_CITY = "CZL_ADDR_05";
    public const String TABLE_NAME = "ADDRESS";

    public const String CASE_FEDERAL_ENTITY = "FederalEntity";
    public const String CASE_DELEGATION = "Delegation";
    public const String CASE_COLONY = "Colony";
    public const String CASE_CITY = "City";

    public struct Address
    {
      public Int32 CountryId;
      public String Zip;
      public String FederalEntity;
      public String Delegation;
      public String Colony;
      public String City;
      public String Street;
      public String ExtNum;
    }

    internal static DataTable CloneZipSetting()
    {
      LoadZipSetting();
      if (m_zip_setting != null)
      {
        return m_zip_setting.Copy();
      }
      return new DataTable();
    }

    private static void LoadZipSetting()
    {
      try
      {
        if (m_zip_setting != null)
        {
          if (Misc.GetElapsedTicks(m_tick_read_setting) < (5 * 60 * 1000))
          {
            return;
          }
    }

        using (DB_TRX _trx = new DB_TRX())
        {
          DataTable _table;
          StringBuilder _sb;

          _table = new DataTable();

          _sb = new StringBuilder();
          _sb.AppendLine("   SELECT   CZS_COUNTRY_ISO2          ");
          _sb.AppendLine("          , CZS_MODE                  ");
          _sb.AppendLine("          , CZS_FORMAT                ");
          _sb.AppendLine("          , CZS_REGEX                 ");
          _sb.AppendLine("          , CZS_ADDR_01               ");
          _sb.AppendLine("          , CZS_ADDR_02               ");
          _sb.AppendLine("          , CZS_ADDR_03               ");
          _sb.AppendLine("          , CZS_ADDR_04               ");
          _sb.AppendLine("          , CZS_ADDR_05               ");
          _sb.AppendLine("     FROM   COUNTRY_ZIP_CODE_SETTINGS ");

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
              {
                _sql_da.Fill(_table);
              }
            }
          }

          m_zip_setting = _table;
          m_tick_read_setting = Misc.GetTickCount();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    internal static DataTable CloneZipList()
    {
      LoadZipList();
      if (m_zip_list != null)
      {
        return m_zip_list.Copy();
      }
      return new DataTable();
    }

    private static void LoadZipList()
    {
      try
      {
        if (m_zip_list != null && m_zip_list.Rows.Count > 0)
        {
          //if (Misc.GetElapsedTicks(m_tick_read_list) < (5 * 60 * 1000))
          //{
          return;
          //}
        }

        using (DB_TRX _trx = new DB_TRX())
        {
          DataTable _table;
          StringBuilder _sb;

          _table = new DataTable();

          _sb = new StringBuilder();

          _sb.AppendLine("   SELECT   CZL_COUNTRY_ISO2                            ");
          _sb.AppendLine("          , CZL_ZIP_CODE                                ");
          _sb.AppendLine("          , CZL_ADDR_01 + ' ' + CZL_ADDR_02 AS COLONY   ");
          _sb.AppendLine("          , CZL_ADDR_03                                 ");
          _sb.AppendLine("          , CZL_ADDR_04                                 ");
          _sb.AppendLine("          , CZL_ADDR_05                                 ");
          _sb.AppendLine("     FROM   COUNTRY_ZIP_CODE_LIST                       ");

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
              {
                _sql_da.Fill(_table);
              }
            }
          }

          m_zip_list = _table;
          m_tick_read_list = Misc.GetTickCount();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public static Boolean ValidationCountryDefault(Int32 CountryId)
    {
      String _name;
      String _adj;
      String _iso2;

      Countries.GetCountryInfo(CountryId, out _name, out _adj, out _iso2);

      LoadZipSetting();

      if (m_zip_setting == null)
      {
        return false;
      }

      try
      {
        DataRow[] _zip_setting;
        _zip_setting = m_zip_setting.Select("CZS_COUNTRY_ISO2 = '" + _iso2 + "' AND CZS_MODE = " + (Int32)COUNTRY_ZIP_CODE_SETTINGS.COUNTRY_LIST);
        if (_zip_setting != null)
        {
              return true;
            }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean ValidationZip(Int32 CountryId, String Zip)
    {
      String _name;
      String _adj;
      String _iso2;
      String _regex;

      Countries.GetCountryInfo(CountryId, out _name, out _adj, out _iso2);

      LoadZipSetting();

      if (m_zip_setting == null)
      {
        return false;
      }

      try
      {
        DataRow[] _zip_setting;
        _zip_setting = m_zip_setting.Select("CZS_COUNTRY_ISO2 = '" + _iso2 + "' AND CZS_MODE = " + (Int32)COUNTRY_ZIP_CODE_SETTINGS.COUNTRY_LIST);

        if (_zip_setting != null && _zip_setting.Length > 0)
        {

          _regex = (String)_zip_setting[0]["CZS_REGEX"];
          if (System.Text.RegularExpressions.Regex.IsMatch(Zip, _regex))
          {

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    public static String GetFilterToAddress(Address Address, Boolean ForceZipFilter)
    {
      String _name;
      String _adj;
      String _iso2;
      String filter;

      Countries.GetCountryInfo(Address.CountryId, out _name, out _adj, out _iso2);
      filter = null;

      if (Address.CountryId != 0)
      {
        filter = SQL_COUNTRY_ISO2 + " = '" + _iso2 + "'";
      }
      if (!String.IsNullOrEmpty(Address.Zip) || ForceZipFilter)
      {
        filter += (String.IsNullOrEmpty(filter) ? "" : " AND ") + SQL_ZIP_CODE + " = '" + Address.Zip + "'";
      }
      if (!String.IsNullOrEmpty(Address.FederalEntity))
      {
        filter += (String.IsNullOrEmpty(filter) ? "" : " AND ") + SQL_FEDERAL_ENTITY + " = '" + Address.FederalEntity + "'";
      }

      if (!String.IsNullOrEmpty(Address.Delegation))
      {
        filter += (String.IsNullOrEmpty(filter) ? "" : " AND ") + SQL_DELEGATION + " = '" + Address.Delegation + "'";
      }

      if (!String.IsNullOrEmpty(Address.Colony))
      {
        filter += (String.IsNullOrEmpty(filter) ? "" : " AND ") + SQL_COLONY + " = '" + Address.Colony + "'";
      }

      if (!String.IsNullOrEmpty(Address.City))
      {
        filter += (String.IsNullOrEmpty(filter) ? "" : " AND ") + SQL_CITY + " = '" + Address.City + "'";
      }

      return filter;

    }

    public static DataTable GetDataTableToAddress(String ParameterFilter, String Filter)
    {
      DataTable _dt;
      DataView _dv;
      DataColumn _dc;

      LoadZipList();

      _dt = new DataTable();

      if (m_zip_list == null)
      {
        return _dt;
      }

      try
      {
        // Use filter Zip & country to minimize datatable filter
        _dv = (ParameterFilter == CASE_FEDERAL_ENTITY) ? _dv = m_zip_list.DefaultView : _dv = m_zip_list_filter.DefaultView;
        _dv.RowFilter = Filter;

        //Use filter
        switch (ParameterFilter)
        {
          case CASE_FEDERAL_ENTITY:
            _dt = _dv.ToTable(TABLE_NAME, true, SQL_FEDERAL_ENTITY);
            m_zip_list_filter = _dv.ToTable();
            break;
          case CASE_DELEGATION:
            _dt = _dv.ToTable(TABLE_NAME, true, SQL_DELEGATION);
            break;
          case CASE_COLONY:
            _dt = _dv.ToTable(TABLE_NAME, true, SQL_COLONY);
            break;
          case CASE_CITY:
            _dt = _dv.ToTable(TABLE_NAME, true, SQL_CITY);
            break;
          default:
            _dt = _dv.ToTable();
            break;
        }

        //Create struct Table
        _dc = new DataColumn();
        _dc.DataType = System.Type.GetType("System.Int32");
        _dc.ColumnName = SQL_ID;
        _dt.Columns.Add(_dc);
        _dt.Columns[0].ColumnName = SQL_ADDR;

        for (int _index = 0; _index < _dt.Rows.Count; _index++)
    {
          _dt.Rows[_index][SQL_ID] = _index;
        }
      }
      catch (Exception _ex)
    {
        Log.Exception(_ex);
      }

      return _dt;
        }
        }

  public static class IdentificationTypes
  {
    private static DataTable m_identification_types = null;
    private static int m_tick_read = 0;
    private static int m_other_id = 0;

    public static int OtherId
      {
      get
      {
        return m_other_id;
      }
    }

    internal static DataTable CloneTable()
    {
      LoadIdentificationTypes();
      if (m_identification_types != null)
      {
        return m_identification_types.Copy();
  }
      return new DataTable();
      }

    private static void LoadIdentificationTypes()
      {
      try
      {
        if (m_identification_types != null)
        {
          if (Misc.GetElapsedTicks(m_tick_read) < (5 * 60 * 1000))
          {
            return;
          }
        }
        // SJA 8-3-2016 removed redundent
        //using (DB_TRX _trx = new DB_TRX())
        //{
        DataTable _table;
        StringBuilder _sb;

        _table = new DataTable();

        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   IDT_ID                                         ");
        _sb.AppendLine("          , IDT_NAME                                       ");
        _sb.AppendLine("          , IDT_ENABLED                                    ");
        _sb.AppendLine("          , IDT_DESCRIPTION                                ");
        _sb.AppendLine("          , IDT_REGEX                                      ");
        _sb.AppendLine("          , IDT_AUX_NAME                                   ");
        _sb.AppendLine("          , IDT_ORDER                                      ");
        _sb.AppendLine("     FROM   IDENTIFICATION_TYPES                           ");
        _sb.AppendLine("    WHERE   IDT_COUNTRY_ISO_CODE2 = @pCountry_iso_code     ");
        _sb.AppendLine("   ORDER BY IDT_ENABLED DESC,                              ");
        _sb.AppendLine("            IDT_ORDER   ASC,                               ");
        _sb.AppendLine("            IDT_NAME    ASC                                ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (
            SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection,
              _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pCountry_iso_code", SqlDbType.NVarChar).Value = Resource.CountryISOCode2;
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_table);
            }
          }
        }

        m_identification_types = _table;
        DataRow[] _identification_type;
        _identification_type = m_identification_types.Select("IDT_ORDER >= 100");
        if (_identification_type != null && _identification_type.Length > 0)
        {
          m_other_id = (Int32)_identification_type[0]["IDT_ID"];
        }
        m_tick_read = Misc.GetTickCount();
        //}
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    internal static String GetIdentificationName(Int32 Id)
    {
      String _name;
      _name = String.Empty;

      LoadIdentificationTypes();

      if (m_identification_types == null)
      {
        return String.Empty;
      }

      DataRow[] _identification_type;
      _identification_type = m_identification_types.Select("IDT_ID = " + (Int32)Id);
      if (_identification_type != null && _identification_type.Length > 0)
      {
        _name = (String)_identification_type[0]["IDT_NAME"];
      }
      return _name;
    }
    internal static String GetIdentificationName(ACCOUNT_HOLDER_ID_TYPE Id)
    {
      return GetIdentificationName((Int32)Id);
    }

    public static Int32 GetIdentificationID(String Name)
    {
      LoadIdentificationTypes();

      if (m_identification_types == null || String.IsNullOrEmpty(Name)) //EOR 19-FEB-2018
      {
        return m_other_id;
      }

      DataRow[] _identification_type;
      _identification_type = m_identification_types.Select("IDT_NAME = '" + Name.Replace("'", "''") + "'");
      if (_identification_type != null && _identification_type.Length > 0)
      {
        return (Int32)_identification_type[0]["IDT_ID"];
      }

      return m_other_id;
    }

    public static Int32 ScannedTypeToID(String ScannedType) // ID, DL, 
    {
      DataRow[] _identification_type;

      LoadIdentificationTypes();

      if (m_identification_types == null)
      {
        return m_other_id;
      }

      _identification_type = m_identification_types.Select("IDT_AUX_NAME = '" + ScannedType.Replace("'", "''") + "'");
      if (_identification_type != null && _identification_type.Length > 0)
      {
        if ((Boolean)_identification_type[0]["IDT_ENABLED"])
        {
          return (Int32)_identification_type[0]["IDT_ID"];
        }
      }

      return m_other_id;
    }


    public static Int32 GetOrderFromID(ACCOUNT_HOLDER_ID_TYPE Id)
    {
      Int32 _order;
      _order = 0;

      LoadIdentificationTypes();

      if (m_identification_types == null)
      {
        return 0;
      }

      DataRow[] _identification_type;
      _identification_type = m_identification_types.Select("IDT_ID = " + (Int32)Id);
      if (_identification_type != null && _identification_type.Length > 0)
      {
        _order = (Int32)_identification_type[0]["IDT_ORDER"];
      }
      return _order;
    }

    public static DataTable GetIdentificationTypes(Boolean RemoveRFCCurp)
    {
      DataTable _table;
      DataRow[] _dr_rows;

      _table = IdentificationTypes.CloneTable();

      if (_table.Rows.Count >= 0)
      {
        if (RemoveRFCCurp)
        {
          _dr_rows = _table.Select("IDT_ID IN (" + (Int32)ACCOUNT_HOLDER_ID_TYPE.RFC + ", " + (Int32)ACCOUNT_HOLDER_ID_TYPE.CURP + ") OR IDT_ENABLED=FALSE");
        }
        else
        {
          _dr_rows = _table.Select("IDT_ENABLED=FALSE");
        }

        foreach (DataRow _row in _dr_rows)
        {
          _table.Rows.Remove(_row);
        }
      }

      return _table;
    }

    public static void GetIdentificationTypesPaymentOrders(out DataTable DocTypes1, out DataTable DocTypes2)
    {
      DataTable _table;
      DataRow[] _dr_rows;
      String _gp_types;

      DocTypes1 = new DataTable();
      DocTypes2 = new DataTable();

      _table = IdentificationTypes.CloneTable();

      _gp_types = GeneralParam.GetString("PaymentOrder", "AllowedDocuments1");

      if (!String.IsNullOrEmpty(_gp_types))
      {
        DocTypes1 = IdentificationTypes.CloneTable();
        DocTypes1.Clear();

        _dr_rows = _table.Select("IDT_ID IN (" + _gp_types + ") AND IDT_ENABLED = TRUE");

        foreach (DataRow _dr in _dr_rows)
        {
          DocTypes1.ImportRow(_dr);
        }

      }

      _gp_types = GeneralParam.GetString("PaymentOrder", "AllowedDocuments2");

      if (!String.IsNullOrEmpty(_gp_types))
      {
        DocTypes2 = IdentificationTypes.CloneTable();
        DocTypes2.Clear();

        _dr_rows = _table.Select("IDT_ID IN (" + _gp_types + ") AND IDT_ENABLED = TRUE");

        foreach (DataRow _dr in _dr_rows)
        {
          DocTypes2.ImportRow(_dr);
        }

      }
    }

    public static Dictionary<Int32, DataRow> GetIdentificationTypesProps()
    {
      Dictionary<Int32, DataRow> _dictionary;
      DataTable _table;

      _table = IdentificationTypes.CloneTable();
      _dictionary = new Dictionary<Int32, DataRow>();

      foreach (DataRow _row in _table.Rows)
      {
        if ((bool)_row["IDT_ENABLED"] == true)
        {
          _dictionary.Add((Int32)_row["IDT_ID"], _row);
        }
      }

      return _dictionary;
    }


    public static Dictionary<Int32, String> GetIdentificationTypes()
    {
      Dictionary<Int32, String> _dictionary;
      DataTable _table;

      _table = IdentificationTypes.CloneTable();
      _dictionary = new Dictionary<Int32, String>();

      foreach (DataRow _row in _table.Rows)
      {
        if ((bool)_row["IDT_ENABLED"] == true)
        {
          _dictionary.Add((Int32)_row["IDT_ID"], (String)_row["IDT_NAME"]);
        }
      }

      return _dictionary;
    }

    public static Boolean IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE Id)
    {
      Boolean _enabled = false;

      LoadIdentificationTypes();

      if (m_identification_types == null)
      {
      return false;
    }

      DataRow[] _identification_type;
      _identification_type = m_identification_types.Select("IDT_ID = " + (Int32)Id);
      if (_identification_type != null && _identification_type.Length > 0)
      {
        _enabled = (Boolean)_identification_type[0]["IDT_ENABLED"];
      }
      return _enabled;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Name string from DB for ACCOUNT_HOLDER_ID_TYPE values
    //
    // PARAMS:
    //   - INPUT: ACCOUNT_HOLDER_ID_TYPE
    //   - OUTPUT: None
    //
    // RETURNS:
    //     - NLS String
    //
    public static String DocIdTypeString(Int32 DocId)
      {
      return GetIdentificationName(DocId);
      }
    public static String DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE Value)
    {
      return IdentificationTypes.GetIdentificationName(Value);
      }

    //------------------------------------------------------------------------------
    // PURPOSE: Get NLS list of strings for ACCOUNT_HOLDER_ID_TYPE values
    //
    // PARAMS:
    //   - INPUT: String (Id3Type)
    //   - OUTPUT: None
    //
    // RETURNS:
    //     - NLS  String list of documents types. IN -> "001,002,001"  OUT --> "RFC, CURP, RFC" 
    //
    public static String DocIdTypeStringList(String Id3Types)
    {
      String[] _doc_types;
      String _strings_id3_types;
      bool _is_numeric;
      int _doc_type_numeric;

      _strings_id3_types = String.Empty;
      _is_numeric = false;

      try
      {
        if (String.IsNullOrEmpty(Id3Types))
        {
          return String.Empty;
        }
        Char[] _char_separator = new char[] { ',' };
        _doc_types = Id3Types.Split(_char_separator, StringSplitOptions.RemoveEmptyEntries);

        foreach (String _doc_type in _doc_types)
        {
          // ATB 05-DEC-2016
          // Avoiding an error when the dropdown option is not numeric
          _is_numeric = int.TryParse(_doc_type, out _doc_type_numeric);

          if (_is_numeric)
          {
            if (!String.IsNullOrEmpty(_strings_id3_types))
            {
              _strings_id3_types = String.Format("{0}, {1}", _strings_id3_types, DocIdTypeString((ACCOUNT_HOLDER_ID_TYPE)Convert.ToInt32(_doc_type)));
            }
            //First Time
            else
            {
              _strings_id3_types = DocIdTypeString((ACCOUNT_HOLDER_ID_TYPE)Convert.ToInt32(_doc_type));
            }
          }
        }
      }
      catch (Exception Ex)
      {
        Log.Error(String.Format(" CardData.DocIdTypeStringList - Retrieving Document Type - '{0}'", Ex.Message));
      }

      return _strings_id3_types;
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Get NLS list of strings for ACCOUNT_HOLDER_ID_TYPE values and not repeated values
    //
    // PARAMS:
    //   - INPUT: String (Id3Type)
    //   - OUTPUT: None
    //
    // RETURNS:
    //     - NLS  String list of documents types. IN -> "001,002,001,001"  OUT --> "RFC, CURP" 
    //
    public static String DocIdTypeStringList(String Id3Types, Boolean UniqueDocuments)
    {
      if (!UniqueDocuments)
      {
        return DocIdTypeStringList(Id3Types);
      }

      String[] _doc_types;
      List<string> _doc_types_out;
      String _strings_id3_types;

      _doc_types_out = new List<string>();
      _strings_id3_types = String.Empty;

      try
      {
        if (String.IsNullOrEmpty(Id3Types))
        {
          return String.Empty;
        }
        Char[] _char_separator = new char[] { ',' };
        _doc_types = Id3Types.Split(_char_separator, StringSplitOptions.RemoveEmptyEntries);

        foreach (String _doc_type in _doc_types)
        {
          if (!_doc_types_out.Contains(_doc_type))
          {
            _doc_types_out.Add(_doc_type);
            if (!String.IsNullOrEmpty(_strings_id3_types))
            {
              _strings_id3_types = String.Format("{0}, {1}", _strings_id3_types, DocIdTypeString((ACCOUNT_HOLDER_ID_TYPE)Convert.ToInt32(_doc_type)));
            }
            //First Time
            else
            {
              _strings_id3_types = DocIdTypeString((ACCOUNT_HOLDER_ID_TYPE)Convert.ToInt32(_doc_type));
            }
          }
        }
      }
      catch (Exception Ex)
      {
        Log.Error(String.Format(" CardData.DocIdTypeStringList - Retrieving Document Type - '{0}'", Ex.Message));
      }

      return _strings_id3_types;
    }





    public static string GetIdentificationName(long p)
        {
      throw new NotImplementedException();
    }
  } // IdentificationTypes

  //public class CardData: WSI.Common.Interfaces.ICardData //TODO FERRAN(TDD)
  [Serializable]
  public class CardData : ICloneable
  {
    public static Int32 ConfiguredSiteId = -1;

    public Int64 AccountId;
    public Boolean Blocked;
    public AccountBlockReason BlockReason;    // RCI 22-OCT-2010
    public String BlockDescription;           // JBP 06-MAY-2013
    public DateTime NotValidBefore;
    public DateTime NotValidAfter;
    public Currency CurrentBalance;
    public Int32 CardReplacementCount;          //AMF 01-JUL-2014
    public Boolean IsRecycled;                  //JCM 18-APR-2012 Flag: Card Recycled
    public Boolean IsNew
    {
      get
      {
        // Card Is new when there is no associated account id and belongs to a accepted site id
        CardTrackData _ct;
        _ct=new CardTrackData();
        _ct.SiteId=this.SiteId;
        return (this.AccountId == 0) && CardData.CheckTrackdataSiteId(_ct);
      }
    }

    public bool ContainNewAccount { get; set; }


    // MBF 25-AUG-2010 Trackdata should not visible in Cashier
    public String TrackData;

    //private String TrackData;

    public Currency MaxDevolution;
    public Currency MaxDevolutionForChips;
    public Currency Nr1Withhold;
    public Currency Deposit;

    // RCI 05-DEC-2012: All cancellable data is saved here.
    public CancellableData Cancellable = new CancellableData();

    // RCI 24-SEP-2010: Flag to indicate if the card has been paid.
    public Boolean CardPaid;

    // RCI 01-OCT-2010: Can't pay prize lower than NonRedeemableWonLock.
    public Boolean WonLockExists;
    public Currency NonRedeemableWonLock;
    public Boolean PromotionToRedeemableAllowed;
    public Currency PromotionToRedeemableAmount;

    // Indicates the number of promos by credit_type.
    public Dictionary<ACCOUNT_PROMO_CREDIT_TYPE, Int32> NumberOfPromosByCreditType = new Dictionary<ACCOUNT_PROMO_CREDIT_TYPE, Int32>();
    public Dictionary<ACCOUNT_PROMO_CREDIT_TYPE, Int32> NumberOfCoverCouponByCreditType = new Dictionary<ACCOUNT_PROMO_CREDIT_TYPE, Int32>();

    // RCI 03-DEC-2010: Site jackpot winner
    public Boolean IsSiteJackpotWinner;                    // Flag to indicate if the player has won the Site Jackpot
    public TYPE_SITE_JACKPOT_WINNER SiteJackpotWinnerInfo; // Info with the Site Jackpot winner info

    // RCI & AJQ 24-FEB-2011
    public Int64 Timestamp;

    // Logging data
    public Boolean IsLoggedIn;
    public Int32 LoggedInTerminalId;
    public String LoggedInTerminalName;
    public String LoggedInTerminalProvider;
    public TerminalTypes LoggedInTerminalType;
    // Lock Card data
    public Boolean IsLocked;
    // Site id.
    public Int32 SiteId;

    // Player tracking
    public PlayerTrackingData PlayerTracking = new PlayerTrackingData();

    public PlaySession CurrentPlaySession = new PlaySession();

    // Min Cashable Cents = Indivisibles
    private Int64 MinCashableCents = Int64.MinValue;

    // Card Recycle
    public const String RECYCLED_TRACK_DATA = "----------";       //Visible Recycled Track Data
    public const String RECYCLED_FLAG_TRACK_DATA = "-RECYCLED-";  //Value add a internal trackdata recycled
    public const String VIRTUAL_FLAG_TRACK_DATA = "-RECYCLED-VIRTUAL-";

    // JAB 18-JUN-2012: Added new fields.
    public MultiPromos.AccountBalance AccountBalance = MultiPromos.AccountBalance.Zero;
    public CreditLine CreditLineData = new CreditLine();

    // QMP 06-FEB-2013: PointsStatus (Point Redemption Allowed / Not Allowed)
    public ACCOUNT_POINTS_STATUS PointsStatus;

    public Boolean IsVirtualCard;

    public CardData()
    {
      this.MinCashableCents = GeneralParam.GetInt64("Cashier", "MinCashableCents");

      PlayerTracking.HolderHasBeneficiary = false;
      PlayerTracking.ShowCommentsOnCashier = false;
      ContainNewAccount = false;
      //DPC
      //PlayerTracking.BeneficiaryOccupationId = -1;
    }

    //public CardData(Int64 MinCashableCent) //TODO FERRAN(TDD)
    //{
    //  this.MinCashableCents = MinCashableCent;

    //  PlayerTracking.HolderHasBeneficiary = false;
    //  PlayerTracking.ShowCommentsOnCashier = false;
    //}

    public bool PromotionRemoveAllowed
    {
      get
      {
        Int32 _num_promos_nr1;
        Int32 _num_promos_nr2;
        Int32 _num_cover_coupon_nr1;
        Int32 _num_cover_coupon_nr2;

        _num_promos_nr1 = 0;
        if (this.NumberOfPromosByCreditType.ContainsKey(ACCOUNT_PROMO_CREDIT_TYPE.NR1))
        {
          _num_promos_nr1 = this.NumberOfPromosByCreditType[ACCOUNT_PROMO_CREDIT_TYPE.NR1];
        }
        _num_promos_nr2 = 0;
        if (this.NumberOfPromosByCreditType.ContainsKey(ACCOUNT_PROMO_CREDIT_TYPE.NR2))
        {
          _num_promos_nr2 = this.NumberOfPromosByCreditType[ACCOUNT_PROMO_CREDIT_TYPE.NR2];
        }

        _num_cover_coupon_nr1 = 0;
        if (this.NumberOfCoverCouponByCreditType.ContainsKey(ACCOUNT_PROMO_CREDIT_TYPE.NR1))
        {
          _num_cover_coupon_nr1 = this.NumberOfCoverCouponByCreditType[ACCOUNT_PROMO_CREDIT_TYPE.NR1];
        }
        _num_cover_coupon_nr2 = 0;
        if (this.NumberOfCoverCouponByCreditType.ContainsKey(ACCOUNT_PROMO_CREDIT_TYPE.NR2))
        {
          _num_cover_coupon_nr2 = this.NumberOfCoverCouponByCreditType[ACCOUNT_PROMO_CREDIT_TYPE.NR2];
        }

        return (_num_promos_nr1 > _num_cover_coupon_nr1
                || _num_promos_nr2 > _num_cover_coupon_nr2
                || (_num_promos_nr1 == 0 && _num_promos_nr2 == 0 && AccountBalance.TotalNotRedeemable > 0)
                || this.WonLockExists);
      }
    } // PromotionRemoveAllowed

    // RCI 25-MAY-2011
    public Boolean CanAddCredit
    {
      get
      {
        return true;
      }
    } // CanAddCredit

    public Boolean CanTransferCredit
    {
      get
      {
        return Accounts.CanTransferCredit(this) == Accounts.TRANSFER_CREDIT_RESULT.TRANSFER_OK;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generates and return the visible trackdata
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String in this format "*****************1234"
    //      - Where 1234 is de visible part of the trackdata
    // 
    //   NOTES:

    public String VisibleTrackdata()
    {
      return WSI.Common.Misc.VisibleTrackdata(TrackData);

    } // VisibleTrackdata

    //------------------------------------------------------------------------------
    // PURPOSE: Get the Owner of a Document Scanned and the id Type of a Document Scanned
    //
    // PARAMS:
    //   - INPUT: String (Document Name)
    //   - OUTPUT: ACCOUNT_SCANNED_OWNER Owner
    //   - OUTPUT: String TypeDoc
    // RETURNS:
    //     - True if is correct
    // NOTES:
    //        Document name structure (example): XXX_Y_Random Number
    //        XXX: Scanned Document Type
    //        Y: Scanned Document Owner --> Holder, Beneficiary.

    public static Boolean GetInfoDocName(String DocName, out ACCOUNT_SCANNED_OWNER Owner, out String TypeDoc)
    {

      Owner = ACCOUNT_SCANNED_OWNER.NONE;
      TypeDoc = "";

      try
      {
        if (String.IsNullOrEmpty(DocName))
        {
          return false;
        }

        TypeDoc = DocName.Substring(0, 3);
        Owner = (ACCOUNT_SCANNED_OWNER)Convert.ToInt32(DocName.Substring(4, 1));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("CardData.GetInfoNameDoc Error:" + _ex.Message);
      }

      return false;
    }

    public static Boolean GetInfoDocName(String DocName, out ACCOUNT_SCANNED_OWNER Owner)
    {
      String _str_dummy;

      return GetInfoDocName(DocName, out Owner, out _str_dummy);
    }

    public static Boolean GetInfoDocName(String DocName, out String TypeDoc)
    {
      ACCOUNT_SCANNED_OWNER _owner_dummy;

      return GetInfoDocName(DocName, out _owner_dummy, out TypeDoc);
    }

    public static Boolean GenerateDocName(ACCOUNT_SCANNED_OWNER Owner, String TypeDoc, out String Name)
    {
      try
      {
        // Document name structure (example): XXX_Y_Random Number
        // XXX: Scanned Document Type
        // Y: Scanned Document Owner --> Holder, Beneficiary.
        Name = String.Format("{0}_{1}_{2}", TypeDoc.PadLeft(3, '0'), (Int32)Owner, Guid.NewGuid().ToString("N"));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      Name = "";

      return false;
    }

    //----------------------------------------------------------------------------
    // PURPOSE: Get NLS strings for Gender values
    //
    // PARAMS:
    //   - INPUT: Value = 1 - Male | 2 - Female
    //   - OUTPUT: None
    //
    // RETURNS:
    //     - NLS String
    //
    public static String GenderString(Int32 Value)
    {
      switch (Value)
      {
        case 1:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE");
        case 2:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE");
        default:
          return "";
      }
    }

    //----------------------------------------------------------------------------
    // PURPOSE: Get NLS strings for ACCOUNT_MARITAL_STATUS values
    //
    // PARAMS:
    //   - INPUT: ACCOUNT_MARITAL_STATUS
    //   - OUTPUT: None
    //
    // RETURNS:
    //     - NLS String
    //
    public static String MaritalStatusString(ACCOUNT_MARITAL_STATUS Value)
    {
      switch (Value)
      {
        case ACCOUNT_MARITAL_STATUS.SINGLE:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_SINGLE");
        case ACCOUNT_MARITAL_STATUS.MARRIED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_MARRIED");
        case ACCOUNT_MARITAL_STATUS.DIVORCED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_DIVORCED");
        case ACCOUNT_MARITAL_STATUS.WIDOWED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_WIDOWED");
        case ACCOUNT_MARITAL_STATUS.UNMARRIED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNMARRIED");
        case ACCOUNT_MARITAL_STATUS.OTHER:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_OTHER");
        case ACCOUNT_MARITAL_STATUS.UNSPECIFIED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNSPECIFIED");
        case ACCOUNT_MARITAL_STATUS.ANNULED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_ANNULED");
        case ACCOUNT_MARITAL_STATUS.EMPTY:
        default:
          return string.Empty;
      }
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get description from a value list of Countries
    //
    //  PARAMS :
    //     - INPUT : CountryId - Country identifier
    //     - OUTPUT :
    //
    // RETURNS : String (description)
    public static String CountriesString(Int32 CountryId)
    {
      String _name;
      String _adj;
      String _iso2;

      Countries.GetCountryInfo(CountryId, out _name, out _adj, out _iso2);

      return _name;
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get description from a value list of Nationalities
    //
    //  PARAMS :
    //     - INPUT : CountryId - Country identifier
    //     - OUTPUT :
    //
    // RETURNS : String (description)
    public static String NationalitiesString(Int32 CountryId)
    {
      String _name;
      String _adj;
      String _iso2;

      Countries.GetCountryInfo(CountryId, out _name, out _adj, out _iso2);

      return _adj;
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get iso2 from a value list of Nationalities
    //
    //  PARAMS :
    //     - INPUT : CountryId - Country identifier
    //     - OUTPUT :
    //
    // RETURNS : String (description)
    public static String Iso2String(Int32 CountryId)
    {
      String _name;
      String _adj;
      String _iso2;

      Countries.GetCountryInfo(CountryId, out _name, out _adj, out _iso2);

      return _iso2;
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get description from a value list of Occupations
    //
    //  PARAMS :
    //     - INPUT : OccupationId - Occupation identifier
    //     - OUTPUT :
    //
    // RETURNS : String (description)
    public static String OccupationsDescriptionString(Int32 OccupationId)
    {
      String _description;
      String _code;

      Occupations.GetOccupationInfo(OccupationId, out _description, out _code);

      return _description;
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get description from a value list of Occupations
    //
    //  PARAMS :
    //     - INPUT : Code - Code Occupation
    //     - OUTPUT :
    //
    // RETURNS : String (description)
    public static String OccupationsDescriptionStringByCode(String Code)
    {
      String _description;
      Int32 _id;

      Occupations.GetOccupationInfoByCode(Code, out _description, out _id);

      return _description;
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get code from a value list of Occupations
    //
    //  PARAMS :
    //     - INPUT : OccupationId - Occupation identifier
    //     - OUTPUT :
    //
    // RETURNS : String (description)
    public static String OccupationsCodeString(Int32 OccupationId)
    {
      String _description;
      String _code;

      Occupations.GetOccupationInfo(OccupationId, out _description, out _code);

      return _code;
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get description from a value list of Federal State Entities
    //
    //  PARAMS :
    //     - INPUT : FedId - Federal State Entity identifier
    //     - OUTPUT : 
    //
    // RETURNS : String (description)
    public static String FedEntitiesString(Int32 StateId)
    {
      return States.StateName(StateId);
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get list of Countries
    //
    //  PARAMS :
    //     - INPUT : 
    //     - OUTPUT : 
    //
    // RETURNS : DataTable
    public static DataTable GetCountriesList()
    {
      DataTable _table;

      _table = Countries.CloneTable();
      //if (_table.Columns.Contains("CO_ISO2"))
      //{
      //  _table.Columns.Remove("CO_ISO2");
      //}

      return _table;

    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get list of Nationalities
    //
    //  PARAMS :
    //     - INPUT : 
    //     - OUTPUT : 
    //
    // RETURNS : DataTable
    public static DataTable GetNationalitiesList()
    {
      DataTable _table;

      _table = Countries.CloneTable();
      if (_table.Columns.Contains("CO_ISO2"))
      {
        _table.Columns.Remove("CO_ISO2");
      }
      if (_table.Columns.Contains("CO_NAME"))
      {
        _table.Columns["CO_NAME"].ColumnName = "CO_ADJECTIVE";
      }

      return _table;
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get list of Occupations
    //
    //  PARAMS :
    //     - INPUT : 
    //     - OUTPUT : 
    //
    // RETURNS : DataTable
    public static DataTable GetOccupationsList()
    {
      DataTable _table;
      DataRow[] _dr_rows;

      _table = Occupations.CloneTable();
      if (_table.Columns.Contains("OC_CODE"))
      {
        _table.Columns.Remove("OC_CODE");
      }

      if (_table.Rows.Count >= 0)
      {
        _dr_rows = _table.Select("OC_ENABLED = FALSE");

        foreach (DataRow _row in _dr_rows)
        {
          _table.Rows.Remove(_row);
        }
      }

      return _table;
    }

    //----------------------------------------------------------------------------
    // PURPOSE : Get list of Federal State Entities
    //
    //  PARAMS :
    //     - INPUT : 
    //     - OUTPUT : 
    //
    // RETURNS : DataTable
    public static DataTable GetFedStatesList()
    {
      return States.CloneTable();
    }



    public static Boolean GetPromotionsToBeCancelledDueToRedeem(Int64 AccountId,
                                                                Int32 FlagOpId,
                                                                List<CancellableOperation> CancelOperationList,
                                                                Boolean CancelAll,
                                                                Boolean UndoOperation,
                                                                out MultiPromos.PromoBalance LostBalance,
                                                                out DataTable Promotions, SqlTransaction Trx)
    {
      ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
      Decimal _promo_balance;
      StringBuilder _sb_select_from;
      StringBuilder _sb_where_1;
      StringBuilder _sb_where_2;
      String _index_1;
      String _index_2;
      String _sql_query;
      List<String> _operation_ids;

      Promotions = new DataTable();
      LostBalance = MultiPromos.PromoBalance.Zero;

      _sql_query = "";

      try
      {
        _sb_select_from = new StringBuilder();
        _sb_select_from.AppendLine(" SELECT   ACP_UNIQUE_ID                            ");
        _sb_select_from.AppendLine("        , ACP_ACCOUNT_ID                           ");
        _sb_select_from.AppendLine("        , ACP_BALANCE                              ");
        _sb_select_from.AppendLine("        , ACP_CREDIT_TYPE                          ");
        _sb_select_from.AppendLine("        , ACP_PROMO_NAME                           ");
        _sb_select_from.AppendLine("        , ACP_PROMO_TYPE                           ");
        _sb_select_from.AppendLine("        , ACP_PROMO_ID                             ");
        _sb_select_from.AppendLine("        , ACP_ACTIVATION                           ");
        _sb_select_from.AppendLine("        , ACP_PRIZE_TYPE                           ");
        _sb_select_from.AppendLine("        , ACP_PRIZE_GROSS                          ");
        _sb_select_from.AppendLine("        , ACP_PRIZE_TAX1                           ");
        _sb_select_from.AppendLine("        , ACP_PRIZE_TAX2                           ");
        _sb_select_from.AppendLine("        , ACP_PRIZE_TAX3                           ");
        _sb_select_from.AppendLine("        , ACP_PLAYED                               ");
        _sb_select_from.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH (INDEX(@pIndex)) ");

        _index_1 = "IX_acp_account_status";
        _index_2 = "IX_acp_operation_id";

        _sb_where_1 = new StringBuilder();
        _sb_where_1.AppendLine("  WHERE   ACP_ACCOUNT_ID   =  @pAccountId    ");
        _sb_where_1.AppendLine("    AND   ACP_STATUS       =  @pStatusActive ");
        _sb_where_1.AppendLine("    AND   ACP_CREDIT_TYPE IN (@pNR1, @pNR2)  ");
        if (!CancelAll)
        {
          _sb_where_1.AppendLine("    AND   ACP_CASH_IN    > 0 ");
        }

        _sb_where_2 = new StringBuilder();
        if (CancelOperationList != null && CancelOperationList.Count != 0)
        {
          _sb_where_2.AppendLine(
            " WHERE ( ACP_OPERATION_ID IN ( @pOpId )                                                                        ");
        }
        else
        {
        // GDA 23-MAR-2016
          _sb_where_2.AppendLine(
            " WHERE ((      ACP_PROMO_ID    NOT IN (SELECT pm_promotion_id FROM promotions WHERE  (pm_apply_tax = 1)) )     ");
        }
      
        _sb_where_2.AppendLine(
          "          AND (    ( ACP_CREDIT_TYPE  =  @pRE          AND ACP_STATUS = @pStatusRedeemed )                       ");
        _sb_where_2.AppendLine(
          "               OR  ( ACP_CREDIT_TYPE  =  @pREinKind    AND ACP_STATUS = @pStatusActive   )                       ");
        _sb_where_2.AppendLine(
          "               OR  ( ACP_CREDIT_TYPE IN (@pNR1, @pNR2) AND ACP_STATUS = @pStatusActive   )                       ");
        _sb_where_2.AppendLine(
          "               OR  ( ACP_CREDIT_TYPE  =  @pPoints      AND ACP_STATUS = @pStatusRedeemed )                       ");
        _sb_where_2.AppendLine(
          "              )                                                                                                  ");
        _sb_where_2.AppendLine(
          "        )                                                                                                        ");
        // RCI & AJQ 14-AUG-2013: The operation has a cancellable amount.
        //                        The promotions to cancel are only the related to CashIn (ACP_CASH_IN > 0).
        // RCI & ICS & DLL 11-OCT-2013: For Undo Operations, all promotions must be cancelled.
        if (!UndoOperation)
        {
          _sb_where_2.AppendLine("    AND   ACP_CASH_IN    > 0 ");
        }

        if (FlagOpId == 0)
        {
          // Cancel all NR promotions related with CashIn
          _sql_query = _sb_select_from.ToString() + _sb_where_1.ToString();
          _sql_query = _sql_query.Replace("@pIndex", _index_1);
        }
        else
        {
          if (CancelOperationList != null && CancelOperationList.Count != 0)
          {
          _operation_ids = new List<String>();
          _operation_ids.AddRange(CancelOperationList.ConvertAll(_x => _x.OperationId.ToString()));

          _sb_where_2 = _sb_where_2.Replace("@pOpId", String.Join(",", _operation_ids.ToArray()));
          }

          if (FlagOpId > 0)
          {
            // Cancel Only NR promotions related with the operation
            _sql_query = _sb_select_from.ToString() + _sb_where_2.ToString();
            _sql_query = _sql_query.Replace("@pIndex", _index_2);
          }
          else
          {
            // Cancel all promos related with CashIn + Operation
            _sql_query = _sb_select_from.ToString() + _sb_where_1.ToString();
            _sql_query = _sql_query.Replace("@pIndex", _index_1);
            _sql_query += "  UNION" + Environment.NewLine;
            _sql_query += _sb_select_from.ToString() + _sb_where_2.ToString();
            _sql_query = _sql_query.Replace("@pIndex", _index_2);
          }
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _sql_cmd.Parameters.Add("@pNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _sql_cmd.Parameters.Add("@pRE", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE;
          _sql_cmd.Parameters.Add("@pREinKind", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pStatusRedeemed", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.REDEEMED;
          _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _sql_cmd.Parameters.Add("@pPoints", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.POINT;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(Promotions);
          }
        }

        foreach (DataRow _promo in Promotions.Rows)
        {
          _promo_balance = (Decimal)_promo[2];
          _credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)_promo[3];

          switch (_credit_type)
          {
            case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
            case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
              LostBalance.Balance.PromoNotRedeemable += _promo_balance;
              break;

            case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
              LostBalance.Balance.PromoRedeemable += _promo_balance;
              break;

            case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:
              LostBalance.Balance.PromoRedeemable += _promo_balance;
              break;

            case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
              LostBalance.Points += _promo_balance;
              break;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetPromotionsToBeCancelledDueToRedeem

    public string[] VoucherAccountInfo(Boolean IncludeCardDocument = false)
    {
      String[] _voucher_account_info;
      String _level;
      String _name;
      String _document;
      Int32 _idx;
      Int32 _num_elems;

      _level = "";
      _name = "";
      _document = "";
      _num_elems = 3;

      if (PlayerTracking.HolderName != "")
      {
        _name = PlayerTracking.HolderName;
        _level = " - " + LoyaltyProgram.LevelName(PlayerTracking.CardLevel);
        if (IncludeCardDocument)
        {
          _document = IdentificationTypes.DocIdTypeString(PlayerTracking.HolderIdType) + ": " + PlayerTracking.HolderId;
          _num_elems++;
        }
      }
      else
      {
        _name = GeneralParam.GetString("Cashier.Voucher", "AnonymousAccount.Description");
      }

      _voucher_account_info = new String[_num_elems];

      _idx = 0;
      // _name: _idx = 0
      _voucher_account_info[_idx] = Resource.String("STR_VOUCHER_CARD_DRAW_HOLDER_NAME")
                            + ": " + _name + "\n";
      _idx++;

      // _document: if (IncludeCardDocument) _idx++
      if (!String.IsNullOrEmpty(_document))
      {
        _voucher_account_info[_idx] = _document;
        _idx++;
      }

      // 02-DEC-2013 DHA   AccountId and TrackData is hidden in voucher for virtual accounts.
      if (IsVirtualCard)
      {
        _voucher_account_info[_idx] = "";
        _idx++;

        _voucher_account_info[_idx] = "";
        _idx++;
      }
      else
      {
        _voucher_account_info[_idx] = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID")
                            + ": " + AccountId.ToString("000000") + _level + "\n";
        _idx++;
        _voucher_account_info[_idx] = Resource.String("STR_VOUCHER_CARD_CARD_ID")
                              + ": " + VisibleTrackdata() + "\n";
        _idx++;
      }

      return _voucher_account_info;
    } // VoucherAccountInfo



    //------------------------------------------------------------------------------
    // PURPOSE: Calculate VoucherAccountInfo with new TrackData
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - NewTrackData
    //
    //
    // RETURNS: 
    //      - VoucherAccountInfo: VoucherAccountInfo calculed with the new TrackData.
    // 
    public string[] VoucherAccountInfo(String TrackDataForShow)
    {
      string[] _voucher_account_info;
      string _current_trackdata;

      _current_trackdata = this.TrackData;
      this.TrackData = TrackDataForShow;

      _voucher_account_info = this.VoucherAccountInfo();
      this.TrackData = _current_trackdata;

      return _voucher_account_info;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate if Card deposit can be redeemed and the amount to be redeemed.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - CardData
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - MustRedeem: Card deposit can be redeemed only for cards that have been paid and are anonym.
    //
    // RETURNS: 
    //      - Currency: The amount that user paid for the card, if card is refundable.
    // 
    public static Currency CardDepositToRedeem(CardData CardData, out Boolean IsDeposit)
    {
      Currency _card_deposit_to_redeem;
      CardSettings CardSetting = new CardSettings();

      _card_deposit_to_redeem = 0;
      IsDeposit = false;

      // TJG 01-MAR-2010
      // RCI 24-SEP-2010
      // Only Cards that have been paid AND are Anonym can be refundable
      if (CardData.CardPaid && CardData.PlayerTracking.HolderName == "" && !CardData.IsVirtualCard)
      {
        // ACC 16-FEB-2010 Future DepositAnonymous and DepositNonAnonymous Flag in general parameters
        DB_CardPriceSettings(ref CardSetting);

        switch (CardSetting.AnonymCardRefundable)
        {
          case 0: // ANONYMOUS_CARD_NON_REFUNDABLE
            _card_deposit_to_redeem = 0;
            IsDeposit = true;
            break;

          case 1: // ANONYMOUS_CARD_REFUNDABLE
            _card_deposit_to_redeem = CardData.Deposit;
            IsDeposit = true;
            break;

          case 2: // ANONYMOUS_CARD_PAID
          default:
            _card_deposit_to_redeem = 0;
            IsDeposit = false;
            break;
        }
      }

      return _card_deposit_to_redeem;
    } // CardDepositToRedeem

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieves the card price settings from the general params table
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Trx
    //
    //      - OUTPUT:
    //          - CardSettings
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //
    public static Boolean DB_CardPriceSettings(ref CardSettings CardSetting)
    {
      CardSetting.AnonymCardPrice = GeneralParam.GetCurrency("Cashier", "CardPrice");
      CardSetting.AnonymCardRefundable = GeneralParam.GetInt32("Cashier", "CardRefundable", 2, 0, 2); // Default: ANONYMOUS_CARD_PAID
      CardSetting.PersonalCardPrice = GeneralParam.GetCurrency("Cashier", "PersonalCardPrice");
      CardSetting.PersonalCardReplacementPrice = GeneralParam.GetCurrency("Cashier", "PersonalCardReplacementPrice");
      CardSetting.PersonalCardReplacementPriceInPoints = GeneralParam.GetInt32("Cashier", "PersonalCardReplacement.PriceInPoints");
      CardSetting.PersonalCardReplacementChargeAfterTimes = GeneralParam.GetInt32("Cashier", "PersonalCardReplacement.ChargeAfterTimes");

      return true;
    } // DB_CardPriceSettings

    //------------------------------------------------------------------------------
    // PURPOSE: Update card info: Pin.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Account
    //          - Pin (ignored if RandomPIN is true)
    //          - OldPin
    //          - RandomPIN
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: data was updated successfully
    //      - false: data was not updated
    // 
    //   NOTES:
    public static Boolean DB_UpdatePinCard(CardData Card, CashierSessionInfo CashierInfo, String Pin, String OldPin, Boolean RandomPIN,
                                           out ArrayList VoucherList, out Int64 OperationId, SqlTransaction Trx)
    {
      AccountMovementsTable _account_movement;
      StringBuilder _sb;
      Int32 _num_rows_updated;
      CardData _card_data;
      TYPE_SPLITS _splits;
      Random _random;
      String _pin;
      Boolean _is_multisite;
      Int64 _operation_id;

      _card_data = Card;
      _num_rows_updated = 0;
      OperationId = 0;
      VoucherList = null;
      _sb = new StringBuilder();
      _is_multisite = GeneralParam.GetBoolean("MultiSite", "IsCenter");

      if (RandomPIN)
      {
        _random = new Random(DateTime.Now.Millisecond);

        _pin = "0000" + _random.Next(10000).ToString();
        _pin = _pin.Substring(_pin.Length - 4);
      }
      else
      {
        _pin = Pin;
      }

      try
      {
        _sb.AppendLine(" UPDATE   ACCOUNTS                     ");
        _sb.AppendLine("    SET   AC_PIN         = @pPin       ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID  = @pAccountId ");

        if (!String.IsNullOrEmpty(OldPin))
        {
          _sb.AppendLine(" AND   AC_PIN         = @pOldPin");
        }
        else
        {
          _sb.AppendLine(" AND   AC_PIN         IS NULL");
        }

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pPin", SqlDbType.NVarChar, 12).Value = _pin;
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Card.AccountId;
          if (!String.IsNullOrEmpty(OldPin))
          {
            _sql_command.Parameters.Add("@pOldPin", SqlDbType.NVarChar, 12).Value = OldPin;
          }

          _num_rows_updated = _sql_command.ExecuteNonQuery();
        }

        if (_num_rows_updated != 1)
        {
          Log.Error("DB_UpdatePinCard. Card not updated. Card: " + Card.AccountId);

          return false;
        }

        // If is multisite, It doesn't insert the operation
        _operation_id = 0;
        if (!_is_multisite)
        {
          if (!Operations.DB_InsertOperation(RandomPIN ? OperationCode.ACCOUNT_PIN_RANDOM : OperationCode.ACCOUNT_PIN_CHANGED,
                                             Card.AccountId, CashierInfo.CashierSessionId,
                                             0, 0, 0, 0, 0, 0, 0, string.Empty, out OperationId, Trx))
          {
            Log.Error("DB_UpdatePinCard. Card not updated. Card: " + Card.AccountId);

            return false;
          }

          _operation_id = OperationId;
        }

        _account_movement = new AccountMovementsTable(CashierInfo);

        if (!_account_movement.Add(OperationId, Card.AccountId, RandomPIN ? MovementType.AccountPINRandom : MovementType.AccountPINChange, Card.CurrentBalance, 0, 0, Card.CurrentBalance))
        {
          Log.Error("DB_UpdatePinCard. Card not updated. Card: " + Card.AccountId);

          return false;
        }

        if (!_account_movement.Save(Trx))
        {
          Log.Error("DB_UpdatePinCard. Card not updated. Card: " + Card.AccountId);

          return false;
        }

        // Voucher generated only when PIN is randomly generated
        if (RandomPIN)
        {
          Split.ReadSplitParameters(out _splits);

          VoucherList = VoucherBuilder.GeneratePIN(_card_data.VoucherAccountInfo(),
                                                     Card.AccountId,
                                                     _splits,
                                                     OperationId,
                                                     _pin,
                                                     PrintMode.Print,
                                                     Trx);
          if (VoucherList.Count == 0)
          {
            Log.Error("DB_UpdatePinCard. Card not updated. Card: " + Card.AccountId);

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // DB_UpdatePinCard



    public static Boolean DB_UpdateDataCard(CardData Account, CashierSessionInfo CashierInfo, Boolean SaveDocuments, out Int64 OperationId, SqlTransaction Trx)
    {
      AccountMovementsTable _account_movement;
      StringBuilder _sb;
      Int32 _num_rows_updated;
      SqlParameter _balance;
      Currency _current_balance;
      Boolean _is_multisite;
      Int64 _operation_id;
      DocumentList _images_list;

      _num_rows_updated = 0;
      _sb = new StringBuilder();
      OperationId = 0;
      _is_multisite = false;
      _images_list = new DocumentList();

      try
      {
        FillHolderDocumentFields(Account.PlayerTracking);

        //
        // If is multisite, It doesn't insert the operation and the movement 
        //
        _is_multisite = GeneralParam.GetBoolean("MultiSite", "IsCenter");

        _sb.AppendLine(" SELECT   @pBalance     = AC_BALANCE                        ");
        _sb.AppendLine("   FROM   ACCOUNTS                                          ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ;                     ");

        _sb.AppendLine(" UPDATE   ACCOUNTS                                          ");
        _sb.AppendLine("    SET   AC_HOLDER_NAME = @pFullName                       ");
        _sb.AppendLine("     , AC_HOLDER_ID = @pDocId                               ");
        _sb.AppendLine("     , AC_HOLDER_ADDRESS_01 = @pAddress01                   ");
        _sb.AppendLine("     , AC_HOLDER_ADDRESS_02 = @pAddress02                   ");
        _sb.AppendLine("     , AC_HOLDER_ADDRESS_03 = @pAddress03                   ");
        _sb.AppendLine("     , AC_HOLDER_CITY = @pCity                              ");
        _sb.AppendLine("     , AC_HOLDER_ZIP = @pZip                                ");
        _sb.AppendLine("     , AC_HOLDER_EMAIL_01 = @pEmail01                       ");
        _sb.AppendLine("     , AC_HOLDER_EMAIL_02 = @pEmail02                       ");
        _sb.AppendLine("     , AC_HOLDER_TWITTER_ACCOUNT = @pTwitter                "); //LEM 28-MAR-2013
        _sb.AppendLine("     , AC_HOLDER_PHONE_NUMBER_01 = @pPhone01                ");
        _sb.AppendLine("     , AC_HOLDER_PHONE_NUMBER_02 = @pPhone02                ");
        _sb.AppendLine("     , AC_HOLDER_COMMENTS = @pComments                      ");
        _sb.AppendLine("     , AC_HOLDER_GENDER = @pGender                          ");
        _sb.AppendLine("     , AC_HOLDER_MARITAL_STATUS = @pMarital                 ");
        _sb.AppendLine("     , AC_HOLDER_LEVEL = CASE WHEN (AC_HOLDER_LEVEL >= 2) THEN AC_HOLDER_LEVEL ELSE 1 END ");
        _sb.AppendLine("     , AC_HOLDER_LEVEL_ENTERED = CASE WHEN (AC_HOLDER_LEVEL >= 1) THEN AC_HOLDER_LEVEL_ENTERED ELSE GETDATE() END ");
        _sb.AppendLine("     , AC_HOLDER_BIRTH_DATE = @pBirthDate                   ");
        _sb.AppendLine("     , AC_USER_TYPE    = @pUserType                         ");
        _sb.AppendLine("     , AC_HOLDER_NAME1   = @pHolderName1                    ");
        _sb.AppendLine("     , AC_HOLDER_NAME2   = @pHolderName2                    ");
        _sb.AppendLine("     , AC_HOLDER_NAME3   = @pHolderName3                    ");
        _sb.AppendLine("     , AC_HOLDER_NAME4   = @pHolderName4                    ");
        _sb.AppendLine("     , AC_HOLDER_ID_TYPE = @pHolderIdType                   ");
        _sb.AppendLine("     , AC_HOLDER_ID1     = @pHolderId1                      ");
        _sb.AppendLine("     , AC_HOLDER_ID2     = @pHolderId2                      ");
        _sb.AppendLine("     , AC_POINTS_STATUS  = CASE WHEN (AC_POINTS_STATUS IS NULL) THEN 0 ELSE AC_POINTS_STATUS END "); // RRB 08-FEB-2013
        _sb.AppendLine("     , AC_HOLDER_WEDDING_DATE       = @pWeddingDate             ");
        _sb.AppendLine("     , AC_HOLDER_EXT_NUM            = @pHolderExtNum            ");
        _sb.AppendLine("     , AC_HOLDER_NATIONALITY        = @pHolderNationality       ");
        _sb.AppendLine("     , AC_HOLDER_BIRTH_COUNTRY      = @pHolderBirthCountry      ");
        _sb.AppendLine("     , AC_HOLDER_FED_ENTITY         = @pHolderFedEntity         ");
        _sb.AppendLine("     , AC_HOLDER_ADDRESS_COUNTRY    = @pHolderAddressCountry    ");
        _sb.AppendLine("     , AC_HOLDER_ADDRESS_VALIDATION = @pHolderAddressValidation ");
        _sb.AppendLine("     , AC_HOLDER_ID1_TYPE           = @pHolderId1Type           ");
        _sb.AppendLine("     , AC_HOLDER_ID2_TYPE           = @pHolderId2Type           ");
        _sb.AppendLine("     , AC_HOLDER_ID3_TYPE           = @pHolderId3Type           ");
        _sb.AppendLine("     , AC_HOLDER_ID3                = @pHolderId3               ");
        _sb.AppendLine("     , AC_HOLDER_HAS_BENEFICIARY    = @pHolderHasBeneficiary    ");
        _sb.AppendLine("     , AC_HOLDER_OCCUPATION_ID      = @pHolderOccupationId      ");
        _sb.AppendLine("     , AC_SHOW_COMMENTS_ON_CASHIER  = @pShowCommentsOnCashier   ");

        _sb.AppendLine("     , AC_BENEFICIARY_NAME         = @pFullBeneficiaryName     ");
        _sb.AppendLine("     , AC_BENEFICIARY_NAME1        = @pBeneficiaryName1        ");
        _sb.AppendLine("     , AC_BENEFICIARY_NAME2        = @pBeneficiaryName2        ");
        _sb.AppendLine("     , AC_BENEFICIARY_NAME3        = @pBeneficiaryName3        ");
        _sb.AppendLine("     , AC_BENEFICIARY_BIRTH_DATE   = @pBeneficiaryBirthDate    ");
        _sb.AppendLine("     , AC_BENEFICIARY_GENDER       = @pBeneficiaryGender       ");
        _sb.AppendLine("     , AC_BENEFICIARY_OCCUPATION_ID = @pBeneficiaryOccupationId ");
        _sb.AppendLine("     , AC_BENEFICIARY_ID1_TYPE     = @pBeneficiaryId1Type      ");
        _sb.AppendLine("     , AC_BENEFICIARY_ID1          = @pBeneficiaryId1          ");
        _sb.AppendLine("     , AC_BENEFICIARY_ID2_TYPE     = @pBeneficiaryId2Type      ");
        _sb.AppendLine("     , AC_BENEFICIARY_ID2          = @pBeneficiaryId2          ");
        _sb.AppendLine("     , AC_BENEFICIARY_ID3_TYPE     = @pBeneficiaryId3Type      ");
        _sb.AppendLine("     , AC_BENEFICIARY_ID3          = @pBeneficiaryId3          ");

        _sb.AppendLine("     , AC_HOLDER_IS_VIP          = @pHolderIsVIP          ");

        _sb.AppendLine(" WHERE AC_ACCOUNT_ID   = @pAccountId                        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pFullName", SqlDbType.NVarChar, 200).Value = Account.PlayerTracking.HolderName.ToUpperInvariant();
          _cmd.Parameters.Add("@pDocId", SqlDbType.NVarChar, 20).Value = Account.PlayerTracking.HolderId.ToUpperInvariant();
          _cmd.Parameters.Add("@pAddress01", SqlDbType.NVarChar, 50).Value = Account.PlayerTracking.HolderAddress01;
          _cmd.Parameters.Add("@pAddress02", SqlDbType.NVarChar, 100).Value = Account.PlayerTracking.HolderAddress02;
          _cmd.Parameters.Add("@pAddress03", SqlDbType.NVarChar, 50).Value = Account.PlayerTracking.HolderAddress03;
          _cmd.Parameters.Add("@pHolderIsVIP", SqlDbType.Bit, 1).Value = Account.PlayerTracking.HolderIsVIP;

          if (String.IsNullOrEmpty(Account.PlayerTracking.HolderCity))
          {
            _cmd.Parameters.Add("@pCity", SqlDbType.NVarChar, 50).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pCity", SqlDbType.NVarChar, 50).Value = Account.PlayerTracking.HolderCity;
          }

          _cmd.Parameters.Add("@pZip", SqlDbType.NVarChar, 10).Value = Account.PlayerTracking.HolderZip;
          _cmd.Parameters.Add("@pEmail01", SqlDbType.NVarChar, 50).Value = IfNullDBNull(Account.PlayerTracking.HolderEmail01);
          _cmd.Parameters.Add("@pEmail02", SqlDbType.NVarChar, 50).Value = IfNullDBNull(Account.PlayerTracking.HolderEmail02);
          _cmd.Parameters.Add("@pTwitter", SqlDbType.NVarChar, 50).Value = IfNullDBNull(Account.PlayerTracking.HolderTwitter);
          _cmd.Parameters.Add("@pPhone01", SqlDbType.NVarChar, 20).Value = IfNullDBNull(Account.PlayerTracking.HolderPhone01);
          _cmd.Parameters.Add("@pPhone02", SqlDbType.NVarChar, 20).Value = IfNullDBNull(Account.PlayerTracking.HolderPhone02);
          _cmd.Parameters.Add("@pComments", SqlDbType.NVarChar, 100).Value = Account.PlayerTracking.HolderComments;
          _cmd.Parameters.Add("@pGender", SqlDbType.Int).Value = Account.PlayerTracking.HolderGender;
          _cmd.Parameters.Add("@pMarital", SqlDbType.Int).Value = Account.PlayerTracking.HolderMaritalStatus;
          if (Account.PlayerTracking.HolderBirthDate == DateTime.MinValue)
          {
            _cmd.Parameters.Add("@pBirthDate", SqlDbType.DateTime).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pBirthDate", SqlDbType.DateTime).Value = Account.PlayerTracking.HolderBirthDate;
          }
          _cmd.Parameters.Add("@pUserType", SqlDbType.Int).Value = ACCOUNT_USER_TYPE.PERSONAL;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Account.AccountId;
          _cmd.Parameters.Add("@pHolderName1", SqlDbType.NVarChar, 50).Value = Account.PlayerTracking.HolderName1.ToUpperInvariant();
          _cmd.Parameters.Add("@pHolderName2", SqlDbType.NVarChar, 50).Value = Account.PlayerTracking.HolderName2.ToUpperInvariant();
          _cmd.Parameters.Add("@pHolderName3", SqlDbType.NVarChar, 50).Value = Account.PlayerTracking.HolderName3.ToUpperInvariant();
          _cmd.Parameters.Add("@pHolderName4", SqlDbType.NVarChar, 50).Value = Account.PlayerTracking.HolderName4.ToUpperInvariant();
          _cmd.Parameters.Add("@pHolderIdType", SqlDbType.Int).Value = Account.PlayerTracking.HolderIdType;
          _cmd.Parameters.Add("@pHolderOccupationId", SqlDbType.Int).Value = Account.PlayerTracking.HolderOccupationId;
          _cmd.Parameters.Add("@pHolderExtNum", SqlDbType.NVarChar, 10).Value = Account.PlayerTracking.HolderExtNumber;
          _cmd.Parameters.Add("@pHolderNationality", SqlDbType.Int).Value = Account.PlayerTracking.HolderNationality;
          _cmd.Parameters.Add("@pHolderBirthCountry", SqlDbType.Int).Value = Account.PlayerTracking.HolderBirthCountry;
          _cmd.Parameters.Add("@pHolderFedEntity", SqlDbType.Int).Value = Account.PlayerTracking.HolderFedEntity;
          _cmd.Parameters.Add("@pHolderAddressCountry", SqlDbType.Int).Value = Account.PlayerTracking.HolderAddressCountry;
          _cmd.Parameters.Add("@pHolderAddressValidation", SqlDbType.Int).Value = Account.PlayerTracking.HolderAddressValidation;
          _cmd.Parameters.Add("@pHolderId1Type", SqlDbType.Int).Value = Account.PlayerTracking.HolderId1Type;
          _cmd.Parameters.Add("@pHolderId2Type", SqlDbType.Int).Value = Account.PlayerTracking.HolderId2Type;
          _cmd.Parameters.Add("@pHolderHasBeneficiary", SqlDbType.Bit).Value = Account.PlayerTracking.HolderHasBeneficiary;
          _cmd.Parameters.Add("@pShowCommentsOnCashier", SqlDbType.Bit).Value = Account.PlayerTracking.ShowCommentsOnCashier;

          _cmd.Parameters.Add("@pFullBeneficiaryName", SqlDbType.NVarChar, 200).Value = IfEmptyStringDBNull((Account.PlayerTracking.BeneficiaryName ?? "").ToUpperInvariant());
          _cmd.Parameters.Add("@pBeneficiaryName1", SqlDbType.NVarChar, 50).Value = IfEmptyStringDBNull((Account.PlayerTracking.BeneficiaryName1 ?? "").ToUpperInvariant());
          _cmd.Parameters.Add("@pBeneficiaryName2", SqlDbType.NVarChar, 50).Value = IfEmptyStringDBNull((Account.PlayerTracking.BeneficiaryName2 ?? "").ToUpperInvariant());
          _cmd.Parameters.Add("@pBeneficiaryName3", SqlDbType.NVarChar, 50).Value = IfEmptyStringDBNull((Account.PlayerTracking.BeneficiaryName3 ?? "").ToUpperInvariant());
          _cmd.Parameters.Add("@pBeneficiaryOccupationId", SqlDbType.Int).Value = Account.PlayerTracking.BeneficiaryOccupationId;
          _cmd.Parameters.Add("@pBeneficiaryId1Type", SqlDbType.Int).Value = Account.PlayerTracking.BeneficiaryId1Type;
          _cmd.Parameters.Add("@pBeneficiaryId2Type", SqlDbType.Int).Value = Account.PlayerTracking.BeneficiaryId2Type;

          if (String.IsNullOrEmpty(Account.PlayerTracking.BeneficiaryId3Type))
          {
            _cmd.Parameters.Add("@pBeneficiaryId3Type", SqlDbType.NVarChar, 50).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pBeneficiaryId3Type", SqlDbType.NVarChar, 50).Value = IfEmptyStringDBNull(Account.PlayerTracking.BeneficiaryId3Type);

          }

          if (Account.PlayerTracking.BeneficiaryId1 == null)
          {
            _cmd.Parameters.Add("@pBeneficiaryId1", SqlDbType.NVarChar, 20).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pBeneficiaryId1", SqlDbType.NVarChar, 20).Value = IfEmptyStringDBNull(Account.PlayerTracking.BeneficiaryId1.ToUpperInvariant());
          }

          if (Account.PlayerTracking.BeneficiaryId2 == null)
          {
            _cmd.Parameters.Add("@pBeneficiaryId2", SqlDbType.NVarChar, 20).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pBeneficiaryId2", SqlDbType.NVarChar, 20).Value = IfEmptyStringDBNull(Account.PlayerTracking.BeneficiaryId2.ToUpperInvariant());
          }

          if (Account.PlayerTracking.BeneficiaryId3 == null)
          {
            _cmd.Parameters.Add("@pBeneficiaryId3", SqlDbType.NVarChar, 20).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pBeneficiaryId3", SqlDbType.NVarChar, 20).Value = IfEmptyStringDBNull(Account.PlayerTracking.BeneficiaryId3.ToUpperInvariant());
          }

          if (DateTime.MinValue.Equals(Account.PlayerTracking.BeneficiaryBirthDate))
          {
            _cmd.Parameters.Add("@pBeneficiaryBirthDate", SqlDbType.DateTime).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pBeneficiaryBirthDate", SqlDbType.DateTime).Value = Account.PlayerTracking.BeneficiaryBirthDate;
          }

          if (Account.PlayerTracking.BeneficiaryGender == -1)
          {
            _cmd.Parameters.Add("@pBeneficiaryGender", SqlDbType.Int).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pBeneficiaryGender", SqlDbType.Int).Value = Account.PlayerTracking.BeneficiaryGender;
          }

          if (String.IsNullOrEmpty(Account.PlayerTracking.HolderId3Type))
          {
            _cmd.Parameters.Add("@pHolderId3Type", SqlDbType.NVarChar, 50).Value = DBNull.Value;

          }
          else
          {
            _cmd.Parameters.Add("@pHolderId3Type", SqlDbType.NVarChar, 50).Value = IfEmptyStringDBNull(Account.PlayerTracking.HolderId3Type);

          }

          if (Account.PlayerTracking.HolderId1 == null)
          {
            _cmd.Parameters.Add("@pHolderId1", SqlDbType.NVarChar, 20).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pHolderId1", SqlDbType.NVarChar, 20).Value = Account.PlayerTracking.HolderId1.ToUpperInvariant();
          }
          if (Account.PlayerTracking.HolderId2 == null)
          {
            _cmd.Parameters.Add("@pHolderId2", SqlDbType.NVarChar, 20).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pHolderId2", SqlDbType.NVarChar, 20).Value = IfEmptyStringDBNull(Account.PlayerTracking.HolderId2.ToUpperInvariant());
          }

          if (Account.PlayerTracking.HolderId3 == null)
          {
            _cmd.Parameters.Add("@pHolderId3", SqlDbType.NVarChar, 20).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pHolderId3", SqlDbType.NVarChar, 20).Value = IfEmptyStringDBNull(Account.PlayerTracking.HolderId3.ToUpperInvariant());
          }

          if (DateTime.MinValue.Equals(Account.PlayerTracking.HolderWeddingDate))
          {
            _cmd.Parameters.Add("@pWeddingDate", SqlDbType.DateTime).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pWeddingDate", SqlDbType.DateTime).Value = Account.PlayerTracking.HolderWeddingDate;
          }

          _balance = _cmd.Parameters.Add("@pBalance", SqlDbType.Money);
          _balance.Direction = ParameterDirection.Output;

          _num_rows_updated = _cmd.ExecuteNonQuery();
        }

        if (_num_rows_updated != 1)
        {
          Log.Error("DB_UpdateDataCard. Card not updated. Card: " + Account.AccountId);

          return false;
        }

        if (SaveDocuments)
        {
          //Save Holder Scanned Documents
          if (Account.PlayerTracking.HolderScannedDocs != null)
          {
            foreach (IDocument _document in Account.PlayerTracking.HolderScannedDocs)
            {
              _images_list.Add(_document);
          }
          }

          if (Account.PlayerTracking.HolderHasBeneficiary)
          {
            //Save Beneficiary Scanned Documents
            if (Account.PlayerTracking.BeneficiaryScannedDocs != null)
            {
              foreach (IDocument _document in Account.PlayerTracking.BeneficiaryScannedDocs)
              {
                _images_list.Add(_document);
            }
          }
          }

          if (!AccountDocuments.Save(Account.AccountId, _images_list, Trx))
          {
            Log.Error("AccountDocuments.Save. Card not updated. Card: " + Account.AccountId);

            return false;
          }
        }

        // If is multisite, It doesn't insert the operation
        _operation_id = 0;
        if (!_is_multisite)
        {
          if (!Operations.DB_InsertOperation(OperationCode.ACCOUNT_PERSONALIZATION, Account.AccountId,
                                             CashierInfo.CashierSessionId, 0, 0, 0, 0, 0, 0, string.Empty, out OperationId, Trx))
          {
            Log.Error("DB_UpdateDataCard. Card not updated. Card: " + Account.AccountId);

            return false;
          }

          _operation_id = OperationId;
        }

        _current_balance = _balance.Value == null ? 0 : (SqlMoney)_balance.SqlValue;

        _account_movement = new AccountMovementsTable(CashierInfo);

        if (!_account_movement.Add(OperationId, Account.AccountId, MovementType.AccountPersonalization, _current_balance, 0, 0, _current_balance))
        {
          Log.Error("DB_UpdateDataCard. Card not updated. Card: " + Account.AccountId);

          return false;
        }
        if (!_account_movement.Save(Trx))
        {
          Log.Error("DB_UpdateDataCard. Card not updated. Card: " + Account.AccountId);

          return false;
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }
    private static object IfNullDBNull(object o)
    {
      if (o == null)
        return DBNull.Value;
      return o;
    }

    private static object IfEmptyStringDBNull(String o)
    {
      if (o == "")
        return DBNull.Value;
      return o;
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Get value for HolderAddressValidationValue
    //  PARAMS :
    //
    //      - INPUT :
    //            - AddressOriginal
    //            - AddressModified
    //            - ManualValidation
    //            - ValidationOriginal
    // RETURNS :
    //      - ADDRESS_VALIDATION
    //   NOTES :
    //
    public static ADDRESS_VALIDATION GetHolderAddressValidationValue(Zip.Address AddressOriginal, Zip.Address AddressModified, Boolean ValidationSkipped,
                                                                     ADDRESS_VALIDATION ValidationOriginal, Int32 AddressCountry)
    {
      if (!Zip.ValidationCountryDefault(AddressCountry))
      {
        return ADDRESS_VALIDATION.VALIDATION_SKIPPED;
      }

      if (ValidationOriginal == ADDRESS_VALIDATION.MANUAL_ENTRY)
      {
        return (ValidationSkipped ? ADDRESS_VALIDATION.VALIDATION_SKIPPED : ADDRESS_VALIDATION.VALIDATED);
      }

      if (AddressOriginal.Equals(AddressModified))
      {
        return (ValidationSkipped ? ValidationOriginal : ADDRESS_VALIDATION.VALIDATED);
      }
      else
      {
        return (ValidationSkipped ? ADDRESS_VALIDATION.VALIDATION_SKIPPED : ADDRESS_VALIDATION.VALIDATED);
      }

    } // GetHolderAddressValidationValue

    //------------------------------------------------------------------------------
    // PURPOSE : Get String for HolderAddressValidationValue
    //  PARAMS :
    //
    //      - INPUT :
    //            - AddressValidation
    // RETURNS :
    //      - String
    //   NOTES :
    //
    public static String GetHolderAddressValidationString(ADDRESS_VALIDATION AddressValidation)
    {
      switch (AddressValidation)
      {
        case ADDRESS_VALIDATION.MANUAL_ENTRY:
          return Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MANUAL_ENTRY");
        case ADDRESS_VALIDATION.VALIDATED:
          return Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MANUAL_ENTRY_VALIDATION");
        case ADDRESS_VALIDATION.VALIDATION_SKIPPED:
          return Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MANUAL_ENTRY_NON_VALIDATION");
        default:
          return Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MANUAL_ENTRY");
      }
    } // GetHolderAddressValidationString

    //------------------------------------------------------------------------------
    // PURPOSE : Fill Holder Documents in apropiate PlayerTracking members.
    //            If RFC is set into HolderId will also set in HolderId1
    //            If CURP is set into HolderID will also set in HolderId2
    //            If RFC is set into HolderId1 and HolderId is empty, also set in HolderId
    //            If CURP is set into HolderId1 and HolderId is empty, also set in HolderId
    //  PARAMS :
    //      - INPUT : PlayerTrackingData Account
    // RETURNS :
    //   NOTES :
    //    Encapsulated code to allow call by Player_Edit in WigosGUI.
    public static void FillHolderDocumentFields(PlayerTrackingData PlayerTrackingData)
    {

      Boolean _is_doc_id_filled;
      Dictionary<ACCOUNT_HOLDER_ID_TYPE, String> _docs_holder_mapping;

      _is_doc_id_filled = false;
      _docs_holder_mapping = new Dictionary<ACCOUNT_HOLDER_ID_TYPE, String>();

      if (!String.IsNullOrEmpty(PlayerTrackingData.HolderId))
      {
        _docs_holder_mapping.Add(PlayerTrackingData.HolderIdType, PlayerTrackingData.HolderId);
      }
      if (!String.IsNullOrEmpty(PlayerTrackingData.HolderId1))
      {
        if (!_docs_holder_mapping.ContainsKey(PlayerTrackingData.HolderId1Type))
        {
          _docs_holder_mapping.Add(PlayerTrackingData.HolderId1Type, PlayerTrackingData.HolderId1);
        }
      }
      if (!String.IsNullOrEmpty(PlayerTrackingData.HolderId2))
      {
        if (!_docs_holder_mapping.ContainsKey(PlayerTrackingData.HolderId2Type))
        {
          _docs_holder_mapping.Add(PlayerTrackingData.HolderId2Type, PlayerTrackingData.HolderId2);
        }
      }

      foreach (KeyValuePair<ACCOUNT_HOLDER_ID_TYPE, String> _doc in _docs_holder_mapping)
      {
        switch (_doc.Key)
        {
          case ACCOUNT_HOLDER_ID_TYPE.RFC:
          case ACCOUNT_HOLDER_ID_TYPE.CURP:
            if (!_is_doc_id_filled)
            {
              PlayerTrackingData.HolderId = _doc.Value;
              PlayerTrackingData.HolderIdType = _doc.Key;
              _is_doc_id_filled = true;
            }
            if (_doc.Key == ACCOUNT_HOLDER_ID_TYPE.RFC)
            {
              PlayerTrackingData.HolderId1 = _doc.Value;
              PlayerTrackingData.HolderId1Type = _doc.Key;
            }
            else if (_doc.Key == ACCOUNT_HOLDER_ID_TYPE.CURP)
            {
              PlayerTrackingData.HolderId2 = _doc.Value;
              PlayerTrackingData.HolderId2Type = _doc.Key;
            }
            break;

          default:
            PlayerTrackingData.HolderId = _doc.Value;
            PlayerTrackingData.HolderIdType = _doc.Key;
            _is_doc_id_filled = true;
            break;
        }
      }
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Retrieve all relevant data related to an account from the database
    //          using the account identifier.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId
    //
    //      - OUTPUT:
    //          - CardInfo
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //
    public static Boolean DB_CardGetAllData(Int64 AccountId,
                                            CardData CardInfo,
                                            Boolean GetAllData)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        SqlTransaction _sql_trx = _db_trx.SqlTransaction;

        if (DB_CardGetAllData(AccountId, CardInfo, GetAllData, _sql_trx))
        {
          _db_trx.Commit();

          return true;
        }
      }

      return false;
    } // DB_CardGetAllData

    public static Boolean DB_CardGetAllData(Int64 AccountId,
                                            CardData CardInfo)
    {
      return DB_CardGetAllData(AccountId, CardInfo, true);
    } // DB_CardGetAllData

    public static Boolean DB_CardGetAllData(Int64 AccountId,
                                            CardData CardInfo,
                                            SqlTransaction SqlTrx)
    {
      return DB_CardGetAllData(AccountId, CardInfo, true, SqlTrx);
    } // DB_CardGetAllData

    public static Boolean DB_CardGetAllData(Int64 AccountId,
                                            CardData CardInfo,
                                            Boolean GetAllData,
                                            SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Boolean _rc;
      Int64 _current_play_session_id;
      Int64 _last_play_session_id;
      Int32 _current_terminal_id;
      Int32 _last_terminal_id;
      String _current_terminal_name;
      AccountType _user_type;
      CreditLine _credit_line;

      String _last_terminal_name;

      String _internal_card_id;
      int _card_type;
      Int64 _sequence_id;

      Boolean _is_multisite;

      _is_multisite = false;
      _rc = true;
      _credit_line = new CreditLine();

      // Initialize card data
      CardInfo.AccountId = AccountId;
      CardInfo.PlayerTracking.HolderName = "";
      CardInfo.Blocked = false;
      CardInfo.BlockReason = AccountBlockReason.NONE;
      CardInfo.BlockDescription = "";       // JBP 07-MAY-2013
      CardInfo.NotValidBefore = WGDB.Now;
      CardInfo.NotValidAfter = WGDB.Now;
      CardInfo.CurrentBalance = 0;
      CardInfo.TrackData = "";
      CardInfo.MaxDevolution = 0;
      CardInfo.Deposit = 0;

      CardInfo.CardPaid = false;            // RCI 24-SEP-2010
      CardInfo.Cancellable = new CancellableData();
      CardInfo.NonRedeemableWonLock = 0;    // RCI 01-OCT-2010
      CardInfo.IsSiteJackpotWinner = false; // RCI 03-DEC-2010

      CardInfo.Timestamp = 0;

      CardInfo.PointsStatus = ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED; // QMP 06-FEB-2013


      // Logging data
      CardInfo.IsLoggedIn = false;
      CardInfo.LoggedInTerminalId = 0;
      CardInfo.LoggedInTerminalName = "";
      CardInfo.LoggedInTerminalProvider = "";
      CardInfo.LoggedInTerminalType = TerminalTypes.UNKNOWN;
      // Lock status
      CardInfo.IsLocked = false;

      // Account Balance
      CardInfo.AccountBalance = MultiPromos.AccountBalance.Zero;
      CardInfo.AccountBalance.ModeReserved = false;

      CardInfo.CardReplacementCount = 0;

      // Initialize variables
      _current_play_session_id = 0;
      _current_terminal_id = 0;
      _current_terminal_name = "";
      _last_play_session_id = 0;
      _last_terminal_id = 0;
      _last_terminal_name = "";

      try
      {
        //
        // If is multisite, It doesn't use play_session, account_operations and terminals
        //
        _is_multisite = GeneralParam.GetBoolean("MultiSite", "IsCenter", false);

        // Get card data
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   ISNULL(AC_TRACK_DATA, '') ");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_NAME, '') ");
        _sb.AppendLine("       , AC_BLOCKED ");
        _sb.AppendLine("       , AC_NOT_VALID_BEFORE ");
        _sb.AppendLine("       , AC_NOT_VALID_AFTER ");
        _sb.AppendLine("       , AC_BALANCE ");
        _sb.AppendLine("       , AC_INITIAL_CASH_IN ");
        _sb.AppendLine("       , AC_CURRENT_PLAY_SESSION_ID ");
        _sb.AppendLine("       , AC_LAST_PLAY_SESSION_ID ");
        _sb.AppendLine("       , AC_CURRENT_TERMINAL_ID ");
        _sb.AppendLine("       , ISNULL(AC_CURRENT_TERMINAL_NAME, '') ");
        _sb.AppendLine("       , AC_LAST_TERMINAL_ID ");
        _sb.AppendLine("       , ISNULL(AC_LAST_TERMINAL_NAME, '') ");
        _sb.AppendLine("       , AC_DEPOSIT ");
        if (_is_multisite)
        {
          _sb.AppendLine("       , 0 "); // LOCKED FALSE
        }
        else
        {
          _sb.AppendLine("       , PS_LOCKED ");
        }
        _sb.AppendLine("       , AC_CARD_PAID ");                  // RCI 24-SEP-2010
        _sb.AppendLine("       , AC_BLOCK_REASON ");               // RCI 22-OCT-2010
        _sb.AppendLine("       , CAST (ISNULL(AC_TIMESTAMP, 0) AS BIGINT) ");  // RCI & AJQ 24-FEB-2011
        _sb.AppendLine("       , AC_RE_BALANCE ");                 // JAB 18-JUN-2012
        _sb.AppendLine("       , AC_PROMO_RE_BALANCE ");           // JAB 18-JUN-2012
        _sb.AppendLine("       , AC_PROMO_NR_BALANCE ");           // JAB 18-JUN-2012
        _sb.AppendLine("       , AC_POINTS_STATUS ");              // QMP 06-FEB-2013
        _sb.AppendLine("       , AC_BLOCK_DESCRIPTION ");          // JBP 07-MAY-2013
        _sb.AppendLine("       , AC_TYPE ");
        _sb.AppendLine("       , AC_HOLDER_BIRTH_DATE ");          // LJM 08-JAN-2014
        _sb.AppendLine("       , AC_HOLDER_ID ");                  // LJM 08-JAN-2014
        _sb.AppendLine("       , ISNULL(AC_CARD_REPLACEMENT_COUNT,0) ");     // AMF 01-JUL-2014
        _sb.AppendLine("       , ISNULL (dbo.GetBucketValue(" + (Int32)Buckets.BucketId.Credit_NR + ", @p1), 0) AS BU_Credit_NR");
        _sb.AppendLine("       , ISNULL (dbo.GetBucketValue(" + (Int32)Buckets.BucketId.Credit_RE + ", @p1), 0) AS BU_Credit_RE");

        if (_is_multisite)
        {
          _sb.AppendLine("     , CAST(0 AS DECIMAL)");             // 04-NOV-2015 AVZ   Backlog Item 6145:BonoPLUS: Reserved credit
          _sb.AppendLine("     , CAST(0 AS BIT) ");                // 26-NOV-2015 AMF   Backlog Item 7079
          _sb.AppendLine("     , CAST(0 AS DECIMAL)");             // TerminalDraw 
          _sb.AppendLine("     , CAST(0 AS DECIMAL) ");            // AC_PROVISION
          _sb.AppendLine("     , CAST(0 AS DECIMAL) ");            // AC_SALES_CHIPS_ROUNDING_AMOUNT
          _sb.AppendLine(" FROM  ACCOUNTS ");
          
        }
        else
        {
          _sb.AppendLine("     , ISNULL(AC_RE_RESERVED,0) ");      // 04-NOV-2015 AVZ   Backlog Item 6145:BonoPLUS: Reserved credit
          _sb.AppendLine("     , AC_MODE_RESERVED ");              // 26-NOV-2015 AMF   Backlog Item 7079
          
          if (Misc.IsTerminalDrawEnabled())
          {
            _sb.AppendLine("     , ISNULL(PENDING_DRAWS.TERMINAL_DRAW_PENDING_AMOUNT, CAST(0 as DECIMAL))"); // TerminalDraw FIELD 31
          }
          else
          {
            _sb.AppendLine("     , CAST(0 as DECIMAL) "); // FIELD 31
          }
          _sb.AppendLine("     , AC_PROVISION ");                     // AC_PROVISION FIELD 32
          _sb.AppendLine("     , AC_SALES_CHIPS_ROUNDING_AMOUNT ");   // AC_SALES_CHIPS_ROUNDING_AMOUNT FIELD 33

          _sb.AppendLine(" FROM ACCOUNTS LEFT OUTER JOIN PLAY_SESSIONS ON AC_CURRENT_PLAY_SESSION_ID = PS_PLAY_SESSION_ID");

          if (Misc.IsTerminalDrawEnabled())
          {
            // TerminalDraw 

            _sb.AppendLine(" LEFT JOIN (                                                                                  ");
            _sb.AppendLine("            SELECT    TDR_ACCOUNT_ID                                                          ");
            _sb.AppendLine("                   ,  SUM (TDR_TOTAL_CASH_IN - TDR_RE_BET) AS TERMINAL_DRAW_PENDING_AMOUNT    ");
            _sb.AppendLine("              FROM    TERMINAL_DRAWS_RECHARGES                                                ");
            _sb.AppendLine("             WHERE    TDR_STATUS = @p2                                                        ");
            _sb.AppendLine("          GROUP BY    TDR_ACCOUNT_ID                                                          ");
            _sb.AppendLine(" ) AS PENDING_DRAWS                                                                           ");
            _sb.AppendLine("                ON    PENDING_DRAWS.TDR_ACCOUNT_ID = AC_ACCOUNT_ID                            "); 
          }
        }

        _sb.AppendLine(" WHERE AC_ACCOUNT_ID = @p1 ");

        try
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
        _sql_cmd.Parameters.Add("@p1", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = AccountId;
        _sql_cmd.Parameters.Add("@p2", SqlDbType.Int).Value = (Int32)TerminalDraw.TerminalDraw.StatusTerminalDraw.PENDING;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (_reader.Read() && !_reader.IsDBNull(0))
          {
                _internal_card_id = (String)_reader[0];
            _card_type = 0;

            //JCM 19-APR-2012
            if (_internal_card_id.Contains(CardData.RECYCLED_FLAG_TRACK_DATA))
            {
              CardInfo.IsRecycled = true;
              CardInfo.TrackData = CardData.RECYCLED_TRACK_DATA;
              CardInfo.SiteId = Convert.ToInt32(_internal_card_id.Substring(0, 4));
              _rc = true;
            }
            else
            {
              CardInfo.IsRecycled = false;
              // Obtain card site id.
              CardNumber.SplitInternalTrackData(_internal_card_id, out CardInfo.SiteId, out _sequence_id);
              _rc = CardNumber.TrackDataToExternal(out CardInfo.TrackData, _internal_card_id, _card_type);
            }

            if (_rc)
            {
              //CardInfo.TrackData = (String) reader[0];
                  CardInfo.PlayerTracking.HolderName = (String)_reader[1];
                  CardInfo.Blocked = (Boolean)_reader[2];
              if (_reader.IsDBNull(3))
              {
                CardInfo.NotValidBefore = WGDB.Now;
              }
              else
              {
                    CardInfo.NotValidBefore = (DateTime)_reader[3];
              }

              if (_reader.IsDBNull(4))
              {
                CardInfo.NotValidAfter = WGDB.Now;
              }
              else
              {
                    CardInfo.NotValidAfter = (DateTime)_reader[4];
              }

              CardInfo.CurrentBalance = _reader.GetSqlMoney(5);
              CardInfo.MaxDevolution = _reader.GetSqlMoney(6);

              if (_reader.IsDBNull(7))
              {
                _current_play_session_id = 0;
              }
              else
              {
                    _current_play_session_id = (Int64)_reader[7];
              }

              if (_reader.IsDBNull(8))
              {
                _last_play_session_id = 0;
              }
              else
              {
                    _last_play_session_id = (Int64)_reader[8];
              }

              if (_reader.IsDBNull(9))
              {
                _current_terminal_id = 0;
              }
              else
              {
                    _current_terminal_id = (Int32)_reader[9];
              }
                  _current_terminal_name = (String)_reader[10];
              if (_reader.IsDBNull(11))
              {
                _last_terminal_id = 0;
              }
              else
              {
                    _last_terminal_id = (Int32)_reader[11];
              }
                  _last_terminal_name = (String)_reader[12];
              CardInfo.Deposit = _reader.GetSqlMoney(13);

              // Detect if card is currently locked or not
              if (_reader.IsDBNull(14))
              {
                CardInfo.IsLocked = false;
              }
              else
              {
                CardInfo.IsLocked = true;
              }

              CardInfo.CardPaid = _reader.GetBoolean(15);
                  CardInfo.BlockReason = (AccountBlockReason)_reader.GetInt32(16);
              CardInfo.Timestamp = _reader.GetInt64(17);
              // 04-NOV-2015 AVZ   Backlog Item 6145:BonoPLUS: Reserved credit
              CardInfo.AccountBalance = new MultiPromos.AccountBalance(_reader.GetDecimal(18),
                                                                       _reader.GetDecimal(19),
                                                                       _reader.GetDecimal(20),
                                                                       _reader.GetDecimal(29),
                                                                       _reader.GetBoolean(30),
                                                                       _reader.GetDecimal(27),
                                                                       _reader.GetDecimal(28),
                                                                       _reader.GetDecimal(32),
                                                                       _reader.GetDecimal(33),
                                                                       GamingTableBusinessLogic.IsEnableRoundingSaleChips());

              // QMP 06-FEB-2013:  Account Points Status
              if (_reader.IsDBNull(21) || _reader.GetInt32(21) == 1)
              {
                CardInfo.PointsStatus = ACCOUNT_POINTS_STATUS.REDEEM_NOT_ALLOWED;
              }
              else
              {
                CardInfo.PointsStatus = ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED;
              }

              // JBP 07-MAY-2013:  Account Block description
                  CardInfo.BlockDescription = (_reader.IsDBNull(22)) ? "" : _reader.GetString(22);

                  _user_type = (_reader.IsDBNull(23)) ? AccountType.ACCOUNT_UNKNOWN : (AccountType)_reader.GetInt32(23);
              CardInfo.IsVirtualCard = (_user_type == AccountType.ACCOUNT_VIRTUAL_TERMINAL ||
                                        _user_type == AccountType.ACCOUNT_VIRTUAL_CASHIER);

              // LJM 08-JAN-2014: To avoid a second access to ACCOUNTS
              if (_reader.IsDBNull(24))
              {
                CardInfo.PlayerTracking.HolderBirthDate = DateTime.MinValue;
              }
              else
              {
                CardInfo.PlayerTracking.HolderBirthDate = _reader.GetDateTime(24);
              }
              if (_reader.IsDBNull(25))
              {
                CardInfo.PlayerTracking.HolderId = string.Empty;
              }
              else
              {
                CardInfo.PlayerTracking.HolderId = _reader.GetString(25);
              }
              // LJM

              // AMF 01-JUL-2014
              CardInfo.CardReplacementCount = _reader.GetInt32(26);

              // FJC 30-NOV-2015
              // Mode Reserved (GameGateway)
              CardInfo.AccountBalance.ModeReserved = _reader.GetBoolean(30);
              CardInfo.AccountBalance.PromoRedeemable -= CardInfo.AccountBalance.BucketRECredit;
              CardInfo.AccountBalance.PromoNotRedeemable -= CardInfo.AccountBalance.BucketNRCredit;
            }
          }
            }
          }
          _credit_line.DB_GetByAccountId(CardInfo.AccountId, SqlTrx);
          CardInfo.CreditLineData = _credit_line;
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        CardInfo.Cancellable.TotalBalance = MultiPromos.AccountBalance.Zero;
        CardInfo.Cancellable.TotalOperationAmount = 0;
        CardInfo.Cancellable.TotalCashInTaxAmount = 0;
        CardInfo.Cancellable.Operations.Clear();

        if (!_is_multisite)
        {
          // JAB 20-JUN-2012
          // LJM 07-ENE-2014: Enables/Disables uc_card button btn_promotion. Also changes it's text if needed.
          CalculatePromotionsAmount(AccountId, CardInfo, SqlTrx);
          // LJM

          // LJM 08-JAN-2014: Refactored into a method. This info is only necesary if we want all the data to be shown
          if (GetAllData)
          {
            GetCancelableAccountOperations(AccountId, CardInfo, SqlTrx);
          }
          // LJM
        }

        //
        // AJQ 09-NOV-2010, Compute the "Initial Cash In", summing up 'Cash-In' on the last 'N' hours
        // LJM 07-GEN-2014, This data is only calculated if we need to
        if (GetAllData && !TITO.Utils.IsTitoMode())
        {
          Accounts.GetMaxDevolution(AccountId, CardInfo.MaxDevolution, out CardInfo.MaxDevolution, SqlTrx);
          if (GamingTableBusinessLogic.IsGamingTablesEnabled() && FeatureChips.IsIntegratedChipAndCreditOperations(CardInfo.IsVirtualCard))
          {
            Accounts.GetMaxDevolution(AccountId, CardInfo.MaxDevolutionForChips, true, out CardInfo.MaxDevolutionForChips, SqlTrx);
          }
        }
        // LJM

        // 17-JAN-2014 JML   Fixed Bug WIG-521  Moved out of "if (GetAllData)"
        if (AccountId > 0)
        {
        CardData.DB_CardGetPersonalData(AccountId, CardInfo, SqlTrx);
        }

        if (_current_terminal_id > 0)
        {
          CardInfo.IsLoggedIn = true;
          CardInfo.LoggedInTerminalId = _current_terminal_id;
        }
        else if (_last_terminal_id > 0)
        {
          CardInfo.LoggedInTerminalId = _last_terminal_id;
        }

        if (!_is_multisite)
        {
          try
          {
          // Get Terminal data
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   TE_NAME ");
          _sb.AppendLine("       , TE_PROVIDER_ID ");
          _sb.AppendLine("       , TE_TERMINAL_TYPE ");
          _sb.AppendLine("  FROM   TERMINALS ");
          _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @terminal_id ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
          _sql_cmd.Parameters.Add("@terminal_id", SqlDbType.BigInt, 8, "TE_TERMINAL_ID").Value = CardInfo.LoggedInTerminalId;

              using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read() && !_reader.IsDBNull(0))
            {
              CardInfo.LoggedInTerminalName = (String)_reader[0];
              CardInfo.LoggedInTerminalProvider = (String)_reader[1];
              CardInfo.LoggedInTerminalType = (TerminalTypes)_reader.GetInt16(2);
            }
          }
            }
          }
          catch (Exception ex)
          {
            Log.Exception(ex);
          }
        }

        if (_current_play_session_id == 0)
        {
          _current_play_session_id = _last_play_session_id;
        }

        if (_current_play_session_id != 0)
        {
          CardInfo.CurrentPlaySession = PlaySession.Read(_current_play_session_id, SqlTrx);
        }

        _rc = false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return !_rc;
    }

    private static Boolean GetCancelableAccountOperations(Int64 AccountId, CardData CardInfo, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      CancellableOperation _cancellable_operation;
      Currency _terminal_draw_amount;
      List<CancellableOperation> _terminal_draw_cancellable_operations;
      OperationCode _opCode;
      _opCode = OperationCode.NOT_SET;

      _terminal_draw_amount = 0;

      CardInfo.Cancellable.TotalBalance = MultiPromos.AccountBalance.Zero;
      CardInfo.Cancellable.TotalOperationAmount = 0;
      CardInfo.Cancellable.TotalCashInTaxAmount = 0;
      CardInfo.Cancellable.TotalNotPlayedRecharge = 0;
      CardInfo.Cancellable.Operations.Clear();

      try
      {
        if (Misc.Common_GetPendingTerminalDrawsRechargesToCancel(AccountId, out _terminal_draw_cancellable_operations))
        {
          _terminal_draw_amount = Misc.Common_GetPendingAmountTerminalDrawsRecharges(_terminal_draw_cancellable_operations);
        }   

        // RCI 05-DEC-2012: Multiple cancellation of B part.          

        _sb = new StringBuilder();
        _sb.AppendLine("DECLARE @_operation_id As BIGINT  ");
        _sb.AppendLine("SELECT @_operation_id = AC_CANCELLABLE_OPERATION_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId ");
        _sb.AppendLine("IF @_operation_id IS NULL RETURN ");
        // DHA 09-OCT-2014: the code has been changed to avoid the next query, and the query has been also changed to avoid the inner join with account
        _sb.AppendLine("SELECT   AO_OPERATION_ID ");
        _sb.AppendLine("       , AO_REDEEMABLE ");
        _sb.AppendLine("       , AO_PROMO_REDEEMABLE ");
        _sb.AppendLine("       , AO_PROMO_NOT_REDEEMABLE ");
        _sb.AppendLine("       , AO_AMOUNT ");
        _sb.AppendLine("       , AO_OPERATION_DATA ");
        _sb.AppendLine("       , AO_CODE ");
        _sb.AppendLine("  FROM   ACCOUNT_OPERATIONS WITH ( INDEX ( PK_ACCOUNT_OPERATIONS ) ) ");
        _sb.AppendLine(" WHERE   AO_ACCOUNT_ID    = @pAccountId ");
        _sb.AppendLine("   AND   AO_OPERATION_ID >= @_operation_id ");
        // EOR 09-MAR-2018: _sb.AppendLine("   AND   AO_CODE       IN ( @pOpCashIn, @pOpCashInPromo, @pMBCashIn, @pMBCashInPromo, @pNACashIn, @pNACashInPromo, @pGiftRedeemableAsCashIn,@pOpPromoRedeemableWithTaxes, @pOpPrizePayoutAndRecharge, @pOpChipsSaleWithRecharge ) ");
        _sb.AppendLine("   AND   AO_CODE       IN ( @pOpCashIn, @pOpCashInPromo, @pMBCashIn, @pMBCashInPromo, @pNACashIn, @pNACashInPromo, @pGiftRedeemableAsCashIn,@pOpPromoRedeemableWithTaxes, @pOpPrizePayoutAndRecharge ) ");
        // RCI & AJQ 14-AUG-2013: Cancellable Operation are only those which have redeemable balance.
        _sb.AppendLine("   AND   (AO_REDEEMABLE    > 0 OR  AO_CODE=@pOpPromoRedeemableWithTaxes) ");
        // RCI 15-NOV-2013: Fixed Bug WIG-408
        _sb.AppendLine("   AND   ISNULL(AO_UNDO_STATUS, @pOperationUndoNone) = @pOperationUndoNone ");
        _sb.AppendLine("ORDER BY AO_OPERATION_ID ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pOpCashIn", SqlDbType.Int).Value = (Int32)OperationCode.CASH_IN;
          _cmd.Parameters.Add("@pOpCashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.PROMOTION;
          _cmd.Parameters.Add("@pMBCashIn", SqlDbType.Int).Value = (Int32)OperationCode.MB_CASH_IN;
          _cmd.Parameters.Add("@pMBCashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.MB_PROMOTION;
          _cmd.Parameters.Add("@pNACashIn", SqlDbType.Int).Value = (Int32)OperationCode.NA_CASH_IN;
          _cmd.Parameters.Add("@pNACashInPromo", SqlDbType.Int).Value = (Int32)OperationCode.NA_PROMOTION;
          _cmd.Parameters.Add("@pGiftRedeemableAsCashIn", SqlDbType.Int).Value = (Int32)OperationCode.GIFT_REDEEMABLE_AS_CASHIN;
          _cmd.Parameters.Add("@pOpPromoRedeemableWithTaxes", SqlDbType.Int).Value = (Int32)OperationCode.PROMOTION_WITH_TAXES;
          _cmd.Parameters.Add("@pOpPrizePayoutAndRecharge", SqlDbType.Int).Value = (Int32)OperationCode.PRIZE_PAYOUT_AND_RECHARGE;
          //_cmd.Parameters.Add("@pOpChipsSaleWithRecharge", SqlDbType.Int).Value = (Int32)OperationCode.CHIPS_SALE_WITH_RECHARGE;
          _cmd.Parameters.Add("@pOperationUndoNone", SqlDbType.Int).Value = (Int32)UndoStatus.None;

          using (SqlDataReader _read = _cmd.ExecuteReader())
          {
            while (_read.Read())
            {
              _cancellable_operation = new CancellableOperation();
              _cancellable_operation.OperationId = _read.GetInt64(0);
              _cancellable_operation.Balance = new MultiPromos.AccountBalance(_read.GetDecimal(1), _read.GetDecimal(2), _read.GetDecimal(3));
              _cancellable_operation.OperationAmount = _read.GetDecimal(4);
              _cancellable_operation.CashInTaxAmount = Math.Max(0, _cancellable_operation.OperationAmount - _cancellable_operation.Balance.TotalRedeemable);

              // DHA 09-OCT-2014: accumulate total, when not played recharge
              CardInfo.Cancellable.TotalNotPlayedRecharge += _cancellable_operation.OperationAmount;

              // DHA 09-OCT-2014: only avoid the "cancel" when the parameter is defined
              if (GeneralParam.GetBoolean("Cashier", "MinCashableCents.AvoidCancel", false))
              {
                // 10-SEP-2012, Avoid "Cancel" the operation when the Redeemable amount is not a multiple of the RoundToCents
                DevolutionPrizeParts _parts;
                _parts = new DevolutionPrizeParts();
                if (_parts.DecimalRoundingEnabled && _parts.RoundToCents > 1)
                {
                  Int64 _cents;

                  _cents = (Int64)(_cancellable_operation.Balance.TotalRedeemable * 100);
                  if (_cents % _parts.RoundToCents != 0)
                  {
                    // Don't add this operation to the list.
                    continue;
                  }
                }
              }


              //JBC 16-DEC-2014 
              if (_read[5] != DBNull.Value && _read.GetInt32(6) == (Int32)OperationCode.CASH_IN)
              {
                _cancellable_operation.CommissionToBAmount = ((Currency)_read.GetInt64(5) / 100);
              }

              // LA SDS 08-06-2016 Bug
              if (_read.GetInt32(6) == (int)OperationCode.PROMOTION_WITH_TAXES
                || !(Misc.PrizeComputationWithoutDevolution()
                   || GeneralParam.GetBoolean("Cashier", "CancellationDisabled", false) 
                   || CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws() 
                   && GeneralParam.GetInt32("CashDesk.Draw", "AccountingMode") == 1))
              {

                DevolutionPrizeParts _parts;
                
                _opCode = (OperationCode)_read.GetInt32(6);

                _parts = new DevolutionPrizeParts(_opCode == OperationCode.CHIPS_SALE_WITH_RECHARGE);

                CardInfo.Cancellable.Operations.Add(_cancellable_operation);

                CardInfo.Cancellable.TotalBalance += _cancellable_operation.Balance;
                CardInfo.Cancellable.TotalOperationAmount += _cancellable_operation.OperationAmount;
                CardInfo.Cancellable.TotalCashInTaxAmount += _cancellable_operation.CashInTaxAmount;

                _parts.MaxDevolutionCustody = CardInfo.AccountBalance.CustodyProvision;
                CardInfo.Cancellable.TotalBalance.CustodyProvision = _parts.CalculateDevolutionCustody(CardInfo.Cancellable.TotalBalance.Redeemable, CardInfo.MaxDevolution, false);


              }
            }
          }
        }

        // Add terminal draw cancellation
        if (_terminal_draw_cancellable_operations.Count > 0)
        {
          foreach (CancellableOperation _operation in _terminal_draw_cancellable_operations)
          {
            CardInfo.Cancellable.Operations.Add(_operation);

            CardInfo.Cancellable.TotalBalance += _operation.Balance;

            CardInfo.Cancellable.TotalNotPlayedRecharge -= _operation.OperationAmount;
            CardInfo.Cancellable.TotalCashInTaxAmount -= _operation.OperationAmount;

            CardInfo.Cancellable.HaveTerminalDrawPromo = true;
          }
        }

        /*
        // DHA 09-OCT-2014: we always iterate the operations to compute "TotalNotPlayedRecharge" then if cancellation is disabled others fields are reset
        // DHA 29-JAN-2015: when cash desk draw is enabled and configured by play sessions, cancellations are disabled
        if (Misc.PrizeComputationWithoutDevolution() ||
            GeneralParam.GetBoolean("Cashier", "CancellationDisabled", false) ||
            CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws() && GeneralParam.GetInt32("CashDesk.Draw", "AccountingMode") == 1) // AccountingMode = 1 --> Play session
        {
          CardInfo.Cancellable.TotalBalance = MultiPromos.AccountBalance.Zero;
          CardInfo.Cancellable.TotalOperationAmount = 0;
          CardInfo.Cancellable.TotalCashInTaxAmount = 0;
          CardInfo.Cancellable.Operations.Clear();
        }
        */

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCancelableAccountOperations

    public static Boolean DB_CardGetPersonalData(String ExternalTrackData, CardData CardInfo, SqlTransaction SqlTrx)
    {
      Int64 _account_id;
      Int32 _site_id;

      if (!DB_CardGetAccountId(ExternalTrackData, out _account_id, out _site_id, SqlTrx))
      {
        return false;
      }

      // TrackData not found.
      if (_account_id == 0)
      {
        return false;
      }

      return DB_CardGetPersonalData(_account_id, CardInfo, SqlTrx);
    }

    public static Boolean DB_CardGetPersonalData(Int64 AccountId, CardData CardInfo, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        // Get player tracking data
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   ISNULL(AC_HOLDER_NAME, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ADDRESS_01, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ADDRESS_02, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ADDRESS_03, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_CITY, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ZIP, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_EMAIL_01, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_EMAIL_02, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_COMMENTS, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_GENDER, 0)");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_MARITAL_STATUS, 0)");
        _sb.AppendLine("       , AC_HOLDER_BIRTH_DATE");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ID, '')");
        _sb.AppendLine("       , ISNULL(dbo.GetBucketValue(" + (Int32)Buckets.BucketId.RedemptionPoints + ", @pAccountId), 0)"); // SGB 05-JAN-2016
        _sb.AppendLine("       , ISNULL (AC_HOLDER_LEVEL, 1)");
        _sb.AppendLine("       , AC_HOLDER_LEVEL_ENTERED");
        _sb.AppendLine("       , AC_HOLDER_LEVEL_EXPIRATION");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_LEVEL_NOTIFY, 0)");
        _sb.AppendLine("       , ISNULL (AC_PIN, '')");
        _sb.AppendLine("       , ISNULL (AC_PIN_FAILURES, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID1, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID2, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_DOCUMENT_ID1, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_DOCUMENT_ID2, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NAME1, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NAME2, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NAME3, '')");
        _sb.AppendLine("       , AC_HOLDER_ID_TYPE "); // DDM 03-AUG-2012
        _sb.AppendLine("       , AC_HOLDER_WEDDING_DATE"); // ICS 13-FEB-2013 
        _sb.AppendLine("       , ISNULL (AC_HOLDER_TWITTER_ACCOUNT, '')"); // LEM 28-MAR-2013 
        _sb.AppendLine("       , ISNULL (AC_HOLDER_EXT_NUM, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NATIONALITY, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_BIRTH_COUNTRY, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_FED_ENTITY, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID1_TYPE, 1)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID2_TYPE, 2)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID3_TYPE, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID3, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_HAS_BENEFICIARY, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_OCCUPATION_ID, 0)");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_NAME, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_NAME1, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_NAME2, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_NAME3, '')");
        _sb.AppendLine("       , AC_BENEFICIARY_BIRTH_DATE ");
        _sb.AppendLine("       , ISNULL(AC_BENEFICIARY_GENDER, 0)");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_OCCUPATION_ID, '-1')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID1_TYPE, 1)");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID1, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID2_TYPE, 2)");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID2, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID3_TYPE, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID3, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_IS_VIP, 0)");
        _sb.AppendLine("       , AC_BLOCKED ");
        _sb.AppendLine("       , AC_BLOCK_DESCRIPTION ");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NAME4, '') "); // 58
        _sb.AppendLine("       , ISNULL (AC_SHOW_COMMENTS_ON_CASHIER, 0)");
        _sb.AppendLine("       , AC_BLOCK_REASON ");
        _sb.AppendLine("       , AC_HOLDER_ADDRESS_COUNTRY"); // SGB 12-AGO-2015
        _sb.AppendLine("       , AC_HOLDER_ADDRESS_VALIDATION "); // AMF 07-SEP-2015 -> 62
        _sb.AppendLine("  FROM   ACCOUNTS ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Error(String.Format("There is not account information for the AccountID:[{0}]", AccountId));
              return false;
            }

            CardInfo.PlayerTracking = new PlayerTrackingData();
            CardInfo.PlayerTracking.HolderName = ((String)_reader[0]).ToUpperInvariant();
            CardInfo.PlayerTracking.HolderAddress01 = (String)_reader[1];
            CardInfo.PlayerTracking.HolderAddress02 = (String)_reader[2];
            CardInfo.PlayerTracking.HolderAddress03 = (String)_reader[3];
            CardInfo.PlayerTracking.HolderCity = (String)_reader[4];
            CardInfo.PlayerTracking.HolderZip = (String)_reader[5];
            CardInfo.PlayerTracking.HolderEmail01 = (String)_reader[6];
            CardInfo.PlayerTracking.HolderEmail02 = (String)_reader[7];
            CardInfo.PlayerTracking.HolderPhone01 = (String)_reader[8];
            CardInfo.PlayerTracking.HolderPhone02 = (String)_reader[9];
            CardInfo.PlayerTracking.HolderComments = (String)_reader[10];
            CardInfo.PlayerTracking.HolderGender = (Int32)_reader[11];
            CardInfo.PlayerTracking.HolderMaritalStatus = (Int32)_reader[12];
            if (_reader.IsDBNull(13))
            {
              CardInfo.PlayerTracking.HolderBirthDate = DateTime.MinValue;
            }
            else
            {
              CardInfo.PlayerTracking.HolderBirthDate = (DateTime)_reader[13];
            }
            CardInfo.PlayerTracking.HolderBirthDateText = CardInfo.PlayerTracking.HolderBirthDate.ToShortDateString();
            CardInfo.PlayerTracking.HolderId = ((String)_reader[14]).ToUpperInvariant();
            CardInfo.PlayerTracking.CurrentPoints = _reader.GetDecimal(15);
            CardInfo.PlayerTracking.TruncatedPoints = Math.Truncate(CardInfo.PlayerTracking.CurrentPoints);
            CardInfo.PlayerTracking.CardLevel = (Int32)_reader[16];

            // RCI 10-NOV-2010: Automatic level change.
            CardInfo.PlayerTracking.HolderLevelEntered = _reader.IsDBNull(17) ? DateTime.MinValue : _reader.GetDateTime(17);
            CardInfo.PlayerTracking.HolderLevelExpiration = _reader.IsDBNull(18) ? DateTime.MinValue : _reader.GetDateTime(18);
            CardInfo.PlayerTracking.HolderLevelNotify = _reader.GetInt32(19);

            // JMM 18-NOV-2011: PIN
            CardInfo.PlayerTracking.Pin = _reader.GetString(20);
            // RCI 24-NOV-2011
            CardInfo.PlayerTracking.PinFailures = _reader.GetInt32(21);

            if (CardInfo.PlayerTracking.HolderName == "")
            {
              CardInfo.PlayerTracking.CardLevel = 0;
              CardInfo.PlayerTracking.HolderLevelEntered = DateTime.MinValue;
              CardInfo.PlayerTracking.HolderLevelExpiration = DateTime.MinValue;
              CardInfo.PlayerTracking.HolderLevelNotify = 0;
            }

            // TJG 01-FEB-2012
            //CardInfo.PlayerTracking.HolderId1 = "";
            //CardInfo.PlayerTracking.HolderId2 = "";
            //CardInfo.PlayerTracking.HolderDocumentId1 = 0;
            //CardInfo.PlayerTracking.HolderDocumentId2 = 0;
            //CardInfo.PlayerTracking.HolderName1 = "";
            //CardInfo.PlayerTracking.HolderName2 = "";
            //CardInfo.PlayerTracking.HolderName3 = "";

            CardInfo.PlayerTracking.HolderId1 = ((String)_reader[22]).ToUpperInvariant();      // AC_HOLDER_ID1
            CardInfo.PlayerTracking.HolderId2 = ((String)_reader[23]).ToUpperInvariant();      // AC_HOLDER_ID2
            CardInfo.PlayerTracking.HolderDocumentId1 = _reader.GetInt64(24);                  // AC_HOLDER_DOCUMENT_ID1
            CardInfo.PlayerTracking.HolderDocumentId2 = _reader.GetInt64(25);                  // AC_HOLDER_DOCUMENT_ID2
            CardInfo.PlayerTracking.HolderName1 = ((String)_reader[26]).ToUpperInvariant();    // AC_HOLDER_NAME1
            CardInfo.PlayerTracking.HolderName2 = ((String)_reader[27]).ToUpperInvariant();    // AC_HOLDER_NAME2
            CardInfo.PlayerTracking.HolderName3 = ((String)_reader[28]).ToUpperInvariant();    // AC_HOLDER_NAME3
            CardInfo.PlayerTracking.HolderName4 = ((String)_reader[58]).ToUpperInvariant();    // AC_HOLDER_NAME4            
            CardInfo.PlayerTracking.HolderIdType = (ACCOUNT_HOLDER_ID_TYPE)_reader.GetInt32(29); // AC_HOLDER_ID_TYPE

            // ICS 13-FEB-2013: Wedding Date
            if (_reader.IsDBNull(30))
            {
              CardInfo.PlayerTracking.HolderWeddingDate = DateTime.MinValue;
            }
            else
            {
              CardInfo.PlayerTracking.HolderWeddingDate = _reader.GetDateTime(30);
            }

            CardInfo.PlayerTracking.HolderTwitter = (String)_reader[31];
            CardInfo.PlayerTracking.HolderExtNumber = (String)_reader[32];
            CardInfo.PlayerTracking.HolderNationality = (Int32)_reader[33];
            CardInfo.PlayerTracking.HolderBirthCountry = (Int32)_reader[34];
            CardInfo.PlayerTracking.HolderFedEntity = (Int32)_reader[35];

            if (_reader.IsDBNull(61) || (Int32)_reader[61] == -1) // SGB 12-AGO-2015
            {
              CardInfo.PlayerTracking.HolderAddressCountry = 0;
            }
            else
            {
              CardInfo.PlayerTracking.HolderAddressCountry = (Int32)_reader[61];
            }

            CardInfo.PlayerTracking.HolderAddressValidation = (ADDRESS_VALIDATION)_reader[62];
            CardInfo.PlayerTracking.HolderId1Type = (ACCOUNT_HOLDER_ID_TYPE)_reader[36];
            CardInfo.PlayerTracking.HolderId2Type = (ACCOUNT_HOLDER_ID_TYPE)_reader[37];
            CardInfo.PlayerTracking.HolderId3Type = (String)_reader[38];
            CardInfo.PlayerTracking.HolderId3 = (String)_reader[39];
            CardInfo.PlayerTracking.HolderHasBeneficiary = (Boolean)_reader[40];
            CardInfo.PlayerTracking.HolderOccupationId = (Int32)_reader[41];
            CardInfo.PlayerTracking.BeneficiaryName = ((String)_reader[42]).ToUpperInvariant();
            CardInfo.PlayerTracking.BeneficiaryName1 = ((String)_reader[43]).ToUpperInvariant();
            CardInfo.PlayerTracking.BeneficiaryName2 = ((String)_reader[44]).ToUpperInvariant();
            CardInfo.PlayerTracking.BeneficiaryName3 = ((String)_reader[45]).ToUpperInvariant();

            if (_reader.IsDBNull(46))
            {
              CardInfo.PlayerTracking.BeneficiaryBirthDate = DateTime.MinValue;
            }
            else
            {
              CardInfo.PlayerTracking.BeneficiaryBirthDate = (DateTime)_reader.GetDateTime(46);
            }

            CardInfo.PlayerTracking.BeneficiaryGender = (Int32)_reader[47];
            CardInfo.PlayerTracking.BeneficiaryOccupationId = (Int32)_reader[48];
            CardInfo.PlayerTracking.BeneficiaryId1Type = (ACCOUNT_HOLDER_ID_TYPE)_reader[49];
            CardInfo.PlayerTracking.BeneficiaryId1 = (String)_reader[50];
            CardInfo.PlayerTracking.BeneficiaryId2Type = (ACCOUNT_HOLDER_ID_TYPE)_reader[51];
            CardInfo.PlayerTracking.BeneficiaryId2 = (String)_reader[52];
            CardInfo.PlayerTracking.BeneficiaryId3Type = (String)_reader[53];
            CardInfo.PlayerTracking.BeneficiaryId3 = (String)_reader[54];
            CardInfo.PlayerTracking.HolderIsVIP = (Boolean)_reader[55];    // AC_HOLDER_IS_VIP
            CardInfo.PlayerTracking.ShowCommentsOnCashier = (Boolean)_reader[59];

            CardInfo.Blocked = (Boolean)_reader[56];
            CardInfo.BlockDescription = (_reader.IsDBNull(57)) ? "" : _reader.GetString(57);

            CardInfo.BlockReason = (AccountBlockReason)_reader[60]; // AC_BLOCK_REASON
            CardInfo.AccountId = AccountId;
            CardInfo.PlayerTracking.HolderWeddingDateText = CardInfo.PlayerTracking.HolderWeddingDate.ToShortDateString();
            CardInfo.PlayerTracking.BeneficiaryBirthDateText = CardInfo.PlayerTracking.BeneficiaryBirthDate.ToShortDateString();

          } // using _reader
        } // using _cmd

        return true;
      } // try
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Update card status: Block / Unblock card.
    /// </summary>
    /// <param name="CardInfo"></param>
    /// <returns></returns>
    public static Boolean DB_BlockUnblockCard(CardData CardInfo, CashierSessionInfo SessionInfo, SqlTransaction Trx)
    {
      return DB_BlockUnblockCard(CardInfo, SessionInfo, AccountBlockReason.NONE, Trx);
    }

    public static Boolean DB_BlockUnblockCard(CardData CardInfo, CashierSessionInfo SessionInfo, AccountBlockReason PreviousBlockReason, SqlTransaction Trx)
    {
      StringBuilder _sb;
      MovementType _account_movement_type;
      AccountMovementsTable _account_movements;

      // RCI 21-MAY-2015: Final check: Can't unlock if account was locked by SPACE or by external PLD system.
      if (!CardInfo.Blocked &&
          Accounts.ExistsBlockReasonInMask(new List<AccountBlockReason>(new AccountBlockReason[] {
                                                    AccountBlockReason.EXTERNAL_SYSTEM_CANCELED , AccountBlockReason.EXTERNAL_AML}),
                                           Accounts.ConvertOldBlockReason((Int32)PreviousBlockReason)))
      {
        return false;
      }

      CardInfo.BlockDescription = (!String.IsNullOrEmpty(CardInfo.BlockDescription)) ? WSI.Common.WGDB.Now.ToShortDateString() + " - " + CardInfo.BlockDescription : String.Empty;
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE  ACCOUNTS                                  ");
        _sb.AppendLine(" SET     AC_BLOCKED           = @pBlocked          ");
        _sb.AppendLine("       , AC_BLOCK_REASON      = CASE WHEN @pBlocked = 0 THEN 0 ELSE (dbo.BlockReason_ToNewEnumerate(AC_BLOCK_REASON) | @pBlockReason) END     ");
        _sb.AppendLine("       , AC_BLOCK_DESCRIPTION = @pBlockDescription ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID        = @pAccountId        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = CardInfo.Blocked;
          _cmd.Parameters.Add("@pBlockReason", SqlDbType.Int).Value = CardInfo.BlockReason;
          _cmd.Parameters.Add("@pBlockDescription", SqlDbType.NVarChar).Value = CardInfo.BlockDescription;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = CardInfo.AccountId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

        }

        // Set account movement type
        _account_movement_type = CardInfo.Blocked ? MovementType.AccountBlocked : MovementType.AccountUnblocked;

        // Set account movement
        _account_movements = new AccountMovementsTable(SessionInfo);

        // Add account movement
        _account_movements.Add(0, CardInfo.AccountId, _account_movement_type,
                               CardInfo.CurrentBalance, 0, 0, CardInfo.CurrentBalance, CardInfo.BlockDescription);

        // Save accounts and cashier movements
        if (!_account_movements.Save(Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // DB_BlockUnblockCard

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate the not redeemable parts.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - NrBalance
    //          - NrManual
    //          - NrGifts
    //
    // RETURNS:
    //      - true: data was calculated successfully
    //      - false: data was not calculated
    // 
    //   NOTES:
    //
    public static Boolean NotRedeemableParts(Int64 AccountId, out Decimal NrBalance, out Decimal NrManual, out Decimal NrGifts, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Promotion.PROMOTION_TYPE _promo_type;
      Currency _promo_balance;

      NrBalance = 0;
      NrManual = 0;
      NrGifts = 0;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT   ACP_PROMO_TYPE                    ");
      _sb.AppendLine("         , SUM (ACP_BALANCE)                 ");
      _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status)) ");
      _sb.AppendLine("   WHERE   ACP_ACCOUNT_ID   = @pAccountId    ");
      _sb.AppendLine("     AND   ACP_STATUS       = @pStatusActive ");
      _sb.AppendLine("     AND   ACP_CREDIT_TYPE IN (@pNR1, @pNR2) ");
      _sb.AppendLine("     AND   ACP_BALANCE      > 0              ");
      _sb.AppendLine("GROUP BY   ACP_PROMO_TYPE                    ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _sql_cmd.Parameters.Add("@pNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_STATUS.ACTIVE;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _promo_type = (Promotion.PROMOTION_TYPE)_reader.GetInt32(0);
              _promo_balance = _reader.GetSqlMoney(1);

              NrBalance += _promo_balance;

              switch (_promo_type)
              {
                case Promotion.PROMOTION_TYPE.MANUAL:
                case Promotion.PROMOTION_TYPE.PREASSIGNED:
                  NrManual += _promo_balance;
                  break;

                case Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE:
                  NrGifts += _promo_balance;
                  break;
              }
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // NotRedeemableParts

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate the promotion amount.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - CardInfo
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: data was calculated successfully
    //      - false: data was not calculated
    // 
    //   NOTES:
    //
    private static Boolean CalculatePromotionsAmount(Int64 AccountId, CardData CardInfo, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Currency _balance;
      ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
      Promotion.PROMOTION_TYPE _promo_type;
      Currency _lock_balance;

      CardInfo.NumberOfPromosByCreditType.Clear();
      CardInfo.NumberOfCoverCouponByCreditType.Clear();

      CardInfo.Nr1Withhold = 0;

      CardInfo.WonLockExists = false;
      CardInfo.NonRedeemableWonLock = 0;
      CardInfo.PromotionToRedeemableAllowed = false;
      CardInfo.PromotionToRedeemableAmount = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   ACP_BALANCE     ");
        _sb.AppendLine("       , ACP_CREDIT_TYPE ");
        _sb.AppendLine("       , ACP_WONLOCK     ");
        _sb.AppendLine("       , ACP_WITHHOLD    ");
        _sb.AppendLine("       , ACP_PLAY_SESSION_ID ");
        _sb.AppendLine("       , ACP_PROMO_TYPE  ");
        _sb.AppendLine("  FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status)) ");
        _sb.AppendLine(" WHERE   ACP_ACCOUNT_ID = @pAccountId	   ");
        _sb.AppendLine("   AND   ACP_STATUS     = @pStatusActive ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _balance = _reader.GetSqlMoney(0);
              // If InSession, the PromoBalance is in the machine, so balance = 0.
              if (!_reader.IsDBNull(4))
              {
                _balance = 0;
              }
              _credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)_reader.GetInt32(1);

              if (CardInfo.NumberOfPromosByCreditType.ContainsKey(_credit_type))
              {
                CardInfo.NumberOfPromosByCreditType[_credit_type] += 1;
              }
              else
              {
                CardInfo.NumberOfPromosByCreditType[_credit_type] = 1;
              }

              switch (_credit_type)
              {
                case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                  CardInfo.Nr1Withhold += _reader.GetDecimal(3);
                  if (!_reader.IsDBNull(2))
                  {
                    _lock_balance = _reader.GetSqlMoney(2);

                    CardInfo.WonLockExists = true;
                    CardInfo.NonRedeemableWonLock += _lock_balance;

                    if (_balance >= _lock_balance)
                    {
                      CardInfo.PromotionToRedeemableAllowed = true;
                      CardInfo.PromotionToRedeemableAmount += _balance;
                    }
                  }
                  break;

                case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:
                  break;

                case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                default:
                  Log.Error("Unexpected Credit Type: " + _credit_type.ToString() + ", AccountId: " + AccountId.ToString());

                  return false;
              }

              _promo_type = (Promotion.PROMOTION_TYPE)_reader.GetInt32(5);
              if (_promo_type == Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON)
              {
                if (CardInfo.NumberOfCoverCouponByCreditType.ContainsKey(_credit_type))
                {
                  CardInfo.NumberOfCoverCouponByCreditType[_credit_type] += 1;
                }
                else
                {
                  CardInfo.NumberOfCoverCouponByCreditType[_credit_type] = 1;
                }
              }
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CalculatePromotionsAmount

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieve all relevant data related to a TrackData from the database
    //          using the card identifier.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardTrackData
    //
    //      - OUTPUT:
    //          - CardInfo
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //
    public static Boolean DB_CardGetAllData(String ExternalTrackData,
                                            CardData CardInfo,
                                            Boolean GetAllData)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!DB_CardGetAccountId(ExternalTrackData, out CardInfo.AccountId, out CardInfo.SiteId, _db_trx.SqlTransaction))
        {
          return false;
        }

        // Check for an existent account.
        if (CardInfo.AccountId != 0)
        {
          if (!CardData.DB_CardGetAllData(CardInfo.AccountId, CardInfo, GetAllData))
          {
            Log.Error("DB_CardGetAllData. DB_GetCardAllData: Error obtaining card whole data.");

            return false;
          }
        }
      }

      return true;
    } // DB_CardGetAllData

    public static Boolean DB_CardGetAccountId(String ExternalTrackData, out Int64 AccountId, out Int32 SiteId, SqlTransaction Trx)
    {

      StringBuilder _sb;
      Object _obj;
      ExternalTrackData _ctd;
      // Initialize card data

      _ctd = new ExternalTrackData(ExternalTrackData);
      AccountId = 0;
      SiteId = _ctd.SiteId;

      if (!CheckTrackdataSiteId(_ctd))
      {
        Log.Error("DB_CardGetAccountId. Card: " + ExternalTrackData + " doesn't belong to this site.");

        return false;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   AC_ACCOUNT_ID       ");
        _sb.AppendLine("   FROM   ACCOUNTS            ");
        _sb.AppendLine("  WHERE   AC_TRACK_DATA = @p1 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@p1", SqlDbType.NVarChar).Value = _ctd.InternalTrackData;

          _obj = _cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            AccountId = (Int64)_obj;
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // DB_CardGetAccountId

    //------------------------------------------------------------------------------
    // PURPOSE: Check if there is a Site Jackpot Winner.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //          - SiteJackpotWinnerInfo: If won, contains the Winner Jackpot info.
    //
    // RETURNS:
    //      - true: Exists a Site Jackpot Winner. Info of the jackpot can be found in the Output parameter.
    //      - false: Doesn't exist any Site Jackpot Winner.
    // 
    public static Boolean CheckWinner(out TYPE_SITE_JACKPOT_WINNER SiteJackpotWinnerInfo)
    {
      return CheckWinner(0, out SiteJackpotWinnerInfo);
    } // CheckWinner

    //------------------------------------------------------------------------------
    // PURPOSE: Check if AccountId has won a Site Jackpot.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //
    //      - OUTPUT:
    //          - SiteJackpotWinnerInfo: If won, contains the Winner Jackpot info.
    //
    // RETURNS:
    //      - true: AccountId has won a Site Jackpot. Info of the jackpot can be found in the Output parameter.
    //      - false: AccountId has not won any Site Jackpot.
    // 
    public static Boolean CheckWinner(Int64 AccountId,
                                      out TYPE_SITE_JACKPOT_WINNER SiteJackpotWinnerInfo)
    {

      String _sql_query;
      String _condition_account_id;
      String _condition_movement_id;
      SqlCommand _sql_cmd;
      SqlDataReader _reader;
      Int32 _time_period;

      SiteJackpotWinnerInfo = new TYPE_SITE_JACKPOT_WINNER();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        SqlTransaction _trx = _db_trx.SqlTransaction;

        // RXM 05-OCT-2012
        _time_period = GeneralParam.GetInt32("Cashier.HandPays", "TimePeriod", 60, 5, 2500);

        _sql_cmd = new SqlCommand();

        _condition_account_id = "";

        if (AccountId != 0)
        {
          _condition_account_id = "    AND   HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID        = @pAccountId ";
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        }

        if (Handpays.SiteJackpotPaymentMode == HANDPAY_PAYMENT_MODE.AUTOMATIC)
        {
          //if automatic payment mode then select last jackpot not paid or not notified
          _condition_movement_id = " ( HP_MOVEMENT_ID IS NULL OR HP_SITE_JACKPOT_NOTIFIED = 0 ) ";
        }
        else
        {
          _condition_movement_id = " HP_MOVEMENT_ID IS NULL ";
        }

        _sql_query = " SELECT   TOP 1 HP_AMOUNT "
                   + "        , HP_DATETIME "
                   + "        , ISNULL ((SELECT TE_PROVIDER_ID FROM TERMINALS WHERE TE_TERMINAL_ID = HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID), '') AS TE_PROVIDER_ID "
                   + "        , ISNULL (HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, 0) "
                   + "        , ISNULL ((SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID = HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID), '') AS TE_NAME "
                   + "        , ISNULL (HP_SITE_JACKPOT_INDEX, 0) "
                   + "        , ISNULL (HP_SITE_JACKPOT_NAME, '') "
                   + "        , ISNULL (HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID, 0) "
                   + "        , ISNULL ((SELECT AC_HOLDER_NAME FROM ACCOUNTS WHERE AC_ACCOUNT_ID = HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID), '') AS AC_HOLDER_NAME "
                   + "        , HP_MOVEMENT_ID "
                   + "   FROM   HANDPAYS"
                   + "  WHERE " + _condition_movement_id
                   + "    AND   HP_DATETIME    >= DATEADD (MINUTE, -@pTimePeriod, GETDATE()) "
                   + "    AND   HP_TYPE        = @pTypeSiteJackpot "
                   + _condition_account_id
                   + " ORDER BY HP_DATETIME DESC ";

        _sql_cmd.CommandText = _sql_query;
        _sql_cmd.Connection = _trx.Connection;
        _sql_cmd.Transaction = _trx;

        _sql_cmd.Parameters.Add("@pTimePeriod", SqlDbType.Int).Value = _time_period;
        _sql_cmd.Parameters.Add("@pTypeSiteJackpot", SqlDbType.Int).Value = HANDPAY_TYPE.SITE_JACKPOT;

        // RCI & AJQ 26-JUL-2011
        _sql_cmd = WGDB.ParametersToVariables(_sql_cmd);

        _reader = null;
        try
        {
          _reader = _sql_cmd.ExecuteReader();
          if (_reader.Read())
          {
            SiteJackpotWinnerInfo.amount = _reader.GetDecimal(0);
            SiteJackpotWinnerInfo.awarded_date = _reader.GetDateTime(1);
            SiteJackpotWinnerInfo.awarded_on_provider_name = _reader.GetString(2);
            SiteJackpotWinnerInfo.awarded_on_terminal_id = _reader.GetInt32(3);
            SiteJackpotWinnerInfo.awarded_on_terminal_name = _reader.GetString(4);
            SiteJackpotWinnerInfo.jackpot_index = _reader.GetInt32(5);
            SiteJackpotWinnerInfo.jackpot_name = _reader.GetString(6);
            SiteJackpotWinnerInfo.awarded_to_account_id = _reader.GetInt64(7);
            SiteJackpotWinnerInfo.awarded_to_account_name = _reader.GetString(8);
            SiteJackpotWinnerInfo.paid = !_reader.IsDBNull(9);

            return true;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          if (_reader != null)
          {
            _reader.Close();
            _reader.Dispose();
            _reader = null;
          }
        }
      }
      return false;
    } // CheckWinner

    public static Boolean UpdateJackpotNotifiedStatus(TYPE_SITE_JACKPOT_WINNER JackpotWinnerInfo, SITE_JACKPOT_NOTIFICATION_STATUS NotifyStatus)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (UpdateJackpotNotifiedStatus(JackpotWinnerInfo, NotifyStatus, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean UpdateJackpotNotifiedStatus(TYPE_SITE_JACKPOT_WINNER JackpotWinnerInfo, SITE_JACKPOT_NOTIFICATION_STATUS NotifyStatus, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   HANDPAYS                                              ");
        _sb.AppendLine("    SET   HP_SITE_JACKPOT_NOTIFIED  = @pNotified                ");
        _sb.AppendLine("  WHERE   HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("    AND   HP_DATETIME               = @pDateTime                ");
        _sb.AppendLine("    AND   HP_AMOUNT                 = @pAmount                  ");
        _sb.AppendLine("    AND   HP_TYPE                   = @pSiteJackpotType         ");
        _sb.AppendLine("    AND   HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID = @pAccountId   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pNotified", SqlDbType.Bit).Value = (Int32)NotifyStatus;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = JackpotWinnerInfo.awarded_on_terminal_id;
          _cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = JackpotWinnerInfo.awarded_date;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = JackpotWinnerInfo.amount.SqlMoney;
          _cmd.Parameters.Add("@pSiteJackpotType", SqlDbType.BigInt).Value = HANDPAY_TYPE.SITE_JACKPOT;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = JackpotWinnerInfo.awarded_to_account_id;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update tables Play_Sessions and Accounts to reflect that the card
    //          has been logged off from a terminal.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: both tables were updated
    //      - false: update failed
    // 
    //   NOTES:
    //
    public static Boolean DB_PlaySessionCloseManually(Int64 AccountId,
                                                      Int64 CurrentPlaySessionId, Decimal CurrentPlaySessionFinalBalance,
                                                      Int32 CurrentTerminalId, String CurrentTerminalName, TerminalTypes CurrentTerminalType,
                                                      String CashierOrGUIName,
                                                      Boolean SelectedByUser, Currency NewBalance, String CloseSessionComment)
    {
      MultiPromos.EndSessionInput _end_input;
      MultiPromos.EndSessionOutput _end_output;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _end_input = new MultiPromos.EndSessionInput();
          _end_input.MovementType = MovementType.ManualEndSession;
          _end_input.CashierName = CashierOrGUIName;
          _end_input.AccountId = AccountId;
          _end_input.PlaySessionId = CurrentPlaySessionId;
          _end_input.FinalBalanceSelectedByUserAfterMismatch = SelectedByUser;
          _end_input.CloseSessionComment = CloseSessionComment;

          if (SelectedByUser)
          {
            _end_input.BalanceFromGM = NewBalance;
          }
          else
          {
            _end_input.BalanceFromGM = CurrentPlaySessionFinalBalance;
          }
          _end_input.HasMeters = true;

          if (!MultiPromos.Trx_OnEndCardSession(CurrentTerminalId, CurrentTerminalType, CurrentTerminalName,
                                                _end_input, out _end_output, _db_trx.SqlTransaction))
          {
            Log.Error(_end_output.Message);

            return false;
          }

          _db_trx.Commit();

#if !SQL_BUSINESS_LOGIC
          BucketsUpdate.SetDataAvailable(); /* wakes up the thread*/
#endif

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_PlaySessionCloseManually

    //------------------------------------------------------------------------------
    // PURPOSE: Get the label according of type document
    //          
    // 
    //  PARAMS:
    //      - INPUT:
    //          - HolderIdType 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //     - String with label value
    // 
    //   NOTES:
    //
    public static String GetLabelDocument(ACCOUNT_HOLDER_ID_TYPE HolderIdType)
    {
      String _name;

      _name = IdentificationTypes.GetIdentificationName(HolderIdType);

      if (HolderIdType == ACCOUNT_HOLDER_ID_TYPE.UNKNOWN || String.IsNullOrEmpty(_name))
      {
        _name = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT") + " " + _name;
      }

      return _name.Trim() + ": ";
    }

    public static Boolean AreAntiMoneyLaunderingFieldsCompleted(CardData CardInfo)
    {
      RequiredData _required_fields;
      ENUM_CARDDATA_FIELD _empty_field = ENUM_CARDDATA_FIELD.NONE;

      _required_fields = new RequiredData(PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_ACCOUNT);
      if (_required_fields.IsAnyEmpty(CardInfo, out _empty_field))
      {
        Log.Error(String.Format("AreAntiMoneyLaunderingFieldsCompleted. Incompleted field:[{0}].", _empty_field.ToString()));
        return false;
      }

      if (CardInfo.PlayerTracking.HolderHasBeneficiary)
      {

        _required_fields = new RequiredData(PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_BENEFICIARY);
        if (_required_fields.IsAnyEmpty(CardInfo, out _empty_field))
        {
          Log.Error(String.Format("AreAntiMoneyLaunderingFieldsCompleted. Incompleted field:[{0}].", _empty_field.ToString()));
          return false;
        }
      }

      return true;
    }

    public static Boolean DB_LinkCard(string CardNumber, WCP_CardTypes CardType, Int32 UserId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("LinkCard", SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;

          _sql_cmd.Parameters.Add("@pExternalTrackData", SqlDbType.Char).Value = CardNumber;
          _sql_cmd.Parameters.Add("@pLinkedType", SqlDbType.Int).Value = CardType;
          _sql_cmd.Parameters.Add("@pLinkedId", SqlDbType.Int).Value = UserId;

          _sql_cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    public class PlaySession : ICloneable
    {
      Int64 m_play_session_id;
      DateTime m_started;
      DateTime m_last_activity;
      Currency m_initial_balance;
      Currency m_final_balance;
      Number m_played_count;
      Currency m_played_amount;
      Number m_won_count;
      Currency m_won_amount;
      Boolean m_balance_mismatch;
      Currency m_cash_in;
      Currency m_total_cash_in;
      Currency m_total_played_amount;
      Currency m_total_won_amount;
      Currency m_total_cash_out;

      public Int64 PlaySessionId
      {
        get
        {
          return m_play_session_id;
        }
      }
      public DateTime Started
      {
        get
        {
          return m_started;
        }
      }
      public DateTime LastActivity
      {
        get
        {
          return m_last_activity;
        }
      }
      public Int32 SecondsSinceLastActivity
      {
        get
        {
          TimeSpan _diff;

          _diff = WGDB.Now - m_last_activity;

          return Convert.ToInt32(_diff.TotalSeconds);
        }
      }
      public Currency InitialBalance
      {
        get
        {
          return m_initial_balance;
        }
      }
      public Currency FinalBalance
      {
        get
        {
          return m_final_balance;
        }
      }
      public Number PlayedCount
      {
        get
        {
          return m_played_count;
        }
      }
      public Currency PlayedAmount
      {
        get
        {
          return m_played_amount;
        }
      }
      public Number WonCount
      {
        get
        {
          return m_won_count;
        }
      }
      public Currency WonAmount
      {
        get
        {
          return m_won_amount;
        }
      }

      public Boolean BalanceMismatch
      {
        get
        {
          return m_balance_mismatch;
        }
      }

      public Currency CashIn
      {
        get
        {
          return m_cash_in;
        }
      }

      public Currency TotalCashIn
      {
        get
        {
          return m_total_cash_in;
        }
      }

      public Currency TotalPlayedAmount
      {
        get
        {
          return m_total_played_amount;
      }
      }

      public Currency TotalWonAmount
      {
        get
        {
          return m_total_won_amount;
        }
      }

      public Currency TotalCashOut
      {
        get
        {
          return m_total_cash_out;
        }
      }

      public static PlaySession Read(Int64 PlaySessionId, SqlTransaction SqlTrx)
      {
        PlaySession _ps;
        SqlCommand _sql_cmd;
        String _str_sql;
        SqlDataReader _reader;
        Boolean _finished;

        _ps = new PlaySession();

        _str_sql = "";
        _str_sql += "SELECT   PS_STARTED";
        _str_sql += "       , PS_FINISHED";
        _str_sql += "       , PS_INITIAL_BALANCE";
        _str_sql += "       , PS_FINAL_BALANCE";
        _str_sql += "       , PS_PLAYED_COUNT";
        _str_sql += "       , PS_PLAYED_AMOUNT";
        _str_sql += "       , PS_WON_COUNT";
        _str_sql += "       , PS_WON_AMOUNT";
        _str_sql += "       , 0 ";
        _str_sql += "       , 0 ";
        _str_sql += "       , PS_BALANCE_MISMATCH ";
        _str_sql += "       , PS_CASH_IN ";
        _str_sql += "       , PS_TOTAL_CASH_IN ";
        _str_sql += "       , PS_TOTAL_CASH_OUT + ISNULL (PS_HANDPAYS_AMOUNT, 0) AS PS_TOTAL_CASH_OUT ";
        _str_sql += "  FROM   PLAY_SESSIONS ";
        _str_sql += " WHERE   PS_PLAY_SESSION_ID = @p1";

        _sql_cmd = new SqlCommand(_str_sql);
        _sql_cmd.Parameters.Add("@p1", SqlDbType.BigInt, 8, "PS_PLAY_SESSION_ID").Value = PlaySessionId;
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;

        _finished = false;

        _reader = _sql_cmd.ExecuteReader();
        if (_reader.Read())
        {
          _ps.m_started = (DateTime)_reader[0];

          if (_reader.IsDBNull(1))
          {
            _ps.m_last_activity = _ps.m_started;
          }
          else
          {
            _ps.m_last_activity = (DateTime)_reader[1];
            _finished = true;
          }

          _ps.m_initial_balance = _reader.GetSqlMoney(2);
          _ps.m_final_balance = _reader.GetSqlMoney(3);

          _ps.m_played_count = (Int32)_reader[4];
          _ps.m_played_amount = _reader.GetSqlMoney(5);
          _ps.m_won_count = (Int32)_reader[6];
          _ps.m_won_amount = _reader.GetSqlMoney(7);

          // (8) not used
          // (9) not used

          _ps.m_balance_mismatch = _reader.IsDBNull(10) ? false : (Boolean)_reader[10];
          _ps.m_cash_in = _reader.GetSqlMoney(11);

          _ps.m_total_cash_in = _reader.GetSqlMoney(12);
          _ps.m_total_played_amount = _reader.GetSqlMoney(5);
          _ps.m_total_won_amount = _reader.GetSqlMoney(7);
          _ps.m_total_cash_out = _reader.GetSqlMoney(13);

        }
        _reader.Close();

        _ps.m_play_session_id = PlaySessionId;

        if (!_finished && _ps.m_played_count > 0)
        {
          Object _obj;

          _str_sql = "";
          _str_sql += "SELECT   MAX(PL_DATETIME)";
          _str_sql += "  FROM   PLAYS WITH( INDEX(IX_plays))";
          _str_sql += " WHERE   PL_PLAY_SESSION_ID = @p1";

          _sql_cmd = new SqlCommand(_str_sql);
          _sql_cmd.Parameters.Add("@p1", SqlDbType.BigInt, 8, "PL_PLAY_SESSION_ID").Value = PlaySessionId;
          _sql_cmd.Connection = SqlTrx.Connection;
          _sql_cmd.Transaction = SqlTrx;

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            _ps.m_last_activity = (DateTime)_obj;
          }
          else
          {
            _ps.m_last_activity = WGDB.Now;
          }
        }

        return _ps;
      }

      public object Clone()
      {
        PlaySession _retval = new PlaySession()
        {
          m_play_session_id = m_play_session_id,
          m_started = m_started,
          m_last_activity = m_last_activity,
          m_initial_balance = (Currency)m_initial_balance.Clone(),
          m_final_balance = (Currency)m_final_balance.Clone(),
          m_played_count = m_played_count,
          m_played_amount = (Currency)m_played_amount.Clone(),
          m_won_count = m_won_count,
          m_won_amount = (Currency)m_won_amount.Clone(),
          m_balance_mismatch = m_balance_mismatch,
          m_cash_in = (Currency)m_cash_in.Clone()
        };

        return _retval;
      }
    }

    public class PlayerTrackingData : ICloneable
    {
      public String HolderName;
      public String HolderId;
      public Points CurrentPoints;
      public Points TruncatedPoints;
      public Points CurrentPointsBar;
      public Points CurrentPointsCreditNR;
      public String HolderAddress01;
      public String HolderAddress02;
      public String HolderAddress03;
      public String HolderCity;
      public String HolderZip;
      public String HolderEmail01;
      public String HolderEmail02;
      public String HolderTwitter; //LEM 28-MAR-2013
      public String HolderPhone01;
      public String HolderPhone02;
      public String HolderComments;
      public Int32 HolderGender;
      public Int32 HolderMaritalStatus;
      public DateTime HolderBirthDate;
      public String HolderBirthDateText;
      public Int32 CardLevel;
      public DateTime LastAddedPoints;
      public DateTime CardCreationDate;
      public DateTime LevelUpgradeDate;

      // o	1: Player (CJ)
      // o	2: Service (restaurant)
      // o	3: Service (playable credits)
      // o	4: Service (expired credits)
      public Int32 SubType;

      // RCI 10-NOV-2010: Automatic level change.
      public DateTime HolderLevelEntered;
      public DateTime HolderLevelExpiration;
      public Int32 HolderLevelNotify;

      // JMM 18-NOV-2011: PIN
      public String Pin;
      // RCI 24-NOV-2011
      public Int32 PinFailures;

      // TJG 01-FEB-2012
      public String HolderId1;
      public String HolderId2;
      public Int64 HolderDocumentId1;
      public Int64 HolderDocumentId2;
      public String HolderName1;
      public String HolderName2;
      public String HolderName3;
      public String HolderName4;

      // DDM 03-AUG-2012
      public ACCOUNT_HOLDER_ID_TYPE HolderIdType;

      // ICS 13-FEB-2013
      public DateTime HolderWeddingDate;
      public string HolderWeddingDateText;

      // ACM 13-JUN-2013
      public String HolderOccupation;
      public Int32 HolderOccupationId;
      public String HolderExtNumber;
      public Int64 HolderNationality;
      public Int64 HolderBirthCountry;
      public Int32 HolderFedEntity;
      public Int32 HolderAddressCountry;
      public ADDRESS_VALIDATION HolderAddressValidation;
      public ACCOUNT_HOLDER_ID_TYPE HolderId1Type;
      public ACCOUNT_HOLDER_ID_TYPE HolderId2Type;
      public String HolderId3Type;
      public DocumentList HolderScannedDocs;
      public String HolderId3;
      public Boolean HolderHasBeneficiary;
      public String BeneficiaryName;
      public String BeneficiaryName1;
      public String BeneficiaryName2;
      public String BeneficiaryName3;
      public DateTime BeneficiaryBirthDate;
      public string BeneficiaryBirthDateText;
      public Int32 BeneficiaryGender;
      public String BeneficiaryOccupation;
      public Int32 BeneficiaryOccupationId;
      public ACCOUNT_HOLDER_ID_TYPE BeneficiaryId1Type;
      public String BeneficiaryId1;
      public ACCOUNT_HOLDER_ID_TYPE BeneficiaryId2Type;
      public String BeneficiaryId2;
      public String BeneficiaryId3Type;
      public String BeneficiaryId3;
      public DocumentList BeneficiaryScannedDocs;
      public Boolean HolderIsVIP;
      public Boolean ShowCommentsOnCashier;

      // RMS 20-AUG-2013
      public String PreferredHolderId
      {
        get
        {
          if (!String.IsNullOrEmpty(this.HolderId))
          {
            return this.HolderId;
          }

          if (!String.IsNullOrEmpty(this.HolderId1))
          {
            return this.HolderId1;
          }

          if (!String.IsNullOrEmpty(this.HolderId2))
          {
            return this.HolderId2;
          }

          return String.Empty;
        }
      }
      public ACCOUNT_HOLDER_ID_TYPE PreferredHolderIdType
      {
        get
        {
          if (!String.IsNullOrEmpty(this.HolderId))
          {
            return this.HolderIdType;
          }
          
          if (!String.IsNullOrEmpty(this.HolderId1))
          {
            return this.HolderId1Type;
          }

          if (!String.IsNullOrEmpty(this.HolderId2))
          {
            return this.HolderId2Type;
          }

          return ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
        }
      }

      public object Clone()
      {
        PlayerTrackingData _retval = new PlayerTrackingData
        {
          HolderName = HolderName,
          HolderId = HolderId,
          CurrentPoints = CurrentPoints,
          TruncatedPoints = TruncatedPoints,
          CurrentPointsBar = CurrentPointsBar,
          CurrentPointsCreditNR = CurrentPointsCreditNR,
          HolderAddress01 = HolderAddress01,
          HolderAddress02 = HolderAddress02,
          HolderAddress03 = HolderAddress03,
          HolderCity = HolderCity,
          HolderZip = HolderZip,
          HolderEmail01 = HolderEmail01,
          HolderEmail02 = HolderEmail02,
          HolderTwitter = HolderTwitter,
          HolderPhone01 = HolderPhone01,
          HolderPhone02 = HolderPhone02,
          HolderComments = HolderComments,
          HolderGender = HolderGender,
          HolderMaritalStatus = HolderMaritalStatus,
          HolderBirthDate = HolderBirthDate,
          HolderBirthDateText = HolderBirthDateText,
          CardLevel = CardLevel,
          LastAddedPoints = LastAddedPoints,
          CardCreationDate = CardCreationDate,
          LevelUpgradeDate = LevelUpgradeDate,
          SubType = SubType,
          HolderLevelEntered = HolderLevelEntered,
          HolderLevelExpiration = HolderLevelExpiration,
          HolderLevelNotify = HolderLevelNotify,
          Pin = Pin,
          PinFailures = PinFailures,
          HolderId1 = HolderId1,
          HolderId2 = HolderId2,
          HolderDocumentId1 = HolderDocumentId1,
          HolderDocumentId2 = HolderDocumentId2,
          HolderName1 = HolderName1,
          HolderName2 = HolderName2,
          HolderName3 = HolderName3,
          HolderName4 = HolderName4,
          HolderIdType = HolderIdType,
          HolderWeddingDate = HolderWeddingDate,
          HolderOccupation = HolderOccupation,
          HolderOccupationId = HolderOccupationId,
          HolderExtNumber = HolderExtNumber,
          HolderNationality = HolderNationality,
          HolderBirthCountry = HolderBirthCountry,
          HolderFedEntity = HolderFedEntity,
          HolderAddressCountry = HolderAddressCountry,
          HolderAddressValidation = HolderAddressValidation,
          HolderId1Type = HolderId1Type,
          HolderId2Type = HolderId2Type,
          HolderId3Type = HolderId3Type,
          HolderScannedDocs = HolderScannedDocs,
          HolderId3 = HolderId3,
          HolderHasBeneficiary = HolderHasBeneficiary,
          BeneficiaryName = BeneficiaryName,
          BeneficiaryName1 = BeneficiaryName1,
          BeneficiaryName2 = BeneficiaryName2,
          BeneficiaryName3 = BeneficiaryName3,
          BeneficiaryBirthDate = BeneficiaryBirthDate,
          BeneficiaryGender = BeneficiaryGender,
          BeneficiaryOccupation = BeneficiaryOccupation,
          BeneficiaryOccupationId = BeneficiaryOccupationId,
          BeneficiaryId1Type = BeneficiaryId1Type,
          BeneficiaryId1 = BeneficiaryId1,
          BeneficiaryId2Type = BeneficiaryId2Type,
          BeneficiaryId2 = BeneficiaryId2,
          BeneficiaryId3Type = BeneficiaryId3Type,
          BeneficiaryId3 = BeneficiaryId3,
          BeneficiaryScannedDocs = BeneficiaryScannedDocs,
          HolderIsVIP = HolderIsVIP,
          ShowCommentsOnCashier = ShowCommentsOnCashier,
          BeneficiaryBirthDateText = BeneficiaryBirthDateText,
          HolderWeddingDateText = HolderWeddingDateText
        };

        //LEM 28-MAR-201
        return _retval;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Static function that returns the full name, according to the type format
    //
    //  PARAMS :
    //      - INPUT:
    //            - FullNameType
    //            - Name
    //            - LastName1
    //            - LastName2
    //
    //      - OUTPUT :
    //            - Balance
    //
    // RETURNS :
    //      - Full name
    //        Format:   WITHHOLDING: "last name 1" "last name 2", "name"
    //        Format:   ACCOUNT:     "name" "last name 1" "last name 2" 
    //
    public static String FormatFullName(FULL_NAME_TYPE FullNameType, String Name, String LastName1, String LastName2)
    {
      return FormatFullName(FullNameType, Name, String.Empty, LastName1, LastName2);
    }

    public static String FormatFullName(FULL_NAME_TYPE FullNameType, String Name, String MiddleName, String LastName1, String LastName2)
    {
      return SharedFunctions.FormatFullName(FullNameType, Name, MiddleName, LastName1, LastName2, GeneralParam.GetDictionary());
    }

    public static Boolean IsValidBirthdate(DateTime BirthDate, out Int32 LegalAge)
    {
      Int32 _age;

      LegalAge = GeneralParam.GetInt32("Cashier", "LegalAge", 0);
      _age = WSI.Common.Misc.CalculateAge(BirthDate);

      return (_age >= LegalAge);
    }

    public static Boolean IsValidWeddingdate(DateTime BirthDate, DateTime WeddingDate)
    {
      Int32 _wedding_age;
      DateTime _birthdate_wedding_year;

      if ((BirthDate.Day == 29) && (BirthDate.Month == 2))
      {
        BirthDate = BirthDate.AddDays(-1);
      }

      _wedding_age = WeddingDate.Year - BirthDate.Year;
      _birthdate_wedding_year = new DateTime(WeddingDate.Year, BirthDate.Month, BirthDate.Day);

      if (_birthdate_wedding_year > WeddingDate)
      {
        _wedding_age = _wedding_age - 1;
      }

      return (_wedding_age >= WSI.Common.ValidateFormat.MINIMUM_MARRIAGE_AGE);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check if ID is already exists in DB
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    True Id Exists
    //    False Id does not exists
    // 
    //   NOTES:
    public static bool CheckIdAlreadyExists(String DocumentId, Int64 AccountId, ACCOUNT_HOLDER_ID_TYPE IdType)
    {
      String _sql_query;
      Int32 _count_id;

      if (string.IsNullOrEmpty(DocumentId))
      {
        return false;
      }

      try
      {
        _sql_query = " SELECT   COUNT(*)                              " +
                     "   FROM   ACCOUNTS WITH(INDEX(IX_AC_HOLDER_ID)) " +
                     "  WHERE   AC_HOLDER_ID = @pDocumentId           " +
                     "    AND   AC_HOLDER_ID_TYPE = @pHolderIdType    " +
                     "    AND   AC_ACCOUNT_ID <> @pAccountId          ";

        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sql_query, _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pDocumentId", SqlDbType.NVarChar, 20).Value = DocumentId;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            _cmd.Parameters.Add("@pHolderIdType", SqlDbType.Int).Value = IdType;

            _count_id = (Int32)_cmd.ExecuteScalar();
          }
        }

        return (_count_id > 0);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static long[] GetDuplicateDocumentAccountsIds(String DocumentId, Int64 AccountId, ACCOUNT_HOLDER_ID_TYPE IdType)
    {
      List<long> _retval = new List<long>();
      String _sql_query;

      if (string.IsNullOrEmpty(DocumentId))
      {
        return _retval.ToArray();
      }

      try
      {
        _sql_query = " SELECT   AC_ACCOUNT_ID                         " +
                     "   FROM   ACCOUNTS WITH(INDEX(IX_AC_HOLDER_ID)) " +
                     "  WHERE   AC_HOLDER_ID = @pDocumentId           " +
                     "    AND   AC_HOLDER_ID_TYPE = @pHolderIdType    " +
                     "    AND   AC_ACCOUNT_ID <> @pAccountId          ";

        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sql_query, _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pDocumentId", SqlDbType.NVarChar, 20).Value = DocumentId;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            _cmd.Parameters.Add("@pHolderIdType", SqlDbType.Int).Value = IdType;

            using (IDataReader _rdr = _cmd.ExecuteReader())
            {
              while (_rdr.Read())
              {
                _retval.Add(_rdr.GetInt64(0));
              }
            }
          }
        }

        return _retval.ToArray();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _retval.ToArray();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Static function that checks if there are other accounts with
    //           the same data.
    //
    //  PARAMS :
    //      - INPUT:
    //            - CardData
    //
    //      - OUTPUT :
    //            - Message: popup message if exits duplicate data
    //            - Allow:   True if GeneralParams allows duplicated data, False otherwise
    //
    // RETURNS :
    //      - False if exists accounts with the same data, True otherwise
    //
    //   NOTES: The list of fields to check for duplicated data is retrieved from
    //          GENERAL_PARAMS (Subject Key = "Account.UniqueField"). An account must
    //          contain the exact same data in all fields to be considered duplicate.
    public static bool CheckDuplicateAccounts(CardData CardData, out String Message, out bool Allow)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return CheckDuplicateAccounts(CardData, out Message, out Allow, _db_trx.SqlTransaction);
      }
    }

    public static bool CheckDuplicateAccounts(CardData CardData, out String Message, out bool Allow, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      StringBuilder _duplicate_data;
      Int32 _rc;
      Object _obj;

      _sb = new StringBuilder();
      _duplicate_data = new StringBuilder();
      _rc = 0;

      try
      {
          using (SqlCommand _cmd = new SqlCommand())
        {
            _sb.AppendLine(" SELECT   COUNT (AC_ACCOUNT_ID) ");
            _sb.AppendLine("   FROM   ACCOUNTS ");

            // Avoid selecting the same account we are checking
            _sb.AppendLine(" WHERE AC_ACCOUNT_ID <> @pAccountId ");
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = CardData.AccountId;

            // Add unique fields to WHERE clause
            if (IsUniqueField("Name3"))
            {
              _sb.AppendLine(" AND AC_HOLDER_NAME3 = @pHolderName3 ");
              _cmd.Parameters.Add("@pHolderName3", SqlDbType.NVarChar, 50).Value = CardData.PlayerTracking.HolderName3;
              _duplicate_data.AppendLine("-" + Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + ": " + CardData.PlayerTracking.HolderName3);
            }

            if (IsUniqueField("Name4"))
            {
              _sb.AppendLine(" AND AC_HOLDER_NAME4 = @pHolderName4 ");
              _cmd.Parameters.Add("@pHolderName4", SqlDbType.NVarChar, 50).Value = CardData.PlayerTracking.HolderName4;
              _duplicate_data.AppendLine("-" + Resource.String("STR_FRM_PLAYER_EDIT_NAME4") + ": " + CardData.PlayerTracking.HolderName4);
            }

            if (IsUniqueField("Name1"))
            {
              _sb.AppendLine(" AND AC_HOLDER_NAME1 = @pHolderName1 ");
              _cmd.Parameters.Add("@pHolderName1", SqlDbType.NVarChar, 50).Value = CardData.PlayerTracking.HolderName1;
              _duplicate_data.AppendLine("-" + Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1") + ": " + CardData.PlayerTracking.HolderName1);
            }

            if (IsUniqueField("Name2"))
            {
              _sb.AppendLine(" AND AC_HOLDER_NAME2 = @pHolderName2 ");
              _cmd.Parameters.Add("@pHolderName2", SqlDbType.NVarChar, 50).Value = CardData.PlayerTracking.HolderName2;
              _duplicate_data.AppendLine("-" + Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2") + ": " + CardData.PlayerTracking.HolderName2);
            }

            if (IsUniqueField("BirthDate"))
            {
              _sb.AppendLine(" AND AC_HOLDER_BIRTH_DATE = @pHolderBirthDate ");
              if (CardData.PlayerTracking.HolderBirthDate == DateTime.MinValue)
              {
                _cmd.Parameters.Add("@pHolderBirthDate", SqlDbType.DateTime).Value = DBNull.Value;
              }
              else
              {
                _cmd.Parameters.Add("@pHolderBirthDate", SqlDbType.DateTime).Value = CardData.PlayerTracking.HolderBirthDate;
              }

              _duplicate_data.AppendLine("-" + Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE") + ": " + CardData.PlayerTracking.HolderBirthDate.ToShortDateString());
            }

            if (IsUniqueField("Gender"))
            {
              _sb.AppendLine(" AND AC_HOLDER_GENDER = @pHolderGender ");
              _cmd.Parameters.Add("@pHolderGender", SqlDbType.Int).Value = CardData.PlayerTracking.HolderGender;
              _duplicate_data.Append("-" + Resource.String("STR_UC_PLAYER_TRACK_GENDER") + ": ");

              if (CardData.PlayerTracking.HolderGender == 1)
              {
                _duplicate_data.AppendLine(Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE"));
              }
              else
              {
                _duplicate_data.AppendLine(Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE"));
              }
            }

            if (IsUniqueField("Document"))
            {
              _sb.AppendLine(" AND (AC_HOLDER_ID = @pHolderId AND AC_HOLDER_ID_TYPE = @pHolderIdType) ");
              _cmd.Parameters.Add("@pHolderId", SqlDbType.NVarChar, 20).Value = CardData.PlayerTracking.HolderId;
              _cmd.Parameters.Add("@pHolderIdType", SqlDbType.Int).Value = CardData.PlayerTracking.HolderIdType;
              _duplicate_data.AppendLine("-" + Resource.String("STR_FRM_PLAYER_EDIT_DOCUMENT") + ": " + CardData.PlayerTracking.HolderId);
            }

            if (IsUniqueField("Phone1"))
            {
              _sb.AppendLine(" AND AC_HOLDER_PHONE_NUMBER_01 = @pHolderPhone1 ");
              _cmd.Parameters.Add("@pHolderPhone1", SqlDbType.NVarChar, 20).Value = CardData.PlayerTracking.HolderPhone01;
              _duplicate_data.AppendLine("-" + Resource.String("STR_UC_PLAYER_TRACK_PHONE_01") + ": " + CardData.PlayerTracking.HolderPhone01);
            }

            // The SQL command is processed only if we added more parameters than account ID
            if (_cmd.Parameters.Count > 1)
            {
              _cmd.CommandText = _sb.ToString();
              _cmd.Connection = SqlTrx.Connection;
              _cmd.Transaction = SqlTrx;

              _obj = _cmd.ExecuteScalar();

              if (_obj == null || _obj == DBNull.Value)
              {
                _rc = 0;
              }
              else
              {
                _rc = (Int32)_obj;
              }
            }
          }

          if (_rc == 0)
          {
            Message = "";
            Allow = true;

            return true;
          }

          if (GeneralParam.GetBoolean("Account", "AllowDuplicate"))
          {
            Message = Resource.String("STR_DUPLICATE_ACCOUNTS_CONFIRMATION", _duplicate_data.ToString());
            Message = Message.Replace("\\r\\n", "\r\n");
            Allow = true;
          }
          else
          {
            Message = Resource.String("STR_DUPLICATE_ACCOUNTS_WARNING", _duplicate_data.ToString());
            Message = Message.Replace("\\r\\n", "\r\n");
            Allow = false;
          }

          return false;
        }
      catch (Exception _ex)
      {
        Message = Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING");
        Allow = true;
        Log.Exception(_ex);
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Static function that checks if there are other accounts with
    //           the same data.
    //
    //  PARAMS :
    //      - INPUT:
    //            - CardData
    //
    //      - OUTPUT :
    //            - Message: popup message if exits duplicate data
    //            - Allow:   True if GeneralParams allows duplicated data, False otherwise
    //
    // RETURNS :
    //      - False if exists accounts with the same data, True otherwise
    //
    //   NOTES: The list of fields to check for duplicated data is retrieved from
    //          GENERAL_PARAMS (Subject Key = "Account.UniqueField"). An account must
    //          contain the exact same data in all fields to be considered duplicate.
    public static bool CheckDuplicateAccountsReception(CardData CardData, out String Message, out bool Allow, bool _bSave = false)
    {
      StringBuilder _sb;
      StringBuilder _duplicate_data;
      Int32 _rc;
      Object _obj;

      _sb = new StringBuilder();
      _duplicate_data = new StringBuilder();
      _rc = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand())
          {
            _sb.AppendLine(" SELECT   COUNT (AC_ACCOUNT_ID) ");
            _sb.AppendLine("   FROM   ACCOUNTS ");

            // Avoid selecting the same account we are checking
            _sb.AppendLine(" WHERE AC_ACCOUNT_ID <> @pAccountId ");
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = CardData.AccountId;

            _sb.AppendLine(" AND AC_HOLDER_NAME1 = @pHolderName1 ");
            _cmd.Parameters.Add("@pHolderName1", SqlDbType.NVarChar, 50).Value = CardData.PlayerTracking.HolderName1;
            _duplicate_data.AppendLine("-" + Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1") + ": " + CardData.PlayerTracking.HolderName1);
            _sb.AppendLine(" AND AC_HOLDER_BIRTH_DATE = @pHolderBirthDate ");
            if (CardData.PlayerTracking.HolderBirthDate == DateTime.MinValue || CardData.PlayerTracking.HolderBirthDate.Year < 1753 || CardData.PlayerTracking.HolderBirthDate == null)
            {
              throw new Exception("CardData.PlayerTracking.HolderBirthDate: " + CardData.PlayerTracking.HolderBirthDate.ToString());
              //_cmd.Parameters.Add("@pHolderBirthDate", SqlDbType.DateTime).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pHolderBirthDate", SqlDbType.DateTime).Value = CardData.PlayerTracking.HolderBirthDate;
            }
            
            _duplicate_data.AppendLine("-" + Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE") + ": " + CardData.PlayerTracking.HolderBirthDate.ToShortDateString());
            _sb.AppendLine(" AND AC_HOLDER_GENDER = @pHolderGender ");
            _cmd.Parameters.Add("@pHolderGender", SqlDbType.Int).Value = CardData.PlayerTracking.HolderGender;
            _duplicate_data.Append("-" + Resource.String("STR_UC_PLAYER_TRACK_GENDER") + ": ");

            if (CardData.PlayerTracking.HolderGender == 1)
            {
              _duplicate_data.AppendLine(Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE"));
            }
            else
            {
              _duplicate_data.AppendLine(Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE"));
            }

            _sb.AppendLine(" AND AC_HOLDER_BIRTH_country = @pHolderBirthCountry ");
            if (CardData.PlayerTracking.HolderBirthDate == DateTime.MinValue)
            {
              _cmd.Parameters.Add("@pHolderBirthCountry", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pHolderBirthCountry", SqlDbType.Int).Value = CardData.PlayerTracking.HolderBirthCountry;
            }

            _duplicate_data.AppendLine(string.Format("-{0}: {1}", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_COUNTRY"), CardData.CountriesString(Convert.ToInt32(CardData.PlayerTracking.HolderBirthCountry))));

            // The SQL command is processed only if we added more parameters than account ID
            if (_cmd.Parameters.Count > 1)
            {
              _cmd.CommandText = _sb.ToString();
              _cmd.Connection = _db_trx.SqlTransaction.Connection;
              _cmd.Transaction = _db_trx.SqlTransaction;

              _obj = _cmd.ExecuteScalar();

              if (_obj == null || _obj == DBNull.Value)
              {
                _rc = 0;
              }
              else
              {
                _rc = (Int32)_obj;
              }
            }
          }

          if (_rc == 0)
          {
            Message = "";
            Allow = true;

            return true;
          }

          if (!_bSave)
          {
            Message = Resource.String("STR_DUPLICATE_ACCOUNTS_WARNING", _duplicate_data.ToString());
            Message = Message.Replace("\\r\\n", "\r\n");
            Allow = false;
          }
          else
          {
          if (GeneralParam.GetBoolean("Account", "AllowDuplicate"))
          {
            Message = Resource.String("STR_DUPLICATE_ACCOUNTS_CONFIRMATION", _duplicate_data.ToString());
            Message = Message.Replace("\\r\\n", "\r\n");
            Allow = true;
          }
          else
          {
            Message = Resource.String("STR_DUPLICATE_ACCOUNTS_WARNING", _duplicate_data.ToString());
            Message = Message.Replace("\\r\\n", "\r\n");
            Allow = false;
          }
          }

          return false;
        }
      }
      catch (Exception _ex)
      {
        Message = Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING");
        Allow = true;
        Log.Exception(_ex);
      }

      return true;
    }

    private static Boolean IsUniqueField(String Field)
    {
      if (!GeneralParam.GetBoolean("Account.UniqueField", Field, true))
      {
        return false;
      }

      if (!GeneralParam.GetBoolean("Account.VisibleField", Field, true))
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Check the card belong to the site (and MultisiteId)
    //
    //  PARAMS :
    //      - INPUT:
    //            - CardSiteId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True  : If CardSiteId match with a valid SiteId ( local or multisite )
    //      - False : Otherwise.
    //
    //   NOTES:
    //
    //   The method doesn't check Site.MultiSiteMember to allow work with 
    //   other cards in 'Singlesite' mode. For developement or exhibitions...
    //   
    public static Boolean CheckTrackdataSiteId(WSI.Common.CardTrackData CardTrackData)
      {


      return CardTrackData.IsCorrectTrackData;
    }

    public Boolean LoadAccountDocuments()
    {
      DocumentList _images_list;
      ACCOUNT_SCANNED_OWNER _account_scanned_owner;
      IDocument _doc_image_old;

      try
      {
        this.PlayerTracking.HolderScannedDocs = new DocumentList();
        this.PlayerTracking.BeneficiaryScannedDocs = new DocumentList();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!AccountDocuments.Load(this.AccountId, out _images_list, _db_trx.SqlTransaction))
          {
            return false;
          }
        }

        foreach (IDocument _doc_image in _images_list)
        {

          if (GetInfoDocName(_doc_image.Name, out  _account_scanned_owner))
          {
            switch (_account_scanned_owner)
            {
              case ACCOUNT_SCANNED_OWNER.HOLDER:
                this.PlayerTracking.HolderScannedDocs.Add(_doc_image);
                break;

              case ACCOUNT_SCANNED_OWNER.BENEFICIARY:
                this.PlayerTracking.BeneficiaryScannedDocs.Add(_doc_image);
                break;

              default:
                break;
            }
          }
        }

        //
        // To support the old way of scanned documents - If exist a Document in the old way (Different name format than XXX_Y_Random Number)
        //

        //Holder
        if (this.PlayerTracking.HolderScannedDocs.Count == 0 &&
            !String.IsNullOrEmpty(this.PlayerTracking.HolderId3Type))
        {
          ReadOnlyDocument _new_doc;
          String _new_name;

          _images_list.Find("holder_id3_1", out _doc_image_old);
          if (_doc_image_old != null)
          {
            // Set the new name
            GenerateDocName(ACCOUNT_SCANNED_OWNER.HOLDER, this.PlayerTracking.HolderId3Type, out _new_name);

            // Clone the content (Image)
            byte[] _new_content = new byte[_doc_image_old.Content.Length];
            _doc_image_old.Content.CopyTo(_new_content, 0);

            // Set the document
            _new_doc = new ReadOnlyDocument(_new_name, _new_content);

            // Add the document in the list
            this.PlayerTracking.HolderScannedDocs.Add(_new_doc);
          }

          _images_list.Find("holder_id3_2", out _doc_image_old);
          if (_doc_image_old != null)
          {
            // Set the new name
            GenerateDocName(ACCOUNT_SCANNED_OWNER.HOLDER, this.PlayerTracking.HolderId3Type, out _new_name);

            // Clone the content (Image)
            byte[] _new_content = new byte[_doc_image_old.Content.Length];
            _doc_image_old.Content.CopyTo(_new_content, 0);

            // Set the document
            _new_doc = new ReadOnlyDocument(_new_name, _new_content);

            // Add the document in the list
            this.PlayerTracking.HolderScannedDocs.Add(_new_doc);
          }
        }


        // Beneficiary
        if (this.PlayerTracking.BeneficiaryScannedDocs.Count == 0 &&
          !String.IsNullOrEmpty(this.PlayerTracking.BeneficiaryId3Type))
        {
          ReadOnlyDocument _new_doc;
          String _new_name;

          _images_list.Find("beneficiary_id3_1", out _doc_image_old);
          if (_doc_image_old != null)
          {
            // Set the new name
            GenerateDocName(ACCOUNT_SCANNED_OWNER.BENEFICIARY, this.PlayerTracking.BeneficiaryId3Type, out _new_name);

            // Clone the content (Image)
            byte[] _new_content = new byte[_doc_image_old.Content.Length];
            _doc_image_old.Content.CopyTo(_new_content, 0);

            // Set the document
            _new_doc = new ReadOnlyDocument(_new_name, _new_content);

            // Add the document in the list
            this.PlayerTracking.BeneficiaryScannedDocs.Add(_new_doc);
          }

          _images_list.Find("beneficiary_id3_2", out _doc_image_old);
          if (_doc_image_old != null)
          {
            // Set the new name
            GenerateDocName(ACCOUNT_SCANNED_OWNER.BENEFICIARY, this.PlayerTracking.BeneficiaryId3Type, out _new_name);

            // Clone the content (Image)
            byte[] _new_content = new byte[_doc_image_old.Content.Length];
            _doc_image_old.Content.CopyTo(_new_content, 0);

            // Set the document
            _new_doc = new ReadOnlyDocument(_new_name, _new_content);

            // Add the document in the list
            this.PlayerTracking.BeneficiaryScannedDocs.Add(_new_doc);
          }
        }
        //End: To support the old way of scanned documents 

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean DB_VirtualCardGetAllData(String TerminalIdentifier, AccountType AccountType, CardData Card)
    {
      Int64 _account_id;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _account_id = Accounts.GetOrCreateVirtualAccount(TerminalIdentifier, AccountType, _db_trx.SqlTransaction);

        if (_account_id == 0)
        {
          Log.Error(String.Format("DB_VirtualCardGetAllData: GetOrCreateVirtualAccount. Error getting account info from Terminal {0}, Type {1}.",
                                  TerminalIdentifier, AccountType));

          return false;
        }

        _db_trx.Commit();
      }

      if (!CardData.DB_CardGetAllData(_account_id, Card))
      {
        Log.Error(String.Format("DB_VirtualCardGetAllData: DB_CardGetAllData. Error loading Account data from Id {0}.", _account_id));

        return false;
      }

      return true;
    }

    public static Boolean DB_CardGetPersonalData(long AccountId, CardData _card_data)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_CardGetPersonalData(AccountId, _card_data, _db_trx.SqlTransaction);
      }
    }

    public static Boolean DB_CardGetPersonalData(int DocumentType, string DocumentNumber, CardData _card_data)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_CardGetPersonalData(DocumentType, DocumentNumber, _card_data, _db_trx.SqlTransaction);
      }
    }

    public static Boolean DB_CardGetPersonalData(int DocumentType, string DocumentNumber, CardData CardInfo, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        // Get player tracking data
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   ISNULL(AC_HOLDER_NAME, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ADDRESS_01, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ADDRESS_02, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ADDRESS_03, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_CITY, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ZIP, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_EMAIL_01, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_EMAIL_02, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_COMMENTS, '')");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_GENDER, 0)");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_MARITAL_STATUS, 0)");
        _sb.AppendLine("       , AC_HOLDER_BIRTH_DATE");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_ID, '')");
        _sb.AppendLine("       , ISNULL(AC_POINTS, 0)"); // AMF 03-FEB-2016
        _sb.AppendLine("       , ISNULL (AC_HOLDER_LEVEL, 1)");
        _sb.AppendLine("       , AC_HOLDER_LEVEL_ENTERED");
        _sb.AppendLine("       , AC_HOLDER_LEVEL_EXPIRATION");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_LEVEL_NOTIFY, 0)");
        _sb.AppendLine("       , ISNULL (AC_PIN, '')");
        _sb.AppendLine("       , ISNULL (AC_PIN_FAILURES, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID1, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID2, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_DOCUMENT_ID1, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_DOCUMENT_ID2, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NAME1, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NAME2, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NAME3, '')");
        _sb.AppendLine("       , AC_HOLDER_ID_TYPE "); // DDM 03-AUG-2012
        _sb.AppendLine("       , AC_HOLDER_WEDDING_DATE"); // ICS 13-FEB-2013 
        _sb.AppendLine("       , ISNULL (AC_HOLDER_TWITTER_ACCOUNT, '')"); // LEM 28-MAR-2013 
        _sb.AppendLine("       , ISNULL (AC_HOLDER_EXT_NUM, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NATIONALITY, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_BIRTH_COUNTRY, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_FED_ENTITY, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID1_TYPE, 1)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID2_TYPE, 2)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID3_TYPE, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_ID3, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_HAS_BENEFICIARY, 0)");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_OCCUPATION_ID, 0)");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_NAME, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_NAME1, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_NAME2, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_NAME3, '')");
        _sb.AppendLine("       , AC_BENEFICIARY_BIRTH_DATE ");
        _sb.AppendLine("       , ISNULL(AC_BENEFICIARY_GENDER, 0)");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_OCCUPATION_ID, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID1_TYPE, 1)");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID1, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID2_TYPE, 2)");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID2, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID3_TYPE, '')");
        _sb.AppendLine("       , ISNULL (AC_BENEFICIARY_ID3, '')");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_IS_VIP, 0)");
        _sb.AppendLine("       , AC_BLOCKED ");
        _sb.AppendLine("       , AC_BLOCK_DESCRIPTION ");
        _sb.AppendLine("       , ISNULL (AC_HOLDER_NAME4, '') "); // 58
        _sb.AppendLine("       , ISNULL (AC_SHOW_COMMENTS_ON_CASHIER, 0)");
        _sb.AppendLine("       , AC_BLOCK_REASON ");
        _sb.AppendLine("       , AC_HOLDER_ADDRESS_COUNTRY"); // SGB 12-AGO-2015
        _sb.AppendLine("       , AC_HOLDER_ADDRESS_VALIDATION "); // AMF 07-SEP-2015 -> 62
        _sb.AppendLine("       , AC_ACCOUNT_ID ");
        _sb.AppendLine("  FROM   ACCOUNTS ");
        _sb.AppendLine(" WHERE   AC_HOLDER_ID_TYPE = @pDocumentType ");
        _sb.AppendLine("   AND   AC_HOLDER_ID = @pDocumentNumber ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DocumentType;
          _cmd.Parameters.Add("@pDocumentNumber", SqlDbType.NVarChar).Value = DocumentNumber;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            CardInfo.PlayerTracking = new PlayerTrackingData();
            CardInfo.PlayerTracking.HolderName = ((String)_reader[0]).ToUpperInvariant();
            CardInfo.PlayerTracking.HolderAddress01 = (String)_reader[1];
            CardInfo.PlayerTracking.HolderAddress02 = (String)_reader[2];
            CardInfo.PlayerTracking.HolderAddress03 = (String)_reader[3];
            CardInfo.PlayerTracking.HolderCity = (String)_reader[4];
            CardInfo.PlayerTracking.HolderZip = (String)_reader[5];
            CardInfo.PlayerTracking.HolderEmail01 = (String)_reader[6];
            CardInfo.PlayerTracking.HolderEmail02 = (String)_reader[7];
            CardInfo.PlayerTracking.HolderPhone01 = (String)_reader[8];
            CardInfo.PlayerTracking.HolderPhone02 = (String)_reader[9];
            CardInfo.PlayerTracking.HolderComments = (String)_reader[10];
            CardInfo.PlayerTracking.HolderGender = (Int32)_reader[11];
            CardInfo.PlayerTracking.HolderMaritalStatus = (Int32)_reader[12];

            if (_reader.IsDBNull(13))
            {
              CardInfo.PlayerTracking.HolderBirthDate = DateTime.MinValue;
            }
            else
            {
              CardInfo.PlayerTracking.HolderBirthDate = (DateTime)_reader[13];
            }

            CardInfo.PlayerTracking.HolderBirthDateText = CardInfo.PlayerTracking.HolderBirthDate.ToShortDateString();
            CardInfo.PlayerTracking.HolderId = ((String)_reader[14]).ToUpperInvariant();
            CardInfo.PlayerTracking.CurrentPoints = _reader.GetDecimal(15);
            CardInfo.PlayerTracking.TruncatedPoints = Math.Truncate(CardInfo.PlayerTracking.CurrentPoints);
            CardInfo.PlayerTracking.CardLevel = (Int32)_reader[16];

            // RCI 10-NOV-2010: Automatic level change.
            CardInfo.PlayerTracking.HolderLevelEntered = _reader.IsDBNull(17) ? DateTime.MinValue : _reader.GetDateTime(17);
            CardInfo.PlayerTracking.HolderLevelExpiration = _reader.IsDBNull(18) ? DateTime.MinValue : _reader.GetDateTime(18);
            CardInfo.PlayerTracking.HolderLevelNotify = _reader.GetInt32(19);

            // JMM 18-NOV-2011: PIN
            CardInfo.PlayerTracking.Pin = _reader.GetString(20);
            // RCI 24-NOV-2011
            CardInfo.PlayerTracking.PinFailures = _reader.GetInt32(21);

            if (CardInfo.PlayerTracking.HolderName == "")
            {
              CardInfo.PlayerTracking.CardLevel = 0;
              CardInfo.PlayerTracking.HolderLevelEntered = DateTime.MinValue;
              CardInfo.PlayerTracking.HolderLevelExpiration = DateTime.MinValue;
              CardInfo.PlayerTracking.HolderLevelNotify = 0;
            }

            // TJG 01-FEB-2012
            //CardInfo.PlayerTracking.HolderId1 = "";
            //CardInfo.PlayerTracking.HolderId2 = "";
            //CardInfo.PlayerTracking.HolderDocumentId1 = 0;
            //CardInfo.PlayerTracking.HolderDocumentId2 = 0;
            //CardInfo.PlayerTracking.HolderName1 = "";
            //CardInfo.PlayerTracking.HolderName2 = "";
            //CardInfo.PlayerTracking.HolderName3 = "";

            CardInfo.PlayerTracking.HolderId1 = ((String)_reader[22]).ToUpperInvariant();      // AC_HOLDER_ID1
            CardInfo.PlayerTracking.HolderId2 = ((String)_reader[23]).ToUpperInvariant();      // AC_HOLDER_ID2
            CardInfo.PlayerTracking.HolderDocumentId1 = _reader.GetInt64(24);                  // AC_HOLDER_DOCUMENT_ID1
            CardInfo.PlayerTracking.HolderDocumentId2 = _reader.GetInt64(25);                  // AC_HOLDER_DOCUMENT_ID2
            CardInfo.PlayerTracking.HolderName1 = ((String)_reader[26]).ToUpperInvariant();    // AC_HOLDER_NAME1
            CardInfo.PlayerTracking.HolderName2 = ((String)_reader[27]).ToUpperInvariant();    // AC_HOLDER_NAME2
            CardInfo.PlayerTracking.HolderName3 = ((String)_reader[28]).ToUpperInvariant();    // AC_HOLDER_NAME3
            CardInfo.PlayerTracking.HolderName4 = ((String)_reader[58]).ToUpperInvariant();    // AC_HOLDER_NAME4            
            CardInfo.PlayerTracking.HolderIdType = (ACCOUNT_HOLDER_ID_TYPE)_reader.GetInt32(29); // AC_HOLDER_ID_TYPE

            // ICS 13-FEB-2013: Wedding Date
            if (_reader.IsDBNull(30))
            {
              CardInfo.PlayerTracking.HolderWeddingDate = DateTime.MinValue;
            }
            else
            {
              CardInfo.PlayerTracking.HolderWeddingDate = _reader.GetDateTime(30);
            }
            CardInfo.PlayerTracking.HolderWeddingDateText = CardInfo.PlayerTracking.HolderWeddingDate.ToShortDateString();

            CardInfo.PlayerTracking.HolderTwitter = (String)_reader[31];
            CardInfo.PlayerTracking.HolderExtNumber = (String)_reader[32];
            CardInfo.PlayerTracking.HolderNationality = (Int32)_reader[33];
            CardInfo.PlayerTracking.HolderBirthCountry = (Int32)_reader[34];
            CardInfo.PlayerTracking.HolderFedEntity = (Int32)_reader[35];

            if (_reader.IsDBNull(61) || (Int32)_reader[61] == -1) // SGB 12-AGO-2015
            {
              int _country_id;

              if (Countries.ISO2ToCountryId(Resource.CountryISOCode2, out _country_id))
              {
                CardInfo.PlayerTracking.HolderAddressCountry = _country_id;
              }
            }
            else
            {
              CardInfo.PlayerTracking.HolderAddressCountry = (Int32)_reader[61];
            }

            CardInfo.PlayerTracking.HolderAddressValidation = (ADDRESS_VALIDATION)_reader[62];
            CardInfo.PlayerTracking.HolderId1Type = (ACCOUNT_HOLDER_ID_TYPE)_reader[36];
            CardInfo.PlayerTracking.HolderId2Type = (ACCOUNT_HOLDER_ID_TYPE)_reader[37];
            CardInfo.PlayerTracking.HolderId3Type = (String)_reader[38];
            CardInfo.PlayerTracking.HolderId3 = (String)_reader[39];
            CardInfo.PlayerTracking.HolderHasBeneficiary = (Boolean)_reader[40];
            CardInfo.PlayerTracking.HolderOccupationId = (Int32)_reader[41];
            CardInfo.PlayerTracking.BeneficiaryName = ((String)_reader[42]).ToUpperInvariant();
            CardInfo.PlayerTracking.BeneficiaryName1 = ((String)_reader[43]).ToUpperInvariant();
            CardInfo.PlayerTracking.BeneficiaryName2 = ((String)_reader[44]).ToUpperInvariant();
            CardInfo.PlayerTracking.BeneficiaryName3 = ((String)_reader[45]).ToUpperInvariant();

            if (_reader.IsDBNull(46))
            {
              CardInfo.PlayerTracking.BeneficiaryBirthDate = DateTime.MinValue;
            }
            else
            {
              CardInfo.PlayerTracking.BeneficiaryBirthDate = (DateTime)_reader.GetDateTime(46);
            }

            CardInfo.PlayerTracking.BeneficiaryBirthDateText = CardInfo.PlayerTracking.BeneficiaryBirthDate.ToShortDateString();

            CardInfo.PlayerTracking.BeneficiaryGender = (Int32)_reader[47];
            CardInfo.PlayerTracking.BeneficiaryOccupationId = (Int32)_reader[48];
            CardInfo.PlayerTracking.BeneficiaryId1Type = (ACCOUNT_HOLDER_ID_TYPE)_reader[49];
            CardInfo.PlayerTracking.BeneficiaryId1 = (String)_reader[50];
            CardInfo.PlayerTracking.BeneficiaryId2Type = (ACCOUNT_HOLDER_ID_TYPE)_reader[51];
            CardInfo.PlayerTracking.BeneficiaryId2 = (String)_reader[52];
            CardInfo.PlayerTracking.BeneficiaryId3Type = (String)_reader[53];
            CardInfo.PlayerTracking.BeneficiaryId3 = (String)_reader[54];
            CardInfo.PlayerTracking.HolderIsVIP = (Boolean)_reader[55];    // AC_HOLDER_IS_VIP
            CardInfo.PlayerTracking.ShowCommentsOnCashier = (Boolean)_reader[59];

            CardInfo.Blocked = (Boolean)_reader[56];
            CardInfo.BlockDescription = (_reader.IsDBNull(57)) ? "" : _reader.GetString(57);

            CardInfo.BlockReason = (AccountBlockReason)_reader[60]; // AC_BLOCK_REASON
            CardInfo.AccountId = (Int64)_reader[63];
          } // using _reader
        } // using _cmd

        return true;
      } // try
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }


    //public static Boolean DB_CarGetLastBankOrCashOutCardMovement(Int64 AccountId, DateTime IniDate, out DataTable CashierMovements)
    //{

    //  try
    //  {
    //    using (DB_TRX _db_trx = new DB_TRX())
    //    {
    //      StringBuilder _sb;

    //      _sb = new StringBuilder();
    //      _sb.AppendLine("    SELECT TOP 1                                         ");
    //      _sb.AppendLine("            CM_INITIAL_BALANCE                           ");
    //      _sb.AppendLine("          , CM_SUB_AMOUNT                                ");
    //      _sb.AppendLine("          , CM_ADD_AMOUNT                                ");
    //      _sb.AppendLine("          , CM_FINAL_BALANCE                             ");
    //      _sb.AppendLine("          , CM_TYPE                                      ");
    //      _sb.AppendLine("          , CM_DATE                                      ");
    //      _sb.AppendLine("    FROM   CASHIER_MOVEMENTS                             ");
    //      _sb.AppendLine("    WITH (INDEX (IX_cm_date_type))                       "); // cm_date (ASC), cm_type (ASC)
    //      _sb.AppendLine("    WHERE  CM_ACCOUNT_ID = @pAccountId                   ");
    //      _sb.AppendLine("    AND     CM_DATE >= @pIniData                         ");

    //      _sb.AppendLine("    AND    (CM_TYPE = @pTypeCashOut                      ");
    //      _sb.AppendLine("    OR      CM_TYPE = @pTypeBank                         ");
    //      _sb.AppendLine("    OR      CM_TYPE = @pTypeCredit                       ");
    //      _sb.AppendLine("    OR      CM_TYPE = @pTypeDebit)                       ");

    //      _sb.AppendLine("    ORDER BY CM_DATE DESC                                ");


    //      SqlTransaction _sql_trx = _db_trx.SqlTransaction;

    //      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx))
    //        _cmd.Parameters.Add("@pIniData", SqlDbType.DateTime).Value = IniDate;
    //        _cmd.Parameters.Add("@pTypeCashOut", SqlDbType.Int).Value = (Int32)CASHIER_MOVEMENT.CASH_OUT;
    //        _cmd.Parameters.Add("@pTypeBank", SqlDbType.Int).Value = (Int32)CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1;
    //       {
    //        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
    //       _cmd.Parameters.Add("@pTypeCredit", SqlDbType.Int).Value = (Int32)CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1;
    //        _cmd.Parameters.Add("@pTypeDebit", SqlDbType.Int).Value = (Int32)CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1;

    //        using (SqlDataAdapter _adapter = new SqlDataAdapter(_cmd))
    //        {
    //          CashierMovements = new DataTable();
    //          _adapter.Fill(CashierMovements);
    //          return true;
    //        }
    //      }
    //    }
    //  }
    //  catch (SqlException _sql_ex)
    //  {
    //    Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);
    //  }
    //  CashierMovements = new DataTable();
    //  return false;
    //}

    //------------------------------------------------------------------------------
    // PURPOSE : Check if the card is possibly used as Automatic Cashier
    // PARAMS :
    //      - OUTPUT :
    //          - suspiciousActivity: True if Suspicious activity has ben be detected; False otherwise
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //public bool CheckSuspiciousActivity(out Boolean suspiciousActivity)
    //{
    //  suspiciousActivity = false;

    //  if (GeneralParam.GetDecimal("Cashier.Withdrawal", "FromBankCardRechargeWithoutPlay.PCT") == 0 ||
    //    !this.SuspiciousActivityDetected())
    //  {
    //    return true;
    //  }
    //  DataTable _cashierMovements;
    //  Int32 _movement_type;

    //  if (!DB_CarGetLastBankOrCashOutCardMovement(this.AccountId, DateTime.Now.AddHours(-12), out _cashierMovements))
    //  {
    //    return false;
    //  }
    //  if (_cashierMovements.Rows.Count == 0)
    //  {
    //    return true;
    //  }

    //  _movement_type = (Int32)_cashierMovements.Rows[0]["CM_TYPE"];
    //  if (
    //    _movement_type == (Int32)CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1 ||
    //    _movement_type == (Int32)CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1 ||
    //    _movement_type == (Int32)CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1) 

    //    suspiciousActivity = true;

    //  return true;
    //}

    //------------------------------------------------------------------------------
    // PURPOSE : Check if the card is possibly used as Automatic Cashier
    //
    // RETURNS :
    //          - True:               If Suspicious activity has ben be detected;
    //          - False:              Otherwise
    //private bool SuspiciousActivityDetected()
    //{
    //  return this.CurrentPlaySession.WonAmount == 0;
    //}


    public object Clone()
    {
      var _retval = new CardData();

      _retval.AccountId = this.AccountId;
      _retval.Blocked = this.Blocked;
      _retval.BlockReason = BlockReason;    // RCI 22-OCT-2010
      _retval.BlockDescription = BlockDescription;            // JBP 06-MAY-2013
      _retval.NotValidBefore = NotValidBefore;
      _retval.NotValidAfter = NotValidAfter;
      _retval.CurrentBalance = CurrentBalance;
      _retval.CardReplacementCount = CardReplacementCount;          //AMF 01-JUL-2014
      _retval.IsRecycled = IsRecycled;                  //JCM 18-APR-2012 Flag: Card Recycled
      _retval.TrackData = TrackData;
      _retval.MaxDevolution = MaxDevolution;
      _retval.MaxDevolutionForChips = MaxDevolutionForChips;
      _retval.Nr1Withhold = Nr1Withhold;
      _retval.Deposit = Deposit;

      _retval.Cancellable = (CancellableData)Cancellable.Clone();
      _retval.CardPaid = CardPaid;
      _retval.WonLockExists = WonLockExists;
      _retval.NonRedeemableWonLock = (Currency)NonRedeemableWonLock.Clone();
      _retval.PromotionToRedeemableAllowed = PromotionToRedeemableAllowed;
      _retval.PromotionToRedeemableAmount = (Currency)PromotionToRedeemableAmount.Clone();
      _retval.NumberOfPromosByCreditType = CopyDictionary(NumberOfPromosByCreditType);
      _retval.NumberOfCoverCouponByCreditType = CopyDictionary(NumberOfCoverCouponByCreditType);

      _retval.IsSiteJackpotWinner = IsSiteJackpotWinner;                    // Flag to indicate if the player has won the Site Jackpot
      _retval.SiteJackpotWinnerInfo = (TYPE_SITE_JACKPOT_WINNER)SiteJackpotWinnerInfo.Clone(); // Info with the Site Jackpot winner info
      _retval.Timestamp = Timestamp;

      _retval.IsLoggedIn = IsLoggedIn;
      _retval.LoggedInTerminalId = LoggedInTerminalId;
      _retval.LoggedInTerminalName = LoggedInTerminalName;
      _retval.LoggedInTerminalProvider = LoggedInTerminalProvider;
      _retval.LoggedInTerminalType = LoggedInTerminalType;
      _retval.IsLocked = IsLocked;
      _retval.SiteId = SiteId;

      _retval.PlayerTracking = (PlayerTrackingData)PlayerTracking.Clone();

      _retval.CurrentPlaySession = (PlaySession)CurrentPlaySession.Clone();

      _retval.AccountBalance = MultiPromos.AccountBalance.Zero;

      _retval.PointsStatus = PointsStatus;

      _retval.IsVirtualCard = IsVirtualCard;

      _retval.AccountBalance = (MultiPromos.AccountBalance)AccountBalance.Clone();

      _retval.CreditLineData = (CreditLine)CreditLineData.Clone();

      return _retval;
    }

    private Dictionary<ACCOUNT_PROMO_CREDIT_TYPE, int> CopyDictionary(Dictionary<ACCOUNT_PROMO_CREDIT_TYPE, int> ToClone)
    {
      Dictionary<ACCOUNT_PROMO_CREDIT_TYPE, int> _retval = new Dictionary<ACCOUNT_PROMO_CREDIT_TYPE, int>();

      foreach (var _row in ToClone)
      {
        _retval.Add(_row.Key, _row.Value);
      }

      return _retval;
    }

    public static string GetBoolString(bool value)
    {
      string _desc;

      _desc = WSI.Common.Resource.String("STR_FORMAT_GENERAL_BUTTONS_YES");
      
      if (!value)
      {
        _desc = WSI.Common.Resource.String("STR_FORMAT_GENERAL_BUTTONS_NO");
      
      }

      return _desc;
    }
  } // CardData

  public class CancellableOperation : ICloneable
  {
    public Int64 OperationId = 0;
    public Decimal OperationAmount = 0;
    public Decimal CashInTaxAmount = 0;
    public MultiPromos.AccountBalance Balance = MultiPromos.AccountBalance.Zero;
    public Decimal CommissionToBAmount = -1;
    public Boolean IsTerminalDraw = false; // Unused: Prepared for withdra with promotion in recharge 

    public object Clone()
    {
      CancellableOperation _retval = new CancellableOperation
      {
        OperationId = OperationId,
        OperationAmount = OperationAmount,
        CashInTaxAmount = CashInTaxAmount,
        Balance = (MultiPromos.AccountBalance)Balance.Clone(),
        CommissionToBAmount = CommissionToBAmount,
        IsTerminalDraw = IsTerminalDraw
      };
      return _retval;
    }
  } // CancellableOperation

  public class CancellableData : ICloneable
  {
    public Decimal TotalOperationAmount = 0;
    public Decimal TotalCashInTaxAmount = 0;
    public Decimal TotalNotPlayedRecharge = 0;
    public MultiPromos.AccountBalance TotalBalance = MultiPromos.AccountBalance.Zero;
    public List<CancellableOperation> Operations = new List<CancellableOperation>();
    public Boolean HaveTerminalDrawPromo = false; // Unused: Prepared for withdra with promotion in recharge 

    public object Clone()
    {
      CancellableData _retval = new CancellableData
      {
        TotalOperationAmount = TotalOperationAmount,
        TotalCashInTaxAmount = TotalCashInTaxAmount,
        TotalNotPlayedRecharge = TotalNotPlayedRecharge,
        HaveTerminalDrawPromo = HaveTerminalDrawPromo,
        TotalBalance = (MultiPromos.AccountBalance)TotalBalance.Clone(),
        Operations = CopyList(Operations)
      };
      return _retval;
    }

    private List<CancellableOperation> CopyList(List<CancellableOperation> CancellableOperations)
    {
      var _retval = new List<CancellableOperation>();
      foreach (var _cancellable_operation in CancellableOperations)
      {
        _retval.Add((CancellableOperation)_cancellable_operation.Clone());
      }
      return _retval;
    }
  } // CancellableData

  public class WithholdData
  {
    public Int64 AccountId;
    public String HolderId;
    public String HolderId1;
    public String HolderId2;
    public ACCOUNT_HOLDER_ID_TYPE HolderIdType;
    public Int64 HolderDocumentId1;
    public Int64 HolderDocumentId2;
    public String HolderName1;
    public String HolderName2;
    public String HolderName3;
    public String HolderName4;
    public String HolderFullName;
    public Int32 HolderLevel;
    public DateTime HolderBirthDate;
    public Int32 HolderGender;

    public String WithholdingHolderName
    {
      get
      {
        return CardData.FormatFullName(FULL_NAME_TYPE.WITHHOLDING, HolderName3, HolderName1, HolderName2);
      }
    }

    public String AccountHolderName
    {
      get
      {
        return CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, HolderName3, HolderName1, HolderName2);
      }
    }

    public WithholdData(CardData Card)
    {
      AccountId = Card.AccountId;                               // Account Id
      HolderIdType = Card.PlayerTracking.HolderIdType;          // Holder Id type
      HolderId = Card.PlayerTracking.HolderId;                  // Holder Id ( elector code, other, RFC, CURP,..)
      HolderId1 = Card.PlayerTracking.HolderId1;                // RFC
      HolderId2 = Card.PlayerTracking.HolderId2;                // CURP
      HolderDocumentId1 = Card.PlayerTracking.HolderDocumentId1;// Holder Document id 1
      HolderDocumentId2 = Card.PlayerTracking.HolderDocumentId2;// Holder Document id 2
      HolderName1 = Card.PlayerTracking.HolderName1;            // Family Name 01
      HolderName2 = Card.PlayerTracking.HolderName2;            // Family Name 02
      HolderName3 = Card.PlayerTracking.HolderName3;            // First Name
      HolderName4 = Card.PlayerTracking.HolderName4;            // First Name
      HolderFullName = Card.PlayerTracking.HolderName;          // Full Name 
      HolderLevel = Card.PlayerTracking.CardLevel;              // Level
      HolderBirthDate = Card.PlayerTracking.HolderBirthDate;    // Birth date
      HolderGender = Card.PlayerTracking.HolderGender;          // 1-Man  2-Woman
    }

    public WithholdData()
    {
    }

  } // WithholdData

}