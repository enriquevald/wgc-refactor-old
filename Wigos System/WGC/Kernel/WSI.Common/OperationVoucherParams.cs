﻿//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: OperationVoucherParams.cs
//  
//   DESCRIPTION: 
// 
//        AUTHOR: David Hernández
// 
// CREATION DATE: 22-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-JUL-2015 DHA    First release.
// 30-SEP-2015 JMM    Fixed Bug 4803: WC2 locks tables & hangs itself
// 05-OCT-2015 ETP    Fixed Bug 4373: Avoid to load HandPay validacion Param. New Function that exclude a list of operation code by mode.
// 26-OCT-2015 JML & DDM Fixed Bug 5581: Only multisite center. (for cash sumary..)
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading;

namespace WSI.Common
{
  public class OperationVoucherParams
  {
    private static Dictionary<OperationCode, VoucherParameters> m_operation_voucher_params = new Dictionary<OperationCode, VoucherParameters>();
    private static ManualResetEvent m_ev_data_available = new ManualResetEvent(false);
    private static ReaderWriterLock m_rw_lock = new ReaderWriterLock();
    private static Boolean m_load_data = false;


    // Operation code that not has to be loaded in the current mode.
    private static WSI.Common.OperationCode[] m_oc_not_in_cashless = { WSI.Common.OperationCode.HANDPAY_VALIDATION };
    private static WSI.Common.OperationCode[] m_oc_not_in_tito = { };


    public static Boolean LoadData
    {
      get { return m_load_data; }
      set { m_load_data = value; }
    } // LoadData

    public static Boolean LoadOperationVoucherParameters()
    {
      Boolean _changed;
      OperationVoucherDictionary _new_operation_voucher_parameters;

      // Only load data in WCP, Cashier and GUI
      if (!LoadData)
      {
        return true;
      }

      _changed = false;

      // Read voucher configuration parameters
      if (!LoadOperationVoucherParameters(out _new_operation_voucher_parameters, out _changed))
      {
        return false;
      }

      if (_changed)
      {
        try
        {
          m_rw_lock.AcquireWriterLock(Timeout.Infinite);

          m_operation_voucher_params = _new_operation_voucher_parameters;
        }
        finally
        {
          m_rw_lock.ReleaseWriterLock();
        }
      }

      m_ev_data_available.Set();

      return true;
    }

    private static Boolean LoadOperationVoucherParameters(out OperationVoucherDictionary OperationVoucherDic, out Boolean Changed)
    {
      OperationVoucherDictionary _new_operation_voucher_dic_copy;

      OperationVoucherDic = OperationVoucherDictionary.Create();
      Changed = false;

      if (OperationVoucherDic == null)
      {
        Log.Warning("Create OperationVoucherParametersDictionary failed!");

        return false;
      }

      if (OperationVoucherDic.Count == 0)
      {
        Log.Warning("Read OperationVoucherDic.Count = 0");

        return false;
      }

      // Copy the new params to another dictionary. Used for detect really new params in the system.
      _new_operation_voucher_dic_copy = new OperationVoucherDictionary();
      foreach (OperationCode _key in OperationVoucherDic.Keys)
      {
        _new_operation_voucher_dic_copy.Add(_key, OperationVoucherDic[_key]);
      }

      Changed = false;
      if (m_operation_voucher_params.Count == 0)
      {
        Changed = true;
      }

      foreach (OperationCode _key in m_operation_voucher_params.Keys)
      {
        if (_new_operation_voucher_dic_copy.ContainsKey(_key))
        {
          if (_new_operation_voucher_dic_copy[_key].Generate != m_operation_voucher_params[_key].Generate ||
              _new_operation_voucher_dic_copy[_key].Print != m_operation_voucher_params[_key].Print ||
              _new_operation_voucher_dic_copy[_key].PrintCopy != m_operation_voucher_params[_key].PrintCopy)
          {
            Changed = true;
          }

          _new_operation_voucher_dic_copy.Remove(_key);
        }
        else
        {
          Changed = true;
        }
      }

      if (m_operation_voucher_params.Count > 0 && _new_operation_voucher_dic_copy.Count > 0)
      {
        Changed = true;
      }

      return true;
    } // LoadOperationVoucherParameters

    public static OperationVoucherDictionary GetDictionary()
    {
      OperationVoucherDictionary _dic;

      try
      {
        // Wait till the values have been retrieved from the DB
        m_ev_data_available.WaitOne();
        m_rw_lock.AcquireReaderLock(Timeout.Infinite);

        _dic = new OperationVoucherDictionary();
        foreach (OperationCode _key in m_operation_voucher_params.Keys)
        {
          _dic.Add(_key, m_operation_voucher_params[_key]);
        }
        return _dic;
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }
    }

    public partial class OperationVoucherDictionary : Dictionary<OperationCode, VoucherParameters>
    {
      public static OperationVoucherDictionary Create()
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            return Create(_db_trx.SqlTransaction);
          }
        }
        catch
        {
          return null;
        }
      }

      public static String ExcludeOpCodeByMode()
      {
        StringBuilder _condition;
        String[] _string_oc_not_in;
        WSI.Common.OperationCode[] _oc_not_in;
         _oc_not_in = null;
               
        if (WSI.Common.Misc.IsCashlessMode() || WSI.Common.Misc.IsMico2Mode()){
          _oc_not_in = m_oc_not_in_cashless;
        }else{
          _oc_not_in = m_oc_not_in_tito;
        }
        
        if (_oc_not_in.Length == 0)
        {
          return "";
        }

        _string_oc_not_in = Array.ConvertAll(_oc_not_in, operation_code => ((int)operation_code).ToString());
        _condition = new StringBuilder();
       
        _condition.Append("AND OVP_OPERATION_CODE NOT IN (");
        _condition.Append(string.Join(",", _string_oc_not_in));
        _condition.Append(")");

        return _condition.ToString();

      }

      public static OperationVoucherDictionary Create(SqlTransaction Trx)
      {
        OperationVoucherDictionary _dic;
        StringBuilder _sb;

        try
        {
          // Get operation voucher parameters
          _dic = new OperationVoucherDictionary();

          // 26-OCT-2015 JML & DDM Fixed Bug 5581: Only multisite center. (for cash sumary..)
          if (GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
          {
            _dic.Add(OperationCode.NOT_SET, new VoucherParameters(true, true, 1));
            
            return _dic;
          }

          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   OVP_OPERATION_CODE                         ");
          _sb.AppendLine("        , OVP_GENERATE                               ");
          _sb.AppendLine("        , OVP_REPRINT                                ");
          _sb.AppendLine("        , OVP_PRINT_COPY                             ");
          _sb.AppendLine("   FROM   OPERATION_VOUCHER_PARAMETERS               ");
          _sb.AppendLine("  WHERE   OVP_ENABLED = 1                            ");
         
          _sb.AppendLine(ExcludeOpCodeByMode());
          
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              // Read operation voucher parameters
              while (_reader.Read())
              {
                _dic.Add((OperationCode)_reader.GetInt32(0), new VoucherParameters(_reader.GetBoolean(1), _reader.GetBoolean(2), _reader.GetInt32(3)));
              }
            }
          }

          return _dic;
        }
        catch 
        {         
          return null;
        }
      }

      public static VoucherParameters GetParameters(OperationCode OperationCode)
      {
        VoucherParameters _voucher_parameters;

        if (!OperationVoucherParams.LoadData)
        {
          Log.Error("VoucherParameters not initialized.");
        }

        // Wait till the values have been retrieved from the DB
        m_ev_data_available.WaitOne();

        // Get the value
        try
        {
          m_rw_lock.AcquireReaderLock(Timeout.Infinite);

          if (!m_operation_voucher_params.TryGetValue(OperationCode, out _voucher_parameters))
          {
            // OperationCode not found get default (OperationCode = 0)
            if (!m_operation_voucher_params.TryGetValue(OperationCode.NOT_SET, out _voucher_parameters))
            {
              _voucher_parameters = new VoucherParameters(true, true, 1);
            }
          }

          // If Generate is disable set the values
          if (!GeneralParam.GetBoolean("Cashier.Voucher", "Generate", true))
          {
            _voucher_parameters.Generate = false;
          }

          // If Print is disable set the values
          if (!GeneralParam.GetBoolean("Cashier.Voucher", "Print", true) || !_voucher_parameters.Generate)
          {
            _voucher_parameters.Print = false;
            _voucher_parameters.PrintCopy = 0;
          }

        }
        finally
        {
          m_rw_lock.ReleaseReaderLock();
        }

        return _voucher_parameters;
      }

      public static Boolean GetGenerate(OperationCode OperationCode)
      {
        return GetParameters(OperationCode).Generate;
      }

      public static Boolean GetPrint(OperationCode OperationCode)
      {
        return GetParameters(OperationCode).Print;
      }

      public static Boolean GetReprint(OperationCode OperationCode)
      {
        return GetParameters(OperationCode).Reprint;
      }

      public static Int32 GetPrintCopy(OperationCode OperationCode)
      {
        return GetParameters(OperationCode).PrintCopy;
      }
    }

    public class VoucherParameters
    {
      public Boolean Generate;
      public Boolean Print;
      public Boolean Reprint;
      public Int32 PrintCopy;

      public VoucherParameters(Boolean Generate, Boolean Reprint, Int32 PrintCopy)
      {
        this.Generate = Generate;
        this.Print = PrintCopy > 0;
        this.Reprint = Reprint;
        this.PrintCopy = PrintCopy;
      }
    }
  }
}
