﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerLevelIcon.cs
// 
//   DESCRIPTION: Miscellaneous utilities for PlayerLevelIcon.
// 
//        AUTHOR: Rubén Lama
// 
// CREATION DATE: 20-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUL-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using WSI.Common.ExtentionMethods;

namespace WSI.Common
{
  public class PlayerLevelIcon
  {

    #region " Constants "

    // Private constants
    private const Int32 DB_PLI_ID               = 0;
    private const Int32 DB_PLI_INDEX            = 1;
    private const Int32 DB_PLI_NAME             = 2;
    private const Int32 DB_PLI_LEVEL            = 3;
    private const Int32 DB_PLI_NLS              = 4;
    private const Int32 DB_PLI_ICON             = 5;
    private const Int32 DB_PLI_PENDING_DOWNLOAD = 6;
    
    // Public constants
    public const Int32 DEFAULT_ID_VALUE = -1;
    public const Int32 DEFAULT_INT_VALUE = 0;

    #endregion

    #region " Enums "

    public enum TypeIcon
    {
      TYPE_ZERO = 0,
      TYPE_ONE = 1,
      TYPE_TWO = 2,
      TYPE_THREE = 3,
    } //TypePallete

    [Flags]
    public enum TypePlayerLevel
    {                     
      NONE     = 0x0000,  // 0
      LEVEL_01 = 0x0100,  // 1
      LEVEL_02 = 0x0200,  // 2
      LEVEL_03 = 0x0400,  // 3
      LEVEL_04 = 0x0800,  // 4
    }

    #endregion

    #region " Properties "

    //Base Properties
    public Int32 Id { get; set; }
    public Int32 Index { get; set; }
    public String Name { get; set; }
    public TypePlayerLevel Level { get; set; }
    public Int32 NLSid { get; set; }
    public Image Icon { get; set; }
    public Boolean PendingDownload { get; set; }
    
    #endregion " Properties "

    #region " Public Methods "

    #region " Constructors "

    public PlayerLevelIcon()
    {
      this.Id              = DEFAULT_ID_VALUE;
      this.Index           = DEFAULT_ID_VALUE;
      this.Name            = String.Empty;
      this.Level           = TypePlayerLevel.NONE;
      this.NLSid           = DEFAULT_INT_VALUE;
      this.Icon            = null;
      this.PendingDownload = false;
    } // PlayerLevelIcon

    public PlayerLevelIcon(SqlTransaction Sqltrx)
    {
      this.DB_GetAllPlayerLevelIcon(Sqltrx);
    } // PlayerLevelIcon

    public List<PlayerLevelIcon> GetAllPlayerLevelIcon()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.GetAllPlayerLevelIcon(_db_trx.SqlTransaction);
      }
    } // GetAllPlayerLevelIcon

    public List<PlayerLevelIcon> GetAllPlayerLevelIcon(SqlTransaction SqlTrx)
    {
      return this.DB_GetAllPlayerLevelIcon(SqlTrx);
    } // GetAllPlayerLevelIcon

    /// <summary>
    /// Concat level to icon
    /// </summary>
    /// <param name="Level"></param>
    public void AddLevel(TypePlayerLevel Level)
    {
      this.Level |= Level;
    } // AddLevel

    #endregion

    /// <summary>
    /// 
    /// Save InTouch Promo Game DB Method
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public virtual Boolean Save()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (this.Save(_db_trx.SqlTransaction))
        {
          return _db_trx.Commit();
        }
      }

      return false;
    } // Save
    public virtual Boolean Save(SqlTransaction SqlTrx)
    {
      return this.DB_Save(SqlTrx);
    } // Save

    /// <summary>
    /// Clone PromoGame 
    /// </summary>
    /// <returns></returns>       
    public PlayerLevelIcon Clone()
    {
      return new PlayerLevelIcon()
        {
        Id              = this.Id,
        Index           = this.Index,
        Name            = this.Name,
        Level           = this.Level,
        NLSid           = this.NLSid,
        Icon            = this.Icon,
        PendingDownload = this.PendingDownload
      };
    } // Clone

    /// <summary>
    /// Reset all members id
    /// </summary>
    public virtual void Reset()
    {
      this.Id = DEFAULT_ID_VALUE;
    } // Reset

    public Boolean DownloadLevelIcons(String DestinationPath)
    {
      return this.DB_DownloadLevelIcons(DestinationPath);
    }

    #endregion " Public Methods "

    #region " Private Methods "

    private void Initialize()
    {

    }

    /// <summary>
    /// Function convert base64 string to image
    /// </summary>
    /// <param name="base64String"></param>
    /// <returns></returns>
    private Image Base64ToImage(String base64String)
    {
      byte[] imageBytes = Convert.FromBase64String(base64String);

      // Convert byte[] to Image
      using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
      {
        Image image = Image.FromStream(ms, false, false);

        return image;
      }
    }

    /// <summary>
    /// To save level image
    /// </summary>
    /// <param name="LevelIndex"></param>
    /// <param name="Base64Img"></param>
    private void SaveLevelIcon(String DestinationPath, PlayerLevelIcon PlayerLevelIcon, Int32 Index)
    {
      String _file_name;

      try
      {
        if(PlayerLevelIcon.PendingDownload)
        {
          _file_name = String.Format("{0}LEVEL_{1}.png", DestinationPath, Index);

          this.DeleteOldIcon(_file_name);

          if (!Directory.Exists(DestinationPath))
          {
            Directory.CreateDirectory(DestinationPath);
          }

          PlayerLevelIcon.Icon.Save(_file_name, System.Drawing.Imaging.ImageFormat.Png);
          this.DB_UpdatePendingDownload(false, PlayerLevelIcon.Id);
        }        
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // SaveLevelImage

    private void DeleteOldIcon(String File)
    {
      if (System.IO.File.Exists(File))
      {
        System.IO.File.Delete(File);
      }
    }

    #region " DataBase Methods "

    /// <summary>
    /// Get all player level icons
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<PlayerLevelIcon> DB_GetAllPlayerLevelIcon(SqlTransaction SqlTrx)
    {
      List<PlayerLevelIcon> _icon_list;
      PlayerLevelIcon _icon = new PlayerLevelIcon();

      _icon_list = new List<PlayerLevelIcon>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_LevelIconQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _icon = this.PlayerLevelIconMappFromSqlReader(_sql_reader, SqlTrx);

              if (_icon != null)
              {
                _icon_list.Add(_icon);
              }
            }
          }

          return _icon_list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("PlayerLevelIcon.DB_GetAllPlayerLevelIcon --> Error on read PlayerLevelIcon");
        Log.Exception(_ex);
      }

      return null;

    } // DB_GetAllPlayerLevelIcon

    /// <summary>
    /// Load PlayerIcon data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_PlayerLevelIconDataMapper(SqlDataReader SqlReader)
    {
      return DB_PlayerLevelIconDataMapper(this, SqlReader);
    } // DB_PlayerLevelIconDataMapper

    /// <summary>
    /// Load PlayerLevelIcon data from SQLReader
    /// </summary>
    /// <param name="LevelIcon"></param>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_PlayerLevelIconDataMapper(PlayerLevelIcon LevelIcon, SqlDataReader SqlReader)
    {
      try
      {
        LevelIcon.Id              = SqlReader.GetInt32(DB_PLI_ID);
        LevelIcon.Index           = SqlReader.GetInt32(DB_PLI_INDEX);
        LevelIcon.Name            = SqlReader.GetString(DB_PLI_NAME);
        LevelIcon.Level           = (TypePlayerLevel)SqlReader.GetInt32(DB_PLI_LEVEL);
        LevelIcon.NLSid           = SqlReader.GetInt32(DB_PLI_NLS);
        LevelIcon.Icon            = this.Base64ToImage(SqlReader.GetString(DB_PLI_ICON));
        LevelIcon.PendingDownload = SqlReader.GetBoolean(DB_PLI_PENDING_DOWNLOAD);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_PlayerLevelIconDataMapper");
        Log.Exception(_ex);
      }

      return false;

    } // DB_PlayerLevelIconDataMapper    

    /// <summary>
    /// Mapp InTouchPallete from sql reader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private PlayerLevelIcon PlayerLevelIconMappFromSqlReader(SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      PlayerLevelIcon _pallete;

      _pallete = new PlayerLevelIcon();

      if (!this.DB_PlayerLevelIconDataMapper(_pallete, SqlReader))
      {
        Log.Error(String.Format("InTouchPallete.InTouchPalleteMappFromSqlReader -> Error on load InTouchPalleteId: {0}", SqlReader.GetInt32(DB_PLI_ID)));
      }

      return _pallete;

    } // InTouchPalleteMappFromSqlReader

    /// <summary>
    /// Update player level icon
    /// </summary>
    /// <returns></returns>
    private String DB_Update_Query()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   PLAYER_LEVEL_ICONS       ");
      _sb.AppendLine("    SET   PLI_NAME = @pName        ");
      _sb.AppendLine("        , PLI_INDEX = @pIndex      ");
      _sb.AppendLine("        , PLI_LEVEL = @pNivel      ");
      _sb.AppendLine("        , PLI_NLS_ID = @pNLSId     ");
      _sb.AppendLine("        , PLI_PENDING_DOWNLOAD = 1 "); // Set to 1 to force the new icons to be downloaded
      _sb.AppendLine("  WHERE   PLI_ID = @pId         ");

      return _sb.ToString();

    } // DB_Update_Query

    /// <summary>
    /// Update pending download field
    /// </summary>
    /// <returns></returns>
    private String DB_UpdatePendingDownload_Query()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.Append(" UPDATE PLAYER_LEVEL_ICONS                      ");
      _sb.Append("    SET PLI_PENDING_DOWNLOAD = @PendingDownload ");
      _sb.Append("  WHERE PLI_ID               = @LevelId         ");

      return _sb.ToString();
    } // DB_UpdatePendingDownload_Query

    /// <summary>
    /// Gets player level icon select query
    /// </summary>
    /// <returns></returns>
    private String DB_LevelIconQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   PLI_ID                       "); // 0
      _sb.AppendLine("           , PLI_INDEX                    "); // 1
      _sb.AppendLine("           , PLI_NAME                     "); // 2
      _sb.AppendLine("           , PLI_LEVEL                    "); // 3
      _sb.AppendLine("           , PLI_NLS_ID                   "); // 4
      _sb.AppendLine("           , PLI_ICON                     "); // 5
      _sb.AppendLine("           , PLI_PENDING_DOWNLOAD         "); // 6
      _sb.AppendLine("      FROM   PLAYER_LEVEL_ICONS           ");

      return _sb.ToString();

    } // DB_LevelIconQuery_Select

    /// <summary>
    /// To get base64 image data from DDBB
    /// </summary>
    /// <returns></returns>
    private Boolean DB_DownloadLevelIcons(String DestinationPath)
    {
      List<PlayerLevelIcon> _list_player_level_icons;
      PlayerLevelIcon _icon;
      int _idx;      

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _list_player_level_icons = this.DB_GetAllPlayerLevelIcon(_db_trx.SqlTransaction);
          _idx = 0;
          
          foreach(TypePlayerLevel _icon_level in Enum.GetValues(typeof(TypePlayerLevel)))
          {
            if (_icon_level != TypePlayerLevel.NONE)
            {
              _icon = _list_player_level_icons.GetIconByLevel(_icon_level);

              this.SaveLevelIcon(DestinationPath, _icon, _idx);             
            }            
            
            _idx++;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetLevelImage

    private Boolean DB_UpdatePendingDownload(Boolean PendingDownload, Int32 LevelId)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(this.DB_UpdatePendingDownload_Query(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@PendingDownload", SqlDbType.Bit).Value = PendingDownload;
            _cmd.Parameters.Add("@LevelId", SqlDbType.Int).Value         = LevelId;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return true;
            }

            return false;
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion

    #region " Mask Methods "

    /// <summary>
    /// Check if level is activated in a bit mask.
    /// </summary>
    /// <param name="Level"></param>
    /// <param name="Mask"></param>
    /// <returns></returns>
    public static Boolean ExistsLevelInMask(TypePlayerLevel Level, Int32 Mask)
    {
      return ((Int32)Level != 0 && (Mask & (Int32)Level) == (Int32)Level);
    } // ExistsBlockReasonInMask
    
    #endregion

    #endregion

    #region "Internal Methods"

    internal virtual Boolean DB_Save(SqlTransaction SqlTrx)
    {
      return this.DB_Update(SqlTrx);

    } // DB_Save

    /// <summary>
    /// Update an specific Jackpot in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal Boolean DB_Update(SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_Update_Query(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId"    , SqlDbType.Int).Value      = this.Id;
          _cmd.Parameters.Add("@pIndex" , SqlDbType.Int).Value      = this.Index;
          _cmd.Parameters.Add("@pName"  , SqlDbType.NVarChar).Value = this.Name;
          _cmd.Parameters.Add("@pNivel" , SqlDbType.Int).Value      = this.Level;
          _cmd.Parameters.Add("@pNLSId" , SqlDbType.Int).Value      = this.NLSid;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }

          Log.Error(String.Format("PlayerLevelIcon.DB_Update -> Id: {0}", this.Id));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PlayerLevelIcon.DB_Update -> Id: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Update

    #endregion  
    
  } // PlayerLevelIcon
}
