using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public static class NumberToLetter
  {

    public static String CurrencyString(Decimal Amount)
    {
      return CurrencyString(Amount, (Resource.CountryISOCode2 == "MX"));
    } // CurrencyString

    public static String PaymentOrderCurrencyString(Decimal Amount)
    {
      return CurrencyString(Amount, true).ToUpper();
    } // PaymentOrderCurrencyString

    private static String CurrencyString(Decimal Amount, Boolean ShortForDecimals)
    {
      String _minus;
      String _cents;
      String _currency;
      String _currency_cents;
      Int64 _int_part;
      Int64 _dec_part;
      String _text;
      String _singular_currency;
      String _plural_currency;
      CurrencyExchange _national_currency;

      _text = "";

      _national_currency = LanguageResource.NationalCurrencyData;

      _plural_currency = String.IsNullOrEmpty(_national_currency.LanguageResource.PluralCurrency) ? "" : _national_currency.LanguageResource.PluralCurrency;
      _singular_currency = String.IsNullOrEmpty(_national_currency.LanguageResource.SingularCurrency) ? "" : _national_currency.LanguageResource.SingularCurrency;

      try
      {
        _minus = String.Empty;

        Amount = Math.Round(Amount, 2, MidpointRounding.AwayFromZero);

        if (Amount < 0)
        {
          _minus = Resource.String("STR_NUMBERTOLETTER_MINUS");

          if (Amount == Decimal.MinValue)
          {
            // Small error ...
            Amount = Decimal.MaxValue;
          }
          else
          {
            Amount = Decimal.Negate(Amount);
          }
        }

        _int_part = Decimal.ToInt64((Amount * 100));
        _dec_part = _int_part % 100;
        _int_part -= _dec_part;
        _int_part /= 100;

        _currency = " " + _plural_currency;
        if (_int_part == 1) _currency = " " + _singular_currency;
        if (_int_part >= 1000000)
        {
          if (_int_part % 1000000 == 0)
          {
            _currency = Resource.String("STR_NUMBERTOLETTER_OF") + _plural_currency;
          }
        }

        _currency_cents = " " + (String.IsNullOrEmpty(_national_currency.LanguageResource.PluralCents) ? "" : _national_currency.LanguageResource.PluralCents);
        if (_dec_part == 1) _currency_cents = " " + (String.IsNullOrEmpty(_national_currency.LanguageResource.SingularCents) ? "" : _national_currency.LanguageResource.SingularCents);

        _cents = "";
        if (_dec_part > 0)
        {
          _cents = Resource.String("STR_NUMBERTOLETTER_AND") + MillionToString(_dec_part) + _currency_cents + "";
        }

        if (ShortForDecimals)
        {
          _cents = " " + _dec_part.ToString("00") + "/100 M.N.";
        }

        _text = _minus + MillionToString(_int_part) + _currency + _cents;
        if (_text.Length > 1)
        {
          _text = _text.Substring(0, 1).ToUpper() + _text.Substring(1, _text.Length - 1);
        }
      }
      catch
      {
      }

      return _text;
    } // CurrencyString

    /// <summary>
    /// Convert units (Int64 format) to String format.
    /// </summary>
    /// <param name="Value">Value to convert</param>
    /// <returns>Returns a text which represents the specified Value</returns>
    private static String UnitToString(Int64 Value)
    {
      String _result;

      _result = String.Empty;

      switch (Value)
      {
        case 0: _result = Resource.String("STR_NUMBERTOLETTER_ZERO"); break;
        case 1: _result = Resource.String("STR_NUMBERTOLETTER_ONE"); break;
        case 2: _result = Resource.String("STR_NUMBERTOLETTER_TWO"); break;
        case 3: _result = Resource.String("STR_NUMBERTOLETTER_THREE"); break;
        case 4: _result = Resource.String("STR_NUMBERTOLETTER_FOUR"); break;
        case 5: _result = Resource.String("STR_NUMBERTOLETTER_FIVE"); break;
        case 6: _result = Resource.String("STR_NUMBERTOLETTER_SIX"); break;
        case 7: _result = Resource.String("STR_NUMBERTOLETTER_SEVEN"); break;
        case 8: _result = Resource.String("STR_NUMBERTOLETTER_EIGHT"); break;
        case 9: _result = Resource.String("STR_NUMBERTOLETTER_NINE"); break;
        default: _result = TenToString(Value); break;
      }

      return _result;
    } // UnitToString

    /// <summary>
    /// Convert tens (Int64 format) to String format.
    /// </summary>
    /// <param name="Value">Value to convert</param>
    /// <returns>Returns a text which represents the specified Value</returns>
    private static String TenToString(Int64 Value)
    {
      if (Value < 10) return UnitToString(Value);
      if (Value > 99) return HundredToString(Value);

      String _result = String.Empty;

      switch (Value)
      {
        case 10: _result = Resource.String("STR_NUMBERTOLETTER_TEN"); break;
        case 11: _result = Resource.String("STR_NUMBERTOLETTER_ELEVEN"); break;
        case 12: _result = Resource.String("STR_NUMBERTOLETTER_TWELVE"); break;
        case 13: _result = Resource.String("STR_NUMBERTOLETTER_THIRTEEN"); break;
        case 14: _result = Resource.String("STR_NUMBERTOLETTER_FOURTEEN"); break;
        case 15: _result = Resource.String("STR_NUMBERTOLETTER_FIFTEEN"); break;
        case 16: _result = Resource.String("STR_NUMBERTOLETTER_SIXTEEN"); break;
        case 17: _result = Resource.String("STR_NUMBERTOLETTER_SEVENTEEN"); break;
        case 18: _result = Resource.String("STR_NUMBERTOLETTER_EIGHTEEN"); break;
        case 19: _result = Resource.String("STR_NUMBERTOLETTER_NINETEEN"); break;
        case 20: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY"); break;
        case 21: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY-ONE"); break;
        case 22: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY-TWO"); break;
        case 23: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY-THREE"); break;
        case 24: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY-FOUR"); break;
        case 25: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY-FIVE"); break;
        case 26: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY-SIX"); break;
        case 27: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY-SEVEN"); break;
        case 28: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY-EIGHT"); break;
        case 29: _result = Resource.String("STR_NUMBERTOLETTER_TWENTY-NINE"); break;

        case 30: _result = Resource.String("STR_NUMBERTOLETTER_THIRTY"); break;
        case 40: _result = Resource.String("STR_NUMBERTOLETTER_FORTY"); break;
        case 50: _result = Resource.String("STR_NUMBERTOLETTER_FIFTY"); break;
        case 60: _result = Resource.String("STR_NUMBERTOLETTER_SIXTY"); break;
        case 70: _result = Resource.String("STR_NUMBERTOLETTER_SEVENTY"); break;
        case 80: _result = Resource.String("STR_NUMBERTOLETTER_EIGHTY"); break;
        case 90: _result = Resource.String("STR_NUMBERTOLETTER_NINETY"); break;
        default:
          {
            Int64 _ten;
            Int64 _unit;

            _unit = Value % 10;
            _ten = Value - _unit;

            _result = TenToString(_ten) + Resource.String("STR_NUMBERTOLETTER_CON") + UnitToString(_unit);

            break;
          }
      }

      return _result;
    } // TenToString

    private static String HundredToString(Int64 Value)
    {
      Int64 _hundred;
      Int64 _remainder;

      if (Value < 100) return TenToString(Value);
      if (Value > 999) return ThousandToString(Value);

      _remainder = 0;
      _hundred = Math.DivRem(Value, 100, out _remainder);

      String _result = String.Empty;

      switch (_hundred)
      {
        case 1:
          {
            _result = Resource.String("STR_NUMBERTOLETTER_HUNDRED");
            if (_remainder == 0)
            {
              _result = Resource.String("STR_NUMBERTOLETTER_HUNDRED_2");
            }
            break;
          }
        case 2: _result = Resource.String("STR_NUMBERTOLETTER_TWO_HUNDRED"); break;
        case 3: _result = Resource.String("STR_NUMBERTOLETTER_THREE_HUNDRED"); break;
        case 4: _result = Resource.String("STR_NUMBERTOLETTER_FOUR_HUNDRED"); break;
        case 5: _result = Resource.String("STR_NUMBERTOLETTER_FIVE_HUNDRED"); break;
        case 6: _result = Resource.String("STR_NUMBERTOLETTER_SIX_HUNDRED"); break;
        case 7: _result = Resource.String("STR_NUMBERTOLETTER_SEVEN_HUNDRED"); break;
        case 8: _result = Resource.String("STR_NUMBERTOLETTER_EIGHT_HUNDRED"); break;
        case 9: _result = Resource.String("STR_NUMBERTOLETTER_NINE_HUNDRED"); break;
      }

      if (_remainder == 0) return _result;

      return _result + " " + TenToString(_remainder);
    } // HundredToString

    /// <summary>
    /// Convert thousands (Int64 format) to String format.
    /// </summary>
    /// <param name="Value">Value to convert</param>
    /// <returns>Returns a text which represents the specified Value</returns>
    private static String ThousandToString(Int64 Value)
    {
      if (Value < 1000) return HundredToString(Value);
      if (Value > 999999) return MillionToString(Value);

      Int64 _remainder = 0;
      Int64 _thousands = System.Math.DivRem(Value, 1000, out _remainder);
      String _str_mil = Resource.String("STR_NUMBERTOLETTER_ONE_THOUSAND");

      if (_thousands > 1) _str_mil = HundredToString(_thousands) + Resource.String("STR_NUMBERTOLETTER_THOUSAND");

      if (_remainder == 0) return _str_mil;

      return _str_mil + " " + HundredToString(_remainder);
    } // ThousandToString

    /// <summary>
    /// Convert millions (Int64 format) to String format.
    /// </summary>
    /// <param name="Value">Value to convert</param>
    /// <returns>Returns a text which represents the specified Value</returns>
    private static String MillionToString(Int64 Value)
    {
      if (Value < 1000000) return HundredToString(Value);

      Int64 _remainder = 0;
      Int64 _millions = System.Math.DivRem(Value, 1000000, out _remainder);

      // MPO 06-NOV-2015: TFS-Bug 6276
      String _str_millions = Resource.String("STR_NUMBERTOLETTER_ONE_MILLION");

      if (_millions > 1)
        _str_millions = ThousandToString(_millions) + Resource.String("STR_NUMBERTOLETTER_MILLION");

      if (_remainder > 0)
        _str_millions = _str_millions + " " + ThousandToString(_remainder);

      return _str_millions;
    } // MillionToString

  } // NumberToLetter

}