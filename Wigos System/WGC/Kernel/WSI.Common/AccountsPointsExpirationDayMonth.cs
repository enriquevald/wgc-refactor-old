﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: AccountsCreditsExpirationDayMonth.cs
//
//   DESCRIPTION: AccountsCreditsExpirationDayMonth class
//
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 31-AUG-2015

// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-AUG-2015 AMF    First release.
// 22-SEP-2015 AMF    Fixed BUG TFS-4628: Don't expire in multisite. Problem with OUTPUT vs Trigger
// 01-FEB-2016 FGB    Better expiration date comparition.
// 14-MAR-2016 FGB    Renamed the class and methods with DEPRECATED_ preffix.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using System.Threading;
using WSI.Common.TITO;

namespace WSI.Common
{
  public static class DEPRECATED_AccountsPointsExpirationDayMonth
  {
    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Points expiration management to date (DEPRECATED)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    public static Boolean DEPRECATED_ProcessAccountsPointsExpirationDayMonth()
    {
      String _str_day_month;
      DateTime DateExpiration;
      DataTable _dt_list;

      _dt_list = GetAccountPointsExpiredList();

      // Pending registers 
      if (_dt_list.Rows.Count > 0)
      {
        if (DEPRECATED_ProcessAccountPointsExpiredList(_dt_list))
        {
          if (!SetExecutionAccountPointsExpiredControl())
          {
            Log.Error("Error in AccountsPointsExpirationDayMonth.SetExecutionAccountPointsExpiredControl pending registers");

            return false;
          }
        }
        else
        {
          Log.Error("Error in AccountsPointsExpirationDayMonth.ProcessAccountPointsExpiredList pending registers");

          return false;
        }

        return true;
      }

      // New registers
      if (!DEPRECATED_CalculateDateExpirationDayMonth(out _str_day_month, out DateExpiration))
      {        
        return true; // GP empty
      }

      if (WGDB.Now >= DateExpiration)
      {
        if (!DEPRECATED_GenerateAccountPointsExpiredList(_str_day_month, DateExpiration.Year))
        {
          Log.Error("Error in AccountsPointsExpirationDayMonth.GenerateAccountPointsExpiredList. DayMonth: " + _str_day_month + " Year: " + DateExpiration.Year.ToString());

          return false;
        }

        _dt_list = GetAccountPointsExpiredList();

        if (_dt_list.Rows.Count > 0)
        {
          if (DEPRECATED_ProcessAccountPointsExpiredList(_dt_list))
          {
            if (!SetExecutionAccountPointsExpiredControl())
            {
              Log.Error("Error in AccountsPointsExpirationDayMonth.SetExecutionAccountPointsExpiredControl. DayMonth: " + _str_day_month + " Year: " + DateExpiration.Year.ToString());

              return false;
            }
          }
          else
          {
            Log.Error("Error in AccountsPointsExpirationDayMonth.ProcessAccountPointsExpiredList. DayMonth: " + _str_day_month + " Year: " + DateExpiration.Year.ToString());

            return false;
          }
        }
      }

      return true;

    } // ProcessAccountsPointsExpirationDayMonth

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate date expiration by dayMonth
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //            - String DayMonth
    //            - DateTime _date_expiration
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean DEPRECATED_CalculateDateExpirationDayMonth(out String DayMonth, out DateTime DateExpiration)
    {
      Int32 _closing_time_HH = 0;
      Int32 _closing_time_MM = 0;

      DateExpiration = WGDB.Now;
      DayMonth = GeneralParam.GetString("PlayerTracking", "PointsExpirationDayMonth");

      if (!String.IsNullOrEmpty(DayMonth))
      {
        int _idx;
        Int32 _day;
        Int32 _month;
        _closing_time_HH = GeneralParam.GetInt32("WigosGUI", "ClosingTime", 0, 0, 23);
        _closing_time_MM = GeneralParam.GetInt32("WigosGUI", "ClosingTimeMinutes", 0, 0, 59);

        DayMonth = DayMonth.Replace("-", "/");
        DayMonth = DayMonth.Replace(".", "/");
        _idx = DayMonth.IndexOf("/");

        if (_idx > 0
        && Int32.TryParse(DayMonth.Substring(0, _idx), out _day)
        && Int32.TryParse(DayMonth.Substring(_idx + 1), out _month))
        {
          if ((_month >= 1 && _month <= 12) && (_day >= 1 && _day <= 31))
          {
            while (_day > 0)
            {
              try
              {
                DateExpiration = new DateTime(WGDB.Now.Year, _month, _day);
                DateExpiration = DateExpiration.AddHours(_closing_time_HH);
                DateExpiration = DateExpiration.AddMinutes(_closing_time_MM);
                DateExpiration = DateExpiration.AddDays(1);

                return true;
              }
              catch
              {
                _day = _day - 1;
              }
            }
          }
        }
        else
        {
          Log.Error("AccountsPointsExpirationDayMonth.CalculateDateExpirationDayMonth. Incorrect GP PlayerTracking.PointsExpirationDayMonth: " + DayMonth);
        }
      }

      return false;

    } /// CalculateDateExpirationDayMonth

    //------------------------------------------------------------------------------
    // PURPOSE : generate account points expired
    //
    //  PARAMS :
    //      - INPUT :
    //            - String DayMonth
    //            - int Year
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean DEPRECATED_GenerateAccountPointsExpiredList(String DayMonth, int Year)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (InsertAccountPointsExpiredControl(DayMonth, Year, _db_trx.SqlTransaction))
          {
            if (SetAccountPointsExpiredList(_db_trx.SqlTransaction))
            {
              _db_trx.Commit();

              return true;
            }
          }

          return true; // has been processed previously
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GenerateAccountPointsExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : process register from account_points_expired_list (DEPRECATED)
    //
    //  PARAMS :
    //      - INPUT :
    //            - DataTable AccountsList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean DEPRECATED_ProcessAccountPointsExpiredList(DataTable AccountsList)
    {
      int _expire_registers;

      _expire_registers = 0;

      foreach (DataRow _account in AccountsList.Rows)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (DEPRECATED_ExpireAccountPointsDayMonth(_account, _db_trx.SqlTransaction))
            {
              if (DeleteRegisterAccountPointsExpiredList((Int64)_account["APEL_ACCOUNT_ID"], _db_trx.SqlTransaction))
              {
                _expire_registers++;
                _db_trx.Commit();
              }
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          continue;
        }
      }

      return (AccountsList.Rows.Count == _expire_registers);

    } // ProcessAccountPointsExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : Credits expiration for one account mode DayMonth (DEPRECATED)
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataRowAccount
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean DEPRECATED_ExpireAccountPointsDayMonth(DataRow DataRowAccount, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int64 _account_id;
      Decimal _points;
      Decimal _inserted_points;
      Decimal _deleted_points;
      AccountMovementsTable _account_mov_table;

      _account_id = (Int64)DataRowAccount["APEL_ACCOUNT_ID"];
      _points = (Decimal)DataRowAccount["APEL_POINTS_TO_EXPIRE"];
      _inserted_points = 0;
      _deleted_points = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("DECLARE @MyTableVar TABLE (                                                              ");
        _sb.AppendLine("                            Inserted money                                               ");
        _sb.AppendLine("                          , Deleted  money                                               ");
        _sb.AppendLine("                          );                                                             ");
        _sb.AppendLine("                                                                                         ");
        _sb.AppendLine("UPDATE   ACCOUNTS                                                                        ");
        _sb.AppendLine("   SET   AC_POINTS = CASE WHEN AC_POINTS < @pPoints THEN 0 ELSE AC_POINTS - @pPoints END ");
        _sb.AppendLine("OUTPUT   INSERTED.AC_POINTS                                                              ");
        _sb.AppendLine("       , DELETED.AC_POINTS AS DELETED_POINTS INTO @MyTableVar                            ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId                                                     ");
        _sb.AppendLine("                                                                                         ");
        _sb.AppendLine("SELECT * FROM @MyTableVar                                                                ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _account_id;
          _sql_cmd.Parameters.Add("@pPoints", SqlDbType.Money).Value = _points;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Error("ExpireAccountPointsDayMonth. Account not updated. Account Id:" + _account_id.ToString());

              return false;
            }
            _inserted_points = _reader.GetDecimal(0);
            _deleted_points = _reader.GetDecimal(1);
          }
        }

        _account_mov_table = new AccountMovementsTable(0, "", 0);

        if (!_account_mov_table.Add(0, _account_id, MovementType.PointsExpired, _deleted_points, _deleted_points - _inserted_points, 0, _inserted_points))
        {
          return false;
        }

        if (!_account_mov_table.Save(SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ExpireAccountPointsDayMonth

    //------------------------------------------------------------------------------
    // PURPOSE : Get register from account_points_expired_list
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //      
    //   NOTES :
    private static DataTable GetAccountPointsExpiredList()
    {
      StringBuilder _sb;
      DataTable _dt;

      _dt = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   APEL_ACCOUNT_ID             ");
        _sb.AppendLine("       , APEL_POINTS_TO_EXPIRE       ");
        _sb.AppendLine("       , APEL_DATETIME               ");
        _sb.AppendLine("  FROM   ACCOUNT_POINTS_EXPIRED_LIST ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_dt);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt;

    } // GetAccountPointsExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : Set registers for account_points_expired_list
    //
    //  PARAMS :
    //      - INPUT :
    //            - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //      
    //   NOTES :
    private static Boolean SetAccountPointsExpiredList(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _dt;

      _dt = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   INSERT INTO   ACCOUNT_POINTS_EXPIRED_LIST ");
        _sb.AppendLine("               (                             ");
        _sb.AppendLine("                 APEL_ACCOUNT_ID             ");
        _sb.AppendLine("               , APEL_POINTS_TO_EXPIRE       ");
        _sb.AppendLine("               , APEL_DATETIME               ");
        _sb.AppendLine("               )                             ");
        _sb.AppendLine("        SELECT   AC_ACCOUNT_ID               ");
        _sb.AppendLine("               , AC_POINTS                   ");
        _sb.AppendLine("               , GETDATE()                   ");
        _sb.AppendLine("          FROM   ACCOUNTS                    ");
        _sb.AppendLine("         WHERE   AC_POINTS > 0               ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // SetAccountPointsExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : Delete from account_points_expired_list
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64 AccountId
    //            - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean DeleteRegisterAccountPointsExpiredList(Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" DELETE FROM   ACCOUNT_POINTS_EXPIRED_LIST   ");
        _sb.AppendLine("       WHERE   APEL_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DeleteRegisterAccountPointsExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : Insert register on account_points_expired_control
    //
    //  PARAMS :
    //      - INPUT :
    //            - String DayMonth
    //            - int Year
    //            - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean InsertAccountPointsExpiredControl(String DayMonth, int Year, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF NOT EXISTS ( SELECT   1                              ");
        _sb.AppendLine("                   FROM   ACCOUNT_POINTS_EXPIRED_CONTROL ");
        _sb.AppendLine("                  WHERE   APEC_DAY_MONTH = @pDayMonth    ");
        _sb.AppendLine("                    AND   APEC_YEAR      = @pYear )      ");
        _sb.AppendLine(" BEGIN                                                   ");
        _sb.AppendLine("   INSERT INTO   ACCOUNT_POINTS_EXPIRED_CONTROL          ");
        _sb.AppendLine("               (                                         ");
        _sb.AppendLine("                 APEC_DAY_MONTH                          ");
        _sb.AppendLine("               , APEC_YEAR                               ");
        _sb.AppendLine("               )                                         ");
        _sb.AppendLine("        VALUES                                           ");
        _sb.AppendLine("               (                                         ");
        _sb.AppendLine("                 @pDayMonth                              ");
        _sb.AppendLine("               , @pYear                                  ");
        _sb.AppendLine("               )                                         ");
        _sb.AppendLine(" END                                                     ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pDayMonth", SqlDbType.NVarChar, 5).Value = DayMonth;
          _sql_cmd.Parameters.Add("@pYear", SqlDbType.Int).Value = Year;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InsertAccountPointsExpiredControl

    //------------------------------------------------------------------------------
    // PURPOSE : Set execution on account_points_expired_control
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean SetExecutionAccountPointsExpiredControl()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNT_POINTS_EXPIRED_CONTROL ");
        _sb.AppendLine("    SET   APEC_EXECUTION = GETDATE()     ");
        _sb.AppendLine("  WHERE   APEC_EXECUTION IS NULL         ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (_sql_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // SetExecutionAccountPointsExpiredControl

    #endregion Private Methods

  } // AccountsPointsExpirationDayMonth
} // WSI.Common
