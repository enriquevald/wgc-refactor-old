//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BigIncrementsData.cs
// 
//   DESCRIPTION: Class to manage Big Increments Deltas
// 
//        AUTHOR: David Rigal
// 
// CREATION DATE: 31-MAR-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAR-2015 DRV    First release. Item 981.
// 09-APR-2015 DRV    Add GP default Values.
// 12-AUG-2015 AMF    Fixed Bug WIG-2635: Only load bigincrement data in WCP service
// 17-SEP-2015 JML    Maximun delta for coin acceptor
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Timers;
using System.Threading;

namespace WSI.Common
{
  public static class BigIncrementsData
  {
    #region 'Members'

    private static Dictionary<Int64, Deltas> m_terminal_meter_delta;
    private static Dictionary<Int32, Int64> m_terminal_group_relation;
    private static Deltas m_gp_deltas;
    private static ManualResetEvent m_ev_data_available = new ManualResetEvent(false);
    private static ReaderWriterLock m_rw_lock = new ReaderWriterLock();
    private static Boolean m_load_data = false;

    #endregion

    #region Properties

    public static Boolean LoadData
    {
      get { return m_load_data; }
      set { m_load_data = value; }
    }

    #endregion Properties

    #region 'Structs'
    private struct Deltas
    {
      public Int64 QuantityGamesPlayed;
      public Int64 CentsPlayed;
      public Int64 QuantityGamesWon;
      public Int64 CentsWon;
      public Int64 CentsJackpot;
      public Int64 QuantityFTToEgm;
      public Int64 CentsFTToEgm;
      public Int64 QuantityFTFromEgm;
      public Int64 CentsFTFromEgm;
      public Int64 CentsHandPays;
      public Int64 CentsTicketIn;
      public Int64 CentsTicketOut;
      public Int64 QuantityTicketIn;
      public Int64 QuantityTicketOut;
      public Int64 QuantityBillIn;
      public Int64 CentsBillIn;
      public Int64 CentsCoinAcceptor;
    }
    #endregion

    #region 'Public Methods'
    //------------------------------------------------------------------------------
    // PURPOSE : Loads all data
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    public static Boolean LoadBigIncrementsData()
    {
      
      // Only load data in WCP service
      if (!LoadData)
      {
        return true;
      }

      LoadDefaultGPDeltas();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (LoadTerminalMeterDeltas(_db_trx.SqlTransaction))
        {
          if (LoadGroupTerminalRelation(_db_trx.SqlTransaction))
          {
            m_ev_data_available.Set();
            return true;
          }
        }
      }
      m_ev_data_available.Set();
      return false;
    }

    public static Int64 QuantityGamesPlayed(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).QuantityGamesPlayed;
    }

    public static Int64 CentsPlayed(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsPlayed;
    }

    public static Int64 QuantityGamesWon(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).QuantityGamesWon;
    }

    public static Int64 CentsWon(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsWon;
    }

    public static Int64 CentsJackpot(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsJackpot;
    }

    public static Int64 QuantityFTToEgm(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).QuantityFTToEgm;
    }

    public static Int64 CentsFTToEgm(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsFTToEgm;
    }

    public static Int64 QuantityFTFromEgm(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).QuantityFTFromEgm;
    }

    public static Int64 CentsFTFromEgm(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsFTFromEgm;
    }

    public static Int64 CentsHandPays(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsHandPays;
    }

    public static Int64 CentsTicketIn(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsTicketIn;
    }

    public static Int64 CentsTicketOut(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsTicketOut;
    }

    public static Int64 QuantityTicketIn(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).QuantityTicketIn;
    }

    public static Int64 QuantityTicketOut(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).QuantityTicketOut;
    }

    public static Int64 QuantityBillIn(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).QuantityBillIn;
    }

    public static Int64 CentsBillIn(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsBillIn;
    }

    public static Int64 CentsCoinAcceptor(Int32 TerminalID)
    {
      return GetMeterDeltaValue(TerminalID).CentsCoinAcceptor;
    }

    

    #endregion

    #region 'Private Methods'

    //------------------------------------------------------------------------------
    // PURPOSE : Loads Grouped meters deltas and fills the cache dictionary
    //
    //  PARAMS :
    //      - INPUT : Sqltransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    private static Boolean LoadTerminalMeterDeltas(SqlTransaction Trx)
    {
      string _query;
      DataTable _dt_delta_data;
      _query = GetTerminalMeterDeltaQuery();
      if (FillDataTable(out _dt_delta_data, _query, Trx))
      {
        if (FillTerminalMeterDeltaDataDictionary(_dt_delta_data))
        {
          return true;
        }
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Fills grouped meter delta dictionary with a datatable
    //
    //  PARAMS :
    //      - INPUT : Datatable
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    private static Boolean FillTerminalMeterDeltaDataDictionary(DataTable DtDeltaData)
    {
      Deltas _deltas;
      Dictionary<Int64, Deltas> _dict_deltas;
      Boolean _error = false;

      _dict_deltas = new Dictionary<Int64, Deltas>();

      try
      {
        foreach (DataRow _row in DtDeltaData.Rows)
        {
          _deltas = FillDeltaStruct(_row);
          _dict_deltas.Add((Int64)_row["TMD_ID"], _deltas);
        }

        m_rw_lock.AcquireWriterLock(Timeout.Infinite);

        m_terminal_meter_delta = _dict_deltas;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _error = true;
      }
      finally
      {
        if (m_rw_lock.IsWriterLockHeld)
        {
          m_rw_lock.ReleaseWriterLock();
        }
      }

      return !_error;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Fills Delta Struct whith data from a DataRow
    //
    //  PARAMS :
    //      - INPUT : DataRow
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    private static Deltas FillDeltaStruct(DataRow Row)
    {
      Deltas _deltas;

      _deltas = new Deltas();

      try
      {

        _deltas.CentsCoinAcceptor = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_COIN_ACCEPTOR", m_gp_deltas.CentsCoinAcceptor);
        _deltas.CentsBillIn = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_BILL_IN", m_gp_deltas.CentsBillIn);
        _deltas.CentsFTFromEgm = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_FT_FROM_EGM", m_gp_deltas.CentsFTFromEgm);
        _deltas.CentsFTToEgm = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_FT_TO_EGM", m_gp_deltas.CentsFTToEgm);
        _deltas.CentsHandPays = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_HANDPAYS", m_gp_deltas.CentsHandPays);
        _deltas.CentsJackpot = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_JACKPOT", m_gp_deltas.CentsJackpot);
        _deltas.CentsPlayed = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_PLAYED", m_gp_deltas.CentsPlayed);
        _deltas.CentsTicketIn = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_TICKET_IN", m_gp_deltas.CentsTicketIn);
        _deltas.CentsTicketOut = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_TICKET_OUT", m_gp_deltas.CentsTicketOut);
        _deltas.CentsWon = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_CENTS_WON", m_gp_deltas.CentsWon);
        _deltas.QuantityBillIn = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_QUANTITY_BILL_IN", m_gp_deltas.QuantityBillIn);
        _deltas.QuantityFTFromEgm = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_QUANTITY_FT_FROM_EGM", m_gp_deltas.QuantityFTFromEgm);
        _deltas.QuantityFTToEgm = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_QUANTITY_FT_TO_EGM", m_gp_deltas.QuantityFTToEgm);
        _deltas.QuantityGamesPlayed = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_QUANTITY_GAMES_PLAYED", m_gp_deltas.QuantityGamesPlayed);
        _deltas.QuantityGamesWon = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_QUANTITY_GAMES_WON", m_gp_deltas.QuantityGamesWon);
        _deltas.QuantityTicketIn = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_QUANTITY_TICKET_IN", m_gp_deltas.QuantityTicketIn);
        _deltas.QuantityTicketOut = FillDeltaStructGetValue(Row, "TMD_SAS_METER_BIG_INC_QUANTITY_TICKET_OUT", m_gp_deltas.QuantityTicketOut);
      }
      catch (Exception _ex)
      {
        Log.Message("Error on FillDeltaStruc");
        Log.Exception(_ex);
        throw _ex;
      }

      return _deltas;
    }

    private static Int64 FillDeltaStructGetValue(DataRow Row, String Name, Int64 DefaultValue)
    {
      try
      {
        if (Row[Name] != DBNull.Value)
        {
          return (Int64)Row[Name];
        }
      }
      catch
      {
        throw new Exception("Error on FillDeltaStructGetValue [" + Row["TMD_ID"] + "][" + Name + "]");
      }
      return DefaultValue;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Loads terminal group relations and fills the cache dictionary
    //
    //  PARAMS :
    //      - INPUT : Sqltransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    private static Boolean LoadGroupTerminalRelation(SqlTransaction Trx)
    {
      String _query;
      DataTable _dt_terminal_group;

      _query = GetTerminalMeterDeltaGroupQuery();

      if (FillDataTable(out _dt_terminal_group, _query, Trx))
      {
        if (FillTerminalGroupRelationDictionary(_dt_terminal_group))
        {
          return true;
        }
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Fills group terminal relation dictionary with a datatable
    //
    //  PARAMS :
    //      - INPUT : DataTable
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    private static Boolean FillTerminalGroupRelationDictionary(DataTable DtTerminalGroup)
    {
      Dictionary<Int32, Int64> _terminal_group;
      Boolean _error = false;

      _terminal_group = new Dictionary<Int32, Int64>();

      try
      {
        foreach (DataRow _row in DtTerminalGroup.Rows)
        {
          _terminal_group.Add((Int32)_row["TE_TERMINAL_ID"], (Int64)_row["TE_METER_DELTA_ID"]);
        }

        m_rw_lock.AcquireWriterLock(Timeout.Infinite);
        m_terminal_group_relation = _terminal_group;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _error = true;
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }

      return !_error;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminal group relation SQL Query
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    private static string GetTerminalMeterDeltaGroupQuery()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   TE_TERMINAL_ID                              ");
      _sb.AppendLine("        , TE_METER_DELTA_ID                           ");
      _sb.AppendLine("   FROM   TERMINALS                                   ");

      return _sb.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminal meter delta SQL Query
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    private static string GetTerminalMeterDeltaQuery()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   TMD_ID                                      ");
      _sb.AppendLine("        , TMD_DESCRIPTION                             ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_COIN_ACCEPTOR   ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_BILL_IN         ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_FT_FROM_EGM     ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_FT_TO_EGM       ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_HANDPAYS        ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_JACKPOT         ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_PLAYED          ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_TICKET_IN       ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_TICKET_OUT      ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_CENTS_WON             ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_QUANTITY_BILL_IN      ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_QUANTITY_FT_FROM_EGM  ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_QUANTITY_FT_TO_EGM    ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_QUANTITY_GAMES_PLAYED ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_QUANTITY_GAMES_WON    ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_QUANTITY_TICKET_IN    ");
      _sb.AppendLine("        , TMD_SAS_METER_BIG_INC_QUANTITY_TICKET_OUT   ");
      _sb.AppendLine("   FROM   TERMINAL_METER_DELTA                        ");

      return _sb.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Fills a datatable with the data from the specified query
    //
    //  PARAMS :
    //      - INPUT : String query, SqlTransaction 
    //
    //      - OUTPUT : DataTable
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    private static Boolean FillDataTable(out DataTable Data, String Query, SqlTransaction Trx)
    {
      Data = new DataTable();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(Query, Trx.Connection, Trx))
        {
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            Data.Load(_reader);
            return true;
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Message("Error on FillDataTable reading data with query: " + Query);
        Log.Exception(_ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminal delta parameters
    //
    //  PARAMS :
    //      - INPUT : Int32 Terminal ID
    //
    //      - OUTPUT :
    //
    // RETURNS : Deltas struct
    //
    //   NOTES : 
    //
    private static Deltas GetMeterDeltaValue(Int32 TerminalId)
    {
      Deltas _delta_value;

      _delta_value = m_gp_deltas;

      if (m_terminal_meter_delta != null && m_terminal_meter_delta.Count > 0)
      {
        _delta_value = GetDeltasFromDictionary(TerminalId);
      }
      return _delta_value;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminal delta parameters from de dictionary
    //
    //  PARAMS :
    //      - INPUT : Int32 Terminal ID
    //
    //      - OUTPUT :
    //
    // RETURNS : Deltas struct
    //
    //   NOTES : 
    //
    private static Deltas GetDeltasFromDictionary(int TerminalId)
    {
      Deltas _delta_value;
      Int64 _group_id;

      _delta_value = m_gp_deltas;

      try
      {
        m_ev_data_available.WaitOne();
        m_rw_lock.AcquireReaderLock(Timeout.Infinite);
        _group_id = GetMeterGroupId(TerminalId);
        m_terminal_meter_delta.TryGetValue(_group_id, out _delta_value);
      }
      catch (Exception _ex)
      {
        Log.Message("Error on GetMeterDeltaValue with TeminalId = " + TerminalId);
        Log.Exception(_ex);
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }
      return _delta_value;
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminal delta group for the specified TerminalId
    //
    //  PARAMS :
    //      - INPUT : Int32 Terminal ID
    //
    //      - OUTPUT :
    //
    // RETURNS : Deltas group Id
    //
    //   NOTES : 
    //
    private static Int64 GetMeterGroupId(Int32 TerminalId)
    {
      Int64 _group_id;

      try
      {
        m_terminal_group_relation.TryGetValue(TerminalId, out _group_id);
      }
      catch (Exception _ex)
      {
        Log.Message("Error on GetMeterGroupId on terminal = " + TerminalId);
        Log.Exception(_ex);

        _group_id = 0;
      }

      return _group_id;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Load GP delta parameters used for default values
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS : 
    //
    //   NOTES : 
    //
    private static void LoadDefaultGPDeltas()
    {
      m_gp_deltas.CentsCoinAcceptor = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.CoinAcceptor");
      m_gp_deltas.CentsBillIn = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.BillIn");
      m_gp_deltas.CentsFTFromEgm = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.FTFromEgm");
      m_gp_deltas.CentsFTToEgm = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.FTToEgm");
      m_gp_deltas.CentsHandPays = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.HandPays");
      m_gp_deltas.CentsJackpot = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.Jackpot");
      m_gp_deltas.CentsPlayed = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.Played");
      m_gp_deltas.CentsTicketIn = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.TicketIn");
      m_gp_deltas.CentsTicketOut = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.TicketOut");
      m_gp_deltas.CentsWon = GeneralParam.GetInt64("SasMeter.BigIncrement", "Cents.Won");
      m_gp_deltas.QuantityBillIn = GeneralParam.GetInt64("SasMeter.BigIncrement", "Quantity.BillIn");
      m_gp_deltas.QuantityFTFromEgm = GeneralParam.GetInt64("SasMeter.BigIncrement", "Quantity.FTFromEgm");
      m_gp_deltas.QuantityFTToEgm = GeneralParam.GetInt64("SasMeter.BigIncrement", "Quantity.FTToEgm");
      m_gp_deltas.QuantityGamesPlayed = GeneralParam.GetInt64("SasMeter.BigIncrement", "Quantity.GamesPlayed");
      m_gp_deltas.QuantityGamesWon = GeneralParam.GetInt64("SasMeter.BigIncrement", "Quantity.GamesWon");
      m_gp_deltas.QuantityTicketIn = GeneralParam.GetInt64("SasMeter.BigIncrement", "Quantity.TicketIn");
      m_gp_deltas.QuantityTicketOut = GeneralParam.GetInt64("SasMeter.BigIncrement", "Quantity.TicketOut");
    }
    #endregion
  }
}
