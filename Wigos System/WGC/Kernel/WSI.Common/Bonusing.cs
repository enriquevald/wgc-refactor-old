//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Bonusing.cs
// 
//   DESCRIPTION: Bonusing operation functions
//
//        AUTHOR: JMM
// 
// CREATION DATE: 03-MAR-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-MAR-2014 JMM     First release.
// 24-MAR-2017 ETP     WIGOS-255: Awarding on Bonus notified to EGM.
// 14-JUN-2017 ETP     WIGOS-1230: Jackpots Awarding - EGM notification.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class Bonusing
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Checks bonus status and the transferred amounts
    //
    //  PARAMS :
    //      - INPUT :
    //          - int JackpotIndex
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //          - MultiPromos.AccountBalance ToTransfer
    //          - BonusAwardInfo Info    
    //
    // RETURNS :
    //      - Boolean: True: No error. False: Error.
    //
    public static Boolean SiteJackpotReadBonusStatus(int JackpotIndex, SqlTransaction Trx, out BonusTransferStatus Status,
                                                     out MultiPromos.AccountBalance Transferred, out Int32 TargetTerminalId, out Int64 TargetAccountId)
    {
      StringBuilder _sb;
      Boolean _show_on_notification_EGM;
      Status = BonusTransferStatus.UnknownTransactionId;
      Transferred = MultiPromos.AccountBalance.Zero;

      TargetTerminalId = 0;
      TargetAccountId = 0;
      _show_on_notification_EGM = GeneralParam.GetBoolean("SiteJackpot", "Bonusing.ShowOnNotificationEGM", true);

      try
      {
        // Read bonus status & transferred amount
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   ISNULL (BNS_TRANSFER_STATUS, 0)      AS BNS_TRANSFER_STATUS      ");
        _sb.AppendLine("           , ISNULL (BNS_TRANSFERRED_RE, 0)       AS BNS_TRANSFERRED_RE       ");
        _sb.AppendLine("           , ISNULL (BNS_TRANSFERRED_PROMO_RE, 0) AS BNS_TRANSFERRED_PROMO_RE ");
        _sb.AppendLine("           , ISNULL (BNS_TRANSFERRED_PROMO_NR, 0) AS BNS_TRANSFERRED_PROMO_NR ");
        _sb.AppendLine("           , ISNULL (BNS_TO_TRANSFER_RE, 0)       AS BNS_TO_TRANSFER_RE       ");
        _sb.AppendLine("           , ISNULL (BNS_TO_TRANSFER_PROMO_RE, 0) AS BNS_TO_TRANSFER_PROMO_RE ");
        _sb.AppendLine("           , ISNULL (BNS_TO_TRANSFER_PROMO_NR, 0) AS BNS_TO_TRANSFER_PROMO_NR ");
        _sb.AppendLine("           , BNS_TARGET_TERMINAL_ID                                           ");
        _sb.AppendLine("           , BNS_TARGET_ACCOUNT_ID                                            ");
        _sb.AppendLine("      FROM   SITE_JACKPOT_INSTANCES                                           ");
        _sb.AppendLine("INNER JOIN   BONUSES ON SJI_BONUS_ID = BNS_BONUS_ID                           ");
        _sb.AppendLine("     WHERE   SJI_INDEX = @JackpotIdx                                          ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@JackpotIdx", SqlDbType.Int).Value = JackpotIndex;

          using (SqlDataReader _dr = _cmd.ExecuteReader())
          {
            if (_dr.Read())
            {
              Status = (BonusTransferStatus)_dr.GetInt32(_dr.GetOrdinal("BNS_TRANSFER_STATUS"));  //TODO JMM 25-FEB-2014: Simplify or use a purpose-made enum

              if (Status == BonusTransferStatus.Notified && _show_on_notification_EGM)
              {
                Transferred.Redeemable = _dr.GetDecimal(_dr.GetOrdinal("BNS_TO_TRANSFER_RE"));
                Transferred.PromoRedeemable = _dr.GetDecimal(_dr.GetOrdinal("BNS_TO_TRANSFER_PROMO_RE"));
                Transferred.PromoNotRedeemable = _dr.GetDecimal(_dr.GetOrdinal("BNS_TO_TRANSFER_PROMO_NR"));
              }
              else
              {
                Transferred.Redeemable = _dr.GetDecimal(_dr.GetOrdinal("BNS_TRANSFERRED_RE"));
                Transferred.PromoRedeemable = _dr.GetDecimal(_dr.GetOrdinal("BNS_TRANSFERRED_PROMO_RE"));
                Transferred.PromoNotRedeemable = _dr.GetDecimal(_dr.GetOrdinal("BNS_TRANSFERRED_PROMO_NR"));
              }
              TargetTerminalId = _dr.GetInt32(_dr.GetOrdinal("BNS_TARGET_TERMINAL_ID"));
              TargetAccountId = _dr.GetInt64(_dr.GetOrdinal("BNS_TARGET_ACCOUNT_ID"));

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" Bonusing.SiteJackpotCheckBonusAwarded: Exception thrown -> " + _ex.Message);
      }

      return false;

    } // SiteJackpotReadBonusStatus

    public static Boolean JackpotDetailsReadBonusStatus(Int64 JackpotDetailId, SqlTransaction Trx, out BonusTransferStatus Status,
                                                     out MultiPromos.AccountBalance Transferred, out Int32 TargetTerminalId, out Int64 TargetAccountId)
    {
      StringBuilder _sb;
      Status = BonusTransferStatus.UnknownTransactionId;
      Transferred = MultiPromos.AccountBalance.Zero;

      TargetTerminalId = 0;
      TargetAccountId = 0;

      try
      {
        // Read bonus status & transferred amount
        _sb = new StringBuilder();
        
        _sb.AppendLine("    SELECT   ISNULL (BNS_TRANSFER_STATUS, 0)      AS BNS_TRANSFER_STATUS      ");
        _sb.AppendLine("           , ISNULL (BNS_TRANSFERRED_RE, 0)       AS BNS_TRANSFERRED_RE       ");
        _sb.AppendLine("           , ISNULL (BNS_TRANSFERRED_PROMO_RE, 0) AS BNS_TRANSFERRED_PROMO_RE ");
        _sb.AppendLine("           , ISNULL (BNS_TRANSFERRED_PROMO_NR, 0) AS BNS_TRANSFERRED_PROMO_NR ");
        _sb.AppendLine("           , ISNULL (BNS_TO_TRANSFER_RE, 0)       AS BNS_TO_TRANSFER_RE       ");
        _sb.AppendLine("           , ISNULL (BNS_TO_TRANSFER_PROMO_RE, 0) AS BNS_TO_TRANSFER_PROMO_RE ");
        _sb.AppendLine("           , ISNULL (BNS_TO_TRANSFER_PROMO_NR, 0) AS BNS_TO_TRANSFER_PROMO_NR ");
        _sb.AppendLine("           , BNS_TARGET_TERMINAL_ID                                           ");
        _sb.AppendLine("           , BNS_TARGET_ACCOUNT_ID                                            ");
        _sb.AppendLine("      FROM   JACKPOTS_AWARD_DETAIL                                            ");
        _sb.AppendLine("INNER JOIN   BONUSES ON JAD_BONUS_ID = BNS_BONUS_ID                           ");
        _sb.AppendLine("     WHERE   JAD_ID = @pJackpotDetailId                                       ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pJackpotDetailId", SqlDbType.Int).Value = JackpotDetailId;

          using (SqlDataReader _dr = _cmd.ExecuteReader())
          {
            if (_dr.Read())
            {
              Status = (BonusTransferStatus)_dr.GetInt32(_dr.GetOrdinal("BNS_TRANSFER_STATUS"));  

              Transferred.Redeemable = _dr.GetDecimal(_dr.GetOrdinal("BNS_TRANSFERRED_RE"));
              Transferred.PromoRedeemable = _dr.GetDecimal(_dr.GetOrdinal("BNS_TRANSFERRED_PROMO_RE"));
              Transferred.PromoNotRedeemable = _dr.GetDecimal(_dr.GetOrdinal("BNS_TRANSFERRED_PROMO_NR"));

              TargetTerminalId = _dr.GetInt32(_dr.GetOrdinal("BNS_TARGET_TERMINAL_ID"));
              TargetAccountId = _dr.GetInt64(_dr.GetOrdinal("BNS_TARGET_ACCOUNT_ID"));

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" Bonusing.JackpotDetailsReadBonusStatus: Exception thrown -> " + _ex.Message);
      }

      return false;

    } // JackpotDetailsReadBonusStatus



    //------------------------------------------------------------------------------
    // PURPOSE : Checks if there is any bonus pending to award
    //
    //  PARAMS :
    //      - INPUT :    
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True: there is at least one bonus pending to award. False: no bonus pending to award.
    //
    public static Boolean AnySiteJackpotBonusPending()
    {
      MultiPromos.AccountBalance _transferred;
      BonusTransferStatus _status;
      Int32 _target_terminal_id;
      Int64 _target_account_id;
      int _jackpot_idx;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        for (_jackpot_idx = 1; _jackpot_idx < 4; _jackpot_idx++)
        {
          if (!SiteJackpotReadBonusStatus(_jackpot_idx, _db_trx.SqlTransaction,
                                          out _status, out _transferred, out _target_terminal_id, out _target_account_id))
          {
            //No bonus related to the jackpot instance
            continue;
          }

          //There is a bonus related to the Jackpot instance, so return true
          return true;
        }
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts a jackpot on the DB to be awarded (bonusing)
    //
    //  PARAMS :
    //      - INPUT :
    //          - int JackpotIndex
    //          - MultiPromos.AccountBalance ToTransfer
    //          - BonusAwardInfo Info
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True: No error. False: Error.
    //
    public static Boolean SiteJackpotInsertToBonus(int JackpotIndex, MultiPromos.AccountBalance ToTransfer,
                                                   BonusAwardInfo Info, SqlTransaction Trx)
    {
      Int64 _bonus_id;
      StringBuilder _sb;
      Int32 _num_rows;

      // InsertBonus 
      _bonus_id = InsertBonus(ToTransfer, Info, Trx);

      if (_bonus_id == 0)
      {
        return false;
      }

      try
      {
        // UpdateJackpotInstance
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   SITE_JACKPOT_INSTANCES     ");
        _sb.AppendLine("   SET   SJI_BONUS_ID = @pBonusID   ");
        _sb.AppendLine(" WHERE   SJI_INDEX    = @JackpotIdx ");
        _sb.AppendLine("   AND   SJI_BONUS_ID IS NULL       ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@JackpotIdx", SqlDbType.Int).Value = JackpotIndex;
          _cmd.Parameters.Add("@pBonusID", SqlDbType.BigInt).Value = _bonus_id;

          _num_rows = _cmd.ExecuteNonQuery();

          if (_num_rows == 1)
          {
            return true;
          }

          Log.Warning(String.Format(" Bonusing.SiteJackpotInsertToBonus: Error updating SITE_JACKPOT_INSTANCES.  Rows affected {0}", _num_rows));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" Bonusing.SiteJackpotInsertToBonus: Exception thrown -> " + _ex.Message);
      }

      return false;
    } // SiteJackpotInsertToBonus

    /// <summary>
    /// Jackpot Detail Insert To Bonus
    /// </summary>
    /// <param name="JackpotDetailIndex"></param>
    /// <param name="JackpotIndex"></param>
    /// <param name="ToTransfer"></param>
    /// <param name="Info"></param>
    /// <param name="DetailsSharedId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean JackpotDetailInsertToBonus(Int64 JackpotDetailIndex, Int32 JackpotIndex, MultiPromos.AccountBalance ToTransfer,
                                                  BonusAwardInfo Info, Int64 DetailsSharedId, SqlTransaction Trx)
    {
      Int64 _bonus_id;
      StringBuilder _sb;
      Int32 _num_rows;
      Int32 _num_to_update;

      // InsertBonus 
      _bonus_id = InsertBonus(ToTransfer, Info, Trx);

      if (_bonus_id == 0)
      {
        return false;
      }

      try
      {
        // UpdateJackpotInstance
        _sb = new StringBuilder();
        
        _sb.AppendLine("UPDATE   JACKPOTS_AWARD_DETAIL             ");
        _sb.AppendLine("   SET   JAD_BONUS_ID = @pBonusID          ");

        if (DetailsSharedId == -1)
        {
          _sb.AppendLine(" WHERE   JAD_ID    = @pDetailIdx         ");
          _num_to_update = 1;
        }
        else //if main winner wins shared part is paid with the same bonus id:
        {
          _sb.AppendLine(" WHERE   JAD_ID  in (@pDetailIdx        ");
          _sb.AppendLine("                  ,  @pDetailsSharedId) ");
          _num_to_update = 2;
        }
        
        
        _sb.AppendLine("   AND   JAD_BONUS_ID IS NULL             ");



        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDetailIdx", SqlDbType.BigInt).Value = JackpotDetailIndex;
          _cmd.Parameters.Add("@pDetailsSharedId", SqlDbType.BigInt).Value = DetailsSharedId;
          _cmd.Parameters.Add("@pBonusID", SqlDbType.BigInt).Value = _bonus_id;

          _num_rows = _cmd.ExecuteNonQuery();

          if (_num_rows == _num_to_update)
          {
            return true;
          }

          Log.Warning(String.Format(" Bonusing.SiteJackpotInsertToBonus: Error updating JACKPOTS_AWARD_DETAIL.  Rows affected {0}", _num_rows));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" Bonusing.SiteJackpotInsertToBonus: Exception thrown -> " + _ex.Message);
      }

      return false;
    } // JackpotDetailInsertToBonus


    //------------------------------------------------------------------------------
    // PURPOSE : Inserts a new bonus on the DB to be awarded
    //
    //  PARAMS :
    //      - INPUT :
    //          - MultiPromos.AccountBalance ToTransfer
    //          - BonusAwardInfo Info  
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Int64: containing the bonus id corresponding to the BNS_BONUS_ID field on the BONUSES table
    //
    private static Int64 InsertBonus(MultiPromos.AccountBalance ToTransfer, BonusAwardInfo Info, SqlTransaction Trx)
    {
      try
      {
        StringBuilder _sb;
        Int32 _num_rows;
        SqlParameter _output_param;

        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO   BONUSES                   ");
        _sb.AppendLine("            ( BNS_TO_TRANSFER_RE        ");
        _sb.AppendLine("            , BNS_TO_TRANSFER_PROMO_RE  ");
        _sb.AppendLine("            , BNS_TO_TRANSFER_PROMO_NR  ");
        _sb.AppendLine("            , BNS_SOURCE_TYPE           ");
        _sb.AppendLine("            , BNS_SOURCE_BIGINT1        ");
        _sb.AppendLine("            , BNS_SOURCE_BIGINT2        ");
        _sb.AppendLine("            , BNS_SOURCE_INT1           ");
        _sb.AppendLine("            , BNS_SOURCE_INT2           ");
        _sb.AppendLine("            , BNS_TARGET_TYPE           ");
        _sb.AppendLine("            , BNS_TARGET_TERMINAL_ID    ");
        _sb.AppendLine("            , BNS_TARGET_ACCOUNT_ID     ");
        _sb.AppendLine("            , BNS_WCP_CMD_ID_AWARD      ");
        _sb.AppendLine("            , BNS_WCP_CMD_ID_STATUS     ");
        _sb.AppendLine("            , BNS_TRANSFER_STATUS)      ");
        _sb.AppendLine("     VALUES ( @pToTransferRe            ");
        _sb.AppendLine("            , @pToTransferPromoRe       ");
        _sb.AppendLine("            , @pToTransferPromoNr       ");
        _sb.AppendLine("            , @pSourceType              ");
        _sb.AppendLine("            , @pSourceBigInt1           ");
        _sb.AppendLine("            , @pSourceBigInt2           ");
        _sb.AppendLine("            , @pSourceInt1              ");
        _sb.AppendLine("            , @pSourceInt2              ");
        _sb.AppendLine("            , @pTargetType              ");
        _sb.AppendLine("            , @pTargetTerminalId        ");
        _sb.AppendLine("            , @pTargetAccountId         ");
        _sb.AppendLine("            , @pWCPCmdIdAward           ");
        _sb.AppendLine("            , @pWCPCmdIdStatus          ");
        _sb.AppendLine("            , @pTransferStatus)         ");

        _sb.AppendLine("SET @pBonusId = SCOPE_IDENTITY()        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          // Amount
          _cmd.Parameters.Add("@pToTransferRe", SqlDbType.Money).Value = ToTransfer.Redeemable;
          _cmd.Parameters.Add("@pToTransferPromoRe", SqlDbType.Money).Value = ToTransfer.PromoRedeemable;
          _cmd.Parameters.Add("@pToTransferPromoNr", SqlDbType.Money).Value = ToTransfer.PromoNotRedeemable;
          // Source
          _cmd.Parameters.Add("@pSourceType", SqlDbType.Int).Value = Info.SourceType;
          _cmd.Parameters.Add("@pSourceBigInt1", SqlDbType.BigInt).Value = Info.SourceBigInt1;
          _cmd.Parameters.Add("@pSourceBigInt2", SqlDbType.BigInt).Value = Info.SourceBigInt2;
          _cmd.Parameters.Add("@pSourceInt1", SqlDbType.Int).Value = Info.SourceInt1;
          _cmd.Parameters.Add("@pSourceInt2", SqlDbType.Int).Value = Info.SourceInt2;
          // Target
          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = Info.TargetType;
          _cmd.Parameters.Add("@pTargetTerminalId", SqlDbType.Int).Value = Info.TargetTerminalId;
          _cmd.Parameters.Add("@pTargetAccountId", SqlDbType.BigInt).Value = Info.TargetAccountId;
          // WCP Cmd
          _cmd.Parameters.Add("@pWCPCmdIdAward", SqlDbType.Int).Value = 0;
          _cmd.Parameters.Add("@pWCPCmdIdStatus", SqlDbType.Int).Value = 0;
          //Transfer status
          _cmd.Parameters.Add("@pTransferStatus", SqlDbType.Int).Value = (int)BonusTransferStatus.Pending;
          //Output parameter
          _output_param = new SqlParameter("@pBonusId", SqlDbType.BigInt);
          _output_param.Direction = ParameterDirection.Output;

          _cmd.Parameters.Add(_output_param);

          _num_rows = _cmd.ExecuteNonQuery();

          if (_num_rows != 1)
          {
            Log.Warning(String.Format(" Bonusing.InsertBonus: Error inserting Bonus.  Rows affected {0}", _num_rows));
          }

          return (Int64)_output_param.Value;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" Bonusing.InsertBonus: Exception thrown -> " + _ex.Message);
      }

      return 0;
    } // InsertBonus

    //------------------------------------------------------------------------------
    // PURPOSE : Set a jackpot as awarded (bonusing)
    //
    //  PARAMS :
    //      - INPUT :
    //          - int JackpotIndex
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True: No error. False: Error.
    //
    public static Boolean BonusAwarded_UnlinkSiteJackpotInstance(int JackpotIndex, SqlTransaction Trx)
    {
      try
      {
        StringBuilder _sb;
        Int32 _num_rows;

        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   SITE_JACKPOT_INSTANCES                 ");
        _sb.AppendLine("   SET   SJI_BONUS_ID    = NULL                 ");
        _sb.AppendLine(" WHERE   SJI_INDEX       = @JackpotIdx          ");
        _sb.AppendLine("   AND   SJI_BONUS_ID IS NOT NULL               ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@JackpotIdx", SqlDbType.Int).Value = JackpotIndex;

          _num_rows = _cmd.ExecuteNonQuery();

          if (_num_rows != 1)
          {
            Log.Warning(String.Format(" Bonusing.SiteJackpotBonusAwarded: Error updating SITE_JACKPOT_INSTANCES.  Rows affected {0}", _num_rows));

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(" Bonusing.SiteJackpotBonusAwarded: Exception thrown -> " + _ex.Message);
      }

      return false;

    } // SiteJackpotBonusAwarded
  }
}
