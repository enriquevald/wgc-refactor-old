﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: WCPPlayerRequestLogin.cs
// 
//      DESCRIPTION: Class check the login of the player than requested the gift
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 31-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-OCT-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public class WCPPlayerRequestLogin
  {
    #region " Constants "
    public enum ErrorCodePlayerRequest
    {
      WCP_RC_WKT_OK = 10000,                            // OK
      WCP_RC_WKT_DISABLED = 10001,                      // Kiosk disabled         / Kiosco deshabilitado
      WCP_RC_WKT_ERROR = 10002,                         // Error                  / Error
      WCP_RC_WKT_MESSAGE_NOT_SUPPORTED = 10003,         // Message Not Supported  / Mensaje no soportado
      WCP_RC_WKT_RESOURCE_NOT_FOUND = 10004,            // Resource Not found     / Recurso no encontrado

      // Login
      WCP_RC_WKT_CARD_NOT_VALID = 10010,                // Card Not Valid     / Tarjeta no válida
      WCP_RC_WKT_ACCOUNT_BLOCKED = 10011,               // Acccount Blocked   / Cuenta bloqueada
      WCP_RC_WKT_ACCOUNT_NO_PIN = 10012,                // PIN not configured / PIN sin configurar
      WCP_RC_WKT_WRONG_PIN = 10013,                     // Wrong PIN          / PIN erróneo

    }
    #endregion

    #region " Constructor "
    public WCPPlayerRequestLogin()
    {

    }
    #endregion

    #region " Public functions "
    public bool WKT_PlayerLogin(String ExternalTrackData, String PIN, out Int64 AccountId, out Int32 ErrorCode)
    {
      String _sql_txt;
      String _internal_track_data;
      Int32 _dummy;

      ErrorCode = (Int32)ErrorCodePlayerRequest.WCP_RC_WKT_ERROR;
      AccountId = 0;

      try
      {
        // is card valid --> Not found WCP_RC_WKT_CARD_NOT_VALID
        if (!CardNumber.TrackDataToInternal(ExternalTrackData, out _internal_track_data, out _dummy))
        {
          ErrorCode = (Int32)ErrorCodePlayerRequest.WCP_RC_WKT_CARD_NOT_VALID;

          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_txt = "";
          _sql_txt += "SELECT   AC_ACCOUNT_ID       ";
          _sql_txt += "  FROM   ACCOUNTS            ";
          _sql_txt += " WHERE   AC_TRACK_DATA = @pIntTrackData";

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt))
          {
            Object _obj;

            _sql_cmd.Parameters.Add("@pIntTrackData", SqlDbType.NVarChar, 50).Value = _internal_track_data;
            _obj = _db_trx.ExecuteScalar(_sql_cmd);

            if (_obj == null)
            {
              ErrorCode = (Int32)ErrorCodePlayerRequest.WCP_RC_WKT_CARD_NOT_VALID;

              return false;
            }

            AccountId = (Int64)_obj;
          }
        }

        switch (Accounts.DB_CheckAccountPIN(AccountId, PIN))
        {
          case PinCheckStatus.OK:
            ErrorCode = (Int32)ErrorCodePlayerRequest.WCP_RC_WKT_OK;
            return true;

          case PinCheckStatus.WRONG_PIN:
            ErrorCode = (Int32)ErrorCodePlayerRequest.WCP_RC_WKT_WRONG_PIN;
            break;

          case PinCheckStatus.ACCOUNT_BLOCKED:
            ErrorCode = (Int32)ErrorCodePlayerRequest.WCP_RC_WKT_ACCOUNT_BLOCKED;
            break;

          case PinCheckStatus.ACCOUNT_NO_PIN:
            ErrorCode = (Int32)ErrorCodePlayerRequest.WCP_RC_WKT_ACCOUNT_NO_PIN;
            break;

          case PinCheckStatus.ERROR:
          default:
            ErrorCode = (Int32)ErrorCodePlayerRequest.WCP_RC_WKT_ERROR;
            break;
        } // switch (_pin_check_status)
      }
      catch
      {
        ErrorCode = (Int32)ErrorCodePlayerRequest.WCP_RC_WKT_ERROR;
      }

      return false;
    } //WKT_PlayerLogin
    #endregion 

  }

}
