﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Card.cs
// 
//   DESCRIPTION: Class to manage CARDS table
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 29-NOV-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-NOV-2016 JBP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public class UserCard
  {
    #region " Constants "

    private const Int16 FIELD_USERDATA_TRACKDATA      =  0;
    private const Int16 FIELD_USERDATA_HOLDERNAME     =  1;
    private const Int16 FIELD_USERDATA_USERNAME       =  2;
    private const Int16 FIELD_USERDATA_LINKED_TYPE    =  3;
    private const Int16 FIELD_USERDATA_LINKED_ID      =  4;
    private const Int16 FIELD_USERDATA_BLOCK_REASON   =  5;
    private const Int16 FIELD_USERDATA_BLOCK_DATETIME =  6;
    private const Int16 FIELD_USERDATA_PIN            =  7;
    private const Int16 FIELD_USERDATA_CHANGED        =  8;
    private const Int16 FIELD_USERDATA_EXPIRES        =  9;
    private const Int16 FIELD_USERDATA_ERRORS         = 10;

    private const Int16 MAX_PIN_ERRORS                = 3;

    #endregion 

    #region " Members "

    private Int32 m_id;
    private String m_holder_name;
    private String m_username;
    private WCP_CardTypes m_card_type;
    private String m_trackdata;
    private String m_pin;
    private DateTime? m_pin_updated;
    private DateTime? m_pin_expires;
    private Int32 m_pin_errors;
    private AccountBlockReason m_block_reason;
    private DateTime? m_block_datetime;
    private UserCardStatus m_status;

    #endregion

    #region " Properties "

    public Int32 Id
    {
      get { return this.m_id; }
      set { this.m_id = value; }
    } // Id

    public String HolderName
    {
      get { return this.m_holder_name; }
      set { this.m_holder_name = value; }
    } // HolderName

    public String UserName
    {
      get { return this.m_username; }
      set { this.m_username = value; }
    } // UserName

    public String TrackData
    {
      get { return this.m_trackdata; }
      set { this.m_trackdata = value; }
    } // TrackData

    public WCP_CardTypes CardType
    {
      get { return this.m_card_type; }
      set { this.m_card_type = value; }
    } // CardType

    public String Pin
    {
      get { return this.m_pin; }
      set { this.m_pin = value; }
    } // Pin

    public DateTime? PinUpdated
    {
      get { return this.m_pin_updated; }
      set { this.m_pin_updated = value; }
    } // PinUpdated

    public DateTime? PinExpires
    {
      get { return this.m_pin_expires; }
      set { this.m_pin_expires = value; }
    } // PinExpires

    public Int32 PinErrors
    {
      get { return this.m_pin_errors; }
      set { this.m_pin_errors = value; }
    } // PinErrors

    public DateTime? BlockDatetime
    {
      get { return this.m_block_datetime; }
      set { this.m_block_datetime = value; }
    } // PinExpires

    public AccountBlockReason BlockReason
    {
      get { return this.m_block_reason; }
      set { this.m_block_reason = value; }
    } // BlockReason

    public UserCardStatus Status
    {
      get { return this.m_status; }
      set { this.m_status = value; }
    } // Status
        
    #endregion

    #region " Public Methods "

    #region " Constructor "

    public UserCard(Int32 UserId, WCP_CardTypes CardType)
    {
      this.GetCardData(UserId, String.Empty, CardType);    
    } // UserCard
    public UserCard(String TrackData, WCP_CardTypes CardType)
    {
      Initialize();

      if (!String.IsNullOrEmpty(TrackData))
      {
        this.GetCardData(TrackData, CardType);
      }
    } // UserCard
    public UserCard(Int32 UserId, String TrackData, WCP_CardTypes CardType)
    {
      Initialize();

      if (UserId > 0)
      {
        this.GetCardData(UserId, TrackData, CardType);
      }
    } // UserCard

    #endregion 

    /// <summary>
    /// Get Card data By UserId and/or TrackData and CardType
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="TrackData"></param>
    /// <param name="CardType"></param>
    /// <returns></returns>
    public Boolean GetCardData(Int32 UserId, WCP_CardTypes CardType)
    {
      return GetCardData(UserId, String.Empty, CardType);
    } // GetCardData
    public Boolean GetCardData(String TrackData, WCP_CardTypes CardType)
    {
      return GetCardData(0, TrackData, CardType);
    } // GetCardData
    public Boolean GetCardData(Int32 UserId, String TrackData, WCP_CardTypes CardType)
    {
      this.m_id        = UserId;
      this.m_trackdata = TrackData;
      this.m_card_type = CardType;

      return this.DB_GetCardData();
    } // GetCardData

    /// <summary>
    /// Check pin user
    /// </summary>
    /// <param name="PinRequest"></param>
    /// <returns></returns>
    public PinCheckStatus CheckUserPin(String PinRequest)
    {
      try
      {
        // check if pin is empty
        if(String.IsNullOrEmpty(this.m_pin))
        {
          return PinCheckStatus.ACCOUNT_NO_PIN;
        }

        // Check pin errors 
        if (this.m_pin_errors >= MAX_PIN_ERRORS)
        {
          return BlockUser();
        }

        // Check pin
        if(PinRequest != this.m_pin)
        {
          return this.AddPinError();
        }

        this.ResetNumTrys();

        return PinCheckStatus.OK;
      }
      catch(Exception _ex)
      {
        Log.Exception(_ex);
      }

      return PinCheckStatus.ERROR;
    } // CheckUserPin

    /// <summary>
    /// Reset num trys
    /// </summary>
    public void ResetNumTrys()
    {
      this.m_pin_errors = 0;

      DB_UpdateCardData();
    } // ResetNumTrys

    /// <summary>
    /// Add pin error
    /// </summary>
    private PinCheckStatus AddPinError()
    {
      this.m_pin_errors++;

      // Check num errors
      if (this.m_pin_errors >= MAX_PIN_ERRORS)
      {
        return BlockUser();    
      }

      // Update card data
      if(!DB_UpdateCardData())
      {
        return PinCheckStatus.ERROR;
      }

      return PinCheckStatus.WRONG_PIN;
    } // AddPinError

    /// <summary>
    /// Block user and card
    /// </summary>
    /// <returns></returns>
    private PinCheckStatus BlockUser()
    {
      this.m_status         = UserCardStatus.AccountBlocked;
      this.m_block_reason   = AccountBlockReason.MAX_PIN_FAILURES;
      this.m_block_datetime = WGDB.Now;
      
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!DB_UpdateCardData(_db_trx.SqlTransaction))
        {
          return PinCheckStatus.ERROR;   
        }

        UserLogin.DisableUser(this.m_id, _db_trx.SqlTransaction);

        _db_trx.Commit();
      }

      return PinCheckStatus.ACCOUNT_BLOCKED;   
    } // BlockUser

    /// <summary>
    /// Update Card Pin
    /// </summary>
    /// <param name="Pin"></param>
    /// <returns></returns>
    public Boolean UpdatePin(String Pin)
    {
      if(this.m_id <= 0)
      {
        this.m_status = UserCardStatus.NoExist;

        return false;
      }

      this.m_pin          = Pin.Trim();
      this.m_pin_errors   = 0;
      this.m_pin_updated  = WGDB.Now;

      return this.DB_UpdateCardData();
    } // UpdatePin
    
    #endregion
    
    #region " Private Methods "

    /// <summary>
    /// Initialize properties
    /// </summary>
    private void Initialize()
    {
      this.m_trackdata      = String.Empty;
      this.m_holder_name    = String.Empty;
      this.m_username       = String.Empty;
      this.m_id             = 0;
      this.m_card_type      = WCP_CardTypes.CARD_TYPE_UNKNOWN;
      this.m_pin            = String.Empty;
      this.m_pin_errors     = 0;      
      this.m_block_reason   = 0;
      this.m_block_datetime = null;
      this.m_pin_expires    = null;
      this.m_pin_updated    = null;
      this.m_status         = UserCardStatus.NoExist;

    } // Initialize

    /// <summary>
    /// Get card data
    /// </summary>
    /// <returns></returns>
    private Boolean DB_GetCardData()
    {
      this.m_status = UserCardStatus.GeneralError;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(GetCardDataQuery(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTrackData" , SqlDbType.Char, 20).Value = this.m_trackdata;
            _sql_cmd.Parameters.Add("@pUserId"    , SqlDbType.BigInt  ).Value = this.m_id;
            _sql_cmd.Parameters.Add("@pCardType"  , SqlDbType.Int     ).Value = (Int32)this.m_card_type;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                this.m_status = UserCardStatus.NoExist;

                return false;
              }
                            
              this.m_trackdata      = (_reader[FIELD_USERDATA_TRACKDATA]      == DBNull.Value) ? String.Empty                     : Convert.ToString(_reader[FIELD_USERDATA_TRACKDATA]).Trim();
              this.m_holder_name    = (_reader[FIELD_USERDATA_HOLDERNAME]     == DBNull.Value) ? String.Empty                     : Convert.ToString(_reader[FIELD_USERDATA_HOLDERNAME]).Trim();
              this.m_username       = (_reader[FIELD_USERDATA_USERNAME]       == DBNull.Value) ? String.Empty                     : Convert.ToString(_reader[FIELD_USERDATA_USERNAME]).Trim();
              this.m_card_type      = (_reader[FIELD_USERDATA_LINKED_TYPE]    == DBNull.Value) ? WCP_CardTypes.CARD_TYPE_UNKNOWN  : (WCP_CardTypes)_reader[FIELD_USERDATA_LINKED_TYPE];
              this.m_id             = (_reader[FIELD_USERDATA_LINKED_ID]      == DBNull.Value) ? 0                                : Convert.ToInt32(_reader[FIELD_USERDATA_LINKED_ID]);
              this.m_block_reason   = (_reader[FIELD_USERDATA_BLOCK_REASON]   == DBNull.Value) ? AccountBlockReason.NONE          : (AccountBlockReason)_reader[FIELD_USERDATA_BLOCK_REASON];
              this.m_pin            = (_reader[FIELD_USERDATA_PIN]            == DBNull.Value) ? String.Empty                     : Convert.ToString(_reader[FIELD_USERDATA_PIN]).Trim();
              this.m_pin_errors     = (_reader[FIELD_USERDATA_ERRORS]         == DBNull.Value) ? 0                                : Convert.ToInt32(_reader[FIELD_USERDATA_ERRORS]);

              // Dates 
              this.m_block_datetime = (_reader[FIELD_USERDATA_BLOCK_DATETIME] == DBNull.Value) ? null                             : (DateTime?)_reader[FIELD_USERDATA_BLOCK_DATETIME];
              this.m_pin_updated    = (_reader[FIELD_USERDATA_CHANGED]        == DBNull.Value) ? null                             : (DateTime?)_reader[FIELD_USERDATA_CHANGED];
              this.m_pin_expires    = (_reader[FIELD_USERDATA_EXPIRES]        == DBNull.Value) ? null                             : (DateTime?)_reader[FIELD_USERDATA_CHANGED];

              // Set status
              this.m_status = (String.IsNullOrEmpty(this.m_pin.Trim())) ? UserCardStatus.NoPin : UserCardStatus.Ok;

              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        this.m_status = UserCardStatus.GeneralError;

        Log.Error(ex.Message);
      }

      return false;

    } // DB_GetCardData

    /// <summary>
    /// Update card data
    /// </summary>
    /// <returns></returns>
    private Boolean DB_UpdateCardData()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if(DB_UpdateCardData(_db_trx.SqlTransaction))
        {
          return _db_trx.Commit();
        }
      }

      return false;
    } // DB_UpdateCardData
    private Boolean DB_UpdateCardData(SqlTransaction SqlTrx)
    {
      this.m_status = UserCardStatus.GeneralError;

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(UpdateCardDataQuery(), SqlTrx.Connection, SqlTrx))
        {
          // Set parameters
          _sql_cmd.Parameters.Add("@pUserId"        , SqlDbType.BigInt  ).Value = this.m_id;
          _sql_cmd.Parameters.Add("@pBlockReason"   , SqlDbType.Int     ).Value = this.m_block_reason;            
          _sql_cmd.Parameters.Add("@pPin"           , SqlDbType.Char    ).Value = this.m_pin.Trim();
          _sql_cmd.Parameters.Add("@pPinErrors"     , SqlDbType.Int     ).Value = this.m_pin_errors;
          _sql_cmd.Parameters.Add("@pCardType"      , SqlDbType.Int     ).Value = this.m_card_type;

          // Date parameters
          _sql_cmd.Parameters.Add(this.AddDateParam("@pBlockDatetime" , this.m_block_datetime));
          _sql_cmd.Parameters.Add(this.AddDateParam("@pPinExpires"    , this.m_pin_expires)   );
          _sql_cmd.Parameters.Add(this.AddDateParam("@pPinChanged"    , this.m_pin_updated)   );
            
          if(_sql_cmd.ExecuteNonQuery() != 1)
          {
            this.m_status = UserCardStatus.NoExist;

            return false;
          }

          this.m_status = UserCardStatus.Ok;

          return true;
        }
      }
      catch (Exception ex)
      {
        this.m_status = UserCardStatus.GeneralError;

        Log.Error(ex.Message);
      }

      return false;

    } // DB_UpdateCardData

    /// <summary>
    /// Add date param
    /// </summary>
    /// <param name="Param"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    private SqlParameter AddDateParam(String Param, DateTime? Value)
    {
      SqlParameter _sql_param;

      _sql_param = new SqlParameter(Param, SqlDbType.DateTime);

      if (Value == null)
      {
        _sql_param.Value = DBNull.Value;
      }
      else
      {
        _sql_param.Value = Value;
      }
      
      return _sql_param;

    } // AddDateParam

    /// <summary>
    /// Query to Get user info from Card table 
    /// </summary>
    /// <returns></returns>
    private string GetCardDataQuery()
    {
      String _operator;
      StringBuilder _sb;

      _operator = "OR";

      if( this.m_id > 0 && !String.IsNullOrEmpty(this.m_trackdata))
      {
        _operator = "AND";
      }

      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   CA_TRACKDATA                                                ");
      _sb.AppendLine("            , GU_FULL_NAME                                                ");
      _sb.AppendLine("            , GU_USERNAME                                                 ");
      _sb.AppendLine("            , CA_LINKED_TYPE                                              ");
      _sb.AppendLine("            , CA_LINKED_ID                                                ");
      _sb.AppendLine("            , CA_BLOCK_REASON                                             ");
      _sb.AppendLine("            , CA_BLOCK_DATETIME                                           ");
      _sb.AppendLine("            , CA_PIN                                                      ");
      _sb.AppendLine("            , CA_PIN_CHANGED                                              ");
      _sb.AppendLine("            , CA_PIN_EXPIRES                                              ");
      _sb.AppendLine("            , CA_PIN_ERRORS                                               ");
      _sb.AppendLine("            , CA_BLOCK_REASON                                             ");
      _sb.AppendLine("       FROM   CARDS                                                       ");
      _sb.AppendLine(" INNER JOIN   GUI_USERS  ON  CA_LINKED_ID = GU_USER_ID                    ");
      _sb.AppendLine("      WHERE   CA_LINKED_TYPE  = @pCardType                                ");
      _sb.AppendLine("        AND ( CA_LINKED_ID    = @pUserId {0} CA_TRACKDATA = @pTrackData ) ");

      return String.Format(_sb.ToString(), _operator);
    }  // GetCardDataQuery

    /// <summary>
    /// Query to Update user info from Card table 
    /// </summary>
    /// <returns></returns>
    private string UpdateCardDataQuery()
    {
      String _block_reason;
      StringBuilder _sb;

      _block_reason = "dbo.BlockReason_ToNewEnumerate(CA_BLOCK_REASON) | @pBlockReason";
      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   CARDS                               ");
      _sb.AppendLine("    SET   CA_BLOCK_REASON   = {0}             ");
      _sb.AppendLine("        , CA_BLOCK_DATETIME = @pBlockDatetime ");
      _sb.AppendLine("        , CA_PIN            = @pPin           ");
      _sb.AppendLine("        , CA_PIN_CHANGED    = @pPinChanged    ");
      _sb.AppendLine("        , CA_PIN_EXPIRES    = @pPinExpires    ");
      _sb.AppendLine("        , CA_PIN_ERRORS     = @pPinErrors     ");
      _sb.AppendLine("   FROM   CARDS                               ");
      _sb.AppendLine("  WHERE   CA_LINKED_ID      = @pUserId        ");
      _sb.AppendLine("    AND   CA_LINKED_TYPE    = @pCardType      ");
      
      return String.Format(_sb.ToString(), _block_reason);
    }  // UpdateCardDataQuery

    #endregion 

  }
}
