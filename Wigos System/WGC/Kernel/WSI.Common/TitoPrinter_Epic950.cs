//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TitoPrinter_Epic950.cs
// 
//   DESCRIPTION: TitoPrinter_Epic950 class
// 
//        AUTHOR: Daniel Dom�nguez Mart�n 
// 
// CREATION DATE: 14-Nov-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-Nov-2013 DDM    First version.
// 21-JAN-2014 DHA    Fixed Bug #WIGOSTITO-1000: Wrong currency format in case '0.XX'
// 12-FEB-2014 DHA    Fixed Bug #WIGOSTITO-1060: When printer is not configured, only logs the first error for each thread iteration
// 03-DIC-2014 DDM    The definition of regions and template 
// 23-AUG-2016 DDM    Fixed Bug 16499.
// 13-FEB-2017 JBP    Bug 23946:En la impresi�n de Tickets TITO en Chile el formato de fecha es incorrecto
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using POSPRINTERLib;
using Microsoft.Win32;

namespace WSI.Common
{
  public class TitoPrinter_Epic950 : ITitoPrinter
  {
    #region Enums and constants

    // Status Mechanism/Return Printer Status
    public static Int32 Code_Epic950_Mechanism_PrinterReady = 0x01;
    public static Int32 Code_Epic950_Mechanism_HeadIsUp = 0x08;
    public static Int32 Code_Epic950_Mechanism_ChasisIsOpen = 0x10;
    public static Int32 Code_Epic950_Mechanism_OutOfTicket = 0x20;

    // Status paper/Request Printer Status
    public static Int32 Code_Epic950_Paper_PaperLow = 0x01;
    public static Int32 Code_Epic950_Paper_PaperJam = 0x80;
    public static Int32 Code_Epic950_Paper_TicketInPath = 0x40;

    public enum CodePortState
    {
      PortUnavailable = 0,
      PortReady = 1,
      SerialDSR = 0x010000,
      SerialCTS = 0x020000,
      Parallel_SEL = 0x000100,
      Parallel_BSY = 0x000200,
      Parallel_OFFLINE = 0x00800,
      Parallel_PE = 0x02000,
    }

    #endregion Enums and constants

    Boolean m_init = false;
    POSPrinterClass m_printer_class;
    private Int32 m_print_time;
    private Boolean m_error_logged = false;

    public int PrintTime
    {
      get
      {
        return m_print_time;
      }
      set
      {
        m_print_time = value;
      }
    }

    public TitoPrinter_Epic950()
    {
      PrintTime = 3000;
    }

    #region ITitoPrinter Members

    public bool Init()
    {
      CodePortState _port_state;

      try
      {
        if (m_init)
        {
          return true;
        }

        // DDM & JML 27-11-2013: Component instalation
        if (!WSI.Common.Misc.InstallExternalComponent("POSPRINTER.ocx"))
        {
          return false;
        }

        m_printer_class = new POSPrinterClass();

        // Try to open the port. Don't care about the return code / staus
        OpenPort(out _port_state);

        if (_port_state == CodePortState.PortReady)
        {
          m_init = true;

          return true;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Print ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //          - PrintTicket ticket
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      True if tickuet printed
    //      False otherwise
    // 
    //   NOTES:
    //    
    public bool PrintTicket(TicketInfo Ticket)
    {
      String _buffer;
      Int32 _result;
      String _title;

      try
      {
        if (m_printer_class == null || !m_init)
        {
          Log.Error("Tito Printer no Initializated.");

          return false;
        }

        if (m_printer_class.PortState() != (Int32)CodePortState.PortReady)
        {
          Log.Error("TitoPrinter. PortState is not ready");

          return false;
        }
        // DDM 23/08/2016: Fixed Bug 16499:Fecha en tickets TITO con formato incorrecto
        //Ticket.StrExpiration = FormatDate(Ticket.Expiration);

        // The printer required the following formats:
        //  - Date:  mm/dd/yyyy
        //  - Money: $x,xxx.xx     
        // NOTE: In the case of another format, It is the firmware who will make the change to another format
        Ticket.StrDate = FormatDate(Ticket.Date);
        Ticket.StrExpiration = FormatDate(Ticket.Expiration); 
        Ticket.StrAmount = FormatCurrency(Ticket.Amount);

        _title = Ticket.StrTitle;
        if (String.IsNullOrEmpty(_title))
        {
          _title = Ticket.GetNameTicketType();
        }

        _buffer = "";
        //1- Validation number
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43;
        _buffer += (Char)0x01;
        _buffer += Ticket.StrValidationNumber;
        _buffer += (Char)0x0D; //CR

        //2- Your Establishment/Casino
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x02;
        _buffer += Ticket.LblLocation + Ticket.StrLocation;
        _buffer += (Char)0x0D; //CR

        //3- Location 
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x03;
        _buffer += Ticket.LblAddress1 + Ticket.StrAddress1;
        _buffer += (Char)0x0D; //CR

        //4- City/State/Zip 
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x04;
        _buffer += Ticket.LblAddress2 + Ticket.StrAddress2;
        _buffer += (Char)0x0D; //CR

        //5- Date 
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x05;
        _buffer += Ticket.LblDate + Ticket.StrDate;
        _buffer += (Char)0x0D; //CR

        //6-Time 
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x06;
        _buffer += Ticket.LblTime + Ticket.StrTime;
        _buffer += (Char)0x0D; //CR

        //7-Ticket #number)
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x07;
        _buffer += Ticket.LblSequence + Ticket.StrSequence;
        _buffer += (Char)0x0D; //CR

        //11-after time 
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x0B;
        _buffer += Ticket.LblExpiration + Ticket.StrExpiration;
        _buffer += (Char)0x0D; //CR

        //12-machine number 
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x0C;
        _buffer += Ticket.LblMachine + Ticket.StrMachine;
        _buffer += (Char)0x0D; //CR

        //16-validation - Validation text
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x10;
        _buffer += Ticket.LblValidationNumber;
        _buffer += (Char)0x0D; //CR

        //15-barcode - Title promo
        _buffer += (Char)0x1D;
        _buffer += (Char)0x43; //C
        _buffer += (Char)0x0F;
        _buffer += _title;
        _buffer += (Char)0x0D; //CR

        if (GeneralParam.GetInt32("Tito", "TicketTemplate", 0) == 1)
        {
          if (!DefineTemplate101())
          {
            return false;
          }

          //10-numeric amount ($ 4)
          _buffer += (Char)0x1D;
          _buffer += (Char)0x43; //C
          _buffer += (Char)0x15;
          _buffer += Ticket.LblAmount + Ticket.StrAmount;
          _buffer += (Char)0x0D; //CR

          //20-alpha dollar amount  (Four Dollars..)
          _buffer += (Char)0x1D;
          _buffer += (Char)0x43; //C
          _buffer += (Char)0x14; //20
          //TODO: pending development...
          _buffer += "";//Ticket.LblAmountLetter + Ticket.StrAmountLetter;
          _buffer += (Char)0x0D; //CR

          //19-barcode 
          _buffer += (Char)0x1D;
          _buffer += (Char)0x43; //C
          _buffer += (Char)0x13; // 19 
          _buffer += Ticket.LblBarcodeNumber + Ticket.StrBarcodeNumber;
          _buffer += (Char)0x0D; //CR

          // Win Template..
          _buffer += (Char)0x1D;
          _buffer += (Char)0x4F; //C
          _buffer += (Char)0x65; //101
        }
        else
        {
          switch (Ticket.TicketType)
          {
            case TITO_TICKET_TYPE.CASHABLE:
            case TITO_TICKET_TYPE.PROMO_REDEEM:
            case TITO_TICKET_TYPE.PROMO_NONREDEEM:
              //10-numeric amount ($ 4)
              _buffer += (Char)0x1D;
              _buffer += (Char)0x43; //C
              _buffer += (Char)0x0A;
              _buffer += Ticket.LblAmount + Ticket.StrAmount;
              _buffer += (Char)0x0D; //CR

              //8-alpha dollar amount  (Four Dollars..)
              _buffer += (Char)0x1D;
              _buffer += (Char)0x43; //C
              _buffer += (Char)0x08;
              _buffer += Ticket.LblAmountLetter + Ticket.StrAmountLetter;
              _buffer += (Char)0x0D; //CR

              //9-continued alpha dollar amount field)
              _buffer += (Char)0x1D;
              _buffer += (Char)0x43; //C
              _buffer += (Char)0x09;
              _buffer += Ticket.LblAmountLetter + Ticket.StrAmountLetter;
              _buffer += (Char)0x0D; //CR

              //13-barcode 
              _buffer += (Char)0x1D;
              _buffer += (Char)0x43; //C
              _buffer += (Char)0x0D;
              _buffer += Ticket.LblBarcodeNumber + Ticket.StrBarcodeNumber;
              _buffer += (Char)0x0D; //CR

              // Transact template..
              _buffer += (Char)0x1D;
              _buffer += (Char)0x4F; //C
              _buffer += (Char)0x52; //82   
              break;

            case TITO_TICKET_TYPE.HANDPAY:
            case TITO_TICKET_TYPE.JACKPOT:
            default:
              Log.Error("Print ticket is not implemented. TicketType: " + Ticket.TicketType.ToString());
              break;
          }
        }
        _result = m_printer_class.SendString(_buffer, _buffer.Length);

        
        if (_result == 1)
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // PrintTicket



    //------------------------------------------------------------------------------
    // PURPOSE: Get status
    // 
    //  PARAMS:
    //      - INPUT:
    //          - NONE
    //         
    //      - OUTPUT:
    //          - IsReady
    //          - Status
    //          - PrinterErrorText
    //
    // RETURNS:
    //          - NONE
    // 
    //   NOTES:
    //          
    public void GetStatus(out Boolean IsReady, out PrinterStatus Status, out String PrinterErrorText)
    {
      String _data;
      Byte[] _binary_state;
      Int32 _mechanism_state;
      Int32 _paper_state;
      CodePortState _port_state;

      IsReady = false;
      PrinterErrorText = "Unknown";
      Status = PrinterStatus.Unknown;

      try
      {
        // Check Port status. If port not ready, you try to open port
        _port_state = GetPortState();
        if (_port_state != CodePortState.PortReady)
        {
          if (!OpenPort(out _port_state))
          {
            Status = PrinterStatus.NotOpenPort;

            PrinterErrorText = "Not open port";
            return;
          }
        }

        // Combined status: Return or paper /Request or Printer 
        _data = "";
        _data += (Char)0x1D;
        _data += (Char)0x79;

        if (m_printer_class.SendString(_data, _data.Length) == 0)
        {
          Status = PrinterStatus.Unknown;

          return;
        }

        _binary_state = (Byte[])m_printer_class.ReadDataArr(512);
        _paper_state = _binary_state[3];
        _mechanism_state = _binary_state[2];

        PrinterErrorText = "0x" + _mechanism_state.ToString("X").PadLeft(2, '0') + " - " + "0x" + _paper_state.ToString("X").PadLeft(2, '0');

        if ((_mechanism_state & Code_Epic950_Mechanism_OutOfTicket) == Code_Epic950_Mechanism_OutOfTicket)
        {
          Status = PrinterStatus.OutOfPaper;
          PrinterErrorText += ": " + " Out of ticket";

          return;
        }
        if ((_mechanism_state & Code_Epic950_Mechanism_HeadIsUp) == Code_Epic950_Mechanism_HeadIsUp)
        {
          Status = PrinterStatus.Error;
          PrinterErrorText += ": " + " Head is up";

          return;
        }
        if ((_mechanism_state & Code_Epic950_Mechanism_ChasisIsOpen) == Code_Epic950_Mechanism_ChasisIsOpen)
        {
          Status = PrinterStatus.Error;
          PrinterErrorText += ": " + " Chasis is open";

          return;
        }

        if ((_paper_state & Code_Epic950_Paper_PaperJam) == Code_Epic950_Paper_PaperJam)
        {
          Status = PrinterStatus.Error;
          PrinterErrorText += ": " + " Paper jam";

          return;
        }

        if ((_paper_state & Code_Epic950_Paper_TicketInPath) == Code_Epic950_Paper_TicketInPath)
        {
          IsReady = true;
          Status = PrinterStatus.TicketInPath;
          PrinterErrorText += ": " + " Ticket In Path";
        }

        if ((_paper_state & Code_Epic950_Paper_PaperLow) == Code_Epic950_Paper_PaperLow)
        {
          IsReady = true;
          if (Status == PrinterStatus.TicketInPath)
          {
            Status = PrinterStatus.TicketInPathAndPaperLow;
            PrinterErrorText += ": " + " Ticket in path and paper low";
          }
          else
          {
            Status = PrinterStatus.PaperLow;
            PrinterErrorText += ": " + " Paper low";
          }

          return;
        }

        if ((_mechanism_state & Code_Epic950_Mechanism_PrinterReady) == Code_Epic950_Mechanism_PrinterReady)
        {
          IsReady = true;
          Status = PrinterStatus.Ok;
          PrinterErrorText = "";

          return;
        }

        Status = PrinterStatus.Error;
        PrinterErrorText += ":" + " Error";
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      Status = PrinterStatus.Unknown;
    } // Status

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Open port Printer
    // 
    //  PARAMS:
    //      - INPUT:
    //          - NONE
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      - True: operation completed successfully
    //      - False: operation failed
    // 
    //   NOTES:
    //    
    private Boolean OpenPort(out CodePortState PortState)
    {
      String _port_name;
      String _error_desc;
      Int32 _result;

      PortState = CodePortState.PortUnavailable;
      try
      {
        if (!FindPortUSB_Epic950(out _port_name, out _error_desc))
        {
          // DHA 12-FEB-2014: Show error for first time
          if (!m_error_logged)
          {
            Log.Error(_error_desc);
            m_error_logged = true;
          }

          return false;
        }

        _result = m_printer_class.OpenPort(_port_name, "");
        if (_result != 1)
        {
          // DHA 12-FEB-2014: Show error for first time
          if (!m_error_logged)
          {
            Log.Error("Tito Printer. Not open port");
            m_error_logged = true;
          }

          return false;
        }

        PortState = GetPortState();

        m_error_logged = false;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    } // OpenPort

    //------------------------------------------------------------------------------
    // PURPOSE: Close port Printer
    // 
    //  PARAMS:
    //      - INPUT:
    //          - NONE
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      - True: operation completed successfully
    //      - False: operation failed
    // 
    //   NOTES:
    //    
    private Boolean ClosePort()
    {
      if (m_printer_class != null)
      {
        return m_printer_class.ClosePort() == 1;
      }
      return false;

    } // ClosePort

    //------------------------------------------------------------------------------
    // PURPOSE: Get port state
    // 
    //  PARAMS:
    //      - INPUT:
    //          - NONE
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //    
    private CodePortState GetPortState()
    {
      Int32 _port_state;

      if (m_printer_class == null)
      {
        return CodePortState.PortUnavailable;
      }

      _port_state = m_printer_class.PortState();

      if (_port_state == 0)
      {
        return CodePortState.PortUnavailable;
      }

      if ((_port_state & (Int32)CodePortState.PortReady) == (Int32)CodePortState.PortReady)
      {
        return CodePortState.PortReady;
      }

      if ((_port_state & (Int32)CodePortState.SerialDSR) == (Int32)CodePortState.SerialDSR)
      {
        return CodePortState.SerialDSR;
      }
      if ((_port_state & (Int32)CodePortState.SerialCTS) == (Int32)CodePortState.SerialCTS)
      {
        return CodePortState.SerialCTS;
      }
      if ((_port_state & (Int32)CodePortState.Parallel_SEL) == (Int32)CodePortState.Parallel_SEL)
      {
        return CodePortState.Parallel_SEL;
      }
      if ((_port_state & (Int32)CodePortState.Parallel_BSY) == (Int32)CodePortState.Parallel_BSY)
      {
        return CodePortState.Parallel_BSY;
      }
      if ((_port_state & (Int32)CodePortState.Parallel_OFFLINE) == (Int32)CodePortState.Parallel_OFFLINE)
      {
        return CodePortState.Parallel_OFFLINE;
      }
      if ((_port_state & (Int32)CodePortState.Parallel_PE) == (Int32)CodePortState.Parallel_PE)
      {
        return CodePortState.Parallel_PE;
      }

      return CodePortState.PortUnavailable;

    } // GetPortState

    //------------------------------------------------------------------------------
    // PURPOSE: Find port usb to the printer epic950.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - NONE
    //
    //      - OUTPUT:
    //          - PortName
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //    
    private Boolean FindPortUSB_Epic950(out String PortName, out String ErrorDescription)
    {
      RegistryKey _reg = Registry.LocalMachine;
      RegistryKey _reg_sub_dir;
      String _path_key;
      Int32 _port_number;
      String _new_path;
      Boolean _vendor_found;
      Boolean _vendor_and_product_found;

      ErrorDescription = "";
      PortName = "";
      _port_number = -1;
      _vendor_found = false;
      _vendor_and_product_found = false;
      try
      {
        _path_key = @"SYSTEM\CurrentControlSet\Control\DeviceClasses\{28d78fad-5a12-11d1-ae5b-0000f803a8c2}";
        _reg_sub_dir = _reg.OpenSubKey(_path_key, false);
        _new_path = "";

        // DHA 12-FEB-2014: Validate the key in system register exist
        if (_reg_sub_dir == null)
        {
          return false;
        }

        foreach (String _sub_key in _reg_sub_dir.GetSubKeyNames())
        {
          if (_sub_key.ToUpper().Contains("VID_0613&PID_2213"))
          {
            Log.Message("Tito Printer. Installation log: " + _sub_key.ToUpper().ToString());
            _vendor_and_product_found = true;

            _new_path = _path_key + @"\" + _sub_key + @"\#" + @"\Device Parameters";
            _reg_sub_dir = _reg.OpenSubKey(_new_path, false);
            _port_number = (Int32)_reg_sub_dir.GetValue("Port Number", -1);

            if (_port_number != -1)
            {
              PortName = "USB" + _port_number.ToString("000");

              return true;
            }
          }
          if (_sub_key.ToUpper().Contains("VID_0613") && !_vendor_and_product_found)
          {
            Log.Message("Tito Printer. Installation log: " + _sub_key.ToUpper().ToString());
            _vendor_found = true;
          }
        }

        ErrorDescription = "Tito Printer is not installed";
        if (_vendor_and_product_found)
        {
          ErrorDescription = "Tito Printer is not connected";

          if (_vendor_found)
          {
            // TODO: In this case, is possible that printer connected does not have the expected firmware..          
            ErrorDescription = "Tito Printer is not connected or does not have the expected firmware";
          }

          return false;
        }
        if (_vendor_found)
        {
          ErrorDescription = "Tito Printer does not have the expected firmware";

          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // FindPortUSB_Epic950

    //------------------------------------------------------------------------------
    // PURPOSE: Format decimal value to currency
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Decimal value
    //
    //      - OUTPUT:
    //          - PortName
    //
    // RETURNS:
    //      String with ',' as grouping digit and '.' as decimal separator with two digits.    
    //      if defined template is used, the format used is the system
    //   NOTES:
    //    
    private String FormatCurrency(Decimal Amount)
    {
      if (GeneralParam.GetInt32("Tito", "TicketTemplate", 0) == 1)
      {
        Currency _curr;
        _curr = Amount;

        return _curr.ToString();
      }
      else
      {
        return Amount.ToString("$#,##0.00", System.Globalization.CultureInfo.InvariantCulture);

      }
    } // FormatCurrency

    //------------------------------------------------------------------------------
    // PURPOSE: Format date value 
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Datetime value
    //
    //      - OUTPUT:    
    //
    // RETURNS:
    //      String with {0:MM/dd/yyyy}.
    //      if defined template is used, the format used is the system
    //   NOTES:
    //    
    private String FormatDate(DateTime Date)
    {
      if (GeneralParam.GetInt32("Tito", "TicketTemplate", 0) == 1)
      {
        return Date.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern);
      }
      else
      {
        return String.Format("{0:" + GeneralParam.GetString("TITO", "TicketInfo.FormatDate", "MM/dd/yyyy") + "}", Date);
      }
    } // FormatDate

    //------------------------------------------------------------------------------
    // PURPOSE: Function that defines the Afeter date. The region number is 115
    // 
    //  PARAMS:
    //      - INPUT:
    //          - None
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      - True: Region generated. False: Otherwise.
    // 
    public Boolean DefineRegion115_AfterDate()
    {
      String _buffer;
      Int32 _result;

      _buffer = "";

      //Create region            
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x73; // 115

      //Print direction
      _buffer += (Char)0x1B; // ESC 
      _buffer += (Char)0x55; // t
      _buffer += (Char)0x01;

      //Print mode
      _buffer += (Char)0x1B; // ESC 
      _buffer += (Char)0x21; // !
      _buffer += (Char)0x01;

      //Select character size
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x21; // !
      _buffer += (Char)0x00;

      //Clear emphasized print
      _buffer += (Char)0x1B; // ESC 
      _buffer += (Char)0x47; // G
      _buffer += (Char)0x00;

      //Set absolute vertical position
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x24; // $
      _buffer += (Char)0x01; // 1 
      _buffer += (Char)0xD0; // 208

      //Set field
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x46; // F
      _buffer += (Char)0x02; // 0     --> (0 - left, 1 - center, 2 - rigth)
      //(50 - 1000) 
      _buffer += (Char)0x00;
      _buffer += (Char)0x32; // 50
      _buffer += (Char)0x03; // 3    
      _buffer += (Char)0xE8; // 232

      //End region            
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x73; // 115

      _result = m_printer_class.SendString(_buffer, _buffer.Length);

      if (_result == 1)
      {
        return true;
      }

      Log.Error("No generated AfterDate region");

      return false;
    } // DefineRegion115_AfterDate

    //------------------------------------------------------------------------------
    // PURPOSE: Function that defines the amount region. The region number is 114
    // 
    //  PARAMS:
    //      - INPUT:
    //          - None
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      - True: Region generated. False: Otherwise.
    // 
    public Boolean DefineRegion114_Amount()
    {
      String _buffer;
      Int32 _result;

      _buffer = "";
      //Create region            
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x72; // 114    
       
      //Set absolute vertical position
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x24; // $
      _buffer += (Char)0x1;  // 1 
      _buffer += (Char)0xA0; // 194

      //Set field
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x46; // F
      _buffer += (Char)0x01;
      _buffer += (Char)0x01;
      _buffer += (Char)0x9F;
      _buffer += (Char)0x04; 
      _buffer += (Char)0x00; 

      //Print direction
      _buffer += (Char)0x1B; // ESC 
      _buffer += (Char)0x55; // t
      _buffer += (Char)0x01;

      //Set Font "ESC T - headline
      _buffer += (Char)0x1B; // ESC 
      _buffer += 'T'; // 0x55;

      //SET emphasized print
      //_buffer += (Char)0x1B; // ESC 
      //_buffer += (Char)0x47; // G
      //_buffer += (Char)0x00;

      //End region            
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x72; // 114    

      _result = m_printer_class.SendString(_buffer, _buffer.Length);

      if (_result == 1)
      {
        return true;
      }

      Log.Error("No generated Amount region");

      return false;
    } // DefineRegion114_Amount

    //------------------------------------------------------------------------------
    // PURPOSE: Function that defines the Alphanumeric amount region. The region number is 112
    // 
    //  PARAMS:
    //      - INPUT:
    //          - None
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      - True: Region generated. False: Otherwise.
    // 
    public Boolean DefineRegion112_AlphaAmount()
    {
      String _buffer;
      Int32 _result;

      _buffer = "";
      //Create region            
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x70; // 112    

      //Print direction
      _buffer += (Char)0x1B; // ESC 
      _buffer += (Char)0x55; // t
      _buffer += (Char)0x01;

      //Print mode
      _buffer += (Char)0x1B; // ESC 
      _buffer += (Char)0x22; // !
      _buffer += (Char)0x00;

      //Select character size
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x21; // !
      _buffer += (Char)0x0;

      //Clear emphasized print
      _buffer += (Char)0x1B; // ESC 
      _buffer += (Char)0x47; // G
      _buffer += (Char)0x00;

      //Set absolute vertical position
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x24; // $
      _buffer += (Char)0x1;  // 1 
      _buffer += (Char)0x78; // 120

      //Set field
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x46; // F
      _buffer += (Char)0x01;
      _buffer += (Char)0x0;
      _buffer += (Char)0x0;
      _buffer += (Char)0x03; // 3     
      _buffer += (Char)0xC0; // 192

      //End region            
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x70; // 112    

      _result = m_printer_class.SendString(_buffer, _buffer.Length);

      if (_result == 1)
      {
        return true;
      }

      Log.Error("No generated AlphaAmount region");

      return false;
    } // DefineRegion112_AlphaAmount

    //------------------------------------------------------------------------------
    // PURPOSE: Function that defines the barcode region. The region number is 118
    // 
    //  PARAMS:
    //      - INPUT:
    //          - None
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      - True: Region generated. False: Otherwise.
    // 
    public Boolean DefineRegion118_BarCode()
    {
      String _buffer;
      Int32 _result;

      _buffer = "";
      //Create region            
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x76; // 118  

      //Print direction
      _buffer += (Char)0x1B; // ESC 
      _buffer += (Char)0x55; // t
      _buffer += (Char)0x03; // set print dirction in page mode =  D

      //Clear emphasized print
      _buffer += (Char)0x1B; // ESC 
      _buffer += (Char)0x47; // G
      _buffer += (Char)0x00;

      //Set absolute vertical position
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x24; // $
      _buffer += (Char)0x00; // 0
      _buffer += (Char)0xA0; // 160

      //Starting position barcode
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x41; // A  
      _buffer += (Char)0x00; // 0
      _buffer += (Char)0xFA; // 250

      //SET barcode element width (thin,thick)
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x57; // W
      _buffer += (Char)0x04; // 4
      _buffer += (Char)0x08; // 8

      //SET barcode element heigth
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x68; // h
      _buffer += (Char)0xAF; // 175

      //Print bar code
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x6B; // k 107
      _buffer += (Char)0x07; // 7
      _buffer += (Char)0x12; // 18D


      //End region            
      _buffer += (Char)0x1D; // GS
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x76; // 118    

      _result = m_printer_class.SendString(_buffer, _buffer.Length);
      
      if (_result == 1)
      {
        return true;
      }

      Log.Error("No generated BarCode region");

      return false;
    } // DefineRegion118_BarCode

    //------------------------------------------------------------------------------
    // PURPOSE: Function that defines the template
    // 
    //  PARAMS:
    //      - INPUT:
    //          - None
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      - True: Template generated. False: Otherwise.
    // 
    //   NOTES:
    //      - For use it must be enabled the general param: 'Tito'-'TicketTemplate'
    //      - The user defined regions and templates are stored in RAM and must be reloaded after a power loss.
    public Boolean DefineTemplate101()
    {
      String _buffer;
      Int32 _result;

      // DDM 03-12-2014: It is not checked the existance of template. 
      // It takes more time checking its existance  than generating the template again

      ////////Check template 
      ////////_buffer = "";
      ////////_buffer += (Char)0x1D; // GS
      ////////_buffer += (Char)0x51; // Q
      ////////_buffer += (Char)0x65; // 101
      ////////_result = m_printer_class.SendString(_buffer, _buffer.Length);
      ////////Byte[] _result_check;
      ////////_result_check = (Byte[])m_printer_class.ReadDataArr(2);

      //
      // Definition of regions
      //
      if (!DefineRegion112_AlphaAmount())
      {
        return false;
      }
      if (!DefineRegion114_Amount())
      {
        return false;
      }
      if (!DefineRegion118_BarCode())
      {
        return false;
      }
      if (!DefineRegion115_AfterDate())
      {
        return false;
      }

      //
      // Definition of template
      //
      _buffer = "";
      //Begin region/template
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x65; // 101

      //Region 1 - Initialize template  
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x01; // region 1

      //Region 2: Using data buffer 1  --> setup for validation # field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x02; // region 2
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x01; // buffer 1
      _buffer += (Char)0x0D; // CR

      //Region 3: Using data buffer 2  --> setup for establisment field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x03; // region 3
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x02; // buffer 2 
      _buffer += (Char)0x0D; //CR

      //Region 4: Using data buffer 3  --> setup for Your address 1  field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x04; // region 4
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x03; // buffer 3
      _buffer += (Char)0x0D; // CR

      //Region 5: Using data buffer 4  --> setup for Your address 2  field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x05; // region 5
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x04; // buffer 4
      _buffer += (Char)0x0D; // CR

      //Region 38: Using data buffer 15   --> setup for dynamic Banner field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x26; // Region 38
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x0F; // buffer 15
      _buffer += (Char)0x0D; // CR

      //Region 39: Not Using data buffer 16 --> setup for dynamic Validation text
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x27; // Region 39. x27,39D
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x10; // buffer 16
      _buffer += (Char)0x0D; // CR

      //Region 8: Not Using data buffer  validation number vertical 
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x08; // region 8
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x01; // buffer 1
      _buffer += (Char)0x0D; // CR

      //Region 9: Using data buffer 5    --> setup for Date field)
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x09; // Region 9
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x05; // buffer 5
      _buffer += (Char)0x0D; // CR

      //Region 10: Using data buffer 6  --> setup for Time field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x0A; // Region 10
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x06; // buffer 6
      _buffer += (Char)0x0D; // CR

      ////Region 11: Using data buffer 7  --> setup for Ticket # field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x0B; // Region 11
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x07; // buffer 7
      _buffer += (Char)0x0D; // CR

      ////Region 12 Using data buffer 8  -->setup for ALPHA dollar amount field
      //_buffer += (Char)0x1D;
      //_buffer += (Char)0x4F; //O
      //_buffer += (Char)0x15; //Region 21  //_buffer += (Char)0x0C; //Region 12
      //_buffer += (Char)0x1D;
      //_buffer += (Char)0x44; //D
      //_buffer += (Char)0x08; // buffer 8
      //_buffer += (Char)0x0D; //CR

      /////Region 112 Using data buffer 20  -->setup for continued ALPHA dollar amount field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x70; // Region 112
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x14; // buffer 20
      _buffer += (Char)0x0D; // CR

      ////Region 13 Using data buffer 9  -->setup for ALPHA dollar amount field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x16; // Region 22//_buffer += (Char)0x0d; //Region 13
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x09; // buffer 9
      _buffer += (Char)0x0D; // CR

      //////Region 14 Using data buffer 10   --> setup for numeric amount field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x72; // Region 114      //_buffer += (Char)0x0E; //Region 14
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x15; // buffer 21
      _buffer += (Char)0x0D; // CR

      //////Region 15 Not Using data buffer   --> setup for void after time field
      //_buffer += (Char)0x1D;
      //_buffer += (Char)0x4F; // O
      //_buffer += (Char)0x0F; // Region 15
      //_buffer += (Char)0x1D;
      //_buffer += (Char)0x44; // D
      //_buffer += (Char)0x0B; // buffer 11
      //_buffer += (Char)0x0D; // CR

      //Region 16 Using data buffer 11   -->setup for void after time field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x73; //
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x0B; // buffer 11 
      _buffer += (Char)0x0D; // CR

      ////Region 17 Using data buffer 12   -->  setup for machine number field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x11; // Region 17
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x0C; // buffer 12
      _buffer += (Char)0x0D; // CR

      ////Region 18 Using data buffer 13   -->setup for barcode field
      //_buffer += (Char)0x1D;
      //_buffer += (Char)0x4F; // O
      //_buffer += (Char)0x12; // Region 18
      //_buffer += (Char)0x1D;
      //_buffer += (Char)0x44; // D
      //_buffer += (Char)0x0D; // buffer 13
      //_buffer += (Char)0x0D; //CR

      //Region 118 Using data buffer 19   -->setup for barcode field
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4F; // O
      _buffer += (Char)0x76; // Region 118
      _buffer += (Char)0x1D;
      _buffer += (Char)0x44; // D
      _buffer += (Char)0x13; // buffer 19
      _buffer += (Char)0x0D; // CR

      //Form Feed
      _buffer += (Char)0x0C;

      //Clear data fields
      _buffer += (Char)0x1D;
      _buffer += (Char)0x43; // C
      _buffer += (Char)0x00;

      //End region/template
      _buffer += (Char)0x1D;
      _buffer += (Char)0x4D; // M
      _buffer += (Char)0x65; // 101

      _result = m_printer_class.SendString(_buffer, _buffer.Length);

      if (_result == 1)
      {
        return true;
      }
      Log.Error("no defined template");

      return false;
    } //DefineTemplate101
  }

}
