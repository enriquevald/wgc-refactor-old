//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Cage.cs
// 
//   DESCRIPTION:
//
//        AUTHOR: Sergi Mart�nez
// 
// CREATION DATE: 08-AUG-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-AUG-2014 SMN    Initial version.
// 10-MAR-2015 YNM    Fixed Bug WIG-2137: Stock on Close Session is different that Current Stock.
// 10-JAN-2017 CCG    Bug 20565:Error resumen de boveda - arqueos.
// 19-JUN-2017 FOs    Bug 27552:Cage session show its open when isn't it.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI;
using WSI.Common;
using System.Collections;

namespace WSI.Common
{
  public class CageSessionReport
  {
    public enum CAGE_SYSTEM_CONCEPTS
    {
      CIRCULANTING = 1,
      STATE_TAX = 2,
      FEDERAL_TAX = 3,
      PROV_PROGRESSIV = 4
    }

    public enum CAGE_SOURCE_TARGETS
    {
      CUSTOM = -1,
      CASHIERS = 10,
      GAMING_TABLES = 11,
      TERMINALS = 12
    }

    public TYPE_CAGE_GLOBAL_SESSION_REPORT ReadCageGlobalReport()
    {
      Boolean _session_status;

      return ReadCageGlobalSessionsReport("ALL", out _session_status);
    }

    public TYPE_CAGE_GLOBAL_SESSION_REPORT ReadCageGlobalSessionsReport(String CageIdList, out Boolean OneSessionStatus)
    {
      DataSet _ds;
      TYPE_CAGE_GLOBAL_SESSION_REPORT _report;

      OneSessionStatus = false;
      _report = new TYPE_CAGE_GLOBAL_SESSION_REPORT();
      _ds = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand("CageGetGlobalReportData", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandType = CommandType.StoredProcedure;

            if (CageIdList != "ALL")
            {
              _sql_cmd.Parameters.Add("@pCageSessionIds", SqlDbType.VarChar).Value = CageIdList;
            }

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_ds);

              _report.CageDtStock = _ds.Tables[0];
              _report.CageDtTransactions = _ds.Tables[1];
              _report.CageDtCustomLiabilities = _ds.Tables[2];
              _report.CageDtSessionSummary = _ds.Tables[3];
              _report.CageDtSessionOtherSystemConcepts = _ds.Tables[4];
              _report.CageDtCounts = _ds.Tables[5];

                if (_ds.Tables[6].Rows.Count > 0)
                {
                  _report.StockIsStored = (Boolean)_ds.Tables[6].Rows[0][0];
                  OneSessionStatus = _report.StockIsStored;
                }
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);

        _report.CageDtStock = new DataTable();
        _report.CageDtTransactions = new DataTable();
        _report.CageDtCustomLiabilities = new DataTable();
        _report.CageDtSessionSummary = new DataTable();
        _report.CageDtCounts = new DataTable();
      }

      return _report;
    }


    public class TYPE_CAGE_GLOBAL_SESSION_REPORT
    {
      #region Member

      private Currency m_cage_deposits;
      private Currency m_cage_withdraws;
      private DataTable m_cage_stock;
      private DataTable m_cage_transactions;
      private DataTable m_cage_custom_liabilities;
      private DataTable m_cage_session_summary;
      private DataTable m_cage_session_other_system_concepts;
      private DataTable m_cage_counts;
      private Boolean m_stock_is_stored;

      #endregion //Member

      #region Properties

      public Currency CageDeposits
      {
        get { return m_cage_deposits; }
        set { m_cage_deposits = value; }
      }
      public Currency CageWithdraws
      {
        get { return m_cage_withdraws; }
        set { m_cage_withdraws = value; }
      }
      public DataTable CageDtStock
      {
        get { return m_cage_stock; }
        set { m_cage_stock = value; }
      }
      public DataTable CageDtTransactions
      {
        get { return m_cage_transactions; }
        set { m_cage_transactions = value; }
      }
      public DataTable CageDtCustomLiabilities
      {
        get { return m_cage_custom_liabilities; }
        set { m_cage_custom_liabilities = value; }
      }
      public DataTable CageDtSessionSummary
      {
        get { return m_cage_session_summary; }
        set { m_cage_session_summary = value; }
      }
      public DataTable CageDtSessionOtherSystemConcepts
      {
        get { return m_cage_session_other_system_concepts; }
        set { m_cage_session_other_system_concepts = value; }
      }
      public DataTable CageDtCounts
      {
        get { return m_cage_counts; }
        set { m_cage_counts = value; }
      }
      public Currency CageResult
      {
        get
        {
          return m_cage_deposits
            - m_cage_withdraws;
        }
      }
      public Boolean StockIsStored
      {
        get { return m_stock_is_stored; }
        set { m_stock_is_stored = value; }
      }

      #endregion //Properties

      public TYPE_CAGE_GLOBAL_SESSION_REPORT()
      {
        m_cage_deposits = 0;
        m_cage_withdraws = 0;
        m_cage_stock = new DataTable();
        m_cage_transactions = new DataTable();
        m_cage_custom_liabilities = new DataTable();
        m_cage_session_summary = new DataTable();
        m_cage_counts = new DataTable();
      }
    };

  }

}
