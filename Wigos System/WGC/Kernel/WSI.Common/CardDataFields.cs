//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AccountAddCredit.cs
// 
//   DESCRIPTION: Class to add credit to accounts
// 
//        AUTHOR: Marcos Piedra
// 
// CREATION DATE: 08-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-JUL-2014 MPO          Initial version
// 22-JUL-2014 MPO & JBC    Initial version
// 22-AUG-2014 JBC          Fixed Bug WIG-1199: Bad type into DOC_TYPE card data. Must be Int32
// 25-AUG-2014 JBC          Fixed Bug WIG-1208: AML second name problem
// 30-JAN-2015 SGB          Fixed Bug WIG-1961: If doc_type is empty show message.
// 13-AUG-2015 SGB          Backlog Item 3296: Add address country in account
// 15-SEP-2015 SGB          Added function return true if all values of address is visibles
// 23-MAY-2017 FAV          Bug 27543:Problemas de pagos manuales
// 03-OCT-2017 AMF          WIGOS-5478 Accounts - Change literals
// 13-FEB-2018 EOR          Bug 31515: WIGOS-8032 [Ticket #12229] Formulario de inscripci�n de jugadores en registro por l�mite de pago 
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace WSI.Common
{

  public enum ENUM_CARDDATA_FIELD
  {
    NONE,
    BIRTHDATE,
    WEDDING_DATE,
    MARITAL_STATUS,
    ID,
    ID1,
    ID2,
    DOC_TYPE,
    DOC_SCAN,
    GENDER,
    NAME3, //name
    NAME4, //middle name
    NAME1,
    NAME2,
    EMAIL1,
    EMAIL2,
    TWITTER,
    PHONE1,
    PHONE2,
    STREET,
    COLONY,
    DELEGATION,
    CITY,
    ZIP,
    PIN,
    COMMENTS,
    OCCUPATION,
    NATIONALITY,
    FEDERAL_ENTITY,
    ADDRESS_COUNTRY,
    BIRTH_COUNTRY,
    EXTERNAL_NUMBER,
    BENEFICIARY_ID1,
    BENEFICIARY_ID2,
    ISVIP, // VISIBLE
    DOCUMENT,
    //SJA 27-JAN-2016 - added PHOTO to list of possible fields to hide
    PHOTO,
    FACEBOOK,
    ALTERNATIVEADDRESS,
    EXPIRATION_DOC_DATE
  }

  public enum PLAYER_DATA_TO_CHECK
  {
    ACCOUNT,
    BENEFICIARY,
    PIN,
    ANTIMONEYLAUNDERING_ACCOUNT,
    ANTIMONEYLAUNDERING_BENEFICIARY,
  }

  public class RequiredData : List<DictionaryEntry>
  {
    public PLAYER_DATA_TO_CHECK _user_data_to_check;

    #region Initialitation

    public RequiredData(PLAYER_DATA_TO_CHECK UserDataToCheck)
    {
      this._user_data_to_check = UserDataToCheck;
    }

    public RequiredData()
    {
    }

    #endregion
    //END THIS CODE IS ONLY FOR COMPILE

    public void Add(ENUM_CARDDATA_FIELD Control, Object Value)
    {
      this.Add(new DictionaryEntry(Control, Value));
    }

    //------------------------------------------------------------------------------
    // PURPOSE : checks if there are empty required fields
    //
    //  PARAMS :
    //      - INPUT:
    //            - ForcedRequired: Ignore GeneralParams value and force check
    //
    //      - OUTPUT :
    //            - FocusControl: ENUM_CARDDATA_FIELD members that represent the empty field, NONE if everything OK. 
    //
    // RETURNS :
    //      - True if exists any empty required field, False otherwise
    //
    //   NOTES: The list of fields to check for required fields is retrieved from
    //          GENERAL_PARAMS (Subject Key = "Account.RequestedField").
    //          Must check new "Account.RequestedField" general params added
    public Boolean IsAnyEmpty(Boolean ForcedRequired, out ENUM_CARDDATA_FIELD FocusControl)
    {
      ENUM_CARDDATA_FIELD _control;
      ENUM_CARDDATA_FIELD _doc_control_to_focus;
      Boolean _required;
      String _group_key;
      String _subject_key;
      List<String> _ls_scan_type;
      Object _value;
      Int32 _int;
      Boolean _check_doc_scan;
      Boolean _check_docs;
      Boolean _document_required;
      Boolean _doc_scan_required;
      String _document_type_list;
      String _doc_scan_type_list;
      String _doc_scan_id_values;
      String _id_value;
      String _id_type;
      Dictionary<String, String> _dic_documents;
      Boolean _doc_required;
      Boolean _doc_type;

      FocusControl = ENUM_CARDDATA_FIELD.NONE;
      _required = false;
      _subject_key = String.Empty;
      _int = 0;
      _check_doc_scan = false;
      _check_docs = false;
      _document_type_list = String.Empty;
      _doc_scan_type_list = String.Empty;
      _ls_scan_type = new List<String>();
      _doc_scan_id_values = String.Empty;
      _id_value = String.Empty;
      _id_type = String.Empty;
      _dic_documents = new Dictionary<String, String>();
      _doc_type = false;

      _group_key = "Account.RequestedField";

      if ((this._user_data_to_check == PLAYER_DATA_TO_CHECK.BENEFICIARY) || (this._user_data_to_check == PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_BENEFICIARY))
      {
        _group_key = "Beneficiary.RequestedField";

        if (!WSI.Common.Misc.HasBeneficiaryAndNotAMLExternalControl())
        {

          return false;
        }
      }

      if (this._user_data_to_check == PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_ACCOUNT || this._user_data_to_check == PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_BENEFICIARY)
      {
        _subject_key = "AntiMoneyLaundering.";
      }

      try
      {
        foreach (DictionaryEntry _dic in this)
        {
          _control = (ENUM_CARDDATA_FIELD)_dic.Key;
          _value = _dic.Value;

          if (ForcedRequired)
          {
            _required = true;
          }
          else
          {
            switch (_control)
            {
              case ENUM_CARDDATA_FIELD.NAME1:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Name1", true);
                break;
              case ENUM_CARDDATA_FIELD.NAME2:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Name2", true);
                break;
              case ENUM_CARDDATA_FIELD.NAME3:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Name3", true);
                break;
              case ENUM_CARDDATA_FIELD.NAME4:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Name4", false);
                break;
              case ENUM_CARDDATA_FIELD.ID1:
                if (_value == null)
                {
                  _value = String.Empty;
                }
                if (!String.IsNullOrEmpty(_value.ToString()))
                {
                  _dic_documents.Add(String.Format("{0,3:000}", (Int32)ACCOUNT_HOLDER_ID_TYPE.RFC), _value.ToString());
                }
                _required = false;
                _check_docs = true;
                break;
              case ENUM_CARDDATA_FIELD.ID2:
                if (_value == null)
                {
                  _value = String.Empty;
                }

                if (!String.IsNullOrEmpty(_value.ToString()))
                {
                  _dic_documents.Add(String.Format("{0,3:000}", (Int32)ACCOUNT_HOLDER_ID_TYPE.CURP), _value.ToString());
                }
                _required = false;
                _check_docs = true;
                break;
              case ENUM_CARDDATA_FIELD.ID:        // Other doc
                if (_value == null)
                {
                  _value = String.Empty;
                }
                _value = _value.ToString();

                _id_value = _value.ToString();
                _required = false;
                _check_docs = true;
                break;
              case ENUM_CARDDATA_FIELD.DOC_TYPE:  // Other doc type
                // TODO: Use defined type to cast.
                if (_value == null || String.IsNullOrEmpty(_value.ToString()))
                {
                  _value = String.Empty;
                  _doc_type = true;
                  break;
                }

                if (Int32.TryParse(_value.ToString(), out _int))
                {
                  _value = String.Format("{0,3:000}", _int);
                }
                if (!String.IsNullOrEmpty(_value.ToString()))
                {
                  _id_type = _value.ToString();
                }
                _required = false;
                _check_docs = true;
                break;
              case ENUM_CARDDATA_FIELD.BIRTHDATE:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "BirthDate", true);
                break;
              case ENUM_CARDDATA_FIELD.GENDER:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Gender", true);
                break;
              case ENUM_CARDDATA_FIELD.OCCUPATION:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Occupation", true);
                break;
              case ENUM_CARDDATA_FIELD.EMAIL1:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Email1", false);
                break;
              case ENUM_CARDDATA_FIELD.EMAIL2:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Email2", false);
                break;
              case ENUM_CARDDATA_FIELD.PHONE1:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Phone1", false);
                break;
              case ENUM_CARDDATA_FIELD.PHONE2:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Phone2", false);
                break;
              case ENUM_CARDDATA_FIELD.BIRTH_COUNTRY:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "BirthCountry", true);
                break;
              case ENUM_CARDDATA_FIELD.NATIONALITY:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Nationality", true);
                break;
              case ENUM_CARDDATA_FIELD.STREET:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Address", true);
                break;
              case ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "ExtNum", true);
                break;
              case ENUM_CARDDATA_FIELD.COLONY:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Address02", true);
                break;
              case ENUM_CARDDATA_FIELD.DELEGATION:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Address03", true);
                break;
              case ENUM_CARDDATA_FIELD.CITY:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "City", true);
                break;
              case ENUM_CARDDATA_FIELD.ZIP:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "Zip", true);
                break;
              case ENUM_CARDDATA_FIELD.FEDERAL_ENTITY:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "FedEntity", true);
                break;
              case ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "AddressCountry", true);
                break;
              case ENUM_CARDDATA_FIELD.DOC_SCAN:
                if (_value == null)
                {
                  _value = String.Empty;
                }

                _check_doc_scan = true;
                _doc_scan_id_values = _value.ToString();
                _required = false;
                break;
              case ENUM_CARDDATA_FIELD.MARITAL_STATUS:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "MaritalStatus", true);
                break;
              case ENUM_CARDDATA_FIELD.WEDDING_DATE:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "WeddingDate", true);
                break;
              case ENUM_CARDDATA_FIELD.TWITTER:
                _required = GeneralParam.GetBoolean(_group_key, _subject_key + "TwitterAccount", true);
                break;

              default:
                _required = false;
                break;
            }
          }

          if (_required)
          {     //Values that can means empty fields
            FocusControl = _control;

            if (_value == null ||
               (_value.GetType() == typeof(DateTime) && (DateTime)_value == DateTime.MinValue) ||
               (_value.GetType() == typeof(Int32) && (Int32)_value < 0) ||
               (_value.GetType() == typeof(String) && String.IsNullOrEmpty(((String)_value).Trim())) ||
               (_value.GetType() == typeof(Boolean) && (Boolean)_value == false))
            {
              return true;
            }
          }
        }

        // Check documents lists
        if (_check_docs)
        {
          _doc_required = true;
          _doc_control_to_focus = ENUM_CARDDATA_FIELD.ID;

          _document_required = GeneralParam.GetBoolean(_group_key, _subject_key + "Document", true);
          _document_type_list = GeneralParam.GetString(_group_key, _subject_key + "DocumentTypeList");

          if (!String.IsNullOrEmpty(_id_type) && !String.IsNullOrEmpty(_id_value))
          {
            _dic_documents.Add(_id_type, _id_value);
          }

          if (!String.IsNullOrEmpty(_document_type_list))
          {
            foreach (String _key in _dic_documents.Keys)
            {
              if (_document_type_list.Contains(_key))
              {
                _doc_required = false;
              }
            }

            if (_document_type_list.Contains("001"))
            {
              _doc_control_to_focus = ENUM_CARDDATA_FIELD.ID1;
            }
            else if (_document_type_list.Contains("002"))
            {
              _doc_control_to_focus = ENUM_CARDDATA_FIELD.ID2;
            }
            else
            {
              if (this._user_data_to_check == PLAYER_DATA_TO_CHECK.BENEFICIARY)
              {
                _doc_control_to_focus = ENUM_CARDDATA_FIELD.ID1;
              }
              else
              {
                _doc_control_to_focus = ENUM_CARDDATA_FIELD.ID;
              }
            }
          }
          else if (_document_required && (_dic_documents.Keys.Count > 0))
          {
            _doc_required = false;
          }
          else if (!_document_required)
          {
            _doc_required = false;
          }
          else
          {
            if (this._user_data_to_check == PLAYER_DATA_TO_CHECK.BENEFICIARY)
            {
              _doc_control_to_focus = ENUM_CARDDATA_FIELD.ID1;
            }
            else
            {
              _doc_control_to_focus = ENUM_CARDDATA_FIELD.ID;
            }
          }

          if (_doc_type == true)
          {
            _doc_control_to_focus = ENUM_CARDDATA_FIELD.DOC_TYPE;
          }

          if (_doc_required)
          {
            FocusControl = _doc_control_to_focus;

            return true;
          }
        }

        if (_check_doc_scan)
        {
          _doc_required = true;
          _doc_control_to_focus = ENUM_CARDDATA_FIELD.DOC_SCAN;

          _doc_scan_required = GeneralParam.GetBoolean(_group_key, _subject_key + "DocScan", false);
          _doc_scan_type_list = GeneralParam.GetString(_group_key, _subject_key + "DocScanTypeList");

          if (!String.IsNullOrEmpty(_doc_scan_type_list))
          {
            if (string.IsNullOrEmpty(_doc_scan_id_values))
            {
              _doc_required = true;
            }
            else
            {
              foreach (String _val in _doc_scan_id_values.Split(','))
              {
                if (_doc_scan_type_list.Contains(_val))
                {
                  _doc_required = false;
                }
              }
            }
          }
          else if (_doc_scan_required && (_doc_scan_id_values.Length > 0))
          {
            _doc_required = false;
          }
          else if (!_doc_scan_required)
          {
            _doc_required = false;
          }

          if (_doc_required)
          {
            FocusControl = _doc_control_to_focus;

            return true;
          }
        }

        return false;
      }
      catch (Exception)
      {
        return true;
      }
    }

    public static Boolean IsDocumentTypeInList(ACCOUNT_HOLDER_ID_TYPE Type, String DocList)
    {
      String _value_type;

      if (String.IsNullOrEmpty(DocList))
      {
        return false;
      }

      _value_type = ((Int32)Type).ToString("000");
      foreach (String _val in DocList.Split(','))
      {
        if (_value_type == _val)
        {
          return true;
        }
      }

      return false;
    } // IsDocumentTypeInList

    public Boolean IsAnyEmpty(CardData CardInfo)
    {
      ENUM_CARDDATA_FIELD _field;

      return this.IsAnyEmpty(CardInfo, out _field);
    }

    public Boolean IsAnyEmpty(CardData CardInfo, out ENUM_CARDDATA_FIELD EmptyField)
    {
      switch (this._user_data_to_check)
      {
        case PLAYER_DATA_TO_CHECK.ACCOUNT:
        case PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_ACCOUNT:

          //DOCUMENT ID CONTROLS
          // DOCS. TYPES
          this.Add(ENUM_CARDDATA_FIELD.ID1, CardInfo.PlayerTracking.HolderId1);         // RFC
          this.Add(ENUM_CARDDATA_FIELD.ID2, CardInfo.PlayerTracking.HolderId2);         // CURP
          this.Add(ENUM_CARDDATA_FIELD.ID, CardInfo.PlayerTracking.HolderId);           // OTHER
          this.Add(ENUM_CARDDATA_FIELD.DOC_TYPE, (Int32)CardInfo.PlayerTracking.HolderIdType); // OTHER TYPE
          // SCAN
          this.Add(ENUM_CARDDATA_FIELD.DOC_SCAN, CardInfo.PlayerTracking.HolderId3Type);

          this.Add(ENUM_CARDDATA_FIELD.EMAIL1, CardInfo.PlayerTracking.HolderEmail01);
          this.Add(ENUM_CARDDATA_FIELD.BIRTH_COUNTRY, CardInfo.PlayerTracking.HolderBirthCountry);
          this.Add(ENUM_CARDDATA_FIELD.BIRTHDATE, CardInfo.PlayerTracking.HolderBirthDate);
          this.Add(ENUM_CARDDATA_FIELD.WEDDING_DATE, CardInfo.PlayerTracking.HolderWeddingDate);
          this.Add(ENUM_CARDDATA_FIELD.MARITAL_STATUS, CardInfo.PlayerTracking.HolderMaritalStatus);
          this.Add(ENUM_CARDDATA_FIELD.GENDER, CardInfo.PlayerTracking.HolderGender);
          this.Add(ENUM_CARDDATA_FIELD.NAME1, CardInfo.PlayerTracking.HolderName1);
          this.Add(ENUM_CARDDATA_FIELD.NAME2, CardInfo.PlayerTracking.HolderName2);
          this.Add(ENUM_CARDDATA_FIELD.NAME3, CardInfo.PlayerTracking.HolderName3);
          this.Add(ENUM_CARDDATA_FIELD.NAME4, CardInfo.PlayerTracking.HolderName4);
          this.Add(ENUM_CARDDATA_FIELD.NATIONALITY, CardInfo.PlayerTracking.HolderNationality);
          this.Add(ENUM_CARDDATA_FIELD.OCCUPATION, CardInfo.PlayerTracking.HolderOccupationId);
          this.Add(ENUM_CARDDATA_FIELD.PHONE1, CardInfo.PlayerTracking.HolderPhone01);
          this.Add(ENUM_CARDDATA_FIELD.PHONE2, CardInfo.PlayerTracking.HolderPhone02);
          this.Add(ENUM_CARDDATA_FIELD.EMAIL1, CardInfo.PlayerTracking.HolderEmail01);
          this.Add(ENUM_CARDDATA_FIELD.EMAIL2, CardInfo.PlayerTracking.HolderEmail02);
          this.Add(ENUM_CARDDATA_FIELD.TWITTER, CardInfo.PlayerTracking.HolderTwitter);
          this.Add(ENUM_CARDDATA_FIELD.STREET, CardInfo.PlayerTracking.HolderAddress01);
          this.Add(ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER, CardInfo.PlayerTracking.HolderExtNumber);
          this.Add(ENUM_CARDDATA_FIELD.COLONY, CardInfo.PlayerTracking.HolderAddress02);
          this.Add(ENUM_CARDDATA_FIELD.DELEGATION, CardInfo.PlayerTracking.HolderAddress03);
          this.Add(ENUM_CARDDATA_FIELD.CITY, CardInfo.PlayerTracking.HolderCity);
          this.Add(ENUM_CARDDATA_FIELD.ZIP, CardInfo.PlayerTracking.HolderZip);
          this.Add(ENUM_CARDDATA_FIELD.FEDERAL_ENTITY, CardInfo.PlayerTracking.HolderFedEntity);
          this.Add(ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY, CardInfo.PlayerTracking.HolderAddressCountry);

          break;

        case PLAYER_DATA_TO_CHECK.BENEFICIARY:
        case PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_BENEFICIARY:

          //DOCUMENT ID CONTROLS
          // DOCS. TYPES
          this.Add(ENUM_CARDDATA_FIELD.ID1, CardInfo.PlayerTracking.BeneficiaryId1); // RFC
          this.Add(ENUM_CARDDATA_FIELD.ID2, CardInfo.PlayerTracking.BeneficiaryId2); // CURP
          // SCAN
          this.Add(ENUM_CARDDATA_FIELD.DOC_SCAN, CardInfo.PlayerTracking.BeneficiaryId3Type);

          this.Add(ENUM_CARDDATA_FIELD.NAME1, CardInfo.PlayerTracking.BeneficiaryName1);
          this.Add(ENUM_CARDDATA_FIELD.NAME2, CardInfo.PlayerTracking.BeneficiaryName2);
          this.Add(ENUM_CARDDATA_FIELD.NAME3, CardInfo.PlayerTracking.BeneficiaryName3);
          this.Add(ENUM_CARDDATA_FIELD.GENDER, CardInfo.PlayerTracking.BeneficiaryGender);
          this.Add(ENUM_CARDDATA_FIELD.OCCUPATION, CardInfo.PlayerTracking.BeneficiaryOccupationId);
          this.Add(ENUM_CARDDATA_FIELD.BIRTHDATE, CardInfo.PlayerTracking.BeneficiaryBirthDate);

          break;
      }

      return this.IsAnyEmpty(false, out EmptyField);

    }
  }

  public class VisibleData
  {
    #region Members

    private List<DictionaryEntry> m_ctrl_ben = new List<DictionaryEntry>();

    private List<DictionaryEntry> m_ctrl_acc = new List<DictionaryEntry>();

    public List<DictionaryEntry> Controls { get { return m_ctrl_acc; } }

    #endregion

    #region Initialitation

    public VisibleData()
    {
    }

    #endregion

    public void AddDataAcc(ENUM_CARDDATA_FIELD Control, Control[] ControlsAffected)
    {
      m_ctrl_acc.Add(new DictionaryEntry(Control, ControlsAffected));
    }

    public void AddDataBen(ENUM_CARDDATA_FIELD Control, Control[] ControlsAffected)
    {
      m_ctrl_acc.Add(new DictionaryEntry(Control, ControlsAffected));
    }

    public static Boolean IsContactVisible()
    {

      return GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "Email1", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "Email2", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "TwitterAccount", true);
    }

    public static Boolean IsAddressVisible()
    {

      return GeneralParam.GetBoolean("Account.VisibleField", "Address", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "ExtNum", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "Address02", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "Address03", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "City", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "Zip", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "FedEntity", true) ||
             GeneralParam.GetBoolean("Account.VisibleField", "AddressCountry", true);
    }

    public static Boolean IsAllAddressVisible()
    {

      return GeneralParam.GetBoolean("Account.VisibleField", "Address", true) &&
             GeneralParam.GetBoolean("Account.VisibleField", "ExtNum", true) &&
             GeneralParam.GetBoolean("Account.VisibleField", "Address02", true) &&
             GeneralParam.GetBoolean("Account.VisibleField", "Address03", true) &&
             GeneralParam.GetBoolean("Account.VisibleField", "City", true) &&
             GeneralParam.GetBoolean("Account.VisibleField", "Zip", true) &&
             GeneralParam.GetBoolean("Account.VisibleField", "FedEntity", true) &&
             GeneralParam.GetBoolean("Account.VisibleField", "AddressCountry", true);
    }
    
    public Boolean HideControls()
    {
      try
      {
        HideControlsByType("Account.VisibleField", m_ctrl_acc);

        //HideControlsByType("Beneficiary.VisibleField", m_ctrl_ben);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    private void HideControlsByType(String GroupKey, List<DictionaryEntry> Ctrls)
    {
      Boolean _visible;
      ENUM_CARDDATA_FIELD _control;
      Control[] _ctrls_to_hide;

      foreach (DictionaryEntry _entry in Ctrls)
      {
        _control = (ENUM_CARDDATA_FIELD)_entry.Key;
        _ctrls_to_hide = (Control[])_entry.Value;
        _visible = true;

        switch (_control)
        {
          case ENUM_CARDDATA_FIELD.BIRTHDATE:
            _visible = GeneralParam.GetBoolean(GroupKey, "Birthdate", true);

            break;
          case ENUM_CARDDATA_FIELD.WEDDING_DATE:
            _visible = GeneralParam.GetBoolean(GroupKey, "WeddingDate", true);

            break;
          case ENUM_CARDDATA_FIELD.MARITAL_STATUS:
            _visible = GeneralParam.GetBoolean(GroupKey, "MaritalStatus", true);

            break;
          case ENUM_CARDDATA_FIELD.DOC_SCAN:
            _visible = GeneralParam.GetBoolean(GroupKey, "DocScan", true);

            break;
          case ENUM_CARDDATA_FIELD.GENDER:
            _visible = GeneralParam.GetBoolean(GroupKey, "Gender", true);

            break;
          case ENUM_CARDDATA_FIELD.NAME3:
            _visible = GeneralParam.GetBoolean(GroupKey, "Name3", true);

            break;
          case ENUM_CARDDATA_FIELD.NAME4:
            _visible = GeneralParam.GetBoolean(GroupKey, "Name4", true);

            break;
          case ENUM_CARDDATA_FIELD.NAME1:
            _visible = GeneralParam.GetBoolean(GroupKey, "Name1", true);

            break;
          case ENUM_CARDDATA_FIELD.NAME2:
            _visible = GeneralParam.GetBoolean(GroupKey, "Name2", true);

            break;
          case ENUM_CARDDATA_FIELD.EMAIL1:
            _visible = GeneralParam.GetBoolean(GroupKey, "Email1", true);

            break;
          case ENUM_CARDDATA_FIELD.EMAIL2:
            _visible = GeneralParam.GetBoolean(GroupKey, "Email2", true);

            break;
          case ENUM_CARDDATA_FIELD.TWITTER:
            _visible = GeneralParam.GetBoolean(GroupKey, "TwitterAccount", true);

            break;
          case ENUM_CARDDATA_FIELD.PHONE1:
            _visible = GeneralParam.GetBoolean(GroupKey, "Phone1", true);

            break;
          case ENUM_CARDDATA_FIELD.PHONE2:
            _visible = GeneralParam.GetBoolean(GroupKey, "Phone2", true);

            break;
          case ENUM_CARDDATA_FIELD.STREET:
            _visible = GeneralParam.GetBoolean(GroupKey, "Address", true);

            break;
          case ENUM_CARDDATA_FIELD.COLONY:
            _visible = GeneralParam.GetBoolean(GroupKey, "Address02", true);

            break;
          case ENUM_CARDDATA_FIELD.DELEGATION:
            _visible = GeneralParam.GetBoolean(GroupKey, "Address03", true);

            break;
          case ENUM_CARDDATA_FIELD.CITY:
            _visible = GeneralParam.GetBoolean(GroupKey, "City", true);

            break;
          case ENUM_CARDDATA_FIELD.ZIP:
            _visible = GeneralParam.GetBoolean(GroupKey, "Zip", true);

            break;
          case ENUM_CARDDATA_FIELD.OCCUPATION:
            _visible = GeneralParam.GetBoolean(GroupKey, "Occupation", true);

            break;
          case ENUM_CARDDATA_FIELD.NATIONALITY:
            _visible = GeneralParam.GetBoolean(GroupKey, "Nationality", true);

            break;
          case ENUM_CARDDATA_FIELD.FEDERAL_ENTITY:
            _visible = GeneralParam.GetBoolean(GroupKey, "FedEntity", true);

            break;
          case ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY:
            _visible = GeneralParam.GetBoolean(GroupKey, "AddressCountry", true);

            break;
          case ENUM_CARDDATA_FIELD.BIRTH_COUNTRY:
            _visible = GeneralParam.GetBoolean(GroupKey, "BirthCountry", true);

            break;
          case ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER:
            _visible = GeneralParam.GetBoolean(GroupKey, "ExtNum", true);

            break;
          case ENUM_CARDDATA_FIELD.ID1: //RFC
            _visible = IdentificationTypes.GetIdentificationTypes().ContainsKey((Int32)ACCOUNT_HOLDER_ID_TYPE.RFC);

            break;
          case ENUM_CARDDATA_FIELD.ID2: //CURP
            _visible = IdentificationTypes.GetIdentificationTypes().ContainsKey((Int32)ACCOUNT_HOLDER_ID_TYPE.CURP);

            break;
          case ENUM_CARDDATA_FIELD.DOCUMENT:
            _visible = GeneralParam.GetBoolean(GroupKey, "Document", true);

            break;
          //SJA 27-JAN-2016 - added PHOTO to list of possible fields to hide
          case ENUM_CARDDATA_FIELD.PHOTO:
            _visible = GeneralParam.GetBoolean(GroupKey, "Photo", true);

            break;

          case ENUM_CARDDATA_FIELD.EXPIRATION_DOC_DATE:
            _visible = GeneralParam.GetBoolean(GroupKey, "ExpirationDocumentDate", true);

            break;

          case ENUM_CARDDATA_FIELD.FACEBOOK:
            _visible = GeneralParam.GetBoolean(GroupKey, "Facebook", true);

            break;
          case ENUM_CARDDATA_FIELD.ALTERNATIVEADDRESS:
            _visible = GeneralParam.GetBoolean(GroupKey, "AlternativeAddress", true);

            break;

          default:
            break;
        }

        if (!_visible)
        {
          foreach (Control _ctrl in _ctrls_to_hide)
          {
            _ctrl.Visible = false;
            _ctrl.Enabled = false;
          }
        }

      }

    }

  }

  public class AccountFields
  {
    public static String GetStateName()
    {
      return GeneralParam.GetString("Account.Fields", "State.Name", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_FED_ENTITY"));
    }

    public static String GetAddressName()
    {
      return GeneralParam.GetString("Account.Fields", "Address.Name", Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_01"));
    }

    public static String GetColonyName()
    {
      return GeneralParam.GetString("Account.Fields", "Colony.Name", Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_02"));
    }

    public static String GetZipName()
    {
      return GeneralParam.GetString("Account.Fields", "Zip.Name", Resource.String("STR_UC_PLAYER_TRACK_ZIP"));
    }

    public static String GetNumExtName()
    {
      return GeneralParam.GetString("Account.Fields", "ExtNumber.Name", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXT_NUM"));
    }

    public static String GetDelegationName()
    {
      return GeneralParam.GetString("Account.Fields", "Delegation.Name", Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_03"));
    }

    public static String GetTownshipName()
    {
      return GeneralParam.GetString("Account.Fields", "Township.Name", Resource.String("STR_UC_PLAYER_TRACK_CITY"));
    }

    public static String GetName()
    {
      return GeneralParam.GetString("Account.Fields", "Name", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME"));
    }

    public static String GetMiddleName()
    {
      return GeneralParam.GetString("Account.Fields", "MiddleName", Resource.String("STR_FRM_PLAYER_EDIT_NAME4"));
    }

    public static String GetSurname1()
    {
      return GeneralParam.GetString("Account.Fields", "Surname1", Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1"));
    }

    public static String GetSurname2()
    {
      return GeneralParam.GetString("Account.Fields", "Surname2", Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2"));
    }

    public static String GetLiteralAccountFieldsbyDescription(String Description, String DefaultResource)
    {
      return GeneralParam.GetString("Account.Fields", Description, Resource.String(DefaultResource));
    }
  }
}
