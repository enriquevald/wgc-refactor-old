//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   PlayerTrackingMovements.cs
// DESCRIPTION:   
// AUTHOR:        Jos� Mart�nez L�pez
// CREATION DATE: 04-JUN-2014
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 04-JUN-2014  JML    Initial version.
// 14-JUL-2016  JML    Product Backlog Item 15065: Gaming tables (Fase 4): Player tracking. Multicurrency
// 14-JUL-2016  RAB    Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to MultiCurrency
// 04-ABR-2017  JML    PBI 26401:MES10 Ticket validation - Ticket status change to cancelled
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{

  public enum PlayerTrackingMovementType
  {
    PlayerIn = 1,         // Player is sitdown to table
    PlayerOut = 2,        // Player finish play in this game/table    

    //TotalPlayed = 11,
    //TotalWon = 12,
    //AveragePlayed = 13,
    //AverageWon = 14,
    //MaxPlayed = 15,
    //MinPlayed = 16,
    PointsWon = 17,
    Speed = 18,
    Skill = 19,
    SwapSeat = 20,
    PausePlay = 21,
    ContinuePlay = 22,
    //BuyChips = 23,
    SaleChips = 24,
    ChipsIn = 25,
    ChipsOut = 26,
    CurrentPlayed = 27,
    DealerCopyValidate = 28,
    
    // Table Movements. Add JML 23-SEP-2014
    PauseTablePlay = 101,
    ContinueTablePlay = 102,
    TableSpeed = 103
  }

  public class PlayerTrackingMovements
  {

    #region Attributes


    private DataTable m_table = null;
    private StringBuilder m_sql_insert;

    private Int32 m_user_id;
    private Int64 m_cashier_id;
    private Int64 m_gaming_table_id;
    private Int64? m_gaming_table_session_id;

    #endregion

    #region Constructor

    public PlayerTrackingMovements(Int32 UserId, Int64 CashierId, Int64 GamingTableId, Int64? GamingTableSessionId)
    {
      this.m_user_id = UserId;
      this.m_cashier_id = CashierId; // Cashier TerminalId 
      this.m_gaming_table_id = GamingTableId;
      this.m_gaming_table_session_id = GamingTableSessionId;
      InitTable();
    }

    #endregion

    #region Private Methods

    private void InitTable()
    {
      // Private table
      m_table = new DataTable();
      // m_table.Columns.Add("GTPM_MOVEMENT_ID", Type.GetType("System.Int64")).AllowDBNull = false;
      m_table.Columns.Add("GTPM_TYPE", Type.GetType("System.Int32")).AllowDBNull = false;

      //m_table.Columns.Add("GTPM_DATETIME", Type.GetType("System.Datetime")).AllowDBNull = true;
      // m_table.Columns.Add("GTPM_USER_ID", Type.GetType("System.Int32")).AllowDBNull = false;
      // m_table.Columns.Add("GTPM_CASHIER_ID", Type.GetType("System.Int64")).AllowDBNull = false;
      // m_table.Columns.Add("GTPM_GAMING_TABLE_ID", Type.GetType("System.Int64")).AllowDBNull = false;
      m_table.Columns.Add("GTPM_SEAT_ID", Type.GetType("System.Int64")).AllowDBNull = true;
      m_table.Columns.Add("GTPM_PLAY_SESSION_ID", Type.GetType("System.Int64")).AllowDBNull = true;

      m_table.Columns.Add("GTPM_TERMINAL_ID", Type.GetType("System.Int64")).AllowDBNull = true;
      m_table.Columns.Add("GTPM_ACCOUNT_ID", Type.GetType("System.Int64")).AllowDBNull = true;

      m_table.Columns.Add("GTPM_OLD_VALUE", Type.GetType("System.Decimal")).AllowDBNull = true;
      m_table.Columns.Add("GTPM_VALUE", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("GTPM_ISO_CODE", Type.GetType("System.String")).AllowDBNull = true;

      m_sql_insert = new StringBuilder();

      m_sql_insert.AppendLine("INSERT INTO   GT_PLAYERTRACKING_MOVEMENTS ");
      m_sql_insert.AppendLine("            ( GTPM_TYPE ");
      m_sql_insert.AppendLine("            , GTPM_DATETIME ");
      m_sql_insert.AppendLine("            , GTPM_USER_ID ");
      m_sql_insert.AppendLine("            , GTPM_CASHIER_ID ");
      m_sql_insert.AppendLine("            , GTPM_GAMING_TABLE_SESSION_ID ");
      m_sql_insert.AppendLine("            , GTPM_GAMING_TABLE_ID ");
      m_sql_insert.AppendLine("            , GTPM_PLAY_SESSION_ID ");
      m_sql_insert.AppendLine("            , GTPM_SEAT_ID ");
      m_sql_insert.AppendLine("            , GTPM_TERMINAL_ID ");
      m_sql_insert.AppendLine("            , GTPM_ACCOUNT_ID ");
      m_sql_insert.AppendLine("            , GTPM_OLD_VALUE ");
      m_sql_insert.AppendLine("            , GTPM_VALUE ");
      m_sql_insert.AppendLine("            , GTPM_ISO_CODE) ");
      m_sql_insert.AppendLine("       VALUES ");
      m_sql_insert.AppendLine("            ( @pType ");
      m_sql_insert.AppendLine("            , @pDate ");
      m_sql_insert.AppendLine("            , @pUserId ");
      m_sql_insert.AppendLine("            , @pCashierId ");
      m_sql_insert.AppendLine("            , @pGamingTableSessionId ");
      m_sql_insert.AppendLine("            , @pGamingTableId ");
      m_sql_insert.AppendLine("            , @pPlaySessionId ");
      m_sql_insert.AppendLine("            , @pSeatId ");
      m_sql_insert.AppendLine("            , @pTerminalId ");
      m_sql_insert.AppendLine("            , @pAccountId ");
      m_sql_insert.AppendLine("            , @pOldValue ");
      m_sql_insert.AppendLine("            , @pValue ");
      m_sql_insert.AppendLine("            , @pIsoCode ) ");

    } // InitTable

    #endregion

    #region Public Methods

    // Table movements
    public Boolean Add(PlayerTrackingMovementType Type, Decimal OldValue, Decimal Value, String CurrencyIsoCode)
    {
      // Null values only valid for table movements
      if (!Add(Type, null, null, null, null, OldValue, Value, CurrencyIsoCode, null, null))
      {
        return false;
      }
      return true;
    }
 
    // Seat movements 
    // Null values only valid for table movements
    public Boolean Add(PlayerTrackingMovementType Type,
                       Int64? SeatId, Int64? PlaySessionId, Int32? TerminalId, Int64? AccountId,
                       Decimal OldValue, Decimal Value,
                       String CurrencyIsoCode, Decimal? CurrencyDenomination, Int64? OperationId)
    {
      DataRow _row;

      try
      {
        _row = m_table.NewRow();

        _row["GTPM_TYPE"] = (Int32)Type;
        if (SeatId.HasValue)
        {
          _row["GTPM_SEAT_ID"] = SeatId;
        }
        if (PlaySessionId.HasValue)
        {
          _row["GTPM_PLAY_SESSION_ID"] = PlaySessionId;
        }
        if (TerminalId.HasValue)
        {
          _row["GTPM_TERMINAL_ID"] = TerminalId;
        }
        if (AccountId.HasValue)
        {
          _row["GTPM_ACCOUNT_ID"] = AccountId;
        }

        if (CurrencyIsoCode.Length > 0)
        {
          _row["GTPM_ISO_CODE"] = CurrencyIsoCode;
        }

        _row["GTPM_OLD_VALUE"] = OldValue;
        _row["GTPM_VALUE"] = Value;

        m_table.Rows.Add(_row);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Add

    public Boolean Save(SqlTransaction SqlTrx)
    {
      StringBuilder _sql_str;

      _sql_str = new StringBuilder();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(m_sql_insert.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "GTPM_TYPE";

          _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = m_user_id;
          _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = m_cashier_id;
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = m_gaming_table_session_id;
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.BigInt).Value = m_gaming_table_id;
          _cmd.Parameters.Add("@pSeatId", SqlDbType.BigInt).SourceColumn = "GTPM_SEAT_ID";
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).SourceColumn = "GTPM_PLAY_SESSION_ID";
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "GTPM_TERMINAL_ID";
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "GTPM_ACCOUNT_ID";

          _cmd.Parameters.Add("@pOldValue", SqlDbType.Decimal).SourceColumn = "GTPM_OLD_VALUE";
          _cmd.Parameters.Add("@pValue", SqlDbType.Decimal).SourceColumn = "GTPM_VALUE";
          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).SourceColumn = "GTPM_ISO_CODE";

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            _da.InsertCommand = _cmd;
            _da.UpdateBatchSize = 500;
            _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

            if (_da.Update(m_table) != m_table.Rows.Count)
            {
              return false;
            }
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Save

    public static string GetMovementDesc(PlayerTrackingMovementType Type, String IsoCode)
    {
      String _Description;
      _Description = "";

      switch (Type)
      {
        case PlayerTrackingMovementType.PlayerIn:
          _Description = Resource.String("STR_PLAYER_TRACKING_TYPE_PLAYER_IN");
          break;

        case PlayerTrackingMovementType.PlayerOut:
          _Description = Resource.String("STR_PLAYER_TRACKING_TYPE_PLAYER_OUT");
          break;

        case PlayerTrackingMovementType.PointsWon:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_POINTS_WON");
          break;

        case PlayerTrackingMovementType.Speed:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_SPEED");
          break;

        case PlayerTrackingMovementType.Skill:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_SKILL");
          break;

        case PlayerTrackingMovementType.SwapSeat:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_SWAP_SEAT");
          break;

        case PlayerTrackingMovementType.PausePlay:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_PAUSE_PLAY");
          break;

        case PlayerTrackingMovementType.ContinuePlay:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_CONTINUE_PLAY");
          break;

        case PlayerTrackingMovementType.CurrentPlayed:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_CURRENT_PLAYED");
          break;

        case PlayerTrackingMovementType.ChipsIn:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_CHIPS_IN");
          break;

        case PlayerTrackingMovementType.ChipsOut:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_CHIPS_OUT");
          break;

        case PlayerTrackingMovementType.SaleChips:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_CHIPS_SALE");
          break;

        case PlayerTrackingMovementType.DealerCopyValidate:
          _Description = Resource.String("STR_PLAYER_TRACKING_SUBTYPE_DEALER_COPY_VALIDATE");
          break;

        case PlayerTrackingMovementType.ContinueTablePlay:
          _Description = Resource.String("STR_PLAYER_TRACKING_TABLE_CONTINUE_PLAY");
          break;
        case PlayerTrackingMovementType.PauseTablePlay :
          _Description = Resource.String("STR_PLAYER_TRACKING_TABLE_PAUSE_PLAY");
          break;
        case PlayerTrackingMovementType.TableSpeed :
          _Description = Resource.String("STR_PLAYER_TRACKING_TABLE_CHANGE_SPEED");
          break;
      }

      if(IsoCode != String.Empty)
      {
        _Description = _Description + " (" + IsoCode + ")";
      }

      return _Description;

    }  // GetMovementDesc


    public static string GetSpeedDesc(GTPlayerTrackingSpeed OldSpeed, GTPlayerTrackingSpeed NewSpeed)
    {
      String _Description;

      _Description = " (";

      switch (OldSpeed)
      {
        case GTPlayerTrackingSpeed.Slow:
          _Description += Resource.String("STR_FRM_PLAYER_TRACKING_CB_SLOW");
          break;
        case GTPlayerTrackingSpeed.Medium:
          _Description += Resource.String("STR_FRM_PLAYER_TRACKING_CB_MEDIUM");
          break;
        case GTPlayerTrackingSpeed.Fast:
          _Description += Resource.String("STR_FRM_PLAYER_TRACKING_CB_FAST");
          break;
      }

      _Description += " -> ";

      switch (NewSpeed)
      {
        case GTPlayerTrackingSpeed.Slow:
          _Description += Resource.String("STR_FRM_PLAYER_TRACKING_CB_SLOW");
          break;
        case GTPlayerTrackingSpeed.Medium:
          _Description += Resource.String("STR_FRM_PLAYER_TRACKING_CB_MEDIUM");
          break;
        case GTPlayerTrackingSpeed.Fast:
          _Description += Resource.String("STR_FRM_PLAYER_TRACKING_CB_FAST");
          break;
      }

      _Description += ")";

      return _Description;
    }

    #endregion

  } // PlayerTrackingMovements

} // namespace WSI.Common
