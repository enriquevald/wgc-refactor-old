//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ITitoPrinter.cs
// 
//   DESCRIPTION: ITitoPrinter class
// 
//        AUTHOR: Daniel Dom�nguez Mart�n 
// 
// CREATION DATE: 14-Nov-2013
// 
// REVISION HISTORY:
// 
// Date        Author      Description
// ----------- --------    ----------------------------------------------------------
// 14-Nov-2013 AJQ & DDM    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{

  //public enum TicketType
  //{
  //  Unknown = 0,
  //  EGM_Cashout = 1,
  //  EGM_PlayableOnly = 2,
  //  EGM_Debit = 3,

  //  Cashable = 11,
  //  PromotionalCashable = 12,
  //  PromotionalPlayableOnly = 13,
  //}

  public enum PrinterStatus
  {
    Unknown = 0,
    Ok = 1,
    Error = 2,
    PaperLow = 3,
    OutOfPaper = 4,
    TicketInPath = 5,
    NotOpenPort = 6,
    TicketInPathAndPaperLow = 7
  }

  public class TitoTicketDefault
  {
    public String Location;
    public String Address1;
    public String Address2;
    public String Machine;

    public TitoTicketDefault(String Machine)
    {
      this.Location = GeneralParam.GetString("TITO", "Tickets.Location", "");
      this.Address1 = GeneralParam.GetString("TITO", "Tickets.Address1", "");
      this.Address2 = GeneralParam.GetString("TITO", "Tickets.Address2", "");
      this.Machine = Machine;
    }
  }

  public class TicketInfo
  {
    public TITO_TICKET_TYPE TicketType;
    public DateTime Date;
    public DateTime Expiration;
    public Decimal Amount;

    public String LblLocation;
    public String LblAddress1;
    public String LblAddress2;
    public String LblMachine;

    public String LblSequence;
    public String LblTitle;
    public String LblValidationNumber; // 12-3456-7890-1234-5678
    public String LblBarcodeNumber;    // 123456789012345678
    public String LblAmount;
    public String LblAmountLetter;
    public String LblDate;
    public String LblTime;
    public String LblExpiration;

    public String StrLocation;
    public String StrAddress1;
    public String StrAddress2;
    public String StrMachine;

    public String StrSequence;
    public String StrTitle;
    public String StrValidationNumber; // 12-3456-7890-1234-5678
    public String StrBarcodeNumber;    // 123456789012345678
    public String StrAmount;
    public String StrAmountLetter;
    public String StrDate;
    public String StrTime;
    public String StrExpiration;

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor TicketInfo. Init Address1,Address2, Location..
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TicketInfo()
    {

    } // TicketInfo

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor TicketInfo. Init.
    //
    //  PARAMS:
    //      - INPUT:
    //          - TicketDefault: The class contains Address1, Address2, Location and Machine.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TicketInfo(TitoTicketDefault TicketDefault)
    {
      this.StrLocation = TicketDefault.Location;
      this.StrAddress1 = TicketDefault.Address1;
      this.StrAddress2 = TicketDefault.Address2;
      this.StrMachine = TicketDefault.Machine;
    } // TicketInfo

    //------------------------------------------------------------------------------
    // PURPOSE: Get default ticket name
    // 
    //  PARAMS:
    //      - INPUT:
    //          - None
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      String with ticket name
    // 
    //   NOTES:
    //    
    public String GetNameTicketType()
    {
      switch (TicketType)
      {
        case TITO_TICKET_TYPE.CASHABLE:
          return GeneralParam.GetString("TITO", "CashableTickets.Title");//"CASHOUT TICKET";
        case TITO_TICKET_TYPE.PROMO_REDEEM:
          return GeneralParam.GetString("TITO", "PromotionalTickets.Redeemable.Title");//"CASHABLE PROMO";
        case TITO_TICKET_TYPE.PROMO_NONREDEEM:
          return GeneralParam.GetString("TITO", "PromotionalTickets.NonRedeemable.Title");//"PLAYABLE ONLY";
        case TITO_TICKET_TYPE.HANDPAY:
        case TITO_TICKET_TYPE.JACKPOT:
        default:
          return "";
      }

    } // GetNameTicketType
  }

  public interface ITitoPrinter
  {
    Int32 PrintTime
    {
      get;
      set;
    }

    Boolean Init();
    Boolean PrintTicket(TicketInfo Ticket);
    
    void GetStatus(out Boolean IsReady, out PrinterStatus Status, out String PrinterErrorText);
  }

  public class TitoPrinter_Null : ITitoPrinter
  {
    #region ITitoPrinter Members

    private Int32 m_print_time;
    public int PrintTime
    {
      get
      {
        return m_print_time;
      }
      set
      {
        m_print_time = value;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Int TitoPrinter_Null
    // 
    //  PARAMS:
    //      - INPUT:
    //          - None
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //      True if init correctly
    //      False otherwise
    // 
    //   NOTES:
    //    
    public bool Init()
    {
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Print ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //          - PrintTicket ticket
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      True if ticket printed
    //      False otherwise
    // 
    //   NOTES:
    //    
    public bool PrintTicket(TicketInfo Ticket)
    {
      return true;
    }  // PrintTicket

    //------------------------------------------------------------------------------
    // PURPOSE: Get status
    // 
    //  PARAMS:
    //      - INPUT:
    //          - NONE
    //         
    //      - OUTPUT:
    //          - IsReady
    //          - Status
    //          - PrinterErrorText
    //
    // RETURNS:
    //          - NONE
    // 
    //   NOTES:
    // 
    public void GetStatus(out Boolean IsReady, out PrinterStatus Status, out String PrinterErrorText)
    {
      PrinterErrorText = "";
      IsReady = true;
      Status = PrinterStatus.Ok;
    } // Status

    #endregion

  }

 }

