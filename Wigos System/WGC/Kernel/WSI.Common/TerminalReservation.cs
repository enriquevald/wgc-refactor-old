//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalReservation.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Toni T�llez
// 
// CREATION DATE: XX-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author   Description
// ----------- ------   ------------------------------------------------
// 03-AUG-2017 ATB      Initial version. PBI 28982: EGM Reserve - Phase 1 - Set free on GUI (WIGOS-4000)
// 10-AUG-2017 ATB      PBI 28710: EGM Reserve � Settings (WIGOS-4297)
// 24-AUG-2017 ATB      PBI 28710: EGM Reserve � Settings (WIGOS-4671)
// 25-AUG-2017 ATB      PBI 28710: EGM Reserve � Settings (WIGOS-4697)
// 28-SEP-2017 FJC      WIGOS-5397 EGM Reserve: minimum coin in does not work in LCD
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using WSI.Common.ReservedTerminal;
using WSI.Common.Utilities;
using System.Xml;

namespace WSI.Common
{
  public class TerminalReservation
  {
    #region Enumerated

    public enum ReserveTerminalResult
    {
      Ok = 0,
      UnknownError = 1,
      AlreadyReserved = 2,
      NoTrackDataAccount = 3,
      AccountHasAlreadyAReservation = 4,
      AccountHasNoSessionOnTerminal = 5,
      TerminalOnAnothersAccountSession = 6,
      TerminalNotReservable = 7,
      AccountCantReserve = 8,
      MinimumCoinInNotReached = 9,
      ReservationNotEnabled = 999,
    };

    public enum TerminalReservationStatus
    {
      NotReserved = 0,
      ReservedByAccount = 1,
      ReservedByAnotherAccount = 2,
      AccountHasAnotherTerminalReserved = 3,

      UndefinedError = 9999,
    };

    #endregion

    #region Constants

    private const int DEFAULT_MINUTES_RESERVED = 10;

    #endregion

    #region SubClasses

    /// <summary>
    /// Used to package the terminals with the minimum info
    /// </summary>
    public class ReservedTerminal
    {
      public Int32 TerminalID { get; set; }
      public Int64 AccountID { get; set; }
      public String TerminalName { get; set; }
      public Int32 Level { get; set; }
    }

    /// <summary>
    /// Used to get the terminals that can be reserved from the database
    /// </summary>
    public class ProviderList
    {
      public Int32[] Terminals { get; set; }
    }

    /// <summary>
    /// Used to return a complete response to many actions
    /// </summary>
    public class ReservedTerminalResponse
    {
      public Int32 Code { get; set; }
      public String Message { get; set; }
      public Boolean Success { get; set; }
      public Boolean Result { get; set; }
      public String MessageExtended { get; set; }
      public String XmlProviderList { get; set; }

    }

    public class ReserveTerminalResultData
    {
      public ReserveTerminalResult ErrorCode { get; set; }
      public String MinimumCoinIn { get; set; }
      public String CurrentCoinIn { get; set; }

    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Reserves a given terminal
    /// </summary>
    public static Boolean Reserve(Int32 TerminalId, Int64 AccountId, Int32 AccountLevel, out ReserveTerminalResultData ErrorData, out Int32 ExpirationMinutes, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_rows;
      Decimal _coin_in;
      Decimal _played_coin_in;
      Int32 _terminal_sas_flags;
      ReservedTerminal request;
      ReservedTerminalResponse _response;

      ErrorData = new ReserveTerminalResultData()
      {
        ErrorCode = ReserveTerminalResult.UnknownError
      };


      ExpirationMinutes = 0;

      try
      {
        if (!Misc.IsTerminalReservationEnabled())
        {
          ErrorData = new ReserveTerminalResultData()
          {
            ErrorCode = ReserveTerminalResult.ReservationNotEnabled
          };

          return true;
        }

        // Check if terminal is reservable
        request = new ReservedTerminal()
        {
          TerminalID = TerminalId,
          AccountID = AccountId,
          Level = AccountLevel,
        };
        _response = TerminalCanBeReserved(request, SqlTrx);

        if (!_response.Success || !_response.Result)
        {
          ErrorData = new ReserveTerminalResultData()
          {
            ErrorCode = ReserveTerminalResult.TerminalNotReservable
          };
          return true;
        }
        #region Checking

        // Check the terminal is not in another's account session
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   COUNT(*)                                   ");
        _sb.AppendLine("  FROM   PLAY_SESSIONS                              ");
        _sb.AppendLine(" WHERE   PS_STATUS      =  @pPlaySessionStatusOpened");
        _sb.AppendLine("   AND   PS_TERMINAL_ID =  @pTerminalId             ");
        _sb.AppendLine("   AND   PS_ACCOUNT_ID  != @pAccountID              ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pPlaySessionStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

          _num_rows = (Int32)_sql_cmd.ExecuteScalar();

          if (_num_rows != 0)
          {
            ErrorData = new ReserveTerminalResultData()
            {
              ErrorCode = ReserveTerminalResult.TerminalOnAnothersAccountSession
            };

            return true;
          }
        }

        // On Cashless mode, check the account is on session on this terminal
        if (!WSI.Common.TITO.Utils.IsTitoMode())
        {
          _sb = new StringBuilder();

          _sb.AppendLine("SELECT   COUNT(*)                                  ");
          _sb.AppendLine("  FROM   PLAY_SESSIONS                             ");
          _sb.AppendLine(" WHERE   PS_STATUS      = @pPlaySessionStatusOpened");
          _sb.AppendLine("   AND   PS_TERMINAL_ID = @pTerminalId             ");
          _sb.AppendLine("   AND   PS_ACCOUNT_ID  = @pAccountID              ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pPlaySessionStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;
            _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

            _num_rows = (Int32)_sql_cmd.ExecuteScalar();

            if (_num_rows == 0)
            {
              ErrorData = new ReserveTerminalResultData()
              {
                ErrorCode = ReserveTerminalResult.AccountHasNoSessionOnTerminal
              };

              return true;
            }

            if (_num_rows > 1)
            {
              Log.Warning(String.Format("Error reserving Terminal [TerminalID: {0} - AccountId: {1}]:  The account has several open play sessions on the terminal!", TerminalId, AccountId));

              return false;
            }
          }
        }

        // Ensure the terminal is not previously reserved
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   TE_SAS_FLAGS                  ");
        _sb.AppendLine("     FROM   TERMINALS                     ");
        _sb.AppendLine("    WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            _terminal_sas_flags = _reader.GetInt32(_reader.GetOrdinal("TE_SAS_FLAGS"));

            _reader.Close();
          }
        }


        if (WSI.Common.Terminal.SASFlagActived(_terminal_sas_flags, SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL))
        {
          ErrorData = new ReserveTerminalResultData()
          {
            ErrorCode = ReserveTerminalResult.AlreadyReserved
          };

          return true;
        }

        //Check the account has no terminal previously reserved
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   COUNT(*)                      ");
        _sb.AppendLine("  FROM   ACCOUNTS                      ");
        _sb.AppendLine(" WHERE   AC_EGM_RESERVED = @pTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

          _num_rows = (Int32)_sql_cmd.ExecuteScalar();

          if (_num_rows > 0)
          {
            ErrorData = new ReserveTerminalResultData()
            {
              ErrorCode = ReserveTerminalResult.AccountHasAlreadyAReservation
            };

            return true;
          }
        }

        // Get the reserve expiration time
        ExpirationMinutes = DEFAULT_MINUTES_RESERVED;
        _coin_in = 0;

        _sb = new StringBuilder();
        _sb.Append("SELECT RTC_MINUTES, RTC_COIN_IN            ");
        _sb.Append("FROM ACCOUNTS                              ");
        _sb.Append("INNER JOIN RESERVED_TERMINAL_CONFIGURATION ");
        _sb.Append("ON AC_HOLDER_LEVEL = RTC_HOLDER_LEVEL      ");
        _sb.Append("WHERE AC_ACCOUNT_ID = @pResultIn           ");
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.CommandType = CommandType.Text;
          _sql_cmd.Parameters.Add("@pResultIn", SqlDbType.BigInt).Value = AccountId;
          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read() && !_reader.IsDBNull(0))
            {
              ExpirationMinutes = _reader.GetInt32(0);
              _coin_in = _reader.GetDecimal(1);

              _reader.Close();
            }
            else
            {
              ErrorData = new ReserveTerminalResultData()
              {
                ErrorCode = ReserveTerminalResult.AccountCantReserve
              };
              return false;
            }
          }
        }

        // Get the played amount
        _played_coin_in = 0;
        _sb = new StringBuilder();

        _sb.AppendLine("  SELECT   TOP 1 PS_PLAYED_AMOUNT                        ");
        _sb.AppendLine("    FROM   PLAY_SESSIONS                                 ");
        _sb.AppendLine("   WHERE   PS_STATUS      =  @pPlaySessionStatusOpened   ");
        _sb.AppendLine("     AND   PS_TERMINAL_ID =  @pTerminalId                ");
        _sb.AppendLine("     AND   PS_ACCOUNT_ID  =  @pAccountID                 ");
        _sb.AppendLine("ORDER BY   PS_PLAY_SESSION_ID DESC                       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pPlaySessionStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

          Object _obj;
          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null)
          {
            _played_coin_in = (Decimal)_sql_cmd.ExecuteScalar();
          }

        }

        // Check if the played session is over the coin-in
        if (_played_coin_in < _coin_in)
        {
          ErrorData = new ReserveTerminalResultData()
          {
            ErrorCode = ReserveTerminalResult.MinimumCoinInNotReached,
            CurrentCoinIn = Currency.Format(_played_coin_in, CurrencyExchange.GetNationalCurrency()).ToString(),
            MinimumCoinIn = Currency.Format(_coin_in, CurrencyExchange.GetNationalCurrency()).ToString()
          };

          return true;
        }

        #endregion

        #region Reservation

        // Set the reserved terminal on accounts
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   ACCOUNTS                      ");
        _sb.AppendLine("   SET   AC_EGM_RESERVED = @pTerminalId");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID   = @pAccountID ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        // Add reservation flag to the terminal & set the reservation account
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   TERMINALS                                ");
        _sb.AppendLine("   SET   TE_SAS_FLAGS |=  @pReserveTerminalFlag   ");
        _sb.AppendLine("       , TE_RESERVE_ACCOUNT_ID = @pAccountID      ");
        _sb.AppendLine("       , TE_RESERVATION_EXPIRES = @pExpirationDate");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pReserveTerminalFlag", SqlDbType.Int, 4).Value = SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL;
          _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pExpirationDate", SqlDbType.DateTime).Value = WGDB.Now.AddMinutes(ExpirationMinutes);
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        // Insert the terminal's reservation into the reservation transaction table
        _sb = new StringBuilder();
        _sb.Append("INSERT INTO RESERVED_TERMINAL_TRANSACTION(   ");
        _sb.Append("      RTT_ACCOUNT_ID                         ");
        _sb.Append("     ,RTT_TERMINAL_ID                        ");
        _sb.Append("     ,RTT_START_RESERVED                     ");
        _sb.Append("     ,RTT_START_UTC                          ");
        _sb.Append("     ,RTT_MAX_MINUTES                        ");
        _sb.Append("     ,RTT_STATUS)                            ");
        _sb.Append("  VALUES(                                    ");
        _sb.Append("      @pAccountID                            ");
        _sb.Append("     ,@pTerminalID                           ");
        _sb.Append("     ,@pStartReserved                        ");
        _sb.Append("     ,@pStartUtc                             ");
        _sb.Append("     ,@pMaxMinutes                           ");
        _sb.Append("     ,@pStatus)                              ");

        try
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _cmd.CommandType = CommandType.Text;
            _cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;
            _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
            _cmd.Parameters.Add("@pStartReserved", SqlDbType.DateTime).Value = WGDB.Now;
            _cmd.Parameters.Add("@pStartUtc", SqlDbType.DateTime).Value = WGDB.Now;
            _cmd.Parameters.Add("@pMaxMinutes", SqlDbType.Int).Value = ExpirationMinutes;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)ReservedStatusType.Reserved;
            _cmd.ExecuteNonQuery();
            _response = new ReservedTerminalResponse()
            {
              Code = 1,
              Message = Resource.String("STR_RESERVATION_MSG_INSERT_IN_RESERVED_TERMINAL_CONFIG_OK"),
              Success = true,
              Result = true
            };
          }
        }
        catch (Exception ex)
        {
          _response = new ReservedTerminalResponse()
          {
            Code = -1,
            Message = ex.Message,
            Success = false
          };
        }

        #endregion

        ErrorData = new ReserveTerminalResultData()
        {
          ErrorCode = ReserveTerminalResult.Ok
        };
      }

      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reserving Terminal . TerminalID: " + TerminalId);

        return false;
      }

      return true;
    }

    /// <summary>
    /// Release a terminal, use in SAS only, and if you want to release a terminal without no extra info
    /// </summary>
    public static Boolean Remove(Int32 TerminalId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DateTime _start_time;
      _start_time = new DateTime();

      try
      {
        // Apply the release at accounts table
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   ACCOUNTS                         ");
        _sb.AppendLine("   SET   AC_EGM_RESERVED = NULL           ");
        _sb.AppendLine(" WHERE   AC_EGM_RESERVED = @pTerminalId   ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;
          _sql_cmd.ExecuteNonQuery();
        }

        // Apply the release at terminals table
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   TERMINALS                                                  ");
        _sb.AppendLine("   SET   TE_SAS_FLAGS &= ~(convert (int, @pReserveTerminalFlag))    ");
        _sb.AppendLine("       , TE_RESERVE_ACCOUNT_ID  = NULL                              ");
        _sb.AppendLine("       , TE_RESERVATION_EXPIRES = NULL                              ");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId                              ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pReserveTerminalFlag", SqlDbType.Int, 4).Value = SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error on CleanReserve. TerminalID: " + TerminalId);

        return false;
      }

      // Getting the start date for transaction
      _sb = new StringBuilder();
      _sb.Append("SELECT         RTT_START_RESERVED                      ");
      _sb.Append("  FROM         RESERVED_TERMINAL_TRANSACTION           ");
      _sb.Append(" WHERE         RTT_TERMINAL_ID   = @pTerminalId        ");
      _sb.Append("   AND         RTT_END_RESERVED IS NULL                ");
      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.CommandType = CommandType.Text;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read() && !_reader.IsDBNull(0))
            {
              _start_time = _reader.GetDateTime(0);

              _reader.Close();
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error on CleanReserve during getting start date from transaction. TerminalID: " + TerminalId);
      }


      // Apply the release at transaction table
      _sb = new StringBuilder();
      _sb.Append("UPDATE   RESERVED_TERMINAL_TRANSACTION           ");
      _sb.Append("   SET                                           ");
      _sb.Append("         RTT_END_RESERVED  =  @pEndReserved      ");
      _sb.Append("        ,RTT_END_UTC       =  @pEndUtc           ");
      _sb.Append("        ,RTT_TOTAL_MINUTES =  @pTotalMinutes     ");
      _sb.Append("        ,RTT_STATUS        =  @pStatus           ");
      _sb.Append(" WHERE   RTT_TERMINAL_ID   =  @pTerminalId       ");
      _sb.Append("   AND   RTT_END_RESERVED  IS NULL               ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.CommandType = CommandType.Text;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          _cmd.Parameters.Add("@pEndReserved", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pEndUtc", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pTotalMinutes", SqlDbType.Int).Value = (WGDB.Now - _start_time).Minutes;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)ReservedStatusType.Completed;
          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error on CleanReserve during updating transaction data. TerminalID: " + TerminalId);
      }

      return true;
    }

    /// <summary>
    /// Release a given terminal, use at external environments, it returns a complete response with info
    /// </summary>
    public static ReservedTerminalResponse Release(Int32 TerminalId, SqlTransaction SqlTrx = null)
    {
      ReservedTerminalResponse _response;

      SqlConnection _sql_conn;
      _sql_conn = null;

      if (SqlTrx == null)
      {
        // Get Sql Connection 
        _sql_conn = WGDB.Connection();
        // Begin Transaction
        SqlTrx = _sql_conn.BeginTransaction();
      }

      object[] _terminal_name_param;
      _terminal_name_param = new object[1];
      _terminal_name_param[0] = GetTerminalName(TerminalId, SqlTrx);

      if (Remove(TerminalId, SqlTrx))
      {
        _response = new ReservedTerminalResponse()
        {
          Code = 1,
          Message = Resource.String("STR_RESERVATION_MSG_TERMINAL_RELEASED", _terminal_name_param[0]),
          Result = true,
          Success = true
        };
      }
      else
      {
        _response = new ReservedTerminalResponse()
        {
          Code = -1,
          Message = Resource.String("STR_RESERVATION_MSG_TERMINAL_NOT_RELEASED", _terminal_name_param[0]),
          Result = false,
          Success = false
        };
      }

      // Close connection (only if has been defined here)
      if (_sql_conn != null)
      {
        try
        {
          SqlTrx.Commit();
          _sql_conn.Close();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        _sql_conn = null;
      }

      return _response;
    }

    /// <summary>
    /// Release all the reserved terminal, use at external environments, it returns a complete response with info
    /// </summary>
    public static ReservedTerminalResponse ReleaseAll(SqlTransaction SqlTrx = null)
    {
      ReservedTerminalResponse _response;

      StringBuilder _terminals_resume;
      List<ReservedTerminal> _reserved_terminals;
      StringBuilder _output_message;
      SqlConnection _sql_conn;

      List<string> _ok_unlocked_terminals;
      List<string> _ko_unlocked_terminals;
      _sql_conn = null;

      if (SqlTrx == null)
      {
        // Get Sql Connection 
        _sql_conn = WGDB.Connection();
        // Begin Transaction
        SqlTrx = _sql_conn.BeginTransaction();
      }

      _response = new ReservedTerminalResponse();
      _ok_unlocked_terminals = new List<string>();
      _ko_unlocked_terminals = new List<string>();
      _terminals_resume = new StringBuilder();
      _output_message = new StringBuilder();

      // Getting all the reserved terminals
      _reserved_terminals = new List<ReservedTerminal>();
      _reserved_terminals = ReservedTerminalsList(SqlTrx);

      if (_reserved_terminals.Count > 0)
      {
        foreach (ReservedTerminal _reserved_terminal in _reserved_terminals)
        {
          // Remove the terminal, if anything goes wrong it will be inserted at ko
          if (Remove(_reserved_terminal.TerminalID, SqlTrx))
          {
            _ok_unlocked_terminals.Add(_reserved_terminal.TerminalName);
          }
          else
          {
            _ko_unlocked_terminals.Add(_reserved_terminal.TerminalName);
          }

        }

        // Mount the _terminals_resume string, this will be the extended message to return
        if (_ok_unlocked_terminals.Count > 0)
        {
          foreach (string _ok_terminal in _ok_unlocked_terminals)
          {
            _terminals_resume.Append(_ok_terminal);


            if (!_ok_terminal.Equals(_ok_unlocked_terminals[_ok_unlocked_terminals.Count - 1]))
            {
              _terminals_resume.Append(",");
            }
          }
        }

        if (_ko_unlocked_terminals.Count > 0)
        {
          _terminals_resume.Append("|");
          foreach (string _ko_terminal in _ko_unlocked_terminals)
          {
            _terminals_resume.Append(_ko_terminal);
            if (!_ko_terminal.Equals(_ko_unlocked_terminals[_ko_unlocked_terminals.Count - 1]))
            {
              _terminals_resume.Append(",");
            }
          }

          // _output_message will return a formatted text to show in a msgbox
          _output_message.AppendLine(Resource.String("STR_RESERVATION_MSG_SOME_TERMINALS_RELEASED"));
          _output_message.AppendLine("OK: " + _terminals_resume.ToString().Split('|')[0].ToString());
          _output_message.AppendLine("KO: " + _terminals_resume.ToString().Split('|')[1].ToString());
        }
        else
        {
          _output_message.Append(Resource.String("STR_RESERVATION_MSG_ALL_TERMINALS_RELEASED"));
        }
      }
      else
      {
        _output_message.Append(Resource.String("STR_RESERVATION_MSG_ALL_TERMINALS_NOT_SELECTED"));
      }

      _response = new ReservedTerminalResponse()
      {
        Code = 1,
        Message = _output_message.ToString(),
        Success = true,
        MessageExtended = _terminals_resume.ToString()
      };

      // Close connection (only if has been defined here)
      if (_sql_conn != null)
      {
        try
        {
          SqlTrx.Commit();
          _sql_conn.Close();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        _sql_conn = null;
      }

      return _response;
    }

    public static String GetErrorMessage(ReserveTerminalResult MsgErrorCode)
    {
      ReserveTerminalResultData _reserve_error_data;
      _reserve_error_data = new ReserveTerminalResultData()
      {
        ErrorCode = MsgErrorCode
      };


      return GetErrorMessage(_reserve_error_data);
    }

    /// <summary>
    /// Return the error message given
    /// </summary>
    public static String GetErrorMessage(ReserveTerminalResultData ErrorData)
    {
      String _msg;

      _msg = "";

      switch (ErrorData.ErrorCode)
      {
        case ReserveTerminalResult.Ok:
          {
            _msg = "";
          }
          break;

        case ReserveTerminalResult.UnknownError:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_UNKNOWN_ERROR");
          }
          break;

        case ReserveTerminalResult.AlreadyReserved:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_ALREADY_RESERVED_ERROR");
          }
          break;

        case ReserveTerminalResult.NoTrackDataAccount:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_NO_TRACK_DATA_ACCOUNT");
          }
          break;

        case ReserveTerminalResult.AccountHasAlreadyAReservation:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_ACCOUNT_HAS_ALREADY_A_RESERVATION");

          }
          break;

        case ReserveTerminalResult.AccountHasNoSessionOnTerminal:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_ACCOUNT_HAS_NO_OPEN_PLAYSESSION");

          }
          break;

        case ReserveTerminalResult.TerminalOnAnothersAccountSession:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_TERMINAL_ON_ANOTHER_ACCOUNT_SESSION");
          }
          break;

        case ReserveTerminalResult.TerminalNotReservable:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_TERMINAL_NOT_ABLE_TO_RESERVE");
          }
          break;

        case ReserveTerminalResult.AccountCantReserve:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_ACCOUNT_NOT_ABLE_TO_RESERVE");
          }
          break;

        case ReserveTerminalResult.MinimumCoinInNotReached:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_RESERVATION_COIN_IN_NOT_REACHED", ErrorData.MinimumCoinIn);
          }
          break;

        case ReserveTerminalResult.ReservationNotEnabled:
          {
            _msg = Resource.String("STR_RESERVE_TERMINAL_RESERVATION_NOT_ENABLED");
          }
          break;

        default:
          {
            Log.Warning("Unexpected TerminalReserveResult code: " + ErrorData.ErrorCode.ToString());
            _msg = "";
          }
          break;
      }

      _msg = _msg.Replace("\\n", Environment.NewLine);

      return _msg;
    }

    /// <summary>
    /// Checks the given terminal status
    /// </summary>
    public static TerminalReservationStatus CheckStatus(Int32 TerminalId, Int64 AccountId, out Int32 ReservedTerminalId, SqlTransaction SqlTrx)
    {
      TerminalReservationStatus _ret;
      StringBuilder _sb;
      Int32 _num_rows;
      Int32 _terminal_sas_flags;

      _ret = TerminalReservationStatus.NotReserved;
      ReservedTerminalId = 0;

      try
      {
        // Check the terminal is not previously reserved
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   TE_SAS_FLAGS                  ");
        _sb.AppendLine("     FROM   TERMINALS                     ");
        _sb.AppendLine("    WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              // Something wrong happened.  We'll manage this as the terminal hasn't be reserved
              return TerminalReservationStatus.NotReserved;
            }

            _terminal_sas_flags = _reader.GetInt32(_reader.GetOrdinal("TE_SAS_FLAGS"));

            _reader.Close();
          }
        }

        if (WSI.Common.Terminal.SASFlagActived(_terminal_sas_flags, SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL))
        {
          // The terminal is reserved.  Let's check by who
          _sb = new StringBuilder();

          _sb.AppendLine("SELECT   COUNT(*)                           ");
          _sb.AppendLine("  FROM   TERMINALS                          ");
          _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId      ");
          _sb.AppendLine("   AND   TE_RESERVE_ACCOUNT_ID = @pAccountID");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

            _num_rows = (Int32)_sql_cmd.ExecuteScalar();

            if (_num_rows == 1)
            {
              // Record found.  This means that is reserved by the account
              _ret = TerminalReservationStatus.ReservedByAccount;
            }
            else
            {
              // Record not found.  This means that is reserved by another account
              _ret = TerminalReservationStatus.ReservedByAnotherAccount;
            }
          }
        }
        else
        {
          // This terminal is not reserved.  Check if the account has another terminal reserved.
          _sb = new StringBuilder();

          _sb.AppendLine("SELECT   TOP 1 TE_TERMINAL_ID               ");
          _sb.AppendLine("  FROM   TERMINALS                          ");
          _sb.AppendLine(" WHERE   TE_RESERVE_ACCOUNT_ID = @pAccountID");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                ReservedTerminalId = _reader.GetInt32(_reader.GetOrdinal("TE_TERMINAL_ID"));

                _ret = TerminalReservationStatus.AccountHasAnotherTerminalReserved;
              }

              _reader.Close();
            }
          }
        }

        return _ret;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error on CheckReservationStatus. TerminalID: " + TerminalId);

        return TerminalReservationStatus.NotReserved;
      }
    }

    /// <summary>
    /// Checks the given terminal expiration
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static bool CheckExpiration(Int32 TerminalId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_rows;
      Int32 _terminal_sas_flags;

      try
      {
        // Check the terminal is not previously reserved
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   TE_SAS_FLAGS                  ");
        _sb.AppendLine("     FROM   TERMINALS                     ");
        _sb.AppendLine("    WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              // Something wrong happened.  We'll manage this as the terminal hasn't be reserved
              return false;
            }

            _terminal_sas_flags = _reader.GetInt32(_reader.GetOrdinal("TE_SAS_FLAGS"));

            _reader.Close();
          }
        }

        if (WSI.Common.Terminal.SASFlagActived(_terminal_sas_flags, SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL))
        {
          _sb = new StringBuilder();

          _sb.AppendLine("SELECT   COUNT(*)                           ");
          _sb.AppendLine("  FROM   TERMINALS                          ");
          _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId      ");
          _sb.AppendLine("   AND   TE_RESERVATION_EXPIRES < GetDate() ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

            _num_rows = (Int32)_sql_cmd.ExecuteScalar();

            return (_num_rows == 1);
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error on CheckReserveExpiration. TerminalID: " + TerminalId);
      }

      return false;
    }

    #endregion // Public Methods

    #region Private Methods

    /// <summary>
    /// Returns a list with all the reserved terminals
    /// </summary>
    private static List<ReservedTerminal> ReservedTerminalsList(SqlTransaction SqlTrx)
    {
      List<ReservedTerminal> _list_reserved_terminals;

      _list_reserved_terminals = new List<ReservedTerminal>();
      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.Append("    SELECT        RTT.RTT_TERMINAL_ID, RTT.RTT_ACCOUNT_ID, T.TE_NAME        ");
        _sb.Append("      FROM        RESERVED_TERMINAL_TRANSACTION RTT                         ");
        _sb.Append("INNER JOIN        TERMINALS T ON RTT.RTT_TERMINAL_ID = T.TE_TERMINAL_ID     ");
        _sb.Append("     WHERE        RTT.RTT_STATUS = 1                                        ");
        _sb.Append("       AND        RTT.RTT_END_RESERVED IS NULL                              ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.CommandType = CommandType.Text;
          SqlDataReader _reader = _cmd.ExecuteReader();
          while (_reader.Read())
          {
            ReservedTerminal _reserved_terminal = new ReservedTerminal();
            _reserved_terminal.TerminalID = _reader.GetInt32(0);
            _reserved_terminal.AccountID = _reader.GetInt64(1);
            _reserved_terminal.TerminalName = _reader.GetString(2);
            _list_reserved_terminals.Add(_reserved_terminal);
          }
          _reader.Close();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return null;
      }
      return _list_reserved_terminals;
    }

    /// <summary>
    /// Returns a terminal name given the id
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static string GetTerminalName(Int32 TerminalId, SqlTransaction SqlTrx)
    {
      String _terminal_name;
      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.Append("    SELECT        TE_NAME                             ");
      _sb.Append("      FROM        TERMINALS                           ");
      _sb.Append("     WHERE        TE_TERMINAL_ID = @pTerminalId       ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
        Object _obj;
        _obj = _cmd.ExecuteScalar();
        if (_obj == null)
        {
          _terminal_name = (string)_obj;
        }
        else
        {
          _terminal_name = string.Empty;
        }
        return _terminal_name;
      }
    }

    /// <summary>
    /// Returns if a terminal can be reserved
    /// </summary>
    private static ReservedTerminalResponse TerminalCanBeReserved(ReservedTerminal Request, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      ReservedTerminalResponse _response;
      // Looking at the exploit table to know if a terminal can be reserved
      _sb = new StringBuilder();
      _sb.Append("SELECT  TG_TERMINAL_ID                        ");
      _sb.Append("FROM    TERMINAL_GROUPS                       ");
      _sb.Append("WHERE   TG_TERMINAL_ID    = @pTerminalID      ");
      _sb.Append("AND     TG_ELEMENT_ID     = @pLevel           ");
      _sb.Append("AND     TG_ELEMENT_TYPE   = @pElementType     ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.BigInt).Value = Request.TerminalID;
        _sql_cmd.Parameters.Add("@pLevel", SqlDbType.BigInt).Value = Request.Level;
        _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.RESERVED_TERMINALS;

        Object _obj;
        _obj = _sql_cmd.ExecuteScalar();

        if (_obj != null)
        {
          _response = new ReservedTerminalResponse()
          {
            Code = 1,
            Message = Resource.String("STR_RESERVATION_MSG_TERMINAL_CAN_BE_RESERVED"),
            Success = true,
            Result = true
          };
        }
        else
        {
          _response = new ReservedTerminalResponse()
          {
            Code = 2,
            Message = Resource.String("STR_RESERVATION_MSG_TERMINAL_CANNOT_BE_RESERVED"),
            Success = true,
            Result = false
          };
        }
      }

      return _response;
    }

    #endregion // Private Methods

  } // TerminalReservation

}  // WSI.Common
