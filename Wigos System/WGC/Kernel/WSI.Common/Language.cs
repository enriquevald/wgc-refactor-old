//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Language.cs
// 
//   DESCRIPTION: Class to get LanguageId from GP configuration
// 
//        AUTHOR: David Rigal
// 
// CREATION DATE: 09-OCT-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-OCT-2014 DRV    Initial Version
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public class Language
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Returns the LanguageIdentifier for de WigosGUI configured language
    // 
    //  PARAMS:
    //
    // RETURNS: 
    //      - LanguageIdentifier
    // 
    public static LanguageIdentifier GetWigosGUILanguageId()
    {
      LanguageIdentifier _language;
      switch (GeneralParam.GetString("WigosGUI", "Language"))
      {
        case "es":
          _language = LanguageIdentifier.Spanish;
          break;

        case "en-US":
          _language = LanguageIdentifier.English;
          break;
        
        case "fr":
          _language = LanguageIdentifier.French;
          break;

        default:
          _language = LanguageIdentifier.English;
          break;
      }

      return _language;
    }
  }
}
