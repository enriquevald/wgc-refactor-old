//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PromotionsUNRImport.cs
// 
//   DESCRIPTION: Class to manage UNR Promotions import 
// 
//        AUTHOR: David Hernandez
// 
// CREATION DATE: 30-SEP-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-SEP-2013 DHA    First release.
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using System.Reflection;
using SpreadsheetGear;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;

namespace WSI.Common
{
  public class PromotionsUNRImport
  {
    #region ENUMS

    public enum IMPORT_STATES
    {
      ReadingDocument = 0,
      DocumentReaded = 1,
      UpdatingDB = 2,
      DBUpdated = 3,
      Aborted = 4

    }

    public enum ERROR
    {
      None = 0,
      NullSite,
      NoExistSite,
      NullMonth,
      NotExistMonth,
      NullPrize,
      NegativePrize,
      NullValueDay,
      NegativeValueDay,

      NullCreationDate,
      NullActivationDate,
      NullAccount,
      NoExistAccount,
      NoExistPromotion,
      AddPromotionToAccount,
      VoucherGenerationFail,
      InsertOperation,
      InsertAccountPromotions,
      PromotionApplySystemAction2,
      PromotionChangesDatesAndCreditToExhausted,
      ReadPromotionData,

    }

    #endregion ENUMS

    #region CONSTANTS

    public const int SQL_COLUMN_ACP_UNIQUE_ID = 0;
    public const int SQL_COLUMN_ACP_ACCOUNT_ID = 2;

    private const Int32 FIRST_DATA_ROW = 1;
    private const Int32 COLUMN_NAMES_ROW = 0;
    private const String TABLE_NAME = "ACCOUNTS";
    private const Decimal PERCENTATGE_ACCOUNTS_MIN = 50;
    private const Decimal PERCENTATGE_ACCOUNTS_MAX = 75;
    private const Decimal PERCENTATGE_COVARIANCE = 25;
    private const Int32 MILISECONDS_IN_A_DAY = 86400000;
    private const Int32 DAYS_FROM_LAST_ACTIVITY = 2;
    private const Int32 ANONYMOUS_ACCOUNTS_TO_CREATE = 5;

    #endregion CONSTANTS

    #region members

    public String m_file_path;
    public String m_file_out;
    public String[] m_Excel_cols;
    public String[] m_DB_cols;
    public Int32 m_not_new_accounts;
    public ColumnDictionary m_column_matches;
    public DataTable m_dt_accounts;
    public RichTextBox m_txt_log;
    public Int32 m_rows_inserted;
    public Int32 m_rows_processed;
    public SpreadsheetGear.IRange _excel_range_output;
    public BackgroundWorker m_worker;
    private Boolean m_is_center;
    private Int32 m_site_id;
    private DataTable m_general_params_to_undo;

    public class ColumnDictionary : Dictionary<String, String>
    {
      public new void Add(String Key, String Value)
      {
        base.Add(Key.Trim().ToUpper(), Value.Trim());
      }
    }

    #endregion

    # region private methods

    /// <summary>
    /// return the db column name that matches in the dicctionary m_column_matches
    /// </summary>
    /// <param name="ExcelColName"></param>
    /// <returns></returns>
    private String GetPromotionsUNRColName(String ExcelColName)
    {
      String _account_col_name;
      _account_col_name = "";

      try
      {
        String _key;
        _key = ExcelColName.Trim().ToUpper();
        if (m_column_matches.ContainsKey(_key))
        {
          _account_col_name = m_column_matches[_key];

        }
      }
      catch
      {
        return "";
      }
      return _account_col_name;
    }

    private String GetExcelColName(String DBColName)
    {
      String _excel_col_name;
      _excel_col_name = "";

      try
      {
        foreach (KeyValuePair<String, String> _key_pair in m_column_matches)
        {
          if (DBColName.Trim().ToUpper() == _key_pair.Value.Trim().ToUpper())
          {
            _excel_col_name = _key_pair.Key;
          }
        }
      }
      catch
      {
        return "";
      }
      return _excel_col_name;
    }

    private void ColummMatches()
    {
      m_column_matches = new ColumnDictionary();
      m_column_matches.Add("Sala", "Site");
      m_column_matches.Add("Mes", "Month");
      m_column_matches.Add("Importe UNR", "UNR_Amount");

      for (Int32 _day = 1; _day <= 31; _day++)
      {
        m_column_matches.Add(_day.ToString(), _day.ToString());
      }
    }
    /// <summary>
    /// returns a formated cell value depending on the column datatype and def values
    /// 
    /// </summary>
    /// <param name="CellValue"></param>
    /// <param name="Column"></param>
    /// <returns></returns>
    private object CheckCell(object CellValue, DataColumn Column, out Boolean Warning, out String WarningMessage)
    {
      Warning = false;
      WarningMessage = "";
      String _str_value;
      String _upper_case_value;

      try
      {
        _str_value = "";
        if (CellValue != null)
        {
          _str_value = CellValue.ToString().Trim();
        }
        _upper_case_value = _str_value.ToUpper();

        if (String.IsNullOrEmpty(_str_value))
        {
          CellValue = DBNull.Value;
        }
        else if (CellValue != DBNull.Value)// !((CellValue == null) || (CellValue.ToString().Trim() == ""))
        {
          switch (Column.DataType.ToString())
          {
            case "System.String":
              {
                Warning = (_str_value.Length > Column.MaxLength);
                if (Warning)
                {
                  CellValue = (_str_value.Substring(0, Column.MaxLength));
                  WarningMessage += "\r\nWARNING: " + GetExcelColName(Column.ColumnName) + Resource.String("STR_AC_IMPORT_TRUNCATED");
                }
                else
                {
                  CellValue = (_str_value);
                }
                break;
              }
            case "System.Int32":
              {
                if (CellValue is Double)
                {
                  CellValue = Convert.ToInt32((Double)CellValue);
                }
                else if (CellValue is Decimal)
                {
                  CellValue = Convert.ToInt32((Decimal)CellValue);
                }
                else if (CellValue is Int64)
                {
                  CellValue = Convert.ToInt32((Int64)CellValue);
                }
                else
                {
                  CellValue = (Int32)CellValue;
                }
                break;
              }
            case "System.DateTime":
              {
                CellValue = DateTime.FromOADate((Double)CellValue);
                break;
              }

            case "System.Decimal":
              if (CellValue is Double)
              {
                CellValue = (Double)CellValue;
              }
              else
              {
                CellValue = (Decimal)CellValue;
              }
              break;

            default:
              break;
          }
        }
      }
      catch (Exception)
      {
        throw new Exception(Resource.String("STR_AC_IMPORT_DATA_FORMAT") + " - " + GetExcelColName(Column.ColumnName));
      }
      return CellValue;
    }

    private Boolean CheckRow(ref DataRow ImportExcelRow, DataColumnCollection Columns, int RowNumber)
    {
      DateTime temp_date_time;

      if (ImportExcelRow["Site"] == DBNull.Value)
      {
        AppendTextBox(string.Format("\tError: la Sala ha de estar informada."), System.Drawing.Color.Red);
        Log.Error("Error: la Sala ha de estar informada.");
        ImportExcelRow["Error"] = ERROR.NullSite;
        return false;
      }
      else if (!m_is_center && (Int32)ImportExcelRow["Site"] != m_site_id)
      {
        AppendTextBox(string.Format("\tError: la Sala no se puede importar."), System.Drawing.Color.Red);
        Log.Error("Error: la Sala no se puede importar.");
        ImportExcelRow["Error"] = ERROR.NoExistSite;
        return false;
      }

      if (ImportExcelRow["Month"] == DBNull.Value)
      {
        AppendTextBox(string.Format("\tError: el mes ha de estar informado."), System.Drawing.Color.Red);
        Log.Error("Error: el m�s ha de estar informado.");
        ImportExcelRow["Error"] = ERROR.NullMonth;
        return false;
      }
      else if (!DateTime.TryParse(ImportExcelRow["Month"].ToString(), out temp_date_time))
      {
        AppendTextBox(string.Format("\tError: la fecha no es correcta."), System.Drawing.Color.Red);
        Log.Error("Error: la fecha no es correcta.");
        ImportExcelRow["Error"] = ERROR.NotExistMonth;
        return false;
      }

      if (ImportExcelRow["UNR_Amount"] == DBNull.Value)
      {
        AppendTextBox(string.Format("\tError: el importe UNR ha de estar informado."), System.Drawing.Color.Red);
        Log.Error("Error: el importe UNR ha de estar informado.");
        ImportExcelRow["Error"] = ERROR.NullPrize;
        return false;
      }
      else if ((Decimal)ImportExcelRow["UNR_Amount"] <= 0)
      {
        AppendTextBox(string.Format("\tError: el premio ha de ser mayor que 0."), System.Drawing.Color.Red);
        Log.Error("Error: el premio ha de ser mayor que 0.");
        ImportExcelRow["Error"] = ERROR.NegativePrize;
        return false;
      }

      Int32 _days_in_month = DateTime.DaysInMonth(temp_date_time.Year, temp_date_time.Month);

      for (Int32 _day = 1; _day <= _days_in_month; _day++)
      {
        if (ImportExcelRow[_day.ToString()] == DBNull.Value)
        {
          AppendTextBox(string.Format("\tError: el valor del d�a {0} ha de estar informado.", _day), System.Drawing.Color.Red);
          Log.Error("Error: el valor del d�a {0} ha de estar informado.");
          ImportExcelRow["Error"] = ERROR.NullValueDay;
          return false;
        }
        else if ((Int32)ImportExcelRow[_day.ToString()] < 0)
        {
          AppendTextBox(string.Format("\tError: el valor del d�a {0} ha de ser mayor o igual que 0.", _day), System.Drawing.Color.Red);
          Log.Error("Error: el valor del d�a {0} ha de ser mayor o igual que 0.");
          ImportExcelRow["Error"] = ERROR.NegativeValueDay;
          return false;
        }
      }

      return true;
    }

    private Boolean GetAccounts(DataTable Accounts)
    {
      StringBuilder _sb_select;

      _sb_select = new StringBuilder();

      try
      {
        using (SqlConnection _conn = WGDB.Connection())
        {
          _sb_select.AppendLine("SELECT   ");
          _sb_select.AppendLine("         AC_ACCOUNT_ID");
          _sb_select.AppendLine("       , AC_CREATED");
          _sb_select.AppendLine("       , AC_LAST_ACTIVITY");
          _sb_select.AppendLine("       , AC_HOLDER_LEVEL");
          _sb_select.AppendLine("FROM " + TABLE_NAME);
          _sb_select.AppendLine("WHERE  AC_HOLDER_LEVEL = 0 ");
          _sb_select.AppendLine("ORDER  BY AC_ACCOUNT_ID");

          using (SqlCommand _select_cmd = new SqlCommand(_sb_select.ToString(), _conn))
          {
            using (SqlDataAdapter _da = new SqlDataAdapter(_select_cmd))
            {
              _da.Fill(m_dt_accounts);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return true;

    }

    private Boolean ChangeGeneralParamsByDate(DataTable GeneralParams, DateTime DateToApply)
    {
      DataRow[] _general_params_to_apply;

      try
      {
        if (GeneralParams == null || GeneralParams.Rows.Count == 0)
        {
          return true;
        }

        if (m_general_params_to_undo == null)
        {
          if (GeneralParams != null)
          {
            m_general_params_to_undo = GeneralParams.Clone();
          }
          else
          {
            m_general_params_to_undo = new DataTable();
          }
        }

        // Undo general params
        if (m_general_params_to_undo.Rows.Count > 0)
        {
          foreach (DataRow _row in m_general_params_to_undo.Rows)
          {
            ///////WSI.Common.GeneralParam.SetValue(_row["GP_GROUP_KEY"].ToString(), _row["GP_SUBJECT_KEY"].ToString(), _row["GP_KEY_VALUE"].ToString());
          }

          m_general_params_to_undo.Clear();
        }

        // Change general params and save old values
        if (GeneralParams.Rows.Count > 0)
        {
          //_general_params_to_apply = GeneralParams.Select(String.Format("GP_DATE_TO_APPLY > {0}", DateToApply.ToString("dd/MM/yyyy HH:mm:ss")));
          _general_params_to_apply = GeneralParams.Select(String.Format("GP_DATE_TO_APPLY > '{0}'", DateToApply.ToString("dd/MM/yyyy HH:mm:ss")));

          foreach (DataRow _row in _general_params_to_apply)
          {
            m_general_params_to_undo.Rows.Add(new object[] {_row["GP_GROUP_KEY"].ToString(), 
                                                             _row["GP_SUBJECT_KEY"].ToString(), 
                                                             WSI.Common.GeneralParam.Value(_row["GP_GROUP_KEY"].ToString(), _row["GP_SUBJECT_KEY"].ToString()),
                                                             null});

            ///////WSI.Common.GeneralParam.SetValue(_row["GP_GROUP_KEY"].ToString(), _row["GP_SUBJECT_KEY"].ToString(), _row["GP_KEY_VALUE"].ToString());
          }
        }
      }
      catch
      {
        return false;
      }

      return true;
    }

    private Boolean InsertPromotions(DataTable PromotionsUNR, Int32 UserId, AccountPromotion Promotion, DataTable GeneralParams, SqlTransaction SqlTrx)
    {
      DataTable _promos;
      CardData _card;
      CashierSessionInfo _session_info;
      Int64 _operation_id;
      DateTime _creation_date;
      DateTime _activation_date;
      OperationCode _operation_code;
      //MultiPromos.PromoBalance _total_balance;
      int _num_applied;
      DateTime _open_cash_date;
      DateTime _close_cash_date;

      m_rows_inserted = 0;
      m_rows_processed = 0;
      _operation_code = OperationCode.PROMOTION;
      _promos = AccountPromotion.CreateAccountPromotionTable();
      m_worker.ReportProgress(0);

      if (!_promos.Columns.Contains("CALCULATED_ACTIVATION_DATE"))
      {
        _promos.Columns.Add("CALCULATED_ACTIVATION_DATE", Type.GetType("System.DateTime"));
      }

      if (!_promos.Columns.Contains("CALCULATED_CREATION_DATE"))
      {
        _promos.Columns.Add("CALCULATED_CREATION_DATE", Type.GetType("System.DateTime"));
      }

      if (!_promos.Columns.Contains("EXPIRATION_VALUE"))
      {
        _promos.Columns.Add("EXPIRATION_VALUE", Type.GetType("System.Int32"));
      }

      if (!_promos.Columns.Contains("EXPIRATION_TYPE"))
      {
        _promos.Columns.Add("EXPIRATION_TYPE", Type.GetType("System.Int32"));
      }

      _card = new CardData();
      _operation_id = 0;

      // Get or open cashier session
      _open_cash_date = (DateTime)PromotionsUNR.Rows[0]["Creation_Date"];
      _session_info = GetOrOpenCashierSessionByOpenDate(UserId, null, _open_cash_date, SqlTrx);

      AppendTextBox(string.Format("{0}Insertando promociones para el mes de {1} de {2}{0}", Environment.NewLine, _open_cash_date.ToString("MMMM"), _open_cash_date.ToString("yyyy")));
      Log.Message(string.Format("Insertando promociones para el mes de {0} de {1}", _open_cash_date.ToString("MMMM"), _open_cash_date.ToString("yyyy")));

      foreach (DataRow row in PromotionsUNR.Rows)
      {
        // Change general params value by date
        if (!ChangeGeneralParamsByDate(GeneralParams, ((DateTime)row["Creation_Date"])))
        {
          AppendTextBox(string.Format("{0}\tError: ChangeGeneralParamsByDate", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: ChangeGeneralParamsByDate");
          return false;
        }

        m_rows_processed++;
        m_worker.ReportProgress(m_rows_processed * 100 / PromotionsUNR.Rows.Count);

        CardData.DB_CardGetAllData((int)row["Account_ID"], _card);
        _promos.Clear();

        Log.Message(string.Format("Cuenta: {0}\tPremio: {1}\tCreado: {2}\t\tActivado: {3}", _card.AccountId, (Decimal)row["Prize_NR_Gross"], ((DateTime)row["Creation_Date"]).ToString("dd/MM/yyyy HH:mm:ss"), ((DateTime)row["Activation_Date"]).ToString("dd/MM/yyyy HH:mm:ss")));

        // Insert a PROMOTION operation.
        if (!Operations.DB_InsertOperation(_operation_code, _card.AccountId, _session_info.CashierSessionId, 0,
                                           Promotion.id, 0, (Decimal)row["Prize_NR_Gross"], 0, 0, string.Empty, out _operation_id, SqlTrx))
        {
          AppendTextBox(string.Format("{0}\tError: DB_InsertOperation", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: DB_InsertOperation");
          row["Error"] = ERROR.InsertOperation;
          return false;
        }

        // Add promotion to account
        if (!AccountPromotion.AddPromotionToAccount(_operation_id, _card.AccountId, (Int32)row["Account_Level"], 0, 0, true, (Decimal)row["Prize_NR_Gross"], Promotion, _promos))
        {
          AppendTextBox(string.Format("{0}\tError: AddPromotionToAccount", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: AddPromotionToAccount");
          row["Error"] = ERROR.AddPromotionToAccount;
          return false;
        }

        // Change dates from promotion
        _creation_date = (DateTime)row["Creation_Date"];
        _activation_date = (DateTime)row["Activation_Date"];
        _promos.Rows[_promos.Rows.Count - 1]["CALCULATED_CREATION_DATE"] = _creation_date;
        _promos.Rows[_promos.Rows.Count - 1]["CALCULATED_ACTIVATION_DATE"] = _activation_date;
        _promos.Rows[_promos.Rows.Count - 1]["EXPIRATION_VALUE"] = Promotion.expiration_value;
        _promos.Rows[_promos.Rows.Count - 1]["EXPIRATION_TYPE"] = Promotion.expiration_type;

        // Generate promotion voucher
        if (!GeneratePromotionVouchers(_operation_id, _promos.Rows[_promos.Rows.Count - 1], _card, SqlTrx))
        {
          AppendTextBox(string.Format("{0}\tError: GeneratePromotionVouchers", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: GeneratePromotionVouchers");
          row["Error"] = ERROR.VoucherGenerationFail;
          return false;
        }

        // Insert account promotions
        if (!AccountPromotion.InsertAccountPromotions(_promos, false, SqlTrx))
        {
          AppendTextBox(string.Format("{0}\tError: InsertAccountPromotions", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: InsertAccountPromotions");
          row["Error"] = ERROR.InsertAccountPromotions;
          return false;
        }

        // Apply promotions
        ///////_num_applied = Trx_PromotionApplySystemActionByCashierSession(AccountPromotion.Trx_ActivatePromotion, _promos, true, _session_info, out _total_balance, SqlTrx);
        _num_applied = 0;

        if (_num_applied != _promos.Rows.Count)
        {
          AppendTextBox(string.Format("{0}\tError: Trx_PromotionApplySystemAction", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: Trx_PromotionApplySystemAction");
          //row["Error"] = ERROR.PromotionApplySystemAction;
          return false;
        }

        // Changes dates and set the credito of promotions to exhausted
        if (!PromotionChangesDatesAndCreditToExhausted(_promos, SqlTrx))
        {
          AppendTextBox(string.Format("{0}\tError: PromotionChangesDatesAndCreditToExhausted", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: PromotionChangesDatesAndCreditToExhausted");
          row["Error"] = ERROR.PromotionChangesDatesAndCreditToExhausted;
          return false;
        }

        row["Error"] = ERROR.None;
        m_rows_inserted++;
      }

      // Close cashier session
      _close_cash_date = _open_cash_date.AddMonths(1).AddMinutes(-1);

      if (!CashierSessionCloseByIdAndDate(_session_info, _close_cash_date, SqlTrx))
      {
        AppendTextBox(string.Format("{0}\tError: CashierSessionCloseByIdAndDate", Environment.NewLine), System.Drawing.Color.Red);
        Log.Error("Error: CashierSessionCloseByIdAndDate");
        return false;
      }

      return true;
    }

    private Boolean AccountPromotionsChangeDateAndCreditToExhausted(DataRow AccountPromotion, String Details, SqlTransaction SqlTrx)
    {
      Int64 _unique_id;
      Int64 _account_id;
      StringBuilder _sb;
      MultiPromos.AccountBalance _ini_bal;
      Decimal _ini_points;
      Int64 _dummy_ps;
      MultiPromos.AccountBalance _promo_balance;
      DateTime _creation_date;
      DateTime _tomorrow;
      TimeSpan _diff;

      try
      {
        // 1 - Lock account.
        _account_id = (Int64)AccountPromotion[SQL_COLUMN_ACP_ACCOUNT_ID];
        _unique_id = (Int64)AccountPromotion[SQL_COLUMN_ACP_UNIQUE_ID];

        _promo_balance = new MultiPromos.AccountBalance(0, 0, -(Decimal)AccountPromotion["ACP_BALANCE"]);

        if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, _promo_balance, out _ini_bal, out _ini_points, out _dummy_ps, SqlTrx))
        {
          AppendTextBox(string.Format("{0}Error: Trx_UpdateAccountBalance", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: Trx_UpdateAccountBalance");
          return false;
        }

        // Change dates from promotion
        _creation_date = (DateTime)AccountPromotion["CALCULATED_CREATION_DATE"];

        switch ((Promotion.EXPIRATION_TYPE)AccountPromotion["EXPIRATION_TYPE"])
        {
          case Promotion.EXPIRATION_TYPE.EXPIRATION_TYPE_DAYS:
            if ((Int32)AccountPromotion["EXPIRATION_VALUE"] > 0)
            {
              _tomorrow = _creation_date.AddDays(1);

              _diff = _tomorrow - _creation_date;
              if ((Int32)AccountPromotion["EXPIRATION_VALUE"] == 1 && _diff.TotalHours <= 8)
              {
                AccountPromotion["ACP_EXPIRATION"] = _tomorrow.AddDays((Int32)AccountPromotion["EXPIRATION_VALUE"]);
              }
              else
              {
                AccountPromotion["ACP_EXPIRATION"] = _creation_date.AddDays((Int32)AccountPromotion["EXPIRATION_VALUE"]);
              }
            }
            break;
          case Promotion.EXPIRATION_TYPE.EXPIRATION_TYPE_HOURS:
            if ((Int32)AccountPromotion["EXPIRATION_VALUE"] > 0)
            {
              AccountPromotion["ACP_EXPIRATION"] = _creation_date.AddHours((Int32)AccountPromotion["EXPIRATION_VALUE"]);
            }
            break;
          case Promotion.EXPIRATION_TYPE.EXPIRATION_TYPE_NOT_SET:
            break;
        }

        // 2 - Exhaust promotion and change date
        try
        {
          _sb = new StringBuilder();
          _sb.AppendLine("  UPDATE   ACCOUNT_PROMOTIONS           ");
          _sb.AppendLine("     SET   ACP_STATUS     = @pStatusExhausted, ");
          _sb.AppendLine("           ACP_CREATED   = @pCreationDate, ");
          _sb.AppendLine("           ACP_PROMO_DATE   = @pCreationDate, ");
          _sb.AppendLine("           ACP_EXPIRATION = @pEpirationDate, ");
          _sb.AppendLine("           ACP_ACTIVATION = @pActivationDate, ");
          _sb.AppendLine("           ACP_UPDATED    = @pActivationDate ");
          _sb.AppendLine("   WHERE   ACP_UNIQUE_ID  = @pUniqueId  ");
          _sb.AppendLine("     AND   ACP_STATUS     in (@pStatusActive) ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
            _sql_cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = (DateTime)AccountPromotion["CALCULATED_CREATION_DATE"];
            _sql_cmd.Parameters.Add("@pEpirationDate", SqlDbType.DateTime).Value = (DateTime)AccountPromotion["ACP_EXPIRATION"];
            _sql_cmd.Parameters.Add("@pStatusExhausted", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.EXHAUSTED;
            _sql_cmd.Parameters.Add("@pActivationDate", SqlDbType.DateTime).Value = (DateTime)AccountPromotion["CALCULATED_ACTIVATION_DATE"];
            _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _unique_id;

            if (_sql_cmd.ExecuteNonQuery() == 0)
            {
              return false;
            }
          }
        }
        catch
        {
          return false;
        }
      }
      catch
      {
        return false;
      }

      return true;
    } // AccountPromotionsChangeDateAndCreditToExhausted

    private Boolean CashierVouchersChangeDate(DataRow AccountPromotion, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   CASHIER_VOUCHERS                  ");
        _sb.AppendLine("     SET   CV_DATETIME      = @pCreationDate ");
        _sb.AppendLine("   WHERE   CV_OPERATION_ID  = @pOperationID  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOperationID", SqlDbType.Int).Value = AccountPromotion["ACP_OPERATION_ID"];
          _sql_cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = (DateTime)AccountPromotion["CALCULATED_CREATION_DATE"];

          if (_sql_cmd.ExecuteNonQuery() == 0)
          {
            return false;
          }
        }
      }
      catch
      {
        return false;
      }
      return true;
    }

    private Boolean AccountPromotionChangeDate(DataRow AccountPromotion, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   ACCOUNT_OPERATIONS                ");
        _sb.AppendLine("     SET   AO_DATETIME      = @pCreationDate ");
        _sb.AppendLine("   WHERE   AO_OPERATION_ID  = @pOperationID  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOperationID", SqlDbType.Int).Value = AccountPromotion["ACP_OPERATION_ID"];
          _sql_cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = (DateTime)AccountPromotion["CALCULATED_CREATION_DATE"];

          if (_sql_cmd.ExecuteNonQuery() == 0)
          {
            return false;
          }
        }
      }
      catch
      {
        return false;
      }

      return true;
    }

    private Boolean CashierMovementsChangeDate(DataRow AccountPromotion, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE  CASHIER_MOVEMENTS                ");
        _sb.AppendLine("     SET   CM_DATE      = @pCreationDate ");
        _sb.AppendLine("   WHERE   CM_OPERATION_ID  = @pOperationID  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOperationID", SqlDbType.Int).Value = AccountPromotion["ACP_OPERATION_ID"];
          _sql_cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = (DateTime)AccountPromotion["CALCULATED_CREATION_DATE"];

          if (_sql_cmd.ExecuteNonQuery() == 0)
          {
            return false;
          }
        }
      }
      catch
      {
        return false;
      }

      return true;
    }

    private Boolean AccountMovementsChangeDate(DataRow AccountPromotion, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE  ACCOUNT_MOVEMENTS                  ");
        _sb.AppendLine("     SET   AM_DATETIME      = @pCreationDate ");
        _sb.AppendLine("   WHERE   AM_OPERATION_ID  = @pOperationID  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOperationID", SqlDbType.Int).Value = AccountPromotion["ACP_OPERATION_ID"];
          _sql_cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = (DateTime)AccountPromotion["CALCULATED_CREATION_DATE"];

          if (_sql_cmd.ExecuteNonQuery() == 0)
          {
            return false;
          }
        }
      }
      catch
      {
        return false;
      }

      return true;
    }

    private Boolean GeneratePromotionVouchers(Int64 OperationId, DataRow Promos, CardData Card, SqlTransaction SqlTrx)
    {
      TYPE_SPLITS _splits;
      ArrayList _voucher_promo_list;
      DataTable _dt_promos;

      _splits = new TYPE_SPLITS();
      _dt_promos = AccountPromotion.CreateAccountPromotionTable();

      if (!_dt_promos.Columns.Contains("CALCULATED_ACTIVATION_DATE"))
      {
        _dt_promos.Columns.Add("CALCULATED_ACTIVATION_DATE", Type.GetType("System.DateTime"));
      }

      _dt_promos.ImportRow(Promos);

      if (_dt_promos != null)
      {
        if (!VoucherBuilder.PromoLost(Card.VoucherAccountInfo(),
                                                 Card.AccountId,
                                                 _splits,
                                                 true,
                                                 _dt_promos,
                                                 true,
                                                 OperationId,
                                                 PrintMode.Print,
                                                 SqlTrx,
                                                 out _voucher_promo_list))
        {
          return false;
        }
      }
      return true;
    }

    private void CreatePromotionsUNRTableFromExcel(DataTable PromotionsUNRExcel)
    {
      PromotionsUNRExcel.Columns.Add("Site", typeof(Int32));
      PromotionsUNRExcel.Columns.Add("Month", typeof(DateTime));
      PromotionsUNRExcel.Columns.Add("UNR_Amount", typeof(Decimal));

      for (Int32 _day = 1; _day <= 31; _day++)
      {
        PromotionsUNRExcel.Columns.Add(_day.ToString(), typeof(Int32));
      }
      PromotionsUNRExcel.Columns.Add("Error", typeof(Int32));
      PromotionsUNRExcel.Columns.Add("Index", typeof(Int32));
    }

    private void CreatePromotionsToApplyTable(DataTable PromotionsToApply)
    {
      PromotionsToApply.Columns.Add("Account_ID", typeof(Int32));
      PromotionsToApply.Columns.Add("Prize_NR_Gross", typeof(Decimal));
      PromotionsToApply.Columns.Add("Creation_Date", typeof(DateTime));
      PromotionsToApply.Columns.Add("Activation_Date", typeof(DateTime));
      PromotionsToApply.Columns.Add("Account_Level", typeof(Int32));
      PromotionsToApply.Columns.Add("Error", typeof(Int32));

    }

    private Boolean CreateAnonymousAccounts(Int32 NumAccountsToCreate, DateTime CreationDate, out DataTable _dt_accounts, SqlTransaction SqlTrx)
    {
      StringBuilder _sql_insert_query;
      SqlCommand _sql_insert_cmd;
      Int32 _time_to_add_by_promotion;

      _dt_accounts = new DataTable();

      if (NumAccountsToCreate == 0)
      {
        return true;
      }
      _time_to_add_by_promotion = (Int32)Math.Round(((Decimal)MILISECONDS_IN_A_DAY / NumAccountsToCreate), 0);

      _sql_insert_query = new StringBuilder();

      _sql_insert_query.AppendLine(" DECLARE @pAccountId AS BIGINT");
      _sql_insert_query.AppendLine(" EXECUTE CreateAccount @pAccountId OUTPUT");
      _sql_insert_query.AppendLine(" UPDATE ACCOUNTS ");
      _sql_insert_query.AppendLine(" SET AC_CREATED = @p_ac_created ");
      _sql_insert_query.AppendLine(" ,AC_PROMO_CREATION = @p_ac_promo_creation ");
      _sql_insert_query.AppendLine(" ,AC_PROMO_EXPIRATION = @p_ac_promo_expiration ");
      _sql_insert_query.AppendLine(" ,AC_LAST_ACTIVITY = @p_ac_last_activity ");
      _sql_insert_query.AppendLine(" WHERE AC_ACCOUNT_ID = @pAccountId");
      _sql_insert_query.AppendLine("SELECT   ");
      _sql_insert_query.AppendLine("         AC_ACCOUNT_ID");
      _sql_insert_query.AppendLine("       , AC_CREATED");
      _sql_insert_query.AppendLine("       , AC_LAST_ACTIVITY");
      _sql_insert_query.AppendLine("       , AC_HOLDER_LEVEL");
      _sql_insert_query.AppendLine("FROM ACCOUNTS ");
      _sql_insert_query.AppendLine("WHERE  AC_ACCOUNT_ID = @pAccountId ");

      for (Int32 _idx = 0; _idx < NumAccountsToCreate; _idx++)
      {
        using (_sql_insert_cmd = new SqlCommand(_sql_insert_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_insert_cmd.Parameters.Add("@p_ac_created", SqlDbType.DateTime).Value = CreationDate;
          _sql_insert_cmd.Parameters.Add("@p_ac_promo_creation", SqlDbType.DateTime).Value = CreationDate;
          _sql_insert_cmd.Parameters.Add("@p_ac_promo_expiration", SqlDbType.DateTime).Value = CreationDate;
          _sql_insert_cmd.Parameters.Add("@p_ac_last_activity", SqlDbType.DateTime).Value = CreationDate.AddDays(DAYS_FROM_LAST_ACTIVITY);
          CreationDate = CreationDate.AddMilliseconds(_time_to_add_by_promotion);

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_insert_cmd))
          {
            _da.Fill(_dt_accounts);
          }
        }
      }

      return true;
    }

    private Boolean AssignAmountToAccountsRandom(DataRow ExcelImportRow, DataTable Accounts, out DataTable PromotionsToApply)
    {
      Decimal _base_amount_unr;
      Decimal _total_amount_unr_day;
      Random _rnd = new Random();
      Int32 _idx_row_rnd;
      Decimal _account_amount_average;
      Decimal _amount_covariance;
      Decimal _amount_calculate;
      DataRow _account_promo;
      DateTime _promotion_day;
      Decimal _probability_accounts;
      DataTable _copy_accounts;
      Int32 _num_probability_accounts;
      DataTable _promotions_to_apply;

      PromotionsToApply = new DataTable();
      _promotions_to_apply = new DataTable();
      CreatePromotionsToApplyTable(_promotions_to_apply);

      _base_amount_unr = (Decimal)ExcelImportRow["UNR_Amount"];
      if (_base_amount_unr <= 0)
      {
        return false;
      }

      for (Int32 _day = 1; _day <= 31; _day++)
      {
        _promotions_to_apply.Clear();
        // Validate if day exist in moth
        try
        {
          _promotion_day = new DateTime(((DateTime)ExcelImportRow["Month"]).Year, ((DateTime)ExcelImportRow["Month"]).Month, _day);
        }
        catch
        {
          break;
        }
        _copy_accounts = Accounts.Copy();

        // Filter accounts by AC_CREATED and AC_LAST_ACTIVITY
        foreach (DataRow _row in _copy_accounts.Rows)
        {
          if ((DateTime)_row["AC_CREATED"] > _promotion_day && (DateTime)_row["AC_LAST_ACTIVITY"] > _promotion_day.AddDays(-2))
          {
            _row.Delete();
          }
        }
        _copy_accounts.AcceptChanges();

        if (_copy_accounts.Rows.Count <= 0)
        {
          AppendTextBox(String.Format("{0}No hay cuentas para el dia: {1}{0}", Environment.NewLine, _promotion_day), System.Drawing.Color.Red);

          continue;
        }

        // Calculate amount to award by day 
        _total_amount_unr_day = (Int32)ExcelImportRow[_day.ToString()] * _base_amount_unr;

        // Calculate the average amount of the accounts
        _probability_accounts = _rnd.Next((int)PERCENTATGE_ACCOUNTS_MIN, (int)PERCENTATGE_ACCOUNTS_MAX);
        _num_probability_accounts = (Int32)Math.Round((_copy_accounts.Select("AC_CREATED <= '" + _promotion_day + "'").Length * _probability_accounts / 100), 0);
        if (_num_probability_accounts < 1)
        {
          _num_probability_accounts = _copy_accounts.Rows.Count;
        }

        _account_amount_average = Math.Round(_total_amount_unr_day / _num_probability_accounts, 1);
        _amount_covariance = Math.Round((_account_amount_average * PERCENTATGE_COVARIANCE / 100), 1);


        try
        {
          while (_total_amount_unr_day > 0)
          {
            _amount_calculate = _rnd.Next((int)(_account_amount_average - _amount_covariance), (int)(_account_amount_average + _amount_covariance));

            if (_total_amount_unr_day < _amount_calculate)
            {
              _amount_calculate = _total_amount_unr_day;
            }

            if (_copy_accounts.Rows.Count > 0)
            {
              _idx_row_rnd = _rnd.Next(_copy_accounts.Rows.Count);
              _account_promo = _promotions_to_apply.NewRow();

              _account_promo["Account_ID"] = _copy_accounts.Rows[_idx_row_rnd]["AC_ACCOUNT_ID"];
              _account_promo["Prize_NR_Gross"] = _amount_calculate;
              _account_promo["Creation_Date"] = _promotion_day.AddMilliseconds(_rnd.Next(MILISECONDS_IN_A_DAY));
              _account_promo["Activation_Date"] = _account_promo["Creation_Date"];
              _account_promo["Account_Level"] = _copy_accounts.Rows[_idx_row_rnd]["AC_HOLDER_LEVEL"];

              _promotions_to_apply.Rows.Add(_account_promo);
              _copy_accounts.Rows[_idx_row_rnd].Delete();
              _copy_accounts.Rows[_idx_row_rnd].AcceptChanges();
            }
            else
            {
              _idx_row_rnd = _rnd.Next(_promotions_to_apply.Rows.Count);
              _account_promo = _promotions_to_apply.Rows[_idx_row_rnd];

              _account_promo["Prize_NR_Gross"] = (Decimal)_account_promo["Prize_NR_Gross"] + _amount_calculate;

            }

            _total_amount_unr_day -= _amount_calculate;
          }
        }
        catch
        {
        }

        AppendTextBox(String.Format("{0}Asignadas {1} promociones para el dia {2}", Environment.NewLine, _promotions_to_apply.Rows.Count, new DateTime(((DateTime)ExcelImportRow["Month"]).Year, ((DateTime)ExcelImportRow["Month"]).Month, _day).ToString("dd/MM/yyyy")));
        Log.Message(String.Format("Asignadas {0} promociones para el dia {1}", _promotions_to_apply.Rows.Count, new DateTime(((DateTime)ExcelImportRow["Month"]).Year, ((DateTime)ExcelImportRow["Month"]).Month, _day).ToString("dd/MM/yyyy")));

        PromotionsToApply.Merge(_promotions_to_apply);
      }

      AppendTextBox(Environment.NewLine);

      return true;
    }

    private Boolean AssignAmountToAccounts(DataRow ExcelImportRow, out DataTable PromotionsToApply, SqlTransaction SqlTrx)
    {
      Decimal _base_amount_unr;
      DataRow _account_promo;
      DateTime _promotion_day;
      DataTable _dt_accounts;
      Int32 _time_to_add_by_promotion;
      Int32 _closing_time;

      PromotionsToApply = new DataTable();
      CreatePromotionsToApplyTable(PromotionsToApply);

      _base_amount_unr = (Decimal)ExcelImportRow["UNR_Amount"];
      if (_base_amount_unr <= 0)
      {
        return false;
      }

      AppendTextBox(Environment.NewLine);

      for (Int32 _day = 1; _day <= 31; _day++)
      {
        // Validate if day exist in moth
        try
        {
          Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("WigosGUI", "ClosingTime"), out _closing_time);
          _promotion_day = new DateTime(((DateTime)ExcelImportRow["Month"]).Year, ((DateTime)ExcelImportRow["Month"]).Month, _day, _closing_time, 0, 0);
        }
        catch
        {
          break;
        }

        CreateAnonymousAccounts((Int32)ExcelImportRow[_day.ToString()], _promotion_day, out _dt_accounts, SqlTrx);

        if (_dt_accounts.Rows.Count <= 0)
        {
          AppendTextBox(String.Format("{0}No hay cuentas para el dia: {1}", Environment.NewLine, _promotion_day.ToString("dd/MM/yyyy")), System.Drawing.Color.Red);
          Log.Message(String.Format("No hay cuentas para el dia: {0}", _promotion_day.ToString("dd/MM/yyyy")));

          continue;
        }

        try
        {
          _time_to_add_by_promotion = (Int32)Math.Round(((Decimal)MILISECONDS_IN_A_DAY / _dt_accounts.Rows.Count), 0);

          foreach (DataRow _account in _dt_accounts.Rows)
          {
            _account_promo = PromotionsToApply.NewRow();
            _account_promo["Account_ID"] = _account["AC_ACCOUNT_ID"];
            _account_promo["Prize_NR_Gross"] = _base_amount_unr;
            _account_promo["Creation_Date"] = _promotion_day;
            _account_promo["Activation_Date"] = _account_promo["Creation_Date"];
            _account_promo["Account_Level"] = _account["AC_HOLDER_LEVEL"];
            PromotionsToApply.Rows.Add(_account_promo);

            _promotion_day = _promotion_day.AddMilliseconds(_time_to_add_by_promotion);

          }
        }
        catch
        {
        }

        AppendTextBox(String.Format("{0}Asignadas {1} promociones para el dia {2}", Environment.NewLine, _dt_accounts.Rows.Count, new DateTime(((DateTime)ExcelImportRow["Month"]).Year, ((DateTime)ExcelImportRow["Month"]).Month, _day).ToString("dd/MM/yyyy")));
        Log.Message(String.Format("Asignadas {0} promociones para el dia {1}", _dt_accounts.Rows.Count, new DateTime(((DateTime)ExcelImportRow["Month"]).Year, ((DateTime)ExcelImportRow["Month"]).Month, _day).ToString("dd/MM/yyyy")));
      }

      AppendTextBox(Environment.NewLine);

      return true;
    }

    private Boolean DisablePromotion(Int64 IdPromotion)
    {
      StringBuilder _sb;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("  UPDATE  PROMOTIONS                ");
          _sb.AppendLine("     SET   PM_DATE_FINISH   = @pFinishDate ");
          _sb.AppendLine("   WHERE   PM_PROMOTION_ID  = @pPromotionID  ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pFinishDate", SqlDbType.DateTime).Value = DateTime.Now;
            _sql_cmd.Parameters.Add("@pPromotionID", SqlDbType.Int).Value = IdPromotion;

            if (_sql_cmd.ExecuteNonQuery() == 0)
            {
              return false;
            }
          }
          _db_trx.Commit();
        }
      }
      catch
      {
        return false;
      }

      return true;
    }

    private Boolean DeletePromotion(Int64 IdPromotion)
    {
      StringBuilder _sb;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("  DELETE  PROMOTIONS                ");
          _sb.AppendLine("   WHERE   PM_PROMOTION_ID  = @pPromotionID  ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pPromotionID", SqlDbType.BigInt).Value = IdPromotion;

            if (_sql_cmd.ExecuteNonQuery() == 0)
            {
              return false;
            }
          }
          _db_trx.Commit();
        }
      }
      catch
      {
        return false;
      }

      return true;
    }

    private Boolean CashierSessionCloseByIdAndDate(CashierSessionInfo SessionInfo, DateTime CashCloseDate, SqlTransaction Trx)
    {
      SqlCommand _sql_cmd;
      String _sql_txt;

      Cashier.TYPE_CASHIER_SESSION_STATS _close_session_stats;
      SortedDictionary<CurrencyIsoType, Decimal> _collected_amount;
      CurrencyIsoType _iso_type;

      Int32 _user_id;
      String _user_name;
      String _national_currency;

      // Initializations
      _close_session_stats = new Cashier.TYPE_CASHIER_SESSION_STATS();
      _national_currency = CurrencyExchange.GetNationalCurrency();
      _collected_amount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      _iso_type = new CurrencyIsoType();
      _iso_type.IsoCode = _national_currency;
      _iso_type.Type = CurrencyExchangeType.CURRENCY;
      _collected_amount.Add(_iso_type, 0);
      _user_id = -1;
      _user_name = "";

      try
      {
        _sql_txt = "SELECT   GU_USER_ID                         " +
                  "        , GU_USERNAME                        " +
                  "        , CS_STATUS                          " +
                  "        , CS_BALANCE                         " +
                  "   FROM   CASHIER_SESSIONS                   " +
                  "          INNER JOIN GUI_USERS               " +
                  "          ON GU_USER_ID = CS_USER_ID         " +
                  "  WHERE   CS_SESSION_ID = @pCashierSessionId ";

        _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx);
        _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = SessionInfo.CashierSessionId;

        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (!_reader.Read())
          {
            return false;
          }

          if (_reader.GetInt32(2) == (Int32)CASHIER_SESSION_STATUS.CLOSED)
          {
            return true;
          }

          _user_id = _reader.GetInt32(0);
          _user_name = _reader.GetString(1);
          _collected_amount[_iso_type] = _reader.GetDecimal(3);
        }

        // AJQ 08-SEP-2007  
        //  - The balance is read from the database instead of the label
        //  - Update the status to close

        _sql_txt = " UPDATE   CASHIER_SESSIONS                             " +
                   "    SET   CS_CLOSING_DATE      = @pCloseDate           " +
                   "        , CS_STATUS            = @pStatusClosed        " +
                   "        , CS_COLLECTED_AMOUNT  = CS_BALANCE            " +
                   "  WHERE   CS_SESSION_ID        = @pSessionId           " +
                   "    AND   CS_STATUS           IN ( @pStatusOpen        " +
                   "                                 , @pStatusOpenPending " +
                   "                                 , @pStatusPending )   ";

        _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx);

        _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionInfo.CashierSessionId;
        _sql_cmd.Parameters.Add("@pCloseDate", SqlDbType.DateTime).Value = CashCloseDate;
        _sql_cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.CLOSED;
        _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
        _sql_cmd.Parameters.Add("@pStatusOpenPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN_PENDING;
        _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING;

        _sql_cmd.ExecuteNonQuery();

        Cashier.ReadCashierSessionData(Trx, SessionInfo.CashierSessionId, ref _close_session_stats);

        CommonCashierInformation.SetCashierInformation(SessionInfo.CashierSessionId, _user_id, _user_name, SessionInfo.TerminalId, SessionInfo.TerminalName);

      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;

    } // CashierSessionCloseById

    //------------------------------------------------------------------------------
    // PURPOSE : Perform an action to each Promotion of the table AccountPromotions.
    //           Transaction control is from outside.
    //
    //  PARAMS :
    //      - INPUT:
    //            - PromotionAction
    //            - AccountPromotions
    //            - Trx
    //
    //      - OUTPUT :
    //            - TotalBalance
    //
    // RETURNS :
    //      - Number of applied promotions
    private static Int32 Trx_PromotionApplySystemActionByCashierSession(AccountPromotion.PromotionAction PromotionAction,
                                                       DataTable AccountPromotions,
                                                       Boolean InsertMovements,
                                                       CashierSessionInfo CashierSession,
                                                       out MultiPromos.PromoBalance TotalBalance,
                                                       SqlTransaction Trx)
    {
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      Int32 _num_applied;
      MultiPromos.PromoBalance _promo_balance;

      TotalBalance = MultiPromos.PromoBalance.Zero;
      _num_applied = 0;

      try
      {
        _account_movements = null;
        _cashier_movements = null;

        if (InsertMovements)
        {
          _account_movements = new AccountMovementsTable(CashierSession);
          _cashier_movements = new CashierMovementsTable(CashierSession);
        }

        foreach (DataRow _promo in AccountPromotions.Rows)
        {
          try
          {
            if (!PromotionAction(_promo, _account_movements, _cashier_movements, null, out _promo_balance, Trx))
            {
              continue;
            }

            TotalBalance += _promo_balance;
            _num_applied++;

          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        } // foreach

        if (InsertMovements)
        {
          // If not save Account or Cashier movements, rollback. Proceed to the next promotion.
          if (!_account_movements.Save(Trx))
          {
            return 0;
          }

          if (!_cashier_movements.Save(Trx))
          {
            return 0;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _num_applied;

    } // Trx_PromotionApplySystemAction

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current System cashier session if exists.
    //          If not exists, open a new system cashier session.
    //
    //  PARAMS:
    //      - INPUT:
    //          - GU_USER_TYPE UserType
    //          - String TerminalName
    //          - DateTime OpenDate
    //          - SqlTransaction Trx
    //
    //      - OUTPUT:
    //          - Int64 NewCashierSessionId 
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    //
    private static CashierSessionInfo GetOrOpenSystemCashierSessionByOpenDate(GU_USER_TYPE UserType,
                                                        String TerminalName,
                                                        DateTime OpenDate,
                                                        SqlTransaction Trx)
    {
      CashierSessionInfo _session;
      String _sql_txt;
      SqlParameter _sql_parameter;
      Int32 _num_rows_created;
      Int64 _session_id;
      String _name;
      String _str_value;
      Decimal _dec_value;
      Int32 _terminal_id;
      String _terminal_name;
      Int32 _user_id;
      String _user_name;
      Boolean _cashier_sessions_by_terminal;
      Object _obj;

      // Initializations
      _str_value = "";

      try
      {
        _cashier_sessions_by_terminal = Cashier.CashierSessionsByTerminal(UserType);

        _terminal_name = Environment.MachineName;

        _terminal_id = Cashier.ReadTerminalId(Trx, _terminal_name, UserType);

        //If User System doesn't exist, error
        if (!Cashier.GetSystemUser(UserType, Trx, out _user_id, out _user_name))
        {
          Log.Error("GetOrOpenSystemCashierSession. Error getting the System User.");

          return null;
        }

        _sql_txt = " DECLARE @pAccountId AS BIGINT                         " +
                   " SELECT   TOP 1 @pAccountId = CS_SESSION_ID            " +
                   "   FROM   CASHIER_SESSIONS                             " +
                   "  WHERE   CS_USER_ID = @pUserId                        " +
                   "    AND   CS_SESSION_BY_TERMINAL = @pSessionByTerminal " +
                   "    AND   cs_opening_date <= @OpenDate                 " +
                   "    AND   cs_closing_date >= @OpenDate                 " +
                   " IF @pAccountId IS NOT NULL                            " +
                   " BEGIN                                                 " +
                   " UPDATE CASHIER_SESSIONS                               " +
                   "        SET CS_STATUS = @pStatusOpen                   " +
                   "           ,CS_CLOSING_DATE = NULL                     " +
                   "  WHERE   CS_SESSION_ID = @pAccountId                  " +
                   " END                                                 " +
                   " SELECT @pAccountId                                    ";


        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;
          _sql_cmd.Parameters.Add("@pSessionByTerminal", SqlDbType.Bit).Value = _cashier_sessions_by_terminal;
          _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = _terminal_id;
          _sql_cmd.Parameters.Add("@OpenDate", SqlDbType.DateTime).Value = OpenDate;

          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _session_id = (Int64)_obj;

            CommonCashierInformation.SetCashierInformation(_session_id, _user_id, _user_name, _terminal_id, _terminal_name);

            _session = CommonCashierInformation.CashierSessionInfo();
            _session.UserType = UserType;
            _session.IsMobileBank = false;

            return _session;
          }
        }

        // Create new cashier session
        _sql_txt = "INSERT INTO   CASHIER_SESSIONS               " +
                  "             ( CS_NAME                        " +
                  "             , CS_USER_ID                     " +
                  "             , CS_OPENING_DATE                " +
                  "             , CS_CASHIER_ID                  " +
                  "             , CS_TAX_A_PCT                   " +
                  "             , CS_TAX_B_PCT                   " +
                  "             , CS_SESSION_BY_TERMINAL         " +
                  "             )                                " +
                  "      VALUES                                  " +
                  "             ( @pName                         " +             // session name
                  "             , @pUserId                       " +             // session_user_id
                  "             , @pOpenDate                     " +             // session_user_id
                  "             , @pTerminalId                   " +             // session_terminal_id
                  "             , @pTax_a_pct                    " +             //
                  "             , @pTax_b_pct                    " +
                  "             , @pSessionByTerminal            " +
                  "             )                                " +             //
                  "         SET   @pSessionId = SCOPE_IDENTITY() ";


        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          // Format Session Name
          _name = _user_name + " " + OpenDate.ToString("dd/MMM/yyyy HH:mm");
          _name = _name.ToUpper();

          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = _name;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _user_id;
          _sql_cmd.Parameters.Add("@pOpenDate", SqlDbType.DateTime).Value = OpenDate;

          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal_id;

          _str_value = Misc.ReadGeneralParams("Cashier", "Split.A.Tax.Pct", Trx);
          if (_str_value != null)
          {
            if (Decimal.TryParse(Format.DBNumberToLocalNumber(_str_value), out _dec_value))
            {
              _sql_cmd.Parameters.Add("@pTax_a_pct", SqlDbType.Decimal).Value = _dec_value;
            }
          }

          _str_value = Misc.ReadGeneralParams("Cashier", "Split.B.Tax.Pct", Trx);
          if (_str_value != null)
          {
            if (Decimal.TryParse(Format.DBNumberToLocalNumber(_str_value), out _dec_value))
            {
              _sql_cmd.Parameters.Add("@pTax_b_pct", SqlDbType.Decimal).Value = _dec_value;
            }
          }

          _sql_cmd.Parameters.Add("@pSessionByTerminal", SqlDbType.Bit).Value = _cashier_sessions_by_terminal;

          _sql_parameter = _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt);
          _sql_parameter.Direction = ParameterDirection.Output;
          _num_rows_created = _sql_cmd.ExecuteNonQuery();

          if (_num_rows_created != 1)
          {
            Log.Error("GetOrOpenSystemCashierSession. Error creating new session");

            return null;
          }
        }

        // store newly created session id
        _session_id = (Int64)_sql_parameter.Value;

        // Insert Cashier Movement
        CommonCashierInformation.SetCashierInformation(_session_id, _user_id, _user_name, _terminal_id, _terminal_name);

        _session = CommonCashierInformation.CashierSessionInfo();
        _session.UserType = UserType;
        _session.IsMobileBank = false;

        return _session;
      }
      catch (Exception _ex)
      {
        // Logger ...
        Log.Exception(_ex);

        return null;
      }
    }

    private static CashierSessionInfo GetOrOpenCashierSessionByOpenDate(Int32 UserId, String TerminalName, DateTime OpenDate, SqlTransaction Trx)
    {
      CashierSessionInfo _session;
      String _sql_txt;
      SqlParameter _sql_parameter;
      Int32 _num_rows_created;
      Int64 _session_id;
      String _name;
      String _str_value;
      Decimal _dec_value;
      Int32 _terminal_id;
      String _terminal_name;
      String _user_name;
      Boolean _cashier_sessions_by_terminal;
      Object _obj;

      // Initializations
      _str_value = "";

      try
      {
        _cashier_sessions_by_terminal = Cashier.CashierSessionsByTerminal(GU_USER_TYPE.USER);

        _terminal_name = Environment.MachineName;

        _terminal_id = Cashier.ReadTerminalId(Trx, _terminal_name, GU_USER_TYPE.USER);

        //If User System doesn't exist, error
        if (!GetUserName(GU_USER_TYPE.USER, Trx, UserId, out _user_name))
        {
          Log.Error("GetOrOpenSystemCashierSession. Error getting the System User.");

          return null;
        }

        _sql_txt = " DECLARE @pAccountId AS BIGINT                         " +
                   " SELECT   TOP 1 @pAccountId = CS_SESSION_ID            " +
                   "   FROM   CASHIER_SESSIONS                             " +
                   "  WHERE   CS_USER_ID = @pUserId                        " +
                   "    AND   CS_SESSION_BY_TERMINAL = @pSessionByTerminal " +
                   "    AND   cs_opening_date <= @OpenDate                 " +
                   "    AND   cs_closing_date >= @OpenDate                 " +
                   " IF @pAccountId IS NOT NULL                            " +
                   " BEGIN                                                 " +
                   " UPDATE CASHIER_SESSIONS                               " +
                   "        SET CS_STATUS = @pStatusOpen                   " +
                   "           ,CS_CLOSING_DATE = NULL                     " +
                   "  WHERE   CS_SESSION_ID = @pAccountId                  " +
                   " END                                                   " +
                   " SELECT @pAccountId                                    ";


        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _sql_cmd.Parameters.Add("@pSessionByTerminal", SqlDbType.Bit).Value = _cashier_sessions_by_terminal;
          _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = _terminal_id;
          _sql_cmd.Parameters.Add("@OpenDate", SqlDbType.DateTime).Value = OpenDate;

          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _session_id = (Int64)_obj;

            CommonCashierInformation.SetCashierInformation(_session_id, UserId, _user_name, _terminal_id, _terminal_name);

            _session = CommonCashierInformation.CashierSessionInfo();
            _session.UserType = GU_USER_TYPE.USER;
            _session.IsMobileBank = false;

            return _session;
          }
        }

        // Create new cashier session
        _sql_txt = "INSERT INTO   CASHIER_SESSIONS               " +
                  "             ( CS_NAME                        " +
                  "             , CS_USER_ID                     " +
                  "             , CS_OPENING_DATE                " +
                  "             , CS_CASHIER_ID                  " +
                  "             , CS_TAX_A_PCT                   " +
                  "             , CS_TAX_B_PCT                   " +
                  "             , CS_SESSION_BY_TERMINAL         " +
                  "             )                                " +
                  "      VALUES                                  " +
                  "             ( @pName                         " +             // session name
                  "             , @pUserId                       " +             // session_user_id
                  "             , @pOpenDate                     " +             // session_user_id
                  "             , @pTerminalId                   " +             // session_terminal_id
                  "             , @pTax_a_pct                    " +             //
                  "             , @pTax_b_pct                    " +
                  "             , @pSessionByTerminal            " +
                  "             )                                " +             //
                  "         SET   @pSessionId = SCOPE_IDENTITY() ";


        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          // Format Session Name
          _name = _user_name + " " + OpenDate.ToString("dd/MMM/yyyy HH:mm");
          _name = _name.ToUpper();

          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = _name;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _sql_cmd.Parameters.Add("@pOpenDate", SqlDbType.DateTime).Value = OpenDate;

          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal_id;

          _str_value = Misc.ReadGeneralParams("Cashier", "Split.A.Tax.Pct", Trx);
          if (_str_value != null)
          {
            if (Decimal.TryParse(Format.DBNumberToLocalNumber(_str_value), out _dec_value))
            {
              _sql_cmd.Parameters.Add("@pTax_a_pct", SqlDbType.Decimal).Value = _dec_value;
            }
          }

          _str_value = Misc.ReadGeneralParams("Cashier", "Split.B.Tax.Pct", Trx);
          if (_str_value != null)
          {
            if (Decimal.TryParse(Format.DBNumberToLocalNumber(_str_value), out _dec_value))
            {
              _sql_cmd.Parameters.Add("@pTax_b_pct", SqlDbType.Decimal).Value = _dec_value;
            }
          }

          _sql_cmd.Parameters.Add("@pSessionByTerminal", SqlDbType.Bit).Value = _cashier_sessions_by_terminal;

          _sql_parameter = _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt);
          _sql_parameter.Direction = ParameterDirection.Output;
          _num_rows_created = _sql_cmd.ExecuteNonQuery();

          if (_num_rows_created != 1)
          {
            Log.Error("GetOrOpenSystemCashierSession. Error creating new session");

            return null;
          }
        }

        // store newly created session id
        _session_id = (Int64)_sql_parameter.Value;

        // Insert Cashier Movement
        CommonCashierInformation.SetCashierInformation(_session_id, UserId, _user_name, _terminal_id, _terminal_name);

        _session = CommonCashierInformation.CashierSessionInfo();
        _session.UserType = GU_USER_TYPE.USER;
        _session.IsMobileBank = false;

        return _session;
      }
      catch (Exception _ex)
      {
        // Logger ...
        Log.Exception(_ex);

        return null;
      }
    }

    public static Boolean GetUserName(GU_USER_TYPE UserType, SqlTransaction SqlTrx, Int64 UserId, out String UserName)
    {
      String _sql_txt;

      UserName = "";

      try
      {
        _sql_txt = " SELECT   GU_USERNAME " +
                   "   FROM   GUI_USERS " +
                   "  WHERE   GU_USER_TYPE = @pUserType " +
                   "    AND   GU_USER_ID = @pUserId ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserType", SqlDbType.Int).Value = UserType;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            // Return the first one found. Must be ONLY ONE.
            if (_reader.Read())
            {
              UserName = _reader.GetString(0);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetSystemUser

    private Boolean PromotionChangesDatesAndCreditToExhausted(DataTable AccountPromotions, SqlTransaction SqlTrx)
    {
      foreach (DataRow _promo in AccountPromotions.Rows)
      {
        if (!AccountPromotionsChangeDateAndCreditToExhausted(_promo, null, SqlTrx))
        {
          AppendTextBox(string.Format("{0}\tError: al modificar las fechas de la promoci�n", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: al modificar las fechas de la promoci�n");
          return false;
        }
        if (!CashierVouchersChangeDate(_promo, SqlTrx))
        {
          AppendTextBox(string.Format("{0}\tError: al modificar las fechas del voucher", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: al modificar las fechas del voucher");
          return false;
        }
        if (!AccountPromotionChangeDate(_promo, SqlTrx))
        {
          AppendTextBox(string.Format("{0}\tError: al modificar las fechas del account operations", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: al modificar las fechas del account operations");
          return false;
        }
        if (!CashierMovementsChangeDate(_promo, SqlTrx))
        {
          AppendTextBox(string.Format("{0}\tError: al modificar las fechas del cashier movements", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: al modificar las fechas del cashier movements");
          return false;
        }
        if (!AccountMovementsChangeDate(_promo, SqlTrx))
        {
          AppendTextBox(string.Format("{0}\tError: al modificar las fechas del account movements", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: al modificar las fechas del account movements");
          return false;
        }
      }

      return true;
    } // PromotionChangesDatesAndCreditToExhausted

    private static Boolean GetUserIdByName(String UserName, out Int32 UserId)
    {
      StringBuilder _sb;
      Object _obj;

      UserId = -1;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        try
        {
          _sb = new StringBuilder();

          _sb.AppendLine(" IF EXISTS   (SELECT   * ");
          _sb.AppendLine("                FROM   GUI_USERS ");
          _sb.AppendLine("               WHERE   GU_USERNAME = @UserName) ");
          _sb.AppendLine(" BEGIN ");
          _sb.AppendLine("   DECLARE   @CashierSessions AS BIGINT  ");
          _sb.AppendLine("    SELECT   @CashierSessions = COUNT(*) ");
          _sb.AppendLine("      FROM   GUI_USERS ");
          _sb.AppendLine("             INNER JOIN CASHIER_SESSIONS ON CS_USER_ID = GU_USER_ID ");
          _sb.AppendLine("     WHERE   GU_USERNAME = @UserName ");
          _sb.AppendLine("       AND   CS_STATUS           IN ( @pStatusOpen        ");
          _sb.AppendLine("                                    , @pStatusOpenPending ");
          _sb.AppendLine("                                    , @pStatusPending )   ");

          _sb.AppendLine("       IF   (@CashierSessions = 0) ");
          _sb.AppendLine("       BEGIN ");
          //_sb.AppendLine("          UPDATE   GUI_USERS ");
          //_sb.AppendLine("             SET   GU_NOT_VALID_BEFORE = CASE WHEN GU_NOT_VALID_BEFORE < @pCreationDate ");
          //_sb.AppendLine("                   THEN GU_NOT_VALID_BEFORE ELSE @pCreationDate END ");
          //_sb.AppendLine("           WHERE   GU_USERNAME = @UserName ");

          //_sb.AppendLine("          UPDATE   GUI_AUDIT ");
          //_sb.AppendLine("             SET   GA_DATETIME = CASE WHEN GA_DATETIME < @pCreationDate ");
          //_sb.AppendLine("                   THEN GA_DATETIME ELSE @pCreationDate END ");
          //_sb.AppendLine("           WHERE   GA_AUDIT_ID = ( ");
          //_sb.AppendLine("                                    SELECT   TOP 1 GA_AUDIT_ID ");
          //_sb.AppendLine("                                      FROM   GUI_AUDIT ");
          //_sb.AppendLine("                                     WHERE   GA_AUDIT_CODE = 7 ");
          //_sb.AppendLine("                                       AND   GA_NLS_PARAM02 = @UserName ");
          //_sb.AppendLine("                                       AND   GA_GUI_ID = 14 ");
          //_sb.AppendLine("                                  ORDER BY   GA_AUDIT_ID ASC ");
          //_sb.AppendLine("                                  ) ");

          _sb.AppendLine("          SELECT   GU_USER_ID ");
          _sb.AppendLine("            FROM   GUI_USERS ");
          _sb.AppendLine("           WHERE   GU_USERNAME = @UserName ");
          _sb.AppendLine("       END ");
          _sb.AppendLine(" END ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
            _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
            _sql_cmd.Parameters.Add("@pStatusOpenPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN_PENDING;
            _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING;
            //_sql_cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = CreationDate;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj == null || _obj == DBNull.Value)
            {
              return false;
            }

            UserId = (Int32)_obj;
          }
        }
        catch
        {
          return false;
        }

        return true;
      }
    }

    private static Boolean CreateUNRPromotion(String PromotionName, DateTime PromotionStartDate, DateTime PromotionFinishDate, out Int64 PromotionId)
    {
      StringBuilder _sb;
      SqlParameter _param;

      PromotionId = 0;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        try
        {
          _sb = new StringBuilder();

          _sb.AppendLine("INSERT INTO   promotions ");
          _sb.AppendLine("            ( pm_name ");
          _sb.AppendLine("            , pm_enabled ");
          _sb.AppendLine("            , pm_type ");
          _sb.AppendLine("            , pm_date_start ");
          _sb.AppendLine("            , pm_date_finish ");
          _sb.AppendLine("            , pm_schedule_weekday ");
          _sb.AppendLine("            , pm_schedule1_time_from ");
          _sb.AppendLine("            , pm_schedule1_time_to ");
          _sb.AppendLine("            , pm_schedule2_enabled ");
          _sb.AppendLine("            , pm_schedule2_time_from ");
          _sb.AppendLine("            , pm_schedule2_time_to ");
          _sb.AppendLine("            , pm_gender_filter ");
          _sb.AppendLine("            , pm_birthday_filter ");
          _sb.AppendLine("            , pm_expiration_type ");
          _sb.AppendLine("            , pm_expiration_value ");
          _sb.AppendLine("            , pm_min_cash_in ");
          _sb.AppendLine("            , pm_min_cash_in_reward ");
          _sb.AppendLine("            , pm_cash_in ");
          _sb.AppendLine("            , pm_cash_in_reward ");
          _sb.AppendLine("            , pm_won_lock ");
          _sb.AppendLine("            , pm_num_tokens ");
          _sb.AppendLine("            , pm_token_name ");
          _sb.AppendLine("            , pm_token_reward ");
          _sb.AppendLine("            , pm_daily_limit ");
          _sb.AppendLine("            , pm_monthly_limit ");
          _sb.AppendLine("            , pm_level_filter ");
          _sb.AppendLine("            , pm_permission ");
          _sb.AppendLine("            , pm_freq_filter_last_days ");
          _sb.AppendLine("            , pm_freq_filter_min_days ");
          _sb.AppendLine("            , pm_freq_filter_min_cash_in ");
          _sb.AppendLine("            , pm_min_spent ");
          _sb.AppendLine("            , pm_min_spent_reward ");
          _sb.AppendLine("            , pm_spent ");
          _sb.AppendLine("            , pm_spent_reward ");
          _sb.AppendLine("            , pm_provider_list ");
          _sb.AppendLine("            , pm_offer_list ");
          _sb.AppendLine("            , pm_global_daily_limit ");
          _sb.AppendLine("            , pm_global_monthly_limit ");
          _sb.AppendLine("            , pm_small_resource_id ");
          _sb.AppendLine("            , pm_large_resource_id ");
          _sb.AppendLine("            , pm_min_played ");
          _sb.AppendLine("            , pm_min_played_reward ");
          _sb.AppendLine("            , pm_played ");
          _sb.AppendLine("            , pm_played_reward ");
          _sb.AppendLine("            , pm_play_restricted_to_provider_list ");
          _sb.AppendLine("            , pm_last_executed ");
          _sb.AppendLine("            , pm_next_execution ");
          _sb.AppendLine("            , pm_global_limit ");
          _sb.AppendLine("            , pm_credit_type ");
          _sb.AppendLine("            , pm_category_id ");
          _sb.AppendLine("            , pm_ticket_footer ");
          _sb.AppendLine("            , pm_visible_on_promobox ");
          _sb.AppendLine("            , pm_expiration_limit) ");
          _sb.AppendLine("VALUES ");
          _sb.AppendLine("            ( @PromoName ");
          _sb.AppendLine("            , 1 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , @PromoStartDate ");
          _sb.AppendLine("            , @PromoFinishDate ");
          _sb.AppendLine("            , 127 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 1 ");
          _sb.AppendLine("            , 21 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , '' ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0.00 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , NULL ");
          _sb.AppendLine("            , 1 ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , '' ");
          _sb.AppendLine("            , 0 ");
          _sb.AppendLine("            , NULL) ");
          _sb.AppendLine("SET @pUniqueId = SCOPE_IDENTITY()");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@PromoName", SqlDbType.NVarChar).Value = PromotionName;
            _sql_cmd.Parameters.Add("@PromoStartDate", SqlDbType.DateTime).Value = PromotionStartDate;
            _sql_cmd.Parameters.Add("@PromoFinishDate", SqlDbType.DateTime).Value = PromotionFinishDate;

            _param = _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
            _param.Direction = ParameterDirection.Output;

            if (_sql_cmd.ExecuteNonQuery() > 0)
            {
              PromotionId = (Int64)_param.Value;
            }
          }
        }
        catch
        {
          return false;
        }

        _db_trx.Commit();
      }
      return true;
    }

    private static Boolean DeleteUNRPromotionsByUserName(Int32 UserId)
    {
      StringBuilder _sb;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        try
        {
          _sb = new StringBuilder();

          _sb.AppendLine(" CREATE TABLE #ACCOUNT_OPERATION_IDS ");
          _sb.AppendLine(" ( ");
          _sb.AppendLine("    AO_ACCOUNT_ID BIGINT, ");
          _sb.AppendLine("    AO_OPERATION_ID BIGINT ");
          _sb.AppendLine(" ) ");

          // Buscar las operaciones de las sesiones de caja del usuario
          _sb.AppendLine(" INSERT INTO   #ACCOUNT_OPERATION_IDS ");
          _sb.AppendLine("      SELECT   AO_ACCOUNT_ID, AO_OPERATION_ID ");
          _sb.AppendLine("        FROM   ACCOUNT_OPERATIONS ");
          _sb.AppendLine("       WHERE   AO_CASHIER_SESSION_ID IN ( SELECT   CS_SESSION_ID ");
          _sb.AppendLine("                                            FROM   CASHIER_SESSIONS ");
          _sb.AppendLine("                                           WHERE   CS_USER_ID = @pIdUser) ");

          // Borrar los movimientos de caja
          _sb.AppendLine("      DELETE ");
          _sb.AppendLine("        FROM   CASHIER_MOVEMENTS ");
          _sb.AppendLine("       WHERE   CM_USER_ID = @pIdUser ");

          // Borrar los movimientos de cuentas
          _sb.AppendLine("      DELETE ");
          _sb.AppendLine("        FROM   ACCOUNT_MOVEMENTS ");
          _sb.AppendLine("       WHERE   AM_OPERATION_ID IN ( SELECT   AO_OPERATION_ID ");
          _sb.AppendLine("                                      FROM   #ACCOUNT_OPERATION_IDS) ");

          // Borrar los vouchers
          _sb.AppendLine("      DELETE ");
          _sb.AppendLine("        FROM   CASHIER_VOUCHERS ");
          _sb.AppendLine("       WHERE   CV_OPERATION_ID IN ( SELECT   AO_OPERATION_ID ");
          _sb.AppendLine("                                      FROM   #ACCOUNT_OPERATION_IDS) ");

          // Borrar las promociones
          _sb.AppendLine("      DELETE ");
          _sb.AppendLine("        FROM   ACCOUNT_PROMOTIONS ");
          _sb.AppendLine("       WHERE   ACP_OPERATION_ID IN ( SELECT   AO_OPERATION_ID ");
          _sb.AppendLine("                                       FROM   #ACCOUNT_OPERATION_IDS) ");

          // Borrar las operaciones de cuetas
          _sb.AppendLine("      DELETE ");
          _sb.AppendLine("        FROM   ACCOUNT_OPERATIONS ");
          _sb.AppendLine("       WHERE   AO_OPERATION_ID IN ( SELECT   AO_OPERATION_ID ");
          _sb.AppendLine("                                      FROM   #ACCOUNT_OPERATION_IDS) ");

          // Borrar las cuentas anonimas
          _sb.AppendLine("      DELETE ");
          _sb.AppendLine("        FROM ACCOUNTS ");
          _sb.AppendLine("       WHERE AC_ACCOUNT_ID IN ( SELECT   AO_ACCOUNT_ID ");
          _sb.AppendLine("                                  FROM  #ACCOUNT_OPERATION_IDS) ");

          // Borrar las sesiones de caja
          _sb.AppendLine("      DELETE ");
          _sb.AppendLine("        FROM   CASHIER_SESSIONS ");
          _sb.AppendLine("       WHERE   CS_USER_ID = @pIdUser ");

          _sb.AppendLine("  DROP TABLE   #ACCOUNT_OPERATION_IDS ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pIdUser", SqlDbType.BigInt).Value = UserId;

            _sql_cmd.ExecuteNonQuery();
          }
        }
        catch
        {
          return false;
        }

        _db_trx.Commit();
      }
      return true;
    }

    #endregion private methods

    #region public methods

    public Boolean ImportPromotionsUNRFromExcel(String FilePath, String UserName, DataTable GeneralParams, RichTextBox TxtLog, BackgroundWorker Worker, DoWorkEventArgs e)
    {
      DataTable _dt_promotions_unr_excel;
      Int32 _id_user;
      Int32 _row_import_processed;
      AccountPromotion _promotion;
      String _promotion_name;
      DateTime _promotion_start_date;
      DateTime _promotion_finish_date;
      Int64 _promotion_id;

      m_txt_log = TxtLog;
      m_file_path = FilePath;
      m_worker = Worker;
      m_site_id = 0;

      // Validate if user exist and all cashier sessions are closed
      if (!GetUserIdByName(UserName, out _id_user))
      {
        AppendTextBox(string.Format("{0}*** El usuario no existe o tiene sesiones de caja abiertas ***", Environment.NewLine), System.Drawing.Color.Red);
        Log.Error("*** El usuario no existe o tiene sesiones de caja abiertas ***");
        return false;
      }

      // Validate Excel Data
      if (!ValidateUNRPromotionsExcel(FilePath, TxtLog, out _dt_promotions_unr_excel))
      {
        return false;
      }

      m_worker.ReportProgress(100, 1);

      if (_dt_promotions_unr_excel.Rows.Count == 0)
      {
        AppendTextBox(string.Format("{0}{0}Error: No hay lineas validas en el excel para aplicar las promociones.", Environment.NewLine), System.Drawing.Color.Red);
        Log.Error("Error: No hay lineas validas en el excel para aplicar las promociones.");
        m_worker.ReportProgress(100, 100);
        return false;
      }

      // Create promotion
      _promotion_name = "Importaci�n - Premio UNR";
      _promotion_start_date = ((DateTime)(_dt_promotions_unr_excel.Select("Month = MIN(Month)")[0]["Month"]));
      _promotion_finish_date = DateTime.Now;

      if (!CreateUNRPromotion(_promotion_name, _promotion_start_date, _promotion_finish_date, out _promotion_id))
      {
        AppendTextBox(string.Format("{0}{0}Error: CreateUNRPromotion", Environment.NewLine), System.Drawing.Color.Red);
        Log.Error("Error: CreateUNRPromotion");
        m_worker.ReportProgress(100, 100);
        return false;
      }

      // Read promotion
      _promotion = new AccountPromotion();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Promotion.ReadPromotionData(_db_trx.SqlTransaction, _promotion_id, _promotion))
        {
          AppendTextBox(string.Format("{0}Error: ReadPromotionData --> Promoci�n no encontrada.", Environment.NewLine), System.Drawing.Color.Red);
          Log.Error("Error: ReadPromotionData --> Promoci�n no encontrada.");
          return false;
        }
      }

      AppendTextBox(string.Format("{0}{0}Promoci�n cargada: {1}", Environment.NewLine, _promotion.name));
      Log.Message(string.Format("Promoci�n cargada: {0}", _promotion.name));

      m_dt_accounts = new DataTable();

      DataTable _promotions_to_apply;

      AppendTextBox(string.Format("{0}{0}*** Iniciando importaci�n ***", Environment.NewLine), System.Drawing.Color.Blue);
      Log.Message("*** Iniciando importaci�n ***");

      _row_import_processed = 0;

      foreach (DataRow _row_import in _dt_promotions_unr_excel.Rows)
      {
        _row_import_processed++;
        m_dt_accounts.Clear();

        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (!AssignAmountToAccounts(_row_import, out _promotions_to_apply, _db_trx.SqlTransaction))
          {
            _promotions_to_apply.Clear();
            continue;
          }

          if (_promotions_to_apply.Rows.Count == 0 || !InsertPromotions(_promotions_to_apply, _id_user, _promotion, GeneralParams, _db_trx.SqlTransaction))
          {
            AppendTextBox(string.Format("{0}Error en la importaci�n del m�s {1} de {2}", Environment.NewLine, ((DateTime)_row_import["Month"]).Month, ((DateTime)_row_import["Month"]).Year), System.Drawing.Color.Red);
            Log.Error(string.Format("Error en la importaci�n del m�s {0} de {1}", ((DateTime)_row_import["Month"]).Month, ((DateTime)_row_import["Month"]).Year));
            m_worker.ReportProgress(100, 100 / _dt_promotions_unr_excel.Rows.Count * _row_import_processed);

            _db_trx.Rollback();

            continue;
          }

          m_worker.ReportProgress(100, (100 / _dt_promotions_unr_excel.Rows.Count) * _row_import_processed);

          _db_trx.Commit();
        }
      }

      // Disabled promotion
      //if (!DisablePromotion(_promotion.id))
      //{
      //  AppendTextBox(string.Format("{0}\tError: DisablePromotion", Environment.NewLine), System.Drawing.Color.Red);
      //  Log.Error("Error: DisablePromotion");
      //  return false;
      //}

      // Delete promotion
      if (!DeletePromotion(_promotion.id))
      {
        AppendTextBox(string.Format("{0}\tError: DeletePromotion", Environment.NewLine), System.Drawing.Color.Red);
        Log.Error("Error: DeletePromotion");
        return false;
      }

      m_worker.ReportProgress(100, 100);

      AppendTextBox(string.Format("{0}{0}*** Importaci�n finalizada correctamente ***{0}", Environment.NewLine), System.Drawing.Color.Green);
      Log.Message("*** Importaci�n finalizada correctamente ***");

      return true;
    }

    public Boolean ValidateUNRPromotionsExcel(String FilePath, RichTextBox TxtLog, out DataTable PromotionsUNRExcel)
    {
      String _execPath;
      DataRow _row;
      object[,] _range_values;
      int _num_rows;
      int _num_cols;
      String _downAddress;

      m_txt_log = TxtLog;
      m_file_path = FilePath;
      m_site_id = 0;
      PromotionsUNRExcel = new System.Data.DataTable("PromotionsUNR");

      // Validate if Excel exist
      if (!File.Exists(FilePath))
      {
        AppendTextBox(string.Format("{0}*** Fichero no encontrado ***", Environment.NewLine), System.Drawing.Color.Red);
        Log.Error("*** Fichero no encontrado ***");
        return false;
      }

      // BD is multisite/ site
      m_is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", false);

      if (m_is_center)
      {
        AppendTextBox(string.Format("{0} La sala es multisite.", Environment.NewLine), System.Drawing.Color.Red);
        Log.Error("La sala es multisite.");
        return false;
      }

      // Validate if general param NR1asUNR is enabled
      if (GeneralParam.GetInt32("Cashier", "Promotion.NR1AsUNR", 0) <= 0)
      {
        AppendTextBox(string.Format("{0}El par�metro general: 'Cashier - Promotion.NR1AsNR' no esta activo.", Environment.NewLine), System.Drawing.Color.Red);
        Log.Error("El par�metro general: 'Cashier - Promotion.NR1AsNR' no esta activo.");
        return false;
      }

      Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out m_site_id);

      AppendTextBox(string.Format("{0}*** Procesando Excel ***", Environment.NewLine), System.Drawing.Color.Blue);
      Log.Message("*** Procesando Excel ***");

      SpreadsheetGear.IWorkbook _excel_book = null;
      SpreadsheetGear.IRange _excel_range = null;

      CreatePromotionsUNRTableFromExcel(PromotionsUNRExcel);

      System.Globalization.CultureInfo _old_ci = System.Threading.Thread.CurrentThread.CurrentCulture;
      System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");

      try
      {
        _execPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
        _excel_book = Factory.GetWorkbook(FilePath);

        foreach (IWorksheet _excel_sheet in _excel_book.Worksheets)
        {
          AppendTextBox(string.Format("{0}{0}Excel {1}", Environment.NewLine, _excel_sheet.Name));
          Log.Message(string.Format("Excel {0}", _excel_sheet.Name));

          _excel_range = _excel_sheet.UsedRange.CurrentRegion;

          // get the address of the bottom, right cell
          _downAddress = _excel_range.GetAddress(false,
                                                 false,
                                                 ReferenceStyle.A1,
                                                 false,
                                                 null);

          // Get the range, then values from a1
          _excel_range = _excel_sheet.Range[_downAddress];
          _range_values = (object[,])_excel_range.Value;
          _num_rows = _range_values.GetLength(0) - 1;
          _num_cols = _range_values.GetLength(1) - 1;

          ColummMatches();

          for (int _idx_row = FIRST_DATA_ROW; _idx_row <= _num_rows; _idx_row++)
          {
            AppendTextBox(string.Format("{0}\tL�nea {1} -->", Environment.NewLine, _idx_row));
            Log.Message(string.Format("\tL�nea {0} -->", _idx_row));
            try
            {
              _row = PromotionsUNRExcel.NewRow();

              for (int _idx_col = 0; _idx_col <= _num_cols; _idx_col++)
              {
                String _col_name;
                Object _cell_value;
                if (_range_values[COLUMN_NAMES_ROW, _idx_col] != null)
                {
                  _col_name = GetPromotionsUNRColName((_range_values[COLUMN_NAMES_ROW, _idx_col]).ToString()); // Excel.ColumnName --> Accounts.ColumnName
                }
                else
                {
                  _col_name = String.Empty;
                }

                if (String.IsNullOrEmpty(_col_name))
                {
                  continue;
                }

                _cell_value = (_range_values[_idx_row, _idx_col]);

                String _warning_message;
                Boolean _warning;

                _row[_col_name] = CheckCell(_cell_value, PromotionsUNRExcel.Columns[_col_name], out _warning, out _warning_message);

              }

              if (CheckRow(ref _row, PromotionsUNRExcel.Columns, _idx_row + 1))
              {
                _row["Index"] = _idx_row;
                PromotionsUNRExcel.Rows.Add(_row);
                AppendTextBox(string.Format("\tCorrecta"), System.Drawing.Color.Green);
                Log.Message("\tCorrecta");
              }
              else
              {
                continue;
              }
            }
            catch (Exception ex)   // row exception
            {
              AppendTextBox(string.Format("{0}{1}", Environment.NewLine, ex.Message), System.Drawing.Color.Red);
              Log.Error(ex.Message);
            }
          }
        }
      }
      catch (Exception)
      {
        if (_excel_range != null)
        {
          _excel_range = null;
        }
        if (_excel_book != null)
        {
          _excel_book.Close();
          _excel_book = null;
        }
      }
      finally
      {
        if (_excel_range != null)
        {
          _excel_range = null;
        }
        if (_excel_book != null)
        {
          _excel_book.Close();
          _excel_book = null;
        }
      }

      System.Threading.Thread.CurrentThread.CurrentCulture = _old_ci;

      AppendTextBox(string.Format("{0}{0}*** Excel Procesado ***{0}", Environment.NewLine), System.Drawing.Color.Blue);
      Log.Message("*** Excel Procesado ***");

      return true;
    }

    public static Boolean DeleteUNRPromotionsByUserName(String UserName)
    {
      Int32 _id_user;

      if (!GetUserIdByName(UserName, out _id_user))
      {
        Log.Message("*** El usuario no existe o tiene sesiones de caja abiertas ***");
        return false;
      }
      if (!DeleteUNRPromotionsByUserName(_id_user))
      {
        Log.Error("*** Se ha producido un error en el borrado de los registros para el usuario: " + UserName + " ***");
        return false;
      }
      Log.Message("*** Se ha borrado correctamente los registros para el usuario:" + UserName + " ***");
      return true;
    }

    public delegate void AppendTextBoxDelegate(string Value, System.Drawing.Color ColorText);

    public void AppendTextBox(string Value)
    {
      AppendTextBox(Value, System.Drawing.Color.Black);
    }

    public void AppendTextBox(string Value, System.Drawing.Color ColorText)
    {
      if (m_txt_log.InvokeRequired)
      {
        AppendTextBoxDelegate myDelegate;
        myDelegate = new AppendTextBoxDelegate(AppendTextBox);
        m_txt_log.Invoke(myDelegate, new object[] { Value, ColorText });
      }
      else
      {
        m_txt_log.SelectionStart = m_txt_log.TextLength;
        m_txt_log.SelectionLength = 0;

        m_txt_log.SelectionColor = ColorText;
        m_txt_log.AppendText(Value);
        m_txt_log.SelectionColor = m_txt_log.ForeColor;
        m_txt_log.SelectionStart = m_txt_log.Text.Length;
        m_txt_log.ScrollToCaret();
      }
    }

    #endregion public methods

    #region events

    #endregion events
  }
}
