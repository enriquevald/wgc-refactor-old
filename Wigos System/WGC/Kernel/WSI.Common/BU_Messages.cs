//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BU_Messages.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 13-JUL-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JUL-2010 RCI    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;

namespace WSI.Common.BatchUpdate
{
  public static class Messages
  {

    #region Constants

    private const Int16 COLUMN_MESSAGE_ID = 0;
    private const Int16 COLUMN_TERMINAL_ID = 1;
    private const Int16 COLUMN_SESSION_ID = 2;
    private const Int16 COLUMN_SEQUENCE_ID = 3;
    private const Int16 COLUMN_TOWARDS_TO_TERMINAL = 4;
    private const Int16 COLUMN_MESSAGE = 5;

    #endregion // Constants

    #region Attributes

    private static ReaderWriterLock m_rw_lock = new ReaderWriterLock();
    private static DataTable m_dt_messages = null;
    private static SqlDataAdapter m_sql_adap = null;
    private static DataTable m_dt_messages_copy = null;

    private static Thread m_thread;

    private static String m_protocol;

    #endregion // Attributes

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init attributes of the static class:
    //           - String for the SQL Command to insert messages (depending on Protocol parameter).
    //           - DataTable for the Messages Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String Protocol
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init(String Protocol)
    {
      SqlCommand _sql_command;
      String _sql_insert;
      SqlParameter _parameter;

      m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

      try
      {
        if (m_dt_messages != null)
        {
          // already init... return!
          Log.Warning("Init: BU_Messages already initialized.");
          
          return;
        }

        if (Protocol == "WC2")
        {
          _sql_insert = "INSERT INTO WC2_MESSAGES ( W2M_TERMINAL_ID " +
                                                " , W2M_SESSION_ID " +
                                                " , W2M_SEQUENCE_ID " +
                                                " , W2M_TOWARDS_TO_TERMINAL " +
                                                " , W2M_MESSAGE ) " +
                                         " VALUES ( @pTerminalId " +
                                                " , @pSessionId " +
                                                " , @pSequenceId " +
                                                " , @pTowards " +
                                                " , @pMessage ) ";
        }
        else if (Protocol == "WCP")
        {
          _sql_insert = "INSERT INTO WCP_MESSAGES ( WM_TERMINAL_ID " +
                                                " , WM_SESSION_ID " +
                                                " , WM_SEQUENCE_ID " +
                                                " , WM_TOWARDS_TO_TERMINAL " +
                                                " , WM_MESSAGE ) " +
                                         " VALUES ( @pTerminalId " +
                                                " , @pSessionId " +
                                                " , @pSequenceId " +
                                                " , @pTowardsToTerminal " +
                                                " , @pMessage ) ";
        }
        else
        {
          throw new Exception("BU_Messages.Init: Protocol " + Protocol + " not supported.");
        }

        _sql_insert += " SET   @pMessageId = SCOPE_IDENTITY() ";

        // Create DataTable Columns

        m_dt_messages = new DataTable("MESSAGES");

        m_dt_messages.Columns.Add("MESSAGE_ID", Type.GetType("System.Int64"));
        m_dt_messages.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
        m_dt_messages.Columns.Add("SESSION_ID", Type.GetType("System.Int64")).Unique = true;
        m_dt_messages.Columns.Add("SEQUENCE_ID", Type.GetType("System.Int64"));
        m_dt_messages.Columns.Add("TOWARDS_TO_TERMINAL", Type.GetType("System.Boolean"));
        m_dt_messages.Columns.Add("MESSAGE", Type.GetType("System.String"));

        m_dt_messages.Columns[COLUMN_MESSAGE_ID].AutoIncrement = true;
        m_dt_messages.Columns[COLUMN_MESSAGE_ID].AutoIncrementSeed = -1;
        m_dt_messages.Columns[COLUMN_MESSAGE_ID].AutoIncrementStep = -1;

        m_dt_messages_copy = m_dt_messages.Clone();

        // Create Insert Command and SqlDataAdapter.

        _sql_command = new SqlCommand(_sql_insert);
        _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
        _sql_command.Parameters.Add("@pSessionId", SqlDbType.BigInt).SourceColumn = "SESSION_ID";
        _sql_command.Parameters.Add("@pSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
        _sql_command.Parameters.Add("@pTowardsToTerminal", SqlDbType.Bit).SourceColumn = "TOWARDS_TO_TERMINAL";
        _sql_command.Parameters.Add("@pMessage", SqlDbType.NVarChar).SourceColumn = "MESSAGE";
        
        _parameter = _sql_command.Parameters.Add("@pMessageId", SqlDbType.BigInt);
        _parameter.SourceColumn = "MESSAGE_ID";
        _parameter.Direction = ParameterDirection.Output;
        
        m_sql_adap = new SqlDataAdapter();
        m_sql_adap.SelectCommand = null;
        m_sql_adap.InsertCommand = _sql_command;
        m_sql_adap.UpdateCommand = null;
        m_sql_adap.DeleteCommand = null;
        m_sql_adap.ContinueUpdateOnError = true;

        m_sql_adap.UpdateBatchSize = 500;
        m_sql_adap.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

        // Don't need it! But leave it here to see example of use.
        //m_sql_adap.RowUpdated += new SqlRowUpdatedEventHandler(adap_MessageRowUpdated);

        m_protocol = Protocol;
        
        m_thread = new Thread(new ThreadStart (Messages.Thread));
        m_thread.Name = "Messages.Thread";
        m_thread.Start();
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Add a message in the DataTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 TerminalId
    //          - Int64 SessionId
    //          - Int64 SequenceId
    //          - Boolean TowardsToTerminal
    //          - String Message
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If message has been added correctly.
    //
    public static Boolean AddMessage(Int32 TerminalId, Int64 SessionId, Int64 SequenceId, Boolean TowardsToTerminal, String Message)
    {
      DataRow _message;

      m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

      try
      {
        if (m_dt_messages == null)
        {
          Log.Error("Not initialized. BU_Messages.AddMessage Terminal: " + TerminalId + ", Session: " + SessionId + ".");

          return false;
        }

        // Insert TerminalId/SessionId
        _message = m_dt_messages.NewRow();
        _message[COLUMN_TERMINAL_ID] = TerminalId;
        _message[COLUMN_SESSION_ID] = SessionId;
        _message[COLUMN_SEQUENCE_ID] = SequenceId;
        _message[COLUMN_TOWARDS_TO_TERMINAL] = TowardsToTerminal;
        _message[COLUMN_MESSAGE] = Message;
        m_dt_messages.Rows.Add(_message);
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: BU_Messages.AddMessage Terminal: " + TerminalId + ", Session: " + SessionId + ".");

        Log.Exception(_ex);
        
        return false;
      }
      finally 
      {
        m_rw_lock.ReleaseWriterLock();
      }

      return true;
    } // AddMessage

    //------------------------------------------------------------------------------
    // PURPOSE : Update DB according to the Cache in memory (DataTable).
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If Cache has been writen to DB correctly.
    //
    public static Boolean Update(SqlTransaction Trx)
    {
      DataRow[] _dr_added;
      Boolean _error;

      m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

      try
      {
        if (m_dt_messages == null)
        {
          Log.Error("Update: BU_Messages not initialized. Call Init first!");
          return false;
        }

        // If no rows to update, return ok.
        if (m_dt_messages.Rows.Count == 0)
        {
          return true;
        }

        _dr_added = m_dt_messages.Select("", "", DataViewRowState.Added);
        // Really, no rows to insert... Return ok.
        if (_dr_added.Length <= 0)
        {
          return true;        
        }

        m_dt_messages_copy.Clear();
        foreach (DataRow _r in _dr_added)
        {
          m_dt_messages_copy.ImportRow(_r);
        }

        // Mark as not inserted
        m_dt_messages.AcceptChanges();
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }

      _error = true;
      try
      {
        m_sql_adap.InsertCommand.Connection = Trx.Connection;
        m_sql_adap.InsertCommand.Transaction = Trx;

        // Insert new rows to DB -- Time consuming
        if (m_sql_adap.Update(m_dt_messages_copy) == m_dt_messages_copy.Rows.Count)
        {
          _error = false;
        }
        else
        {
          DataRow[] _rows_with_errors;

          _rows_with_errors = m_dt_messages_copy.GetErrors();

          Log.Warning("------------- BEGIN " + m_protocol + " MESSAGE NOT INSERTED -------------");
          foreach (DataRow _row in _rows_with_errors)
          {
            Log.Warning("Terminal: " + _row[COLUMN_TERMINAL_ID].ToString() +
                      ", Session: " + _row[COLUMN_SESSION_ID].ToString() +
                      ", Sequence: " + _row[COLUMN_SEQUENCE_ID].ToString() +
                      ", TowardToTerminal: " + _row[COLUMN_TOWARDS_TO_TERMINAL].ToString() +
                      ", Message: " + _row[COLUMN_MESSAGE].ToString() +
                      ". Details: " + _row.RowError);
          }
          Log.Warning("------------- END   " + m_protocol + " MESSAGE NOT INSERTED -------------");
        }
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: BU_Messages.Update.");
        Log.Exception(_ex);
      }

      // Don't care about not inserted rows

      return !_error;
    } // Update

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for Insert to DB every second.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void Thread()
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;

      _sql_conn = null;
      _sql_trx = null;

      while (true)
      {
        try
        {
          try
          {
            System.Threading.Thread.Sleep(1000);

            if (_sql_conn == null)
            {
              _sql_conn = WGDB.Connection();
            }

            _sql_trx = _sql_conn.BeginTransaction();

            Messages.Update(_sql_trx);

            _sql_trx.Commit();
          }
          catch
          {
            if (_sql_trx != null)
            {
              if (_sql_trx.Connection != null)
              {
                _sql_trx.Rollback();
              }
              _sql_trx.Dispose();
              _sql_trx = null;
            }

            if (_sql_conn != null)
            {
              _sql_conn.Close();
              _sql_conn.Dispose();
              _sql_conn = null;
            }
          }

        }
        catch
        {
          _sql_trx = null;
          _sql_conn = null;
        }
      }
    } // Thread


    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // You don't use it now, but leave it here to see example of use of RowCount and CopyToRows for BatchUpdates.
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //private static void adap_MessageRowUpdated(object sender, SqlRowUpdatedEventArgs e)
    //{
    //  DataRow[] _rows;
    //  Int64 _message_id;

    //  if (e.StatementType == StatementType.Insert)
    //  {
    //    _rows = new DataRow[e.RowCount];
    //    e.CopyToRows(_rows);
    //    foreach (DataRow _row in _rows)
    //    {
    //      _message_id = (Int64)_row[COLUMN_MESSAGE_ID];
    //    }
    //    // It doesn't work!! In Batch Update can't access Row, must use CopyToRows()!!
    //    //e.Row["WM_MESSAGE_ID"] = (Int64)e.Command.Parameters["@p6"].Value;
    //  }
    //} // adap_MessageRowUpdated
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    #endregion // Private Methods

  } // Messages

} // WSI.Common.BatchUpdate
