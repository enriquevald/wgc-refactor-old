﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public interface IPromoGame
  {
    List<PromoGame> GetListPromoGame(SqlTransaction SqlTrx);
    PromoGame GetPromoGameById(Int64 id);
    List<PromoGame> GetListPromoGameByType(PromoGame.GameType Type, Int64 PromotionId, SqlTransaction SqlTrx);
    Boolean InsertPromoGame(SqlTransaction SqlTrx);
    Boolean UpdatePromoGame(SqlTransaction SqlTrx);
  }
}
