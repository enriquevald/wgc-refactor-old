//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Cage.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. CommonCashierSessionInfo
//                2. CashierMovementsTable
// 
//        AUTHOR: Francesc Borrel
// 
// CREATION DATE: 30-JUN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2014 FBA    Initial version.
// 16-MAY-2014 JAB    Added new functionality: Cage auto mode.
// 16-MAY-2014 CCG    Fixed bug #WIG-925: Change the query for general param "cashier.defaultcagesession".
// 16-MAY-2014 DLL    Fixed Bug WIG-924: Delete ApplyExchange function. Values save in CM_AUX_AMOUNT.
// 21-MAY-2014 JCA    Fixed Bug WIG-937: Error in "AutoMode" and "AutoMode.CageStock" when FILL_OUT CHECK.
// 06-JUN-2014 JAB    Added two decimal characters in denominations fields.
// 25-JUL-2014 DLL    Chips are moved to a separate table
// 08-OCT-2014 LEM    Fixed Bug WIG-1038: Wrong denomination description when Cage.ShowDenominations = 2.
// 09-OCT-2014 LEM    Fixed Bug WIG-1460: Show wrong value (-100.00) for Coins in collected column of currency grid
// 13-NOV-2014 LRS    Add currency type functionality
// 26-FEB-2015 JPJ    Fixed Bug WIG-2111: With Autocagemode on quantities aren't reported correctly (always with 0).
// 27-FEB-2015 JPJ    Fixed Bug WIG-2111: Coins aren't supported.
// 20-MAY-2015 DLL    Fixed Bug WIG-2364: Don't apply Gaming Day for Progressive Provisions
// 29-MAY-2015 YNM    Fixed Bug WIG-2393: Endowment to open table don't apply GamingDay
// 09-JUL-2015 YNM    TFS-2074: Alarm any pending collection
// 21-AUG-2015 MPO    TFS-3884: Bugs & new functionalities for Undo withdrawal
// 08-SEP-2015 FOS    Backlog Item 3709
// 30-SEP-2015 ETP    Fixed Bug-4683: Delete CAGE_PENDING_MOVEMENTS in any case when Update Cage Movement Status.
// 22-OCT-2015 JML    Fixed Bug-5581: cash summary no run in multisite
// 26-OCT-2015 YNM    Fixed Bug-5530: Alarm botton don't appear
// 04-APR-2016 DHA    Product Backlog Item 9756: added chips (RE, NR and Color)
// 19-APR-2016 ETP    Fixed Bug-12198: Cage: Automode --> opens new session
// 13-APR-2016 RAB    Product Backlog Item 10855: Tables (Phase 1): Currency/chip Selector.
// 13-MAY-2016 DHA    Product Backlog Item 9848: added gaming table dropbox
// 24-MAY-2016 DHA    Product Backlog Item 9848: drop box gaming tables
// 27-MAY-2016 RAB    Bug 13157: Gaming Table: Second movement of vault game table proposes a different session that was generated. 
// 09-JUN-2016 RAB    PBI 11755: Tables (Phase 1): Correction errors as a result of the backward compatibility of the chips group
// 19-JUL-2016 FAV    Fixed Bug 7317: Flash report with new formules
// 28-JUN-2016 DHA    Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
// 25-AGO-2016 FJC    Fixed Bug 16888:CountR: No aparece el numero de tickets en el Recaudado.
// 28-JUN-2016 DHA    Product Backlog Item 16766: added check and bank card on gaming tables collections
// 10-NOV-2016 JML    Fixed Bug 13791: Game Tables: Proposes an incorrect vault session when integrating cashier into a game table
// 17-NOV-2016 FJC    PBI 20186:C2Go - B�veda - Recargas
// 17-NOV-2016 FJC    PBI 20491:C2Go - B�veda - Retiros
// 23-NOV-2016 FGB    PBI 19199: Reopen cashier sessions and gambling tables sessions
// 08-DIC-2016 RGR    Bug 18295: Cage: The general report is not showing the correct data when a withdrawal is canceled
// 15-DEC-2016 FGB    Bug 21547: Cashier: Cannot "reopen cash" when closed with credit card, check and cash.
// 18-JAN-2017 FAV    Bug 19045: TITO, Error in Collection for tickets with Cage.automode = 1
// 08-FEB-2017 DHA    PBI 24378: Reopen cashier/gambling tables sesions
// 15-FEB-2017 ATB    Bug 24595: WigosGUI: the system mistakes between coins and bills at money collection
// 09-FEB-2017 JML    PBI 24378: Reabrir sesiones de caja y mesas de juego: rehacer reapertura y correcci�n de Bugs
// 01-MAR-2017 ETP    Fixed Bug 25225: WigosGUI: Se estan considerando las monedas como billetes en la recaudaci�n si el cajero esta en ingles.
// 02-MAY-2017 FOS    Fixed Bug 26929: Stock with 6� denomination.
// 24-MAY-2017 JML    PBI 27484:WIGOS-1226 Win / loss - Introduce the WinLoss - EDITION SCREEN - PER DENOMINATION
// 24-MAY-2017 JML    PBI 27486:WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
// 25-MAY-2017 JML    Bug 27690:WIGOS-2349 Win/Loss. After edit the register it is not possible to enter a decimal value as amount.
// 03-AUG-2017 DPC    PBI 25788: Hide Currency Credit Line
// 04-OCT-2017 DHA    Bug 30062:WIGOSWIGOS-5510 [Ticket #9235] Reporte Diario Proveedores / Caja Dep�sito de Caja v03.006.0023
// 20-NOV-2017 RAB    Bug 30855:WIGOS-6527 [Ticket #10394] e03.007.0012.01 [v3.007.]MKT: No aparece el s�mbolo de Alarma al realizar una recaudaci�n en caja
// 23-JAN-2018 DHA & AGS  Bug 31304:WIGOS-7194 [Ticket #11260] Release 03.005.141 - In cashier is allowed to cancel the withdrawal already received in the cage
// 30-APR-2018 JML    Fixed Bug 32484:WIGOS-10534 Cage stock amount and denominations are incorrect in database
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI;
using WSI.Common;
using System.Collections;

namespace WSI.Common
{
  public class Cage
  {
    public const string CHIPS_ISO_CODE = "X01";
    public const Int32 COINS_COLLECTION_CODE = -50;
    public const Int32 COINS_CODE = -100;
    public const Int32 TICKETS_CODE = -200;
    public const String TOTALS_CODE = "TOTALS";
    public const Int32 BANK_CARD_CODE = -1;
    public const Int32 CHECK_CODE = -2;
    public const Int16 MAX_ISO_CODE_DIGITS = 3;

    public const string CHIPS_VALUE = "X01";
    public const string CHIPS_COLOR = "X02";

    public const Int16 CREDIT_LINES = -7;

    // Cage enums
    public enum CageStatus
    {
      Any = -1,
      Sent = 0,
      Canceled = 1,
      OK = 2,
      OkDenominationImbalance = 3,
      OkAmountImbalance = 4,
      InProgress = 5,
      CanceledDueToImbalanceOrOther = 6,
      UserCancelToConfirmAmounts = 7,
      ErrorTotalImbalance = 8,
      SentToCustom = 9,
      ReceptionFromCustom = 10,
      FinishedOpenCloseCage = 11,
      FinishedCountCage = 12,
      CanceledUncollectedCashierRetirement = 13
    }

    public enum CageOperationType
    {
      // 0 TO 99 IS FOR OUTPUT MONEY
      ToCashier = 0,
      ToCustom = 1,
      CloseCage = 2,
      CountCage = 3,
      ToGamingTable = 4,
      ToTerminal = 5,
      LostQuadrature = 99,

      // 100 TO 199 IS FOR INPUT MONEY
      FromCashier = 100,
      FromCustom = 101,
      OpenCage = 102,
      FromGamingTable = 103,
      FromTerminal = 104,
      ReOpenCage = 105,
      FromGamingTableDropbox = 106,
      FromGamingTableDropboxWithExpected = 107,
      FromCashierClosing = 108,
      FromGamingTableClosing = 109,
      GainQuadrature = 199,

      // Cashier requesting money
      RequestOperation = 200,

      // Progressives
      ProgressiveProvision = 300,
      ProgressiveAwarded = 301,
      ProgressiveCancelProvision = 302,
      ProgressiveCancelAwarded = 303
    }

    public enum PendingMovementType
    {
      ToCashierUser = 1,
      ToCashierTerminal = 2,
      ToGamingTable = 3
    }

    public enum ShowDenominationsMode
    {
      HideAllDenominations = 0,
      ShowAllDenominations = 1,
      ShowOnlyChipsDenominations = 2
    }

    public enum NoteAcCollectionType
    {
      HideAll = 0,
      ShowTotals = 1,
      ShowAll = 2,
    }

    public enum CollectionSuggestMode
    {
      None = 0,
      Expected = 1,
      SoftCountOrNone = 2,
      SoftCountOrExpected = 3,
    }

    public enum DefaultCageSession
    {
      FirstCageSessionOpened = 0,
      LastCageSessionOpened = 1,
      ChooseCageSession = 2
    }

    //YNM 08-JUL-2015
    public enum MovTypeToShow
    {
      None = 0,
      AllPending = 1,
      CashierMoneyRequest = 2,
      MoneyCollect = 3,
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts into cashier movements the Cage bill amounts
    //
    //  PARAMS :
    //      - INPUT :
    //              - CageAmounts
    //              - CashierMovement
    //              - MovementId
    //              - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if the operation goes well
    //
    //   NOTES :
    //
    public static Boolean InsertCageCurrenciesIntoCashierMovements(CageDenominationsItems.CageDenominationsDictionary CageAmounts
                                                              , CASHIER_MOVEMENT CashierMovement
                                                              , Int64 MovementId
                                                              , CashierSessionInfo CashierSessionInfo
                                                              , Int64 OperationId
                                                              , SqlTransaction Trx)
    {
      DataTable _dt_cage_currencies;
      CashierMovementsTable _cashier_movements;
      Decimal _total;
      Decimal _denomination;
      CageCurrencyType _cage_currency_type;
      Decimal _un;
      String _iso_code;
      Decimal _aux_amount;
      String _national_currency;
      CurrencyExchange _currency_exchange;
      CurrencyExchangeResult _exchange_result;
      CurrencyExchangeType _currency_type;
      Int64? _chip_id;
      Boolean _closing_with_zero;

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
      _un = 0;
      _aux_amount = -1;
      _cage_currency_type = CageCurrencyType.Bill;
      _closing_with_zero = true;

      try
      {
        _cashier_movements = new CashierMovementsTable(CashierSessionInfo);

        foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _item in CageAmounts)
        {
          _un = 0;
          _dt_cage_currencies = _item.Value.ItemAmounts;

          foreach (DataRow _row in _dt_cage_currencies.Rows)
          {
            _chip_id = null;

            if (FeatureChips.IsCageCurrencyTypeChips(_item.Key.Type))
            {
              _cage_currency_type = _item.Key.Type;
              _chip_id = (Int64)_row["CHIP_ID"];

              if (_item.Key.Type != CageCurrencyType.ChipsColor)
              {
                if (_item.Key.Type == CageCurrencyType.ChipsRedimible)
                {
                  _cage_currency_type = CageCurrencyType.ChipsRedimible;
                }
                else
                {
                  _cage_currency_type = CageCurrencyType.ChipsNoRedimible;
                }

                Decimal.TryParse(_row["DENOMINATION"].ToString(), out _denomination);

                if (_denomination > 0)
                {
                  Decimal.TryParse(_row["UN"].ToString().Trim(), out _un);
                }
              }
              else
              {
                _cage_currency_type = CageCurrencyType.ChipsColor;

                Decimal.TryParse(_row["UN"].ToString().Trim(), out _un);
              }
            }
            else
            {
              _aux_amount = -1;
              Decimal.TryParse(_row["DENOMINATION"].ToString(), out _denomination);

              _cage_currency_type = CageCurrencyType.Bill;
              if (_row["CURRENCY_TYPE"].ToString() == Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN")) _cage_currency_type = CageCurrencyType.Coin;

              if (_denomination > 0) // Billetes
              {
                Decimal.TryParse(_row["UN"].ToString().Trim(), out _un);
              }
              else // -1 tarjeta bancaria, -2 cheque, -100 monedas, -200 Tiquets
              {
                if (_denomination == Cage.TICKETS_CODE)
                {
                  Decimal.TryParse(_row["UN"].ToString().Trim(), out _un);
                  _row["TOTAL"] = _un;
                  CheckExpectedTickets((Int32)_un, CashierSessionInfo, Trx);
                }
                else
                {
                  Decimal.TryParse(_row["TOTAL"].ToString().Trim(), out _un);
                }
              }
            }
            if (_un != 0)
            {
              _closing_with_zero = false;

              Decimal.TryParse(_row["TOTAL"].ToString(), out _total);
              Decimal.TryParse(_row["DENOMINATION"].ToString(), out _denomination);
              _iso_code = _item.Key.ISOCode.Substring(0, (_item.Key.ISOCode.Length < 3) ? _item.Key.ISOCode.Length : 3);

              if (_iso_code != _national_currency && CashierMovement == CASHIER_MOVEMENT.CAGE_CLOSE_SESSION)
              {
                CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, _iso_code, out _currency_exchange, Trx);
                _currency_exchange.ApplyExchange(_total, out _exchange_result);
                _aux_amount = _exchange_result.GrossAmount;
              }

              switch ((Int32)_denomination)
              {
                case Cage.CHECK_CODE:
                  _currency_type = CurrencyExchangeType.CHECK;
                  break;

                case Cage.BANK_CARD_CODE:
                  _currency_type = CurrencyExchangeType.CARD;
                  break;

                default:
                  _currency_type = CurrencyExchangeType.CURRENCY;
                  if (_iso_code == Cage.CHIPS_ISO_CODE)
                  {
                    _currency_type = CurrencyExchangeType.CASINOCHIP;
                  }
                  break;
              }

              if (FeatureChips.IsCageCurrencyTypeChips(_item.Key.Type))
              {
                _currency_type = FeatureChips.ConvertToCurrencyExchangeType(_item.Key.Type);
              }

              _cashier_movements.Add(OperationId, CashierMovement, _total, 0, "", "", _iso_code, _denomination, MovementId, 0, _aux_amount, _currency_type, _cage_currency_type, _chip_id);
            }
          }
        }

        if (_closing_with_zero && CashierMovement == CASHIER_MOVEMENT.CAGE_CLOSE_SESSION)
        {
          _cashier_movements.Add(OperationId, CashierMovement, 0, 0, "", "", CurrencyExchange.GetNationalCurrency(), 0, MovementId, 0, 0, CurrencyExchangeType.CURRENCY, CageCurrencyType.Bill, null);
        }

        if (!_cashier_movements.Save(Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InsertCageCurrenciesIntoCashierMovements

    public static Boolean GetAmountFromMovement(CageDenominationsItems.CageDenominationsDictionary CageAmounts
                                              , CashierSessionInfo CashierSessionInfo
                                              , out Decimal _total)
    {

      DataTable _dt_cage_currencies;
      CashierMovementsTable _cashier_movements;
      Decimal _denomination;
      Decimal _un;
      Decimal _amount;

      _un = 0;
      _total = 0;

      try
      {
        _cashier_movements = new CashierMovementsTable(CashierSessionInfo);

        foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _item in CageAmounts)
        {
          _un = 0;
          _dt_cage_currencies = _item.Value.ItemAmounts;

          foreach (DataRow _row in _dt_cage_currencies.Rows)
          {
            Decimal.TryParse(_row["DENOMINATION"].ToString(), out _denomination);

            if (_denomination > 0) // Billetes
            {
              Decimal.TryParse(_row["UN"].ToString().Trim(), out _un);
            }
            else // -1 tarjeta bancaria, -2 cheque, -100 monedas, -200 Tiquets
            {
              if (_denomination == Cage.TICKETS_CODE)
              {
                Decimal.TryParse(_row["UN"].ToString().Trim(), out _un);
                _row["TOTAL"] = _un;
              }
              else
              {
                Decimal.TryParse(_row["TOTAL"].ToString().Trim(), out _un);
              }
            }

            if (_un != 0)
            {
              Decimal.TryParse(_row["TOTAL"].ToString(), out _amount);
              _total += _amount;
            }
          }
        }

        return true;
      }
      catch (Exception)
      {
        throw;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Check expected tickets and generate alarms if there is a mismatch
    //
    //  PARAMS :
    //      - INPUT :
    //                - NumTickets
    //                - SessionInfo
    //                - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - None
    //
    //   NOTES :
    //
    public static void CheckExpectedTickets(Int32 NumTickets, CashierSessionInfo SessionInfo, SqlTransaction Trx)
    {
      Int32 _expected_tickets;
      StringBuilder _sb;
      Object _obj;
      String _description;
      AlarmSeverity _severity;
      Int32 _imbalance;
      String _source_name;

      _expected_tickets = -1;
      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   COUNT(TI_TICKET_ID) ");
      _sb.AppendLine("       FROM   TICKETS ");
      _sb.AppendLine(" INNER JOIN   MONEY_COLLECTIONS ");
      _sb.AppendLine("         ON   TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID ");
      _sb.AppendLine("      WHERE   MC_CASHIER_SESSION_ID = @pSessionId ");
      _sb.AppendLine("        AND   (TI_COLLECTED_MONEY_COLLECTION IS NULL OR TI_COLLECTED_MONEY_COLLECTION <> MC_COLLECTION_ID) ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionInfo.CashierSessionId;
        _obj = _sql_cmd.ExecuteScalar();

        if (_obj != null && _obj != DBNull.Value)
        {
          _expected_tickets = (Int32)_obj;
        }
      }

      if (_expected_tickets < 0)
      {
        Log.Message("CheckExpectedTickets: Couldn't recover the expected tickets.");
        return;
      }

      if (NumTickets != _expected_tickets)
      {
        _imbalance = NumTickets - _expected_tickets;
        _description = Resource.String("STR_EA_CASH_DESK_CLOSE_TICKETS_DIFF_MISSING", _imbalance.ToString("+#;-#"));
        _severity = _imbalance > 0 ? AlarmSeverity.Info : AlarmSeverity.Warning;
        _source_name = SessionInfo.UserName + "@" + SessionInfo.TerminalName;

        if (SessionInfo.UserName != SessionInfo.AuthorizedByUserName)
        {
          _source_name += " - " + Resource.String("STR_VOUCHER_CARD_HANDPAY_SIGN_AUTHORIZED_BY") + ": " + SessionInfo.AuthorizedByUserName;
        }

        //Recording the alarm using the Alarm.Register
        Alarm.Register(AlarmSourceCode.User, SessionInfo.UserId, _source_name,
                       (UInt32)AlarmCode.User_WrongDelivered, _description, _severity, DateTime.MinValue, Trx);
      }
    } // CheckExpectedTickets

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Creates Movement
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : This function is used to maintain compatibility with old calls
    //
    public static Boolean CreateCageMovement(Int64 CageSessionId, CashierSessionInfo SessionInfo, out Int64 MovementId, SqlTransaction Trx)
    //public static Boolean CreateCageMovement(SqlTransaction SqlTrx, out Int64 MovementId, Int64 CageSessionId, CashierSessionInfo Session)
    {
      return CreateCageMovement(CageSessionId, SessionInfo, CageOperationType.FromCashier, Cage.CageStatus.Sent, -1, out MovementId, null, CASHIER_MOVEMENT.NOT_ASSIGNED, 0, Trx);
    }

    public static Boolean CreateCageMovement(Int64 CageSessionId, CashierSessionInfo SessionInfo,
                                             CageOperationType OperationType, Cage.CageStatus CageStatus,
                                             Int64 MoneyCollectionId, out Int64 MovementId, SqlTransaction Trx)
    {
      return CreateCageMovement(CageSessionId, SessionInfo, OperationType, Cage.CageStatus.Sent, -1, out MovementId, null, CASHIER_MOVEMENT.NOT_ASSIGNED, 0, Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Creates Movement
    //
    //  PARAMS :
    //      - INPUT :
    //                - CageSessionId
    //                - SessionInfo
    //                - OperationType
    //                - CageStatus
    //                - MoneyCollectionId
    //                - Trx
    //      - OUTPUT :
    //                - MovementId
    //
    // RETURNS :
    //            - True: If cage movement has been created correctly
    //            - False: Otherwise
    //
    public static Boolean CreateCageMovement(Int64 CageSessionId, CashierSessionInfo SessionInfo,
                                             CageOperationType OperationType, Cage.CageStatus CageStatus,
                                             Int64 MoneyCollectionId, out Int64 MovementId,
                                             CageDenominationsItems.CageDenominationsDictionary CageAmounts,
                                             CASHIER_MOVEMENT CashierMovement,
                                             Int32 GamingTableId,
                                             SqlTransaction Trx)
    {
      StringBuilder _sb_movement;
      StringBuilder _sb_movement_detail;
      SqlParameter _param;
      Boolean _process;
      Decimal _quantity;
      Decimal _denomination;
      Int64? _chip_id;
      DataTable _dt_detail;
      CageCurrencyType _cage_currency_type;

      MovementId = 0;
      _process = (CageAmounts != null && CashierMovement != CASHIER_MOVEMENT.NOT_ASSIGNED);

      try
      {
        _sb_movement = new StringBuilder();
        _sb_movement.AppendLine(" INSERT INTO   CAGE_MOVEMENTS                ");
        _sb_movement.AppendLine("              ( CGM_USER_CASHIER_ID          ");
        _sb_movement.AppendLine("             ,  CGM_USER_CAGE_ID             ");
        _sb_movement.AppendLine("             ,  CGM_TERMINAL_CASHIER_ID      ");
        _sb_movement.AppendLine("             ,  CGM_TYPE                     "); // before: CGM_OPERATION_ID
        _sb_movement.AppendLine("             ,  CGM_STATUS                   ");
        _sb_movement.AppendLine("             ,  CGM_CASHIER_SESSION_ID       ");
        _sb_movement.AppendLine("             ,  CGM_CAGE_SESSION_ID          ");
        _sb_movement.AppendLine("             ,  CGM_MOVEMENT_DATETIME        ");

        if (GamingTableId > 0)
        {
          _sb_movement.AppendLine("             ,  CGM_GAMING_TABLE_ID          ");
        }

        if (MoneyCollectionId > 0)
        {
          _sb_movement.AppendLine("             ,  CGM_MC_COLLECTION_ID         ");
        }

        _sb_movement.AppendLine("             )                               ");
        _sb_movement.AppendLine("                VALUES (                     ");
        _sb_movement.AppendLine("                @pUserCashier                ");
        _sb_movement.AppendLine("             ,  @pUserCageId                 ");
        _sb_movement.AppendLine("             ,  @pTerminalCashier            ");
        _sb_movement.AppendLine("             ,  @pOperationID                ");
        _sb_movement.AppendLine("             ,  @pStatus                     ");
        _sb_movement.AppendLine("             ,  @pCashierSessionId           ");
        _sb_movement.AppendLine("             ,  @pCageSessionId              ");
        _sb_movement.AppendLine("             ,  @pMovementDatetime           ");

        if (GamingTableId > 0)
        {
          _sb_movement.AppendLine("            ,  @pGamingTableId               ");
        }

        if (MoneyCollectionId > 0)
        {
          _sb_movement.AppendLine("           ,  @pCollectionId                 ");
        }

        _sb_movement.AppendLine("             )                              ");

        _sb_movement.AppendLine("SET @pMovementId = SCOPE_IDENTITY()");

        using (SqlCommand _cmd = new SqlCommand(_sb_movement.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOperationID", SqlDbType.Int).Value = (Int32)OperationType;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)CageStatus;
          _cmd.Parameters.Add("@pUserCashier", SqlDbType.BigInt).Value = SessionInfo.UserId;
          _cmd.Parameters.Add("@pUserCageId", SqlDbType.BigInt).Value = SessionInfo.UserId;
          _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.BigInt).Value = SessionInfo.TerminalId;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = SessionInfo.CashierSessionId;
          _cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = CageSessionId;
          _cmd.Parameters.Add("@pMovementDatetime", SqlDbType.DateTime).Value = WGDB.Now;

          if (MoneyCollectionId > 0)
          {
            _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;
          }

          if (GamingTableId > 0)
          {
            _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GamingTableId;
          }

          _param = _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            MovementId = (Int64)_param.Value;
            if (!_process)
            {
              return true;
            }
          }
        }

        // Prepare table for batch update
        _dt_detail = new DataTable();

        _dt_detail.Columns.Add("CMD_MOVEMENT_ID", Type.GetType("System.Int64"));
        _dt_detail.Columns.Add("CMD_QUANTITY", Type.GetType("System.Decimal"));
        _dt_detail.Columns.Add("CMD_DENOMINATION", Type.GetType("System.Decimal"));
        _dt_detail.Columns.Add("CMD_ISO_CODE", Type.GetType("System.String"));
        _dt_detail.Columns.Add("CMD_CHIP_ID", Type.GetType("System.Int64"));
        _dt_detail.Columns.Add("CMD_CAGE_CURRENCY_TYPE", Type.GetType("System.Decimal"));

        foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_amount in CageAmounts)
        {
          if (_cur_amount.Value.TotalAmount > 0)
          {
            foreach (DataRow _cur_denomination in _cur_amount.Value.ItemAmounts.Rows)
            {
              _quantity = 0;
              _denomination = 0;
              _chip_id = null;
              _cage_currency_type = CageCurrencyType.Bill;

              Decimal.TryParse(_cur_denomination[9].ToString(), out _quantity);
              Decimal.TryParse(_cur_denomination[8].ToString(), out _denomination);

              if (_denomination != Cage.BANK_CARD_CODE && _denomination != Cage.CHECK_CODE)
              {
                _cage_currency_type = (CageCurrencyType)_cur_denomination[11];
              }

              if (_denomination < 0)
              {
                if (Cage.IsCageAutoMode() && _denomination == Cage.TICKETS_CODE)
                {
                  _denomination = 0;
                  _quantity = Cage.TICKETS_CODE;
                }
                else
                {
                  _quantity = _denomination;
                  Decimal.TryParse(_cur_denomination[10].ToString(), out _denomination);

                }
              }

              if ((String)_cur_denomination[6] == Cage.CHIPS_ISO_CODE || FeatureChips.IsChipsType(_cage_currency_type))
              {
                _chip_id = (Int64)_cur_denomination[12];
              }

              if ((!String.IsNullOrEmpty(_cur_denomination[9].ToString()) && Format.ParseCurrency(_cur_denomination[9].ToString()) > 0) ||
                  (!String.IsNullOrEmpty(_cur_denomination[10].ToString()) && Format.ParseCurrency(_cur_denomination[10].ToString().Trim()) > 0))
              {
                DataRow _dr;

                _dr = _dt_detail.NewRow();

                _dr["CMD_MOVEMENT_ID"] = MovementId;
                _dr["CMD_QUANTITY"] = _quantity;              // CAA_UN
                _dr["CMD_DENOMINATION"] = _denomination;    // CAA_DENOMINATION
                _dr["CMD_ISO_CODE"] = (_cur_denomination[6].ToString().Length > MAX_ISO_CODE_DIGITS) ?
                                                                                  _cur_denomination[6].ToString().Substring(0, MAX_ISO_CODE_DIGITS) :
                                                                                  _cur_denomination[6]; // CAA_ISO_CODE
                if (_chip_id == null)
                {
                  _dr["CMD_CHIP_ID"] = DBNull.Value;
                }
                else
                {
                  _dr["CMD_CHIP_ID"] = _chip_id;
                }

                if (FeatureChips.IsChipsType(_cage_currency_type))
                {
                  _dr["CMD_CAGE_CURRENCY_TYPE"] = _cage_currency_type;
                }
                // "B" for Bills and "M" for Coins
                // ATB 15-FEB-2017
                // The currency type position at the array is 1
                else if (_cur_denomination[1].ToString() == Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN"))
                {
                  _dr["CMD_CAGE_CURRENCY_TYPE"] = CageCurrencyType.Coin;
                }
                else
                {
                  _dr["CMD_CAGE_CURRENCY_TYPE"] = CageCurrencyType.Bill;
                }

                _dt_detail.Rows.Add(_dr);

              }
            }
          }
        }

        if (Cage.IsCageAutoMode())
        {
          // Add tickets to collection
          int _count_tickets = 0;

          Ticket.GetNumTicketsForSession(SessionInfo, out _count_tickets, Trx);

          if (_count_tickets > 0)
          {
            DataRow _dr;
            _dr = _dt_detail.NewRow();

            _dr["CMD_MOVEMENT_ID"] = MovementId;
            _dr["CMD_QUANTITY"] = Cage.TICKETS_CODE; // tickets
            _dr["CMD_DENOMINATION"] = _count_tickets;
            _dr["CMD_ISO_CODE"] = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
            _dr["CMD_CHIP_ID"] = DBNull.Value;
            _dr["CMD_CAGE_CURRENCY_TYPE"] = CageCurrencyType.Others;

            _dt_detail.Rows.Add(_dr);

            List<Int64> _list_ticket_id;
            Ticket.GetTicketsByCashierSessionId(SessionInfo, out _list_ticket_id, Trx);

            foreach (Int64 _ticket_id in _list_ticket_id)
            {
              // Is necessary update ticket by ticket because there is a Trigger rule
              Ticket.DB_UpdateCollectedTickets(_ticket_id, MovementId, Trx);
            }
          }
        }

        // Batch Update
        _sb_movement_detail = new StringBuilder();

        _sb_movement_detail.AppendLine("   INSERT INTO   CAGE_MOVEMENT_DETAILS      ");
        _sb_movement_detail.AppendLine("               ( CMD_MOVEMENT_ID            ");
        _sb_movement_detail.AppendLine("               , CMD_QUANTITY               ");
        _sb_movement_detail.AppendLine("               , CMD_DENOMINATION           ");
        _sb_movement_detail.AppendLine("               , CMD_ISO_CODE               ");
        _sb_movement_detail.AppendLine("               , CMD_CHIP_ID                ");
        _sb_movement_detail.AppendLine("               , CMD_CAGE_CURRENCY_TYPE     ");
        _sb_movement_detail.AppendLine("               )                            ");
        _sb_movement_detail.AppendLine("        VALUES                              ");
        _sb_movement_detail.AppendLine("               ( @pMovementId               ");
        _sb_movement_detail.AppendLine("               , @pQuantity                 ");
        _sb_movement_detail.AppendLine("               , @pDenomination             ");
        _sb_movement_detail.AppendLine("               , @pIsoCode                  ");
        _sb_movement_detail.AppendLine("               , @pChipID                   ");
        _sb_movement_detail.AppendLine("               , @CageCurrencyType          ");
        _sb_movement_detail.AppendLine("               )                            ");

        using (SqlCommand _sql_cmd_update = new SqlCommand(_sb_movement_detail.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd_update.Parameters.Add("@pMovementId", SqlDbType.BigInt).SourceColumn = "CMD_MOVEMENT_ID";
          _sql_cmd_update.Parameters.Add("@pQuantity", SqlDbType.Int).SourceColumn = "CMD_QUANTITY";
          _sql_cmd_update.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "CMD_DENOMINATION";
          _sql_cmd_update.Parameters.Add("@pIsoCode", SqlDbType.NVarChar, 3).SourceColumn = "CMD_ISO_CODE";
          _sql_cmd_update.Parameters.Add("@pChipID", SqlDbType.BigInt).SourceColumn = "CMD_CHIP_ID";
          _sql_cmd_update.Parameters.Add("@CageCurrencyType", SqlDbType.BigInt).SourceColumn = "CMD_CAGE_CURRENCY_TYPE";

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            _da.InsertCommand = _sql_cmd_update;

            if (_da.Update(_dt_detail) != _dt_detail.Rows.Count)
            {
              // Not all data has been inserted
              Log.Message("Cage.CreateCageMovement: Not all data has been updated");

              return false;
            }

          } //SqlDataAdapter
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }

      return false;
    } // CreateCageMovement

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Creates Pending Movement
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static Boolean CreateCagePendingMovement(Int64 MovementId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   CAGE_PENDING_MOVEMENTS ");
        _sb.AppendLine("             ( CPM_MOVEMENT_ID ");
        _sb.AppendLine("             , CPM_USER_ID ");
        _sb.AppendLine("             ) ");
        _sb.AppendLine("               VALUES ");
        _sb.AppendLine("             ( @pMovementID ");
        _sb.AppendLine("             , @pUserID ");
        _sb.AppendLine("             ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pMovementID", SqlDbType.BigInt).Value = MovementId;
          _cmd.Parameters.Add("@pUserID", SqlDbType.Int).Value = -1; // -1 indicate movement generated from cashier
          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // CreateCagePendingMovement


    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Returns value of GP Cage.Enabled
    //
    public static Boolean IsCageEnabled()
    {
      return GeneralParam.GetBoolean("Cage", "Enabled", false);
    } // IsCageEnabled

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Returns value of GP Cashier.DefaultCageSession
    //
    public static Int32 CashierDefaultCageSession()
    {
      return GeneralParam.GetInt32("Cage", "Cashier.DefaultCageSession", 0);
    } // CashierDefaultCageSession

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Returns value of GP Collection.DefaultCageSession
    //
    public static Int32 CollectionDefaultCageSession()
    {
      return GeneralParam.GetInt32("Cage", "Collection.DefaultCageSession", 0);
    } //CollectionDefaultCageSession

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Returns value of GP AutoMode
    //
    public static Boolean IsCageAutoMode()
    {
      return GeneralParam.GetBoolean("Cage", "AutoMode", false);
    } //IsCageAutoMode

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Returns value of GP AutoMode.CageStock
    //
    public static Boolean IsCageAutoModeCageStock()
    {
      return GeneralParam.GetBoolean("Cage", "AutoMode.CageStock", false);
    } //IsCageAutoModeCageStock

    public static String GetConceptDescriptionGeneric()
    {
      return GeneralParam.GetString("Cashier.Voucher", "Concept.Name.Generic",
                                    Resource.String("STR_FRM_VOUCHERS_REPRINT_090"));
    } //GetConceptDescriptionGeneric

    public static String GetConceptDescription(Int32 Code)
    {
      return GeneralParam.GetString("Cashier.Voucher", "Concept.Name." + Code.ToString(),
                                    GeneralParam.GetString("Cashier.Voucher", "Concept.Name.Generic",
                                    Resource.String("STR_FRM_VOUCHERS_REPRINT_090")));
    } //GetConceptDescription

    public static Boolean IsCageSessionOpen(out Int64 CageSessionId, Int64 UserCashierId)
    {
      return IsCageSessionOpen(out CageSessionId, UserCashierId, "CGM_USER_CASHIER_ID");
    } // IsCageSessionOpen

    //------------------------------------------------------------------------------
    // PURPOSE : Check if Cage Session is open
    //
    //  PARAMS :
    //      - INPUT : cage session id
    //
    //      - OUTPUT :
    //
    // RETURNS : true if opened, false if closed
    //
    //   NOTES :
    //
    public static Boolean IsCageSessionOpen(out Int64 CageSessionId, Int64 UserCashierId, string FieldUserOrTerminal)
    {
      return IsCageSessionOpen(out CageSessionId, UserCashierId, FieldUserOrTerminal, false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Check if Cage Session is open
    //
    //  PARAMS :
    //      - INPUT : cage session id
    //
    //      - OUTPUT :
    //
    // RETURNS : true if opened, false if closed
    //
    //   NOTES :
    //
    public static Boolean IsCageSessionOpen(out Int64 CageSessionId, Int64 UserCashierId, string FieldUserOrTerminal, Boolean VerifyCashierSessionIsClosed)
    {
      CageSessionId = 0;

      Boolean _result;
      Object _obj;
      StringBuilder _sb;

      _result = false;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("       SELECT    TOP 1 CGM_CAGE_SESSION_ID                                                                      ");
        _sb.AppendLine("         FROM    CAGE_MOVEMENTS                                                                                 ");
        _sb.AppendLine("   INNER JOIN    CAGE_SESSIONS ON CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID = CAGE_SESSIONS.CGS_CAGE_SESSION_ID        ");
        if (VerifyCashierSessionIsClosed)
        {
          //_sb.AppendLine(" INNER JOIN    CASHIER_SESSIONS ON CS_USER_ID = CGM_USER_CASHIER_ID AND CS_CASHIER_ID=CGM_TERMINAL_CASHIER_ID ");
          _sb.AppendLine(" INNER JOIN    CASHIER_SESSIONS ON CS_SESSION_ID = CGM_CASHIER_SESSION_ID ");
        }
        _sb.AppendLine("        WHERE    CAGE_SESSIONS.CGS_CLOSE_DATETIME IS NULL                                                       ");
        _sb.AppendLine("          AND    " + FieldUserOrTerminal + " = " + UserCashierId);
        if (VerifyCashierSessionIsClosed)
        {
          _sb.AppendLine("        AND    CS_STATUS = @pStatus                                                                           ");
        }
        _sb.AppendLine("     ORDER BY    CGM_MOVEMENT_DATETIME DESC                                                                     ");
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (VerifyCashierSessionIsClosed)
            {
              _sql_command.Parameters.Add("@pStatus", SqlDbType.BigInt).Value = CASHIER_SESSION_STATUS.OPEN;
            }
            _obj = _sql_command.ExecuteScalar();
          }// SqlCommand
          if (_obj != null && _obj != DBNull.Value)
          {
            CageSessionId = (Int64)_obj;
            _result = true;
          }
          else
          {
            _result = false;
          }
        }// DB_TRX
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _result;
    } // IsCageSessionOpen

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Checks if a cage session is opened via cage session id
    //
    //  PARAMS :
    //      - INPUT : cage session id
    //
    //      - OUTPUT :
    //
    // RETURNS : true if opened, false if closed
    //
    //   NOTES :
    //
    public static Boolean IsCageSessionOpen(Int64 CageSessionId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return IsCageSessionOpen(CageSessionId, _db_trx.SqlTransaction);
      }
    }//IsCageSessionOpen

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Checks if a cage session is opened via cage session id
    //
    //  PARAMS :
    //      - INPUT : cage session id
    //      -         Trx
    //
    //      - OUTPUT :
    //
    // RETURNS : true if opened, false if closed
    //
    //   NOTES :
    //
    public static Boolean IsCageSessionOpen(Int64 CageSessionId, SqlTransaction Trx)
    {
      Object _obj;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT    CGS_CAGE_SESSION_ID ");
        _sb.AppendLine("  FROM    CAGE_SESSIONS ");
        _sb.AppendLine(" WHERE    CAGE_SESSIONS.CGS_CLOSE_DATETIME IS NULL ");
        _sb.AppendLine("   AND    CGS_CAGE_SESSION_ID = @pCageSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = CageSessionId;

          _obj = _cmd.ExecuteScalar();
        }// SqlCommand

        if (_obj != null && _obj != DBNull.Value)
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsCageSessionOpen

    public static Boolean IsCageSessionOpen(DateTime WorkingDay, SqlTransaction Trx)
    {
      Object _obj;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT    CGS_CAGE_SESSION_ID ");
        _sb.AppendLine("  FROM    CAGE_SESSIONS ");
        _sb.AppendLine(" WHERE    CAGE_SESSIONS.CGS_CLOSE_DATETIME IS NULL ");
        _sb.AppendLine("   AND    CGS_WORKING_DAY = @pWorkingDay ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = WorkingDay;

          _obj = _cmd.ExecuteScalar();
        }// SqlCommand
        if (_obj != null && _obj != DBNull.Value)
        {
          return true;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsCageSessionOpen

    public static Boolean AreCageAndCashierSessionOpen(out Int64 CageSessionId, Int64 TerminalId)
    {
      Boolean _result;
      Object _obj;
      StringBuilder _sb;

      CageSessionId = 0;
      _result = false;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT    TOP 1 CGM_CAGE_SESSION_ID                ");
        _sb.AppendLine("      FROM    CAGE_MOVEMENTS                           ");
        _sb.AppendLine("INNER JOIN    CAGE_SESSIONS ON CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID = CAGE_SESSIONS.CGS_CAGE_SESSION_ID ");
        _sb.AppendLine("INNER JOIN    CASHIER_SESSIONS ON CASHIER_SESSIONS.CS_CASHIER_ID = CAGE_MOVEMENTS.CGM_TERMINAL_CASHIER_ID");
        _sb.AppendLine("INNER JOIN    GUI_USERS ON CS_USER_ID = GU_USER_ID  ");
        _sb.AppendLine("     WHERE    CAGE_SESSIONS.CGS_CLOSE_DATETIME IS NULL ");
        _sb.AppendLine("       AND    CS_STATUS = 0 ");
        _sb.AppendLine("       AND    CGM_TERMINAL_CASHIER_ID = " + TerminalId);
        _sb.AppendLine("       AND    (CS_SESSION_BY_TERMINAL = 1 or GU_USER_TYPE NOT IN (" + Misc.SystemUsersListListToString() + "))");

        if (Misc.IsGamingDayEnabled())
        {
          _sb.AppendLine("           AND   CGS_WORKING_DAY = @pWorkingDay     ");
          _sb.AppendLine("   ORDER BY 1 DESC");
        }
        else
        {
          _sb.AppendLine("     ORDER BY CGM_MOVEMENT_DATETIME DESC");
        }


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (Misc.IsGamingDayEnabled())
            {
              _sql_command.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = Misc.TodayOpening();
            }
            _obj = _sql_command.ExecuteScalar();

          }// SqlCommand
          if (_obj != null && _obj != DBNull.Value)
          {
            CageSessionId = (Int64)_obj;
            _result = true;
          }
          else
          {
            _result = false;
          }
        }// DB_TRX
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _result;
    } // AreCageAndCashierSessionOpen


    public static Boolean GetCageSessionIdFromCashierSessionId(long CashierSessionId, out long CageSessionId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetCageSessionIdFromCashierSessionId(CashierSessionId, out CageSessionId, _db_trx.SqlTransaction);
      }
    }//GetCageSessionIdFromCashierSessionId

    public static Boolean GetCageSessionIdFromCashierSessionId(long CashierSessionId, out long CageSessionId, SqlTransaction Trx)
    {
      Object _obj;
      StringBuilder _sb;

      CageSessionId = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   CGM_CAGE_SESSION_ID                             ");
        _sb.AppendLine("     FROM   CAGE_MOVEMENTS                                  ");
        _sb.AppendLine("    WHERE   CGM_CASHIER_SESSION_ID = @pCashierSessionId     ");
        _sb.AppendLine("      AND   CGM_TYPE <> @pType                              ");
        _sb.AppendLine(" ORDER BY   CGM_MOVEMENT_DATETIME DESC                      ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_command.Parameters.Add("@pType", SqlDbType.Int).Value = Cage.CageOperationType.RequestOperation;

          _obj = _sql_command.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            CageSessionId = (Int64)_obj;
            return true;
          }
        }// SqlCommand
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCageSessionIdFromCashierSessionId

    public static Boolean GetCurrentCageSessionId(out Int64 CageSessionId, Boolean LastSession)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetCurrentCageSessionId(out CageSessionId, LastSession, _db_trx.SqlTransaction);
      }
    }//GetCurrentCageSessionId

    public static Boolean GetCurrentCageSessionId(out Int64 CageSessionId, Boolean LastSession, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      CageSessionId = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   TOP 1 CGS_CAGE_SESSION_ID                       ");
        _sb.AppendLine("     FROM   CAGE_SESSIONS                                   ");
        _sb.AppendLine("    WHERE   CGS_CLOSE_DATETIME IS NULL                      ");
        if (!LastSession)
        {
          _sb.AppendLine(" ORDER BY   CGS_OPEN_DATETIME ASC                           ");
        }
        else
        {
          _sb.AppendLine(" ORDER BY   CGS_OPEN_DATETIME DESC                          ");
        }

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _obj = _sql_command.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            CageSessionId = (Int64)_obj;
          }
          return true;
        }// SqlCommand
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCurrentCageSessionId

    public static Boolean GetCageOpenBySessionId(Int64 CageSessionId)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" SELECT   TOP 1 CGS_CAGE_SESSION_ID ");
      _sb.AppendLine("   FROM   CAGE_SESSIONS ");
      _sb.AppendLine("  WHERE   CGS_CAGE_SESSION_ID = @pCageSessionId ");
      _sb.AppendLine("    AND   CGS_CLOSE_DATETIME IS NULL ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString()))
          {
            _sql_command.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = CageSessionId;

            using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_command))
            {
              return _sql_reader.Read();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCageOpenBySessionId

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Return Count of Cage Session opened
    //
    //  PARAMS :
    //      - INPUT : GamingDay DateTime
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Int32 with number of Cage Session opened
    //
    //   NOTES :
    //
    public static Int32 GetNumberOfOpenedCages()
    {
      return GetNumberOfOpenedCages(null);
    }

    public static Int32 GetNumberOfOpenedCages(DateTime? GamingDay)
    {
      StringBuilder _sb;
      Object _obj;

      _sb = new StringBuilder();

      _sb.AppendLine("  SELECT   COUNT(CGS_CAGE_SESSION_ID) AS NUM_OPENED_CAGES ");
      _sb.AppendLine("    FROM   CAGE_SESSIONS ");
      _sb.AppendLine("   WHERE   CGS_CLOSE_DATETIME IS NULL ");

      if (GamingDay.HasValue && GamingDay.Value != DateTime.MinValue)
      {
        _sb.AppendLine("     AND   CGS_WORKING_DAY = CAST('" + GamingDay.Value.ToString("yyyy-MM-dd'T'HH:mm:ss") + "' AS DATETIME)");
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _obj = _sql_command.ExecuteScalar();
            if (_obj != null && _obj != DBNull.Value)
            {
              return ((Int32)_obj);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return -1;
    } // GetNumberOfOpenedCages


    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Return name of Cage Session
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64:  Cage Session Id
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String with Cage Session Name 
    //
    //   NOTES :
    //
    public static String GetCageSessionName(Int64 CageSessionID)
    {
      StringBuilder _sb;
      Object _obj;

      _sb = new StringBuilder();

      _sb.AppendLine("  SELECT    CGS_SESSION_NAME            ");
      _sb.AppendLine("    FROM    CAGE_SESSIONS               ");
      _sb.AppendLine("   WHERE    CGS_CAGE_SESSION_ID = " + CageSessionID);

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _obj = _sql_command.ExecuteScalar();
            if (_obj != null && _obj != DBNull.Value)
            {
              return ((String)_obj);
            }

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return String.Empty;
    } // GetCageSessionName

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Return a string with the denomination
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int16:  Denomination
    //            - String: IsoCode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String with denomination 
    //
    //   NOTES :
    //
    public static String GetDenomination(Decimal Denomination, String IsoCode)
    {
      String _result;

      _result = Currency.Format(Denomination, "");

      if (Denomination <= 0)
      {
        switch ((Int32)Denomination)
        {
          case Cage.COINS_CODE:
            if (IsoCode == Cage.CHIPS_ISO_CODE)
            {
              _result = Resource.String("STR_CAGE_TIPS");    // "Propinas"
            }
            else
            {
              if (GeneralParam.GetInt32("Cage", "ShowDenominations", (Int32)ShowDenominationsMode.ShowAllDenominations) != (Int32)ShowDenominationsMode.ShowAllDenominations)
              {
                _result = Resource.String("STR_CAGE_AMOUNT");    // "Monto"
              }
              else
              {
                _result = Resource.String("STR_CAGE_COINS");    // "Monedas"
              }
            }
            break;

          case Cage.TICKETS_CODE:
            _result = Resource.String("STR_CAGE_TICKETS");  // "Ticket"
            break;

          case Cage.BANK_CARD_CODE:
            _result = Resource.String("STR_CAGE_BANKCARD", ""); // "Tarjeta bancaria"
            break;

          case Cage.CHECK_CODE:
            _result = Resource.String("STR_CAGE_CHECK", "");    // "Cheque"
            break;

          default:
            _result = "";
            break;
        }
      }

      return _result;
    } // GetDenomination

    //------------------------------------------------------------------------------
    // PURPOSE : Get cage movement linked to a money collection
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64: MoneyCollectionId
    //
    //      - OUTPUT :
    //            - Int64: CageMovementId
    //
    // RETURNS :
    //      - true: if has been abled to obtain the cage movement
    //      - false: otherwise
    //
    public static Boolean GetCageMovement(Int64 MoneyCollectionId, out Int64 CageMovementId)
    {
      Object _obj;
      StringBuilder _sb;

      CageMovementId = 0;

      _sb = new StringBuilder();
      _sb.AppendLine("   SELECT   CGM_MOVEMENT_ID                                 ");
      _sb.AppendLine("     FROM   CAGE_MOVEMENTS                                  ");
      _sb.AppendLine("    WHERE   CGM_MC_COLLECTION_ID = @pCollectionId           ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_command.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;
            _obj = _sql_command.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              CageMovementId = (Int64)_obj;

              return true;
            }
          }// SqlCommand
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCageSessionId

    //------------------------------------------------------------------------------
    // PURPOSE : Get cage movement linked to a cashier session
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64: CashierSessionId
    //
    //      - OUTPUT :
    //            - Int64: MovementId
    //
    // RETURNS :
    //      - true: if has been abled to obtain the cage movement
    //      - false: otherwise
    //
    public static Boolean GetLastestCloseMovementIdByCashierSessionId(Int64 CashierSessionId, out Int64 MovementId, out Int32 Status, SqlTransaction Trx)
    {
      StringBuilder _sb;

      MovementId = 0;
      Status = 0;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   A.CGM_MOVEMENT_ID      ");
      _sb.AppendLine("        , A.CGM_STATUS           ");
      _sb.AppendLine("   FROM   CAGE_MOVEMENTS AS A    ");
      _sb.AppendLine("  INNER   JOIN (SELECT MAX(CGM_MOVEMENT_ID) AS CGM_MOVEMENT_ID                      ");
      _sb.AppendLine("                FROM   CAGE_MOVEMENTS                                               ");
      _sb.AppendLine("               WHERE   CGM_CASHIER_SESSION_ID = @pCashierSessionId                  ");
      _sb.AppendLine("                 AND   CGM_TYPE IN (@pFromCashierClosing, @pFromGamingTableClosing) ");
      _sb.AppendLine("               ) AS B ON A.CGM_MOVEMENT_ID = B.CGM_MOVEMENT_ID                      ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _sql_cmd.Parameters.Add("@pFromCashierClosing", SqlDbType.Int).Value = CageOperationType.FromCashierClosing;
          _sql_cmd.Parameters.Add("@pFromGamingTableClosing", SqlDbType.Int).Value = CageOperationType.FromGamingTableClosing;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              MovementId = _reader.GetInt64(0);
              Status = _reader.GetInt32(1);
            }
            else
            {
              return false;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Initial
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int16:  Denomination
    //            - String: IsoCode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - void 
    //
    //   NOTES :
    //
    public static DataTable CageAmountsInitTable()
    {
      DataTable DataTableCageAmounts;
      DataTableCageAmounts = new DataTable();
      DataTableCageAmounts.Columns.Add("IMAGE", typeof(Image));
      DataTableCageAmounts.Columns.Add("CURRENCY_TYPE", Type.GetType("System.String"));
      DataTableCageAmounts.Columns.Add("DENOMINATION_SHOWED", Type.GetType("System.String")); // denomination showed, include coins...      
      DataTableCageAmounts.Columns.Add("CHIP_NAME", Type.GetType("System.String"));
      DataTableCageAmounts.Columns.Add("CHIP_DRAW", Type.GetType("System.String"));
      DataTableCageAmounts.Columns.Add("COLOR", Type.GetType("System.Int32"));
      DataTableCageAmounts.Columns.Add("ISO_CODE", Type.GetType("System.String"));
      DataTableCageAmounts.Columns.Add("ALLOWED", Type.GetType("System.Boolean"));
      DataTableCageAmounts.Columns.Add("DENOMINATION", Type.GetType("System.Decimal"));
      DataTableCageAmounts.Columns.Add("UN", Type.GetType("System.Decimal"));
      DataTableCageAmounts.Columns.Add("TOTAL", Type.GetType("System.String"));
      DataTableCageAmounts.Columns.Add("CAGE_CURRENCY_TYPE_ENUM", Type.GetType("System.Int32"));
      DataTableCageAmounts.Columns.Add("CHIP_ID", Type.GetType("System.Int64"));
      DataTableCageAmounts.Columns.Add("CHIP_SET_ID", Type.GetType("System.Int64"));

      return DataTableCageAmounts;
    } // CageAmountsInitTable

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Prepare row to insert
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //            - ref DataTable DataTableCageAmounts
    //
    // RETURNS :
    //      - void 
    //
    //   NOTES :
    //
    public static void CageAmountsPrepareRow(ref DataTable DataTableCageAmounts, Object Image, Object CaaColor, Object CaaIsoCode, Object CaaDenomination,
                                                 Object CaaAllowed, Object CaaDenominationShowed, Object CaaCurrencyType, Object CaaCurrencyTypeEnum, Object CaaCurrencyChipName,
                                                 Object CaaCurrencyChipDrawing, Object CaaUn, Object CaaTotal, Object CaaChipId, Object CaaChipSetId)
    {
      DataRow DataRowCageAmounts;
      DataRowCageAmounts = DataTableCageAmounts.NewRow();
      DataRowCageAmounts["IMAGE"] = Image;
      DataRowCageAmounts["COLOR"] = CaaColor;
      DataRowCageAmounts["ISO_CODE"] = CaaIsoCode;
      DataRowCageAmounts["DENOMINATION"] = CaaDenomination;
      DataRowCageAmounts["CURRENCY_TYPE"] = CaaCurrencyType;
      DataRowCageAmounts["ALLOWED"] = CaaAllowed;
      DataRowCageAmounts["DENOMINATION_SHOWED"] = CaaDenominationShowed;
      DataRowCageAmounts["UN"] = CaaUn;
      DataRowCageAmounts["TOTAL"] = CaaTotal;
      DataRowCageAmounts["CAGE_CURRENCY_TYPE_ENUM"] = CaaCurrencyTypeEnum;
      DataRowCageAmounts["CHIP_NAME"] = CaaCurrencyChipName;
      DataRowCageAmounts["CHIP_DRAW"] = CaaCurrencyChipDrawing;
      DataRowCageAmounts["CHIP_ID"] = CaaChipId;
      DataRowCageAmounts["CHIP_SET_ID"] = CaaChipSetId;
      DataTableCageAmounts.Rows.Add(DataRowCageAmounts);
    } // CageAmountsPrepareRow

    //------------------------------------------------------------------------------
    // PURPOSE : Create cage movements to link a cashier session with one cage session
    //
    //  PARAMS :
    //      - INPUT :
    //              - CashierSessionInfo
    //              - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If process has been succesful
    //      - False: Otherwise
    //
    public static Boolean LinkTerminalWithCageSession(CashierSessionInfo CSInfo, SqlTransaction Trx)
    {
      Int64 _default_cage_session;
      Int64 _cage_session_id;
      Int64 _cage_movement_id;

      _cage_session_id = 0;

      if (!Cage.IsCageEnabled())
      {
        return true;
      }

      // Set new Cage Session if it's needed
      _default_cage_session = GeneralParam.GetInt32("Cage", "Terminal.DefaultCageSession", 0);

      switch (_default_cage_session)
      {
        case 0:
          if (!Cage.GetCurrentCageSessionId(out _cage_session_id, true, Trx))
          {
            return false;
          }
          break;

        case 1:
          if (!Cage.GetCurrentCageSessionId(out _cage_session_id, false, Trx))
          {
            return false;
          }
          break;

        case 2:
          // Do nothing, cashier session will be linked with cage later ...
          break;

        default:
          Log.Message("Unexpected Cage.Terminal.DefaultCageSession, defaulting to 2.");
          // Do nothing, cashier session will be linked with cage later ...
          break;
      }

      // Create cage movement
      if (_cage_session_id != 0)
      {
        if (!Cage.CreateCageMovement(_cage_session_id, CSInfo, Cage.CageOperationType.ToTerminal, Cage.CageStatus.OK, -1, out _cage_movement_id, Trx))
        {
          Log.Error("LinkCashierTerminalSessionWithCageSession. Error creating cage movement. Cage session id: " + _cage_session_id.ToString());

          return false;
        }
      }

      return true;
    }//LinkTerminalWithCageSession

    //------------------------------------------------------------------------------
    // PURPOSE : Check if it's a virtual cashier or not
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64:  CashierSessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if it's virtual cashier. 
    //
    //   NOTES :
    //
    public static Boolean IsVirtualCashier(Int64 CashierSessionId)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT    CS_SESSION_BY_TERMINAL ");
      _sb.AppendLine("  FROM    CASHIER_SESSIONS ");
      _sb.AppendLine(" WHERE    CS_SESSION_ID = @pCashierSessionId ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString()))
          {
            _sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;

            using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_command))
            {
              if (_sql_reader.Read())
              {
                return _sql_reader.GetBoolean(0);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsVirtualCashier

    /// <summary>
    /// Is Cashier Session Open
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean IsCashierSessionOpen(Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT    1                                    ");
      _sb.AppendLine("  FROM    CASHIER_SESSIONS                     ");
      _sb.AppendLine(" WHERE    CS_SESSION_ID = @pCashierSessionId   ");
      _sb.AppendLine("   AND    CS_CLOSING_DATE IS NULL              ");

      try
      {
        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        //using (SqlCommand _sql_command = new SqlCommand(_sb.ToString()))
        {
          _sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;

          using (SqlDataReader _sql_reader = _sql_command.ExecuteReader())
          {
            return _sql_reader.Read();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsCashierSessionOpen

    //------------------------------------------------------------------------------
    // PURPOSE : Check if it's a gambling table or not
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64:  CashierSessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if it's gambling cashier. 
    //
    //   NOTES :
    //
    public static Boolean IsGamingTable(Int64 CashierSessionId)
    {
      StringBuilder _sb;
      Object _obj;

      // JML 22-OCT-2015 Fixed Bug-5581: cash summary no run in multisite
      if (CashierSessionId == 0 || GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
      {
        return false;
      }

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT    TOP 1 GTS_GAMING_TABLE_SESSION_ID ");
      _sb.AppendLine("  FROM    GAMING_TABLES_SESSIONS ");
      _sb.AppendLine(" WHERE    GTS_CASHIER_SESSION_ID = @pCashierSessionId ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
            _obj = _sql_command.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              return (Int64)_obj > 0;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsGamingTable


    // PURPOSE : Check if it's a gambling table or not
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64:  CashierSessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if it's gambling cashier. 
    //
    //   NOTES :
    //
    public static Boolean GetOpenOrDefaultCageSessionId(Int64 SessionId, out Int64 CageSessionId, DateTime GamingDay, SqlTransaction Trx)
    {
      CageSessionId = 0;

      if ((!Cage.GetCageSessionIdFromCashierSessionId(SessionId, out CageSessionId, Trx) || !Cage.IsCageSessionOpen(CageSessionId, Trx)) &&
              (!GetCageDefaultSessionId(GamingDay, out CageSessionId, Trx)))
      {
        if (GeneralParam.GetBoolean("Cage", "AutoMode"))
        {
          if (!Cage.OpenCageSession(WGDB.Now.ToString("d"), WGDB.Now, 3, out CageSessionId, Trx))
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;

    } //GetOpenOrDefaultCageSessionId


    //------------------------------------------------------------------------------
    // PURPOSE : Opens new cage Session & insert movement.
    //
    //  PARAMS :
    //      - INPUT :
    //            - String:  SessionName
    //            - DateTime: WorkingDay
    //            - Int32: CurrentUserId
    //
    //      - OUTPUT :
    //            Int64: CageSessionId
    //
    // RETURNS :
    //      - True if new session is open.
    //
    //   NOTES :
    //
    public static Boolean OpenCageSession(String SessionName, DateTime WorkingDay, Int32 CurrentUser, out Int64 CageSessionId, SqlTransaction Trx)
    {

      if (!Cage.OpenCageSessionDB(SessionName, WorkingDay, CurrentUser, out CageSessionId, Trx))
      {
        Log.Error("OpenCageSession: Could not open cage session");
        return false;
      }

      if (!CageMeters.OpenCage_CreateCageSessionMeters(CageSessionId, Trx))
      {
        Log.Error("OpenCageSession: Could not create cage session meters");
        return false;
      }

      Int64 _movement_id;
      CashierSessionInfo CashierSession;
      CashierSession = new CashierSessionInfo();
      if (!Cashier.GetCashierSessionById(CommonCashierInformation.SessionId, out CashierSession, Trx))
      {
        Log.Error("OpenCageSession: Could not Get CashSessionInfo");
        return false;
      }

      if (!CreateCageMovement(CageSessionId, CashierSession, CageOperationType.OpenCage, Cage.CageStatus.FinishedOpenCloseCage, 0, out _movement_id, null, CASHIER_MOVEMENT.NOT_ASSIGNED, 0, Trx))
      {
        Log.Error("OpenCageSession: Could not insert movements");
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Opens new cage Session in DB.
    //
    //  PARAMS :
    //      - INPUT :
    //            - String:  SessionName
    //            - DateTime: WorkingDay
    //            - Int32: CurrentUserId
    //
    //      - OUTPUT :
    //            Int64: CageSessionId
    //
    // RETURNS :
    //      - True if new session is open.
    //
    //   NOTES :
    //
    public static Boolean OpenCageSessionDB(String SessionName, DateTime WorkingDay, Int32 CurrentUser, out Int64 CageSessionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _param;
      Boolean _result;
      DateTime _closing_time;
      DateTime _working_day;

      _closing_time = WSI.Common.Misc.TodayOpening();
      _working_day = new DateTime(WorkingDay.Year, WorkingDay.Month, WorkingDay.Day, _closing_time.Hour, _closing_time.Minute, 0);
      CageSessionId = 0;
      _result = true;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   CAGE_SESSIONS                        ");
        _sb.AppendLine("             ( CGS_OPEN_DATETIME                    ");
        _sb.AppendLine("             , CGS_OPEN_USER_ID                     ");
        _sb.AppendLine("             , CGS_SESSION_NAME                     ");
        _sb.AppendLine("             , CGS_WORKING_DAY                      ");
        _sb.AppendLine("             )                                      ");
        _sb.AppendLine("      VALUES (                                      ");
        _sb.AppendLine("               @pOpenDateTime                       ");
        _sb.AppendLine("             , @pOpenUserId                         ");
        _sb.AppendLine("             , @pSessionName                        ");
        _sb.AppendLine("             , @pWorkingDay                         ");
        _sb.AppendLine("             )                                      ");
        _sb.AppendLine("         SET   @pCageSessionId = SCOPE_IDENTITY()   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pOpenUserId", SqlDbType.BigInt).Value = CurrentUser;
          _cmd.Parameters.Add("@pOpenDateTime", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pSessionName", SqlDbType.VarChar).Value = SessionName;
          _cmd.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = _working_day;

          _param = _cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            _result = false;
          }
          else
          {
            CageSessionId = (Int64)_param.Value;
          }

        }
      }
      catch (Exception ex)
      {
        Log.Error("cls_cage_control2 - OpenCageSessionDB" + ex.Message);
        _result = false;
      }

      return _result;
    }

    public static Boolean GetCageDefaultSessionId(out Int64 CageSessionId, SqlTransaction Trx)
    {
      return GetCageDefaultSessionId(DateTime.MinValue, out CageSessionId, Trx);
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Return the cage session id.
    //
    //  PARAMS :
    //      - INPUT :
    //          - _order_by: String
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: true = Has pending movements.
    //
    //   NOTES :
    // 
    public static Boolean GetCageDefaultSessionId(DateTime GamingDay, out Int64 CageSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;
      String _order_by;
      Cage.DefaultCageSession _default_cage_session;

      CageSessionId = 0;
      _order_by = "";
      _default_cage_session = (Cage.DefaultCageSession)Cage.CashierDefaultCageSession();

      switch (_default_cage_session)
      {
        case Cage.DefaultCageSession.FirstCageSessionOpened:
          _order_by = " ASC";
          break;

        case Cage.DefaultCageSession.LastCageSessionOpened:
          _order_by = " DESC";
          break;

        case Cage.DefaultCageSession.ChooseCageSession:
          _order_by = "";
          break;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("          SELECT   TOP 1 CGS_CAGE_SESSION_ID          ");
        _sb.AppendLine("            FROM   CAGE_SESSIONS                      ");
        _sb.AppendLine("           WHERE   CGS_CLOSE_DATETIME IS NULL         ");

        if (Misc.IsGamingDayEnabled())
        {
          if (GamingDay != DateTime.MinValue)
          {
            _sb.AppendLine("           AND   CGS_WORKING_DAY = @pWorkingDay     ");
          }
        }

        _sb.AppendLine("        ORDER BY   CGS_OPEN_DATETIME" + _order_by);

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          if (Misc.IsGamingDayEnabled() && GamingDay != DateTime.MinValue)
          {
            _cmd.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = GamingDay;
          }
          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            CageSessionId = (Int64)_obj;

            return (CageSessionId > 0);
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCageDefaultSessionId

    //// PURPOSE: Updates movement
    ////
    //// PARAMS:
    ////     - Status
    ////     - Trx
    ////
    //// RETURNS:
    ////     - db result
    ////
    //// NOTES:
    public static bool UpdateCageMovementStatus(Int64 CageMovementId, CageStatus Status, Int64 UserId, Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      StringBuilder _sb_cancelations_params;
      Boolean _result;

      _result = true;
      _sb = new StringBuilder();
      _sb_cancelations_params = new StringBuilder();

      if (Status == Cage.CageStatus.CanceledUncollectedCashierRetirement)
      {
        _sb_cancelations_params.AppendLine("       , CGM_CANCELLATION_USER_ID = @pUserCageIdLastModification  ");
        _sb_cancelations_params.AppendLine("       , CGM_CANCELLATION_DATETIME = @pDateTimeLastModification   ");
      }

      if (CageMovementId == 0)
      {
        _sb.AppendLine(" DECLARE @cgm_type AS INT");
        _sb.AppendLine(" DECLARE @cgm_status AS INT");
        _sb.AppendLine(" DECLARE @cgm_cage_movement_id AS BIGINT");

        _sb.AppendLine("  SELECT   TOP 1");
        _sb.AppendLine("           @cgm_cage_movement_id = CGM_MOVEMENT_ID");
        _sb.AppendLine("         , @cgm_type = CGM_TYPE");
        _sb.AppendLine("         , @cgm_status = CGM_STATUS");
        _sb.AppendLine("    FROM   CAGE_MOVEMENTS");
        _sb.AppendLine("   WHERE   CGM_CASHIER_SESSION_ID = @CashierSessionId");
        _sb.AppendLine("ORDER BY   CGM_MOVEMENT_ID DESC");

        if (Cage.IsCageAutoMode())
        {
          _sb.AppendLine("IF  @cgm_type = 100 ");
        }
        else
        {
          _sb.AppendLine("IF  @cgm_type = 100 AND @cgm_status = 0");
        }

        _sb.AppendLine("  UPDATE   CAGE_MOVEMENTS");
        _sb.AppendLine("     SET   CGM_STATUS = @pStatus");
        _sb.AppendLine(_sb_cancelations_params.ToString());
        _sb.AppendLine("   WHERE   CGM_MOVEMENT_ID = @cgm_cage_movement_id");
      }
      else
      {
        _sb.AppendLine("  UPDATE   CAGE_MOVEMENTS");
        _sb.AppendLine("     SET   CGM_STATUS = @pStatus");
        _sb.AppendLine(_sb_cancelations_params.ToString());
        _sb.AppendLine("   WHERE   CGM_MOVEMENT_ID = @pMovementId");
      }
      _sb.AppendLine("     AND   CGM_STATUS <> @pCanceledStatus");

      if (CageMovementId == 0)
      {
        if (Status == Cage.CageStatus.CanceledUncollectedCashierRetirement)
        {
          _sb.AppendLine(" SET NOCOUNT ON");
          _sb.AppendLine(" DELETE FROM CAGE_PENDING_MOVEMENTS WHERE CPM_MOVEMENT_ID = @cgm_cage_movement_id ");
          _sb.AppendLine(" SET NOCOUNT OFF");
        }
      }

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;
          _cmd.Parameters.Add("@pCanceledStatus", SqlDbType.Int).Value = CageStatus.CanceledUncollectedCashierRetirement;
          _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = CageMovementId;
          _cmd.Parameters.Add("@pUserCageIdLastModification", SqlDbType.BigInt).Value = UserId;
          _cmd.Parameters.Add("@pDateTimeLastModification", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@CashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            _result = false;
          }
        }
      }
      catch// (Exception ex)
      {
        // Trace.WriteLine(ex.ToString());
        //Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, 
        //                      "cls_cage_control",
        //                      "UpdateMovementStatus", 
        //                      ex.Message); 
        _result = false;
      }


      return _result;
    }

    public static Int64 GetAccountOperatioByCageMovement(Int64 CageMovmentId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      object _result;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   AO_OPERATION_ID");
      _sb.AppendLine("FROM   ACCOUNT_OPERATIONS");
      _sb.AppendLine("INNER JOIN   CASHIER_MOVEMENTS ON AO_OPERATION_ID = CAShier_movements.cm_operation_id");
      _sb.AppendLine("INNER JOIN   CAGE_CASHIER_MOVEMENT_RELATION ON CAGE_CASHIER_MOVEMENT_RELATION.CM_MOVEMENT_ID = CASHIER_MOVEMENTS.CM_MOVEMENT_ID");
      _sb.AppendLine("INNER JOIN   CAGE_MOVEMENTS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_CASHIER_MOVEMENT_RELATION.CGM_MOVEMENT_ID");
      _sb.AppendLine("WHERE   CAGE_CASHIER_MOVEMENT_RELATION.CGM_MOVEMENT_ID = @pCageMovementId");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pCageMovementId", SqlDbType.BigInt).Value = CageMovmentId;

        _result = _sql_cmd.ExecuteScalar();

        if (_result == null || (Int64)_result == 0)
        {
          _result = -1;
        }
      }

      return (Int64)_result;
    }

    public static long GetCageMovementByAccountOperation(long AccountOperationID, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      object _result;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT  CAGE_MOVEMENTS.CGM_MOVEMENT_ID                                                                                           ");
      _sb.AppendLine("FROM   ACCOUNT_OPERATIONS                                                                                                        ");
      _sb.AppendLine("INNER JOIN   CASHIER_MOVEMENTS ON AO_OPERATION_ID = CAShier_movements.cm_operation_id                                            ");
      _sb.AppendLine("INNER JOIN   CAGE_CASHIER_MOVEMENT_RELATION ON CAGE_CASHIER_MOVEMENT_RELATION.CM_MOVEMENT_ID = CASHIER_MOVEMENTS.CM_MOVEMENT_ID  ");
      _sb.AppendLine("INNER JOIN   CAGE_MOVEMENTS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_CASHIER_MOVEMENT_RELATION.CGM_MOVEMENT_ID                   ");
      _sb.AppendLine("WHERE  ACCOUNT_OPERATIONS.AO_OPERATION_ID = @pAccountOperationID                                                                 ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pAccountOperationID", SqlDbType.BigInt).Value = AccountOperationID;

        _result = _sql_cmd.ExecuteScalar();

        if (_result == null || (long)_result == 0)
        {
          _result = -1;
        }
      }

      return (long)_result;
    }

    /// <summary>
    /// Get Cage Movement Id & status by Account Operation
    /// </summary>
    /// <param name="AccountOperationID"></param>
    /// <param name="CageMovementId"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean GetCageMovementStatusByAccountOperation(Int64 AccountOperationID, out Int64 CageMovementId, out Cage.CageStatus Status, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      CageMovementId = 0;
      Status = CageStatus.OK;

      try
      {

        _sb.AppendLine("    SELECT   CAGE_MOVEMENTS.CGM_MOVEMENT_ID                                                                                           ");
        _sb.AppendLine("           , CAGE_MOVEMENTS.CGM_STATUS                                                                                                ");
        _sb.AppendLine("      FROM   ACCOUNT_OPERATIONS                                                                                                       ");
        _sb.AppendLine("INNER JOIN   CASHIER_MOVEMENTS ON AO_OPERATION_ID = CAShier_movements.cm_operation_id                                                 ");
        _sb.AppendLine("INNER JOIN   CAGE_CASHIER_MOVEMENT_RELATION ON CAGE_CASHIER_MOVEMENT_RELATION.CM_MOVEMENT_ID = CASHIER_MOVEMENTS.CM_MOVEMENT_ID       ");
        _sb.AppendLine("INNER JOIN   CAGE_MOVEMENTS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_CASHIER_MOVEMENT_RELATION.CGM_MOVEMENT_ID                        ");
        _sb.AppendLine("     WHERE   ACCOUNT_OPERATIONS.AO_OPERATION_ID = @pAccountOperationID                                                                ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountOperationID", SqlDbType.BigInt).Value = AccountOperationID;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              CageMovementId = _reader.GetInt64(0);
              Status = (Cage.CageStatus)_reader.GetInt32(1);
            }
          }
        }

        if (CageMovementId > 0)
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    }

    public static Boolean IsMoneyRequestPending()
    {
      DataTable _dt;

      _dt = CheckPendingMovements((Int16)MovTypeToShow.CashierMoneyRequest);

      if (_dt.Rows.Count > 0)
      {
        return true;
      }
      else
      {
        return false;
      }

    }// IsMoneyRequestPending

    public static DataTable CheckPendingMovements(Int16 MovType)
    {
      StringBuilder _sb;
      DataTable _cage_movements;

      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;

      // Get Sql Connection 
      _sql_conn = WSI.Common.WGDB.Connection();
      _sql_trx = _sql_conn.BeginTransaction();

      _cage_movements = new DataTable();

      try
      { //Se puede hacer un case con el MovementsTypeForAlert para hacer la busq. mas modular

        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT  CGM_MOVEMENT_ID                                  ");
        _sb.AppendLine("          ,CGM_CAGE_SESSION_ID                              ");
        _sb.AppendLine("          ,CGM_TYPE                                         ");
        _sb.AppendLine("     FROM  CAGE_MOVEMENTS                                   ");

        switch (MovType)
        {
          case (Int16)MovTypeToShow.AllPending:
            _sb.AppendLine("    WHERE  (CGM_STATUS = @pStatusSent AND CGM_TYPE = @pRequestOp)");
            _sb.AppendLine("    OR (CGM_STATUS = @pStatusSent AND CGM_TYPE IN (@pFromCashier, @pFromGamingTable, @pFromCashierClosing, @pFromGamingTableClosing, @pFromGamingTableDropbox, @pFromGamingTableDropboxWithExpected))");
            break;

          case (Int16)MovTypeToShow.CashierMoneyRequest:
            _sb.AppendLine("    WHERE  (CGM_STATUS IN (200) OR (CGM_STATUS = 0 AND CGM_TYPE = @pRequestOp))");
            break;

          case (Int16)MovTypeToShow.MoneyCollect:
            _sb.AppendLine("    WHERE  (CGM_STATUS = 0 AND CGM_TYPE IN (@pFromCashier, @pFromGamingTable, @pFromCashierClosing, @pFromGamingTableClosing, @pFromGamingTableDropbox, @pFromGamingTableDropboxWithExpected))");
            break;

          case (Int16)MovTypeToShow.None:
          default:
            break;
        }

        _sb.AppendLine(" ORDER BY  CGM_MOVEMENT_DATETIME DESC, CGM_MOVEMENT_ID DESC ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx))
        {
          _sql_command.Parameters.Add("@pRequestOp", SqlDbType.BigInt).Value = CageOperationType.RequestOperation;
          _sql_command.Parameters.Add("@pFromCashier", SqlDbType.BigInt).Value = CageOperationType.FromCashier;
          _sql_command.Parameters.Add("@pFromGamingTable", SqlDbType.Int).Value = CageOperationType.FromGamingTable;
          _sql_command.Parameters.Add("@pFromCashierClosing", SqlDbType.Int).Value = CageOperationType.FromCashierClosing;
          _sql_command.Parameters.Add("@pFromGamingTableClosing", SqlDbType.Int).Value = CageOperationType.FromGamingTableClosing;
          _sql_command.Parameters.Add("@pFromGamingTableDropbox", SqlDbType.Int).Value = CageOperationType.FromGamingTableDropbox;
          _sql_command.Parameters.Add("@pFromGamingTableDropboxWithExpected", SqlDbType.Int).Value = CageOperationType.FromGamingTableDropboxWithExpected;
          _sql_command.Parameters.Add("@pStatusSent", SqlDbType.Int).Value = CageStatus.Sent;

          using (SqlDataReader _reader = _sql_command.ExecuteReader())
          {
            _cage_movements.Load(_reader);
          }
        } // SqlCommand
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _cage_movements;
    } //CheckPendingMovements

    public static DateTime GetCageSessionWorkingDayById(Int64 CageSessionId, SqlTransaction Trx)
    {
      String _sql_txt = String.Empty;
      DateTime _session_gaming_day = DateTime.MinValue;
      object _obj;

      // Search for an open session
      _sql_txt = " SELECT   CGS_WORKING_DAY                              " +
                 "   FROM   CAGE_SESSIONS                                " +
                 "  WHERE   CGS_CAGE_SESSION_ID  = @pCageSessionId       ";

      using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
      {
        _sql_cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = CageSessionId;

        _obj = _sql_cmd.ExecuteScalar();

        if (_obj != null && _obj != DBNull.Value)
        {
          _session_gaming_day = (DateTime)_obj;
        }
      }

      return _session_gaming_day;
    }

    public static Boolean CheckSameGamingDay(Int64 CageSessionId, Int32 UserId, SqlTransaction Trx)
    {
      return CheckSameGamingDay(CageSessionId, UserId, false, Trx);
    }

    public static Boolean CheckSameGamingDay(Int64 CageSessionId, Int32 UserId, Boolean IsTerminal, SqlTransaction Trx)
    {
      DateTime _cage_session_working_day = GetCageSessionWorkingDayById(CageSessionId, Trx);
      if (_cage_session_working_day == DateTime.MinValue)
      {
        return false;
      }

      return CheckSameGamingDay(_cage_session_working_day, UserId, IsTerminal, Trx);
    }

    public static Boolean CheckSameGamingDay(DateTime GamingDay, Int32 UserId, SqlTransaction Trx)
    {
      return CheckSameGamingDay(GamingDay, UserId, false, Trx);
    }

    public static Boolean CheckSameGamingDay(DateTime GamingDay, Int32 UserId, Boolean IsTerminal, SqlTransaction Trx)
    {
      String _sql_txt = String.Empty;
      DateTime _session_gaming_day = DateTime.MinValue;
      object _obj;

      // Search for an open session
      _sql_txt = " SELECT   CS_GAMING_DAY                                " +
                 "   FROM   CASHIER_SESSIONS                             " +
                 "  WHERE   CS_STATUS  = @pStatusOpen                    ";

      if (!IsTerminal)
      {
        _sql_txt += "    AND   CS_USER_ID = @pId                           ";
      }
      else
      {
        _sql_txt += "    AND   CS_CASHIER_ID = @pId                        ";
      }

      using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
      {
        _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
        _sql_cmd.Parameters.Add("@pId", SqlDbType.Int).Value = UserId;

        _obj = _sql_cmd.ExecuteScalar();

        if (_obj != null && _obj != DBNull.Value)
        {
          _session_gaming_day = (DateTime)_obj;

          if (_session_gaming_day == GamingDay)
          {
            // Same gaming day
            return true;
          }
          else
          {
            // Different gaming day
            return false;
          }
        }
        else
        {
          // No session
          return true;
        }
      }

    } // CheckSameGamingDay

    public static Boolean GamingDayHasOpenedCashierSessions(DateTime GamingDay)
    {
      String _sql_txt = String.Empty;
      DateTime _session_gaming_day = DateTime.MinValue;
      object _obj;

      _sql_txt = " SELECT   TOP 1 CS_SESSION_ID                          " +
                 "   FROM   CASHIER_SESSIONS                             " +
                 "  WHERE   CS_STATUS  = @pStatusOpen                    " +
                 "    AND   CS_CLOSING_DATE IS NULL                      " +
                 "    AND   CS_GAMING_DAY = @pGamingDay                  ";

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
            _sql_cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = GamingDay;

            _obj = _sql_cmd.ExecuteScalar();

            return (_obj != null && _obj != DBNull.Value);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets total deposit expected for flash report
    //
    //  PARAMS :
    //      - INPUT :
    //            - DateTime: Day
    //            - SqlTransaction
    //
    //      - OUTPUT :
    //            - Decimal:  DepositExpected
    // RETURNS :
    //      - True if the operation goes well
    //
    //   NOTES :
    //
    public static Boolean GetTotalDepositExpected(out Decimal DepositExpected, DateTime Day, SqlTransaction SqlTrx)
    {
      Boolean _result;
      StringBuilder _sb;
      Object _obj;

      _result = false;
      DepositExpected = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   SUM(CMD_QUANTITY * CMD_DENOMINATION) AS TOTAL_ISO_NATIONAL                     ");
        _sb.AppendLine("  FROM   CAGE_MOVEMENT_DETAILS                                                          ");
        _sb.AppendLine(" WHERE   CMD_MOVEMENT_ID                                                                ");
        _sb.AppendLine("    IN   (SELECT  CGM_MOVEMENT_ID                                                       ");
        _sb.AppendLine("            FROM  CAGE_MOVEMENTS                                                        ");
        _sb.AppendLine("           WHERE  CGM_TYPE = 0                                                          ");
        _sb.AppendLine("             AND  convert(varchar, cgm_movement_datetime, 112) = @pDay)                 ");
        _sb.AppendLine("   AND   CMD_QUANTITY > 0                                                               ");
        _sb.AppendLine("   AND   CMD_CAGE_CURRENCY_TYPE NOT IN (@pChipsRe, @pChipsNR, @pChipsColor)             ");
        _sb.AppendLine("   AND   CMD_ISO_CODE = @pCurrencyCode                                                  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pDay", SqlDbType.NVarChar).Value = Day.ToString("yyyyMMdd");
          _sql_cmd.Parameters.Add("@pCurrencyCode", SqlDbType.NVarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
          _sql_cmd.Parameters.Add("@pChipsRe", SqlDbType.Int).Value = CageCurrencyType.ChipsRedimible;
          _sql_cmd.Parameters.Add("@pChipsNR", SqlDbType.Int).Value = CageCurrencyType.ChipsNoRedimible;
          _sql_cmd.Parameters.Add("@pChipsColor", SqlDbType.Int).Value = CageCurrencyType.ChipsColor;
          _obj = _sql_cmd.ExecuteScalar();
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          DepositExpected = (Decimal)_obj;
          _result = true;
        }
        else
        {
          _result = false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _result;
    }

    public static bool DeleteFromPendingMovement(long movementID, SqlTransaction sqlTrx)
    {
      StringBuilder _sb;
      bool _operation_ok;

      _operation_ok = true;
      _sb = new StringBuilder();

      _sb.AppendLine("DELETE FROM    CAGE_PENDING_MOVEMENTS ");
      _sb.AppendLine("      WHERE    CPM_MOVEMENT_ID = @pMovementId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), sqlTrx.Connection, sqlTrx))
      {
        _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = movementID;
        try
        {
          if (_cmd.ExecuteNonQuery() != 1)
          {
            _operation_ok = false;
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          _operation_ok = false;
        }
      }
      return _operation_ok;

    }

    public static bool UpdateCageStock(long cageMovementId, SqlTransaction sqlTrx)
    {
      DataTable _cage_movement_details;
      string _sql;

      if (!GetCageMovementDetails(cageMovementId, out _cage_movement_details, sqlTrx))
      {
        return false;
      }

      foreach (DataRow _row in _cage_movement_details.Rows)
      {
        _sql = WSI.Common.GamingHall.CageStock.GetSqlCommandUpdateCageStock(
          FeatureChips.IsCurrencyExchangeTypeChips((CurrencyExchangeType)_row["CMD_CAGE_CURRENCY_TYPE"]),
                                                    (decimal)_row["CMD_DENOMINATION"], CageOperationType.ToTerminal, true);

        if (!ExecuteUpdateCageStock(_row, _sql, sqlTrx))
        {
          return false;
        }
      }

      return true;
    }

    private static bool ExecuteUpdateCageStock(DataRow row, string strQuery, SqlTransaction sqlTrx)
    {
      bool _success;
      _success = false;

      try
      {
        using (SqlCommand _cmd = new SqlCommand(strQuery, sqlTrx.Connection, sqlTrx))
        {
          _cmd.Parameters.Add("@pISOCode", SqlDbType.NVarChar, 3).Value = row["CMD_ISO_CODE"];
          _cmd.Parameters.Add("@pISOType", SqlDbType.Int).Value = row["CMD_CAGE_CURRENCY_TYPE"];
          _cmd.Parameters.Add("@pColor", SqlDbType.Int).Value = -1;
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar, 50).Value = string.Empty;
          _cmd.Parameters.Add("@pDrawing", SqlDbType.NVarChar, 50).Value = string.Empty;
          _cmd.Parameters.Add("@pChipId", SqlDbType.BigInt).Value = row.IsNull("CMD_CHIP_ID") ? 0 : row["CMD_CHIP_ID"];

          if ((int)row["CMD_QUANTITY"] == Cage.COINS_CODE)
          {
            _cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = (int)CageCurrencyType.Bill;
            _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).Value = row["CMD_QUANTITY"];
            _cmd.Parameters.Add("@pQuantity", SqlDbType.Decimal).Value = decimal.Parse(row["CMD_DENOMINATION"].ToString()) * -1;
          }
          else
          {
            _cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = row["CMD_CAGE_CURRENCY_TYPE"];
            _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).Value = row["CMD_DENOMINATION"];
            _cmd.Parameters.Add("@pQuantity", SqlDbType.Decimal).Value = decimal.Parse(row["CMD_QUANTITY"].ToString()) * -1;
          }

          _cmd.ExecuteNonQuery();

          _success = true;
        }
      }
      catch (Exception ex)
      {
        _success = false;
        Log.Exception(ex);
      }

      return _success;
    }

    private static bool GetCageMovementDetails(long cageMovementId, out DataTable dt, SqlTransaction sqlTrx)
    {
      dt = new DataTable();

      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT                                  ");
        _sb.AppendLine("    CMD_QUANTITY                        ");
        _sb.AppendLine("  , CMD_DENOMINATION                    ");
        _sb.AppendLine("  , CMD_ISO_CODE                        ");
        _sb.AppendLine("  , CMD_CHIP_ID                         ");
        _sb.AppendLine("  , CMD_CAGE_CURRENCY_TYPE              ");
        _sb.AppendLine("FROM CAGE_MOVEMENT_DETAILS              ");
        _sb.AppendLine("WHERE CMD_MOVEMENT_ID = @pMovementId    ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), sqlTrx.Connection, sqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = cageMovementId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(dt);
          }
        }

        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// List of cage movements status that are collected
    /// </summary>
    /// <returns></returns>
    public static CageStatus[] CageMovementsCollectedStatusList()
    {
      return new CageStatus[] {CageStatus.OK,
                               CageStatus.OkDenominationImbalance,
                               CageStatus.OkAmountImbalance
                              };
    }

    /// <summary>
    /// List of cage movements status that are collected as String
    /// </summary>
    /// <returns></returns>
    public static String GetCageMovementsCollectedStatusListToString()
    {
      return String.Join(", ", Array.ConvertAll(CageMovementsCollectedStatusList(), item => ((int)item).ToString()));
    }

    /// <summary>
    /// List of cage movements operation type that are collected
    /// </summary>
    /// <returns></returns>
    public static CageOperationType[] CageMovementsClosingCollectedOperationTypeList()
    {
      return new CageOperationType[] {CageOperationType.FromCashierClosing,
                                      CageOperationType.FromGamingTableClosing,
                                      CageOperationType.FromGamingTableDropbox,
                                      CageOperationType.FromGamingTableDropboxWithExpected
                                     };
    }

    /// <summary>
    /// List of cage movements operation type that are collected as String
    /// </summary>
    /// <returns></returns>
    public static String GetCageMovementsClosingCollectedOperationTypeListToString()
    {
      return String.Join(", ", Array.ConvertAll(CageMovementsClosingCollectedOperationTypeList(), item => ((int)item).ToString()));
    }

    /// <summary>
    /// Return if exists collected cage movements for te cashier session
    /// </summary>
    /// <param name="SessionInfo"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean ExistsCollectedCageMovementsForCashierSession(CashierSessionInfo SessionInfo, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;
      Int32 _num_collected_movs;

      _num_collected_movs = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT  COUNT(*)  AS  NUM_COLLECTED_MOVS             ");
        _sb.AppendLine("     FROM  CAGE_MOVEMENTS                               ");
        _sb.AppendLine("    WHERE  CGM_CASHIER_SESSION_ID  = @pCashierSessionId ");
        _sb.AppendLine("      AND  CGM_STATUS IN (" + GetCageMovementsCollectedStatusListToString() + ") ");
        _sb.AppendLine("      AND  CGM_TYPE IN (" + GetCageMovementsClosingCollectedOperationTypeListToString() + ") ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = SessionInfo.CashierSessionId;

          _obj = _sql_command.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _num_collected_movs = ((Int32)_obj);
          }
        } // SqlCommand
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return (_num_collected_movs > 0);
    }
  }
}
