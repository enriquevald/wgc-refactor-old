﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchGift.cs
// 
//      DESCRIPTION: Class to get the player gift request
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 31-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-OCT-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.InTouch
{
  public class InTouchGift
  {
    #region " Constants " 
    private const string DB_GIFT_ID         = "GI_GIFT_ID";
    private const string DB_GI_GIFT_TYPE    = "GI_GIFT_TYPE";
    private const string DB_GI_GIFT_POINTS  = "LEVEL";
    #endregion

    #region " Properties "
    public Int64 GiftId { get; set; }
    public Int32 GiftType { get; set; }
    public Decimal GiftPoints { get; set; }
    #endregion 

    #region " Constructors "
    public InTouchGift()
    {

    }
    public InTouchGift (Int64 PromotionId, String DataAccountLevel)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        this.DB_GetGift(PromotionId, DataAccountLevel, _db_trx.SqlTransaction);
      }
    }
    #endregion

    #region " Private Functions "
    /// <summary>
    /// Function that returns Query for search requested player gift
    /// </summary>
    /// <param name="DataAccountLevel">Set level points that returns in the query</param>
    /// <returns></returns>
    private String GetGiftsRequested_Query(String DataAccountLevel)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("      SELECT    GIFTS.GI_GIFT_ID                                  ");
      _sb.Append("                 ,                                                    ");
      _sb.Append(DataAccountLevel);
      _sb.AppendLine("         AS   LEVEL                                               "); 
      _sb.AppendLine("       FROM   PROMOTIONS                                          ");
      _sb.AppendLine("  LEFT JOIN   PROMOGAMES                                          ");
      _sb.AppendLine("         ON   PG_ID = PM_PROMOGAME_ID                             ");
      _sb.AppendLine("  LEFT JOIN   GIFTS                                               ");
      _sb.AppendLine("         ON   GI_POINTS_TO_CREDITS_ID =  PM_POINTS_TO_CREDITS_ID  ");
      _sb.AppendLine("      WHERE   PM_ENABLED = 1                                      ");
      _sb.AppendLine("        AND   GETDATE() BETWEEN PM_DATE_START AND PM_DATE_FINISH  ");
      _sb.AppendLine("        AND   PM_POINTS_TO_CREDITS_ID  IS NOT NULL                ");
      _sb.AppendLine("        AND   PG_ID IS NOT NULL                                   ");
      _sb.AppendLine("        AND   PM_PROMOTION_ID = @pPromotionId                     ");
       

      return _sb.ToString();
    } // GetGiftsRequested_Query

    /// <summary>
    /// Function that returns the requested gift
    /// </summary>
    /// <param name="PromotionId"></param>
    /// <param name="DataAccountLevel"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetGift(Int64 PromotionId, String DataAccountLevel, SqlTransaction SqlTrx)
    {

      try
      {
        // IMPORTANT: ONLY FOR SAME GAMEID

          using (SqlCommand _cmd = new SqlCommand(this.GetGiftsRequested_Query(DataAccountLevel), SqlTrx.Connection, SqlTrx))
          {
            _cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = PromotionId;
     
              using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
              {
                if (_sql_reader.Read())
                {
                  if (!this.DB_GiftDataMapper(_sql_reader, SqlTrx))
                  {
                    return false;
                  }
                }
            }
          }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("InTouchGiftModel.DB_GetGift -> PromotionId: {0}", PromotionId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetGift

    /// <summary>
    /// Mapper the query with the object
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GiftDataMapper(SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      return DB_GiftDataMapper(this, SqlReader, SqlTrx);
    } // DB_GiftDataMapper

    /// <summary>
    /// Mapper the query with the object
    /// </summary>
    /// <param name="Gifts"></param>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GiftDataMapper(InTouchGift Gifts, SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {

      try
      {
        Gifts.GiftId = SqlReader.GetInt64(SqlReader.GetOrdinal(DB_GIFT_ID));
        Gifts.GiftPoints  = SqlReader.GetDecimal(SqlReader.GetOrdinal(DB_GI_GIFT_POINTS));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_GiftDataMapper");
        Log.Exception(_ex);
      }

      return false;
    } // DB_GiftDataMapper    
  #endregion
  }
  
}
