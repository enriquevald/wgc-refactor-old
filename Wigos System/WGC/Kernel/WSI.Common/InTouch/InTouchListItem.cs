﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchListItem.cs
// 
//      DESCRIPTION: Class to get a List Item to promotions
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 08-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-SEP-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.InTouch
{
  public class InTouchListItem
  {
    #region " Constants " 

    // Private constants
    private const Int32 DB_PROMOTION_ID          = 0;
    private const Int32 DB_PROMOTION_NAME        = 1;
    private const Int32 DB_PROMOTION_TYPE        = 2;
    private const Int32 DB_PROMOTION_DATE        = 3;
    private const Int32 DB_PROMOGAME_ID          = 4;
    private const Int32 DB_PROMOGAME_URL         = 5;
    private const Int32 DB_LEVEL                 = 6;
    private const Int32 DB_CREDITS               = 7;
    private const Int32 DB_PROMOGAME_UNITS_TYPE  = 8;
    private const Int32 DB_GI_AWARD_ON_PROMOGAME = 9;
    private const Int32 DB_GI_GIFT_ID            = 10;

    #endregion

    #region " Properties "

    public Int64 PromotionId { get; set; }
    public String PromotionName { get; set; }
    public Int32 PromotionType { get; set; }
    public DateTime  PromotionServerDate { get; set; }
    public String PromotionDate { get; set; }
    public Int64 PromoGameId { get; set; }
    public String  PromoGameUrl { get; set; }
    public String ItemRedirect { get; set; }
    public Decimal LevelValue{ get; set; }
    public Decimal CreditsValue { get; set; }
    public String PrizeValue { get; set; }
    public String EnableButton { get; set; }
    public Int64 PromoGameReturnUnitsId { get; set; }
    public Boolean  CanShow { get; set; }
    public Int32 GiftId { get; set; }
    #endregion

    #region  " Constructor "

    public InTouchListItem()
    { 
    }

    public List<InTouchListItem> GetAllRewardItemList(Int64 AccountId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.GetAllRewardItemList(_db_trx.SqlTransaction, AccountId);
      }
    } // GetAllRewardItemList

    public List<InTouchListItem> GetAllRewardItemList(SqlTransaction SqlTrx,Int64 AccountId)
    {
      return this.DB_GetAllRewardListItem(SqlTrx, AccountId);
    } // GetAllRewardItemList

    public List<InTouchListItem> GetAllPointsItemList(String DataAccountLevel,Int64 AccountId)
    {
      String _gift_filter;

      using (DB_TRX _db_trx = new DB_TRX())
      {

        _gift_filter =  FilterAvailableGifts(AccountId);

        return this.DB_GetAllPointsListItem(_db_trx.SqlTransaction, DataAccountLevel, _gift_filter);
      }
    } // GetAllPointsItemList

    private String FilterAvailableGifts(Int64 AccountId)
    {
      DataSet _gifts;
      String _gifts_filter;

      _gifts = InitGifts();

      if (GetPlayerGifts(AccountId, _gifts) != WCPPlayerRequestResponseCodes.WCP_RC_WKT_OK)
      {
        return "";
      }

      String _gifts_ids;

      _gifts_ids = String.Empty;

      foreach (DataRow row in _gifts.Tables[0].Rows)
      {

        _gifts_ids += row[0].ToString() + ",";

      }

      _gifts_ids = _gifts_ids.Substring(0, _gifts_ids.Length - 1);

      _gifts_filter = String.Format(" AND GIFTS.GI_GIFT_ID IN ({0})", _gifts_ids);

      return _gifts_filter;
    }


    public List<InTouchListItem> GetAllPointsItemList(SqlTransaction SqlTrx, String DataAccountLevel)
    {
      return this.DB_GetAllPointsListItem(SqlTrx, DataAccountLevel, string.Empty);
    } // GetAllPointsItemList

    #endregion 

    #region " Private Methods "

    /// <summary>
    /// Get a list of rewards Items
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<InTouchListItem> DB_GetAllRewardListItem(SqlTransaction SqlTrx,Int64 AccountId)
    {
      List<InTouchListItem> _promogame_list;
      InTouchListItem _promogame;

      _promogame_list = new List<InTouchListItem>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_PromoListItemQuery_Select(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = PromoGame.GameType.PlayReward;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _promogame = this.ListItemMappFromSqlReader(_sql_reader, SqlTrx);

              if (_promogame != null)
              {
                _promogame_list.Add(_promogame);
              }
            }
          }

          return _promogame_list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("PromoGame.DB_GetAllPromoGame --> Error on read all Promo Games list");
        Log.Exception(_ex);
      }

      return null;

    } // DB_GetAllRewardListItem

    /// <summary>
    /// Query to obtain a PromoListItems
    /// </summary>
    /// <returns></returns>
    private String DB_PromoListItemQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("DECLARE @Now AS DATETIME ");
      _sb.AppendLine();
      _sb.AppendLine("SET @Now = GETDATE() ");

      _sb.AppendLine("     SELECT    PM_PROMOTION_ID                      "); //0 
      _sb.AppendLine("             , PM_NAME                              "); //1
      _sb.AppendLine("             , PM_TYPE                              "); //2
      _sb.AppendLine("             , PM_DATE_FINISH                       "); //3
      _sb.AppendLine("             , PROMOGAMES.PG_ID                     "); //4
      _sb.AppendLine("             , PROMOGAMES.PG_GAME_URL               "); //5
      _sb.AppendLine("             , NULL                                 "); //6
      _sb.AppendLine("             , NULL                                 "); //7
      _sb.AppendLine("             , PROMOGAMES.PG_RETURN_UNITS           "); //8
      _sb.AppendLine("        FROM   PROMOTIONS                           ");
      _sb.AppendLine("   LEFT JOIN   PROMOGAMES                           ");
      _sb.AppendLine("          ON   PG_ID = PM_PROMOGAME_ID              ");
      _sb.AppendLine("  INNER JOIN   ACCOUNT_PROMOTIONS                   ");
      _sb.AppendLine("          ON   PM_PROMOTION_ID  = ACP_PROMO_ID      ");
      _sb.AppendLine("       WHERE   PM_ENABLED = 1                       ");
      _sb.AppendLine("         AND   (PM_TYPE IN (4,5,6,9)                ");
      _sb.AppendLine("         AND    ACP_STATUS = 12                     ");
      _sb.AppendLine("         AND    ACP_ACCOUNT_ID  = @pAccountId)      "); // STATUS = 12(PENDING_PLAYER) AND PERIODIC PROMOTIONS & PREASSIGNED 
      _sb.AppendLine("         AND   PROMOGAMES.PG_type  = @pType         ");
      _sb.AppendLine("    GROUP BY   PM_PROMOTION_ID , PM_NAME , PM_TYPE  ");                           
      _sb.AppendLine("             , PM_DATE_FINISH                       ");
      _sb.AppendLine("             , PM_NEXT_EXECUTION                    ");
      _sb.AppendLine("             , PROMOGAMES.PG_ID                     ");
      _sb.AppendLine("             , PROMOGAMES.PG_GAME_URL               ");
      _sb.AppendLine("             , PROMOGAMES.PG_RETURN_UNITS           ");
            
      return _sb.ToString();

    } // DB_PromoListItemQuery_Select

    /// <summary>
    /// Load ListItem data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_ListItemDataMapper(SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      return DB_ListItemDataMapper(this, SqlReader, SqlTrx);
    } // DB_ListItemDataMapper

    /// <summary>
    /// Load ListItem data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_ListItemDataMapper(InTouchListItem ListItem, SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      try
      {

        ListItem.PromotionId = SqlReader.GetInt64(DB_PROMOTION_ID);
        ListItem.PromotionName = SqlReader.GetString(DB_PROMOTION_NAME);
        ListItem.PromotionType = SqlReader.GetInt32(DB_PROMOTION_TYPE);
        ListItem.PromotionServerDate= SqlReader.GetDateTime(DB_PROMOTION_DATE);
        ListItem.PromoGameId = SqlReader.IsDBNull(DB_PROMOGAME_ID) ? 0 : SqlReader.GetInt64(DB_PROMOGAME_ID);
        ListItem.PromoGameUrl = SqlReader.IsDBNull(DB_PROMOGAME_URL) ? String.Empty : SqlReader.GetString(DB_PROMOGAME_URL);

        ListItem.PromotionDate = ListItem.PromotionServerDate.ToLongDateString();
        ListItem.LevelValue =  Convert.ToInt32(SqlReader.IsDBNull(DB_LEVEL) ? 0 : SqlReader.GetDecimal(DB_LEVEL));
        ListItem.CreditsValue = Convert.ToInt32(SqlReader.IsDBNull(DB_CREDITS) ? 0 : SqlReader.GetDecimal(DB_CREDITS));
        ListItem.PromoGameReturnUnitsId = SqlReader.IsDBNull(DB_PROMOGAME_UNITS_TYPE) ? 0 : SqlReader.GetInt32(DB_PROMOGAME_UNITS_TYPE);
        ListItem.CanShow = SqlReader.GetBoolean(DB_GI_AWARD_ON_PROMOGAME);
        ListItem.GiftId = SqlReader.IsDBNull(DB_GI_GIFT_ID) ? 0 : SqlReader.GetInt32(DB_GI_GIFT_ID);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_ListItemDataMapper");
        Log.Exception(_ex);
      }

      ListItem = null;

      return false;

    } // DB_ListItemDataMapper 

    /// <summary>
    /// Map ListItem data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private InTouchListItem ListItemMappFromSqlReader(SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      InTouchListItem _listItem;

      _listItem = new InTouchListItem();

      if (!DB_ListItemDataMapper(_listItem, SqlReader, SqlTrx))
      {
        Log.Error(String.Format("InTouchListItem.ListItemMappFromSqlReader -> Error on load PromotionId: {0}", SqlReader.GetInt64(DB_PROMOTION_ID)));
      }

      return _listItem;
    } // ListItemMappFromSqlReader

    /// <summary>
    /// Get a list of rewards Items
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<InTouchListItem> DB_GetAllPointsListItem(SqlTransaction SqlTrx, String DataAccountLevel, String GiftsFilter)
    {
      List<InTouchListItem> _promogame_list;
      InTouchListItem _promogame;

      _promogame_list = new List<InTouchListItem>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_PointsListItemQuery_Select(DataAccountLevel, GiftsFilter), SqlTrx.Connection, SqlTrx))
        {
         // _cmd.Parameters.Add("@pLevel", SqlDbType.NVarChar).Value = DataAccountLevel;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _promogame = this.ListItemMappFromSqlReader(_sql_reader, SqlTrx);

              if (_promogame != null)
              {
                _promogame_list.Add(_promogame);
              }
            }
          }

          return _promogame_list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("PromoGame.DB_GetAllPromoGame --> Error on read all Promo Games list");
        Log.Exception(_ex);
      }

      return null;

    } // DB_GetAllRewardListItem

    /// <summary>
    /// Query to obtain a PointsListItems
    /// </summary>
    /// <returns></returns>
    private String DB_PointsListItemQuery_Select(String DataAccountLevel, String GiftFilter)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("   SELECT    PM_PROMOTION_ID                                    "); //0 
      _sb.AppendLine("           , PM_NAME                                            "); //1
      _sb.AppendLine("           , PM_TYPE                                            "); //2
      _sb.AppendLine("           , PM_DATE_FINISH                                     "); //3
      _sb.AppendLine("           , PROMOGAMES.PG_ID                                   "); //4
      _sb.AppendLine("           , PROMOGAMES.PG_GAME_URL                             "); //5
      _sb.Append("               ,                                                    ");
      _sb.Append(DataAccountLevel);
      _sb.AppendLine("             AS LEVEL                                           "); //6
      _sb.AppendLine("           , GIFTS.GI_CONVERSION_TO_NRC AS CREDITS              "); //7
      _sb.AppendLine("           , PROMOGAMES.PG_RETURN_UNITS                         "); //8
      _sb.AppendLine("           , GIFTS.GI_AWARD_ON_PROMOBOX                         "); //9
      _sb.AppendLine("           , GIFTS.GI_GIFT_ID                                   "); //10
      _sb.AppendLine("      FROM   PROMOTIONS                                         ");
      _sb.AppendLine(" LEFT JOIN   PROMOGAMES                                         ");
      _sb.AppendLine("        ON   PG_ID = PM_PROMOGAME_ID                            ");
      _sb.AppendLine(" LEFT JOIN   GIFTS                                              ");
      _sb.AppendLine("        ON   GI_POINTS_TO_CREDITS_ID =  PM_POINTS_TO_CREDITS_ID ");
      _sb.AppendLine("     WHERE   PM_ENABLED = 1                                     ");
      _sb.AppendLine("       AND   GETDATE() BETWEEN PM_DATE_START AND PM_DATE_FINISH ");
      _sb.AppendLine("       AND   PM_POINTS_TO_CREDITS_ID  IS NOT NULL               ");
      _sb.Append("           AND                                                      ");
      _sb.Append(DataAccountLevel  );
      _sb.Append("                          IS NOT NULL                               ");
      _sb.AppendLine("      AND  PROMOGAMES.PG_ID IS NOT NULL                          ");
      if(GiftFilter.Length > 0)
      {
        _sb.AppendLine(GiftFilter);
      }

      return _sb.ToString();

    } // DB_PromoListItemQuery_Select
#endregion

    #region " Functions from WCP "
    /// <summary>
    ///  Returns a DataSet with all gifts redeemeable and not redeemable filtered by points.
    /// </summary>
    /// <param name="AccountId">Holder account who's asking the gift reedimeables</param>
    /// <param name="Gifts">With 2 DataTables: [DATATABLE_NAME_REEDEMEABLE] = 0 / [DATATABLE_NAME_NONE_REEDEMEABLE] = 1</param>
    /// <returns>Result of operation  </returns>
    private static WCPPlayerRequestResponseCodes GetPlayerGifts(Int64 AccountId, DataSet Gifts)
  {
    Int32 _gp_value;
    Decimal _player_points;
    Decimal _future_player_points;   // Value of prize Higher to show = holder_points * %_shows
    Decimal _future_pct_points;      // Limit % price points to show (%_show)
    Int32 _min_gifts;                // Min Gifts of catalog (if %_shows is defined)
    Int32 _max_gifts;                // Max Gifts of catalog 

    StringBuilder _sb;
    Decimal _price_points;
    DataRow _row;
    Boolean _add_row;
    Boolean _gift_inside_interval;
    DateTime _today;
    DateTime _first_day_of_month;
    Int32 _holder_level;
    Int32 _holder_gender;

    String _gi_points_col;
    String _dr_points_col;

    Boolean _is_vip;

    try
    {
      _sb = new StringBuilder();

      // Get actual balance points from account
      using (DB_TRX _db_trx = new DB_TRX())
      {
        _sb.Length = 0;
        _sb.AppendLine("SELECT   DBO.GETBUCKETVALUE(@pBucketId, @pAccountId) ");
        _sb.AppendLine("        ,AC_HOLDER_LEVEL             ");
        _sb.AppendLine("        ,AC_HOLDER_GENDER            ");
        _sb.AppendLine("        ,ISNULL(AC_HOLDER_IS_VIP, 0) ");
        _sb.AppendLine("  FROM   ACCOUNTS                    ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
        {
          _cmd.Parameters.Add("@pBucketId", SqlDbType.Int).Value = (Int32)Buckets.BucketId.RedemptionPoints;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _db_trx.ExecuteReader(_cmd))
          {
            if (!_reader.Read())
            {
              Gifts.Clear();

              return WCPPlayerRequestResponseCodes.WCP_RC_WKT_ERROR;
            }

            _player_points = _reader.GetDecimal(0);
            _player_points = Math.Max(_player_points, 0);     // Ensure > 0
            _player_points = Math.Floor(_player_points);      // Truncate
            _holder_level = _reader.GetInt32(1);
            _holder_gender = _reader.GetInt32(2);
            _is_vip = _reader.GetBoolean(3);
          }
        }

        _future_player_points = 0;
        _future_pct_points = 0;
        _max_gifts = 0;
        _min_gifts = 0;

        // Get from General Params, % points of over valued gifts, Max/Min Gifts to show
        // FutureGifts.ExtraPointsPct
        if (Int32.TryParse(GeneralParam.Value("WigosKiosk", "FutureGifts.ExtraPointsPct"), out _gp_value))
        {
          _gp_value = Math.Max(_gp_value, 0);

          if (_gp_value != 0)
          {
            // Future Player Points = Player Points + 30%
            _future_pct_points = _gp_value;
            _future_player_points = Math.Floor(_player_points * (1.0m + (Decimal)_gp_value / 100.0m));
          }

          // FutureGifts.MinGifts to Return if % is defined
          if (Int32.TryParse(GeneralParam.Value("WigosKiosk", "FutureGifts.MinCount"), out _min_gifts))
          {
            _min_gifts = Math.Max(0, _min_gifts);
          }
        }

        // FutureGifts.MaxGifts to Return
        if (Int32.TryParse(GeneralParam.Value("WigosKiosk", "FutureGifts.MaxCount"), out _gp_value))
        {
          _max_gifts = Math.Max(0, _gp_value);
        }

        _today = Misc.TodayOpening();
        _first_day_of_month = new DateTime(_today.Year, _today.Month, 1, _today.Hour, _today.Minute, _today.Second);

        _gi_points_col = "GI_POINTS_LEVEL" + _holder_level;
        _dr_points_col = "DR_POINTS_LEVEL" + _holder_level;

        // Get Redeemable Gifts/Draws
        _sb.Length = 0;
        _sb.AppendLine("SELECT   GI_GIFT_ID                                                         ");
        _sb.AppendLine("       , " + _gi_points_col + " AS GI_POINTS                                ");
        _sb.AppendLine("       , CASE WHEN LEN(GI_TEXT_ON_PROMOBOX) > 0 THEN ISNULL (GI_TEXT_ON_PROMOBOX, GI_NAME) ELSE GI_NAME END AS GI_NAME ");
        _sb.AppendLine("       , GI_DESCRIPTION                                                     ");
        _sb.AppendLine("       , GI_SMALL_RESOURCE_ID                                               ");
        _sb.AppendLine("       , GI_LARGE_RESOURCE_ID                                               ");
        _sb.AppendLine("       , GI_TYPE                                                            ");
        _sb.AppendLine("       , GI_CURRENT_STOCK                                                   ");
        _sb.AppendLine("       , 0                                     AS GI_MAX_ALLOWED_UNITS  ");
        _sb.AppendLine("       , GI_CONVERSION_TO_NRC                  AS GI_NON_REDEEMABLE     ");
        _sb.AppendLine("       , 0                                     AS GI_DRAW_ID            ");
        _sb.AppendLine("       , ISNULL(GI_ACCOUNT_DAILY_LIMIT     , 0)AS GI_ACCOUNT_DAILY_LIMIT     ");
        _sb.AppendLine("       , ISNULL(GI_ACCOUNT_MONTHLY_LIMIT   , 0)AS GI_ACCOUNT_MONTHLY_LIMIT  ");
        _sb.AppendLine("       , ISNULL(GI_GLOBAL_DAILY_LIMIT      , 0)AS GI_GLOBAL_DAILY_LIMIT        ");
        _sb.AppendLine("       , ISNULL(GI_GLOBAL_MONTHLY_LIMIT    , 0)AS GI_GLOBAL_MONTHLY_LIMIT    ");
        _sb.AppendLine("       , ISNULL(TOT_REQUESTED.ACC_DAY      , 0)AS REQ_ACCOUNT_DAILY_TOTAL_SPENT    ");
        _sb.AppendLine("       , ISNULL(TOT_REQUESTED.ACC_MONTH    , 0)AS REQ_ACCOUNT_MONTH_TOTAL_SPENT    ");
        _sb.AppendLine("       , ISNULL(TOT_REQUESTED.GLOBAL_DAY   , 0)AS REQ_GLOBAL_DAILY_TOTAL_SPENT    ");
        _sb.AppendLine("       , ISNULL(TOT_REQUESTED.GLOBAL_MONTH , 0)AS REQ_GLOBAL_MONTHLY_TOTAL_SPENT    ");
        _sb.AppendLine("       , 0                                     AS GI_DRAW_LEVEL_FILTER      ");
        _sb.AppendLine("       , 0                                     AS GI_GENDER_FILTER          ");
        _sb.AppendLine("       , ISNULL(GI_VIP, 0)                     AS GI_VIP          ");


        // ADD Limit fields so that this can be compared on the while clause and filter out none-available gifts
        _sb.AppendLine("  FROM   GIFTS                                                        ");
        _sb.AppendLine("  LEFT   JOIN                                                         ");
        _sb.AppendLine("   (SELECT   SUM(CASE WHEN GIN_REQUESTED >= @pTodayOpening AND GIN_ACCOUNT_ID    = @pAccountId THEN GIN_NUM_ITEMS ELSE 0 END) AS ACC_DAY      ");
        _sb.AppendLine("           , SUM(CASE WHEN GIN_ACCOUNT_ID = @pAccountId                                        THEN GIN_NUM_ITEMS ELSE 0 END) AS ACC_MONTH    ");
        _sb.AppendLine("           , SUM(CASE WHEN GIN_REQUESTED >= @pTodayOpening                                     THEN GIN_NUM_ITEMS ELSE 0 END) AS GLOBAL_DAY   ");
        _sb.AppendLine("           , SUM(GIN_NUM_ITEMS)                                                                                               AS GLOBAL_MONTH ");
        _sb.AppendLine("           , GIN_GIFT_ID   ");
        _sb.AppendLine("      FROM   GIFT_INSTANCES                           ");
        _sb.AppendLine("     WHERE   GIN_REQUESTED >= @pFirstDayOfMonth   ");
        _sb.AppendLine("  GROUP BY   GIN_GIFT_ID   ");
        _sb.AppendLine("   ) AS TOT_REQUESTED ON TOT_REQUESTED.GIN_GIFT_ID = GI_GIFT_ID     ");
        _sb.AppendLine("     WHERE   GIFTS.GI_AVAILABLE = 1                                   ");
        _sb.AppendLine("       AND  ( ISNULL(GI_ACCOUNT_DAILY_LIMIT  , 0) = 0 OR GI_ACCOUNT_DAILY_LIMIT   > ISNULL(TOT_REQUESTED.ACC_DAY    , 0))  ");
        _sb.AppendLine("       AND  ( ISNULL(GI_ACCOUNT_MONTHLY_LIMIT, 0) = 0 OR GI_ACCOUNT_MONTHLY_LIMIT > ISNULL(TOT_REQUESTED.ACC_MONTH     , 0))  ");
        _sb.AppendLine("       AND  ( ISNULL(GI_GLOBAL_DAILY_LIMIT   , 0) = 0 OR GI_GLOBAL_DAILY_LIMIT    > ISNULL(TOT_REQUESTED.GLOBAL_DAY  , 0))  ");
        _sb.AppendLine("       AND  ( ISNULL(GI_GLOBAL_MONTHLY_LIMIT , 0) = 0 OR GI_GLOBAL_MONTHLY_LIMIT  > ISNULL(TOT_REQUESTED.GLOBAL_MONTH   , 0)) ");

        _sb.AppendLine("       AND (( GIFTS.GI_TYPE = @pGiftTypeObject  AND  GIFTS.GI_CURRENT_STOCK > 0 AND   GIFTS.GI_CURRENT_STOCK > GIFTS.GI_REQUEST_COUNTER )");
        _sb.AppendLine("               OR   GIFTS.GI_TYPE = @pGiftTypeNRC                      ");
        _sb.AppendLine("               OR   GIFTS.GI_TYPE = @pGiftTypeRC                       ");
        _sb.AppendLine("               OR   GIFTS.GI_TYPE = @pGiftTypeServices                 ");
        _sb.AppendLine("           )                                                           ");
        _sb.AppendLine("       AND  GI_AWARD_ON_PROMOBOX = 1                                   ");
        _sb.AppendLine("     UNION                                                             ");
        _sb.AppendLine("SELECT   0                                AS GI_GIFT_ID            ");
        _sb.AppendLine("       , " + _dr_points_col + "           AS GI_POINTS             ");
        _sb.AppendLine("       , CASE WHEN LEN(DR_TEXT_ON_PROMOBOX) > 0 THEN ISNULL (DR_TEXT_ON_PROMOBOX, DR_NAME) ELSE DR_NAME END AS GI_NAME ");
        _sb.AppendLine("       , DR_NAME                          AS GI_DESCRIPTION        ");    //TODO: CHANGE dr_name by Draw.Descritption
        _sb.AppendLine("       , ISNULL(DR_SMALL_RESOURCE_ID, 0)  AS GI_SMALL_RESOURCE_ID  ");
        _sb.AppendLine("       , ISNULL(DR_LARGE_RESOURCE_ID, 0)  AS GI_LARGE_RESOURCE_ID  ");
        _sb.AppendLine("       , @pGiftTypeDrawNumbers            AS GI_TYPE               ");
        _sb.AppendLine("       , CASE WHEN DR_LAST_NUMBER = -1                             ");    //Case not selled any draw number
        _sb.AppendLine("              THEN DR_MAX_NUMBER - DR_INITIAL_NUMBER + 1           ");
        _sb.AppendLine("              ELSE DR_MAX_NUMBER - DR_LAST_NUMBER                  ");
        _sb.AppendLine("         END AS GI_CURRENT_STOCK                                   ");
        _sb.AppendLine("       , 0                               AS GI_MAX_ALLOWED_UNITS   ");
        _sb.AppendLine("       , DR_NUMBER_PRICE                 AS GI_NON_REDEEMABLE      ");
        _sb.AppendLine("       , DR_ID                           AS GI_DRAW_ID             ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , DR_LEVEL_FILTER                 AS GI_DRAW_LEVEL_FILTER   ");
        _sb.AppendLine("       , DR_GENDER_FILTER                AS GI_GENDER_FILTER       ");
        _sb.AppendLine("       , ISNULL(DR_VIP, 0)               AS GI_VIP                 ");
        _sb.AppendLine("  FROM   DRAWS                                                     ");
        _sb.AppendLine(" WHERE   DR_STARTING_DATE <= GETDATE ()                            ");
        _sb.AppendLine("   AND   DR_ENDING_DATE   >  GETDATE ()                            ");
        _sb.AppendLine("   AND   DR_STATUS        = 0                                      ");
        _sb.AppendLine("   AND   DR_LAST_NUMBER   < DR_MAX_NUMBER                          ");
        _sb.AppendLine("   AND   " + _dr_points_col + " > 0                                ");
        _sb.AppendLine("   AND   DR_AWARD_ON_PROMOBOX = 1                                  ");
        _sb.AppendLine(" ORDER BY GI_POINTS ASC, GI_NAME ASC                               ");


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
        {
          _sql_cmd.Parameters.Add("@pGiftTypeObject", SqlDbType.Int).Value = GIFT_TYPE.OBJECT;
          _sql_cmd.Parameters.Add("@pGiftTypeNRC", SqlDbType.Int).Value = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
          _sql_cmd.Parameters.Add("@pGiftTypeRC", SqlDbType.Int).Value = GIFT_TYPE.REDEEMABLE_CREDIT;
          _sql_cmd.Parameters.Add("@pGiftTypeServices", SqlDbType.Int).Value = GIFT_TYPE.SERVICES;
          _sql_cmd.Parameters.Add("@pGiftTypeDrawNumbers", SqlDbType.Int).Value = GIFT_TYPE.DRAW_NUMBERS;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = _today;
          _sql_cmd.Parameters.Add("@pFirstDayOfMonth", SqlDbType.DateTime).Value = _first_day_of_month;

          using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
          {
            while (_sql_reader.Read())
            {
              if (!Misc.IsVIP(_is_vip, _sql_reader.GetBoolean(21)))
              {
                // Skip
                continue;
              }

              _price_points = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetDecimal(1);

              if (_price_points <= 0)
              {
                // Skip the gift
                continue;
              }

              if (_sql_reader.GetInt64(10) != 0)  // It's a draw
              {
                if (!Misc.IsLevelAllowed(_holder_level, _sql_reader.GetInt32(19))) // it's not availible for this account level
                {
                  // If the draw can't be reached from this account, skip the gift
                  continue;
                }

                if (!Misc.IsGenderAllowed(_holder_gender, (DrawGenderFilter)_sql_reader.GetInt32(20))) // it's not availible for this gender
                {
                  continue;
                }
              }

              //verificar el sexo para el cual esta validado el sorteo

              // Filter gift/draw redeemable or near redeemable
              if (_price_points <= _player_points)
              {
                // Add to Gifts list
                _row = Gifts.Tables[0].NewRow();
              }
              else
              {
                // Add row to Next Gifts
                _add_row = false;
                _gift_inside_interval = false;

                if (_future_pct_points == 0
                  || (_future_player_points > 0 && _price_points <= _future_player_points))
                {
                  _add_row = true;
                  _gift_inside_interval = true;
                }

                if (_future_pct_points > 0
                  && _min_gifts > 0
                  && Gifts.Tables[1].Rows.Count < _min_gifts)
                {
                  _add_row = true;
                }

                // Gift will be added if there is room in the future list
                if (_max_gifts > 0)
                {
                  _add_row = (Gifts.Tables[1].Rows.Count < _max_gifts);
                }

                // Restrictions
                if (_add_row)
                {
                  // Gift should be added, there is room in the future list
                  if (!_gift_inside_interval)
                  {
                    // But it is not in the interval range. 
                    // In this case we will check if we have already the minimum gifts to show. 
                    // If so gift will be excluded from the future list 
                    if (_future_pct_points > 0 && Gifts.Tables[1].Rows.Count >= _min_gifts)
                    {
                      _add_row = false;
                    }
                  }
                }

                if (!_add_row)
                {
                  // Candidate gift are sorted ascending by their value in points 
                  break;
                }

                // Add to Next Gifts list
                _row = Gifts.Tables[1].NewRow();
              }

              _row[0] = _sql_reader.GetInt64(0);
              _row[1] = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetDecimal(1);
              _row[2] = _sql_reader.GetString(2);
              _row[3] = _sql_reader.GetString(3);
              _row[4] = _sql_reader.IsDBNull(4) ? 0 : _sql_reader.GetInt64(4);
              _row[5] = _sql_reader.IsDBNull(5) ? 0 : _sql_reader.GetInt64(5);
              _row[6] = _sql_reader.GetInt32(6);
              _row[7] = _sql_reader.GetInt64(7) > Int32.MaxValue ? Int32.MaxValue : (Int32)_sql_reader.GetInt64(7);
              _row[8] = _sql_reader.GetInt32(8);
              _row[9] = _sql_reader.IsDBNull(9) ? 0 : _sql_reader.GetDecimal(9);
              _row[10] = _sql_reader.GetInt64(10);

              _row[11] = _sql_reader.IsDBNull(11) ? 0 : _sql_reader.GetInt32(11);
              _row[12] = _sql_reader.IsDBNull(12) ? 0 : _sql_reader.GetInt32(12);
              _row[13] = _sql_reader.IsDBNull(13) ? 0 : _sql_reader.GetInt32(13);
              _row[14] = _sql_reader.IsDBNull(14) ? 0 : _sql_reader.GetInt32(14);

              _row[15] = _sql_reader.IsDBNull(15) ? 0 : _sql_reader.GetInt32(15);
              _row[16] = _sql_reader.IsDBNull(16) ? 0 : _sql_reader.GetInt32(16);
              _row[17] = _sql_reader.IsDBNull(17) ? 0 : _sql_reader.GetInt32(17);
              _row[18] = _sql_reader.IsDBNull(18) ? 0 : _sql_reader.GetInt32(18);

              _row.Table.Rows.Add(_row);

            } // while(_sql_reader.Read())

          } // using dataReader
        }   // using sqlCommand

        // Get max units reedeamable for each Gift/Draw
        if (!MaxAllowedUnits(AccountId, Gifts, _db_trx.SqlTransaction))
        {
          Gifts.Clear();

          return WCPPlayerRequestResponseCodes.WCP_RC_WKT_ERROR;
        }
      }     // using transaction

      return WCPPlayerRequestResponseCodes.WCP_RC_WKT_OK;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      Gifts.Clear();

      return WCPPlayerRequestResponseCodes.WCP_RC_WKT_ERROR;
    }
  } // GetPlayerGifts

    /// <summary>
    /// Get limit of Max Redeemeable units for each Gift/Draw, without point limitation
    /// </summary>
    /// <param name="AccountId">Holder account who's asking the gift reedimeables</param>
    /// <param name="Gifts">DataSet where are stored de Gifts/Draws</param>
    /// <param name="SqlTrx"></param>
    /// <returns> 
    /// True if there wasn't any error
    /// False if there was any error
    /// </returns>
    private static Boolean MaxAllowedUnits(Int64 AccountId, DataSet Gifts, SqlTransaction SqlTrx)
    {
      Gift _gift;
      GiftInstance _gift_inst;
      GiftInstance.GIFT_INSTANCE_MSG _gift_inst_msg;
      CardData _card;
      Currency _max_redeem;
      Currency _max_no_redeem;
      Boolean _can_request_no_redeem_credit;
      Boolean _can_request_redeem_credit;

      try
      {
        _card = new CardData();
        CardData.DB_CardGetAllData(AccountId, _card, SqlTrx);

        //Check if current account can request redeem and non redeem credit
        CanRequestCredit(_card, out _can_request_redeem_credit, out _can_request_no_redeem_credit);

        //Check the max NRC and RC for the user
        if (!GiftRequest.MaxBuyableCredits(_card, out _max_redeem, out _max_no_redeem, SqlTrx))
        {
          Log.Error("MaxAllowedUnits. Cannot get max redeem and no redeem  allowed credits for account with Id: " + AccountId + " .");

          return false;
        }

        // 6   --> GI_TYPE                Gift Type
        // 8   --> GI_MAX_ALLOWED_UNITS   Max Units Can Request
        // 9   --> GI_CONVERSION_TO_NRC   Amount Credit Redeem or Not Redeem of the Gift

        foreach (DataRow _dr_gift in Gifts.Tables[0].Rows)
        {
          _dr_gift[8] = 0;

          if (_card.PointsStatus == ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED)
          {
            switch ((GIFT_TYPE)_dr_gift[6])
            {
              case GIFT_TYPE.SERVICES:
              case GIFT_TYPE.OBJECT:
                // TODO: define this parameter in GeneralParams ...
                // Limit 1 per Operation
                _dr_gift[8] = 1;
                break;

              case GIFT_TYPE.DRAW_NUMBERS:
                _gift = new Gift(_dr_gift);
                _gift_inst = new GiftInstance(_gift, _card);

                if (!_gift_inst.CheckDrawNumbersGift(out _gift_inst_msg))
                {
                  _dr_gift[8] = 0;
                }
                else
                {
                  _dr_gift[8] = Math.Floor(_gift_inst.DrawMaxPointsToSpend / _gift_inst.DrawNumberPointsPrice);
                }
                break;

              case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:

                // if account can request NonRedeem Credits
                if (_can_request_no_redeem_credit)
                {
                  _dr_gift[8] = _max_no_redeem == Decimal.MaxValue ? Int32.MaxValue : Math.Floor(_max_no_redeem / (Decimal)_dr_gift[9]);
                }
                else
                {
                  _dr_gift[8] = 0;
                }
                break;

              case GIFT_TYPE.REDEEMABLE_CREDIT:

                // if account can request Redeem Credits
                if (_can_request_redeem_credit)
                {
                  _dr_gift[8] = _max_redeem == Decimal.MaxValue ? Int32.MaxValue : Math.Floor(_max_redeem / (Decimal)_dr_gift[9]);
                }
                else
                {
                  _dr_gift[8] = 0;
                }
                break;

              case GIFT_TYPE.UNKNOWN:
              default:
                //TODO define limit for unknown Gift Type
                Log.Error("MaxAllowedUnits. Unknown Gift Type with Id: " + _dr_gift[0] + " .");
                _dr_gift[8] = 0;
                break;
            }
          }

          _dr_gift[8] = Math.Max(0, (Int32)_dr_gift[8]);

          // Now we check the gift redeem limit per account and globally
          _gift = new Gift(_dr_gift);
          _gift_inst = new GiftInstance(_gift, _card);
          _gift_inst.GetNumOfAvailableGifts((Int32)_dr_gift[15], (Int32)_dr_gift[16], (Int32)_dr_gift[17], (Int32)_dr_gift[18]);
          _dr_gift[8] = Math.Min(_gift_inst.NumAvailable, (Int32)_dr_gift[8]);

        }  // foreach row of redeem Gifts

        // For Near Gift, MaxUnits = Default value 0
      }
      catch (Exception _ex)
      {
        Log.Error("MaxAllowedUnits. Cannot get max allowed units assigned for account with Id: " + AccountId + " .");
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // MaxAllowedUnits

    /// <summary>
    /// Check if Account has reached the allowed RC and NR daily limit
    /// </summary>
    /// <param name="Card">Holder account who's asking </param>
    /// <param name="CanRedeemableCredit">If can get redeem credit</param>
    /// <param name="CanNoRedeemableCredit">If can get not redeem credit</param>
    private static void CanRequestCredit(CardData Card, out Boolean CanRedeemableCredit, out Boolean CanNoRedeemableCredit)
    {
      Gift _gift;
      Gift.GIFT_MSG _dummy_gift_msg;

      //Check if current account can request redeem and non redeem credit
      _gift = new Gift();
      _gift.Available = true;

      _gift.Type = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
      CanRedeemableCredit = _gift.CheckGiftRequest(Card, false, out _dummy_gift_msg);

      _gift.Type = GIFT_TYPE.REDEEMABLE_CREDIT;
      CanNoRedeemableCredit = _gift.CheckGiftRequest(Card, false, out _dummy_gift_msg);

    } // CanRequestCredit

    /// <summary>
    /// Initialize Dataset Gift
    /// </summary>
    /// <returns></returns>
    public DataSet InitGifts()
    {
      DataSet Gifts;
      DataTable _tbl_gift;

      Gifts = new DataSet();

      foreach (String _table_name in new String[] { "RedeemeablesGifts", "NearRedeemableGifts" })
      {
        _tbl_gift = new DataTable(_table_name);

        _tbl_gift.Columns.Add("GI_GIFT_ID", Type.GetType("System.Int64"));
        _tbl_gift.Columns.Add("GI_POINTS", Type.GetType("System.Decimal"));
        _tbl_gift.Columns.Add("GI_NAME", Type.GetType("System.String"));
        _tbl_gift.Columns.Add("GI_DESCRIPTION", Type.GetType("System.String"));
        _tbl_gift.Columns.Add("GI_SMALL_RESOURCE_ID", Type.GetType("System.Int64"));
        _tbl_gift.Columns.Add("GI_LARGE_RESOURCE_ID", Type.GetType("System.Int64"));
        _tbl_gift.Columns.Add("GI_TYPE", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_CURRENT_STOCK", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("MAX_ALLOWED_UNITS", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_NON_REDEEMABLE", Type.GetType("System.Decimal"));
        _tbl_gift.Columns.Add("GI_DRAW_ID", Type.GetType("System.Int64"));

        _tbl_gift.Columns.Add("GI_ACCOUNT_DAILY_LIMIT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_ACCOUNT_MONTHLY_LIMIT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_GLOBAL_DAILY_LIMIT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("GI_GLOBAL_MONTHLY_LIMIT", Type.GetType("System.Int32"));

        _tbl_gift.Columns.Add("REQ_ACCOUNT_DAILY_TOTAL_SPENT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("REQ_ACCOUNT_MONTHLY_TOTAL_SPENT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("REQ_GLOBAL_DAILY_TOTAL_SPENT", Type.GetType("System.Int32"));
        _tbl_gift.Columns.Add("REQ_GLOBAL_MONTHLY_TOTAL_SPENT", Type.GetType("System.Int32"));

        Gifts.Tables.Add(_tbl_gift);
      }

      return Gifts;
    } //InitGifts
    #endregion
  }

}
