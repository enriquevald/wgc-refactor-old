﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: ExtensionMethods.cs
// 
//      DESCRIPTION: Extension Methods
// 
//           AUTHOR: Rubén Lama
// 
//    CREATION DATE: 26-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUL-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.TerminalDraw.Persistence;

namespace WSI.Common.ExtentionMethods
{
  public static class ExtensionMethods
  {

    #region " PlayerLevelIcon "

    /// <summary>
    /// Clone PlayerLevelIcon list 
    /// </summary>
    /// <returns></returns>
    public static List<PlayerLevelIcon> Clone(this List<PlayerLevelIcon> PlayerLevelIcon)
    {
      List<PlayerLevelIcon> _player_level_icon_list = new List<PlayerLevelIcon>();

      foreach (PlayerLevelIcon _player_level_icon in PlayerLevelIcon)
      {
        _player_level_icon_list.Add(_player_level_icon.Clone());
      }

      return _player_level_icon_list;
    } // Clone


    /// <summary>
    /// Save PlayerLevelIcon list 
    /// </summary>
    /// <returns></returns>
    public static List<PlayerLevelIcon> Save(this List<PlayerLevelIcon> PlayerLevelIconList, SqlTransaction SqlTrx)
    {
      foreach (PlayerLevelIcon _icon in PlayerLevelIconList)
      {
        _icon.Save(SqlTrx);
      }

      return PlayerLevelIconList;
    } // Clone

    /// <summary>
    /// Get Icon by Level
    /// </summary>
    /// <returns></returns>
    public static PlayerLevelIcon GetIconByLevel(this List<PlayerLevelIcon> PlayerLevelIconList, PlayerLevelIcon.TypePlayerLevel Level)
    {
      foreach (PlayerLevelIcon _icon in PlayerLevelIconList)
      {
        if (PlayerLevelIcon.ExistsLevelInMask(Level, (Int32)_icon.Level))
        {
          return _icon;
        }
      }

      return PlayerLevelIconList.GetIconByIndex(0);
    } // GetIconByLevel

    /// <summary>
    /// GetIndex by index
    /// </summary>
    /// <returns></returns>
    public static PlayerLevelIcon GetIconByIndex(this List<PlayerLevelIcon> PlayerLevelIconList, Int32 Index)
    {
      foreach (PlayerLevelIcon _icon in PlayerLevelIconList)
      {
        if (_icon.Index == Index)
        {
          return _icon;
        }
      }

      return null;
    } // GetIconByIndex

    #endregion

    #region " PromoGame "

    /// <summary>
    /// Total minimum prize
    /// </summary>
    /// <param name="PromoGames"></param>
    public static Currency TotalMinimumPrize (this List<PromoGame> PromoGames)
    {
      Currency _total_minimum_prize;

      _total_minimum_prize = 0;

      foreach (PromoGame _game in PromoGames)
      {
        _total_minimum_prize += _game.MinimumPrize;
      }

      return _total_minimum_prize;

    } // TotalMinimumPrize

    /// <summary>
    /// Total minimum prize
    /// </summary>
    /// <param name="PromoGames"></param>
    /// <returns></returns>
    public static Currency TotalPrice(this List<PromoGame> PromoGames)
    {
      Currency _total_prize;

      _total_prize = 0;

      foreach (PromoGame _game in PromoGames)
      {
        _total_prize += _game.Price;
      }

      return _total_prize;

    } // TotalPrice

    /// <summary>
    /// Return if is necessary show buy dialog of PromoGames List
    /// </summary>
    /// <param name="PromoGames"></param>
    /// <returns></returns>
    public static Boolean ShowBuyDialog(this List<PromoGame> PromoGames)
    {
      // Show buy dialog
      foreach (PromoGame _game in PromoGames)
      {
        return _game.ShowBuyDialog;
      }

      return false;
    } // ShowBuyDialog

    /// <summary>
    /// Return game type of PromoGames List
    /// </summary>
    /// <param name="PromoGames"></param>
    /// <returns></returns>
    public static PromoGame.GameType GetGameType(this List<PromoGame> PromoGames)
    {
      PromoGame.GameType _type;
      
      if(PromoGames.Count == 0)
      {
        return  PromoGame.GameType.Unknown;
      }

      // Check all types
      _type = PromoGames[0].Type;
      foreach (PromoGame _game in PromoGames)
      {        
        if (_type != _game.Type)
        {
          return PromoGame.GameType.Unknown;
        }
      }

      return _type;

    } // GetGameType

    /// <summary>
    /// Return game ReturnUnits type of PromoGames List
    /// </summary>
    /// <param name="PromoGames"></param>
    /// <returns></returns>
    public static PrizeType GetReturnUnits(this List<PromoGame> PromoGames)
    {
      PrizeType _type;

      if (PromoGames.Count == 0)
      {
        return PrizeType.Unknown;
      }

      // Check all types
      _type = PromoGames[0].ReturnUnits;
      foreach (PromoGame _game in PromoGames)
      {
        if (_type != _game.ReturnUnits)
        {
          return PrizeType.Unknown;
        }
      }

      return _type;
    } // GetReturnUnits

    /// <summary>
    /// Return game PriceUnits type of PromoGames List
    /// </summary>
    /// <param name="PromoGames"></param>
    /// <returns></returns>
    public static PrizeType GetPriceUnits(this List<PromoGame> PromoGames)
    {
      PrizeType _type;

      if (PromoGames.Count == 0)
      {
        return PrizeType.Unknown;
      }

      // Check all types
      _type = PromoGames[0].PriceUnits;
      foreach (PromoGame _game in PromoGames)
      {
        if (_type != _game.PriceUnits)
        {
          return PrizeType.Unknown;
        }
      }

      return _type;
    } // GetPriceUnits

    /// <summary>
    /// Get Game id form List<PromoGame>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Int64 Id(this List<PromoGame> Games)
    {
      foreach (PromoGame _game in Games)
      {
        return _game.Id;
      }

      return 0;
    } // Id

    /// <summary>
    /// Get PromotionId form List<PromoGame>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Int64 PromotionId(this List<PromoGame> Games)
    {
      foreach (PromoGame _game in Games)
      {
        return _game.PromotionId;
      }

      return 0;
    } // PromotionId
    
    /// <summary>
    /// Get GameUrl form List<PromoGame>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static String GameUrl(this List<PromoGame> Games)
    {
      foreach (PromoGame _game in Games)
      {
        return _game.GameURL;
      }

      return String.Empty;
    } // GameUrl

    #endregion 

    #region " TerminalDrawAccountEntity "

    /// <summary>
    /// Check Operation Id form List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Boolean CheckOperationId(this List<TerminalDrawGamesEntity> Games)
    {
      foreach (TerminalDrawGamesEntity _game in Games)
      {
        return (_game.Recharges.OperationId() > 0);
      }

      return true;
    } // CheckOperationId

    /// <summary>
    /// Get operations id form List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static List<Int64> OperationsId(this List<TerminalDrawGamesEntity> Games)
    {
      List<Int64> _operations_id;

      _operations_id = new List<Int64>();

      foreach (TerminalDrawGamesEntity _game in Games)
      {
        _operations_id.AddRange(_game.Recharges.OperationsId());
      }

      return _operations_id;
    } // OperationsId

    /// <summary>
    /// Get game type form List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Int64 Id(this List<TerminalDrawGamesEntity> Games)
    {
      foreach (TerminalDrawGamesEntity _game in Games)
      {
        return _game.Recharges.GameId();
      }

      return 0;
    } // Id

    /// <summary>
    /// Get game type form List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static PromoGame.GameType Type(this List<TerminalDrawGamesEntity> Games)
    {
      foreach (TerminalDrawGamesEntity _game in Games)
      {
        return _game.Recharges.GameType();
      }

      return PromoGame.GameType.Unknown;
    } // Type

    /// <summary>
    /// Get play session id form List<TerminalDrawGamesEntity>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Int64 PlaySessionId(this List<TerminalDrawGamesEntity> Games)
    {
      foreach (TerminalDrawGamesEntity _game in Games)
      {
        if (_game.PlaySessionId > 0)
        {
          return _game.PlaySessionId;
        }
      }

      return 0;
    } // PlaySessionId

    /// <summary>
    /// Get total bet amount form List<TerminalDrawGamesEntity>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Decimal TotalBetAmount(this List<TerminalDrawGamesEntity> Games)
    {
      Decimal _total_bet_amount;

      _total_bet_amount = 0;

      foreach (TerminalDrawGamesEntity _game in Games)
      {
        _total_bet_amount += _game.TotalBetAmount;
      }

      return _total_bet_amount;
    } // TotalBetAmount

    /// <summary>
    /// Get TimeOut form List<TerminalDrawGamesEntity>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Int64 TimeOut(this List<TerminalDrawGamesEntity> Games)
    {
      return (Games.Count > 0) ? Games[0].FirstScreenTimeOut : 0;
    } // TimeOut

    /// <summary>
    /// Get minimum prize amount form List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Decimal TotalMinimumPrize(this List<TerminalDrawGamesEntity> Games)
    {
      Decimal _minimum_prize;

      _minimum_prize = 0;

      foreach (TerminalDrawGamesEntity _game in Games)
      {
        _minimum_prize += _game.Recharges.TotalMinimumPrize();
      }

      return _minimum_prize;
    } // TotalMinimumPrize

    #endregion

    #region " TerminalDrawRechargeEntity "

    /// <summary>
    /// Get minimum prize amount form List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Recharges"></param>
    /// <returns></returns>
    public static Decimal TotalMinimumPrize(this List<TerminalDrawRechargeEntity> Recharges)
    {
      Decimal _minimum_prize;

      _minimum_prize = 0;

      foreach (TerminalDrawRechargeEntity _recharge in Recharges)
      {
        _minimum_prize += _recharge.MinimumPrize;
      }

      return _minimum_prize;
    } // MinimumPrize

    /// <summary>
    /// Get operation id List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Recharges"></param>
    /// <returns></returns>
    public static Int64 OperationId(this List<TerminalDrawRechargeEntity> Recharges)
    {
      foreach (TerminalDrawRechargeEntity _recharge in Recharges)
      {
        return _recharge.OperationId;
      }

      return 0;
    } // OperationId

    /// <summary>
    /// Get operations id form List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static List<Int64> OperationsId(this List<TerminalDrawRechargeEntity> Recharges)
    {
      List<Int64> _operations_id;

      _operations_id = new List<Int64>();

      foreach (TerminalDrawRechargeEntity _recharge in Recharges)
      {
        _operations_id.Add(_recharge.OperationId);
      }

      return _operations_id;
    } // OperationsId

    /// <summary>
    /// Get game id form List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Recharges"></param>
    /// <returns></returns>
    public static Int64 GameId(this List<TerminalDrawRechargeEntity> Recharges)
    {
      foreach (TerminalDrawRechargeEntity _recharge in Recharges)
      {
        return _recharge.GameId;
      }

      return 0;
    } // GameId

    /// <summary>
    /// Get game type form List<TerminalDrawRechargeEntity>
    /// </summary>
    /// <param name="Recharges"></param>
    /// <returns></returns>
    public static PromoGame.GameType GameType(this List<TerminalDrawRechargeEntity> Recharges)
    {
      foreach (TerminalDrawRechargeEntity _recharge in Recharges)
      {
        return (PromoGame.GameType)_recharge.GameType;
      }

      return PromoGame.GameType.Unknown;
    } // GameType

    #endregion

    #region " List<TerminalDrawPrizePlanDetail> "

    /// <summary>
    /// Get play session id form List<TerminalDrawPrizePlanDetail>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Int64 PlaySessionId(this List<TerminalDrawPrizePlanDetail> Games)
    {
      foreach (TerminalDrawPrizePlanDetail _game in Games)
      {
        if(_game.PlaySessionId > 0)
        {
          return _game.PlaySessionId;
        }
      }

      return 0;
    } // PlaySessionId

    /// <summary>
    /// Get total bet amount form List<TerminalDrawPrizePlanDetail>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Decimal TotalBetAmount(this List<TerminalDrawPrizePlanDetail> Games)
    {
      Decimal _total_bet_amount;

      _total_bet_amount = 0;

      foreach(TerminalDrawPrizePlanDetail _game in Games)
      {
        // TODO REVISE Provisional(All)
        _total_bet_amount += _game.AmountRePlayedDraw;
        _total_bet_amount += _game.AmountNrPlayedDraw;
        _total_bet_amount += _game.AmountPointsPlayedDraw;
      }

      return _total_bet_amount;
    } // TotalBetAmount

    /// <summary>
    /// Get total win amount form List<TerminalDrawPrizePlanDetail>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Decimal TotalWinAmount(this List<TerminalDrawPrizePlanDetail> Games)
    {
      Decimal _total_win_amount;

      _total_win_amount = 0;

      foreach (TerminalDrawPrizePlanDetail _game in Games)
      {
        // TODO REVISE Provisional(All)
        _total_win_amount += _game.AmountRePlayedWon;
        _total_win_amount += _game.AmountNrPlayedWon;
        _total_win_amount += _game.AmountPointsPlayedWon;
      }

      return _total_win_amount;
    } // TotalWinAmount

    /// <summary>
    /// Get IsWinner form List<TerminalDrawPrizePlanDetail>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Boolean IsWinner(this List<TerminalDrawPrizePlanDetail> Games)
    {
      foreach (TerminalDrawPrizePlanDetail _game in Games)
      {
        if(!_game.IsWinner)
        {
          return false;
        }
      }

      return true;
    } // IsWinner
    
    /// <summary>
    /// Get TimeOut form List<TerminalDrawPrizePlanDetail>
    /// </summary>
    /// <param name="Games"></param>
    /// <returns></returns>
    public static Int64 TimeOut(this List<TerminalDrawPrizePlanDetail> Games)
    {
      return (Games.Count > 0) ? Games[0].TerminalGameTimeOut : 0;
    } // TimeOut

    #endregion

  }
}