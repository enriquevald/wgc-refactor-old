//------------------------------------------------------------------------------
// Copyright � 2007-2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Format.cs
// 
//   DESCRIPTION: 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-SEP-2007 AJQ    First release.
// 01-MAR-2013 ICS    Fixed Bug #629: Check that CurrencySymbol is not empty.
// 07-MAR-2013 RCI    ParseCurrency: In cashier, don't change the currency symbol.
// 11-MAR-2013 RXM    Added DBDateTimeToLocalDateTime function.
// 02-JUL-2013 DLL    Added new currency format function
// 30-OCT-2013 ICS    Added multiplication operator to Currency struct
// 11-JUL-2014 AMF    Regionalization
// 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 23-MAR-2015 MPO    WIG-2170: Wrong decimal rounding for negative currency.
// 08-MAY-2015 DLL    WIG-2309: Wrong currency rounding for operations.
// 13-JAN-2016 DHA    Task 8287: Floor Dual Currency
// 28-JUN-2016 RAB    PBI 14524: GamingTables (Phase 4): Management from deposit and withdrawals - Multicurrency in chips.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Threading;

namespace WSI.Common
{
  //////public static class Format
  //////{
  //////  public static String Number (Int64 Value)
  //////  {
  //////    return Value.ToString ("N");
  //////  }
  //////  public static String Decimal (Decimal Value)
  //////  {
  //////    return Value.ToString ("F");
  //////  }
  //////  public static String Decimal (Decimal Value, int NumberOfDecimalPlaces)
  //////  {
  //////    return Value.ToString ("F" + NumberOfDecimalPlaces.ToString ());
  //////  }
  //////  public static String Percent (Decimal Value)
  //////  {
  //////    return Value.ToString ("P");
  //////  }
  //////  public static String Percent (Decimal Value, int NumberOfDecimalPlaces)
  //////  {
  //////    return Value.ToString ("P" + NumberOfDecimalPlaces.ToString ());
  //////  }
  //////  public static String Currency (Decimal Value)
  //////  {
  //////    return Value.ToString ("C");
  //////  }
  //////  public static String Currency (Decimal Value, int NumberOfDecimalPlaces)
  //////  {
  //////    return Value.ToString ("C" + NumberOfDecimalPlaces.ToString ());
  //////  }
  //////}

  public class Format
  {
    private static CurrencyExchange m_national_currency = null;
    private static List<CurrencyExchange> m_currencies = null;

    public static CurrencyExchange NationalCurrency
    {
      get
      {
        if (m_national_currency == null)
        {
          // Don't need to check the return status.
          CurrencyExchange.GetAllowedCurrencies(true, out m_national_currency, out m_currencies);
        }

        return Format.m_national_currency;
      }
    }

    public static Int32 GetFormattedDecimals(String IsoCode)
    {
      CurrencyExchange _currency;

      // RCI & ACC 11-DEC-2014: If DB not initialized, return default number of decimals. Useful for WCP protocol.
      if (!WGDB.Initialized)
      {
        return CurrencyExchange.DEFAULT_FORMATTED_DECIMALS;
      }

      if (m_currencies == null || m_national_currency == null)
      {
        if (!CurrencyExchange.GetAllowedCurrencies(true, out m_national_currency, out m_currencies))
        {
          return CurrencyExchange.DEFAULT_FORMATTED_DECIMALS;
        }
      }

      if (String.IsNullOrEmpty(IsoCode))
      {
        IsoCode = m_national_currency.CurrencyCode;
      }

      _currency = m_currencies.Find(delegate(CurrencyExchange ce) { return (ce.CurrencyCode == IsoCode && ce.Type == CurrencyExchangeType.CURRENCY); });

      if (_currency == null)
      {
        return CurrencyExchange.DEFAULT_FORMATTED_DECIMALS;
      }
      else
      {
        return _currency.FormattedDecimals;
      }
    }

    public static String CompactNumber(Object Value)
    {
      if (Value.GetType() == typeof(Currency))
      {
        return ((Currency)(Value)).ToString("G29");
      }

      if (Value.GetType() == typeof(Decimal))
      {
        return ((Decimal)(Value)).ToString("G29");
      }

      return Value.ToString();
    }

    public static String DateTimeNormalizedString()
    {
      String[] _split;
      String _format;
      Char _simbol;
      Int32 _width;
      DateTimeFormatInfo _dfi;

      _dfi = Thread.CurrentThread.CurrentCulture.DateTimeFormat;
      _format = _dfi.ShortDatePattern;
      _split = _format.Split(new String[] { _dfi.DateSeparator }, StringSplitOptions.RemoveEmptyEntries);

      foreach (String _str in _split)
      {
        _simbol = _str[0];
        _width = (_simbol == 'y' || _simbol == 'a') ? 4 : 2;

        _format = _format.Replace(_str, _str.PadLeft(_width, _simbol));
      }

      return _format;
    }

    public static String CustomFormatDateTime(DateTime DateTime, Boolean ShowTime)
    {
      String _datetime_format;

      _datetime_format = DateTimeCustomFormatString(ShowTime);

      return DateTime.ToString(_datetime_format);
    }

    public static String DateTimeCustomFormatString(Boolean ShowTime)
    {
      return DateTimeCustomFormatString(ShowTime, true);
    }

    public static String DateTimeCustomFormatString(Boolean ShowTime, Boolean ShowSeconds)
    {
      DateTimeFormatInfo _dfi;
      String _date_format;

      _dfi = Thread.CurrentThread.CurrentCulture.DateTimeFormat;

      _date_format = _dfi.ShortDatePattern;
      if (ShowTime)
      {
        _date_format += " " + TimeCustomFormatString(ShowSeconds);
      }

      return _date_format;
    }

    public static String CustomFormatTime(DateTime Time, Boolean ShowSeconds)
    {
      String _time_format;

      _time_format = TimeCustomFormatString(ShowSeconds);

      return Time.ToString(_time_format);
    }

    public static String TimeCustomFormatString(Boolean ShowSeconds)
    {
      DateTimeFormatInfo _dfi;
      String _time_format;

      _dfi = Thread.CurrentThread.CurrentCulture.DateTimeFormat;

      _time_format = "HH" + _dfi.TimeSeparator + "mm";
      if (ShowSeconds)
      {
        _time_format += _dfi.TimeSeparator + "ss";
      }

      return _time_format;
    }

    public static DateTime DBDatetimeToLocalDateTime(String Datetime)
    {
      DateTime _datetime = Convert.ToDateTime(Datetime, System.Globalization.CultureInfo.GetCultureInfo("es-ES").DateTimeFormat);
      return _datetime;
    }

    public static String LocalNumberToDBNumber(String LocalNumber)
    {
      double num = Convert.ToDouble(LocalNumber);
      return num.ToString(System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat);
    }

    public static String DBNumberToLocalNumber(String DBNumber)
    {
      double num = Convert.ToDouble(DBNumber, System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat);
      return num.ToString();
    }

    public static Decimal ParseCurrency(String StringValue)
    {
      Boolean _is_number;
      Decimal _result;

      // RCI 07-MAR-2013: In cashier, don't change the currency symbol.
      //if (Thread.CurrentThread.CurrentCulture.IsReadOnly)
      //{
      //  Thread.CurrentThread.CurrentCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
      //  Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol = WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol");
      //  Thread.CurrentThread.CurrentUICulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
      //  Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencySymbol = WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol");
      //}

      // OC-changed to parse only currency
      _is_number = Decimal.TryParse(StringValue, NumberStyles.Currency, null, out _result);

      return _is_number ? _result : 0;
    }

    //' PURPOSE: Obtain the string representation of the TimeSpan in format 99 d hh:mm:ss.
    //'
    //'  PARAMS:
    //'     - INPUT:
    //'           - Time
    //'     - OUTPUT:
    //'           - None
    //'
    //' RETURNS:
    //'     - String

    public static String TimeSpanToString(TimeSpan TimeSpan, Boolean ShowSeconds)
    {
      String _str_time = "";

      _str_time = new TimeSpan(TimeSpan.Hours, TimeSpan.Minutes, TimeSpan.Seconds).ToString();
      if (TimeSpan.Days > 0)
      {
        _str_time = TimeSpan.Days + "d " + _str_time;
      }
      if (!ShowSeconds || TimeSpan.Days > 0)
      {
        _str_time = _str_time.Substring(0, _str_time.Length - 3);
      }
      return _str_time;

    } // TimeSpanToString
  }

  public struct Number
  {
    private Int64 m_value;

    public static implicit operator Number(Int64 rhs)
    {
      Number n;
      n.m_value = rhs;

      return n;
    }

    static public Number Zero()
    {
      Number x = 0;
      return x;
    }

    public Int32 Int32
    {
      get
      {
        return (Int32)m_value;
      }
    }

    public static implicit operator Int64(Number rhs)
    {
      return rhs.m_value;
    }

    public override string ToString()
    {
      return m_value.ToString("N0");
    }
  }

  public struct Currency:ICloneable
  {
    private Decimal m_value;
    private String m_currency_iso_code;


    public String CurrencyIsoCode
    {
      get { return m_currency_iso_code; }
      set { m_currency_iso_code = value; }
    }

    private Decimal RoundedValue
    {
      get
      {
        Int32 _formatted_decimals = WSI.Common.Format.GetFormattedDecimals(m_currency_iso_code);
        return Math.Round(m_value, Math.Abs(_formatted_decimals));
      }
    }

    public SqlMoney SqlMoney
    {
      get
      {
        return (SqlMoney)m_value;
      }
    }
    public Decimal Decimal
    {
      get
      {
        return m_value;
      }
    }

    public static Currency operator *(Currency c1, Currency c2)
    {
      Currency c;
      c = new Currency();
      c.m_value = c1.m_value * c2.m_value;

      return c;
    }

    public static Currency operator *(Currency cur, Int32 integer)
    {
      Currency c;
      c = new Currency();
      c.m_value = cur.m_value * integer;

      return c;
    }

    public static implicit operator Currency(SqlMoney rhs)
    {
      Currency c;
      c = new Currency();
      c.m_value = (Decimal)rhs;

      return c;
    }

    public static implicit operator Currency(decimal rhs)
    {
      Currency c;
      c = new Currency();
      c.m_value = rhs;

      return c;
    }

    public static implicit operator decimal(Currency rhs)
    {
      return rhs.m_value;
    }

    public override string ToString()
    {
      Boolean _found;
      Int32 _decimals;
      Decimal _rounded_value;

      _decimals = WSI.Common.Format.GetFormattedDecimals(m_currency_iso_code);

      if (_decimals < 0)
      {
        // RoundedValue is a calculated property. Assign outside while.
        _rounded_value = RoundedValue;

        _found = false;
        _decimals = Math.Abs(_decimals);
        while (!_found && _decimals >= 0)
        {
          _found = ((_rounded_value * (Decimal)Math.Pow(10, _decimals)) % 10) != 0;
          _decimals--;
        }
        _decimals++;
      }

      return m_value.ToString("C" + _decimals.ToString());

    }

    public string ToStringWithoutSymbols()
    {
      NumberFormatInfo _nfi;
      String _aux;

      _nfi = Thread.CurrentThread.CurrentCulture.NumberFormat;

      _aux = this.ToString(false);

      if (!String.IsNullOrEmpty(_nfi.CurrencyGroupSeparator))
      {
        _aux = _aux.Replace(_nfi.CurrencyGroupSeparator, "");
      }

      _aux = _aux.Replace(_nfi.CurrencyDecimalSeparator + "00", "");

      return _aux;
    }

    public string ToString(Boolean ShowCurrencySymbol)
    {
      NumberFormatInfo _nfi;
      String _value;

      _nfi = Thread.CurrentThread.CurrentCulture.NumberFormat;

      _value = this.ToString();

      if (!ShowCurrencySymbol)
      {
        if (!String.IsNullOrEmpty(_nfi.CurrencySymbol))
        {
          _value = _value.Replace(_nfi.CurrencySymbol, "").Trim();
        }
      }

      return _value;
    }

    public string ToString(String s)
    {
      return m_value.ToString(s);
    }

    static public Currency Zero()
    {
      Currency x = 0;
      return x;
    }

    public static String Format(Decimal Amount, String IsoCode)
    {
      NumberFormatInfo _nfi;
      String _aux;
      Currency _amount;

      _nfi = Thread.CurrentThread.CurrentCulture.NumberFormat;

      _amount = (Currency)Amount;
      // RAB 28-JUN-2016: Comented since it propagate when it is not the national currency.
      //_amount.CurrencyIsoCode = IsoCode;
      _aux = _amount.ToString();

      if (!String.IsNullOrEmpty(_nfi.CurrencySymbol))
      {
        _aux = _aux.Replace(_nfi.CurrencySymbol, "");
      }
      return _aux + " " + IsoCode;
    }

    // PURPOSE: Unformat string amount with ISO Code to decimal value
    //
    //  PARAMS:
    //     - INPUT:
    //           - Amount
    //           - ISOCode
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - Decimal
    public static Decimal Unformat(String Amount, String IsoCode)
    {
      Decimal _aux;
      String _amount;

      _amount = Amount;

      if (!String.IsNullOrEmpty(IsoCode))
      {
        _amount = Amount.Replace(IsoCode, "");
      }

      Decimal.TryParse(_amount, out _aux);

      return _aux;
    }

    public object Clone()
    {
      Currency _retval = new Currency
      {
        CurrencyIsoCode = CurrencyIsoCode,
        m_value = m_value
      };
      return _retval;
    }
  }

  public struct Points:ICloneable
  {
    private Decimal m_value;

    public SqlMoney SqlMoney
    {
      get
      {
        return (SqlMoney)m_value;
      }
    }

    public static implicit operator Points(SqlMoney rhs)
    {
      Points c;
      c.m_value = (Decimal)rhs;

      return c;
    }

    public static implicit operator Points(decimal rhs)
    {
      Points c;
      c.m_value = rhs;

      return c;
    }

    public static implicit operator decimal(Points rhs)
    {
      return rhs.m_value;
    }

    public override string ToString()
    {
      return m_value.ToString("#,##0");
    }

    public string ToString(String s)
    {
      return m_value.ToString(s);
    }

    static public Points Zero()
    {
      Points x = 0;
      return x;
    }

    public object Clone()
    {
      Points _retval = new Points();
      _retval.m_value = m_value;
      return _retval;
    }
  }


  public struct Percentage
  {
    private Decimal m_value;

    public static implicit operator Percentage(decimal rhs)
    {
      Percentage p;
      p.m_value = rhs;

      return p;
    }

    public static implicit operator decimal(Percentage rhs)
    {
      return rhs.m_value;
    }

    public override string ToString()
    {
      return m_value.ToString("P");
    }

    public string ToStringSmall()
    {
      return m_value.ToString("0.##%");
    }

    public static string ToString(Decimal Value)
    {
      Percentage _aux_value;

      _aux_value = Value / 100;

      return _aux_value.ToStringSmall();
    }
  }



}
