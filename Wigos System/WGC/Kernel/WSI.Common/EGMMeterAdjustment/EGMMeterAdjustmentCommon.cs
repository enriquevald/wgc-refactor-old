﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: EGMMeterAdjustmentCommon.cs 
// 
//   DESCRIPTION: EGMMeterAdjustmentCommon class
//  
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 20-DEC-2017
// 
// REVISION HISTORY:
// 
// Date        Author      Description
// ----------- ----------- -----------------------------------------------------
// 20-DEC-2017 FGB         First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;

namespace WSI.Common.EGMMeterAdjustment
{
  public class EGMMeterAdjustmentCommon
  {
    [Flags]
    public enum EGMSasMetersHistoryAdjustment
    {
      NONE = 0,
      NORMAL = 1,
      METER_RESET = 2,
      ROLLOVER = 4,
      FIRST_TIME = 8,
      DISCARDED = 16,
      DENOM_CHANGE = 32,
      SERVICE_RAM_CLEAR = 64,
      MACHINE_RAM_CLEAR = 128,
      MISSING_COUNTERS = 256,
      MISSING_METERS = 512,
      NEW_METERS = 1024,
      MANUAL_INPUT = 2048
    }

    [Flags]
    public enum EGMStatus
    {
      SYSTEM = 0,
      VALIDATED = 1,
      REOPENED = 2
    }

    //Color constants
    private static readonly Color ADJUSTMENT_COLOR_DEFAULT = Color.White;                 //White (default) 
    private static readonly Color ADJUSTMENT_COLOR_RED = Color.FromArgb(255, 0, 0);       //Red
    private static readonly Color ADJUSTMENT_COLOR_BLUE = Color.FromArgb(192, 255, 255);  //Blue
    private static readonly Color ADJUSTMENT_COLOR_ORANGE = Color.FromArgb(255, 165, 0);  //Orange
    private static readonly Color ADJUSTMENT_COLOR_YELLOW = Color.Yellow;                 //Yellow
    private static readonly Color ADJUSTMENT_COLOR_GRAY = Color.Gray;                     //Gray

    /// <summary>
    /// Get EGM adjustment from flags by priority
    /// </summary>
    /// <param name="Flags"></param>
    /// <returns></returns>
    public static EGMSasMetersHistoryAdjustment GetEGMAdjustmentFromFlagsByPriority(Int32 Flags)
    {
      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.DENOM_CHANGE))
      {
        return EGMSasMetersHistoryAdjustment.DENOM_CHANGE;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.MISSING_COUNTERS))
      {
        return EGMSasMetersHistoryAdjustment.MISSING_COUNTERS;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.MISSING_METERS))
      {
        return EGMSasMetersHistoryAdjustment.MISSING_METERS;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.DISCARDED))
      {
        return EGMSasMetersHistoryAdjustment.DISCARDED;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.ROLLOVER))
      {
        return EGMSasMetersHistoryAdjustment.ROLLOVER;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.MACHINE_RAM_CLEAR))
      {
        return EGMSasMetersHistoryAdjustment.MACHINE_RAM_CLEAR;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.SERVICE_RAM_CLEAR))
      {
        return EGMSasMetersHistoryAdjustment.SERVICE_RAM_CLEAR;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.METER_RESET))
      {
        return EGMSasMetersHistoryAdjustment.METER_RESET;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.NEW_METERS))
      {
        return EGMSasMetersHistoryAdjustment.NEW_METERS;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.MANUAL_INPUT))
      {
        return EGMSasMetersHistoryAdjustment.MANUAL_INPUT;
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMSasMetersHistoryAdjustment.FIRST_TIME))
      {
        return EGMSasMetersHistoryAdjustment.FIRST_TIME;
      }

      return EGMSasMetersHistoryAdjustment.NONE;
    }

    /// <summary>
    /// Get EGM adjustment from meter type
    /// </summary>
    /// <param name="RecordType"></param>
    /// <returns></returns>
    public static EGMSasMetersHistoryAdjustment GetEGMAdjustmentFromMeterType(ENUM_SAS_METER_HISTORY RecordType)
    {
      switch (RecordType)
      {
        case ENUM_SAS_METER_HISTORY.TSMH_METER_RESET:
          return EGMSasMetersHistoryAdjustment.METER_RESET;

        case ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER:
        case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_ROLLOVER:
          return EGMSasMetersHistoryAdjustment.ROLLOVER;

        case ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME:
          return EGMSasMetersHistoryAdjustment.FIRST_TIME;

        case ENUM_SAS_METER_HISTORY.TSMH_METER_DISCARDED:
          return EGMSasMetersHistoryAdjustment.DISCARDED;

        case ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE:
        case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SAS_ACCOUNT_DENOM_CHANGE:
          return EGMSasMetersHistoryAdjustment.DENOM_CHANGE;

        case ENUM_SAS_METER_HISTORY.TSMH_METER_SERVICE_RAM_CLEAR:
        case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SERVICE_RAM_CLEAR:
          return EGMSasMetersHistoryAdjustment.SERVICE_RAM_CLEAR;

        case ENUM_SAS_METER_HISTORY.TSMH_METER_MACHINE_RAM_CLEAR:
        case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_MACHINE_RAM_CLEAR:
          return EGMSasMetersHistoryAdjustment.MACHINE_RAM_CLEAR;

        case ENUM_SAS_METER_HISTORY.TSMH_MANUAL_INPUT:
          return EGMSasMetersHistoryAdjustment.MANUAL_INPUT;

        case ENUM_SAS_METER_HISTORY.NONE:
        case ENUM_SAS_METER_HISTORY.TSMH_HOURLY:
        case ENUM_SAS_METER_HISTORY.TSMH_MINCETUR_DAILY_REPORT:
        default:
          return EGMSasMetersHistoryAdjustment.NONE;
      }
    }

    /// <summary>
    /// Get Color Anomaly And Text Anomaly By Priority
    /// </summary>
    /// <param name="Flags"></param>
    /// <param name="AreThereMissingPeriodRecords"></param>
    /// <param name="AreThereBigIncrements"></param>
    /// <param name="Color"></param>
    /// <param name="ValueEnumStr"></param>
    public static void GetColorByPriorityWarning(Int32 Flags, Boolean AreThereMissingPeriodRecords, Boolean AreThereBigIncrements, ref System.Drawing.Color Color, ref String ValueEnumStr)
    {
      String _aux_enum_str;
      List<String> _lst_anom;

      Color = ADJUSTMENT_COLOR_DEFAULT;
      ValueEnumStr = String.Empty;
      _aux_enum_str = String.Empty;

      _lst_anom = new List<String>();

      //From lower to higher priority
      if (AreThereBigIncrements)
      {
        Color = ADJUSTMENT_COLOR_BLUE;
        _aux_enum_str = Resource.String("EGMSasMetersHistoryThereAreBigIncrements");
        _lst_anom.Add(_aux_enum_str);
      }

      if (AreThereMissingPeriodRecords)
      {
        Color = ADJUSTMENT_COLOR_GRAY;
        _aux_enum_str = Resource.String("EGMSasMetersHistoryThereAreMissingPeriodRecords");
        _lst_anom.Add(_aux_enum_str);
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MANUAL_INPUT))
      {
        GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MANUAL_INPUT, ref Color, ref _aux_enum_str);
        //_lst_anom.Add(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MANUAL_INPUT.ToString());
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.FIRST_TIME))
      {
        GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.FIRST_TIME, ref Color, ref _aux_enum_str);
        _lst_anom.Add(_aux_enum_str);
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.SERVICE_RAM_CLEAR))
      {
        GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.SERVICE_RAM_CLEAR, ref Color, ref _aux_enum_str);
        _lst_anom.Add(_aux_enum_str);
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.DISCARDED))
      {
        GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.DISCARDED, ref Color, ref _aux_enum_str);
        _lst_anom.Add(_aux_enum_str);
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.ROLLOVER))
      {
        GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.ROLLOVER, ref Color, ref _aux_enum_str);
        _lst_anom.Add(_aux_enum_str);
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MACHINE_RAM_CLEAR))
      {
        GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MACHINE_RAM_CLEAR, ref Color, ref _aux_enum_str);
        _lst_anom.Add(_aux_enum_str);
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.METER_RESET))
      {
        GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.METER_RESET, ref Color, ref _aux_enum_str);
        _lst_anom.Add(_aux_enum_str);
      }

      if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.DENOM_CHANGE)
         || GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MISSING_COUNTERS))
      {
        GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.DENOM_CHANGE, ref Color, ref _aux_enum_str);
        _lst_anom.Add(_aux_enum_str);
      }

      //if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MISSING_METERS))
      //{
      //  GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MISSING_METERS, ref Color, ref _aux_enum_str);
      //  _lst_anom.Add(_aux_enum_str);
      //}

      //if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.NEW_METERS))
      //{
      //  GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.NEW_METERS, ref Color, ref _aux_enum_str);
      //  _lst_anom.Add(_aux_enum_str);
      //}

      //if (GetMaskFlagBitEgm(Flags, (Int32)EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MANUAL_INPUT))
      //{
      //  GetColorByEGMHistoryAdjustment(EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MANUAL_INPUT, ref Color, ref _aux_enum_str);
      //  _lst_anom.Add(_aux_enum_str);
      //}

      //Descriptions are ordered from lower to higher priority.
      //Reverse to get higher priority first
      _lst_anom.Reverse();

      //Get descriptions concatenated
      ValueEnumStr = String.Join(",", _lst_anom.ToArray());
    }

    /// <summary>
    /// Get color for EGM Adjustment
    /// </summary>
    /// <param name="EGMAdjustment"></param>
    /// <returns></returns>
    public static Color GetEGMAdjustmentColor(EGMSasMetersHistoryAdjustment EGMAdjustment)
    {
      switch (EGMAdjustment)
      {
        case EGMSasMetersHistoryAdjustment.DENOM_CHANGE:
        case EGMSasMetersHistoryAdjustment.MISSING_COUNTERS:
          return ADJUSTMENT_COLOR_RED;

        case EGMSasMetersHistoryAdjustment.ROLLOVER:
        case EGMSasMetersHistoryAdjustment.METER_RESET:
        case EGMSasMetersHistoryAdjustment.SERVICE_RAM_CLEAR:
        case EGMSasMetersHistoryAdjustment.MACHINE_RAM_CLEAR:
        case EGMSasMetersHistoryAdjustment.DISCARDED:
          return ADJUSTMENT_COLOR_BLUE;

        case EGMSasMetersHistoryAdjustment.FIRST_TIME:
          return ADJUSTMENT_COLOR_ORANGE;

        case EGMSasMetersHistoryAdjustment.MANUAL_INPUT:
          return ADJUSTMENT_COLOR_YELLOW;

        case EGMSasMetersHistoryAdjustment.NONE:
        case EGMSasMetersHistoryAdjustment.NORMAL:
        case EGMSasMetersHistoryAdjustment.NEW_METERS:
        case EGMSasMetersHistoryAdjustment.MISSING_METERS:
        default:
          return ADJUSTMENT_COLOR_DEFAULT;
      }
    }

    /// <summary>
    /// Get description for EGM Adjustment
    /// </summary>
    /// <param name="EGMAdjustment"></param>
    /// <returns></returns>
    public static String GetEGMAdjustmentDescription(EGMSasMetersHistoryAdjustment EGMAdjustment)
    {
      switch (EGMAdjustment)
      {
        case EGMSasMetersHistoryAdjustment.NORMAL:
          return Resource.String("EGMSasMetersHistoryAdjustmentNORMAL");

        case EGMSasMetersHistoryAdjustment.METER_RESET:
          return Resource.String("EGMSasMetersHistoryAdjustmentMETER_RESET");

        case EGMSasMetersHistoryAdjustment.ROLLOVER:
          return Resource.String("EGMSasMetersHistoryAdjustmentROLLOVER");

        case EGMSasMetersHistoryAdjustment.FIRST_TIME:
          return Resource.String("EGMSasMetersHistoryAdjustmentFIRST_TIME");

        case EGMSasMetersHistoryAdjustment.DISCARDED:
          return Resource.String("EGMSasMetersHistoryAdjustmentDISCARDED");

        case EGMSasMetersHistoryAdjustment.DENOM_CHANGE:
          return Resource.String("EGMSasMetersHistoryAdjustmentDENOM_CHANGE");

        case EGMSasMetersHistoryAdjustment.SERVICE_RAM_CLEAR:
          return Resource.String("EGMSasMetersHistoryAdjustmentSERVICE_RAM_CLEAR");

        case EGMSasMetersHistoryAdjustment.MACHINE_RAM_CLEAR:
          return Resource.String("EGMSasMetersHistoryAdjustmentMACHINE_RAM_CLEAR");

        case EGMSasMetersHistoryAdjustment.MISSING_COUNTERS:
          return Resource.String("EGMSasMetersHistoryAdjustmentMISSING_COUNTERS");

        case EGMSasMetersHistoryAdjustment.MISSING_METERS:
          return Resource.String("EGMSasMetersHistoryAdjustmentMISSING_METERS");

        case EGMSasMetersHistoryAdjustment.NEW_METERS:
          return Resource.String("EGMSasMetersHistoryAdjustmentNEW_METERS");

        case EGMSasMetersHistoryAdjustment.MANUAL_INPUT:
          return Resource.String("EGMSasMetersHistoryAdjustmentMANUAL_INPUT");

        case EGMSasMetersHistoryAdjustment.NONE:
        default:
          return String.Empty;
      }
    }

    /// <summary>
    /// Get Color Anomaly And Text Anomaly By Priority
    /// </summary>
    /// <param name="Flags"></param>
    /// <param name="Color"></param>
    /// <param name="ValueEnumStr"></param>
    public static void GetColorByPriorityTsmh(ENUM_SAS_METER_HISTORY RecordType, ref System.Drawing.Color Color, ref String ValueEnumStr)
    {
      EGMSasMetersHistoryAdjustment _EGM_Adjustment;

      //Get adjustment by priority
      _EGM_Adjustment = GetEGMAdjustmentFromMeterType(RecordType);

      GetColorByEGMHistoryAdjustment(_EGM_Adjustment, ref Color, ref ValueEnumStr);
    }

    /// <summary>
    /// Get Color Anomaly And Text Anomaly from EGM history adjustment
    /// </summary>
    /// <param name="EGMAdjustment"></param>
    /// <param name="Color"></param>
    /// <param name="ValueEnumStr"></param>
    public static void GetColorByEGMHistoryAdjustment(EGMSasMetersHistoryAdjustment EGMAdjustment, ref System.Drawing.Color Color, ref String ValueEnumStr)
    {
      Color = GetEGMAdjustmentColor(EGMAdjustment);
      ValueEnumStr = GetEGMAdjustmentDescription(EGMAdjustment);
    }

    /// <summary>
    /// Get BitMask Flag
    /// </summary>
    /// <param name="Flags"></param>
    /// <param name="Flag"></param>
    /// <returns></returns>
    public static Boolean GetMaskFlagBitEgm(Int32 Flags, Int32 Flag)
    {
      return ((Flags & Flag) != 0);
    }

    /// <summary>
    /// Set BitMask Flag
    /// </summary>
    /// <param name="Flags"></param>
    /// <param name="Flag"></param>
    /// <param name="SetValue"></param>
    public static void SetMaskFlagBit(ref Int32 Flags, Int32 Flag, Boolean SetValue)
    {
      if (SetValue)
      {
        // We are setting the bit (OR)
        Flags |= Flag;
      }
      else
      {
        // We are clearing the bit
        Flags &= ~Flag;
      }
    }
  }
}