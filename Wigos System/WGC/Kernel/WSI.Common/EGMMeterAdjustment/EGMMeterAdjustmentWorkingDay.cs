﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: EGMMeterAdjustmentWorkingDay.cs
// 
//   DESCRIPTION: EGMMeterAdjustmentWorkingDay
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 21-DEC-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-DEC-2017 FGB    Initial release
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.EGMMeterAdjustment
{
  public class EGMMeterAdjustmentWorkingDay
  {
    public EGMWorkingDayStatus GetWorkingDayStatus(Int32 WorkingDay, Int32 SiteId)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT   ED_STATUS                       ");
      _sb.AppendLine("    FROM   EGM_DAILY                       ");
      _sb.AppendLine("   WHERE   ED_WORKING_DAY  =  @pWorkingDay ");
      _sb.AppendLine("     AND   ED_SITE_ID      =  @pSiteId     ");

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = WorkingDay;
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if ((_reader.Read()) && (!_reader.IsDBNull(0)))
              {
                return (EGMWorkingDayStatus)(Byte)_reader["ED_STATUS"];
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("GetWorkingDayStatus: There was an error finding workingday status");
        Log.Exception(_ex);
      }

      return EGMWorkingDayStatus.CLOSED;
    }

    public EGMWorkingDayStatus GetWorkingDayStatus(DateTime WorkingDay, Int32 SiteId)
    {
      return GetWorkingDayStatus(Int32.Parse(WorkingDay.ToString("yyyyMMdd")), SiteId);
    }


    public Boolean IsWorkingDayClosed(Int32 WorkingDay, Int32 SiteId)
    {
      EGMWorkingDayStatus _status;

      _status = GetWorkingDayStatus(WorkingDay, SiteId);
      return ((_status == EGMWorkingDayStatus.CLOSED) || (_status == EGMWorkingDayStatus.NONE));
    }

    public Boolean IsWorkingDayClosed(DateTime WorkingDay, Int32 SiteId)
    {
      return (IsWorkingDayClosed(Int32.Parse(WorkingDay.ToString("yyyyMMdd")), SiteId));
    }
  }
}
