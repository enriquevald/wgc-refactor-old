﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: EGMWorkingDayStatus.cs
// 
//   DESCRIPTION: EGMWorkingDayStatus
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 21-DEC-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-DEC-2017 FGB    Initial release
//------------------------------------------------------------------------------
namespace WSI.Common.EGMMeterAdjustment
{
  public enum EGMWorkingDayStatus
  {
    NONE = -1,
    OPEN = 0,
    CLOSED = 1,
    IN_PROGRESS = 2
  }
}
