﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: EGMMeterAdjustmentByPeriod.cs 
// 
//   DESCRIPTION: EGMMeterAdjustmentByPeriod class
//  
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 20-DEC-2017
// 
// REVISION HISTORY:
// 
// Date        Author      Description
// ----------- ----------- -----------------------------------------------------
// 20-DEC-2017 FGB         First version.
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.EGMMeterAdjustment
{
  public class EGMMeterAdjustmentByPeriod
  {
    public Int32 SiteId { get; set; }

    public Int32 WorkingDay { get; set; }

    public Int32 TerminalId { get; set; }

    public DataTable PeriodItemsDT { get; set; }

    public EGMMeterAdjustmentByPeriod(Int32 SiteId, Int32 WorkingDay, Int32 TerminalId)
    {
      this.SiteId = SiteId;
      this.WorkingDay = WorkingDay;
      this.TerminalId = TerminalId;

      PeriodItemsDT = new DataTable();
    }

    /// <summary>
    /// Load datatable (previous) don't remove
    /// </summary>
    /// <returns></returns>
    public Boolean LoadDataTablePrevious()
    {
      StringBuilder _sb;
      StringBuilder _sb_previous_working_day;
      DataTable _dt_previous_working_day;
      DataTable _dt_working_day;
      try
      {
        PeriodItemsDT = new DataTable();
        _dt_previous_working_day = new DataTable();
        _dt_working_day = new DataTable();

        _sb_previous_working_day = new StringBuilder();
        _sb_previous_working_day.AppendLine("      SELECT TOP 1  EMP_SITE_ID                                                                       "); //00
        _sb_previous_working_day.AppendLine("                  , EMP_TERMINAL_ID                                                                   "); //01
        _sb_previous_working_day.AppendLine("                  , EMP_WORKING_DAY                                                                   "); //02
        _sb_previous_working_day.AppendLine("                  , EMP_GAME_ID                                                                       "); //03
        _sb_previous_working_day.AppendLine("                  , EMP_DENOMINATION                                                                  "); //04
        _sb_previous_working_day.AppendLine("                  , EMP_DATETIME                                                                      "); //05
        _sb_previous_working_day.AppendLine("                  , CASE WHEN EMP_USER_IGNORED = 1 THEN 0                                             ");
        _sb_previous_working_day.AppendLine("                    ELSE (EMP_MC_0000_INCREMENT - (EMP_MC_0001_INCREMENT + EMP_MC_0002_INCREMENT) ) * ");
        _sb_previous_working_day.AppendLine("                         ( (ISNULL(EMP_SAS_ACCOUNTING_DENOM, 0.01) ) / 100) END AS EMP_WIN            ");
        _sb_previous_working_day.AppendLine("                  , EMP_MC_0000                                                                       "); //07
        _sb_previous_working_day.AppendLine("                  , EMP_MC_0001                                                                       "); //08
        _sb_previous_working_day.AppendLine("                  , EMP_MC_0002                                                                       "); //09
        _sb_previous_working_day.AppendLine("                  , EMP_RECORD_TYPE                                                                   "); //10
        _sb_previous_working_day.AppendLine("                  , EMP_USER_IGNORED                                                                  "); //11
        _sb_previous_working_day.AppendLine("                  , EMP_SAS_ACCOUNTING_DENOM                                                          "); //12
        _sb_previous_working_day.AppendLine("                  , EMP_LAST_CREATED_DATETIME                                                         "); //13
        _sb_previous_working_day.AppendLine("                  , EMP_LAST_REPORTED_DATETIME                                                        "); //14
        _sb_previous_working_day.AppendLine("                  , EMP_USER_IGNORED_DATETIME                                                         "); //15
        _sb_previous_working_day.AppendLine("                  , EMP_LAST_UPDATED_USER_ID                                                          "); //16
        _sb_previous_working_day.AppendLine("                  , EMP_LAST_UPDATED_USER_DATETIME                                                    "); //17
        _sb_previous_working_day.AppendLine("                  , EMP_STATUS                                                                        "); //18
        _sb_previous_working_day.AppendLine("                  , CAST(0 AS BIT)                              AS EMP_MODIFIED                       "); //19
        _sb_previous_working_day.AppendLine("                  , EMP_MC_0000_INCREMENT                                                             "); //20
        _sb_previous_working_day.AppendLine("                  , EMP_MC_0001_INCREMENT                                                             "); //21
        _sb_previous_working_day.AppendLine("                  , EMP_MC_0002_INCREMENT                                                             "); //22
        _sb_previous_working_day.AppendLine("        FROM        EGM_METERS_BY_PERIOD                                                              ");
        _sb_previous_working_day.AppendLine("       WHERE        EMP_WORKING_DAY  <  @pWorkingDay                                                  ");
        _sb_previous_working_day.AppendLine("         AND        EMP_SITE_ID      =  @pSiteId                                                      ");
        _sb_previous_working_day.AppendLine("         AND        EMP_TERMINAL_ID  =  @pTerminalId                                                  ");
        _sb_previous_working_day.AppendLine("         AND        EMP_USER_IGNORED =  0                                                             ");
        _sb_previous_working_day.AppendLine("    ORDER BY        EMP_WORKING_DAY  DESC                                                             ");
        _sb_previous_working_day.AppendLine("                  , EMP_DATETIME     DESC                                                             ");

        _sb = new StringBuilder();
        _sb.AppendLine("      SELECT   EMP_SITE_ID                                                                                      "); //00
        _sb.AppendLine("             , EMP_TERMINAL_ID                                                                                  "); //01
        _sb.AppendLine("             , EMP_WORKING_DAY                                                                                  "); //02
        _sb.AppendLine("             , EMP_GAME_ID                                                                                      "); //03
        _sb.AppendLine("             , EMP_DENOMINATION                                                                                 "); //04
        _sb.AppendLine("             , EMP_DATETIME                                                                                     "); //05
        _sb.AppendLine("             , CASE WHEN EMP_USER_IGNORED = 1 THEN 0                                                            ");
        _sb.AppendLine("               ELSE (EMP_MC_0000_INCREMENT - (EMP_MC_0001_INCREMENT + EMP_MC_0002_INCREMENT) ) *                ");
        _sb.AppendLine("                    ( (ISNULL(EMP_SAS_ACCOUNTING_DENOM, 0.01) ) / 100) END AS EMP_WIN                           ");
        _sb.AppendLine("             , EMP_MC_0000                                                                                      "); //07
        _sb.AppendLine("             , EMP_MC_0001                                                                                      "); //08
        _sb.AppendLine("             , EMP_MC_0002                                                                                      "); //09
        _sb.AppendLine("             , EMP_RECORD_TYPE                                                                                  "); //10
        _sb.AppendLine("             , EMP_USER_IGNORED                                                                                 "); //11
        _sb.AppendLine("             , EMP_SAS_ACCOUNTING_DENOM                                                                         "); //12
        _sb.AppendLine("             , EMP_LAST_CREATED_DATETIME                                                                        "); //13
        _sb.AppendLine("             , EMP_LAST_REPORTED_DATETIME                                                                       "); //14
        _sb.AppendLine("             , EMP_USER_IGNORED_DATETIME                                                                        "); //15
        _sb.AppendLine("             , EMP_LAST_UPDATED_USER_ID                                                                         "); //16
        _sb.AppendLine("             , EMP_LAST_UPDATED_USER_DATETIME                                                                   "); //17
        _sb.AppendLine("             , EMP_STATUS                                                                                       "); //18
        _sb.AppendLine("             , CAST(0 AS BIT)                              AS EMP_MODIFIED                                      "); //19
        _sb.AppendLine("             , EMP_MC_0000_INCREMENT                                                                            "); //20
        _sb.AppendLine("             , EMP_MC_0001_INCREMENT                                                                            "); //21
        _sb.AppendLine("             , EMP_MC_0002_INCREMENT                                                                            "); //22
        _sb.AppendLine("        FROM   EGM_METERS_BY_PERIOD                                                                             ");
        _sb.AppendLine("       WHERE   EMP_WORKING_DAY  =  @pWorkingDay                                                                 ");
        _sb.AppendLine("         AND   EMP_SITE_ID      =  @pSiteId                                                                     ");
        _sb.AppendLine("         AND   EMP_TERMINAL_ID  =  @pTerminalId                                                                 ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb_previous_working_day.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = this.WorkingDay;
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = this.SiteId;
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = this.TerminalId;
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(PeriodItemsDT); // Previous workingday 
            }
          }

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = this.WorkingDay;
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = this.SiteId;
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = this.TerminalId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {

              _sql_da.Fill(_dt_working_day); // Actual workingday

              //Merge between actual working day and top 1 previous working day
              PeriodItemsDT.Merge(_dt_working_day);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("EGMMeterAdjustmentByPeriod.LoadDataTable -> WorkingDay: {0} - SiteId: {1} - TerminalId: {2}",
                                this.WorkingDay, this.SiteId, this.TerminalId));
        Log.Exception(_ex);
      }

      return false;
    }




    /// <summary>
    /// Load datatable (After)
    /// </summary>
    /// <returns></returns>
    public Boolean LoadDataTable()
    {
      StringBuilder _sb_current_day;
      StringBuilder _sb_next_day;
      DataTable _dt_next_day;

      try
      {
        PeriodItemsDT = new DataTable();

        _dt_next_day = new DataTable();

        //Get first record of next day
        _sb_next_day = new StringBuilder();
        _sb_next_day.AppendLine("    SELECT TOP 1  EMP_SITE_ID                                                                                  "); //00
        _sb_next_day.AppendLine("                , EMP_TERMINAL_ID                                                                              "); //01
        _sb_next_day.AppendLine("                , EMP_WORKING_DAY                                                                              "); //02
        _sb_next_day.AppendLine("                , EMP_GAME_ID                                                                                  "); //03
        _sb_next_day.AppendLine("                , EMP_DENOMINATION                                                                             "); //04
        _sb_next_day.AppendLine("                , EMP_DATETIME                                                                                 "); //05
        _sb_next_day.AppendLine("                , (CASE WHEN ((EMP_USER_IGNORED = 0) AND (EMP_RECORD_TYPE IN (@pType1, @pType2)))              ");
        _sb_next_day.AppendLine("                          THEN  (EMP_MC_0000_INCREMENT - (EMP_MC_0001_INCREMENT + EMP_MC_0002_INCREMENT) )     ");
        _sb_next_day.AppendLine("                          ELSE  0                                                                              ");
        _sb_next_day.AppendLine("                   END)  AS  EMP_WIN                                                                           ");
        _sb_next_day.AppendLine("                , EMP_MC_0000                                                                                  "); //07
        _sb_next_day.AppendLine("                , EMP_MC_0001                                                                                  "); //08
        _sb_next_day.AppendLine("                , EMP_MC_0002                                                                                  "); //09
        _sb_next_day.AppendLine("                , EMP_RECORD_TYPE                                                                              "); //10
        _sb_next_day.AppendLine("                , EMP_USER_IGNORED                                                                             "); //11
        _sb_next_day.AppendLine("                , EMP_SAS_ACCOUNTING_DENOM                                                                     "); //12
        _sb_next_day.AppendLine("                , EMP_LAST_CREATED_DATETIME                                                                    "); //13
        _sb_next_day.AppendLine("                , EMP_LAST_REPORTED_DATETIME                                                                   "); //14
        _sb_next_day.AppendLine("                , EMP_USER_IGNORED_DATETIME                                                                    "); //15
        _sb_next_day.AppendLine("                , EMP_LAST_UPDATED_USER_ID                                                                     "); //16
        _sb_next_day.AppendLine("                , EMP_LAST_UPDATED_USER_DATETIME                                                               "); //17
        _sb_next_day.AppendLine("                , EMP_STATUS                                                                                   "); //18
        _sb_next_day.AppendLine("                , CAST(0 AS BIT)                              AS  EMP_MODIFIED                                 "); //19
        _sb_next_day.AppendLine("                , EMP_MC_0000_INCREMENT                                                                        "); //20
        _sb_next_day.AppendLine("                , EMP_MC_0001_INCREMENT                                                                        "); //21
        _sb_next_day.AppendLine("                , EMP_MC_0002_INCREMENT                                                                        "); //22
        _sb_next_day.AppendLine("                , CALCULE_METER_MAX_VALUE.METER_MAX_VALUE                                                      ");
        _sb_next_day.AppendLine("      FROM        EGM_METERS_BY_PERIOD                                                                         ");
        _sb_next_day.AppendLine(" LEFT JOIN (                                                                                                   ");
        _sb_next_day.AppendLine("            SELECT   MAX(EMMV_MAX_VALUE) AS METER_MAX_VALUE                                                    ");
        _sb_next_day.AppendLine("                   , EMMV_SITE_ID                                                                              ");
        _sb_next_day.AppendLine("                   , EMMV_TERMINAL_ID                                                                          ");
        _sb_next_day.AppendLine("                   , EMMV_DATETIME                                                                             ");
        _sb_next_day.AppendLine("              FROM   EGM_METERS_MAX_VALUES                                                                     ");
        _sb_next_day.AppendLine("             WHERE   EGM_METERS_MAX_VALUES.EMMV_SITE_ID      = @pSiteId                                        ");
        _sb_next_day.AppendLine("               AND   EGM_METERS_MAX_VALUES.EMMV_TERMINAL_ID  = @pTerminalId                                    ");
        _sb_next_day.AppendLine("               AND   EMMV_TYPE = @pType3                                                                       ");
        _sb_next_day.AppendLine("          GROUP BY   EMMV_SITE_ID                                                                              ");
        _sb_next_day.AppendLine("                   , EMMV_TERMINAL_ID                                                                          ");
        _sb_next_day.AppendLine("                   , EMMV_DATETIME                                                                             ");
        _sb_next_day.AppendLine("           )  AS  CALCULE_METER_MAX_VALUE                                                                      ");
        _sb_next_day.AppendLine("               ON   CALCULE_METER_MAX_VALUE.EMMV_SITE_ID      = EMP_SITE_ID                                    ");
        _sb_next_day.AppendLine("              AND   CALCULE_METER_MAX_VALUE.EMMV_TERMINAL_ID  = EMP_TERMINAL_ID                                ");
        _sb_next_day.AppendLine("              AND   CALCULE_METER_MAX_VALUE.EMMV_DATETIME     = EMP_DATETIME                                   ");

        _sb_next_day.AppendLine("     WHERE        EMP_WORKING_DAY  >  @pWorkingDay                                                             ");
        _sb_next_day.AppendLine("       AND        EMP_SITE_ID      =  @pSiteId                                                                 ");
        _sb_next_day.AppendLine("       AND        EMP_TERMINAL_ID  =  @pTerminalId                                                             ");
        _sb_next_day.AppendLine("       AND        EMP_USER_IGNORED =  0                                                                        ");
        _sb_next_day.AppendLine("  ORDER BY        EMP_WORKING_DAY  ASC                                                                         ");
        _sb_next_day.AppendLine("                , EMP_DATETIME     ASC                                                                         ");

        //Get records of current day
        _sb_current_day = new StringBuilder();
        _sb_current_day.AppendLine("    SELECT   EMP_SITE_ID                                                                                    "); //00
        _sb_current_day.AppendLine("           , EMP_TERMINAL_ID                                                                                "); //01
        _sb_current_day.AppendLine("           , EMP_WORKING_DAY                                                                                "); //02
        _sb_current_day.AppendLine("           , EMP_GAME_ID                                                                                    "); //03
        _sb_current_day.AppendLine("           , EMP_DENOMINATION                                                                               "); //04
        _sb_current_day.AppendLine("           , EMP_DATETIME                                                                                   "); //05
        _sb_current_day.AppendLine("           , (CASE WHEN ((EMP_USER_IGNORED = 0) AND (EMP_RECORD_TYPE IN (@pType1, @pType2)))                ");
        _sb_current_day.AppendLine("                     THEN  (EMP_MC_0000_INCREMENT - (EMP_MC_0001_INCREMENT + EMP_MC_0002_INCREMENT) )       ");
        _sb_current_day.AppendLine("                     ELSE  0                                                                                ");
        _sb_current_day.AppendLine("              END)  AS  EMP_WIN                                                                             ");
        _sb_current_day.AppendLine("           , EMP_MC_0000                                                                                    "); //07
        _sb_current_day.AppendLine("           , EMP_MC_0001                                                                                    "); //08
        _sb_current_day.AppendLine("           , EMP_MC_0002                                                                                    "); //09
        _sb_current_day.AppendLine("           , EMP_RECORD_TYPE                                                                                "); //10
        _sb_current_day.AppendLine("           , EMP_USER_IGNORED                                                                               "); //11
        _sb_current_day.AppendLine("           , EMP_SAS_ACCOUNTING_DENOM                                                                       "); //12
        _sb_current_day.AppendLine("           , EMP_LAST_CREATED_DATETIME                                                                      "); //13
        _sb_current_day.AppendLine("           , EMP_LAST_REPORTED_DATETIME                                                                     "); //14
        _sb_current_day.AppendLine("           , EMP_USER_IGNORED_DATETIME                                                                      "); //15
        _sb_current_day.AppendLine("           , EMP_LAST_UPDATED_USER_ID                                                                       "); //16
        _sb_current_day.AppendLine("           , EMP_LAST_UPDATED_USER_DATETIME                                                                 "); //17
        _sb_current_day.AppendLine("           , EMP_STATUS                                                                                     "); //18
        _sb_current_day.AppendLine("           , CAST(0 AS BIT)                              AS  EMP_MODIFIED                                   "); //19
        _sb_current_day.AppendLine("           , EMP_MC_0000_INCREMENT                                                                          "); //20
        _sb_current_day.AppendLine("           , EMP_MC_0001_INCREMENT                                                                          "); //21
        _sb_current_day.AppendLine("           , EMP_MC_0002_INCREMENT                                                                          "); //22
        _sb_current_day.AppendLine("           , CALCULE_METER_MAX_VALUE.METER_MAX_VALUE                                                        ");
        _sb_current_day.AppendLine("      FROM   EGM_METERS_BY_PERIOD                                                                           ");
        _sb_current_day.AppendLine(" LEFT JOIN (                                                                                                ");
        _sb_current_day.AppendLine("            SELECT   MAX(EMMV_MAX_VALUE) AS METER_MAX_VALUE                                                 ");
        _sb_current_day.AppendLine("                   , EMMV_SITE_ID                                                                           ");
        _sb_current_day.AppendLine("                   , EMMV_TERMINAL_ID                                                                       ");
        _sb_current_day.AppendLine("                   , EMMV_DATETIME                                                                          ");
        _sb_current_day.AppendLine("              FROM   EGM_METERS_MAX_VALUES                                                                  ");
        _sb_current_day.AppendLine("             WHERE   EGM_METERS_MAX_VALUES.EMMV_SITE_ID      = @pSiteId                                     ");
        _sb_current_day.AppendLine("               AND   EGM_METERS_MAX_VALUES.EMMV_TERMINAL_ID  = @pTerminalId                                 ");
        _sb_current_day.AppendLine("               AND   EMMV_TYPE = @pType3                                                                    ");
        _sb_current_day.AppendLine("          GROUP BY   EMMV_SITE_ID                                                                           ");
        _sb_current_day.AppendLine("                   , EMMV_TERMINAL_ID                                                                       ");
        _sb_current_day.AppendLine("                   , EMMV_DATETIME                                                                          ");
        _sb_current_day.AppendLine("           )  AS  CALCULE_METER_MAX_VALUE                                                                   ");
        _sb_current_day.AppendLine("               ON   CALCULE_METER_MAX_VALUE.EMMV_SITE_ID      = EMP_SITE_ID                                 ");
        _sb_current_day.AppendLine("              AND   CALCULE_METER_MAX_VALUE.EMMV_TERMINAL_ID  = EMP_TERMINAL_ID                             ");
        _sb_current_day.AppendLine("              AND   CALCULE_METER_MAX_VALUE.EMMV_DATETIME     = EMP_DATETIME                                ");

        _sb_current_day.AppendLine("     WHERE   EMP_WORKING_DAY  =  @pWorkingDay                                                               ");
        _sb_current_day.AppendLine("       AND   EMP_SITE_ID      =  @pSiteId                                                                   ");
        _sb_current_day.AppendLine("       AND   EMP_TERMINAL_ID  =  @pTerminalId                                                               ");
        _sb_current_day.AppendLine("       AND   EMP_RECORD_TYPE  in (@pType1, @pType2, @pType3, @pType4, @pType5, @pType6, @pType7, @pType8)   ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb_current_day.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = this.WorkingDay;
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = this.SiteId;
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = this.TerminalId;
            _cmd.Parameters.Add("@pType1", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_HOURLY;
            _cmd.Parameters.Add("@pType2", SqlDbType.Int).Value = EGMMeterAdjustmentConstants.RECORD_TYPE_CUT;
            _cmd.Parameters.Add("@pType3", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER;

            _cmd.Parameters.Add("@pType4", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SAS_ACCOUNT_DENOM_CHANGE;
            _cmd.Parameters.Add("@pType5", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SERVICE_RAM_CLEAR;
            _cmd.Parameters.Add("@pType6", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_MACHINE_RAM_CLEAR;

            _cmd.Parameters.Add("@pType7", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME;
            _cmd.Parameters.Add("@pType8", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_METER_RESET;
            _cmd.Parameters.Add("@pType9", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_METER_DISCARDED;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(PeriodItemsDT); // Current WorkingDay
            }
          }

          using (SqlCommand _cmd = new SqlCommand(_sb_next_day.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = this.WorkingDay;
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = this.SiteId;
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = this.TerminalId;
            _cmd.Parameters.Add("@pType1", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_HOURLY;
            _cmd.Parameters.Add("@pType2", SqlDbType.Int).Value = EGMMeterAdjustmentConstants.RECORD_TYPE_CUT;
            _cmd.Parameters.Add("@pType3", SqlDbType.Int).Value = ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_dt_next_day); // Next day
              PeriodItemsDT.Merge(_dt_next_day);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("EGMMeterAdjustmentByPeriod.LoadDataTable -> WorkingDay: {0} - SiteId: {1} - TerminalId: {2}",
                                this.WorkingDay, this.SiteId, this.TerminalId));
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Is datatable modified?
    /// </summary>
    /// <param name="DtModified"></param>
    /// <returns></returns>
    public Boolean IsDataTableModified(DataTable DtModified)
    {
      try
      {
        foreach (DataRow _data_row in DtModified.Rows)
        {
          if (IsDataRowModified(_data_row, DtModified.Columns.Count))
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("EGMMeterAdjustmentByPeriod.IsDataTableModified");
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Is datarow modified?
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="NumberOfColumns"></param>
    /// <returns></returns>
    private Boolean IsDataRowModified(DataRow Row, Int32 NumberOfColumns)
    {
      if (Row == null)
      {
        return false;
      }

      switch (Row.RowState)
      {
        case DataRowState.Modified:
          for (int _column_index = 0; _column_index <= (NumberOfColumns - 1); _column_index++)
          {
            if (IsDataColumnModified(Row, _column_index))
            {
              return true;
            }
          }
          break;

        case DataRowState.Added:
        case DataRowState.Deleted:
          return true;
      }

      return false;
    }

    /// <summary>
    /// Is DataColumn modified?
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="ColumnIndex"></param>
    /// <returns></returns>
    private Boolean IsDataColumnModified(DataRow Row, Int32 ColumnIndex)
    {
      long _current_long;
      long _original_long;
      decimal _current_decimal;
      decimal _original_decimal;
      Boolean _current_bool;
      Boolean _original_bool;

      if (Row == null)
      {
        return false;
      }

      if ((Row[ColumnIndex] != DBNull.Value))
      {
        switch (ColumnIndex)
        {
          case EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000:
          case EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001:
          case EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002:
            _current_long = (long)Row[ColumnIndex, DataRowVersion.Current];
            _original_long = (long)Row[ColumnIndex, DataRowVersion.Original];

            return (_original_long != _current_long);

          case EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0000_INCREMENT:
          case EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0001_INCREMENT:
          case EGMMeterAdjustmentConstants.SQL_COLUMN_MC_0002_INCREMENT:
          case EGMMeterAdjustmentConstants.SQL_COLUMN_WIN:
            _current_decimal = (decimal)Row[ColumnIndex, DataRowVersion.Current];
            _original_decimal = (decimal)Row[ColumnIndex, DataRowVersion.Original];

            return (_original_decimal != _current_decimal);

          case EGMMeterAdjustmentConstants.SQL_COLUMN_USER_IGNORED:
            _current_bool = (bool)Row[ColumnIndex, DataRowVersion.Current];
            _original_bool = (bool)Row[ColumnIndex, DataRowVersion.Original];

            return (_original_bool != _current_bool);
        }
      }

      return false;
    }
  }
}
