﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: EGMMeterAdjustmentConstants.cs
// 
//   DESCRIPTION: EGMMeterAdjustmentConstants
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 21-DEC-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-DEC-2017 FGB    Initial release
//------------------------------------------------------------------------------
namespace WSI.Common.EGMMeterAdjustment
{
  public static class EGMMeterAdjustmentConstants
  {
    public const int RECORD_TYPE_CUT = 99;
    
    public const int SQL_COLUMN_SITE_ID = 00;
    public const int SQL_COLUMN_TERMINAL_ID = 01;
    public const int SQL_COLUMN_WORKING_DAY = 02;
    public const int SQL_COLUMN_GAME_ID = 03;
    public const int SQL_COLUMN_DENOMINATION = 04;
    public const int SQL_COLUMN_DATETIME = 05;
    public const int SQL_COLUMN_WIN = 06;
    public const int SQL_COLUMN_MC_0000 = 07; // COIN IN
    public const int SQL_COLUMN_MC_0001 = 08; // COIN OUT
    public const int SQL_COLUMN_MC_0002 = 09; // JACKPOT
    public const int SQL_COLUMN_RECORD_TYPE = 10;
    public const int SQL_COLUMN_USER_IGNORED = 11;
    public const int SQL_COLUMN_SAS_ACCOUNTING_DENOM = 12;
    public const int SQL_COLUMN_LAST_CREATED_DATETIME = 13;
    public const int SQL_COLUMN_LAST_REPORTED_DATETIME = 14;
    public const int SQL_COLUMN_USER_IGNORED_DATETIME = 15;
    public const int SQL_COLUMN_LAST_UPDATED_USER_ID = 16;
    public const int SQL_COLUMN_LAST_UPDATED_USER_DATETIME = 17;
    public const int SQL_COLUMN_STATUS = 18;
    public const int SQL_COLUMN_MODIFIED = 19;
    public const int SQL_COLUMN_MC_0000_INCREMENT = 20;
    public const int SQL_COLUMN_MC_0001_INCREMENT = 21;
    public const int SQL_COLUMN_MC_0002_INCREMENT = 22;
    public const int SQL_COLUMN_METER_MAX_VALUE = 23;
    public const int SQL_COLUMN_ROW_INDEX_SEQ = 24;
  }
}
