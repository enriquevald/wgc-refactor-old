//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Resource.cs
// 
//   DESCRIPTION: Class that offers culture associated methods.
// 
//        AUTHOR: AJQ
// 
// CREATION DATE: 26-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-AUG-2007 AJQ    First release.
// 08-FEB-2012 MPO    New static method, get a memorystream of the resource
// 03-APR-2012 MPO & RCI    Init without initializazing language, only neutral resources.   
// 22-MAY-2012 ACC    Suport multi-user access resource files. (GUI).
// 28-MAY-2013 NMR    Fixed blocking parameters reading while connection to DB is missing
//
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Drawing;
using System.IO;


namespace WSI.Common
{
  public static class Resource
  {
    private static bool m_folders_created = false;
    private static ResourceManager m_resource_manager;
    private static CultureInfo m_short_culture_info;
    private static CultureInfo m_culture_info;
    private static CultureInfo m_culture_info_en = new CultureInfo("en-US");
    private static CompareInfo m_compare_info;

    #region Properties
    //Gets current language for multilanguage purposes
    public static Int32 LanguageId
    {
      get
      {
        switch (Misc.ReadGeneralParams("Cashier", "Language"))
        {
          case "es":
            {

              return (Int32)LanguageIdentifier.Spanish;
            }
          case "en-us":
            {

              return (Int32)LanguageIdentifier.English;
            }
          case "fr":
            {

                return (Int32)LanguageIdentifier.French;
            }
          default:
            {

              return (Int32)LanguageIdentifier.English;
            }
        }
      }
    }

    //Gets current ISOCODE for multilanguage purposes
    public static String CountryISOCode2
    {
      get
      {
        return GeneralParam.GetString("RegionalOptions", "CountryISOCode2", "MX");
      }
    }
    #endregion

    /// <summary>
    /// Initialization
    /// </summary>
    public static void Init()
    {
      Init(false);
    }

    /// <summary>
    /// Initialization
    /// </summary>
    /// <param name="NeutralLanguage"></param>
    public static void Init(Boolean NeutralLanguage)
    {
      String _name;

      _name = Misc.ReadGeneralParams("Cashier", "Language");

      if (_name == null || _name == "")
      {
        _name = CultureInfo.CurrentCulture.Name;
      }

      if (NeutralLanguage)
      {
        m_folders_created = true;
      }

      Init(_name);
    }

    /// <summary>
    /// Initialization
    /// </summary>
    /// <param name="CultureName">Culture Name</param>
    public static void Init(String CultureName)
    {
      String[] _lang_files;
      String[] _tmp_files;
      String _resource_file_name;
      String _tmp_file_name;
      String _directory_file;

      if (!m_folders_created)
      {
        //
        // Create Per Language Folders
        //
        _lang_files = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory(), "*_WSI.Common.resources.dll");

        foreach (String _lang_file in _lang_files)
        {
          _directory_file = _lang_file.Substring(0, _lang_file.IndexOf("_WSI.Common.resources.dll"));
          _resource_file_name = _directory_file + "\\WSI.Common.resources.dll";

          // Create Language folder
          System.IO.Directory.CreateDirectory(_directory_file);

          // Copy Language DLL
          for (int _idx_try = 0; _idx_try < 3; _idx_try++)
          {
            if (_idx_try > 0)
            {
              System.Threading.Thread.Sleep(500);
            }
            try
            {
              System.IO.File.Copy(_lang_file, _resource_file_name, true);

              break;
            }
            catch { }

            //
            // Rename files with TMP files
            //
            if (_idx_try < 2)
            {
              try
              {
                _tmp_file_name = _directory_file + "\\WSI_RESOURCE_" + Path.GetRandomFileName() + ".TMP";
                System.IO.File.Move(_resource_file_name, _tmp_file_name);
              }
              catch { }
            }
          } // for  _idx_try

          //
          // Delete TMP files
          //
          _tmp_files = System.IO.Directory.GetFiles(_directory_file, "WSI_RESOURCE_*.TMP");
          foreach (String _tmp_file in _tmp_files)
          {
            try
            {
              System.IO.File.Delete(_tmp_file);
            }
            catch { }
          }

        } // for each lang

        m_folders_created = true;
      }

      if (m_resource_manager != null)
      {
        m_resource_manager.ReleaseAllResources();
        m_resource_manager = null;
      }

      m_resource_manager = new ResourceManager("WSI.Common.Resources.Resource", System.Reflection.Assembly.GetExecutingAssembly());

      // Create application cultures
      try
      {
        m_culture_info = new CultureInfo(CultureName);
      }
      catch
      {
        CultureName = "en-US";
        m_culture_info = new CultureInfo(CultureName);
      }

      m_short_culture_info = new CultureInfo(m_culture_info.TwoLetterISOLanguageName);

      // Get culture comparison class
      m_compare_info = CompareInfo.GetCompareInfo(CultureName);
    }

    public static String String(String Name)
    {
      Boolean _found;

      return String(Name, out _found);
    }

    /// <summary>
    /// Returns a resource string from the resource files.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static String String(String Name, out Boolean Found)
    {
      String _str;
      Int32 _mode;
      String _resource_name;

      Found = false;

      _str = null;

      if (WGDB.Initialized)
      {
        if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier.Voucher", "Mode"), out _mode))
        {
          _mode = 0;
        }

        if (_mode > 0)
        {
          _resource_name = "M" + _mode.ToString("00") + "-" + Name;

          try
          {
            _str = m_resource_manager.GetString(_resource_name, m_culture_info);
            if (_str != null)
            {
              Found = true;

              return _str;
            }
            _str = m_resource_manager.GetString(_resource_name, m_short_culture_info);
            if (_str != null)
            {
              Found = true;

              return _str;
            }
          }
          catch { }
        }
      }

      return StringNoVoucherMode(Name, out Found);
    }

    /// <summary>
    /// Returns a resource string from the resource files without using the parameter Cashier.Voucher.Mode.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static String StringNoVoucherMode(String Name)
    {
      Boolean _found;

      return StringNoVoucherMode(Name, out _found);
    }

    public static String StringNoVoucherMode(String Name, out Boolean Found)
    {
      String _str;

      Found = false;

      _str = null;

      try
      {
        _str = m_resource_manager.GetString(Name, m_culture_info);
        if (_str != null)
        {
          Found = true;

          return _str;
        }
        _str = m_resource_manager.GetString(Name, m_short_culture_info);
        if (_str != null)
        {
          Found = true;

          return _str;
        }
      }
      catch { }

      return "String '" + Name + "' not found.";
    }

    public static String String_En(String Name)
    {
      return m_resource_manager.GetString(Name, m_culture_info_en);
    }

    /// <summary>
    /// Parametrized string.
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public static String String(String Name, params Object[] list)
    {
      Boolean _found;

      return String(Name, out _found, list);
    }

    public static String String(String Name, out Boolean Found, params Object[] list)
    {
      int i;
      String nls;
      String old_value;
      String new_value;

      Found = false;

      if (WGDB.Initialized)
      {
        nls = Resource.String(Name, out Found);
      }
      else
      {
        nls = Resource.StringNoVoucherMode(Name, out Found);
      }

      i = 0;
      foreach (Object param in list)
      {
        old_value = "@p" + i.ToString();
        new_value = "";
        try
        {
          new_value = param.ToString();
        }
        catch
        {
          //
        }
        nls = nls.Replace(old_value, new_value);
        i++;
      }

      return nls;
    }


    /// <summary>
    /// Returns a resource string from the resource files.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static Bitmap Image(String ImageName)
    {
      Bitmap image;

      image = null;

      // Try to get image on localized country-subculture
      image = (Bitmap)m_resource_manager.GetObject(ImageName, m_culture_info);

      if (image == null)
      {
        // Try to get image on localized country culture
        image = (Bitmap)m_resource_manager.GetObject(ImageName, m_short_culture_info);
      }

      // Try to get image on default resource 
      if (image == null)
      {
        // Try to get image on localized country culture
        image = (Bitmap)m_resource_manager.GetObject(ImageName);
      }

      return image;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static MemoryStream MemoryStream(String Name)
    {
      MemoryStream _memory_stream;
      Byte[] _resource;

      _memory_stream = null;
      _resource = null;

      _resource = (Byte[])m_resource_manager.GetObject(Name, m_culture_info);

      if (_resource == null)
      {
        _resource = (Byte[])m_resource_manager.GetObject(Name, m_short_culture_info);
      }

      if (_resource == null)
      {
        _resource = (Byte[])m_resource_manager.GetObject(Name);
      }

      if (_resource != null)
      {
        _memory_stream = new MemoryStream(_resource);
      }

      return _memory_stream;
    }

    public static CompareInfo GetCompareInfo()
    {
      if(m_compare_info == null)
      {
        m_compare_info = CompareInfo.GetCompareInfo("en-US");
      }

      return m_compare_info;
    } // GetCompareInfo

  }
}
