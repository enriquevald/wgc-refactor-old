using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace WSI.Common
{
  public enum ENUM_TSV_PACKET
  {
    TSV_PACKET_WIN_CASHIER = 0,
    TSV_PACKET_WIN_KIOSK = 1,
    TSV_PACKET_WIN_GUI = 2,
    TSV_PACKET_SAS_HOST = 5,
    TSV_PACKET_WIN_MULTISITE_GUI = 12,
    TSV_PACKET_WIN_CASHDESK_DRAWS_GUI = 15,
    TSV_PACKET_SITE = 100,
    TSV_PACKET_SITE_JACKPOT = 101,
    TSV_PACKET_MOBILE_BANK = 102,
    TSV_PACKET_MOBILE_BANK_IMB = 103,
    TSV_PACKET_ISTATS = 104,
    TSV_PACKET_PROMOBOX = 105,
    //
    // Services
    //
    TSV_PACKET_WCP_SERVICE = 3,
    TSV_PACKET_WC2_SERVICE = 4,
    TSV_PACKET_WWP_SITE_SERVICE = 6,
    TSV_PACKET_WWP_CENTER_SERVICE = 7,
    TSV_PACKET_WXP_SERVICE = 8,
    //
    // Bigaboom
    //
    TSV_PACKET_BGJOBS_SERVICE = 10,
    //
    // PSA
    //
    TSV_PACKET_WAS_SERVICE = 20,
    TSV_PACKET_PSA_SERVICE = 21,
    TSV_PACKET_PSA_CLIENT = 22,
    //DATA SERVICE
    //TODO NEW ENUM TO DATA SERVICE
    TSV_PACKET_SMARTFLOOR_DATA_SERVICE = 23,
    //AFIP
    TSV_PACKET_AFIP_CLIENT = 24,
    //Protocols Service
    TSV_PACKET_PROTOCOLS_SERVICE= 25,

    TSV_PACKET_NOT_SET = -1
  }

  public enum SITE_STATE
  {
    UNKNOWN = -1,
    NOT_ACTIVE = 0,
    ENABLED = 1,
    DISABLED = 2,
    CLOSED = 3,
  }

  public enum PAY_MODE_CARD_REPLACEMENT
  {
    CASH = 1,
    POINT = 2,
    FREE = 3,
    CHECK = 4,
    CASH_CURRENCY = 5,
    CARD_CREDIT = 6,
    CARD_DEBIT = 7,
    CARD_GENERIC = 8,
    CREDITLINE = 9,
  }

  public struct TYPE_CARD_REPLACEMENT
  {
    // Structure used to print the card replacement voucher
    public Int64 account_id;
    public string old_track_data;
    public string new_track_data;
    public Currency card_replacement_price;
    public PAY_MODE_CARD_REPLACEMENT type;
    public Currency TotalBaseTaxSplit2;
    public Currency TotalTaxSplit2;
    public CurrencyExchangeResult ExchangeResult;
    public TYPE_SPLITS Splits;
    public CASHIER_MOVEMENT CashierMovement;
    public String[] AccountInfo;
  }

  public struct TYPE_CASH_REDEEM
  {
    public Currency total_cash_in;          // Initial cash in

    public Currency TotalDevolution         // devolution + cancellation
    {
      get
      {
        return cancellation + devolution;
      }
    }

    public Currency cancellation;              // Amount not used for play

    public Currency devolution;                // Amount used for play
    public Currency prize;                     // Amount won
    public String tax1_name;                   // tax1 name
    public String tax2_name;                   // tax2 name
    public String tax3_name;                   // tax3 name
    public Currency tax1;                      // tax1
    public Currency tax2;                      // tax2
    public Currency tax3;                      // tax3
    public Percentage tax1_pct;
    public Percentage tax2_pct;
    public Percentage tax3_pct;

    public Percentage TotalTaxesPct
    {
      get { return tax1_pct + tax2_pct + tax3_pct; }
    }

    public Currency TotalTaxes                 // tax1 + tax2+ tax3
    {
      get { return tax1 + tax2 + tax3; }
    }

    public Boolean tax_return_enabled;
    public Currency tax_return;

    public Boolean decimal_rounding_enabled;
    public Currency decimal_rounding;

    public Currency service_charge;

    public Boolean is_deposit;  // Indicate if the card deposit must be redeemed
    public Currency card_deposit;              // Deposit for the card

    public Currency won_lock_to_cancel;        // Amount to substract to WonLock if it is a cancellation

    public MultiPromos.PromoBalance lost_balance; // Total lost due redeem
    public DataTable lost_promotions;             // Lost promotions

    public Currency TotalRedeemed              // TotalDevolution + prize
    {
      get
      {
        return TotalDevolution + prize;
      }
    }
    public Currency TotalRedeemAfterTaxes      // TotalRedeemed - TotalTaxes + tax_return
    {
      get
      {
        //EOR 20-APR-2016
        return TotalRedeemed - TotalTaxes + TotalRounding - ServiceCharge + DevolutionCustody;
      }
    }

    public Currency TotalPaid                  // TotalRedeemAfterTaxes + card_deposit
    {
      get
      {
        return TotalRedeemAfterTaxes + ((is_deposit) ? card_deposit : 0);
      }
    }

    public Currency cancelled_promo_redeemable;       // Amount cancelled (Split B)

    public List<CancellableOperation> cancellable_operations;   // Includes SequenceId for devolution/cancel

    public Currency not_played_recharge;

    public Boolean RoundingEnabled
    {
      get
      {
        return tax_return_enabled || decimal_rounding_enabled;
      }
    }

    public Currency TotalRounding
    {
      get
      {
        return tax_return + decimal_rounding;
      }
    }

    public Currency ServiceCharge
    {
      get
      {
        return service_charge;
      }
    }

    //EOR 20-APR-2016
    public Currency DevolutionCustody;

  }

  public struct TYPE_SPLIT_ELEMENT
  {
    public String company_name;
    public String name;
    public Decimal cashin_pct;
    public Decimal cancellation_pct;           // Same as cashin_pct
    public Decimal devolution_pct;
    public String tax_name;
    public Decimal tax_pct;
    public String header;
    public String footer;
    public String footer_cashin;
    public String footer_dev;
    public String footer_cancel;
    public Boolean hide_total;
    public Boolean hide_currency_symbol;
    public Boolean total_in_letters;
    public String cash_in_voucher_title;
    public String cash_out_voucher_title;
    public Decimal chips_sale_pct;  //12-JUL-2016 EOR
  }

  public struct TYPE_SPLITS
  {
    public Boolean enabled_b;
    public TYPE_SPLIT_ELEMENT company_a;
    public TYPE_SPLIT_ELEMENT company_b;
    public Boolean prize_coupon;
    public String prize_coupon_text;
    public Boolean hide_promo;
    public String text_promo;
    public Boolean promo_and_coupon;
    public String text_promo_and_coupon;
    public Boolean rounding_enabled;
    public Int32 rounding_cents;
  }

  public struct TYPE_SITE_JACKPOT_WINNER : ICloneable
  {
    public DateTime awarded_date;
    public Int32 jackpot_index;
    public String jackpot_name;
    public String awarded_on_provider_name;
    public Int32 awarded_on_terminal_id;
    public String awarded_on_terminal_name;
    public Int64 awarded_to_account_id;
    public String awarded_to_account_name;
    public Currency amount;
    public Boolean paid;

    public object Clone()
    {
      TYPE_SITE_JACKPOT_WINNER _retval = new TYPE_SITE_JACKPOT_WINNER
      {
        awarded_date = awarded_date,
        jackpot_index = jackpot_index,
        jackpot_name = jackpot_name,
        awarded_on_provider_name = awarded_on_provider_name,
        awarded_on_terminal_id = awarded_on_terminal_id,
        awarded_on_terminal_name = awarded_on_terminal_name,
        awarded_to_account_id = awarded_to_account_id,
        awarded_to_account_name = awarded_to_account_name,
        amount = (Currency)amount.Clone(),
        paid = paid
      };

      return _retval;
    }
  }

  public enum SITE_JACKPOT_NOTIFICATION_STATUS
  {
    NotNotified = 0,
    Notified = 1
  }

  public struct TYPE_MAILING_PARAMETERS
  {
    public String authorized_server_list;
    public Boolean enabled;
    public Boolean smtp_enabled;
    public String smtp_server;
    public Int32 smtp_port;
    public Boolean smtp_secured;
    public String smtp_username;
    public String smtp_password;
    public String smtp_domain;
    public String smtp_email_from;
  }

  public struct TYPE_MAILING_PROVIDER_STATS
  {
    public String name;
    public Currency played;
    public Currency won;
    public Currency netwin;
    public Int32 num_terminals;
  }

  public struct TYPE_OCCUPATION_SESSION_STATS
  {
    public Boolean is_set_num_opened_sessions;
    public Boolean is_set_now;
    public Boolean is_set_last_hour;
    public Boolean is_set_till_now;
    public Int32 total_terminals;
    public Int32 num_opened_sessions;
    public Percentage now;
    public Percentage last_hour;
    public Percentage till_now;
  }

  public struct TYPE_CAPACITY_SESSION_STATS
  {
    public Int32 male_capacity;
    public Int32 female_capacity;
    public Int32 anonymous_capacity;
  }

  public struct TYPE_ACCOUNTS_SESSION_STATS
  {
    public Int32 accounts_created;
    public Int32 accounts_active30;
    public Int32 accounts_active90;
    public Int32 accounts_active180;
    public Int32 accounts_inactive;
    public Int32 accounts_total;
  }

  public struct TYPE_MAILING_REPORT
  {
    public String site_name;
    public DateTime date_from;
    public DateTime date_to;
    public Currency cashier_inputs;
    // DHA 11-MAR-2015
    public Currency cashier_cash_in_tax;
    // JML 03-MAR-2016
    public Currency tax_provisions;
    public Currency cashier_outputs;
    public Currency cashier_prize_gross;
    public Currency cashier_prize_taxes;
    public Currency cashier_result;
    public Currency site_jackpot;
    public Currency chips_sale;
    public Currency chips_purchase;
    public Currency game_profit;
    public Cashier.TYPE_LIABILITIES liabilities_to;
    public Cashier.TYPE_LIABILITIES liabilities_from;

    public TYPE_OCCUPATION_SESSION_STATS occupation;
    // DHA 11-MAR-2015
    public TYPE_CAPACITY_SESSION_STATS capacity_stats;
    public TYPE_ACCOUNTS_SESSION_STATS accounts_stats;
    public TYPE_MAILING_PROVIDER_STATS[] provider_stats;
  }

  public struct TYPE_MAILING_HOLDVSWIN_RECEPTION
  {
    public Boolean Enabled;
    public Int32 Visits;
    public Int32 VisitsLevel0;
    public Int32 VisitsLevel1;
    public Int32 VisitsLevel2;
    public Int32 VisitsLevel3;
    public Int32 VisitsLevel4;
    public Int32 VisitsLevel5;
    public Int32 VisitsMale;
    public Int32 VisitsFemale;
    public Int32 VisitsAnonymous;
  }

  public struct TYPE_MAILING_HOLDVSWIN_CASHIER
  {
    public Currency cashier_bill_in;
    public Currency cashier_tickets_created;
    public Currency cashier_cash_promos;
    public Currency cashier_tickets_paid;
    public Currency cashier_daily_netwin;
    public Currency cashier_handpays_paid;
    public String cashier_netwin_percent;
  }

  public struct TYPE_MAILING_HOLDVSWIN
  {
    public String site_name;
    public DateTime date_from;
    public DateTime date_to;
    public TYPE_MAILING_HOLDVSWIN_RECEPTION reception_data;
    public TYPE_MAILING_HOLDVSWIN_CASHIER cashier_data;

    public Currency egm_cash_total_in;
    public Currency egm_cash_total_out;
    public Currency egm_cash_netwin;
    public String egm_cash_netwin_percent;
    public Currency egm_cash_bill_in;
    public Currency egm_cash_ticket_in_redimible;
    public Currency egm_cash_ticket_out_redimible;
    public Currency egm_cash_netwin_redimible;
    public String egm_cash_netwin_redimible_percent;
    public Currency egm_cash_jackpots;
    public Currency egm_cash_handpays;

    public Currency egm_total_coin_in;
    public Currency egm_total_total_won;
    public Currency egm_total_hold;
    public String egm_total_hold_percent;
  }

  public enum CASH_MODE
  {
    PARTIAL_REDEEM = 0,
    TOTAL_REDEEM = 1
  }

  public enum MBMovementType
  {
    None = -1,
    ManualChangeLimit = 0,
    TransferCredit = 1,
    DepositCash = 2,
    CloseSession = 3,
    CreditNonRedeemable = 4,
    CreditCoverCoupon = 5,
    CreditNonRedeemable2 = 6,   // For future promotions
    AutomaticChangeLimit = 7,
    CloseSessionWithCashLost = 8,
    CashExcess = 9,
    CloseSessionWithExcess = 10,
    CashShortFall = 11,
  }

  public enum GIFT_TYPE
  {
    OBJECT = 0,
    NOT_REDEEMABLE_CREDIT = 1,
    DRAW_NUMBERS = 2,
    SERVICES = 3,
    REDEEMABLE_CREDIT = 4,
    UNKNOWN = 999
  }

  public enum GIFT_REQUEST_STATUS
  {
    PENDING = 0,
    DELIVERED = 1,
    EXPIRED = 2,
    CANCELED = 3,
  }

  public enum OperationSource
  {
    CASHIER = 1,
    MOBILE_BANK = 2,
    LAST_VOUCHER = 3,
    LAST_TICKET = 4
  }

  public enum VoucherSource
  {
    FROM_OPERATION = 1,
    FROM_VOUCHER = 2
  }

  public enum PinCheckStatus
  {
    OK = 0,
    WRONG_PIN = 1,
    ACCOUNT_BLOCKED = 2,
    ERROR = 3,
    ACCOUNT_NO_PIN = 4
  }

  public enum DrawType
  {
    BY_PLAYED_CREDIT = 0,
    BY_REDEEMABLE_SPENT = 1,
    BY_TOTAL_SPENT = 2,
    BY_POINTS = 3,
    BY_CASH_IN = 4,
    BY_CASH_IN_POSTERIORI = 5
  }

  public enum DrawAccountType
  {
    ALL = 0,
    ONLY_ANONYMOUS = 1,
    ONLY_PERSONALIZED = 2
  }

  public enum DrawGenderFilter
  {
    ALL = 0,
    MEN_ONLY = 1,
    WOMEN_ONLY = 2
  }

  // RCI 22-DEC-2010: Automatic Mailing Programming
  public enum MAILING_PROGRAMMING_TYPE
  {
    CASH_INFORMATION = 1,
    WSI_STATISTICS = 2,
    CONNECTED_TERMINALS = 3,
    ACCOUNT_MOVEMENTS = 4,
    TERMINALS_WITHOUT_ACTIVITY = 5,
    // QMP 16-APR-2013: General categories 
    // (for Mailing Instances not related to Mailing Programming)
    GENERAL_TEXT = 6,
    GENERAL_HTML = 7,
    WXP_MACHINE_METERS = 8,
    PATTERNS = 9,
    TITO_TICKET_OUT_REPORT = 10,
    MACHINE_AND_GAME_REPORT = 11,
    TITO_HOLD_VS_WIN = 12,
    // DMT 28/10/2016
    WIN_UP_FEEDBACK = 13,
    // DHA 29-JUN-2017 Score Report
    SCORE_REPORT = 14,    

    // EOR 28-JUN-2016: Report Tool
    // (Not Used Enum 101 to 199 Reserved for Report Tool
    REPORT_TOOL_101 = 101,
    // 101... 199
    REPORT_TOOL_199 = 199,
    
  }

  public enum MAILING_INSTANCES_STATUS
  {
    PENDING = 0,
    READY = 1,
    SENT = 2,
    ERROR_SENDING = 3,
    ERROR_PROCESSING = 4,
    ERROR_CONVERSION = 5
  }

  public enum CashierMovementsFilter
  {
    SESSION_ID = 1,
    SESSION_ID_BY_QUERY = 2,
    DATE_RANGE = 3,
    SESSIONS_FROM_MOVEMENTS = 4,
    GAMING_DAY_RANGE = 5
  }

  public enum GroupDateFilter
  {
    NO_FILTER = 0,
    HOURLY = 1,
    G24_HOUR = 2,
    DAILY = 3,
    WEEKLY = 4,
    MONTHLY = 5
  }

  public enum QuerySalesProviderFilter
  {
    NO_FILTER = 0,
    DATE_PROVIDER_TERMINAL = 1,
    DATE_PROVIDER = 2,
    PROVIDER_TERMINAL = 3,
    PROVIDER = 4,
    DATE = 5
  }

  public enum LicenseStatus
  {
    NOT_CHECKED = 0,
    INVALID = 1,
    VALID = 2,
    NOT_AUTHORIZED = 3,
    EXPIRED = 4,
    VALID_EXPIRES_SOON = 5
  }

  public enum DataSetRawFormat
  {
    BIN_DEFLATE_BASE64 = 1,
    XML_ZLIB_BASE64 = 2,
  }

  // SGB 06-OCT-2014 Field voucher
  public enum CashierVoucherField
  {
    Header = 0,
    Footer = 1,
    Title = 2
  }

  public enum PrizeComputationMode
  {
    RecalculateInitialCashInEveryTime = 0,  // Default
    KeepCountingInitialCashIn = 1,
    WithoutDevolution = 2
  }

  public enum AlarmSourceCode
  {
    NotSet = 0,
    GameTerminal = 1,
    Cashier = 2,
    User = 3,

    TerminalSASHost = 4,
    TerminalSASMachine = 5,
    TerminalSystem = 6,
    TerminalWCP = 7,
    Terminal3GS = 8,

    Service = 9,
    System = 10,

    PSASendReports = 11,
    MultiSite = 12,
    ELP01 = 13,
    Pattern = 14,
    NoteCounter = 15,

      //Intellia Manager
    WebService = 44,           //WebService
    ReceivedData = 45,         //Verify Data from client
    XMLGenerate = 46,          //XML Generate
    SATResponse = 47           //SAT RESPONSES
  }

  public enum AlarmCode
  {
    Unknown = 0x00000000,  // Warn

    TerminalSystem_GameMeterReset = 0x00020001,  // Warn
    TerminalSystem_GameMeterBigIncrement = 0x00020002,  // Warn
    TerminalSystem_GameMeterBigIncrement_ByMeter = 0x000A0000,  // Warn => 0x000A0000 + MeterCode
    TerminalSystem_HPCMeterBigIncrement = 0x00020003,  // Warn
    TerminalSystem_MachineMeterReset = 0x00020004,  // Warn
    TerminalSystem_MachineMeterBigIncrement = 0x00020005,  // Warn
    TerminalSystem_MachineMeterBigIncrement_ByMeter = 0x000B0000,  // Warn => 0x000B0000 + MeterCode
    TerminalSystem_GameMeterDenomination = 0x00020006,  // Warn
    TerminalSystem_PlaySessionWasManuallyClosed = 0x00020007,  // Warn
    TerminalSystem_SasMeterReset = 0x00020008,  // Warn
    TerminalSystem_SasMeterBigIncrement = 0x00020009,  // Warn
    TerminalSystem_MaxValueModified = 0x00020010,  // Warn
    TerminalSystem_SasHostFirstTime = 0x0002000A,  // Info
    TerminalSystem_UnreportedMetes = 0x00034001,  //Warn
    TerminalSystem_MeterRollover = 0x00034002,
    TerminalSystem_HandpayWarningAmount = 0x00034003,  // Warn
    TerminalSystem_BillInserted = 0x00034004,  // Info

    //Intellia
    //WebServiceXXXXXXX = 0x00060001
    WebService_Transaction_Begin = 0x00060001,
    WebService_Transaction_Error = 0x00060002,
    WebService_Transaction_End = 0x00060003,

    WebService_Session_Begin = 0x00060004,
    WebService_Session_TimeOut = 0x00060005,            //DATA_RC_SESSION_TIMEOUT
    WebService_Session_End = 0x00060006,                //DATA_RC_OK
    WebService_Session_BadSeqID = 0x00060007,           //DATA_RC_BAD_SEQID,
    WebService_Session_BadMessageType = 0x00060008,     //DATA_RC_BAD_MESSAGE_TYPE,
    WebService_Session_BadChecksum = 0x00060009,        //DATA_RC_BAD_CHECKSUM,
    WebService_Session_IncoherenceWithDBData = 0x00060010,    //DATA_RC_INCOHERENCE_WITH_DB_DATA,
    WebService_Session_InternalError = 0x00060011,            //DATA_RC_INTERNAL_DATA_ERROR,
    WebService_Session_BadLogin = 0x00060012,                  //DATA_RC_BAD_LOGIN,
    WebService_Session_TooManyRetrys = 0x00060013,            //DATA_RC_TOO_MANY_RETRYS,
    WebService_Session_Kill_TimeOut = 0x00060014,
    WebService_Session_Remove = 0x00060015,
    WebService_Session_Default = 0x00060016,
    WebService_Session_BadTransactionID = 0x00060017,

    ReceivedData_BadMessageData = 0x00070001,          //DATA_RC_BAD_MESSAGE_DATA

    // Protocols
    // - WCP
    TerminalSystem_WcpConnected = 0x00020011,     // Info
    TerminalSystem_WcpDisconnected = 0x00020012,  // Error
    // - WC2
    TerminalSystem_Wc2Connected = 0x00020021,  // Info
    TerminalSystem_Wc2Disconnected = 0x00020022,  // Error
    // - WWP Center
    TerminalSystem_WwpSiteConnected = 0x00020031,     // Info
    TerminalSystem_WwpSiteDisconnected = 0x00020032,  // Error
    // - WWP Site
    TerminalSystem_WwpConnectedToCenter = 0x00020041,     // Info
    TerminalSystem_WwpDisconnectedFromCenter = 0x00020042,  // Error

    // SERVICES
    // - Start/Stop
    Service_Started = 0x00030001,  // Info
    Service_Stopped = 0x00030002,  // Error
    Service_Stopping = 0x00030003,  // Warn
    Service_Running = 0x00030004,  // Info
    Service_StandBy = 0x00030005,  // Info
    Service_OperationsAfterHours = 0x00030006, // Warn
    Service_TITO_OfflineTicketDiscardError = 0x00030007, // Warn    
    Service_Device_Disconnection = 0x00030008,

    // - License
    Service_LicenseExpired = 0x00030011, // Error
    Service_LicenseWillExpireSoon = 0x00030012, // Warn
    // - Software update
    Service_NewVersionDownloaded = 0x00030021,  // Info
    Service_NewVersionAvailable = 0x00030022,  // Info

    // - Ticket amount
    Service_Ticket_NewAmountIsNullOrZero = 0x00030031, // 1 - New amount is null or zero
    Service_Ticket_NewAmountGreaterOfOldAmount = 0x00030032, // 2 - New amount greater of old amount
    Service_Ticket_NewAmountSmallerOfOldAmount = 0x00030033, // 3 - New amount smaller of old amount
    Service_Ticket_NewCurrencyIsDiferentOfOlder = 0x00030034, // 4 - New currency is diferent of older
    Service_Ticket_NewAmountGreaterOfOldAmountWithDiferentCurrency = 0x00030035, // 5 - New amount greater of old amount with diferent currency
    Service_Ticket_NewAmountSmallerOfOldAmountWithDiferentCurrency = 0x00030036, // 6 - New amount smaller of old amount with diferent currency



    // MACHINE
    Machine_Memory_corruptionOne = 0x00060028,
    Machine_Memory_corruptionBoth = 0x00060029,
    Machine_VLT_Blocked = 0x0006002A,
    Machine_VLT_Unblocked = 0x0006002B,
    Machine_Terminal_Transfer_Status = 0x0006002C,
    Machine_TITO_Wrong_Printed_Ticket_ID = 0x0006002D,
    Machine_TITO_Cant_Read_Ticket_Info = 0x0006002E,
    Machine_AFT_Not_Authorized = 0x0006002F,
    Machine_Serial_Number_Not_Expected = 0x00060030,
    Machine_Asset_Number_Not_Expected = 0x00060031,
    Machine_Manual_Block = 0x00060032,
    Machine_Manual_Unblock = 0x00060033,
    Machine_Sas_Accounting_Denomination_Changed = 0x00060034,

    // Reserved 0x00060035..0x00060040


    // USER
    // - Login
    User_MaxLoginAttemps = 0x00040001,
    //-Closing Cashier
    User_WrongDelivered = 0x00040002,
    //-User Cashier Session closed from GUI
    User_CashierSessionClosedFromGUI = 0x00040003,
    User_TITO_TicketOffline = 0x00040005,
    User_DenominationDepositUnbalanced = 0x00040006,
    User_DenominationCollectionUnbalanced = 0x00040007,
    User_DenominationCollectionWithZero = 0x00040009,

    // Reopen cage session
    User_CageSessionReopened = 0x00040008,
    User_CashierSessionReopened = 0x00040010,

    // Draw numbers
    User_PlaSessionDiscardedToGiveDrawNumbers = 0x00040012,

    // Ticket conciliation
    User_ReconciledTicketNotExpected = 0x00040014,
    User_ReconciledTicketNotValid = 0x00040015,

    // Gambling tables alarms
    User_GamingTables_BetsOverHandByCustomer = 0x00040100,
    User_GamnigTables_SessionLossFor = 0x00040101,
    User_GamnigTables_InvestmentsWithCreditDebitCard_EqualOrOver = 0x00040102,
    User_GamingTables_CustomerGetsPaidWithoutAPurchase = 0x00040103,
    User_GamingTables_BetsVariationCustomerPerSession = 0x00040104,
    User_GamingTables_PaymentDoneByPlayer_UnderMinutes = 0x00040105,
    User_GamingTables_PlayerSittingTime = 0x00040106,    
    User_GamingTables_AmountPurchaseInOneOperation = 0x00040107,
    User_GamingTables_AmountPaidInOneOperation = 0x00040108,
    User_GamnigTables_GamingTable3DayLost = 0x00040109,


    //
    // Until we do all alarms of Logrand:
    //    Don't use numbers to 0x00040150,  Thanks
    //    Esimated time of finish: 30-oct-2017
    //





    // SYSTEM
    // Summary:
    //     A change in the power status of the computer is detected, such as a switch
    //     from battery power to A/C. The system also broadcasts this event when remaining
    //     battery power slips below the threshold specified by the user or if the battery
    //     power changes by a specified percentage.
    SystemPowerStatus_PowerStatusChange = 0x00050001,
    // Summary:
    //     The system has resumed operation after a critical suspension caused by a
    //     failing battery.
    SystemPowerStatus_ResumeCritical = 0x00050002,
    //
    // Summary:
    //     Battery power is low.
    SystemPowerStatus_BatteryLow = 0x00050003,


    // WCP Operations "Events" --> Alarms 0x0006 
    //
    // 0x00060000 + OperationCode
    //

    // WCP DeviceStatus        --> Alarms 0x0007 
    //
    // 0x00070000 + ((DeviceCode << 8) & 0x0000FF00) + (DeviceStatus & 0x000000FF)
    //

    // WPP Psa Client --> Alarms 0x0008 
    // Alarm message is set in resource named STR_EA_0x0008000?
    WWPClientPSA_ErrorGettingDataDailyReport = 0x00080001,
    WWPClientPSA_ErrorGettingDataPlaySessions = 0x00080002,
    WWPClientPSA_ErrorGettingDataStatistics = 0x00080003,

    // MultiSite --> Alarms 0x0009 
    // Alarm message is set in resource named STR_EA_0x0009000?
    MultiSite_AccountWithNonUniqueHolderId = 0x00090001,       // Warning --> MultiSite: Cuenta con Documento '@p0' existente en otra cuenta (@p1)
    MultiSite_AccountWithExistsTrackData = 0x00090002,         // Warning --> MultiSite: Cuenta con Tarjeta '@p0' existente. Se recicla la cuenta existente (@p1)
    MultiSite_AccountWithChangesOfOtherSite = 0x00090003,      // Warning --> MultiSite: Cuenta con cambios de otra sala. Se guardan los cambios para la sala: @p0
    MultiSite_AccountWithNegativePoints = 0x00090004,          // Warning --> MultiSite: Se actualiz� el balance de puntos de la cuenta (@p0) quedando en negativo (@p1).
    MultiSite_UnknownSiteTriedToConnect = 0x00090005,          // Warning --> MultiSite: La sala @p0 no est� habilitada para conectarse al MultiSite
    MultiSite_AccountWithWrongAccountType = 0x00090006,        // Warning --> MultiSite: La sala @p0 sube la cuenta anonima @p1 y en el centro es personal
    MultiSite_MultiCurrencyMultiSiteNewCurrency = 0x00090007,         // Warning --> MultiSite: New national currency in the site @p0: @p1
    MultiSite_MultiCurrencySiteChangeNationalCurrency = 0x00090008,   // Warning --> MultiSite: Change national currency in the site @p0: @p1 (before @p2)
    MultiSite_MultiCurrencySiteChangeForeignCurrency = 0x00090009,    // Info --> MultiSite: Change foreign currency in the site @p0: @p1 (before @p2)

    // Reserved                                          
    // TerminalSystem_GameMeterBigIncrement_ByMeter    = 0x000AXXXX   // Warn => 0x000AXXXX + MeterCode (from 0 to 1000)
    // TerminalSystem_MachineMeterBigIncrement_ByMeter = 0x000BXXXX   // Warn => 0x000BXXXX + MeterCode (from 0 to 1000)

    // ELP --> Alarms 0x0010
    ELP_IncorrectExternalTrackdata = 0x00100001,
    ELP_ErrorConfirmationRedeem = 0x00100002,
    ELP_ErrorUpdateAccount = 0x00100003,
    ELP_ErrorTrackdataRelation = 0x00100004,
    ELP_ErrorStatus = 0x00100005,
    ELP_ErrorInsertTransactionInterface = 0x00100006,
    ELP_ErrorTrackdataNotAssignedToAccount = 0x00100008,
    ELP_ErrorInsertCashInterface = 0x00100009,

    // WCP Error code(s) Alarms 0x0011
    WCP_TerminalParamsDoNotMatch = 0x00110001,
    WCP_TITO_StackerChangeFail = 0x00110002,
    WCP_TITO_WrongStackerState = 0x00110003,
    WCP_TITO_EmptyTerminal = 0x00110004,
    WCP_TITO_WrongStackerTerminal = 0x00110005,
    WCP_TITO_WrongValidationNumber = 0x00110009,
    WCP_SuspectedHandPay = 0x00110010,
    WCP_SwValidationError = 0x00110011,
    WCP_SwValidationOK = 0x00110012,

    // Custom Alarms
    CustomAlarm_Played = 0x00200001,
    CustomAlarm_Won = 0x00200002,
    CustomAlarm_Canceled_Credit = 0x00200003,
    CustomAlarm_Jackpot = 0x00200004,
    CustomAlarm_Payout = 0x00200005,
    CustomAlarm_Counterfeit = 0x00200006,
    CustomAlarm_Without_Plays = 0x00200007,
    CustomAlarm_Player_Birthday = 0x00200008,
    CustomAlarm_Soon_Player_Birthday = 0x00200009,
    CustomAlarm_HighRoller = 0x00200010,
    CustomAlarm_HighRoller_Anonymous = 0x00200011,

    // Reader Machine Alarms
    Reader_Stranger_Detected = 0x00300001,
    Reader_Denomination_Changed = 0x00300002,
    Reader_Strap_Limit = 0x00300003,
    Reader_Facing_Error = 0x00300004,
    Reader_Orientation_Error = 0x00300005,
    Reader_Double_Detected = 0x00300006,
    Reader_Suspect_Document = 0x00300007,
    Reader_Jam_Detected = 0x00300008,
    Reader_Stacker_Full = 0x00300009,
    Reader_No_Call = 0x00300010,
    Reader_Chain_Detected = 0x00300011,
    Reader_Configuration = 0x00300012,
    Reader_Combined_Denomination_Change_Strap_Limit = 0x00300013,
    Reader_Comunication_Error = 0x00300014,
    Reader_Changing_Operating_Mode = 0x00300015,
    Reader_Changing_Strap_Limit = 0x00300016,
    Reder_Invalid_Ticket = 0x00300017,
    Reader_Ticket_Already_Collected = 0x00300018,
    Reader_Ticket_With_Duplicated_Number = 0x00300019,
    Reader_Ticket_Sent_Two_Times = 0x00300020,
    Reader_Undefined_Message = 0x00300021,
    Reader_Undefined_Elements = 0x00300022,
    Reader_No_Count = 0x00300023,

    // CountR Alarms
    CountR_Door_MainDoor_Closed = 0x00500001,
    CountR_Door_MainDoor_Opened = 0x00500002,
    CountR_Door_HeadDoor_Closed = 0x00500003,
    CountR_Door_HeadDoor_Opened = 0x00500004,
    CountR_OperatorCard_Rejected = 0x00500005,
    CountR_OperatorCard_Inserted = 0x00500006,
    CountR_OperatorCard_Captured = 0x00500007,
    CountR_OperatorCard_Issued = 0x00500008,
    CountR_CustomerCard_Rejected = 0x00500009,
    CountR_CustomerCard_Inserted = 0x00500010,
    CountR_CustomerCard_Captured = 0x00500011,
    CountR_CustomerCard_Issued = 0x00500012,
    CountR_Banknote_Rejected = 0x00500013,
    CountR_Banknote_Inserted = 0x00500014,
    CountR_Banknote_Stacked = 0x00500015,
    CountR_Ticket_Rejected = 0x00500016,
    CountR_Ticket_Inserted = 0x00500017,
    CountR_Ticket_Stacked = 0x00500018,
    CountR_CallOperator = 0x00500019,

    //KioskR
    CountR_BankCard_Inserted = 0x00500020,
    CountR_BankCard_Captured = 0x00500021,
    CountR_BankCard_Issued = 0x00500022,
    CountR_Bank_Transaction_Started = 0x00500023,
    CountR_Bank_Transaction_End_Success = 0x00500024,
    CountR_Bank_Transaction_End_Failure = 0x00500025,
    CountR_NFC_Device_Arrived = 0x00500026,
    CountR_NFC_Device_Departed = 0x00500027,
    CountR_NFC_Device_Rejected = 0x00500028,
    CountR_Incorrect_Paid_Ticket = 0x00500029,
    CountR_Incorrect_Unpaid_Ticket = 0x00500030,
    CountR_Incorrect_Created_Ticket = 0x00500031,
    CountR_Incorrect_Ticket_Status = 0x00500032,
    CountR_GetTicket_Error = 0x00500033,
    CountR_Payment_Error = 0x00500034,
    CountR_Transaction_Error = 0x00500035,

    // Reserved To - 0x00500500
    CountR_Custom_Event = 0x00500500,


    // Gaming Tables Tournaments Alarms
    Gaming_Tables_Tournaments_Manually_Started = 0x00120000,
    Gaming_Tables_Tournaments_Manually_Finished = 0x00120001,


    // Alarms reserved for pattern alarms
    //0x00980000
    // ...to....
    //0x01000000

  }

  public enum AlarmSeverity
  {
    Info = 1,
    Warning = 2,
    Error = 3
  }

  public enum ENUM_GUI
  {
    GUI_SETUP = 0,
    GUI_ALARMS = 1,
    GUI_VERSION_CONTROL = 2,
    GUI_AUDITOR = 3,
    GUI_CONFIGURATION = 4,
    GUI_COMMUNICATIONS = 5,
    GUI_CONTROL = 6,
    GUI_TICKETSMANAGER = 7,
    GUI_STATISTICS = 8,
    GUI_JACKPOTMANAGER = 9,
    GUI_SYSTEMMONITOR = 10,
    GUI_INVOICE_SYSTEM = 11,
    GUI_SOFTWARE_DOWNLOAD = 12,
    GUI_DATECODE = 13,
    WIGOS_GUI = 14,
    CASHIER = 15,
    WINUP = 16,
    MOBIBANK = 17,
    ISTATS = 104,
    MULTISITE_GUI = 200,
    CASHDESK_DRAWS_GUI = 201,
    CHILE_SIOC = 202,
    SMARTFLOOR = 203,
    SMARTFLOOR_APP = 204,
    REPORTS_TOOL = 205
  }

  public enum ENUM_MONEY_SOURCE
  {
    MONEY_SOURCE_UNKNOWN = 0,

    MONEY_SOURCE_CREDIT_LKAS = 1,
    MONEY_SOURCE_VOUCHER = 2,
    MONEY_SOURCE_SMARTCARD = 3,
    MONEY_SOURCE_CREDIT_LKT = 4,

    MONEY_SOURCE_NOTE_ACCEPTOR = 5,
    MONEY_SOURCE_COIN_ACCEPTOR = 6,

    MONEY_SOURCE_GAME_PRIZE = 7,

    MONEY_SOURCE_SAS_HOST = 20,
    MONEY_SOURCE_HANDPAY = 21,

    MONEY_SOURCE_EGM_NOTE_ACCEPTOR = 30,

  }

  public enum ENUM_MONEY_TARGET
  {
    MONEY_TARGET_UNKNOWN = 0,

    MONEY_TARGET_CREDIT_LKAS = 1,
    MONEY_TARGET_VOUCHER = 2,
    MONEY_TARGET_SMARTCARD = 3,
    MONEY_TARGET_CREDIT_LKT = 4,

    MONEY_TARGET_GAME_PLAY = 5,

    MONEY_TARGET_COIN_HOPPER = 10,

    MONEY_TARGET_SAS_HOST = 20,
    MONEY_TARGET_HANDPAY = 21,

    MONEY_TARGET_EGM_CREDIT = 30,

  }

  public enum ENUM_ACCEPTED_MONEY_STATUS
  {
    ACCEPTED_MONEY_STATUS_NONE = 0,
    ACCEPTED_MONEY_STATUS_ACCEPTED = 1,
    ACCEPTED_MONEY_STATUS_TRANSFERRING = 2,
    ACCEPTED_MONEY_STATUS_TRANSFERRED = 3,
    ACCEPTED_MONEY_STATUS_TRANSFER_FAILED = 4,
    ACCEPTED_MONEY_STATUS_ERROR = 5,
  }

  //SSC 21-MAR-2012
  public enum MB_USER_TYPE
  {
    NOT_ASSIGNED = -1,
    ANONYMOUS = 0,
    PERSONAL = 1,
    SYS_ACCEPTOR = 2,
    SYS_PROMOBOX = 3,
    SYS_TITO = 4
  };

  public enum DOCUMENT_TYPE
  {
    WITHOLDING = 1,
    SIGNATURE = 2,
    PHOTO_GIFT = 3,
    PAYMENT_ORDER = 4,
    PAYMENT_ORDER_LOGO = 5,
    STATEMENT_LOGO = 6,
    STATEMENT_PDF = 7
  };

  // ANG 21-MAY-2012
  public enum MEDIA_TYPE
  {
    IMAGE = 1,
    AUDIO = 2,
    VIDEO = 3
  };


  // ANG 24-MAY-2012
  public enum PLAYER_INFO_FIELD_TYPE
  {
    FULL_NAME = 1,
    CITY = 2,
    ZIP = 3,
    PIN = 4,
    ADDRESS_01 = 10,
    ADDRESS_02 = 11,
    ADDRESS_03 = 12,
    EMAIL_01 = 20,
    EMAIL_02 = 21,
    PHONE_NUMBER_01 = 30,
    PHONE_NUMBER_02 = 31,
    ID1 = 40,
    ID2 = 41,
    NAME = 52,
    SURNAME1 = 50,
    SURNAME2 = 51,
    MIDDLE_NAME = 53,
    BIRTHDAY = 60
  };


  // ANG 24-MAY-2012
  public enum PLAYER_FUNCTIONALITIES_TYPE
  {
    SHOW_DRAW = 0,
    SHOW_PROMOTIONS = 1,
    SHOW_FUTURE_PROMOTIONS = 2
  }

  public enum WKT_IMAGES_ID
  {

    IMG01 = 0,
    IMG02 = 1,
    IMG03 = 2,
    IMG04 = 3,
    IMG05 = 4,
    IMG06 = 5,
    IMG07 = 6,
    IMG08 = 7,
    IMG09 = 9,
    IMG010 = 10
  }

  public enum WKT_FIELD_TYPE
  {
    NUMBER = 1,
    TEXT = 2,
    EMAIL = 3,
    ZIPCODE = 4,
    PHONE = 5,
    DATE = 6
  }


  // ANG 14-06-2012
  public enum WKT_UPDATE_FIELD_STATUS
  {
    INTERNAL_BEING_UPDATED = -1,
    UPDATED = 0,
    NOT_UPDATED = 1,
    ERROR_NOT_EDITABLE = 2,
    ERROR_MIN_LENGTH = 3,
    ERROR_MAX_LENGTH = 4,
    ERROR_VALIDATION = 5,
    ERROR_INVALID_DATE = 6,
    ERROR_LEGAL_AGE = 7,
    ERROR = 8
  }

  // DDM 03-AUG-2012
  public enum ACCOUNT_MARITAL_STATUS
  {
    //RGR 05-05-2017
    EMPTY = 0,
    SINGLE = 1,
    MARRIED = 2,
    DIVORCED = 3,
    WIDOWED = 4,
    UNMARRIED = 5,
    OTHER = 6,
    UNSPECIFIED = 7,
    ANNULED = 8

  }

  // DDM 03-AUG-2012
  public enum ACCOUNT_HOLDER_ID_TYPE
  {
    UNKNOWN = 0,   // is shown as "OTHER"
    RFC = 1,
    CURP = 2,
    // RCI & XIT 11-JUL-2014: Do not define new hoder id types.
    //ELECTOR_CODE = 3,
    //PASSPORT = 4,
    //MILITARY_SERVICE_CARD = 5,
    //FM2 = 6,
    //FM3 = 7,
    //DRIVING_LICENSE = 8,
    //PROFESSIONAL_CERTIFICATE = 9,
    //CONSULAR_ID = 10,
    //DNI = 11,
    //MAX_TYPES = 12
  }

  // XCD 16-AUG-2012
  public enum GUI_USER_BLOCK_REASON
  {
    NONE = 0x0000,
    DISABLED = 0x0001,
    WRONG_PASSWORD = 0x0002,
    INACTIVITY = 0x0004,
    UNSUBSCRIBED = 0x0008
  }

  // DDM 27-AUG-2012
  public enum ACP_USER_TYPE
  {
    CASHIER = 0,
    MOBILE_BANK = 1
  }

  // RXM 01-OCT-2012
  public enum PHONE_TYPE
  {
    MOBILE_PHONE = 1,
    HOME_PHONE = 2,
    RADIO = 3,
    MESSAGE_PHONE = 4,
    UNDEFINED_PHONE = 5,
    BUSINESS_PHONE = 6,
    FAX = 7,
  }

  public enum GENDER
  {
    UNKNOWN = 0,
    MALE = 1,
    FEMALE = 2
  }

  public enum PLAYER_RECHARGE_STATUS
  {
    PENDING = 1,
    IN_PROGRESS = 2,
    ERROR = 3,
    OK = 4,
    TIMEOUT = 5
  }

  // RRB 06-FEB-2013
  public enum ACCOUNT_POINTS_STATUS
  {
    REDEEM_ALLOWED = 0,
    REDEEM_NOT_ALLOWED = 1,
  }

  // QMP 18-FEB-2013
  public enum OPERATIONS_SCHEDULE_TYPE
  {
    DAY_OF_WEEK = 0,
    DATE_RANGE = 1,
  }

  public enum ACCOUNT_FLAG_STATUS
  {
    ACTIVE = 0,
    NO_ACTIVE = 1,
    EXPIRED = 2,
    CANCELLED = 3
  }

  public enum FLAG_EXPIRATION_TYPE
  {
    UNKNOWN = 0,

    NEVER = 1,
    DATETIME = 2,

    //SECONDS = 10,
    MINUTES = 11,
    HOURS = 12,
    DAYS = 13,
    MONTH = 14,
  }

  public enum PROMOTION_FLAG_TYPE
  {
    AWARDED = 0,
    REQUIRED = 1,
    PREASSIGNED = 2,
  }

  public enum MB_SALES_LIMIT_OPERATION
  {
    EXTEND_LIMIT = 0,       // Limit Extension
    CHANGE_LIMIT = 1,       // Limit Edit
    DEPOSIT = 2,            // After Deposit 
    CLOSE_SESSION = 3       // Close MB Cash Session 
  }

  public enum CASHIER_SESSION_CLOSE_STATUS
  {
    OK = 0,                 // The process was finished successfully
    ALREADY_CLOSED = 1,     // The session was already closed
    ERROR = 2               // There was an error 
  }

  public static class SecureTcpClientConnection
  {
    public const int Timeout = 3 * 60 * 1000; // 3 minutes
  }

  //LEM 06-FEB-2013 Brought from CashierBusinessLogic
  public enum ACCOUNT_USER_TYPE
  {
    NONE = -1,
    ANONYMOUS = 0,
    PERSONAL = 1
  }

  //JMM 27-MAY-2013
  public enum MB_CASHIER_SESSION_CLOSE_STATUS
  {
    OK = 0,                   // The process was finished successfully
    NO_SESSION_ASSIGNED = 1,  // There is no session assigned to the terminal
    ERROR = 2                 // There was an error 
  }

  // RBG 25-JUN-2013: GROUPS
  public enum GROUP_CONTENT_TYPE
  {
    TERMINALS = 0,
    CLIENTS = 1
  }

  // ACM 19-JUL-2013
  public enum ACCOUNT_SCANNED_OWNER
  {
    NONE = 0,
    HOLDER = 1,
    BENEFICIARY = 2
  }

  // Levels when user's recharge/redemm is equal or greater than n times the SMGVDF.    
  public enum ENUM_ANTI_MONEY_LAUNDERING_LEVEL
  {
    None = 0,
    IdentificationWarning = 1,
    ReportWarning = 2,
    Identification = 3,
    Report = 4,
    MaxAllowed = 5
  }

  public enum ENUM_CARD_RECYCLE_STATUS
  {
    Recycled = 0,
    ErrorOccured = 1,
    AccountBalanceNotZero = 2,
    AccountBalanceIsNull = 3,
    TrackDataInvalid = 4,
    InvalidAccount = 5,
    FailedToCreateCashierMovement = 6,
    FailedToCreateAccountsMovement = 7,
    TrackDataConvertToInternalFailed = 8,
    ExceptionOccured = 9
  }

  // DDL & RCI 28-OCT-2013
  public enum ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT
  {
    Never = 0,
    OnlyWhenCross = 1,
    Always = 2
  }

  // Defines if we are doing a recharge or a payout.
  public enum ENUM_CREDITS_DIRECTION
  {
    None = 0,
    Recharge = 1,
    Redeem = 2
  }

  public enum ENUM_SAS_METER_HISTORY
  {
    NONE = 0,
    TSMH_HOURLY = 1,
    TSMH_METER_RESET = 10,
    TSMH_METER_ROLLOVER = 11,
    TSMH_METER_FIRST_TIME = 12, // AJQ 12-APR-2018, This is a group event for the first time (we don't change its name to "GROUP_xxx")
    TSMH_METER_FIRST_TIME_SINGLE_METER = 21, // AJQ 12-APR-2018, This is an event for the first time of a single meter

    TSMH_METER_DISCARDED = 13,
    TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE = 14,
    TSMH_METER_SERVICE_RAM_CLEAR = 15,                            //FGB  16-JUN-2016 Generated by the service when it detects all meters equal to 0.
    TSMH_METER_MACHINE_RAM_CLEAR = 16,                              //FGB  16-JUN-2016 Generated by an event sent by a machine when SAS detects all meters equal to 0.
    TSMH_MINCETUR_DAILY_REPORT = 20,
    TSMH_MANUAL_INPUT = 99,                                         //XGJ  23-GEN-2018 Lottery cut 
    //Grouped meters
    TSMH_METER_GROUP_ROLLOVER = 110,                                //FGB  16-JUN-2016 Grouped meters of Rollover
    TSMH_METER_GROUP_SAS_ACCOUNT_DENOM_CHANGE = 140,                //FGB  16-JUN-2016 Grouped meters of SAS Account Denom Change
    TSMH_METER_GROUP_SERVICE_RAM_CLEAR = 150,                       //FGB  16-JUN-2016 Grouped meters of Service RAM Clear
    TSMH_METER_GROUP_MACHINE_RAM_CLEAR = 160,                       //FGB  16-JUN-2016 Grouped meters of Machine RAM Clear
    TSMH_METER_GROUP_LAST_KNOWN_METERS = 170,                       //FGB  16-JUN-2018 Grouped meters of pre-event RAM Clear or SAS Account Denom Change
    TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION = 180,  //FGB  30-APR-2018 Grouped meters of E-Box Before Disconnection
    TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_CONNECTION = 190      //FGB  30-APR-2018 Grouped meters of E-Box Before Connection
  }

  [Flags]
  public enum ENUM_SHOW_EXTENDED_METER_TYPE
  {
    SEMT_NONE = 0x000,
    SEMT_RESET_TYPE_10 = 0x001,                             //Old Reset type    (not detailed)    (meter_type = 10)
    SEMT_ROLLOVER_TYPE_11 = 0x002,                          //Old Rollover type (not detailed)    (meter_type = 11)
    SEMT_DENOM_CHANGE = 0x004,                              //Denom change      (not detailed)    (meter_type = 14)
    SEMT_SERVICE_RAM_CLEAR = 0x008,                         //Service RAM Clear (not detailed)    (meter_type = 15)
    SEMT_MACHINE_RAM_CLEAR = 0x010,                         //Machine RAM Clear (not detailed)    (meter_type = 16)
    //Group Meters
    SEMT_GROUP_ROLLOVER = 0x020,                            //Rollover          (detailed)        (meter_type = 110)
    SEMT_GROUP_DENOM_CHANGE = 0x040,                        //Denom change      (detailed)        (meter_type = 140)
    SEMT_GROUP_SERVICE_RAM_CLEAR = 0x080,                   //Service RAM Clear (detailed)        (meter_type = 150)
    SEMT_GROUP_MACHINE_RAM_CLEAR = 0x100                    //Machine RAM Clear (detailed)        (meter_type = 160)
  }

  public enum ENUM_SAS_FLAGS
  {
    SITE_DEFINED = 0,
    NOT_ACTIVATED = 1,
    ACTIVATED = 2,
    SYSTEM_DEFINED = 3,
    MACHINE_DEFINED = 4,
    EXTENDED_METERS_LENGHT = 5,
    NON_EXTENDED_METERS_LENGHT = 6,
    ACTIVATED_AND_AUTOMATICALLY = 7,
  }

  public enum ENUM_AUTHENTICATION_METHOD
  {
    NONE = 0x00,
    CRC16 = 0x01,
    CRC32 = 0x02,
    MD5 = 0x04,
    KOBETRON_I = 0x08,
    KOBETRON_II = 0x10,
    SHA1 = 0x20,
    ROMCRC16 = 0x81,
  }

  public enum ENUM_AUTHENTICATION_STATUS
  {
    UNKNOWN = 0,
    OK = 1,
    ERROR = 2
  }

  public enum GENERIC_AMOUNT_INPUT_TYPE
  {
    VISITS = 0,
    AMOUNT = 1,
    METER = 2,
    CURRENCY = 3,
    //ESE 28-07-2016
    HOUR = 4
  }

  public enum SMARTFLOOR_TASK_SEVERITY
  {
    LOW = 1,
    MEDIUM = 2,
    HIGH = 3
  }

  public enum SMARTFLOOR_TASK_STATE
  {
    NOT_PENDING = 0,
    PENDING = 1,
    PROGRESS = 2,
    SOLVED = 3,
    SCALED = 4,
    VALIDATED = 5
  }

  public enum SMARTFLOOR_ALARM_TYPE
  {
    CLIENT = 41,
    TERMINAL = 42
  }

  public enum SMARTFLOOR_ALARM_SOURCE
  {
    APP = 1,
    SYSTEM = 2
  }

  // LJM 11-FEB-2014: Needed for both the WCP_Bonusing and the GUI
  public enum BonusTransferStatus
  {
    UnknownTransactionId = 0,      // The host doesn't have any information about this Bonus
    Pending = 1,                   // Received, pending to send to EGM
    Notified = 2,                  // Host -> EGM
    NotifiedInDoubt = 3,           // Host -> EGM (InDoubt!!!)
    Confirmed = 10,                // Confirmed by the EGM (Exception 7C + Delta Meters)
    CanceledTimeout = 21,          // > No player active
    NoCredits = 22,                // > No Credits
    NonZeroRestrictedCredits = 23, // > Has Non Restricted Credits
    TrackdataMismatch = 24,        // Trackdata Mismatch
    Error = 30,                    // Error
  }

  public class BonusAwardInfo
  {
    public const int BONUS_SOURCE_SITE_JACKPOT = 1;
    public Int32 SourceType;
    public Int64 SourceBigInt1;
    public Int64 SourceBigInt2;
    public Int32 SourceInt1;
    public Int32 SourceInt2;

    public const int BONUS_TARGET_TERMINAL = 1;
    public Int32 TargetType;
    public Int32 TargetTerminalId;
    public Int64 TargetAccountId;
  }

  public enum BonusSource
  {
    Unknown = 0,
    SiteJackpot = 1,
  }

  public enum ReopenMode
  {
    NoReopen = 0,
    ReopenInThisSession = 1,
    ReopenInOtherSession = 2,
    ReopenAlways = 3
  }
  public enum MbSessionStatus
  {
    CLOSED = 0,
    OPEN = 1
  }

  // DHA 07-APR-2014
  public enum SoftwareValidationStatus
  {
    Unknown = 0,
    Pending = 1,                   // Received, pending to send to EGM
    Notified = 2,                  // Host -> EGM
    ConfirmedNonValidated = 10,    // Confirmed by the EGM
    ConfirmedOK = 15,              // Confirmed by the EGM
    ConfirmedError = 20,           // Confirmed by the EGM
    CanceledTimeout = 25,          // Timed Out
    Error = 30,                    // Error
  }

  public enum SoftwareValidationStatusLKT
  {
    UNKNOWN = 0,
    PENDING = 5,
    TIMEOUT = 10,
    REQUESTED_TO_EGM = 20,
    REQUESTED_TO_EGM_NACK = 21,
    REQUESTED_TO_EGM_ACK = 22,
    ANSWERED_FROM_EGM = 99,
    ANSWERED = 100,
  }

  public enum ChipSaleType
  {
    CashDeskSale = 0,
    RegisterTableSale = 1
  }

  public enum LayoutRangesFields
  {
    TerminalStatus = 2,
    PlaySessionStatus = 3,
    PlayerGender = 7,
    PlayerAge = 8,
    PlayerLevel = 9,
    PlayerIsVIP = 10,
    PlayedAmount = 22,
    WonAmount = 23,
    PlaySessionBetAverage = 24,
    TerminalBetAverage = 25,
    PlaySessionDuration = 26,
    PlaySessionTotalPlayed = 27,
    PlaySessionPlayedCount = 28,
    TerminalProvidername = 31,
    PlaySessionWonAmount = 32,
    NetWin = 33,
    PlaySessionNetWin = 34,
    TerminalPlayedCount = 36
  }

  
  public enum WCP_CardTypes
  { 
      CARD_TYPE_UNKNOWN            = 0
    , CARD_TYPE_PLAYER             = 1
    , CARD_TYPE_MOBILE_BANK        = 2
    , CARD_TYPE_PLAYER_PIN         = 3

    , CARD_TYPE_TECH               = 10
    , CARD_TYPE_TECH_PIN           = 11
    , CARD_TYPE_CHANGE_STACKER     = 12
    , CARD_TYPE_CHANGE_STACKER_PIN = 13

    , CARD_TYPE_EMPLOYEE           = 14
  }

  public enum UserCardStatus
  {
    NoExist         = 0,
    AccountBlocked  = 1,
    NoPin           = 2,
    InvalidPin      = 3,    
    GeneralError    = 4,

    Ok              = 10
  }

  public enum PageSize
  {
    A4 = 0,
    A5 = 1,
    Letter = 2
  }

  [Flags]
  public enum HANDPAY_STATUS
  {
    // MASK_STATUS
    PENDING = 0x0000,
    PAID = 0x8000,
    EXPIRED = 0x4000,
    VOIDED = 0x2000,
    AUTOMATICALLY_PAID = 0x1000,
    // MASK_AUTHORIZATION
    AUTHORIZED = 0x0010,
    // MASK_TAX
    TAXED = 0x0002,
    NO_TAX = 0x0001,
    // 
    MASK_AUTHORIZATION = 0x00F0,
    MASK_STATUS = 0xF000,
    MASK_TAX = 0x000F,
  }

  public enum SMIB_COMMUNICATION_TYPE
  {
    NONE = 0,
    WCP = 1,
    SAS = 2,
    PULSES = 3,
    SAS_IGT_POKER = 4
  }

  [FlagsAttribute]
  public enum PROMOTION_AWARD_TARGET
  {
    NOT_SET = 0,
    PROMOBOX = 1,
    INTOUCH = 2
  }

  public class CageConceptInformation
  {
    public Int64 ConceptId;
    public String Name;
    public String Description;
    public Decimal UnitPrice;
    public CageMeters.CageSourceTargetCashierAction CashierAction;
    public Boolean OnlyNationalCurrency;
    public Decimal PriceFactor;
    public SequenceId SequenceId;
    public OperationCode OperationCode;
    public CashierVoucherType VoucherType;
  } // class

  //SDS 28-07-2016
  public enum ADS_TARGET
  {
    Other=0,
    Events=1,
    Dining=2,
    RecentWinners=3,
    CasinoOffers=4,
    Promotions=5,
  }

  public enum ENUM_FEATURES
  {
    NONE = 0,
    CASH_CAGE = 1,
    GAMING_TABLES = 2,
    TITO = 3,
    PATTERNS = 4,
    BONUSING = 5,
    GAMING_SESSION_ADJUSTMENT = 6,
    GAMING_TABLES_PLAYER_TRACKING = 7,
    WAS = 8,
    CASHLESS = 9,
    BILL_COUNTER = 10,
    FEATURES_GAMING_TABLES = 11,
    STACKERS = 12,
    STACKERS_ONLY_CASHLESS = 13,
    NON_CAGE_AUTO_MODE = 14,
    CASHLESS_NON_AML_EXTERNAL_CONTROL = 15,
    MEXICO = 16,
    WIN_LOSS = 17,
    MULTISITE = 18,
    MULTISITE_MULTICURRENCY = 19,
    ELP_MODE_1 = 20,
    GAMING_HALL = 21,
    GAME_GATEWAY = 22,
    CUSTOMERS_RECEPTION = 23,
    TITO_TITA = 24,
    ELP_MODE_0 = 25,
    CASH_CAGE_WINLOSS = 26,
    SMARTFLOOR = 27,
    COUNTR = 28,
    PINPAD = 29,
    MICO2 = 30,
    GAME_GATEWAY_PARIPLAY = 31,
    CREDIT_LINE = 32,
    JACKPOT = 33,
    JUNKETS = 34,
    GAMING_TABLES_TOURNAMENTS = 35,
    AGG = 36,
    
    THRESHOLD = 38,
    FBM = 39
  }

  public enum TYPE_LCD_FUNCTIONALITIES
  {
    ACCOUNT_INFO    = 1,
    RAFFLES         = 2,
    PROMOTIONS      = 3,
    JACKPOT         = 4,
    LOYALTY_CLUB    = 5,
    EXCHANGES       = 6,
    NOTIFY_PASSWORD = 7,
    GAMEGATEWAY     = 8,
    FB              = 9
  }


  public enum ENUM_HOW_TO_GET_THERE_ITEM_TYPE
  {
    TYPE_TELEPHONE = 1,
    TYPE_EMAIL = 2
  }

  public class CLS_HOW_TO_GET_THERE_ITEM_TYPE
  {
    Dictionary<int, int> _m_dict;
  
    public CLS_HOW_TO_GET_THERE_ITEM_TYPE()
    {
      _m_dict = new Dictionary<int, int>();
      _m_dict.Add((int)ENUM_HOW_TO_GET_THERE_ITEM_TYPE.TYPE_TELEPHONE, 7633);
      _m_dict.Add((int)ENUM_HOW_TO_GET_THERE_ITEM_TYPE.TYPE_EMAIL, 7634);
    }
    public Dictionary<int, int> Items()
    {
      return _m_dict;
    }
  }
  
  public enum ENUM_HANDPAY_AUTO_PAY_MODE
  {
    DISABLED = 0,
    ENABLED  = 1,
  }

  public enum CHIPS_SALE_MODE
  {
    NONE = -1,
    GP_CONFIGURATION = 0,
    FROM_ACCOUNT = 1,
    INTEGRATED_RECHARGE = 2
  }

  public enum BILLIN_LIMIT_LOCK_MODE
  {
    BILLIN_LIMIT_LOCK_MODE_NONE           = 0x00000000,  // None
    BILLIN_LIMIT_LOCK_MODE_NOTE_ACCEPTOR  = 0x00000001,  // Note Acceptor
    BILLIN_LIMIT_LOCK_MODE_EGM            = 0x00000002,  // EGM
    BILLIN_LIMIT_LOCK_MODE_HANDPAY        = 0x00000004,  // Handpay
  }
}