//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AccountTransferCredit.cs
// 
//   DESCRIPTION: Implementing an Account method to perform a total redeemable credit transfer between accounts.
// 
//        AUTHOR: Ram�n Moncl�s
// 
// CREATION DATE: 20-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-NOV-2013 RMS    First release.
// 23-DEC-2014 RCI    Fixed Bug WIG-1890: Coupon Cover promotions are not cancelled (and must be!)
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 07-DIC-2015 FJC    Fixed BUG: 7363
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public partial class Accounts
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Class to contain all data for the voucher
    //
    public partial class TransferCreditAmounts
    {
      public Decimal SourceInitialRedeemableBalance;
      public Decimal SourceTransferredAmount;
      public Decimal SourceFinalRedeemableBalance;
      public Decimal TargetInitialRedeemableBalance;
      public Decimal TargetTransferredAmount;
      public Decimal TargetFinalRedeemableBalance;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Enum to get a valid result of transfer credit operation.
    //
    public enum TRANSFER_CREDIT_RESULT
    {
      ERROR_UNKNOWN = 0,                          // Internal error
      TRANSFER_OK = 1,                            // All ok
      ERROR_NO_CREDIT = 2,                        // Source account hasn't redeemable credit
      ERROR_SOURCE_ACCOUNT_HAVE_PROMO = 3,
      ERROR_SOURCE_ACCOUNT_PLAYING = 4,           // Source account is playing
      ERROR_TARGET_ACCOUNT_PLAYING = 5,           // Target account is playing
      ERROR_TARGET_ACCOUNT_IS_SOURCE_ACCOUNT = 6,
      ERROR = 100
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check the availability of Transfer All Credit operation from a CardData.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountCardData
    //
    //      - OUTPUT:
    //          - 
    //
    // RETURNS:
    //      - TRANSFER_CREDIT_RESULT
    //
    public static TRANSFER_CREDIT_RESULT CanTransferCredit(CardData AccountCardData)
    {
      if (AccountCardData.IsLoggedIn)
      {
        return TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_PLAYING;
      }

      if (AccountCardData.AccountBalance.TotalRedeemable == 0)
      {
        return TRANSFER_CREDIT_RESULT.ERROR_NO_CREDIT;
      }

      if (AccountCardData.AccountBalance.TotalNotRedeemable > 0)
      {
        return TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_HAVE_PROMO;
      }

      return TRANSFER_CREDIT_RESULT.TRANSFER_OK;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Transfer ALL redeemable credit from source account to destiny account.
    //
    //  PARAMS:
    //      - INPUT:
    //          - SourceAccount
    //          - TargetAccount
    //          - InfoCashierSession
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - VouchersToPrint
    //
    // RETURNS:
    //      - TRANSFER_CREDIT_RESULT
    //
    public static TRANSFER_CREDIT_RESULT DB_TransferAllRedeemableCredit(CardData SourceAccount,
                                                                        CardData TargetAccount,
                                                                        CashierSessionInfo InfoCashierSession,
                                                                        out ArrayList VouchersToPrint,
                                                                        SqlTransaction Trx)
    {
      MultiPromos.AccountBalance _source_initial_balance;
      MultiPromos.AccountBalance _source_final_balance;
      Decimal _source_points;
      CardData _source_card_data;
      Int64 _target_play_session_id;
      MultiPromos.AccountBalance _target_initial_balance;
      MultiPromos.AccountBalance _target_final_balance;
      MultiPromos.AccountBalance _amount;
      StringBuilder _sb;
      Int64 _operation_id_out;
      Int64 _operation_id_in;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      VoucherCardCreditTransfer _temp_voucher;
      ArrayList _promo_lost_vouchers;
      Int32 _voucher_on_promo_lost;
      MultiPromos.PromoBalance _lost_balance;
      DataTable _lost_promos;
      TYPE_SPLITS _splits;
      TransferCreditAmounts _final_amounts;
      TRANSFER_CREDIT_RESULT _check_result;
      String _error_msg;

      VouchersToPrint = null;

      try
      {
        // Obtain Source Account Balance
        if (!MultiPromos.Trx_UpdateAccountBalance(SourceAccount.AccountId, MultiPromos.AccountBalance.Zero, Decimal.Zero,
                                                  out _source_initial_balance, out _source_points, Trx))
        {
          return TRANSFER_CREDIT_RESULT.ERROR;
        }

        // Read source account data
        _source_card_data = new CardData();
        if (!CardData.DB_CardGetAllData(SourceAccount.AccountId, _source_card_data, Trx))
        {
          return TRANSFER_CREDIT_RESULT.ERROR;
        }

        _check_result = CanTransferCredit(_source_card_data);
        if (_check_result != TRANSFER_CREDIT_RESULT.TRANSFER_OK
         && _check_result != TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_HAVE_PROMO)
        {
          return _check_result;
        }

        // Obtain Target Account Balance
        if (!MultiPromos.Trx_UpdateAccountBalance(TargetAccount.AccountId, MultiPromos.AccountBalance.Zero,
                                                  out _target_initial_balance, out _target_play_session_id,
                                                  Trx))
        {
          return TRANSFER_CREDIT_RESULT.ERROR;
        }

        if (_target_play_session_id != 0)
        {
          return TRANSFER_CREDIT_RESULT.ERROR_TARGET_ACCOUNT_PLAYING;
        }

        _amount = new MultiPromos.AccountBalance(_source_initial_balance.Redeemable,
                                                 _source_initial_balance.PromoRedeemable,
                                                 0,
                                                 _source_initial_balance.Reserved,
                                                 _source_initial_balance.ModeReserved);

        // Fill data for the voucher
        _final_amounts = new TransferCreditAmounts();
        _final_amounts.SourceInitialRedeemableBalance = _source_initial_balance.TotalRedeemable;
        _final_amounts.TargetInitialRedeemableBalance = _target_initial_balance.TotalRedeemable;
        _final_amounts.SourceTransferredAmount = _amount.TotalRedeemable;
        _final_amounts.TargetTransferredAmount = _amount.TotalRedeemable;

        // Update Source Account Balance
        if (!MultiPromos.Trx_UpdateAccountBalance(SourceAccount.AccountId, MultiPromos.AccountBalance.Negate(_amount, true),
                                                  out _source_final_balance, Trx))
        {
          return TRANSFER_CREDIT_RESULT.ERROR;
        }

        // Update Target Account Balance
        if (!MultiPromos.Trx_UpdateAccountBalance(TargetAccount.AccountId, _amount, out _target_final_balance, Trx))
        {
          return TRANSFER_CREDIT_RESULT.ERROR;
        }

        _final_amounts.SourceFinalRedeemableBalance = _source_final_balance.TotalRedeemable;
        _final_amounts.TargetFinalRedeemableBalance = _target_final_balance.TotalRedeemable;

        // Initial Cash in update command
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   ACCOUNTS ");
        _sb.AppendLine("   SET   AC_INITIAL_CASH_IN           = 0 ");
        _sb.AppendLine("       , AC_CANCELLABLE_OPERATION_ID  = NULL ");
        _sb.AppendLine("       , AC_LAST_ACTIVITY             = GETDATE() ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID                = @pAccountId ");

        // Update InitialCashIn for Source account
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = SourceAccount.AccountId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return TRANSFER_CREDIT_RESULT.ERROR;
          }
        }

        _sb.Length = 0;
        _sb.AppendLine("UPDATE   ACCOUNTS ");
        _sb.AppendLine("   SET   AC_INITIAL_CASH_IN           = AC_INITIAL_CASH_IN + @pInitialCashInIncrement ");
        //_sb.AppendLine("       , AC_CANCELLABLE_OPERATION_ID  = NULL ");
        _sb.AppendLine("       , AC_LAST_ACTIVITY             = GETDATE() ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID                = @pAccountId ");

        // Update InitialCashIn for Target account
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          Boolean _keep_cash_in_history;
          Decimal _increment_cash_in;

          _keep_cash_in_history = GeneralParam.GetBoolean("Cashier", "TransferCredit.KeepSourceCashInHistory");

          _increment_cash_in = _keep_cash_in_history ?
                               Math.Min(_source_card_data.MaxDevolution, _source_initial_balance.Redeemable) : 0;

          _cmd.Parameters.Add("@pInitialCashInIncrement", SqlDbType.Money).Value = _increment_cash_in;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = TargetAccount.AccountId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return TRANSFER_CREDIT_RESULT.ERROR;
          }
        }

        // Create Transfer OUT operation
        if (!Operations.DB_InsertOperation(OperationCode.TRANSFER_CREDIT_OUT, SourceAccount.AccountId,
                                           InfoCashierSession.CashierSessionId, 0,
                                           0, // promo id
                                           _final_amounts.SourceTransferredAmount, // currency amount
                                           0, // currency NR
                                           0, // Operation Data
                                           0,
                                           string.Empty,
                                           out _operation_id_out,
                                           out _error_msg,
                                           Trx))
        {
          Log.Error("DB_TransferCredit. Error calling DB_InsertOperation. Source CardId: " + SourceAccount.AccountId.ToString());

          return TRANSFER_CREDIT_RESULT.ERROR;
        }

        // Create Transfer IN operation
        if (!Operations.DB_InsertOperation(OperationCode.TRANSFER_CREDIT_IN, TargetAccount.AccountId,
                                           InfoCashierSession.CashierSessionId, 0,
                                           0, // promo id
                                           _final_amounts.TargetTransferredAmount, // currency amount
                                           0, // currency NR
                                           0, // Operation Data
                                           0,
                                           string.Empty,
                                           out _operation_id_in,
                                           out _error_msg,
                                           Trx))
        {
          Log.Error("DB_TransferCredit. Error calling DB_InsertOperation. Target CardId: " + TargetAccount.AccountId.ToString());

          return TRANSFER_CREDIT_RESULT.ERROR;
        }

        // Operation movements
        _cashier_movements = new CashierMovementsTable(InfoCashierSession);
        _account_movements = new AccountMovementsTable(InfoCashierSession);

        // Source account cashier and account movements
        _cashier_movements.Add(_operation_id_out, CASHIER_MOVEMENT.TRANSFER_CREDIT_OUT,
                               _final_amounts.SourceTransferredAmount, SourceAccount.AccountId, SourceAccount.TrackData,
                               SourceAccount.AccountId.ToString());

        _account_movements.Add(_operation_id_out, SourceAccount.AccountId, MovementType.TransferCreditOut,
                               _source_initial_balance.TotalBalance, _amount.TotalBalance, 0, _source_final_balance.TotalBalance,
                               TargetAccount.AccountId.ToString());

        _lost_promos = null;

        // Cancell all NR promotions from source account
        if (_source_card_data.AccountBalance.TotalNotRedeemable > 0)
        {
          if (!CardData.GetPromotionsToBeCancelledDueToRedeem(SourceAccount.AccountId, 0, null,
                                                              true, false, out _lost_balance, out _lost_promos, Trx))
          {
            return TRANSFER_CREDIT_RESULT.ERROR;
          }

          if (_lost_balance.Balance.TotalNotRedeemable > 0)
          {
            MultiPromos.PromoBalance _calculated_lost_balance;
            Int32 _num_applied;

            _num_applied = AccountPromotion.Trx_PromotionApplySystemAction(AccountPromotion.Trx_CancelByPlayerPromotion,
                                                                           _lost_promos, false, out _calculated_lost_balance, Trx);
            if (_num_applied != _lost_promos.Rows.Count
                || _calculated_lost_balance.Balance.TotalNotRedeemable > _lost_balance.Balance.TotalNotRedeemable)
            {
              return TRANSFER_CREDIT_RESULT.ERROR;
            }

            _cashier_movements.Add(_operation_id_out, CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE,
                                   _lost_balance.Balance.TotalNotRedeemable, SourceAccount.AccountId, SourceAccount.TrackData);

            _account_movements.Add(_operation_id_out, SourceAccount.AccountId, MovementType.CancelNotRedeemable,
                                   _source_final_balance.TotalBalance, _lost_balance.Balance.TotalNotRedeemable, 0,
                                   _source_final_balance.TotalBalance - _lost_balance.Balance.TotalNotRedeemable);
          }
        }

        // Target account cashier and account movements
        _cashier_movements.Add(_operation_id_in, CASHIER_MOVEMENT.TRANSFER_CREDIT_IN,
                               _final_amounts.TargetTransferredAmount, TargetAccount.AccountId, TargetAccount.TrackData,
                               TargetAccount.AccountId.ToString());

        _account_movements.Add(_operation_id_in, TargetAccount.AccountId, MovementType.TransferCreditIn,
                               _target_initial_balance.TotalBalance, 0, _amount.TotalBalance, _target_final_balance.TotalBalance,
                               SourceAccount.AccountId.ToString());

        // Save movements
        if (!_account_movements.Save(Trx))
        {
          return TRANSFER_CREDIT_RESULT.ERROR;
        }

        if (!_cashier_movements.Save(Trx))
        {
          return TRANSFER_CREDIT_RESULT.ERROR;
        }

        // Create operation vouchers
        VouchersToPrint = new ArrayList();

        _temp_voucher = new VoucherCardCreditTransfer(SourceAccount.AccountId, TargetAccount.AccountId,
                                                      _final_amounts, PrintMode.Print, Trx);
        _temp_voucher.Save(_operation_id_out, Trx);

        VouchersToPrint.Add(_temp_voucher);

        if (_lost_promos != null && _lost_promos.Rows.Count > 0)
        {
          _voucher_on_promo_lost = GeneralParam.GetInt32("Cashier.Voucher", "VoucherOnPromotionCancelled");

          if (_voucher_on_promo_lost > 0)
          {
            if (!Split.ReadSplitParameters(out _splits))
            {
              return TRANSFER_CREDIT_RESULT.ERROR;
            }

            if (!VoucherBuilder.PromoLost(SourceAccount.VoucherAccountInfo(), SourceAccount.AccountId,
                                          _splits, (_voucher_on_promo_lost == 1), _lost_promos,
                                          false, _operation_id_out, PrintMode.Print, Trx, out _promo_lost_vouchers))
            {
              return TRANSFER_CREDIT_RESULT.ERROR;
            }

            if (_promo_lost_vouchers != null)
            {
              foreach (Voucher _voucher in _promo_lost_vouchers)
              {
                VouchersToPrint.Add(_voucher);
              }
            }
          }
        }

        return TRANSFER_CREDIT_RESULT.TRANSFER_OK;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return TRANSFER_CREDIT_RESULT.ERROR_UNKNOWN;
    }

    public static String TranslateTransferCreditResultToMessage(TRANSFER_CREDIT_RESULT result)
    {
      String _translation;

      switch (result)
      {
        case WSI.Common.Accounts.TRANSFER_CREDIT_RESULT.TRANSFER_OK:
          _translation = "";
          break;
        case Accounts.TRANSFER_CREDIT_RESULT.ERROR_NO_CREDIT:
          _translation = Resource.String("STR_INPUT_TARGET_CARD_ERROR_NO_CREDIT");
          break;
        case Accounts.TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_HAVE_PROMO:
          _translation = Resource.String("STR_TRANSFER_CREDIT_SOURCE_ACCOUNT_HAVE_PROMO");
          break;
        case Accounts.TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_PLAYING:
          _translation = Resource.String("STR_INPUT_TARGET_CARD_ERROR_SOURCE_ACCOUNT_PLAYING");
          break;
        case Accounts.TRANSFER_CREDIT_RESULT.ERROR_TARGET_ACCOUNT_PLAYING:
          _translation = Resource.String("STR_INPUT_TARGET_CARD_ERROR_TARGET_ACCOUNT_PLAYING");
          break;
        case TRANSFER_CREDIT_RESULT.ERROR_TARGET_ACCOUNT_IS_SOURCE_ACCOUNT:
          _translation = Resource.String("STR_INPUT_TARGET_CARD_ERROR_TARGET_ACCOUNT_IS_SOURCE_ACCOUNT");
          break;
        default:
          _translation = Resource.String("STR_INPUT_TARGET_CARD_TRANSFER_ERROR");
          break;
      }

      return _translation.Replace("\\r\\n", "\r\n");
    }
  }
}