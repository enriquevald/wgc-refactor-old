//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SmibProtocols.cs
// 
//   DESCRIPTION: Class to manage UNR Promotions import 
// 
// 
// CREATION DATE: 28-APR-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2015 XCD    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace WSI.Common
{
  public class SmibProtocols
  {
#region "Members"
    public SMIB_COMMUNICATION_TYPE Protocol;
    public PCDProtocolConfiguration PcdParams;
#endregion
    #region "Constructor"
    public SmibProtocols()
    {
      PcdParams = new PCDProtocolConfiguration();
    }
    #endregion

    #region "Public"
    public SMIB_COMMUNICATION_TYPE GetProtocol(String ProtocolString)
    {
      if (String.Equals(ProtocolString, SMIB_COMMUNICATION_TYPE.PULSES.ToString()))
      {
        return SMIB_COMMUNICATION_TYPE.PULSES;
      }
      if (String.Equals(ProtocolString, SMIB_COMMUNICATION_TYPE.WCP.ToString()))
      {
        return SMIB_COMMUNICATION_TYPE.WCP;
      }
      if (String.Equals(ProtocolString, SMIB_COMMUNICATION_TYPE.SAS.ToString()))
      {
        return SMIB_COMMUNICATION_TYPE.SAS;
      }
      
      return SMIB_COMMUNICATION_TYPE.NONE;     
    }

#endregion

    #region "Protocol Params definitions"
    public class PCDProtocolConfiguration
    {
      #region "Constants"
      public const Int32 PCD_NUM_INPUT_PORTS = 16;
      public const Int32 PCD_NUM_OUTPUT_PORTS = 2;
      public const Int64 PCD_NULL_VALUE = -1;
      public const Int32 PCD_DOOR_MASK = 0x10000;
      public const Int32 PCD_PORT_METER_MASK = 0x2000;

      // 1st byte Input/Output
      public const Int32 PCD_IO_TYPE_MASK = 0xF;
      public enum IoType
      {
        INPUT = 0x01,
        OUTPUT = 0x02,
      }

      // 2nd byte as signal type
      public const Int32 PCD_PULSES_TYPE_MASK = 0xF0;
      public enum EdgeType
      {
        UNKNOWN = 0,
        PULSE_UP = 0x10,
        PULSE_DOWN = 0x20,
        SIGNAL_UP_DOWN = 0x40,
        SIGNAL_DOWN_UP = 0x80,
      }

      public enum OutputCodes
      {
        SHUTDOWN = 0,
        CASH_IN = 1,
      }

      public enum DoorCodes
      {
        SLOT_DOOR = 65553, // Same as SAS Code for door opened
        DROP_DOOR = 65555,
        CARD_CAGE = 65557,
        CASHBOX_DOOR = 65561,
        BELLY_DOOR = 65565,
      }

      public enum PortType
      {
        NOT_DEFINED = 0,
        METER = 1,
        DOOR = 2,
        CASH_IN = 3,
        SHUTDOWN = 4,
      }

      // Meter codes defined in cls_terminal_sas_meters

      #endregion

      #region "Structs"

      [StructLayout(LayoutKind.Sequential)]
      public struct Polling
      {
        public UInt32 MetersIdle;
        public UInt32 MetersInSession;
        public UInt32 MetersOnCashout;
        public UInt32 CashoutFinished;
      }

      [StructLayout(LayoutKind.Sequential)]
      public class PCDPort
      {
        public Int32 PcdIoNumber;
        public PortType PcdIoType;
        public Int64 CodeNumber;
        public Decimal CodeNumberMultiplier;
        public EdgeType PulseEdgeType;
        public UInt32 TimePulseDownMs;
        public UInt32 TimePulseUpMs;

        public PCDPort()
        {
          PcdIoNumber = (Int32) PCD_NULL_VALUE;
          CodeNumber = PCD_NULL_VALUE;
          PcdIoType = PortType.NOT_DEFINED;
          CodeNumberMultiplier = 0;
          PulseEdgeType = EdgeType.UNKNOWN;
          TimePulseDownMs = 0;
          TimePulseUpMs = 0;
        }
      }

#endregion

      #region "Members"
      private Polling m_polling_params;
      //private Int32 m_configuration_id;
      //private String m_configuration_name;
      //private String m_configuration_description;
      private PCDPort[] m_input_ports;
      private PCDPort[] m_output_ports;

      #endregion

      #region "Constructors"
      public PCDProtocolConfiguration()
      {
        // Polling params now are fixed
        m_polling_params.MetersIdle = 30000;
        m_polling_params.MetersInSession = 5000;
        m_polling_params.MetersOnCashout = 1000;
        m_polling_params.CashoutFinished = 5;

        m_input_ports = new PCDPort[PCD_NUM_INPUT_PORTS];
        m_output_ports = new PCDPort[PCD_NUM_OUTPUT_PORTS];

        for (Int16 _idx_port = 0; _idx_port <  PCD_NUM_INPUT_PORTS; _idx_port++)
        {
          m_input_ports[_idx_port] = new PCDPort();
        }

        for (Int16 _idx_port = 0; _idx_port < PCD_NUM_OUTPUT_PORTS; _idx_port++)
        {
          m_output_ports[_idx_port] = new PCDPort();
        }

      }
      #endregion

      #region "Properties"

      public Polling PollingParams
      {
        get { return m_polling_params; }
        set { m_polling_params = value; }
      }

      //public Int32 ConfigurationId
      //{
      //  get { return m_configuration_id; }
      //  set { m_configuration_id = value; }
      //}

      //public String ConfigurationName
      //{
      //  get { return m_configuration_name; }
      //  set { m_configuration_name = value; }
      //}

      //public String ConfigurationDescription
      //{
      //  get { return m_configuration_description; }
      //  set { m_configuration_description = value; }
      //}

      public PCDPort InputPort(Int32 PortNumber)
      {
        return m_input_ports[PortNumber];
      }

      public PCDPort OutputPort(Int32 PortNumber)
      {
        return m_output_ports[PortNumber];
      }

      public PortType GetPortType(String PortTypeString)
      {
        if (String.Equals(PortTypeString, PortType.CASH_IN.ToString()))
        {
          return PortType.CASH_IN;
        }
        if (String.Equals(PortTypeString, PortType.DOOR.ToString()))
        {
          return PortType.DOOR;
        }
        if (String.Equals(PortTypeString, PortType.METER.ToString()))
        {
          return PortType.METER;
        }
        if (String.Equals(PortTypeString, PortType.SHUTDOWN.ToString()))
        {
          return PortType.SHUTDOWN;
        }

        return PortType.NOT_DEFINED;
      }

      public EdgeType GetEdgeType(String EdgeTypeString)
      {
        if (String.Equals(EdgeTypeString, EdgeType.PULSE_UP.ToString()))
        {
          return EdgeType.PULSE_UP;
        }
        if (String.Equals(EdgeTypeString, EdgeType.PULSE_DOWN.ToString()))
        {
          return EdgeType.PULSE_DOWN;
        }
        if (String.Equals(EdgeTypeString, EdgeType.SIGNAL_UP_DOWN.ToString()))
        {
          return EdgeType.SIGNAL_UP_DOWN;
        }
        if (String.Equals(EdgeTypeString, EdgeType.SIGNAL_DOWN_UP.ToString()))
        {
          return EdgeType.SIGNAL_DOWN_UP;
        }

        return EdgeType.UNKNOWN;
      }

      #endregion
    }
#endregion
  }
}
