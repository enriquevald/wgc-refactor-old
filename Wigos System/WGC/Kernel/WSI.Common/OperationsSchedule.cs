//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: OperationsSchedule.cs
// 
//   DESCRIPTION: Class to manage Operations Schedule
// 
//        AUTHOR: Quim Morales
// 
// CREATION DATE: 20-FEB-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-FEB-2013 QMP    First release.
// 29-JUN-2015 DRV    Added terminal lock function
// 16-JUL-2015 DRV    Fixed Bug WIG-2585: Problems with workday start hour
// 21-AUG-2017 ATB    PBI 29405: Game Machines showing "Disabled by host" message (WIGOS-4248)
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;

namespace WSI.Common
{
  public class OperationsSchedule
  {
    public class TYPE_OPERATIONS_SCHEDULE_ITEM
    {

      public enum VALIDATION_RESULT
      {
        VALIDATION_OK = 0,
        OUTSIDE_WORKDAY = 1,
        OVERLAPPING_RANGES = 2,
        VALIDATION_ERROR = 3,
      }

      public Int64 id;
      public OPERATIONS_SCHEDULE_TYPE type;
      public Boolean operations_allowed;
      public DayOfWeek day_of_week;
      public DateTime date_from;
      public DateTime date_to;
      public Int32 time1_from_minutes;
      public Int32 time1_to_minutes;
      public Int32 time2_from_minutes;
      public Int32 time2_to_minutes;
      public Boolean next_day_operations_allowed;
      public Int32 next_day_time1_from_minutes;
      public Int32 next_day_time1_to_minutes;
      public Boolean previous_day_operations_allowed;
      public Int32 previous_day_time1_from_minutes;
      public Int32 previous_day_time1_to_minutes;

      public override string ToString()
      {
        string _result;
        DateTime _now;

        _result = "";
        _now = WGDB.Now;

        //Day
        switch (this.type)
        {
          case OPERATIONS_SCHEDULE_TYPE.DAY_OF_WEEK:
            switch (this.day_of_week)
            {
              case DayOfWeek.Monday:
                _result += Resource.String("STR_OPERATIONS_SCHEDULE_MONDAY");
                break;
              case DayOfWeek.Tuesday:
                _result += Resource.String("STR_OPERATIONS_SCHEDULE_TUESDAY");
                break;
              case DayOfWeek.Wednesday:
                _result += Resource.String("STR_OPERATIONS_SCHEDULE_WEDNESDAY");
                break;
              case DayOfWeek.Thursday:
                _result += Resource.String("STR_OPERATIONS_SCHEDULE_THURSDAY");
                break;
              case DayOfWeek.Friday:
                _result += Resource.String("STR_OPERATIONS_SCHEDULE_FRIDAY");
                break;
              case DayOfWeek.Saturday:
                _result += Resource.String("STR_OPERATIONS_SCHEDULE_SATURDAY");
                break;
              case DayOfWeek.Sunday:
                _result += Resource.String("STR_OPERATIONS_SCHEDULE_SUNDAY");
                break;
            }
            break;

          case OPERATIONS_SCHEDULE_TYPE.DATE_RANGE:
            _result += this.date_from.ToShortDateString();

            //Date range
            if (this.date_from != this.date_to)
            {
              _result += " " + Resource.String("STR_OPERATIONS_SCHEDULE_TO") + " " + this.date_to.ToShortDateString();
            }
            break;
        }

        //Separator
        _result += " - ";

        //Operations not allowed
        if (!this.operations_allowed)
        {
          _result += Resource.String("STR_OPERATIONS_SCHEDULE_NOT_ALLOWED");
        }
        else
        {
          _result += Resource.String("STR_OPERATIONS_SCHEDULE_ALLOWED");

          //First time range
          _result += " " + _now.Date.AddMinutes(this.time1_from_minutes).ToString("HH:mm");
          _result += " " + Resource.String("STR_OPERATIONS_SCHEDULE_TO");
          _result += " " + _now.Date.AddMinutes(this.time1_to_minutes).ToString("HH:mm");

          //Second time range
          if (this.time2_from_minutes != -1 && this.time2_to_minutes != -1)
          {
            _result += " " + Resource.String("STR_OPERATIONS_SCHEDULE_AND_FROM");
            _result += " " + _now.Date.AddMinutes(this.time2_from_minutes).ToString("HH:mm");
            _result += " " + Resource.String("STR_OPERATIONS_SCHEDULE_TO");
            _result += " " + _now.Date.AddMinutes(this.time2_to_minutes).ToString("HH:mm");
          }
        }

        return _result;
      } // ToString

      public TYPE_OPERATIONS_SCHEDULE_ITEM Clone()
      {
        return (TYPE_OPERATIONS_SCHEDULE_ITEM)this.MemberwiseClone();
      } // Clone

      //------------------------------------------------------------------------------
      // PURPOSE: Check if a schedule is valid
      // 
      //  PARAMS:
      //      - INPUT:
      //        - DateTime (Closing time, only hour and minute will be used)
      //
      //      - OUTPUT:
      //        - VALIDATION_RESULT (Result of the validation, with error description)
      //
      // RETURNS:
      //    - Boolean (TRUE if operation was successful, FALSE if any errors occurred)
      // 
      //   NOTES:
      public Boolean IsValid(DateTime ClosingTime, out VALIDATION_RESULT Reason)
      {
        DateTime _closing_from;
        DateTime _closing_to;
        DateTime _time1_from;
        DateTime _time1_to;
        DateTime _time2_from;
        DateTime _time2_to;

        _closing_from = new DateTime(2000, 1, 1, ClosingTime.Hour, ClosingTime.Minute, 0);
        _closing_to = _closing_from.AddDays(1);

        //First time range
        _time1_from = new DateTime(2000, 1, 1, 0, 0, 0).AddMinutes(this.time1_from_minutes);
        _time1_to = new DateTime(2000, 1, 1, 0, 0, 0).AddMinutes(this.time1_to_minutes);

        if (_time1_from < _closing_from)
        {
          _time1_from = _time1_from.AddDays(1);
        }

        if (_time1_to <= _closing_from)
        {
          _time1_to = _time1_to.AddDays(1);
        }

        if (_time1_to < _time1_from || _time1_to > _closing_to)
        {
          Reason = VALIDATION_RESULT.OUTSIDE_WORKDAY; //Outside of workday

          return false;
        }

        if (_time1_from == _time1_to && _time1_from != _closing_from)
        {
          Reason = VALIDATION_RESULT.OUTSIDE_WORKDAY; //Outside of workday

          return false;
        }

        //Second time range
        if (this.time2_from_minutes != -1 && this.time2_to_minutes != -1)
        {
          _time2_from = new DateTime(2000, 1, 1, 0, 0, 0).AddMinutes(this.time2_from_minutes);
          _time2_to = new DateTime(2000, 1, 1, 0, 0, 0).AddMinutes(this.time2_to_minutes);

          if (_time2_from < _closing_from)
          {
            _time2_from = _time2_from.AddDays(1);
          }

          if (_time2_to <= _closing_from)
          {
            _time2_to = _time2_to.AddDays(1);
          }

          if (_time2_to <= _time2_from || _time2_to > _closing_to)
          {
            Reason = VALIDATION_RESULT.OUTSIDE_WORKDAY;

            return false;
          }

          if (_time2_from <= _time1_to)
          {
            Reason = VALIDATION_RESULT.OVERLAPPING_RANGES;

            return false;
          }
        }

        Reason = VALIDATION_RESULT.VALIDATION_OK;

        return true;

      } // IsValid
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the operations schedule that applies to a particular day
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateTime
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //        - TYPE_OPERATIONS_SCHEDULE_ITEM
    //
    // RETURNS:
    //    - Boolean (TRUE if operation was successful, FALSE if any errors occurred)
    // 
    //   NOTES:
    public static Boolean GetOperationsSchedule(DateTime DateTime,
                                                out TYPE_OPERATIONS_SCHEDULE_ITEM Schedule,
                                                SqlTransaction Trx)
    {
      return GetOperationsSchedule(DateTime, false, out Schedule, Trx);
    }

    public static Boolean GetOperationsSchedule(DateTime DateTime,
                                              Boolean TerminalsSchedule,
                                              out TYPE_OPERATIONS_SCHEDULE_ITEM Schedule,
                                              SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _closing_hour;
      DateTime _working_day;
      DateTime _next_working_day;
      DateTime _previous_working_day;

      Schedule = new TYPE_OPERATIONS_SCHEDULE_ITEM();
      Schedule.type = OPERATIONS_SCHEDULE_TYPE.DATE_RANGE;

      //Get closing hour
      _closing_hour = GeneralParam.GetInt32("WigosGUI", "ClosingTime", -1);

      if (_closing_hour == -1)
      {
        return false;
      }


      //Determine the right working day, depending on closing time
      _working_day = Misc.Opening(DateTime, _closing_hour, 0);
      Schedule.date_from = _working_day.Date;
      Schedule.date_to = _working_day.Date;
      Schedule.day_of_week = _working_day.DayOfWeek;

      try
      {
        //Search in order for specific date, date range and day of week.
        //We will only process the first result
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   TOP 1 *                                   ");
        _sb.AppendLine("     FROM ( SELECT   1 RANK                           ");
        _sb.AppendLine("                   , OS_OPERATIONS_ALLOWED            ");
        _sb.AppendLine("                   , OS_TIME1_FROM                    ");
        _sb.AppendLine("                   , OS_TIME1_TO                      ");
        _sb.AppendLine("                   , OS_TIME2_FROM                    ");
        _sb.AppendLine("                   , OS_TIME2_TO                      ");
        _sb.AppendLine("              FROM   OPERATIONS_SCHEDULE              ");
        _sb.AppendLine("             WHERE   OS_TYPE        = @pTypeDateRange ");
        _sb.AppendLine("               AND   OS_DATE_TO     = @pDate          ");
        _sb.AppendLine("               AND   OS_DATE_FROM   = @pDate          ");
        _sb.AppendLine("                     UNION ALL                        ");
        _sb.AppendLine("            SELECT   2 RANK                           ");
        _sb.AppendLine("                   , OS_OPERATIONS_ALLOWED            ");
        _sb.AppendLine("                   , OS_TIME1_FROM                    ");
        _sb.AppendLine("                   , OS_TIME1_TO                      ");
        _sb.AppendLine("                   , OS_TIME2_FROM                    ");
        _sb.AppendLine("                   , OS_TIME2_TO                      ");
        _sb.AppendLine("              FROM   OPERATIONS_SCHEDULE              ");
        _sb.AppendLine("             WHERE   OS_TYPE        = @pTypeDateRange ");
        _sb.AppendLine("               AND   OS_DATE_TO    >= @pDate          ");
        _sb.AppendLine("               AND   OS_DATE_FROM  <= @pDate          ");
        _sb.AppendLine("                     UNION ALL                        ");
        _sb.AppendLine("            SELECT   3 RANK                           ");
        _sb.AppendLine("                   , OS_OPERATIONS_ALLOWED            ");
        _sb.AppendLine("                   , OS_TIME1_FROM                    ");
        _sb.AppendLine("                   , OS_TIME1_TO                      ");
        _sb.AppendLine("                   , OS_TIME2_FROM                    ");
        _sb.AppendLine("                   , OS_TIME2_TO                      ");
        _sb.AppendLine("              FROM   OPERATIONS_SCHEDULE              ");
        _sb.AppendLine("             WHERE   OS_TYPE        = @pTypeDayOfWeek ");
        _sb.AppendLine("               AND   OS_DAY_OF_WEEK = @pDayOfWeek     ");
        _sb.AppendLine("          ) RESULTS                                   ");
        _sb.AppendLine(" ORDER BY   RANK                                      ");

        if (TerminalsSchedule)
        {
          _sb.AppendLine("   SELECT   TOP 1 *                                   ");
          _sb.AppendLine("     FROM ( SELECT   1 RANK                           ");
          _sb.AppendLine("                   , OS_OPERATIONS_ALLOWED            ");
          _sb.AppendLine("                   , OS_TIME1_FROM                    ");
          _sb.AppendLine("                   , OS_TIME1_TO                      ");
          _sb.AppendLine("                   , OS_TIME2_FROM                    ");
          _sb.AppendLine("                   , OS_TIME2_TO                      ");
          _sb.AppendLine("              FROM   OPERATIONS_SCHEDULE              ");
          _sb.AppendLine("             WHERE   OS_TYPE        = @pTypeDateRange ");
          _sb.AppendLine("               AND   OS_DATE_TO     = @pPreviousDate  ");
          _sb.AppendLine("               AND   OS_DATE_FROM   = @pPreviousDate  ");
          _sb.AppendLine("                     UNION ALL                        ");
          _sb.AppendLine("            SELECT   2 RANK                           ");
          _sb.AppendLine("                   , OS_OPERATIONS_ALLOWED            ");
          _sb.AppendLine("                   , OS_TIME1_FROM                    ");
          _sb.AppendLine("                   , OS_TIME1_TO                      ");
          _sb.AppendLine("                   , OS_TIME2_FROM                    ");
          _sb.AppendLine("                   , OS_TIME2_TO                      ");
          _sb.AppendLine("              FROM   OPERATIONS_SCHEDULE              ");
          _sb.AppendLine("             WHERE   OS_TYPE        = @pTypeDateRange ");
          _sb.AppendLine("               AND   OS_DATE_TO    >= @pPreviousDate  ");
          _sb.AppendLine("               AND   OS_DATE_FROM  <= @pPreviousDate  ");
          _sb.AppendLine("                     UNION ALL                        ");
          _sb.AppendLine("            SELECT   3 RANK                           ");
          _sb.AppendLine("                   , OS_OPERATIONS_ALLOWED            ");
          _sb.AppendLine("                   , OS_TIME1_FROM                    ");
          _sb.AppendLine("                   , OS_TIME1_TO                      ");
          _sb.AppendLine("                   , OS_TIME2_FROM                    ");
          _sb.AppendLine("                   , OS_TIME2_TO                      ");
          _sb.AppendLine("              FROM   OPERATIONS_SCHEDULE              ");
          _sb.AppendLine("             WHERE   OS_TYPE        = @pTypeDayOfWeek ");
          _sb.AppendLine("               AND   OS_DAY_OF_WEEK = @pPreviousDayOfWeek ");
          _sb.AppendLine("          ) RESULTS                                   ");
          _sb.AppendLine(" ORDER BY   RANK                                      ");
          _sb.AppendLine("   SELECT   TOP 1 *                                   ");
          _sb.AppendLine("     FROM ( SELECT   1 RANK                           ");
          _sb.AppendLine("                   , OS_OPERATIONS_ALLOWED            ");
          _sb.AppendLine("                   , OS_TIME1_FROM                    ");
          _sb.AppendLine("                   , OS_TIME1_TO                      ");
          _sb.AppendLine("                   , OS_TIME2_FROM                    ");
          _sb.AppendLine("                   , OS_TIME2_TO                      ");
          _sb.AppendLine("              FROM   OPERATIONS_SCHEDULE              ");
          _sb.AppendLine("             WHERE   OS_TYPE        = @pTypeDateRange ");
          _sb.AppendLine("               AND   OS_DATE_TO     = @pNextDate      ");
          _sb.AppendLine("               AND   OS_DATE_FROM   = @pNextDate      ");
          _sb.AppendLine("                     UNION ALL                        ");
          _sb.AppendLine("            SELECT   2 RANK                           ");
          _sb.AppendLine("                   , OS_OPERATIONS_ALLOWED            ");
          _sb.AppendLine("                   , OS_TIME1_FROM                    ");
          _sb.AppendLine("                   , OS_TIME1_TO                      ");
          _sb.AppendLine("                   , OS_TIME2_FROM                    ");
          _sb.AppendLine("                   , OS_TIME2_TO                      ");
          _sb.AppendLine("              FROM   OPERATIONS_SCHEDULE              ");
          _sb.AppendLine("             WHERE   OS_TYPE        = @pTypeDateRange ");
          _sb.AppendLine("               AND   OS_DATE_TO    >= @pNextDate      ");
          _sb.AppendLine("               AND   OS_DATE_FROM  <= @pNextDate      ");
          _sb.AppendLine("                     UNION ALL                        ");
          _sb.AppendLine("            SELECT   3 RANK                           ");
          _sb.AppendLine("                   , OS_OPERATIONS_ALLOWED            ");
          _sb.AppendLine("                   , OS_TIME1_FROM                    ");
          _sb.AppendLine("                   , OS_TIME1_TO                      ");
          _sb.AppendLine("                   , OS_TIME2_FROM                    ");
          _sb.AppendLine("                   , OS_TIME2_TO                      ");
          _sb.AppendLine("              FROM   OPERATIONS_SCHEDULE              ");
          _sb.AppendLine("             WHERE   OS_TYPE        = @pTypeDayOfWeek ");
          _sb.AppendLine("               AND   OS_DAY_OF_WEEK = @pNextDayOfWeek ");
          _sb.AppendLine("          ) RESULTS                                   ");
          _sb.AppendLine(" ORDER BY   RANK                                      ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTypeDateRange", System.Data.SqlDbType.Int).Value = OPERATIONS_SCHEDULE_TYPE.DATE_RANGE;
          _cmd.Parameters.Add("@pTypeDayOfWeek", System.Data.SqlDbType.Int).Value = OPERATIONS_SCHEDULE_TYPE.DAY_OF_WEEK;
          _cmd.Parameters.Add("@pDate", System.Data.SqlDbType.DateTime).Value = _working_day.Date;
          _cmd.Parameters.Add("@pDayOfWeek", System.Data.SqlDbType.Int).Value = _working_day.DayOfWeek;
          if (TerminalsSchedule)
          {

            _next_working_day = _working_day.AddDays(1);
            _previous_working_day = _working_day.AddDays(-1);
            _cmd.Parameters.Add("@pNextDate", System.Data.SqlDbType.DateTime).Value = _next_working_day.Date;
            _cmd.Parameters.Add("@pNextDayOfWeek", System.Data.SqlDbType.Int).Value = _next_working_day.DayOfWeek;
            _cmd.Parameters.Add("@pPreviousDate", System.Data.SqlDbType.DateTime).Value = _previous_working_day.Date;
            _cmd.Parameters.Add("@pPreviousDayOfWeek", System.Data.SqlDbType.Int).Value = _previous_working_day.DayOfWeek;
          }

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              Schedule.operations_allowed = _reader.GetBoolean(1);
              Schedule.time1_from_minutes = _reader.GetInt32(2);
              Schedule.time1_to_minutes = _reader.GetInt32(3);
              Schedule.time2_from_minutes = _reader.IsDBNull(4) ? -1 : _reader.GetInt32(4);
              Schedule.time2_to_minutes = _reader.IsDBNull(5) ? -1 : _reader.GetInt32(5);

              if (TerminalsSchedule)
              {
                if (_reader.NextResult())
                {
                  if (_reader.Read())
                  {
                    Schedule.previous_day_operations_allowed = _reader.GetBoolean(1);
                    Schedule.previous_day_time1_from_minutes = _reader.GetInt32(2);
                    Schedule.previous_day_time1_to_minutes = _reader.GetInt32(3);
                    Schedule.previous_day_time1_from_minutes = _reader.IsDBNull(4) ? Schedule.previous_day_time1_from_minutes : _reader.GetInt32(4);
                    Schedule.previous_day_time1_to_minutes = _reader.IsDBNull(5) ? Schedule.previous_day_time1_to_minutes : _reader.GetInt32(5);
                  }
                  else
                  {
                    return false;
                  }
                }
                else
                {
                  return false;
                }
                if (_reader.NextResult())
                {
                  if (_reader.Read())
                  {
                    Schedule.next_day_operations_allowed = _reader.GetBoolean(1);
                    Schedule.next_day_time1_from_minutes = _reader.GetInt32(2);
                    Schedule.next_day_time1_to_minutes = _reader.GetInt32(3);
                  }
                  else
                  {
                    return false;
                  }
                }
                else
                {
                  return false;
                }
              }
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetOperationsSchedule

    //------------------------------------------------------------------------------
    // PURPOSE: Check if operations are allowed for a given date and time
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateTime
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //        - Boolean (TRUE if operations are allowed, FALSE if operations are
    //            not allowed)
    //
    // RETURNS:
    //    - Boolean (TRUE if operation was successful, FALSE if any errors occurred)
    // 
    //   NOTES:
    public static Boolean OperationsAllowed(DateTime DateTime,
                                            TYPE_OPERATIONS_SCHEDULE_ITEM Schedule,
                                            out Boolean AllowOperations,
                                            SqlTransaction Trx)
    {
      Int32 _closing_hour;
      TimeSpan _datetime_span;
      TimeSpan _closing_time_span;
      Boolean _operations_schedule_enabled;

      AllowOperations = false;
      _operations_schedule_enabled = false;

      //Check if operations schedule is globally enabled/disabled
      _operations_schedule_enabled = GeneralParam.GetBoolean("OperationsSchedule", "Enabled");

      if (!_operations_schedule_enabled)
      {
        AllowOperations = true;

        return true;
      }


      //Get closing hour
      _closing_hour = GeneralParam.GetInt32("WigosGUI", "ClosingTime", -1);

      if (_closing_hour == -1)
      {

        return false;
      }

      _closing_time_span = DateTime.Today.AddHours(_closing_hour).TimeOfDay;

      //Check if operations are allowed for the day
      if (!Schedule.operations_allowed)
      {
        AllowOperations = false;

        return true;
      }

      _datetime_span = DateTime - Schedule.date_from.Date;

      //Check if we are on next natural day (but inside the same workday)
      if (Schedule.time1_from_minutes < _closing_time_span.TotalMinutes)
      {
        Schedule.time1_from_minutes += 1440;
        Schedule.time1_to_minutes += 1440;
      }
      else if (Schedule.time1_to_minutes <= _closing_time_span.TotalMinutes)
      {
        Schedule.time1_to_minutes += 1440;
      }

      if (_datetime_span.TotalMinutes >= Schedule.time1_from_minutes &&
          _datetime_span.TotalMinutes < Schedule.time1_to_minutes)
      {
        AllowOperations = true;

        return true;
      }

      //Check second time range
      if (Schedule.time2_from_minutes == -1 || Schedule.time2_to_minutes == -1)
      {
        AllowOperations = false;

        return true;
      }

      //Check if we are on next natural day (but inside the same workday)
      if (Schedule.time2_from_minutes < _closing_time_span.TotalMinutes)
      {
        Schedule.time2_from_minutes += 1440;
        Schedule.time2_to_minutes += 1440;
      }
      else if (Schedule.time2_to_minutes <= _closing_time_span.TotalMinutes)
      {
        Schedule.time2_to_minutes += 1440;
      }

      if (_datetime_span.TotalMinutes >= Schedule.time2_from_minutes &&
          _datetime_span.TotalMinutes < Schedule.time2_to_minutes)
      {
        AllowOperations = true;

        return true;
      }

      return true;
    } // OperationsAllowed

    //------------------------------------------------------------------------------
    // PURPOSE: Check if terminals are enabled for a given date and time
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateTime
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //        - Boolean (TRUE if terminals are enabled, FALSE if terminals are disabled)
    //
    // RETURNS:
    //    - Boolean (TRUE if operation was successful, FALSE if any errors occurred)
    // 
    //   NOTES:
    public static Boolean TerminalsEnabled(DateTime DateTime,
                                           TYPE_OPERATIONS_SCHEDULE_ITEM Schedule,
                                           out Boolean EnableTerminals,
                                           SqlTransaction Trx)
    {
      Int32 _closing_hour;
      TimeSpan _datetime_span;
      TimeSpan _closing_time_span;
      Boolean _terminals_schedule_enabled;
      DateTime _dt_aux;

      EnableTerminals = false;
      _terminals_schedule_enabled = false;

      //Check if operations schedule is globally enabled/disabled
      _terminals_schedule_enabled = GeneralParam.GetBoolean("OperationsSchedule", "DisableMachine");


      if (!_terminals_schedule_enabled)
      {
        EnableTerminals = true;

        return true;
      }


      //Get closing hour
      _closing_hour = GeneralParam.GetInt32("WigosGUI", "ClosingTime", -1);

      if (_closing_hour == -1)
      {

        return false;
      }

      _dt_aux = DateTime.Today.AddHours(_closing_hour);

      // Having in mind the configured time at operations schedule
      TimeSpan _ts = new TimeSpan(0, GeneralParam.GetInt32("OperationsSchedule", "DisableMachine.StartTrading"), 0);
      _closing_time_span = _dt_aux.TimeOfDay + _ts;

      _datetime_span = DateTime - Schedule.date_from.Date;


      if (Schedule.previous_day_time1_to_minutes != Schedule.time1_from_minutes)
      {
        Schedule.time1_from_minutes = Schedule.time1_from_minutes + GeneralParam.GetInt32("OperationsSchedule", "DisableMachine.StartTrading");
      }
      else
      {
        //no limit
        Schedule.time1_from_minutes = -1;
      }

      if (Schedule.time2_to_minutes == -1)
      {
        if (Schedule.time1_to_minutes != Schedule.next_day_time1_from_minutes)
        {
          Schedule.time1_to_minutes = Schedule.time1_to_minutes + GeneralParam.GetInt32("OperationsSchedule", "DisableMachine.EndTrading");
        }
        else
        {
          //no limit
          Schedule.time1_to_minutes = 2880;
        }
      }
      else
      {
        Schedule.time1_to_minutes = Schedule.time1_to_minutes + GeneralParam.GetInt32("OperationsSchedule", "DisableMachine.EndTrading");
      }

      if (Schedule.time2_from_minutes != -1)
      {
        Schedule.time2_from_minutes = Schedule.time2_from_minutes + GeneralParam.GetInt32("OperationsSchedule", "DisableMachine.StartTrading");

        if (Schedule.time2_to_minutes != Schedule.next_day_time1_from_minutes)
        {
          Schedule.time2_to_minutes = Schedule.time2_to_minutes + GeneralParam.GetInt32("OperationsSchedule", "DisableMachine.EndTrading");
        }
        else
        {
          //no limit
          Schedule.time2_to_minutes = 2880;
        }
      }

      //Check if we are on next natural day (but inside the same workday)
      if (Schedule.time1_from_minutes < _closing_time_span.TotalMinutes && Schedule.time1_from_minutes != -1)
      {
        Schedule.time1_from_minutes += 1440;
        Schedule.time1_to_minutes += 1440;
      }
      else if (Schedule.time1_to_minutes <= _closing_time_span.TotalMinutes)
      {
        Schedule.time1_to_minutes += 1440;
      }

      //Check if operations are allowed for the day
      if (!Schedule.operations_allowed)
      {
        EnableTerminals = false;

        return true;
      }

      if (_datetime_span.TotalMinutes >= Schedule.time1_from_minutes &&
          _datetime_span.TotalMinutes < Schedule.time1_to_minutes)
      {
        EnableTerminals = true;

        return true;
      }

      //Check second time range
      if (Schedule.time2_from_minutes != -1 && Schedule.time2_to_minutes != -1)
      {
        //Check if we are on next natural day (but inside the same workday)
        if (Schedule.time2_from_minutes <= _closing_time_span.TotalMinutes)
        {
          Schedule.time2_from_minutes += 1440;
          Schedule.time2_to_minutes += 1440;
        }
        else if (Schedule.time2_to_minutes <= _closing_time_span.TotalMinutes)
        {
          Schedule.time2_to_minutes += 1440;
        }
      }

      if (_datetime_span.TotalMinutes >= Schedule.time2_from_minutes &&
          _datetime_span.TotalMinutes < Schedule.time2_to_minutes)
      {
        EnableTerminals = true;

        return true;
      }

      return true;
    } // OperationsAllowed

    public static Boolean OperationsAllowed(DateTime DateTime,
                                            out Boolean AllowOperations,
                                            out Boolean EnabledTerminals)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return OperationsAllowed(DateTime, out AllowOperations, out EnabledTerminals, _db_trx.SqlTransaction);
      }
    }

    public static Boolean OperationsAllowed(DateTime DateTime,
                                            out Boolean AllowOperations,
                                            out Boolean EnabledTerminals,
                                            SqlTransaction Trx)
    {
      TYPE_OPERATIONS_SCHEDULE_ITEM _sched;
      TYPE_OPERATIONS_SCHEDULE_ITEM _terminals_sched;

      AllowOperations = true;
      EnabledTerminals = true;

      if (!GetOperationsSchedule(DateTime, out _sched, Trx))
      {

        return false;
      }

      if (!GetOperationsSchedule(DateTime, true, out _terminals_sched, Trx))
      {

        return false;
      }

      if (OperationsAllowed(DateTime, _sched, out AllowOperations, Trx) && TerminalsEnabled(DateTime, _terminals_sched, out EnabledTerminals, Trx))
      {
        return true;
      }
      else
      {
        return false;
      }
    } // OperationsAllowed

  }
}
