//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : TerminalFundsTransfer.cs
// 
//   DESCRIPTION : Class to contain terminal funds transfer
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-MAY-2015 XCD    First release.
// 15-JUL-2015 RCI    Fixed Bug WIG-2599: Error transferring credit from LCD Touch to terminal
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class TerminalFundsTransfer
  {
    public enum TRANSFER_STATUS
    {
      READY = 0,
      TRANSFER_PENDING = 1,
      NOTIFIED = 2,
      TRANSFERRING = 3,
      TRANSFERRED = 4,
      CANCELED_BY_USER = 5,
      CANCELED_BY_TERMINAL = 6,
    }

    // PURPOSE : Set value value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - FundsTransferStatus
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    public static Boolean DB_UpdateTerminalFundsTransferStatus(Int32 TerminalId, TRANSFER_STATUS NewStatus, SqlTransaction Trx)
    {
      StringBuilder _sb;
      TRANSFER_STATUS _current_status;
      Boolean _incorrect_new_status;

      try
      {
        // RCI 17-JUL-2015: This method is only used for FundsTransfers from Cashier (not for LCD Touch transfers).
        //                  So if GP is not enabled return true.
        if (!GeneralParam.GetBoolean("SasHost", "TransferFromCashier", false))
        {
          return true;
        }

        if (!DB_GetTransferStatus(TerminalId, out _current_status, Trx))
        {
          throw new Exception("Error on DB_GetTransferStatus: TerminalId:" + TerminalId);
        }

        _incorrect_new_status = true;

        switch (_current_status)
        {
          case TRANSFER_STATUS.READY:
            if (NewStatus == TRANSFER_STATUS.TRANSFER_PENDING
              || NewStatus == TRANSFER_STATUS.READY)
            {
              _incorrect_new_status = false;
            }
            break;

          case TRANSFER_STATUS.TRANSFER_PENDING:
            if (NewStatus == TRANSFER_STATUS.NOTIFIED
              || NewStatus == TRANSFER_STATUS.CANCELED_BY_USER)
            {
              _incorrect_new_status = false;
            }
            break;

          case TRANSFER_STATUS.NOTIFIED:
            if (NewStatus == TRANSFER_STATUS.TRANSFERRING
              || NewStatus == TRANSFER_STATUS.CANCELED_BY_USER
              || NewStatus == TRANSFER_STATUS.NOTIFIED)
            {
              _incorrect_new_status = false;
            }
            break;

          case TRANSFER_STATUS.TRANSFERRING:
            if (NewStatus == TRANSFER_STATUS.TRANSFERRED
              || NewStatus == TRANSFER_STATUS.CANCELED_BY_TERMINAL)
            {
              _incorrect_new_status = false;
            }
            break;

          case TRANSFER_STATUS.TRANSFERRED:
            if (NewStatus == TRANSFER_STATUS.READY)
            {
              _incorrect_new_status = false;
            }
            break;

          case TRANSFER_STATUS.CANCELED_BY_USER:
            if (NewStatus == TRANSFER_STATUS.READY
              || NewStatus == TRANSFER_STATUS.TRANSFER_PENDING)
            {
              _incorrect_new_status = false;
            }
            break;

          case TRANSFER_STATUS.CANCELED_BY_TERMINAL:
            if (NewStatus == TRANSFER_STATUS.READY
              || NewStatus == TRANSFER_STATUS.TRANSFER_PENDING
              || NewStatus == TRANSFER_STATUS.CANCELED_BY_USER)
            {
              _incorrect_new_status = false;
            }
            break;
          default:
            break;

        }

        if (_current_status == TRANSFER_STATUS.READY && _incorrect_new_status)
        {
          // RCI 15-JUL-2015: If transferring from Cashier, status can't be READY, so this would be an error, but when transferring from LCD Touch,
          //                  status will be READY. In this case, return true to continue.
          //                  There is no other option to filter between these two modes of transfer.
          return true;
        }

        if (_incorrect_new_status)
        {
          Log.Warning("DB_UpdateTerminalFundsTransferStatus: Incorrect status: TerminalId:" + TerminalId.ToString() + " OldStatus: " + _current_status.ToString() + " NewStatus:" + NewStatus.ToString());

          return false;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   TERMINALS ");
        _sb.AppendLine("   SET   TE_TRANSFER_STATUS  = @pFundsTransfer ");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pFundsTransfer", SqlDbType.Int).Value = (Int32)NewStatus;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = (Int32)TerminalId;
          _sql_cmd.ExecuteNonQuery();
        } 

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("DB_UpdateTerminalFundsTransferStatus: Updating TerminalId:" + TerminalId.ToString() + " Status:" + NewStatus.ToString());

        return false;
      }
    }

    public static Boolean DB_GetTransferStatus(Int32 TerminalId, out TRANSFER_STATUS TransferStatus, SqlTransaction Trx)
    {
      Object _obj;

      TransferStatus = TRANSFER_STATUS.READY;

      try
      {
        using (SqlCommand _cmd = new SqlCommand("SELECT TE_TRANSFER_STATUS FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId", Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null)
          {
            TransferStatus = (TRANSFER_STATUS)_obj;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

  }
}
