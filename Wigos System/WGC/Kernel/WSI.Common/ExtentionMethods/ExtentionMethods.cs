﻿using System.Data;
namespace System.Runtime.CompilerServices
{
}
namespace WSI.Common.ExtentionMethods
{

  public static class ExtentionMethods
  {
    public static IDbCommand CreateIDbCommand(this IDbTransaction Trx, string _sql_query)
    {
      IDbCommand _cmd = Trx.Connection.CreateCommand();
      _cmd.Connection = Trx.Connection;
      _cmd.Transaction = Trx;
      _cmd.CommandText = _sql_query;
      return _cmd;
    }

    public static IDbCommand AddParameter<T>(this IDbCommand Cmd, string ParamName, DbType Type, T Value)
    {
      IDbDataParameter _param = Cmd.CreateParameter();
      _param.ParameterName = ParamName;
      _param.DbType = Type;
      _param.Value = Value;
      Cmd.Parameters.Add(_param);
      return Cmd;
    }

    public static IDbCommand AddOutputParameter(this IDbCommand Cmd, string ParamName, DbType Type,
      out IDbDataParameter Param)
    {
      Param = Cmd.CreateParameter();
      Param.ParameterName = ParamName;
      Param.DbType = Type;
      Param.Direction = ParameterDirection.Output;
      Cmd.Parameters.Add(Param);
      return Cmd;
    }

    public static IDbCommand AddOutputParameter(this IDbCommand Cmd, string ParamName, DbType Type,
      out IDbDataParameter Param, string SrcCol)
    {
      Param = Cmd.CreateParameter();
      Param.ParameterName = ParamName;
      Param.DbType = Type;
      Param.Direction = ParameterDirection.Output;
      Param.SourceColumn = SrcCol;
      Cmd.Parameters.Add(Param);
      return Cmd;
    }

    public static IDbCommand AddParameter<T>(this IDbCommand Cmd, string ParamName, DbType Type, int Length, T Value)
    {
      IDbDataParameter _param = Cmd.CreateParameter();
      _param.ParameterName = ParamName;
      _param.DbType = Type;
      _param.Value = Value;
      _param.Size = Length;
      Cmd.Parameters.Add(_param);
      return Cmd;
    }

    public static IDbCommand AddParameter<T>(this IDbCommand Cmd, string ParamName, DbType Type, string SrcCol,
      int Length, T Value)
    {
      IDbDataParameter _param = Cmd.CreateParameter();
      _param.ParameterName = ParamName;
      _param.DbType = Type;
      _param.Value = Value;
      _param.SourceColumn = SrcCol;
      _param.Size = Length;
      Cmd.Parameters.Add(_param);
      return Cmd;
    }

    public static IDbCommand AddSourceParameter(this IDbCommand Cmd, string ParamName, DbType Type, string SrcCol,
      int Length)
    {
      IDbDataParameter _param = Cmd.CreateParameter();
      _param.ParameterName = ParamName;
      _param.DbType = Type;
      _param.SourceColumn = SrcCol;
      _param.Size = Length;
      Cmd.Parameters.Add(_param);
      return Cmd;
    }

    public static IDbCommand AddSourceParameter(this IDbCommand Cmd, string ParamName, DbType Type, string SrcCol)
    {
      IDbDataParameter _param = Cmd.CreateParameter();
      _param.ParameterName = ParamName;
      _param.DbType = Type;
      _param.SourceColumn = SrcCol;

      Cmd.Parameters.Add(_param);
      return Cmd;
    }
  }


}
