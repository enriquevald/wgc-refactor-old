//------------------------------------------------------------------------------
// Copyright � 2007-2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Voucher.cs
// 
//   DESCRIPTION : File than implement the following classes:
// 
//      VoucherBusinessLogic: Database logic for creating and saving vouchers
//      VoucherBuilder:       Builder class for vouchers, 
//                            based on operations that create more than one 
//                            voucher at time
//
//      1. Class: Voucher (Main voucher class)
//
//      Card Operations Vouchers: 
//      2. Class: VoucherCardAdd
//      3. Class: VoucherCardRedeem
//
//      Cash Desk Operation vouchers:
//      4. Class: VoucherCashDeskOpen
//      5. Class: VoucherCashDeskClose
// 
//      ...
// 
//      Handpay vouchers:
//      12. VoucherCardHandpay
//      13. VoucherCardHandpayCancellation
// 
//      Gift  vouchers:
//      14. VoucherGiftRequest
//      15. VoucherGiftDelivery
//
//      16. Generate PIN
//      17. Recharges Summary
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-AUG-2007 AJQ    First release.
// 23-JUL-2010 TJG    Handpay Voucher.
// 06-AUG-2010 TJG    Handpay Cancellation Voucher.
// 21-SEP-2010 MBF    Moved to WSI.Common.
// 26-OCT-2011 MPO    Name of mobile bank (For voucher mobile bank case)
// 22-NOV-2011 JMM    PIN Changed Voucher
// 23-NOV-2011 RCI    Optional sections in Handpay vouchers: signatures, game meters and play session.
// 02-DEC-2011 JMM    Adding denomination to Handpay vouchers.
// 21-DEC-2011 RCI    Added Bingo format flag.
// 23-DEC-2011 MPO    Added fields for the ticket Cash in, free text of prize coupon and information of recharge.
// 27-DEC-2011 MPO    (CashIn) Show promotion if different of 0, text for promotion.
// 04-JAN-2012 RCI    Optionally, Hide Totals in A and B tickets. Remove field for the ticket Cash in.
// 05-JAN-2012 JMM    Added a promotion losing voucher.
// 08-MAR-2012 MPO    When load a voucher set the hidden div after the cashier information.
// 09-MAR-2012 MPO    Set the initial balance in Voucher.A.ChasOut.
// 21-MAR-2012 MPO    Added "promo_and_coupon". If it shows text "promo_and_coupon" then it didn't shows promotion text and prize coupon text.
// 27-MAR-2012 JAB    Fixed bug #241, edit and remove incorrect information in "VoucherPromoLost" and "VoucherGeneratePIN" class.
// 06-JUN-2012 SSC    Added parameters control for null values in method VoucherPrintInit.
// 06-AUG-2012 MPO    Added POINTS_AWARDED or POINTS_CANCELED in voucher CashIn, CashOut and .Promo.Lost.
// 07-AUG-2012 JCM    Added Sign on MB Limit Extension and MB deposit. Renamed and redesign fields on Sign on MB Limit Extension and MB deposit.
// 26-SEP-2012 MPO    Voucher: VoucherGiftCancellation.
// 31-OCT-2012 JMM    Added a recharges summary voucher launched from PromoBOX.
// 05-DEC-2012 RCI    Support for multiple cancellation.
// 01-MAR-2013 ICS    Fixed Bug #629: Check that CurrencySymbol is not empty.
// 19-MAR-2013 RRB    Hide Prize Coupon in awarded promotion vouchers.
// 02-APR-2013 QMP & RCI   Hide Prize Coupon in ALL promotion vouchers (awarded and canceled).
// 19-APR-2013 ICS    Changed the machine name by its alias.
// 02-JUL-2013 DLL    New voucher for currency exchange.
// 26-AUG-2013 RMS    New voucher for currency exchange with bank card.
//                    Currency exchange voucher loads different html files ( by MovementType ).
//                    Title of currency exchange voucher changes by MovementType.
//                    Removed unnecessary currency activation status in vouchers CashDeskClose and CashDeskStatus.
// 27-AUG-2013 RMS    Removed unnecessary parameters in CashDeskClose functions ( SessionId and OperationId ).
// 28-AUG-2013 JAB    Added new camps in Customized's Voucher.
// 29-AUG-2013 JCOR   Added STR_VOUCHER_AUTHORIZED_BY.
// 30-AUG-2013 FBA    Added Recharge for points in CashDeskClose and CashDeskStatus. Fixed bug WIG-173.
// 02-SEP-2013 NMR    Return ID of ticket asociated to 'voucher'.
// 05-SEP-2013 DLL    Fixed Bug WIG-186: CloseCash & CashDeskClose show deposit and withdraws amount with all currencies.
// 09-SEP-2013 NMR    Adding TITO-Ticket print customization and preparing for data for Ticket print extension.
// 10-SEP-2013 DLL    Added Voucher for Check Recharge.
// 25-SEP-2013 DLL    Currency exchange values moved to VoucherCurrencyCash.
// 01-OCT-2013 DHA    Fixed Bug #WIG-246: CashIn Voucher with negative promotion due to Cash Desk Draw UNR.
// 04-OCT-2013 DDM & DHA    Fixed Bug #WIG-261: Error cancelling Cash Desk Draw promotions.
// 14-OCT-2013 DLL    UNR's Taxes.
// 15-OCT-2013 LEM    Added tickets info to VoucherCashDeskOverview if TITOMode is activated and IsCashier is True.
// 16-OCT-2013 DHA    Fixed Bug #WIG-282: Cash Desk Draw voucher, characters near right margin are not visible.
// 16-OCT-2013 DHA    Fixed Bug #WIG-283: Cash Desk Draw voucher, the promotion's footer now is under tax detail.
//                                        Promotion's summary voucher, when the promotion has footer and taxes, the taxes were wrong format.
// 17-OCT-2013 RMS    Added function to obtain a sequence id for split2 of an operation by operation id.
// 18-OCT-2013 DHA    Fixed Bug #WIG-286: PromoLost, when tax is 0 not shown on voucher.
// 25-OCT-2013 ACM    Fixed Bug #WIG-313: Ticket does't print nationality and birthplace.
// 29-OCT-2013 DHA    Fixed Bug #WIG-333: Hide prize cupon when Cash Desk Draw is enable.
// 30-OCT-2013 DLL    Fixed Bug #WIG-339: Show liabilities with '---' when no has data.
// 30-OCT-2013 ICS    Added undo operation vouchers suport.
// 18-NOV-2013 DHA    Fixed Bug #WIG-411: Wrong format in UNR promotions.
// 26-NOV-2013 LJM    Added Approval's signature if required.
// 28-NOV-2013 LJM    Fixed Bug #WIG-449: Error on total Withdraw.
// 30-NOV-2013 DLL    Added ticket chips sale.
// 09-DEC-2013 DLL    Fixed Bug WIG-467.
// 11-DEC-2013 JAB    Added ShowBalance functionality to printed vouchers.
// 18-DEC-2013 LJM    Added functionality to print Chips.
// 02-JAN-2014 DLL    Added CashInTax to VoucherCashDeskOverview and VoucherCashIn.
// 14-JAN-2014 RMS    Added VoucherCardCreditTransfer for TransferCredit operation.
// 15-JAN-2014 DLL    Added VoucherChipsPurchaseSale & VoucherChipsChange.
// 05-FEB-2014 DHA    Added Ticket redeem handpay/jackpot to VoucherCashDeskOverview.
// 12-FEB-2014 FBA    Added account holder id type control via CardData class IdentificationTypes. 
// 13-FEB-2014 DHA    Added ticket TITO information when generate the CashOut voucher.
// 12-MAR-2014 ICS    Fixed Bug #WIGOSTITO-1140: In Handpay Manual doesn't appears amount without deductions.
// 18-MAR-2014 JPJ    While in Multisite certain items change behaviour.
// 09-APR-2014 DLL    Fixed Bug WIG-801: It's necessary to show game profit when negative amount.
// 25-APR-2014 DHA    Added Tickets TITO detail.
// 30-APR-2014 DLL    Added VoucherChipsSaleDealerCopy and adapt VoucherChipsPurchaseSale for not show denominations when GP(ShowDenominations) is disabled.
// 05-JUN-2014 DLL    Fixed Bug WIG-1008: Javascript error when print Barcode. 
// 05-JUN-2014 DLL    Fixed Bug WIG-1009: Text cutted when print with Barcode.
// 06-JUN-2014 JAB    Added two decimal characters in denominations fields.
// 10-JUN-2014 DLL    Fixed Bug WIG-1024: Incorrect messagen when cash balance = 0 and game profit > 0
// 13-JUN-2014 LEM    Added CashInTax in Total for VoucherCashIn
// 03-JUL-2014 AMF    Added VoucherPointToCardReplacement
// 11-JUL-2014 JMM    Fixed Bug #WIGOSTITO-1244: Error on pay handpay when there is no playsession and GP to print played amount is active
// 14-JUL-2014 AMF    Regionalization
// 25-JUL-2014 DLL    Chips are moved to a separate table
// 31-JUL-2014 RMS    Fixed Bug WIG-1144: Corrected denominations ticket printing
// 07-AUG-2014 RMS & MPO & DRV Fixed Bug WIG-1166: incorrect currency title label on credit card and check.
// 08-AUG-2014 RMS    Added supoort for Specific Bank Card Types
// 16-SEP-2014 SMN    Added new concepts in voucher
// 16-SEP-2014 RMS    Cash Balance summarized or CashDeskOverview
// 16-SEP-2014 LMRS   Added VoucherSpecials
// 18-SEP-2014 RMS    Added Cash Advance concept in CashDeskOverview
// 02-OCT-2014 SGB    New footer voucher and overload voucher
// 07-OCT-2014 SGB    Setup footer voucher if specific or general & modify voucherCashDeskOpen to call
// 17-OCT-2014 MPO    Added GP "Cashier.Voucher", "Handpay.TotalInLetters"
// 18-NOV-2014 MPO    Added html special char (&nbsp;) for currency and integer string in voucher.
// 18-NOV-2014 LEM    Fixed Bug WIG-1694: Wrong amount "Check : In cash desk".
// 24-NOV-2014 MPO & OPC     LOGRAND CON02: Added IVA calculated (when needed) and input params class for cashin.
// 25-NOV-2014 OPC    Fixed Bug WIG-1750: fixed the error when select Cashier.Voucher.Mode 0 and 3.
// 11-DEC-2014 AMF    Fixed Bug WIG-1822: show Company B in MultiSite
// 19-DEC-2014 JBC    Fixed Bug WIG-1875: base amount on CashInVoucher
// 23-DEC-2014 JBC    Fixed Bug WIG-1893: Cancel voucher doesn't show card commission.
// 17-DEC-2014 DHA    Added Operation ID for all tickets
// 19-DEC-2014 DHA    Fixed Bug #WIG-1863: for cash desk draw tickets, set SplitA sequence for both tickets and asociated the new type for complementary voucher
// 05-JAN-2014 OPC    Added new comission Voucher for Cash Advance operations
// 08-JAN-2014 JBC    Fixed Bug WIG-1912: When IsCommissionToCompanyBEnabled, cash closing voucher shows Commission in B Voucher.
// 19-FEB-2014 YNM    Fixed Bug WIG-1894: Changes that allows to list the annulled tickets in "Cash Desk voucher report"
// 25-FEB-2014 YNM    Fixed Bug WIG-2102: Concepts voucher amounts was in IsoCode instead of Currency Symbol.
// 09-MAR-2015 DRV    Fixed Bug WIG-2143: Error on cash in voucher when integrated chips operations are enabled
// 17-MAR-2015 MPO    Fixed bug WIG-2154: It isn't necessary show the base amount when there aren't tax amount.
// 25-MAR-2015 YNM    Fixed Bug WIG-2176: Ticket Handpay: The concept deduction must be breakdown.
// 26-MAR-2015 YNM & MPO    Fixed Bug WIG-2176: Ticket Handpay: Decimal rounding for negative.
// 14-APR-2015 YNM    Fixed Bug TFS-1088: Ticket Handpay/Ticket CashOut: Tax breakdown on Tax Detail/Deductions. 
// 13-APR-2015 FJC    BackLog Item 710
// 17-APR-2015 YNM    Fixed Bug WIG-2222: Ticket Handpay error calculating Taxes.
// 20-APR-2015 YNM    Fixed Bug WIG-2226: Ticket CashOut cashless error on prize amount.  
// 23-APR-2015 YNM    Fixed Bug WIG-2240: Payment Order + Chips Buy take cashier taxes 
// 19-MAY-2015 RCI    Fixed Bug WIG-2367: Wrong GP names for cash deposit and withdrawal personalized footer.
// 28-MAY-2015 DHA    Added changes in cash desk draw and promo lost ticket for Winpot
// 27-MAY-2015 DCS    Added new multisite multicurrency filter and necessary changes to operate in multicurrency mode
// 23-JUN-2015 DHA    Added custom parameters for cashin chips operations
// 17-AUG-2015 DCS    Fixed Bug: WIG-2181 The barcode is not printed when it hides the currency symbol
// 03-AUG-2015 ETP    Fixed BUG 3496 Use of common header for card replacement.
// 05-AUG-2015 FAV    TFS 2797. Changes in VoucherSpecials to print a comment and footer about Concepts (using general params).
// 07-AUG-2015 ETP    Fixed BUG-3596 Corrected white space for expired redeem in visualization of cage summary.
// 24-AUG-2015 FAV    TFS-3257: Transfer between cashier sessions. 
// 29-AUG-2015 YNM    TFS-2796: BV-15: Print Difference Voucher in Cashier Closing.
// 27-AUG-2015 DHA    Show cash over/short only when is cashier session not integrated
// 28-AUG-2015 RCI & DDM   Bug-4027:  Added general param "GamingTables-IntegratedChipsSales.Voucher.Promotion.Text" in promotion concept.
// 01-SEP-2015 DHA    Added number of tickets to pay concept
// 22-SEP-2015 FAV    Fixed BUG-4593: Cahiers summary, error with collected value
// 05-OCT-2015 AMF    Fixed Bug TFS-4922: Postal Code, Federal entity depends country
// 06-OCT-2015 SDS   Fixed Bug 4594: Hist�rico de t�ckets de caja: algunas denominaciones no son visibles en el cierre
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 12-NOV-2015 FAV    Product Backlog Item 4695: Gaming Hall, Collect and Refill vouchers
// 17-NOV-2015 FAV    Backlog Item 6629: Custom reimbursement voucher (Migration to MICO)
// 19-NOV-2015 ECP    Backlog Item 6463 Garcia River-09 Cash Report Resume & Closing Cash Voucher - Include Tickets Tito
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 17-NOV-2015 FAV    Backlog Item 6629: Custom reimbursement voucher (Migration to MICO)
// 26-NOV-2015 MPO    Fixed TFS Bug 7124: Card deposit for voucher mode 7
// 01-DEC-2015 FOS    Fixed Bug 7205: Protected taxWaiverMode and renamed function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015 FOS    Fixed Bug 7208: Incorrect Tax Value on Handpays
// 17-NOV-2015 FAV    Backlog Item 6629: Custom reimbursement voucher (Migration to MICO)
// 26-NOV-2015 MPO    Fixed TFS Bug 7124: Card deposit for voucher mode 7
// 13-JAN-2015 JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 19-JAN-2016 DDS    Bug 8545:Mesas de Juego: No se puede cerrar mesas de juego con divisa/fichas
// 25-JAN-2016 YNM    Product Backlog Item 6656:Visitas / Recepci�n: Crear ticket de entrada
// 01-FEB-2016 AVZ    Product Backlog Item 8673:Visitas / Recepci�n: Configuraci�n de Modo de Recepci�n
// 09-FEB-2016 FAV    PBI 9148: Added changes in VoucherTitoTicketOperation to support a void of Machine Ticket
// 16-FEB-2016 DDS    PBI 9442: Palacio de los N�meros. Use 'Texto cup�n' field on Voucher
// 01-MAR-2016 DHA    Product Backlog Item 10083: Winpot - Provisioning Tax
// 01-MAR-2016 JML    Product Backlog Item 10084: Winpot - Provisioning Tax: Cashdesk draw
// 02-MAR-2016 DDS    Fixed Bug 10157: Missing info in 'Res�men de caja'
// 03-MAR-2016 JML    Product Backlog Item 10085: Winpot - Tax Provisions: Include movement reports
// 17-MAR-2016 EOR    Product Backlog Item 10235:TITA: Reporte de tickets
// 17-MAR-2016 ESE    Product Backlog Item 10229:TITA: Create swap ticket
// 14-ABR-2016 LTC    Product Backlog Item 11295: Codere - Custody Tax: Implement tax
// 19-APR-2016 RGR    Product Backlog Item 11297: Codere - Tax Custody: Reports and cash movements and account
// 21-APR-2016 LTC    Product Backlog Item 11296:Codere - Tax Custody: Redeem Tax Implementation
// 27-APR-2016 RGR    Product Backlog Item 11298: Codere - Tax Custody: Modify reports GUI Income and Return
// 10-MAY-2016 EOR    Fixed Bug 12997: Tax Custody: Result of Cashier wrong amount
// 22-MAR-2106 YNM    Bug 10759: Reception error paying with currency
// 24-MAR-2016 RAB    Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 14-APR-2016 FAV    Bug 11718: Changes in Cash Closing voucher to shows Cash Excess and Cash Shortfall for foreign exchanges.
// 21-APR-2016 FAV    Bug 11718: Added support to payments with card and cheque
// 03-MAY-2016 FAV    Product Backlog Item 12653:Cajero: Currency exchange for multiple denominations (Mallorca)
// 10-MAY-2015 JBP    Product Backlog Item 12931:Visitas / Recepci�n: 2� Ticket LOPD
// 19-MAY-2016 FAV    Bug 13394: Cashier summary with information unaligned
// 03-JUN-2016 FAV    Fixed Bug 11718: Changes in Cash Closing voucher to shows Cash Excess and Cash Shortfall for foreign exchanges.
// 01-JUL-2016 ETP    Fixed Bug 15018:Asociaci�n de tarjeta: Vista previa incorrecta
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public enum PrintMode
  {
    Preview = 0,
    Print = 1
  }

  public enum CashDeskCloseVoucherType
  {
    ALL = 0,
    A = 1,
    B = 2
  }

  public class VoucherValidationNumber
  {
    public Int64 ValidationNumber;
    public Currency Amount;
    public TITO_VALIDATION_TYPE ValidationType;
    public TITO_TICKET_TYPE TicketType;
    public String CreatedTerminalName;
    public Int64 CreatedPlaySessionId;
    public Int64 CreatedAccountId;
    public Int64 TicketId;
  }

  public class VoucherBusinessLogic
  {
    #region Public

    /// <summary>
    /// Voucher database initialization:
    /// 1. Get a new voucher id: For voucher mode preview vooucher id. is not requested from db)
    /// 2. Get voucher header, footer and print flag from general params.
    /// </summary>
    /// <param name="Mode">Voucher mode:</param>
    ///                    1. Preview.
    ///                    2. Print.
    /// <param name="SqlTrx">Sql Transaction</param>
    /// <param name="Header">Voucher header</param>
    /// <param name="Footer">Voucher footer</param>
    /// <param name="Print">Voucher print</param>
    /// <returns></returns>
    /// 
    public static Int64 VoucherPrintInit(Int64 SessionId
                                       , Int32 UserId
                                       , String UserName
                                       , Int32 TerminalId
                                       , String TerminalName
                                       , SqlTransaction SqlTrx
                                       , CashierVoucherType Type
                                       , OperationCode OperationCode
                                       , out String Header
                                       , out String Footer
                                       , out Boolean Print)
    {
      String _sql_str;
      SqlCommand _sql_cmd;
      SqlParameter _sql_param;

      // Initializations:
      Header = "";
      Footer = "";
      Print = false;

      // Steps:
      // 1. Get a new voucher id.

      // Note: Only for voucher mode Print an id is requested from database.
      _sql_str = "";
      _sql_str += "INSERT INTO   cashier_vouchers ( cv_session_id, cv_user_id, cv_user_name, cv_cashier_id, cv_cashier_name ) ";
      _sql_str += "     VALUES                    ( @p0, @p1, @p2, @p3, @p4 )  ";
      _sql_str += "        SET    @p5 = SCOPE_IDENTITY() ";

      _sql_cmd = new SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx);
      if (SessionId == 0)
      {
        _sql_cmd.Parameters.Add("@p0", SqlDbType.BigInt).Value = DBNull.Value;
      }
      else
      {
        _sql_cmd.Parameters.Add("@p0", SqlDbType.BigInt).Value = SessionId;
      }

      if (UserId == 0)
      {
        _sql_cmd.Parameters.Add("@p1", SqlDbType.Int).Value = DBNull.Value;
      }
      else
      {
        _sql_cmd.Parameters.Add("@p1", SqlDbType.Int).Value = UserId;
      }

      if (UserName == null)
      {
        _sql_cmd.Parameters.Add("@p2", SqlDbType.NVarChar, 50).Value = "";
      }
      else
      {
        _sql_cmd.Parameters.Add("@p2", SqlDbType.NVarChar, 50).Value = UserName;
      }

      if (TerminalId == 0)
      {
        _sql_cmd.Parameters.Add("@p3", SqlDbType.Int).Value = DBNull.Value;
      }
      else
      {
        _sql_cmd.Parameters.Add("@p3", SqlDbType.Int).Value = TerminalId;
      }

      if (TerminalName == null)
      {
        _sql_cmd.Parameters.Add("@p4", SqlDbType.NVarChar, 50).Value = "";
      }
      else
      {
        _sql_cmd.Parameters.Add("@p4", SqlDbType.NVarChar, 50).Value = TerminalName;
      }

      _sql_param = _sql_cmd.Parameters.Add("@p5", SqlDbType.BigInt);
      _sql_param.Direction = ParameterDirection.Output;

      if (_sql_cmd.ExecuteNonQuery() != 1)
      {
        Log.Error("VoucherInit. New voucher id couldn�t be retrieved.");

        return 0;
      }

      // Get header, footer and print information from db.
      GetVoucherInformation(SqlTrx, Type, out Header, out Footer, out Print);

      return (Int64)_sql_param.Value;

    } // VoucherPrintInit

    /// <summary>
    /// Voucher initialization:
    /// Only gets voucher header and footer from general params.
    /// </summary>
    /// <param name="Mode">Voucher mode:</param>
    ///                    1. Preview.
    ///                    2. Print.
    /// <param name="SqlTrx">Sql Transaction</param>
    /// <param name="Header">Voucher header</param>
    /// <param name="Footer">Voucher footer</param>
    /// <returns></returns>
    /// 
    public static void GetVoucherInformation(SqlTransaction SqlTrx, CashierVoucherType Type, out String Header, out String Footer, out Boolean Print)
    {
      Footer = GetCashierVoucherText(Type, CashierVoucherField.Footer);
      Header = GetCashierVoucherText(Type, CashierVoucherField.Header);
      Print = GetPrintVoucher(SqlTrx);
    } // GetVoucherInformation

    /// <summary>
    /// Insert HTML voucher into database. (Voucher Audit)
    /// </summary>
    /// <param name="VoucherId">Voucher id.</param>
    /// <param name="Html">Voucher HTML</param>
    /// <param name="SqlTrx">Sql Transaction</param>
    /// <returns></returns>
    public static Boolean VoucherSetHtml(Int64 VoucherId, Int64 OperationId, String Html, SqlTransaction SqlTrx)
    {
      String sql_str;
      SqlCommand sql_command;

      sql_str = "";
      sql_str += "  UPDATE   cashier_vouchers    ";
      sql_str += "     SET   cv_html         = @p1          ";
      sql_str += "       ,   cv_operation_id = @OperationId ";
      sql_str += "   WHERE   cv_voucher_id   = @p2          ";

      sql_command = new SqlCommand(sql_str, SqlTrx.Connection, SqlTrx);

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, Int32.MaxValue, "cv_html").Value = Html;
      sql_command.Parameters.Add("@OperationId", SqlDbType.BigInt, 8, "cv_operation_id").Value = OperationId;
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "cv_voucher_id").Value = VoucherId;

      if (sql_command.ExecuteNonQuery() != 1)
      {
        return false;
      }

      return true;

    } // VoucherSetHtml

    /// <summary>
    /// Get Print mode from general params.
    /// </summary>
    /// <returns></returns>
    public static Boolean GetPrintVoucher()
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Boolean _print_voucher;

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        _print_voucher = GetPrintVoucher(_sql_trx);

        _sql_trx.Commit();

        return _print_voucher;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }
    }

    /// <summary>
    /// Get Print mode from general params.
    /// </summary>
    /// <param name="SqlTrx">Sql Transaction</param>
    /// <returns></returns>
    public static Boolean GetPrintVoucher(SqlTransaction SqlTrx)
    {
      String _print_voucher;
      String _env_print_voucher;

      _print_voucher = "";

      try
      {
        // If environment variable CASHIER_VOUCHER_PRINT is defined, use it instead of 
        // GeneralParam Cashier.Voucher.Print.
        _env_print_voucher = Environment.GetEnvironmentVariable("CASHIER_VOUCHER_PRINT");
        if (_env_print_voucher != null)
        {
          return (Resource.GetCompareInfo().Compare(_env_print_voucher, "1") == 0);
        }
        _print_voucher = GeneralParam.Value("Cashier.Voucher", "Print");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return (Resource.GetCompareInfo().Compare(_print_voucher, "1") == 0);
    } // GetPrintVoucher

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher text to footer, title or header
    //
    //  PARAMS:
    //      - INPUT:
    //          - CashierVoucherType: Taking the type Voucher as enum
    //          - CashierVoucherField: Taking the field Voucher as enum
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private static string GetCashierVoucherText(CashierVoucherType Type, CashierVoucherField Field)
    {
      String _type;
      String _field;
      String _gp;
      String _text;

      switch (Type)
      {
        case CashierVoucherType.NotSet:
          _type = "";
          break;
        case CashierVoucherType.MBSalesLimitChange:
          _type = "MobileBank.SalesLimitChange";
          break;
        case CashierVoucherType.MBDeposit:
          _type = "MobileBank.Deposit";
          break;
        case CashierVoucherType.MBClosing:
          _type = "MobileBank.Closing";
          break;
        default:
          _type = Type.ToString();
          break;
      }

      _field = Field.ToString();

      _gp = _field;
      if (!String.IsNullOrEmpty(_type))
      {
        _gp = _type + "." + _field;
      }

      _text = GeneralParam.Value("Cashier.Voucher", _gp);
      if (String.IsNullOrEmpty(_text))
      {
        _text = GeneralParam.Value("Cashier.Voucher", _field);
      }

      return _text;
    } // GetCashierVoucherText

    #endregion


  } // VoucherBusinessLogic

  public class Voucher
  {

    #region Constants

    public const String VOUCHER_DIV_REPRINT = "<div id=\"HIDDEN_REPRINT_TEXT\"></div>";
    public const String VOUCHER_DIV_UNDO_OPERATION = "<div id=\"HIDDEN_UNDO_OPERATION_TEXT\"></div>";
    public const String VOUCHER_DIV_EXCHANGE = "@HIDDEN_EXCHANGE_TEXT";
    public const int VOUCHER_CACHIER_INFO_MAX_ELEMENTS = 20;

    #endregion

    #region Members

    public String VoucherFileName;
    public Boolean PrintVoucher;
    public String VoucherHTML;

    // RCI 17-MAY-2011
    public Int32 VoucherMode;
    public DateTime VoucherDateTime;
    // RCI 25-MAY-2011: For Mode 01 (Palacio de los N�meros)
    public SequenceId VoucherSequenceId;
    public Int64 VoucherSeqNumber;
    public PrintMode PrintMode;
    public SqlTransaction SqlTrx;
    // It's only used for B part.
    public Int64 CancelledSequenceId;

    private Dictionary<String, Object> m_dictionary = new Dictionary<String, Object>();
    private CashierVoucherType VoucherType = CashierVoucherType.NotSet;

    // DHA 17-JUL-2015
    public OperationCode OperationCode = OperationCode.NOT_SET;
    public OperationVoucherParams.VoucherParameters m_voucher_params;

    private Int32 m_num_pending_copies;
    public Int32 NumPendingCopies
    {
      get
      {
        return m_num_pending_copies;
      }
      set
      {
        m_num_pending_copies = value;
      }
    }

    #endregion

    #region Class Atributes

    public Int64 VoucherId;
    String VoucherHeader;
    String VoucherFooter;

    #endregion

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE: Generic constructor for vouchers
    //
    //  PARAMS:
    //      - INPUT:
    //          - PrintMode Mode: Print or preview mode
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public Voucher(PrintMode Mode, SqlTransaction SQLTransaction)
    {
      InitializeVoucher(Mode, SQLTransaction);

    } // Voucher

    //------------------------------------------------------------------------------
    // PURPOSE: Generic constructor for vouchers
    //
    //  PARAMS:
    //      - INPUT:
    //          - PrintMode Mode: Print or preview mode
    //          - CashierVoucherType: Taking the type Voucher as enum
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public Voucher(PrintMode Mode, CashierVoucherType Type, SqlTransaction SQLTransaction)
    {
      VoucherType = Type;
      SetValue("CV_TYPE", (Int32)VoucherType);

      InitializeVoucher(Mode, SQLTransaction);
    }

    public Voucher(PrintMode Mode, OperationCode OperationCode, SqlTransaction SQLTransaction)
    {
      this.OperationCode = OperationCode;

      InitializeVoucher(Mode, SQLTransaction);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize the Voucher fields
    //
    //  PARAMS:
    //      - INPUT:
    //          - PrintMode Mode: Print or preview mode
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private void InitializeVoucher(PrintMode Mode, SqlTransaction SQLTransaction)
    {
      // Class Initializations
      VoucherId = 0;
      VoucherHTML = "";
      VoucherFileName = "";
      VoucherHeader = "";
      VoucherFooter = "";
      VoucherDateTime = DateTime.MinValue;

      VoucherSequenceId = SequenceId.NotSet;
      PrintMode = Mode;
      SqlTrx = SQLTransaction;

      // Load voucher configuration parameters
      m_voucher_params = OperationVoucherParams.OperationVoucherDictionary.GetParameters(OperationCode);
      m_num_pending_copies = m_voucher_params.PrintCopy;

      if (!Int32.TryParse(GeneralParam.Value("Cashier.Voucher", "Mode"), out VoucherMode))
      {
        VoucherMode = 0;
      }
      SetValue("CV_MODE", VoucherMode);

      // Initialize voucher.
      if (Mode == PrintMode.Preview)
      {
        // Voucher Mode: Get header, footer and print information from db.
        VoucherBusinessLogic.GetVoucherInformation(SQLTransaction, VoucherType, out VoucherHeader, out VoucherFooter, out PrintVoucher);
      }
      else if (!m_voucher_params.Generate)
      {
        VoucherId = 0;
      }
      else
      {
        // Voucher Mode: Print - Get header and footer and Voucher id. from db.
        VoucherId = VoucherBusinessLogic.VoucherPrintInit(CommonCashierInformation.SessionId
                                                        , CommonCashierInformation.UserId
                                                        , CommonCashierInformation.UserName
                                                        , (Int32)CommonCashierInformation.TerminalId
                                                        , CommonCashierInformation.TerminalName
                                                        , SQLTransaction
                                                        , VoucherType
                                                        , OperationCode
                                                        , out VoucherHeader
                                                        , out VoucherFooter
                                                        , out PrintVoucher);

        // DHA 17-JUL-2015: Set print mode with voucher configuration
        PrintVoucher = PrintVoucher && m_voucher_params.Print;
      }

    } //InitializeVoucher

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor with no parameters
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //      - RCI 27-DEC-2010: Used in Email class.
    // 
    public Voucher()
    {
      // Class Initializations
      VoucherId = 0;
      VoucherHTML = "";
      VoucherFileName = "";
      VoucherHeader = "";
      VoucherFooter = "";
      PrintVoucher = false;
    } // Voucher

    #endregion

    #region Properties

    public Int64 Id
    {
      get
      {
        return VoucherId;
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Insert string into voucher HTML
    /// </summary>
    /// <param name="Parameter"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    public Boolean SetParameterValue(String Parameter, String Value)
    {
      String _modified_voucher;

      try
      {
        // Insert title into html voucher
        _modified_voucher = VoucherHTML.Replace(Parameter, Value);
        VoucherHTML = _modified_voucher;

        return true;
      }
      catch (Exception ex)
      {
        Log.Error(String.Format("SetParameterValue ({0}, {1}) failed!. Exception: {2}", Parameter, Value, ex.Message));
      }

      return false;

    } // SetParameterValue

    /// <summary>
    /// Convert especial characters into HTML check code.
    /// </summary>
    /// <param name="InitialStr"></param>
    /// <returns></returns>
    private string EncodeHTMLSpecialChars(String InputString, Boolean HTMLSpaceChar)
    {
      String _html;

      _html = HttpUtility.HtmlEncode(InputString);

      if (HTMLSpaceChar)
      {
        _html = _html.Replace(" ", "&nbsp;");
      }

      return _html;
    }

    private string[] VoucherCashierInfo()
    {
      String[] _voucher_cashier_info;
      int _idx;

      _idx = 0;
      _voucher_cashier_info = new String[VOUCHER_CACHIER_INFO_MAX_ELEMENTS];

      String _series = "";
      Int64 _sequence = 0;
      Boolean _show_sequence = false;

      if (VoucherSequenceId != SequenceId.NotSet)
      {
        // Get Branch, Sequence and Series.
        switch (VoucherSequenceId)
        {
          case SequenceId.VouchersSplitA:
            _series = GeneralParam.Value("Cashier.Voucher.Mode.01", "Series.A");
            _show_sequence = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.A.ShowSequence", false);
            break;

          case SequenceId.VouchersSplitB:
          case SequenceId.VouchersSplitBCancel:
            _series = GeneralParam.Value("Cashier.Voucher.Mode.01", "Series.B");
            _show_sequence = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.B.ShowSequence", false);
            break;
          case SequenceId.VoucherEntrances:
            _show_sequence = true;
            break;

          case SequenceId.VoucherTaxProvisions:
            _series = GeneralParam.Value("Cashier.TaxProvisions", "Series");
            _show_sequence = GeneralParam.GetBoolean("Cashier.TaxProvisions", "Voucher.ShowSequence", false);
            break;
          // LTC 14-ABR-2016
          case SequenceId.VoucherTaxCustody:
            _series = GeneralParam.Value("Cashier.TaxCustody", "Series");
            _show_sequence = GeneralParam.GetBoolean("Cashier.TaxCustody", "Voucher.ShowSequence", false);
            break;

          default:
            // AMF 12-FEB-2015: "Foliado" CageConcepts
            if (VoucherSequenceId >= SequenceId.CageConcepts && VoucherSequenceId <= SequenceId.CageConcepts_Last)
            {
              _series = "";
              _show_sequence = true;
            }
            break;
        }

        if (PrintMode == PrintMode.Print && _show_sequence && m_voucher_params.Generate)
        {
          Sequences.GetValue(SqlTrx, VoucherSequenceId, out _sequence);
          SetValue("CV_SEQUENCE", _sequence);
          VoucherSeqNumber = _sequence;
        }

      }

      // JML 14-AUG-2012
      switch (VoucherMode)
      {
        case 0:
        default:
          // DHA 19-DEC-2014: moved to esay way to understand
          if (CommonCashierInformation.MobileBankId != 0)
          {
            String _mb_id;

            _mb_id = CommonCashierInformation.MobileBankId.ToString();
            if (!String.IsNullOrEmpty(CommonCashierInformation.MobileBankUserName))
            {
              _mb_id += " - " + CommonCashierInformation.MobileBankUserName.Substring(0, Math.Min(20, CommonCashierInformation.MobileBankUserName.Length));
            }

            _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_GENERAL_NAME_MB") + ": " + _mb_id;
          }

          _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + CommonCashierInformation.UserName;

          _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + Computer.Alias(CommonCashierInformation.TerminalName);

          // DHA 17-DEC-2014: added general param to show OperationId
          if (GeneralParam.GetBoolean("Cashier.Voucher", "ShowOperationId", false))
          {
            if (PrintMode == PrintMode.Preview)
            {
              // On preview mode show OperationId = 0
              _voucher_cashier_info[_idx++] = Resource.String("STR_FRM_VOUCHERS_REPRINT_014") + ": " + "0";
            }
            else
            {
              // On print, set a key for the Save method can insert the OperationId
              //_voucher_cashier_info[_idx++] = Resource.String("STR_FRM_VOUCHERS_REPRINT_014") + ": " + "@VOUCHER_OPERATION_ID";
              _voucher_cashier_info[_idx++] = "@VOUCHER_OPERATION_ID";
            }
          }

          // DHA 17-DEC-2014: added general param to show VoucherId
          if (GeneralParam.GetBoolean("Cashier.Voucher", "ShowVoucherId", true))
          {
            _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_VOUCHER_ID") + ": " + VoucherId.ToString();
          }

          if (_show_sequence)
          {
            _voucher_cashier_info[_idx] = Resource.String("STR_VOUCHER_FOLIO_ID")
                                  + ": " + _sequence.ToString("000000000000");
            if (_series != "")
            {
              _voucher_cashier_info[_idx] = _voucher_cashier_info[_idx]
                        + " " + Resource.String("STR_VOUCHER_SERIES_ID") + ": " + _series;
            }
            _idx++;
            // Para imprimir el n� de secuencia del documento devuelto
            if (CancelledSequenceId > 0) // no imprimir para Palacio de los N�meros
            {
              _voucher_cashier_info[_idx] = Resource.String("STR_VOUCHER_FOLIO_ID_REF")
                        + ": " + CancelledSequenceId.ToString("000000000000");
              if (_series != "")
              {
                _voucher_cashier_info[_idx] = _voucher_cashier_info[_idx]
                          + " " + Resource.String("STR_VOUCHER_SERIES_ID") + ": " + _series;
              }
              _idx++;
            }
          }
          break;

        case 1: // Palacio de los N�meros
          {
            // Always Show Sequence in "Palacio de los N�meros".
            _show_sequence = true;

            _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_CASHIER_ID")
                                  + " " + Computer.Alias(CommonCashierInformation.TerminalName)
                                  + " - " + CommonCashierInformation.UserName;
            _voucher_cashier_info[_idx++] = GeneralParam.Value("Cashier.Voucher.Mode.01", "Branch")
                                  + " " + VoucherDateTime.ToString();

            // DHA 17-DEC-2014: added general param to show OperationId
            if (GeneralParam.GetBoolean("Cashier.Voucher", "ShowOperationId", false))
            {
              if (PrintMode == PrintMode.Preview)
              {
                // On preview mode show OperationId = 0
                _voucher_cashier_info[_idx++] = Resource.String("STR_FRM_VOUCHERS_REPRINT_014") + ": " + "0";
              }
              else
              {
                // On print, set a key for the Save method can insert the OperationId
                _voucher_cashier_info[_idx++] = "@VOUCHER_OPERATION_ID";
              }
            }

            // DHA 17-DEC-2014: added general param to show VoucherId
            if (GeneralParam.GetBoolean("Cashier.Voucher", "ShowVoucherId", true))
            {
              _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_VOUCHER_ID") + ": " + VoucherId.ToString();
            }
          }

          if (_show_sequence)
          {
            _voucher_cashier_info[_idx] = Resource.String("STR_VOUCHER_FOLIO_ID")
                                  + ": " + _sequence.ToString("000000000000");
            if (_series != "")
            {
              _voucher_cashier_info[_idx] = _voucher_cashier_info[_idx]
                        + " " + Resource.String("STR_VOUCHER_SERIES_ID") + ": " + _series;
            }
            _idx++;
          }
          break;
      }

      if (CommonCashierInformation.UserId != CommonCashierInformation.AuthorizedByUserId && CommonCashierInformation.AuthorizedByUserId != 0)
      {
        _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ": " + CommonCashierInformation.AuthorizedByUserName; // jcor   

        //LJM 26-NOV-2013 If approver's signature is required
        if (GeneralParam.GetInt32("Cashier.Voucher", "AuthorizedBySignature") > 0)
        {
          _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_AUTHORIZED_BY_SIGNATURE") + ": ";

          for (int i = 0; i < GeneralParam.GetInt32("Cashier.Voucher", "AuthorizedBySignature", 0, 0, 10); i++)
          {
            _voucher_cashier_info[_idx - 1] += System.Environment.NewLine;
          }
        }
      }

      return _voucher_cashier_info;
    } // VoucherCashierInfo

    private string[] VoucherExchangeInfo()
    {
      String[] _voucher_cashier_info;
      int _idx;

      _voucher_cashier_info = new String[VOUCHER_CACHIER_INFO_MAX_ELEMENTS];
      _idx = 0;

      // Show exchange rate when is floor dual currency
      if (Misc.IsFloorDualCurrencyEnabled())
      {
        List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();
        List<CurrencyExchange> _currencies = null;
        CurrencyExchange _national_curr = null;

        _types.Add(CurrencyExchangeType.CURRENCY);

        CurrencyExchange.GetAllowedCurrencies(true, _types, false, out _national_curr, out _currencies);

        if (_currencies.Count > 1)
        {
          _voucher_cashier_info[_idx++] = "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
          foreach (CurrencyExchange _cur in _currencies)
          {
            if (_national_curr.CurrencyCode != _cur.CurrencyCode)
            {
              _voucher_cashier_info[_idx++] = "<tr><td colspan='2 'align='center'><p>" + Resource.String("STR_VOUCHER_EXCHANGE_RATE") + " 1 " + _cur.CurrencyCode + "=" + _cur.ChangeRate + " " + _national_curr.CurrencyCode + "</b></p></td></tr>";
            }
          }
          _voucher_cashier_info[_idx++] = "</table>";
        }
      }

      return _voucher_cashier_info;
    } // VoucherCashierInfo

    private string ConvertCurrencyToLetters(Currency Value)
    {
      return Common.NumberToLetter.CurrencyString(Value);
    } // ConvertCurrencyToLetters

    private String GetDefaultSignatureRoom(Boolean ShowAuthorizedBy)
    {
      String _default_signature =
                "<center><b>" + Resource.String("STR_VOUCHER_SIGN_TITLE") + "</b></center>" +
                "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" +
                  "<tr>" +
                    "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SIGN_CLIENT") + ":</td>" +
                  "</tr>" +
                  "<tr><td>&nbsp;</td></tr>" +
                  "<tr><td>&nbsp;</td></tr>" +

                  (ShowAuthorizedBy ?
                  "<tr>" +
                    "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SIGN_AUTHORIZED_BY") + ":</td>" +
                  "</tr>" : String.Empty) +

                  "<tr><td>&nbsp;</td></tr>" +
                  "<tr><td>&nbsp;</td></tr>" +
                "</table>" +
                "<hr noshade size=\"4\">";

      return _default_signature;
    }

    #endregion

    #region Public Methods

    public void SetValue(String Key, Object Value)
    {
      if (!m_dictionary.ContainsKey(Key))
      {
        m_dictionary.Add(Key, Value);
      }
      else
      {
        m_dictionary[Key] = Value;
      }
    } // SetValue

    /// <summary>
    /// Chage page setup settings for printing.
    /// 1. Clear header and footer.
    /// 2. Set proper margins.
    /// </summary>
    public static Boolean PageSetup()
    {
      RegistryKey regkey;
      Object ie_header;
      Object ie_footer;
      Object margin_top;
      Object margin_bottom;
      Object margin_left;
      Object margin_right;
      Object shrink_to_fit;
      PrintDocument _dummy_print_document;
      String _printer_name;
      Decimal _margin_left_value;
      Decimal _margin_right_value;

      // 1. Clear header and footer.
      regkey = Registry.CurrentUser;

      try
      {
        regkey = regkey.OpenSubKey(@"Software\Microsoft\Internet Explorer\PageSetup", true);
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error reading registry PageSetup key. " + ex.Message);

        return false;
      }

      if (regkey == null)
      {
        Log.Error("PageSetup. Registry key does not exist");

        return false;
      }

      try
      {
        ie_header = regkey.GetValue("header");
        regkey.SetValue("header", "");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating header key on registry PageSetup. " + ex.Message);

        return false;
      }

      try
      {
        ie_footer = regkey.GetValue("footer");
        regkey.SetValue("footer", "");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating footer key on registry PageSetup. " + ex.Message);

        return false;
      }

      // 2. Set proper margins.
      try
      {
        margin_top = regkey.GetValue("margin_top");
        regkey.SetValue("margin_top", "0");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating margin_top key on registry PageSetup. " + ex.Message);

        return false;
      }

      try
      {
        margin_bottom = regkey.GetValue("margin_bottom");
        regkey.SetValue("margin_bottom", "0");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating margin_bottom key on registry PageSetup. " + ex.Message);

        return false;
      }

      _dummy_print_document = new PrintDocument();
      _printer_name = _dummy_print_document.PrinterSettings.PrinterName.ToUpper();

      // XID 06-SEP-2010. Increased to 0.16 to support Epson TM-T88V
      _margin_left_value = 0.16m;
      _margin_right_value = 0;

      if (_printer_name.Contains("CUSTOM_SIZE"))
      {
        _margin_left_value = GeneralParam.GetDecimal("Cashier.Voucher", "PrinterCustomSize.Margin.Left", 0.16m);
        _margin_right_value = GeneralParam.GetDecimal("Cashier.Voucher", "PrinterCustomSize.Margin.Right", 0);
      }

      try
      {
        margin_left = regkey.GetValue("margin_left");
        regkey.SetValue("margin_left", Format.LocalNumberToDBNumber(_margin_left_value.ToString()));
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating margin_left key on registry PageSetup. " + ex.Message);

        return false;
      }

      try
      {
        margin_right = regkey.GetValue("margin_right");
        regkey.SetValue("margin_right", Format.LocalNumberToDBNumber(_margin_right_value.ToString()));
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating margin_right key on registry PageSetup. " + ex.Message);

        return false;
      }

      try
      {
        shrink_to_fit = regkey.GetValue("Shrink_To_Fit");
        regkey.SetValue("Shrink_To_Fit", "yes");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating Shrink_To_Fit key on registry PageSetup. " + ex.Message);

        return false;
      }

      return true;
    } // PageSetup

    /// <summary>
    /// Call a browser control to print the input voucher.
    /// </summary>
    public string GetFileName()
    {
      FileStream file_stream;
      StreamWriter file_writer;
      String voucher_filename;

      file_writer = null;
      voucher_filename = "Voucher.html";

      try
      {
        // Create a temporary file with voucher to print
        voucher_filename = Path.Combine(Directory.GetCurrentDirectory(), voucher_filename);

        file_stream = new FileStream(voucher_filename, FileMode.Create, FileAccess.Write);
        file_writer = new StreamWriter(file_stream);

        // Write voucher
        file_writer.BaseStream.Seek(0, SeekOrigin.Begin);
        file_writer.Write(VoucherHTML);

      }
      catch (Exception ex)
      {
        Log.Error("Print. Error writing html voucher. File: " + voucher_filename.ToString());
        Log.Exception(ex);
      }
      finally
      {
        file_writer.Close();

        VoucherFileName = voucher_filename;
      }

      return voucher_filename;
    } // GetFileName

    public void LoadHeader(String Value)
    {
      VoucherHeader = Value;
    } // LoadHeader

    public void LoadFooter(String Footer, String ExtraFooter)
    {
      VoucherFooter = Footer;
      if (ExtraFooter != "")
      {
        VoucherFooter += "\n" + ExtraFooter;
      }
    } // LoadFooter


    /// <summary>
    /// Loads voucher structure from resource file.
    /// </summary>
    /// <returns>HTML voucher string</returns>
    public void LoadVoucher(String VoucherName)
    {
      String _html_doc;
      Int32 _position;
      Boolean _insert_hidden_div;
      String _data_before_div;

      _html_doc = "";

      // Get voucher structure from resource file.
      try
      {
        _html_doc = Resource.String(VoucherName);
      }
      catch (Exception ex)
      {
        Log.Error("Error reading voucher structure file from resource." + VoucherName);
        Log.Exception(ex);
      }

      if (String.IsNullOrEmpty(_html_doc))
      {
        Log.Error("LoadVoucher. Voucher retrieved from Db is empty. Voucher: " + VoucherName);
      }

      // UNDO OPERATION
      _insert_hidden_div = false;
      _data_before_div = "@VOUCHER_HEADER</div>";

      _position = _html_doc.IndexOf(_data_before_div);
      if (_position >= 0)
      {
        _position += _data_before_div.Length;
        _insert_hidden_div = true;
      }

      if (_insert_hidden_div)
      {
        _html_doc = _html_doc.Insert(_position, VOUCHER_DIV_UNDO_OPERATION);
      }

      // ExChange
      _insert_hidden_div = false;
      _data_before_div = "@VOUCHER_CASHIER_INFO";

      _position = _html_doc.IndexOf(_data_before_div);
      if (_position >= 0 && Misc.IsFloorDualCurrencyEnabled())
      {
        _position += _data_before_div.Length;
        _insert_hidden_div = true;
      }

      if (_insert_hidden_div)
      {
        _html_doc = _html_doc.Insert(_position, VOUCHER_DIV_EXCHANGE);
      }

      // REPRINT
      _insert_hidden_div = false;
      _data_before_div = "@VOUCHER_CASHIER_INFO";

      _position = _html_doc.IndexOf(_data_before_div);
      if (_position >= 0)
      {
        _position += _data_before_div.Length;
        _insert_hidden_div = true;
      }

      if (_insert_hidden_div)
      {
        _html_doc = _html_doc.Insert(_position, VOUCHER_DIV_REPRINT);
      }

      VoucherHTML = _html_doc;
    } // LoadVoucher



    /// <summary>
    /// Load voucher general data
    /// </summary>
    /// <param name="AddFooter">Checks if should to add a the general footer</param>
    protected void LoadVoucherGeneralData(bool AddFooter)
    {
      String[] header_data;
      String[] footer_data;
      String[] _vc_info;
      Int32 num_rows;
      Boolean _encode;

      // Header: Casino data from database - Split database string
      if (!String.IsNullOrEmpty(VoucherHeader))
      {
        String header = VoucherHeader.Replace("\\n", "\n");
        header_data = header.Split('\n');
      }
      else
      {
        header_data = new String[1] { "" };
      }
      AddMultipleLineString("VOUCHER_HEADER", header_data);

      // MBF 20-SEP-2010 Cannot insert Voucher Logo in database
      AddString("VOUCHER_LOGO", "");

      // VoucherDateTime must be set before calling VoucherCashierInfo().
      VoucherDateTime = WGDB.Now;
      AddDateTime("VOUCHER_DATE_TIME", VoucherDateTime);

      // Return an array of String containing the Voucher Cashier Info, but also
      // set the Sequence (Folio) for Mode 01 (Palacio de los N�meros).
      _vc_info = VoucherCashierInfo();

      AddMultipleLineString("VOUCHER_CASHIER_INFO", _vc_info);

      // Return an array of String containing the currencies exchange
      _vc_info = VoucherExchangeInfo();
      AddMultipleLineString("HIDDEN_EXCHANGE_TEXT", _vc_info, false);

      // Footer: Footer from database - Split database string
      // A final text should be added at the end in order to print line breaks.
      if (!String.IsNullOrEmpty(VoucherFooter))
      {
        num_rows = 0;
        String footer = VoucherFooter.Replace("\\n", "\n");
        footer_data = footer.Split('\n');

        // Line breaks are replaced by blanks un voucher printing: Add a 
        // character on the last line if there is an blank character line.
        // APB (05-MAR-2009)
        // GetUpperBound returns a 0-based number: it is not safe to use its result - 1
        num_rows = footer_data.GetUpperBound(num_rows);
        if (footer_data[num_rows].CompareTo(" ") == 0)
        {
          footer_data[num_rows] = String.Concat(footer_data[num_rows], ".");
        }
      }
      else
      {
        footer_data = new String[1] { "" };
      }

      //SGB 08-OCT-2014 Use especific CashierVoucherType for especial voucher footer.
      switch (VoucherType)
      {
        case CashierVoucherType.CashOpening:
        case CashierVoucherType.CashClosing:
        case CashierVoucherType.MBDeposit:
        case CashierVoucherType.MBClosing:
        case CashierVoucherType.MBSalesLimitChange:
        case CashierVoucherType.CashDeposit:
        case CashierVoucherType.CashWithdrawal:
        case CashierVoucherType.CashDeskCloseUnbalanced:
        case CashierVoucherType.CashierTransferCashDeposit:
        case CashierVoucherType.CashierTransferCashWithdrawal:
          _encode = false;
          break;
        default:
          _encode = true;
          break;
      }

      if (AddFooter)
        AddMultipleLineString("VOUCHER_FOOTER", footer_data, _encode);

    } // LoadVoucherGeneralData


    /// <summary>
    /// Load voucher general data
    /// </summary>
    protected void LoadVoucherGeneralData()
    {
      LoadVoucherGeneralData(true);
    } // LoadVoucherGeneralData

    /// <summary>
    /// Load voucher general data
    /// </summary>
    protected void LoadVoucherCageGeneralDataFromGUI(String[] VcInfo)
    {
      String[] header_data;
      String[] footer_data;
      int num_rows;

      // Header: Casino data from database - Split database string
      if (!String.IsNullOrEmpty(VoucherHeader))
      {
        String header = VoucherHeader.Replace("\\n", "\n");
        header_data = header.Split('\n');
      }
      else
      {
        header_data = new String[1] { "" };
      }
      AddMultipleLineString("VOUCHER_HEADER", header_data);

      // MBF 20-SEP-2010 Cannot insert Voucher Logo in database
      AddString("VOUCHER_LOGO", "");

      // VoucherDateTime must be set before calling VoucherCashierInfo().
      VoucherDateTime = WGDB.Now;
      AddDateTime("VOUCHER_DATE_TIME", VoucherDateTime);

      // Return an array of String containing the Voucher Cashier Info, but also
      // set the Sequence (Folio) for Mode 01 (Palacio de los N�meros).

      VcInfo[0] = Resource.String("STR_VOUCHER_CAGE_NEW_OPERATION_SESSION") + ": " + VcInfo[0];
      VcInfo[1] = Resource.String("STR_VOUCHER_CAGE_NEW_OPERATION_USER") + ": " + VcInfo[1];
      VcInfo[2] = Resource.String("STR_VOUCHER_VOUCHER_ID") + ": " + VoucherId.ToString();
      VcInfo[3] = "";

      AddMultipleLineString("VOUCHER_CAGE_NEW_OPERATION_INFO", VcInfo);

      // Footer: Footer from database - Split database string
      // A final text should be added at the end in order to print line breaks.
      if (!String.IsNullOrEmpty(VoucherFooter))
      {
        num_rows = 0;
        String footer = VoucherFooter.Replace("\\n", "\n");
        footer_data = footer.Split('\n');

        // Line breaks are replaced by blanks un voucher printing: Add a 
        // character on the last line if there is an blank character line.
        // APB (05-MAR-2009)
        // GetUpperBound returns a 0-based number: it is not safe to use its result - 1
        num_rows = footer_data.GetUpperBound(num_rows);
        if (footer_data[num_rows].CompareTo(" ") == 0)
        {
          footer_data[num_rows] = String.Concat(footer_data[num_rows], ".");
        }
      }
      else
      {
        footer_data = new String[1] { "" };
      }
      AddMultipleLineString("VOUCHER_FOOTER", footer_data);

    } // LoadVoucherGeneralData

    /// <summary>
    /// Save a voucher into the vouchers table
    /// </summary>
    /// <param name="SQLTransaction"></param>
    public Boolean Save(SqlTransaction SQLTransaction)
    {
      return Save(0, SQLTransaction);
    } // Save

    /// <summary>
    /// Save a voucher into the vouchers table
    /// </summary>
    /// <param name="SQLTransaction"></param>
    public Boolean Save(Int64 OperationId, SqlTransaction SQLTransaction)
    {
      Int64 _account_id;

      // DHA 17-JUL-2015: Save only if the operation code is configured
      if (!m_voucher_params.Generate)
      {
        return true;
      }

      if (OperationId > 0)
      {
        if (Operations.DB_GetAccountIdFromOperation(OperationId, out _account_id, SQLTransaction))
        {
          SetValue("CV_ACCOUNT_ID", _account_id);
        }
        // DHA 19-DEC-2014: added to set the operation id in tickets
        VoucherHTML = VoucherHTML.Replace("@VOUCHER_OPERATION_ID", Resource.String("STR_FRM_VOUCHERS_REPRINT_014") + ": " + OperationId.ToString());
      }

      VoucherHTML = VoucherHTML.Replace("@VOUCHER_OPERATION_ID<br>", "");

      SetValue("CV_HTML", VoucherHTML);
      SetValue("CV_OPERATION_ID", OperationId);

      return VoucherSaveDictionary();
    } // Save

    public Boolean VoucherSaveDictionary()
    {
      String _sql_txt;
      String _param_name;
      Int32 _idx_param;
      SqlCommand _sql_cmd;
      Decimal _dec_value;

      if (m_dictionary.Count == 0)
      {
        return true;
      }

      _sql_cmd = new SqlCommand();
      _sql_cmd.Connection = SqlTrx.Connection;
      _sql_cmd.Transaction = SqlTrx;

      _sql_txt = "";
      _sql_txt += "UPDATE   CASHIER_VOUCHERS ";

      _idx_param = 1;
      foreach (String _key in m_dictionary.Keys)
      {
        _param_name = "@p" + _idx_param;
        _sql_txt += _idx_param == 1 ? "  SET   " : "       , ";
        _sql_txt += _key + " = " + _param_name;

        if (m_dictionary[_key] is Currency)
        {
          _dec_value = (Decimal)((Currency)m_dictionary[_key]);
          _sql_cmd.Parameters.Add(new SqlParameter(_param_name, _dec_value));
        }
        else
        {
          _sql_cmd.Parameters.Add(new SqlParameter(_param_name, m_dictionary[_key]));
        }

        _idx_param++;
      }
      _sql_txt += " WHERE   CV_VOUCHER_ID   = @pVoucherId ";

      _sql_cmd.Parameters.Add("@pVoucherId", SqlDbType.BigInt).Value = VoucherId;

      _sql_cmd.CommandText = _sql_txt;

      if (_sql_cmd.ExecuteNonQuery() != 1)
      {
        return false;
      }

      return true;
    } // VoucherSaveDictionary

    /// <summary>
    /// Add string parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddString(String Parameter, String Value)
    {
      //if (Parameter == "VOUCHER_CARD_ACCOUNT_ID" || Parameter == "VOUCHER_CARD_CARD_ID")
      //{
      //  Value = EncryptText(Parameter, Value);
      //}

      String param_value = "";

      // Check special characters
      param_value = EncodeHTMLSpecialChars(Value, false);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddString

    /// <summary>
    /// Add string percent parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddStringPercent(String Parameter, Decimal Value, Int32 Decimals)
    {
      //if (Parameter == "VOUCHER_CARD_ACCOUNT_ID" || Parameter == "VOUCHER_CARD_CARD_ID")
      //{
      //  Value = EncryptText(Parameter, Value);
      //}

      String param_value = "";

      // Check special characters
      param_value = EncodeHTMLSpecialChars(Convert.ToString(Math.Round(Value, Decimals)) + " %", true);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddStringPercent

    private string EncryptText(string Parameter, string Value)
    {
      try
      {
        switch (Parameter)
        {
          case "VOUCHER_CARD_CARD_ID":
            Value = "************" + Value.Remove(0, 12); /* total number width - only number left for display */
            break;

          case "VOUCHER_CARD_ACCOUNT_ID":
            Value = "**************" + Value.Remove(0, 2);
            break;
        }
      }
      catch (Exception)
      {
        return "";
      }

      return Value;
    } // EncryptText

    public Boolean AddMultipleLineString(String Parameter, String PlainData)
    {
      String[] _str_multiline;

      if (!String.IsNullOrEmpty(PlainData))
      {
        String _str = PlainData.Replace("\\n", "\n");
        _str_multiline = _str.Split('\n');
      }
      else
      {
        _str_multiline = new String[1] { "" };
      }

      return AddMultipleLineString(Parameter, _str_multiline);
    } // AddMultipleLineString

    /// <summary>
    /// Add string parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddMultipleLineString(String Parameter, String[] MultiLineData)
    {
      return AddMultipleLineString(Parameter, MultiLineData, true);
    }

    public Boolean AddMultipleLineString(String Parameter, String[] MultiLineData, Boolean Encode)
    {
      String _param_value = "";
      String _param_complete = "";
      Int32 _num_lines;
      Int32 _idx_lines;

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      _num_lines = MultiLineData.Length;

      for (_idx_lines = 0; _idx_lines < _num_lines; _idx_lines++)
      {
        // Check for null string
        if (!String.IsNullOrEmpty(MultiLineData[_idx_lines]))
        {
          // Check special characters
          if (Encode)
          {
            _param_value = EncodeHTMLSpecialChars(MultiLineData[_idx_lines], false);
          }
          else
          {
            _param_value = MultiLineData[_idx_lines];
          }
          _param_value = _param_value.Replace(System.Environment.NewLine, "<br />");
          _param_value = _param_value + "<br>\n";

          _param_complete = String.Concat(_param_complete, _param_value);
        }
      }

      // Insert title into html voucher
      return SetParameterValue(Parameter, _param_complete);
    } // AddMultipleLineString

    /// <summary>
    /// Add currency parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddCurrency(String Parameter, Currency Value)
    {
      return AddCurrency(Parameter, Value, false);
    } // AddCurrency

    /// <summary>
    /// Add currency parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <param name="WithLetters">Indicate if to write the currency with letters</param>
    /// <returns></returns>
    public Boolean AddCurrency(String Parameter, Currency Value, Boolean WithLetters)
    {
      String param_value = "";
      String param_letters = "";

      // Apply currency format to string
      param_value = Value.ToString();

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, true);

      if (WithLetters)
      {
        param_letters = EncodeHTMLSpecialChars(ConvertCurrencyToLetters(Value), false);
        param_value += "<br/>" + param_letters;
      }

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddCurrency

    /// <summary>
    /// Add numeric parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddNumber(String Parameter, Int32 Value)
    {
      String param_value = "";

      // Apply currency format to string
      param_value = Value.ToString("#,##0.00");

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, true);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddNumber

    public Boolean AddPoint(String Parameter, Points Value)
    {
      String param_value = "";

      // Apply currency format to string
      param_value = Value.ToString();

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, true);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddPoint

    /// <summary>
    /// Add integer parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddInteger(String Parameter, Int32 Value)
    {
      String param_value = "";

      // Apply currency format to string
      param_value = Value.ToString("#,##0");

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, true);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddInteger

    /// <summary>
    /// Add datetime parameter into voucher.
    /// </summary>
    /// <param name="Parameter"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    public Boolean AddDateTime(String Parameter, DateTime Value)
    {
      String param_value = "";

      // Apply currency format to string
      param_value = Format.CustomFormatDateTime(Value, true);

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, false);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddDateTime

    /// <summary>
    /// Add currency in letters.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddCurrencyInLetters(String Parameter, Currency Value)
    {
      String param_value;

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(ConvertCurrencyToLetters(Value), false);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      return SetParameterValue(Parameter, param_value);
    } // AddCurrencyInLetters

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the voucher Sequence Id for B by Operation Id
    //
    //  PARAMS :
    //      - INPUT:
    //            - OperationId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - 0 : If operation have no sequence 
    //      - >0 : A voucher sequence number 
    //
    //   NOTES: 
    //   
    internal Int64 GetSecuenceId_B(Int64 OperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;
      Int64 _sequence_id;

      _sequence_id = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   CV_SEQUENCE                      ");
        _sb.AppendLine("  FROM   CASHIER_VOUCHERS                 ");
        _sb.AppendLine(" WHERE   CV_OPERATION_ID  = @pOperationId ");
        _sb.AppendLine("   AND   CV_TYPE         >= 2             ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _sequence_id = (Int64)_obj;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _sequence_id;
    } // GetSequenceId_B

    /// <summary>
    /// Delete currency simbol in voucher Html
    /// </summary>
    /// <param name="VoucherHtml">Html from voucher</param>
    /// <returns></returns>
    public void ReplaceCurrencySimbol(String ReplacementValue)
    {
      String _modified_voucher;

      _modified_voucher = VoucherHTML.Replace("<script type='text/javascript'>$", "@STR_JAVASCRIPT");

      _modified_voucher = _modified_voucher.Replace(NumberFormatInfo.CurrentInfo.CurrencySymbol, ReplacementValue);

      _modified_voucher = _modified_voucher.Replace("@STR_JAVASCRIPT", "<script type='text/javascript'>$");

      VoucherHTML = _modified_voucher;
    } // ReplaceCurrencySimbol

    /// <summary>
    /// Set the parameter value for the Signature Romm by Voucher type
    /// </summary>
    /// <param name="VoucherType"></param>
    internal void SetSignatureRoomByVoucherType(Type VoucherType)
    {
      Boolean _sign_room;
      String _param;

      if (VoucherType == typeof(VoucherCardHandpay))
      {
        _sign_room = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.SignaturesRoom");
        _param = "@VOUCHER_CARD_HANDPAY_SIGNATURES_ROOM";
      }
      else if (VoucherType == typeof(VoucherHandpayAuthorized))
      {
        _sign_room = true;
        _param = "@VOUCHER_AUTHORITZATION_SIGNATURES";
      }
      else if (VoucherType == typeof(VoucherCashOut))
      {
        _sign_room = GeneralParam.GetBoolean("Cashier.Voucher", "Redeem.SignaturesRoom");
        _param = "@VOUCHER_REDEEM_SIGNATURES_ROOM";
      }
      else
      {
        throw new Exception("The voucher type passed like a parameter is invalid to get a Signature room");
      }

      if (_sign_room)
      {
        SetParameterValue(_param, GetDefaultSignatureRoom(true));
      }
      else
      {
        SetParameterValue(_param, "");
      }
    }

    #endregion
  } // Voucher

  public static class VoucherBuilder
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashIn operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int64 CashierSessionId
    //          - Int64 OperationId   : Operation id (not need in Preview mode)
    //          - PrintMode Mode      : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashDeskClose(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                                        , PrintMode Mode
                                        , SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      Voucher _temp_voucher;      
      _voucher_list = new ArrayList();
      Boolean _print_unbalanced;
      VoucherCashDeskCloseUnbalanced _unbalanced_voucher;
      Decimal _difference;

      // Voucher for Cash Desk (All)
      if (Misc.IsGamingHallMode())
      {
        _temp_voucher = new VoucherGamingHallCashDeskClose(Mode, SQLTransaction, CashierSessionStats);
      }
      else 
      {
      _temp_voucher = new VoucherCashDeskClose(CashierSessionStats
                                             , CashDeskCloseVoucherType.ALL
                                             , Mode
                                             , SQLTransaction);
      }
      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      if (CashierSessionStats.split_enabled_b)
      {
        // Voucher for Cash Desk Business A
        _temp_voucher = new VoucherCashDeskClose(CashierSessionStats
                                               , CashDeskCloseVoucherType.A
                                               , Mode
                                               , SQLTransaction);

        if (Mode == PrintMode.Print)
        {
          _temp_voucher.Save(SQLTransaction);
        }

        _voucher_list.Add(_temp_voucher);

        // Voucher for Cash Desk Business B
        _temp_voucher = new VoucherCashDeskClose(CashierSessionStats
                                               , CashDeskCloseVoucherType.B
                                               , Mode
                                               , SQLTransaction);

        if (Mode == PrintMode.Print)
        {
          _temp_voucher.Save(SQLTransaction);
        }

        _voucher_list.Add(_temp_voucher);

      }

      _print_unbalanced = GeneralParam.GetBoolean("Cashier.Voucher", "CashClosing.Unbalanced.Print", false);

      if (_print_unbalanced)
      {
        _difference = 0;

        foreach (KeyValuePair<CurrencyIsoType, Decimal> collected_diff in CashierSessionStats.collected_diff)
        {
          _difference += collected_diff.Value;
        }

        if (_difference != 0)
        {
          //print VoucherCurrenciesDifferences
          _unbalanced_voucher = new VoucherCashDeskCloseUnbalanced(CashierSessionStats
                                                                  , Mode
                                                                  , SQLTransaction);
          if (Mode == PrintMode.Print)
          {
            _unbalanced_voucher.Save(SQLTransaction);
          }
          _voucher_list.Add(_unbalanced_voucher);
        }
      }
      return _voucher_list;
    } // CashIn

    public static ArrayList CashIn(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , VoucherCashIn.InputDetails Details
                                   , Result CashDeskDrawResult
                                   , Int64 OperationId
                                   , PrintMode Mode
                                   , SqlTransaction SQLTransaction)
    {
      return CashIn(VoucherAccountInfo
                   , AccountId
                   , SplitInformation
                   , Details
                   , CashDeskDrawResult
                   , OperationId
                   , OperationCode.NOT_SET
                   , Mode
                   , SQLTransaction);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashIn operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName     : Player holder name
    //          - String CardId         : Player account id
    //          - String Trackdata      : Player card trackdata
    //          - Currency CurrentBalance : Actual Balance of the card
    //          - Currency CashIn1      : Amount to pay as Deposit
    //          - Currency CashIn2      : Amount to pay as Uso de sala
    //          - Int64 OperationId     : Operation id (not need in Preview mode)
    //          - PrintMode Mode        : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashIn(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , VoucherCashIn.InputDetails Details
                                   , Result CashDeskDrawResult
                                   , Int64 OperationId
                                   , OperationCode OpCode
                                   , PrintMode Mode
                                   , SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherCashIn _temp_voucher;
      List<VoucherDrawCashDesk> _temp_voucher_draw_cash_desk;
      Currency _cash_in_2_aux;
      CurrencyExchangeType _payment_type;
      Currency _exchange_commission;
      Boolean _is_foreign_currency;
      CASHIER_MOVEMENT _voucher_cashier_movement;

      _voucher_list = new ArrayList();
      _cash_in_2_aux = Details.CashIn2;
      _payment_type = Details.PaymentType;
      _exchange_commission = Details.ExchangeCommission;
      _is_foreign_currency = Details.WasForeingCurrency;
      _voucher_cashier_movement = CASHIER_MOVEMENT.NOT_ASSIGNED;

      // DHA 29-OCT-2013: Hide prize cupon when Cash Desk Draw is enable
      // DRV & RCI 30-JUL-2014: Integrated chip operations never use CashDeskDraw.
      if (!Details.IsIntegratedChipsSale &&
          CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws())
      {
        if (CashDeskDrawResult != null)
        {
          Details.FinalBalance.Balance.PromoNotRedeemable -= CashDeskDrawResult.m_nr_awarded;
        }
        if (CashDeskDrawResult != null &&
            (!CashDeskDrawResult.m_is_winner || GeneralParam.GetInt32("CashDesk.Draw", "AccountingMode") != 1))
        {
          Details.FinalBalance.Balance.PromoRedeemable -= CashDeskDrawResult.m_re_awarded;
        }

        _cash_in_2_aux = 0;
      }

      // Voucher for Cash In Deposit
      VoucherCashIn.InputDetails _details = new VoucherCashIn.InputDetails();

      _details.InitialBalance = Details.InitialBalance;
      _details.FinalBalance = Details.FinalBalance;
      _details.CashIn1 = Details.CashIn1;
      _details.CashIn2 = _cash_in_2_aux;
      _details.CardPrice = Details.CardPrice;
      _details.CashInTaxSplit1 = Details.CashInTaxSplit1;
      _details.CashInTaxSplit2 = Details.CashInTaxSplit2;
      _details.CashInTaxCustody = Details.CashInTaxCustody; // LTC 14-ABR-2016
      _details.TotalBaseTaxSplit2 = Details.TotalBaseTaxSplit2;
      _details.TotalTaxSplit2 = Details.TotalTaxSplit2;
      _details.IsIntegratedChipsSale = Details.IsIntegratedChipsSale;

      _temp_voucher = new VoucherCashIn(VoucherAccountInfo
                                       , AccountId
                                       , SplitInformation
                                       , _details
                                       , CASHIER_MOVEMENT.CASH_IN_SPLIT1
                                       , OpCode
                                       , Mode
                                       , SQLTransaction);

      if (SplitInformation.company_a.hide_currency_symbol &&
         !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
      {
        _temp_voucher.ReplaceCurrencySimbol("");
      }

      if (Mode == PrintMode.Print)
      {
        //If its a chips swap it means that there are not cash in
        if (OpCode != OperationCode.CHIPS_SWAP)
        {
          _temp_voucher.Save(OperationId, SQLTransaction);
        }
      }

      //If its a chips swap it means that there are not cash in
      if (OpCode != OperationCode.CHIPS_SWAP)
      { 
        _voucher_list.Add(_temp_voucher);
      }

      if (_temp_voucher.VoucherMode == 5)  // VoucherMode = 5 -> Logrand mode: Only one voucher (including A & B)
      {
        return _voucher_list;
      }

      if (Details.CashIn1 > 0 && SplitInformation.enabled_b)
      {
        _details = new VoucherCashIn.InputDetails();

        _details.InitialBalance = Details.InitialBalance;
        _details.FinalBalance = Details.FinalBalance;
        _details.CashIn1 = Details.CashIn1;
        _details.CashIn2 = Details.CashIn2;
        _details.CardPrice = 0;
        _details.CashInTaxSplit1 = 0;
        _details.CashInTaxSplit2 = Details.CashInTaxSplit2;

        // Voucher for Cash In Uso de Sala
        _temp_voucher = new VoucherCashIn(VoucherAccountInfo
                                         , AccountId
                                         , SplitInformation
                                         , _details
                                         , CASHIER_MOVEMENT.CASH_IN_SPLIT2
                                         , OpCode
                                         , Mode
                                         , SQLTransaction);

        if (SplitInformation.company_b.hide_currency_symbol &&
         !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          _temp_voucher.ReplaceCurrencySimbol("");
        }

        if (Mode == PrintMode.Print)
        {
          //If its a chips swap it means that there are not cash in
          if (OpCode != OperationCode.CHIPS_SWAP)
          {
            _temp_voucher.Save(OperationId, SQLTransaction);
          }
        }

        //If its a chips swap it means that there are not cash in
        if (OpCode != OperationCode.CHIPS_SWAP)
        {
          _voucher_list.Add(_temp_voucher);
        }

        //JBC 24-DEC-2014 Commission B Voucher
        if (Misc.IsCommissionToCompanyBEnabled() && _exchange_commission >= 0)
        {
          switch (_payment_type)
          {
            case CurrencyExchangeType.CARD:
              _voucher_cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2;
              break;

            case CurrencyExchangeType.CHECK:
              _voucher_cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2;
              break;

            case CurrencyExchangeType.CURRENCY:
              if (_is_foreign_currency)
              {
                _voucher_cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2;
              }
              break;

            default:
              break;
          }

          if (_voucher_cashier_movement != CASHIER_MOVEMENT.NOT_ASSIGNED)
          {
            _details.ExchangeCommission = _exchange_commission;
            // Voucher for Cash In Uso de Sala
            _temp_voucher = new VoucherCashIn(VoucherAccountInfo
                                             , AccountId
                                             , SplitInformation
                                             , _details
                                                 , _voucher_cashier_movement
                                             , OpCode
                                             , Mode
                                             , SQLTransaction);

            if (SplitInformation.company_b.hide_currency_symbol &&
                !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
            {
              _temp_voucher.ReplaceCurrencySimbol("");
            }

            if (Mode == PrintMode.Print)
            {
              //If its a chips swap it means that there are not cash in
              if (OpCode != OperationCode.CHIPS_SWAP)
              {
              _temp_voucher.Save(OperationId, SQLTransaction);
              }
            }

            //If its a chips swap it means that there are not cash in
            if (OpCode != OperationCode.CHIPS_SWAP)
            {
            _voucher_list.Add(_temp_voucher);
            }
        }
      }
      }

      if (Misc.IsCardPlayerToCompanyB() && Details.CardPrice > 0)
      {
        _details = new VoucherCashIn.InputDetails();

        _details.InitialBalance = Details.InitialBalance;
        _details.FinalBalance = Details.FinalBalance;
        _details.CashIn1 = 0;
        _details.CashIn2 = 0;
        _details.CardPrice = Details.CardPrice;
        _details.CashInTaxSplit1 = 0;
        _details.CashInTaxSplit2 = 0;


        // Voucher for Cash In Uso de Sala
        _temp_voucher = new VoucherCashIn(VoucherAccountInfo
                                         , AccountId
                                         , SplitInformation
                                         , _details
                                         , CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN
                                         , OpCode
                                         , Mode
                                         , SQLTransaction);
        if (SplitInformation.company_a.hide_currency_symbol &&
         !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          _temp_voucher.VoucherHTML = _temp_voucher.VoucherHTML.Replace(NumberFormatInfo.CurrentInfo.CurrencySymbol, "");
        }

        if (Mode == PrintMode.Print)
        {
          //If its a chips swap it means that there are not cash in
          if (OpCode != OperationCode.CHIPS_SWAP)
          {
          _temp_voucher.Save(OperationId, SQLTransaction);
        }
        }

        //If its a chips swap it means that there are not cash in
        if (OpCode != OperationCode.CHIPS_SWAP)
        {
          _voucher_list.Add(_temp_voucher);
        }

      }

      _temp_voucher_draw_cash_desk = CashDeskDrawBusinessLogic.GenerateVoucherDrawCashDesk(VoucherAccountInfo, CashDeskDrawResult, OperationId, OpCode, Mode, SQLTransaction, SplitInformation);

      foreach (VoucherDrawCashDesk voucher in _temp_voucher_draw_cash_desk)
      {
        //If its a chips swap it means that there are not cash in
        if (OpCode != OperationCode.CHIPS_SWAP)
        {
        _voucher_list.Add(voucher);
        }
      }

      //#if DEBUG
      //      VoucherPrizeCouponDraw _temp_draw_voucher;
      //      _temp_draw_voucher = new VoucherPrizeCouponDraw(VoucherAccountInfo, Mode, SQLTransaction);

      //      if (Mode == PrintMode.Print)
      //      {
      //        _temp_draw_voucher.Save(OperationId, SQLTransaction);
      //      }

      //      _voucher_list.Add(_temp_draw_voucher);
      //#endif

      return _voucher_list;
    } // CashIn

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashOut operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName            : Player holder name
    //          - String CardId                : Player account id
    //          - String Trackdata             : Player card trackdata
    //          - Currency Balance             : Total Balance
    //          - TYPE_SPLITS SplitInformation : Split/Company A/B information
    //          - TYPE_CASH_REDEEM CompanyA    : Company/Voucher A parameteres
    //          - TYPE_CASH_REDEEM CompanyB    : Company/Voucher B parameteres
    //          - Int64 OperationId            : Operation id (not need in Preview mode)
    //          - PrintMode Mode               : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashOut(String[] VoucherAccountInfo
                                  , Int64 AccountId
                                  , Currency BalanceBeforeCashOut
                                  , TYPE_SPLITS SplitInformation
                                  , TYPE_CASH_REDEEM CompanyA
                                  , TYPE_CASH_REDEEM CompanyB
                                  , Int64 OperationId
                                  , PrintMode Mode
                                  , List<VoucherValidationNumber> ValidationNumbers
                                  , SqlTransaction Trx
                                  , Boolean IsChipsPurchaseWithCashOut
                                  , Boolean IsApplyTax
                                  , Boolean IsTicketPayment)
    {
      PaymentOrder _pay_order;

      _pay_order = new PaymentOrder();
      return CashOut(VoucherAccountInfo
                    , AccountId
                    , BalanceBeforeCashOut
                    , SplitInformation
                    , CompanyA
                    , CompanyB
                    , OperationId
                    , Mode
                    , ValidationNumbers
                    , _pay_order
                    , OperationCode.NOT_SET
                    , Trx
                    , IsChipsPurchaseWithCashOut
                    , IsApplyTax
                    , IsTicketPayment);
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashOut operation in Form Prize Payout & PrizePayut and Recharge
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName            : Player holder name
    //          - String CardId                : Player account id
    //          - String Trackdata             : Player card trackdata
    //          - Currency Balance             : Total Balance
    //          - TYPE_SPLITS SplitInformation : Split/Company A/B information
    //          - TYPE_CASH_REDEEM CompanyA    : Company/Voucher A parameteres
    //          - TYPE_CASH_REDEEM CompanyB    : Company/Voucher B parameteres
    //          - Int64 OperationId            : Operation id (not need in Preview mode)
    //          - PrintMode Mode               : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashOut(String[] VoucherAccountInfo
                                  , Int64 AccountId
                                  , Currency BalanceBeforeCashOut
                                  , TYPE_SPLITS SplitInformation
                                  , TYPE_CASH_REDEEM CompanyA
                                  , TYPE_CASH_REDEEM CompanyB
                                  , Int64 OperationId
                                  , PrintMode Mode
                                  , List<VoucherValidationNumber> ValidationNumbers
                                  , SqlTransaction Trx
                                  , Boolean IsChipsPurchaseWithCashOut
                                  , Boolean IsApplyTax
                                  , Boolean IsTicketPayment
                                  , OperationCode OperationCode)
    {
      PaymentOrder _pay_order;

      _pay_order = new PaymentOrder();
      return CashOut(VoucherAccountInfo
                    , AccountId
                    , BalanceBeforeCashOut
                    , SplitInformation
                    , CompanyA
                    , CompanyB
                    , OperationId
                    , Mode
                    , ValidationNumbers
                    , _pay_order
                    , OperationCode
                    , Trx
                    , IsChipsPurchaseWithCashOut
                    , IsApplyTax
                    , IsTicketPayment);
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashOut operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName            : Player holder name
    //          - String CardId                : Player account id
    //          - String Trackdata             : Player card trackdata
    //          - Currency Balance             : Total Balance
    //          - TYPE_SPLITS SplitInformation : Split/Company A/B information
    //          - TYPE_CASH_REDEEM CompanyA    : Company/Voucher A parameteres
    //          - TYPE_CASH_REDEEM CompanyB    : Company/Voucher B parameteres
    //          - Int64 OperationId            : Operation id (not need in Preview mode)
    //          - PrintMode Mode               : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashOut(String[] VoucherAccountInfo
                                  , Int64 AccountId
                                  , Currency BalanceBeforeCashOut
                                  , TYPE_SPLITS SplitInformation
                                  , TYPE_CASH_REDEEM CompanyA
                                  , TYPE_CASH_REDEEM CompanyB
                                  , Int64 OperationId
                                  , PrintMode Mode
                                  , List<VoucherValidationNumber> ValidationNumbers
                                  , PaymentOrder PayOrder
                                  , OperationCode OperationCode
                                  , SqlTransaction Trx
                                  , Boolean IsChipsPurchaseWithCashOut
                                  , Boolean IsApplyTaxes
                                  , Boolean IsTicketPayment)
    {
      ArrayList _voucher_list;
      VoucherCashOut _temp_voucher_cashout;
      VoucherCancel _temp_voucher_cancel;
      VoucherCashOutCompanyB _temp_voucher_cashout_company_b;

      _voucher_list = new ArrayList();

      // Pay Order
      Barcode _voucher_number;

      _voucher_number = Barcode.Create(BarcodeType.PayOrder, OperationId);

      // Devolution
      _temp_voucher_cashout = new VoucherCashOut(VoucherAccountInfo
                                               , AccountId
                                               , BalanceBeforeCashOut
                                               , SplitInformation
                                               , CompanyA
                                               , CompanyB
                                               , CASHIER_MOVEMENT.DEV_SPLIT1
                                               , Mode
                                               , ValidationNumbers
                                               , _voucher_number.ExternalCode
                                               , PayOrder.CashPayment
                                               , PayOrder.CheckPayment
                                               , OperationCode
                                               , Trx
                                               , IsChipsPurchaseWithCashOut
                                               , IsApplyTaxes
                                               , IsTicketPayment);

      if (SplitInformation.company_a.hide_currency_symbol &&
         !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
      {
        if (_temp_voucher_cashout.VoucherMode == 3) // CODERE
        {
          // AJQ Always show the currency symbol (The CashOut ticket is not really 'A' it's 'A+B')
        }
        else
        {
          _temp_voucher_cashout.ReplaceCurrencySimbol("");
        }
      }

      if (Mode == PrintMode.Print)
      {
        _temp_voucher_cashout.Save(OperationId, Trx);
      }

      _voucher_list.Add(_temp_voucher_cashout);

      if (SplitInformation.company_b.devolution_pct > 0
       || CompanyB.cancellation > 0)
      {
        // RCI 05-DEC-2012: Multiple cancellation.
        foreach (CancellableOperation _cancellable_operation in CompanyB.cancellable_operations)
        {
          // Don't create cancellation voucher when PromoBalance = 0.
          if (_cancellable_operation.Balance.PromoRedeemable == 0)
          {
            continue;
          }

          // Cancellation of Uso de Sala Voucher.B.Cancel
          _temp_voucher_cancel = new VoucherCancel(VoucherAccountInfo
                                                 , SplitInformation
                                                 , CompanyB
                                                 , _cancellable_operation
                                                 , CASHIER_MOVEMENT.DEV_SPLIT2
                                                 , OperationCode
                                                 , Mode
                                                 , Trx);

          if (SplitInformation.company_b.hide_currency_symbol &&
             !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
          {
            _temp_voucher_cancel.ReplaceCurrencySimbol("");
          }

          if (Mode == PrintMode.Print)
          {
            _temp_voucher_cancel.Save(OperationId, Trx);
          }

          _voucher_list.Add(_temp_voucher_cancel);

        }
      }

      if (Misc.IsCardPlayerToCompanyB() & CompanyB.is_deposit)
      {
        _temp_voucher_cashout_company_b = new VoucherCashOutCompanyB(VoucherAccountInfo
                                               , SplitInformation
                                               , CompanyB
                                               , CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT
                                               , OperationCode
                                               , Mode
                                               , Trx);

        if (SplitInformation.company_b.hide_currency_symbol &&
           !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          _temp_voucher_cashout_company_b.VoucherHTML = _temp_voucher_cashout_company_b.VoucherHTML.Replace(NumberFormatInfo.CurrentInfo.CurrencySymbol, "");
        }

        if (Mode == PrintMode.Print)
        {
          _temp_voucher_cashout_company_b.Save(OperationId, Trx);
        }

        _voucher_list.Add(_temp_voucher_cashout_company_b);
      }


      if (CompanyB.ServiceCharge > 0)
      {
        VoucherCashIn.InputDetails _details = new VoucherCashIn.InputDetails();

        _details.InitialBalance = MultiPromos.PromoBalance.Zero;
        _details.FinalBalance = MultiPromos.PromoBalance.Zero;
        _details.CashIn1 = 0;
        _details.CashIn2 = CompanyB.ServiceCharge;
        _details.CardPrice = 0;
        _details.CashInTaxSplit1 = 0;
        _details.CashInTaxSplit2 = 0;

        VoucherCashIn _voucher;

        _voucher = new VoucherCashIn(VoucherAccountInfo
                                           , AccountId
                                           , SplitInformation
                                           , _details
                                           , CASHIER_MOVEMENT.SERVICE_CHARGE
                                           , OperationCode
                                           , Mode
                                           , Trx);

        if (SplitInformation.company_b.hide_currency_symbol &&
           !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          _voucher.ReplaceCurrencySimbol("");
        }

        if (Mode == PrintMode.Print)
        {
          _voucher.Save(OperationId, Trx);
        }

        _voucher_list.Add(_voucher);
      }

      return _voucher_list;

    }// CashOut

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the promotion lost on CashOut operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName            : Player holder name
    //          - String CardId                : Player account id
    //          - String Trackdata             : Player card trackdata
    //          - Currency Balance             : Total Balance
    //          - TYPE_SPLITS SplitInformation : Split/Company A/B information
    //          - TYPE_CASH_REDEEM CompanyA    : Company/Voucher A parameteres
    //          - TYPE_CASH_REDEEM CompanyB    : Company/Voucher B parameteres
    //          - Int64 OperationId            : Operation id (not need in Preview mode)
    //          - PrintMode Mode               : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList PromoLost(String[] VoucherAccountInfo
                               , Int64 AccountId
                               , TYPE_SPLITS SplitInformation
                                , DataTable Promotions
                                , Boolean RewardTitle
                               , Int64 OperationId
                               , PrintMode Mode
                               , SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      PromoLost(VoucherAccountInfo
                , AccountId
                , SplitInformation
                , true
                , Promotions
                , RewardTitle
                , OperationId
                , Mode
                , SQLTransaction
                , out _voucher_list);
      return _voucher_list;
    }

    public static Boolean PromoLost(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , Boolean SingleTicket
                                   , DataTable Promotions
                                   , Boolean IsReward
                                   , Int64 OperationId
                                   , PrintMode Mode
                                   , SqlTransaction SQLTransaction
                                   , out ArrayList VoucherList)
    {
      return PromoLost(VoucherAccountInfo
                , AccountId
                , SplitInformation
                , SingleTicket
                , Promotions
                , IsReward
                , OperationId
                , Mode
                , OperationCode.NOT_SET
                , null
                , SQLTransaction
                , out VoucherList);
    }


    public static Boolean PromoLost(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , Boolean SingleTicket
                                   , DataTable Promotions
                                   , Boolean IsReward
                                   , Int64 OperationId
                                   , PrintMode Mode
                                   , OperationCode OperationCode
                                   , String PromotionalTitoTicket
                                   , SqlTransaction SQLTransaction
                                   , out ArrayList VoucherList)
    {
      VoucherPromoLost _voucher;
      DataTable[] _promos;
      DataRow _row;
      DataTable _copy_of_promotions;
      Boolean _hide_prize_cupon_on_promotion;
      Boolean _hide_voucher_hide_cash_desk_draw_winner;
      Boolean _hide_voucher_hide_cash_desk_draw_loser;
      Boolean _hide_voucher_cash_desk_draw_not_selected;

      // RRB 19-MAR-2013: Hide prize coupon
      _hide_prize_cupon_on_promotion = GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HidePrizeCoupon", false);
      _hide_voucher_hide_cash_desk_draw_winner = GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HideCashDeskDrawWinner", true);
      _hide_voucher_hide_cash_desk_draw_loser = GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HideCashDeskDrawLoser", true);

      // DHA 21-JAN-2015: Hide Cash desk draw no participe promotion
      _hide_voucher_cash_desk_draw_not_selected = GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HideCashDeskDrawNotSelected", false);

      VoucherList = new ArrayList();

      try
      {

        if (!Promotions.Columns.Contains("ACP_TICKET_FOOTER"))
        {
          Promotions.Columns.Add("ACP_TICKET_FOOTER", Type.GetType("System.String"));
        }
        _promos = new DataTable[1];


        _copy_of_promotions = Promotions.Copy();

        _copy_of_promotions.AcceptChanges();
        // If found prize coupon delete datarow
        foreach (DataRow _dr in _copy_of_promotions.Rows)
        {
          switch ((Promotion.PROMOTION_TYPE)_dr["ACP_PROMO_TYPE"])
          {
            case Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON:
              if (_hide_prize_cupon_on_promotion)
              {
                _dr.Delete();
              }
              break;
            // DHA 21-JAN-2015: Hide Cash desk draw no participe promotion
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO:
              if (_hide_voucher_cash_desk_draw_not_selected)
              {
                _dr.Delete();
              }
              break;
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND:
              if (_hide_voucher_hide_cash_desk_draw_winner)
              {
                _dr.Delete();
              }
              break;
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND:
              if (_hide_voucher_hide_cash_desk_draw_loser)
              {
                _dr.Delete();
              }
              break;
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN:
              _dr.Delete();
              break;

            default:
              break;
          }
        }
        _copy_of_promotions.AcceptChanges();

        // Avoid create empty voucher
        if (_copy_of_promotions.Rows.Count == 0)
        {
          return true;
        }

        _promos[0] = _copy_of_promotions;

        if (!SingleTicket)
        {
          _promos = new DataTable[_copy_of_promotions.Rows.Count];
        }
        for (int _idx = 0; _idx < _copy_of_promotions.Rows.Count; _idx++)
        {
          String _promo_footer;
          _row = _copy_of_promotions.Rows[_idx];
          if (!GetPromoFooter((long)_row["ACP_PROMO_ID"], SQLTransaction, out _promo_footer))
          {
            return false;
          }
          else
          {
            _row["ACP_TICKET_FOOTER"] = _promo_footer;
          }
          if (!SingleTicket)
          {
            _promos[_idx] = _copy_of_promotions.Clone();
            _promos[_idx].ImportRow(_row);
          }

        }

        foreach (DataTable _table in _promos)
        {
          // Voucher
          _voucher = new VoucherPromoLost(VoucherAccountInfo
                                         , AccountId
                                         , SplitInformation
                                         , Mode
                                         , _table
                                         , IsReward
                                         , SingleTicket
                                         , OperationCode
                                         , PromotionalTitoTicket
                                         , SQLTransaction);

          if ((SplitInformation.company_a.hide_currency_symbol || GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HideCurrencySymbol")) &&
             !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
          {
            _voucher.ReplaceCurrencySimbol("");
          }

          if (Mode == PrintMode.Print)
          {
            _voucher.Save(OperationId, SQLTransaction);
          }

          VoucherList.Add(_voucher);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // PromoLost


    private static Boolean GetPromoFooter(long PromoId, SqlTransaction SQLTransaction, out String PromoFooter)
    {

      StringBuilder _sb;

      PromoFooter = "";
      try
      {

        _sb = new StringBuilder();
        _sb.Length = 0;
        _sb.AppendLine("SELECT   PM_TICKET_FOOTER                    ");
        _sb.AppendLine("  FROM   PROMOTIONS                          ");
        _sb.AppendLine("  WHERE  (PM_PROMOTION_ID = @pPromoId)       ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SQLTransaction.Connection, SQLTransaction))
        {
          _cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromoId;
          Object _ticket_footer = _cmd.ExecuteScalar();
          PromoFooter = _ticket_footer == DBNull.Value ? "" : (String)_ticket_footer;
        }
      }
      catch (Exception)
      {
        return false;
      }
      return true;

    } // PromoLost

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Ticket printing operation 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - MrvFontPath: Full path to MRV Font
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public static ArrayList TicketPrintOut(Int64 OperationId,
                                           String[] VoucherAccountInfo,
                                           Int64 TicketId,
                                           String ValidationNumber,
                                           Currency Amount,
                                           DateTime CreatedDateTime,
                                           DateTime ExpirationDateTime,
                                           Number PromotionId,
                                           String MrvFontPath,
                                           PrintMode Mode,
                                           SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherTitoTicketPrintout _temp_voucher;

      _voucher_list = new ArrayList();

      // Gift request
      _temp_voucher = new VoucherTitoTicketPrintout(VoucherAccountInfo,
                                                    TicketId,
                                                    ValidationNumber,
                                                    Amount,
                                                    CreatedDateTime,
                                                    ExpirationDateTime,
                                                    PromotionId,
                                                    MrvFontPath,
                                                    Mode,
                                                    SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // TicketPrintOut

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Ticket Print operation; is a second version
    //           All parameters are stored in instance of class SmartParams, that are
    //           accesibles by name
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - TicketId: ID of Ticket to be printed
    //          - ValidationNumber: Ticket validation number
    //          - Amount: amount of ticket
    //          - CreatedDateTime: creation date
    //          - ExpirationDateTime: ticket expiration date
    //          - PromotionId: ID of promotion, if exists
    //          - MrvFontPath: Full path to MRV Font
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - ArrayList of Voucher properties
    //
    //public static ArrayList TicketPrintOut(SmartParams Params, SqlTransaction SQLTransaction)
    //{
    //  Int64 _operation_Id;
    //  PrintMode _mode;
    //  ArrayList _voucher_list;
    //  VoucherTitoTicketPrintout _temp_voucher;

    //  // Gift request
    //  _mode = (PrintMode)Params.Get("PrintMode");
    //  _temp_voucher = new VoucherTitoTicketPrintout(Params, _mode, SQLTransaction);
    //  if (_mode == PrintMode.Print)
    //  {
    //    _operation_Id = Params.AsLong("OperationId");
    //    _temp_voucher.Save(_operation_Id, SQLTransaction);
    //  }

    //  _voucher_list = new ArrayList();
    //  _voucher_list.Add(_temp_voucher);

    //  return _voucher_list;
    //} // TicketPrintOut

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Gift Request operation 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public static ArrayList GiftRequest(Int64 OperationId,
                                         String[] VoucherAccountInfo,
                                         Points InitialPoints,
                                         Points SpentPoints,
                                         GIFT_TYPE GiftType,
                                         String GiftNumber,
                                         String GiftName,
                                         Currency NotRedeemableCredits,
                                         Decimal GiftUnits,
                                         DateTime ExpirationDateTime,
                                         PrintMode Mode,
                                         SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherGiftRequest _temp_voucher;

      _voucher_list = new ArrayList();

      // Gift request
      _temp_voucher = new VoucherGiftRequest(VoucherAccountInfo,
                                              InitialPoints,
                                              SpentPoints,
                                              GiftType,
                                              GiftName,
                                              GiftNumber,
                                              NotRedeemableCredits,
                                              GiftUnits,
                                              ExpirationDateTime,
                                              Mode,
                                              SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // GiftRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Gift Delivery operation 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public static ArrayList GiftDelivery(Int64 OperationId,
                                          String[] VoucherAccountInfo,
                                          String GiftNumber,
                                          String GiftName,
                                          PrintMode Mode,
                                          SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherGiftDelivery _temp_voucher;

      _voucher_list = new ArrayList();

      // Gift request
      _temp_voucher = new VoucherGiftDelivery(VoucherAccountInfo,
                                               GiftNumber,
                                               GiftName,
                                               Mode,
                                               SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // GiftRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Gift cancellation operation 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public static ArrayList GiftCancellation(Int64 OperationId,
                                             String[] VoucherAccountInfo,
                                             Points InitialPoints,
                                             Points SpentPoints,
                                             GIFT_TYPE GiftType,
                                             String GiftNumber,
                                             String GiftName,
                                             Currency NotRedeemableCredits,
                                             Decimal GiftUnits,
                                             PrintMode Mode,
                                             SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherGiftCancellation _temp_voucher;

      _voucher_list = new ArrayList();

      // Gift cancellation
      _temp_voucher = new VoucherGiftCancellation(VoucherAccountInfo,
                                                  InitialPoints,
                                                  SpentPoints,
                                                  GiftType,
                                                  GiftNumber,
                                                  GiftName,
                                                  NotRedeemableCredits,
                                                  GiftUnits,
                                                  Mode,
                                                  SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // GiftCancellation

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the Generate PIN operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName     : Player holder name
    //          - String CardId         : Player account id
    //          - String Trackdata      : Player card trackdata
    //          - Int64 OperationId     : Operation id (not need in Preview mode)
    //          - PrintMode Mode        : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList GeneratePIN(String[] VoucherAccountInfo
                                      , Int64 AccountId
                                      , TYPE_SPLITS SplitInformation
                                      , Int64 OperationId
                                      , String NewPin
                                      , PrintMode Mode
                                      , SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherGeneratePIN _temp_voucher;

      _voucher_list = new ArrayList();

      // Voucher for Cash In Deposit
      _temp_voucher = new VoucherGeneratePIN(VoucherAccountInfo
                                            , AccountId
                                            , SplitInformation
                                            , NewPin
                                            , Mode
                                            , SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // GeneratePIN

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the Generate PIN operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName     : Player holder name
    //          - String CardId         : Player account id
    //          - String Trackdata      : Player card trackdata
    //          - Int64 OperationId     : Operation id (not need in Preview mode)
    //          - PrintMode Mode        : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList RechargesSummary(String[] VoucherAccountInfo,
                                             String[] NotesList,
                                             Currency TotalRecharges,
                                             Int64 AccountId,
                                             Int64 OperationId,
                                             PrintMode Mode,
                                             SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherRechargesSummary _temp_voucher;

      _voucher_list = new ArrayList();

      // Voucher for Recharges Summary
      _temp_voucher = new VoucherRechargesSummary(VoucherAccountInfo,
                                                  NotesList,
                                                  TotalRecharges,
                                                  AccountId,
                                                  Mode,
                                                  SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // RechargesSummary

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Currency Exchange Operation
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    public static ArrayList CurrencyExchange(String[] VoucherAccountInfo
                               , Int64 AccountId
                               , TYPE_SPLITS SplitInformation
                               , CurrencyExchangeResult ExchangeResult
                               , CurrencyExchangeType ExchangeType
                               , String NationalCurrency
                               , Int64 OperationId
                               , PrintMode Mode
                               , SqlTransaction Trx)
    {
      return CurrencyExchange(VoucherAccountInfo, AccountId, SplitInformation, ExchangeResult, ExchangeType, NationalCurrency, OperationId, OperationCode.CASH_IN, Mode, Trx);
    }

    public static ArrayList CurrencyExchange(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , CurrencyExchangeResult ExchangeResult
                                   , CurrencyExchangeType ExchangeType
                                   , String NationalCurrency
                                   , Int64 OperationId
                                   , OperationCode Operation
                                   , PrintMode Mode
                                   , SqlTransaction Trx)
    {
      ArrayList _voucher_list;
      VoucherCardAddCurrencyExchange _temp_voucher;
      Boolean _hide_currency_exchange;

      _voucher_list = new ArrayList();

      switch (ExchangeType)
      {
        case CurrencyExchangeType.CARD:
          switch (Operation)
          {
            case OperationCode.CASH_IN:
              _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.BankCard.HideVoucher");
              break;
            case OperationCode.CASH_ADVANCE:
              _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.CashAdvance.BankCard.HideVoucher");
              break;
            default:
              _hide_currency_exchange = false;
              break;
          }
          break;

        case CurrencyExchangeType.CHECK:
          switch (Operation)
          {
            case OperationCode.CASH_IN:
              _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.Check.HideVoucher");
              break;
            case OperationCode.CASH_ADVANCE:
              _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.CashAdvance.Check.HideVoucher");
              break;
            default:
              _hide_currency_exchange = false;
              break;
          }
          break;

        case CurrencyExchangeType.CURRENCY:
          _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.Currency.HideVoucher");
          break;

        default:
          _hide_currency_exchange = false;
          break;
      }

      if (_hide_currency_exchange)
      {
        return _voucher_list;
      }

      // Currency Exchange
      _temp_voucher = new VoucherCardAddCurrencyExchange(VoucherAccountInfo
                                                         , AccountId
                                                         , SplitInformation
                                                         , ExchangeResult
                                                         , ExchangeType
                                                         , NationalCurrency
                                                         , Operation
                                                         , Mode
                                                         , false
                                                         , Trx);

      // DLL & RCI 21-AUG-2013: Hide currency (if configured) for bank card recharge.
      if (SplitInformation.company_a.hide_currency_symbol &&
          !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
      {
        _temp_voucher.ReplaceCurrencySimbol("");
      }

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, Trx);
      }

      _voucher_list.Add(_temp_voucher);

      // OPC: Add comission voucher just for Cash advance.
      if (Misc.IsCommissionToCompanyBEnabled() && ExchangeResult.Comission >= 0 && Operation == OperationCode.CASH_ADVANCE)
      {
        VoucherCardAddCurrencyExchange _temp;

        _temp = new VoucherCardAddCurrencyExchange(VoucherAccountInfo
                                                   , AccountId
                                                   , SplitInformation
                                                   , ExchangeResult
                                                   , ExchangeType
                                                   , NationalCurrency
                                                   , Operation
                                                   , Mode
                                                   , true
                                                   , Trx);

        if (SplitInformation.company_b.hide_currency_symbol &&
          !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          _temp.ReplaceCurrencySimbol("");
        }

        _voucher_list.Add(_temp);

        if (Mode == PrintMode.Print)
        {
          _temp.Save(OperationId, Trx);
        }
      }

      return _voucher_list;
    } // CurrencyExchange

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the Undo Operations
    //
    //  PARAMS:
    //      - INPUT:
    //              - OperationId
    //              - UndoOperationId
    //              - Mode
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //       - ArrayList with undo operation vouchers
    // 
    public static ArrayList UndoOperation(Int64 OperationId
                                        , Int64 UndoOperationId
                                        , OperationCode OpCode
                                        , PrintMode Mode
                                        , SqlTransaction Trx)
    {
      DataTable _dt_vouchers;
      Voucher _temp_voucher;
      ArrayList _voucher_list;
      String _voucher_html;

      _voucher_list = new ArrayList();
      _dt_vouchers = VoucherManager.GetVouchersFromId(VoucherSource.FROM_OPERATION, OperationId, Trx);

      foreach (DataRow _voucher in _dt_vouchers.Rows)
      {
        _voucher_html = (String)_voucher["CV_HTML"];
        _temp_voucher = new VoucherUndoOperation(_voucher_html, OpCode, Mode, Trx);
        _temp_voucher.SetValue("CV_TYPE", _voucher["CV_TYPE"]);
        _temp_voucher.SetValue("CV_SEQUENCE", _voucher["CV_SEQUENCE"]);
        _temp_voucher.SetValue("CV_M01_DEV", (_voucher["CV_M01_DEV"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_DEV"]));
        _temp_voucher.SetValue("CV_M01_BASE", (_voucher["CV_M01_BASE"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_BASE"]));
        _temp_voucher.SetValue("CV_M01_TAX1", (_voucher["CV_M01_TAX1"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_TAX1"]));
        _temp_voucher.SetValue("CV_M01_TAX2", (_voucher["CV_M01_TAX2"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_TAX2"]));
        _temp_voucher.SetValue("CV_M01_FINAL", (_voucher["CV_M01_FINAL"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_FINAL"]));

        if (Mode == PrintMode.Print)
        {
          _temp_voucher.Save(UndoOperationId, Trx);
        }
        _voucher_list.Add(_temp_voucher);
      }// foreach

      return _voucher_list;
    } // UndoOperation


    public static ArrayList PointToCardReplacement(Int64 OperationId,
                                                   String[] VoucherAccountInfo,
                                                   Points InitialPoints,
                                                   Points SpentPoints,
                                                   PrintMode Mode,
                                                   SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherPointToCardReplacement _temp_voucher;

      _voucher_list = new ArrayList();

      // PointToCardReplacement
      _temp_voucher = new VoucherPointToCardReplacement(VoucherAccountInfo,
                                                        InitialPoints,
                                                        SpentPoints,
                                                        Mode,
                                                        SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // PointToCardReplacement

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the Card player replacement
    //
    //  PARAMS:
    //      - INPUT:
    //              - OperationId
    //              - ParamsReplacement
    //              - OpCode
    //              - Mode
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //       - ArrayList with replacement voucher and if it's required the voucher with company b comissions
    // 
    public static ArrayList CardPlayerReplacement(Int64 OperationId
                                                , TYPE_CARD_REPLACEMENT ParamsReplacement
                                                , OperationCode OpCode
                                                , PrintMode Mode
                                                , SqlTransaction Trx
                                                , Boolean CardPaid = true)
    {
      ArrayList _voucher_list;
      VoucherCashIn _temp_voucher_cash_in;
      VoucherCardReplacement _temp_voucher_replacement;
      VoucherCashIn.InputDetails _details;
      CASHIER_MOVEMENT _cashier_movement;

      _voucher_list = new ArrayList();

      // Voucher Card Replacement 
      // RAB 24-MAR-2016: Bug 10745: Add input parameter CardPaid
      _temp_voucher_replacement = new VoucherCardReplacement(Mode, ParamsReplacement, Trx, CardPaid);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher_replacement.Save(OperationId, Trx);
      }

      _voucher_list.Add(_temp_voucher_replacement);

      // Only when card player is send to company B and comission greather than 0
      // Voucher Cash IN comission company B
      if (Misc.IsCardPlayerToCompanyB() & ParamsReplacement.ExchangeResult.Comission > 0)
      {
        _details = new VoucherCashIn.InputDetails();
        _details.ExchangeCommission = ParamsReplacement.ExchangeResult.Comission;
        _details.InitialBalance = MultiPromos.PromoBalance.Zero;
        _details.FinalBalance = MultiPromos.PromoBalance.Zero;


        switch (ParamsReplacement.ExchangeResult.InType)
        {
          case CurrencyExchangeType.CARD:
            _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2;
            break;
          case CurrencyExchangeType.CHECK:
            _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2;
            break;
          case CurrencyExchangeType.CURRENCY:
            _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2;
            break;
          default:
            _cashier_movement = CASHIER_MOVEMENT.CARD_REPLACEMENT;
            break;
        }



        _temp_voucher_cash_in = new VoucherCashIn(ParamsReplacement.AccountInfo
                                                 , ParamsReplacement.account_id
                                                 , ParamsReplacement.Splits
                                                 , _details
                                                 , _cashier_movement
                                                 , OpCode
                                                 , Mode
                                                 , Trx);
        if (Mode == PrintMode.Print)
        {
          _temp_voucher_cash_in.Save(OperationId, Trx);
        }

        _voucher_list.Add(_temp_voucher_cash_in);
      }



      return _voucher_list;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashIn operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName     : Player holder name
    //          - String CardId         : Player account id
    //          - String Trackdata      : Player card trackdata
    //          - Currency CurrentBalance : Actual Balance of the card
    //          - Currency CashIn1      : Amount to pay as Deposit
    //          - Currency CashIn2      : Amount to pay as Uso de sala
    //          - Int64 OperationId     : Operation id (not need in Preview mode)
    //          - PrintMode Mode        : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList VoucherReception ( String[] VoucherAccountInfo
                                             , Int64 AccountId
                                             , CashierVoucherType VoucherType
                                             , Currency[] Amount
                                             , CurrencyExchangeResult ExchangeResult
                                             , String Coupon
                                             , Int32 ValidGamingDays
                                             , Boolean Agreement
                                             , PrintMode Mode
                                             , SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherReception _temp_voucher_reception;
      VoucherReceptionLOPD _temp_voucher_reception_lopd;

      _voucher_list = new ArrayList();
      _temp_voucher_reception_lopd = null;

      // Voucher for reception
      _temp_voucher_reception = new VoucherReception(VoucherAccountInfo, VoucherType, AccountId, Amount, ExchangeResult, Coupon,ValidGamingDays, Mode, SQLTransaction);
      if (Mode == PrintMode.Print)
      {
        _temp_voucher_reception.Save(0, SQLTransaction);
      }
      _voucher_list.Add(_temp_voucher_reception);

      // Voucher for reception LOPD
      if (Agreement)
      {
        _temp_voucher_reception_lopd = new VoucherReceptionLOPD(VoucherAccountInfo, VoucherType, AccountId, Mode, SQLTransaction);
        if (Mode == PrintMode.Print)
        {          
          _temp_voucher_reception_lopd.Save(0, SQLTransaction);
        }
        _voucher_list.Add(_temp_voucher_reception_lopd);
    }

      return _voucher_list;
    } // VoucherReception

  } // VoucherBuilder

  public class VoucherCashIn : Voucher
  {

    #region Clase

    public class InputDetails
    {
      public MultiPromos.PromoBalance InitialBalance;
      public MultiPromos.PromoBalance FinalBalance;
      public Currency CashIn1;
      public Currency CashIn2;
      public Currency CardPrice;
      public Currency CashInTaxSplit1;
      public Currency CashInTaxSplit2;
      public Currency CashInTaxCustody; // LTC 14-ABR-2016
      public Currency TotalTaxSplit2;
      public Currency TotalBaseTaxSplit2;
      public CurrencyExchangeType PaymentType;
      public Currency ExchangeCommission;
      public Boolean WasForeingCurrency;
      public Boolean IsIntegratedChipsSale;
    }

    #endregion

    #region Constructor

    public VoucherCashIn(String[] VoucherAccountInfo
                       , Int64 AccountId
                       , TYPE_SPLITS Splits
                       , InputDetails Details
                       , CASHIER_MOVEMENT MovementType
                       , OperationCode OperationCode
                       , PrintMode Mode
                       , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {

      Currency _cash_in_split1;
      Currency _cash_in_split2;
      Int32 _days;
      String _str_aux;
      Currency _cur_aux;
      Currency _promos_without_prize_coupon;
      Currency _total_promos;
      Boolean _hide_total;
      MultiPromos.PromoBalance _increment_bal;
      Boolean _add_currency_in_letters;
      Currency _cash_in_tax_split1;
      Currency _cash_in_tax_custody;

      _increment_bal = Details.FinalBalance - Details.InitialBalance;

      _cash_in_split1 = 0;
      _cash_in_split2 = 0;
      _cash_in_tax_split1 = 0;
      _cash_in_tax_custody = 0; // LTC 13-ABR-2016
      _str_aux = "";
      _hide_total = false;
      _add_currency_in_letters = false;

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.

      // 1. Load HTML structure.
      switch (MovementType)
      {
        case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
          LoadVoucher("Voucher.A.CashIn");
          LoadHeader(Splits.company_a.header);
          LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

          //DHA 23-JUN-2015 Added custom parameters for cashin chips operations
          if (Details.IsIntegratedChipsSale)
          {
            AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("GamingTables", "IntegratedChipsSales.Split.A.CashInVoucherTitle", Splits.company_a.cash_in_voucher_title));
            AddString("VOUCHER_CONCEPT", GeneralParam.GetString("GamingTables", "IntegratedChipsSales.Split.A.Name", Splits.company_a.name));
          }
          else
          {
            AddString("VOUCHER_SPLIT_NAME", Splits.company_a.cash_in_voucher_title);
            AddString("VOUCHER_CONCEPT", Splits.company_a.name);
          }
          _cash_in_split1 = Details.CashIn1;
          _cash_in_split2 = Details.CashIn2;

          VoucherSequenceId = SequenceId.VouchersSplitA;
          SetValue("CV_TYPE", (Int32)CashierVoucherType.CashIn_A);

          _hide_total = Splits.company_a.hide_total;
          _add_currency_in_letters = Splits.company_a.total_in_letters;

          if (Details.CashInTaxSplit1 > 0)
          {
            _str_aux = GeneralParam.GetString("Cashier", "CashInTax.VoucherText", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX")) + ":";
            AddString("STR_VOUCHER_CASH_IN_TAX", _str_aux);
            // Logrand mode: Only one voucher (including A & B)
            _cash_in_tax_split1 = (VoucherMode == 5) ? Details.CashInTaxSplit1.SqlMoney + Details.CashInTaxSplit2.SqlMoney : Details.CashInTaxSplit1.SqlMoney;
            AddCurrency("VOUCHER_CASH_IN_TAX", _cash_in_tax_split1);
          }
          else
          {
            AddString("STR_VOUCHER_CASH_IN_TAX", "");
            AddString("VOUCHER_CASH_IN_TAX", "");
          }

          break;

        case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", Splits.company_b.cash_in_voucher_title);
          AddString("VOUCHER_CONCEPT", Splits.company_b.name);
          SetValue("CV_TYPE", (Int32)CashierVoucherType.CashIn_B);
          break;

        case CASHIER_MOVEMENT.SERVICE_CHARGE:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", Splits.company_b.cash_in_voucher_title);
          AddString("VOUCHER_CONCEPT", GeneralParam.Value("Cashier.ServiceCharge", "Text"));
          SetValue("CV_TYPE", (Int32)CashierVoucherType.ServiceCharge_B);
          break;

        case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.BankCard.VoucherTitle", Splits.company_b.cash_in_voucher_title));
          AddString("VOUCHER_CONCEPT", Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION"));
          SetValue("CV_TYPE", (Int32)CashierVoucherType.Commission_B);
          break;

        case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Check.VoucherTitle", Splits.company_b.cash_in_voucher_title));
          AddString("VOUCHER_CONCEPT", Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION"));
          SetValue("CV_TYPE", (Int32)CashierVoucherType.CheckCommissionB);
          break;

        case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Currency.VoucherTitle", Splits.company_b.cash_in_voucher_title));
          AddString("VOUCHER_CONCEPT", Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION"));
          SetValue("CV_TYPE", (Int32)CashierVoucherType.ExchangeCommissionB);
          break;

        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", Splits.company_b.cash_in_voucher_title);
          AddString("VOUCHER_CONCEPT", WSI.Common.Misc.GetCardPlayerConceptName());
          SetValue("CV_TYPE", (Int32)CashierVoucherType.CardDepositIn_B);
          break;

        //case new

        default:
          {
            Log.Error("VoucherCashIn. MovementType not valid: " + MovementType.ToString());
            return;
          }
      }

      if (MovementType != CASHIER_MOVEMENT.CASH_IN_SPLIT1)
      {
        if (MovementType == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2 ||
            MovementType == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2 ||
            MovementType == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2)
        {
          _cash_in_split1 = Details.ExchangeCommission;

          VoucherSequenceId = SequenceId.VouchersSplitB;

          _hide_total = false;
          _add_currency_in_letters = true;

          AddString("STR_VOUCHER_CASH_IN_TAX", "");
          AddString("VOUCHER_CASH_IN_TAX", "");
        }
        else
        {
          _cash_in_split1 = Details.CashIn2;
          _cash_in_tax_split1 = Details.CashInTaxSplit2;

          VoucherSequenceId = SequenceId.VouchersSplitB;

          _hide_total = Splits.company_b.hide_total;
          _add_currency_in_letters = Splits.company_b.total_in_letters;

          if (Details.CashInTaxSplit2 > 0 && MovementType == CASHIER_MOVEMENT.CASH_IN_SPLIT2)
          {
            _str_aux = GeneralParam.GetString("Cashier", "CashInTax.VoucherText", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX")) + ":";
            AddString("STR_VOUCHER_CASH_IN_TAX", _str_aux);
            AddCurrency("VOUCHER_CASH_IN_TAX", Details.CashInTaxSplit2);
          }
          else
          {
            AddString("STR_VOUCHER_CASH_IN_TAX", "");
            AddString("VOUCHER_CASH_IN_TAX", "");
          }
        }
      }

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      _total_promos = _increment_bal.Balance.PromoRedeemable + _increment_bal.Balance.PromoNotRedeemable;

      // RRB 04-OCT-2012 Add VoucherMode 2 (Miravalle) 
      // Now have promotion part B and promos without prizecoupon (after promotion part B and total promos) TODO: Move to switch with new variable
      if (Splits.prize_coupon || VoucherMode == 2)
      {
        _promos_without_prize_coupon = _total_promos - Details.CashIn2;
      }
      else
      {
        _promos_without_prize_coupon = _total_promos;
      }

      if (Splits.promo_and_coupon)
      {
        _cur_aux = _total_promos;
        _str_aux = Splits.text_promo_and_coupon;
        if (String.IsNullOrEmpty(_str_aux))
        {
          _str_aux = Resource.String("STR_VOUCHER_CARD_ADD_PROMOANDCOUPON");
        }
        SetParameterValue("@VOUCHER_CARD_ADD_PROMOANDCOUPON",
            "<tr>" +
              "<td align=\"left\"  width=\"50%\">" + _str_aux + ":</td>" +
              "<td align=\"right\" width=\"50%\">" + _cur_aux.ToString() + "</td>" +
            "</tr>");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_ADD_PROMOANDCOUPON", "");
      }

      String _points_awarded;

      _points_awarded = "";

      if (_increment_bal.Points > 0)
      {
        String _label_points;
        _label_points = "";

        _points_awarded += "<tr>";
        _points_awarded += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\"></td>";
        _points_awarded += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\"></td>";
        _points_awarded += "</tr>";

        _points_awarded += "<tr>";
        _points_awarded += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\">#LABEL_POINTS_AWARDED#</td>";
        _points_awarded += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\">#POINTS#</td>";
        _points_awarded += "</tr>";

        _points_awarded += "<tr>";
        _points_awarded += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\"></td>";
        _points_awarded += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\"></td>";
        _points_awarded += "</tr>";

        _label_points = Resource.String("STR_VOUCHER_CARD_ADD_POINTS_AWARDED");

        _points_awarded = _points_awarded.Replace("#LABEL_POINTS_AWARDED#", _label_points);
        _points_awarded = _points_awarded.Replace("#POINTS#", ((Points)_increment_bal.Points).ToString());
      }

      SetParameterValue("@VOUCHER_CARD_ADD_POINTS_AWARDED", _points_awarded);


      if (Splits.promo_and_coupon || !Splits.prize_coupon || Details.CashIn2 <= 0)
      {
        SetParameterValue("@VOUCHER_CARD_ADD_CARD_PRIZE_COUPON", "");
      }
      else if (Splits.prize_coupon)
      {
        _str_aux = Splits.prize_coupon_text;
        if (String.IsNullOrEmpty(_str_aux))
        {
          _str_aux = Resource.String("STR_VOUCHER_CARD_ADD_CARD_PRIZE_COUPON");
        }

        // DHA 23-JUN-2015 Added custom parameters for cashin chips operations
        // DDM & RCI 28-AUG-2015:  Bug-4027
        if (Details.IsIntegratedChipsSale)
        {
          _str_aux = GeneralParam.GetString("GamingTables", "IntegratedChipsSales.Voucher.Promotion.Text", _str_aux);
        }

        SetParameterValue("@VOUCHER_CARD_ADD_CARD_PRIZE_COUPON",
            "<tr>" +
              "<td align=\"left\"  width=\"50%\">" + _str_aux + ":</td>" +
              "<td align=\"right\" width=\"50%\">" + Details.CashIn2 + "</td>" +
            "</tr>");
      }

      if (Splits.promo_and_coupon || Splits.hide_promo || _promos_without_prize_coupon == 0)
      {
        SetParameterValue("@VOUCHER_CARD_ADD_PROMOTION", "");
      }
      else
      {

        _str_aux = Splits.text_promo;

        //DHA 23-JUN-2015 Added custom parameters for cashin chips operations
        if (Details.IsIntegratedChipsSale)
        {
          _str_aux = GeneralParam.GetString("GamingTables", "IntegratedChipsSales.Voucher.Promotion.Text", _str_aux);
        }

        if (String.IsNullOrEmpty(_str_aux))
        {
          _str_aux = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION");
        }

        SetParameterValue("@VOUCHER_CARD_ADD_PROMOTION",
            "<tr>" +
              "<td align=\"left\" width=\"50%\">" + _str_aux + ":</td>" +
              "<td align=\"right\" width=\"50%\">" + _promos_without_prize_coupon.ToString() + "</td>" +
            "</tr>");
      }


      if (MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN)
      {
        if (Misc.IsCardPlayerToCompanyB())
        {
          AddString("VOUCHER_CONCEPT", Misc.GetCardPlayerConceptName());
          AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", Details.CardPrice);
        }
      }
      else
      {
        if (Misc.IsCardPlayerToCompanyB())
        {
          SetParameterValue("@VOUCHER_CARD_ADD_CARD", "");
        }
        else
        {
          SetParameterValue("@VOUCHER_CARD_ADD_CARD",
                            "<tr>" +
                              "<td align=\"left\"  width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CARD_NAME:</td>" +
                              "<td align=\"right\" width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CARD_DEPOSIT</td>" +
                            "</tr>");
          AddCurrency("VOUCHER_CARD_ADD_CARD_DEPOSIT", Details.CardPrice);
          AddString("VOUCHER_CARD_ADD_CARD_NAME", Misc.GetCardPlayerConceptName());
        }
      }

      AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", _cash_in_split1);

      // RCI 04-JAN-2012: Optional, hide total.
      if (_hide_total)
      {
        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT", "");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT",
            "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
              "<tr>" +
                "<td align=\"center\">TOTAL</td>" +
              "</tr>" +
              "<tr>" +
                "<td align=\"center\"><B>@VOUCHER_CARD_ADD_TOTAL_AMOUNT</B></td>" +
              "</tr>" +
            "</table>" +
            "<hr noshade size=\"4\">");
      }
      if (VoucherMode == 5)
      {
        if (Misc.IsCardPlayerToCompanyB())
        {
          AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", _cash_in_split1 + _cash_in_split2 + Details.CashInTaxSplit1 + Details.CashInTaxSplit2, _add_currency_in_letters);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", _cash_in_split1 + _cash_in_split2 + Details.CardPrice + Details.CashInTaxSplit1 + Details.CashInTaxSplit2, _add_currency_in_letters);
        }
      }
      else
      {
        if (Misc.IsCardPlayerToCompanyB() &&
         !(MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN))
        {
          AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", _cash_in_split1 + _cash_in_tax_split1, _add_currency_in_letters);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", _cash_in_split1 + Details.CardPrice + _cash_in_tax_split1, _add_currency_in_letters);
        }
      }
      // RCI 17-MAY-2011: Mode 01: Palacio de los N�meros
      // RCI 03-JUN-2011: Mode 02: Miravalle

      // JML save data for all Modes
      //switch (VoucherMode)
      //{
      //  case 1: // Palacio de los N�meros
      //    {
      Currency _ini_redeemable_balance;
      Currency _fin_redeemable_balance;

      // TODO: Revisar. Abans hi havia aix�... no entenc perqu� es resta CashIn1 i es suma CashIn2 !!!!!!!!!
      //_fin_redeemable_balance = FinalRedeemableBalance;
      //_ini_redeemable_balance = FinalRedeemableBalance - CashIn1 + CashIn2;

      _ini_redeemable_balance = Details.InitialBalance.Balance.TotalRedeemable;
      _fin_redeemable_balance = Details.InitialBalance.Balance.TotalRedeemable + (Details.CashIn1 + Details.CashIn2);

      AddString("VOUCHER_CARD_ACCOUNT_ID", AccountId.ToString("000000"));
      AddCurrency("VOUCHER_CARD_ADD_AMOUNT_BALANCE_INITIAL", _ini_redeemable_balance);
      AddCurrency("VOUCHER_CARD_ADD_AMOUNT_BALANCE_FINAL", _fin_redeemable_balance);
      AddCurrency("VOUCHER_CARD_ADD_AMOUNT_PART_B", Details.CashIn2);
      AddCurrencyInLetters("VOUCHER_CARD_ADD_LETTERS_TOTAL_B_AMOUNT", Details.CashIn2);

      if (!Int32.TryParse(GeneralParam.Value("Cashier", "CreditsExpireAfterDays"), out _days))
      {
        _days = 0;
      }

      if (_days == 0)
      {
        AddString("VOUCHER_CARD_ADD_EXPIRATION_DATE", "---");
      }
      else
      {
        AddString("VOUCHER_CARD_ADD_EXPIRATION_DATE", Format.CustomFormatDateTime(VoucherDateTime.AddDays(_days), false));
      }

      // Set necessary values for reports.
      switch (VoucherSequenceId)
      {
        case SequenceId.VouchersSplitA:
        default:
          SetValue("CV_M01_DEV", Details.CashIn1);
          SetValue("CV_M01_BASE", Details.CashIn2);
          SetValue("CV_M01_FINAL", _fin_redeemable_balance);
          break;

        case SequenceId.VouchersSplitB:
          {
            Currency _base_b;
            Currency _tax_b;

            if (MovementType == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2)
            {
              _base_b = Details.ExchangeCommission;
              _tax_b = Math.Round(_base_b * Splits.company_b.tax_pct / 100, 2, MidpointRounding.AwayFromZero);
              _base_b = _base_b - _tax_b;
            }
            else
            {
              if (MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN)
              {
                _base_b = Math.Round(Details.CardPrice / (1 + Splits.company_b.tax_pct / 100), 2, MidpointRounding.AwayFromZero);
                _tax_b = Math.Round(_base_b * Splits.company_b.tax_pct / 100, 2, MidpointRounding.AwayFromZero);
                _base_b = Details.CardPrice - _tax_b;
              }
              else
              {
                _base_b = Math.Round(Details.CashIn2 / (1 + Splits.company_b.tax_pct / 100), 2, MidpointRounding.AwayFromZero);
                _tax_b = Math.Round(_base_b * Splits.company_b.tax_pct / 100, 2, MidpointRounding.AwayFromZero);
                _base_b = Details.CashIn2 - _tax_b;
              }
            }

            SetValue("CV_M01_BASE", _base_b);
            SetValue("CV_M01_TAX1", _tax_b);
          }
          break;
      } // switch (VoucherSequenceId)
      //}
      //break; // case 1

      switch (VoucherMode)
      {
        case 1: // Palacio de los N�meros
          String _promo_text;

          _promo_text = GeneralParam.GetBoolean("Cashier", "SplitAsWon", false) ?
              GeneralParam.GetString("Cashier", "SplitAsWon.Text") : Resource.String("M01-STR_VOUCHER_CARD_ADD_PROMOTION");

            AddString("VOUCHER_PROMOTION", _promo_text);

            break; // case 1. Palacio de los N�meros
        case 2: // Miravalle
          {
            if (Splits.enabled_b && !Splits.prize_coupon)
            {
              SetParameterValue("@VOUCHER_CARD_ADD_PART_B_PROMOTION",
                  "<tr>" +
                    "<td align=\"left\"  width=\"50%\">" + Resource.String("STR_VOUCHER_CARD_ADD_PART_B_PROMOTION") + ":</td>" +
                    "<td align=\"right\" width=\"50%\">" + Details.CashIn2 + "</td>" +
                  "</tr>");
            }
            else
            {
              SetParameterValue("@VOUCHER_CARD_ADD_PART_B_PROMOTION", "");
            }
          }
          break; // case 2
        case 5:
          AddString("VOUCHER_SPLIT2_CONCEPT", Splits.company_b.name);
          AddCurrency("VOUCHER_SPLIT2_AMOUNT", _cash_in_split2);

          // MPO & OPC LOGRAND CON02
          AddString("VOUCHER_TAX_DETAIL_SPLIT2", Resource.String("STR_TAX_DETAIL", Splits.company_b.tax_name));
          AddString("VOUCHER_BASE_TAX_NAME_SPLIT2", Resource.String("STR_BASE"));
          String _pct = String.Format("{0:P2}", (Splits.company_b.tax_pct / 100));
          AddString("VOUCHER_TAX_NAME_SPLIT2", Splits.company_b.tax_name + " (" + _pct.Trim() + ")");
          AddCurrency("VOUCHER_BASE_TAX_SPLIT2", Details.TotalBaseTaxSplit2);
          AddCurrency("VOUCHER_TAX_SPLIT2", Details.TotalTaxSplit2);
          break;

      } // switch (VoucherMode)

    } // VoucherCashIn

    #endregion
  }

  public class VoucherGiftList : Voucher
  {
    #region Constructor

    public VoucherGiftList(String[] VoucherAccountInfo
                         , Points Points
                         , String AvailableGiftList
                         , String FutureGiftList
                         , PrintMode Mode
                         , SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      LoadVoucher("Voucher.Gift.List");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      AddString("VOUCHER_GIFT_LIST_ACCOUNT_POINTS", Points.ToString());

      SetParameterValue("@VOUCHER_GIFT_LIST_AVAILABLE_GIFTS", AvailableGiftList);
      SetParameterValue("@VOUCHER_GIFT_LIST_FUTURE_GIFTS", FutureGiftList);

    } // VoucherGiftList

    #endregion
  }

  public class VoucherCashOut : Voucher
  {
    #region Constructor

    public VoucherCashOut(String[] VoucherAccountInfo
                           , Int64 AccountId
                           , Currency BalanceBeforeCashOut
                           , TYPE_SPLITS SplitInformation
                           , TYPE_CASH_REDEEM CompanyA
                           , TYPE_CASH_REDEEM CompanyB
                           , CASHIER_MOVEMENT MovementType
                           , PrintMode Mode
                           , List<VoucherValidationNumber> ValidationNumbers
                           , String BarCodeExternal
                           , Currency CashPayment
                           , Currency CheckPayment
                           , OperationCode OperationCode
                           , SqlTransaction SQLTransaction
                           , Boolean IsChipsPurchaseWithCashOut
                           , Boolean IsApplyTaxes
                           , Boolean IsTicketPayment)
      : base(Mode, OperationCode, SQLTransaction)
    {
      Currency _tax1, _tax2;
      Currency _deductions;
      Currency _total_dev_ab;
      String _str_html;
      String _previous_terminal_name;
      String _payment_check;
      String _style_barcode;

      _tax1 = 0;
      _tax2 = 0;
      _previous_terminal_name = String.Empty;

      switch (MovementType)
      {
        case CASHIER_MOVEMENT.DEV_SPLIT1:
          {
            if (WSI.Common.TITO.Utils.IsTitoMode())
            {
              LoadVoucher("Voucher.A.TITO.CashOut");
            }
            else
            {
              LoadVoucher("Voucher.A.CashOut");
            }
            LoadHeader(SplitInformation.company_a.header);
            LoadFooter(SplitInformation.company_a.footer, SplitInformation.company_a.footer_dev);
            AddString("VOUCHER_SPLIT_NAME", SplitInformation.company_a.cash_out_voucher_title);

            VoucherSequenceId = SequenceId.VouchersSplitA;
            SetValue("CV_TYPE", (Int32)CashierVoucherType.CashOut_A);
          }
          break;
        case CASHIER_MOVEMENT.HANDPAY:
          {
            if (WSI.Common.TITO.Utils.IsTitoMode())
            {
              LoadVoucher("Voucher.A.TITO.CashOut");
            }
            else
            {
              LoadVoucher("Voucher.A.CashOut");
            }
            LoadHeader(SplitInformation.company_a.header);
            LoadFooter(SplitInformation.company_a.footer, SplitInformation.company_a.footer_dev);
            AddString("VOUCHER_SPLIT_NAME", SplitInformation.company_a.cash_out_voucher_title);

            VoucherSequenceId = SequenceId.VouchersSplitA;
            SetValue("CV_TYPE", (Int32)CashierVoucherType.CashOut_A);
          }
          break;
        default:
          {
            Log.Error("VoucherCashOut. MovementType not valid: " + MovementType.ToString());
            return;
          }
      }

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      if (ValidationNumbers != null)
      {
        // Group terminals
        ValidationNumbers.Sort(delegate(VoucherValidationNumber p1, VoucherValidationNumber p2)
        {
          return p1.CreatedTerminalName.CompareTo(p2.CreatedTerminalName);
        });

        _str_html = "<tr>";
        _str_html += "<td><p>";
        _str_html += String.Format("{0} {1}", Resource.String("STR_VOUCHER_VALIDATION_NUMBERS_TITLE"), ValidationNumbers.Count);
        _str_html += "</p></td>";
        _str_html += "</tr>";
        for (int _idx = 0; _idx < ValidationNumbers.Count; _idx++)
        {
          // Add terminal name for a groups of tickets
          if (String.Compare(_previous_terminal_name, ValidationNumbers[_idx].CreatedTerminalName) != 0)
          {
            _previous_terminal_name = ValidationNumbers[_idx].CreatedTerminalName;
            _str_html += "<td colspan=\"2\" align=\"left\">@TICKET_TITO_TERMINAL_NAME</td>";
            _str_html = _str_html.Replace("@TICKET_TITO_TERMINAL_NAME", HttpUtility.HtmlEncode(Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + _previous_terminal_name));
          }
          _str_html += "<tr>";
          _str_html += "<td align=\"left\"  width=\"80%\">&nbsp&nbsp@VALIDATION_NUMBER</td>";
          _str_html += "<td align=\"right\" width=\"20%\">@VALUE</td>";
          _str_html += "</tr>";

          _str_html = _str_html.Replace("@VALIDATION_NUMBER", ValidationNumberManager.FormatValidationNumber(ValidationNumbers[_idx].ValidationNumber, (Int32)ValidationNumbers[_idx].ValidationType, true));
          _str_html = _str_html.Replace("@VALUE", HttpUtility.HtmlEncode(ValidationNumbers[_idx].Amount.ToString()));
        }
        _str_html += "<tr><td>&nbsp;</td></tr>";
        SetParameterValue("@VALIDATION_NUMBERS", _str_html);
      }
      else
      {
        SetParameterValue("@VALIDATION_NUMBERS", "");
      }

      // Deductions = tax1 + tax2 + service_charge, deductions is a negative number
      if (IsApplyTaxes)
      {
        _deductions = -(CompanyA.tax1 + CompanyA.tax2 + CompanyB.service_charge);
      }
      else
      {
        _deductions = 0;
      }

      // Total devolution AB
      _total_dev_ab = CompanyA.TotalDevolution + CompanyB.TotalDevolution;

      // Taxes is a negative number
      if (CompanyA.tax1 > 0)
      {
        _tax1 = CompanyA.tax1 * -1;
      }
      // Taxes is a negative number
      if (CompanyA.tax2 > 0)
      {
        _tax2 = CompanyA.tax2 * -1;
      }

      AddCurrency("VOUCHER_TOTAL_CASH_IN", CompanyA.total_cash_in);
     //SDS 18-05-2016 Bug 13001 Prize Payout cancel Not Redeemable
      if(OperationCode!=OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
      AddCurrency("VOUCHER_CARD_CASH_OUT_NO_REDEEM", CompanyA.lost_balance.Balance.PromoNotRedeemable);
      else
        AddCurrency("VOUCHER_CARD_CASH_OUT_NO_REDEEM", 0);

      if (Tax.EnableTitoTaxWaiver(true))
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_DEVOLUTION", _deductions);
      }
      else
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_DEVOLUTION", CompanyA.TotalDevolution);

      // 21-APR-2016 LTC
      if (Misc.IsTaxCustodyEnabled())
      {
        SetParameterValue("@VOUCHER_CARD_ADD_CUSTODY",
                            "<tr>" +
                              "<td align=\"left\"  width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CUSTODY_NAME:</td>" +
                              "<td align=\"right\" width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CUSTODY_DEV</td>" +
                            "</tr>");

        AddString("VOUCHER_CARD_ADD_CUSTODY_NAME", Misc.TaxCustodyRefundName());
        AddCurrency("VOUCHER_CARD_ADD_CUSTODY_DEV", CompanyA.DevolutionCustody);
      }
      else {
        SetParameterValue("@VOUCHER_CARD_ADD_CUSTODY", "");
      }

      }
      else {
        SetParameterValue("@VOUCHER_CARD_ADD_CUSTODY", "");
      }

      }
      if (!TITO.Utils.IsTitoMode())
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_PRIZE", CompanyA.prize);
      }

      AddString("VOUCHER_CARD_CASH_OUT_TAX1_NAME", CompanyA.tax1_name + ":");
      if (IsApplyTaxes)
      {
        AddString("VOUCHER_CARD_CASH_OUT_TAX1_VALUE", _tax1.ToString());
      }
      AddString("VOUCHER_CARD_CASH_OUT_TAX2_NAME", CompanyA.tax2_name + ":");
      if (IsApplyTaxes)
      {
        AddString("VOUCHER_CARD_CASH_OUT_TAX2_VALUE", _tax2.ToString());
      }

      if (VoucherMode == 7)
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_CARD_DEPOSIT", CompanyA.card_deposit);
      }

      if (Misc.IsCardPlayerToCompanyB())
      {
        SetParameterValue("@VOUCHER_CARD_ADD_CARD", "");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_ADD_CARD",
                                    "<tr>" +
                                      "<td align=\"left\"  width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CARD_NAME:</td>" +
                                      "<td align=\"right\" width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CARD_DEPOSIT</td>" +
                                    "</tr>");

        AddString("VOUCHER_CARD_ADD_CARD_NAME", Misc.GetCardPlayerConceptName());
        AddCurrency("VOUCHER_CARD_ADD_CARD_DEPOSIT", CompanyA.card_deposit);
      }

      if (IsTicketPayment)
      {
        if (IsApplyTaxes)
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.TotalPaid, SplitInformation.company_a.total_in_letters);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.prize, SplitInformation.company_a.total_in_letters);
        }
      }
      else
      {
          AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.TotalPaid, SplitInformation.company_a.total_in_letters);
      }

      if (CompanyA.RoundingEnabled)
      {
        _str_html = "";
        _str_html += "<tr>";
        _str_html += "<td align=\"left\"  width=\"70%\">@ROUNDING_NAME</td>";
        _str_html += "<td align=\"right\" width=\"30%\">@ROUNDING_VALUE</td>";
        _str_html += "</tr>";

        SetParameterValue("@VOUCHER_CARD_CASH_OUT_ROUNDING", _str_html);

        AddString("ROUNDING_NAME", Resource.String("STR_UC_CARD_BALANCE_ROUNDING") + ":");
        AddCurrency("ROUNDING_VALUE", CompanyA.TotalRounding);
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_CASH_OUT_ROUNDING", "");
      }

      String _points_canceled;

      _points_canceled = "";

      if (CompanyA.lost_balance.Points > 0)
      {
        String _label_points;
        _label_points = "";

        _points_canceled += "<tr>";
        _points_canceled += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\"></td>";
        _points_canceled += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\"></td>";
        _points_canceled += "</tr>";

        _points_canceled += "<tr>";
        _points_canceled += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\">#LABEL_POINTS_CANCELED#</td>";
        _points_canceled += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\">#POINTS#</td>";
        _points_canceled += "</tr>";

        _points_canceled += "<tr>";
        _points_canceled += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\"></td>";
        _points_canceled += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\"></td>";
        _points_canceled += "</tr>";

        _label_points = Resource.String("STR_VOUCHER_CARD_ADD_POINTS_CANCELED");
        _points_canceled = _points_canceled.Replace("#LABEL_POINTS_CANCELED#", _label_points);
        _points_canceled = _points_canceled.Replace("#POINTS#", ((Points)CompanyA.lost_balance.Points).ToString());
      }

      SetParameterValue("@VOUCHER_CARD_CASH_OUT_POINTS_CANCELED", _points_canceled);

      // RCI 18-MAY-2011: Mode 01: Palacio de los N�meros
      // JML save data for all Modes
      //if (VoucherMode == 1)
      //{
      Currency _ini_redeemable_balance;
      Currency _fin_redeemable_balance;

      _ini_redeemable_balance = BalanceBeforeCashOut - CompanyA.lost_balance.Balance.PromoRedeemable - CompanyA.lost_balance.Balance.PromoNotRedeemable;
      _fin_redeemable_balance = _ini_redeemable_balance - CompanyA.TotalRedeemed;

      AddString("VOUCHER_CARD_ACCOUNT_ID", AccountId.ToString("000000"));
      AddCurrency("VOUCHER_CARD_CASH_OUT_PREVIOUS_BALANCE", _ini_redeemable_balance);
      AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_CASH_OUT", CompanyA.TotalRedeemed);
      AddCurrency("VOUCHER_CARD_CASH_OUT_TAXES_VALUE", CompanyA.TotalTaxes);
      if (IsTicketPayment)
      {
        if (IsApplyTaxes)
        {
        AddCurrency("VOUCHER_CARD_CASH_OUT_AMOUNT_TO_PAY", CompanyA.TotalRedeemAfterTaxes);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_AMOUNT_TO_PAY", CompanyA.prize);
        }
      }
      else
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_AMOUNT_TO_PAY", CompanyA.TotalRedeemAfterTaxes);
      }
      AddCurrency("VOUCHER_CARD_CASH_OUT_FINAL_BALANCE", _fin_redeemable_balance);

      // RRB 28-SEP-2012: Mode 03: Codere
      AddCurrency("VOUCHER_TOTAL_BEFORE_CASH_OUT", BalanceBeforeCashOut);
      AddCurrency("VOUCHER_PROMOTION", -CompanyA.lost_balance.Balance.TotalBalance + CompanyB.TotalDevolution);
      AddCurrency("VOUCHER_DEDUCTIONS", _deductions);
      AddCurrency("ROUNDING_VALUE", CompanyA.TotalRounding);
      AddCurrency("VOUCHER_TOTAL_DEVOLUTION", _total_dev_ab);
      if (IsTicketPayment)
      {
        if (IsApplyTaxes)
        {
        AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_PAID_AB", CompanyA.TotalPaid + CompanyB.TotalPaid, SplitInformation.company_a.total_in_letters);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_PAID_AB", CompanyA.prize, SplitInformation.company_a.total_in_letters);
        }
      }
      else
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_PAID_AB", CompanyA.TotalPaid + CompanyB.TotalPaid, SplitInformation.company_a.total_in_letters);
      }

      // Set necessary values for reports.
      SetValue("CV_M01_BASE", CompanyA.prize);
      SetValue("CV_M01_TAX1", CompanyA.tax1);
      SetValue("CV_M01_TAX2", CompanyA.tax2);
      SetValue("CV_M01_DEV", CompanyA.TotalDevolution);
      SetValue("CV_M01_FINAL", _fin_redeemable_balance);
      //} // if (VoucherMode == 1)

      // DCS 16-03-2014:  If exists check include divided payment with check and payment with cash
      _payment_check = "";
      _style_barcode = "";

      if (CheckPayment > 0)
      {
        _payment_check += "<br />";
        _payment_check += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
        _payment_check += "  <tr>";
        _payment_check += "    <td align=\"left\" width=\"50%\">#PAYMENT_ORDER_CASH_NAME#</td>";
        _payment_check += "    <td align=\"right\" width=\"50%\">#PAYMENT_ORDER_CASH#</td>";
        _payment_check += "  </tr>";
        _payment_check += "  <tr>";
        _payment_check += "    <td align=\"left\" width=\"50%\">#PAYMENT_ORDER_CHECK_NAME#</td>";
        _payment_check += "    <td align=\"right\" width=\"50%\">#PAYMENT_ORDER_CHECK#</td>";
        _payment_check += "  </tr>";
        _payment_check += "  <tr>";
        _payment_check += "    <td colspan=\"2\" align=\"center\">";
        _payment_check += "      <br />";
        _payment_check += "      <div id='inputdata'/>";
        _payment_check += "     </td>";
        _payment_check += "  </tr>";
        _payment_check += "</table>";

        _payment_check = _payment_check.Replace("#PAYMENT_ORDER_CASH_NAME#", Resource.String("STR_VOUCHER_PAY_CHECK_CASH"));
        _payment_check = _payment_check.Replace("#PAYMENT_ORDER_CASH#", CashPayment.ToString());

        _payment_check = _payment_check.Replace("#PAYMENT_ORDER_CHECK_NAME#", Resource.String("STR_VOUCHER_PAY_CHECK_CHECK"));
        _payment_check = _payment_check.Replace("#PAYMENT_ORDER_CHECK#", CheckPayment.ToString());

        _style_barcode = "<script type='text/javascript'>$('#inputdata').barcode('#PAYMENT_BARCODE_ORDER_ID#', 'int25');</script>";
        _style_barcode = _style_barcode.Replace("#PAYMENT_BARCODE_ORDER_ID#", BarCodeExternal);
      }

      SetParameterValue("@VOUCHER_BARCODE_PAYMENT_CHECK", _style_barcode);
      SetParameterValue("@VOUCHER_AMOUNTS_PAYMENT_CHECK", _payment_check);

      if (WSI.Common.TITO.Utils.IsTitoMode() || VoucherMode == 7)
      {
        String _param_name_tax;
        StringBuilder _html_code;

        _html_code = new StringBuilder();

        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("	<td>");
        _html_code.AppendLine("		<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td colspan='3'>");
        _html_code.AppendLine("					@VOUCHER_TAX_DETAIL:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td width='5%'></td>");
        _html_code.AppendLine("				<td>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX_NAME:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("				<td width='40%' align='right'>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			@TAXES");
        _html_code.AppendLine("		</table>");
        _html_code.AppendLine("		<hr noshade size='4' /> ");
        _html_code.AppendLine("	</td>");
        _html_code.AppendLine("</tr>");

        SetParameterValue("@TABLE_WITHHOLDING", _html_code.ToString());

        _param_name_tax = Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING");

        AddString("VOUCHER_TAX_DETAIL", Resource.String("STR_HANDPAY_TAX_DETAIL", _param_name_tax.ToString()));
        AddString("VOUCHER_BASE_TAX_NAME", Resource.String("STR_BASE"));
        SetParameterValue("@TAXES", DevolutionPrizeParts.GetTaxesHtml(CompanyA.prize, true, true, IsChipsPurchaseWithCashOut, IsApplyTaxes, OperationCode));


        // MPO 17-MAR-2015: WIG-2154: It isn't necessary show the base amount when there aren't tax amount.
        AddCurrency("VOUCHER_BASE_TAX", CompanyA.prize);

      }
      else
      {
        SetParameterValue("@TABLE_WITHHOLDING", String.Empty);
      }

      SetSignatureRoomByVoucherType(this.GetType());
    } // VoucherCashOut

    #endregion
  }

  public class VoucherCancel : Voucher
  {
    #region Constructor

    public VoucherCancel(String[] VoucherAccountInfo
                       , TYPE_SPLITS SplitInformation
                       , TYPE_CASH_REDEEM CompanyB
                       , CancellableOperation CancellableOperation
                       , CASHIER_MOVEMENT MovementType
                       , OperationCode OperationCode
                       , PrintMode Mode
                       , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {
      Currency _total_devolution_b;

      switch (MovementType)
      {

        case CASHIER_MOVEMENT.DEV_SPLIT2:
          {
            CancelledSequenceId = GetSecuenceId_B(CancellableOperation.OperationId, SQLTransaction);

            LoadVoucher("Voucher.B.Cancel");
            LoadHeader(SplitInformation.company_b.header);
            LoadFooter(SplitInformation.company_b.footer, SplitInformation.company_b.footer_cancel);
            AddString("VOUCHER_SPLIT_NAME", SplitInformation.company_b.cash_out_voucher_title);
            AddString("VOUCHER_CONCEPT", SplitInformation.company_b.name);

            VoucherSequenceId = SequenceId.VouchersSplitB;
            if (this.VoucherMode == 3)
            {
              VoucherSequenceId = SequenceId.VouchersSplitBCancel;
            }

            SetValue("CV_TYPE", (Int32)CashierVoucherType.Cancel_B);
          }
          break;

        default:
          {
            Log.Error("VoucherCancel. MovementType not valid: " + MovementType.ToString());
            return;
          }
      }

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      // TODO ANDREU: Verificar si amb PromoRedeemable es suficient.
      _total_devolution_b = CancellableOperation.Balance.PromoRedeemable;

      AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", _total_devolution_b);
      // RCI 18-MAY-2011: Need to use separate TAGs to be able to use AmountWithLetters only in one TAG.
      AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_CASHED_AMOUNT", _total_devolution_b, SplitInformation.company_b.total_in_letters);

      // RCI 18-MAY-2011: Mode 01: Palacio de los N�meros
      // JML save data for all Modes
      //if (VoucherMode == 1)
      //{
      Currency _base_b;
      Currency _tax_b;

      AddCurrencyInLetters("VOUCHER_CARD_CASH_OUT_LETTERS_TOTAL_AMOUNT", _total_devolution_b);

      _base_b = Math.Round(_total_devolution_b / (1 + SplitInformation.company_b.tax_pct / 100), 2, MidpointRounding.AwayFromZero);
      _tax_b = Math.Round(_base_b * SplitInformation.company_b.tax_pct / 100, 2, MidpointRounding.AwayFromZero);
      _base_b = _total_devolution_b - _tax_b;

      SetValue("CV_M01_BASE", _base_b);
      SetValue("CV_M01_TAX1", _tax_b);

      if (CancelledSequenceId > 0)
      {
        SetValue("CV_M01_CANCELLED_SEQUENCE", CancelledSequenceId);
      }

      //} // if (VoucherMode == 1)

    } // VoucherCancel

    #endregion
  }

  // 1. Card Buy/Add Credit (Compra/Recarga)
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Recharge Amount: $
  // Total:           $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherCardAdd : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardAdd(Boolean ShowDeposit, Boolean Promotion, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardAdd");

      // 2. Load voucher resources.
      LoadVoucherResources(ShowDeposit, Promotion);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Boolean ShowDeposit, Boolean Promotion)
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CARD_ADD_TITLE");
      AddString("STR_VOUCHER_CARD_ADD_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_MB_ID") + ":";
      AddString("STR_VOUCHER_CARD_MB_ID", value);

      // Voucher Specific 
      if (Promotion)
      {
        AddString("STR_VOUCHER_CARD_ADD_CURRENT", "");

        value = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION_EXPIRED_DATE") + ":";
        AddString("STR_VOUCHER_CARD_ADD_AMOUNT_TO_ADD", value);

        value = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION_VOUCHER") + ":";
        AddString("STR_VOUCHER_CARD_ADD_BALANCE_INITIAL", value);
      }
      else
      {
        value = Resource.String("STR_VOUCHER_CARD_ADD_CURRENT") + ":";
        AddString("STR_VOUCHER_CARD_ADD_CURRENT", value);

        value = Resource.String("STR_VOUCHER_CARD_ADD_AMOUNT_TO_ADD") + ":";
        AddString("STR_VOUCHER_CARD_ADD_AMOUNT_TO_ADD", value);

        value = Resource.String("STR_VOUCHER_CARD_ADD_BALANCE_INITIAL") + ":";
        AddString("STR_VOUCHER_CARD_ADD_BALANCE_INITIAL", value);
      }
      value = "";
      if (ShowDeposit)
      {
        value = WSI.Common.Misc.GetCardPlayerConceptName() + ":";
      }
      AddString("STR_VOUCHER_CARD_ADD_CARD_DEPOSIT", value);

      if (Promotion)
      {
        AddString("STR_VOUCHER_CARD_ADD_BALANCE_FINAL", "");
      }
      else
      {
        value = Resource.String("STR_VOUCHER_CARD_ADD_BALANCE_FINAL") + ":";
        AddString("STR_VOUCHER_CARD_ADD_BALANCE_FINAL", value);
      }

      value = Resource.String("STR_VOUCHER_CARD_ADD_TOTAL_AMOUNT");
      AddString("STR_VOUCHER_CARD_ADD_TOTAL_AMOUNT", value);

    }

    #endregion

    #region Public Methods

    #endregion
  }

  // 2. Card Redeem (Partial/Total) (Reintegro Parcial/Total)
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Current Balance: $
  // Redeem Amount:   $
  // Total:           $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer

  public class VoucherCardRedeem : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardRedeem(CASH_MODE RedeemType, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardRedeem");

      // 2. Load voucher resources.
      LoadVoucherResources(RedeemType);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(CASH_MODE RedeemType)
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      if (RedeemType == CASH_MODE.TOTAL_REDEEM)
      {
        // Total Redeem
        value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL_TITLE");
      }
      else
      {
        // Partial Redeem
        value = Resource.String("STR_VOUCHER_CARD_REDEEM_PARTIAL_TITLE");
      }
      AddString("STR_VOUCHER_CARD_REDEEM_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_MB_ID") + ":";
      AddString("STR_VOUCHER_CARD_MB_ID", value);

      // - Voucher Specific
      value = Resource.String("STR_VOUCHER_CARD_REDEEM_BALANCE_INITIAL") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_BALANCE_INITIAL", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_CASH_FROM_CASH_IN") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_CASH_FROM_CASH_IN", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_CASH_FROM_WON") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_CASH_FROM_WON", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_CASH_FROM_TAXES") + ": ";
      AddString("STR_VOUCHER_CARD_REDEEM_CASH_FROM_TAXES", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_TOTAL", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_BALANCE_FINAL") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_BALANCE_FINAL", value);

      value = Resource.String("STR_VOUCHER_CLIENT_SIGN") + ":";
      AddString("STR_VOUCHER_CLIENT_SIGN", value);

      value = Resource.String("STR_VOUCHER_CLIENT_ACCEPT");
      AddString("STR_VOUCHER_CLIENT_ACCEPT", value);

      value = "";
      if (RedeemType == CASH_MODE.TOTAL_REDEEM)
      {
        // Total Redeem
        value = WSI.Common.Misc.GetCardPlayerConceptName() + ":";
      }
      AddString("STR_VOUCHER_CARD_REDEEM_CARD_DEPOSIT", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_AMOUNT_TO_REDEEM");
      AddString("STR_VOUCHER_CARD_REDEEM_AMOUNT_TO_REDEEM", value);
    }

    #endregion

    #region Public Methods

    #endregion

  }

  // 3. Cash Desk Open
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Initial Amount:   $
  // Open Amount:      $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  public class VoucherCashDeskOpen : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskOpen(CASHIER_MOVEMENT MovementType, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      InitializeVoucher(MovementType);
    }

    public VoucherCashDeskOpen(CASHIER_MOVEMENT MovementType, CashierVoucherType VoucherType, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, VoucherType, SQLTransaction)
    {
      InitializeVoucher(MovementType);
    }

    private void InitializeVoucher(CASHIER_MOVEMENT MovementType)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskOpen");

      // 2. Load voucher resources.
      LoadVoucherResources(MovementType);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    /// <param name="MovementType">Movement Type:
    ///                            Fill In
    ///                            Open Session</param>
    private void LoadVoucherResources(CASHIER_MOVEMENT MovementType)
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
      {
        value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_IN_TITLE");
      }
      else
      {
        value = Resource.String("STR_VOUCHER_CASH_DESK_OPEN_TITLE");
      }
      AddString("STR_VOUCHER_CASH_DESK_OPEN_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);
    }

    #endregion

    #region Public Methods

    #endregion

  }

  public class VoucherCashDeskOpenBody : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskOpenBody(CASHIER_MOVEMENT MovementType, CurrencyIsoType IsoType, Decimal Initial, Decimal Amount, Boolean ShowBalance)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskOpenBody");

      // 2. Load voucher resources.
      LoadVoucherResources(MovementType, IsoType, Initial, Amount, ShowBalance);
    }

    public VoucherCashDeskOpenBody(CASHIER_MOVEMENT MovementType, CurrencyIsoType IsoType, Decimal Initial, Decimal Amount)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskOpenBody");

      // 2. Load voucher resources.
      LoadVoucherResources(MovementType, IsoType, Initial, Amount, true);
    }

    #endregion

    #region Private Methods
    private void LoadVoucherResources(CASHIER_MOVEMENT MovementType, CurrencyIsoType IsoType, Decimal Initial, Decimal Amount, Boolean ShowBalance)
    {
      String _value;
      String _national_currency;
      CurrencyExchange _currency_exchange = new CurrencyExchange();

      _national_currency = CurrencyExchange.GetNationalCurrency();

      CurrencyExchange.ReadCurrencyExchange(IsoType.Type, IsoType.IsoCode, out _currency_exchange);
      //Title
      _value = _currency_exchange.Description;
      if (_national_currency == IsoType.IsoCode && IsoType.Type == CurrencyExchangeType.CURRENCY)
      {
        _value = "";
      }
      AddString("STR_VOUCHER_CASH_DESK_OPEN_TITLE", _value);

      // - Voucher Specific
      _value = "";
      if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_OPEN_BALANCE_INITIAL") + ":";
      }
      AddString("STR_VOUCHER_CASH_DESK_OPEN_BALANCE_INITIAL", _value);

      _value = "";
      if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_IN_AMOUNT") + ":";
      }
      AddString("STR_VOUCHER_CASH_DESK_OPEN_AMOUNT", _value);

      if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_IN_BALANCE_FINAL") + ":";
      }
      else
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_OPEN_BALANCE_FINAL") + ":";
      }
      AddString("STR_VOUCHER_CASH_DESK_OPEN_BALANCE_FINAL", _value);

      if (_national_currency == IsoType.IsoCode)
      {
        if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
        {
          if (ShowBalance)
          {
            AddCurrency("VOUCHER_CASH_DESK_OPEN_BALANCE_INITIAL", (Currency)Initial);
            AddCurrency("VOUCHER_CASH_DESK_OPEN_AMOUNT", (Currency)Amount);
            AddCurrency("VOUCHER_CASH_DESK_OPEN_BALANCE_FINAL", (Currency)(Initial + Amount));
          }
          else
          {
            AddString("VOUCHER_CASH_DESK_OPEN_BALANCE_INITIAL", "---");
            AddCurrency("VOUCHER_CASH_DESK_OPEN_AMOUNT", (Currency)Amount);
            AddString("VOUCHER_CASH_DESK_OPEN_BALANCE_FINAL", "---");
          }
        }
        else
        {
          AddString("VOUCHER_CASH_DESK_OPEN_BALANCE_INITIAL", "");
          AddString("VOUCHER_CASH_DESK_OPEN_AMOUNT", "");
          AddCurrency("VOUCHER_CASH_DESK_OPEN_BALANCE_FINAL", Amount);
        }
      }
      else
      {
        if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
        {
          if (ShowBalance)
          {
            AddString("VOUCHER_CASH_DESK_OPEN_BALANCE_INITIAL", Currency.Format(Initial, IsoType.IsoCode));
            AddString("VOUCHER_CASH_DESK_OPEN_AMOUNT", Currency.Format(Amount, IsoType.IsoCode));
            AddString("VOUCHER_CASH_DESK_OPEN_BALANCE_FINAL", Currency.Format(Initial + Amount, IsoType.IsoCode));
          }
          else
          {
            AddString("VOUCHER_CASH_DESK_OPEN_BALANCE_INITIAL", "---");
            AddString("VOUCHER_CASH_DESK_OPEN_AMOUNT", Currency.Format(Amount, IsoType.IsoCode));
            AddString("VOUCHER_CASH_DESK_OPEN_BALANCE_FINAL", "---");
          }
        }
        else
        {
          AddString("VOUCHER_CASH_DESK_OPEN_BALANCE_INITIAL", "");
          AddString("VOUCHER_CASH_DESK_OPEN_AMOUNT", "");
          AddString("VOUCHER_CASH_DESK_OPEN_BALANCE_FINAL", Currency.Format(Initial + Amount, IsoType.IsoCode));
        }
      }

      _value = "";
      if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
      {
        _value = "<hr noshade size='2'>";
      }
      SetParameterValue("@VOUCHER_LINE", _value);

    } // SetupCashStatusVoucher
    #endregion

    #region Public Methods

    #endregion

  }

  // 4. Cash Desk Close
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Initial Amount:   $
  // Close Amount:     $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  public class VoucherCashDeskClose : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskClose(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                              , CashDeskCloseVoucherType Type
                              , PrintMode Mode
                              , SqlTransaction SQLTransaction)
      : base(Mode, CashierVoucherType.CashClosing, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.
      //  4. Load specific voucher data.


      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskClose");

      // 2. Load voucher resources.
      LoadVoucherResources(Type);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // 4. Load specific voucher data.
      SetupCashCloseVoucher(CashierSessionStats, Type);

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(CashDeskCloseVoucherType Type)
    {

      String _str_value;
      TYPE_SPLITS _split_data;

      if (!Split.ReadSplitParameters(out _split_data))
      {
        return;
      }

      // TODO: Check return codes!!!

      // NLS Values

      // - Title
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TITLE", _str_value);

      // - General
      _str_value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", _str_value);

      _str_value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", _str_value);

      _str_value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", _str_value);

      _str_value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", _str_value);

      _str_value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", _str_value);

      // Cash desk close voucher
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_NUMBERS");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_NUMBERS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_OUT_DETAILS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_OUT_DETAILS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_MB_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_MB_DEPOSIT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENT_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENT_AMOUNT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_PENDING_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_PENDING_AMOUNT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_INITIAL_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_INITIAL_AMOUNT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_DEPOSIT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_WITHDRAW", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SUB_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUB_TOTAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_PRIZES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON", _str_value);

      _str_value = Resource.String("STR_FRM_CASH_DESK_PRIZE_PAYED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_PAYED_WON", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_DEVOLUTIONS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_DEV", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TAXES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TAXES", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TOTAL_CASH_DESK") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TOTAL_CASH_DESK", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_NOT_REDEEMABLE") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_NOT_REDEEMABLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TAXES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TAX_TAXES", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET", _str_value);

      if (Type == CashDeskCloseVoucherType.ALL)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED", "");
      }

      _str_value = Resource.String("STR_UC_BANK_HANDPAYS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_HANDPAY_005") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_HANDPAY_014") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL", _str_value);

      _str_value = Resource.String("STR_UC_CARD_BALANCE_PROMO_TO_REDEEM") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM", _str_value);

      //value = Resource.String("STR_UC_CARD_BALANCE_NOT_REDEEMABLE") + ":";
      _str_value = _split_data.text_promo + ":";
      if (String.IsNullOrEmpty(_str_value))
      {
        _str_value = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION") + ":";
      }
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED", _str_value);

    } // LoadVoucherResources

    #endregion

    #region Public Methods

    public void SetupCashCloseVoucher(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                                     , CashDeskCloseVoucherType Type)
    {
      Currency signed_amount;
      String _str_value;
      Currency _cs_mb_diff;
      CurrencyIsoType _iso_type;
      Boolean _gp_pending_cash_partial;
      CashierSessionInfo _session_info;
      Boolean _is_tito_mode;

      //  1. Depositos
      //  2. Retiros
      //  3. Entradas
      //  4. Salidas
      //     ---------------------------
      //  5. Saldo (Efectivo en Caja)
      // 
      //     Detalle Salidas
      //  6. Premios
      //  7. Impuestos
      //  8. Premios pagados
      // 
      //     ---------------------------
      _iso_type = new CurrencyIsoType();
      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();
      _session_info = CommonCashierInformation.CashierSessionInfo();
      _is_tito_mode = Utils.IsTitoMode();

      switch (Type)
      {
        case CashDeskCloseVoucherType.ALL:
          {
            //AddString("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_NUM", CashierSessionStats.total_initial_movs.ToString());
            //AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_AMT", CashierSessionStats.initial_balance);

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUBTITLE", "");

            //  1. Depositos
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", CashierSessionStats.deposits);

            //  2. Retiros
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", CashierSessionStats.withdraws);

            //  3. Cash In
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.total_cash_in);

            //  4. Cash Out
            signed_amount = CashierSessionStats.total_cash_out;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", signed_amount);

            //  Diferencia Bancos M�viles
            _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF_MISSING") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _str_value);
            _cs_mb_diff = CashierSessionStats.mb_total_balance_lost + CashierSessionStats.mb_pending;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _cs_mb_diff);

            //  Sobrante Bancos M�viles
            if (_gp_pending_cash_partial)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL") + ":";
            }
            else
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER") + ":";
            }

            AddString("STR_VOUCHER_CASH_OVER", _str_value);
            AddCurrency("VOUCHER_CASH_OVER", CashierSessionStats.total_cash_over);

            //  Chech Payments
            _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", _str_value);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", CashierSessionStats.payment_orders);

            //Credit Card Exchange
            if (CashierSessionStats.total_bank_card > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.total_bank_card);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
            }

            //Check
            if (CashierSessionStats.total_check > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CHECK") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CHECK", CashierSessionStats.total_check);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CHECK", "");
            }

            // Currency Exchange
            if (CashierSessionStats.total_currency_exchange > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", CashierSessionStats.total_currency_exchange);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
            }

            //Commission Exchange
            if (CashierSessionStats.total_commission > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COMMISSION", CashierSessionStats.total_commission);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
            }

            //Recharge for Points
            if (CashierSessionStats.points_as_cashin > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", CashierSessionStats.points_as_cashin);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
            }

            //Service Charge
            if (CashierSessionStats.service_charge > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", CashierSessionStats.service_charge);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
            }

            //  5. Saldo
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_CASH_NET", CashierSessionStats.final_balance);

            //  5.1 Entregado
            _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
            _iso_type.Type = CurrencyExchangeType.CURRENCY;
            if (CashierSessionStats.collected.ContainsKey(_iso_type))
            {
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", CashierSessionStats.collected[_iso_type]);
            }
            else
            {
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", 0);
            }

            //  5.2 Diferencia Cajero
            _str_value = "";

            // DHA: only show cash over/short when cashier session desk is not integrated
            if (_session_info.UserType == GU_USER_TYPE.USER && !Cage.IsGamingTable(_session_info.CashierSessionId))
            {
              Decimal _collected_diff;

              if (CashierSessionStats.collected_diff.ContainsKey(_iso_type))
              {
                _collected_diff = CashierSessionStats.collected_diff[_iso_type];
                if (_collected_diff < 0)
                {
                  _collected_diff = -_collected_diff;
                  AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
                  AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");

                  _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISSING") + ":";
                  AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", _str_value);
                  AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", (Currency)_collected_diff);
                }
                else
                {
                  _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA") + ":";
                  AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", _str_value);
                  AddMultipleLineString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", ((Currency)_collected_diff).ToString());

                  AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
                  AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
                }
              }
              else
              {
                _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISSING") + ":";
                AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", _str_value);
                AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", CashierSessionStats.final_balance);

                AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
                AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
              }
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");

              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            }

            //  6. Premios
            signed_amount = CashierSessionStats.prize_gross;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_AMT", signed_amount);

            // 7. Impuestos
            AddString("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_NUM", " ");
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_AMT", CashierSessionStats.prize_taxes);

            //  8. Premios
            signed_amount = CashierSessionStats.prize_payment;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_PAYED_AMT", signed_amount);

            //  9. Handpay
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED_AMT", CashierSessionStats.handpay_payment);
            signed_amount = CashierSessionStats.handpay_canceled;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED_AMT", signed_amount);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL_AMT", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

            // 10. Promotions
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM_AMT", CashierSessionStats.promo_nr_to_re);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM_AMT", CashierSessionStats.promo_nr);

            // 11. Tickets TITO
            AddInteger("VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED_ACCOUNT", CashierSessionStats.tickets_cancelled_count);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED_AMOUNT", CashierSessionStats.tickets_cancelled_amount);

            Voucher _voucher;
            _voucher = new WSI.Common.VoucherCurrencyCash(CashierSessionStats, true);

            SetParameterValue("@VOUCHER_CURRENCY_CASH", _voucher.VoucherHTML);

            AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");

          }
          break;

        case CashDeskCloseVoucherType.A:
          {

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUBTITLE", CashierSessionStats.split_a_company_name);

            //  1. Depositos
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", CashierSessionStats.deposits);

            //  2. Retiros
            signed_amount = CashierSessionStats.withdraws;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", signed_amount);

            //  3. Cash In
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.total_cash_in_only_a);

            //  4. Cash Out
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", CashierSessionStats.a_total_cash_out);

            //  Diferencia Bancos M�viles
            _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF_MISSING") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _str_value);
            _cs_mb_diff = CashierSessionStats.mb_total_balance_lost + CashierSessionStats.mb_pending;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _cs_mb_diff);

            //  Sobrante Bancos M�viles
            if (_gp_pending_cash_partial)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL") + ":";
            }
            else
            {

              _str_value = Resource.String("STR_VOUCHER_CASH_OVER") + ":";
            }

            AddString("STR_VOUCHER_CASH_OVER", _str_value);
            AddCurrency("VOUCHER_CASH_OVER", CashierSessionStats.total_cash_over);

            //  Chech Payments
            _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", _str_value);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", CashierSessionStats.payment_orders);

            //Credit Card Exchange
            if (CashierSessionStats.total_bank_card > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.bank_card_amount_only_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
            }

            //Check
            if (CashierSessionStats.total_check > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CHECK") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CHECK", CashierSessionStats.check_amount_only_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CHECK", "");
            }


            // Currency Exchange
            if (CashierSessionStats.total_currency_exchange > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", CashierSessionStats.currency_exchange_amount_only_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
            }

            //Commission Exchange
            if (CashierSessionStats.total_commission_a > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COMMISSION", CashierSessionStats.total_commission_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
            }

            //Recharge for Points
            if (CashierSessionStats.points_as_cashin > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", CashierSessionStats.points_as_cashin_only_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
            }

            //Service Charge
            if (CashierSessionStats.service_charge > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", CashierSessionStats.service_charge);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
            }

            //  5. Saldo
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_CASH_NET", CashierSessionStats.final_balance_only_a);

            AddString("VOUCHER_CASH_DESK_CLOSE_COLLECTED", "");

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");

            //  6. Premios
            signed_amount = CashierSessionStats.prize_gross;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_AMT", signed_amount);

            // 7. Impuestos
            AddString("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_NUM", " ");
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_AMT", CashierSessionStats.prize_taxes);

            //  8. Premios
            signed_amount = CashierSessionStats.prize_payment;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_PAYED_AMT", signed_amount);

            //  9. Handpay
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED_AMT", CashierSessionStats.handpay_payment);
            signed_amount = CashierSessionStats.handpay_canceled;// *-1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED_AMT", signed_amount);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL_AMT", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

            // 10. Promotions
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM_AMT", CashierSessionStats.promo_nr_to_re);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM_AMT", CashierSessionStats.promo_nr);

            SetParameterValue("@VOUCHER_CURRENCY_CASH", "");

            AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");

          }
          break;

        case CashDeskCloseVoucherType.B:
          {
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUBTITLE", CashierSessionStats.split_b_company_name);

            //  1. Depositos
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", 0);

            //  2. Retiros
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", 0);


            //  3. Cash In
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.b_total_in);

            //  4. Cash Out
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", CashierSessionStats.b_total_dev);

            //  Diferencia Bancos M�viles
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", "");

            //  Sobrante Bancos M�viles
            AddString("STR_VOUCHER_CASH_OVER", "");
            AddString("VOUCHER_CASH_OVER", "");

            //  Chech Payments
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", "");

            // Currency Exchange
            if (CashierSessionStats.total_currency_exchange > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", CashierSessionStats.currency_exchange_amount_only_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
            }

            //Credit Card Exchange
            if (CashierSessionStats.total_bank_card > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.bank_card_amount_only_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
            }

            //Check
            if (CashierSessionStats.total_check > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CHECK") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CHECK", CashierSessionStats.check_amount_only_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CHECK", "");
            }


            //Recharge for Points
            if (CashierSessionStats.points_as_cashin > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", CashierSessionStats.points_as_cashin_only_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
            }

            //Service Charge
            if (CashierSessionStats.service_charge > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", CashierSessionStats.service_charge);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
            }

            //Commission Exchange
            if (CashierSessionStats.total_commission_b > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COMMISSION", CashierSessionStats.total_commission_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
            }

            //  5. Saldo
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_CASH_NET", CashierSessionStats.final_balance_only_b);

            AddString("VOUCHER_CASH_DESK_CLOSE_COLLECTED", "");

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");

            //  6. Premios
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_AMT", 0);

            // 7. Impuestos
            AddString("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_NUM", " ");
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_AMT", 0);

            //  8. Premios
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_PAYED_AMT", 0);

            //  9. Handpay
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED_AMT", 0);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED_AMT", 0);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL_AMT", 0);

            // 10. Promotions
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM_AMT", 0);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM_AMT", 0);

            SetParameterValue("@VOUCHER_CURRENCY_CASH", "");

            AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");

          }
          break;

        default:
          {
            Log.Error("VoucherCashDesk. CashDeskCloseVoucherType not valid: " + Type.ToString());
            return;
          }

      }


    } // LoadVoucherResources
    #endregion

  }

  // 5. Cash Desk Withdraw
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Initial Amount:   $
  // Close Amount:     $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  public class VoucherCashDeskWithdraw : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskWithdraw(CASHIER_MOVEMENT MovementType, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      InitializeVoucher(MovementType);
    }

    public VoucherCashDeskWithdraw(CASHIER_MOVEMENT MovementType, PrintMode Mode, SqlTransaction SQLTransaction, CashierVoucherType CashierVoucherType)
      : base(Mode, CashierVoucherType, SQLTransaction)
    {
      InitializeVoucher(MovementType);
    }

    private void InitializeVoucher(CASHIER_MOVEMENT MovementType)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskWithdraw");

      // 2. Load voucher resources.
      LoadVoucherResources(MovementType);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(CASHIER_MOVEMENT MovementType)
    {
      String value = "";

      // Images
      // AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_TITLE");
      if (MovementType == CASHIER_MOVEMENT.CLOSE_SESSION)
      {
        value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE");
      }
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

    } // LoadVoucherResources

    #endregion

    #region Public Methods

    #endregion

  }

  public class VoucherCashDeskWithdrawBody : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskWithdrawBody(CurrencyIsoType IsoType, Decimal Initial, Decimal Amount, Boolean ShowBalance)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskWithdrawBody");

      // 2. Load voucher resources.
      LoadVoucherResources(IsoType, Initial, Amount, ShowBalance);
    }

    public VoucherCashDeskWithdrawBody(CurrencyIsoType IsoType, Decimal Initial, Decimal Amount)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskWithdrawBody");

      // 2. Load voucher resources.
      LoadVoucherResources(IsoType, Initial, Amount, true);
    }

    #endregion

    #region Private Methods
    private void LoadVoucherResources(CurrencyIsoType IsoType, Decimal Initial, Decimal Amount, Boolean ShowBalance)
    {
      String _value;
      String _national_currency;
      CurrencyExchange _currency_exchange = new CurrencyExchange();

      _national_currency = CurrencyExchange.GetNationalCurrency();

      CurrencyExchange.ReadCurrencyExchange(IsoType.Type, IsoType.IsoCode, out _currency_exchange);
      //Title
      _value = _currency_exchange.Description;
      if (_national_currency == IsoType.IsoCode && IsoType.Type == CurrencyExchangeType.CURRENCY)
      {
        _value = "";
      }
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_TITLE", _value);

      // - Voucher Specific for Filler Out
      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_BALANCE_INITIAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", _value);

      _value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", _value);

      _value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_BALANCE_FINAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", _value);

      if (_national_currency == IsoType.IsoCode)
      {
        if (ShowBalance)
        {
          AddCurrency("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", (Currency)Initial);
          AddCurrency("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", (Currency)Amount);
          AddCurrency("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", (Currency)(Initial - Amount));
        }
        else
        {
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", "---");
          AddCurrency("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", (Currency)Amount);
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", "---");
        }
      }
      else
      {
        if (ShowBalance)
        {
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", Currency.Format(Initial, IsoType.IsoCode));
          AddString("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", Currency.Format(Amount, IsoType.IsoCode));
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", Currency.Format(Initial - Amount, IsoType.IsoCode));
        }
        else
        {
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", "---");
          AddString("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", Currency.Format(Amount, IsoType.IsoCode));
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", "---");
        }
      }

    } // SetupCashStatusVoucher
    #endregion

    #region Public Methods

    #endregion

  }

  // 6. MBCard Change Limit
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Limit Change: $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherMBCardChange : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherMBCardChange(PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, CashierVoucherType.MBSalesLimitChange, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherMBCardChangeLimit");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_MBCARD_CHANGE_TITLE");
      AddString("STR_VOUCHER_MBCARD_CHANGE_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_CARD_ID") + ":";
      AddString("STR_VOUCHER_CARD_CARD_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID") + ":";
      AddString("STR_VOUCHER_CARD_ACCOUNT_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_MB_ID") + ":";
      AddString("STR_VOUCHER_CARD_MB_ID", value);

      // Voucher Specific 
      value = Resource.String("STR_VOUCHER_MBCARD_CHANGE_INITIAL") + ":";
      AddString("STR_VOUCHER_MBCARD_CHANGE_INITIAL", value);

      value = Resource.String("STR_VOUCHER_MBCARD_CHANGE_FINAL") + ":";
      AddString("STR_VOUCHER_MBCARD_CHANGE_FINAL", value);

    }

    #endregion

    #region Public Methods

    #endregion
  } // VoucherMBCardChange

  // 7. MBCard Deposit Cash
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Cash Amount: $
  // New Balance: $
  // Pending Cash: $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherMBCardDepositCash : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherMBCardDepositCash(PrintMode Mode, CashierVoucherType VoucherType, SqlTransaction SQLTransaction)
      : base(Mode, VoucherType, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherMBCardDepositCash");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_CARD_ID") + ":";
      AddString("STR_VOUCHER_CARD_CARD_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID") + ":";
      AddString("STR_VOUCHER_CARD_ACCOUNT_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_MB_ID") + ":";
      AddString("STR_VOUCHER_CARD_MB_ID", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA") + ":";
      AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_EXCESS", value);

      // Voucher Specific 
      value = Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_INITIAL") + ":";
      AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_INITIAL", value);

      value = Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_FINAL") + ":";
      AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_FINAL", value);

    }

    #endregion

    #region Public Methods

    #endregion
  } // VoucherMBCardDepositCash

  // 8. Cash Desk Status
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  //  Here data...
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  public class VoucherCashDeskStatus : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskStatus(PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskStatus");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources()
    {
      String value = "";
      TYPE_SPLITS _split_data;

      if (!Split.ReadSplitParameters(out _split_data))
      {
        return;
      }

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CASH_DESK_STATUS_TITLE");
      AddString("STR_VOUCHER_CASH_DESK_STATUS_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      // Cash desk close voucher
      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_NUMBERS");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_NUMBERS", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_OUT_DETAILS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_OUT_DETAILS", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_MB_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_MB_DEPOSIT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENT_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENT_AMOUNT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_PENDING_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_PENDING_AMOUNT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_INITIAL_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_INITIAL_AMOUNT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_DEPOSIT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_WITHDRAW", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SUB_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUB_TOTAL", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_PRIZES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON", value);

      value = Resource.String("STR_FRM_CASH_DESK_PRIZE_PAYED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_PAYED_WON", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_DEVOLUTIONS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_DEV", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TAXES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TAXES", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TOTAL_CASH_DESK") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TOTAL_CASH_DESK", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_NOT_REDEEMABLE") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_NOT_REDEEMABLE", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TAXES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TAX_TAXES", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET", value);

      value = Resource.String("STR_UC_BANK_HANDPAYS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TITLE", value);

      value = Resource.String("STR_VOUCHER_CARD_HANDPAY_005") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED", value);

      value = Resource.String("STR_VOUCHER_CARD_HANDPAY_014") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL", value);

      value = Resource.String("STR_UC_CARD_BALANCE_PROMO_TO_REDEEM") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM", value);

      //value = Resource.String("STR_UC_CARD_BALANCE_NOT_REDEEMABLE") + ":";
      value = _split_data.text_promo + ":";
      if (String.IsNullOrEmpty(value))
      {
        value = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION");
      }
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED", value);

    } // LoadVoucherResources

    #endregion

    #region Public Methods

    public void SetupCashStatusVoucher(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      Currency signed_amount;
      String _str_value;
      Currency _cs_mb_diff;
      Boolean _is_tito_mode;
      Boolean _gp_pending_cash_partial;

      _is_tito_mode = Utils.IsTitoMode();
      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      //  1. Depositos
      //  2. Retiros
      //  3. Entradas
      //  4. Salidas
      //     ---------------------------
      //  5. Saldo (Efectivo en Caja)
      // 
      //     Detalle Salidas
      //  6. Premios
      //  7. Impuestos
      //  8. Premios pagados
      // 
      //     ---------------------------

      //AddString("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_NUM", CashierSessionStats.total_initial_movs.ToString());
      //AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_AMT", CashierSessionStats.initial_balance);

      //  1. Depositos
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", CashierSessionStats.deposits);

      //  2. Retiros
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", CashierSessionStats.withdraws);

      //  3. Cash In
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.total_cash_in);

      //  4. Cash Out
      signed_amount = CashierSessionStats.total_cash_out; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", signed_amount);

      //  Diferencia Bancos M�viles
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF_MISSING") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _str_value);
      _cs_mb_diff = CashierSessionStats.mb_total_balance_lost + CashierSessionStats.mb_pending;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _cs_mb_diff);

      //  Sobrante Bancos M�viles
      if (_gp_pending_cash_partial)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL_CASH_DESK_STATUS") + ":";
      }
      else
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_OVER") + ":";
      }

      AddString("STR_VOUCHER_CASH_OVER", _str_value);
      AddCurrency("VOUCHER_CASH_OVER", CashierSessionStats.total_cash_over);

      //  Chech Payments
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", _str_value);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", CashierSessionStats.payment_orders);

      //Credit Card Exchange
      if (CashierSessionStats.total_bank_card > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.total_bank_card);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
      }

      //Check
      if (CashierSessionStats.total_check > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CHECK") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_CHECK", CashierSessionStats.total_check);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_CHECK", "");
      }

      // Currency Exchange
      if (CashierSessionStats.total_currency_exchange > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", CashierSessionStats.total_currency_exchange);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
      }

      //Commission Exchange
      if (CashierSessionStats.total_commission > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_COMMISSION", CashierSessionStats.total_commission);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
      }

      //Recharge for Points
      if (CashierSessionStats.points_as_cashin > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", CashierSessionStats.points_as_cashin);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
      }

      //Service Charge
      if (CashierSessionStats.service_charge > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", CashierSessionStats.service_charge);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
      }

      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET") + ":");
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_CASH_NET", CashierSessionStats.final_balance);

      //  6. Premios
      signed_amount = CashierSessionStats.prize_gross; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_AMT", signed_amount);

      // 7. Impuestos
      AddString("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_NUM", " ");
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_AMT", CashierSessionStats.prize_taxes);

      //  8. Premios
      signed_amount = CashierSessionStats.prize_payment; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_PAYED_AMT", signed_amount);

      //  9. Handpay
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED_AMT", CashierSessionStats.handpay_payment);
      signed_amount = CashierSessionStats.handpay_canceled; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED_AMT", signed_amount);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL_AMT", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

      // 10. Promotions
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM_AMT", CashierSessionStats.promo_nr_to_re);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM_AMT", CashierSessionStats.promo_nr);

      // 11. Tickets TITO
      AddInteger("VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED_ACCOUNT", CashierSessionStats.tickets_cancelled_count);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED_AMOUNT", CashierSessionStats.tickets_cancelled_amount);

      Voucher _voucher;
      _voucher = new WSI.Common.VoucherCurrencyCash(CashierSessionStats, false);

      SetParameterValue("@VOUCHER_CURRENCY_CASH", _voucher.VoucherHTML);
      //_voucher.VoucherHTML

      AddString("STR_MB_DEPOSITS_VISIBLE", _is_tito_mode ? "hidden" : "");
      AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");

    } // SetupCashStatusVoucher
    #endregion
  }
  // 9. Tito Ticket printing
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Origin Card Id: 
  // Origin Account Id:
  //
  // Destiny Card Id: 
  // Destiny Account Id:
  //
  // Initial Points Balance: $
  // Transfered Points Amount: $
  // Total Points:           $
  //
  //  Final Points Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  /// 

  public class VoucherTitoTicketPrintout : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructores

    public VoucherTitoTicketPrintout(String[] VoucherAccountInfo,
                                     Int64 TicketId,
                                     String ValidationNumber,
                                     Currency Amount,
                                     DateTime CreatedDateTime,
                                     DateTime ExpirationDateTime,
                                     Number PromotionId,
                                     String MrvFontPath,
                                     PrintMode Mode,
                                     SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Set specific params

      SetValue("CV_TYPE", (Int32)CashierVoucherType.TicketOut);
      //TODO: revisar si es posible utilizar esta columna
      SetValue("CV_SEQUENCE", TicketId);

      // 1. Load HTML structure.
      LoadVoucher("VoucherTitoTicket");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Set params
      SetParameterValue("@URL_TICKET_MRV_FONT", "'" + MrvFontPath + "'");

      SetParameterValue("@VOUCHER_TICKET_HEADER_1", "xINSERT THIS SIDE UP");
      SetParameterValue("@VOUCHER_TICKET_HEADER_2", "xUnreadable header text goes here");

      SetParameterValue("@VOUCHER_TICKET_INFO_1", TicketId.ToString());
      SetParameterValue("@VOUCHER_TICKET_INFO_2", "xGAMING VOUCHER");
      SetParameterValue("@VOUCHER_TICKET_INFO_3", Resource.String("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER") + ":&nbsp;" + ValidationNumber);
      SetParameterValue("@VOUCHER_TICKET_INFO_4", Resource.String("STR_UC_TICKET_CREATED_AT") + ":&nbsp;" + CreatedDateTime +
                                                  "&nbsp;&nbsp;" +
                                                  Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION_EXPIRED_DATE") + ":&nbsp;" + ExpirationDateTime);
      SetParameterValue("@VOUCHER_TICKET_INFO_5", "xInstructions on how to use this ticket go here here and here");
      SetParameterValue("@VOUCHER_TICKET_INFO_6", Resource.String("STR_UC_TICKET_QUANTITY") + ":&nbsp;" + Amount.ToString());
      SetParameterValue("@VOUCHER_TICKET_INFO_7", "xUnreadable text goes here. TDB");

      SetParameterValue("@VOUCHER_TICKET_FOOTER_1", "xCompany info 3 here");
      SetParameterValue("@VOUCHER_TICKET_FOOTER_2", "xCompany info 2 here");
      SetParameterValue("@VOUCHER_TICKET_FOOTER_3", "xCompany info 1 here");
      SetParameterValue("@VOUCHER_TICKET_FOOTER_4", "xINSERT THIS SIDE UP");
    }

    /// <summary>
    /// second version of constructor with dynamic parameters
    /// </summary>
    /// <param name="Params"></param>
    /// <param name="SQLTransaction"></param>
    //public VoucherTitoTicketPrintout(SmartParams Params, PrintMode Mode, SqlTransaction SQLTransaction)
    //  : base(Mode, SQLTransaction)
    //{
    //  // Constructor actions:
    //  //  1. Load passed params
    //  //  2. Load HTML structure.
    //  //  3. Load general voucher data.
    //  //  4. Set specific params

    //  //TODO: String[] VoucherAccountInfo = (String[])Params.Get("VoucherAccountInfo");
    //  String _html_space = ":&nbsp;";

    //  SetValue("CV_TYPE", Params.AsShort("CashierVoucherType"));
    //  //TODO: revisar si es posible utilizar esta columna
    //  SetValue("CV_SEQUENCE", Params.AsString("TicketId"));

    //  // 1. Load HTML structure.
    //  LoadVoucher("VoucherTitoTicket");

    //  // 2. Load general voucher data.
    //  LoadVoucherGeneralData();

    //  // 3. Set params
    //  SetParameterValue("@URL_TICKET_MRV_FONT", "'" + Params.AsString("MrvFontPath") + "'");

    //  SetParameterValue("@VOUCHER_TICKET_HEADER_1", "xINSERT THIS SIDE UP");
    //  SetParameterValue("@VOUCHER_TICKET_HEADER_2", "xUnreadable header text goes here");

    //  SetParameterValue("@BAR_CODE_FIRST", Params.AsString("ValidationNumber"));
    //  SetParameterValue("@BAR_CODE_SECOND", Params.AsString("ValidationNumber"));

    //  SetParameterValue("@VOUCHER_TICKET_INFO_1", Params.AsString("Title"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_2", Params.AsString("Name"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_3", Resource.String("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER") + _html_space +
    //                                              Params.AsString("ValidationNumber"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_4", Resource.String("STR_UC_TICKET_CREATED_AT") + _html_space +
    //                                              Params.AsString("CreationDate") + _html_space + _html_space +
    //                                              Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION_EXPIRED_DATE") + _html_space +
    //                                              Params.AsString("ExpirationDate"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_5", Params.AsString("Description"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_6", Resource.String("STR_UC_TICKET_QUANTITY") + _html_space +
    //                                              Params.AsString("Amount"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_7", "xUnreadable text goes here. TDB");

    //  SetParameterValue("@VOUCHER_TICKET_FOOTER_1", Params.AsString("Location"));
    //  SetParameterValue("@VOUCHER_TICKET_FOOTER_2", Params.AsString("Address1"));
    //  SetParameterValue("@VOUCHER_TICKET_FOOTER_3", Params.AsString("Address2"));
    //  SetParameterValue("@VOUCHER_TICKET_FOOTER_4", "xINSERT THIS SIDE UP");
    //}

    #endregion
  }

  // 9. Transfer Points
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Origin Card Id: 
  // Origin Account Id:
  //
  // Destiny Card Id: 
  // Destiny Account Id:
  //
  // Initial Points Balance: $
  // Transfered Points Amount: $
  // Total Points:           $
  //
  //  Final Points Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  /// 
  public class VoucherCardTransferPoints : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardTransferPoints(PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardTransferPoints");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_TITLE");
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_TRANSFER_POINTS_ORIGIN");
      AddString("STR_VOUCHER_TRANSFER_POINTS_ORIGIN", value);

      value = Resource.String("STR_VOUCHER_TRANSFER_POINTS_DESTINY");
      AddString("STR_VOUCHER_TRANSFER_POINTS_DESTINY", value);

      value = Resource.String("STR_VOUCHER_CARD_CARD_ID") + ":";
      AddString("STR_VOUCHER_CARD_CARD_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID") + ":";
      AddString("STR_VOUCHER_CARD_ACCOUNT_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_MB_ID") + ":";
      AddString("STR_VOUCHER_CARD_MB_ID", value);

      // Voucher Specific 
      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_INITIAL") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_INITIAL", value);

      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_TO_ADD") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TO_ADD", value);

      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_TRANSFERED") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TRANSFERED", value);

      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_FINAL") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_FINAL", value);

      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_TOTAL_AMOUNT");
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TOTAL_AMOUNT", value);

    }

    #endregion

    #region Public Methods

    #endregion
  }

  // 10. Card Replacement
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Recharge Amount: $
  // Total:           $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherCardReplacement : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardReplacement(PrintMode Mode, TYPE_CARD_REPLACEMENT Params, SqlTransaction SQLTransaction, Boolean CardPaid)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardReplacement");

      // 2. Load voucher resources.
      // RAB 24-MAR-2016: Bug 10745: Add input parameter to function to load card associate or card remplacement operation.
      LoadVoucherResources(CardPaid);

      if (WSI.Common.Misc.IsCardPlayerToCompanyB())
      {
        VoucherSequenceId = SequenceId.VouchersSplitB;
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CardReplacement_B);
        SetValue("CV_M01_BASE", Params.TotalBaseTaxSplit2);
        SetValue("CV_M01_TAX1", Params.TotalTaxSplit2);
      }
      else
      {
        VoucherSequenceId = SequenceId.VouchersSplitA;
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CardReplacement);
        SetValue("CV_M01_BASE", Params.TotalBaseTaxSplit2);
        SetValue("CV_M01_TAX1", Params.TotalTaxSplit2);
      }

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // 4. Load specific data.
      VoucherValuesCardReplacement(Params);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Boolean CardPaid)
    {
      String value = "";


      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":"; AddString("STR_VOUCHER_SITE_ID", value);
      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":"; AddString("STR_VOUCHER_TERMINAL_USERNAME", value);
      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":"; AddString("STR_VOUCHER_AUTHORIZED_BY", value);
      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":"; AddString("STR_VOUCHER_TERMINAL_ID", value);
      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":"; AddString("STR_VOUCHER_VOUCHER_ID", value);

      // Title
      value = CardPaid ? Resource.String("STR_VOUCHER_CARD_REPLACEMENT_001") : Resource.String("STR_FRM_CARD_ASSIGN_001"); AddString("STR_VOUCHER_TITLE", value);

      // Details
      value = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_002") + ":"; AddString("STR_VOUCHER_OLD_CARD_ID", value);
      value = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_006") + ":"; AddString("STR_VOUCHER_NEW_CARD_ID", value);
      value = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_003") + ":"; AddString("STR_VOUCHER_ACCOUNT_ID", value);
      value = CardPaid ? Resource.String("STR_VOUCHER_CARD_REPLACEMENT_004") + ":" : Resource.String("STR_VOUCHER_CARD_REDEEM_PLAYER_CARD") + ":"; AddString("STR_VOUCHER_CARD_REPLACEMENT_PRICE", value);

      // DHA 14-JUL-2015: Comissions
      value = Resource.String("STR_VOUCHER_COMISSION") + ":"; AddString("STR_VOUCHER_COMISSION", value);

      value = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_005"); AddString("STR_VOUCHER_CARD_REPLACEMENT_TOTAL_AMOUNT", value);
    }

    #endregion

    #region Private Methods

    private void VoucherValuesCardReplacement(TYPE_CARD_REPLACEMENT Params)
    {
      //  1. Dep�sito
      //     ---------------------------
      //  2. Total
      //     ---------------------------

      Currency _comissions;

      _comissions = 0;

      // DHA 14-JUL-2015 set comissions on card replacement
      if (Params.ExchangeResult != null)
      {
        _comissions = Params.ExchangeResult.Comission;
      }

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", Params.AccountInfo);
      AddCurrency("VOUCHER_CARD_REPLACEMENT_PRICE", Params.card_replacement_price);

      String _comisisons_html;


        AddCurrency("VOUCHER_CARD_REPLACEMENT_TOTAL_AMOUNT", Params.card_replacement_price + _comissions);

      _comisisons_html = "";

      if ((!Misc.IsCardPlayerToCompanyB() && _comissions > 0) || VoucherMode == 5)
      {
        _comisisons_html += "<tr>";
        _comisisons_html += "  <td align=\"left\" width=\"50%\">#STR_VOUCHER_COMISSION</td>";
        _comisisons_html += "  <td align=\"right\" width=\"50%\">#VOUCHER_COMISSION_AMOUNT</td>";
        _comisisons_html += "</tr>";

        _comisisons_html = _comisisons_html.Replace("#STR_VOUCHER_COMISSION", Resource.String("STR_VOUCHER_COMISSION"));
        _comisisons_html = _comisisons_html.Replace("#VOUCHER_COMISSION_AMOUNT", _comissions.ToString());
      }

      SetParameterValue("@VOUCHER_CARD_COMISSIONS", _comisisons_html);

      switch (VoucherMode)
      {
        case 5: //Logrand
          String _param_name_tax = GeneralParam.GetString("Cashier", "Split.B.Tax.Name");
          Decimal _param_pct_tax = GeneralParam.GetDecimal("Cashier", "Split.B.Tax.Pct");

          AddString("VOUCHER_TAX_DETAIL_SPLIT2", Resource.String("STR_TAX_DETAIL", _param_name_tax));
          AddString("VOUCHER_BASE_TAX_NAME_SPLIT2", Resource.String("STR_BASE"));
          String _pct = String.Format("{0:P2}", (_param_pct_tax / 100));
          AddString("VOUCHER_TAX_NAME_SPLIT2", _param_name_tax + " (" + _pct.Trim() + ")");
          AddCurrency("VOUCHER_BASE_TAX_SPLIT2", Params.TotalBaseTaxSplit2);
          AddCurrency("VOUCHER_TAX_SPLIT2", Params.TotalTaxSplit2);
          break;
      }

    } // VoucherValuesCardReplacement

    #endregion
  }

  // 11. Draw
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Initial Amount:   $
  // Open Amount:      $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  public class VoucherCardDraw : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardDraw(PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardDraw");

      // 2. Load voucher resources.
      LoadVoucherResources(false, false);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Boolean ShowDeposit, Boolean Promotion)
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CARD_DRAW_TITLE");
      AddString("STR_VOUCHER_CARD_DRAW_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER") + ":";
      AddString("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER", value);

    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Generates String Numbers - Voucher
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Parameter
    //          - FirstDrawNum
    //          - LastDrawNum
    //          - MaxNumber
    //          - HasBingoFormat
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true or false
    // 
    //   NOTES:
    public Boolean AddStringNumbers(String Parameter,
                                     Int64 FirstDrawNum,
                                     Int64 LastDrawNum,
                                     Int64 MaxNumber,
                                     Boolean HasBingoFormat)
    {
      Int64 _draw_num_mod;
      Int64 _idx_draw_num;
      String _table_draw_num;
      String _str_draw_num;
      Int32 _max_number_digits;
      String _font_size;


      // MaxNumber is used to define the digits to format the numbers.
      _max_number_digits = MaxNumber.ToString().Length;

      if (_max_number_digits >= 6)
      {
        _font_size = "100";
      }
      else
      {
        _font_size = "150";
      }

      if (HasBingoFormat)
      {
        _table_draw_num = "<td>";
      }
      else
      {
        _table_draw_num = "<td style=\"font-size:150%\">";
      }

      _table_draw_num += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" STYLE=\"font-size:" + _font_size + "%\" >";

      if (!HasBingoFormat)
      {
        _table_draw_num += "<tr>";
      }

      for (_idx_draw_num = FirstDrawNum; _idx_draw_num <= LastDrawNum; _idx_draw_num++)
      {
        _draw_num_mod = (_idx_draw_num - FirstDrawNum) % 2;

        if (HasBingoFormat)
        {
          _str_draw_num = DrawBingoFormat.NumberToBingo(_idx_draw_num, MaxNumber);
          _table_draw_num += "<tr><td align=\"center\">" + _str_draw_num + "</td></tr>";
          if (_idx_draw_num < LastDrawNum)
          {
            _table_draw_num += "<tr><td><table><tr><td>&nbsp;</td></tr></table></td></tr>";
          }
        }
        else
        {
          _str_draw_num = _idx_draw_num.ToString().PadLeft(_max_number_digits, '0');

          switch (_draw_num_mod)
          {
            case 0:
              _table_draw_num += "<td align=\"left\">" + _str_draw_num + "</td>";
              break;
            //case 1:
            //  _table_draw_num += "<td align=\"center\">" + _str_draw_num + "</td>";
            //  break;
            case 1:
              _table_draw_num += "<td align=\"right\">" + _str_draw_num + "</td>";
              if (_idx_draw_num < LastDrawNum)
              {
                _table_draw_num += "</tr><tr>";
              }
              break;
          }
        }
      }

      if (!HasBingoFormat)
      {
        _table_draw_num += "</tr>";
      }
      _table_draw_num += "</table></td>";

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, _table_draw_num);
    }

    #endregion
  }

  // 12. HandPay Generation
  // Format:
  //     Voucher Logo
  //    Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //    Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Handpay:         $
  // Final Balance:   $
  //
  //   Final Balance
  //       $$$$
  //
  //   Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherCardHandpay : Voucher
  {
    #region Class Attributes


    public class VoucherCardHandpayInputParams
    {
      public String[] VoucherAccountInfo;
      public String TerminalName;
      public String TerminalProvider;
      public HANDPAY_TYPE Type;
      public String TypeName;
      public Currency CardBalance;
      public Decimal Amount;
      public Currency SessionPlayedAmount;
      public String DenominacionStr;
      public String DenominacionValue;
      public PrintMode Mode;
      public VoucherValidationNumber ValidationNumber;
      public Decimal InitialAmount;
      public Decimal TaxBaseAmount;
      public Decimal TaxAmount;
      public Decimal TaxPct;
      public Decimal Deductions;
    }

    #endregion

    #region Constructor

    public VoucherCardHandpay(VoucherCardHandpayInputParams InputParams, OperationCode OperationCode, SqlTransaction SQLTransaction)
      : base(InputParams.Mode, OperationCode, SQLTransaction)
    {
      Boolean _gm_room;
      Boolean _sign_room;
      Boolean _print_played_amount;
      String _str_html;
      Currency _deductions;
      Currency _rounding;


      _deductions = -(InputParams.Deductions);

      _rounding = (InputParams.Amount - InputParams.InitialAmount) + InputParams.TaxAmount;

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardHandpay");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // Setup voucher's data
      //    - Account Number and Account Holder Name
      //    - Card Number (Trackdata) 
      //    - Initial Balance
      //    - Handpay Amount
      //    - Final Balance

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", InputParams.VoucherAccountInfo);

      AddString("VOUCHER_CARD_HANDPAY_TERMINAL_NAME", InputParams.TerminalName);
      AddString("VOUCHER_CARD_HANDPAY_PROVIDER_NAME", InputParams.TerminalProvider);
      AddString("VOUCHER_CARD_HANDPAY_TYPE", InputParams.TypeName);
      AddCurrency("VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", InputParams.Amount);

      if (Utils.IsTitoMode())
      {
        AddString("VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", "");
        AddString("VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", "");
        AddCurrency("VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", InputParams.InitialAmount);
        AddCurrency("VOUCHER_CARD_HANDPAY_DEDUCTIONS", _deductions);
      }
      else
      {
        AddCurrency("VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", InputParams.CardBalance);
        AddCurrency("VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", InputParams.CardBalance + InputParams.Amount);
        AddString("VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", "");
        AddString("VOUCHER_CARD_HANDPAY_DEDUCTIONS", "");
      }

      if (Utils.IsTitoMode() && _rounding != 0)
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_ROUNDING", Resource.String("STR_UC_CARD_BALANCE_ROUNDING") + ":");
        AddCurrency("VOUCHER_CARD_HANDPAY_ROUNDING", _rounding);
      }
      else
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_ROUNDING", "");
        AddString("VOUCHER_CARD_HANDPAY_ROUNDING", "");
      }

      if (InputParams.ValidationNumber != null)
      {
        _str_html = "<tr><td><p>";
        _str_html += Resource.String("STR_VOUCHER_VALIDATION_NUMBERS_TITLE");
        _str_html += "</p></td></tr>";

        _str_html += "<td colspan=\"2\" align=\"left\">@TICKET_TITO_TERMINAL_NAME</td>";
        _str_html = _str_html.Replace("@TICKET_TITO_TERMINAL_NAME", HttpUtility.HtmlEncode(Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + InputParams.ValidationNumber.CreatedTerminalName));

        _str_html += "<tr>";
        _str_html += "<td align=\"left\"  width=\"80%\">&nbsp&nbsp@VALIDATION_NUMBER</td>";
        _str_html += "<td align=\"right\" width=\"20%\">@VALUE</td>";
        _str_html += "</tr>";

        _str_html = _str_html.Replace("@VALIDATION_NUMBER", ValidationNumberManager.FormatValidationNumber(InputParams.ValidationNumber.ValidationNumber, (Int32)InputParams.ValidationNumber.ValidationType, true));
        _str_html = _str_html.Replace("@VALUE", HttpUtility.HtmlEncode(InputParams.ValidationNumber.Amount.ToString()));

        _str_html += "<tr><td>&nbsp;</td></tr>";
        SetParameterValue("@VALIDATION_NUMBERS", _str_html);
      }
      else
      {
        SetParameterValue("@VALIDATION_NUMBERS", "");
      }
      // If Manual Handpay return. Don't show any of the optional sections...
      if (InputParams.Type == HANDPAY_TYPE.MANUAL)
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_LEAVE_ROOM", "");
        SetParameterValue("@VOUCHER_CARD_HANDPAY_SIGNATURES_ROOM", "");
        SetParameterValue("@VOUCHER_CARD_HANDPAY_GAME_METERS_ROOM", "");
        SetParameterValue("@VOUCHER_CARD_HANDPAY_PRINT_PLAYED_AMOUNT", "");

        return;
      }

      // RCI 22-NOV-2011: Exists 3 GeneralParams to indicate if a section in the voucher has to be printed.
      _sign_room = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.SignaturesRoom");
      _gm_room = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.GameMetersRoom");
      _print_played_amount = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.PrintPlayedAmount");

      // If have to print any of them, leave extra space after final balance.
      if (_sign_room || _gm_room || _print_played_amount)
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_LEAVE_ROOM",
                     "<tr><td>&nbsp;</td></tr>" +
                     "<tr><td>&nbsp;</td></tr>");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_LEAVE_ROOM", "");
      }

      SetSignatureRoomByVoucherType(this.GetType());

      if (_gm_room)
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_GAME_METERS_ROOM",
          "<center><b>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_TITLE") + "</b></center>" +
          "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" +
            "<tr>" +
              "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_PLAYED") + "</td>" +
              "<td>$____________</td>" +
              "<td>#____________</td>" +
            "</tr>" +
            "<tr>" +
              "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_WON") + "</td>" +
              "<td>$____________</td>" +
              "<td>#____________</td>" +
            "</tr>" +
            "<tr>" +
              "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_JACKPOT") + "</td>" +
              "<td>$____________</td>" +
              "<td>&nbsp;</td>" +
            "</tr>" +
            "<tr>" +
              "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_DROP") + "</td>" +
              "<td>$____________</td>" +
              "<td>&nbsp;</td>" +
            "</tr>" +
          "</table>" +
          "<hr noshade size=\"4\">");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_GAME_METERS_ROOM", "");
      }

      if (_print_played_amount)
      {
        if (InputParams.DenominacionValue != "")
        {
          InputParams.DenominacionStr += ":";
        }

        SetParameterValue("@VOUCHER_CARD_HANDPAY_PRINT_PLAYED_AMOUNT",
          "<center><b>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_PRINT_PLAYED_AMOUNT_TITLE") + "</b></center>" +
          "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" +
            "<tr>" +
              "<td align=\"left\"  width=\"50%\">" + InputParams.DenominacionStr + "</td>" +
              "<td align=\"right\" width=\"50%\">" + InputParams.DenominacionValue + "</td>" +
            "</tr>" +
            "<tr>" +
              "<td align=\"left\"  width=\"50%\">" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_PLAYED_AMOUNT") + ":</td>" +
              "<td align=\"right\" width=\"50%\">" + (InputParams.SessionPlayedAmount == -1 ? "---" : InputParams.SessionPlayedAmount.ToString()) + "</td>" +
            "</tr>" +
          "</table>" +
          "<hr noshade size=\"4\">");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_PRINT_PLAYED_AMOUNT", "");
      }

      if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        String _param_name_tax;
        String _pct;
        StringBuilder _html_code;

        _html_code = new StringBuilder();

        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("	<td>");
        _html_code.AppendLine("		<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td colspan='3'>");
        _html_code.AppendLine("					@VOUCHER_TAX_DETAIL:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td width='5%'></td>");
        _html_code.AppendLine("				<td>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX_NAME:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("				<td width='40%' align='right'>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			@TAXES");
        _html_code.AppendLine("		</table>");
        _html_code.AppendLine("		<hr noshade size='4' /> ");
        _html_code.AppendLine("	</td>");
        _html_code.AppendLine("</tr>");

        SetParameterValue("@TABLE_WITHHOLDING", _html_code.ToString());

        _param_name_tax = Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING");
        _pct = String.Format("{0:P2}", InputParams.TaxPct);

        AddString("VOUCHER_TAX_DETAIL", Resource.String("STR_HANDPAY_TAX_DETAIL", _param_name_tax.ToString()));
        AddString("VOUCHER_BASE_TAX_NAME", Resource.String("STR_BASE"));
        SetParameterValue("@TAXES", DevolutionPrizeParts.GetTaxesHtml(InputParams.TaxBaseAmount, true, true, false, (_deductions != 0), OperationCode));

        // MPO 17-MAR-2015: WIG-2154: It isn't necessary show the base amount when there aren't tax amount.
        AddCurrency("VOUCHER_BASE_TAX", InputParams.TaxAmount != 0 ? InputParams.TaxBaseAmount : 0);

        SetParameterValue("@HR_CASHLESS_SEPARATOR", String.Empty);
      }
      else
      {
        StringBuilder _html_code;

        _html_code = new StringBuilder();

        _html_code.AppendLine("<hr noshade size='4' />");

        SetParameterValue("@TABLE_WITHHOLDING", String.Empty);
        SetParameterValue("@HR_CASHLESS_SEPARATOR", _html_code.ToString());
      }

    } // VoucherCardHandpay

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images
      // AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_001"));

      // Details
      AddString("STR_VOUCHER_CARD_HANDPAY_TERMINAL_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_008") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_PROVIDER_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_009") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_TYPE", Resource.String("STR_VOUCHER_CARD_HANDPAY_010") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_005") + ":");

      if (Utils.IsTitoMode())
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_001") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_DEDUCTIONS", Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING") + ":");
        AddString("STR_VOUCHER_FINAL_TITLE", "");
      }
      else
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_004") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_006") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_DEDUCTIONS", "");
        AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_007"));
      }
    }

    #endregion

    #region Public Methods

    //public void VoucherValuesCardHandpay(CashierBusinessLogic.TYPE_CARD_HANDPAY Params)
    //{
    //  //  1. Dep�sito
    //  //     ---------------------------
    //  //  2. Total
    //  //     ---------------------------

    //  //Params.account_id
    //  //Params.track_data
    //  //Params.amount
    //  //Params.terminal_name
    //  //Params.provider_name

    //  //AddString("VOUCHER_OLD_CARD_ID", Params.old_track_data);
    //  //AddString("VOUCHER_NEW_CARD_ID", Params.new_track_data);
    //  //AddString("VOUCHER_ACCOUNT_ID", Params.account_id.ToString());
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_PRICE", Params.card_replacement_price);
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_TOTAL_AMOUNT", Params.card_replacement_price);

    //} // VoucherValuesCardHandpay

    #endregion
  }

  // 13. HandPay Cancellation 
  // Format:
  //     Voucher Logo
  //    Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //    Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Handpay:         $
  // Final Balance:   $
  //
  //   Final Balance
  //       $$$$
  //
  //   Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherCardHandpayCancellation : Voucher
  {
    #region Class Attributes

    public class VoucherCardHandpayCancellationInputParams
    {
      public String[] VoucherAccountInfo;
      public String TerminalName;
      public String TerminalProvider;
      public HANDPAY_TYPE Type;
      public Int32 Level;
      public Decimal InitialAmount;
      public Decimal FinalAmount;
      public Decimal HandpayAmount;
    }

    #endregion

    #region Constructor

    public VoucherCardHandpayCancellation(VoucherCardHandpayCancellationInputParams InputParams,
                                          PrintMode Mode,
                                          OperationCode OperationCode,
                                          SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardHandpayCancellation");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", InputParams.VoucherAccountInfo);
      AddString("VOUCHER_CARD_HANDPAY_TERMINAL_NAME", InputParams.TerminalName);
      AddString("VOUCHER_CARD_HANDPAY_PROVIDER_NAME", InputParams.TerminalProvider);
      AddString("VOUCHER_CARD_HANDPAY_TYPE", Handpays.HandpayTypeString(InputParams.Type, InputParams.Level));
      AddCurrency("VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", InputParams.InitialAmount);
      AddCurrency("VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", InputParams.HandpayAmount);
      AddCurrency("VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", InputParams.FinalAmount);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_TYPE", Resource.String("STR_VOUCHER_CARD_HANDPAY_010") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_013"));

      // Details
      AddString("STR_VOUCHER_CARD_HANDPAY_TERMINAL_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_008") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_PROVIDER_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_009") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_004") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_006") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_014") + ":");
      AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_007"));
    }

    #endregion

    #region Public Methods

    //public void VoucherValuesCardHandpayCancellation(CashierBusinessLogic.TYPE_CARD_HANDPAY Params)
    //{
    //  //  1. Dep�sito
    //  //     ---------------------------
    //  //  2. Total
    //  //     ---------------------------

    //  //Params.account_id
    //  //Params.track_data
    //  //Params.amount
    //  //Params.terminal_name
    //  //Params.provider_name

    //  //AddString("VOUCHER_OLD_CARD_ID", Params.old_track_data);
    //  //AddString("VOUCHER_NEW_CARD_ID", Params.new_track_data);
    //  //AddString("VOUCHER_ACCOUNT_ID", Params.account_id.ToString());
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_PRICE", Params.card_replacement_price);
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_TOTAL_AMOUNT", Params.card_replacement_price);

    //} // VoucherValuesCardHandpayCancellation

    #endregion
  }

  public class VoucherTitoHandpayCancellation : Voucher
  {
    #region Class Attributes

    public class VoucherTitoHandpayCancellationInputParams
    {
      public String[] VoucherAccountInfo;
      public String TerminalName;
      public String TerminalProvider;
      public HANDPAY_TYPE Type;
      public Int32 Level;
      public Currency PayAmount;
      public Currency TotalAmount;
      public Currency Deductions;
    }

    #endregion

    #region Constructor

    public VoucherTitoHandpayCancellation(VoucherTitoHandpayCancellationInputParams InputParams,
                                          PrintMode Mode,
                                          SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTITOHandpayCancelation");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", InputParams.VoucherAccountInfo);
      AddString("VOUCHER_TERMINAL_NAME", InputParams.TerminalName);
      AddString("VOUCHER_PROVIDER_NAME", InputParams.TerminalProvider);
      AddString("VOUCHER_TYPE", Handpays.HandpayTypeString(InputParams.Type, InputParams.Level));
      AddCurrency("VOUCHER_VOIDING", InputParams.PayAmount);
      AddCurrency("VOUCHER_WITHHOLDING", InputParams.Deductions);
      AddCurrency("VOUCHER_VOIDED_PAYMENT", InputParams.TotalAmount);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");
      AddString("STR_VOUCHER_TYPE", Resource.String("STR_VOUCHER_CARD_HANDPAY_010") + ":");
      AddString("STR_VOUCHER_WITHHOLDING", Resource.String("STR_DEDUCTIONS") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_013"));

      // Details
      AddString("STR_VOUCHER_TERMINAL_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_008") + ":");
      AddString("STR_VOUCHER_PROVIDER_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_009") + ":");

      AddString("STR_VOUCHER_VOIDING", Resource.String("STR_VOUCHER_HANDPAY_CANCELATION_01") + ":");
      AddString("STR_VOUCHER_VOIDED_PAYMENT", Resource.String("STR_VOUCHER_HANDPAY_CANCELATION_02") + ":");

      AddString("STR_VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_014") + ":");
      AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_007"));

    }

    #endregion

    #region Public Methods

    //public void VoucherValuesCardHandpayCancellation(CashierBusinessLogic.TYPE_CARD_HANDPAY Params)
    //{
    //  //  1. Dep�sito
    //  //     ---------------------------
    //  //  2. Total
    //  //     ---------------------------

    //  //Params.account_id
    //  //Params.track_data
    //  //Params.amount
    //  //Params.terminal_name
    //  //Params.provider_name

    //  //AddString("VOUCHER_OLD_CARD_ID", Params.old_track_data);
    //  //AddString("VOUCHER_NEW_CARD_ID", Params.new_track_data);
    //  //AddString("VOUCHER_ACCOUNT_ID", Params.account_id.ToString());
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_PRICE", Params.card_replacement_price);
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_TOTAL_AMOUNT", Params.card_replacement_price);

    //} // VoucherValuesCardHandpayCancellation

    #endregion
  }

  // 14. Gift Request 
  // Format:
  //     Voucher Logo
  //    Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //    Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Points Balance: $
  // Consumed Points:        $
  // Final Points Balance:   $
  //
  //        Gift Name
  //
  //       Gift Number
  //
  //   Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>

  public class VoucherGiftRequest : Voucher
  {
    #region Constructor

    public VoucherGiftRequest(String[] VoucherAccountInfo,
                               Points InitialPoints,
                               Points SpentPoints,
                               GIFT_TYPE GiftType,
                               String GiftNumber,
                               String GiftName,
                               Currency NotRedeemableCredits,
                               Decimal GiftUnits,
                               DateTime ExpirationDateTime,
                               PrintMode Mode,
                               SqlTransaction SQLTransaction)

      : base(Mode, SQLTransaction)
    {
      string[] _message_params = { "", "", "", "", "", "" };

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.
      //  4. Load voucher parameters

      // 1. Load HTML structure.
      LoadVoucher("VoucherGiftRequest");

      // RCI 20-JUN-2011: For gifts that are delivered immediately, don't show either the Expiration Date or the Trackdata.
      //                  Also, change the Voucher Title, to indicate that the gift is delivered immediately, not only requested.
      if (GiftType == GIFT_TYPE.NOT_REDEEMABLE_CREDIT
        || GiftType == GIFT_TYPE.REDEEMABLE_CREDIT
        || GiftType == GIFT_TYPE.DRAW_NUMBERS)
      {
        AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_GIFT_DELIVERY_001"));

        AddString("STR_VOUCHER_GIFT_REQUEST_EXPIRATION", "");
        AddString("VOUCHER_GIFT_REQUEST_EXPIRATION", "");
        AddString("STR_VOUCHER_GIFT_REQUEST_TRACKDATA", "");
        AddString("VOUCHER_GIFT_REQUEST_TRACKDATA", "");
      }

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      //  4. Load voucher parameters
      //      - Account Info
      //      - Initial Points Balance
      //      - Spent Points
      //      - Final Points Balance
      //      - Gift Name
      //      - Voucher track data

      Points _final_points;

      //      - Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //      - Initial Points Balance
      AddString("VOUCHER_GIFT_REQUEST_INITIAL_POINTS", InitialPoints.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Spent Points
      AddString("VOUCHER_GIFT_REQUEST_SPENT_POINTS", SpentPoints.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Final Points Balance
      _final_points = InitialPoints - SpentPoints;
      AddString("VOUCHER_GIFT_REQUEST_FINAL_POINTS", _final_points.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Gift Name
      switch (GiftType)
      {
        case GIFT_TYPE.OBJECT:
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", GiftName);
          break;

        case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
          // @p0 cr�ditos no redimibles
          _message_params[0] = NotRedeemableCredits.ToString();
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", Resource.String(("STR_VOUCHER_GIFT_REQUEST_003"), _message_params));
          break;

        case GIFT_TYPE.REDEEMABLE_CREDIT:
          // @p0 cr�ditos redimibles
          _message_params[0] = NotRedeemableCredits.ToString();
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", Resource.String(("STR_VOUCHER_GIFT_REQUEST_011"), _message_params));
          break;

        case GIFT_TYPE.DRAW_NUMBERS:
          AddMultipleLineString("VOUCHER_GIFT_REQUEST_GIFT_NAME", GiftName + "\n" + GiftUnits.ToString() + " " + Resource.String("STR_VOUCHER_GIFT_REQUEST_010"));
          break;

        case GIFT_TYPE.SERVICES:
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", GiftName);
          break;

        default:
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", "");
          break;
      }

      //      - Voucher track data
      AddString("VOUCHER_GIFT_REQUEST_TRACKDATA", GiftNumber);

      //      - Expiration date
      AddDateTime("VOUCHER_GIFT_REQUEST_EXPIRATION", ExpirationDateTime);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_GIFT_REQUEST_001"));

      // Details
      AddString("STR_VOUCHER_GIFT_REQUEST_INITIAL_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_004") + ":");
      AddString("STR_VOUCHER_GIFT_REQUEST_SPENT_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_005") + ":");
      AddString("STR_VOUCHER_GIFT_REQUEST_FINAL_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_006") + ":");
      AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_GIFT_REQUEST_007"));
      AddString("STR_VOUCHER_GIFT_REQUEST_TRACKDATA", Resource.String("STR_VOUCHER_GIFT_REQUEST_002"));
      AddString("STR_VOUCHER_GIFT_REQUEST_EXPIRATION", Resource.String("STR_VOUCHER_GIFT_REQUEST_008") + ":");
    }

    #endregion

    #region Public Methods
    #endregion
  }

  // 15. Gift Delivery
  // Format:
  //     Voucher Logo
  //    Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //    Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Points Balance: $
  // Consumed Points:        $
  // Final Points Balance:   $
  //
  //        Gift Name
  //
  //       Gift Number
  //
  //   Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>

  public class VoucherGiftDelivery : Voucher
  {
    #region Constructor

    public VoucherGiftDelivery(String[] VoucherAccountInfo,
                                String GiftNumber,
                                String GiftName,
                                PrintMode Mode,
                                SqlTransaction SQLTransaction)

      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.
      //  4. Load voucher parameters

      // 1. Load HTML structure.
      LoadVoucher("VoucherGiftDelivery");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      //  4. Load voucher parameters
      //      - Account Info
      //      - Initial Points Balance
      //      - Spent Points
      //      - Gift Name
      //      - Voucher track data

      //      - Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //      - Gift Name
      AddString("VOUCHER_GIFT_DELIVERY_NUMBER", GiftNumber);
      AddString("VOUCHER_GIFT_DELIVERY_GIFT_NAME", GiftName);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_GIFT_DELIVERY_001"));   // "Entrega de Regalo"

      // Details
      AddString("STR_VOUCHER_GIFT_DELIVERY_NUMBER", Resource.String("STR_VOUCHER_GIFT_DELIVERY_002") + ":");
      AddString("STR_VOUCHER_GIFT_DELIVERY_REQUEST_DATE", Resource.String("STR_VOUCHER_GIFT_DELIVERY_004") + ":");
      AddString("STR_VOUCHER_GIFT_DELIVERY_GIFT_NAME", Resource.String("STR_VOUCHER_GIFT_DELIVERY_003") + ":");
    }

    #endregion

    #region Public Methods
    #endregion
  }

  public class VoucherGiftCancellation : Voucher
  {
    #region Constructor

    public VoucherGiftCancellation(String[] VoucherAccountInfo,
                                   Points InitialPoints,
                                   Points SpentPoints,
                                   GIFT_TYPE GiftType,
                                   String GiftNumber,
                                   String GiftName,
                                   Currency NotRedeemableCredits,
                                   Decimal GiftUnits,
                                   PrintMode Mode,
                                   SqlTransaction SQLTransaction)

      : base(Mode, SQLTransaction)
    {
      string[] _message_params = { "", "", "", "", "", "" };

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.
      //  4. Load voucher parameters

      // 1. Load HTML structure.
      LoadVoucher("VoucherGiftCancellation");

      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_GIFT_CANCELLATION_001"));

      AddString("STR_VOUCHER_GIFT_REQUEST_TRACKDATA", "");
      AddString("VOUCHER_GIFT_REQUEST_TRACKDATA", "");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      //  4. Load voucher parameters
      //      - Account Info
      //      - Initial Points Balance
      //      - Spent Points
      //      - Final Points Balance
      //      - Gift Name
      //      - Voucher track data

      Points _final_points;

      //      - Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //      - Initial Points Balance
      AddString("VOUCHER_GIFT_REQUEST_INITIAL_POINTS", InitialPoints.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Spent Points
      AddString("VOUCHER_GIFT_REQUEST_SPENT_POINTS", SpentPoints.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Final Points Balance
      _final_points = InitialPoints + SpentPoints;
      AddString("VOUCHER_GIFT_REQUEST_FINAL_POINTS", _final_points.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Gift Name
      switch (GiftType)
      {
        case GIFT_TYPE.OBJECT:
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", GiftName);
          break;

        case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
          // @p0 cr�ditos no redimibles
          _message_params[0] = NotRedeemableCredits.ToString();
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", Resource.String(("STR_VOUCHER_GIFT_REQUEST_003"), _message_params));
          break;

        case GIFT_TYPE.REDEEMABLE_CREDIT:
          // @p0 cr�ditos redimibles
          _message_params[0] = NotRedeemableCredits.ToString();
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", Resource.String(("STR_VOUCHER_GIFT_REQUEST_011"), _message_params));
          break;

        case GIFT_TYPE.DRAW_NUMBERS:
          AddMultipleLineString("VOUCHER_GIFT_REQUEST_GIFT_NAME", GiftName + "\n" + GiftUnits.ToString() + " " + Resource.String("STR_VOUCHER_GIFT_REQUEST_010"));
          break;

        case GIFT_TYPE.SERVICES:
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", GiftName);
          break;

        default:
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", "");
          break;
      }

      //      - Voucher track data
      AddString("VOUCHER_GIFT_REQUEST_TRACKDATA", GiftNumber);

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_GIFT_REQUEST_001"));

      // Details
      AddString("STR_VOUCHER_GIFT_REQUEST_INITIAL_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_004") + ":");
      AddString("STR_VOUCHER_GIFT_REQUEST_SPENT_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_005") + ":");
      AddString("STR_VOUCHER_GIFT_REQUEST_FINAL_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_006") + ":");
      AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_GIFT_REQUEST_007"));
      AddString("STR_VOUCHER_GIFT_REQUEST_TRACKDATA", Resource.String("STR_VOUCHER_GIFT_REQUEST_002"));
      AddString("STR_VOUCHER_GIFT_REQUEST_EXPIRATION", Resource.String("STR_VOUCHER_GIFT_REQUEST_008") + ":");
    }

    #endregion

    #region Public Methods
    #endregion
  }

  // 16. Generate PIN
  // Format:
  //     Voucher Logo
  //    Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:  
  // Cusotomer:  
  // Account Id:
  // Card Id: 
  //
  //      New PIN
  //
  //   Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherGeneratePIN : Voucher
  {
    #region Constructor

    public VoucherGeneratePIN(String[] VoucherAccountInfo
                            , Int64 AccountId
                            , TYPE_SPLITS SplitInformation
                            , String NewPin
                            , PrintMode Mode
                            , SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.

      // 1. Load HTML structure.
      LoadVoucher("Voucher.PIN.Change");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.     
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      AddString("VOUCHER_CARD_CHANGE_PIN_INFO", Resource.String("STR_VOUCHER_CARD_CHANGE_PIN_NEW_PIN"));
      AddString("VOUCHER_CARD_CHANGE_PIN_NEW_PIN", NewPin);

    } // VoucherCashIn

    #endregion
  }

  // 17. Recharges Summary
  // Format:
  //    Voucher Header
  //  
  //      Recharges List
  //
  //   Voucher Footer
  public class VoucherRechargesSummary : Voucher
  {
    #region Constructor

    public VoucherRechargesSummary(String[] VoucherAccountInfo,
                                   String[] NotesList,
                                   Currency TotalRecharges,
                                   Int64 AccountId,
                                   PrintMode Mode,
                                   SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.

      // 1. Load HTML structure.
      LoadVoucher("Voucher.Recharges.Summary");

      LoadHeader(GeneralParam.Value("WigosKiosk", "Voucher.Recharges.Header"));
      LoadFooter(GeneralParam.Value("WigosKiosk", "Voucher.Recharges.Footer"), "");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      AddString("VOUCHER_RECHARGES_SUMMARY_TITLE", Resource.String("STR_VOUCHER_RECHARGES_SUMMARY_TITLE"));

      AddString("VOUCHER_RECHARGES_SUMMARY_LIST", Resource.String("STR_VOUCHER_RECHARGES_SUMMARY_LIST"));

      AddString("VOUCHER_RECHARGES_SUMMARY_NAME", Resource.String("STR_VOUCHER_RECHARGES_SUMMARY_NAME"));

      AddMultipleLineString("VOUCHER_NOTES_LIST", NotesList);

      AddCurrency("VOUCHER_TOTAL_RECHARGES", TotalRecharges);

    } // VoucherCashIn

    #endregion
  }

  public class VoucherPromoLost : Voucher
  {
    #region Constructor

    public VoucherPromoLost(String[] VoucherAccountInfo
                          , Int64 AccountId
                          , TYPE_SPLITS SplitInformation
                          , PrintMode Mode
                          , DataTable Promotions
                          , Boolean IsReward
                          , Boolean SingleTicket
                          , OperationCode OperationCode
                          , String PromotionalTitoTicket
                          , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {
      MultiPromos.PromoBalance PromotionBalance;
      DataRow[] _promos;
      String _detail;
      String _title_voucher;
      Boolean _add_details;

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.
      //  4. Detail of promotion

      // 1. Load HTML structure.
      LoadVoucher("Voucher.Promo.Lost");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // DHA 17-DEC-20104: added sequence to promo lost voucher
      VoucherSequenceId = SequenceId.VouchersSplitA;

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.     
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      if (!String.IsNullOrEmpty(PromotionalTitoTicket) && SingleTicket)
      {
        AddString("PROMOTIONAL_TITO_TICKET", Resource.String("STR_VOUCHER_PROMO_PROMOTIONAL_TITO_TICKET") + ": " + PromotionalTitoTicket);
      }
      else
      {
        AddString("PROMOTIONAL_TITO_TICKET", "");
      }

      // 4. Specific data

      // Title
      // DHA 29-OCT-2013: set the promotion voucher title from general params
      _title_voucher = IsReward ? GeneralParam.GetString("Cashier.Voucher", "VoucherOnPromotionAwarded.Title") : GeneralParam.GetString("Cashier.Voucher", "VoucherOnPromotionCancelled.Title");

      if (String.IsNullOrEmpty(_title_voucher))
      {
        _title_voucher = IsReward ? Resource.String("STR_VOUCHER_PROMO_REWARD_TITLE") : Resource.String("STR_VOUCHER_PROMO_CANCEL_TITLE");
      }

      AddString("PROMO_ACTION", _title_voucher);

      // Distributed among different parameters
      SetParameterValue("@VOUCHER_PROMO_DETAILS",
      "@VOUCHER_PROMO_NR_STRING" +
      "\n@VOUCHER_PROMO_NR_DETAIL" +
      "\n@VOUCHER_PROMO_NR_TOTAL" +
      "\n@VOUCHER_PROMO_UNR_STRING" +
      "\n@VOUCHER_PROMO_UNR_DETAIL" +
      "\n@VOUCHER_PROMO_UNR_TOTAL" +
      "\n@VOUCHER_PROMO_RE_STRING" +
      "\n@VOUCHER_PROMO_RE_DETAIL" +
      "\n@VOUCHER_PROMO_RE_TOTAL" +
      "\n@VOUCHER_PROMO_URE_STRING" +
      "\n@VOUCHER_PROMO_URE_DETAIL" +
      "\n@VOUCHER_PROMO_URE_TOTAL" +
      "\n@VOUCHER_PROMO_PT_STRING" +
      "\n@VOUCHER_PROMO_PT_DETAIL" +
      "\n@VOUCHER_PROMO_PT_TOTAL");

      // Details
      // * VOUCHER_PROMO_NR_DETAIL
      // * VOUCHER_PROMO_RE_DETAIL
      // * VOUCHER_PROMO_PT_DETAIL
      _detail = "";
      PromotionBalance = MultiPromos.PromoBalance.Zero;

      // RCI 05-SEP-2012: Only print promotions with balance > 0. This can happen with Point Promotions.
      _promos = Promotions.Select("ACP_BALANCE > 0", "ACP_CREDIT_TYPE");


      Currency _balance;
 //     Currency _balance_promo_ure;
      Object _footer;
      Currency _promo_unr;
      Currency _promo_ure;

      _promo_unr = 0;
      _promo_ure = 0;

      foreach (DataRow _promo in _promos)
      {
        _balance = (Decimal)_promo["ACP_BALANCE"];
        //_balance_promo_ure = (_promo["ACP_PRIZE_GROSS"] == DBNull.Value ? 0.0M : (Decimal)_promo["ACP_PRIZE_GROSS"]);
        _detail = NewRow("@PROMO_NAME", "@BALANCE", true);
        _detail += "@VOUCHER_PROMO_TAX"; // Add tax if necessary
        _detail += "@VOUCHER_PROMO_STATE_TAX"; 
        _detail += "@VOUCHER_PROMO_FEDERAL_TAX"; 

        _footer = _promo["ACP_TICKET_FOOTER"];

        if ((_footer != DBNull.Value) && _footer.ToString().Trim() != "")
        {
          _detail += NewFooterRow(); // @PROMO_FOOTER 
        }


        switch ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"])
        {
          case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
          case ACCOUNT_PROMO_CREDIT_TYPE.NR2:

            if (_promo.Table.Columns.Contains("ACP_PROMO_TYPE") && _promo.Table.Columns.Contains("ACP_DETAILS"))
            {
              if (_promo["ACP_PROMO_TYPE"] != DBNull.Value
                  && _promo["ACP_DETAILS"] != DBNull.Value)
              {
                Promotion.PROMOTION_TYPE _type = ((Promotion.PROMOTION_TYPE)_promo["ACP_PROMO_TYPE"]);

                _add_details = false;

                switch (_type)
                {
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_01:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_02:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_03:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_04:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_05:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_06:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_07:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_08:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_09:
                    if (_promo["ACP_DETAILS"].ToString().Trim() != "")
                    {
                      _add_details = true;
                    }
                    break;
                }

                if (_add_details)
                {
                  _detail += NewPromoDetailRow(_promo["ACP_DETAILS"].ToString().Trim());
                }
              }
            }

            // Taxes from account
            if (_promo.Table.Columns.Contains("ACP_PRIZE_TYPE") &&
                _promo["ACP_PRIZE_TYPE"] != DBNull.Value &&
                ((AccountPromotion.PrizeType)_promo["ACP_PRIZE_TYPE"] == AccountPromotion.PrizeType.InKind1_TaxesFromAccount ||
                (AccountPromotion.PrizeType)_promo["ACP_PRIZE_TYPE"] == AccountPromotion.PrizeType.InKind2_TaxesFromSite))
            {
              _promo_unr += _balance;

              _detail += "@VOUCHER_PROMO_UNR_DETAIL"; // Continue details if necessary
              SetParameterValue("@VOUCHER_PROMO_UNR_DETAIL", _detail);

              // Set gross amount 
              SetParameterValue("@BALANCE", _balance.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.UNRLabel"));
              AddPromotionTaxes(_promo);
            }
            else
            {
              PromotionBalance.Balance.PromoNotRedeemable += _balance;

              _detail += "@VOUCHER_PROMO_NR_DETAIL"; // Continue details if necessary
              SetParameterValue("@VOUCHER_PROMO_NR_DETAIL", _detail);

              AddCurrency("BALANCE", _balance);
            }

            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
            PromotionBalance.Balance.Redeemable += _balance;

            if (_promo.Table.Columns.Contains("ACP_PROMO_TYPE") && _promo.Table.Columns.Contains("ACP_DETAILS"))
            {
              if (_promo["ACP_PROMO_TYPE"] != DBNull.Value
                  && _promo["ACP_DETAILS"] != DBNull.Value)
              {
                if (((Promotion.PROMOTION_TYPE)_promo["ACP_PROMO_TYPE"]) == Promotion.PROMOTION_TYPE.EXTERNAL_REDEEM && _promo["ACP_DETAILS"].ToString().Trim() != "")
                {
                  _detail += NewPromoDetailRow(_promo["ACP_DETAILS"].ToString().Trim());
                }
              }
            }

            _detail += "@VOUCHER_PROMO_RE_DETAIL"; // Continue details if necessary
            SetParameterValue("@VOUCHER_PROMO_RE_DETAIL", _detail);
            //
            AddCurrency("BALANCE", (Decimal)_promo["ACP_BALANCE"]);

            //AVZ 23-FEB-2016 - Add state and federal taxes to redeemable promotion
            //AddRedeemablePromotionTaxes(_promo, PromotionBalance.Balance);

            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:
            //PromotionBalance.Balance.Redeemable += _balance;

            if (_promo.Table.Columns.Contains("ACP_PROMO_TYPE") && _promo.Table.Columns.Contains("ACP_DETAILS"))
            {
              if (_promo["ACP_PROMO_TYPE"] != DBNull.Value
                  && _promo["ACP_DETAILS"] != DBNull.Value)
              {
                Promotion.PROMOTION_TYPE _type = ((Promotion.PROMOTION_TYPE)_promo["ACP_PROMO_TYPE"]);
                _detail += NewPromoDetailRow(_promo["ACP_DETAILS"].ToString().Trim());

              }
            }

            // Taxes from account
            if (_promo.Table.Columns.Contains("ACP_PRIZE_TYPE") &&
                _promo["ACP_PRIZE_TYPE"] != DBNull.Value &&
                ((AccountPromotion.PrizeType)_promo["ACP_PRIZE_TYPE"] == AccountPromotion.PrizeType.InKind1_TaxesFromAccount ||
                (AccountPromotion.PrizeType)_promo["ACP_PRIZE_TYPE"] == AccountPromotion.PrizeType.InKind2_TaxesFromSite))
            {
              _promo_ure += _balance;

              _detail += "@VOUCHER_PROMO_URE_DETAIL"; // Continue details if necessary
              SetParameterValue("@VOUCHER_PROMO_URE_DETAIL", _detail);

              //
              //SetParameterValue("@BALANCE", _balance_promo_ure.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.UNRLabel"));
              SetParameterValue("@BALANCE", _balance.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.UNRLabel"));
              AddPromotionTaxes(_promo);
            }
            else
            {
              PromotionBalance.Balance.PromoNotRedeemable += _balance;

              _detail += "@VOUCHER_PROMO_URE_DETAIL"; // Continue details if necessary
              SetParameterValue("@VOUCHER_PROMO_URE_DETAIL", _detail);

              AddCurrency("BALANCE", _balance);
            }
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
            PromotionBalance.Points += _balance;
            _detail += "@VOUCHER_PROMO_PT_DETAIL"; // Continue details if necessary
            SetParameterValue("@VOUCHER_PROMO_PT_DETAIL", _detail);
            //
            AddPoint("BALANCE", (Decimal)_promo["ACP_BALANCE"]);
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
          default:
            break;
        }

        AddString("PROMO_NAME", (String)_promo["ACP_PROMO_NAME"]);
        if ((_footer != DBNull.Value) && _footer.ToString().Trim() != "")
        {
          AddMultipleLineString("PROMO_FOOTER", _footer.ToString());
        }

        SetParameterValue("@VOUCHER_PROMO_TAX", "");
        SetParameterValue("@VOUCHER_PROMO_STATE_TAX", "");
        SetParameterValue("@VOUCHER_PROMO_FEDERAL_TAX", "");

      }

      // Clear parameters
      SetParameterValue("@VOUCHER_PROMO_NR_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_UNR_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_RE_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_URE_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_PT_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_TAX", "");
      SetParameterValue("@VOUCHER_PROMO_STATE_TAX", "");
      SetParameterValue("@VOUCHER_PROMO_FEDERAL_TAX", "");

      // Totals
      // VOUCHER_PROMO_NR_STRING
      // VOUCHER_PROMO_NR_TOTAL

      // VOUCHER_PROMO_RE_STRING
      // VOUCHER_PROMO_RE_TOTAL

      // VOUCHER_PROMO_PT_STRING
      // VOUCHER_PROMO_PT_TOTAL

      // No redeemable promotion.
      if (PromotionBalance.Balance.TotalNotRedeemable > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_NR_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_NR_PROMO_LOST") : "");

        // Total
        _detail = LineTotal();
        _detail += NewRow("@TOTAL_STR", "@BALANCE", true);
        _detail += NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_NR_TOTAL", _detail);
        AddString("TOTAL_STR", "Total");
        AddCurrency("BALANCE", PromotionBalance.Balance.TotalNotRedeemable);
      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_NR_TOTAL", "");
        SetParameterValue("@VOUCHER_PROMO_NR_STRING", "");
      }

      // UNR Promotions
      if (_promo_unr.SqlMoney > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_UNR_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_UNR_PROMO_LOST") : "");

        // DHA 02-MAR-2016: removed total line for UNR
        // Total
        _detail = NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_UNR_TOTAL", _detail);

      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_UNR_TOTAL", "");
        SetParameterValue("@VOUCHER_PROMO_UNR_STRING", "");
      }

      // Redeemable Promotions.
      if (PromotionBalance.Balance.Redeemable > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_RE_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_RE_PROMO_LOST") : "");

        // Total
        _detail = LineTotal();
        _detail += NewRow("@TOTAL_STR", "@BALANCE", true);
        _detail += NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_RE_TOTAL", _detail);
        AddString("TOTAL_STR", "Total");
        AddCurrency("BALANCE", PromotionBalance.Balance.Redeemable);
      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_RE_STRING", "");
        SetParameterValue("@VOUCHER_PROMO_RE_TOTAL", "");
      }

      // URE Promotions
      if (_promo_ure.SqlMoney > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_URE_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_URE_PROMO_LOST") : "");

        // Total
        _detail = LineTotal();
        _detail += NewRow("@TOTAL_STR", "@BALANCE", true);
        _detail += NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_URE_TOTAL", _detail);
        AddString("TOTAL_STR", "Total");
        SetParameterValue("@BALANCE", _promo_ure.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.UNRLabel"));
      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_URE_TOTAL", "");
        SetParameterValue("@VOUCHER_PROMO_URE_STRING", "");
      }

      // Points Promotions.
      if (PromotionBalance.Points > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_PT_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_PT_PROMO_LOST") : "");

        //Total
        _detail = LineTotal();
        _detail += NewRow("@TOTAL_STR", "@BALANCE", true);
        _detail += NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_PT_TOTAL", _detail);
        AddString("TOTAL_STR", "Total");
        AddPoint("BALANCE", PromotionBalance.Points);
      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_PT_TOTAL", "");
        SetParameterValue("@VOUCHER_PROMO_PT_STRING", "");
      }

      // Signature if is canceled promotions

      SetSignatures();

    }
    /*
    private void AddRedeemablePromotionTaxes(DataRow Promo, MultiPromos.AccountBalance AccountBalance)
    {
      if (Promo.Table.Columns.Contains("ACP_PRIZE_PROMO_RED_APPLY_TAX") &&
                Promo["ACP_PRIZE_PROMO_RED_APPLY_TAX"] != DBNull.Value &&
                (bool)Promo["ACP_PRIZE_PROMO_RED_APPLY_TAX"] )
      {
        decimal _federal_tax = (decimal)Promo["ACP_PRIZE_PROMO_RED_FEDERAL_TAX"];
        if (_federal_tax != 0)
        {
          string _detail_federal_tax = NewRow("@FEDERAL_TAX_STR", "@FEDERAL_TAX_VALUE", true);
          _detail_federal_tax += "@VOUCHER_PROMO_FEDERAL_TAX";
          SetParameterValue("@VOUCHER_PROMO_FEDERAL_TAX", _detail_federal_tax);

          AddString("FEDERAL_TAX_STR", Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name"));
          AddCurrency("FEDERAL_TAX_VALUE", _federal_tax * -1);

          AccountBalance.Redeemable -= _federal_tax;
        }

        decimal _state_tax = (decimal)Promo["ACP_PRIZE_PROMO_RED_STATE_TAX"];
        if (_state_tax != 0)
        {
          string _detail_state_tax = NewRow("@STATE_TAX_STR", "@STATE_TAX_VALUE", true);
          _detail_state_tax += "@VOUCHER_PROMO_STATE_TAX";
          SetParameterValue("@VOUCHER_PROMO_STATE_TAX", _detail_state_tax);

          AddString("STATE_TAX_STR", Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name"));
          AddCurrency("STATE_TAX_VALUE", _state_tax * -1);

          AccountBalance.Redeemable -= _state_tax;
    }
      }      
    }*/

    private String NewRow(String NameKey, String NameValue, Boolean IsPromo)
    {
      return
         "<tr>" +
   "<td align=\"left\" width=\"65%\" colspan=\"2\"> " + (IsPromo ? "" : "<b>") + NameKey + (IsPromo ? ":" : "</b>") + "</td>" +
   "<td align=\"right\" width=\"35%\">" + NameValue + "</td>" +
   "</tr>";
    }

    private String NewPromoDetailRow(String PromoDetail)
    {
      return "<tr><td colspan=\"2\" align=\"left\" width=\"100%\"><i>" + PromoDetail + "</i></td></tr>";
    }

    private String NewFooterRow()
    {
      return NewRow("&nbsp;", "", false) +
        "<tr><td colspan=\"2\" align=\"left\" width=\"100%\"><font size=\"1\"><i>@PROMO_FOOTER</i></font></td></tr>" +
        NewRow("&nbsp;", "", false);
    }

    private String LineTotal()
    {
      return "<tr><td colspan=\"3\"><hr noshade size=\"1\"></td></tr>";
    }

    private void SetSignatures()
    {
      SetParameterValue("@VOUCHER_PROMO_SIGNATURES",
      "<center><b>" + Resource.String("STR_VOUCHER_CLIENT_SIGN") + "</b></center>" +
      "<br />" +
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" +
        "<tr><td>&nbsp;</td></tr>" +
        "<tr><td>&nbsp;</td></tr>" +
      "</table>" +
      "<hr noshade size=\"4\">");
    }

    private String NewRowTax(String NameKey, String NameValue, Boolean IsPromo, Boolean WithTab)
    {
      if (!WithTab)
      {
        NewRow(NameKey, NameValue, IsPromo);
      }
      return
         "<tr>" +
   "<td align=\"right\" width=\"5%\"></td>" +
   "<td align=\"left\" width=\"60%\">" + (IsPromo ? "" : "<b>") + NameKey + (IsPromo ? ":" : "</b>") + "</td>" +
   "<td align=\"right\" width=\"35%\">" + NameValue + "</td>" +
   "</tr>";
    }

    private void AddPromotionTaxes(DataRow Promo)
    {
      String _detail;
      Int32 _factor;

      _detail = String.Empty;
      _factor = 1;

      for (int _idx = 1; _idx <= 2; _idx++)
      {
        if (Promo["ACP_PRIZE_TAX" + _idx] != DBNull.Value)
        {
          if (GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind." + _idx + ".Pct", 0m) != 0)
          {
            _detail = NewRowTax("@PROMO_TAX", "@TAX_VALUE", true, true);
            _detail += "@VOUCHER_PROMO_TAX"; // Add tax if necessary

            SetParameterValue("@VOUCHER_PROMO_TAX", _detail);
            SetParameterValue("@PROMO_TAX", Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind." + _idx + ".Name"));

            // DHA 29-MAY-2015 added to set the taxes to negative value
            if (GeneralParam.GetBoolean("Cashier", "Tax.OnPrizeInKind.SetNegative", false))
            {
              _factor = -1;
            }

            AddCurrency("TAX_VALUE", (Decimal)Promo["ACP_PRIZE_TAX" + _idx] * _factor);
          }
        }
      }

      SetParameterValue("@VOUCHER_PROMO_TAX", "");
    }

    #endregion
  }

  // 18. Lottery Summary
  // Format:
  //    Voucher Header
  //  
  //      Lottery info
  //
  //   Voucher Footer
  public class VoucherPrizeCouponDraw : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherPrizeCouponDraw(String[] VoucherAccountInfo,
                                  PrintMode Mode,
                              SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherPrizeCouponDraw");

      // 2. Load voucher resources.
      LoadVoucherResources(VoucherAccountInfo);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo)
    {
      Currency _draw_prize;

      _draw_prize = 0;

      // - Title
      AddString("VOUCHER_PRIZE_COUPON_DRAW_TITLE", "Comprobante de sorteo"); //TODO Resource

      SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_VOUCHER_ID", "Recibo de venta: 75338<br/>N�mero sorteo: 10920<br/>Fecha sorteo: 08/11/2012 18:21:47<br/><br/>");

      // - Selection numbers
      SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_SELECTION_NUMBERS", "<center><B><font size=\"5\">04 07 14 19</font></B></center><br/>"); //TODO

      // - Drawn numbers
      SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_DRAWN_NUMBERS", "Extracto<br/><center><B>01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20</B></center><br/>"); //TODO

      // - Prize //TODO: Parametro con el premio
      if (_draw_prize > 0)
      {
        SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_TOTAL", "Premio: " + _draw_prize);  //TODO Resource
      }
      else
      {
        SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_TOTAL", "Sin premio"); //TODO Resource
      }

      // - AcccountInfo
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);
    }

    #endregion

    #region Public Methods

    #endregion
  }

  public class VoucherCardAddCurrencyExchange : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor


    public VoucherCardAddCurrencyExchange(String[] VoucherAccountInfo
                                          , Int64 AccountId
                                          , TYPE_SPLITS Splits
                                          , CurrencyExchangeResult ExchangeResult
                                          , CurrencyExchangeType ExchangeType
                                          , String NationalCurrency
                                          , OperationCode Operation
                                          , PrintMode Mode
                                          , Boolean ShowOnlyComission
                                          , SqlTransaction Trx)
      : base(Mode, Trx)
    {
      String _str_aux;
      String _voucher_to_load;

      _str_aux = "";

      // Constructor actions:
      //  0. Select the voucher to load by MovementType
      //  1. Load HTML structure.
      //  2. Load Header and Footer
      //  3. Load general voucher data.
      //  4. Load NLS strings from resource.
      //  5. Load specific voucher data.

      //  0. Select the voucher to load by MovementType

      if (ShowOnlyComission && Operation == OperationCode.CASH_ADVANCE)
      {
        _voucher_to_load = "Voucher.B.CashIn";
      }
      else
      {
        switch (ExchangeType)
        {
          case CurrencyExchangeType.CARD:
            _voucher_to_load = "VoucherCardAddBankCard";
            break;
          case CurrencyExchangeType.CHECK:
            _voucher_to_load = "VoucherCardAddCheck";
            break;
          case CurrencyExchangeType.CURRENCY:
            _voucher_to_load = "VoucherCardAddCurrencyExchange";
            break;
          default:
            return;
        }
      }

      // 1. Load HTML structure.
      LoadVoucher(_voucher_to_load);

      SetValue("CV_ACCOUNT_ID", AccountId);

      //  2. Load Header and Footer
      if (ShowOnlyComission && Operation == OperationCode.CASH_ADVANCE)
      {
        LoadHeader(Splits.company_b.header);
        LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);

        switch (ExchangeType)
        {
          case CurrencyExchangeType.CARD:
            AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.BankCard.VoucherTitle", Splits.company_b.cash_in_voucher_title));
            break;
          case CurrencyExchangeType.CHECK:
            AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Check.VoucherTitle", Splits.company_b.cash_in_voucher_title));
            break;
          case CurrencyExchangeType.CURRENCY:
            AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Currency.VoucherTitle", Splits.company_b.cash_in_voucher_title));
            break;
        }

        AddString("VOUCHER_SPLIT_NAME", Splits.company_b.cash_in_voucher_title);
        AddString("VOUCHER_CONCEPT", Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION"));
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CashIn_B);

        AddString("STR_VOUCHER_CASH_IN_TAX", String.Empty);
        AddString("VOUCHER_CASH_IN_TAX", String.Empty);

        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT",
            "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
              "<tr>" +
                "<td align=\"center\">TOTAL</td>" +
              "</tr>" +
              "<tr>" +
                "<td align=\"center\"><B>@VOUCHER_CARD_ADD_TOTAL_AMOUNT</B></td>" +
              "</tr>" +
            "</table>" +
            "<hr noshade size=\"4\">");

        AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", ExchangeResult.Comission, Splits.company_b.total_in_letters);
      }
      else
      {
        LoadHeader(Splits.company_a.header);
        LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);
      }

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      if (ShowOnlyComission && Operation == OperationCode.CASH_ADVANCE)
      {
        AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", ExchangeResult.Comission);
        _str_aux = GeneralParam.GetString("Cashier", "CashInTax.VoucherText", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX")) + ":";
        AddString("STR_VOUCHER_CASH_IN_TAX", _str_aux);
      }
      else
      {
        // Change applied
        if (ExchangeResult.WasForeingCurrency)
        {
          _str_aux = Resource.String("STR_VOUCHER_APPLIED_EXCHANGE");

          SetParameterValue("@VOUCHER_CHANGE_APPLIED",
          "<tr>" +
            "<td align=\"left\"  width=\"35%\">" + _str_aux + " " + ExchangeResult.InCurrencyCode + ":</td>" +
            "<td align=\"right\" width=\"65%\">" + CurrencyExchange.ChangeRateFormat(ExchangeResult.ChangeRate) + " " + NationalCurrency +
            "</td>" +
          "</tr>");

          // Total Currency Exchange
          AddString("VOUCHER_EQUIVAL_CHANGE_AMOUNT", Currency.Format(ExchangeResult.GrossAmount, NationalCurrency));
          _str_aux = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS");
          AddString("STR_VOUCHER_EQUIVAL_CHANGE", _str_aux);

          //  5. Load specific voucher data.
          // In amount
          AddString("VOUCHER_IN_AMOUNT", Currency.Format(ExchangeResult.InAmount, ExchangeResult.InCurrencyCode));

          // Card price
          AddString("VOUCHER_CARD_PRICE_AMOUNT", Currency.Format(ExchangeResult.CardPrice, NationalCurrency));

          // Commission
          AddString("VOUCHER_COMISSION_AMOUNT", Currency.Format(ExchangeResult.Comission, NationalCurrency));

          // NR2 promotion. Control 0 value for promotion
          if (ExchangeResult.ArePromotionsEnabled)
          {
            AddString("VOUCHER_NR2_AMOUNT", Currency.Format(ExchangeResult.NR2Amount, NationalCurrency));
            _str_aux = Resource.String("STR_VOUCHER_NR2") + ":";
          }
          else
          {
            AddString("VOUCHER_NR2_AMOUNT", "");
            _str_aux = "";
          }
          AddString("STR_VOUCHER_NR2", _str_aux);

          // Total amount
          AddString("VOUCHER_TOTAL_AMOUNT", Currency.Format(ExchangeResult.NetAmount, NationalCurrency));
        }
        else
        {
          AddString("VOUCHER_CHANGE_APPLIED", "");
          AddString("VOUCHER_EQUIVAL_CHANGE_AMOUNT", "");
          AddString("STR_VOUCHER_EQUIVAL_CHANGE", "");

          //  5. Load specific voucher data.
          // In amount
          AddString("VOUCHER_IN_AMOUNT", ((Currency)ExchangeResult.InAmount).ToString());

          // Card price
          AddString("VOUCHER_CARD_PRICE_AMOUNT", ((Currency)ExchangeResult.CardPrice).ToString());

          // Commission
          AddString("VOUCHER_COMISSION_AMOUNT", ((Currency)ExchangeResult.Comission).ToString());

          // NR2 promotion. Control 0 value for promotion
          if (ExchangeResult.ArePromotionsEnabled && Operation == OperationCode.CASH_IN)
          {
            AddString("VOUCHER_NR2_AMOUNT", ((Currency)ExchangeResult.NR2Amount).ToString());
            _str_aux = Resource.String("STR_VOUCHER_NR2") + ":";
          }
          else
          {
            AddString("VOUCHER_NR2_AMOUNT", "");
            _str_aux = "";
          }
          AddString("STR_VOUCHER_NR2", _str_aux);

          // Total amount
          AddString("VOUCHER_TOTAL_AMOUNT", ((Currency)ExchangeResult.NetAmount).ToString());
        }

      }
      if (_voucher_to_load == "VoucherCardAddCurrencyExchange")
      {
        AddString("CURRENCY_STR_VOUCHER_TOTAL_AMOUNT", "");
        AddString("CURRENCY_VOUCHER_TOTAL_AMOUNT", "");
      }
      if (_voucher_to_load == "VoucherCardAddCurrencyExchange")
      {
        AddString("CURRENCY_STR_VOUCHER_TOTAL_AMOUNT", "");
        AddString("CURRENCY_VOUCHER_TOTAL_AMOUNT", "");
      }

      //  4. Load NLS strings from resource
      if (!LoadVoucherResources(ExchangeType, Operation))
      {
        return;
      }

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private Boolean LoadVoucherResources(CurrencyExchangeType ExchangeType, OperationCode Operation)
    {
      String value = "";

      // NLS Values

      // - Title
      switch (ExchangeType)
      {
        case CurrencyExchangeType.CURRENCY:
          value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.Currency.Title");
          if (String.IsNullOrEmpty(value))
          {
            value = Resource.String("STR_VOUCHER_CURRENCY_EXCHANGE");
          }
          AddString("STR_VOUCHER_TITLE", value);
          break;
        case CurrencyExchangeType.CARD:
          switch (Operation)
          {
            case OperationCode.CASH_IN:
              value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.BankCard.Title");
              if (String.IsNullOrEmpty(value))
              {
                value = Resource.String("STR_VOUCHER_CURRENCY_CARD_EXCHANGE");
              }
              break;
            case OperationCode.CASH_ADVANCE:
              value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.CashAdvance.BankCard.Title");
              if (String.IsNullOrEmpty(value))
              {
                value = Resource.String("STR_VOUCHER_CURRENCY_CARD_CASH_ADVANCE");
              }
              break;
          }

          AddString("STR_VOUCHER_TITLE", value);
          break;
        case CurrencyExchangeType.CHECK:
          switch (Operation)
          {
            case OperationCode.CASH_IN:
              value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.Check.Title");
              if (String.IsNullOrEmpty(value))
              {
                value = Resource.String("STR_VOUCHER_CURRENCY_CHECK_EXCHANGE");
              }
              break;
            case OperationCode.CASH_ADVANCE:
              value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.CashAdvance.Check.Title");
              if (String.IsNullOrEmpty(value))
              {
                value = Resource.String("STR_VOUCHER_CURRENCY_CHECK_CASH_ADVANCE");
              }
              break;
          }
          AddString("STR_VOUCHER_TITLE", value);
          break;
        case CurrencyExchangeType.CASINOCHIP:
          AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_CASINO_CHIPS_EXCHANGE"));
          break;
        default:
          Log.Error("VoucherCardAddCurrencyExchange. ExchangeType not valid: " + ExchangeType.ToString());
          return false;
      }

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_IN_CHARGED") + ":";
      AddString("STR_VOUCHER_IN_AMOUNT", value);

      value = Misc.GetCardPlayerConceptName() + ":";
      AddString("STR_VOUCHER_CARD_PRICE", value);

      value = Resource.String("STR_VOUCHER_COMISSION") + ":";
      AddString("STR_VOUCHER_COMISSION", value);

      switch (Operation)
      {
        case OperationCode.CASH_IN:
          value = Resource.String("STR_VOUCHER_RECHARGE") + ":";
          break;
        case OperationCode.CASH_ADVANCE:
          value = Resource.String("STR_VOUCHER_CASH_ADVANCE") + ":";
          break;
        default:
          value = "";
          break;
      }
      AddString("STR_VOUCHER_TOTAL_AMOUNT", value);




      return true;
    }

    #endregion
  }




  // 19. Card Holder Data
  // Format:
  //    Voucher Header
  //  
  //      Card Holder info
  //
  //   Voucher Footer
  public class VoucherCardHolderUpdate : Voucher
  {

    #region Constructor

    public VoucherCardHolderUpdate(CardData Account,
      //String[] VoucherCardHolderNewData,
                                  PrintMode Mode,
                              SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      String[] _voucher_card_holder_info;
      String[] _voucher_client_data;
      String _nls_string;

      _voucher_card_holder_info = Account.VoucherAccountInfo();

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.

      // 1. Load HTML structure.
      LoadHeader(GeneralParam.Value("Cashier.Voucher", "Header"));
      LoadVoucher("Voucher.AccountCustomize");
      LoadFooter(GeneralParam.Value("Cashier.Voucher", "AccountCustomize.Footer"), "");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.
      _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_CLIENT_DATA_TITLE");
      AddString("VOUCHER_ACCOUNT_CUSTOMIZE_CLIENT_DATA_TITLE", _nls_string);

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", _voucher_card_holder_info);

      _voucher_client_data = GenerateVoucherDataInfo(Account);
      AddMultipleLineString("VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA", _voucher_client_data);

    } // VoucherCardHolderUpdate

    #endregion

    #region Private Methods

    private String GetDocumentTypeString(ACCOUNT_HOLDER_ID_TYPE HolderIdType)
    {
      return IdentificationTypes.GetIdentificationName(HolderIdType); ;
    } // GetDocumentTypeString

    private String GetHolderFedEntity(Int64 FedEntityId)
    {
      DataTable _dt_fed_entities;
      String _fed_entity;

      _fed_entity = "";

      try
      {
        if (!States.GetStates(out _dt_fed_entities, Resource.CountryISOCode2))
        {
          return _fed_entity;
        }

        // Search the federal entity
        foreach (DataRow _dr in _dt_fed_entities.Rows)
        {
          if ((int)_dr.ItemArray[0] == FedEntityId)
          {
            _fed_entity = (String)_dr.ItemArray[1];
            break;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _fed_entity;
    } // GetHolderFedEntity

    private String GetConditionalString(String Prefix, String Text, String Suffix)
    {
      if (Text == "")
      {
        return "";
      }

      return Prefix + Text + Suffix;
    } // GetConditionalString

    private String[] GenerateVoucherDataInfo(CardData Account)
    {
      String[] _data_info;
      String _nls_string;
      String _fed_entity;
      const int _max_num_lines = 17;
      int _idx_line;

      _idx_line = 0;
      _data_info = new String[_max_num_lines];

      // Name
      if (GeneralParam.GetBoolean("Account.VisibleField", "Name3", true))
      {
        _nls_string = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderName3);
      }

      // Name 4
      if (GeneralParam.GetBoolean("Account.VisibleField", "Name4", true))
      {
        _nls_string = Resource.String("STR_FRM_PLAYER_EDIT_NAME4");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderName4);
      }

      // Name 1
      if (GeneralParam.GetBoolean("Account.VisibleField", "Name1", true))
      {
        _nls_string = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderName1);
      }

      // Name 2
      if (GeneralParam.GetBoolean("Account.VisibleField", "Name2", true))
      {
        _nls_string = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderName2);
      }

      // HolderBirthDate
      if (GeneralParam.GetBoolean("Account.VisibleField", "BirthDate", true))
      {
        if (!DateTime.MinValue.Equals(Account.PlayerTracking.HolderBirthDate) && !String.IsNullOrEmpty(Account.PlayerTracking.HolderBirthDateText))
        {
          _nls_string = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_DATE");
          _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderBirthDateText);
        }
      }

      // HolderBirthCountry
      if (GeneralParam.GetBoolean("Account.VisibleField", "BirthCountry", true))
      {
        _nls_string = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_COUNTRY");
        _data_info[_idx_line++] = DataInfo(_nls_string, CardData.CountriesString((Int32)Account.PlayerTracking.HolderBirthCountry));
      }

      // HolderNationality
      if (GeneralParam.GetBoolean("Account.VisibleField", "Nationality", true))
      {
        _nls_string = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NATIONALITY");
        _data_info[_idx_line++] = DataInfo(_nls_string, CardData.NationalitiesString((Int32)Account.PlayerTracking.HolderNationality));
      }

      // HolderId1 (R.F.C.)
      if (IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.RFC))
      {
        _nls_string = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC);
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderId1);
      }

      // HolderId1 (C.U.R.P.)
      if (IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.CURP))
      {
        _nls_string = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP);
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderId2);
      }

      if (Account.PlayerTracking.HolderIdType != ACCOUNT_HOLDER_ID_TYPE.RFC && Account.PlayerTracking.HolderIdType != ACCOUNT_HOLDER_ID_TYPE.CURP)
      {
        // Document type
        if (GeneralParam.GetBoolean("Account.VisibleField", "Document", true))
        {
          _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_DOCUMENT_TYPE");
          _data_info[_idx_line++] = DataInfo(_nls_string, GetDocumentTypeString(Account.PlayerTracking.HolderIdType));

          // Document id
          _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_DOCUMENT_NUMBER");
          _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderId);
        }
      }

      // Address
      _fed_entity = GetHolderFedEntity(Account.PlayerTracking.HolderFedEntity);
      _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_ADDRESS");
      String _address = "";
      Boolean _address_visible = false;
      if (GeneralParam.GetBoolean("Account.VisibleField", "Address", true))
      {
        _address = GetConditionalString("", Account.PlayerTracking.HolderAddress01, ""); // Street
        _address_visible = true;
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "ExtNum", true))
      {
        _address += GetConditionalString(_address_visible ? " " : "", Account.PlayerTracking.HolderExtNumber, ""); // Ext Number
        _address_visible = true;
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "City", true))
      {
        _address += GetConditionalString(_address_visible ? " " : "", Account.PlayerTracking.HolderCity, ""); // Municipio
        _address_visible = true;
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "Address02", true))
      {
        _address += GetConditionalString(_address_visible ? ", " : "", Account.PlayerTracking.HolderAddress02, ""); // Colonia
        _address_visible = true;
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "Address03", true))
      {
        _address += GetConditionalString(_address_visible ? ", " : "", Account.PlayerTracking.HolderAddress03, ""); // Delegacion
        _address_visible = true;
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "FedEntity", true))
      {
        _address += GetConditionalString(_address_visible ? ", " : "", _fed_entity, ""); // Fed entity
        _address_visible = true;
      }
      if (_address_visible)
      {
        _data_info[_idx_line++] = DataInfo(_nls_string, _address);
      }

      // Postal code
      if (GeneralParam.GetBoolean("Account.VisibleField", "Zip", true))
      {
        _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_ZIP_CODE");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderZip);
      }

      // Phone number
      if (GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true))
      {
        _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_PHONE_1");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderPhone01);
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
      {
        if (Account.PlayerTracking.HolderPhone02 != "")
        {
          _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_PHONE_2");
          _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderPhone02);
        }
      }

      // Occupation
      if (GeneralParam.GetBoolean("Account.VisibleField", "Occupation", true))
      {
        _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_OCCUPATION");
        _data_info[_idx_line++] = DataInfo(_nls_string, CardData.OccupationsDescriptionString(Account.PlayerTracking.HolderOccupationId));
      }

      if (WSI.Common.Misc.HasBeneficiaryAndNotAMLExternalControl())
      {
        _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_BENEFICIARY");
        if (Account.PlayerTracking.HolderHasBeneficiary)
        {
          _data_info[_idx_line++] = _nls_string + Account.PlayerTracking.BeneficiaryName1
                        + " " + Account.PlayerTracking.BeneficiaryName2
                        + " " + Account.PlayerTracking.BeneficiaryName3;
        }
      }

      return _data_info;

    } // GenerateVoucherDataInfo

    private String DataInfo(String Title, String Data)
    {
      return Title.TrimEnd(new Char[] { ':', ' ' }) + ": " + (String.IsNullOrEmpty(Data) ? "---" : Data);
    }
    #endregion

  }

  // 20. Draw Cashier Ticket
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  // 
  // Draw number:
  // Prize:
  //          Voucher Title
  //
  // Numbers
  //
  //         Voucher Footer
  public class VoucherDrawCashDesk : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherDrawCashDesk(String[] VoucherAccountInfo,
                              Result CashierDrawResult,
                          Int64 OperationId,
                          Boolean ComplementaryVoucher,
                          OperationCode OperationCode,
                          PrintMode Mode,
                          SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashierDraw");

      // DHA 17-DEC-20104: added sequence to cash desk draw voucher
      VoucherSequenceId = SequenceId.VouchersSplitA;

      // Set parameters voucher
      if (!ComplementaryVoucher)
      {
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CashDeskDraw_Winner);
      }
      else
      {
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CashDeskDraw_Loser);
      }

      // 2. Load voucher resources.
      LoadVoucherResources(VoucherAccountInfo, ComplementaryVoucher);

      if (Mode == PrintMode.Print)
      {
        VoucherHTML = VoucherHTML.Replace("@VOUCHER_DRAW_INFO", AddCashDeskInfo(CashierDrawResult, ComplementaryVoucher));
      }
      else
      {
        String _html_string;
        _html_string = String.Empty;

        _html_string += @"</BR>";
        _html_string += @"<div style=""font: bold 14px arial; text-align:center"">";
        _html_string += Resource.String("STR_VOUCHER_CASHIER_DRAW_WILL_BE_RUN");
        _html_string += @"</BR></BR>";
        _html_string += @"<hr noshade size=""4"">";

        VoucherHTML = VoucherHTML.Replace("@VOUCHER_DRAW_INFO", _html_string);
      }

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

    }

    public VoucherDrawCashDesk(Result CashDeskDrawResult,
                               Boolean ComplementaryVoucher)
    {
      VoucherHTML = AddCashDeskInfo(CashDeskDrawResult, ComplementaryVoucher);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    /// 
    private void LoadVoucherResources(String[] VoucherAccountInfo, Boolean ComplementaryVoucher)
    {
      String _value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      if (ComplementaryVoucher)
      {
        _value = CashDeskDrawBusinessLogic.GetCashDeskDrawVoucherComplementaryTitle();
      }
      else
      {
        _value = CashDeskDrawBusinessLogic.GetCashDeskDrawVoucherTitle();
      }
      AddString("STR_VOUCHER_CAHIER_DRAW_TITLE", _value);

      // - General
      _value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", _value);

      _value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", _value);

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      _value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", _value);

    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Generates String Account Info and Numbers - Voucher
    // 
    //  PARAMS:
    //      - INPUT:
    //          - VoucherAccountInfo
    //          - DrawNumbersPlayer
    //          - DrawNumbersSystem
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true or false
    // 
    //   NOTES:
    public String AddCashDeskInfo(Result CashDeskDrawResult,
                                Boolean ComplementaryVoucher)
    {
      String _html_info;
      Currency _amount;
      Int32 _factor;

      _html_info = String.Empty;
      _factor = 1;

      _html_info += @"<div style=""font: normal 14px arial"">";

      if (ComplementaryVoucher)
      {
        // DHA 12-FEB-2014: Added general param to configure loser prize label
        _amount = CashDeskDrawResult.m_is_winner ? 0m : CashDeskDrawResult.m_nr_awarded + CashDeskDrawResult.m_re_awarded;
        _html_info += Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.LoserPrizeLabel") + ": " + _amount.ToString();
        _html_info += @"<hr noshade size=""4"">";

        SetValue("CV_M01_BASE", _amount);
      }
      else
      {
        _html_info += Resource.String("STR_VOUCHER_CASHIER_DRAW_NUMBER") + ": " + CashDeskDrawResult.m_draw_id;
        _html_info += @"</BR>";

        _amount = CashDeskDrawResult.m_is_winner ? CashDeskDrawResult.m_nr_awarded + CashDeskDrawResult.m_re_awarded : 0m;

        // DHA 12-FEB-2014: Added general param to configure winner prize label
        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.UNR || CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.ReInKind)
        {
          _html_info += Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.WinnerPrizeLabel") + ": " + _amount.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.UNRLabel");
        }
        else
        {
          _html_info += Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.WinnerPrizeLabel") + ": " + _amount.ToString();
        }

        SetValue("CV_M01_BASE", _amount);

        // DHA 29-MAY-2015 added to set the taxes to negative value
        if (GeneralParam.GetBoolean("Cashier", "Tax.OnPrizeInKind.SetNegative", false))
        {
          _factor = -1;
        }

        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.UNR && CashDeskDrawResult.m_unr_tax1 != 0)
        {
          _html_info += @"</BR>";
          _html_info += Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.1.Name") + ": " + (CashDeskDrawResult.m_unr_tax1 * _factor).ToString();
        }

        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.UNR && CashDeskDrawResult.m_unr_tax2 != 0)
        {
          _html_info += @"</BR>";
          _html_info += Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.2.Name") + ": " + (CashDeskDrawResult.m_unr_tax2 * _factor).ToString();
        }
        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.ReInKind && CashDeskDrawResult.m_ure_tax1 != 0)
        {
          _html_info += @"</BR>";
          _html_info += Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.1.Name") + ": " + (CashDeskDrawResult.m_ure_tax1 * _factor).ToString();
        }

        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.ReInKind && CashDeskDrawResult.m_ure_tax2 != 0)
        {
          _html_info += @"</BR>";
          _html_info += Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.2.Name") + ": " + (CashDeskDrawResult.m_ure_tax2 * _factor).ToString();
        }
        _html_info += @"<hr noshade size=""4"">";
        _html_info += AddStringNumbers(CashDeskDrawResult.m_bet_numbers, CashDeskDrawResult.m_winning_combination);
        _html_info += "</div>";
      }
      return _html_info;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generates String Numbers - Voucher
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DrawNumbersPlayer
    //          - DrawNumbersSystem
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true or false
    // 
    //   NOTES:
    public string AddStringNumbers(List<Int32> DrawNumbers,
                                     List<Int32> DrawNumbersResult)
    {

      string _html_numbers = string.Empty;

      _html_numbers = @"<style type=""text/css"">";
      _html_numbers += ".numbers {          font: bold 17px/20px Helvetica, Verdana, Tahoma;";
      _html_numbers += "word-wrap: break-word;";
      _html_numbers += "text-align:center;         }";
      _html_numbers += "</style>";

      _html_numbers += @" <div class=""numbers"">";
      for (int _idx = 0; _idx <= DrawNumbers.Count - 1; _idx++)
      {
        if (_idx > 0)
        {
          _html_numbers += @"&nbsp ";
        }
        _html_numbers += DrawNumbers[_idx].ToString("00");
      }
      _html_numbers += @"</div>";

      if (DrawNumbersResult.Count > 0)
      {
        _html_numbers += Resource.String("STR_VOUCHER_CASHIER_DRAW_RESULTS");
      }

      _html_numbers += @" <div class=""numbers"">";
      for (int _idx = 0; _idx <= DrawNumbersResult.Count - 1; _idx++)
      {
        if (_idx > 0)
        {
          _html_numbers += @"&nbsp ";
        }
        _html_numbers += DrawNumbersResult[_idx].ToString("00");
      }
      _html_numbers += @"</div>";

      _html_numbers += @"<hr noshade size=""4"">";

      return _html_numbers;
    }

    #endregion
  }

  public class VoucherCurrencyCash : Voucher
  {

    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCurrencyCash(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected)
      : this(CashierSessionData, Collected, false, false)
    {

    }
    public VoucherCurrencyCash(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected, Boolean IsMovementFilter, Boolean HideChipsAndCurrenciesConcepts)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      //LJM 18-DEC-2013: All the generating the structure, etc. Are inside the funcion, since now the loop is there
      LoadVoucherResources(CashierSessionData, Collected, IsMovementFilter, HideChipsAndCurrenciesConcepts);
    }
    #endregion

    #region Private Methods
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected, Boolean IsMovementFilter, Boolean HideChipsAndCurrenciesConcepts)
    {
      String _value;
      Decimal _currency_value;
      String _national_currency;
      Decimal _game_profit;
      Voucher _temp_voucher;
      string _currencies_html = "";
      Int32 _exchange_currencies_count;
      Decimal _exchange_currency_value;
      Decimal _collected_value;
      Decimal _collected_diff;

      _national_currency = CurrencyExchange.GetNationalCurrency();
      _exchange_currencies_count = 0;
      _exchange_currency_value = 0;

      // Count difference currencies configured in the systems differnts of national currency
      foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in CashierSessionData.currencies_balance)
      {
        if (_currency.Key.Type.ToString() == CurrencyExchangeType.CURRENCY.ToString() && _currency.Key.IsoCode != _national_currency)
        {
          _exchange_currency_value = CashierSessionData.currency_cash_in_card_deposit_split1 + CashierSessionData.currency_cash_in_card_deposit_split2;

          _exchange_currencies_count += 1;
        }
      }

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in CashierSessionData.currencies_balance)
      {
        switch (_currency.Key.Type)
        {
          default:

            if (HideChipsAndCurrenciesConcepts || _currency.Key.IsoCode == _national_currency)
            {
              break;
            }

            _game_profit = 0;
            // Load HTML structure for the current currency
            LoadVoucher("VoucherCurrencyCash");

            // RMS & MPO & DRV: incorrect currency title label on credit card and check.
            _value = CurrencyExchange.GetDescription(_currency.Key.Type, _currency.Key.IsoCode, _currency.Key.IsoCode);

            //Title
            AddString("STR_VOUCHER_CURRENCY_CASH_TITLE", _value);

            //Deposit
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT");
            AddString("STR_VOUCHER_CURRENCY_CASH_DEPOSIT", _value);

            _currency_value = CashierSessionData.fill_in_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_in_balance[_currency.Key] : 0;
            AddCurrencyType("VOUCHER_CURRENCY_CASH_DEPOSIT", _currency.Key, _currency_value, _national_currency);

            //Withdraws
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW");
            AddString("STR_VOUCHER_CURRENCY_CASH_WITHDRAW", _value);

            _currency_value = CashierSessionData.fill_out_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_out_balance[_currency.Key] : 0;
            AddCurrencyType("VOUCHER_CURRENCY_CASH_WITHDRAW", _currency.Key, _currency_value, _national_currency);

            //Game profit
            if (CashierSessionData.currency_game_profit.ContainsKey(_currency.Key))
            {
              _value = Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT");
              AddString("STR_VOUCHER_GAME_PROFIT", _value);

              _game_profit = CashierSessionData.currency_game_profit[_currency.Key];
              AddCurrencyType("VOUCHER_GAME_PROFIT", _currency.Key, _game_profit, _national_currency);
            }
            else
            {
              AddString("STR_VOUCHER_GAME_PROFIT", "");
              AddString("VOUCHER_GAME_PROFIT", "");
            }

            //Cash In
            // LJM 14-DEC-2013: Not needed on chips
            if (!_currency.Key.IsCasinoChip)
            {
              _value = Resource.String("STR_UC_MB_CARD_BALANCE_CASH_IN_NAME");
              AddString("STR_VOUCHER_CURRENCY_CASH_CASH_IN", _value);

              _currency_value = CashierSessionData.currency_cash_in.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in[_currency.Key] : 0;
              AddCurrencyType("VOUCHER_CURRENCY_CASH_CASH_IN", _currency.Key, _currency_value, _national_currency);
            }
            else
            {
              AddString("STR_VOUCHER_CURRENCY_CASH_CASH_IN", "");
              AddString("VOUCHER_CURRENCY_CASH_CASH_IN", "");
            }

            // FAV 03-MAY-2015 (Currency exchange multiple denominations)
            if (Misc.IsMultiCurrencyExchangeEnabled())
            {
              _value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_CASH_DESK");
              AddString("STR_VOUCHER_CURRENCY_EXCHANGE_CASH_IN", _value);

              _currency_value = CashierSessionData.currency_exchange_cash_in.ContainsKey(_currency.Key) ? CashierSessionData.currency_exchange_cash_in[_currency.Key] : 0;
              AddCurrencyType("VOUCHER_CURRENCY_EXCHANGE_CASH_IN", _currency.Key, _currency_value, _national_currency);
            }
            else
            {
              AddString("STR_VOUCHER_CURRENCY_EXCHANGE_CASH_IN", "");
              AddString("VOUCHER_CURRENCY_EXCHANGE_CASH_IN", "");
            }

            // FAV 03-MAY-2015 (Currency exchange multiple denominations)
            // Devoluciones de divisas
            if (Misc.IsMultiCurrencyExchangeEnabled())
            {
              _value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DEVOLUTION_CASH_DESK");
              AddString("STR_VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", _value);

              _currency_value = CashierSessionData.currency_exchange_cash_out.ContainsKey(_currency.Key) ? CashierSessionData.currency_exchange_cash_out[_currency.Key] : 0;
              AddCurrencyType("VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", _currency.Key, _currency_value, _national_currency);
            }
            else
            {
              AddString("STR_VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", "");
              AddString("VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", "");
            }

            //Balance
            _value = Resource.String("STR_IN_CASH_DESK");
            AddString("STR_VOUCHER_CURRENCY_CASH_BALANCE", _value);

            if (IsMovementFilter && _currency.Key.IsoCode == _national_currency)
            {
              AddString("VOUCHER_CURRENCY_CASH_BALANCE", "---");
            }
            else
            {
              AddCurrencyType("VOUCHER_CURRENCY_CASH_BALANCE", _currency.Key, _currency.Value, _national_currency);
              _currency_value = _currency.Value;
            }

            // FAV 14-APR-2016
            _collected_diff = 0;
            if (CashierSessionData.collected_diff.ContainsKey(_currency.Key))
            {
              _collected_diff = CashierSessionData.collected_diff[_currency.Key];
            }

            // FAV 14-APR-2016
            _collected_diff = 0;
            if (CashierSessionData.collected_diff.ContainsKey(_currency.Key))
            {
              _collected_diff = CashierSessionData.collected_diff[_currency.Key];
            }

            // JML 05-AUG-2015
            if (_collected_diff == 0)
            {
              AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", "");
              AddString("VOUCHER_CURRENCY_CASH_COLLECTED", "");

              AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
              AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
              AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
              AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
            }
            else
            {
            //Collected
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
            AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", _value);

            _collected_value = _currency_value;

              if (_collected_diff < 0)
              {
                _collected_diff = -_collected_diff;

            //Short
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
                _collected_value -= _collected_diff;

            AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", _value);
                AddCurrencyType("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", _currency.Key, _collected_diff, _national_currency);

                AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
                AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
              }
              else
              {
            //Over
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
                _collected_value += _collected_diff;

            AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", _value);
                AddCurrencyType("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", _currency.Key, _collected_diff, _national_currency);

                AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
                AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
              }

            // Collected
            AddCurrencyType("VOUCHER_CURRENCY_CASH_COLLECTED", _currency.Key, _collected_value, _national_currency);
            }

            if (_currency.Key.IsoCode != _national_currency && !_currency.Key.IsCasinoChip)
            {
              //Total Amount
              SetParameterValue("@STR_FONT_SIZE", "font-size: 90%;");
              _value = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS");
              AddString("STR_VOUCHER_CURRENCY_CASH_AMOUNT", _value);

              _currency_value = CashierSessionData.currency_cash_in_exchange.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_exchange[_currency.Key] : 0;
              AddCurrency("VOUCHER_CURRENCY_CASH_AMOUNT", (Currency)_currency_value);
            }
            else
            {
              SetParameterValue("@STR_FONT_SIZE", "");
              AddString("STR_VOUCHER_CURRENCY_CASH_AMOUNT", "");
              AddString("VOUCHER_CURRENCY_CASH_AMOUNT", "");
            }

            //Card deposit only if one exchange currency avalaible
            if (_exchange_currencies_count == 1)
            {
              if (!_currency.Key.IsCasinoChip && _currency.Key.IsoCode != _national_currency && _currency.Key.Type != CurrencyExchangeType.CHECK && _currency.Key.Type != CurrencyExchangeType.CARD)
              {
                SetParameterValue("@VOUCHER_CURRENCY_CASH_CARD_DEPOSIT", AddCardDepositExchange(_exchange_currency_value, true));
              }
            }
            AddString("VOUCHER_CURRENCY_CASH_CARD_DEPOSIT", "");

            //Commission
            if (!_currency.Key.IsCasinoChip)
            {
              _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION");
              AddString("STR_VOUCHER_CURRENCY_CASH_COMMISSION", _value);

              _currency_value = CashierSessionData.currency_cash_in_commissions.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_commissions[_currency.Key] : 0;
              AddCurrency("VOUCHER_CURRENCY_CASH_COMMISSION", (Currency)_currency_value);
            }
            else
            {
              AddString("STR_VOUCHER_CURRENCY_CASH_COMMISSION", "");
              AddString("VOUCHER_CURRENCY_CASH_COMMISSION", "");
            }

            //Chips
            if (_currency.Key.IsCasinoChip)
            {
              _currency_value = 0;
              _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_IN");
              AddString("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_IN", _value);

              if (CashierSessionData.chips_purchase.ContainsKey(_currency.Key))
              {
                _currency_value = CashierSessionData.chips_purchase[_currency.Key];
              }

              AddCurrencyType("VOUCHER_SUMMARY_CHIPS_CASH_IN", _currency.Key, (Currency)_currency_value, _national_currency);

              _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_OUT");
              AddString("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_OUT", _value);

              _currency_value = 0;
              if (CashierSessionData.chips_sale.ContainsKey(_currency.Key))
              {
                _currency_value = CashierSessionData.chips_sale[_currency.Key];
              }

              AddCurrencyType("VOUCHER_CHIPS_CASH_OUT", _currency.Key, (Currency)_currency_value, _national_currency);
            }
            else
            {
              AddString("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_IN", "");
              AddString("VOUCHER_SUMMARY_CHIPS_CASH_IN", "");
              AddString("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_OUT", "");
              AddString("VOUCHER_CHIPS_CASH_OUT", "");
            }

            //SMN 16-SEP-2014
            if (_currency.Key.IsoCode != _national_currency)
            {
              _value = Resource.String("STR_VOUCHER_CONCEPTS_IN");
              AddString("STR_VOUCHER_CURRENCY_CONCEPTS_IN", _value);
              _value = Resource.String("STR_VOUCHER_CONCEPTS_OUT");
              AddString("STR_VOUCHER_CURRENCY_CONCEPTS_OUT", _value);

              _currency_value = CashierSessionData.concepts_currency_input.ContainsKey(_currency.Key) ? CashierSessionData.concepts_currency_input[_currency.Key] : 0;
              AddCurrencyType("VOUCHER_CURRENCY_CONCEPTS_IN", _currency.Key, _currency_value, _national_currency);

              _currency_value = CashierSessionData.concepts_currency_output.ContainsKey(_currency.Key) ? CashierSessionData.concepts_currency_output[_currency.Key] : 0;
              AddCurrencyType("VOUCHER_CURRENCY_CONCEPTS_OUT", _currency.Key, _currency_value, _national_currency);

              //AVZ 01-FEB-2016
              if (Entrance.GetReceptionMode() == Entrances.Enums.ENUM_RECEPTION_MODE.TicketWithPrice || CashierSessionData.entrances_currency_input != null)
              {
                _value = Resource.String("STR_MOVEMENT_TYPE_CUSTOMER_ENTRANCE");
                AddString("STR_VOUCHER_CURRENCY_ENTRANCES_IN", _value);

                _currency_value = CashierSessionData.entrances_currency_input.ContainsKey(_currency.Key) ? CashierSessionData.entrances_currency_input[_currency.Key] : 0;
                AddCurrencyType("VOUCHER_CURRENCY_ENTRANCES_IN", _currency.Key, _currency_value, _national_currency);
            }
            else
            {
                AddString("STR_VOUCHER_CURRENCY_ENTRANCES_IN", String.Empty);
                AddString("VOUCHER_CURRENCY_ENTRANCES_IN", String.Empty);
              }
            }
            else
            {
              AddString("STR_VOUCHER_CURRENCY_CONCEPTS_IN", "");
              AddString("STR_VOUCHER_CURRENCY_CONCEPTS_OUT", "");
              AddString("VOUCHER_CURRENCY_CONCEPTS_IN", "");
              AddString("VOUCHER_CURRENCY_CONCEPTS_OUT", "");
            }

            _currencies_html += VoucherHTML;

            break;

          case CurrencyExchangeType.CHECK:
            _temp_voucher = new Voucher();

            _temp_voucher.LoadVoucher("VoucherCashBankTransactions");

            _value = CurrencyExchange.GetDescription(_currency.Key.Type, _currency.Key.IsoCode, _currency.Key.IsoCode);
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_TITLE", _value);

            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CREDIT_CARD", "");
            _temp_voucher.AddString("VOUCHER_CASH_BANK_CREDIT_CARD", "");
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DEBIT_CARD", "");
            _temp_voucher.AddString("VOUCHER_CASH_BANK_DEBIT_CARD", "");

            //Withdraws
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW");
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_WITHDRAWS", _value);
            _currency_value = CashierSessionData.fill_out_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_out_balance[_currency.Key] : 0;
            _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_WITHDRAWS", _currency_value);


            // Inputs
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN");
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CASH_IN", _value);
            _currency_value = CashierSessionData.currency_cash_in.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in[_currency.Key] : 0;
            _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CASH_IN", _currency_value);


            // Balance
            _value = Resource.String("STR_IN_CASH_DESK");
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_BALANCE", _value);
            _currency_value = (CashierSessionData.currencies_balance.ContainsKey(_currency.Key) ? CashierSessionData.currencies_balance[_currency.Key] : 0);
            _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_BALANCE", (Currency)_currency_value);

            // FAV 14-APR-2016
            _collected_diff = 0;
            if (CashierSessionData.collected_diff.ContainsKey(_currency.Key))
            {
              _collected_diff = CashierSessionData.collected_diff[_currency.Key];
            }

            // JML 05-AUG-2015
            if (_collected_diff == 0)
            {
              _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");
              _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");

              _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
              _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
              _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
              _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
            }
            else
            {
            //Collected
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
            _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", _value);

            _currency_value = CashierSessionData.collected.ContainsKey(_currency.Key) ? CashierSessionData.collected[_currency.Key] : 0;
            _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", _currency_value);

              if (_collected_diff < 0)
              {
                _collected_diff = -_collected_diff;

            //Short
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
                //_currency_value = CashierSessionData.short_currency.ContainsKey(_currency.Key) ? CashierSessionData.short_currency[_currency.Key] : 0;

                _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_DIFF", _collected_diff);
            _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", _value);
                _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", _collected_diff);

                _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
                _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
              }
              else
              {
            //Over
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
                //_currency_value = CashierSessionData.over_currency.ContainsKey(_currency.Key) ? CashierSessionData.over_currency[_currency.Key] : 0;

            _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", _value);
                _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", _collected_diff);

                _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
                _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
              }
            }

            //Card deposit
            _value = Misc.GetCardPlayerConceptName();
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_DEPOSIT", _value);
            _currency_value = CashierSessionData.currency_cash_in_card_deposit.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_card_deposit[_currency.Key] : 0;
            _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CARD_DEPOSIT", _currency_value); // + CashierSessionData.cash_advance_check_comissions);

            // Comission
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION");
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_COMMISSION", _value);
            _currency_value = CashierSessionData.currency_cash_in_commissions.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_commissions[_currency.Key] : 0;
            _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_COMMISSION", _currency_value); // + CashierSessionData.cash_advance_check_comissions);

            _currencies_html += _temp_voucher.VoucherHTML;

            break;

          case CurrencyExchangeType.CARD:
            _temp_voucher = new Voucher();

            _temp_voucher.LoadVoucher("VoucherCashBankTransactions");

            _value = CurrencyExchange.GetDescription(_currency.Key.Type, _currency.Key.IsoCode, _currency.Key.IsoCode);
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_TITLE", _value);

            // Check for enabled specific bank card types
            if (GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false))
            {
              // Credit Cards
              _value = Resource.String("STR_FRM_AMOUNT_BANK_CREDIT_CARD");
              _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CREDIT_CARD", _value);
              _currency_value = CashierSessionData.total_bank_credit_card + CashierSessionData.total_cash_advance_bank_credit_card;
              _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CREDIT_CARD", _currency_value);
              // Debit Cards
              _value = Resource.String("STR_FRM_AMOUNT_BANK_DEBIT_CARD");
              _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DEBIT_CARD", _value);
              _currency_value = CashierSessionData.total_bank_debit_card + CashierSessionData.total_cash_advance_bank_debit_card;
              _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_DEBIT_CARD", _currency_value);
              // Others (in Withdraws)
              _value = Resource.String("STR_VOUCHER_CASH_BANK_CARD_OTHERS");
              _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_WITHDRAWS", _value);
              _currency_value = (CashierSessionData.total_bank_card - CashierSessionData.total_bank_debit_card - CashierSessionData.total_bank_credit_card) + CashierSessionData.cash_advance_generic_card + CashierSessionData.cash_advance_generic_card_comissions;
              _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_WITHDRAWS", _currency_value);

              // Inputs
              _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CASH_IN", "");
              _temp_voucher.AddString("VOUCHER_CASH_BANK_CASH_IN", "");
            }
            else
            {
              // Credit Cards
              _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CREDIT_CARD", "");
              _temp_voucher.AddString("VOUCHER_CASH_BANK_CREDIT_CARD", "");
              // Debit Cards
              _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DEBIT_CARD", "");
              _temp_voucher.AddString("VOUCHER_CASH_BANK_DEBIT_CARD", "");
              // Others (in Withdraws)
              _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_WITHDRAWS", "");
              _temp_voucher.AddString("VOUCHER_CASH_BANK_WITHDRAWS", "");

              // Inputs
              _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN");
              _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CASH_IN", _value);
              _currency_value = CashierSessionData.total_bank_card + CashierSessionData.total_cash_advance_bank_card;
              _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CASH_IN", _currency_value);

            }

            // Balance
            _value = Resource.String("STR_IN_CASH_DESK");
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_BALANCE", _value);
            _currency_value = CashierSessionData.total_bank_card + CashierSessionData.total_cash_advance_bank_card;
            _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_BALANCE", (Currency)_currency_value);

            // FAV 14-APR-2016
            _collected_diff = 0;
            if (CashierSessionData.collected_diff.ContainsKey(_currency.Key))
            {
              _collected_diff = CashierSessionData.collected_diff[_currency.Key];
            }

            // JML 05-AUG-2015
            if (_collected_diff == 0)
            {
              _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");
              _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");

              _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
              _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
              _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
              _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
            }
            else
            {
            //Collected
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
            _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", _value);

            _collected_value = _currency_value;

              if (_collected_diff < 0)
              {
                _collected_diff = -_collected_diff;

            //Short
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
                //_currency_value = CashierSessionData.short_currency.ContainsKey(_currency.Key) ? CashierSessionData.short_currency[_currency.Key] : 0;
                _collected_value -= _collected_diff;

                _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_DIFF", _collected_diff);
            _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", _value);
                _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", _collected_diff);

                _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
                _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");

              }
              else
              {
            //Over
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
                //_currency_value = CashierSessionData.over_currency.ContainsKey(_currency.Key) ? CashierSessionData.over_currency[_currency.Key] : 0;
                _collected_value += _collected_diff;

            _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", _value);
                _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", _collected_diff);

                _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
                _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
              }

            // Collected
            _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", _collected_value);
            }

            //Card deposit
            _value = Misc.GetCardPlayerConceptName();
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_DEPOSIT", _value);
            _currency_value = CashierSessionData.currency_cash_in_card_deposit.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_card_deposit[_currency.Key] : 0;
            _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CARD_DEPOSIT", _currency_value);

            // Comission
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION");
            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_COMMISSION", _value);
            _currency_value = CashierSessionData.currency_cash_in_commissions.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_commissions[_currency.Key] : 0;
            _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_COMMISSION", _currency_value);

            _currencies_html += _temp_voucher.VoucherHTML;

            break;
        }
      }
      //Card deposit only if more than one exchange currency avalaible
      if (_exchange_currencies_count > 1)
      {
        _currencies_html += AddCardDepositExchange(_exchange_currency_value, false);
      }

      VoucherHTML = _currencies_html;
    }

    private String AddCardDepositExchange(Currency TotalCardDeposit, Boolean InsertTab)
    {
      StringBuilder _html;
      String _tab_format;

      _html = new StringBuilder();
      _tab_format = string.Empty;

      if (InsertTab)
      {
        _tab_format = "font-size: 90%;";
      }

      _html.AppendLine("<tr valign=top>");
      _html.AppendLine(String.Format("<td class=\"left\" align=\"left\"  width=\"57%\"><span style=\"{0} margin-left:10px\">{1}</span></td>", _tab_format, Misc.GetCardPlayerConceptName()));
      _html.AppendLine("<td class=\"middle\"></td>");
      _html.AppendLine(String.Format("<td class=\"right\" align=\"right\" width=\"43%\"><span style=\"{0}\">{1}</span></td>", _tab_format, TotalCardDeposit.ToString()));
      _html.AppendLine("</tr>");

      return _html.ToString();
    }

    private void AddCurrencyType(String Parameter, CurrencyIsoType IsoType, Decimal Value, String NationalCurrency)
    {
      if (IsoType.IsoCode == NationalCurrency || IsoType.IsCasinoChip)
      {
        AddCurrency(Parameter, (Currency)Value);
      }
      else
      {
        AddString(Parameter, Currency.Format(Value, IsoType.IsoCode));
      }
    }

    #endregion

    #region Public Methods

    #endregion
  }

  public class VoucherCashDeskOverview : Voucher
  {
    #region Class Attributes

    public class ParametersCashDeskOverview
    {
      public Boolean FromCashier;
      public Boolean WithLiabilities;
      public Boolean IsFilterMovements;
      public Int64 CashierSessionId;
      public Color BackColor;
      public GU_USER_TYPE UserType;
      public Boolean HideChipsAndCurrenciesConcepts;
      public String CurrencyIsoCode;

      public ParametersCashDeskOverview()
      {
        FromCashier = false;
        WithLiabilities = false;
        IsFilterMovements = false;
        CashierSessionId = 0;
        UserType = GU_USER_TYPE.NOT_ASSIGNED;
        HideChipsAndCurrenciesConcepts = false;
        CurrencyIsoCode = CurrencyExchange.GetNationalCurrency();
      }
    }
    #endregion

    #region Constructor

    public VoucherCashDeskOverview(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                                 , PrintMode Mode
                                 , ParametersCashDeskOverview ParametersOverview
                                 , SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.


      // 1. Load HTML structure.

      if (!Misc.IsGamingHallMode())
      {
        LoadVoucher("VoucherCashDeskOverview");
      }
      else
      {
        LoadVoucher("VoucherGamingHallCashDeskOverview");
      }

      LoadVoucherResources(CashierSessionStats, ParametersOverview);

      // 4. Load specific voucher data.
      CashDeskOverview(CashierSessionStats, ParametersOverview);

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats, ParametersCashDeskOverview ParametersOverview)
    {

      String _str_value;
      String _str_pending;
      String _str_deposit;
      Boolean _is_tito_mode;
      Boolean _mb_pending_visible;
      Boolean _gp_pending_cash_partial;
      VoucherCurrencyCashSummary.ParametersCurrencyCashSummary _parameters_currency_cash_summary;
      CurrencyIsoType _currency_iso_type;
      Boolean _show_refill_cashier;
      Boolean _show_refill_cage;
      Boolean _show_collect_cashier;
      Boolean _show_collect_cage;
     
     
      _is_tito_mode = Utils.IsTitoMode();

      _mb_pending_visible = ParametersOverview.UserType != GU_USER_TYPE.SYS_GAMING_TABLE
                         && ParametersOverview.UserType != GU_USER_TYPE.SYS_SYSTEM
                        && (ParametersOverview.UserType != GU_USER_TYPE.USER || !_is_tito_mode);

      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      // CASH SUMMARY
      _parameters_currency_cash_summary = new VoucherCurrencyCashSummary.ParametersCurrencyCashSummary();
      _parameters_currency_cash_summary.HideChipsAndCurrenciesConcepts = ParametersOverview.HideChipsAndCurrenciesConcepts;
      _parameters_currency_cash_summary.IsFilterMovements = ParametersOverview.IsFilterMovements;
      _parameters_currency_cash_summary.CurrencyIsoCode = ParametersOverview.CurrencyIsoCode;
      _str_value = new VoucherCurrencyCashSummary(CashierSessionStats, _parameters_currency_cash_summary).VoucherHTML;
      this.VoucherHTML = this.VoucherHTML.Replace("@STR_VOUCHER_CASH_DESK_CASH_SUMMARY", _str_value);

      // - Inputs
      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_IN");
      AddString("STR_VOUCHER_ENTRIES_TITLE", _str_value);

      TYPE_SPLITS _split_data;

      if (String.IsNullOrEmpty(CashierSessionStats.split_a_name))
      {
        Split.ReadSplitParameters(out _split_data);
        //    - Inputs
        CashierSessionStats.split_a_name = _split_data.company_a.name;
        //    - uso de sala
        CashierSessionStats.split_b_name = _split_data.company_b.name;
      }

      if (String.IsNullOrEmpty(CashierSessionStats.split_b_name))
      {
        CashierSessionStats.split_b_name = Resource.String("STR_COMPANY_B_CONCEPT_CONFIG");
      }

      AddString("STR_SPLIT_B_HIDDEN", CashierSessionStats.split_enabled_b
                                      || CashierSessionStats.b_total_in != 0
                                      || CashierSessionStats.b_total_dev != 0 ? "" : "hidden");

      _str_value = CashierSessionStats.split_a_name;
      AddString("STR_VOUCHER_INPUTS", _str_value);

      _str_value = CashierSessionStats.split_b_name;
      AddString("STR_VOUCHER_B_TOTAL_IN", _str_value);

      _str_value = WSI.Common.Misc.GetCardPlayerConceptName();
      AddString("STR_VOUCHER_CARD_USAGE", _str_value);

      _str_value = Resource.String("STR_FRM_LAST_MOVEMENTS_SERVICE_CHARGE");
      AddString("STR_VOUCHER_SERVICE_CHARGE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_COMMISSION");
      AddString("STR_VOUCHER_COMMISSIONS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL");
      AddString("STR_VOUCHER_TOTAL_ENTRIES", _str_value);

      // - Outputs
      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_OUT");
      AddString("STR_VOUCHER_EXITS_TITLE", _str_value);

      _str_value = CashierSessionStats.split_a_name;
      AddString("STR_VOUCHER_REFUND_CHARGES", _str_value);

      _str_value = CashierSessionStats.split_b_name;
      AddString("STR_VOUCHER_B_TOTAL_REFUND", _str_value);

      _str_value = WSI.Common.Misc.GetCardPlayerConceptName();
      AddString("STR_VOUCHER_REFUND_CARD_USAGE", _str_value);

      _str_value = Resource.String("STR_FRM_CASH_DESK_PRIZE_PAYED");
      AddString("STR_VOUCHER_TOTAL_PRIZES", _str_value);

      if (Misc.IsTaxCustodyEnabled() || CashierSessionStats.tax_custody_return > 0)
      {
        _str_value = Misc.TaxCustodyRefundName();
        AddString("STR_TAX_CUSTODY_RETURN", _str_value);
      }
      else
      {
        AddString("STR_TAX_CUSTODY_RETURN", "");
        AddString("VOUCHER_TAX_CUSTODY_RETURN", "");
      }

      _str_value = Resource.String("STR_FRM_LAST_MOVEMENTS_DECIMAL_ROUNDING");
      AddString("STR_VOUCHER_DECIMAL_ROUNDING", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL");
      AddString("STR_VOUCHER_TOTAL_OUTPUTS", _str_value);

      // - Prizes
      _str_value = Resource.String("STR_UC_BANK_PRIZES");
      AddString("STR_VOUCHER_PRIZES", _str_value);

      _str_value = Resource.String("STR_UC_BANK_TAXES");
      AddString("STR_VOUCHER_TAXES", _str_value);

      _str_value = Resource.String("STR_UC_BANK_TAX_RETURNING_ON_PRIZE_COUPON");
      AddString("STR_VOUCHER_ROUNDING", _str_value);

      _str_value = Resource.String("STR_FRM_CASH_DESK_PRIZE_PAYED");
      AddString("STR_VOUCHER_TOTAL_PRIZES", _str_value);

      // - Taxes
      _str_value = CashierSessionStats.prize_taxes_1_name;
      AddString("STR_VOUCHER_TAX_ON_PRIZE_1", _str_value);

      _str_value = CashierSessionStats.prize_taxes_2_name;
      AddString("STR_VOUCHER_TAX_ON_PRIZE_2", _str_value);

      if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled") || CashierSessionStats.cash_in_tax_split1 > 0)
      {
        _str_value = GeneralParam.GetString("Cashier", "CashInTax.Name", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX"));
        AddString("STR_VOUCHER_CASH_IN_TAX", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_IN_TAX", "");
        AddString("VOUCHER_CASH_IN_TAX", "");
      }

      if (Misc.IsTaxProvisionsEnabled() || CashierSessionStats.tax_provisions > 0)
      {
        _str_value = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_PROVISIONS_TITLE"));
        AddString("STR_VOUCHER_TAX_PROVISIONS", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_TAX_PROVISIONS", "");
        AddString("VOUCHER_TAX_PROVISIONS", "");
      }

      // EOR 10-MAY-2016
      if (Misc.IsTaxCustodyEnabled() || CashierSessionStats.tax_custody > 0 || CashierSessionStats.tax_custody_return > 0)
      {
        _str_value = Misc.TaxCustodyName();
        AddString("STR_VOUCHER_CUSTODY", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CUSTODY", "");
        AddString("VOUCHER_CUSTODY", "");
      }

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL");
      AddString("STR_VOUCHER_TOTAL_TAXES", _str_value);

      // - Cash Desk Result
      _str_value = Resource.String("STR_UC_BANK_TOTAL");
      AddString("STR_VOUCHER_CASH_DESK_RESULT", _str_value);

      // - HandPays
      _str_value = Resource.String("STR_UC_BANK_HANDPAYS");
      AddString("STR_VOUCHER_HANDPAYS_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_HANDPAY_005");
      AddString("STR_VOUCHER_HANDPAYS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_HANDPAY_014");
      AddString("STR_VOUCHER_CANCELLATION_HANDPAYS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL");
      AddString("STR_VOUCHER_TOTAL_HANDPAYS", _str_value);

      _str_value = Resource.String("STR_UC_BANK_PROMOTION_REDEEMABLE");
      AddString("STR_VOUCHER_PROMO_RE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_PROMOTION_NON_REDEEMABLE");
      AddString("STR_VOUCHER_PROMO_NR", _str_value);

      _str_value = Resource.String("STR_UC_BANK_NRE_TO_RE");
      AddString("STR_VOUCHER_NR_TO_RE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CANCEL_PROMOTION_REDEEMABLE");
      AddString("STR_VOUCHER_CANCEL_PROMO_RE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CANCEL_PROMOTION_NON_REDEEMABLE");
      AddString("STR_VOUCHER_CANCEL_PROMO_NR", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CANCEL_PROMOTION_NON_REDEEMABLE");
      AddString("STR_VOUCHER_TOTAL_PRIZES", _str_value);

      // - Acceptors Deposits
      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_DEPOSIT");
      AddString("STR_VOUCHER_DEPOSITS", _str_value);

      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_WITHDRAW");
      AddString("STR_VOUCHER_WITHDRAW", _str_value);

      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_IN");
      AddString("STR_VOUCHER_ENTRIES_TITLE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_OUT");
      AddString("STR_VOUCHER_EXITS_TITLE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CHECK_PAYMENTS");
      AddString("STR_VOUCHER_CHECK_PAYMENTS", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CREDIT_CARD_PAYMENT");
      AddString("STR_VOUCHER_CREDIT_CARD_RECHARGES", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CHECK_RECHARGE");
      AddString("STR_VOUCHER_CHECK_RECHARGES", _str_value);

      _str_value = Resource.String("STR_CURRENCY_EXCHANGE");
      AddString("STR_VOUCHER_CURRENCY_EXCHANGES", _str_value);

      // FAV 03-MAY-2016
      if (Misc.IsMultiCurrencyExchangeEnabled())
      {
        _str_value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DEVOLUTION");
        AddString("STR_VOUCHER_DEVOLUTION_EXCHANGE", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_DEVOLUTION_EXCHANGE", "");
      }

      // RCI & DRV 01-AUG-2014: Changed && in condition
      if (GeneralParam.GetBoolean("GamingTables", "Cashier.RegisterChipSaleAtTableEnabled") || CashierSessionStats.chips_sale_register_total > 0)
      {
        _str_value = "<tr>";
        _str_value += "<td class='left'>";
        _str_value += Resource.String("STR_VOUCHER_CHIP_SALE_REGISTER") + "</td>";
        _str_value += "<td class='middle'>";
        _str_value += "<td class='right'>";
        _str_value += "@VOUCHER_CHIPS_SALE_REGISTER</td>";
        _str_value += "</tr>";
        _str_value += "<tr><td class='separator' colspan='2'>&#32;</td></tr>";

        SetParameterValue("@STR_VOUCHER_CHIPS_SALE_REGISTER", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CHIPS_SALE_REGISTER", "");
        AddString("VOUCHER_CHIPS_SALE_REGISTER", "");
      }
      _str_value = Resource.String("STR_UC_BANK_REDEEMABLE_CASH_IN");
      AddString("STR_VOUCHER_POINTS_AS_CASHIN", _str_value);

      _str_value = Resource.String("STR_FRM_CASH_DESK_NET_BALANCE");
      AddString("STR_VOUCHER_NATIONAL_BALANCE", _str_value);

      _currency_iso_type = new CurrencyIsoType();
      _currency_iso_type.IsoCode = ParametersOverview.CurrencyIsoCode;
      _currency_iso_type.Type = CurrencyExchangeType.CURRENCY;

      // JML 05-AUG-2015
      if (!CashierSessionStats.show_short_over)
      {
        AddString("STR_VOUCHER_COLLECTED_NATIONAL_BALANCE", "");

        AddString("STR_VOUCHER_SHORT_NATIONAL_BALANCE", "");
        AddString("STR_VOUCHER_OVER_NATIONAL_BALANCE", "");
      }

      //Collected
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
      AddString("STR_VOUCHER_COLLECTED_NATIONAL_BALANCE", _str_value);

      ////Short
      //_str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
      //AddString("STR_VOUCHER_SHORT_NATIONAL_BALANCE", _str_value);

      ////Over
      //_str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
      //AddString("STR_VOUCHER_OVER_NATIONAL_BALANCE", _str_value);

      if (ParametersOverview.FromCashier)
      {
        AddString("STR_VOUCHER_EXPIRATIONS_TITLE", "");
        AddString("STR_VOUCHER_EXPIRATION_DEV", "");
        AddString("STR_VOUCHER_EXPIRATION_PRIZE", "");
        AddString("STR_VOUCHER_EXPIRATION_NR", "");
      }
      else
      {
        _str_value = Resource.String("STR_UC_BANK_EXPIRATION");
        AddString("STR_VOUCHER_EXPIRATIONS_TITLE", _str_value);

        if (_is_tito_mode)
        {
          _str_value = Resource.String("STR_VOUCHER_TITO_TICKET_REDEEMABLE");
          AddString("STR_VOUCHER_EXPIRATION_DEV", _str_value);
          AddCurrency("VOUCHER_EXPIRATION_DEV", CashierSessionStats.tickets_expired_redeemable);

          _str_value = Resource.String("STR_VOUCHER_TITO_TICKET_PROMO_REDEEMABLE");
          AddString("STR_VOUCHER_EXPIRATION_PRIZE", _str_value);
          AddCurrency("VOUCHER_EXPIRATION_PRIZE", CashierSessionStats.tickets_expired_promo_redeemable);

          _str_value = Resource.String("STR_VOUCHER_TITO_TICKET_PLAYABLE");
          AddString("STR_VOUCHER_EXPIRATION_NR", _str_value);
          AddCurrency("VOUCHER_EXPIRATION_NR", CashierSessionStats.tickets_expired_promo_no_redeemable);
        }
        else
        {
          _str_value = Resource.String("STR_UC_BANK_EXPIRED_REDEEMABLE_REFUND");
          AddString("STR_VOUCHER_EXPIRATION_DEV", _str_value);

          _str_value = Resource.String("STR_UC_BANK_EXPIRED_REDEEMABLE_WINNINGS");
          AddString("STR_VOUCHER_EXPIRATION_PRIZE", _str_value);

          _str_value = Resource.String("STR_UC_BANK_EXPIRED_NON_REDEEMABLE");
          AddString("STR_VOUCHER_EXPIRATION_NR", _str_value);
        }
      }

      if (_is_tito_mode)
      {
        _str_pending = Resource.String("STR_VOUCHER_STACKER_PENDING");
        _str_deposit = Resource.String("STR_VOUCHER_STACKER_COLLECTED");
      }
      else
      {
        switch (ParametersOverview.UserType)
        {
          case GU_USER_TYPE.NOT_ASSIGNED:
            //For global cash reports from GUI
            _str_pending = Resource.String("STR_VOUCHER_BACKORDERED");
            _str_deposit = Resource.String("STR_VOUCHER_COLLECTED_MB_ACCEPTOR");

            if (_gp_pending_cash_partial)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL");
            }
            else
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER");
            }

            break;
          case GU_USER_TYPE.SYS_REDEMPTION:
          case GU_USER_TYPE.USER:
            _str_pending = Resource.String("STR_UC_BANK_PENDING_AMOUNT");
            _str_deposit = Resource.String("STR_VOUCHER_COLLECTED_MB");

            if (_gp_pending_cash_partial)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL");
            }
            else
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER");
            }

            break;
          case GU_USER_TYPE.SYS_ACCEPTOR:
          case GU_USER_TYPE.SYS_PROMOBOX:
            _str_pending = Resource.String("STR_VOUCHER_PENDING_ACCEPTORS");
            _str_deposit = Resource.String("STR_UC_BANK_COLLECTED_ACCEPTORS");
            _str_value = "";
            break;
          default:
            _str_pending = "";
            _str_deposit = "";
            _str_value = "";
            break;
        }
      }

      AddString("STR_VOUCHER_PENDING_AMOUNT", _str_pending);
      AddString("STR_VOUCHER_CASH_OVER", _str_value);
      AddString("STR_VOUCHER_ACCEPTORS_DEPOSITS", _str_deposit);

      // RMS 18-SEP-2014   Cash Advance
      if (CurrencyExchange.IsCashAdvanceEnabled() || CashierSessionStats.total_cash_advance.SqlMoney > 0)
      {
        AddString("STR_VOUCHER_CASH_ADVANCE", Resource.String("STR_VOUCHER_CASH_ADVANCE"));
        AddCurrency("VOUCHER_CASH_ADVANCE", CashierSessionStats.total_cash_advance);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_ADVANCE", "");
        AddString("VOUCHER_CASH_ADVANCE", "");
      }

      //visible/hidden labels
      AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");
      AddString("STR_MB_PENDING_VISIBLE", _mb_pending_visible ? "" : "hidden");
      AddString("STR_MB_OVERCASH_VISIBLE", _mb_pending_visible && !String.IsNullOrEmpty(_str_value) ? "" : "hidden");

      AddString("STR_MB_DEPOSITS_VISIBLE", !ParametersOverview.FromCashier && _mb_pending_visible ? "" : "hidden");
      AddString("STR_FROM_CASHIER_HIDDEN", ParametersOverview.FromCashier ? "hidden" : "");

      // JMV - 18-DEC-12 Gaming Hall
      if (!Misc.IsGamingHallMode())
      {
        _str_value = Resource.String("STR_VOUCHER_CONCEPTS");
      }
      else
      {
        _str_value = Resource.String("STR_VOUCHER_CONCEPTS_IN");
      }      
      AddString("STR_VOUCHER_CONCEPTS_IN", _str_value);
      AddCurrency("VOUCHER_CONCEPTS_IN", CashierSessionStats.concepts_input);

      //AVZ 01-FEB-2016
      if (Entrance.GetReceptionMode() == Entrances.Enums.ENUM_RECEPTION_MODE.TicketWithPrice || CashierSessionStats.entrances_input != 0)
      {
      //YNM 25-JAN-2016
      _str_value = Resource.String("STR_MOVEMENT_TYPE_CUSTOMER_ENTRANCE");
      AddString("STR_VOUCHER_ENTRANCES_IN", _str_value);
      AddCurrency("VOUCHER_ENTRANCES_IN", CashierSessionStats.entrances_input);
      }
      else
      {
        AddString("STR_VOUCHER_ENTRANCES_IN", "");
        AddString("VOUCHER_ENTRANCES_IN", "");
      }

      if (!Misc.IsGamingHallMode())
      {
        _str_value = Resource.String("STR_VOUCHER_CONCEPTS");
      }
      else
      {
        _str_value = Resource.String("STR_VOUCHER_CONCEPTS_OUT");
      }     
      AddString("STR_VOUCHER_CONCEPTS_OUT", _str_value);
      AddCurrency("VOUCHER_CONCEPTS_OUT", CashierSessionStats.concepts_output);

      if (Misc.IsGamingHallMode())
      {
        _show_refill_cashier = GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cashier != 0;
        _show_refill_cage = !GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cage != 0;
        _show_collect_cashier = GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cashier != 0;
        _show_collect_cage = !GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cage != 0;

        AddCurrency("VOUCHER_TOTAL_REFILL_TO_CASHIER", CashierSessionStats.refill_amount_to_cashier);
        AddString("STR_VOUCHER_TOTAL_REFILL_TO_CASHIER_HIDDEN", _show_refill_cashier ? "" : "hidden");

        AddCurrency("VOUCHER_TOTAL_REFILL_TO_CAGE", CashierSessionStats.refill_amount_to_cage);
        AddString("STR_VOUCHER_TOTAL_REFILL_TO_CAGE_HIDDEN", _show_refill_cage ? "" : "hidden");

        AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CASHIER", CashierSessionStats.collect_amount_to_cashier);
        AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CASHIER_HIDDEN", _show_collect_cashier ? "" : "hidden");

        AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CAGE", CashierSessionStats.collect_amount_to_cage);
        AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CAGE_HIDDEN", _show_collect_cage ? "" : "hidden");

        AddCurrency("VOUCHER_GAMING_HALL_BALANCE", CashierSessionStats.final_balance);
        _str_value = Resource.String("STR_VOUCHER_TOTAL_REFILL");
        AddString("STR_VOUCHER_TOTAL_REFILL", _str_value);
        _str_value = Resource.String("STR_VOUCHER_TOTAL_COLLECT");
        AddString("STR_VOUCHER_TOTAL_COLLECT", _str_value);
      }


    } // LoadVoucherResources

    private String LiabilitiesHTML(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      StringBuilder _sb_html;
      String _initial;
      String _final;
      String _increment;
      String _no_data;

      _no_data = "---";

      _sb_html = new StringBuilder();
      _sb_html.AppendLine(" <table border='0' align='left' cellpadding='0' cellspacing='0' width='100%'>");
      _sb_html.AppendLine("  	<tr> ");
      _sb_html.AppendLine("  	  <td align='left'>" + Resource.String("STR_UC_BANK_LIABILITIES") + "</td> ");
      _sb_html.AppendLine("  	  <td align='right'>" + Resource.String("STR_UC_BANK_INITIAL") + "</td> ");
      _sb_html.AppendLine("  	  <td align='right'>" + Resource.String("STR_UC_BANK_FINAL") + "</td> ");
      _sb_html.AppendLine("  	  <td align='right'>" + Resource.String("STR_UC_BANK_INCREMENT") + "</td> ");
      _sb_html.AppendLine("  	</tr> ");

      _initial = _no_data;
      _final = _no_data;
      _increment = _no_data;

      if (CashierSessionStats.liabilities_from.has_data)
      {
        _initial = CashierSessionStats.liabilities_from.re_balance.ToString();
      }
      if (CashierSessionStats.liabilities_to.has_data)
      {
        _final = CashierSessionStats.liabilities_to.re_balance.ToString();
      }
      if (CashierSessionStats.liabilities_from.has_data && CashierSessionStats.liabilities_to.has_data)
      {
        _increment = ((Currency)(CashierSessionStats.liabilities_to.re_balance - CashierSessionStats.liabilities_from.re_balance)).ToString();
      }

      _sb_html.AppendLine("  	<tr>  ");
      _sb_html.AppendLine("  	  <td align='left' >" + Resource.String("STR_VOUCHER_RE_PROMO_LOST") + "</td> ");
      _sb_html.AppendLine("  	  <td align='right'>" + _initial + "</td> ");
      _sb_html.AppendLine("  	  <td align='right'>" + _final + "</td>  ");
      _sb_html.AppendLine("  	  <td align='right'>" + _increment + "</td> ");
      _sb_html.AppendLine("  	</tr> ");

      if (CashierSessionStats.liabilities_from.has_data)
      {
        _initial = CashierSessionStats.liabilities_from.promo_re_balance.ToString();
      }
      if (CashierSessionStats.liabilities_to.has_data)
      {
        _final = CashierSessionStats.liabilities_to.promo_re_balance.ToString();
      }
      if (CashierSessionStats.liabilities_from.has_data && CashierSessionStats.liabilities_to.has_data)
      {
        _increment = ((Currency)(CashierSessionStats.liabilities_to.promo_re_balance - CashierSessionStats.liabilities_from.promo_re_balance)).ToString();
      }
      _sb_html.AppendLine("  	<tr>  ");
      _sb_html.AppendLine("  	  <td align='left'>" + Resource.String("STR_UC_BANK_PROMO_RE") + "</td> ");
      _sb_html.AppendLine("  	  <td align='right'>" + _initial + "</td> ");
      _sb_html.AppendLine("  	  <td align='right'>" + _final + "</td> ");
      _sb_html.AppendLine("  	  <td align='right'>" + _increment + "</td> ");
      _sb_html.AppendLine("  	</tr> ");

      if (CashierSessionStats.liabilities_from.has_data)
      {
        _initial = CashierSessionStats.liabilities_from.promo_nr_balance.ToString();
      }
      if (CashierSessionStats.liabilities_to.has_data)
      {
        _final = CashierSessionStats.liabilities_to.promo_nr_balance.ToString();
      }
      if (CashierSessionStats.liabilities_from.has_data && CashierSessionStats.liabilities_to.has_data)
      {
        _increment = ((Currency)(CashierSessionStats.liabilities_to.promo_nr_balance - CashierSessionStats.liabilities_from.promo_nr_balance)).ToString();
      }
      _sb_html.AppendLine("  	<tr>  ");
      _sb_html.AppendLine("  	  <td align='left'>" + Resource.String("STR_UC_BANK_PROMO_NR") + "</td> ");
      _sb_html.AppendLine("  	  <td align='right'>" + _initial + "</td>   ");
      _sb_html.AppendLine("  	  <td align='right'>" + _final + "</td>     ");
      _sb_html.AppendLine("  	  <td align='right'>" + _increment + "</td> ");
      _sb_html.AppendLine("  	</tr> ");

      if (CashierSessionStats.liabilities_from.has_data)
      {
        _initial = ((Currency)(CashierSessionStats.liabilities_from.re_balance +
                               CashierSessionStats.liabilities_from.promo_re_balance +
                               CashierSessionStats.liabilities_from.promo_nr_balance)).ToString();
      }
      if (CashierSessionStats.liabilities_to.has_data)
      {
        _final = ((Currency)(CashierSessionStats.liabilities_to.re_balance +
                             CashierSessionStats.liabilities_to.promo_re_balance +
                             CashierSessionStats.liabilities_to.promo_nr_balance)).ToString();
      }
      if (CashierSessionStats.liabilities_from.has_data && CashierSessionStats.liabilities_to.has_data)
      {
        _increment = ((Currency)((CashierSessionStats.liabilities_to.re_balance - CashierSessionStats.liabilities_from.re_balance) +
                                 (CashierSessionStats.liabilities_to.promo_re_balance - CashierSessionStats.liabilities_from.promo_re_balance) +
                                 (CashierSessionStats.liabilities_to.promo_nr_balance - CashierSessionStats.liabilities_from.promo_nr_balance))).ToString();
      }
      _sb_html.AppendLine("  	<tr>  ");
      _sb_html.AppendLine("  	  <td align='left' style='border-top: solid 2px black;'>" + Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_TOTAL") + "</td> ");
      _sb_html.AppendLine("  	  <td align='right' style='border-top: solid 2px black;'> " + _initial + "</td> ");
      _sb_html.AppendLine("  	  <td align='right' style='border-top: solid 2px black'>" + _final + "</td> ");
      _sb_html.AppendLine("  	  <td align='right' style='border-top: solid 2px black;'>" + _increment + "</td> ");
      _sb_html.AppendLine("  	</tr> ");
      _sb_html.AppendLine(" </table> ");

      return _sb_html.ToString();
    }

    private String TitoTicketsHTML(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats, GU_USER_TYPE UserType)
    {
        StringBuilder _sb_html;

        _sb_html = new StringBuilder();

        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td colspan=\"3\" style=\"height:15px;\"> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        // Tickets TITO
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td colspan=\"3\" align=\"center\" ");
        _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
        _sb_html.AppendLine("           " + Resource.String("STR_VOUCHER_TITO_TICKETS_TITLE"));
      
      if (Misc.IsFloorDualCurrencyEnabled())
      {
        _sb_html.AppendLine(" - " + CurrencyExchange.GetNationalCurrency());
      }

        _sb_html.AppendLine("       </span> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        if (UserType == GU_USER_TYPE.USER || UserType == GU_USER_TYPE.NOT_ASSIGNED || UserType == GU_USER_TYPE.SYS_REDEMPTION)
        {
            // Cashier
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"left @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_TITLE"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middle @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"right @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Created RE
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_RE_CREATED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_printed_cashable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_printed_cashable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Created Promo RE
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_PROMO_RE_CREATED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_printed_promo_redeemable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_printed_promo_redeemable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Created Promo NR
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_PROMO_NR_CREATED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_printed_promo_no_redeemable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_printed_promo_no_redeemable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Paid RE
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_RE_PAID"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_paid_cashable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_paid_cashable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Paid Promo RE
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_PROMO_RE_PAID"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_paid_promo_redeemable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_paid_promo_redeemable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Paid Handpay
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_HANPAY_PAID"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cancelled_redeemable_handpay_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cancelled_redeemable_handpay).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Paid Jackpot
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_JACKPOT_PAID"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cancelled_redeemable_jackpot_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cancelled_redeemable_jackpot).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");
        }


        // Expired Redeemed
        if (CashierSessionStats.tickets_cashier_expired_redeemed > 0 || GeneralParam.GetInt32("TITO", "Payment.GracePeriod.Mode") != 0)
        {
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_PAID_EXPIRED") + "");// + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_JACKPOT_CREATED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_expired_redeemed_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_expired_redeemed).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");
        }

        // EOR 20-MAY-2016 Tickets Swap for Chips
        if (GeneralParam.GetBoolean("TITA", "Enabled", false))
        {
          _sb_html.AppendLine("<tr> ");
          _sb_html.AppendLine("   <td class=\"sub-left @STR_TITO_VISIBLE\"> ");
          _sb_html.AppendLine("       " + Resource.String("STR_SWAP_CHIPS") + "");
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
          _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_swap_chips_count).ToString());
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("   <td class=\"right @STR_TITO_VISIBLE\"> ");
          _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_swap_chips).ToString());
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("</tr> ");
        }

        //Totals
        if (UserType == GU_USER_TYPE.USER || UserType == GU_USER_TYPE.NOT_ASSIGNED || UserType == GU_USER_TYPE.SYS_REDEMPTION)
        {
          _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"left border @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_TOTAL_CREATED") + "");
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO border @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_created_count).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO border @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_created_amount).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"left @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_TOTAL_CANCELLED") + "");
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cancelled_count).ToString());
            _sb_html.AppendLine("       </span> ");
          _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cancelled_amount).ToString());
            _sb_html.AppendLine("       </span> ");
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("</tr> ");

            // White Line
            _sb_html.AppendLine("<tr>");
            _sb_html.AppendLine("  <td class='separator' colspan='3'>");
            _sb_html.AppendLine("&#32;</td>");
            _sb_html.AppendLine("</tr>");
        }

        if (UserType == GU_USER_TYPE.SYS_TITO || UserType == GU_USER_TYPE.NOT_ASSIGNED)
        {
            // Machine
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"left @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_TITLE"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middle @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"right @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Played RE
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_RE_PLAYED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_played_cashable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_played_cashable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Played Promo RE
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_PROMO_RE_PLAYED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_played_promo_redeemable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_played_promo_redeemable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Played Promo NR
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_PROMO_NR_PLAYED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_played_promo_no_redeemable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_played_promo_no_redeemable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Created RE
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_RE_CREATED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_printed_cashable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_printed_cashable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Created Promo NR
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_NR_CREATED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_printed_promo_no_redeemable_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_printed_promo_no_redeemable).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Created Handpay
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_HANDPAY_CREATED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_created_redeemable_handpay_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_created_amount_redeemable_handpay).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            // Created Jackpot
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_JACKPOT_CREATED"));
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_created_redeemable_jackpot_count).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_created_amount_redeemable_jackpot).ToString());
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            //Totals
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"left border @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_TOTAL_TICKET_IN") + "");
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO border @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_played_machine_count).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO border @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_played_machine_amount).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"left @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_TOTAL_TICKET_OUT") + "");
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_created_machine_count).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_created_machine_amount).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            //Totals
            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"left border @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_TOTAL_TICKET_IN") + "");
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO border @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_played_machine_count).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO border @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_played_machine_amount).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");

            _sb_html.AppendLine("<tr> ");
            _sb_html.AppendLine("   <td class=\"left @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_TOTAL_TICKET_OUT") + "");
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_created_machine_count).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
            _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
            _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_created_machine_amount).ToString());
            _sb_html.AppendLine("       </span> ");
            _sb_html.AppendLine("   </td> ");
            _sb_html.AppendLine("</tr> ");
        }

        return _sb_html.ToString();
    }

    #endregion

    #region Public Methods

    public void CashDeskOverview(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats, ParametersCashDeskOverview ParametersOverview)
    {
      Voucher _voucher;
      String _str;
      Boolean _is_tito_mode;
      Currency _mb_pending;
      CurrencyIsoType _currency_iso_type;
      Currency _tmp_amount;
      Currency _collected_amount;

      _is_tito_mode = Utils.IsTitoMode();
      _mb_pending = 0;

      // - Inputs
      AddCurrency("VOUCHER_IMPUTS", CashierSessionStats.a_total_in);

      AddCurrency("VOUCHER_B_TOTAL_IN", CashierSessionStats.b_total_in);

      AddCurrency("VOUCHER_CARD_USAGE", CashierSessionStats.cards_usage);

      AddCurrency("VOUCHER_SERVICE_CHARGE", CashierSessionStats.service_charge);

      AddCurrency("VOUCHER_COMMISSIONS", CashierSessionStats.total_commission);

      AddCurrency("VOUCHER_TOTAL_ENTRIES", CashierSessionStats.total_cash_in);

      // - Outputs
      AddCurrency("VOUCHER_REFUND_CHARGES", CashierSessionStats.a_total_dev);

      AddCurrency("VOUCHER_B_TOTAL_REFUND", CashierSessionStats.b_total_dev);

      AddCurrency("VOUCHER_REFUND_CARD_USAGE", CashierSessionStats.refund_cards_usage);

      AddCurrency("VOUCHER_TOTAL_PRIZES", CashierSessionStats.prize_payment);

      if (Misc.IsTaxCustodyEnabled() || CashierSessionStats.tax_custody_return  > 0)
      {
        _str = Misc.TaxCustodyRefundName();
        AddString("STR_TAX_CUSTODY_RETURN", _str);
        AddCurrency("VOUCHER_TAX_CUSTODY_RETURN", CashierSessionStats.tax_custody_return);
      }
      else
      {
        AddString("STR_TAX_CUSTODY_RETURN", "");
        AddString("VOUCHER_TAX_CUSTODY_RETURN", "");
      }

      AddCurrency("VOUCHER_DECIMAL_ROUNDING", CashierSessionStats.decimal_rounding);

      AddCurrency("VOUCHER_TOTAL_OUTPUTS", CashierSessionStats.total_cash_out);

      // - Prizes
      AddCurrency("VOUCHER_PRIZES", CashierSessionStats.prize_gross);

      AddCurrency("VOUCHER_TAXES", CashierSessionStats.prize_taxes);

      AddCurrency("VOUCHER_ROUNDING", CashierSessionStats.tax_returning_on_prize_coupon);

      AddCurrency("VOUCHER_TOTAL_PRIZES", CashierSessionStats.prize_payment);

      // - Taxes
      AddCurrency("VOUCHER_TAX_ON_PRIZE_1", CashierSessionStats.prize_taxes_1);

      AddCurrency("VOUCHER_TAX_ON_PRIZE_2", CashierSessionStats.prize_taxes_2);

      AddCurrency("VOUCHER_TOTAL_TAXES", CashierSessionStats.prize_taxes);

      // - UNR
      if (GeneralParam.GetBoolean("Cashier", "Promotion.HideUNR", true) &&
          CashierSessionStats.unr_k1_gross == 0 && CashierSessionStats.unr_k2_gross == 0 &&
          CashierSessionStats.ure_k1_gross == 0 && CashierSessionStats.ure_k2_gross == 0)
      {
        AddString("STR_VOUCHER_UNR_KIND1", "");
        AddString("STR_VOUCHER_UNR_KIND2", "");

        AddString("VOUCHER_UNR_KIND1", "");
        AddString("VOUCHER_UNR_KIND2", "");
      }
      else
      {
        _str = Resource.String("STR_FRM_CASH_DESK_UNR_KIND", 1);
        AddString("STR_VOUCHER_UNR_KIND1", _str);

        _str = Resource.String("STR_FRM_CASH_DESK_UNR_KIND", 2);
        AddString("STR_VOUCHER_UNR_KIND2", _str);

        AddCurrency("VOUCHER_UNR_KIND1", CashierSessionStats.unr_k1_gross + CashierSessionStats.ure_k1_gross);
        AddCurrency("VOUCHER_UNR_KIND2", CashierSessionStats.unr_k2_gross + CashierSessionStats.ure_k2_gross);
      }

      // - Cash Desk Result
      AddCurrency("VOUCHER_TOTAL_INPUTS", CashierSessionStats.total_cash_in);

      AddCurrency("VOUCHER_TOTAL_OUTPUTS", CashierSessionStats.total_cash_out);

      AddCurrency("VOUCHER_TOTAL_TAX_1", CashierSessionStats.total_tax1);
      AddCurrency("VOUCHER_TOTAL_TAX_2", CashierSessionStats.total_tax2);

      if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled") || CashierSessionStats.cash_in_tax_split1 > 0)
      {
        _str = GeneralParam.GetString("Cashier", "CashInTax.Name", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX"));
        AddString("STR_VOUCHER_CASH_IN_TAX", _str);
        AddCurrency("VOUCHER_CASH_IN_TAX", CashierSessionStats.cash_in_tax_split1 + CashierSessionStats.cash_in_tax_split2);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_IN_TAX", "");
        AddString("VOUCHER_CASH_IN_TAX", "");
      }

      if (Misc.IsTaxProvisionsEnabled() || CashierSessionStats.tax_provisions > 0)
      {
        _str = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_PROVISIONS_TITLE"));
        AddString("STR_VOUCHER_TAX_PROVISIONS", _str);
        AddCurrency("VOUCHER_TAX_PROVISIONS", CashierSessionStats.tax_provisions);
      }
      else
      {
        AddString("STR_VOUCHER_TAX_PROVISIONS", "");
        AddString("VOUCHER_TAX_PROVISIONS", "");
      }

      // EOR 10-MAY-2016
      if (Misc.IsTaxCustodyEnabled() || CashierSessionStats.tax_custody > 0)
      {
        _str = Misc.TaxCustodyName();
        AddString("STR_VOUCHER_CUSTODY", _str);
        AddString("STR_VOUCHER_TAX_CUSTODY", _str);

        AddCurrency("VOUCHER_CUSTODY", CashierSessionStats.tax_custody - CashierSessionStats.tax_custody_return);
        AddCurrency("VOUCHER_TAX_CUSTODY", CashierSessionStats.tax_custody);
      }
      else
      {
        AddString("STR_VOUCHER_CUSTODY", "");
        AddString("STR_VOUCHER_TAX_CUSTODY", "");

        AddString("VOUCHER_CUSTODY", "");
        AddString("VOUCHER_TAX_CUSTODY", "");
      }

      AddCurrency("VOUCHER_CASH_DESK_RESULT", CashierSessionStats.result_cashier);

      if (CashierSessionStats.result_cashier < 0)
      {
        SetParameterValue("@STR_NEGATIVE", "negative");
      }
      else
      {
        AddString("STR_NEGATIVE", "");
      }

      AddCurrency("VOUCHER_HANDPAYS", CashierSessionStats.handpay_payment);

      AddCurrency("VOUCHER_CANCELLATION_HANDPAYS", CashierSessionStats.handpay_canceled);

      AddCurrency("VOUCHER_TOTAL_HANDPAYS", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

      AddCurrency("VOUCHER_PROMO_RE", CashierSessionStats.promo_re);

      AddCurrency("VOUCHER_PROMO_NR", CashierSessionStats.promo_nr);

      AddCurrency("VOUCHER_NR_TO_RE", CashierSessionStats.promo_nr_to_re);

      AddCurrency("VOUCHER_CANCEL_PROMO_RE", CashierSessionStats.cancel_promo_re);

      AddCurrency("VOUCHER_CANCEL_PROMO_NR", CashierSessionStats.cancel_promo_nr);

      AddCurrency("VOUCHER_DEPOSITS", CashierSessionStats.deposits);

      AddCurrency("VOUCHER_WITHDRAW", CashierSessionStats.withdraws);

      AddCurrency("VOUCHER_TOTAL_INPUTS", CashierSessionStats.total_cash_in);

      AddCurrency("VOUCHER_TOTAL_OUTPUTS", CashierSessionStats.total_cash_out);

      AddCurrency("VOUCHER_CHECK_PAYMENTS", CashierSessionStats.payment_orders);

      AddCurrency("VOUCHER_CREDIT_CARD_RECHARGES", CashierSessionStats.total_bank_card + CashierSessionStats.total_cash_advance_bank_card);

      AddCurrency("VOUCHER_CHECK_RECHARGES", CashierSessionStats.total_check + CashierSessionStats.cash_advance_check + CashierSessionStats.cash_advance_check_comissions);

      AddCurrency("VOUCHER_CURRENCY_EXCHANGES", CashierSessionStats.total_currency_exchange);

      // FAV 03-MAY-2016
      if (Misc.IsMultiCurrencyExchangeEnabled())
      {
        AddCurrency("VOUCHER_DEVOLUTION_EXCHANGE", CashierSessionStats.cash_in_currency_exchange);
      }
      else
      {
        AddString("VOUCHER_DEVOLUTION_EXCHANGE", "");
      }

      AddCurrency("VOUCHER_CHIPS_SALE_REGISTER", CashierSessionStats.chips_sale_register_total);

      AddCurrency("VOUCHER_POINTS_AS_CASHIN", CashierSessionStats.points_as_cashin);

      if (CashierSessionStats.national_game_profit != 0)
      {
        AddString("STR_VOUCHER_GAME_PROFIT", Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT"));
        AddCurrency("VOUCHER_GAME_PROFIT", CashierSessionStats.national_game_profit);
      }
      else
      {
        AddString("STR_VOUCHER_GAME_PROFIT", "");
        AddString("VOUCHER_GAME_PROFIT", "");
      }

      if (ParametersOverview.IsFilterMovements)
      {
        AddString("VOUCHER_TOTAL_NATIONAL_BALANCE", "---");
        AddString("VOUCHER_PENDING_AMOUNT", "---");
        AddString("VOUCHER_CASH_OVER", "---");
        AddString("VOUCHER_TOTAL_NATIONAL_BALANCE", "---");
        AddString("VOUCHER_TOTAL_COLLECTED_NATIONAL_BALANCE", "---");
        AddString("VOUCHER_TOTAL_SHORT_NATIONAL_BALANCE", "---");
        AddString("VOUCHER_TOTAL_OVER_NATIONAL_BALANCE", "---");
      }
      else
      {
        AddCurrency("VOUCHER_TOTAL_NATIONAL_BALANCE", CashierSessionStats.final_balance);
        AddCurrency("VOUCHER_PENDING_AMOUNT", ((Currency)(CashierSessionStats.mb_pending + CashierSessionStats.mb_total_balance_lost)));
        AddCurrency("VOUCHER_CASH_OVER", ((Currency)(CashierSessionStats.total_cash_over)));

        // JML 05-AUG-2015
        if (!CashierSessionStats.show_short_over)
        {
          AddString("VOUCHER_TOTAL_COLLECTED_NATIONAL_BALANCE", "");

          AddString("VOUCHER_TOTAL_SHORT_NATIONAL_BALANCE", "");
          AddString("VOUCHER_TOTAL_OVER_NATIONAL_BALANCE", "");
        }

        _currency_iso_type = new CurrencyIsoType();
        _currency_iso_type.IsoCode = ParametersOverview.CurrencyIsoCode;
        _currency_iso_type.Type = CurrencyExchangeType.CURRENCY;

        //Collected
        _collected_amount = CashierSessionStats.final_balance;

        //Short
        _tmp_amount = 0;
        if (CashierSessionStats.short_currency != null && CashierSessionStats.short_currency.ContainsKey(_currency_iso_type))
        {
          _tmp_amount = CashierSessionStats.short_currency[_currency_iso_type];
          if (_tmp_amount < 0)
          {
            _tmp_amount = -_tmp_amount;
          }
        }
        _collected_amount -= _tmp_amount;

        String _str_value;

        if (_tmp_amount > 0)
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
          AddString("STR_VOUCHER_SHORT_NATIONAL_BALANCE", _str_value);

          AddCurrency("VOUCHER_TOTAL_SHORT_NATIONAL_BALANCE", _tmp_amount);
        }
        else
        {
          AddString("STR_VOUCHER_SHORT_NATIONAL_BALANCE", "");
          AddString("VOUCHER_TOTAL_SHORT_NATIONAL_BALANCE", "");
        }

        //Over
        _tmp_amount = 0;
        if (CashierSessionStats.over_currency != null && CashierSessionStats.over_currency.ContainsKey(_currency_iso_type))
        {
          _tmp_amount = CashierSessionStats.over_currency[_currency_iso_type];
          if (_tmp_amount < 0)
          {
            _tmp_amount = -_tmp_amount;
          }
        }
        _collected_amount += _tmp_amount;

        if (_tmp_amount > 0)
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
          AddString("STR_VOUCHER_OVER_NATIONAL_BALANCE", _str_value);
          AddCurrency("VOUCHER_TOTAL_OVER_NATIONAL_BALANCE", _tmp_amount);
        }
        else
        {
          AddString("STR_VOUCHER_OVER_NATIONAL_BALANCE", "");
          AddString("VOUCHER_TOTAL_OVER_NATIONAL_BALANCE", "");
        }

        //Collected
        AddCurrency("VOUCHER_TOTAL_COLLECTED_NATIONAL_BALANCE", _collected_amount);

      }


      if (MobileBank.GP_GetRegisterShortfallOnDeposit())
      {
        _mb_pending = (Currency)CashierSessionStats.mb_pending + CashierSessionStats.mb_total_balance_lost;
      }
      else
      {
        _mb_pending = CashierSessionStats.mb_pending;

      }

      if (_mb_pending > 0 && !ParametersOverview.IsFilterMovements)
      {
        SetParameterValue("@STR_COLOR_PENDING", "pending");
      }
      else
      {
        AddString("STR_COLOR_PENDING", "");
      }

      if (ParametersOverview.FromCashier)
      {
        AddString("VOUCHER_EXPIRATION_DEV", "");
        AddString("VOUCHER_EXPIRATION_PRIZE", "");
        AddString("VOUCHER_EXPIRATION_NR", "");
        AddString("VOUCHER_ACCEPTORS_DEPOSITS", "");
      }
      else
      {
        AddCurrency("VOUCHER_EXPIRATION_DEV", CashierSessionStats.redeemable_dev_expired);
        AddCurrency("VOUCHER_EXPIRATION_PRIZE", CashierSessionStats.redeemable_prize_expired);
        AddCurrency("VOUCHER_EXPIRATION_NR", CashierSessionStats.not_redeemable_expired);
        AddCurrency("VOUCHER_ACCEPTORS_DEPOSITS", CashierSessionStats.mb_deposit_amount);
      }

      SetParameterValue("@VOUCHER_STYLE", "BODY{background-color:'" + ColorTranslator.ToHtml(ParametersOverview.BackColor) + "';}");

      if (ParametersOverview.WithLiabilities)
      {
        SetParameterValue("@VOUCHER_LIABILITIES", LiabilitiesHTML(CashierSessionStats));
      }
      else
      {
        AddString("VOUCHER_LIABILITIES", "");
      }
      // DHA 25-APR-2014 Added TITO ticket detail
      if (_is_tito_mode && (ParametersOverview.UserType == GU_USER_TYPE.NOT_ASSIGNED || ParametersOverview.UserType == GU_USER_TYPE.SYS_TITO || ParametersOverview.UserType == GU_USER_TYPE.USER
        || ParametersOverview.UserType == GU_USER_TYPE.SYS_REDEMPTION))
      {
        SetParameterValue("@VOUCHER_TITO_TICKETS", TitoTicketsHTML(CashierSessionStats, ParametersOverview.UserType));
      }
      else
      {
        AddString("VOUCHER_TITO_TICKETS", "");
      }

      _voucher = new WSI.Common.VoucherCurrencyCash(CashierSessionStats, false, ParametersOverview.IsFilterMovements, ParametersOverview.HideChipsAndCurrenciesConcepts);
      SetParameterValue("@VOUCHER_CURRENCY_CASH", _voucher.VoucherHTML);
    }
    #endregion
  }

  public class VoucherUndoOperation : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherUndoOperation(String VoucherHtml
                              , OperationCode OperationCode
                              , PrintMode Mode
                              , SqlTransaction Trx)
      : base(Mode, OperationCode, Trx)
    {
      String _undo_header;
      String _undone_text;

      // Read parameters configuration
      if (OperationCode == OperationCode.HANDPAY)
      {
        this.m_voucher_params = OperationVoucherParams.OperationVoucherDictionary.GetParameters(OperationCode.HANDPAY_CANCELLATION);
        this.NumPendingCopies = this.m_voucher_params.PrintCopy;
      }

      if (VoucherHtml.Contains(Voucher.VOUCHER_DIV_UNDO_OPERATION))
      {
        _undo_header = "<div align=\"right\">@VOUCHER_DATE_TIME</div>\n";
        _undo_header += "@VOUCHER_CASHIER_INFO\n";
        _undo_header += "<div align=\"center\"><br/><b>" + GeneralParam.GetString("Cashier.Voucher", "OperationUndo.Title") + "</b><br/><br/></div>\n";
        VoucherHtml = VoucherHtml.Replace(Voucher.VOUCHER_DIV_UNDO_OPERATION, _undo_header);
      }

      if (VoucherHtml.Contains(Voucher.VOUCHER_DIV_REPRINT))
      {
        //Replace the hidden div for the text of undone
        _undone_text = Voucher.VOUCHER_DIV_REPRINT;
        _undone_text += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n";
        _undone_text += "<tr><td align=\"center\"><br/><b>" + GeneralParam.GetString("Cashier.Voucher", "OperationUndo.UndoneText") + "</b><br/><br/></td></tr>\n";
        _undone_text += "</table>";
        VoucherHtml = VoucherHtml.Replace(Voucher.VOUCHER_DIV_REPRINT, _undone_text);
      }

      this.VoucherHTML = VoucherHtml;

      // Load cashier info and datetime
      LoadVoucherGeneralData();

    }

    #endregion
  }

  public class VoucherChipsPurchaseSale : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherChipsPurchaseSale(String[] VoucherAccountInfo
                              , TYPE_SPLITS Splits
                              , ChipsAmount ChipsAmount
                              , PrintMode Mode
                              , SqlTransaction Trx)
      : base(Mode, Trx)
    {


      // 1. Load HTML structure.
      LoadVoucher("VoucherChipsPurchaseSale");

      //  4. Load NLS strings from resource
      LoadVoucherResources(VoucherAccountInfo, Splits, ChipsAmount);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , TYPE_SPLITS Splits
                                    , ChipsAmount ChipsAmount)
    {
      String _str_value;
      StringBuilder _sb_chips_denomination_header;
      StringBuilder _sb_chips_denomination;
      String _hr_4;
      String _hr_2;

      _hr_2 = "";
      _hr_4 = "";

      _str_value = "";
      _sb_chips_denomination_header = new StringBuilder();
      _sb_chips_denomination = new StringBuilder();

      //  2. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      if (ChipsAmount.OperationType == ChipsOperation.SALE)
      {
        SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsSale);

        AddString("VOUCHER_GAMING_TABLE_NAME", ChipsAmount.SelectedGamingTable.TypeName + " - " + ChipsAmount.SelectedGamingTable.TableName);
        _str_value = Resource.String("STR_VOUCHER_CASH_CHIPS_SALE_TITLE");
      }
      else
      {
        SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsBuy);

        AddString("VOUCHER_GAMING_TABLE_NAME", "");
        _str_value = Resource.String("STR_VOUCHER_CASH_CHIPS_PURCHASE_TITLE");
      }
      AddString("STR_VOUCHER_CASH_CHIPS_TITLE", _str_value);

      if (TITO.Utils.IsTitoMode() && ChipsAmount.OperationType == ChipsOperation.SALE)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_CHIPS_INITIAL_AMOUNT");
        AddCurrency("VOUCHER_CASH_CHIPS_INITIAL_AMOUNT", ChipsAmount.InitialAmount);
      }
      else
      {
        _str_value = "";
        AddString("VOUCHER_CASH_CHIPS_INITIAL_AMOUNT", "");
      }
      AddString("STR_VOUCHER_CASH_CHIPS_INITIAL_AMOUNT", _str_value);


      _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");
      AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);

      AddCurrency("VOUCHER_CASH_CHIPS_TOTAL", ChipsAmount.Delivered);

      if (TITO.Utils.IsTitoMode() && ChipsAmount.OperationType == ChipsOperation.SALE)
      {
        if (ChipsAmount.Diference >= 0)
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
          AddCurrency("VOUCHER_CASH_CHIPS_REFUND", ChipsAmount.Diference);
        }
        else
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
          AddCurrency("VOUCHER_CASH_CHIPS_REFUND", -ChipsAmount.Diference);
        }
        AddString("STR_VOUCHER_CASH_CHIPS_REFUND", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_CHIPS_REFUND", "");
        AddString("VOUCHER_CASH_CHIPS_REFUND", "");
      }

      if ((!GeneralParam.GetBoolean("GamingTables", "Cashier.Mode") && !ChipsAmount.IsChipsSaleRegister || ChipsAmount.OperationType == ChipsOperation.PURCHASE) &&
         (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1) != Cage.ShowDenominationsMode.HideAllDenominations)
      {
        // Header
        _sb_chips_denomination_header.AppendLine("	<tr>  ");
        _sb_chips_denomination_header.AppendLine("	   <td align='right'  width='30%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION") + "</td> ");
        _sb_chips_denomination_header.AppendLine("	   <td align='right' width='30%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + "</td> ");
        _sb_chips_denomination_header.AppendLine("	   <td align='right' width='40%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + "</td> ");
        _sb_chips_denomination_header.AppendLine("	</tr> ");

        foreach (KeyValuePair<Decimal, Int32> _denominations in ChipsAmount.ChipsDenomination)
        {
          if (_denominations.Value > 0)
          {
            _sb_chips_denomination.AppendLine("	<tr>  ");
            _sb_chips_denomination.AppendLine("	   <td align='right'  width='30%'>" + ((Currency)_denominations.Key).ToString() + "</td> ");
            _sb_chips_denomination.AppendLine("	   <td align='right' width='30%'>" + _denominations.Value + "</td> ");
            _sb_chips_denomination.AppendLine("	   <td align='right' width='40%'>" + ((Currency)_denominations.Key) * _denominations.Value + "</td> ");
            _sb_chips_denomination.AppendLine("	</tr> ");
          }
        }
        _hr_2 = "<hr noshade size='2'>";
        _hr_4 = "<hr noshade size='4'>";
      }

      SetParameterValue("@STR_HR_SIZE_4", _hr_4);
      SetParameterValue("@STR_HR_SIZE_2", _hr_2);

      SetParameterValue("@VOUCHER_DESCRIPTION_CHIPS", _sb_chips_denomination_header.ToString());
      SetParameterValue("@VOUCHER_DENOMINATION_CHIPS", _sb_chips_denomination.ToString());

        }

    #endregion
  }
      SetParameterValue("@VOUCHER_SWAP_TICKETS_BODY", _str_ticket_body.ToString());

  //ESE 17-Mar-2016
  public class VoucherChipsSwapWithTicketTito : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherChipsSwapWithTicketTito(String[] VoucherAccountInfo
                              , TYPE_SPLITS Splits
                              , ChipsAmount ChipsAmount
                              , IDictionary<long, decimal> Tickets
                              , Boolean IsDealerCopy
                              , PrintMode Mode
                              , SqlTransaction Trx)
      : base(Mode, Trx)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherChipsSwapWithTicketTito");

      //  4. Load NLS strings from resource
      LoadVoucherResources(VoucherAccountInfo, Splits, ChipsAmount, Tickets, IsDealerCopy);
    }

    #endregion

    #region Private Methods
    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , TYPE_SPLITS Splits
                                    , ChipsAmount ChipsAmount
                                    , IDictionary<long, decimal> Tickets
                                    , Boolean IsDealerCopy)
    {
      String _str_value;
      StringBuilder _sb_chips_denomination_header;
      StringBuilder _sb_chips_denomination;
      String _hr_4;
      String _hr_2;
      StringBuilder _str_ticket_head;
      StringBuilder _str_ticket_body;
      StringBuilder _str_mount;

      _str_ticket_head = new StringBuilder();
      _str_ticket_body = new StringBuilder();
      _str_mount = new StringBuilder();
      _sb_chips_denomination_header = new StringBuilder();
      _sb_chips_denomination = new StringBuilder();
      _hr_2 = String.Empty;
      _hr_4 = String.Empty;
      _str_value = String.Empty;

      //  2. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsSwap);

      //Load the gaming table
      AddString("VOUCHER_GAMING_TABLE_NAME", ChipsAmount.SelectedGamingTable.TypeName + " - " + ChipsAmount.SelectedGamingTable.TableName);


      //Voucher title
      _str_value = Resource.String("STR_VOUCHER_SWAP_CHIPS_TITLE");
      AddString("STR_VOUCHER_SWAP_CHIPS_TITLE", _str_value);

      if (!IsDealerCopy)
      {
        //Voucher subtitle
        AddString("STR_VOUCHER_CASH_CHIPS_DEALER_COPY", String.Empty);
      }
      else
      {
        //Voucher subtitle
        AddString("STR_VOUCHER_CASH_CHIPS_DEALER_COPY", GeneralParam.GetString("GamingTables", "Cashier.Mode.DealerCopy.Title"));
      }

      //For the swap tickets TITO
      _str_ticket_head.AppendLine("	<tr>  ");
      _str_ticket_head.AppendLine("    <td align='left' width='50%'>" + Resource.String("STR_VOUCHER_SWAP_SUBTITLE") + "</td>");
      _str_ticket_head.AppendLine("    <td align='right' width='50%'></td>");
      _str_ticket_head.AppendLine("	</tr>  ");
      SetParameterValue("@VOUCHER_SWAP_TICKETS_HEAD", _str_ticket_head.ToString());
      foreach (KeyValuePair<long, decimal> kvp in Tickets)
      {
        //Obtain all the ticket data
        Ticket _aux_ticket;
        _aux_ticket = Ticket.LoadTicket(kvp.Key);

        _str_ticket_body.AppendLine("	<tr>  ");
        _str_ticket_body.AppendLine("    <td align='left' width='50%'>" + _aux_ticket.ValidationNumber + "</td>");
        _str_ticket_body.AppendLine("    <td align='right' width='50%'>" + decimal.Round(kvp.Value, 2).ToString("C") + "</td>");
        _str_ticket_body.AppendLine("	</tr>  ");
      }
      SetParameterValue("@VOUCHER_SWAP_TICKETS_BODY", _str_ticket_body.ToString());


      _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");
      AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);
      AddCurrency("VOUCHER_CASH_CHIPS_TOTAL", ChipsAmount.Delivered);

      //Diference between tickets - chips
      _str_value = Resource.String("STR_VOUCHER_SWAP_DESK_CLOSE_COLL_DIFF_MISSING");
      AddString("STR_VOUCHER_CASH_CHIPS_REFUND", _str_value);
      AddCurrency("VOUCHER_CASH_CHIPS_REFUND", ChipsAmount.Diference);

      //Header from the table chips
      _sb_chips_denomination_header.AppendLine("	<tr>  ");
      _sb_chips_denomination_header.AppendLine("	   <td align='right' width='30%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION") + "</td> ");
      _sb_chips_denomination_header.AppendLine("	   <td align='right' width='30%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + "</td> ");
      _sb_chips_denomination_header.AppendLine("	   <td align='right' width='40%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + "</td> ");
      _sb_chips_denomination_header.AppendLine("	</tr> ");

      foreach (KeyValuePair<Decimal, Int32> _denominations in ChipsAmount.ChipsDenomination)
      {
        if (_denominations.Value > 0)
        {
          _sb_chips_denomination.AppendLine("	<tr>  ");
          _sb_chips_denomination.AppendLine("	   <td align='right'  width='30%'>" + ((Currency)_denominations.Key).ToString() + "</td> ");
          _sb_chips_denomination.AppendLine("	   <td align='right' width='30%'>" + _denominations.Value + "</td> ");
          _sb_chips_denomination.AppendLine("	   <td align='right' width='40%'>" + ((Currency)_denominations.Key) * _denominations.Value + "</td> ");
          _sb_chips_denomination.AppendLine("	</tr> ");
        }
      }
      _hr_2 = "<hr noshade size='2'>";
      _hr_4 = "<hr noshade size='4'>";

      SetParameterValue("@STR_HR_SIZE_4", _hr_4);
      SetParameterValue("@STR_HR_SIZE_2", _hr_2);

      SetParameterValue("@VOUCHER_DESCRIPTION_CHIPS", _sb_chips_denomination_header.ToString());
      SetParameterValue("@VOUCHER_DENOMINATION_CHIPS", _sb_chips_denomination.ToString());
    }
    #endregion
  }

  public class VoucherChipsSaleDealerCopy : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherChipsSaleDealerCopy(String[] VoucherAccountInfo
                              , TYPE_SPLITS Splits
                              , ChipsAmount ChipsAmount
                              , String BarCodeExternal
                              , PrintMode Mode
                              , SqlTransaction Trx)
      : base(Mode, Trx)
    {

      // Configured only for one print
      m_voucher_params.PrintCopy = 1;
      NumPendingCopies = m_voucher_params.PrintCopy;

      // 1. Load HTML structure.
      LoadVoucher("VoucherChipsSaleDealerCopy");

      //  4. Load NLS strings from resource
      LoadVoucherResources(VoucherAccountInfo, Splits, ChipsAmount);

      // DHA 23-JUN-2015: print barcode
      PrintBarcode(BarCodeExternal);

      PrintSignatures();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , TYPE_SPLITS Splits
                                    , ChipsAmount ChipsAmount)
    {
      String _str_value;
      StringBuilder _sb_chips_denomination;

      _str_value = "";
      _sb_chips_denomination = new StringBuilder();

      //  2. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsSaleDealerCopy);
      AddString("VOUCHER_GAMING_TABLE_NAME", ChipsAmount.SelectedGamingTable.TypeName + " - " + ChipsAmount.SelectedGamingTable.TableName);

      _str_value = Resource.String("STR_VOUCHER_CASH_CHIPS_SALE_TITLE");
      AddString("STR_VOUCHER_CASH_CHIPS_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");
      AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);

      AddCurrency("VOUCHER_CASH_CHIPS_TOTAL", ChipsAmount.Delivered);

      AddString("STR_VOUCHER_CASH_CHIPS_DEALER_COPY", GeneralParam.GetString("GamingTables", "Cashier.Mode.DealerCopy.Title"));

    }

    /// <summary>
    /// Print signatures if actived in General Params.
    /// </summary>
    private void PrintSignatures()
    {
      if (GeneralParam.GetBoolean("GamingTables", "Cashier.Mode.DealerCopy.Signature"))
      {
        SetParameterValue("@VOUCHER_SIGNATURE_DEALER",
          "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" +
            "<tr>" +
              "<td></br><b>" + Resource.String("STR_VOUCHER_AUTHORIZED_BY_SIGNATURE") + ": " + "</b></td>" +
            "</tr>" +
            "<tr style='height:80px'><td>&nbsp;</td></tr>" +
          "</table>" +
          "<hr noshade size='4'>");
      }
      else
      {
        AddString("VOUCHER_SIGNATURE_DEALER", "");
      }
    }

    /// <summary>
    /// Print barcode if actived in General Params.
    /// </summary>
    private void PrintBarcode(String BarCodeExternal)
    {
      String _style;
      String _style_barcode;
      String _barcode_data;

      _style = String.Empty;
      _style_barcode = String.Empty;
      _barcode_data = String.Empty;

      _barcode_data += "<br />";
      _barcode_data += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
      _barcode_data += "  <tr>";
      _barcode_data += "    <td colspan=\"2\" align=\"center\">";
      _barcode_data += "      <br />";
      _barcode_data += "      <div id='inputdata'/>";
      _barcode_data += "     </td>";
      _barcode_data += "  </tr>";
      _barcode_data += "</table>";

      _style_barcode = "<script type='text/javascript'>$('#inputdata').barcode('#PAYMENT_BARCODE_ORDER_ID#', 'int25');</script>";
      _style_barcode = _style_barcode.Replace("#PAYMENT_BARCODE_ORDER_ID#", BarCodeExternal);

      SetParameterValue("@VOUCHER_BARCODE_STYLE", _style_barcode);
      SetParameterValue("@VOUCHER_BARCODE_DATA", _barcode_data);
    }

    #endregion
  }

  public class VoucherChipsChange : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherChipsChange(TYPE_SPLITS Splits
                              , ChipsAmount ChipsAmountIn
                              , ChipsAmount ChipsAmountOut
                              , PrintMode Mode
                              , SqlTransaction Trx)
      : base(Mode, Trx)
    {


      // 1. Load HTML structure.
      LoadVoucher("VoucherChipsChange");

      //  4. Load NLS strings from resource
      LoadVoucherResources(Splits, ChipsAmountIn, ChipsAmountOut);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(TYPE_SPLITS Splits
                                    , ChipsAmount ChipsAmountIn
                                    , ChipsAmount ChipsAmountOut)
    {
      String _str_value;
      StringBuilder _sb_chips_denomination;
      StringBuilder _sb_chips_value;
      Currency _amount;
      Currency _denomination_amount;

      SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsChange);

      _amount = 0;
      _denomination_amount = 0;
      _str_value = "";
      _sb_chips_denomination = new StringBuilder();

      //  2. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      _str_value = Resource.String("STR_VOUCHER_CHIPS_CHANGE_TITLE");
      AddString("STR_VOUCHER_CASH_CHIPS_CHANGE_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CHIPS_CHANGE_ENTER");
      AddString("STR_VOUCHER_ENTER_CHIPS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CHIPS_CHANGE_OUTPUT");
      AddString("STR_VOUCHER_OUTPUT_CHIPS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");
      AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);

      // Enter chips
      _sb_chips_denomination.AppendLine("	<tr>  ");
      _sb_chips_denomination.AppendLine("	   <td align='right'  width='30%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION") + "</td> ");
      _sb_chips_denomination.AppendLine("	   <td align='right' width='30%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + "</td> ");
      _sb_chips_denomination.AppendLine("	   <td align='right' width='40%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + "</td> ");
      _sb_chips_denomination.AppendLine("	</tr> ");
      SetParameterValue("@VOUCHER_DESCRIPTION_ENTER_CHIPS", _sb_chips_denomination.ToString());
      SetParameterValue("@VOUCHER_DESCRIPTION_OUTPUT_CHIPS", _sb_chips_denomination.ToString());

      _sb_chips_value = new StringBuilder();
      foreach (KeyValuePair<Decimal, Int32> _denominations in ChipsAmountIn.ChipsDenomination)
      {
        if (_denominations.Value > 0)
        {
          _denomination_amount = ((Currency)_denominations.Key) * _denominations.Value;
          _sb_chips_value.AppendLine("	<tr>  ");
          _sb_chips_value.AppendLine("	   <td align='right'  width='30%'>" + ((Currency)_denominations.Key).ToString() + "</td> ");
          _sb_chips_value.AppendLine("	   <td align='right' width='30%'>" + _denominations.Value + "</td> ");
          _sb_chips_value.AppendLine("	   <td align='right' width='40%'>" + _denomination_amount + "</td> ");
          _sb_chips_value.AppendLine("	</tr> ");
          _amount += _denomination_amount;
        }
      }
      SetParameterValue("@VOUCHER_DENOMINATION_ENTER_CHIPS", _sb_chips_value.ToString());
      AddCurrency("VOUCHER_CASH_CHIPS_ENTER_TOTAL", _amount);


      _amount = 0;
      _sb_chips_value = new StringBuilder();
      foreach (KeyValuePair<Decimal, Int32> _denominations in ChipsAmountOut.ChipsDenomination)
      {
        if (_denominations.Value > 0)
        {
          _denomination_amount = ((Currency)_denominations.Key) * _denominations.Value;

          _sb_chips_value.AppendLine("	<tr>  ");
          _sb_chips_value.AppendLine("	   <td align='right'  width='30%'>" + ((Currency)_denominations.Key).ToString() + "</td> ");
          _sb_chips_value.AppendLine("	   <td align='right' width='30%'>" + _denominations.Value + "</td> ");
          _sb_chips_value.AppendLine("	   <td align='right' width='40%'>" + _denomination_amount + "</td> ");
          _sb_chips_value.AppendLine("	</tr> ");
          _amount += _denomination_amount;
        }
      }
      SetParameterValue("@VOUCHER_DENOMINATION_OUTPUT_CHIPS", _sb_chips_value.ToString());
      AddCurrency("VOUCHER_CASH_CHIPS_OUTPUT_TOTAL", _amount);
    }

    #endregion
  }

  public class VoucherCardCreditTransfer : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardCreditTransfer(Int64 SourceAccountId
                                    , Int64 TargetAccountId
                                    , Accounts.TransferCreditAmounts Amounts
                                    , PrintMode Mode
                                    , SqlTransaction Trx)
      : base(Mode, Trx)
    {


      // 1. Load HTML structure.
      LoadVoucher("VoucherCardTransferPoints");

      //  4. Load NLS strings from resource
      LoadVoucherResources(SourceAccountId, TargetAccountId, Amounts);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Int64 SourceAccountId
                                    , Int64 TargetAccountId
                                    , Accounts.TransferCreditAmounts Amounts)
    {
      TYPE_SPLITS _splits;
      String _str_value;
      _str_value = String.Empty;

      // Get Splits information
      if (!Split.ReadSplitParameters(out _splits))
      {
        return;
      }

      //  2. Load Header and Footer
      LoadHeader(_splits.company_a.header);
      LoadFooter(_splits.company_a.footer, _splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // General

      _str_value = Resource.String("STR_VOUCHER_TRANSFER_CREDIT_TITLE");
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_TRANSFER_POINTS_ORIGIN");
      AddString("STR_VOUCHER_TRANSFER_POINTS_ORIGIN", _str_value);

      _str_value = Resource.String("STR_VOUCHER_TRANSFER_POINTS_DESTINY");
      AddString("STR_VOUCHER_TRANSFER_POINTS_DESTINY", _str_value);

      //_str_value = Resource.String("STR_VOUCHER_CARD_CARD_ID") + ":";
      AddString("STR_VOUCHER_CARD_CARD_ID", String.Empty);

      _str_value = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID") + ":";
      AddString("STR_VOUCHER_CARD_ACCOUNT_ID", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_BALANCE_INITIAL") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_INITIAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_TRANSFERED") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TRANSFERED", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_BALANCE_FINAL") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_FINAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_TO_ADD") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TO_ADD", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_TOTAL_AMOUNT") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TOTAL_AMOUNT", _str_value);

      // Source account info
      AddString("VOUCHER_TRANSFER_POINTS_CARD_ID_ORIGIN", String.Empty);
      AddString("VOUCHER_TRANSFER_POINTS_ACCOUNT_ID_ORIGIN", SourceAccountId.ToString());

      // Source account Data
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_ORIGIN_BALANCE_INITIAL", Amounts.SourceInitialRedeemableBalance);
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_ORIGIN_TO_ADD", Amounts.SourceTransferredAmount);
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_ORIGIN_BALANCE_FINAL", Amounts.SourceFinalRedeemableBalance);

      // Destiny account info
      AddString("VOUCHER_TRANSFER_POINTS_CARD_ID_DESTINY", String.Empty);
      AddString("VOUCHER_TRANSFER_POINTS_ACCOUNT_ID_DESTINY", TargetAccountId.ToString());

      // Destiny account Data
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_BALANCE_INITIAL", Amounts.TargetInitialRedeemableBalance);
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_TO_ADD", Amounts.TargetTransferredAmount);
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_BALANCE_FINAL", Amounts.TargetFinalRedeemableBalance);

      // Total
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_TOTAL_AMOUNT", Amounts.TargetTransferredAmount);
    }

    #endregion
  }

  public class VoucherCageFillInFillOutBody : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCageFillInFillOutBody(CASHIER_MOVEMENT MovementType, CageAmountsDictionary CageAmounts)
    {


      // 1. Load HTML structure.
      LoadVoucher("VoucherCageFillInFillOutBody");

      //  4. Load NLS strings from resource
      LoadVoucherResources(MovementType, CageAmounts);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(CASHIER_MOVEMENT MovementType, CageAmountsDictionary CageAmounts)
    {
      String _str_value;
      String _voucher_html;
      StringBuilder _sb_chips_denomination;
      StringBuilder _sb_chips_header;
      DataTable _values;
      Int16 _set_another_currency;
      String _national_currency;
      int _count_cage_currencies;
      Decimal _total_currency_nacional = 0;
      CurrencyExchange _currency_exchange = new CurrencyExchange();

      Decimal _denomination;
      String _currency_type;

      Currency _sub_total;
      bool _currency_changed;
      String _currency_string;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      _voucher_html = "";
      _set_another_currency = 0;
      _sb_chips_denomination = new StringBuilder();

      _sb_chips_denomination.AppendLine("<table border='0' align='left' cellpadding='0' cellspacing='0' width='270'>");
      _sb_chips_denomination.AppendLine("   <tr>");
      _sb_chips_denomination.AppendLine("   <td>");
      _voucher_html += _sb_chips_denomination.ToString();

      _count_cage_currencies = CageAmounts.Count;
      CageAmountsDictionary _cage_amounts_ordered;

      _cage_amounts_ordered = GetCageCurrenciesOrderByNational(CageAmounts);

      foreach (KeyValuePair<string, CageAmountsDictionaryItem> _cur_currency in _cage_amounts_ordered) //CageAmounts)
      {
        _currency_string = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL_FULL");
        _sub_total = 0;
        _currency_changed = false;
        //if is MXN-2 break.
        //SDS 06-10-2015
        //When "ISO CODE" && "-1" (Cage.BANK_CARD_CODE), it shouldn't print
        if ((MovementType == CASHIER_MOVEMENT.CLOSE_SESSION || _cur_currency.Value.TotalCurrency > 0 || _cur_currency.Value.HasTicketsTito) && (_cur_currency.Key.ToString().Length == 3 || _cur_currency.Key.ToString().Contains(Cage.CHECK_CODE.ToString())))
        {
          _sb_chips_denomination = new StringBuilder();
          _sb_chips_header = new StringBuilder();

          _set_another_currency++;
          if (_set_another_currency == 2)
          {
            _voucher_html += ("</td></tr><tr><td><br/></td></tr><tr><td>");
          }

          //  1. Load HTML structure for the current currency
          LoadVoucher("VoucherCageFillInFillOutBody");

          //  2. Load Header and Footer

          // Header
          _sb_chips_header.AppendLine("	<tr> ");
          _sb_chips_header.AppendLine("	   <td width='100%' colspan=4><hr noshade size=\"4\"></td> ");
          _sb_chips_header.AppendLine("	</tr> ");
          _sb_chips_header.AppendLine("	<tr>  ");
          _sb_chips_header.AppendLine("	   <td align='left' width='35%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION") + "</td> ");
          _sb_chips_header.AppendLine("	   <td align='right' width='9%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE") + "</td> ");
          _sb_chips_header.AppendLine("	   <td align='right' width='18%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + "</td> ");
          _sb_chips_header.AppendLine("	   <td align='right' width='40%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + "</td> ");
          _sb_chips_header.AppendLine("	</tr> ");
          _sb_chips_header.AppendLine("	<tr> ");
          _sb_chips_header.AppendLine("	   <td width='100%' colspan=4><hr noshade size=\"2\"></td> ");
          _sb_chips_header.AppendLine("	</tr> ");

          StringBuilder _sb_currency;
          _sb_currency = new StringBuilder();
          CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, _cur_currency.Key, out _currency_exchange);
          //SDS 06-10-2015
          if (_national_currency == _cur_currency.Key)
          {
            SetParameterValue("@VOUCHER_DESCRIPTION_CURRENCY", "");
          }
          else
          {
            _sb_currency.AppendLine("<br>");
            if (_cur_currency.Key == Cage.CHIPS_ISO_CODE)
              _sb_currency.AppendLine(Resource.String("STR_UC_TICKET_CREATE_GB_CHIPS"));

            else if (_cur_currency.Key == (_national_currency + Cage.CHECK_CODE.ToString()))
              _sb_currency.AppendLine(Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CHECK"));
            else
              _sb_currency.AppendLine(_currency_exchange.Description);

            SetParameterValue("@VOUCHER_DESCRIPTION_CURRENCY", _sb_currency.ToString());
          }
          //Rows

          String _deno_string;
          _values = _cur_currency.Value.ItemAmounts;
          foreach (DataRow _row in _values.Rows)
          {
            _denomination = (Decimal)_row["DENOMINATION"];
            //SDS 06-10-2015
            if (_row.IsNull("CURRENCY_TYPE"))
              _currency_type = "";
            else
              _currency_type = (String)_row["CURRENCY_TYPE"];
            if (_cur_currency.Key != Cage.CHIPS_ISO_CODE && _currency_type != Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL") && !_currency_changed)
            {
              //print and reset subtotal. Only 1 time

              if (_national_currency == _cur_currency.Key.Substring(0, Cage.MAX_ISO_CODE_DIGITS))
                _sb_chips_denomination.AppendLine("<tr><td colspan='4' border='1'></br><span style='float:right'><b>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + " " + _currency_string + "</b>&nbsp;&nbsp;&nbsp;" + _sub_total.ToString() + "</span></br></br></td><tr>");
              else
                _sb_chips_denomination.AppendLine("<tr><td colspan='4' border='1'></br><span style='float:right'><b>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + " " + _currency_string + "</b>&nbsp;&nbsp;&nbsp;" + Currency.Format(_sub_total, _cur_currency.Key) + "</span></br></br></td><tr>");

              _sub_total = 0;
              _currency_changed = true;
              _currency_string = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN_FULL");
            }
            if (_row.IsNull("TOTAL"))
            {
              _row["TOTAL"] = 0;
            }

            // RMS 01-AUG-2014: Don't show denomination items without values
            if ((_row.IsNull("UN") || Convert.ToInt32(_row["UN"]) == 0) && Convert.ToDecimal(_row["TOTAL"]) == 0)
            {
              continue;
            }

            if (Convert.ToDecimal(_row["TOTAL"]) > 0 || MovementType == CASHIER_MOVEMENT.CLOSE_SESSION)
            {
              _sb_chips_denomination.AppendLine("	<tr>  ");

              if (_denomination < 0)
              {
                _deno_string = getDenominationOther(_denomination, _row["ISO_CODE"].ToString());
                _sb_chips_denomination.AppendLine("	   <td align='right' width='27%'>" + _deno_string + "</td> ");
                if (_row.IsNull("UN"))
                {
                  if (Convert.ToDecimal(_row["TOTAL"]) > 0)
                  {
                    _row["UN"] = 1;
                  }
                }
                else if (_denomination == Cage.TICKETS_CODE)
                {
                  _row["TOTAL"] = 0;
                }
              }
              else
              {
                _sb_chips_denomination.AppendLine("	   <td align='right' width='27%'>" + Currency.Format(_denomination, "") + "</td> ");

              }
              _sb_chips_denomination.AppendLine("	   <td align='right' width='14%'>" + _currency_type + "</td> ");
              if (_row.IsNull("UN"))
              {
                _row["UN"] = 0;
              }

              if (_denomination == Cage.COINS_CODE)
              {
                _sb_chips_denomination.AppendLine("	   <td align='right' width='18%'></td> ");
              }
              else
              {
                int _aux_un = Convert.ToInt32(_row["UN"]);
                _sb_chips_denomination.AppendLine("	   <td align='right' width='18%'>" + _aux_un.ToString("#,##0") + "</td> ");
              }

              if (_national_currency == _cur_currency.Key.Substring(0, Cage.MAX_ISO_CODE_DIGITS))
              {
                if (_denomination == Cage.TICKETS_CODE)
                {
                  _sb_chips_denomination.AppendLine("	   <td align='right' width='40%'> </td> ");
                }
                else
                {
                  _sb_chips_denomination.AppendLine("	   <td align='right' width='40%'>" + (Currency)Convert.ToDecimal(_row["TOTAL"].ToString()) + "</td> ");
                }
              }
              else
              {
                _sb_chips_denomination.AppendLine("	   <td align='right' width='40%'>" + Currency.Format(Convert.ToDecimal(_row["TOTAL"].ToString()), _cur_currency.Key) + " " + "</td> ");
              }
              _sub_total += Convert.ToDecimal(_row["TOTAL"].ToString());
              _sb_chips_denomination.AppendLine("	</tr> ");
            }

          }
          if (_cur_currency.Key != Cage.CHIPS_ISO_CODE && _sub_total > 0)
          {




            if (_national_currency == _cur_currency.Key.Substring(0, Cage.MAX_ISO_CODE_DIGITS))
              _sb_chips_denomination.AppendLine("<tr><td colspan='4' border='1'></br><span style='float:right'><b>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + " " + _currency_string + "</b>&nbsp;&nbsp;&nbsp;" + _sub_total.ToString() + "</span></br></br></td><tr>");
            else
              _sb_chips_denomination.AppendLine("<tr><td colspan='4' border='1'></br><span style='float:right'><b>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + " " + _currency_string + "</b>&nbsp;&nbsp;&nbsp;" + Currency.Format(_sub_total, _cur_currency.Key) + "</span></br></br></td><tr>");



          }

          if (String.IsNullOrEmpty(_sb_chips_denomination.ToString()))
          {
            SetParameterValue("@VOUCHER_DESCRIPTION_CHIPS", "");
          }
          else
          {
            SetParameterValue("@VOUCHER_DESCRIPTION_CHIPS", _sb_chips_header.ToString());
          }

          SetParameterValue("@VOUCHER_DENOMINATION_CHIPS", _sb_chips_denomination.ToString());

          _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");

          AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);

          if (_national_currency == _cur_currency.Key.Substring(0, Cage.MAX_ISO_CODE_DIGITS))
          {
            AddCurrency("VOUCHER_CASH_CHIPS_TOTAL", (Currency)_cur_currency.Value.TotalCurrency + _total_currency_nacional);
          }
          else
          {
            //AddString("VOUCHER_CASH_CHIPS_TOTAL", (_cur_currency.Value.TotalCurrency + _total_currency_nacional) + " " + _cur_currency.Key);
            AddString("VOUCHER_CASH_CHIPS_TOTAL", Currency.Format(Convert.ToDecimal((_cur_currency.Value.TotalCurrency + _total_currency_nacional).ToString()), _cur_currency.Key));
          }
          _total_currency_nacional = 0;


          _voucher_html += VoucherHTML;
          _set_another_currency = 1;
        }
      }
      _sb_chips_denomination.Length = 0;
      _sb_chips_denomination.AppendLine("     </td>");
      _sb_chips_denomination.AppendLine("    </tr>");
      _sb_chips_denomination.AppendLine("  </table>");
      VoucherHTML = _voucher_html;
    }

    /// <summary>
    /// Get String of Denomination.
    /// </summary>
    private String getDenominationOther(Decimal Denomination, String _iso_code)
    {
      String _denomination;

      _denomination = "";

      Int16 _deno_int16;
      _deno_int16 = (Int16)Denomination;

      switch (_deno_int16)
      {
        case Cage.COINS_CODE:
          if (_iso_code == Cage.CHIPS_ISO_CODE)
          {
            _denomination = Resource.String("STR_CAGE_TIPS"); //Propinas
          }
          else
          {
            _denomination = Resource.String("STR_CAGE_COINS"); //Monedas
          }
          break;

        case Cage.TICKETS_CODE:
          _denomination = Resource.String("STR_FRM_TITO_HANDPAYS_TICKETS");  // Tickets
          break;

        case Cage.CHECK_CODE:
          _denomination = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CHECK_PAYMENT"); // Cheque
          break;

        case Cage.BANK_CARD_CODE:
          _denomination = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_BANK_CARD"); // Tarjeta bancaria
          break;

        default:
          if (Denomination.ToString() == Cage.CHIPS_ISO_CODE)
          {
            _denomination = Resource.String("STR_CAGE_CASINO_CHIPS"); // Fichas
          }
          break;

      }
      return _denomination;

    }

    /// <summary>
    /// Get CageCurrencies with national coin the first.
    /// </summary>
    private CageAmountsDictionary GetCageCurrenciesOrderByNational(CageAmountsDictionary CageAmounts)
    {
      String _national_currency;
      _national_currency = CurrencyExchange.GetNationalCurrency();

      CageAmountsDictionary _outCageAmounts;
      _outCageAmounts = new CageAmountsDictionary();

      foreach (KeyValuePair<string, CageAmountsDictionaryItem> _cur_currency in CageAmounts)
      {
        if (_cur_currency.Key.ToString() == _national_currency)
        {
          _outCageAmounts.Add(_cur_currency.Key, _cur_currency.Value);
          break;
        }
      }

      foreach (KeyValuePair<string, CageAmountsDictionaryItem> _cur_currency in CageAmounts)
      {
        if (_cur_currency.Key.ToString() != _national_currency)
        {
          _outCageAmounts.Add(_cur_currency.Key, _cur_currency.Value);
        }
      }

      return _outCageAmounts;

    }

    #endregion
  }

  public class VoucherCashDeskCloseUnbalanced : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskCloseUnbalanced(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                              , PrintMode Mode
                              , SqlTransaction SQLTransaction)
      : base(Mode, CashierVoucherType.CashDeskCloseUnbalanced, SQLTransaction)
    {
      InitializeVoucher(CashierSessionStats);

    }

    private void InitializeVoucher(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String _footer;
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.


      LoadVoucher("VoucherCashDeskCloseUnbalanced");

      _footer = GeneralParam.GetString("Cashier.Voucher", "CashClosing.Unbalanced.Footer");

      if (!String.IsNullOrEmpty(_footer))
      {
        LoadFooter(_footer, null);
      }

      // 2. Load voucher resources.
      LoadVoucherResources(CashierSessionStats);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String value = "";
      VoucherCashDeskCloseUnbalancedBody _voucher_body;

      // - Title
      SetParameterValue("@STR_VOUCHER_CASH_DESK_CLOSE_TITLE", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE"));

      value = Resource.String("STR_VOUCHER_CASH_DESK_DIFFERENCE_TITLE");
      SetParameterValue("@STR_VOUCHER_CASH_DESK_DIFFERENCE_TITLE", value);

      _voucher_body = new VoucherCashDeskCloseUnbalancedBody(CashierSessionStats);
      SetParameterValue("@VOUCHER_CASH_DESK_DIFFERENCE_BODY", _voucher_body.VoucherHTML);

    } // LoadVoucherResources

    #endregion

    #region Public Methods

    #endregion

  }

  public class VoucherCashDeskCloseUnbalancedBody : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskCloseUnbalancedBody(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskCloseUnbalancedBody");

      //  4. Load NLS strings from resource
      LoadVoucherResources(CashierSessionStats);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String _voucher_html;
      Int16 _set_another_currency;
      String _national_currency;
      CurrencyExchange _currency_exchange = new CurrencyExchange();
      String _iso_Code;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      _voucher_html = "";
      _set_another_currency = 0;

      foreach (KeyValuePair<CurrencyIsoType, Decimal> collected_diff in CashierSessionStats.collected_diff) //CageAmounts)
      {
        _iso_Code = collected_diff.Key.IsoCode;

        if (CashierSessionStats.collected_diff[collected_diff.Key] != 0)
        {
          if (_set_another_currency != 0)
          {
            _voucher_html += ("</td></tr><tr><td><hr noshade size=\"2\"><br /></td></tr><tr><td>");
          }
          _set_another_currency++;

          //  1. Load HTML structure for the current currency
          LoadVoucher("VoucherCashDeskCloseUnbalancedBody");

          StringBuilder _sb_currency;
          _sb_currency = new StringBuilder();

          if (collected_diff.Key.IsoCode != Cage.CHIPS_ISO_CODE)
          {
            CurrencyExchange.ReadCurrencyExchange(collected_diff.Key.Type, _iso_Code, out _currency_exchange);
            _sb_currency.AppendLine(_currency_exchange.Description);

          }
          else
          {
            _sb_currency.AppendLine(Resource.String("STR_CAGE_CASINO_CHIPS"));
          }

          // Header
          SetParameterValue("@VOUCHER_DESCRIPTION_CURRENCY", _sb_currency.ToString());
          SetParameterValue("@STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_DESCRIPTION", Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_DESCRIPTION"));
          SetParameterValue("@STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS"));

          //DETAILS
          SetParameterValue("@STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED"));
          SetParameterValue("@STR_VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED", Resource.String("STR_VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED"));

          if (collected_diff.Value > 0)
            SetParameterValue("@STR_VOUCHER_DIFFERENCE", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA"));
          else
            SetParameterValue("@STR_VOUCHER_DIFFERENCE", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING"));


          if (_national_currency == collected_diff.Key.IsoCode && collected_diff.Key.Type == CurrencyExchangeType.CURRENCY)
          {
            AddCurrency("VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED", CashierSessionStats.final_balance);
            if (CashierSessionStats.collected.ContainsKey(collected_diff.Key))
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", CashierSessionStats.collected[collected_diff.Key]);
            else
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", 0);
            AddCurrency("VOUCHER_DIFFERENCE", Math.Abs(collected_diff.Value));
          }
          else
          {
            if (_national_currency == collected_diff.Key.IsoCode && collected_diff.Key.Type != CurrencyExchangeType.CURRENCY)
            {
              AddCurrency("VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED", CashierSessionStats.currencies_balance[collected_diff.Key]);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", CashierSessionStats.collected[collected_diff.Key]);
              AddCurrency("VOUCHER_DIFFERENCE", Math.Abs(collected_diff.Value));
            }
            else
            {
              // 19-JAN-2016. Bug 8545. Si se usa divisa, currencies_balance parece no tener registros
              Decimal _currency_balance = 0;
              if (CashierSessionStats.currencies_balance.ContainsKey(collected_diff.Key)) 
                _currency_balance = CashierSessionStats.currencies_balance[collected_diff.Key];

              SetParameterValue("@VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED", Currency.Format(_currency_balance, _iso_Code));

              SetParameterValue("@VOUCHER_CASH_DESK_CLOSE_COLLECTED", Currency.Format(CashierSessionStats.collected[collected_diff.Key], _iso_Code));
              SetParameterValue("@VOUCHER_DIFFERENCE", Currency.Format(collected_diff.Value, _iso_Code));
            }
          }
          _voucher_html += VoucherHTML;
        }
      }
      VoucherHTML = _voucher_html;

    }
    #endregion
  }
  // N. Cage GUI new operation
  // Format:
  //          Voucher Logo
  //          Voucher Header
  //
  // Cage Session:
  // User: 
  // Voucher Id:
  // Tipo de movimiento (origen/destino)
  //
  //         Voucher Footer
  //
  public class VoucherCageNewOperation : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCageNewOperation(PrintMode Mode, String[] VcInfo, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      //LoadVoucher("VoucherCashDeskWithdraw");
      LoadVoucher("VoucherCageNewOperation");

      // 2. Load voucher resources.
      LoadVoucherCageResourcesFromGUI((String.IsNullOrEmpty(VcInfo[3])) ? "" : VcInfo[3]);

      // 3. Load general voucher data.
      LoadVoucherCageGeneralDataFromGUI(VcInfo);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherCageResourcesFromGUI(String OperationTitle)
    {
      // - Title
      AddString("STR_VOUCHER_CAGE_NEW_OPERATION_TITLE", OperationTitle);

    } // LoadVoucherResources

    #endregion

    #region Public Methods

    #endregion

  }





  // Tito Ticket Operation
  // Format:
  //          Voucher Logo
  //          Voucher Header
  //
  // Cashier Session:
  // User: 
  // Voucher Id:
  // Validation Number    Amount
  //
  //         Voucher Footer
  //
  public class VoucherTitoTicketOperation : Voucher
  {
    #region Constructor

    public VoucherTitoTicketOperation(String[] VoucherAccountInfo
                           , OperationCode OperationCode
                           , Boolean IsDiscardOperation
                           , List<VoucherValidationNumber> ValidationNumbers
                           , PrintMode Mode
                           , SqlTransaction SQLTransaction
                           , Ticket Ticket = null)
      : base(Mode, SQLTransaction)
    {
      String _str_html;
      String _title_voucher;
      String _ticket_label;
      String _msg_info;

      _title_voucher = String.Empty;
      _ticket_label = String.Empty;
      _msg_info = String.Empty;

      if (IsDiscardOperation)
      {
        _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_VOIDING") + ": ";
      }

      switch (OperationCode)
      {
        case OperationCode.TITO_OFFLINE:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_OFFLINE_TICKET");


            if (ValidationNumbers[0].TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_NR", (Currency)ValidationNumbers[0].Amount);
              _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_VOIDED");
            }
            else
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_REDEEMED", (Currency)ValidationNumbers[0].Amount);
              _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_REDEEMED");
            }
          }
          break;

        case OperationCode.TITO_REISSUE:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_REISSUE_TICKET");
            _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_VOIDED");

            if (ValidationNumbers[0].TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_NR", (Currency)ValidationNumbers[0].Amount);
            }
            else
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_RE", (Currency)ValidationNumbers[0].Amount);
            }
          }
          break;

        case OperationCode.PROMOTION:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_PROMO_TICKET");

            if (IsDiscardOperation)
            {
              _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_VOIDED");

              if (ValidationNumbers[0].TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
              {
                _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_NR_NUM", new object[] { ValidationNumbers.Count, (Currency)ValidationNumbers[0].Amount });
              }
              else
              {
                _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_RE_NUM", new object[] { ValidationNumbers.Count, (Currency)ValidationNumbers[0].Amount });
              }
            }
            else
            {
              _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS");
            }
          }
          break;

        case OperationCode.TITO_TICKET_VALIDATION:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKET_IN");

            _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_IN");

            if (ValidationNumbers[0].TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKET_IN_MSG_INFO_NR_NUM");
            }
            else
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKET_IN_MSG_INFO_RE_NUM");
            }
          }
          break;

        case OperationCode.TITO_VOID_MACHINE_TICKET:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_MACHINE_TICKET_VOID_TITLE");

            if (Ticket != null)
            {
              _ticket_label = Resource.String("STR_VOUCHER_TITO_MACHINE_TICKET_MACHINE_TITLE", Ticket.CreatedTerminalName);
            }

            _msg_info = "";
          }
          break;

        default:
          {
            Log.Error("VoucherTitoTicketOperation. OperationCode not valid: " + OperationCode.ToString());
            return;
          }
      }

      LoadVoucher("VoucherTitoTicketOperation");

      AddString("STR_VOUCHER_TITO_TICKET_OPERATION", _title_voucher);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      if (ValidationNumbers != null)
      {
        _str_html = "<tr><td><p>";
        _str_html += _ticket_label;
        _str_html += "</p></td></tr>";
        for (int _idx = 0; _idx < ValidationNumbers.Count; _idx++)
        {
          _str_html += "<tr>";
          _str_html += "<td align=\"left\"  width=\"75%\">@VALIDATION_NUMBER</td>";
          _str_html += "<td align=\"right\" width=\"25%\">@VALUE</td>";
          _str_html += "</tr>";

          _str_html = _str_html.Replace("@VALIDATION_NUMBER", ValidationNumberManager.FormatValidationNumber(ValidationNumbers[_idx].ValidationNumber, (Int32)ValidationNumbers[_idx].ValidationType, true));
          _str_html = _str_html.Replace("@VALUE", HttpUtility.HtmlEncode(ValidationNumbers[_idx].Amount.ToString()));
        }
        _str_html += "<tr><td>&nbsp;</td></tr>";

        if (!String.IsNullOrEmpty(_msg_info))
        {
          _str_html += "<tr><td>&nbsp;</td></tr>";
          _str_html += "<tr><td colspan=2><p>";
          _str_html += _msg_info;
          _str_html += "</p></td></tr>";
          _str_html += "<tr><td>&nbsp;</td></tr>";
        }

        SetParameterValue("@VALIDATION_NUMBERS", _str_html);
      }
      else
      {
        SetParameterValue("@VALIDATION_NUMBERS", "");
      }

    } // VoucherCashOut

    #endregion
  }

  public class VoucherPointToCardReplacement : Voucher
  {
    #region Constructor

    public VoucherPointToCardReplacement(String[] VoucherAccountInfo,
                                         Points InitialPoints,
                                         Points SpentPoints,
                                         PrintMode Mode,
                                         SqlTransaction SQLTransaction)

      : base(Mode, SQLTransaction)
    {
      string[] _message_params = { "", "", "", "", "", "" };

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.
      //  4. Load voucher parameters

      // 1. Load HTML structure.
      LoadVoucher("VoucherPointToCardReplacement");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      //  4. Load voucher parameters
      //      - Account Info
      //      - Initial Points Balance
      //      - Spent Points
      //      - Final Points Balance
      //      - Gift Name
      //      - Voucher track data

      Points _final_points;

      //      - Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //      - Initial Points Balance
      AddString("VOUCHER_GIFT_REQUEST_INITIAL_POINTS", InitialPoints.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Spent Points
      AddString("VOUCHER_GIFT_REQUEST_SPENT_POINTS", SpentPoints.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Final Points Balance
      _final_points = InitialPoints - SpentPoints;
      AddString("VOUCHER_GIFT_REQUEST_FINAL_POINTS", _final_points.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", "Petici�n de tarjeta");

      ////// Details
      AddString("STR_VOUCHER_GIFT_REQUEST_INITIAL_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_004") + ":");
      AddString("STR_VOUCHER_GIFT_REQUEST_SPENT_POINTS", "Tarjeta:");
      AddString("STR_VOUCHER_GIFT_REQUEST_FINAL_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_006") + ":");
    }

    #endregion

    #region Public Methods
    #endregion
  }


  //SafeKeeping Voucher (Fill In && Fill Out)
  // Format:
  //          Voucher Logo
  //          Voucher Header
  //
  // Cashier Session:
  // User: 
  // Voucher Id:
  //
  // FillIn or FillOut  Amount
  // Current Balance    Amount
  //
  //         Voucher Footer
  //
  public class VoucherSafeKeepingFillInFillOut : Voucher
  {

    #region Class Attributes

    public class VoucherSafeKeepingFillInFillOutInputParams
    {
      public String CageSession;
      public String GuiUser;
      public Int64 HolderId;
      public String HolderName;
      public OperationCode TypeFillInOutOperation;
      public Currency AmountFillInOut;
      public Currency CurrentBalance;
      public Int64 OperationId;
    }

    #endregion

    #region Constructor


    public VoucherSafeKeepingFillInFillOut(PrintMode Mode,
                                           VoucherSafeKeepingFillInFillOutInputParams InputParams,
                                           SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      String[] VcInfo;
      String _operation;

      _operation = "";
      VcInfo = new String[4];

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load descriptions
      //  4. Load Amounts
      //  5. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherSafeKeepingFillInFillOut");

      // 2. Load voucher resources.
      // - Title
      AddString("STR_VOUCHER_SAFE_KEEPING_TITLE", Resource.String("STR_SAFE_KEEPING_TITLE"));

      // 3. Load descriptions
      AddString("VOUCHER_SAFE_KEEPING_TITLE_BALANCE", Resource.String("STR_SAFE_KEEPING_ACTUAL_BALANCE"));

      switch (InputParams.TypeFillInOutOperation)
      {
        case OperationCode.SAFE_KEEPING_DEPOSIT:
          _operation = Resource.String("STR_SAFE_KEEPING_FILL_IN");

          break;
        case OperationCode.SAFE_KEEPING_WITHDRAW:
          _operation = Resource.String("STR_SAFE_KEEPING_FILL_OUT");

          break;
        default:

          break;
      }

      AddString("VOUCHER_SAFEKEEPING_HOLDER_ID_NAME", Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ": " +
                                                      InputParams.HolderId + " - " + InputParams.HolderName);

      AddString("VOUCHER_SAFE_KEEPING_TITLE_FILL_IN_OUT", _operation);

      // 4. Load Amounts
      AddString("VOUCHER_SAFE_KEEPING_AMOUNT_FILL_IN_OUT", InputParams.AmountFillInOut.ToString());
      AddString("VOUCHER_SAFE_KEEPING_AMOUNT_BALANCE", InputParams.CurrentBalance.ToString());

      // 5. Load general voucher data.
      VcInfo[0] = InputParams.CageSession;
      VcInfo[1] = InputParams.GuiUser;
      VcInfo[2] = _operation;
      VcInfo[3] = String.Empty;

      LoadVoucherCageGeneralDataFromGUI(VcInfo);

      SetValue("CV_USER_NAME", InputParams.GuiUser);
      SetValue("CV_AMOUNT", InputParams.AmountFillInOut);
      SetValue("CV_CASHIER_NAME", Environment.MachineName);
    }

    #endregion
  }


  /// <summary>
  /// Print ticket for special I/O paiments
  /// </summary>
  public class VoucherSpecials : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherSpecials(Int64 OperationId,
                          CageConceptInformation CageConcept,
                          CashierConceptOperationResult Result,
                          PrintMode Mode,
                          SqlTransaction SQLTransaction)
      : base(Mode, CageConcept.VoucherType, SQLTransaction)
    //    public VoucherSpecials(String Title,
    //                    Currency Amount,
    //                    String Concept,
    //                        PrintMode Mode,
    //                        SqlTransaction SQLTransaction)
    //: base(Mode, SQLTransaction)
    {

      String _voucher_concept;
      String _voucher_currency_type;
      String _national_currency;

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load voucher data.
      //  4. Save voucher 

      // 1. Load HTML structure.
      LoadVoucher("VoucherSpecials");

      // 2. Load general voucher data.
      VoucherSequenceId = CageConcept.SequenceId;
      SetValue("CV_M01_DEV", Result.Amount);
      if (CageConcept.CashierAction == CageMeters.CageSourceTargetCashierAction.CashierInput
          && !string.IsNullOrEmpty(GeneralParam.Value("Cashier.Voucher", "Concept.In.Footer")))
      {
        LoadVoucherGeneralData(false);
        SetParameterValue("@VOUCHER_FOOTER", GeneralParam.Value("Cashier.Voucher", "Concept.In.Footer"));
      }
      else if (CageConcept.CashierAction == CageMeters.CageSourceTargetCashierAction.CashierOutput
          && !string.IsNullOrEmpty(GeneralParam.Value("Cashier.Voucher", "Concept.Out.Footer")))
      {
        LoadVoucherGeneralData(false);
        SetParameterValue("@VOUCHER_FOOTER", GeneralParam.Value("Cashier.Voucher", "Concept.Out.Footer"));
      }
      else
      {
        LoadVoucherGeneralData();
      }

      //  3. Load voucher data.
      if (CageConcept.CashierAction == CageMeters.CageSourceTargetCashierAction.CashierInput)
      { _voucher_concept = Resource.String("STR_VOUCHER_CONCEPTS_IN"); }
      else
      { _voucher_concept = Resource.String("STR_VOUCHER_CONCEPTS_OUT"); }
      if (Result.CurrencyType != CurrencyExchangeType.CASINOCHIP)
      { _voucher_currency_type = Result.IsoCode; }
      else
      { _voucher_currency_type = Resource.String("STR_VOUCHER_OTHER_OPERATIONS_IN_CHIPS"); }

      AddString("STR_VOUCHER_TITLE", CageConcept.Name);

      _national_currency = CurrencyExchange.GetNationalCurrency();
      if (_voucher_currency_type == _national_currency)
      {
        AddString("VOUCHER_AMOUNT", ((Currency)Result.Amount).ToString());
      }
      else
      {
        AddString("VOUCHER_AMOUNT", Currency.Format(Result.Amount, _voucher_currency_type));
      }

      AddString("STR_VOUCHER_CONCEPTS_IN", _voucher_concept);

      if (!string.IsNullOrEmpty(Result.Comment))
      {
        SetParameterValue("@VOUCHER_COMMENT", "<tr><td>" + Result.Comment + "</td></tr><tr><td>&nbsp;</td></tr>");
      }
      else
      {
        SetParameterValue("@VOUCHER_COMMENT", "");
      }

      //  4. Save voucher 
      this.Save(OperationId, SQLTransaction);

    } // VoucherSpecials

    #endregion
  }

  public class VoucherCurrencyCashSummary : Voucher
  {

    #region Class Attributes

    public class ParametersCurrencyCashSummary
    {
      public Boolean IsFilterMovements;
      public Boolean HideChipsAndCurrenciesConcepts;
      public String CurrencyIsoCode;

      public ParametersCurrencyCashSummary()
      {
        IsFilterMovements = false;
        HideChipsAndCurrenciesConcepts = false;
        CurrencyIsoCode = CurrencyExchange.GetNationalCurrency();
      }
    }

    #endregion

    #region Constructor

    public VoucherCurrencyCashSummary(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, ParametersCurrencyCashSummary ParametersCashSummary)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      //LJM 18-DEC-2013: All the generating the structure, etc. Are inside the funcion, since now the loop is there
      LoadVoucherResources(CashierSessionData, ParametersCashSummary);
    }
    #endregion

    #region Private Methods

    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, ParametersCurrencyCashSummary ParametersCashSummary)
    {
      String _value;
      Decimal _currency_value;
      Decimal _balance_value;
      Decimal _short_value;
      Decimal _over_value;
      String _national_currency;
      string _currencies_html = "";
      Voucher _temp_voucher;
      String _currencies_balance = "";
      String _bank_card_balance = "";
      String _check_balance = "";
      String _chips_balance = "";
      CurrencyIsoType _currency_iso_type;
      String _short_over_national = "";

      // Load HTML structure for the current currency
      LoadVoucher("VoucherCurrencyCashSummary");

      _national_currency = ParametersCashSummary.CurrencyIsoCode;

      // Title
      AddString("STR_VOUCHER_CASH_BALANCE_TITLE", Resource.String("STR_VOUCHER_CASH_BALANCE_TITLE"));

      // Balance title
      AddString("STR_VOUCHER_CASH_SUMMARY_TITLE", Resource.String("STR_FRM_CASH_DESK_NET_BALANCE"));

      // National Currency
      AddString("STR_VOUCHER_CASH_SUMMARY_CASH", CurrencyExchange.GetDescription(CurrencyExchangeType.CURRENCY, _national_currency, ""));
      if (!ParametersCashSummary.IsFilterMovements)
      {
        AddCurrency("VOUCHER_CASH_SUMMARY_CASH", CashierSessionData.final_balance);
      }
      else
      {
        AddString("VOUCHER_CASH_SUMMARY_CASH", "---");
      }
      _currency_iso_type = new CurrencyIsoType();
      _currency_iso_type.IsoCode = _national_currency;
      _currency_iso_type.Type = CurrencyExchangeType.CURRENCY;
      if (CashierSessionData.show_short_over)
      {
        _temp_voucher = new Voucher();
        _currencies_html = "<tr valign=top>";
        _currencies_html += " <td class=\"sub-left-subitem\"><span style=\"font-size:90%\">@DELIVERED_NAME</span></td>";
        _currencies_html += " <td class=\"right\"><span style=\"font-size:90%\">@DELIVERED_VALUE</span></td>";
        _currencies_html += "</tr>";
        _currencies_html += "<tr valign=top>";
        _currencies_html += " <td class=\"sub-left-subitem\"><span style=\"font-size:90%\">@SHORT_NAME</span></td>";
        _currencies_html += " <td class=\"right\"><span style=\"font-size:90%\">@SHORT_VALUE</span></td>";
        _currencies_html += "</tr>";
        _currencies_html += "<tr valign=top>";
        _currencies_html += " <td class=\"sub-left-subitem\"><span style=\"font-size:90%\">@OVER_NAME</span></td>";
        _currencies_html += " <td class=\"right\"><span style=\"font-size:90%\">@OVER_VALUE</span></td>";
        _currencies_html += "</tr>";
        _temp_voucher.VoucherHTML = _currencies_html;

        _short_value = 0;
        if (CashierSessionData.short_currency.ContainsKey(_currency_iso_type))
        {
          _short_value = CashierSessionData.short_currency[_currency_iso_type];
        }

        _over_value = 0;
        if (CashierSessionData.over_currency.ContainsKey(_currency_iso_type))
        {
          _over_value = CashierSessionData.over_currency[_currency_iso_type];
        }

        _balance_value = CashierSessionData.final_balance + _over_value - _short_value;

        _temp_voucher.AddString("DELIVERED_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED"));
        _temp_voucher.AddCurrency("DELIVERED_VALUE", _balance_value);

        _temp_voucher.AddString("SHORT_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING"));
        _temp_voucher.AddCurrency("SHORT_VALUE", _short_value);

        _temp_voucher.AddString("OVER_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA"));
        _temp_voucher.AddCurrency("OVER_VALUE", _over_value);

        _short_over_national = _temp_voucher.VoucherHTML;
      }
      // Currencies
      foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in CashierSessionData.currencies_balance)
      {
        if (_currency.Key.Type == CurrencyExchangeType.CURRENCY && _currency.Key.IsoCode == _national_currency)
        {
          continue;
        }
        _temp_voucher = new Voucher();

        _value = CurrencyExchange.GetDescription(_currency.Key.Type, _currency.Key.IsoCode, _currency.Key.IsoCode);
        _currency_value = _currency.Value;

        _currencies_html = "<tr valign=top>";
        _currencies_html += " <td class=\"@CURRENCY_LEVEL\">@CURRENCY_NAME</td>";
        _currencies_html += " <td class=\"right\">@CURRENCY_VALUE</td>";
        _currencies_html += "</tr>";
        if (CashierSessionData.show_short_over)
        {
          _currencies_html += "<tr valign=top>";
          _currencies_html += " <td class=\"@SHORT_OVER_LEVEL\"><span style=\"font-size: 90%;\">@DELIVERED_NAME</span></td>";
          _currencies_html += " <td class=\"right\"><span style=\"font-size: 90%;\">@DELIVERED_VALUE</span></td>";
          _currencies_html += "</tr>";
          _currencies_html += "<tr valign=top>";
          _currencies_html += " <td class=\"@SHORT_OVER_LEVEL\"><span style=\"font-size: 90%;\">@SHORT_NAME</span></td>";
          _currencies_html += " <td class=\"right\"><span style=\"font-size: 90%;\">@SHORT_VALUE</span></td>";
          _currencies_html += "</tr>";
          _currencies_html += "<tr valign=top>";
          _currencies_html += " <td class=\"@SHORT_OVER_LEVEL\"><span style=\"font-size: 90%;\">@OVER_NAME</span></td>";
          _currencies_html += " <td class=\"right\"><span style=\"font-size: 90%;\">@OVER_VALUE</span></td>";
          _currencies_html += "</tr>";
        }
        _temp_voucher.VoucherHTML = _currencies_html;

        _temp_voucher.AddString("CURRENCY_NAME", _value);

        if (CurrencyMultisite.IsCenterAndMultiCurrency() || _currency.Key.IsoCode == _national_currency || _currency.Key.IsCasinoChip)
        {
          _temp_voucher.AddCurrency("CURRENCY_VALUE", (Currency)_currency_value);
        }
        else
        {
          _temp_voucher.AddString("CURRENCY_VALUE", Currency.Format(_currency_value, _currency.Key.IsoCode));
        }

        if (CashierSessionData.show_short_over)
        {
          _temp_voucher.AddString("DELIVERED_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED"));
          _temp_voucher.AddString("SHORT_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING"));
          _temp_voucher.AddString("OVER_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA"));

          _balance_value = _currency.Value;

          _currency_value = 0;
          if (CashierSessionData.short_currency.ContainsKey(_currency.Key))
          {
            _currency_value = CashierSessionData.short_currency[_currency.Key];
          }
          _balance_value -= _currency_value;
          if (CurrencyMultisite.IsCenterAndMultiCurrency() || _currency.Key.IsoCode == _national_currency || _currency.Key.IsCasinoChip)
          {
            _temp_voucher.AddCurrency("SHORT_VALUE", _currency_value);
          }
          else
          {
            _temp_voucher.AddString("SHORT_VALUE", Currency.Format(_currency_value, _currency.Key.IsoCode));
          }

          _currency_value = 0;
          if (CashierSessionData.over_currency.ContainsKey(_currency.Key))
          {
            _currency_value = CashierSessionData.over_currency[_currency.Key];
          }
          _balance_value += _currency_value;
          if (CurrencyMultisite.IsCenterAndMultiCurrency() || _currency.Key.IsoCode == _national_currency || _currency.Key.IsCasinoChip)
          {
            _temp_voucher.AddCurrency("OVER_VALUE", _currency_value);
          }
          else
          {
            _temp_voucher.AddString("OVER_VALUE", Currency.Format(_currency_value, _currency.Key.IsoCode));
          }

          if (CurrencyMultisite.IsCenterAndMultiCurrency() || _currency.Key.IsoCode == _national_currency || _currency.Key.IsCasinoChip)
          {
            _temp_voucher.AddCurrency("DELIVERED_VALUE", _balance_value);
          }
          else
          {
            _temp_voucher.AddString("DELIVERED_VALUE", Currency.Format(_balance_value, _currency.Key.IsoCode));
          }
        }


        switch (_currency.Key.Type)
        {
          case CurrencyExchangeType.CURRENCY:
            if (!ParametersCashSummary.HideChipsAndCurrenciesConcepts)
            {
              _temp_voucher.AddString("CURRENCY_LEVEL", "sub-left");
              _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left-subitem");
              _currencies_balance += _temp_voucher.VoucherHTML;
            }
            break;
          case CurrencyExchangeType.CARD:
            _temp_voucher.AddString("CURRENCY_LEVEL", "left");
            _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left");
            _bank_card_balance = _temp_voucher.VoucherHTML;
            break;
          case CurrencyExchangeType.CHECK:
            _temp_voucher.AddString("CURRENCY_LEVEL", "left");
            _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left");
            _check_balance = _temp_voucher.VoucherHTML;
            break;
          case CurrencyExchangeType.CASINOCHIP:
            if (!ParametersCashSummary.HideChipsAndCurrenciesConcepts)
            {
              _temp_voucher.AddString("CURRENCY_LEVEL", "left");
              _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left");
              _chips_balance = _temp_voucher.VoucherHTML;
            }
            break;
          default:
            break;
        }
      }

      _currencies_balance = _short_over_national + _currencies_balance;

      this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CURRENCIES", _currencies_balance);

      // Bank Card
      this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_BANK_CARD", _bank_card_balance);

      // Check
      this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHECK", _check_balance);

      if (!ParametersCashSummary.HideChipsAndCurrenciesConcepts)
      {
        // Chips
        this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHIPS", _chips_balance);
      }
      else
      {
        this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHIPS", "");
      }
    }

    #endregion

    #region Public Methods

    #endregion
  }

  public class VoucherHandpayAuthorized : Voucher
  {
    #region Class Attributes

    public class VoucherHandpayAuthorizedInputParams
    {
      public String TerminalName;
      public String TerminalProvider;
      public String TypeName;
      public Currency TotalRedeemed;
      public Currency Taxes;
      public Currency TotalToPay;
      public PrintMode Mode;
      public String HandpayNumber;
      public Currency TaxAmount;
      public Currency TaxBaseAmount;
      public Decimal TaxPct;
    }

    #endregion

    #region Constructor

    public VoucherHandpayAuthorized(VoucherHandpayAuthorizedInputParams InputParams, OperationCode OperationCode,
                                    SqlTransaction SQLTransaction)
      : base(InputParams.Mode, OperationCode, SQLTransaction)
    {
      Currency _rounding;
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherAuthorization");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // Setup voucher's data
      //    - Account Number and Account Holder Name
      //    - Pay withot tax
      //    - Taxes
      //    - Total to pay

      AddString("VOUCHER_AUTHORITZATION_TERMINAL_NAME", InputParams.TerminalName);
      AddString("VOUCHER_AUTHORITZATION_PROVIDER_NAME", InputParams.TerminalProvider);
      AddString("VOUCHER_AUTHORITZATION_TYPE", InputParams.TypeName);

      AddCurrency("VOUCHER_AUTHORITZATION_INITIAL_AMOUNT_TO_TRANSFER", InputParams.TotalRedeemed);
      AddCurrency("VOUCHER_AUTHORITZATION_DEDUCTIONS", -InputParams.Taxes);
      AddCurrency("VOUCHER_AUTHORITZATION_AMOUNT_TO_TRANSFER", InputParams.TotalToPay);

      _rounding = (InputParams.TotalToPay - InputParams.TotalRedeemed) + InputParams.Taxes;

      if (Utils.IsTitoMode() && _rounding != 0)
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_ROUNDING", Resource.String("STR_UC_CARD_BALANCE_ROUNDING") + ":");
        AddCurrency("VOUCHER_CARD_HANDPAY_ROUNDING", _rounding);
      }
      else
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_ROUNDING", "");
        AddString("VOUCHER_CARD_HANDPAY_ROUNDING", "");
      }

      if (GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.TotalInLetters", false))
      {
        AddCurrencyInLetters("VOUCHER_AMOUNT_TO_TRANSFER_STRING", InputParams.TotalToPay);
      }
      else
      {
        AddString("VOUCHER_AMOUNT_TO_TRANSFER_STRING", "");
      }

      SetSignatureRoomByVoucherType(this.GetType());

      //      - Voucher track data
      AddString("VOUCHER_HANDPAY_REQUEST_TRACKDATA", InputParams.HandpayNumber);

      if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        String _param_name_tax;
        String _pct;
        StringBuilder _html_code;

        _html_code = new StringBuilder();

        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("	<td>");
        _html_code.AppendLine("		<hr noshade size='4' />");
        _html_code.AppendLine("	</td>");
        _html_code.AppendLine("</tr>");
        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("	<td>");
        _html_code.AppendLine("		<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td colspan='3'>");
        _html_code.AppendLine("					@VOUCHER_TAX_DETAIL:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td width='5%'>");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("				<td>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX_NAME:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("				<td width='40%' align='right'>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			@TAXES");
        _html_code.AppendLine("		</table>");
        _html_code.AppendLine("	</td>");
        _html_code.AppendLine("</tr>");
        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("	<td>");
        _html_code.AppendLine("		<hr noshade size='4' />");
        _html_code.AppendLine("	</td>");
        _html_code.AppendLine("</tr>");

        SetParameterValue("@TABLE_WITHHOLDING", _html_code.ToString());

        _param_name_tax = Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING");
        _pct = String.Format("{0:P2}", InputParams.TaxPct);

        AddString("VOUCHER_TAX_DETAIL", Resource.String("STR_HANDPAY_TAX_DETAIL", _param_name_tax.ToString()));
        AddString("VOUCHER_BASE_TAX_NAME", Resource.String("STR_BASE"));
        SetParameterValue("@TAXES", DevolutionPrizeParts.GetTaxesHtml(InputParams.TaxAmount != 0 ? InputParams.TaxBaseAmount : 0, true, true, false, true, OperationCode));

        AddCurrency("VOUCHER_BASE_TAX", InputParams.TaxAmount != 0 ? InputParams.TaxBaseAmount : 0);

      }
      else
      {
        SetParameterValue("@TABLE_WITHHOLDING", String.Empty);
      }

    } // VoucherAuthoritzation

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_AUTHORIZATION_001"));

      // Details
      AddString("STR_VOUCHER_CARD_HANDPAY_TERMINAL_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_008") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_PROVIDER_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_009") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_TYPE", Resource.String("STR_VOUCHER_CARD_HANDPAY_010") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_005") + ":");
      if (Utils.IsTitoMode())
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_001") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_DEDUCTIONS", Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING") + ":");
      }
      else
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_004") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_006") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_DEDUCTIONS", "");
      }
      AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_007"));
    }

    #endregion

    #region Public Methods

    #endregion
  }

  public class VoucherGamingTableSessionOverview : Voucher
  {
    #region Class Attributes

    public class ParametersGamingTableSessionOverview
    {
      public Color BackColor;

      public ParametersGamingTableSessionOverview()
      {
        BackColor = Color.Transparent;
      }
    }
    #endregion

    #region Constructor

    public VoucherGamingTableSessionOverview(WSI.Common.GamingTable GamingTable
                                 , PrintMode Mode
                                 , ParametersGamingTableSessionOverview ParametersOverview
                                  )
      : base(Mode, null)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.


      // 1. Load HTML structure.
      LoadVoucher("VoucherGamingTableSessionInfo");

      // 2. Load NLS strings from resource.
      LoadVoucherResources(GamingTable, ParametersOverview);

      // 4. Load specific voucher data.
      LoadVoucherData(GamingTable);

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.GamingTable GamingTable, ParametersGamingTableSessionOverview ParametersOverview)
    {
      String _value;

      // Fill IN
      _value = Resource.String("STR_GAMING_TABLE_REPORT_FILL_IN");
      AddString("STR_FILL_IN", _value);

      // Fill IN - Initial 
      _value = Resource.String("STR_GAMING_TABLE_REPORT_INITIAL_AMOUNT");
      AddString("STR_INITIAL_AMOUNT", _value);

      // Fill IN - Total 
      _value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");
      AddString("STR_TOTAL_FILL_IN", _value);

      // Withdraw
      _value = Resource.String("STR_GAMING_TABLE_REPORT_WITHDRAW");
      AddString("STR_TOTAL_WITHDRAW", _value);

      // Drop
      _value = Resource.String("STR_GAMING_TABLE_REPORT_DROP");
      AddString("STR_TOTAL_DROP", _value);

      // Session Name
      _value = GamingTable.GamingTableName;
      AddString("SESSION_NAME", _value);

      // Session Date Opening
      _value = GamingTable.CashierSessionOpening.ToString("dd/MMM/yyyy HH:mm");
      AddString("SESSION_DATE_OPEN", _value);

      SetParameterValue("@VOUCHER_STYLE", "BODY{background-color:'" + ColorTranslator.ToHtml(ParametersOverview.BackColor) + "';}");


    } // LoadVoucherResources

    /// <summary>
    /// Create cell with value
    /// </summary>
    private String GetHtmlStringWithValue(String Value)
    {
      String _str_currency;

      _str_currency = string.Empty;

      _str_currency = @"<td class=""left"">";
      _str_currency += Value;
      _str_currency += @"</td>";

      return _str_currency;
    } // GetHtmlStringWithValue

    #endregion

    #region Public Methods

    /// <summary>
    /// Load Personalized Data into voucher
    /// </summary>
    public void LoadVoucherData(WSI.Common.GamingTable GamingTable)
    {
      String _str;
      int _idx;
      int _num_currencies;

      _str = string.Empty;

      // Decrease 1 more for Headers Rows
      _num_currencies = GamingTable.CashierSessionSummary.Columns.Count - 1;

      // Currency Headers
      for (_idx = 0; _idx < _num_currencies; _idx++)
      {
        //Literal Chips
        if (GamingTable.CashierSessionSummary.Columns[_idx + 1].ColumnName == Cage.CHIPS_ISO_CODE.ToString())
        {
          _str += GetHtmlStringWithValue(Resource.String("STR_CURRENCY_TYPE_0_ISO_X01"));
        }
        else
        {
          _str += GetHtmlStringWithValue(GamingTable.CashierSessionSummary.Columns[_idx + 1].ColumnName);
        }

      }
      SetParameterValue("@STR_HEADERS_ISO_CODE", _str);
      _str = string.Empty;

      // Fill in Rows
      for (_idx = 0; _idx < _num_currencies; _idx++)
      {
        _str += GetHtmlStringWithValue(String.Empty);

      }
      SetParameterValue("@STR_ROWS_FILL_IN", _str);
      _str = string.Empty;

      // Currency Initial Amount
      for (_idx = 0; _idx < _num_currencies; _idx++)
      {
        if (GamingTable.CashierSessionSummary.Rows[0].ItemArray[_idx + 1].ToString() == String.Empty)
        {
          _str += GetHtmlStringWithValue(Currency.Format((Decimal)0, ""));
        }
        else
        {
          _str += GetHtmlStringWithValue(Currency.Format((Decimal)GamingTable.CashierSessionSummary.Rows[0].ItemArray[_idx + 1], ""));
        }
      }
      SetParameterValue("@STR_VALUE_INITIAL_FILL_IN", _str);
      _str = string.Empty;

      // Currency Total Fill IN 
      for (_idx = 0; _idx < _num_currencies; _idx++)
      {
        if (GamingTable.CashierSessionSummary.Rows[1].ItemArray[_idx + 1] == System.DBNull.Value)
        {
          _str += GetHtmlStringWithValue(Currency.Format(0, ""));
        }
        else
        {
          _str += GetHtmlStringWithValue(Currency.Format((Decimal)GamingTable.CashierSessionSummary.Rows[1].ItemArray[_idx + 1], ""));
        }

      }
      SetParameterValue("@STR_VALUE_TOTAL_FILL_IN", _str);
      _str = string.Empty;

      // Currency Total Withdraw
      for (_idx = 0; _idx < _num_currencies; _idx++)
      {
        if (GamingTable.CashierSessionSummary.Rows.Count <= 2 || GamingTable.CashierSessionSummary.Rows[2].ItemArray[_idx + 1] == System.DBNull.Value)
        {
          _str += GetHtmlStringWithValue(Currency.Format(0, ""));
        }
        else
        {
          _str += GetHtmlStringWithValue(Currency.Format((Decimal)GamingTable.CashierSessionSummary.Rows[2].ItemArray[_idx + 1], ""));
        }
      }
      SetParameterValue("@STR_VALUE_TOTAL_WITHDRAW", _str);
      _str = string.Empty;

      // Currency Drop
      SetParameterValue("@STR_VALUE_TOTAL_DROP", GetHtmlStringWithValue(Currency.Format((Decimal)GamingTable.CashierSessionDrop, "")));

    } //LoadVoucherData

    #endregion
  }


  public class VoucherCashOutCompanyB : Voucher
  {
    #region Constructor

    public VoucherCashOutCompanyB(String[] VoucherAccountInfo
                       , TYPE_SPLITS SplitInformation
                       , TYPE_CASH_REDEEM CompanyB
                       , CASHIER_MOVEMENT MovementType
                       , OperationCode OperationCode
                       , PrintMode Mode
                       , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {
      String _concept_name;
      Currency _concept_value;

      // 1. Load HTML structure.
      switch (MovementType)
      {
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT:
          {
            LoadVoucher("Voucher.B.CashOut");
            LoadHeader(SplitInformation.company_b.header);
            LoadFooter(SplitInformation.company_b.footer, SplitInformation.company_b.footer_cancel);
            AddString("VOUCHER_SPLIT_NAME", SplitInformation.company_b.cash_out_voucher_title);

            VoucherSequenceId = SequenceId.VouchersSplitB;
            if (this.VoucherMode == 3)
            {
              VoucherSequenceId = SequenceId.VouchersSplitBCancel;
            }

            SetValue("CV_TYPE", (Int32)CashierVoucherType.CardDepositOut_B);

            _concept_name = Misc.GetCardPlayerConceptName();
            _concept_value = CompanyB.card_deposit;

            SetValue("CV_M01_BASE", CompanyB.card_deposit);
            SetValue("CV_M01_TAX1", CompanyB.tax1);
            SetValue("CV_M01_TAX2", CompanyB.tax2);
            SetValue("CV_M01_DEV", CompanyB.TotalDevolution);

          }
          break;

        default:
          {
            Log.Error("VoucherCashOutCompanyB. MovementType not valid: " + MovementType.ToString());
            return;
          }
      }

      // 2. Load general voucher data.
      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      // 3. Load specific voucher data.
      AddString("VOUCHER_CONCEPT", _concept_name);
      AddCurrency("VOUCHER_CARD_CASH_OUT_AMOUNT", _concept_value);
      AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyB.TotalPaid - CompanyB.TotalDevolution);

    } // VoucherCashOutCompanyB

    #endregion
  }

  public class VoucherTransferCashiersReceived : Voucher
  {

    #region Constructor

    public VoucherTransferCashiersReceived(CashierVoucherType VoucherType, CashierSessionInfo SourceCashierSession, CashierSessionInfo TargetCashierSession, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, VoucherType, SQLTransaction)
    {
      InitializeVoucher(SourceCashierSession, TargetCashierSession);
    }

    private void InitializeVoucher(CashierSessionInfo SourceCashierSession, CashierSessionInfo TargetCashierSession)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTransferCashiersReceived");

      // 2. Load voucher resources.
      LoadVoucherResources(SourceCashierSession, TargetCashierSession);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    /// <param name="MovementType">Movement Type:
    ///                            Fill In
    ///                            Open Session</param>
    private void LoadVoucherResources(CashierSessionInfo SourceCashierSession, CashierSessionInfo TargetCashierSession)
    {
      String value = "";

      // - Title
      value = Resource.String("STR_VOUCHER_CASHIERS_TRANSFER_IN_TITLE");
      AddString("STR_VOUCHER_CASHIERS_TRANSFER_IN_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      // - Transfer Information
      List<string> _data = new List<string>();
      _data.Add(Resource.String("STR_VOUCHER_CASHIERS_SOURCE_SESSION_NAME"));
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + SourceCashierSession.UserName);
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + SourceCashierSession.TerminalName);
      _data.Add(Resource.String("STR_VOUCHER_CASHIERS_TARGET_SESSION_NAME"));
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + TargetCashierSession.UserName);
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + TargetCashierSession.TerminalName);
      AddMultipleLineString("CASHIER_SESSION_SOURCE_TARGET", _data.ToArray(), false);
    } // LoadVoucherResources

    #endregion

  }

  public class VoucherTransferCashiersSent : Voucher
  {

    #region Constructor

    public VoucherTransferCashiersSent(CashierVoucherType CashierVoucherType, CashierSessionInfo SourceCashierSession, CashierSessionInfo TargetCashierSession, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, CashierVoucherType, SQLTransaction)
    {
      InitializeVoucher(SourceCashierSession, TargetCashierSession);
    }

    private void InitializeVoucher(CashierSessionInfo SourceCashierSession, CashierSessionInfo TargetCashierSession)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTransferCashiersSent");

      // 2. Load voucher resources.
      LoadVoucherResources(SourceCashierSession, TargetCashierSession);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(CashierSessionInfo SourceCashierSession, CashierSessionInfo TargetCashierSession)
    {
      String value = "";

      // - Title
      value = Resource.String("STR_VOUCHER_CASHIERS_TRANSFER_OUT_TITLE");
      AddString("STR_VOUCHER_CASHIERS_TRANSFER_OUT_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      // - Transfer Information
      List<string> _data = new List<string>();
      _data.Add(Resource.String("STR_VOUCHER_CASHIERS_SOURCE_SESSION_NAME"));
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + SourceCashierSession.UserName);
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + SourceCashierSession.TerminalName);
      _data.Add(Resource.String("STR_VOUCHER_CASHIERS_TARGET_SESSION_NAME"));
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + TargetCashierSession.UserName);
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + TargetCashierSession.TerminalName);
      AddMultipleLineString("CASHIER_SESSION_SOURCE_TARGET", _data.ToArray(), false);

    } // LoadVoucherResources

    #endregion

  }

  public class VoucherTerminalsCollect : Voucher
  {

    #region Constructor

    public VoucherTerminalsCollect(CashierVoucherType VoucherType, List<TerminalCollectRefill> TerminalsToCollectAndRefill, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, VoucherType, SQLTransaction)
    {
      InitializeVoucher(TerminalsToCollectAndRefill);
    }

    private void InitializeVoucher(List<TerminalCollectRefill> TerminalsToCollectAndRefill)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTerminalsCollect");

      // 2. Load voucher resources.
      LoadVoucherResources(TerminalsToCollectAndRefill);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(List<TerminalCollectRefill> TerminalsToCollectAndRefill)
    {
      String value = "";

      // - Title
      value = Resource.String("STR_TERMINAL_COLLECT_TITLE");
      AddString("STR_TERMINAL_COLLECT_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

    } // LoadVoucherResources

    #endregion

  }
  public class VoucherTerminalsCollectDetail : Voucher
  {

    #region Constructor

    public VoucherTerminalsCollectDetail(String TerminalName, Decimal TotalToCollect, Decimal TotalToRefill)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTerminalsCollectDetail");

      // 2. Load voucher resources.
      LoadVoucherResources(TerminalName, TotalToCollect, TotalToRefill);
    }

    #endregion

    #region Private Methods
    private void LoadVoucherResources(String TerminalName, Decimal TotalToCollect, Decimal TotalToRefill)
    {
      String value;
      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);
      AddString("VOUCHER_TERMINAL_COLLECT_REFILL_NAME", TerminalName);

      value = Resource.String("STR_LBL_REFILL") + ":";
      AddString("STR_LBL_REFILL", value);
      AddCurrency("VOUCHER_REFILL_AMOUNT", TotalToRefill);

      value = Resource.String("STR_LBL_COLLECT") + ":";
      AddString("STR_LBL_COLLECT", value);
      AddCurrency("VOUCHER_COLLECT_AMOUNT", TotalToCollect);
    }
    #endregion

  }

  public class VoucherTerminalsRefill : Voucher
  {

    #region Constructor

    public VoucherTerminalsRefill(CashierVoucherType VoucherType, List<TerminalCollectRefill> TerminalsToRefill, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, VoucherType, SQLTransaction)
    {
      InitializeVoucher(TerminalsToRefill);
    }

    private void InitializeVoucher(List<TerminalCollectRefill> TerminalsToRefill)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTerminalsRefill");

      // 2. Load voucher resources.
      LoadVoucherResources(TerminalsToRefill);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(List<TerminalCollectRefill> TerminalsToRefill)
    {
      String value = "";

      // - Title
      value = Resource.String("STR_TERMINAL_REFILL_TITLE");
      AddString("STR_TERMINAL_REFILL_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

    } // LoadVoucherResources

    #endregion

  }
  public class VoucherTerminalsRefillDetail : Voucher
  {

    #region Constructor

    public VoucherTerminalsRefillDetail(String TerminalName, Decimal TotalToRefill)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTerminalsRefillDetail");

      // 2. Load voucher resources.
      LoadVoucherResources(TerminalName, TotalToRefill);
    }

    #endregion

    #region Private Methods
    private void LoadVoucherResources(String TerminalName, Decimal TotalToRefill)
    {
      String value;
      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);
      AddString("VOUCHER_TERMINAL_COLLECT_REFILL_NAME", TerminalName);

      value = Resource.String("STR_LBL_REFILL") + ":";
      AddString("STR_LBL_REFILL", value);
      AddCurrency("VOUCHER_REFILL_AMOUNT", TotalToRefill);
  }
    #endregion

  }
  public class VoucherReception : Voucher
  {

    #region Constructor

    public VoucherReception(String[] VoucherAccountInfo
                            , CashierVoucherType VoucherType
                            , Int64 AccountId
                            , Currency[] Amount
                            , CurrencyExchangeResult ExchangeResult
                            , String Coupon
                            , Int32 ValidGamingDays
                            , PrintMode Mode
                            , SqlTransaction SQLTransaction)
      : base(Mode, VoucherType, SQLTransaction)
    {      
      InitializeVoucher(VoucherAccountInfo, AccountId, Amount, ExchangeResult, Coupon, ValidGamingDays);

      PrintBarcode();
    }

    private void InitializeVoucher(String[] VoucherAccountInfo
                                  , Int64 AccountId
                                  , Currency[] Amount
                                  , CurrencyExchangeResult ExchangeResult
                                  , String Coupon
                                  , Int32 ValidGamingDays)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherReception");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load voucher resources.
      LoadVoucherResources(VoucherAccountInfo, Amount, Coupon, ExchangeResult, ValidGamingDays);
      
      VoucherSequenceId = SequenceId.VoucherEntrances;

      LoadFooter(String.Empty, GeneralParam.GetString("Cashier.Voucher", "Footer"));

      // 3. Load general voucher data.
      LoadVoucherGeneralData(true);
      
  }

    #endregion
    private string EncodeHTMLSpecialChars(String InputString, Boolean HTMLSpaceChar)
    {
      String _html;

      _html = HttpUtility.HtmlEncode(InputString);

      if (HTMLSpaceChar)
      {
        _html = _html.Replace(" ", "&nbsp;");
      }

      return _html;
    }
    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , Currency[] Amount
                                    , String Coupon
                                    , CurrencyExchangeResult ExchangeResult
                                    , Int32 ValidationGamingDays)
    {
      String value = "";     
      String[] _params; 
      String _national_currency;
      String _str_aux;
      string _str_dif;
      string _diff;

      _diff= string.Empty;
      _str_dif = "";

      _national_currency = CurrencyExchange.GetNationalCurrency();
      
      // - Title
      value = Resource.String("STR_VOUCHER_RECEPTION_TITLE");
      AddString("STR_VOUCHER_RECEPTION_TITLE", value);

      //Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //Base Price
      value = Resource.String("STR_VOUCHER_RECEPTION_CONCEPT");
      AddString("STR_VOUCHER_RECEPTION_CONCEPT", value);

      //Difference
      value = Resource.String("STR_VOUCHER_RECEPTION_DIF_CONCEPT");
      AddString("STR_VOUCHER_RECEPTION_DIF_CONCEPT", value);


      if (ExchangeResult == null || !ExchangeResult.WasForeingCurrency)
      {
        if (Amount[1] < Amount[0])
        {
          AddCurrency("STR_VOUCHER_RECEPTION_AMOUNT", Amount[0]);
          _diff = EncodeHTMLSpecialChars(Amount[2].ToString(),true);
        }
        else
        {
          AddCurrency("STR_VOUCHER_RECEPTION_AMOUNT", Amount[1]);
        }
        //Paid Price 
        AddCurrency("STR_VOUCHER_RECEPTION_TOTAL_AMOUNT", Amount[1]);
        AddString("VOUCHER_CHANGE_APPLIED", String.Empty);
      }
      else
      {
        if (Amount[1] < Amount[0])
        {
          _diff = Currency.Format(Amount[2], ExchangeResult.InCurrencyCode);
          AddString("STR_VOUCHER_RECEPTION_AMOUNT", Currency.Format(Amount[0], ExchangeResult.InCurrencyCode));
        }
        else
        {
          AddString("STR_VOUCHER_RECEPTION_AMOUNT", Currency.Format(Amount[1], ExchangeResult.InCurrencyCode));
        }

        //Paid Price 
        AddString("STR_VOUCHER_RECEPTION_TOTAL_AMOUNT", Currency.Format(Amount[1], ExchangeResult.InCurrencyCode));

        _str_aux = Resource.String("STR_VOUCHER_APPLIED_EXCHANGE");

        SetParameterValue("@VOUCHER_CHANGE_APPLIED",
          "<tr>" +
          "<td align=\"left\" >" + _str_aux + " " + ExchangeResult.InCurrencyCode + ":</td>" +
          "<td align=\"right\" >" + CurrencyExchange.ChangeRateFormat(ExchangeResult.ChangeRate) + " " +
          _national_currency +
          "</td>" +
          "</tr>" +
          "<tr>" +
          "<td align=\"left\" >" + Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS") + "</td>" +
          "<td align=\"right\" >" + Currency.Format(ExchangeResult.InAmount, _national_currency) +
          "</td>" +
          "</tr>");


      }
      if (!string.IsNullOrEmpty(_diff))
      {
        _str_dif = "<tr>" +
                   "<td align=\"left\" style=\"width: 40%\">" +
                    Resource.String("STR_VOUCHER_RECEPTION_DIF_CONCEPT") +
                   "</td>" +
                   "<td align=\"right\" width=\"60%\">" +
                  _diff +
                   "</td>" +
                   "</tr>";
      }

      SetParameterValue("@VOUCHER_DIFFERENCE", _str_dif);

      if (!String.IsNullOrEmpty(Coupon))
      {
        value = Resource.String("STR_FRM_RECEPTION_TICKET_COUPON_NUMBER") + " " + Coupon;
      }
      else 
      {
        value = String.Empty;
      }
      AddString("STR_VOUCHER_RECEPTION_COUPON", value);

      //Comissions 
      value = String.Empty;//Resource.String("STR_VOUCHER_RECEPTION_COMSIONS");
      AddString("STR_VOUCHER_RECEPTION_COMSIONS", value);
      
      //TOTAL text
      value = Resource.String("STR_VOUCHER_RECEPTION_TOTAL");
      AddString("STR_VOUCHER_RECEPTION_TOTAL_TEXT", value);

      //Expiration 
      _params = new String[1];
      _params[0] = Entrance.GetExpirationDate(ValidationGamingDays).ToShortDateString();
      value = Resource.String("STR_VOUCHER_EXPIRATION_TEXT", _params);
      AddString("STR_VOUCHER_EXPIRATION_TEXT", value);
      
    } // LoadVoucherResources

    private void PrintBarcode()
    {
      Barcode _barcode; 
      String _style;
      String _style_barcode;
      String _barcode_data;


      //Cashier Session Info: Cashier.CashierSessionInfo();  
            
      _barcode = Barcode.Create(BarcodeType.Reception, VoucherSeqNumber);  

      _style = String.Empty;
      _style_barcode = String.Empty;
      _barcode_data = String.Empty;

      _barcode_data += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
      _barcode_data += "  <tr>";
      _barcode_data += "    <td colspan=\"2\" align=\"center\">";
      _barcode_data += "      <div id='inputdata'/>";
      _barcode_data += "     </td>";
      _barcode_data += "  </tr>";
      _barcode_data += "</table>";

      _style_barcode = "<script type='text/javascript'>$('#inputdata').barcode('#PAYMENT_BARCODE_ORDER_ID#', 'int25');</script>";
      _style_barcode = _style_barcode.Replace("#PAYMENT_BARCODE_ORDER_ID#", _barcode.ExternalCode);

      SetParameterValue("@VOUCHER_BARCODE_STYLE", _style_barcode);
      SetParameterValue("@VOUCHER_BARCODE_DATA", _barcode_data);
    }

    #endregion

  }


  public class VoucherReceptionLOPD : Voucher
  {

    #region Constructor

    public VoucherReceptionLOPD(String[] VoucherAccountInfo
                            , CashierVoucherType VoucherType
                            , Int64 AccountId
                            , PrintMode Mode
                            , SqlTransaction SQLTransaction)
      : base(Mode, VoucherType, SQLTransaction)
    {
      InitializeVoucher(VoucherAccountInfo, AccountId);
    }

    private void InitializeVoucher(String[] VoucherAccountInfo
                                  , Int64 AccountId)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherReceptionLOPD");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load voucher resources.
      LoadVoucherResources(VoucherAccountInfo);

      //VoucherSequenceId = SequenceId.AccountId;
      
      LoadFooter(GeneralParam.GetString("Cashier.Voucher", "Footer"), String.Empty);
            

      // 3. Load general voucher data.
      LoadVoucherGeneralData(true);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo)
    {
      String value = "";
      String _national_currency;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      // - Title
      value = Resource.String("STR_VOUCHER_RECEPTION_TITLE");
      AddString("STR_VOUCHER_RECEPTION_TITLE", value);

      //Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);
      

      SetParameterValue("@VOUCHER_LOPD_DESCRIPTION1", GeneralParam.GetString("Reception", "Agreement.Text1"));

      // JMV - 07-06-16
      SetParameterValue("@VOUCHER_LOPD_DESCRIPTION2", GeneralParam.GetString("Reception", "Agreement.Text2"));
      SetParameterValue("@VOUCHER_LOPD_DESCRIPTION3", GeneralParam.GetString("Reception", "Agreement.Text3"));
      SetParameterValue("@VOUCHER_LOPD_DESCRIPTION4", GeneralParam.GetString("Reception", "Agreement.Text4"));

    } // LoadVoucherResources
    
    #endregion
      }


  // Bucket Change
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Bucket name:
  // Bucket initial:    $/#
  // Bucket increment:  $/#
  // Bucket final:      $/#
  //
  //         Voucher Footer
  public class VoucherBucketChange : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructores

    public VoucherBucketChange(BucketVoucher BucketVoucher, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Set specific params

      // 1. Load HTML structure.
      LoadVoucher("VoucherBucketChange");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Set params
      LoadVoucherResources(BucketVoucher);
  }
    #endregion

    #region Private Methods
    private void LoadVoucherResources(BucketVoucher BucketVoucher)
    {
      String paramName;
      String paramValue;
      paramName = "STR_VOUCHER_BUCKET_CHANGE_TITLE";
      paramValue = Resource.String(paramName) + " " + BucketVoucher.BucketName;
      AddString(paramName, paramValue);

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", BucketVoucher.AccountInfo);

      paramName = "STR_BUCKET_INITIAL";
      paramValue = Resource.String("STR_UC_BANK_INITIAL") + ":";
      AddString(paramName, paramValue);

      paramName = "STR_BUCKET_INCREMENT";
      paramValue = Resource.String("STR_UC_BANK_INCREMENT") + ":";
      AddString(paramName, paramValue);

      paramName = "STR_BUCKET_FINAL";
      paramValue = Resource.String("STR_UC_BANK_FINAL") + ":";
      AddString(paramName, paramValue);

      paramName = "STR_BUCKET_DETAILS";
      paramValue = Resource.String("STR_UC_PLAYER_TRACK_COMMENTS") + ":";
      AddString(paramName, paramValue);

      AddString("VOUCHER_BUCKET_INITIAL", BucketVoucher.Initial);
      AddString("VOUCHER_BUCKET_INCREMENT", BucketVoucher.Increment);
      AddMultipleLineString("VOUCHER_BUCKET_DETAILS", BucketVoucher.Details);
      AddString("VOUCHER_BUCKET_FINAL", BucketVoucher.Final);
    }
    #endregion

  }

  public class VoucherTaxProvisions : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherTaxProvisions(String[] VoucherAccountInfo
                                , TYPE_SPLITS Splits
                                , Currency TaxProvisions
                                , PrintMode Mode
                                , SqlTransaction Trx)
      : base(Mode, Trx)
    {


      // 1. Load HTML structure.
      LoadVoucher("VoucherTaxProvisions");

      //  4. Load NLS strings from resource
      LoadVoucherResources(VoucherAccountInfo, Splits, TaxProvisions);
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , TYPE_SPLITS Splits
                                    , Currency TaxProvisions)
    {
      Boolean _hide_total;
      Boolean _add_currency_in_letters;
      
      SetValue("CV_TYPE", (Int32)CashierVoucherType.TaxProvisions);
      VoucherSequenceId = SequenceId.VoucherTaxProvisions;

      //  2. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_PROVISIONS_TITLE")));
      AddString("VOUCHER_CONCEPT", GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Name", Resource.String("STR_VOUCHER_TAX_PROVISIONS_CONCEPT")));

      AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", TaxProvisions);

      _hide_total = Splits.company_a.hide_total;
      _add_currency_in_letters = Splits.company_a.total_in_letters;

      if (_hide_total)
      {
        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT", "");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT",
            "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
              "<tr>" +
                "<td align=\"center\">TOTAL</td>" +
              "</tr>" +
              "<tr>" +
                "<td align=\"center\"><B>@VOUCHER_CARD_ADD_TOTAL_AMOUNT</B></td>" +
              "</tr>" +
            "</table>" +
            "<hr noshade size=\"4\">");

        AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", TaxProvisions, _add_currency_in_letters);
      }     

    }

    #endregion

  }

  public class VoucherGamingHallCashDeskStatus : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherGamingHallCashDeskStatus(PrintMode Mode, SqlTransaction SQLTransaction, 
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherGamingHallCashDeskStatus");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load voucher resources and data
      LoadVoucherResources(CashierSessionStats);
     
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String _value = "";
      TYPE_SPLITS _split_data;
      //VoucherCurrencyCashSummary.ParametersCurrencyCashSummary _parameters_currency_cash_summary;
      Boolean _show_refill_cashier;
      Boolean _show_refill_cage;
      Boolean _show_collect_cashier;
      Boolean _show_collect_cage;

      if (!Split.ReadSplitParameters(out _split_data))
      {
        return;
      }

      // CASH SUMMARY
      /*_parameters_currency_cash_summary = new VoucherCurrencyCashSummary.ParametersCurrencyCashSummary();
      _parameters_currency_cash_summary.HideChipsAndCurrenciesConcepts = ParametersOverview.HideChipsAndCurrenciesConcepts;
      _parameters_currency_cash_summary.IsFilterMovements = ParametersOverview.IsFilterMovements;
      _parameters_currency_cash_summary.CurrencyIsoCode = ParametersOverview.CurrencyIsoCode;
      _value = new VoucherCurrencyCashSummary(CashierSessionStats, _parameters_currency_cash_summary).VoucherHTML;
      this.VoucherHTML = this.VoucherHTML.Replace("@STR_VOUCHER_CASH_DESK_CASH_SUMMARY", _value);*/
      

      // NLS Values

      // - Title
      _value = Resource.String("STR_VOUCHER_CASH_DESK_STATUS_TITLE");
      AddString("STR_VOUCHER_CASH_DESK_STATUS_TITLE", _value);

      // - General
      _value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", _value);

      _value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", _value);

      _value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", _value);

      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_DEPOSIT");
      AddString("STR_VOUCHER_DEPOSITS", _value);
      AddCurrency("VOUCHER_DEPOSITS", CashierSessionStats.deposits);

      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_WITHDRAW");
      AddString("STR_VOUCHER_WITHDRAW", _value);
      AddCurrency("VOUCHER_WITHDRAW", CashierSessionStats.withdraws);

      _value = Resource.String("STR_UC_BANK_HANDPAYS");
      AddString("STR_VOUCHER_HANDPAYS_TITLE", _value);
      AddCurrency("VOUCHER_TOTAL_HANDPAYS", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

      _show_refill_cashier = GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cashier != 0;
      _show_refill_cage = !GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cage != 0;
      _show_collect_cashier = GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cashier != 0;
      _show_collect_cage = !GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cage != 0;

      AddCurrency("VOUCHER_TOTAL_REFILL_TO_CASHIER", CashierSessionStats.refill_amount_to_cashier);
      AddString("STR_VOUCHER_TOTAL_REFILL_TO_CASHIER_HIDDEN", _show_refill_cashier ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_REFILL_TO_CAGE", CashierSessionStats.refill_amount_to_cage);
      AddString("STR_VOUCHER_TOTAL_REFILL_TO_CAGE_HIDDEN", _show_refill_cage ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CASHIER", CashierSessionStats.collect_amount_to_cashier);
      AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CASHIER_HIDDEN", _show_collect_cashier ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CAGE", CashierSessionStats.collect_amount_to_cage);
      AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CAGE_HIDDEN", _show_collect_cage ? "" : "hidden");

      _value = Resource.String("STR_VOUCHER_CONCEPTS_IN");
      AddString("STR_VOUCHER_CONCEPTS_IN", _value);
      AddCurrency("VOUCHER_CONCEPTS_IN", CashierSessionStats.concepts_input);

      _value = Resource.String("STR_VOUCHER_CONCEPTS_OUT");
      AddString("STR_VOUCHER_CONCEPTS_OUT", _value);
      AddCurrency("VOUCHER_CONCEPTS_OUT", CashierSessionStats.concepts_output);

      _value = Resource.String("STR_FRM_CASH_DESK_NET_BALANCE");
      AddString("STR_VOUCHER_NATIONAL_BALANCE", _value);

      AddCurrency("VOUCHER_GAMING_HALL_BALANCE", CashierSessionStats.final_balance);
      _value = Resource.String("STR_VOUCHER_TOTAL_REFILL");
      AddString("STR_VOUCHER_TOTAL_REFILL", _value);
      _value = Resource.String("STR_VOUCHER_TOTAL_COLLECT");
      AddString("STR_VOUCHER_TOTAL_COLLECT", _value);     




    } // LoadVoucherResources

    #endregion


  }

  public class VoucherGamingHallCashDeskClose : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherGamingHallCashDeskClose(PrintMode Mode, SqlTransaction SQLTransaction,
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherGamingHallCashDeskClose");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load voucher resources and data
      LoadVoucherResources(CashierSessionStats);

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String _value = "";
      TYPE_SPLITS _split_data;
      //VoucherCurrencyCashSummary.ParametersCurrencyCashSummary _parameters_currency_cash_summary;
      Boolean _show_refill_cashier;
      Boolean _show_refill_cage;
      Boolean _show_collect_cashier;
      Boolean _show_collect_cage;
      CashierSessionInfo _session_info;
      CurrencyIsoType _iso_type;

      _iso_type = new CurrencyIsoType();
      _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
      _iso_type.Type = CurrencyExchangeType.CURRENCY;

      if (CashierSessionStats.collected.ContainsKey(_iso_type))
      {
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", CashierSessionStats.collected[_iso_type]);
      }
      else
      {
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", 0);
      }

      if (!Split.ReadSplitParameters(out _split_data))
      {
        return;
      }

      // CASH SUMMARY
      /*_parameters_currency_cash_summary = new VoucherCurrencyCashSummary.ParametersCurrencyCashSummary();
      _parameters_currency_cash_summary.HideChipsAndCurrenciesConcepts = ParametersOverview.HideChipsAndCurrenciesConcepts;
      _parameters_currency_cash_summary.IsFilterMovements = ParametersOverview.IsFilterMovements;
      _parameters_currency_cash_summary.CurrencyIsoCode = ParametersOverview.CurrencyIsoCode;
      _value = new VoucherCurrencyCashSummary(CashierSessionStats, _parameters_currency_cash_summary).VoucherHTML;
      this.VoucherHTML = this.VoucherHTML.Replace("@STR_VOUCHER_CASH_DESK_CASH_SUMMARY", _value);*/

      // NLS Values

      // - Title
      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TITLE", _value);

      // - General
      _value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", _value);

      _value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", _value);

      _value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", _value);

      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_DEPOSIT");
      AddString("STR_VOUCHER_DEPOSITS", _value);
      AddCurrency("VOUCHER_DEPOSITS", CashierSessionStats.deposits);

      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_WITHDRAW");
      AddString("STR_VOUCHER_WITHDRAW", _value);
      AddCurrency("VOUCHER_WITHDRAW", CashierSessionStats.withdraws);

      _value = Resource.String("STR_UC_BANK_HANDPAYS");
      AddString("STR_VOUCHER_HANDPAYS_TITLE", _value);
      AddCurrency("VOUCHER_TOTAL_HANDPAYS", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

      _show_refill_cashier = GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cashier != 0;
      _show_refill_cage = !GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cage != 0;
      _show_collect_cashier = GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cashier != 0;
      _show_collect_cage = !GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cage != 0;

      AddCurrency("VOUCHER_TOTAL_REFILL_TO_CASHIER", CashierSessionStats.refill_amount_to_cashier);
      AddString("STR_VOUCHER_TOTAL_REFILL_TO_CASHIER_HIDDEN", _show_refill_cashier ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_REFILL_TO_CAGE", CashierSessionStats.refill_amount_to_cage);
      AddString("STR_VOUCHER_TOTAL_REFILL_TO_CAGE_HIDDEN", _show_refill_cage ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CASHIER", CashierSessionStats.collect_amount_to_cashier);
      AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CASHIER_HIDDEN", _show_collect_cashier ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CAGE", CashierSessionStats.collect_amount_to_cage);
      AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CAGE_HIDDEN", _show_collect_cage ? "" : "hidden");

      _value = Resource.String("STR_VOUCHER_CONCEPTS_IN");
      AddString("STR_VOUCHER_CONCEPTS_IN", _value);
      AddCurrency("VOUCHER_CONCEPTS_IN", CashierSessionStats.concepts_input);

      _value = Resource.String("STR_VOUCHER_CONCEPTS_OUT");
      AddString("STR_VOUCHER_CONCEPTS_OUT", _value);
      AddCurrency("VOUCHER_CONCEPTS_OUT", CashierSessionStats.concepts_output);

      _value = Resource.String("STR_FRM_CASH_DESK_NET_BALANCE");
      AddString("STR_VOUCHER_NATIONAL_BALANCE", _value);

      AddCurrency("VOUCHER_GAMING_HALL_BALANCE", CashierSessionStats.final_balance);
      _value = Resource.String("STR_VOUCHER_TOTAL_REFILL");
      AddString("STR_VOUCHER_TOTAL_REFILL", _value);
      _value = Resource.String("STR_VOUCHER_TOTAL_COLLECT");
      AddString("STR_VOUCHER_TOTAL_COLLECT", _value);

      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED", _value);

      _session_info = CommonCashierInformation.CashierSessionInfo();

      if (_session_info.UserType == GU_USER_TYPE.USER && !Cage.IsGamingTable(_session_info.CashierSessionId))
      {
        Decimal _collected_diff;

        if (CashierSessionStats.collected_diff.ContainsKey(_iso_type))
        {
          _collected_diff = CashierSessionStats.collected_diff[_iso_type];
          if (_collected_diff < 0)
          {
            _collected_diff = -_collected_diff;
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");

            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISSING") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", _value);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", (Currency)_collected_diff);
          }
          else
          {
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", _value);
            AddMultipleLineString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", ((Currency)_collected_diff).ToString());

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
          }
        }
        else
        {
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISSING") + ":";
          AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", _value);
          AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", CashierSessionStats.final_balance);

          AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
          AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
        }
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");

        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
    }


    } // LoadVoucherResources

    #endregion

    #region Public Methods

    public void SetupCashStatusVoucher(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      Currency signed_amount;
      String _str_value;
      Currency _cs_mb_diff;
      Boolean _is_tito_mode;
      Boolean _gp_pending_cash_partial;

      _is_tito_mode = Utils.IsTitoMode();
      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      //  1. Depositos
      //  2. Retiros
      //  3. Entradas
      //  4. Salidas
      //     ---------------------------
      //  5. Saldo (Efectivo en Caja)
      // 
      //     Detalle Salidas
      //  6. Premios
      //  7. Impuestos
      //  8. Premios pagados
      // 
      //     ---------------------------

      //AddString("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_NUM", CashierSessionStats.total_initial_movs.ToString());
      //AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_AMT", CashierSessionStats.initial_balance);

      //  1. Depositos
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", CashierSessionStats.deposits);

      //  2. Retiros
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", CashierSessionStats.withdraws);

      //  3. Cash In
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.total_cash_in);

      //  4. Cash Out
      signed_amount = CashierSessionStats.total_cash_out; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", signed_amount);

      //  Diferencia Bancos M�viles
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF_MISSING") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _str_value);
      _cs_mb_diff = CashierSessionStats.mb_total_balance_lost + CashierSessionStats.mb_pending;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _cs_mb_diff);

      //  Sobrante Bancos M�viles
      if (_gp_pending_cash_partial)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL_CASH_DESK_STATUS") + ":";
  }
      else
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_OVER") + ":";
}

      AddString("STR_VOUCHER_CASH_OVER", _str_value);
      AddCurrency("VOUCHER_CASH_OVER", CashierSessionStats.total_cash_over);

      //  Chech Payments
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", _str_value);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", CashierSessionStats.payment_orders);

      //Credit Card Exchange
      if (CashierSessionStats.total_bank_card > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.total_bank_card);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
      }

      }

    #endregion

  }

  public class VoucherMultiCurrencyExchange : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructores

    public VoucherMultiCurrencyExchange(CurrencyExchangeVoucher CurrencyExchange, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Set specific params

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardAddCurrencyExchange");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Set params
      LoadMultiCurrencyResources(CurrencyExchange);
    }
    #endregion

    #region Private Methods
    private void LoadMultiCurrencyResources(CurrencyExchangeVoucher CurrencyExchangeVoucher)
    {
      String _str_aux;

      String paramName;
      String value;
     
      paramName = "STR_VOUCHER_TITLE";
     
      switch (CurrencyExchangeVoucher.CashierMovement)
      {
        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY:
          value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE"); ;
          break;
        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY:
          value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DEVOLUTION");
          break;
        default:
          value = "";
          Log.Warning("Voucher.LoadMultiCurrencyResources: Not movement definet");
          break;
      }
      AddString(paramName, value);

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", CurrencyExchangeVoucher.AccountInfo);

      _str_aux = Resource.String("STR_VOUCHER_APPLIED_EXCHANGE");

      SetParameterValue("@VOUCHER_CHANGE_APPLIED",
         "<tr>" +
           "<td align=\"left\" >" + _str_aux + " " + CurrencyExchangeVoucher.InIsoCode + ":</td>" +
           "<td align=\"right\" >" + CurrencyExchange.ChangeRateFormat(CurrencyExchangeVoucher.ChangeRate) + " " + CurrencyExchangeVoucher.OutIsoCode +
           "</td>" +
         "</tr>"
         );


      value = Resource.String("STR_VOUCHER_IN_CHARGED") + ":";
      AddString("STR_VOUCHER_IN_AMOUNT", value);



      value = Resource.String("STR_VOUCHER_COMISSION") + ":";
      AddString("STR_VOUCHER_COMISSION", value);

      value = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS");
      AddString("STR_VOUCHER_EQUIVAL_CHANGE", value);



        AddString("STR_VOUCHER_NR2", "");
        AddString("VOUCHER_NR2_AMOUNT", "");

      value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DELIVERED") + ":";
      AddString("CURRENCY_STR_VOUCHER_TOTAL_AMOUNT", value);
      AddString("CURRENCY_VOUCHER_TOTAL_AMOUNT", Currency.Format(CurrencyExchangeVoucher.TotalAmount, CurrencyExchangeVoucher.OutIsoCode));

      if (CurrencyExchangeVoucher.NetAmountDevolution > 0)
      {
      AddString("STR_VOUCHER_TOTAL_AMOUNT", value);
        AddString("VOUCHER_TOTAL_AMOUNT", Currency.Format(CurrencyExchangeVoucher.NetAmountDevolution, CurrencyExchangeVoucher.InIsoCode));
      }
      else
      {
        AddString("VOUCHER_TOTAL_AMOUNT", "");
        AddString("STR_VOUCHER_TOTAL_AMOUNT", "");
      }


      AddString("VOUCHER_IN_AMOUNT", Currency.Format(CurrencyExchangeVoucher.Amount, CurrencyExchangeVoucher.InIsoCode));
      AddString("VOUCHER_EQUIVAL_CHANGE_AMOUNT", Currency.Format(CurrencyExchangeVoucher.EquivalChange, CurrencyExchangeVoucher.OutIsoCode));
      AddString("VOUCHER_CARD_PRICE_AMOUNT", "");
      AddString("VOUCHER_COMISSION_AMOUNT", Currency.Format(CurrencyExchangeVoucher.Comission, CurrencyExchangeVoucher.OutIsoCode));

      AddString("STR_VOUCHER_CARD_PRICE", "");
    }
    #endregion

  }

  /// LTC 14-ABR-2016
  /// <summary>
  /// Voucher LayOut for Tax Custody
  /// </summary>
  public class VoucherTaxCustody : Voucher
  {
      #region Class Attributes

      #endregion

      #region Constructor

      public VoucherTaxCustody(String[] VoucherAccountInfo
                                  , TYPE_SPLITS Splits
                                  , Currency TaxCustody
                                  , PrintMode Mode
                                  , SqlTransaction Trx
        )
          : base(Mode, Trx)
      {
          // 1. Load HTML structure.
          LoadVoucher("VoucherTaxCustody");

          //  4. Load NLS strings from resource
          LoadVoucherResources(VoucherAccountInfo, Splits, TaxCustody);
      }

      #endregion

      #region Private Methods

      /// <summary>
      /// Load voucher resources: Images, NLS.
      /// </summary>
      private void LoadVoucherResources(String[] VoucherAccountInfo
                                      , TYPE_SPLITS Splits
                                      , Currency TaxCustody)
      {
          Boolean _hide_total;
          Boolean _add_currency_in_letters;

          SetValue("CV_TYPE", (Int32)CashierVoucherType.TaxCustody);
          VoucherSequenceId = SequenceId.VoucherTaxCustody;

          //  2. Load Header and Footer
          LoadHeader(Splits.company_a.header);
          LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

          // 3. Load general voucher data.
          LoadVoucherGeneralData();

          AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

          AddString("VOUCHER_SPLIT_NAME", Misc.TaxCustodyName());
          AddString("VOUCHER_CONCEPT", Misc.TaxCustodyConcept());

          AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", TaxCustody);

          _hide_total = Splits.company_a.hide_total;
          _add_currency_in_letters = Splits.company_a.total_in_letters;

          if (_hide_total)
          {
              SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT", "");
          }
          else
          {
              SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT",
                  "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
                    "<tr>" +
                      "<td align=\"center\">TOTAL</td>" +
                    "</tr>" +
                    "<tr>" +
                      "<td align=\"center\"><B>@VOUCHER_CARD_ADD_TOTAL_AMOUNT</B></td>" +
                    "</tr>" +
                  "</table>" +
                  "<hr noshade size=\"4\">");

              AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", TaxCustody, _add_currency_in_letters);
          }
      }

      #endregion

  }
      
}
  

