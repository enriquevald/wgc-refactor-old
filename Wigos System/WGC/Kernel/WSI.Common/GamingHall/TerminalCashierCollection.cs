﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalCashierCollection.cs
// 
//   DESCRIPTION: Class to make a collect and/or a refill currencies in a terminal
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 26-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-OCT-2015 YNM    First release.
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 22-MAR-2016 ETP    Fixed Bug 10798: Multiple collect error when meters not readed.
// 08-APR-2016 ETP    Fixed Bug 11600: Needs to show / Save all Meters not only 3.
// 11-APR-2016 ETP    Fixed Bug 11600: Only for PCD terminals
// 29-SEP-2016 FJC    Fixed Bug (Reopened) 17669:Anular retiro de caja desde bóveda/Cajero: no deshace el stock y conceptos de bóveda
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.TITO;
using WSI.Common.CollectionImport;

namespace WSI.Common.GamingHall
{
  /// <summary>
  /// Business class: TerminalCashierCollection
  /// </summary>
  public class TerminalCashierCollection
  {
    #region Attributes
    private const int MAX_TIME_SECONDS_TO_VALIDATE_CHANGE_STACKER = 10;

    /// <summary>
    /// Cashier session of this cashier
    /// </summary>
    private CashierSessionInfo m_cashier_session;

    private Boolean m_is_collect_to_cashier;
    private Boolean m_is_refill_to_cashier;
    # endregion

    #region Internal Class
    public class SessionOperation
    {
      #region Attributes
      private CashierSessionInfo m_terminal_session_collect;
      private CashierSessionInfo m_terminal_session_refill;

      private Int64 m_operation_id_cashier;
      private Int64 m_operation_id_terminal_collect;
      private Int64 m_operation_id_terminal_refill;

      #endregion

      #region Constructor
      public SessionOperation()
      {
      }
      #endregion

      #region Properties
      public CashierSessionInfo TerminalCashierSessionCollect
      {
        get
        {
          return m_terminal_session_collect;
        }
      }

      public CashierSessionInfo TerminalCashierSessionRefill
      {
        get
        {
          return m_terminal_session_refill;
        }
      }

      public Int64 OperationIdCashier
      {
        get
        {
          return m_operation_id_cashier;
        }
      }

      public Int64 OperationIdTerminalCollect
      {
        get
        {
          return m_operation_id_terminal_collect;
        }

      }

      public Int64 OperationIdTerminalRefill
      {
        get
        {
          return m_operation_id_terminal_refill;
        }
      }
      #endregion

      #region Public Methods
      public Boolean BuildCollect(CashierSessionInfo CashierSession, TerminalCollectRefill TerminalCollectRefill, Boolean IsCollectToCashier, SqlTransaction SqlTrx)
      {
        // Cashier
        if (!Operations.DB_InsertOperation((IsCollectToCashier ? OperationCode.CASHIER_COLLECT_REFILL : OperationCode.CASHIER_COLLECT_REFILL_CAGE), 0,
                                      CashierSession.CashierSessionId, 0, 0, 0, 0, 0, 0, string.Empty,
                                      out m_operation_id_cashier, SqlTrx))
        {
          Log.Message(string.Format("Error on DB_InsertOperation: Error getting an OperationId for the CashierSessionId {0}", CashierSession.CashierSessionId));
          return false;
        }

        // Terminal Collect        
        if (!GetCashierSessionFromTerminal(CashierSession, TerminalCollectRefill.TerminalId, CASHIER_SESSION_STATUS.PENDING_CLOSING, out m_terminal_session_collect, SqlTrx))
        {
          Log.Message(string.Format("There is not a cashier's session in Pending_Closing status for the TerminalId {0}", TerminalCollectRefill.TerminalId));
          return false;
        }
        if (!Operations.DB_InsertOperation(OperationCode.TERMINAL_COLLECT_DROPBOX, 0,
                                m_terminal_session_collect.CashierSessionId, 0, 0,
                                TerminalCollectRefill.DenominationsCollect.TotalCurrency, 0, 0, 0, string.Empty,
                                out m_operation_id_terminal_collect, SqlTrx))
        {
          Log.Message(string.Format("Error on DB_InsertOperation: Error getting an OperationId for the TerminalId {0}", TerminalCollectRefill.TerminalId));
          return false;
        }

        // Terminal Refill
        if (!GetCashierSessionFromTerminal(CashierSession, TerminalCollectRefill.TerminalId, CASHIER_SESSION_STATUS.OPEN, out m_terminal_session_refill, SqlTrx))
        {
          Log.Message(string.Format("There is not a cashier's session in Open status for the TerminalId {0}", TerminalCollectRefill.TerminalId));
          return false;
        }
        if (!Operations.DB_InsertOperation(OperationCode.TERMINAL_REFILL_HOPPER, 0,
                                    m_terminal_session_refill.CashierSessionId, 0, 0,
                                    TerminalCollectRefill.DenominationsRefill.TotalCurrency, 0, 0, 0, string.Empty,
                                    out m_operation_id_terminal_refill, SqlTrx))
        {
          Log.Message(string.Format("Error on DB_InsertOperation: Error getting an OperationId for the TerminalId {0}", TerminalCollectRefill.TerminalId));
          return false;
        }

        return true;
      }

      public Boolean BuildRefill(CashierSessionInfo CashierSession, TerminalCollectRefill TerminalCollectRefill, Boolean IsRefillToCashier, SqlTransaction SqlTrx)
      {
        // Cashier
        if (!Operations.DB_InsertOperation((IsRefillToCashier ? OperationCode.CASHIER_REFILL_HOPPER : OperationCode.CASHIER_REFILL_HOPPER_CAGE), 0,
                                      CashierSession.CashierSessionId, 0, 0,
                                      TerminalCollectRefill.DenominationsRefill.TotalCurrency, 0, 0, 0, string.Empty,
                                      out m_operation_id_cashier, SqlTrx))
        {
          Log.Message(string.Format("Error on DB_InsertOperation: Error getting an OperationId for the CashierSessionId {0}", CashierSession.CashierSessionId));
          return false;
        }

        // Terminal Collect
        m_terminal_session_collect = null;
        m_operation_id_terminal_collect = 0;

        // Terminal Refill
        if (!GetCashierSessionFromTerminal(CashierSession, TerminalCollectRefill.TerminalId, CASHIER_SESSION_STATUS.OPEN, out m_terminal_session_refill, SqlTrx))
        {
          Log.Message(string.Format("There is not a cashier's session in Open status for the TerminalId {0}", TerminalCollectRefill.TerminalId));
          return false;
        }
        if (!Operations.DB_InsertOperation(OperationCode.TERMINAL_REFILL_HOPPER, 0,
                                    m_terminal_session_refill.CashierSessionId, 0, 0,
                                    TerminalCollectRefill.DenominationsRefill.TotalCurrency, 0, 0, 0, string.Empty,
                                    out m_operation_id_terminal_refill, SqlTrx))
        {
          Log.Message(string.Format("Error on DB_InsertOperation: Error getting an OperationId for the TerminalId {0}", TerminalCollectRefill.TerminalId));
          return false;
        }

        return true;
      }

      #endregion

      #region Private Methods
      private Boolean GetCashierSessionFromTerminal(CashierSessionInfo CashierSession, Int32 TerminalId, CASHIER_SESSION_STATUS Status, out CashierSessionInfo CashierSessionInTerminal, SqlTransaction SqlTrx)
      {
        CashierSessionInTerminal = null;

        Int32 _user_id;
        String _user_name;

        try
        {
          // All terminals uses SYS_TITO
          if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_TITO, SqlTrx, out _user_id, out _user_name))
          {
            Log.Message("Error on GetSystemUser: Error getting the user (SYS_TITO) for the Terminal");
            return false;
          }


          if (Cashier.GetCashierSessionFromTerminal(TerminalId, _user_id, Status, out CashierSessionInTerminal, SqlTrx)
              && CashierSessionInTerminal.CashierSessionId > 0)
          {

            CashierSessionInTerminal.AuthorizedByUserId = CashierSession.AuthorizedByUserId;
            CashierSessionInTerminal.AuthorizedByUserName = CashierSession.AuthorizedByUserName;
            return true;
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        return false;
      }

      #endregion

    }

    #endregion

    # region Constructors
    public TerminalCashierCollection(CashierSessionInfo CashierSessionInfo)
    {
      if (CashierSessionInfo == null || CashierSessionInfo.CashierSessionId < 1)
        throw new Exception("The CashierSessionInfo object is invalid");

      m_cashier_session = CashierSessionInfo.Copy();

      m_is_collect_to_cashier = GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled", false);
      m_is_refill_to_cashier = GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled", false);
    }

    # endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: It sends command to change stacker for start the collect for a terminal list
    //
    //  PARAMS:
    //      - INPUT:       
    //        - Terminals
    //      - OUTPUT:
    //        - TerminalsWithError
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean TerminalsChangeStacker(ref List<TerminalCollectRefill> Terminals, out List<TerminalCollectRefill> TerminalsWithError)
    {
      Int64 _operation_id;
      TerminalsWithError = new List<TerminalCollectRefill>();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Operations.DB_InsertOperation(OperationCode.TERMINAL_CHANGE_STACKER_REQUEST, 0, m_cashier_session.CashierSessionId, 0, 0, 0, 0, 0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction))
          {
            Log.Message("Error on TerminalsChangeStacker: Could not create a valid operation id");
            return false;
          }

          foreach (TerminalCollectRefill _terminal in Terminals)
          {
            ChangeStackerGamingHall(_terminal.TerminalId, _operation_id, _db_trx.SqlTransaction);
          }

          _db_trx.SqlTransaction.Commit();
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          ValidateChangeStacker(ref Terminals, ref TerminalsWithError, _operation_id, _db_trx.SqlTransaction);
          _db_trx.SqlTransaction.Commit();
          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generate movements for a terminal list to collect
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalsToCollectoAndRefill: Terminal List to collect and refill
    //      - OUTPUT:
    //        - TerminalsWithError
    //        - Voucher
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean TerminalsCollectAndRefill(ref List<TerminalCollectRefill> TerminalsToCollectoAndRefill,
                                             out List<TerminalCollectRefill> TerminalsWithError, out VoucherTerminalsCollect Voucher)
    {
      List<TerminalCollectRefill> _terminals_collected;
      MoneyCollectionInfo _money_collection_pending;
      Boolean _change_offline;
      Boolean _continue;

      Voucher = null;
      TerminalsWithError = new List<TerminalCollectRefill>();
      _terminals_collected = new List<TerminalCollectRefill>();

      try
      {
        foreach (TerminalCollectRefill _terminal in TerminalsToCollectoAndRefill)
        {
          _change_offline = false;
          _continue = true;

          using (DB_TRX _db_trx = new DB_TRX())
          {
            SessionOperation _session_operation = new SessionOperation();
            TITO_MONEY_COLLECTION_STATUS _current_status;
            _terminal.SetCollectOK = false;

            // Gets the current MoneyCollection for the terminal
            MoneyCollection.GetMoneyCollection(_terminal.MoneyCollectionId.Value, out _money_collection_pending, _db_trx.SqlTransaction);
            _current_status = _money_collection_pending.Status.Value;

            if (_current_status == TITO_MONEY_COLLECTION_STATUS.OPEN || _current_status == TITO_MONEY_COLLECTION_STATUS.PENDING)
            {
              // If the terminal is in 'pending' status is because the stacker was changed previously
              _terminal.SetChangeStackerOK = (_current_status == TITO_MONEY_COLLECTION_STATUS.PENDING);

              if (!_terminal.IsStackerChangedOK)
              {
                // Change of stacker in offline mode
                ChangeStackerGamingHallOffline(_terminal.TerminalId, _db_trx.SqlTransaction);

                // If the change stacker (offline) task was completed OK then it has changed the status to Pending
                MoneyCollection.GetMoneyCollection(_terminal.MoneyCollectionId.Value, out _money_collection_pending, _db_trx.SqlTransaction);

                _terminal.SetChangeStackerOK = (_money_collection_pending.Status.Value == TITO_MONEY_COLLECTION_STATUS.PENDING);

                // Insert a information movement in the cashier
                InsertCashierMovements(CASHIER_MOVEMENT.CASHIER_CHANGE_STACKER_TERMINAL, _session_operation.OperationIdCashier, m_cashier_session, _db_trx.SqlTransaction);

                _change_offline = true;
              }

              if (_terminal.IsStackerChangedOK)
              {
                
                if (!_change_offline)
                {
                  // Create new CashierSesssion and TerminalMoneyCollection and MoneyCollectionMeter for the Refill (In opened status)
                  if (!TITO_ChangeStacker.InitializeStacker(_terminal.TerminalId, null, 0, _db_trx.SqlTransaction))
                  {
                    _continue = false;
                    Log.Error(string.Format("The call to InitializeStacker for the Terminal [{0}] has returned an error", _terminal.TerminalId));
                  }
                }

                if (_continue)
                {
                  // Create CashierSessions and OperationCodes for the Cashier and Terminal
                  _continue = _session_operation.BuildCollect(m_cashier_session, _terminal, m_is_collect_to_cashier, _db_trx.SqlTransaction);
                }

                // Close the CollectionMeter for the collected session. A online meter reading doesn't close the MoneyCollectionMeter.
                if (_continue && !_change_offline
                    && !MoneyCollectionMeter.CloseMoneyCollectionMeter(_session_operation.TerminalCashierSessionCollect.CashierSessionId, _db_trx.SqlTransaction))
                {
                  _continue = false;
                  Log.Error(String.Format("The call to CloseMoneyCollectionMeter (SessionId={0}) has returned error", _session_operation.TerminalCashierSessionCollect.CashierSessionId));
                }
                
                if (_continue)
                {
                  // Collect and Refill Terminal
                  _terminal.SetCollectOK = CollectAndRefillTerminal(_terminal, _session_operation, _money_collection_pending, _db_trx.SqlTransaction);
                }

              }
            }
            else
            { 
              Log.Error (string.Format("The MoneyCollection for the Terminal [{0}] is in a invalid status [status: {1}]" , _terminal.TerminalId, _current_status.ToString()));
            }

            if (_terminal.IsCollectedOK)
            {
              _terminals_collected.Add(_terminal);
              _db_trx.SqlTransaction.Commit();
            }
            else
            {
              TerminalsWithError.Add(_terminal);
              _db_trx.SqlTransaction.Rollback();
            }
          }
        }

        if (TerminalsWithError.Count == TerminalsToCollectoAndRefill.Count)
        {
          // Rollback because any terminal was collected
          Log.Error("TerminalsCollectAndRefill failed beacuse all terminals failed in the operation to collect");
          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!GenerateCollectedVoucher(_terminals_collected, out Voucher, _db_trx.SqlTransaction))
          {
            Log.Message("Error on GenerateCollectedVoucher: It failed to generate the voucher");
            return false;
          }

          _db_trx.SqlTransaction.Commit();
          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generate movements for a terminal list to refill
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalsToRefill: Terminal List to refill
    //      - OUTPUT:
    //        - TerminalsWithError
    //        - Voucher
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean TerminalsRefillHopper(ref List<TerminalCollectRefill> TerminalsToRefill,
                                         out List<TerminalCollectRefill> TerminalsWithError, out VoucherTerminalsRefill Voucher)
    {
      List<TerminalCollectRefill> _terminals_refilled;

      Voucher = null;
      TerminalsWithError = new List<TerminalCollectRefill>();

      try
      {
        _terminals_refilled = new List<TerminalCollectRefill>();

        foreach (TerminalCollectRefill _terminal in TerminalsToRefill)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Build cashier session and operations ids for the Cashier and Terminal
            SessionOperation _session_operation = new SessionOperation();
            _session_operation.BuildRefill(m_cashier_session, _terminal, m_is_refill_to_cashier, _db_trx.SqlTransaction);

            _terminal.SetRefillOK = RefillTerminal(_terminal, _session_operation, _db_trx.SqlTransaction);

            if (_terminal.IsRefilledOK)
            {
              _terminals_refilled.Add(_terminal);
              _db_trx.SqlTransaction.Commit();
            }
            else
            {
              TerminalsWithError.Add(_terminal);
              _db_trx.SqlTransaction.Rollback();
            }
          }
        }

        if (TerminalsWithError.Count == TerminalsToRefill.Count)
        {
          // Rollback because any terminal was refilled
          Log.Error("TerminalsRefillHopper failed beacuse all terminals failed in the operation to refill");
          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!GenerateRefilledVoucher(_terminals_refilled, out Voucher, _db_trx.SqlTransaction))
          {
            Log.Message("Error on GenerateRefilledVoucher: It failed to generate the voucher");
            return false;
          }

          _db_trx.SqlTransaction.Commit();
          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Cancel a Collect 
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalCashierSessionId
    //        - TerminalId
    //        - MoneyCollectionId
    //        - DenominationsCollected
    //      - OUTPUT:
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean TerminalCancelCollect(Int64 TerminalCashierSessionId, Int32 TerminalId, Int64 MoneyCollectionId, TerminalDenominations DenominationsCollected)
    {
      try
      {
        CageStock _cage_stock;
        CageMovement _cage_movement;
        Int64 _cage_movement_id;
        ArrayList _voucher_list;
        MoneyCollectionInfo _money_collection_info_opened;
        UndoCollect _undo_collect;
        SessionOperationCollectInfo _session_operation_to_undo;

        // The Cancellation is for a movement from Terminal to Cage
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _cage_stock = new CageStock();
          _cage_movement = new CageMovement();

          _undo_collect = new UndoCollect();
          if (!_undo_collect.GetSessionAndOperationsCollected(MoneyCollectionId, out _session_operation_to_undo, _db_trx.SqlTransaction))
          {
            Log.Error(string.Format("There was an error getting the CashierSessions and AccountOperations used in the Collected. MoneyCollectionId {0}", MoneyCollectionId));
            return false;
          }

          // gets the Money Collection opened in the terminal
          if (!MoneyCollection.GetMoneyCollection(TerminalId, _session_operation_to_undo.CashierSessionTerminalRefill.CashierSessionId, TITO_MONEY_COLLECTION_STATUS.OPEN, out _money_collection_info_opened, _db_trx.SqlTransaction))
          {
            Log.Error(string.Format("The CashierSessionId {0} of the Terminal {1} is not open. Is impossible to cancel the collected", TerminalId));
            return false;
          }

          // Checks if there is not transaction in the Terminal (input or output movements of money)
          if (_money_collection_info_opened.OutBills > 0 || _money_collection_info_opened.OutCents > 0 || _money_collection_info_opened.OutCoins > 0
              || _money_collection_info_opened.ExpectedBillAmount > 0 || _money_collection_info_opened.ExpectedBillCount > 0
              || _money_collection_info_opened.ExpectedCoinAmount > 0 || _money_collection_info_opened.ExpectedCoinCount > 0)
          {
            Log.Error(string.Format("The cancel operation of the collect selected is impossible because exists operations in the Terminal {0}, MoneyCollectionId {1}", TerminalId, MoneyCollectionId));
            return false;
          }


          // It checks and update the stock in Cage
          if (!_cage_stock.CheckAndUpdateCageStock(DenominationsCollected, CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL, _db_trx.SqlTransaction))
          {
            Log.Error(string.Format("There is not enought stock in the Cage to Cancel the Collect in the Terminal {0}", TerminalId));
            return false;
          }

          // Change the status of cage_movement to 0 (Sent). 
          if (!Cage.GetCageMovement(MoneyCollectionId, out _cage_movement_id))
          {
            Log.Error(string.Format("There is not a valid cage_movement for the TerminalId {0} and MoneyCollection {1}", TerminalId, MoneyCollectionId));
            return false;
          }
          if (!_cage_movement.UpdateMovement(_cage_movement_id, TerminalCashierSessionId, true, null, null, Cage.CageStatus.Sent, _db_trx.SqlTransaction))
          {
            Log.Error(string.Format("Error on UpdateMovement for the cage_movement to change the status to 0. TerminalId {0} and CageMovementId {1}", TerminalId, _cage_movement_id));
            return false;
          }

          // Delete movements in Cage_movement_details
          if (!_cage_movement.DeleteMovementDetails(_cage_movement_id, _db_trx.SqlTransaction))
          {
            Log.Error(string.Format("Error on DeleteMovementDetails deleting cage movement details. TerminalId {0} and CageMovementId {1}", TerminalId, _cage_movement_id));
            return false;
          }

          // Cancel the money collection and delete the records in MONEY_COLLECTION_DETAILS
          if (MoneyCollection.CancelMoneyCollectionFromTerminal(TerminalCashierSessionId, MoneyCollectionId, _cage_movement_id, _db_trx.SqlTransaction))
          {
            Log.Error(string.Format("Error on DeleteMovementDetails deleting cage movement details. TerminalId {0} and CageMovementId {1}", TerminalId, _cage_movement_id));
            return false;
          }

          // Undo movements in the terminal (Dropbox collected)
          if (!UndoOperationAndMovements(OperationCode.TERMINAL_COLLECT_DROPBOX,_session_operation_to_undo.CashierSessionTerminalCollect, 
                                         _session_operation_to_undo.OperationIdTerminalCollect, out _voucher_list, _db_trx.SqlTransaction))
          {
            Log.Error(string.Format("The UndoOperation for OperationCode.TERMINAL_COLLECT_DROPBOX has been failed. TerminalId {0}", TerminalId));
            return false;
          }

          // Undo movements in the terminal (Hopper refilled)
          if (!UndoOperationAndMovements(OperationCode.TERMINAL_REFILL_HOPPER, _session_operation_to_undo.CashierSessionTerminalRefill, 
                                         _session_operation_to_undo.OperationIdTerminalRefill, out _voucher_list, _db_trx.SqlTransaction))
          {
            Log.Error(string.Format("The UndoOperation for OperationCode.TERMINAL_REFILL_HOPPER has been failed. TerminalId {0}", TerminalId));
            return false;
          }

          // Undo movements in the Cashier
          if (m_is_collect_to_cashier)
          {
            if (!UndoOperationAndMovements(OperationCode.CASHIER_COLLECT_REFILL, _session_operation_to_undo.CashierSessionCashier, 
                                           _session_operation_to_undo.OperationIdCashier, out _voucher_list, _db_trx.SqlTransaction))
            {
              Log.Error(string.Format("The UndoOperation for OperationCode.CASHIER_COLLECT_REFILL has been failed. TerminalId {0}", TerminalId));
              return false;
            }
          }
          else
          {
            if (!UndoOperationAndMovements(OperationCode.CASHIER_COLLECT_REFILL_CAGE, _session_operation_to_undo.CashierSessionCashier,
                                           _session_operation_to_undo.OperationIdCashier, out _voucher_list, _db_trx.SqlTransaction))
            {
              Log.Error(string.Format("The UndoOperation for OperationCode.CASHIER_COLLECT_REFILL_CAGE has been failed. TerminalId {0}", TerminalId));
              return false;
            }
          }

          // TODO: Remover
          return false;

        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    #endregion

    #region Private Methods

    private Boolean CollectAndRefillTerminal(TerminalCollectRefill TerminalToCollectoAndRefill, SessionOperation SessionsAndOperations, MoneyCollectionInfo MoneyCollectionPending, SqlTransaction SqlTrx)
    {

      MoneyCollectionInfo _money_collection_opened;
      MoneyCollectionInfo _money_collection_closed;

      Int32 _terminal_id;

      // Validations
      if (TerminalToCollectoAndRefill == null)
      {
        Log.Message("Error on CollectAndRefillTerminal: The TerminalToCollectoAndRefill parameter is invalid because is NULL");
        return false;
      }
      if (TerminalToCollectoAndRefill.DenominationsCollect.DenominationsWithQuantity.Count == 0)
      {
        Log.Message("Error on CollectAndRefillTerminal: There are not denominations with quantities for to collect");
        return false;
      }
      if (TerminalToCollectoAndRefill.TerminalId < 1)
      {
        Log.Message("Error on CollectAndRefillTerminal: The TerminalId in the TerminalToCollectoAndRefill parameter is invalid");
        return false;
      }

      _terminal_id = TerminalToCollectoAndRefill.TerminalId;

      TerminalToCollectoAndRefill.SetCashierSessionCollect = SessionsAndOperations.TerminalCashierSessionCollect;
      TerminalToCollectoAndRefill.SetCashierSessionRefill = SessionsAndOperations.TerminalCashierSessionRefill;

      // The money collection used in the collect is in status pending in this moment
      TerminalToCollectoAndRefill.SetMoneyCollectionInfoCollect = MoneyCollectionPending;

      // Get the money collection opened. Is is going to use in the refill
      if (!MoneyCollection.GetMoneyCollection(_terminal_id, SessionsAndOperations.TerminalCashierSessionRefill.CashierSessionId, TITO_MONEY_COLLECTION_STATUS.OPEN, out _money_collection_opened, SqlTrx))
      {
        Log.Message(string.Format("There is not a valid MoneyCollection in status opened. Terminal {0}", _terminal_id));
        return false;
      }

      TerminalToCollectoAndRefill.SetMoneyCollectionInfoRefill = _money_collection_opened;

      // Collect
      if (!MovementsToCollectAndRefillTerminal(TerminalToCollectoAndRefill, SessionsAndOperations, out _money_collection_closed, SqlTrx))
      {
        Log.Message(string.Format("Error on CollectAndRefillTerminal: Could not complete the collect in the TerminalId {0}", _terminal_id));
        return false;
      }

      TerminalToCollectoAndRefill.SetMoneyCollectionInfoCollect = _money_collection_closed;

      // Add movement to close the cashier session in the terminal. The OperationID = 0 (It cannot undo)
      if (!AddMovementToCloseCashierSession(_terminal_id, 0, SessionsAndOperations.TerminalCashierSessionCollect, SqlTrx))
      {
        Log.Message(string.Format("Error on AddMovementToCloseCashierSession: Could not complete the task to close the cashier session in the TerminalId {0}", _terminal_id));
        return false;
      }

      // Refill
      if (!MovementsToRefillTerminal(_terminal_id, SessionsAndOperations, TerminalToCollectoAndRefill.DenominationsRefill, TerminalToCollectoAndRefill.MoneyCollectionId, SqlTrx))
      {
        Log.Message(string.Format("Error on CollectAndRefillTerminal: Could not complete the refill in Hopper for the TerminalId {0}", _terminal_id));
        return false;
      }

      // Update the refilledHopper field in the current collect for use in by default in the next collect
      if (!MoneyCollection.UpdateRefilledHopperFieldOnMoneyCollection(_money_collection_opened.CollectionId, TITO_MONEY_COLLECTION_STATUS.OPEN, 
                                                                        TerminalToCollectoAndRefill.DenominationsRefill, SqlTrx))
      {
        Log.Message(string.Format("Error on UpdateRefillInHopperOnMoneyCollection to update the information about the refill for the next collect. Terminal Id {0}", _terminal_id));
        return false;
      }

      return true;
    }

    private Boolean RefillTerminal(TerminalCollectRefill TerminalToRefill, SessionOperation SessionsAndOperations, SqlTransaction SqlTrx)
    {
      Int32 _terminal_id;

      // Validations
      if (TerminalToRefill == null)
      {
        Log.Message("Error on RefillTerminal: The TerminalToRefill parameter is invalid because is NULL");
        return false;
      }
      if (TerminalToRefill.DenominationsRefill.IsEmpty)
      {
        Log.Message("Error on RefillTerminal: There are not denominations with quantities for to refill Hoppers");
        return false;
      }
      if (TerminalToRefill.TerminalId < 1)
      {
        Log.Message("Error on RefillTerminal: The TerminalId in the TerminalToRefill parameter is invalid");
        return false;
      }
      if (!Cashier.IsSessionOpen(SessionsAndOperations.TerminalCashierSessionRefill.CashierSessionId, SqlTrx))
      {
        Log.Message("Error on RefillTerminal: The CashierSession of the Terminal is not open");
        return false;
      }

      _terminal_id = TerminalToRefill.TerminalId;

      TerminalToRefill.SetCashierSessionRefill = SessionsAndOperations.TerminalCashierSessionRefill;

      if (!MovementsToRefillTerminal(_terminal_id, SessionsAndOperations, TerminalToRefill.DenominationsRefill, null, SqlTrx))
      {
        Log.Message(string.Format("Error on RefillTerminal: Could not complete the refill in Hopper for the TerminalId {0}", _terminal_id));
        return false;
      }

      return true;
    }

    private Boolean MovementsToCollectAndRefillTerminal(TerminalCollectRefill TerminalToCollectoAndRefill , SessionOperation SessionsAndOperations, 
                                                        out MoneyCollectionInfo NewMoneyCollection, SqlTransaction SqlTrx)
    {
      Int64 _cage_movement_id = 0;
      List<MoneyCollectionDetailsInfo> _money_collection_details_list;
      List<CashierTerminalMoneyInfo> _cashier_terminal_money_list;
      CageStock _cage_stock;
      Int32 _terminal_id;
      Int64 _money_collection_id_pending;
      Int64 _money_collection_id_open;

      NewMoneyCollection = null;
      _terminal_id = TerminalToCollectoAndRefill.TerminalId;

      // Check status pending to collect
      if (TerminalToCollectoAndRefill.MoneyCollectionInfoCollect != null &&
          TerminalToCollectoAndRefill.MoneyCollectionInfoCollect.Status != TITO_MONEY_COLLECTION_STATUS.PENDING)
      {
        Log.Message(string.Format("Error on MovementsToCollectAndRefillTerminal: The TerminalId {0} is not in Pending status to collect", _terminal_id));
        return false;
      }

      // Check that exists a new open collection opened
      if (TerminalToCollectoAndRefill.MoneyCollectionInfoRefill != null &&
          TerminalToCollectoAndRefill.MoneyCollectionInfoRefill.Status != TITO_MONEY_COLLECTION_STATUS.OPEN)
      {
        Log.Message(string.Format("Error on MovementsToCollectAndRefillTerminal: The TerminalId {0} has not a money collection in open status", _terminal_id));
        return false;
      }

      _money_collection_id_pending = TerminalToCollectoAndRefill.MoneyCollectionInfoCollect.CollectionId;
      _money_collection_id_open = TerminalToCollectoAndRefill.MoneyCollectionInfoRefill.CollectionId;

      // Update Meters on money_collections (Last and New)
      if (!MoneyCollection.UpdateMetersInMoneyCollections (_money_collection_id_pending, _money_collection_id_open,
                                                           TerminalToCollectoAndRefill.CurrentMeters, SqlTrx))
      {
        Log.Message(string.Format("Error on MovementsToCollectAndRefillTerminal: Could update meters on money collection for the TerminalId {0}", _terminal_id));
        return false;
      }

      //Update Money collection to collected
      if (!MoneyCollection.ChangeStatusPendingToClose(_terminal_id, SessionsAndOperations.TerminalCashierSessionCollect.UserId,
                                                      TerminalToCollectoAndRefill.DenominationsCollect, TerminalToCollectoAndRefill.DenominationsRefill,
                                                      TerminalToCollectoAndRefill.MoneyCollectionInfoCollect, SqlTrx))
      {
        Log.Message(string.Format("Error on MovementsToCollectAndRefillTerminal: Could not do a collect for the TerminalId {0}", _terminal_id));
        return false;
      }

      // Gets a list from CashierTerminalMoney with expected denominations and quantities to collect
      if (!CashierTerminalMoney.GetCashierTerminalMoneyList(_money_collection_id_pending, out _cashier_terminal_money_list, SqlTrx))
      {
        Log.Message(string.Format("Error on MovementsToCollectAndRefillTerminal: Could not get a valid CashierTerminalMoney for the TerminalId {0} and MoneyCollectionId {1}",
                                  _terminal_id, _money_collection_id_pending));
        return false;
      }

      // Insert MoneyCollectionDetails
      if (!MoneyCollection.InsertMoneyCollectionDetails(TerminalToCollectoAndRefill.MoneyCollectionInfoCollect, TerminalToCollectoAndRefill.DenominationsCollect, 
                                                        _cashier_terminal_money_list, out _money_collection_details_list, SqlTrx))
      {
        Log.Message(string.Format("Error on MovementsToCollectAndRefillTerminal: Could not add money collection details for the TerminalId {0}", _terminal_id));
        return false;
      }

      // Cage Movements
      if (!m_is_collect_to_cashier)
      {
        if (!UpdateOrInsertCageMovementsCollected(TerminalToCollectoAndRefill, SessionsAndOperations, out _cage_movement_id, SqlTrx))
        {
          return false;
        }

        if (!UpdateOrInsertCageMovementsRefilled(TerminalToCollectoAndRefill, SessionsAndOperations, out _cage_movement_id, SqlTrx))
        {
          return false;
        }
      }

      // Insert movements for the Cashier
      // 1. Summary movement (TERMINAL_COLLECT_REQUEST  or TERMINAL_COLLECT_REQUEST_INFO)
      CASHIER_MOVEMENT _movement;
      if (m_is_collect_to_cashier)
      {
        // Movement from Terminal to Cashier
        _movement = CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL;
      }
      else
      {
        // Movement from Terminal to Cage
        _movement = CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE;
      }
      if (!InsertCashierMovements(_movement, SessionsAndOperations.OperationIdCashier, m_cashier_session, TerminalToCollectoAndRefill.DenominationsCollect, _money_collection_id_pending, SqlTrx))
      {
        Log.Error(string.Format("Error on MovementsToCollectAndRefillTerminal: Could not insert request movements in the CashierSessionId {0}", m_cashier_session.CashierSessionId));
        return false;
      }

      // 2. Insert detail
      if (!InsertCageCurrenciesIntoCashierMovements(TerminalToCollectoAndRefill.DenominationsCollect, CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL, 
                                                    _cage_movement_id, m_cashier_session, SessionsAndOperations.OperationIdCashier, SqlTrx))
      {
        Log.Error(string.Format("Error on MovementsToCollectAndRefillTerminal: Could not insert request movements in the CashierSessionId {0}", m_cashier_session.CashierSessionId));
        return false;
      }

      // Insert movements for the terminal
      if (!InsertCashierMovements(CASHIER_MOVEMENT.MB_CASH_IN, SessionsAndOperations.OperationIdTerminalCollect, SessionsAndOperations.TerminalCashierSessionCollect,
                                  TerminalToCollectoAndRefill.DenominationsCollect, _money_collection_id_pending, SqlTrx))
      {
        Log.Error(string.Format("Error on MovementsToCollectAndRefillTerminal: Could not insert cashiers movements for the Terminal in its CashierSessionId {0}", SessionsAndOperations.TerminalCashierSessionCollect.CashierSessionId));
        return false;
      }

      // Get the Money collection with the updated information
      if (!MoneyCollection.GetMoneyCollection(_money_collection_id_pending, out NewMoneyCollection, SqlTrx))
      {
        Log.Message(string.Format("Error on MovementsToCollectAndRefillTerminal: Could not Get the Money collection with the updated information in the CashierSessionId {0}", SessionsAndOperations.TerminalCashierSessionCollect.CashierSessionId));
        return false;
      }

      // Update cage stock
      if (!m_is_collect_to_cashier)
      {
        _cage_stock = new CageStock();

        if (!_cage_stock.CheckAndUpdateCageStock(TerminalToCollectoAndRefill.DenominationsCollect, CASHIER_MOVEMENT.FILLER_OUT, SqlTrx))
        {
          Log.Error(string.Format("Error on MovementsToCollectAndRefillTerminal: Error updating the stock in the Cage for the Collect in the Terminal {0}", _terminal_id));
          return false;
        }
      }

      return true;
    }

    private Boolean MovementsToRefillTerminal(Int32 TerminalId, SessionOperation SessionsAndOperations, TerminalDenominations DenominationsRefill, Int64? RelatedId, SqlTransaction SqlTrx)
    {
      Int64 _cage_movement_id = 0;
      MoneyCollectionInfo _money_collection_info;
      CageStock _cage_stock;

      if (DenominationsRefill != null && DenominationsRefill.DenominationsWithQuantity.Count == 0)
      {
        // Nothing for to fill in the Hoppers
        return true;
      }

      // Get the opened money collection
      if (!MoneyCollection.GetMoneyCollection(TerminalId, SessionsAndOperations.TerminalCashierSessionRefill.CashierSessionId, TITO_MONEY_COLLECTION_STATUS.OPEN, out _money_collection_info, SqlTrx))
      {
        Log.Message("Error on GetMoneyCollection: Could not Find and open money collection for the terminal");
        return false;
      }

      // Insert movements in the Cage
      if (!m_is_refill_to_cashier)
      {
        if (!InsertCageMovements(SessionsAndOperations.TerminalCashierSessionRefill, DenominationsRefill, Cage.CageOperationType.ToTerminal, null, out _cage_movement_id, SqlTrx))
        {
          Log.Message(string.Format("Could not insert cage movements to a refill. Terminal CashierSessionId {0}", SessionsAndOperations.TerminalCashierSessionRefill.CashierSessionId));
          return false;
        }
      }

      // Insert movements for the Cashier
      // 1. Summary movement (HOPPER_FILLER_IN_REQUEST  or HOPPER_FILLER_IN_REQUEST_INFO)
      CASHIER_MOVEMENT _movement;
      if (m_is_refill_to_cashier)
      {
        // Movement from Cashier to Terminal
        _movement = CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL;

        var _balance = Cashier.UpdateSessionBalance(SqlTrx, m_cashier_session.CashierSessionId, 0, "", null);
        if (_balance < DenominationsRefill.Total)
        {
          Log.Message(string.Format("Error on MovementsToRefillTerminal: There is insufficient cash balance in the CashierSessionId {0}", m_cashier_session.CashierSessionId));
          return false;
        }
      }
      else
      {
        // Movement from Cage to Terminal
        _movement = CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL_CAGE;
      }
      if (!InsertCashierMovements(_movement, SessionsAndOperations.OperationIdCashier, m_cashier_session, DenominationsRefill, RelatedId ?? SessionsAndOperations.OperationIdTerminalRefill, SqlTrx))
      {
        Log.Error(string.Format("Error on InsertCashierMovements: Could not insert request movements in the CashierSessionId {0}", m_cashier_session.CashierSessionId));
        return false;
      }

      // 2. Detail movements
      if (!InsertCageCurrenciesIntoCashierMovements(DenominationsRefill, CASHIER_MOVEMENT.CAGE_CASHIER_REFILL_HOPPER_TERMINAL, _cage_movement_id, m_cashier_session, SessionsAndOperations.OperationIdCashier, SqlTrx))
      {
        Log.Message(string.Format("Error on InsertCageCurrenciesIntoCashierMovements: Could not insert request movements in the CashierSessionId {0}", m_cashier_session.CashierSessionId));
        return false;
      }

      // Insert movements for the terminal
      if (!InsertCashierMovements(CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER, SessionsAndOperations.OperationIdTerminalRefill, SessionsAndOperations.TerminalCashierSessionRefill, DenominationsRefill, RelatedId ?? SessionsAndOperations.OperationIdCashier, SqlTrx))
      {
        Log.Error(string.Format("Error on InsertCashierMovements: Could not insert cashiers movements for the Terminal in its CashierSessionId {0}", SessionsAndOperations.TerminalCashierSessionRefill.CashierSessionId));
        return false;
      }

      // Update cage stock
      if (!m_is_refill_to_cashier && DenominationsRefill.Total > 0)
      {
        _cage_stock = new CageStock();

        if (!_cage_stock.CheckAndUpdateCageStock(DenominationsRefill, CASHIER_MOVEMENT.FILLER_IN, SqlTrx))
        {
          Log.Error(string.Format("There is not enought stock in the Cage to refill the hopper in the Terminal {0}", TerminalId));
          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// Insert movements in the Cate
    /// </summary>
    /// <param name="TerminalCashierSession"></param>
    /// <param name="OperationId"></param>
    /// <param name="TerminalDenominations"></param>
    /// <param name="OperationType"></param>
    /// <param name="MoneyCollectionId"></param>
    /// <param name="CageMovementId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean InsertCageMovements(CashierSessionInfo TerminalCashierSession, TerminalDenominations TerminalDenominations,
                                        Cage.CageOperationType OperationType, long? MoneyCollectionId, out Int64 CageMovementId, SqlTransaction SqlTrx)
    {
      CageMovementId = 0;

      try
      {
        if (Cage.IsCageEnabled())
        {
          if (Cage.IsCageAutoMode())
          {
            CageMovement _cage_movements = new CageMovement();
            return _cage_movements.InsertCageMovements(TerminalCashierSession, TerminalDenominations, OperationType, Cage.CageStatus.OK, Cage.PendingMovementType.ToCashierTerminal, MoneyCollectionId, out CageMovementId, SqlTrx);
          }
          else
          {
            Log.Message("Error on InsertCageMovements: The cage is not in Automode");
            return false;
          }
        }
        else
        {
          Log.Message("Error on InsertCageMovements: The cage is not enabled");
          return false;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    /// <summary>
    /// Update movements in the Cage
    /// </summary>
    /// <param name="CageMovementId"></param>
    /// <param name="TerminalCashierSession"></param>
    /// <param name="MoneyCollectionId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean UpdateCageMovements(Int64 CageMovementId, CashierSessionInfo TerminalCashierSession, Int64 MoneyCollectionId, SqlTransaction SqlTrx)
    {
      try
      {
        if (Cage.IsCageEnabled())
        {
          if (Cage.IsCageAutoMode())
          {
            CageMovement _cage_movements = new CageMovement();

            Cage.CageStatus _status = Cage.CageStatus.OK;

            // TODO: Validar si al actualizar no es necesario actualizar CGM_CAGE_SESSION_ID 
            return _cage_movements.UpdateMovement(CageMovementId, TerminalCashierSession.CashierSessionId, false, null, MoneyCollectionId, _status, SqlTrx);
          }
          else
          {
            throw new ApplicationException("The cage is not in Automode");
          }
        }
        else
        {
          throw new ApplicationException("The cage is not enabled");
        }
      }
      catch (ApplicationException ex)
      {
        Log.Error(ex.Message);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="TerminalDenominations"></param>
    /// <param name="CashierMovement"></param>
    /// <param name="CageMovementId"></param>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="OperationId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean InsertCageCurrenciesIntoCashierMovements(TerminalDenominations TerminalDenominations, CASHIER_MOVEMENT CashierMovement, Int64 CageMovementId,
                                                             CashierSessionInfo CashierSessionInfo, Int64 OperationId, SqlTransaction SqlTrx)
    {
      CashierMovementsTable _cashier_movements;
      Decimal _aux_amount;

      _cashier_movements = new CashierMovementsTable(CashierSessionInfo);

      foreach (TerminalCurrencyItem _item in TerminalDenominations.DenominationsWithQuantity)
      {
        _aux_amount = -1;
        _cashier_movements.Add(OperationId, CashierMovement, _item.Total, 0, "", "", _item.IsoCode,
                              _item.Denomination, CageMovementId, 0, _aux_amount, _item.CurrencyExchangeType, _item.CageCurrencyType, null);
      }

      return _cashier_movements.Save(SqlTrx);
    }

    /// <summary>
    /// Insert a movement with quantity and denomitation information
    /// </summary>
    /// <param name="MovementType"></param>
    /// <param name="OperationId"></param>
    /// <param name="MovementCashierSession"></param>
    /// <param name="TerminalDenominations"></param>
    /// <param name="RelatedId">Id used to group all operations in one transaction</param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean InsertCashierMovements(CASHIER_MOVEMENT MovementType, Int64 OperationId, CashierSessionInfo MovementCashierSession, 
                                           TerminalDenominations TerminalDenominations, Int64? RelatedId, SqlTransaction SqlTrx)
    {
      Decimal _aux_amount = -1;
      CashierMovementsTable _cm_mov_table;
      String _national_currency;
      CurrencyExchange _currency_exchange;
      CurrencyExchangeResult _exchange_result;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      _cm_mov_table = new CashierMovementsTable(MovementCashierSession);

      if (TerminalDenominations.IsoCode != _national_currency && TerminalDenominations.IsoCode != Cage.CHIPS_ISO_CODE)
      {
        CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, TerminalDenominations.IsoCode, out _currency_exchange, SqlTrx);
        _currency_exchange.ApplyExchange(TerminalDenominations.Total, out _exchange_result);
        _aux_amount = _exchange_result.GrossAmount;
      }

      if (TerminalDenominations.IsoCode == _national_currency)
      {
        _cm_mov_table.Add(OperationId, MovementType, TerminalDenominations.Total, 0, "");
      }
      else
      {
        _cm_mov_table.Add(OperationId, MovementType, TerminalDenominations.Total, 0, String.Empty, String.Empty, TerminalDenominations.IsoCode, 0, 0, 0, _aux_amount, CurrencyExchangeType.CURRENCY, null, null);
      }

      if (RelatedId != null && RelatedId > 0)
      {
        _cm_mov_table.AddRelatedIdInLastMov(RelatedId.Value);
      }

      return _cm_mov_table.Save(SqlTrx);
    }

    /// <summary>
    /// Insert a simple movement
    /// </summary>
    /// <param name="MovementType"></param>
    /// <param name="OperationId"></param>
    /// <param name="MovementCashierSession"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean InsertCashierMovements(CASHIER_MOVEMENT MovementType, Int64 OperationId,
                                           CashierSessionInfo MovementCashierSession, SqlTransaction SqlTrx)
    {
      CashierMovementsTable _cm_mov_table;
      _cm_mov_table = new CashierMovementsTable(MovementCashierSession);
      _cm_mov_table.Add(OperationId, MovementType, 0, 0, "");

      return _cm_mov_table.Save(SqlTrx);
    }

    private Boolean AddMovementToCloseCashierSession(Int32 TerminalId, Int64 OperationId, CashierSessionInfo CashierSessionTerminal, SqlTransaction SqlTrx)
    {
      if (InsertCashierMovements(CASHIER_MOVEMENT.CLOSE_SESSION, OperationId, CashierSessionTerminal, SqlTrx))
      {
        return Cashier.CashierSessionCloseById(CashierSessionTerminal.CashierSessionId, TerminalId, CashierSessionTerminal.TerminalName, SqlTrx);
      }

      return false;
    }

    /// <summary>
    /// Generate voucher with the list of terminals collected and refilled
    /// </summary>
    /// <param name="TerminalsToCollectAndRefill"></param>
    /// <param name="OperationId"></param>
    /// <param name="Voucher"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean GenerateCollectedVoucher(List<TerminalCollectRefill> TerminalsToCollectAndRefill, out VoucherTerminalsCollect Voucher, SqlTransaction SqlTrx)
    {
      VoucherTerminalsCollect _voucher;
      VoucherTerminalsCollectDetail _voucher_detail;

      Voucher = null;

      try
      {
        _voucher = new VoucherTerminalsCollect(CashierVoucherType.TerminalsToCollect, TerminalsToCollectAndRefill, PrintMode.Print, SqlTrx);

        foreach (TerminalCollectRefill _item in TerminalsToCollectAndRefill)
        {
          _voucher_detail = new VoucherTerminalsCollectDetail(_item.TerminalName, _item.DenominationsCollect.Total, _item.DenominationsRefill.Total);
          _voucher.SetParameterValue("@VOUCHER_TERMINALS_COLLECT_DETAIL", _voucher_detail.VoucherHTML);
        }

        _voucher.AddString("VOUCHER_TERMINALS_COLLECT_DETAIL", "");

        if (!_voucher.Save(SqlTrx))
        {
          Log.Message("Error on GenerateCollectedVoucher _voucher.Save: There was an exception recording the voucher in the table");
          return false;
        }

        Voucher = _voucher;

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    /// <summary>
    /// Generate voucher with the list of terminals refilled
    /// </summary>
    /// <param name="TerminalsToCollectAndRefill"></param>
    /// <param name="Voucher"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean GenerateRefilledVoucher(List<TerminalCollectRefill> TerminalsToRefill, out VoucherTerminalsRefill Voucher, SqlTransaction SqlTrx)
    {
      VoucherTerminalsRefill _voucher;
      VoucherTerminalsRefillDetail _voucher_detail;

      Voucher = null;

      try
      {
        _voucher = new VoucherTerminalsRefill(CashierVoucherType.TerminalsToRefill, TerminalsToRefill, PrintMode.Print, SqlTrx);

        foreach (TerminalCollectRefill _item in TerminalsToRefill)
        {
          _voucher_detail = new VoucherTerminalsRefillDetail(_item.TerminalName, _item.DenominationsRefill.Total);
          _voucher.SetParameterValue("@VOUCHER_TERMINALS_REFILL_DETAIL", _voucher_detail.VoucherHTML);
        }

        _voucher.AddString("VOUCHER_TERMINALS_REFILL_DETAIL", "");

        if (!_voucher.Save(SqlTrx))
        {
          Log.Message("Error on GenerateRefilledVoucher _voucher.Save: There was an exception recording the voucher in the tabl");
          return false;
        }

        Voucher = _voucher;

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    /// <summary>
    /// Update or Insert a movement of collected denominations
    /// </summary>
    /// <param name="TerminalToCollectoAndRefill"></param>
    /// <param name="SessionOperation"></param>
    /// <param name="CageMovementId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean UpdateOrInsertCageMovementsCollected(TerminalCollectRefill TerminalToCollectoAndRefill, SessionOperation SessionOperation, out Int64 CageMovementId, SqlTransaction SqlTrx)
    {
      Int64 _money_collection_id_collect;
      CageMovementId = 0;

      _money_collection_id_collect = TerminalToCollectoAndRefill.MoneyCollectionInfoCollect.CollectionId;

      Cage.GetCageMovement(_money_collection_id_collect, out CageMovementId);

      if (CageMovementId == 0)
      {
        // Insert movements in the Cage
        if (!InsertCageMovements(SessionOperation.TerminalCashierSessionCollect,TerminalToCollectoAndRefill.DenominationsCollect,
                                 Cage.CageOperationType.FromTerminal, _money_collection_id_collect, out CageMovementId, SqlTrx))
        {
          Log.Message(string.Format("Could not insert cage movements for the Terminal in its CashierSessionId {0}", SessionOperation.TerminalCashierSessionCollect.CashierSessionId));
          return false;
        }
      }
      else
      {
        // Actualiza movements in Cage
        if (!UpdateCageMovements(CageMovementId, SessionOperation.TerminalCashierSessionCollect, _money_collection_id_collect, SqlTrx))
        {
          Log.Message(string.Format("Could not update cage movements for the Terminal in its CashierSessionId {0}", SessionOperation.TerminalCashierSessionCollect.CashierSessionId));
          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// Update or Insert a movement of collected denominations
    /// </summary>
    /// <param name="TerminalToCollectoAndRefill"></param>
    /// <param name="SessionOperation"></param>
    /// <param name="CageMovementId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean UpdateOrInsertCageMovementsRefilled(TerminalCollectRefill TerminalToCollectoAndRefill, SessionOperation SessionOperation, out Int64 CageMovementId, SqlTransaction SqlTrx)
    {
      Int64 _money_collection_id_refill;
      CageMovementId = 0;

      _money_collection_id_refill = TerminalToCollectoAndRefill.MoneyCollectionInfoRefill.CollectionId;

      Cage.GetCageMovement(_money_collection_id_refill, out CageMovementId);

      if (CageMovementId == 0)
      {
        // Insert movements in the Cage
        if (!InsertCageMovements(SessionOperation.TerminalCashierSessionRefill, TerminalToCollectoAndRefill.DenominationsRefill,
                                 Cage.CageOperationType.FromTerminal, _money_collection_id_refill, out CageMovementId, SqlTrx))
        {
          Log.Message(string.Format("Could not insert cage movements for the Terminal in its CashierSessionId {0}", SessionOperation.TerminalCashierSessionRefill.CashierSessionId));
          return false;
        }
      }
      else
      {
        // Actualiza movements in Cage
        if (!UpdateCageMovements(CageMovementId, SessionOperation.TerminalCashierSessionRefill, _money_collection_id_refill, SqlTrx))
        {
          Log.Message(string.Format("Could not update cage movements for the Terminal in its CashierSessionId {0}", SessionOperation.TerminalCashierSessionRefill.CashierSessionId));
          return false;
        }
      }

      return true;
    }

    private void ChangeStackerGamingHall(Int32 TerminalId, Int64 OperationId, SqlTransaction SqlTrx)
    {
      if (!WSI.Common.WcpCommands.InsertWcpCommand(TerminalId, WCP_CommandCode.RequestChangeStacker, "", SqlTrx))
      {
        Log.Error("The call to InsertWcpCommand has answered with an error");
      }
    }

    private Boolean ChangeStackerGamingHallOffline(Int32 TerminalId, SqlTransaction SqlTrx)
    {
      bool _result = false;

      if (MB_CASHIER_SESSION_CLOSE_STATUS.ERROR != (MB_CASHIER_SESSION_CLOSE_STATUS)Cashier.WCPCashierSessionPendingClosing(TerminalId, SqlTrx))
      // We don't send any stacker, we are closing the actual one and leaving the terminal without one.
      {
        if (WSI.Common.TITO.TITO_ChangeStacker.ChangeStackerInTitoMode(TerminalId, null, 0, false, SqlTrx))
        {
          _result = TerminalStatusFlags.SetTerminalStatus(TerminalId, TerminalStatusFlags.WCP_TerminalEvent.ChangeStacker, SqlTrx);
        }
      }

      // Set again the current values for the Cashier session because "ChangeStackerGamingHallOffline" changes these values
      CommonCashierInformation.SetCashierInformation(m_cashier_session.CashierSessionId, m_cashier_session.UserId,
                                                     m_cashier_session.UserName, m_cashier_session.TerminalId,
                                                     m_cashier_session.TerminalName, m_cashier_session.GamingDay);

      return _result;
    }

    private void ValidateChangeStacker(ref List<TerminalCollectRefill> Terminals, ref List<TerminalCollectRefill> TerminalsWithError, Int64 OperationId, SqlTransaction SqlTrx)
    {
      DateTime _currentTime;
      DateTime _maxTime;

      _currentTime = DateTime.Now;
      _maxTime = _currentTime.AddSeconds(MAX_TIME_SECONDS_TO_VALIDATE_CHANGE_STACKER);

      while (_maxTime > _currentTime)
      {
        int _terminals_stacker_ok = 0;

        foreach (TerminalCollectRefill _terminal in Terminals)
        {
          _terminal.SetChangeStackerOK = IsProcesed(_terminal.MoneyCollectionId, SqlTrx);

          if (_terminal.IsStackerChangedOK)
          {
            InsertCashierMovements(CASHIER_MOVEMENT.CASHIER_CHANGE_STACKER_TERMINAL, OperationId, m_cashier_session, SqlTrx);
            _terminals_stacker_ok++;

            SetTerminalCurrentMeters(_terminal, SqlTrx);
            SaveTerminalMetersOnMoneyCollection(_terminal, SqlTrx);
          }
        }

        if (Terminals.Count == _terminals_stacker_ok)
          break;

        System.Threading.Thread.Sleep(1000);

        _currentTime = DateTime.Now;
      }

      foreach (TerminalCollectRefill _terminal in Terminals)
      {
        //Pruebas mientras no pueda probar SasHost. Quitar en RELEASE
#if DEBUG
        //SetTerminalCurrentMeters(_terminal, SqlTrx);
#endif
        if (!_terminal.IsStackerChangedOK)
          TerminalsWithError.Add(_terminal);

      }
    }

    private static void SetTerminalCurrentMeters(TerminalCollectRefill _terminal, SqlTransaction SqlTrx)
    {
      String _meters_by_terminal;
      List<TerminalMeter> _terminalMeters;
      SMIB_COMMUNICATION_TYPE _smib_comm_type;
      Int64 _smib_conf_id;
      DataSet _pcd_info;
      DataRow[] _pcd_config_items;

      Int32 _meter_out_bills;
      Int32 _meter_out_coins;
      Int32 _meter_out_cents;
      Int64 _meter_code;

      _meter_out_bills = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutBills", 0);
      _meter_out_coins = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutCoins", 0);
      _meter_out_cents = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutCents", 0);

      Terminal.Trx_GetTerminalSmibProtocol(_terminal.TerminalId, TerminalTypes.SAS_HOST, SqlTrx, out _smib_comm_type, out _smib_conf_id);
      _meters_by_terminal = String.Empty;


      if (_smib_comm_type == SMIB_COMMUNICATION_TYPE.PULSES)
      {
        _pcd_info = TerminalMetersInfo.ReadPCDConfiguration(SqlTrx, _smib_conf_id);
        _pcd_config_items = _pcd_info.Tables[1].Select("PMT_PCD_IO_TYPE = 1");
        
         DataRow last = _pcd_config_items[_pcd_config_items.Length-1];
         foreach (DataRow meter_info in _pcd_config_items)
         {
            _meter_code = (Int64)meter_info["PMT_EGM_NUMBER"];

            _meters_by_terminal += _meter_code.ToString();
           
           if (!meter_info.Equals(last))
           {
             _meters_by_terminal += ",";
           }
         }
      }
      else
      {
        _meters_by_terminal = _meter_out_bills.ToString() + "," + _meter_out_coins.ToString() + "," + _meter_out_cents.ToString();
      }


      if (!TerminalMetersInfo.ReadTerminalMeters(
        _terminal.TerminalId,
        _meters_by_terminal,
        out _terminalMeters, SqlTrx))
      {
        _terminal.SetChangeStackerOK = false;
        Log.Message(String.Format("Error on ReadTerminalMeters: Could not read meters on terminal {0}", _terminal.TerminalId));
      }
      else
      {
        _terminal.CurrentMeters = TerminalMetersInfo.GetMetersXml(_terminalMeters);
      }
    }

    private void SaveTerminalMetersOnMoneyCollection(TerminalCollectRefill _terminal, SqlTransaction SqlTrx)
    {
        Boolean _result;
        MoneyCollectionDAO _dao;

        _dao = new MoneyCollectionDAO();

        _result = _dao.DB_UpdateFinalMetersById(_terminal.MoneyCollectionId.Value, _terminal.CurrentMeters, SqlTrx);
        if(!_result)
            Log.Message(String.Format("Error on SaveTerminalMetersOnMoneyCollection: Could not save meters on terminal {0}, MoneyCollection {1} "
                , _terminal.TerminalId, _terminal.MoneyCollectionId.Value));
    }

    private Boolean IsProcesed(Int64? Collection_id, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlCommand _cmd;
      int _status;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT MC_STATUS FROM MONEY_COLLECTIONS              ");
      _sb.AppendLine(" WHERE MC_COLLECTION_ID = @pCollectionId AND MC_STATUS = @pStatus ");

      using (_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pCollectionId", SqlDbType.Int).Value = Collection_id;
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)TITO_MONEY_COLLECTION_STATUS.PENDING;

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            _status = (int)_reader.GetInt32(0);

            return (_status == (int)TITO_MONEY_COLLECTION_STATUS.PENDING);
          }
        }
      }

      return false;
    }

    private Boolean UndoOperationAndMovements(OperationCode OperationCode, CashierSessionInfo CashierSessionInfo, Int64 OperationId, out ArrayList VoucherList, SqlTransaction SqlTrx)
    {
      OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
      _params.CodeOperation = OperationCode;
      _params.CardData = new CardData();
      _params.GenerateVoucherForUndo = true;

      _params.CashierSessionInfo = CashierSessionInfo;
      _params.OperationIdForUndo = OperationId;

      return OperationUndo.UndoOperation(_params, false, out VoucherList, SqlTrx);
    }

    private Boolean GetCashierSessionFromTerminal(Int32 TerminalId, CASHIER_SESSION_STATUS Status, out CashierSessionInfo CashierSessionInTerminal, SqlTransaction SqlTrx)
    {
      CashierSessionInTerminal = null;

      Int32 _user_id;
      String _user_name;

      try
      {
        // All terminals uses SYS_TITO
        if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_TITO, SqlTrx, out _user_id, out _user_name))
        {
          Log.Message("Error on GetSystemUser: Error getting the user (SYS_TITO) for the Terminal");
          return false;
        }

        return (Cashier.GetCashierSessionFromTerminal(TerminalId, _user_id, Status, out CashierSessionInTerminal, SqlTrx)
                && CashierSessionInTerminal.CashierSessionId > 0);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }
    #endregion

  }
}
