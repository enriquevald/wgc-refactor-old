﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalCurrencyDetails.cs
// 
//   DESCRIPTION: It has classes to insert, update or get records (terminal_currency_details table) 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 FAV    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingHall
{
  /// <summary>
  /// Business class: TerminalCurrencyDetails
  /// </summary>
  public class TerminalCurrencyDetails
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Gets the details (TerminalCurrencyDetailsInfo list) by a TerminalCurrencyInfo
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalCurrencyInfo
    //        - TerminalCashDirection  
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - 
    //
    // RETURNS :
    //      - List<TerminalCurrencyDetailsInfo>
    //
    //   NOTES:
    //
    public List<TerminalCurrencyDetailsInfo> GetTerminalCurrencyDetailsList(TerminalCurrencyInfo TerminalCurrencyInfo, 
                                                                            TerminalCashDirection TerminalCashDirection, SqlTransaction Trx)
    {
      TerminalCurrencyDetailsInfoDAO _dao;
      List<TerminalCurrencyDetailsInfo> terminal_currency_detail_info_list = null;

      _dao = new TerminalCurrencyDetailsInfoDAO();
      _dao.DB_GetTerminalCurrencyDetailsList(TerminalCurrencyInfo, TerminalCashDirection, out terminal_currency_detail_info_list, Trx);

      return terminal_currency_detail_info_list;
    }

  }

  /// <summary>
  /// Entity class: TerminalCurrencyDetailsInfo
  /// </summary>
  public class TerminalCurrencyDetailsInfo
  {
    #region Properties
    public Int32 TerminalCurrencyId { get; set; }

    public String IsoCode { get; set; }

    public Decimal Denomination { get; set; }

    public CageCurrencyType CurrencyType { get; set; }

    public TerminalCashDirection TerminalCashDirection { get; set; }
    #endregion
  }

  /// <summary>
  /// Internal data access class: TerminalCurrencyDetailsInfoDAO
  /// </summary>
  internal class TerminalCurrencyDetailsInfoDAO
  {
    #region Attributes

    private const int CONNECTION_EXCEPTION_TIME_OUT = -2;

    #endregion

    #region Public Methods

    public Boolean DB_GetTerminalCurrencyDetailsList(TerminalCurrencyInfo TerminalCurrencyInfo, TerminalCashDirection TerminalCashDirection,
                                                     out List<TerminalCurrencyDetailsInfo> TerminalCurrencyDetailInfoList, SqlTransaction Trx)
    {
      StringBuilder _sb;
      TerminalCurrencyDetailInfoList = null;
      List<TerminalCurrencyDetailsInfo> _list = new List<TerminalCurrencyDetailsInfo>();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   TCD_ISO_CODE");
        _sb.AppendLine("       , TCD_DENOMINATION");
        _sb.AppendLine("       , TCD_CURRENCY_TYPE");
        _sb.AppendLine("       , TCD_DIRECTION");
        _sb.AppendLine("  FROM   TERMINAL_CURRENCY_DETAILS");
        _sb.AppendLine(" WHERE   TCD_TERMNAL_CURRENCY_ID = @pTerminalCurrencyId");
        _sb.AppendLine("         AND  TCD_DIRECTION = @pDirection");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalCurrencyId", SqlDbType.Int).Value = TerminalCurrencyInfo.Id;
          _cmd.Parameters.Add("@pTerminalCashDirection", SqlDbType.Int).Value = TerminalCashDirection;


          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              TerminalCurrencyDetailsInfo _transfer_cashier_info = new TerminalCurrencyDetailsInfo()
              {
                TerminalCurrencyId = TerminalCurrencyInfo.Id,
                IsoCode = (String)_reader[0],
                Denomination = (Decimal)_reader[1],
                CurrencyType = (CageCurrencyType)_reader[2],
                TerminalCashDirection = (TerminalCashDirection)_reader[3],
              };

              _list.Add(_transfer_cashier_info);
            }

            TerminalCurrencyDetailInfoList = _list;
            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion

  }
}
