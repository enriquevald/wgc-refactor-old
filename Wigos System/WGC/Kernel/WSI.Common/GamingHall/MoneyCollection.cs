﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalMoneyCollection.cs
// 
//   DESCRIPTION: It has classes to insert, update or get records (MONEY_COLLECTIONS and MONEY_COLLECTION_DETAILS tables) 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 02-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-NOV-2015 FAV    First release.
// 01-MAR-2016 DDS    Fixed Bug 10135: DropBox value is -100 when Cage.ShowDenominations = 0
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingHall
{
  public static class MoneyCollection
  {
    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Gets a TerminalMoneyCollectionInfo by TerminalId, CashierSessionId and Status
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId
    //        - CashierSessionTerminalId
    //        - Status
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - TerminalMoneyCollectionInfo
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public static Boolean GetMoneyCollection(Int32 TerminalId, Int64 CashierSessionTerminalId, TITO_MONEY_COLLECTION_STATUS Status,
                                             out MoneyCollectionInfo TerminalMoneyCollectionInfo, SqlTransaction Trx)
    {
      MoneyCollectionDAO _dao;

      _dao = new MoneyCollectionDAO();
      return _dao.DB_GetMoneyCollection(TerminalId, CashierSessionTerminalId, Status, out TerminalMoneyCollectionInfo, Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Gets a TerminalMoneyCollectionInfo by Id
    //
    //  PARAMS:
    //      - INPUT:
    //        - MoneyCollectionId
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - TerminalMoneyCollectionInfo
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public static Boolean GetMoneyCollection(Int64 MoneyCollectionId, out MoneyCollectionInfo TerminalMoneyCollectionInfo, SqlTransaction Trx)
    {
      MoneyCollectionDAO _dao;

      _dao = new MoneyCollectionDAO();
      return _dao.DB_GetMoneyCollectionById(MoneyCollectionId, out TerminalMoneyCollectionInfo, Trx);
    }
    
    //------------------------------------------------------------------------------
    // PURPOSE: Change status MoneyCollection of pending to close (CLOSED_BALANCED or CLOSED_IN_IMBALANCE)
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId
    //        - UserId
    //        - DenominationsCollect: Quantities and denominations entered by the user
    //        - TerminalMoneyCollectionInfo
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public static Boolean ChangeStatusPendingToClose(Int32 TerminalId, Int32 UserId, TerminalDenominations DenominationsCollect,TerminalDenominations DenominationsRefill,
                                                     MoneyCollectionInfo TerminalMoneyCollectionInfo, SqlTransaction Trx)
    {
      MoneyCollectionDAO _dao;

      _dao = new MoneyCollectionDAO();
      return _dao.DB_UpdatePendingMoneyCollectionToClose(TerminalId, UserId, DenominationsCollect, DenominationsRefill, TerminalMoneyCollectionInfo, Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert MoneyCollectionDetails
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalMoneyCollectionInfo
    //        - DenominationsCollect: It has the entered values by the user
    //        - CashierTerminalMoneyList: It has the expected values
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - TerminalMoneyCollectionDetailsInfoList
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public static Boolean InsertMoneyCollectionDetails(MoneyCollectionInfo TerminalMoneyCollectionInfo, TerminalDenominations DenominationsCollect,
                                                       List<CashierTerminalMoneyInfo> CashierTerminalMoneyList,
                                                       out List<MoneyCollectionDetailsInfo> TerminalMoneyCollectionDetailsInfoList, SqlTransaction Trx)
    {
      MoneyCollectionDAO _dao;

      _dao = new MoneyCollectionDAO();
      return _dao.DB_InsertMoneyCollectionDetails(TerminalMoneyCollectionInfo, DenominationsCollect, CashierTerminalMoneyList, out TerminalMoneyCollectionDetailsInfoList, Trx);
    }
    
    //------------------------------------------------------------------------------
    // PURPOSE: Update Final_Meters and Initial_Meters
    //
    //  PARAMS:
    //      - INPUT:
    //        - MoneyCollectionIdPending
    //        - MoneyCollectionIdOpened
    //        - CurrentMeters
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public static Boolean UpdateMetersInMoneyCollections(Int64 MoneyCollectionIdPending, Int64 MoneyCollectionIdOpened, String CurrentMeters, SqlTransaction Trx)
    {
      MoneyCollectionDAO _dao;
      Boolean _result;

      _dao = new MoneyCollectionDAO();

      _result = _dao.DB_UpdateFinalMetersById(MoneyCollectionIdPending, CurrentMeters, Trx);

      if (_result)
      {
        _dao.DB_UpdateInitialMetersById(MoneyCollectionIdOpened, CurrentMeters, Trx);
      }

      return _result;
    }

    public static Boolean UpdateRefilledHopperFieldOnMoneyCollection(Int64 MoneyCollectionId, TITO_MONEY_COLLECTION_STATUS ValidStatus, TerminalDenominations DenominationsRefill, SqlTransaction Trx)
    {
      MoneyCollectionDAO _dao;

      _dao = new MoneyCollectionDAO();
      return _dao.DB_UpdateRefillInHopperOnMoneyCollection(MoneyCollectionId, ValidStatus, DenominationsRefill, Trx);
    }

    public static Boolean CancelMoneyCollectionFromTerminal(Int64 TerminalCashierSessionId, Int64 MoneyCollectionId, Int64 CageMovementId, SqlTransaction Trx)
    {
      MoneyCollectionDAO _dao;

      _dao = new MoneyCollectionDAO();
      return _dao.CancelMoneyCollectionFromTerminal(TerminalCashierSessionId, MoneyCollectionId, CageMovementId, Trx);
    }

    #endregion
  }

  internal class MoneyCollectionDAO
  {
    #region Attributes

    private const int CONNECTION_EXCEPTION_TIME_OUT = -2;

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Gets a TerminalMoneyCollectionInfo by TerminalId, CashierSessionId and Status
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId
    //        - CashierSessionId
    //        - Status
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - TerminalMoneyCollectionInfo
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean DB_GetMoneyCollection(Int32 TerminalId, Int64 CashierSessionId, TITO_MONEY_COLLECTION_STATUS Status,
                                         out MoneyCollectionInfo TerminalMoneyCollectionInfo, SqlTransaction Trx)
    { 
      StringBuilder _sb;
      TerminalMoneyCollectionInfo = new MoneyCollectionInfo();

      try
      {
        _sb = new StringBuilder();  
        _sb.AppendLine("SELECT    MC_COLLECTION_ID                                                    ");
        _sb.AppendLine("		    , MC_TERMINAL_ID 	                                                    ");
        _sb.AppendLine("		    , MC_USER_ID                                                          ");
        _sb.AppendLine("		    , MC_CASHIER_SESSION_ID                                               ");
        _sb.AppendLine("		    , MC_COLLECTION_DATETIME                                              ");
        _sb.AppendLine("		    , MC_EXTRACTION_DATETIME                                              ");
        _sb.AppendLine("		    , MC_STATUS                                                           ");
        _sb.AppendLine("		    , MC_EXPECTED_BILL_AMOUNT                                             ");
        _sb.AppendLine("		    , MC_EXPECTED_BILL_COUNT                                              ");
        _sb.AppendLine("		    , MC_EXPECTED_COIN_AMOUNT                                             ");
        _sb.AppendLine("		    , MC_OUT_BILLS                                                        ");
        _sb.AppendLine("		    , MC_OUT_COINS                                                        ");
        _sb.AppendLine("		    , MC_OUT_CENTS                                                        ");
        _sb.AppendLine("		    , MC_INITIAL_METERS                                                   ");
        _sb.AppendLine("		    , MC_FINAL_METERS                                                     ");
        _sb.AppendLine("  FROM    MONEY_COLLECTIONS   WITH(INDEX(IX_mc_cashier_session_terminal_id))  ");
        _sb.AppendLine(" WHERE    MC_CASHIER_SESSION_ID = @pCashierSessionId                          ");
        _sb.AppendLine("   AND    MC_TERMINAL_ID = @pTerminalId                                       ");
        _sb.AppendLine("   AND    MC_STATUS = @pStatus                                                ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              TerminalMoneyCollectionInfo.CollectionId = (Int64)_reader[0];
              TerminalMoneyCollectionInfo.TerminalId = _reader.IsDBNull(1) ? (Int32?)null : (Int32)_reader[1];
              TerminalMoneyCollectionInfo.UserlId = _reader.IsDBNull(2) ? (Int32?)null : (Int32)_reader[2];
              TerminalMoneyCollectionInfo.CashierSessionId = _reader.IsDBNull(3) ? (Int64?)null : (Int64)_reader[3];
              TerminalMoneyCollectionInfo.Collection = _reader.IsDBNull(4) ? (DateTime?)null : (DateTime)_reader[4];
              TerminalMoneyCollectionInfo.Extraction = _reader.IsDBNull(5) ? (DateTime?)null : (DateTime)_reader[5];
              TerminalMoneyCollectionInfo.Status = _reader.IsDBNull(6) ? (TITO_MONEY_COLLECTION_STATUS?)null : (TITO_MONEY_COLLECTION_STATUS)_reader[6];
              TerminalMoneyCollectionInfo.ExpectedBillAmount =  _reader.IsDBNull(7) ? 0 : (Decimal)_reader[7];
              TerminalMoneyCollectionInfo.ExpectedBillCount = _reader.IsDBNull(8) ? 0 : (Int32)_reader[8];
              TerminalMoneyCollectionInfo.ExpectedCoinAmount = _reader.IsDBNull(9) ? 0 : (Decimal)_reader[9];
              TerminalMoneyCollectionInfo.OutBills = _reader.IsDBNull(10) ? (Decimal?)null : (Decimal)_reader[10];
              TerminalMoneyCollectionInfo.OutCoins = _reader.IsDBNull(11) ? (Decimal?)null : (Decimal)_reader[11];
              TerminalMoneyCollectionInfo.OutCents = _reader.IsDBNull(12) ? (Decimal?)null : (Decimal)_reader[12];
              TerminalMoneyCollectionInfo.InitialMeters = _reader.IsDBNull(13) ? String.Empty : (String)_reader[13];
              TerminalMoneyCollectionInfo.FinalMeters = _reader.IsDBNull(14) ? String.Empty : (String)_reader[14];

              TerminalMoneyCollectionInfo.ExpectedCoinCount = 0;
              return true;
            }
            
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Gets a TerminalMoneyCollectionInfo by Id
    //
    //  PARAMS:
    //      - INPUT:
    //        - MoneyCollectionId
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - TerminalMoneyCollectionInfo
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean DB_GetMoneyCollectionById(Int64 MoneyCollectionId, out MoneyCollectionInfo TerminalMoneyCollectionInfo, SqlTransaction Trx)
    {
      StringBuilder _sb;
      TerminalMoneyCollectionInfo = new MoneyCollectionInfo();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT    MC_COLLECTION_ID                                                    ");
        _sb.AppendLine("		    , MC_TERMINAL_ID 	                                                    ");
        _sb.AppendLine("		    , MC_USER_ID                                                          ");
        _sb.AppendLine("		    , MC_CASHIER_SESSION_ID                                               ");
        _sb.AppendLine("		    , MC_COLLECTION_DATETIME                                              ");
        _sb.AppendLine("		    , MC_EXTRACTION_DATETIME                                              ");
        _sb.AppendLine("		    , MC_STATUS                                                           ");
        _sb.AppendLine("		    , MC_EXPECTED_BILL_AMOUNT                                             ");
        _sb.AppendLine("		    , MC_EXPECTED_BILL_COUNT                                              ");
        _sb.AppendLine("		    , MC_EXPECTED_COIN_AMOUNT                                             ");
        _sb.AppendLine("		    , MC_OUT_BILLS                                                        ");
        _sb.AppendLine("		    , MC_OUT_COINS                                                        ");
        _sb.AppendLine("		    , MC_OUT_CENTS                                                        ");
        _sb.AppendLine("		    , MC_INITIAL_METERS                                                   ");
        _sb.AppendLine("		    , MC_FINAL_METERS                                                     ");
        _sb.AppendLine("  FROM    MONEY_COLLECTIONS                                                   ");
        _sb.AppendLine(" WHERE    MC_COLLECTION_ID = @pMoneyCollectionId                              ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              TerminalMoneyCollectionInfo.CollectionId = (Int64)_reader[0];
              TerminalMoneyCollectionInfo.TerminalId = _reader.IsDBNull(1) ? (Int32?)null : (Int32)_reader[1];
              TerminalMoneyCollectionInfo.UserlId = _reader.IsDBNull(2) ? (Int32?)null : (Int32)_reader[2];
              TerminalMoneyCollectionInfo.CashierSessionId = _reader.IsDBNull(3) ? (Int64?)null : (Int64)_reader[3];
              TerminalMoneyCollectionInfo.Collection = _reader.IsDBNull(4) ? (DateTime?)null : (DateTime)_reader[4];
              TerminalMoneyCollectionInfo.Extraction = _reader.IsDBNull(5) ? (DateTime?)null : (DateTime)_reader[5];
              TerminalMoneyCollectionInfo.Status = _reader.IsDBNull(6) ? (TITO_MONEY_COLLECTION_STATUS?)null : (TITO_MONEY_COLLECTION_STATUS)_reader[6];
              TerminalMoneyCollectionInfo.ExpectedBillAmount = _reader.IsDBNull(7) ? 0 : (Decimal)_reader[7];
              TerminalMoneyCollectionInfo.ExpectedBillCount = _reader.IsDBNull(8) ? 0 : (Int32)_reader[8];
              TerminalMoneyCollectionInfo.ExpectedCoinAmount = _reader.IsDBNull(9) ? 0 : (Decimal)_reader[9];
              TerminalMoneyCollectionInfo.OutBills = _reader.IsDBNull(10) ? (Decimal?)null : (Decimal)_reader[10];
              TerminalMoneyCollectionInfo.OutCoins = _reader.IsDBNull(11) ? (Decimal?)null : (Decimal)_reader[11];
              TerminalMoneyCollectionInfo.OutCents = _reader.IsDBNull(12) ? (Decimal?)null : (Decimal)_reader[12];
              TerminalMoneyCollectionInfo.InitialMeters = _reader.IsDBNull(13) ? String.Empty : (String)_reader[13];
              TerminalMoneyCollectionInfo.FinalMeters = _reader.IsDBNull(14) ? String.Empty : (String)_reader[14];
              TerminalMoneyCollectionInfo.ExpectedCoinCount = 0;

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update MoneyCollection of pending to close (CLOSED_BALANCED or CLOSED_IN_IMBALANCE)
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId
    //        - UserId
    //        - DenominationsCollect
    //        - TerminalMoneyCollectionInfo
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean DB_UpdatePendingMoneyCollectionToClose(Int32 TerminalId, Int32 UserId, TerminalDenominations DenominationsCollect, TerminalDenominations DenominationsRefill,
                                                          MoneyCollectionInfo TerminalMoneyCollectionInfo, SqlTransaction Trx)
    {
      Int32 _rows_affected;
      StringBuilder _sb;

      try
      {
        bool _is_balanced = false;
        _is_balanced = TerminalMoneyCollectionInfo.ExpectedBillAmount == DenominationsCollect.GetTotalByType(CageCurrencyType.Bill)
                        && TerminalMoneyCollectionInfo.ExpectedBillCount == DenominationsCollect.GetCountByType(CageCurrencyType.Bill)
                        && TerminalMoneyCollectionInfo.ExpectedCoinAmount == DenominationsCollect.GetTotalByType(CageCurrencyType.Coin)
                        && TerminalMoneyCollectionInfo.ExpectedCoinCount == DenominationsCollect.GetCountByType(CageCurrencyType.Coin);

        _sb = new StringBuilder();  
        _sb.AppendLine("  UPDATE   MONEY_COLLECTIONS                                           ");
        _sb.AppendLine("     SET   MC_USER_ID = @pUserId                                       ");
        _sb.AppendLine("         , MC_COLLECTION_DATETIME = @pCollectionDateTime               ");
        _sb.AppendLine("         , MC_STATUS = @pNewStatus                                     ");
        _sb.AppendLine("         , MC_COLLECTED_BILL_AMOUNT = @pBillAmount                     ");
        _sb.AppendLine("         , MC_COLLECTED_BILL_COUNT = @pBillCount                       ");
        _sb.AppendLine("         , MC_COLLECTED_COIN_AMOUNT  =  @pCoinCollectionAmount         ");
        _sb.AppendLine("         , MC_REFILLED_HOPPER  =  @pDenominationsRefilled              ");
        _sb.AppendLine("   WHERE   MC_COLLECTION_ID = @pCollectionId              ");
        _sb.AppendLine("     AND   MC_STATUS = @pOldStatus                        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _cmd.Parameters.Add("@pCollectionDateTime", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = _is_balanced ?  TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED : TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE;
          _cmd.Parameters.Add("@pBillAmount", SqlDbType.Decimal).Value = DenominationsCollect.GetTotalByType(CageCurrencyType.Bill);
          _cmd.Parameters.Add("@pBillCount", SqlDbType.Int).Value = DenominationsCollect.GetCountByType(CageCurrencyType.Bill);
          _cmd.Parameters.Add("@pCoinCollectionAmount", SqlDbType.Decimal).Value = DenominationsCollect.GetTotalByType(CageCurrencyType.Coin);
          _cmd.Parameters.Add("@pDenominationsRefilled", SqlDbType.Xml).Value = XMLList.ListToXML(DenominationsRefill.DenominationsWithQuantity); 
          _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = TerminalMoneyCollectionInfo.CollectionId;
          _cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING;

          _rows_affected = _cmd.ExecuteNonQuery();

          return (_rows_affected == 1);
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Insert MoneyCollectionDetails
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalMoneyCollectionInfo
    //        - DenominationsCollect: It has the entered values by the user
    //        - CashierTerminalMoneyList: It has the expected values
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - TerminalMoneyCollectionDetailsInfoList
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean DB_InsertMoneyCollectionDetails(MoneyCollectionInfo TerminalMoneyCollectionInfo, TerminalDenominations DenominationsCollect,
                                                   List<CashierTerminalMoneyInfo> CashierTerminalMoneyList,
                                                   out List <MoneyCollectionDetailsInfo> TerminalMoneyCollectionDetailsInfoList, SqlTransaction Trx)
    {
      Int32 _rows_affected;
      StringBuilder _sb;
      Boolean _show_denominations;
      bool _insert_ok = true;
      _show_denominations = false;

      TerminalMoneyCollectionDetailsInfoList = new List<MoneyCollectionDetailsInfo>();

      try
      {
        foreach (TerminalCurrencyItem _item in DenominationsCollect.DenominationsWithQuantity)
        {
          _sb = new StringBuilder();
          _sb.AppendLine("INSERT INTO   MONEY_COLLECTION_DETAILS  ");
          _sb.AppendLine("            ( MCD_COLLECTION_ID         ");
          _sb.AppendLine("            , MCD_FACE_VALUE            ");
          _sb.AppendLine("            , MCD_NUM_COLLECTED         ");
          _sb.AppendLine("            , MCD_NUM_EXPECTED          ");
          _sb.AppendLine("            , MCD_CAGE_CURRENCY_TYPE )  ");
          _sb.AppendLine("     VALUES                             ");
          _sb.AppendLine("            ( @pCollectionId            ");
          _sb.AppendLine("            , @pFaceValue               ");
          _sb.AppendLine("            , @pNumCollected            ");
          _sb.AppendLine("            , @pNumExpected             ");
          _sb.AppendLine("            , @pCurrencyType )          ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {

            decimal _expected_quantity = GetExpectedQuantityFromCashierTerminalMoney(_item.Denomination, _item.CageCurrencyType, CashierTerminalMoneyList);
            _show_denominations = GeneralParam.GetInt32("Cage", "ShowDenominations",
                 (Int32)WSI.Common.Cage.ShowDenominationsMode.ShowAllDenominations) == (Int32)WSI.Common.Cage.ShowDenominationsMode.ShowAllDenominations;

            _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = TerminalMoneyCollectionInfo.CollectionId;
            _cmd.Parameters.Add("@pFaceValue", SqlDbType.Money).Value = _item.Denomination;
            _cmd.Parameters.Add("@pNumCollected", SqlDbType.Money).Value = _show_denominations ? _item.Quantity.Value : _item.Total;
            _cmd.Parameters.Add("@pNumExpected", SqlDbType.Money).Value = _expected_quantity; // TODO: Quantity or Total?
            _cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = (Int32)_item.CageCurrencyType;

            _rows_affected = _cmd.ExecuteNonQuery();

            _insert_ok = _insert_ok && (_rows_affected == 1);

            if (_insert_ok)
            { 
              MoneyCollectionDetailsInfo _detail = new MoneyCollectionDetailsInfo()
              {
                CollectionId = TerminalMoneyCollectionInfo.CollectionId,
                Face = _item.Denomination,
                Collected = _item.Quantity.Value,
                Expected = _expected_quantity,
                CageCurrencyType = _item.CageCurrencyType,
              };

              TerminalMoneyCollectionDetailsInfoList.Add(_detail);
            }
          }
        }

        return _insert_ok;
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    public Boolean CancelMoneyCollectionFromTerminal(Int64 TerminalCashierSessionId, Int64 MoneyCollectionId, Int64 CageMovementId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      String _currency_iso_code;
      Object _bill_amount;
      Object _bill_count;
      Boolean _is_tito_mode;

      _sb = new StringBuilder();
      _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
      _bill_amount = 0;
      _bill_count = 0;

      _is_tito_mode = TITO.Utils.IsTitoMode();

      _sb.AppendLine(" DELETE FROM   MONEY_COLLECTION_DETAILS ");
      _sb.AppendLine("       WHERE   MCD_COLLECTION_ID = @pMoneyCollectionId ;");
      _sb.AppendLine("DECLARE   @pCollectedTicketAmount        AS MONEY   ");
      _sb.AppendLine("DECLARE   @pCollectedTicketCount         AS INTEGER ");
      _sb.AppendLine("DECLARE   @pCollectedReTicketAmount      AS MONEY   ");
      _sb.AppendLine("DECLARE   @pCollectedReTicketCount       AS INTEGER ");
      _sb.AppendLine("DECLARE   @pCollectedPromoReTicketAmount AS MONEY   ");
      _sb.AppendLine("DECLARE   @pCollectedPromoReTicketCount  AS INTEGER ");
      _sb.AppendLine("DECLARE   @pCollectedPromoNrTicketAmount AS MONEY   ");
      _sb.AppendLine("DECLARE   @pCollectedPromoNrTicketCount  AS INTEGER ");

      _sb.AppendLine("SET       @pCollectedTicketAmount        = 0");
      _sb.AppendLine("SET       @pCollectedTicketCount         = 0");
      _sb.AppendLine("SET       @pCollectedReTicketAmount      = 0");
      _sb.AppendLine("SET       @pCollectedReTicketCount       = 0");
      _sb.AppendLine("SET       @pCollectedPromoReTicketAmount = 0");
      _sb.AppendLine("SET       @pCollectedPromoReTicketCount  = 0");
      _sb.AppendLine("SET       @pCollectedPromoNrTicketAmount = 0");
      _sb.AppendLine("SET       @pCollectedPromoNrTicketCount  = 0");

      UpdateCancelFromMoneyCollection(ref _sb);

      if (_is_tito_mode)
      {
        _sb.AppendLine(" UPDATE   TICKETS ");
        _sb.AppendLine("    SET   TI_COLLECTED_MONEY_COLLECTION = NULL ,");
        _sb.AppendLine("          TI_CAGE_MOVEMENT_ID = NULL ");
        _sb.AppendLine("  WHERE   TI_CAGE_MOVEMENT_ID = @pCageMovementId ;");
      }

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {

          _cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;
          _cmd.Parameters.Add("@pMcStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING;
          _cmd.Parameters.Add("@pCageMovementId", SqlDbType.BigInt).Value = CageMovementId;
          _cmd.Parameters.Add("@pBillAmount", SqlDbType.Money).Value = (Decimal)(_bill_amount);
          _cmd.Parameters.Add("@pBillCount", SqlDbType.Int).Value = (Int32)(_bill_count);
          _cmd.Parameters.Add("@pCashable", SqlDbType.Int).Value = TITO_TICKET_TYPE.CASHABLE;
          _cmd.Parameters.Add("@pHandpay", SqlDbType.Int).Value = TITO_TICKET_TYPE.HANDPAY;
          _cmd.Parameters.Add("@pJackpot", SqlDbType.Int).Value = TITO_TICKET_TYPE.JACKPOT;
          _cmd.Parameters.Add("@pOffline", SqlDbType.Int).Value = TITO_TICKET_TYPE.OFFLINE;
          _cmd.Parameters.Add("@pPromoRE", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_REDEEM;
          _cmd.Parameters.Add("@pPromoNR", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_NONREDEEM;
          _cmd.Parameters.Add("@pRedeemed", SqlDbType.Int).Value = TITO_TICKET_STATUS.REDEEMED;
          _cmd.Parameters.Add("@pDiscarded", SqlDbType.Int).Value = TITO_TICKET_STATUS.DISCARDED;

          if (_cmd.ExecuteNonQuery() > 0)
          {
            return RollbackCashierSessionInfo(TerminalCashierSessionId, SqlTrx);
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update Final Meters field in the MoneyCollection 
    //
    //  PARAMS:
    //      - INPUT:
    //        - MoneyCollectionId
    //        - FinalMeters
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean DB_UpdateFinalMetersById(Int64 MoneyCollectionId, String FinalMeters, SqlTransaction Trx)
    {
      Int32 _rows_affected;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   MONEY_COLLECTIONS                     ");
        _sb.AppendLine("     SET   MC_FINAL_METERS = @pMeters            ");
        _sb.AppendLine("   WHERE   MC_COLLECTION_ID = @pCollectionId     ");
        
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pMeters", SqlDbType.Xml).Value = FinalMeters;
          _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;

          _rows_affected = _cmd.ExecuteNonQuery();
        }

        return (_rows_affected == 1);
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update Initial Meters field in the MoneyCollection 
    //
    //  PARAMS:
    //      - INPUT:
    //        - MoneyCollectionId
    //        - InitialMeters
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean DB_UpdateInitialMetersById(Int64 MoneyCollectionId, String InitialMeters, SqlTransaction Trx)
    {
      Int32 _rows_affected;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   MONEY_COLLECTIONS                     ");
        _sb.AppendLine("     SET   MC_INITIAL_METERS = @pMeters            ");
        _sb.AppendLine("   WHERE   MC_COLLECTION_ID = @pCollectionId     ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pMeters", SqlDbType.Xml).Value = InitialMeters;
          _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;

          _rows_affected = _cmd.ExecuteNonQuery();
        }

        return (_rows_affected == 1);
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Update MC_REFILLED_HOPPER on MoneyCollection with he value passed like a parameter cheking the status
    //
    //  PARAMS:
    //      - INPUT:
    //        - MoneyCollectionId
    //        - ValidStatus
    //        - DenominationsRefill
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean DB_UpdateRefillInHopperOnMoneyCollection(Int64 MoneyCollectionId, TITO_MONEY_COLLECTION_STATUS ValidStatus, TerminalDenominations DenominationsRefill, SqlTransaction Trx)
    {
      Int32 _rows_affected;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   MONEY_COLLECTIONS                              ");
        _sb.AppendLine("     SET   MC_REFILLED_HOPPER = @pDenominationsRefilled   ");
        _sb.AppendLine("   WHERE   MC_COLLECTION_ID = @pMoneyCollectionId              ");
        _sb.AppendLine("     AND   MC_STATUS = @pValidStatus                      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDenominationsRefilled", SqlDbType.Xml).Value = XMLList.ListToXML(DenominationsRefill.DenominationsWithQuantity);
          _cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;
          _cmd.Parameters.Add("@pValidStatus", SqlDbType.Int).Value = ValidStatus;

          _rows_affected = _cmd.ExecuteNonQuery();
          return (_rows_affected == 1);
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    #endregion

    #region Private Methods

    private int GetExpectedQuantityFromCashierTerminalMoney(Decimal Denomination, CageCurrencyType CageCurrencyType, List<CashierTerminalMoneyInfo> CashierTerminalMoneyList)
    {
      foreach (CashierTerminalMoneyInfo _item in CashierTerminalMoneyList)
      {
        if (_item.Denomination == Denomination) // TODO: The current table (CASHIER_TERMINAL_MONEY) doesn't have a CageCurrencyType field.
          return _item.Quantity;
      }

      return 0;
    }

    private void UpdateCancelFromMoneyCollection(ref StringBuilder Sb)
    {
      Sb.AppendLine(" UPDATE   MONEY_COLLECTIONS ");
      Sb.AppendLine("    SET   MC_USER_ID = NULL ,");
      Sb.AppendLine("          MC_COLLECTION_DATETIME = NULL ,");
      Sb.AppendLine("          MC_STATUS = @pMcStatus ,");
      Sb.AppendLine("          MC_COLLECTED_BILL_AMOUNT = 0,");
      Sb.AppendLine("          MC_COLLECTED_BILL_COUNT = 0,");
      Sb.AppendLine("          MC_COLLECTED_TICKET_AMOUNT = @pCollectedTicketAmount,");
      Sb.AppendLine("          MC_COLLECTED_TICKET_COUNT = @pCollectedTicketCount,");
      Sb.AppendLine("          MC_COLLECTED_RE_TICKET_AMOUNT = @pCollectedReTicketAmount,");
      Sb.AppendLine("          MC_COLLECTED_RE_TICKET_COUNT = @pCollectedReTicketCount,");
      Sb.AppendLine("          MC_COLLECTED_PROMO_RE_TICKET_AMOUNT = @pCollectedPromoReTicketAmount,");
      Sb.AppendLine("          MC_COLLECTED_PROMO_RE_TICKET_COUNT = @pCollectedPromoReTicketCount,");
      Sb.AppendLine("          MC_COLLECTED_PROMO_NR_TICKET_AMOUNT = @pCollectedPromoNrTicketAmount,");
      Sb.AppendLine("          MC_COLLECTED_PROMO_NR_TICKET_COUNT = @pCollectedPromoNrTicketCount");
      Sb.AppendLine("  WHERE   MC_COLLECTION_ID = @pMoneyCollectionId ;");
    }

    private Boolean RollbackCashierSessionInfo(Int64 TerminalCashierSessionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Currency _balance;

      _balance = Currency.Zero();
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("DECLARE @pMB_CASH_MovId AS BIGINT                         ");
        _sb.AppendLine("DECLARE @pCLOSE_MovId AS BIGINT                           ");
        _sb.AppendLine("                                                          ");
        _sb.AppendLine("SELECT   @pMB_CASH_MovId = MAX(CM_MOVEMENT_ID)            ");
        _sb.AppendLine("  FROM   CASHIER_MOVEMENTS                                ");
        _sb.AppendLine(" WHERE   CM_SESSION_ID = @pCashierSessionId               ");
        _sb.AppendLine("   AND   CM_TYPE = @MB_CLOSE_TYPE                         ");
        _sb.AppendLine("SELECT   @pCLOSE_MovId = MAX(CM_MOVEMENT_ID)              ");
        _sb.AppendLine("  FROM   CASHIER_MOVEMENTS                                ");
        _sb.AppendLine(" WHERE   CM_SESSION_ID = @pCashierSessionId               ");
        _sb.AppendLine("   AND   CM_TYPE = @CS_CLOSE_TYPE                         ");
        _sb.AppendLine("                                                          ");
        _sb.AppendLine("UPDATE   CASHIER_MOVEMENTS                                ");
        _sb.AppendLine("   SET   CM_UNDO_STATUS = @pUndoStatus                    ");
        _sb.AppendLine(" WHERE   CM_SESSION_ID = @pCashierSessionId               ");
        _sb.AppendLine("   AND   CM_MOVEMENT_ID IN (@pMB_CASH_MovId,@pCLOSE_MovId)");

        _sb.AppendLine(" UPDATE   CASHIER_SESSIONS ");
        _sb.AppendLine("    SET   CS_CLOSING_DATE = NULL ");
        _sb.AppendLine("        , CS_STATUS = @pPendingClosing ");
        _sb.AppendLine("        , CS_COLLECTED_AMOUNT = 0");
        _sb.AppendLine("  WHERE   CS_SESSION_ID = @pCashierSessionId ;");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {

          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = TerminalCashierSessionId;
          _cmd.Parameters.Add("@pDepositCash", SqlDbType.Int).Value = MBMovementType.DepositCash;
          _cmd.Parameters.Add("@pPendingClosing", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING;
          _cmd.Parameters.Add("@pUndoStatus", SqlDbType.Int).Value = UndoStatus.Undone;
          _cmd.Parameters.Add("@CS_CLOSE_TYPE", SqlDbType.Int).Value = CASHIER_MOVEMENT.CLOSE_SESSION;
          _cmd.Parameters.Add("@MB_CLOSE_TYPE", SqlDbType.Int).Value = CASHIER_MOVEMENT.MB_CASH_IN;

          if (_cmd.ExecuteNonQuery() == 0)
          {
            return false;
          }
        }


        //TODO: Pendiente de respuesta de DAVID y RAUL


        //if (m_type_new_collection.real_bills_sum > 0)
        //{
        //  if (!NACardCashDeposit(CashierSessionInfo.CashierSessionId, CashierSessionInfo.UserType
        //                                  , Decimal.Negate(m_type_new_collection.real_bills_sum)
        //                                  , m_type_new_collection.terminal_id, SqlTrx))
        //  {
        //    return false;
        //  }
        //}

        //if (!CloseSessionTITO(SqlTrx))
        //  return false; 
       
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return true;
    }
    #endregion
  }

  public class MoneyCollectionInfo
  {
    #region Properties
    public Int64 CollectionId { get; set; }
    public Int32? TerminalId { get; set; }
    public Int32? UserlId { get; set; }
    public Int64? CashierSessionId { get; set; }
    public DateTime? Collection { get; set; }
    public DateTime? Extraction { get; set; }
    public TITO_MONEY_COLLECTION_STATUS? Status { get; set; }
    public Decimal ExpectedBillAmount { get; set; }
    public Int32 ExpectedBillCount { get; set; }
    public Decimal ExpectedCoinAmount { get; set; }
    public Int32 ExpectedCoinCount { get; set; }
    public Decimal? OutBills { get; set; }
    public Decimal? OutCoins { get; set; }
    public Decimal? OutCents { get; set; }
    public String InitialMeters { get; set; }
    public String FinalMeters { get; set; }

    #endregion
  }

  public class MoneyCollectionDetailsInfo
  {
    #region Properties
    public Int64 CollectionId { get; set; }
    public Decimal Face { get; set; }
    public Decimal Expected { get; set; }
    public Decimal Collected { get; set; }
    public CageCurrencyType CageCurrencyType { get; set; }

    #endregion
  }
}
