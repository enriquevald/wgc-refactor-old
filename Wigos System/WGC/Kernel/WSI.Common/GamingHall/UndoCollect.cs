﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: UndoCollect.cs
// 
//   DESCRIPTION: Class that implements methods to cancel a Collect in Gaming Hall.
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 09-FEB-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 FAV    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingHall
{
  public class UndoCollect
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Gets a object with information about Session Id and Operation generated in a operation of collected
    //
    //  PARAMS:
    //      - INPUT:
    //        - MoneyCollectionId
    //      - OUTPUT:
    //        - SessionOperationCollectInfo   
    //
    // RETURNS :
    //      - True / False
    //
    //   NOTES:
    //
    public bool GetSessionAndOperationsCollected(Int64 MoneyCollectionId, out SessionOperationCollectInfo SessionOperationCollectInfo, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _counter = 0;
      CashierSessionInfo _cs_cashier;
      CashierSessionInfo _cs_terminal_collect;
      CashierSessionInfo _cs_terminal_refill;
      Int64 _cs_Id_cashier = 0;
      Int64 _cs_Id_terminal_collect = 0;
      Int64 _cs_Id_terminal_refill = 0;

      SessionOperationCollectInfo = new SessionOperationCollectInfo();

      try
      {
        _sb = new StringBuilder();  
        _sb.AppendLine("SELECT    CM_SESSION_ID                           ");
        _sb.AppendLine("		    , CM_OPERATION_ID                         ");
        _sb.AppendLine("		    , CM_TYPE                                 ");
        _sb.AppendLine("  FROM    CASHIER_MOVEMENTS                       ");
        _sb.AppendLine(" WHERE    CM_RELATED_ID = @pMoneyCollectionId     ");
        _sb.AppendLine("   AND    CM_TYPE IN (@p1, @p2, @p3)              ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;
          _cmd.Parameters.Add("@p1", SqlDbType.Int).Value = (int)CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE;  //Cashier
          _cmd.Parameters.Add("@p2", SqlDbType.Int).Value = (int)CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER; // Terminal refill
          _cmd.Parameters.Add("@p3", SqlDbType.Int).Value = (int)CASHIER_MOVEMENT.MB_CASH_IN;   // Terminal collect

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              if ((CASHIER_MOVEMENT)_reader[2] == CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE)
              {
                _cs_Id_cashier = (Int64)_reader[0];
                SessionOperationCollectInfo.OperationIdCashier = (Int64)_reader[1];
                _counter += 1;
              }
              else if ((CASHIER_MOVEMENT)_reader[2] == CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER)
              {
                _cs_Id_terminal_refill = (Int64)_reader[0];
                SessionOperationCollectInfo.OperationIdTerminalRefill = (Int64)_reader[1];
                _counter += 1;
              }
              else if ((CASHIER_MOVEMENT)_reader[2] == CASHIER_MOVEMENT.MB_CASH_IN)
              {
                _cs_Id_terminal_collect = (Int64)_reader[0];
                SessionOperationCollectInfo.OperationIdTerminalCollect = (Int64)_reader[1];
                _counter += 1;
              }
            }

          }
        }

        if (!Cashier.GetCashierSessionById(_cs_Id_cashier, out _cs_cashier, Trx)
             || !Cashier.GetCashierSessionById(_cs_Id_terminal_refill, out _cs_terminal_refill, Trx)
             || !Cashier.GetCashierSessionById(_cs_Id_terminal_collect, out _cs_terminal_collect, Trx)
             || _counter != 3) // Should load three values
        {
          return false;
        }

        SessionOperationCollectInfo.CashierSessionCashier = _cs_cashier;
        SessionOperationCollectInfo.CashierSessionTerminalRefill = _cs_terminal_refill;
        SessionOperationCollectInfo.CashierSessionTerminalCollect = _cs_terminal_collect;
        return true;
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    
    }
  }


  public class SessionOperationCollectInfo
  {
    public CashierSessionInfo CashierSessionTerminalCollect { get; set; }

    public CashierSessionInfo CashierSessionTerminalRefill { get; set; }

    public CashierSessionInfo CashierSessionCashier { get; set; }

    public Int64 OperationIdTerminalCollect { get; set; }

    public Int64 OperationIdTerminalRefill { get; set; }

    public Int64 OperationIdCashier { get; set; }
  }

}
