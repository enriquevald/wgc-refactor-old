﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalDenominations.cs
// 
//   DESCRIPTION: Class with denominations and its quantity found in a Box or Hopper for GamingHall (Collection or replacement)
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 FAV    First release.
// 01-MAR-2016 DDS    Fixed Bug 10135: DropBox value is -100 when Cage.ShowDenominations = 0
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingHall
{
  public class TerminalDenominations: ICloneable
  {
    #region Attributes
    private TerminalBoxType m_terminal_box_type;
    private String m_iso_code;
    private CurrencyExchangeType m_currency_exchange_type;
    #endregion

    #region Constructor
    public TerminalDenominations(TerminalBoxType TerminalBoxType, String IsoCode, CurrencyExchangeType CurrencyExchangeType)
    {
      m_terminal_box_type = TerminalBoxType;
      m_iso_code = IsoCode;
      m_currency_exchange_type = CurrencyExchangeType;
    }

    #endregion

    #region Properties
    public List<TerminalCurrencyItem> Denominations { get; set; }

    public List<TerminalCurrencyItem> DenominationsWithQuantity
    {
      get
      {
        List<TerminalCurrencyItem> _list = new List<TerminalCurrencyItem>();
        foreach (TerminalCurrencyItem _item in this.Denominations ?? new List<TerminalCurrencyItem>())
        {
          if (_item.Quantity != null)
            _list.Add(_item);
        }

        return _list;
      }
    }

    public CurrencyIsoType CurrencyIsoType
    {
      get
      {
        CurrencyIsoType _cur_iso_type = new CurrencyIsoType();
        _cur_iso_type.IsoCode = m_iso_code;
        _cur_iso_type.Type = m_currency_exchange_type;

        return _cur_iso_type;
      }
    }

    public String IsoCode 
    {
      get
      {
        return m_iso_code;
      }
    }

    public Decimal Total
    {
      get
      {
        decimal _total = 0;
        foreach (TerminalCurrencyItem _item in this.DenominationsWithQuantity)
          _total += _item.Total;

        return _total;
      }
    }

    public Int32 Count
    {
      get
      {
        int _count = 0;
        foreach (TerminalCurrencyItem _item in this.DenominationsWithQuantity)
          _count += _item.Quantity.Value;

        return _count;
      }
    }

    public Currency TotalCurrency
    {
      get
      {
        Currency _currency = (Currency)this.Total;
        _currency.CurrencyIsoCode = m_iso_code;
        return _currency;
      }
    }

    public TerminalBoxType TerminalBoxType
    {
      get
      {
        return m_terminal_box_type;
      }
    }

    public Boolean IsEmpty
    {
      get
      {
        return (DenominationsWithQuantity.Count == 0); 
      }
    }
    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Gets Total (Quantity x Denomination) by CageCurrencyType
    //
    //  PARAMS:
    //      - INPUT:
    //        - CageCurrencyType
    //      - OUTPUT:
    //
    // RETURNS :
    //      - Total
    //
    //   NOTES:
    //
    public decimal GetTotalByType(CageCurrencyType CageCurrencyType)
    {
      decimal _total = 0;
      foreach (TerminalCurrencyItem _item in this.DenominationsWithQuantity)
      {
        if (_item.CageCurrencyType == CageCurrencyType)
          _total += _item.Total;
      }

      return _total;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Gets Count (Quantity) by CageCurrencyType
    //
    //  PARAMS:
    //      - INPUT:
    //        - CageCurrencyType
    //      - OUTPUT:
    //
    // RETURNS :
    //      - Quantity
    //
    //   NOTES:
    //
    public Int32 GetCountByType(CageCurrencyType CageCurrencyType)
    {
      int _count = 0;
      foreach (TerminalCurrencyItem _item in this.DenominationsWithQuantity)
      {
        if (_item.CageCurrencyType == CageCurrencyType)
          _count += _item.Quantity.Value;
      }

      return _count;
    }
    
    public object Clone()
    {
      return this.MemberwiseClone();
    }

    public void LoadDenominationsCollected(Int64 Money_collection_Id)
    {
      TerminalDenominationsDAO _dao;
      _dao = new TerminalDenominationsDAO();

      this.Denominations = _dao.GetDenominationsCollected(Money_collection_Id);
      
    }

    #endregion

  }
  /// <summary>
  /// Internal data access class: TerminalDenominationsDAO
  /// </summary>
  internal class TerminalDenominationsDAO
  {
    public List<TerminalCurrencyItem> GetDenominationsCollected(Int64? Money_collection_Id)
    {
      List<TerminalCurrencyItem> _denominations;
      DataTable _dt;
      TerminalCurrencyItem _item;
      StringBuilder _sb;
      String _isoCode;
        Boolean _show_denom;
      Decimal _quantity; 

      _denominations = new List<TerminalCurrencyItem>();
      _dt = new DataTable();

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT      MCD_FACE_VALUE, MCD_NUM_COLLECTED");
      _sb.AppendLine(" FROM       MONEY_COLLECTION_DETAILS");
      _sb.AppendLine("WHERE       MCD_COLLECTION_ID = @pMoneyCollection");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pMoneyCollection", SqlDbType.Int).Value = Money_collection_Id;
                   
            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {                       
              _isoCode = CurrencyExchange.GetNationalCurrency();
              _show_denom = GeneralParam.GetInt32("Cage", "ShowDenominations",
                   (Int32)WSI.Common.Cage.ShowDenominationsMode.ShowAllDenominations) == (Int32)WSI.Common.Cage.ShowDenominationsMode.ShowAllDenominations;
              while (_reader.Read())
              {
                _item = new TerminalCurrencyItem(_isoCode, CurrencyExchangeType.CURRENCY,_show_denom );
                _item.Denomination = (Decimal)_reader[0];
                _quantity = (Decimal)_reader[1];
                if (_show_denom)
                    _item.Quantity = (int?)_quantity;
                else
                    _item.Total = _quantity;
                _item.TerminalCashDirection = TerminalCashDirection.In;
                
                _denominations.Add(_item);
              }               
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return _denominations;
    }
  }
}
