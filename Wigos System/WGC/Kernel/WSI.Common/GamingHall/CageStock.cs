﻿//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CageStock.cs
//  
//   DESCRIPTION: Implements the class CageStock
// 
//        AUTHOR: ----
// 
// CREATION DATE: ----
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// ---         ---        Inital Draft.
// 05-APR-2016 DHA        Product Backlog Item 9756: added chips types
// 20-SEP-2016 FJC        Bug 17669:Anular retiro de caja desde bóveda/Cajero: no deshace el stock y conceptos de bóveda
// 04-OCT-2016 FJC        Fixed Bug 17669 (reopened): Anular retiro de caja desde bóveda/Cajero: no deshace el stock y conceptos de bóveda

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingHall
{
  public class CageStock
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Check if has enough cage stock.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: true = Has enough stock.
    //
    //   NOTES :
    //  
    public bool CheckAndUpdateCageStock(TerminalDenominations TerminalDenominations, CASHIER_MOVEMENT MovementType, SqlTransaction Trx)
    {
      StringBuilder _sb_cage_stock;
      String _currencies_used;
      DataTable _dt_cage_stock;
      Boolean _has_enough_stock;
      Boolean _matches_found;
      String _cur_amount_iso_code;
      Decimal _cur_amount_denomination;
      Int64 _cur_amount_chip_id;
      CageCurrencyType _cage_currency_type;
      Decimal _cur_amount_quantity;
      Decimal _quantity_to_update;
      Decimal _denomination_to_update;
      CageCurrencyType _cage_currency_type_to_update;
      String _cur_stock_iso_code;
      Decimal _cur_stock_denomination;
      Int64 _cur_stock_chip_id;
      CageCurrencyType _cage_currency_type_stock;
      Decimal _cur_stock_quantity;
      StringBuilder _sb;
      DataTable _dt_currencies;
      DataTable _dt_chips;
      DataRow _dr;
      StringBuilder _sb_update;
      StringBuilder _sb_insert;


      _currencies_used = "";
      _sb = new StringBuilder();

      // Tables for batch update
      _dt_currencies = new DataTable();
      _dt_chips = new DataTable();

      _dt_currencies.Columns.Add("CGS_ISO_CODE", Type.GetType("System.String"));
      _dt_currencies.Columns.Add("CGS_DENOMINATION", Type.GetType("System.Decimal"));
      _dt_currencies.Columns.Add("CGS_CURRENCY_TYPE", Type.GetType("System.Decimal"));
      _dt_currencies.Columns.Add("CGS_QUANTITY", Type.GetType("System.Decimal"));
      _dt_currencies.Columns.Add("ROW_ACTION", Type.GetType("System.Int16"));

      _dt_chips.Columns.Add("CHSK_CHIP_ID", Type.GetType("System.Int64"));
      _dt_chips.Columns.Add("CHSK_QUANTITY", Type.GetType("System.Int32"));
      _dt_chips.Columns.Add("ROW_ACTION", Type.GetType("System.Int16"));


      foreach (TerminalCurrencyItem _cur_amount in TerminalDenominations.DenominationsWithQuantity)
      {
        if (_cur_amount.Quantity > 0)
        {
          if (!String.IsNullOrEmpty(_currencies_used))
          {
            _currencies_used += ", ";
          }
          _currencies_used += "'" + ((_cur_amount.IsoCode.Length > Cage.MAX_ISO_CODE_DIGITS) ? _cur_amount.IsoCode.Substring(0, Cage.MAX_ISO_CODE_DIGITS) : _cur_amount.IsoCode) + "'";
        }
      }
      if (String.IsNullOrEmpty(_currencies_used))
      {
        _currencies_used = "''";
      }
      _sb_cage_stock = new StringBuilder();
      _sb_cage_stock.AppendLine(" SELECT   CGS_ISO_CODE  AS ISO_CODE  ");
      _sb_cage_stock.AppendLine("		     , CGS_DENOMINATION  AS DENOMINATION  ");
      _sb_cage_stock.AppendLine("		     , CGS_CAGE_CURRENCY_TYPE  AS CURRENCY_TYPE  ");
      _sb_cage_stock.AppendLine("		     , CGS_QUANTITY  AS QUANTITY  ");
      _sb_cage_stock.AppendLine("		     , 0 AS CHIP_ID  ");
      _sb_cage_stock.AppendLine("		FROM   CAGE_STOCK ");
      _sb_cage_stock.AppendLine("  WHERE	 CGS_ISO_CODE IN (" + _currencies_used + ") ");
      _sb_cage_stock.AppendLine("  UNION  ");
      _sb_cage_stock.AppendLine(" SELECT   CH_ISO_CODE AS ISO_CODE ");
      _sb_cage_stock.AppendLine("        , CH_DENOMINATION  AS DENOMINATION   ");
      _sb_cage_stock.AppendLine("		     , CH_CHIP_TYPE AS CURRENCY_TYPE  ");// is the same value CageCurrencyType and ChipsType (Only for chips)
      _sb_cage_stock.AppendLine("        , CHSK_QUANTITY  AS QUANTITY   ");
      _sb_cage_stock.AppendLine("		     , CH_CHIP_ID AS CHIP_ID  ");
      _sb_cage_stock.AppendLine("   FROM   CHIPS_STOCK ");
      _sb_cage_stock.AppendLine("  INNER   JOIN CHIPS  ON CHSK_CHIP_ID = CH_CHIP_ID");
      _sb_cage_stock.AppendLine("  WHERE	 CH_ISO_CODE IN (" + _currencies_used + ") ");

      _dt_cage_stock = new DataTable();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb_cage_stock.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_dt_cage_stock);
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        // Not connection timeout
        if (_sql_ex.Number != -2)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      if (MovementType == CASHIER_MOVEMENT.FILLER_IN && _dt_cage_stock.Rows.Count == 0)
      {
        return false;
      }

      foreach (TerminalCurrencyItem _cur_amount in TerminalDenominations.DenominationsWithQuantity)
      {
        _cur_amount_quantity = 0;
        _cage_currency_type_stock = CageCurrencyType.Bill;

        if (_cur_amount.Total > 0)
        {
          DataRow[] _rows_cage_stock = _dt_cage_stock.Select(String.Format("ISO_CODE = {0}", "'" + ((_cur_amount.IsoCode.Length > Cage.MAX_ISO_CODE_DIGITS) ? _cur_amount.IsoCode.Substring(0, Cage.MAX_ISO_CODE_DIGITS) : _cur_amount.IsoCode) + "'"));
          if (MovementType == CASHIER_MOVEMENT.FILLER_IN && _rows_cage_stock.Length == 0)
          {
            return false;
          }

          _has_enough_stock = false;
          _matches_found = false;
          _cur_amount_chip_id = 0;

          // IsoCode      (CAA_ISO_CODE)
          _cur_amount_iso_code = _cur_amount.IsoCode;
          _cur_amount_iso_code = (_cur_amount_iso_code.Length > Cage.MAX_ISO_CODE_DIGITS) ? _cur_amount_iso_code.Substring(0, Cage.MAX_ISO_CODE_DIGITS) : _cur_amount_iso_code;
          // Denomination (CAA_DENOMINATION)
          _cur_amount_denomination = Format.ParseCurrency(_cur_amount.Denomination.ToString());
          // Denomination (CAA_CURRENCY_tYPE)
          _cage_currency_type = CageCurrencyType.Bill; //default

          // DHA 04-MAY-2016: pending to add chips in a future for Gaming Halls
          //if (_cur_amount.["CHIP_ID"] != DBNull.Value)
          //{
          //   DHA REVISAR
          //  _cur_amount_chip_id = (Int64)_row_cur_amount["CHIP_ID"];
          //}

          if (_cur_amount.CageCurrencyType == CageCurrencyType.Coin)
          {
            _cage_currency_type = CageCurrencyType.Coin;
          }

          if (FeatureChips.IsChipsType(_cur_amount.CurrencyIsoType.Type))
          {
            _cage_currency_type = FeatureChips.ConvertToCageCurrencyType(_cur_amount.CurrencyIsoType.Type);
          }

          // Quantity     (CAA_UN / CAA_TOTAL)
          if (_cur_amount_denomination >= 0 || FeatureChips.IsChipsType(_cage_currency_type))
          {
            _cur_amount_quantity = Format.ParseCurrency(_cur_amount.Quantity.ToString());
          }
          else
          {
            _cur_amount_quantity = Format.ParseCurrency(_cur_amount.Total.ToString());
          }

          if (_cur_amount_quantity == 0)
          {
            continue;
          }

          _quantity_to_update = _cur_amount_quantity;
          _denomination_to_update = _cur_amount_denomination;
          _cage_currency_type_to_update = _cage_currency_type;
          foreach (DataRow _row_cage_stock in _rows_cage_stock)
          {
            // IsoCode      (CGS_ISO_CODE)
            _cur_stock_iso_code = _row_cage_stock["ISO_CODE"].ToString();

            // Denomination (CGS_DENOMINATION)
            _cur_stock_denomination = Format.ParseCurrency(_row_cage_stock["DENOMINATION"].ToString());
            _cur_stock_chip_id = (Int64)_row_cage_stock["CHIP_ID"];

            // Denomination (CGS_CURRENCY_TYPE)

            _cage_currency_type_stock = (CageCurrencyType)_row_cage_stock["CURRENCY_TYPE"];

            // Quantity     (CGS_QUANTITY)
            _cur_stock_quantity = Format.ParseCurrency(_row_cage_stock["QUANTITY"].ToString());

            if (_cur_amount_iso_code == _cur_stock_iso_code &&
                _cage_currency_type == _cage_currency_type_stock
                )
            {
              if (_cur_amount_denomination == _cur_stock_denomination && !FeatureChips.IsChipsType(_cage_currency_type_stock)
                   || _cur_amount_chip_id == _cur_stock_chip_id && FeatureChips.IsChipsType(_cage_currency_type_stock))
              {
                _matches_found = true;
                if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
                {
                  if (_cur_amount_quantity <= _cur_stock_quantity)
                  {
                    _quantity_to_update = _cur_stock_quantity - _cur_amount_quantity;
                    //_cur_stock_cuantity -= _cur_amount_cuantity;
                    _has_enough_stock = true;
                  }
                }
                else
                {
                  _quantity_to_update = _cur_amount_quantity + _cur_stock_quantity;
                  //_cur_amount_cuantity += _cur_stock_cuantity;
                }

                // Update data
                if (!FeatureChips.IsChipsType(_cage_currency_type_to_update))
                {
                  _dr = _dt_currencies.NewRow();
                  _dr["CGS_ISO_CODE"] = _cur_amount_iso_code;
                  _dr["CGS_DENOMINATION"] = _denomination_to_update;
                  _dr["CGS_CURRENCY_TYPE"] = _cage_currency_type_to_update;
                  _dr["CGS_QUANTITY"] = _quantity_to_update;
                  _dr["ROW_ACTION"] = 1;

                  _dt_currencies.Rows.Add(_dr);
                }
                else
                {
                  _dr = _dt_chips.NewRow();
                  _dr["CHSK_CHIP_ID"] = _cur_amount_chip_id;
                  _dr["CHSK_QUANTITY"] = _quantity_to_update;
                  _dr["ROW_ACTION"] = 1;

                  _dt_chips.Rows.Add(_dr);
                }

                break;
              }
            }
          } //foreach 

          if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
          {
            if (!_matches_found || !_has_enough_stock)
            {
              return false;
            }
          }
          else
          {
            if (!_matches_found)

              // Insert data
              //DLL11111
              if (!FeatureChips.IsChipsType(_cage_currency_type))
              {
                _dr = _dt_currencies.NewRow();
                _dr["CGS_ISO_CODE"] = _cur_amount_iso_code;
                _dr["CGS_DENOMINATION"] = _denomination_to_update;
                _dr["CGS_CURRENCY_TYPE"] = _cage_currency_type_to_update;
                _dr["CGS_QUANTITY"] = _quantity_to_update;
                _dr["ROW_ACTION"] = 2;

                _dt_currencies.Rows.Add(_dr);
              }
              else
              {
                _dr = _dt_chips.NewRow();
                _dr["CHSK_CHIP_ID"] = _denomination_to_update;
                _dr["CHSK_QUANTITY"] = _quantity_to_update;
                _dr["ROW_ACTION"] = 2;

                _dt_chips.Rows.Add(_dr);

              }

          }
        }
      }

      try
      {

        // Update Chips datatable
        if (_dt_chips.Rows.Count > 0)
        {
          _dt_chips.AcceptChanges();

          foreach (DataRow row in _dt_chips.Rows)
          {
            switch ((Int16)row["ROW_ACTION"])
            {
              case 1:
                row.SetModified();
                break;

              case 2:
                row.SetAdded();
                break;
            }
          }

          _sb_update = new StringBuilder();
          _sb_insert = new StringBuilder();
          // JCA & DDM 21-OCT-2014: Error in SQL 2005. Not allowed subquerys in insert
          _sb_insert.AppendLine("  INSERT INTO   CHIPS_STOCK    ");
          _sb_insert.AppendLine("              ( CHSK_CHIP_ID   ");
          _sb_insert.AppendLine("              , CHSK_QUANTITY  ");
          _sb_insert.AppendLine("              )                ");
          _sb_insert.AppendLine("       VALUES                  ");
          _sb_insert.AppendLine("              (  @pChipId       ");
          _sb_insert.AppendLine("              ,  @pQuantity    ");
          _sb_insert.AppendLine("              )                ");

          _sb_update.AppendLine("  UPDATE  CHIPS_STOCK  ");
          _sb_update.AppendLine("     SET  CHSK_QUANTITY  = @pQuantity");
          _sb_update.AppendLine("   WHERE  CHSK_CHIP_ID   = @pChipId ");

          using (SqlCommand _sql_cmd_insert = new SqlCommand(_sb_insert.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd_insert.Parameters.Add("@pQuantity", SqlDbType.Int).SourceColumn = "CHSK_QUANTITY";
            _sql_cmd_insert.Parameters.Add("@pChipId", SqlDbType.Decimal).SourceColumn = "CHSK_CHIP_ID";

            using (SqlCommand _sql_cmd_update = new SqlCommand(_sb_update.ToString(), Trx.Connection, Trx))
            {
              _sql_cmd_update.Parameters.Add("@pQuantity", SqlDbType.Int).SourceColumn = "CHSK_QUANTITY";
              _sql_cmd_update.Parameters.Add("@pChipId", SqlDbType.BigInt).SourceColumn = "CHSK_CHIP_ID";

              // Batch Update
              using (SqlDataAdapter _da = new SqlDataAdapter())
              {
                _da.InsertCommand = _sql_cmd_insert;
                _da.UpdateCommand = _sql_cmd_update;
                _da.UpdateBatchSize = 500;
                _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

                if (_da.Update(_dt_chips) != _dt_chips.Rows.Count)
                {
                  // Not all data has been inserted
                  Log.Message("uc_bank.CheckCageStock Not all data has been updated");

                  return false;
                }

              } //SqlDataAdapter
            }
          }
        }

        // Update Currencies datatable
        if (_dt_currencies.Rows.Count > 0)
        {

          _dt_currencies.AcceptChanges();

          foreach (DataRow row in _dt_currencies.Rows)
          {
            switch ((Int16)row["ROW_ACTION"])
            {
              case 1:
                row.SetModified();
                break;

              case 2:
                row.SetAdded();
                break;
            }
          }

          _sb_update = new StringBuilder();
          _sb_insert = new StringBuilder();

          _sb_insert.AppendLine(" INSERT INTO   CAGE_STOCK        ");
          _sb_insert.AppendLine("             (                   ");
          _sb_insert.AppendLine("               CGS_QUANTITY      ");
          _sb_insert.AppendLine("            ,  CGS_ISO_CODE      ");
          _sb_insert.AppendLine("            ,  CGS_DENOMINATION  ");
          _sb_insert.AppendLine("            ,  CGS_CAGE_CURRENCY_TYPE  ");

          _sb_insert.AppendLine("             )                   ");
          _sb_insert.AppendLine("      VALUES (                   ");
          _sb_insert.AppendLine("               @pQuantity        ");
          _sb_insert.AppendLine("             , @pIsoCode         ");
          _sb_insert.AppendLine("             , @pDenomination    ");
          _sb_insert.AppendLine("             , @pCurrencyType    ");
          _sb_insert.AppendLine("             )                   ");

          _sb_update.AppendLine("  UPDATE  CAGE_STOCK          ");
          _sb_update.AppendLine("     SET  CGS_QUANTITY =   @pQuantity   ");
          _sb_update.AppendLine("   WHERE  CGS_ISO_CODE =   @pIsoCode ");
          _sb_update.AppendLine("     AND	 CGS_DENOMINATION =  @pDenomination ");
          _sb_update.AppendLine("     AND	 CGS_CAGE_CURRENCY_TYPE =  @pCurrencyType ");

          using (SqlCommand _sql_cmd_insert = new SqlCommand(_sb_insert.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd_insert.Parameters.Add("@pQuantity", SqlDbType.Decimal).SourceColumn = "CGS_QUANTITY";
            _sql_cmd_insert.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "CGS_DENOMINATION";
            _sql_cmd_insert.Parameters.Add("@pCurrencyType", SqlDbType.Int).SourceColumn = "CGS_CURRENCY_TYPE";
            _sql_cmd_insert.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).SourceColumn = "CGS_ISO_CODE";

            using (SqlCommand _sql_cmd_update = new SqlCommand(_sb_update.ToString(), Trx.Connection, Trx))
            {
              _sql_cmd_update.Parameters.Add("@pQuantity", SqlDbType.Decimal).SourceColumn = "CGS_QUANTITY";
              _sql_cmd_update.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "CGS_DENOMINATION";
              _sql_cmd_update.Parameters.Add("@pCurrencyType", SqlDbType.Int).SourceColumn = "CGS_CURRENCY_TYPE";
              _sql_cmd_update.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).SourceColumn = "CGS_ISO_CODE";

              // Batch Update
              using (SqlDataAdapter _da = new SqlDataAdapter())
              {
                _da.InsertCommand = _sql_cmd_insert;
                _da.UpdateCommand = _sql_cmd_update;
                _da.UpdateBatchSize = 500;
                _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

                if (_da.Update(_dt_currencies) != _dt_currencies.Rows.Count)
                {
                  // Not all data has been inserted
                  Log.Message("uc_bank.CheckCageStock Not all data has been updated");

                  return false;
                }
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }

      return false;

    } // CheckCageStock

    /// <summary>
    /// Get SQL Command when updating cagestock
    /// </summary>
    /// <param name="FeatureChips_IsCurrencyExchangeTypeChips"></param>
    /// <param name="CurDem"></param>
    /// <param name="OperationType"></param>
    /// <param name="IsCancellMovement"></param>
    /// <returns></returns>
    public static String GetSqlCommandUpdateCageStock(Boolean FeatureChips_IsCurrencyExchangeTypeChips,
                                                      Decimal CurDem,
                                                      Cage.CageOperationType OperationType,
                                                      Boolean IsCancellMovement)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        if (!FeatureChips_IsCurrencyExchangeTypeChips)
        {
          _sb.AppendLine(" IF EXISTS ( SELECT   1                                                 ");
          _sb.AppendLine("               FROM   CAGE_STOCK                                        ");
          _sb.AppendLine("              WHERE   CGS_ISO_CODE       = @pIsoCode                    ");
          _sb.AppendLine("                AND   CGS_DENOMINATION       = @pDenomination           ");
          _sb.AppendLine("                AND   CGS_CAGE_CURRENCY_TYPE = @pCurrencyType )         ");
          _sb.AppendLine("  UPDATE   CAGE_STOCK                                                   ");
          if (OperationType == Cage.CageOperationType.CountCage)
          {
            _sb.AppendLine("     SET  CGS_QUANTITY        = @pQuantity                            ");
          }
          else
          {
            _sb.AppendLine("     SET  CGS_QUANTITY        = CGS_QUANTITY + @pQuantity             ");
          }
          _sb.AppendLine("   WHERE  CGS_ISO_CODE            = @pIsoCode                           ");
          _sb.AppendLine("     AND  CGS_DENOMINATION        = @pDenomination                      ");
          _sb.AppendLine("     AND  CGS_CAGE_CURRENCY_TYPE  = @pCurrencyType                      ");
          if (OperationType == Cage.CageOperationType.CountCage && !IsCancellMovement)
          {
            _sb.AppendLine("   AND  CGS_QUANTITY  = @pCashierQuantity                             ");
          }
          _sb.AppendLine("  ELSE                                                                  ");
          _sb.AppendLine("     INSERT INTO   CAGE_STOCK                                           ");
          _sb.AppendLine("                 ( CGS_ISO_CODE                                         ");
          _sb.AppendLine("                 , CGS_DENOMINATION                                     ");
          _sb.AppendLine("                 , CGS_QUANTITY                                         ");
          _sb.AppendLine("                 , CGS_CAGE_CURRENCY_TYPE                               ");
          _sb.AppendLine("                 )                                                      ");
          _sb.AppendLine("          VALUES                                                        ");
          _sb.AppendLine("                 ( @pIsoCode                                            ");
          _sb.AppendLine("                 , @pDenomination                                       ");
          _sb.AppendLine("                 , @pQuantity                                           ");
          _sb.AppendLine("                 , @pCurrencyType                                       ");
          _sb.AppendLine("                 )                                                      ");
        }
        else
        {
          if (CurDem == Cage.COINS_CODE)
          {
            return String.Empty;
          }
          // JCA & DDM 21-OCT-2014: Error in SQL 2005. Not allowed subquerys in insert
          _sb.AppendLine("Declare @ChipId as BigInt");
          _sb.AppendLine("SET @ChipId = isNull(@pChipId, (SELECT TOP 1 CH_CHIP_ID FROM CHIPS      ");
          _sb.AppendLine("                                 WHERE CH_ISO_CODE = @pISOCode          ");
          //_sb.AppendLine("                                   AND CH_CHIP_TYPE = @pISOType ")
          _sb.AppendLine("                                   AND CH_DENOMINATION = @pDenomination ");
          _sb.AppendLine("                                   AND CH_COLOR = @pColor               ");
          _sb.AppendLine("                                   AND CH_NAME = @pName                 ");
          _sb.AppendLine("                                   AND CH_DRAWING = @pDrawing ))        ");

          _sb.AppendLine(" IF EXISTS ( SELECT   1                                                 ");
          _sb.AppendLine("               FROM   CHIPS_STOCK                                       ");
          _sb.AppendLine("              WHERE   CHSK_CHIP_ID   = @ChipId )                        ");
          _sb.AppendLine("  UPDATE   CHIPS_STOCK                                                  ");
          if (OperationType == Cage.CageOperationType.CountCage)
          {
            _sb.AppendLine("     SET  CHSK_QUANTITY     = @pQuantity                              ");
          }
          else
          {
            _sb.AppendLine("     SET  CHSK_QUANTITY     = CHSK_QUANTITY + @pQuantity              ");
          }
          _sb.AppendLine("   WHERE  CHSK_CHIP_ID        = @ChipId                                 ");
          if (OperationType == Cage.CageOperationType.CountCage && !IsCancellMovement)
          {
            _sb.AppendLine("   AND  CHSK_QUANTITY       = @pCashierQuantity                       ");
          }
          _sb.AppendLine("  ELSE                                                                  ");
          _sb.AppendLine("     INSERT INTO   CHIPS_STOCK                                          ");
          _sb.AppendLine("                 ( CHSK_CHIP_ID                                         ");
          _sb.AppendLine("                 , CHSK_QUANTITY                                        ");
          _sb.AppendLine("                 )                                                      ");
          _sb.AppendLine("          VALUES                                                        ");
          _sb.AppendLine("                 ( @ChipId                                              ");
          _sb.AppendLine("                 , @pQuantity                                           ");
          _sb.AppendLine("                 )                                                      ");
        }

        return _sb.ToString();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return String.Empty;
    } // GetCommandUpdateCageStock

    /// <summary>
    /// Updates CageStock when cancel cash withdrawl 
    /// </summary>
    /// <param name="_dt_undo_denominations"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean UpdateUndoCageStock(DataTable TableUndoCurrency, SqlTransaction Trx)
    {
      String _str_query;
      decimal _quantity;

      _str_query = String.Empty;
      _quantity = 0;

      try
      {
        foreach (DataRow _dr in TableUndoCurrency.Select("    CM_CURRENCY_DENOMINATION > 0 " +
                                                         " OR CM_CURRENCY_DENOMINATION =   " + Cage.COINS_CODE +
                                                         " OR CM_CURRENCY_DENOMINATION =   " + Cage.CHECK_CODE))
        {
          _str_query = GetSqlCommandUpdateCageStock(FeatureChips.IsCurrencyExchangeTypeChips((CurrencyExchangeType)_dr[27]),
                                                   (Decimal)_dr[20],
                                                   Cage.CageOperationType.FromCashier,
                                                   true);
          if (!string.IsNullOrEmpty(_str_query))
          {
            using (SqlCommand _cmd = new SqlCommand(_str_query, Trx.Connection, Trx))
            {
              _cmd.Parameters.Add("@pISOCode", SqlDbType.NVarChar, 3).Value = _dr[17].ToString();                                   // CURRENCY_ISO_CODE
              _cmd.Parameters.Add("@pISOType", SqlDbType.Int).Value = _dr[27];                                                      // CURRENCY_TYPE
              _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).Value = _dr[20];                                               // CURRENCY_DENOMINATION
              _cmd.Parameters.Add("@pColor", SqlDbType.Int).Value = DBNull.Value;                                                   // NOT APPLY
              _cmd.Parameters.Add("@pName", SqlDbType.NVarChar, 50).Value = DBNull.Value;                                           // NOT APPLY
              _cmd.Parameters.Add("@pDrawing", SqlDbType.NVarChar, 50).Value = DBNull.Value;                                        // NOT APPLY
              _cmd.Parameters.Add("@pChipId", SqlDbType.BigInt).Value = (_dr[22] != DBNull.Value) ? _dr[22] : DBNull.Value;         // CHIP_ID

              if ((CageCurrencyType)_dr[27] == CageCurrencyType.Others)
              {
                _cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = (Int32)(CageCurrencyType.Bill);
              }
              else
              {
                _cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = _dr[27].ToString();
              }

              // Calculate quantity
              if ((Decimal)_dr[20] == Cage.COINS_CODE || (Decimal)_dr[20] == Cage.CHECK_CODE) // -100 (rest) or -2 (check bank)
              {
                _quantity = (Decimal)_dr[8];
              }
              else                                                                            // Denomination > 0
              {
                _quantity = (Decimal)_dr[8] / (Decimal)_dr[20];
              }

              _quantity = Math.Abs(_quantity) * -1;


              if (FeatureChips.IsCurrencyExchangeTypeChips((CurrencyExchangeType)_dr[27]))
              {
                _cmd.Parameters.Add("@pQuantity", SqlDbType.Money).Value = _quantity;
              }
              else
              {
                _cmd.Parameters.Add("@pQuantity", SqlDbType.Int).Value = (Int32)(_quantity);
              }

              _cmd.ExecuteNonQuery();
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateUndoCageStock
  }
}
