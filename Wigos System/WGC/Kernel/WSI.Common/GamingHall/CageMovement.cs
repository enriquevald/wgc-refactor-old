﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CageMovement.cs
// 
//   DESCRIPTION: It has classes to insert, update or get records (CAGE_MOVEMENTS and CAGE_MOVEMENT_DETAILS tables) 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 FAV    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingHall
{
  public class CageMovement
  {

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Insert a cage movement and details in CAGE_MOVEMENTS and CAGE_MOVEMENT_DETAILS tables
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalCashierSession
    //        - TerminalDenominations
    //        - CageOperationType
    //        - CageOperationStatus
    //        - TargetType
    //        - MoneyCollectionId
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - MovementId
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean InsertCageMovements(CashierSessionInfo TerminalCashierSession, TerminalDenominations TerminalDenominations, Cage.CageOperationType CageOperationType,
                                       Cage.CageStatus CageOperationStatus, Cage.PendingMovementType? TargetType, Int64? MoneyCollectionId, out Int64 MovementId, SqlTransaction Trx)
    {
      StringBuilder _sb_movement;
      StringBuilder _sb_movement_detail;
      SqlParameter _param;
      Int64 _movement_id;
      long _cage_session_id;

      _cage_session_id = 0;
      _movement_id = 0;
      MovementId = _movement_id;

      if (!Cage.GetOpenOrDefaultCageSessionId(TerminalCashierSession.CashierSessionId, out _cage_session_id, TerminalCashierSession.GamingDay, Trx))
      {
        return false;
      }

      _sb_movement = new StringBuilder();
      _sb_movement.AppendLine(" INSERT  INTO   CAGE_MOVEMENTS 				");
      _sb_movement.AppendLine("			 (								                ");
      _sb_movement.AppendLine("			   CGM_TERMINAL_CASHIER_ID		    ");
      _sb_movement.AppendLine("			  ,CGM_USER_CASHIER_ID			      ");
      _sb_movement.AppendLine("			  ,CGM_USER_CAGE_ID			          ");
      _sb_movement.AppendLine("			  ,CGM_TYPE						            ");
      _sb_movement.AppendLine("			  ,CGM_MOVEMENT_DATETIME			    ");
      _sb_movement.AppendLine("			  ,CGM_STATUS					            ");
      _sb_movement.AppendLine("			  ,CGM_CASHIER_SESSION_ID		      ");
      _sb_movement.AppendLine("			  ,CGM_TARGET_TYPE				        ");
      _sb_movement.AppendLine("			  ,CGM_CAGE_SESSION_ID  	        ");
      _sb_movement.AppendLine("			  ,CGM_MC_COLLECTION_ID  	        ");
      _sb_movement.AppendLine("			 )								                ");
      _sb_movement.AppendLine("VALUES ( 								              ");
      _sb_movement.AppendLine("			  @pTerminalCashierId							");
      _sb_movement.AppendLine("			 ,@pUserCashierId								  ");
      _sb_movement.AppendLine("			 ,@pUserCageId								    ");
      _sb_movement.AppendLine("			 ,@pType								          ");
      _sb_movement.AppendLine("			 ,@pMovementDatetime							");
      _sb_movement.AppendLine("			 ,@pStatus								        ");
      _sb_movement.AppendLine("			 ,@pCashierSessionId							");
      _sb_movement.AppendLine("			 ,@pTargetType								    ");
      _sb_movement.AppendLine("		   ,@pSessionId   			            ");
      _sb_movement.AppendLine("		   ,@pMoneyCollectionId             ");
      _sb_movement.AppendLine("			 )								                ");

      _sb_movement.AppendLine("  SET  @pMovementId = SCOPE_IDENTITY() ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb_movement.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalCashierId", SqlDbType.BigInt).Value = TerminalCashierSession.TerminalId;
          _cmd.Parameters.Add("@pUserCashierId", SqlDbType.BigInt).Value = TerminalCashierSession.UserId;
          _cmd.Parameters.Add("@pUserCageId", SqlDbType.BigInt).Value = TerminalCashierSession.UserId;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = CageOperationType;
          _cmd.Parameters.Add("@pMovementDatetime", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = CageOperationStatus;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = TerminalCashierSession.CashierSessionId;
          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = (object)TargetType ?? DBNull.Value;
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = _cage_session_id;
          _cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = (object)MoneyCollectionId ?? DBNull.Value;

          _param = _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            _movement_id = (Int64)_param.Value;
          }

        }// SqlCommand

        if (TerminalDenominations.Total > 0)
        {
          foreach (TerminalCurrencyItem _item in TerminalDenominations.DenominationsWithQuantity)
          {
            if (_item.Quantity > 0)
            {
              _sb_movement_detail = new StringBuilder();
              _sb_movement_detail.AppendLine("   INSERT INTO   CAGE_MOVEMENT_DETAILS      ");
              _sb_movement_detail.AppendLine("               ( CMD_MOVEMENT_ID            ");
              _sb_movement_detail.AppendLine("               , CMD_QUANTITY               ");
              _sb_movement_detail.AppendLine("               , CMD_DENOMINATION           ");
              _sb_movement_detail.AppendLine("               , CMD_ISO_CODE               ");
              _sb_movement_detail.AppendLine("               , CMD_CHIP_ID                ");
              _sb_movement_detail.AppendLine("               , CMD_CAGE_CURRENCY_TYPE     ");
              _sb_movement_detail.AppendLine("               )                            ");
              _sb_movement_detail.AppendLine("        VALUES                              ");
              _sb_movement_detail.AppendLine("               ( @pMovementId               ");
              _sb_movement_detail.AppendLine("               , @pQuantity                 ");
              _sb_movement_detail.AppendLine("               , @pDenomination             ");
              _sb_movement_detail.AppendLine("               , @pIsoCode                  ");
              _sb_movement_detail.AppendLine("               , @pChipID                   ");
              _sb_movement_detail.AppendLine("               , @CageCurrencyType          ");
              _sb_movement_detail.AppendLine("               )                            ");

              using (SqlCommand _cmd = new SqlCommand(_sb_movement_detail.ToString(), Trx.Connection, Trx))
              {
                _cmd.Parameters.Clear();
                _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = _movement_id;
                _cmd.Parameters.Add("@pQuantity", SqlDbType.Int).Value = _item.Quantity;
                _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).Value = _item.Denomination;
                _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar, 3).Value = _item.IsoCode;
                _cmd.Parameters.Add("@pChipID", SqlDbType.BigInt).Value = DBNull.Value;
                _cmd.Parameters.Add("@CageCurrencyType", SqlDbType.BigInt).Value = _item.CageCurrencyType;

                _cmd.ExecuteNonQuery();
              }
            }
          }
        }

        MovementId = _movement_id;

        return true;
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE:   Checks if the movement still has the same status
    //
    //  PARAMS:
    //      - INPUT:
    //          - MovID
    //          - Status
    //
    //      - OUTPUT:
    //
    // RETURNS :
    //      - True if it has the same status.
    //
    //   NOTES:
    //
    public Boolean CheckIfSameStatus(Int64 MovID, Cage.CageStatus Status, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;
      Boolean _same_status;

      _same_status = false;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT    CGM_STATUS ");
      _sb.AppendLine("  FROM    CAGE_MOVEMENTS ");
      _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pMovementId ");
      _sb.AppendLine("   AND    CGM_STATUS = @pStatus");

      try
      {

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
            _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = MovID;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              _same_status = true;
            }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _same_status;
    }

    //------------------------------------------------------------------------------
    // PURPOSE:   Updates movement
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS :
    //       db result
    //
    //   NOTES:
    //
    public Boolean UpdateMovement(Int64 MovementId, Int64 CashierSessionId, Boolean SetToNullCageId, Int32? UserCageId, Int64? MoneyCollectionId,
                                  Cage.CageStatus Status, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("UPDATE    CAGE_MOVEMENTS ");
      _sb.AppendLine("   SET    CGM_STATUS = @pStatus ");

      if (SetToNullCageId || (!SetToNullCageId && UserCageId != null))
      {
        _sb.AppendLine("         ,CGM_USER_CAGE_ID = @pUserCage ");
      }

      if (MoneyCollectionId != null)
      {
        _sb.AppendLine("               ,CGM_MC_COLLECTION_ID = @pMcCollectionId");
      }

      if (Status == Cage.CageStatus.Canceled)
      {
        _sb.AppendLine("        , CGM_CANCELLATION_USER_ID = @pCageIdLastModification        ");
        _sb.AppendLine("        , CGM_CANCELLATION_DATETIME = @pDatetimeLastModification     ");
      }

      _sb.AppendLine(" WHERE    CGM_MOVEMENT_ID = @pMovementId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

          if (!SetToNullCageId && UserCageId != null)
          {
            _cmd.Parameters.Add("@pUserCage", SqlDbType.BigInt).Value = UserCageId;
          }
          else if (SetToNullCageId)
          {
            _cmd.Parameters.Add("@pUserCage", SqlDbType.BigInt).Value = DBNull.Value;
          }

          _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = MovementId;

          if (Status == Cage.CageStatus.Canceled)
          {
            _cmd.Parameters.Add("@pCageIdLastModification", SqlDbType.Int).Value = CashierSessionId;
            _cmd.Parameters.Add("@pDatetimeLastModification", SqlDbType.DateTime).Value = WGDB.Now;
          }

          if (MoneyCollectionId != null)
          {
            _cmd.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;
          }

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }

        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE:    To delete the movement details
    //
    //  PARAMS:
    //      - INPUT
    //        - TerminalDenominations
    //
    //      - OUTPUT:
    //
    // RETURNS :
    //       - DeleteMovementDetails
    //
    //   NOTES:
    //
    public Boolean DeleteMovementDetails(Int64 MovementId, SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;
        _sb = new StringBuilder();

        _sb.AppendLine("DELETE   FROM CAGE_MOVEMENT_DETAILS ");
        _sb.AppendLine(" WHERE   CMD_MOVEMENT_ID = @pMovementId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = MovementId;

          return (_cmd.ExecuteNonQuery() >= 0);
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE:    Deletes movement from pending movement table
    //
    //  PARAMS:
    //      - INPUT
    //        - TerminalDenominations
    //        - CageOperationType
    //        - SqlTrx
    //        - IsCancellMovement
    //
    //      - OUTPUT:
    //
    // RETURNS :
    //       - True if everything went OK. False if there was an error
    //
    //   NOTES:
    //
    public Boolean DeleteFromPendingMovement(Int64 MovementId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("DELETE FROM    CAGE_PENDING_MOVEMENTS ");
        _sb.AppendLine("      WHERE    CPM_MOVEMENT_ID = @pMovementId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {

          _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = MovementId;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion
   
  }
}
