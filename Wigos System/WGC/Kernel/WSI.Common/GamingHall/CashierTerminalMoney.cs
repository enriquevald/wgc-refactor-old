﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashierTerminalMoney.cs
// 
//   DESCRIPTION: It has classes to insert, update or get records (CASHIER_TERMINAL_MONEY table) 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 02-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-NOV-2015 FAV    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingHall
{
  public static class CashierTerminalMoney
  {
    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Gets a list of CashierTerminalMoneyInfo by MoneyCollectionId
    //
    //  PARAMS:
    //      - INPUT:
    //        - MoneyCollectionId
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - CashierTerminalMoneyInfoList
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public static Boolean GetCashierTerminalMoneyList(Int64 MoneyCollectionId, out List<CashierTerminalMoneyInfo> CashierTerminalMoneyInfoList, SqlTransaction Trx)
    {
      CashierTerminalMoneyDAO _dao;

      _dao = new CashierTerminalMoneyDAO();
      return _dao.DB_GetCashierTerminalMoneyList(MoneyCollectionId, out CashierTerminalMoneyInfoList, Trx);
    }

    #endregion
  }

  public class CashierTerminalMoneyInfo
  {
    #region Properties
    public Int64 SessionId { get; set; }

    public Int32 TerminaId { get; set; }

    public Decimal Denomination { get; set; }

    public Int32 Quantity { get; set; }

    public Int64 MoneyCollectionId { get; set; }
    #endregion
  }

  internal class CashierTerminalMoneyDAO
  {

    #region Attributes

    private const int CONNECTION_EXCEPTION_TIME_OUT = -2;

    #endregion

    #region Public Methods

    public Boolean DB_GetCashierTerminalMoneyList(Int64 MoneyCollectionId, out List<CashierTerminalMoneyInfo> CashierTerminalMoneyInfoList, SqlTransaction Trx)
    {
      StringBuilder _sb;
      CashierTerminalMoneyInfoList = new List<CashierTerminalMoneyInfo>();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT		CTM_SESSION_ID                                                     ");
        _sb.AppendLine("			  , CTM_TERMINAL_ID                                                    ");
        _sb.AppendLine("			  , CTM_DENOMINATION                                                   ");
        _sb.AppendLine("			  , CTM_QUANTITY                                                       ");
        _sb.AppendLine("			  , CTM_MONEY_COLLECTION_ID                                            ");
        _sb.AppendLine("  FROM		CASHIER_TERMINAL_MONEY    WITH(INDEX(IX_ctm_money_collection_id))  ");
        _sb.AppendLine(" WHERE		CTM_MONEY_COLLECTION_ID = @pMoneyCollectionId                      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              CashierTerminalMoneyInfo _cashier_terminal_money = new CashierTerminalMoneyInfo()
              {
                SessionId = (Int64)_reader[0],
                TerminaId = (Int32)_reader[1],
                Denomination = (Decimal)_reader[2],
                Quantity =  (Int32)_reader[3],
                MoneyCollectionId =  (Int64)_reader[4]
              };

              CashierTerminalMoneyInfoList.Add(_cashier_terminal_money);
            }

            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion
  }

}
