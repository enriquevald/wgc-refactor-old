﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalCurrency.cs
// 
//   DESCRIPTION: It has classes to insert, update or get records for terminal_currency table
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 FAV    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingHall
{
  /// <summary>
  /// Business class: TerminalCurrency
  /// </summary>
  public class TerminalCurrency
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Get a TerminalCurencyInfo object by Id
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalCurrencyId :   Id of the terminal_currency table
    //        - TerminalCashDirection TerminalCashDirection
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - 
    //
    // RETURNS :
    //      - TerminalCurrencyInfo
    //
    //   NOTES:
    //
    public TerminalCurrencyInfo GetTerminalCurrencyById(Int32 TerminalCurrencyId, TerminalCashDirection TerminalCashDirection, SqlTransaction Trx)
    {
      TerminalCurrencyInfoDAO _dao;
      TerminalCurrencyInfo _terminal_currency_info = null;

      _dao = new TerminalCurrencyInfoDAO();
      _dao.DB_GetTerminalCurrencyById(TerminalCurrencyId, TerminalCashDirection, out _terminal_currency_info, Trx);

      return _terminal_currency_info;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the TerminalCurreny Id by Terminal Id.
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId :   Terminal Id
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - 
    //
    // RETURNS :
    //      - Terminal curreny Id
    //
    //   NOTES:
    //
    public Int32? GetTerminalCurrencyIdByTerminalId(Int32 TerminalId, SqlTransaction Trx)
    {
      TerminalCurrencyInfoDAO _dao;

      _dao = new TerminalCurrencyInfoDAO();
      return _dao.GetTerminalCurrencyIdByTerminalId(TerminalId, Trx);
    
    }

  }

  /// <summary>
  /// Entity class: TerminalCurrencyInfo
  /// </summary>
  public class TerminalCurrencyInfo
  {
    #region Properties
    public Int32 Id { get; set; }

    public String Name { get; set; }

    public List<TerminalCurrencyDetailsInfo> TerminalCurrencyDetailsList { get; set; }

    #endregion
  }

  /// <summary>
  /// Internal data access class: TerminalCurrencyInfoDAO
  /// </summary>
  internal class TerminalCurrencyInfoDAO
  {
    #region Attributes

    private const int CONNECTION_EXCEPTION_TIME_OUT = -2;

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Gets a row from the TERMINAL_CURRENCY table by Id
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalCurrencyId :    Id of the terminal_currency table
    //        - TerminalCashDirection : TerminalCashDirection
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //        - TerminalCurrencyInfo:
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public Boolean DB_GetTerminalCurrencyById(Int32 TerminalCurrencyId, TerminalCashDirection TerminalCashDirection,
                                           out TerminalCurrencyInfo TerminalCurrencyInfo, SqlTransaction Trx)
    {
      StringBuilder _sb;
      List<TerminalCurrencyDetailsInfo> _terminal_currency_detail_info_list = null;
      TerminalCurrencyInfo = null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   TC_ID");
        _sb.AppendLine("       , TC_NAME");
        _sb.AppendLine("  FROM   TERMINAL_CURRENCY");
        _sb.AppendLine(" WHERE   TC_ID = @pTerminalCurrencyId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalCurrencyId", SqlDbType.Int).Value = TerminalCurrencyId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            TerminalCurrencyDetailsInfoDAO _dao_detail = new TerminalCurrencyDetailsInfoDAO();
            if (_reader.Read())
            {
              TerminalCurrencyInfo = new TerminalCurrencyInfo()
              {
                Id = (Int32)_reader[0],
                Name = (String)_reader[1],
              };

              // Get detail information for the TerminalCurrency
              _dao_detail.DB_GetTerminalCurrencyDetailsList(TerminalCurrencyInfo, TerminalCashDirection, out _terminal_currency_detail_info_list, Trx);

              TerminalCurrencyInfo.TerminalCurrencyDetailsList = _terminal_currency_detail_info_list;

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the TerminalCurrencyId from de Terminals table.
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId :   Terminal id
    //        - Trx : SQL Transaction
    //      - OUTPUT:
    //
    // RETURNS:
    //    - Int32? : TermnalCurrencyId of the Terminals table 
    //   NOTES:
    //
    public Int32? GetTerminalCurrencyIdByTerminalId(Int32 TerminalId, SqlTransaction Trx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   TE_TERMINAL_CURRENCY_ID");
        _sb.AppendLine("  FROM   TERMINALS");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          Object id = _cmd.ExecuteScalar();

          if (id != null)
            return (Int32)id;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;
    }

    #endregion

  }

}
