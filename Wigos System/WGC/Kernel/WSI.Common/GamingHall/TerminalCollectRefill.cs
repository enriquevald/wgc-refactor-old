﻿
//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalCollectRefill.cs
// 
//   DESCRIPTION: It has all information about the terminal to collect and refill
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 10-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 FAV    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.GamingHall
{
  public class TerminalCollectRefill
  {

    #region Attributes
    private Int32 m_terminal_id;
    private Boolean m_collect;
    private Boolean m_refill;
    private Boolean m_change_stacker;
    private CashierSessionInfo m_terminal_cashier_session_collect;
    private MoneyCollectionInfo m_money_collection_info_collect;
    private CashierSessionInfo m_terminal_cashier_session_refill;
    private MoneyCollectionInfo m_money_collection_info_refill;
    #endregion

    #region Constructor
    public TerminalCollectRefill(Int32 TerminalId)
    {
      m_terminal_id = TerminalId;
    }
    #endregion

    #region Properties
    public Int32 TerminalId 
    {
      get
      {
        return m_terminal_id;
      }
    }

    public String TerminalName { get; set; }

    public String ProviderName { get; set; }

    public Int64? MoneyCollectionId { get; set; }

    /// <summary>
    /// Quantity to Collect
    /// </summary>
    public TerminalDenominations DenominationsCollect { get; set; }

    /// <summary>
    /// Quantitiy to Refill
    /// </summary>
    public TerminalDenominations DenominationsRefill { get; set; }

    public Boolean IsCollectedOK { get { return m_collect; } }

    public Boolean IsRefilledOK { get { return m_refill; } }

    public Boolean IsStackerChangedOK { get { return m_change_stacker; } }

    public CashierSessionInfo CashierSessionCollect
    {
      get
      {
        return m_terminal_cashier_session_collect;
      }
    }

    public MoneyCollectionInfo MoneyCollectionInfoCollect
    {
      get
      {
        return m_money_collection_info_collect;
      }
    }

    public CashierSessionInfo CashierSessionRefill
    {
      get
      {
        return m_terminal_cashier_session_refill;
      }
    }

    public MoneyCollectionInfo MoneyCollectionInfoRefill
    {
      get
      {
        return m_money_collection_info_refill;
      }
    }

    public String CurrentMeters { get; set; }

    #endregion

    #region Internal Properties
    internal Boolean SetCollectOK
    {
      set
      {
        m_collect = value;
      }
    }

    internal Boolean SetRefillOK
    {
      set
      {
        m_refill = value;
      }
    }

    internal Boolean SetChangeStackerOK
    {
      set
      {
        m_change_stacker = value;
      }
    }

    internal CashierSessionInfo SetCashierSessionCollect
    {
      set
      {
        m_terminal_cashier_session_collect = value;
      }
    }

    internal CashierSessionInfo SetCashierSessionRefill
    {
      set
      {
        m_terminal_cashier_session_refill = value;
      }
    }

    internal MoneyCollectionInfo SetMoneyCollectionInfoCollect
    {
      set
    {
        m_money_collection_info_collect = value;
      }
    }

    internal MoneyCollectionInfo SetMoneyCollectionInfoRefill
    {
      set
      {
        m_money_collection_info_refill = value;
      }
    }

    #endregion

    public object Clone()
    {
      return this.MemberwiseClone();
    }

  }
}
