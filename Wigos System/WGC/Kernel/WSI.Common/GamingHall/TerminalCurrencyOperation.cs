﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalCurrencyOperation.cs
// 
//   DESCRIPTION: 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 FAV    First release.
// 01-MAR-2016 DDS    Fixed Bug 10135: DropBox value is -100 when Cage.ShowDenominations = 0
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.GamingHall
{
  public class TerminalCurrencyOperation
  {
      #region Members

      private Boolean m_show_denominations;

      #endregion

      #region Public Methods

      #region Constructor

      public TerminalCurrencyOperation()
      {
         this.m_show_denominations = GeneralParam.GetInt32("Cage", "ShowDenominations",
        (Int32)WSI.Common.Cage.ShowDenominationsMode.ShowAllDenominations) == (Int32)WSI.Common.Cage.ShowDenominationsMode.ShowAllDenominations;
      }

      #endregion Constructor

      //------------------------------------------------------------------------------
    // PURPOSE: Gets a list of TerminalCurrencyItems (In or Out) for a Terminal Id (If is NULL returns the values for the Cage) and type.
    // The quantity for each currency by default is equal to cero
    //
    //  PARAMS:
    //      - INPUT:
    //        - TerminalId :   Terminal id
    //        - TerminalBoxType : Box or Hopper
    //      - OUTPUT:
    //        - List<TerminalCurrencyItem>
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    //   NOTES:
    //
    public bool GetTerminalCurrencyItems(Int32 TerminalId,
                                         TerminalBoxType TerminalBoxType,
                                         out List<TerminalCurrencyItem> TerminalCurrencyItems)
    {
      SqlConnection _sql_conn = null;
      SqlTransaction _sql_trx = null;
      Int32? _terminal_currency_id;
      List<TerminalCurrencyItem> _terminal_currency_items = new List<TerminalCurrencyItem>();
      TerminalCurrency _terminal_currency = new TerminalCurrency();
      TerminalCashDirection _terminal_cash_direction = TerminalCashDirection.Both;

      TerminalCurrencyItems = null;

      try
      {
        _sql_conn = WSI.Common.WGDB.Connection();

        using (_sql_trx = _sql_conn.BeginTransaction())
        {
          _terminal_currency_id = _terminal_currency.GetTerminalCurrencyIdByTerminalId(TerminalId, _sql_trx);

          if (TerminalBoxType == Common.TerminalBoxType.Box)
            _terminal_cash_direction = TerminalCashDirection.In;
          else if (TerminalBoxType == Common.TerminalBoxType.Hopper)
            _terminal_cash_direction = TerminalCashDirection.Out;
          else
            throw new Exception(string.Format ("The TerminalBoxType '{0}' is invalid for the GetTerminalCurrencyItems method", TerminalBoxType.ToString()));


          if (_terminal_currency_id == null)
          {
            // Gets currencies items for national money when there is not a configuration in TeminalCurreny table
            GetTerminalCurrencyItemsFromCage(CurrencyExchange.GetNationalCurrency(), _terminal_cash_direction, out _terminal_currency_items, _sql_trx);
          }
          else
          {
            GetTerminalCurrencyItemsFromTerminalCurrency(_terminal_currency_id.Value, _terminal_cash_direction, out _terminal_currency_items, _sql_trx);
          }

          _sql_trx.Commit();

          TerminalCurrencyItems = _terminal_currency_items;
          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (_sql_trx != null)
        _sql_trx.Rollback();

      return false;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Gets a list of TerminalCurrencyItem from the TerminalCurrency table by Id
    /// </summary>
    /// <param name="TerminalCurrencyId"></param>
    /// <param name="TerminalCashDirection"></param>
    /// <param name="TerminalCurrencyItems"></param>
    /// <param name="Trx"></param>
    private void GetTerminalCurrencyItemsFromTerminalCurrency(Int32 TerminalCurrencyId,
                                TerminalCashDirection TerminalCashDirection,
                                out List<TerminalCurrencyItem> TerminalCurrencyItems, SqlTransaction Trx)
    {
      TerminalCurrency _terminal_currency = new TerminalCurrency();
      TerminalCurrencyInfo _terminal_currency_info = null;
      TerminalCurrencyItems = new List<TerminalCurrencyItem>();

      _terminal_currency_info = _terminal_currency.GetTerminalCurrencyById(TerminalCurrencyId, TerminalCashDirection, Trx);

      if (_terminal_currency_info != null && _terminal_currency_info.TerminalCurrencyDetailsList != null)
      {
        foreach (TerminalCurrencyDetailsInfo _detail in _terminal_currency_info.TerminalCurrencyDetailsList)
        {

          if (_detail.TerminalCashDirection == TerminalCashDirection 
               && (_detail.CurrencyType == CageCurrencyType.Bill || _detail.CurrencyType == CageCurrencyType.Coin))
          {
            // Only currencies: bill or coins
            TerminalCurrencyItem _item = new TerminalCurrencyItem(_detail.IsoCode, CurrencyExchangeType.CURRENCY, this.m_show_denominations)
            {
              Denomination = _detail.Denomination,
              CageCurrencyType = _detail.CurrencyType,
              TerminalCashDirection = TerminalCashDirection,
              Quantity = null,
            };

            TerminalCurrencyItems.Add(_item);
          }
        }
      }
    }

    /// <summary>
    /// Gets a list of TerminalCurrencyItem from the cage_currencies table (Only coins or bills)
    /// </summary>
    /// <param name="IsoCode"></param>
    /// <param name="TerminalCashDirection"></param>
    /// <param name="TerminalCurrencyItems"></param>
    /// <param name="Trx"></param>
    private void GetTerminalCurrencyItemsFromCage(String IsoCode,
                                         TerminalCashDirection TerminalCashDirection,
                                         out List<TerminalCurrencyItem> TerminalCurrencyItems, SqlTransaction Trx)
    {
      StringBuilder _sb;
      TerminalCurrencyItems = null;
      List<TerminalCurrencyItem> _list = new List<TerminalCurrencyItem>();

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   CGC_ISO_CODE");
      _sb.AppendLine("       , CGC_DENOMINATION");
      _sb.AppendLine("       , CGC_ALLOWED");
      _sb.AppendLine("       , CGC_CAGE_CURRENCY_TYPE");
      _sb.AppendLine("  FROM   CAGE_CURRENCIES");
      _sb.AppendLine(" WHERE   CGC_ISO_CODE = @pIsoCode");
      _sb.AppendLine("   AND   CGC_ALLOWED  = @pAllowed");
      _sb.AppendLine("   AND   CGC_DENOMINATION > 0");
      _sb.AppendLine(" ORDER   BY CGC_DENOMINATION DESC");


      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar, 3).Value = IsoCode;
        _cmd.Parameters.Add("@pAllowed", SqlDbType.Bit).Value = 1;

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          while (_reader.Read())
          {
            if ((CageCurrencyType)_reader[3] == CageCurrencyType.Bill || (CageCurrencyType)_reader[3] == CageCurrencyType.Coin)
            {
              // Only currencies: bill or coins
              TerminalCurrencyItem _item = new TerminalCurrencyItem((String)_reader[0], CurrencyExchangeType.CURRENCY, this.m_show_denominations)
              {
                Denomination = Convert.ToDecimal(_reader[1]),
                CageCurrencyType = (CageCurrencyType)_reader[3],
                TerminalCashDirection = TerminalCashDirection,
                Quantity = null,
              };

              _list.Add(_item);
            }
          }

          TerminalCurrencyItems = _list;
        }
      }
    }

    #endregion
  }
}
