﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalCurrencyItem.cs
// 
//   DESCRIPTION: Entity class with information about currencies and quantity
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 FAV    First release.
// 14-JAN-2016 DDS    PBI 8000: Salones(2): Cajero: Recaudación con montos totales o por denominación
// 01-MAR-2016 DDS    Fixed Bug 10135: DropBox value is -100 when Cage.ShowDenominations = 0
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;

namespace WSI.Common.GamingHall
{
  [Serializable]
  public class TerminalCurrencyItem
  {
    #region Attributes
    private String m_iso_code;
    private CurrencyExchangeType m_currency_exchange_type;
    private Boolean m_use_denominations;
    private Decimal m_total;
    #endregion

    #region Constructor

    private TerminalCurrencyItem()
    {
    }

    public TerminalCurrencyItem(String IsoCode, CurrencyExchangeType CurrencyExchangeType, Boolean UseDenominations)
    {
      m_iso_code = IsoCode;
      m_currency_exchange_type = CurrencyExchangeType;
      m_use_denominations = UseDenominations;
      if (!m_use_denominations)
        Quantity = 1;
    }

    #endregion

    #region Properties
    [Browsable(false)]
    public Decimal Denomination { get; set; }

    [Browsable(false)]
    public Boolean UseDenominations
    {
      get
      {
        return this.m_use_denominations;
      }
    }

    [Browsable(false)]
    public String IsoCode
    {
      get
      {
        return m_iso_code;
      }
      set
      {
        m_iso_code = value;
      }
    }

    [Browsable(false)]
    public Int32? Quantity { get; set; }

    [Browsable(false)]
    public CageCurrencyType CageCurrencyType { get; set; }

    [Browsable(false)]
    public Decimal Total
    {
      get
      {
        if (!this.UseDenominations)
          return this.m_total;

        if (this.Quantity == null)
          return 0;

        return this.Quantity.Value * this.Denomination;
      }
      set
      {
        m_total = value;
      }
    }

    [Browsable(false)]
    public TerminalCashDirection TerminalCashDirection { get; set; }

    [Browsable(false)]
    public CurrencyExchangeType CurrencyExchangeType
    {
      get
      {
        return m_currency_exchange_type;
      }
      set
      {
        m_currency_exchange_type = value;
      }
    }

    [Browsable(false)]
    public CurrencyIsoType CurrencyIsoType
    {
      get
      {
        CurrencyIsoType _iso_type = new CurrencyIsoType();
        _iso_type.IsoCode = m_iso_code;
        _iso_type.Type = m_currency_exchange_type;
        return _iso_type;
      }
    }

    public String DenominationStr
    {
      get
      {
        if (this.UseDenominations)
          return Currency.Format(this.Denomination, "").Trim();
        else
          return "TOTAL";
      }
    }

    public String CurrencyTypeStr
    {
      get
      {
        if (!this.UseDenominations)
          return String.Empty;

        if (this.CageCurrencyType == CageCurrencyType.Bill)
          return Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL"); //"B"
        else
          return Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN"); //"M"
      }
    }

    public String QuantityStr
    {
      get
      {
        if (!this.UseDenominations)
          return Currency.Format(this.m_total, "");

        if (this.Quantity == null)
          return String.Empty;

        return this.Quantity.ToString();
      }

    }

    public String TotalStr
    {
      get
      {
        if (!this.UseDenominations)
          return Currency.Format(m_total, "");

        if (this.Quantity == null)
          return string.Empty;

        return Currency.Format(this.Total, "");
      }
    }

    #endregion

  }
}
