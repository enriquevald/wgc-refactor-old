﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CountR.cs
// 
//   DESCRIPTION: Miscellaneous utilities for CountR
// 
//        AUTHOR: Yineth Nuñez
// 
// CREATION DATE: 23-MAR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAR-2016 YNM    First release (Header).
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Text;

namespace WSI.Common.CountR
{
  public class CountRTransaction
  {
    #region " Enums "

    public enum TRANSACTION_STATUS
    {
      PENDING = 0,
      PAID = 1,
      EXPIRED = 2
    }

    #endregion " Enums "

    #region Properties

    public Int64 Id { get; set; }
    public Int64 PaidTicketId { get; set; }
    public Int64 OperationId { get; set; }
    public Currency PaidAmount { get; set; }
    public Int64? NewTicketId { get; set; }
    public Currency NewTicketAmount { get; set; }
    public Currency CashPaidAmount { get; set; }
    public Int32 CountrId { get; set; }
    public Int64 CashierSessionId { get; set; }
    public DateTime GamingDay { get; set; }
    public TRANSACTION_STATUS Status { get; set; }

    #endregion

    #region Public Methods

    /// <summary>
    /// Insert CountR transaction
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        // New Ticket: unimplemented. Not used at the moment
        _sb = new StringBuilder();
        _sb.AppendLine("  INSERT INTO  COUNTR_TRANSACTION     ");
        _sb.AppendLine("             ( CRT_PAID_TICKET_ID     ");
        _sb.AppendLine("             , CRT_OPERATION_ID       ");
        _sb.AppendLine("             , CRT_PAID_AMOUNT        ");
        //_sb.AppendLine("             , CRT_NEW_TICKET_ID      ");
        //_sb.AppendLine("             , CRT_NEW_TICKET_AMOUNT  ");
        _sb.AppendLine("             , CRT_CASH_AMOUNT        ");
        _sb.AppendLine("             , CRT_COUNTR_ID          ");
        _sb.AppendLine("             , CRT_CASHIER_SESSION_ID ");
        _sb.AppendLine("             , CRT_GAMING_DAY         ");
        _sb.AppendLine("             , CRT_STATUS             ");
        _sb.AppendLine("             , CRT_LAST_MODIFIED )    ");
        _sb.AppendLine("      VALUES                          ");
        _sb.AppendLine("             ( @pPaidTicketId         ");
        _sb.AppendLine("             , @pOperationId          ");
        _sb.AppendLine("             , @pPaidAmount           ");
        //_sb.AppendLine("             , @pNewTicketId          ");
        //_sb.AppendLine("             , @pNewTicketAmount      ");
        _sb.AppendLine("             , @pCashAmount           ");
        _sb.AppendLine("             , @pCountRId             ");
        _sb.AppendLine("             , @pCashierSessionId     ");
        _sb.AppendLine("             , @pGamingDay            ");
        _sb.AppendLine("             , @pStatus               ");
        _sb.AppendLine("             , @pLastModified )       ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPaidTicketId", SqlDbType.BigInt).Value = PaidTicketId;
          _cmd.Parameters.Add("@pPaidAmount", SqlDbType.Money).Value = (Decimal)PaidAmount;
          _cmd.Parameters.Add("@pOperationId", SqlDbType.Money).Value = (Decimal)OperationId;
          //_cmd.Parameters.Add("@pNewTicketId", SqlDbType.BigInt).Value = NewTicketId;
          //_cmd.Parameters.Add("@pNewTicketAmount", SqlDbType.Money).Value = (Decimal)NewTicketAmount;
          _cmd.Parameters.Add("@pCashAmount", SqlDbType.Money).Value = (Decimal)CashPaidAmount;
          _cmd.Parameters.Add("@pCountRId", SqlDbType.Int).Value = CountrId;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = Misc.TodayOpening();
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TRANSACTION_STATUS.PENDING;
          _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountRTransaction.DB_Insert -> CountrId: {0}, CashierSessionId: {1}, PaidTicketId: {2}", CountrId, CashierSessionId, PaidTicketId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Get transaction By Ticket Paid and status
    /// </summary>
    /// <param name="TicketNbr"></param>    
    /// <param name="Status"></param>
    /// <param name="CountRTrans"></param>
    /// <param name="SqlTransaction"></param>
    /// <returns></returns>
    public static Boolean DB_GetByTicketAndStatus(String TicketNbr, TRANSACTION_STATUS Status, out CountRTransaction CountRTrans, SqlTransaction SqlTransaction)
    {
      StringBuilder _sb;

      CountRTrans = null;

      try
      {
        // New Ticket: unimplemented. Not used at the moment
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   CRT_TRANSACTION_ID                           ");
        _sb.AppendLine("           , CRT_PAID_TICKET_ID                           ");
        _sb.AppendLine("           , CRT_OPERATION_ID                             ");
        _sb.AppendLine("           , CRT_PAID_AMOUNT                              ");
        //_sb.AppendLine   ("        , CRT_NEW_TICKET_ID                            ");
        //_sb.AppendLine   ("        , CRT_NEW_TICKET_AMOUNT                        ");
        _sb.AppendLine("           , CRT_CASH_AMOUNT                              ");
        _sb.AppendLine("           , CRT_COUNTR_ID                                ");
        _sb.AppendLine("           , CRT_CASHIER_SESSION_ID                       ");
        _sb.AppendLine("           , CRT_GAMING_DAY                               ");
        _sb.AppendLine("           , CRT_STATUS                                   ");
        _sb.AppendLine("      FROM   COUNTR_TRANSACTION                           ");
        _sb.AppendLine(" LEFT JOIN   TICKETS ON TI_TICKET_ID = CRT_PAID_TICKET_ID ");
        _sb.AppendLine("     WHERE   TI_VALIDATION_NUMBER    = @pTicketNbr        ");
        _sb.AppendLine("       AND   CRT_STATUS              = @pStatus           ");
        _sb.AppendLine("  ORDER BY   CRT_TRANSACTION_ID DESC                      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTransaction.Connection, SqlTransaction))
        {
          _cmd.Parameters.Add("@pTicketNbr", SqlDbType.BigInt).Value = TicketNbr;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {

            if (_reader.Read())
            {
              CountRTrans = new CountRTransaction();
              CountRTrans.Id = (Int64)_reader["CRT_TRANSACTION_ID"];
              CountRTrans.PaidTicketId = (Int64)_reader["CRT_PAID_TICKET_ID"];
              CountRTrans.OperationId = (Int64)_reader["CRT_OPERATION_ID"];
              CountRTrans.PaidAmount = (Decimal)_reader["CRT_PAID_AMOUNT"];
              //CountRTrans.NewTicketId = (Int64)_reader["CRT_NEW_TICKET_ID"];
              //CountRTrans.NewTicketAmount = (Decimal)_reader["CRT_NEW_TICKET_AMOUNT"];
              CountRTrans.CashPaidAmount = (Decimal)_reader["CRT_CASH_AMOUNT"];
              CountRTrans.CountrId = (Int32)_reader["CRT_COUNTR_ID"];
              CountRTrans.CashierSessionId = (Int64)_reader["CRT_CASHIER_SESSION_ID"];
              CountRTrans.GamingDay = (DateTime)_reader["CRT_GAMING_DAY"];
              CountRTrans.Status = (TRANSACTION_STATUS)_reader["CRT_STATUS"];

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountRTransaction.DB_GetTransactionByTicket -> Ticket: {0}", TicketNbr));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetByTicketAndStatus

    /// <summary>
    /// Update CountR Transaction
    /// </summary>
    /// <param name="SqlTransaction"></param>
    /// <returns></returns>
    public Boolean DB_Update(SqlTransaction SqlTransaction)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   COUNTR_TRANSACTION                     ");
        _sb.AppendLine("    SET   CRT_STATUS            = @pStatus       ");
        _sb.AppendLine("        , CRT_LAST_MODIFIED     = @pLastModified ");
        _sb.AppendLine("  WHERE   CRT_TRANSACTION_ID    = @pId           ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTransaction.Connection, SqlTransaction))
        {
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TRANSACTION_STATUS.PAID;
          _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Id;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountRTransaction.DB_UpdateCountRTransaction -> OperationID: {0}", OperationId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_Update

    /// <summary>
    /// Change ticket status with payment timeout
    /// </summary>
    /// <returns></returns>
    public static Boolean PaymentTimeOut()
    {
      StringBuilder _sb;
      Int32 _interval_timeout;
      DateTime _timeout;

      try
      {
        _interval_timeout = GeneralParam.GetInt32("CountR", "Payment.TimeOut", 60);
        _timeout = WGDB.Now.AddSeconds(-_interval_timeout);

        if (_interval_timeout == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("     UPDATE   TICKETS                                                                            ");
        _sb.AppendLine("        SET   TI_STATUS = CASE                                                                   ");
        _sb.AppendLine("                           WHEN TI_STATUS  = @pTicketStatusPending THEN @pTicketStatusValid      ");
        _sb.AppendLine("                           WHEN TI_STATUS  = @pTicketStatusPendExp THEN @pTicketStatusExpired    ");
        _sb.AppendLine("                          END                                                                    ");
        _sb.AppendLine("            , TI_LAST_ACTION_DATETIME      = NULL                                                ");
        _sb.AppendLine("            , TI_LAST_ACTION_TERMINAL_ID   = NULL                                                ");
        _sb.AppendLine("            , TI_LAST_ACTION_TERMINAL_TYPE = NULL                                                ");
        _sb.AppendLine("            , TI_LAST_ACTION_ACCOUNT_ID    = NULL                                                ");
        _sb.AppendLine("            , TI_TRANSACTION_ID            = NULL                                                ");
        _sb.AppendLine("       FROM   TICKETS                                                                            ");
        _sb.AppendLine(" INNER JOIN   COUNTR_TRANSACTION ON TI_TICKET_ID = CRT_PAID_TICKET_ID                            ");
        _sb.AppendLine("        AND   CRT_STATUS                         = @pTransactionStatusPending                    ");
        _sb.AppendLine("        AND   CRT_LAST_MODIFIED                  < @pTime                                        ");
        _sb.AppendLine("      WHERE   TI_STATUS                        IN (@pTicketStatusPending, @pTicketStatusPendExp) ");
        _sb.AppendLine("                                                                                                 ");
        _sb.AppendLine("     UPDATE   COUNTR_TRANSACTION                                                                 ");
        _sb.AppendLine("        SET   CRT_STATUS                         = @pTransactionStatusExpired                    ");
        _sb.AppendLine("            , CRT_LAST_MODIFIED                  = @pTime                                        ");
        _sb.AppendLine("      WHERE   CRT_STATUS                         = @pTransactionStatusPending                    ");
        _sb.AppendLine("        AND   CRT_LAST_MODIFIED                  < @pTime                                        ");

        using (DB_TRX _db_trx = new DB_TRX())
        {

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pTicketStatusPending", SqlDbType.Int).Value = TITO_TICKET_STATUS.PENDING_PAYMENT;
            _cmd.Parameters.Add("@pTicketStatusValid", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID;
            _cmd.Parameters.Add("@pTicketStatusPendExp", SqlDbType.Int).Value = TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION;
            _cmd.Parameters.Add("@pTicketStatusExpired", SqlDbType.Int).Value = TITO_TICKET_STATUS.EXPIRED;
            _cmd.Parameters.Add("@pTime", SqlDbType.DateTime).Value = _timeout;
            _cmd.Parameters.Add("@pTransactionStatusExpired", SqlDbType.Int).Value = TRANSACTION_STATUS.EXPIRED;
            _cmd.Parameters.Add("@pTransactionStatusPending", SqlDbType.Int).Value = TRANSACTION_STATUS.PENDING;

            if (_cmd.ExecuteNonQuery() > 0)
            {
              _db_trx.Commit();
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("CountRTransaction.PaymentTimeOut");
        Log.Exception(_ex);
      }

      return false;
    } // PaymentTimeOut

    #endregion
  }
}
