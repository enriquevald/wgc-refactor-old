﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CountR.cs
// 
//   DESCRIPTION: Miscellaneous utilities for CountR
// 
//        AUTHOR: Yineth Nuñez
// 
// CREATION DATE: 23-MAR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAR-2016 YNM    First release (Header).
// 23-FEB-2017  MS    Removed unneeded SQl Parameter in CheckCountRShowLog
// 21-AUG-2017 ETP    PBI 29415: Add Redemption Kiosk Protocol logic
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;

namespace WSI.Common.CountR
{
  public class CountR
  {
   #region " Properties "

    public Int32 Id { get; set; }
    public Int32 Code { get; set; }
    public String Name { get; set; }
    public String Description { get; set; }
    public String Provider { get; set; }
    public Int32 AreaId { get; set; }
    public String AreaName { get; set; }
    public Boolean Zone { get; set; }
    public Int32 BankId { get; set; }
    public String BankName { get; set; }
    public String FloorId { get; set; }
    public Currency MaxPayment { get; set; }
    public Currency MinPayment { get; set; }
    public String IsoCode { get; set; }
    public Boolean ShowLog { get; set; }
    public Boolean CreateTicket { get; set; }
    public Boolean Enabled { get; set; }
    public DateTime Created { get; set; }
    public DateTime LastModified { get; set; }
    public DateTime RetirementDate { get; set; }
    public Int64 RetirementUserId { get; set; }
    public String RetirementUserName { get; set; }
    public DateTime LastConnection { get; set; }
    public int Status { get; set; }
    public String IpAddress { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Get data from specific CountR 
    /// </summary>
    /// <param name="CountRId"></param>
    /// <param name="CountR"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_GetCountR(Int32 CountRId, out CountR CountR, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      CountR = null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   CR_COUNTR_ID                                    "); // 0
        _sb.AppendLine("           , CR_CODE                                         "); // 1
        _sb.AppendLine("           , CR_NAME                                         "); // 2
        _sb.AppendLine("           , CR_DESCRIPTION                                  "); // 3
        _sb.AppendLine("           , CR_PROVIDER                                     "); // 4
        _sb.AppendLine("           , CR_AREA_ID                                      "); // 5
        _sb.AppendLine("           , AR_NAME                                         "); // 6
        _sb.AppendLine("           , AR_SMOKING                                      "); // 7
        _sb.AppendLine("           , CR_BANK_ID                                      "); // 8
        _sb.AppendLine("           , BK_NAME                                         "); // 9
        _sb.AppendLine("           , CR_FLOOR_ID                                     "); //10
        _sb.AppendLine("           , CR_MAX_PAYMENT                                  "); //11
        _sb.AppendLine("           , CR_MIN_PAYMENT                                  "); //12
        _sb.AppendLine("           , CR_CURRENT_ISOCODE                              "); //13
        _sb.AppendLine("           , CR_ENABLED                                      "); //14
        _sb.AppendLine("           , CR_CREATED                                      "); //15
        _sb.AppendLine("           , CR_LAST_MODIFIED                                "); //16
        _sb.AppendLine("           , CR_RETIREMENT_DATE                              "); //17
        _sb.AppendLine("           , CR_RETIREMENT_USER_ID                           "); //18
        _sb.AppendLine("           , GU_USERNAME                                     "); //19
        _sb.AppendLine("           , CR_LAST_CONNECTION                              "); //20
        _sb.AppendLine("           , CR_STATUS                                       "); //21
        _sb.AppendLine("           , CR_IP_ADDRESS                                   "); //22
        _sb.AppendLine("           , CR_SHOW_LOG                                     "); //23
        _sb.AppendLine("           , CR_CREATE_TICKET                                "); //24
        _sb.AppendLine("      FROM   COUNTR                                          ");
        _sb.AppendLine(" LEFT JOIN   AREAS     ON AR_AREA_ID = CR_AREA_ID            ");
        _sb.AppendLine(" LEFT JOIN   BANKS     ON BK_BANK_ID = CR_BANK_ID            ");
        _sb.AppendLine(" LEFT JOIN   GUI_USERS ON GU_USER_ID = CR_RETIREMENT_USER_ID ");
        _sb.AppendLine("     WHERE   CR_COUNTR_ID = @pCountrId                       ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCountrId", SqlDbType.Int).Value = CountRId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              CountR = new CountR();

              CountR = LoadCountRFromReader(CountR, _sql_reader);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.DB_GetCountR -> CountRId: {0}", CountRId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_GetCountR

    /// <summary>
    /// Get data from specific CountR b a given code 
    /// </summary>
    /// <param name="CountRId"></param>
    /// <param name="CountR"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_GetCountRByCode(Int32 Code, out CountR CountR, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      CountR = null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   CR_COUNTR_ID                                    "); // 0
        _sb.AppendLine("           , CR_CODE                                         "); // 1
        _sb.AppendLine("           , CR_NAME                                         "); // 2
        _sb.AppendLine("           , CR_DESCRIPTION                                  "); // 3
        _sb.AppendLine("           , CR_PROVIDER                                     "); // 4
        _sb.AppendLine("           , CR_AREA_ID                                      "); // 5
        _sb.AppendLine("           , AR_NAME                                         "); // 6
        _sb.AppendLine("           , AR_SMOKING                                      "); // 7
        _sb.AppendLine("           , CR_BANK_ID                                      "); // 8
        _sb.AppendLine("           , BK_NAME                                         "); // 9
        _sb.AppendLine("           , CR_FLOOR_ID                                     "); //10
        _sb.AppendLine("           , CR_MAX_PAYMENT                                  "); //11
        _sb.AppendLine("           , CR_MIN_PAYMENT                                  "); //12
        _sb.AppendLine("           , CR_CURRENT_ISOCODE                              "); //13
        _sb.AppendLine("           , CR_ENABLED                                      "); //14
        _sb.AppendLine("           , CR_CREATED                                      "); //15
        _sb.AppendLine("           , CR_LAST_MODIFIED                                "); //16
        _sb.AppendLine("           , CR_RETIREMENT_DATE                              "); //17
        _sb.AppendLine("           , CR_RETIREMENT_USER_ID                           "); //18
        _sb.AppendLine("           , GU_USERNAME                                     "); //19
        _sb.AppendLine("           , CR_LAST_CONNECTION                              "); //20
        _sb.AppendLine("           , CR_STATUS                                       "); //21
        _sb.AppendLine("           , CR_IP_ADDRESS                                   "); //22
        _sb.AppendLine("           , CR_SHOW_LOG                                     "); //23
        _sb.AppendLine("           , CR_CREATE_TICKET                                "); //24
        _sb.AppendLine("      FROM   COUNTR                                          ");
        _sb.AppendLine(" LEFT JOIN   AREAS     ON AR_AREA_ID = CR_AREA_ID            ");
        _sb.AppendLine(" LEFT JOIN   BANKS     ON BK_BANK_ID = CR_BANK_ID            ");
        _sb.AppendLine(" LEFT JOIN   GUI_USERS ON GU_USER_ID = CR_RETIREMENT_USER_ID ");
        _sb.AppendLine("     WHERE   CR_CODE = @pCode                                ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = Code;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              CountR = LoadCountRFromReader(CountR, _sql_reader);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.DB_GetCountRByCode -> Code: {0}", Code));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetCountRByCode

    /// <summary>
    /// Check CountR and set activity in CountR
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="CountRIPAddress"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_CheckCountRAndSetActivity(Int32 CountRCode, String CountRIPAddress, SqlTransaction SqlTrx)
    {
      try
      {
        //First checks if CountR is valid and is enable.
        if (!CountR.DB_CheckCountR(CountRCode, CountRIPAddress, SqlTrx))
        {
          return false;
        }

        //Second: Update CountR last Activity.
        if (!CountR.DB_UpdateCountRActivity(CountRCode, SqlTrx))
        {
          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.DB_CheckCountRAndSetActivity -> Code: {0}", CountRCode));
        Log.Exception(_ex);
      }

      return false;
    } // DB_CheckCountRAndSetActivity

   /// <summary>
    /// DB_Update CountR Activity
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_UpdateCountRActivity(Int32 CountRCode, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   COUNTR                            ");
        _sb.AppendLine("    SET   CR_LAST_CONNECTION = @pDate       ");
        _sb.AppendLine("        , CR_STATUS          = @pStatus     ");
        _sb.AppendLine("  WHERE   CR_CODE            = @pCountrCode ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = CountRBusinessLogic.COUNTR_STATUS.ONLINE;
          _cmd.Parameters.Add("@pCountrCode", SqlDbType.Int).Value = CountRCode;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.DB_UpdateCountRActivity -> Code: {0}", CountRCode));
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateCountRActivity

    /// <summary>
    /// Checks if CountR is valid and if its status is TRUE
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="CountRIPAddress"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DB_CheckCountR(Int32 CountRCode, String CountRIPAddress, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   1                                 ");
        _sb.AppendLine("   FROM   COUNTR                            ");
        _sb.AppendLine("  WHERE   CR_CODE       = @pCountrCode      ");
        _sb.AppendLine("    AND   CR_IP_ADDRESS = @pCountRIPAddress ");
        _sb.AppendLine("    AND   CR_ENABLED    = @pCountREnabled   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCountrCode", SqlDbType.Int).Value = CountRCode;
          _cmd.Parameters.Add("@pCountRIPAddress", SqlDbType.NVarChar, 20).Value = CountRIPAddress;
          _cmd.Parameters.Add("@pCountREnabled", SqlDbType.Bit).Value = CountRBusinessLogic.ENABLED_COUNTR.TRUE;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            return ((Int32)_obj == 1);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("CountR.DB_CheckCountR -> Code: {0}", CountRCode));
        Log.Exception(_ex);
      }

      return false;
    } // DB_CheckCountR

    /// <summary>
    /// Set status to offline  if last connection is bigger than general param 
    /// </summary>
    /// <returns></returns>
    public static Boolean DB_SetStatusOffLine()
    {
      StringBuilder _sb;
      Int32 _interval_offline;

      try
      {
        _interval_offline = GeneralParam.GetInt32("CountR", "Offline.Interval", 45);

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   COUNTR                                                     ");
        _sb.AppendLine("    SET   CR_STATUS          = @pStatus                              ");
        _sb.AppendLine("        , CR_LAST_MODIFIED   = GETDATE()                             ");
        _sb.AppendLine("  WHERE   CR_LAST_CONNECTION < DATEADD(SECOND,-@pInterval,GETDATE()) ");
        _sb.AppendLine("     OR   CR_LAST_CONNECTION IS NULL                                 ");

        using (DB_TRX _db_trx = new DB_TRX())
        {

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pInterval", SqlDbType.Int).Value = _interval_offline;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)CountRBusinessLogic.COUNTR_STATUS.OFFLINE;

            if (_cmd.ExecuteNonQuery() > 0)
            {
              _db_trx.Commit();
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("CountR.DB_SetStatusOffLine");
        Log.Exception(_ex);
      }

      return false;
    } // DB_SetStatusOffLine

    /// <summary>
    /// Get a List of CountR form a specific filter
    /// </summary>
    /// <param name="Filter"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    /// 
    public List<CountR> DB_GetCountRs(String Filter, SqlTransaction SqlTrx)
    {
      List<CountR> _list_countr;

      _list_countr = new List<CountR>();

      return _list_countr;
    } // DB_GetCountRs

    /// <summary>
    /// Insert an specific countR in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   COUNTR             ");
        _sb.AppendLine("             ( CR_CODE            ");
        _sb.AppendLine("             , CR_NAME            ");
        _sb.AppendLine("             , CR_DESCRIPTION     ");
        _sb.AppendLine("             , CR_PROVIDER        ");
        _sb.AppendLine("             , CR_AREA_ID         ");
        _sb.AppendLine("             , CR_BANK_ID         ");
        _sb.AppendLine("             , CR_FLOOR_ID        ");
        _sb.AppendLine("             , CR_MAX_PAYMENT     ");
        _sb.AppendLine("             , CR_MIN_PAYMENT     ");
        _sb.AppendLine("             , CR_CURRENT_ISOCODE ");
        _sb.AppendLine("             , CR_SHOW_LOG        ");
        _sb.AppendLine("             , CR_CREATE_TICKET   ");
        _sb.AppendLine("             , CR_ENABLED         ");
        _sb.AppendLine("             , CR_CREATED         ");
        _sb.AppendLine("             , CR_IP_ADDRESS      ");
        _sb.AppendLine("             , CR_LAST_MODIFIED   ");
        _sb.AppendLine("             , CR_STATUS        ) ");
        _sb.AppendLine("      VALUES                      ");
        _sb.AppendLine("             ( @pCode             ");
        _sb.AppendLine("             , @pName             ");
        _sb.AppendLine("             , @pDescription      ");
        _sb.AppendLine("             , @pProvider         ");
        _sb.AppendLine("             , @pAreaId           ");
        _sb.AppendLine("             , @pBankId           ");
        _sb.AppendLine("             , @pFloorId          ");
        _sb.AppendLine("             , @pMaxPayment       ");
        _sb.AppendLine("             , @pMinPayment       ");
        _sb.AppendLine("             , @pIsoCode          ");
        _sb.AppendLine("             , @pShowLog          ");
        _sb.AppendLine("             , @pCreateTicket     ");
        _sb.AppendLine("             , @pEnabled          ");
        _sb.AppendLine("             , @pCreated          ");
        _sb.AppendLine("             , @pIpAddress        ");
        _sb.AppendLine("             , @pLastModified     ");
        _sb.AppendLine("             , @pStatus       )   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = Code;
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Name;
          _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Description; //Description
          _cmd.Parameters.Add("@pProvider", SqlDbType.NVarChar).Value = Provider;  //Provider;
          _cmd.Parameters.Add("@pAreaId", SqlDbType.BigInt).Value = AreaId;
          _cmd.Parameters.Add("@pBankId", SqlDbType.BigInt).Value = BankId;
          _cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).Value = FloorId;  //FloorId;;
          _cmd.Parameters.Add("@pMaxPayment", SqlDbType.Money).Value = (Decimal)MaxPayment;
          _cmd.Parameters.Add("@pMinPayment", SqlDbType.Money).Value = (Decimal)MinPayment;
          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IsoCode;  //IsoCode ;
          _cmd.Parameters.Add("@pShowLog", SqlDbType.Bit).Value = ShowLog;
          _cmd.Parameters.Add("@pCreateTicket", SqlDbType.Bit).Value = CreateTicket;
          _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Enabled;
          _cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pIpAddress", SqlDbType.NVarChar).Value = IpAddress;  //IpAddress;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.DB_Insert -> Code: {0}", Code));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific countR in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      int _cashier_id;

      try
      {

        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   COUNTR SET                               ");
        _sb.AppendLine("          CR_CODE               = @pCode           ");
        _sb.AppendLine("        , CR_NAME               = @pName           ");
        _sb.AppendLine("        , CR_DESCRIPTION        = @pDescription    ");
        _sb.AppendLine("        , CR_PROVIDER           = @pProvider       ");
        _sb.AppendLine("        , CR_AREA_ID            = @pAreaId         ");
        _sb.AppendLine("        , CR_BANK_ID            = @pBankId         ");
        _sb.AppendLine("        , CR_FLOOR_ID           = @pFloorId        ");
        _sb.AppendLine("        , CR_MAX_PAYMENT        = @pMaxPayment     ");
        _sb.AppendLine("        , CR_MIN_PAYMENT        = @pMinPayment     ");
        _sb.AppendLine("        , CR_CURRENT_ISOCODE    = @pIsoCode        ");
        _sb.AppendLine("        , CR_SHOW_LOG           = @pShowLog        ");
        _sb.AppendLine("        , CR_CREATE_TICKET      = @pCreateTicket   ");
        _sb.AppendLine("        , CR_ENABLED            = @pEnabled        ");
        _sb.AppendLine("        , CR_RETIREMENT_DATE    = @pRetirementDate ");
        _sb.AppendLine("        , CR_RETIREMENT_USER_ID = @pRetirementUser ");
        _sb.AppendLine("        , CR_LAST_MODIFIED      = @pLastModified   ");
        _sb.AppendLine("        , CR_IP_ADDRESS         = @pIpAddress      ");
        _sb.AppendLine("  WHERE   CR_COUNTR_ID          = @pCountrId       ");



        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCountrId", SqlDbType.Int).Value = Id;
          _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = Code;
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Name;
          _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).Value = Description; //Description
          _cmd.Parameters.Add("@pProvider", SqlDbType.NVarChar).Value = Provider;  //Provider;;
          _cmd.Parameters.Add("@pAreaId", SqlDbType.BigInt).Value = AreaId;
          _cmd.Parameters.Add("@pBankId", SqlDbType.BigInt).Value = BankId;
          _cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).Value = FloorId;  //FloorId
          _cmd.Parameters.Add("@pMaxPayment", SqlDbType.Money).Value = (Decimal)MaxPayment;
          _cmd.Parameters.Add("@pMinPayment", SqlDbType.Money).Value = (Decimal)MinPayment;
          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IsoCode;  //IsoCode ;
          _cmd.Parameters.Add("@pShowLog", SqlDbType.Bit).Value = ShowLog;
          _cmd.Parameters.Add("@pCreateTicket", SqlDbType.Bit).Value = CreateTicket;
          _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Enabled;
          _cmd.Parameters.Add("@pIpAddress", SqlDbType.NVarChar).Value = IpAddress;

          if (RetirementDate == DateTime.MinValue)
          {
            _cmd.Parameters.Add("@pRetirementDate", SqlDbType.DateTime).Value = DBNull.Value;
            _cmd.Parameters.Add("@pRetirementUser", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pRetirementDate", SqlDbType.DateTime).Value = RetirementDate;
            _cmd.Parameters.Add("@pRetirementUser", SqlDbType.BigInt).Value = RetirementUserId;
          }

          if (_cmd.ExecuteNonQuery() == 1)
          {
            _cashier_id = 0;

            if (Cashier.GetCashierTerminalIdFromCountR(Id, out _cashier_id, SqlTrx))
            {
              if (!(_cashier_id > 0) && UpdateCashierTerminalName(_cashier_id, Name, SqlTrx))
              {
                return false;
              }
            }
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.DB_Update -> CountRId: {0}", Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Update

    /// <summary>
    /// Delete an specific CountR from database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_Delete(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" DELETE                             ");
        _sb.AppendLine("   FROM   COUNTR                    ");
        _sb.AppendLine("  WHERE   CR_COUNTR_ID = @pCountrId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCountrId", SqlDbType.Int).Value = this.Id;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.DB_Delete -> CountRId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Delete

    /// <summary>
    /// Check whether there is no other CountR with the same name or Code
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean CheckDuplicades(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   COUNT(CR_COUNTR_ID)         ");
        _sb.AppendLine("   FROM   COUNTR                      ");
        _sb.AppendLine("  WHERE   (CR_CODE       = @pCode     ");
        _sb.AppendLine("     OR    CR_NAME       = @pName)    ");
        _sb.AppendLine("    AND   CR_COUNTR_ID  <> @pCountrId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = Code;
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Name;
          _cmd.Parameters.Add("@pCountrId", SqlDbType.Int).Value = Id;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            return ((Int32)_obj > 0);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.CheckDuplicades -> CountRId: {0}, Code: {1}, Name: {2}", Id, Code, Name));
        Log.Exception(_ex);
      }

      return false;
    } // CheckDuplicades

    /// <summary>
    /// Insert CountR Log
    /// </summary>
    /// <param name="Code"></param>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="Tick"></param>
    /// <returns></returns>
    public static Boolean InsertCountRLog(Int32 Code, String Request, String Response, Int32 Tick)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   COUNTR_LOG       ");
        _sb.AppendLine("             ( CRL_CODE         ");
        _sb.AppendLine("             , CRL_REQUEST      ");
        _sb.AppendLine("             , CRL_RESPONSE     ");
        _sb.AppendLine("             , CRL_ELAPSED_TIME ");
        _sb.AppendLine("             , CRL_DATETIME )   ");
        _sb.AppendLine("      VALUES                    ");
        _sb.AppendLine("             ( @pCode           ");
        _sb.AppendLine("             , @pRequest        ");
        _sb.AppendLine("             , @pResponse       ");
        _sb.AppendLine("             , @pElapsedTime    ");
        _sb.AppendLine("             , @pDatetime )     ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = Code;
            _cmd.Parameters.Add("@pRequest", SqlDbType.NVarChar).Value = Request;
            _cmd.Parameters.Add("@pResponse", SqlDbType.NVarChar).Value = Response;
            _cmd.Parameters.Add("@pElapsedTime", SqlDbType.Int).Value = Tick;
            _cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = WGDB.Now;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.InsertCountRLog -> Code: {0}", Code));
        Log.Exception(_ex);
      }

      return false;
    } // InsertCountRLog

    /// <summary>
    /// Checks if CountR show log is enabled
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <returns></returns>
    public static Boolean CheckCountRShowLog(Int32 CountRCode)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CR_SHOW_LOG                  ");
        _sb.AppendLine("   FROM   COUNTR                       ");
        _sb.AppendLine("  WHERE   CR_CODE    = @pCountrCode    ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCountrCode", SqlDbType.Int).Value = CountRCode;

            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              return ((Boolean)_obj);
            }
            else
            {
              return true; // If not exists, show Log always
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("CountR.CheckCountRShowLog -> Code: {0}", CountRCode));
        Log.Exception(_ex);
      }

      return false;
    } // CheckCountRShowLog

    /// <summary>
    /// Checks if the specified countr allows to create tickets
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <returns></returns>
    public static Boolean AllowsCreateTicket(Int32 CountRCode)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CR_CREATE_TICKET             ");
        _sb.AppendLine("   FROM   COUNTR                       ");
        _sb.AppendLine("  WHERE   CR_CODE    = @pCountrCode    ");
        _sb.AppendLine("    AND   CR_ENABLED = @pCountREnabled ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCountrCode", SqlDbType.Int).Value = CountRCode;
            _cmd.Parameters.Add("@pCountREnabled", SqlDbType.Bit).Value = CountRBusinessLogic.ENABLED_COUNTR.TRUE;

            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              return ((Boolean)_obj);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("CountR.AllowsCreateTicket -> Code: {0}", CountRCode));
        Log.Exception(_ex);
      }

      return false;
    } // AllowsCreateTicket

    /// <summary>
    /// Update Alarm Group for CountR
    /// </summary>
    /// <returns></returns>
    public static Boolean UpdateAlarmGroupCountR()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   ALARM_GROUPS                           ");
        _sb.AppendLine("    SET   ALG_VISIBLE           = @pGroupEnabled ");
        _sb.AppendLine("  WHERE   ALG_ALARM_GROUP_ID    = 5              ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pGroupEnabled", SqlDbType.Bit).Value = Misc.IsCountREnabled();

            if (_cmd.ExecuteNonQuery() == 2)
            {
              _db_trx.Commit();

              return true;
            }
            else
            {
              Log.Warning(String.Format("Dont Update: CountR.UpdateAlarmGroupCountR -> GroupEnabled: {0}", Misc.IsCountREnabled()));
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.UpdateAlarmGroupCountR -> GroupEnabled: {0}", Misc.IsCountREnabled()));
        Log.Exception(_ex);
      }

      return false;
    } // UpdateAlarmGroupCountR

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Loads CountR info from SQLReader
    /// </summary>
    /// <param name="CountR"></param>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private static CountR LoadCountRFromReader(CountR CountR, SqlDataReader SqlReader)
    {
      CountR = new CountR();

      try
      {
        CountR.Id = SqlReader.GetInt32(0);
        CountR.Code = SqlReader.GetInt32(1);
        CountR.Name = SqlReader.IsDBNull(2) ? String.Empty : SqlReader.GetString(2);
        CountR.Description = SqlReader.IsDBNull(3) ? String.Empty : SqlReader.GetString(3);
        CountR.Provider = SqlReader.IsDBNull(4) ? String.Empty : SqlReader.GetString(4);
        CountR.AreaId = SqlReader.IsDBNull(5) ? 0 : (int)SqlReader[5];
        CountR.AreaName = SqlReader.IsDBNull(6) ? String.Empty : SqlReader.GetString(6);
        CountR.Zone = SqlReader.IsDBNull(7) ? false : true;
        CountR.BankId = SqlReader.IsDBNull(8) ? 0 : (int)SqlReader[8]; ;
        CountR.BankName = SqlReader.IsDBNull(9) ? String.Empty : SqlReader.GetString(9);
        CountR.FloorId = SqlReader.IsDBNull(10) ? String.Empty : SqlReader.GetString(10);
        CountR.MaxPayment = SqlReader.IsDBNull(11) ? 0 : SqlReader.GetDecimal(11);
        CountR.MinPayment = SqlReader.IsDBNull(12) ? 0 : SqlReader.GetDecimal(12);
        CountR.IsoCode = SqlReader.IsDBNull(13) ? String.Empty : SqlReader.GetString(13);
        CountR.Enabled = SqlReader.GetBoolean(14);
        CountR.Created = SqlReader.IsDBNull(15) ? DateTime.MinValue : SqlReader.GetDateTime(15);
        CountR.LastModified = SqlReader.IsDBNull(16) ? DateTime.MinValue : SqlReader.GetDateTime(16);
        CountR.RetirementDate = SqlReader.IsDBNull(17) ? DateTime.MinValue : SqlReader.GetDateTime(17);
        CountR.RetirementUserId = SqlReader.IsDBNull(18) ? 0 : SqlReader.GetInt64(18);
        CountR.RetirementUserName = SqlReader.IsDBNull(19) ? String.Empty : SqlReader.GetString(19);
        CountR.LastConnection = SqlReader.IsDBNull(20) ? DateTime.MinValue : SqlReader.GetDateTime(20);
        CountR.Status = SqlReader.IsDBNull(21) ? 0 : (int)SqlReader[21];
        CountR.IpAddress = SqlReader.IsDBNull(22) ? String.Empty : SqlReader.GetString(22);
        CountR.ShowLog = SqlReader.GetBoolean(23);
        CountR.CreateTicket = SqlReader.GetBoolean(24);
      }
      catch (Exception _ex)
      {
        Log.Error("CountR LoadCountRFromReader");
        Log.Exception(_ex);
      }

      return CountR;

    } // LoadCountRFromReader

    /// <summary>
    /// Update the name in cashier_terminals table
    /// </summary>
    /// <param name="CountRId"></param>
    /// <param name="Name"></param>
    /// <param name="SqlTrx"></param>
    private Boolean UpdateCashierTerminalName(Int32 CashierId, String Name, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE  CASHIER_TERMINALS                  ");
        _sb.AppendLine("    SET   CT_NAME             = @pName       ");
        _sb.AppendLine("  WHERE   CT_CASHIER_ID       = @pCashierId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CashierId;
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Name;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.UpdateCashierTerminalName -> CahierId: {0}, Name: {1}", CashierId, Name));
        Log.Exception(_ex);
      }

      return false;
    } // UpdateCashierTerminalName

    #endregion " Private Methods "
  }
}
