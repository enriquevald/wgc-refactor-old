﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CountRSession.cs
// 
//   DESCRIPTION: Miscellaneous utilities for CountRSessions
// 
//        AUTHOR: Yineth Nuñez
// 
// CREATION DATE: 07-APR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-APR-2016 YNM    First release (Header).
// 11-JUL-2016 JBP    Bug 15315:CountR: no se puede cerrar una sesión de caja de countr teniendo el kiosco deshabilitado
// 15-JUL-2016 JBP    Bug 15409:CountR: No se insertan los registros en "Faltantes y sobrantes"
//                    Bug 15626:CountR: No se actualiza la sesión de CountR
// 03-AUG-2016 JBP    Bug 17167:CountR: Duplicado de sesiones y otras inconsistencias
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.CountR
{
  public class CountRSession
  {
    #region " Properties "

    public Int64 Id { get; set; }
    public Int32 CountRId { get; set; }
    public Int64 CashierSesionId { get; set; }
    public CASHIER_SESSION_STATUS CountRSessionStatus { get; set; }
    public Currency InitialAmount { get; set; }
    public Currency FinalAmount { get; set; }
    public Currency CollectedAmount { get; set; }
    public Currency WithdrawalsAmount { get; set; }
    public Currency DepositsAmount { get; set; }
    public Currency TicketAmount { get; set; }
    public DateTime OpenningDate { get; set; }
    public DateTime ClosedDate { get; set; }
    public Currency ExpectedAmount { get; set; }

    #endregion " Properties "

    #region " Enums "

    private enum ICountId
    {
      COUNTR_ID = 0,
      COUNTR_SESSION_ID = 1,
      CASHIER_SESSION_ID = 2
    }

    #endregion " Enums "

    #region " Public Methods "

    ///// <summary>
    ///// Get OpenSession for a given CountR
    ///// </summary>
    ///// <param name="CountRId"></param>
    ///// <param name="CountRSessionId"></param>
    ///// <param name="CashierSessionId"></param>
    ///// <param name="SqlTrx"></param>
    ///// <returns></returns>
    //public CountRSession DB_GetSession(Int32 CountRId, Int64 CountRSessionId, Int64 CashierSessionId, SqlTransaction SqlTrx)
    //{
    //  return DB_GetSession(true, CountRId, CountRSessionId, CashierSessionId, SqlTrx);
    //}

    /// <summary>
    /// Get OpenSession for a given CountR
    /// </summary>
    /// <param name="CountRId"></param>
    /// <param name="CountRSessionId"></param>
    /// <param name="CashierSessionId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public CountRSession DB_GetOpenSession(Int32 CountRId, Int64 CountRSessionId, Int64 CashierSessionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      CountRSession _countr_session;

      _countr_session = null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   CRS_SESSION_ID                                                               "); // 0
        _sb.AppendLine("         , CRS_COUNTR_ID                                                                "); // 1
        _sb.AppendLine("         , CRS_CASHIER_SESSION_ID                                                       "); // 2
        _sb.AppendLine("         , CRS_INITIAL_AMOUNT                                                           "); // 3
        _sb.AppendLine("         , CRS_FINAL_AMOUNT                                                             "); // 4
        _sb.AppendLine("         , CRS_COLLECTED_AMOUNT                                                         "); // 5
        _sb.AppendLine("         , CRS_OPENNING_DATE                                                            "); // 6
        _sb.AppendLine("         , CRS_CLOSED_DATE                                                              "); // 7
        _sb.AppendLine("         , CRS_WITHDRAWALS_AMOUNT                                                       "); // 8
        _sb.AppendLine("         , CRS_DEPOSITS_AMOUNT                                                          "); // 9
        _sb.AppendLine("         , CRS_STATUS                                                                   "); // 10
        _sb.AppendLine("         , ISNULL(CRS_FINAL_AMOUNT, 0) - ISNULL(CRS_TICKET_AMOUNT, 0) AS EXPECTED_AMOUNT"); // 11
        _sb.AppendLine("    FROM   COUNTR_SESSIONS        ");
        _sb.AppendLine("   WHERE                          ");

        if (CountRId > 0)
        {
          _sb.AppendLine("      CRS_COUNTR_ID = @pCountrId   ");
          _sb.AppendLine("  AND CRS_STATUS    = @pOpenStatus ");
        }

        if (CountRSessionId > 0)
        {
          _sb.AppendLine("      CRS_SESSION_ID = @pCountrSessionId ");
          _sb.AppendLine("  AND CRS_STATUS     = @pOpenStatus      ");
        }

        if (CashierSessionId > 0)
        {
          _sb.AppendLine("      CRS_CASHIER_SESSION_ID = @pCashierSessionId ");
          _sb.AppendLine("  AND CRS_STATUS             = @pOpenStatus       ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          if (CountRId > 0)
          {
            _cmd.Parameters.Add("@pCountrId", SqlDbType.Int).Value = CountRId;
            _cmd.Parameters.Add("@pOpenStatus", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          }

          if (CountRSessionId > 0)
          {
            _cmd.Parameters.Add("@pCountrSessionId", SqlDbType.BigInt).Value = CountRSessionId;
            _cmd.Parameters.Add("@pOpenStatus", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          }

          if (CashierSessionId > 0)
          {
            _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
            _cmd.Parameters.Add("@pOpenStatus", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          }

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              _countr_session = new CountRSession();

              _countr_session.Id = _sql_reader.GetInt64(0);
              _countr_session.CountRId = _sql_reader.GetInt32(1);
              _countr_session.CashierSesionId = _sql_reader.GetInt64(2);
              _countr_session.InitialAmount = _sql_reader.IsDBNull(3) ? 0 : _sql_reader.GetDecimal(3);
              _countr_session.FinalAmount = _sql_reader.IsDBNull(4) ? 0 : _sql_reader.GetDecimal(4);
              _countr_session.CollectedAmount = _sql_reader.IsDBNull(5) ? 0 : _sql_reader.GetDecimal(5);
              _countr_session.OpenningDate = _sql_reader.IsDBNull(6) ? DateTime.MinValue : _sql_reader.GetDateTime(6);
              _countr_session.ClosedDate = _sql_reader.IsDBNull(7) ? DateTime.MinValue : _sql_reader.GetDateTime(7);
              _countr_session.WithdrawalsAmount = _sql_reader.IsDBNull(8) ? 0 : _sql_reader.GetDecimal(8);
              _countr_session.DepositsAmount = _sql_reader.IsDBNull(9) ? 0 : _sql_reader.GetDecimal(9);
              _countr_session.CountRSessionStatus = _sql_reader.IsDBNull(10) ? CASHIER_SESSION_STATUS.OPEN : (CASHIER_SESSION_STATUS)_sql_reader.GetInt32(10);
              _countr_session.ExpectedAmount = _sql_reader.GetDecimal(11);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountRSession.DB_GetOpenSession -> CountRId: {0}, CountRSessionId: {1}, CashierSessionId: {2}", CountRId, CountRSessionId, CashierSessionId));
        Log.Exception(_ex);
      }

      return _countr_session;
    } // DB_GetOpenSession

    /// <summary>
    /// Insert CountR Session
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="CountrId"></param>
    /// <returns></returns>
    public static Boolean IsCountRSession(Int64 CashierSessionId, out Int32 CountrId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!DB_IsCountrSession(CashierSessionId, out CountrId, _db_trx.SqlTransaction))
        {
          return false;
        }
      }

      return true;
    } // IsCountRSession

    /// <summary>
    /// Create a CountR Open Session
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _param;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   COUNTR_SESSIONS        ");
        _sb.AppendLine("             ( CRS_COUNTR_ID          "); // 0
        _sb.AppendLine("             , CRS_CASHIER_SESSION_ID "); // 1
        _sb.AppendLine("             , CRS_OPENNING_DATE      "); // 2
        _sb.AppendLine("             , CRS_INITIAL_AMOUNT     "); // 3
        _sb.AppendLine("             , CRS_STATUS             "); // 4 
        _sb.AppendLine("             , CRS_FINAL_AMOUNT       "); // 5 
        _sb.AppendLine("             , CRS_COLLECTED_AMOUNT   "); // 6 
        _sb.AppendLine("             , CRS_WITHDRAWALS_AMOUNT "); // 7 
        _sb.AppendLine("             , CRS_DEPOSITS_AMOUNT    "); // 8 
        _sb.AppendLine("             , CRS_TICKET_AMOUNT )    "); // 9 
        _sb.AppendLine("      VALUES                          ");
        _sb.AppendLine("             ( @pCountRId             ");
        _sb.AppendLine("             , @pCashierSessionId     ");
        _sb.AppendLine("             , @pOpenningDate         ");
        _sb.AppendLine("             , @pInitialAmount        ");
        _sb.AppendLine("             , @pStatus               ");
        _sb.AppendLine("             , 0                      ");
        _sb.AppendLine("             , 0                      ");
        _sb.AppendLine("             , 0                      ");
        _sb.AppendLine("             , 0                      ");
        _sb.AppendLine("             , 0 )                    ");
        _sb.AppendLine("SET @pCountrSessionId = SCOPE_IDENTITY()");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCountRId", SqlDbType.Int).Value = CountRId;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSesionId;
          _cmd.Parameters.Add("@pOpenningDate", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pInitialAmount ", SqlDbType.Money).Value = (Decimal)InitialAmount;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)CASHIER_SESSION_STATUS.OPEN;

          _param = _cmd.Parameters.Add("@pCountrSessionId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() > 0)
          {
            Id = (Int64)_param.Value;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountRSession.DB_Insert -> CountRId: {0}, CashierSessionId: {1}", CountRId, CashierSesionId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update session with ticket increment
    /// </summary>
    /// <param name="TicketAmount"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean UpdateSession_Ticket(Decimal TicketAmount, SqlTransaction SqlTrx)
    {
      return this.DB_Update(false, 0, 0, TicketAmount, 0, SqlTrx);
    }

    /// <summary>
    /// Update session with withdrawal, deposit or collect increment
    /// </summary>
    /// <param name="WithdrawalIncrement"></param>
    /// <param name="DepositIncrement"></param>
    /// <param name="CollectedAmount"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean UpdateSession_Movement(Currency WithdrawalIncrement, Currency DepositIncrement, SqlTransaction SqlTrx)
    {
      return this.DB_Update(false, WithdrawalIncrement, DepositIncrement, 0, 0, SqlTrx);
    }

    /// <summary>
    /// Update CollectedAmount and close session
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean UpdateSession_Collected(Currency CollectedAmount, SqlTransaction SqlTrx)
    {
      return this.DB_Update(true, 0, 0, 0, CollectedAmount, SqlTrx);
    }

    /// <summary>
    /// Undo CollectedAmount
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean UpdateSession_UndoCollected(SqlTransaction SqlTrx)
    {
      return this.DB_Update(false, 0, 0, 0, 0, SqlTrx);
    }

    /// <summary>
    /// Update CollectedAmount and close session
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean UpdateSession_Close(SqlTransaction SqlTrx)
    {
      return this.DB_Update(true, 0, 0, 0, 0, SqlTrx);
    }

    /// <summary>
    /// Update CountR Session
    /// </summary>
    /// <param name="isClosing"></param>
    /// <param name="WithdrawalIncrement"></param>
    /// <param name="DepositIncrement"></param>
    /// <param name="TicketIncrement"></param>
    /// <param name="CollectedAmount"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_Update(Boolean isClosing, Currency WithdrawalIncrement, Currency DepositIncrement, Currency TicketIncrement, Currency CollectedAmount, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DateTime _closing_date;

      _closing_date = DateTime.MinValue;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("        UPDATE   COUNTR_SESSIONS SET                                                              ");
        _sb.AppendLine("                 CRS_FINAL_AMOUNT       = CRS_FINAL_AMOUNT       + @pDIncements - @pWAIncrements  ");
        _sb.AppendLine("               , CRS_WITHDRAWALS_AMOUNT = CRS_WITHDRAWALS_AMOUNT + @pWAIncrements                 ");
        _sb.AppendLine("               , CRS_DEPOSITS_AMOUNT    = CRS_DEPOSITS_AMOUNT    + @pDIncements                   ");
        _sb.AppendLine("               , CRS_COLLECTED_AMOUNT   = @pCollectAmount                                         ");
        _sb.AppendLine("               , CRS_TICKET_AMOUNT      = CRS_TICKET_AMOUNT      + @pTicketAmount                 ");
        _sb.AppendLine((isClosing) ? " , CRS_CLOSED_DATE        = @pCloseDate                                             " : String.Empty);
        _sb.AppendLine((isClosing) ? " , CRS_STATUS             = @pStatus                                                " : String.Empty);
        _sb.AppendLine("         WHERE                                                                                    ");

        if (Id != 0)
        {
          _sb.AppendLine("               CRS_SESSION_ID = @pCountrSessionId                                               ");
        }
        else
        {
          _sb.AppendLine("               CRS_COUNTR_ID          = @pCountrId                                              ");
          _sb.AppendLine("         AND   CRS_CASHIER_SESSION_ID = @pCashierSessionId                                      ");
        }

        //_sb.AppendLine("           AND   CRS_STATUS = @pStatusOpen                                                            ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pDIncements", SqlDbType.Money).Value = (Decimal)DepositIncrement;
          _cmd.Parameters.Add("@pWAIncrements", SqlDbType.Money).Value = (Decimal)WithdrawalIncrement;
          _cmd.Parameters.Add("@pCollectAmount", SqlDbType.Money).Value = (Decimal)CollectedAmount;
          _cmd.Parameters.Add("@pTicketAmount", SqlDbType.Money).Value = (Decimal)TicketIncrement;

          if (isClosing)
          {
            _closing_date = WGDB.Now;
            _cmd.Parameters.Add("@pCloseDate", SqlDbType.DateTime).Value = _closing_date;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.CLOSED;
          }

          if (Id != 0)
          {
            _cmd.Parameters.Add("@pCountrSessionId", SqlDbType.BigInt).Value = Id;
          }
          else
          {
            _cmd.Parameters.Add("@pCountrId", SqlDbType.BigInt).Value = CountRId;
            _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSesionId;
          }

          if (_cmd.ExecuteNonQuery() > 0)
          {
            FinalAmount       += (DepositIncrement - WithdrawalIncrement);
            DepositsAmount    += DepositIncrement;
            WithdrawalsAmount += WithdrawalIncrement;
            TicketAmount      += TicketIncrement;

            if (isClosing)
            {
              CountRSessionStatus = CASHIER_SESSION_STATUS.CLOSED;
              ClosedDate = _closing_date;
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountRSession.DB_Update: CountRSessionId: {0}, CountRId: {1}, CashierSessionId: {2}", Id, CountRId, CashierSesionId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Update

    //------------------------------------------------------------------------------
    // PURPOSE: Get a CountR session by id
    //
    //  PARAMS:
    //      - INPUT:
    //          - CountRSessionId
    //          
    //      - OUTPUT:
    //          -  CountRSession 
    //
    // RETURNS: Boolean (TRUE if operation was successful, FALSE if there were any errors)
    public static Boolean GetCountRSessionById(Int64 CountRSessionId, out CountRSession CountRSession, out CashierSessionInfo CashierSessionInfo, SqlTransaction SqlTrx)
    {      
      StringBuilder _sb = new StringBuilder();
      CountRSession = new CountRSession();
      CashierSessionInfo = new CashierSessionInfo();
      Boolean _valid_session;
      String _expected_amount;

      try
      {
        _valid_session = false;

        if (CountRSessionId <= 0)
        {
          CountRSession = null;
          return false;
        }

        _expected_amount = "ISNULL(CRS_FINAL_AMOUNT, 0) - ISNULL(CRS_TICKET_AMOUNT, 0)";

        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   CRS_SESSION_ID                               "); // 0
        _sb.AppendLine("         , CRS_COUNTR_ID                                "); // 1
        _sb.AppendLine("         , CRS_CASHIER_SESSION_ID                       "); // 2
        _sb.AppendLine("         , CRS_INITIAL_AMOUNT                           "); // 3
        _sb.AppendLine("         , CRS_FINAL_AMOUNT                             "); // 4
        _sb.AppendLine("         , CRS_COLLECTED_AMOUNT                         "); // 5
        _sb.AppendLine("         , CRS_OPENNING_DATE                            "); // 6
        _sb.AppendLine("         , CRS_CLOSED_DATE                              "); // 7
        _sb.AppendLine("         , CRS_WITHDRAWALS_AMOUNT                       "); // 8
        _sb.AppendLine("         , CRS_DEPOSITS_AMOUNT                          "); // 9
        _sb.AppendLine("         , CRS_STATUS                                   "); // 10
        _sb.AppendLine("         , " + _expected_amount + " AS EXPECTED_AMOUNT  "); // 11
        _sb.AppendLine("    FROM   COUNTR_SESSIONS                              ");
        _sb.AppendLine("   WHERE   CRS_CASHIER_SESSION_ID = @pCountrSessionId   ");
        
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCountrSessionId", SqlDbType.BigInt).Value = CountRSessionId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              CountRSession = new CountRSession();

              CountRSession.Id = _sql_reader.GetInt64(0);
              CountRSession.CountRId = _sql_reader.GetInt32(1);
              CountRSession.CashierSesionId = _sql_reader.GetInt64(2);
              CountRSession.InitialAmount = _sql_reader.IsDBNull(3) ? 0 : _sql_reader.GetDecimal(3);
              CountRSession.FinalAmount = _sql_reader.IsDBNull(4) ? 0 : _sql_reader.GetDecimal(4);
              CountRSession.CollectedAmount = _sql_reader.IsDBNull(5) ? 0 : _sql_reader.GetDecimal(5);
              CountRSession.OpenningDate = _sql_reader.IsDBNull(6) ? DateTime.MinValue : _sql_reader.GetDateTime(6);
              CountRSession.ClosedDate = _sql_reader.IsDBNull(7) ? DateTime.MinValue : _sql_reader.GetDateTime(7);
              CountRSession.WithdrawalsAmount = _sql_reader.IsDBNull(8) ? 0 : _sql_reader.GetDecimal(8);
              CountRSession.DepositsAmount = _sql_reader.IsDBNull(9) ? 0 : _sql_reader.GetDecimal(9);
              CountRSession.CountRSessionStatus = _sql_reader.IsDBNull(10) ? CASHIER_SESSION_STATUS.OPEN : (CASHIER_SESSION_STATUS)_sql_reader.GetInt32(10);
              CountRSession.ExpectedAmount = _sql_reader.GetDecimal(11);

              _valid_session = true;
            }
          }
          
          // Gets cashier Session info by id
          if(_valid_session)
          {
            _valid_session = Cashier.GetCashierSessionById(CountRSession.CashierSesionId, out CashierSessionInfo, SqlTrx);
          }

          return _valid_session;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // GetCashierSessionById
    /// <summary>
    /// Look for an Open Session, if not find then creates it 
    /// </summary>
    /// <param name="CountRId"></param>
    /// <param name="Csi"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static CountRSession GetorOpenCountRSession(Int32 CountRId, CashierSessionInfo Csi, SqlTransaction SqlTrx)
    {
      CountRSession _crs;

      _crs = new CountRSession();
      _crs = _crs.DB_GetOpenSession(CountRId, 0, 0, SqlTrx);

      if (_crs == null || _crs.CountRSessionStatus != CASHIER_SESSION_STATUS.OPEN)
      {
        _crs = new CountRSession();
        _crs.CountRId = CountRId;
        _crs.CashierSesionId = Csi.CashierSessionId;

        if (!_crs.DB_Insert(SqlTrx))
        {
          return null;
        }

        _crs = _crs.DB_GetOpenSession(0, _crs.Id, 0, SqlTrx);
      }

      return _crs;
    } // GetorOpenCountRSession


    /// <summary>
    /// Look for an existing open CountR Session
    /// </summary>
    /// <param name="CountRId"></param>
    /// <returns></returns>
    public static Boolean ExistOpenCountRSession(Int32 CountRId)
    {
      using (DB_TRX _sql_trx = new DB_TRX())
      {
        return ExistOpenCountRSession(CountRId, _sql_trx.SqlTransaction);
      }
    }

    /// <summary>
    /// Look for an existing open CountR Session
    /// </summary>
    /// <param name="CountRId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean ExistOpenCountRSession(Int32 CountRId, SqlTransaction SqlTrx)
    {
      CountRSession _crs;

      if (CountRId <= 0)
      {
        return false;
      }

      _crs = new CountRSession();
      _crs = _crs.DB_GetOpenSession(CountRId, 0, 0, SqlTrx);

      return (_crs != null);
    } // ExistOpenCountRSession

    /// <summary>
    /// Close open session
    /// </summary>
    /// <param name="CountRSessionId"></param>
    /// <param name="CollectedAmount"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean CloseCountRSession(Int64 CountRSessionId, SqlTransaction SqlTrx)
    {
      CountRSession _crs;

      _crs = new CountRSession();
      _crs = _crs.DB_GetOpenSession(0, CountRSessionId, 0, SqlTrx);

      if (_crs == null)
      {
        throw new Exception(String.Format("CloseCountRSession: Could not find a CountRSession for the given Id. ID: {1}", CountRSessionId.ToString()));
      }

      if (_crs.ClosedDate != DateTime.MinValue)
      {
        throw new Exception(String.Format("CloseCountRSession: The CountRSession is already closed on {1}", _crs.ClosedDate.ToString()));
      }

      return _crs.UpdateSession_Close(SqlTrx);
    } // CloseCountRSession

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Check CountR Session
    /// </summary>
    /// <param name="CashierSessionID"></param>
    /// <param name="CountrID"></param>
    /// <param name="SqlTransaction"></param>
    /// <returns></returns>
    private static Boolean DB_IsCountrSession(Int64 CashierSessionID, out Int32 CountrID, SqlTransaction SqlTransaction)
    {
      StringBuilder _str;
      Int32 _value;

      _value = 0;
      CountrID = 0;

      try
      {
        _str = new StringBuilder();
        _str.AppendLine(" SELECT   CRS_COUNTR_ID                               ");
        _str.AppendLine("   FROM   COUNTR_SESSIONS                             ");
        _str.AppendLine("  WHERE   CRS_CASHIER_SESSION_ID = @pCashierSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_str.ToString(), SqlTransaction.Connection, SqlTransaction))
        {
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionID;

          _value = Convert.ToInt32(_cmd.ExecuteScalar());

          if (_value > 0)
          {
            CountrID = _value;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountRSession.DB_IsCountrSession -> CashierSessionId: {0}", CashierSessionID));
        Log.Exception(_ex);
      }

      return false;
    } // DB_IsCountrSession

    #endregion " Private Methods "
  }
}
