﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CountRBusinessLogic.cs
// 
//   DESCRIPTION: Miscellaneous utilities for CountR
// 
//        AUTHOR: Yineth Nuñez
// 
// CREATION DATE: 07-APR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-APR-2016 YNM    First release (Header).
// 08-APR-2016 ADI    Validation Ticket & Create New Ticket
// 04-JUL-2016 AMF    Bug 15150:CountR: Permite pagar tickets caducados
// 11-JUL-2016 JBP    Bug 15315:CountR: no se puede cerrar una sesión de caja de countr teniendo el kiosco deshabilitado
// 11-JUL-2016 JCA    Bug 15435:CountR: Operación incorrecta al cancelar un ticket desde el cajero que ya sido pagado por CountR
// 15-JUL-2016 JBP    Bug 15409:CountR: No se insertan los registros en "Faltantes y sobrantes"
//                    Bug 15626:CountR: No se actualiza la sesión de CountR
// 24-AGO-2016 FJC    Bug 15896:CountR: En las sesiones de kiosko no se tiene en cuenta los tickets, por tanto no cuadra la diferencia (faltante y sobrante)
// 30-AGO-2016 JBP    Bug 17105:CountR: No se actualiza la columna "entregado" de la sesión de CountR cuando se cancela su recaudación asociada.
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 15-DEC-2016 FAV    Bug 17386:CountR: Invalid value of taxes for payment
// 19-DEC-2016 FAV    Bug 21719:CountR: Invalid tax when Cashier.TITOTaxWaiver is enabled
// 10-JAN-2017 CCG    Bug 22321:CountR: La creación de tickets no se refleja en la sesión de CountR
// 31-JAN-2017 ATB        Bug 21719:CountR is applying taxes when it never owns
// 08-FEB-2017 FAV    PBI 25268:Third TAX - Payment Voucher
// 04-MAY-2017 DPC    WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
// 21-AUG-2017 MS     WIGOS-3947: WRKP TITO - Get Ticket Data - Promoted CheckTicket to Public
// 13-JUL-2018 AGS    Bug 33616:WIGOS-13496 Win Recycling Voucher Numbers
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.TITO;

namespace WSI.Common.CountR
{
  public class CountRBusinessLogic
  {
    #region " Properties "

    public static Ticket m_current_ticket;
    private static Int64 m_out_operationID = 0;

    #endregion " Properties "

    #region " Enums "

    public enum ENABLED_COUNTR
    {
      FALSE = 0,
      TRUE = 1
    }

    public enum COUNTR_STATUS
    {
      OFFLINE = 0,
      ONLINE = 1
    }

    public enum AckExtended
    {
      None = 0,
      ValidCashable = 1,
      ValidRestricted = 2,
      ValidNonRestricted = 3,
      UnableToValidate = 4,
      NumberNotValid = 5,
      NumberUnknown = 6,
      PendingInSystem = 7,
      AlreadyRedeemed = 8,
      Expired = 9,
      InformationNotAvailable = 10,
      AmountMismatch = 11,
      ExceedsLimit = 12,
      LockCardBecauseOfTransactionMismatch = 13,
      LockCardBecauseOfAmountMismatch = 14
    }

    public enum PurseType
    {
      Unused = 0,
      Player = 1,
      Promotional = 2,
      BonusPoints = 3
    }

    public enum Ack
    {
      ACK = 1,
      NAK = 2,
      UnknownCashIO = 3,
      AccountInvalid = 4,
      BufferOverflow = 5,
      InvalidPIN = 6
    }

    public enum Event
    {
      OpenDoor = 1,
      OperatorCardInserted = 2,
      CustomerCardInserted = 3,
      BanknoteOrTicketInserted = 4,
      CallOperatorMode = 5
    }

    public enum SubEvent
    {
      None = 0,
      MainDoor_Banknote = 1,
      HeadDoor_Ticket = 2
    }

    public enum Mode : int
    {
      Closed_Rejected = 0,
      Opened_Inserted = 1,
      Captured_Stacked = 2,
      Issued = 3
    }


    public enum CLOSE_SESSION_RC
    {
      OK = 0,
      NOT_ENABLED = 1,
      SESSION_INFO_ERROR = 2,
      MONEY_COLLECTION_ERROR = 3,
      CAGE_METERS_ERROR = 4,
      CLOSE_SESSION_ERROR = 5
    }

    #endregion " Enums "

    #region " Public Methods "

    /// <summary>
    /// Gets CountRSession and CashierSessionInfo by SessionId
    /// </summary>
    /// <param name="CountRSessionId"></param>
    /// <param name="CountRSession"></param>
    /// <param name="SqlTrx"></param>
    /// <returns>Exist CountR session</returns>
    public static Boolean GetCountRSessionsById(Int32 CountRSessionId, out CountRSession CountRSession, SqlTransaction SqlTrx)
    {
      CountRSession = null;
      CashierSessionInfo _csi;

      _csi = null;
      return CountRSession.GetCountRSessionById(CountRSessionId, out CountRSession, out _csi, SqlTrx);
    }

    /// <summary>
    /// Gets CountRSession and CashierSessionInfo by SessionId
    /// </summary>
    /// <param name="CountRSessionId"></param>
    /// <param name="CountRSession"></param>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="SqlTrx"></param>
    /// <returns>Exist CountR session</returns>
    public static Boolean GetCountRSessionsById(Int32 CountRSessionId, out CountRSession CountRSession, out CashierSessionInfo CashierSessionInfo, SqlTransaction SqlTrx)
    {
      CountRSession = null;
      CashierSessionInfo = null;

      return CountRSession.GetCountRSessionById(CountRSessionId, out CountRSession, out CashierSessionInfo, SqlTrx);
    }

    /// <summary>
    /// Find or create Cashier and CountR session for a specific Countr
    /// </summary>
    /// <param name="CountR"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="CRS"></param>
    /// <returns></returns>
    public static CashierSessionInfo GetorOpenCountRSessions(CountR CountR, SqlTransaction SqlTrx, out CountRSession CRS)
    {
      Int64 _cashier_session_id;
      Boolean _is_new_session;
      CashierSessionInfo _csi;

      _csi = null;
      CRS = null;

      if (CountR.Enabled)
      {
        _csi = new CashierSessionInfo();
        Cashier.GetOrOpenSystemCashierSession(GU_USER_TYPE.SYS_REDEMPTION, CountR.Name, out _cashier_session_id, out _is_new_session, SqlTrx);

        if (_cashier_session_id > 0)
        {
          Cashier.GetCashierSessionById(_cashier_session_id, out _csi, SqlTrx);

          if (_csi != null)
          {
            CRS = CountRSession.GetorOpenCountRSession(CountR.Id, _csi, SqlTrx);
          }
        }
      }

      return _csi;
    } // GetorOpenCountRSessions

    /// <summary>
    /// Look for an existing open CountR Session
    /// </summary>
    /// <param name="CountRId"></param>
    /// <param name="Enabled"></param>
    /// <returns></returns>
    public static Boolean ExistOpenCountRSession(Int32 CountRId, Boolean Enabled)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return ExistOpenCountRSession(CountRId, Enabled, _db_trx.SqlTransaction);
      }
    } // ExistOpenCountRSession

    /// <summary>
    /// Look for an existing open CountR Session
    /// </summary>
    /// <param name="CountRId"></param>
    /// <param name="Enabled"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean ExistOpenCountRSession(Int32 CountRId, Boolean Enabled, SqlTransaction SqlTrx)
    {
      return Enabled && CountRSession.ExistOpenCountRSession(CountRId, SqlTrx);
    } // ExistOpenCountRSession

    /// <summary>
    /// Close session opened for a specific CountR
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="CountR"></param>
    /// <param name="UserID"></param>
    /// <param name="UserName"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static CLOSE_SESSION_RC CloseCountRSessions(Int32 CashierSessionId, CountR CountR, Int32 UserID, String UserName, SqlTransaction SqlTrx)
    {
      CashierSessionInfo _csi;
      CountRSession _crs;

      if (CountR.Id <= 0)
      {
        return CLOSE_SESSION_RC.CLOSE_SESSION_ERROR;
      }

      // JBP Check Redeem Kiosk is enabled.
      if (!CountR.Enabled)
      {
        return CLOSE_SESSION_RC.NOT_ENABLED;
      }

      // Get cashier session and set gui user.
      if (!GetCashierSessionInfoByIdWithGUIUser(CashierSessionId, UserID, UserName, SqlTrx, out _csi, out _crs))
      {
        return CLOSE_SESSION_RC.SESSION_INFO_ERROR;
      }

      // Close pending money collection
      if (!ClosePendingMoneyCollection(_csi, SqlTrx))
      {
        return CLOSE_SESSION_RC.MONEY_COLLECTION_ERROR;
      }

      // Update cage meters
      if (!CashierSessionClose_UpdateCageMeters(_csi.CashierSessionId, SqlTrx))
      {
        return CLOSE_SESSION_RC.CAGE_METERS_ERROR;
      }

      // Close cashier and CountR session
      if (CloseCashierAndCountRSession(CountR, _crs, _csi, SqlTrx))
      {
        return CLOSE_SESSION_RC.OK;
      }

      return CLOSE_SESSION_RC.CLOSE_SESSION_ERROR;
    } // CloseCountRSessions

    /// <summary>
    /// Close Cashier and CountR sessions
    /// </summary>
    /// <param name="CountR"></param>
    /// <param name="Crs"></param>
    /// <param name="Csi"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean CloseCashierAndCountRSession(CountR CountR, CountRSession Crs, CashierSessionInfo Csi, SqlTransaction SqlTrx)
    {
      ArrayList _vouchers;
      Int32 _cashier_user_id;
      CASHIER_SESSION_CLOSE_STATUS _rc_close_status;

      // Get session info
      if (!Cashier.GetCashierInfoBySession(Csi.CashierSessionId, SqlTrx, out _cashier_user_id))
      {
        return false;
      }

      // Close cashier session
      _rc_close_status = Cashier.CashierSessionClose(Crs.CollectedAmount, Csi, _cashier_user_id, true, out _vouchers, SqlTrx);
      if (_rc_close_status == CASHIER_SESSION_CLOSE_STATUS.OK)
      {
        // Close CountR session
        if (CountRSession.CloseCountRSession(Crs.Id, SqlTrx))
        {
          return true;
        }
      }

      return false;
    } // CloseCashirAndCountRSession

    /// <summary>
    /// Cashier sesssion close: Update Cage Meters (Call Stored Procedure: CageUpdateSystemMeters)
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean CashierSessionClose_UpdateCageMeters(Int64 CashierSessionId, SqlTransaction SqlTrx)
    {
      if (Cage.IsCageEnabled())
      {
        if (!CageMeters.CashierSessionClose_UpdateCageSystemMeters(CashierSessionId, SqlTrx))
        {
          return false;
        }
      }

      return true;
    } // CashierSessionClose_UpdateCageMeters

    /// <summary>
    /// Colse pending money collection
    /// </summary>
    /// <param name="Csi"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean ClosePendingMoneyCollection(CashierSessionInfo Csi, SqlTransaction SqlTrx)
    {
      MoneyCollection _money_collection;
      Int64 _money_collection_id;

      if (TITO.Utils.IsTitoMode())
      {
        if (!TITO.MoneyCollection.GetCashierMoneyCollectionId(Csi.CashierSessionId, out _money_collection_id, SqlTrx))
        {
          return false;
        }

        _money_collection = TITO.MoneyCollection.DB_Read(_money_collection_id, SqlTrx);
        return _money_collection.DB_ChangeStatusToPendingClosing(SqlTrx);
      }

      return true;
    } // ClosePendingMoneyCollection

    /// <summary>
    /// Check if a given Ticket has a valid status
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="TicketNumber"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean CheckAccount(Int32 CountRCode, String TicketNumber, out ACKRedeemKiosk Response, SqlTransaction SqlTrx)
    {
      Ticket _ticket;
      CountR _countr;

      return CheckTicket(CountRCode, TicketNumber, 0, false, false, out Response, out _ticket, out _countr, SqlTrx);

    } // CheckAccount

    /// <summary>
    /// Start payment transaction
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="TicketNumber"></param>tinpending
    /// <param name="Amount"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean StartPayment(Int32 CountRCode, String TicketNumber, Int64 Amount, out ACKRedeemKiosk Response, SqlTransaction SqlTrx)
    {
      Ticket _ticket;
      CountR _countr;

      if (CheckTicket(CountRCode, TicketNumber, Amount, true, true, out Response, out _ticket, out _countr, SqlTrx))
      {
        _ticket.LastActionTerminalID = (Int32)_countr.Id;
        _ticket.LastActionTerminalName = _countr.Name;
        _ticket.LastActionTerminalType = (Int32)TITO_TERMINAL_TYPE.COUNTR;
        _ticket.LastActionDateTime = WGDB.Now;

        if (!UpdateTicketStatus(_ticket, SqlTrx))
        {
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        AmountTaxes(_ticket);

        return true;
      }

      return false;
    } // StartPayment

    /// <summary>
    /// End payment transaction
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="TicketNumber"></param>
    /// <param name="Amount"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean EndPayment(Int32 CountRCode, String TicketNumber, Int64 Amount, out ACKRedeemKiosk Response, SqlTransaction SqlTrx)
    {
      CountR _countr;
      Int64 _ticket_number;
      CashierSessionInfo _cashier_session_info;
      CountRSession _countr_session;
      Int32 _user_id;
      String _user_name;
      CardData _card_data;
      Ticket _paid_ticket;
      String _condition;
      ParamsTicketOperation _operation_params;
      CountRTransaction _countr_trans;
      ArrayList _voucher_list;
      Boolean _witholding_needed;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _final_balance;
      TITO_VALIDATION_TYPE _validation_type;
      String _error_msg;
      Decimal _amount_ticket;
      Currency _total_taxes;

      Response = new ACKRedeemKiosk();

      try
      {
        _countr = new CountR();

        // CountR
        if (!CountR.DB_GetCountRByCode(CountRCode, out _countr, SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment: Redeem Kiosk not found");
          Response.Ack = Ack.UnknownCashIO;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        // CashierSessionInfo
        _cashier_session_info = new CashierSessionInfo();
        _cashier_session_info = CountRBusinessLogic.GetorOpenCountRSessions(_countr, SqlTrx, out _countr_session);
        if (_cashier_session_info == null)
        {
          Log.Error("CountRBusinessLogic EndPayment: CashierSessionInfo not found");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.InformationNotAvailable;

          return false;
        }
        if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_REDEMPTION, SqlTrx, out _user_id, out _user_name))
        {
          Log.Message("CountRBusinessLogic EndPayment.GetSystemUser: Error getting the user (SYS_Redemption) for the Redeem Kiosk");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.InformationNotAvailable;

          return false;
        }
        _cashier_session_info.AuthorizedByUserId = _user_id;
        _cashier_session_info.AuthorizedByUserName = _user_name;

        //Get ticket
        _ticket_number = ValidationNumberManager.UnFormatValidationNumber(TicketNumber, out _validation_type);
        if (_ticket_number == 0)
        {
          Response.Ack = Ack.AccountInvalid;
          Response.AckExtended = AckExtended.NumberNotValid;

          return false;
        }
        _condition = " TI_VALIDATION_NUMBER = '" + _ticket_number.ToString() + "' ";
        _paid_ticket = SelectTicketByCondition(_condition, SqlTrx);

        if (_paid_ticket.TicketID == 0)
        {
          Response.Ack = Ack.AccountInvalid;
          Response.AckExtended = AckExtended.NumberUnknown;

          return false;
        }

        switch (_paid_ticket.Status)
        {
          case TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION:
          case TITO_TICKET_STATUS.PENDING_PAYMENT:
            break;

          default:
            Response.Ack = Ack.AccountInvalid;
            Response.AckExtended = AckExtended.AlreadyRedeemed;

            return false; // Status Incorrect
        }

        // Check amount
        if (Decimal.ToInt64(_paid_ticket.Amount * 100.00m) != Amount)
        {
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.AmountMismatch;

          return false;
        }

        // Check Max/Min Payment amount
        if ((_paid_ticket.Amount < _countr.MinPayment) || ((_countr.MaxPayment > 0) && (_paid_ticket.Amount > _countr.MaxPayment)))
        {
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.ExceedsLimit;

          return false;
        }

        // card Data
        _card_data = new CardData();
        if (!CardData.DB_VirtualCardGetAllData(_cashier_session_info.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, _card_data))
        {
          Log.Message("CountRBusinessLogic EndPayment.DB_VirtualCardGetAllData. Error");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.InformationNotAvailable;

          return false;
        }

        // ParamsTicketOperation
        _operation_params = new ParamsTicketOperation();
        _card_data.MaxDevolution = Utils.MaxDevolutionAmount(_paid_ticket.Amount);
        _operation_params.in_account = _card_data;
        _operation_params.session_info = _cashier_session_info;
        _operation_params.cashier_terminal_id = _cashier_session_info.TerminalId;
        _operation_params.tito_terminal_types = TITO_TERMINAL_TYPE.COUNTR;
        _operation_params.in_cash_amount = _paid_ticket.Amount;
        _operation_params.in_cash_amt_0 = _paid_ticket.Amount;
        _operation_params.cash_redeem_total_paid = _paid_ticket.Amount;
        _operation_params.max_devolution = Utils.MaxDevolutionAmount(_paid_ticket.Amount);
        _operation_params.validation_numbers = new List<VoucherValidationNumber>();
        _operation_params.out_operation_id = m_out_operationID;

        AmountTaxes(_paid_ticket);

        // Redeem ticket
        if (!RedeemTicket(_paid_ticket, OperationCode.CASH_OUT, ref _operation_params, SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment.RedeemTicketList. Error updating ticket data on redeemption of ticket list.");

          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.InformationNotAvailable;

          return false;
        }

        VoucherValidationNumber _voucher_ticket_data;

        if (_paid_ticket != null)
        {
          _voucher_ticket_data = new VoucherValidationNumber();
          _voucher_ticket_data.ValidationNumber = _paid_ticket.ValidationNumber;
          _voucher_ticket_data.Amount = _paid_ticket.Amount;
          _voucher_ticket_data.ValidationType = _paid_ticket.ValidationType;
          _voucher_ticket_data.TicketType = _paid_ticket.TicketType;
          _voucher_ticket_data.CreatedTerminalName = _paid_ticket.CreatedTerminalName;
          _voucher_ticket_data.CreatedPlaySessionId = _paid_ticket.CreatedPlaySessionId;
          _voucher_ticket_data.CreatedAccountId = _paid_ticket.CreatedAccountID;
          _voucher_ticket_data.TicketId = _paid_ticket.TicketID;
          _voucher_ticket_data.CreatedTerminalType = _paid_ticket.CreatedTerminalType;
          _operation_params.validation_numbers.Add(_voucher_ticket_data);
        }

        _operation_params.is_apply_tax = !Tax.ApplyTaxCollectionTicketPayment();

        if (!AccountsRedeem.DB_CardCreditRedeem(_card_data.AccountId, null, new PaymentOrder(), _paid_ticket.Amount, CASH_MODE.TOTAL_REDEEM, null,
                                                CreditRedeemSourceOperationType.DEFAULT, _card_data, OperationCode.CASH_OUT, SqlTrx, _operation_params.is_apply_tax,
                                                _cashier_session_info, out _total_taxes, out _voucher_list, out _witholding_needed,
                                                out _error_msg, ref m_out_operationID))
        {
          Log.Error("CountRBusinessLogic EndPayment.DB_CardCreditRedeem. Error updating");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.InformationNotAvailable;

          return false;
        }

        // Update CountR transaction
        if (!CountRTransaction.DB_GetByTicketAndStatus(_paid_ticket.ValidationNumber.ToString(), CountRTransaction.TRANSACTION_STATUS.PENDING, out _countr_trans, SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment  DB_GetTransactionByTicket: Error get Redeem Kiosk transaction");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        if (_countr_trans.Status != CountRTransaction.TRANSACTION_STATUS.PENDING)
        {
          Log.Error("CountRBusinessLogic EndPayment:  Redeem Kiosk Transaction Status Not PENDING ");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }
        if (!_countr_trans.DB_Update(SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment  DB_Insert: Error creating Redeem Kiosk transaction");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        if (_operation_params.is_apply_tax)
        {
          _amount_ticket = _paid_ticket.Amount - _total_taxes;  //_paid_ticket.TotalRedeemAfterTaxes;
        }
        else
        {
          _amount_ticket = _paid_ticket.Amount;
        }

        // Update CountR session
        if (!_countr_session.UpdateSession_Ticket(_amount_ticket, SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment  DB_Insert: Error updating Redeem Kiosk session");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        // Update account balance
        if (!Utils.AccountBalanceIn(_card_data.AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _final_balance, SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment.AccountBalanceIn. Error updating");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.InformationNotAvailable;

          return false;
        }
        if (_final_balance.TotalRedeemable > 0)
        {
          MultiPromos.AccountBalance _to_add = new MultiPromos.AccountBalance(-_final_balance.Redeemable, -_final_balance.PromoRedeemable, 0);

          if (!MultiPromos.Trx_UpdateAccountBalance(_card_data.AccountId, _to_add, out _final_balance, SqlTrx))
          {
            Log.Error("CountRBusinessLogic EndPayment.DB_CardCreditRedeem. Error updating ticket data on redeemption of ticket list.");
            Response.Ack = Ack.NAK;
            Response.AckExtended = AckExtended.InformationNotAvailable;

            return false;
          }
        }

        Response.Ack = Ack.ACK;
        Response.AckExtended = AckExtended.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("CountRBusinessLogic EndPayment");
        Log.Exception(_ex);
        Response.Ack = Ack.NAK;
        Response.AckExtended = AckExtended.InformationNotAvailable;
      }

      return false;
    } // EndPayment

    /// <summary>
    /// Get Validation Number to create a ticket
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="Amount"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean GetValidAccountNumber(Int32 CountRCode, out ACKRedeemKiosk Response, out Int64 SeqId, SqlTransaction SqlTrx)
    {
      Int64 _validation_number;
      CountR _countr;
      Int32 _cashier_id;
      Int64 _sequence;

      Response = new ACKRedeemKiosk();
      _sequence = 0;
      SeqId = _sequence;

      try
      {
        _countr = new CountR();

        // CountR
        if (!CountR.DB_GetCountRByCode(CountRCode, out _countr, SqlTrx))
        {
          Log.Error("CountRBusinessLogic GetValidAccountNumber: CountR not found");
          Response.Ack = Ack.UnknownCashIO;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        if (!_countr.CreateTicket)
        {
          Log.Error(String.Format("CountRBusinessLogic GetValidAccountNumber: Redeem Kiosk {1} is unable to create tickets", _countr.Name));
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        _cashier_id = Cashier.ReadTerminalId(SqlTrx, _countr.Name, GU_USER_TYPE.SYS_REDEMPTION, false, out _cashier_id);

        if (_cashier_id <= 0)
        {
          Log.Error("CountRBusinessLogic GetValidAccountNumber: Cashier Terminal not found");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        _validation_number = Cashier.Common_ComposeValidationNumber(out _sequence, SqlTrx, _cashier_id);

        if (_validation_number <= 0)
        {
          Log.Error("CountRBusinessLogic GetValidAccountNumber: Unable to generate validation number");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        Response.ValidationNumberSeq = _validation_number.ToString().PadLeft(18, '0');

        Response.TicketPetitionDate = WGDB.Now;
        Response.Ack = Ack.ACK;
        Response.AckExtended = AckExtended.None;
        SeqId = _sequence;
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("CountRBusinessLogic GetValidAccountNumber: An unexpected error has ocurred");
        Log.Exception(_ex);
        Response.Ack = Ack.NAK;
        Response.AckExtended = AckExtended.UnableToValidate;
      }

      return false;
    } // CreateTicket

    /// <summary>
    /// Create Ticket with introduced amount; perform all movements related to this operation.
    /// FUNCIONALITY UNIMPLEMENTED. NOT USED AT THE MOMENT
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="Amount"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean ChargeAccount(Int32 CountRCode, String TicketNumber, Decimal Amount, TITO_TICKET_TYPE TicketType, Int64 SeqId, out ACKRedeemKiosk Response, SqlTransaction SqlTrx)
    {
      CountR _cr;
      CashierSessionInfo _csi;
      CountRSession _crs;
      Int32 _user_id;
      String _user_name;
      CardData _card_data;
      CountRTransaction _crtrans;
      ParamsTicketOperation _operation_params;
      RechargeInputParameters _input_params;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      RechargeOutputParameters _output_params;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _final_balance;
      VoucherValidationNumber _ticket_data;
      Currency _currency_amount;

      Response = new ACKRedeemKiosk();
      _currency_amount = 0;

      try
      {
        _cr = new CountR();
        if (!CountR.DB_GetCountRByCode(CountRCode, out _cr, SqlTrx))
        {
          Log.Error("CountRBusinessLogic_CreateTicket DB_GetCountRByCode: Redeem Kiosk not found. Code: " + CountRCode.ToString());
          Response.Ack = Ack.UnknownCashIO;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        if (!_cr.CreateTicket)
        {
          Log.Error(String.Format("CountRBusinessLogic GetValidAccountNumber: Redeem Kiosk {1} is unable to create tickets", _cr.Name));
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        _csi = CountRBusinessLogic.GetorOpenCountRSessions(_cr, SqlTrx, out _crs);
        if (_csi == null)
        {
          Log.Message("CountRBusinessLogic_CreateTicket GetorOpenCountRSessions: Error getting the CashierSessionInfo for Redeem Kiosk: " + _cr.Code.ToString());
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_REDEMPTION, SqlTrx, out _user_id, out _user_name))
        {
          Log.Message("CountRBusinessLogic_CreateTicket GetSystemUser: Error getting the user (SYS_Redemption) for Redeem Kiosk: " + _cr.Name);
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }
        _csi.AuthorizedByUserId = _user_id;
        _csi.AuthorizedByUserName = _user_name;

        _card_data = new CardData();
        if (!CardData.DB_VirtualCardGetAllData(_csi.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, _card_data))
        {
          Log.Error("CountRBusinessLogic_CreateTicket DB_VirtualCardGetAllData: CardData not found for " + _csi.TerminalName);
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        _currency_amount = (Currency)Amount / 100;
        if (!(_currency_amount > 0))
        {
          Log.Error("CountRBusinessLogic_CreateTicket Currency Amount: Currency Amount equal or less than 0");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.AmountMismatch;

          return false;
        }

        _operation_params = new ParamsTicketOperation();
        _operation_params.in_account = _card_data;
        _operation_params.ticket_type = TicketType;

        if (!Operations.DB_InsertOperation(OperationCode.CASH_OUT,
                                           _operation_params.in_account.AccountId,
                                           _csi.CashierSessionId,
                                           0,
                                           _operation_params.out_promotion_id,
                                           0,
                                           0,
                                           0,                      // Operation Data
                                           _operation_params.AccountOperationReasonId,
                                           _operation_params.AccountOperationComment,
                                           out m_out_operationID, SqlTrx))
        {
          Log.Error("CountRBusinessLogic_CreateTicket DB_InsertOperation: Error creating operation.");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        _output_params = new RechargeOutputParameters();
        _account_movements = new AccountMovementsTable(_csi);
        _cashier_movements = new CashierMovementsTable(_csi);

        _input_params = new RechargeInputParameters();
        _input_params = GetInputParameters(_card_data);

        _operation_params.session_info = _csi;
        _operation_params.cashier_terminal_id = _csi.TerminalId;
        _operation_params.tito_terminal_types = TITO_TERMINAL_TYPE.COUNTR;
        _operation_params.out_cash_amount = _currency_amount;
        _operation_params.in_cash_amt_0 = _currency_amount;
        _operation_params.out_operation_id = m_out_operationID;
        _operation_params.validation_numbers = new List<VoucherValidationNumber>();

        _ticket_data = new VoucherValidationNumber();
        if (!Int64.TryParse(TicketNumber, out _ticket_data.ValidationNumber))
        {
          Log.Error("CountRBusinessLogic_CreateTicket  DB_CardCreditAdd: unable to convert Ticket Number");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.NumberNotValid;

          return false;
        }
        _ticket_data.SequenceId = SeqId;
        _operation_params.validation_numbers.Add(_ticket_data);

        if (!Accounts.DB_CardCreditAdd(_input_params, m_out_operationID, _account_movements, _cashier_movements, _operation_params.out_cash_amount,
                                       _operation_params.out_promotion_id, 0, 0, 0, null,
                                       ParticipateInCashDeskDraw.Default, OperationCode.CASH_IN, SqlTrx, null, out _output_params))
        {
          Log.Error("CountRBusinessLogic_CreateTicket  DB_CardCreditAdd: Error creating movements");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        if (!Cashier.Common_CreateTicket(_operation_params, true, _currency_amount, out m_current_ticket, SqlTrx))
        {
          Log.Error("CountRBusinessLogic_CreateTicket  DB_CardCreditAdd: Error creating Ticket");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        _crtrans = new CountRTransaction();
        _crtrans.OperationId = m_out_operationID;
        _crtrans.NewTicketId = m_current_ticket.TicketID;
        _crtrans.NewTicketAmount = m_current_ticket.Amount;
        _crtrans.CountrId = _cr.Id;
        _crtrans.CashierSessionId = _csi.CashierSessionId;

        if (!_crtrans.DB_Insert(SqlTrx))
        {
          Log.Error("CountRBusinessLogic_CreateTicket  DB_Insert: Error creating Redeem Kiosk transaction");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        if (!Utils.AccountBalanceIn(_card_data.AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _final_balance, SqlTrx))
        {
          Log.Error("CountRBusinessLogic_CreateTicket  AccountBalanceIn: Error updating account balance in");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        if (_final_balance.TotalRedeemable > 0)
        {
          MultiPromos.AccountBalance _to_add = new MultiPromos.AccountBalance(-_final_balance.Redeemable, -_final_balance.PromoRedeemable, 0);

          if (!MultiPromos.Trx_UpdateAccountBalance(_card_data.AccountId, _to_add, out _final_balance, SqlTrx))
          {
            Log.Error("CountRBusinessLogic_CreateTicket  Trx_UpdateAccountBalance: Error updating account balance");
            Response.Ack = Ack.NAK;
            Response.AckExtended = AckExtended.UnableToValidate;

            return false;
          }
        }

        // Update CountR Session to add the ticket amount to deposits
        if (!_crs.UpdateSession_Movement(Currency.Zero(), _currency_amount, SqlTrx))
        {
          Log.Error("CountRBusinessLogic_CreateTicket  UpdateSession_Movement: Error updating kiosk session deposit movement");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        Response.Ack = Ack.ACK;
        Response.AckExtended = AckExtended.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Response.Ack = Ack.NAK;
        Response.AckExtended = AckExtended.UnableToValidate;
      }

      return false;
    } // CreateTicket

    /// <summary>
    /// Redeem ticket
    /// </summary>
    /// <param name="PaidTicket"></param>
    /// <param name="OperationCode"></param>
    /// <param name="Params"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean RedeemTicket(Ticket PaidTicket, OperationCode OperationCode, ref ParamsTicketOperation Params, SqlTransaction SqlTrx)
    {
      Int64 _money_collection_id;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _final_balance;
      MoneyCollection _money_collection;
      CashierSessionInfo _session_info;
      CashierMovementsTable _cashier_movements;
      AccountMovementsTable _account_movements;
      VoucherValidationNumber _voucher_number;
      CardData _card_for_handpay;
      CASHIER_MOVEMENT _cashier_mov_type;
      MovementType _account_mov_type;
      Handpays.HandpayRegisterOrCancelParameters _handpay_param;
      String _cm_details;
      Int16 _handpay_error_msg;

      _final_balance = new MultiPromos.AccountBalance();
      _money_collection_id = 0;
      _session_info = Params.session_info;
      _card_for_handpay = null;
      _cm_details = "";

      if (!MoneyCollection.GetCashierMoneyCollectionId(Params.session_info.CashierSessionId, out _money_collection_id, SqlTrx))
      {
        Log.Error("ReedemTicketList: Error getting collection id. Session Id:" + Params.session_info.CashierSessionId);

        return false;
      }

      _account_movements = new AccountMovementsTable(_session_info);
      _cashier_movements = new CashierMovementsTable(_session_info);
      _money_collection = MoneyCollection.DB_Read(_money_collection_id, SqlTrx);

      // Update ticket status
      PaidTicket.Status = PaidTicket.Status == TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION ? TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION : TITO_TICKET_STATUS.REDEEMED;
      PaidTicket.LastActionDateTime = WGDB.Now;
      PaidTicket.LastActionTerminalID = _session_info.TerminalId;
      PaidTicket.LastActionTerminalType = (Int32)TITO_TERMINAL_TYPE.COUNTR;
      PaidTicket.LastActionAccountID = PaidTicket.CreatedAccountID;
      PaidTicket.Amt_1 = PaidTicket.Amount;
      PaidTicket.MoneyCollectionID = _money_collection_id;
      //JCALERO 11-07-2016   Bug 15435:CountR: Operación incorrecta al cancelar un ticket desde el cajero que ya sido pagado por CountR
      //No se debe cambiar el operationID.
      //PaidTicket.TransactionId = m_out_operationID;

      if (!Ticket.DB_UpdateTicket(PaidTicket, SqlTrx))
      {
        return false;
      }

      switch (PaidTicket.TicketType)
      {
        case TITO_TICKET_TYPE.HANDPAY:
        case TITO_TICKET_TYPE.JACKPOT:

          Int64 _play_session_id;

          // Check if ticket has a valid Handpay
          _handpay_param = new Handpays.HandpayRegisterOrCancelParameters();

          // DRV 19-03-14: Calculate amounts for expected money collection totalizator
          _money_collection.ExpectedRETicketAmount += PaidTicket.Amount;
          _money_collection.ExpectedRETicketCount += 1;

          if (TITO.HandPay.GetTicketHandpayStatus(PaidTicket, out _handpay_param, SqlTrx) != TITO.HandPay.TICKET_HANDPAY_STATUS.VALID)
          {
            Log.Message("RedeemTicketList: HasValidHandPay: Ticket [" + PaidTicket.TicketID + "] hasn't a valid handpay.");
            return false;
          }

          // Set the account 
          if (_card_for_handpay == null)
          {
            _card_for_handpay = new CardData();
            CardData.DB_CardGetAllData(Params.in_account.AccountId, _card_for_handpay, false);
            _card_for_handpay.AccountBalance = MultiPromos.AccountBalance.Zero;
          }
          _handpay_param.CardData = _card_for_handpay;

          _voucher_number = new VoucherValidationNumber();
          _voucher_number.Amount = PaidTicket.Amount;
          _voucher_number.CreatedTerminalName = _handpay_param.TerminalName;
          _voucher_number.TicketId = PaidTicket.TicketID;
          _voucher_number.TicketType = PaidTicket.TicketType;
          _voucher_number.ValidationNumber = PaidTicket.ValidationNumber;
          _voucher_number.ValidationType = PaidTicket.ValidationType;
          _voucher_number.CreatedTerminalType = PaidTicket.CreatedTerminalType;

          // Register the handpay
          if (!Handpays.RegisterOrCancel(_handpay_param, _session_info, false, _voucher_number, OperationCode, ref Params.out_operation_id, out _play_session_id, out _handpay_error_msg, SqlTrx))
          {
            Log.Message("RedeemTicketList: RegisterOrCancel: Ticket [" + PaidTicket.TicketID + "] unable to register handpay.");
            return false;
          }

          // Update ticket play session
          if (!WSI.Common.TITO.HandPay.DB_SetTicketPlaySessionId(PaidTicket.TicketID, _play_session_id, SqlTrx))
          {
            Log.Message("RedeemTicketList: DB_SetTicketPlaySessionId: Ticket [" + PaidTicket.TicketID + "] unable to update play session.");
            return false;
          }

          break;

        default:

          switch (PaidTicket.TicketType)
          {
            case TITO_TICKET_TYPE.PROMO_REDEEM:
              _money_collection.ExpectedPromoRETicketAmount += PaidTicket.Amount;
              _money_collection.ExpectedPromoRETicketCount += 1;
              break;
            case TITO_TICKET_TYPE.OFFLINE:
            case TITO_TICKET_TYPE.CASHABLE:
              _money_collection.ExpectedRETicketAmount += PaidTicket.Amount;
              _money_collection.ExpectedRETicketCount += 1;
              break;
          }

          // Normal pay process for the tickets 
          // Create the cash_out operation
          if (!Utils.AccountBalanceIn(Params.in_account.AccountId, PaidTicket.Amount, out _ini_balance, out _final_balance, SqlTrx))
          {
            return false;
          }

          if (PaidTicket.TicketType != TITO_TICKET_TYPE.OFFLINE)
          {
            if (PaidTicket.TicketType == TITO_TICKET_TYPE.CASHABLE)
            {
              if (PaidTicket.Status == TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION)
              {
                _account_mov_type = MovementType.TITO_TicketCashierExpiredPaidCashable;//new
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE;//new
              }
              else
              {
                _account_mov_type = MovementType.TITO_TicketCashierPaidCashable;
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE;
              }
            }
            else
            {
              if (PaidTicket.Status == TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION)
              {
                _account_mov_type = MovementType.TITO_TicketCashierExpiredPaidPromoRedeemable;
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE;
              }
              else
              {
                _account_mov_type = MovementType.TITO_TicketCashierPaidPromoRedeemable;
                _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE;
              }
            }
            _account_movements.Add(Params.out_operation_id, Params.in_account.AccountId, _account_mov_type,
                         _ini_balance.TotalBalance, 0, PaidTicket.Amount, _final_balance.TotalBalance);
            _cashier_movements.Add(Params.out_operation_id, _cashier_mov_type, PaidTicket.Amount, Params.in_account.AccountId, Params.in_account.TrackData, _cm_details);
          }

          break;
      }

      _money_collection.ExpectedTicketAmount += PaidTicket.Amount;
      _money_collection.ExpectedTicketCount += 1;

      //Updates Money collection totalizators
      if (!_money_collection.DB_UpdateExpectedValues(SqlTrx))
      {
        return false;
      }

      // We must set operation_id to min value because it will be updated later with the correct operation_id
      if (!_account_movements.Save(SqlTrx))
      {
        return false;
      }

      // We must set operation_id to min value because it will be updated later with the correct operation_id
      if (!_cashier_movements.Save(SqlTrx))
      {
        return false;
      }

      return true;
    } // RedeemTicket

    /// <summary>
    /// Send CountR Event
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="CountRName"></param>
    /// <param name="CountREvent"></param>
    /// <param name="CountRSubEvent"></param>
    /// <param name="CountRMode"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean SendEvent(Int32 CountRCode, String CountRName, Event CountREvent, SubEvent CountRSubEvent, Mode CountRMode, out ACKRedeemKiosk Response, SqlTransaction SqlTrx)
    {
      AlarmCode _alarm_code;

      Response = new ACKRedeemKiosk();
      _alarm_code = AlarmCode.CountR_CallOperator;

      try
      {
        switch (CountREvent)
        {
          case Event.OpenDoor:
            switch (CountRSubEvent)
            {
              case SubEvent.MainDoor_Banknote:
                switch (CountRMode)
                {
                  case Mode.Closed_Rejected:
                    _alarm_code = AlarmCode.CountR_Door_MainDoor_Closed;
                    break;
                  case Mode.Opened_Inserted:
                    _alarm_code = AlarmCode.CountR_Door_MainDoor_Opened;
                    break;
                }
                break;
              case SubEvent.HeadDoor_Ticket:
                switch (CountRMode)
                {
                  case Mode.Closed_Rejected:
                    _alarm_code = AlarmCode.CountR_Door_HeadDoor_Closed;
                    break;
                  case Mode.Opened_Inserted:
                    _alarm_code = AlarmCode.CountR_Door_HeadDoor_Opened;
                    break;
                }
                break;
            }
            break;

          case Event.OperatorCardInserted:
            switch (CountRMode)
            {
              case Mode.Closed_Rejected:
                _alarm_code = AlarmCode.CountR_OperatorCard_Rejected;
                break;
              case Mode.Opened_Inserted:
                _alarm_code = AlarmCode.CountR_OperatorCard_Inserted;
                break;
              case Mode.Captured_Stacked:
                _alarm_code = AlarmCode.CountR_OperatorCard_Captured;
                break;
              case Mode.Issued:
                _alarm_code = AlarmCode.CountR_OperatorCard_Issued;
                break;
            }
            break;

          case Event.CustomerCardInserted:
            switch (CountRMode)
            {
              case Mode.Closed_Rejected:
                _alarm_code = AlarmCode.CountR_CustomerCard_Rejected;
                break;
              case Mode.Opened_Inserted:
                _alarm_code = AlarmCode.CountR_CustomerCard_Inserted;
                break;
              case Mode.Captured_Stacked:
                _alarm_code = AlarmCode.CountR_CustomerCard_Captured;
                break;
              case Mode.Issued:
                _alarm_code = AlarmCode.CountR_CustomerCard_Issued;
                break;
            }
            break;

          case Event.BanknoteOrTicketInserted:
            switch (CountRSubEvent)
            {
              case SubEvent.MainDoor_Banknote:
                switch (CountRMode)
                {
                  case Mode.Closed_Rejected:
                    _alarm_code = AlarmCode.CountR_Banknote_Rejected;
                    break;
                  case Mode.Opened_Inserted:
                    _alarm_code = AlarmCode.CountR_Banknote_Inserted;
                    break;
                  case Mode.Captured_Stacked:
                    _alarm_code = AlarmCode.CountR_Banknote_Stacked;
                    break;
                }
                break;
              case SubEvent.HeadDoor_Ticket:
                switch (CountRMode)
                {
                  case Mode.Closed_Rejected:
                    _alarm_code = AlarmCode.CountR_Ticket_Rejected;
                    break;
                  case Mode.Opened_Inserted:
                    _alarm_code = AlarmCode.CountR_Ticket_Inserted;
                    break;
                  case Mode.Captured_Stacked:
                    _alarm_code = AlarmCode.CountR_Ticket_Stacked;
                    break;
                }
                break;
            }
            break;

          case Event.CallOperatorMode:
          default:
            _alarm_code = AlarmCode.CountR_CallOperator;
            break;
        }


        if (!Alarm.Register(AlarmSourceCode.Cashier,
                            0,
                            CountRCode + "@" + CountRName,
                            (UInt32)_alarm_code,
                            "",
                            AlarmSeverity.Warning,
                            WGDB.Now,
                            SqlTrx))
        {

          return false;
        }

        Response.Ack = Ack.ACK;
        Response.AckExtended = AckExtended.None;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("CountRBusinessLogic SendEvent");
        Log.Exception(_ex);
        Response.Ack = Ack.NAK;
        Response.AckExtended = AckExtended.InformationNotAvailable;
      }

      return false;
    } // SendEvent

    /// <summary>
    /// Validate if the terminal is CountR
    /// Used in GUI
    /// </summary>
    /// <param name="CashierTerminalId"></param>
    /// <param name="CountRId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean IsCountRByCashierTerminalId(Int32 CashierTerminalId, out Int32 CountRId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      CountRId = -1;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   ISNULL(CT_COUNTR_ID, -1)    ");
        _sb.AppendLine("   FROM   CASHIER_TERMINALS           ");
        _sb.AppendLine("  WHERE   CT_CASHIER_ID = @pCashierId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CashierTerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              CountRId = _reader.GetInt32(0);

              if (CountRId > -1)
              {
                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsCountRByCashierTerminalId

    /// <summary>
    /// Get or open cashier session info with GUI User
    /// </summary>
    /// <param name="CountR"></param>
    /// <param name="UserID"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="Csi"></param>
    /// <param name="Crs"></param>
    /// <returns></returns>
    public static Boolean GetOrOpenCashierSessionInfoWithGUIUser(CountR CountR, Int32 UserID, String UserName, SqlTransaction SqlTrx, out CashierSessionInfo Csi, out CountRSession Crs)
    {
      // Get CashierSessionInfo and CountRSessionInfo. 
      // CashierSession info must be overwrite by GUI User session info after.
      Crs = null;
      Csi = GetorOpenCountRSessions(CountR, SqlTrx, out Crs);

      if (Csi == null)
      {
        return false;
      }

      CommonCashierInformation.SetCashierInformation_GUIUser(Csi.CashierSessionId, UserID, UserName, SqlTrx, out Csi);

      return true;
    } // GetOrOpenCashierSessionInfoWithGUIUser

    /// <summary>
    /// Get cashier session info by id with GUI User
    /// </summary>
    /// <param name="CountRSessionId"></param>
    /// <param name="UserID"></param>
    /// <param name="UserName"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="Csi"></param>
    /// <param name="Crs"></param>
    /// <returns></returns>
    public static Boolean GetCashierSessionInfoByIdWithGUIUser(Int32 CountRSessionId, Int32 UserID, String UserName, SqlTransaction SqlTrx, out CashierSessionInfo Csi, out CountRSession Crs)
    {
      Crs = null;
      Csi = null;

      // Get CashierSessionInfo and CountRSessionInfo by id. 
      if (!GetCountRSessionsById(CountRSessionId, out Crs, SqlTrx))
      {
        return false;
      }

      CommonCashierInformation.SetCashierInformation_GUIUser(CountRSessionId, UserID, UserName, SqlTrx, out Csi);

      return true;
    } // GetCashierSessionInfoByIdWithGUIUser

    /// <summary>
    /// Check ticket
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="TicketNumber"></param>
    /// <param name="Amount"></param>
    /// <param name="CheckAmount"></param>
    /// <param name="CreateOperation"></param>
    /// <param name="Response"></param>
    /// <param name="PayTicket"></param>
    /// <param name="PayCountR"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean CheckTicket(Int32 CountRCode, String TicketNumber, Int64 Amount, Boolean CheckAmount, Boolean CreateOperation,
                                       out ACKRedeemKiosk Response, out Ticket PayTicket, out CountR PayCountR, SqlTransaction SqlTrx)
    {
      TITO_VALIDATION_TYPE _validation_type;
      Int64 _ticket_number;
      String _condition;
      DateTime _date_limit_payment;
      CashierSessionInfo _cashier_session_info;
      CountRSession _countr_session;
      Int32 _user_id;
      String _user_name;
      CardData _card_data;
      ParamsTicketOperation _operation_params;
      CountRTransaction _countr_trans;
      Boolean _allow_pay_tickets_pending_print;
      Boolean _allow_to_redeem_gp;


      Response = new ACKRedeemKiosk();
      PayCountR = new CountR();
      PayTicket = new Ticket();
      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);

      if (!CountR.DB_GetCountRByCode(CountRCode, out PayCountR, SqlTrx))
      {
        Log.Error(String.Format("CountRBusinessLogic.CheckTicket: Redeem Kiosk {0} not found", CountRCode));
        Response.Ack = Ack.UnknownCashIO;
        Response.AckExtended = AckExtended.UnableToValidate;

        return false;
      }

      _ticket_number = ValidationNumberManager.UnFormatValidationNumber(TicketNumber, out _validation_type);
      if (_ticket_number == 0)
      {
        Response.Ack = Ack.AccountInvalid;
        Response.AckExtended = AckExtended.NumberNotValid;

        return false;
      }

      //Get Ticket
      _condition = " TI_VALIDATION_NUMBER = '" + _ticket_number.ToString() + "' ";
      PayTicket = SelectTicketByCondition(_condition, SqlTrx);

      if (PayTicket.TicketID <= 0)
      {
        Response.Ack = Ack.AccountInvalid;
        Response.AckExtended = AckExtended.NumberUnknown;

        return false;
      }
      if (PayTicket.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
      {
        Response.Ack = Ack.NAK;
        Response.AckExtended = AckExtended.ValidRestricted;// PENDING TO VALIDATE THIS OPTION

        return false;
      }

      switch (PayTicket.Status)
      {
        case TITO_TICKET_STATUS.EXPIRED:
          CheckGracePeriod(PayTicket.ExpirationDateTime, out _date_limit_payment);
          if (_date_limit_payment < WGDB.Now)
          {
            Response.Ack = Ack.AccountInvalid;
            Response.AckExtended = AckExtended.Expired;

            return false;
          } // TICKET EXPIRED

          Response.Ack = Ack.ACK;
          Response.AckExtended = AckExtended.ValidRestricted; // PENDING TO VALIDATE THIS OPTION
          PayTicket.Status = TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION;
          break;

        case TITO_TICKET_STATUS.PENDING_CANCEL:
          _allow_to_redeem_gp = GeneralParam.GetBoolean("TITO", "PendingCancel.AllowToRedeem", false);

          if (_allow_to_redeem_gp)
          {
            PayTicket.Status = TITO_TICKET_STATUS.PENDING_PAYMENT;
            Response.Ack = Ack.ACK;
            Response.AckExtended = AckExtended.None;
          }
          break;

        case TITO_TICKET_STATUS.VALID:
          PayTicket.Status = TITO_TICKET_STATUS.PENDING_PAYMENT;
          Response.Ack = Ack.ACK;
          Response.AckExtended = AckExtended.None;
          break;

        case TITO_TICKET_STATUS.PENDING_PRINT:
          if (_allow_pay_tickets_pending_print)
          {
            PayTicket.Status = TITO_TICKET_STATUS.PENDING_PAYMENT;
            Response.Ack = Ack.ACK;
            Response.AckExtended = AckExtended.None;
          }
          Log.Message(String.Format("CountRBusnessLogic CheckTicket: Ticket {0} status is PENDING PRINTING", PayTicket.ValidationNumber));
          break;

        default:
          Response.Ack = Ack.AccountInvalid;
          Response.AckExtended = AckExtended.AlreadyRedeemed;

          return false; // Incorrect status
      }

      Response.AmountCents = Decimal.ToInt64(PayTicket.Amount * 100.00m); //amount in cents

      // Check amount
      if (CheckAmount)
      {
        // Check ticket amount
        if (Amount != Response.AmountCents)
        {
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.AmountMismatch;

          return false;
        }

        // Check Max/Min Payment amount
        if ((PayTicket.Amount < PayCountR.MinPayment) || ((PayCountR.MaxPayment > 0) && (PayTicket.Amount > PayCountR.MaxPayment)))
        {
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.ExceedsLimit;

          return false;
        }
      }

      // Create operation
      if (CreateOperation)
      {
        // CashierSessionInfo
        _cashier_session_info = new CashierSessionInfo();
        _cashier_session_info = CountRBusinessLogic.GetorOpenCountRSessions(PayCountR, SqlTrx, out _countr_session);
        if (_cashier_session_info == null)
        {
          Log.Error("CountRBusinessLogic CheckTicket: CashierSessionInfo not found");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.InformationNotAvailable;

          return false;
        }
        if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_REDEMPTION, SqlTrx, out _user_id, out _user_name))
        {
          Log.Message("CountRBusinessLogic CheckTicket.GetSystemUser: Error getting the user (SYS_COUNTR) for the CountR");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.InformationNotAvailable;

          return false;
        }
        _cashier_session_info.AuthorizedByUserId = _user_id;
        _cashier_session_info.AuthorizedByUserName = _user_name;

        // card Data
        _card_data = new CardData();
        if (!CardData.DB_VirtualCardGetAllData(_cashier_session_info.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, _card_data))
        {
          Log.Message("CountRBusinessLogic CheckTicket.DB_VirtualCardGetAllData: Error");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.InformationNotAvailable;

          return false;
        }

        // ParamsTicketOperation
        _operation_params = new ParamsTicketOperation();
        _card_data.MaxDevolution = Utils.MaxDevolutionAmount(PayTicket.Amount);
        _operation_params.in_account = _card_data;
        _operation_params.session_info = _cashier_session_info;
        _operation_params.cashier_terminal_id = _cashier_session_info.TerminalId;
        _operation_params.tito_terminal_types = TITO_TERMINAL_TYPE.COUNTR;
        _operation_params.in_cash_amount = PayTicket.Amount;
        _operation_params.in_cash_amt_0 = PayTicket.Amount;
        _operation_params.cash_redeem_total_paid = PayTicket.Amount;
        _operation_params.max_devolution = Utils.MaxDevolutionAmount(PayTicket.Amount);
        _operation_params.validation_numbers = new List<VoucherValidationNumber>();
        _operation_params.out_operation_id = m_out_operationID;

        // Create Operation
        if (!Operations.DB_InsertOperation(OperationCode.CASH_OUT,
                                           _operation_params.in_account.AccountId,
                                           _cashier_session_info.CashierSessionId,
                                           0,
                                           _operation_params.out_promotion_id,
                                           0,
                                           0,
                                           0,                      // Operation Data
                                           _operation_params.AccountOperationReasonId,
                                           _operation_params.AccountOperationComment,
                                           out m_out_operationID, SqlTrx))
        {
          Log.Error("CountRBusinessLogic CheckTicket.DB_InsertOperation: Error creating operation.");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }

        // Create CountR transaction
        _countr_trans = new CountRTransaction();
        _countr_trans.PaidTicketId = PayTicket.TicketID;
        _countr_trans.OperationId = m_out_operationID;
        _countr_trans.PaidAmount = PayTicket.Amount;
        _countr_trans.CashPaidAmount = PayTicket.Amount; // Pending validate correct amount
        _countr_trans.CountrId = PayCountR.Id;
        _countr_trans.CashierSessionId = _cashier_session_info.CashierSessionId;

        if (!_countr_trans.DB_Insert(SqlTrx))
        {
          Log.Error("CountRBusinessLogic CheckTicket.DB_Insert: Error creating CountR transaction");
          Response.Ack = Ack.NAK;
          Response.AckExtended = AckExtended.UnableToValidate;

          return false;
        }
      }

      return true;
    } // CheckTicket

    /// <summary>
    /// Calculate Taxes Federal and State
    /// </summary>
    /// <param name="CountRTaxes"></param>
    /// <param name="Amount"></param>
    /// <returns></returns>
    public static Boolean AmountTaxes(Ticket Ticket)
    {
      DevolutionPrizeParts _parts;
      CountRTaxes _countr_taxes;

      _parts = new DevolutionPrizeParts(false);

      _countr_taxes = new CountRTaxes();
      if (!Tax.EnableTitoTaxWaiver(true))
      {
        return false;
      }

      _parts.Prize = Ticket.Amount;
      _parts.Compute(Ticket.Amount, 0, false, false);

      _countr_taxes.Amount = _parts.Tax1.TaxAmount;
      _countr_taxes.TaxName = _parts.Tax1.TaxName;
      _countr_taxes.Percentage = _parts.Tax1.TaxPct;
      Ticket.Taxes = new List<CountRTaxes>();

      Ticket.Taxes.Add(_countr_taxes);

      _countr_taxes = null;
      _countr_taxes = new CountRTaxes();
      _countr_taxes.Amount = _parts.Tax2.TaxAmount;
      _countr_taxes.TaxName = _parts.Tax2.TaxName;
      _countr_taxes.Percentage = _parts.Tax2.TaxPct;

      Ticket.Taxes.Add(_countr_taxes);


      _countr_taxes = null;
      _countr_taxes = new CountRTaxes();
      _countr_taxes.Amount = _parts.Tax3.TaxAmount;
      _countr_taxes.TaxName = _parts.Tax3.TaxName;
      _countr_taxes.Percentage = _parts.Tax3.TaxPct;

      Ticket.Taxes.Add(_countr_taxes);

      return true;
    } // AmountTaxes

    /// <summary>
    /// Get the ticket by TicketNumber
    /// </summary>
    /// <param name="GeneralCondition"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Ticket SelectTicketByCondition(String GeneralCondition, SqlTransaction SqlTrx)
    {
      DataTable _tickets;
      Ticket _ticket;
      Boolean _ok;
      DataRow _row;

      _tickets = new DataTable();
      _ticket = new Ticket();
      _ok = true;
      _row = null;

      if (!Ticket.LoadTicketsByCondition(GeneralCondition, ref _tickets, SqlTrx) || _tickets.Rows.Count == 0 || _tickets.Rows.Count > 1)
      {
        _ok = false;
      }

      if (_ok)
      {
        _row = _tickets.Rows[0];
        _ticket = GetTicket(_row);
      }

      return _ticket;
    } // SelectTicketByCondition

    /// <summary>
    /// Check if Ticket is in grace period
    /// </summary>
    /// <param name="ExpirationDate"></param>
    /// <param name="DateLimit"></param>
    public static void CheckGracePeriod(DateTime ExpirationDate, out DateTime DateLimit)
    {
      Object _grace_period;
      DateTime _grace_period_limit;

      _grace_period = Misc.GetTITOPaymentGracePeriod();
      _grace_period_limit = DateTime.MinValue;

      if (_grace_period != null)
      {
        if (_grace_period is Int32)
        {
          _grace_period_limit = ExpirationDate.AddDays((Int32)_grace_period);
        }
        else if (_grace_period is DateTime)
        {
          _grace_period_limit = new DateTime(ExpirationDate.Year, ((DateTime)_grace_period).Month, ((DateTime)_grace_period).Day);
        }
      }

      if (ExpirationDate > _grace_period_limit)
      {
        DateLimit = _grace_period_limit.AddYears(1);
      }
      else
      {
        DateLimit = _grace_period_limit;
      }
    } // CheckGracePeriod

    /// <summary>
    /// Update status ticket
    /// </summary>
    /// <param name="Ticket"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean UpdateTicketStatus(Ticket Ticket, SqlTransaction SqlTrx)
    {
      try
      {
        return Ticket.DB_UpdateTicket(Ticket, SqlTrx);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error(String.Format("CountRBusinessLogic.UpdateTicketStatus -> Ticket {0}", Ticket.ValidationNumber));
      }

      return false;
    } // UpdateTicketStatus

    #endregion " Public Methods "


    #region " Private Methods "

    /// <summary>
    /// Set values from datarow to Ticket Class
    /// </summary>
    /// <param name="RowTicket"></param>
    /// <returns>Ticket</returns>
    private static Ticket GetTicket(DataRow RowTicket)
    {
      Ticket _ticket;

      _ticket = new Ticket();

      try
      {
        _ticket.TicketID = (Int64)RowTicket["TI_TICKET_ID"];
        _ticket.ValidationNumber = (Int64)RowTicket["TI_VALIDATION_NUMBER"];
        _ticket.ValidationType = (TITO_VALIDATION_TYPE)RowTicket["TI_VALIDATION_TYPE"];
        _ticket.Amount = (Decimal)RowTicket["TI_AMOUNT"];
        _ticket.Status = (TITO_TICKET_STATUS)RowTicket["TI_STATUS"];
        _ticket.TicketType = (TITO_TICKET_TYPE)RowTicket["TI_TYPE_ID"];
        _ticket.CreatedDateTime = (DateTime)RowTicket["TI_CREATED_DATETIME"];
        _ticket.CreatedTerminalId = (Int32)RowTicket["TI_CREATED_TERMINAL_ID"];
        _ticket.CreatedTerminalName = (String)RowTicket["TERMINAL_NAME"];
        _ticket.CreatedTerminalType = (Int32)RowTicket["TI_CREATED_TERMINAL_TYPE"];
        _ticket.TransactionId = (Int64)RowTicket["TI_TRANSACTION_ID"];
        _ticket.CreatedPlaySessionId = (Int64)RowTicket["TI_CREATED_PLAY_SESSION_ID"];
        if (RowTicket["TI_EXPIRATION_DATETIME"] != null && RowTicket["TI_EXPIRATION_DATETIME"] != DBNull.Value)
        {
          _ticket.ExpirationDateTime = (DateTime)RowTicket["TI_EXPIRATION_DATETIME"];
        }
        _ticket.MoneyCollectionID = (Int64)RowTicket["MONEY_COLLECTION"];
        _ticket.CreatedAccountID = (Int64)RowTicket["CREATED_ACCOUNT_ID"];
        _ticket.LastActionTerminalID = (Int32)RowTicket["LAST_TERMINAL_ID"];
        _ticket.LastActionTerminalName = (String)RowTicket["LAST_TERMINAL_NAME"];
        if (RowTicket["TI_LAST_ACTION_DATETIME"] != null && RowTicket["TI_LAST_ACTION_DATETIME"] != DBNull.Value)
        {
          _ticket.LastActionDateTime = (DateTime)RowTicket["TI_LAST_ACTION_DATETIME"];
        }
        _ticket.LastActionAccountID = (Int64)RowTicket["LAST_ACCOUNT_ID"];
        _ticket.LastActionTerminalType = (Int32)RowTicket["LAST_TERMINAL_TYPE"];
        _ticket.PromotionID = (Int64)RowTicket["PROMOTION"];
        _ticket.CollectedMoneyCollection = (Int64)RowTicket["COLLECTED"];
        _ticket.MachineTicketNumber = (Int32)RowTicket["TI_MACHINE_NUMBER"];

        _ticket.Amt_0 = (Decimal)RowTicket["TI_AMT0"];
        _ticket.Cur_0 = (String)RowTicket["TI_CUR0"];
        if (RowTicket["TI_AMT1"] != null && RowTicket["TI_AMT1"] != DBNull.Value)
        {
          _ticket.Amt_1 = (Decimal)RowTicket["TI_AMT1"];

        }
        if (RowTicket["TI_CUR1"] != null && RowTicket["TI_CUR1"] != DBNull.Value)
        {
          _ticket.Cur_1 = (String)RowTicket["TI_CUR1"];
        }
      }
      catch (Exception _ex)
      {
        Log.Error("CountRBusinessLogic GetTicket");
        Log.Exception(_ex);
      }

      return _ticket;
    } // GetTicket


    /// <summary>
    /// Get input parameters from CardData
    /// </summary>
    /// <param name="CardData"></param>
    /// <returns></returns>
    private static RechargeInputParameters GetInputParameters(CardData CardData)
    {
      RechargeInputParameters _input_params;

      _input_params = new RechargeInputParameters();

      try
      {
        _input_params.AccountId = CardData.AccountId;
        _input_params.AccountLevel = CardData.PlayerTracking.CardLevel;
        _input_params.CardPaid = CardData.CardPaid;
        _input_params.CardPrice = Accounts.CardDepositToPay(CardData);
        _input_params.ExternalTrackData = CardData.TrackData;
        _input_params.VoucherAccountInfo = CardData.VoucherAccountInfo();
        _input_params.ParticipateInCashDeskDraw = ParticipateInCashDeskDraw.No;
        _input_params.ChipSaleRegister = false;
        _input_params.GenerateVouchers = false;
      }
      catch (Exception _ex)
      {
        Log.Error("CountRBusiness GetInputParameters");
        Log.Exception(_ex);
      }

      return _input_params;
    } // GetInputParameters

    #endregion " Private Methods "
  }

  public class ACKRedeemKiosk
  {
    public Int64 AmountCents { get; set; }
    public CountRBusinessLogic.Ack Ack { get; set; }
    public CountRBusinessLogic.AckExtended AckExtended { get; set; }
    public CountRBusinessLogic.PurseType PurseType { get; set; }
    public String ValidationNumberSeq { get; set; }
    public DateTime TicketPetitionDate { get; set; }

    public ACKRedeemKiosk()
    {
      this.Ack = CountRBusinessLogic.Ack.NAK;
      this.PurseType = CountRBusinessLogic.PurseType.Player;
      this.AckExtended = CountRBusinessLogic.AckExtended.None;
      this.AmountCents = 0;
      this.ValidationNumberSeq = String.Empty.PadLeft(18, '0');
    }
  }
}

