﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CountR.cs
// 
//   DESCRIPTION: Taxes for CountR
// 
//        AUTHOR: Yineth Nuñez
// 
// CREATION DATE: 23-MAR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAR-2016 YNM    First release (Header).
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.CountR
{
  public class CountRTaxes
  {
    #region Public Properties

    public Currency Amount { get; set; }
    public String TaxName { get; set; }
    public Decimal Percentage { get; set; }
    
    #endregion     
  }
}
