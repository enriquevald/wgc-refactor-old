//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PasswordPolicy.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. PasswordPolicy
//
//        AUTHOR: JCM
// 
// CREATION DATE: 30-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2007 JCM    First release.
// 10-JUL-2013 DHA    Changed the length from the field 'Password' from 15 to 22 characters
// 16-JUL-2014 DDM-RCI    Fixed Bug WIG-1086:
// 29-NOV-2016 FAV    PBI 20347:EGASA: New LOGIN card in CASHIER
// 24-JAN-2017 FOS    Fixed Bug 17568: Error to apply the new password's rules
// 24-JAN-2017 FOS    Fixed Bug 23509:GP Password.OldRules: es necesario acceder con el password en may�sculas
// 28-FEB-2018 DPC    Bug 31753:[WIGOS-3289]: [Ticket #6818] EGASA/Kurrsaal-Politica de contrase�a
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;

namespace WSI.Common
{
  public class PasswordPolicy
  {
    #region Private Constants

    public const int MIN_PASSWORD_LENGHT_CONST = 4;
    public const int MAX_PASSWORD_LENGTH_CONST = 22;
    public const int MIN_LOGIN_LENGTH_CONST = 4;
    public const int MAX_LOGIN_LENGTH_CONST = 15;
    public const int MIN_PASSWORD_DIGITS_CONST = 0;
    public const int MAX_PASSWORD_DIGITS_CONST = 22;
    public const int MIN_PASSWORD_LOWERCASE_CONST = 0;
    public const int MAX_PASSWORD_LOWERCASE_CONST = 22;
    public const int MIN_PASSWORD_UPPERCASE_CONST = 0;
    public const int MAX_PASSWORD_UPPERCASE_CONST = 22;
    public const int MIN_PASSWORD_SPECIALCHARACTER_CONST = 0;
    public const int MAX_PASSWORD_SPECIALCHARACTER_CONST = 22;
    public const int MIN_LOGIN_ATTEMPTS_CONST = 1;
    public const int MAX_LOGIN_ATTEMPTS_CONST = Int32.MaxValue;
    public const int MIN_PASSWORD_HISTORY_CONST = 0;
    public const int MAX_PASSWOD_HISTORY_CONST = 5;
    public const int MIN_CHANGE_PASS_N_DAYS = 0;
    public const int MAX_CHANGE_PASS_N_DAYS = Int32.MaxValue;

    private const int DEFAULT_PASSWOD_LENGTH = 6;
    private const int DEFAULT_LOGIN_LENGTH = 6;
    private const int DEFAULT_COUNT_OF_DIGITS = 2;
    private const int DEFAULT_COUNT_OF_LOWERCASE = 2;
    private const int DEFAULT_COUNT_OF_UPPERCASE = 2;
    private const int DEFAULT_COUNT_OF_SPECIAL_CHARACTER = 0;
    private const int DEFAULT_COUNT_OF_LOGIN_ATTEMPT = 3;
    private const int DEFAULT_COUNT_OF_PASSWORD_HISTORY = 5;
    private const int DEFAULT_COUNT_OF_DAYS_WITHOUT_LOGIN = 24;
    private const int DEFAULT_CHANGE_PASSWORD_IN_N_DAYS = 0;
    private const int DEFAULT_LOGIN_ATTEMPTS = 3;

    #endregion

    #region Private Values

    private Boolean m_rules_enabled;
    private Boolean m_old_rules_enabled;
    private Boolean m_is_super_user;
    private Int32 m_min_password_length;
    private Int32 m_min_digit;
    private Int32 m_min_lower_case;
    private Int32 m_min_upper_case;
    private Int32 m_min_special_case;
    private Int32 m_max_password_history;
    private Int32 m_max_login_attempts;
    private Int32 m_min_login_length;
    private Int32 m_max_days_without_login;
    private Int32 m_change_in_n_days;

    #endregion

    #region Public Geters

    public Int32 MaxLoginAttempts
    {
      get { return m_max_login_attempts; }
    }
    public Boolean RulesEnabled
    {
      get { return m_rules_enabled; }
    }
    public Boolean OldRulesEnabled
    {
      get { return m_old_rules_enabled; }
    }

    public Int32 MinPasswordLength
    {
      get { return m_min_password_length; }
    }
    public Int32 MinPasswordDigits
    {
      get { return m_min_digit; }
    }
    public Int32 MinPasswordLowerCase
    {
      get { return m_min_lower_case; }
    }
    public Int32 MinPasswordUpperCase
    {
      get { return m_min_upper_case; }
    }
    public Int32 MinPasswordSpecialCase
    {
      get { return m_min_special_case; }
    }
    public Int32 MaxPasswordHistory
    {
      get { return m_max_password_history; }
    }
    public Int32 MinLoginLength
    {
      get { return m_min_login_length; }
    }
    public Int32 ChangeInNDays
    {
      get { return m_change_in_n_days; }
    }
    public Int32 MaxDaysWithoutLogin
    {
      get { return m_max_days_without_login; }
    }

    #endregion

    public PasswordPolicy()
    {
      Int32 _gp_value;

      //Set default Values
      m_rules_enabled = false;
      m_max_login_attempts = DEFAULT_LOGIN_ATTEMPTS;
      m_min_password_length = DEFAULT_PASSWOD_LENGTH;
      m_min_digit = DEFAULT_COUNT_OF_DIGITS;
      m_min_lower_case = DEFAULT_COUNT_OF_LOWERCASE;
      m_min_upper_case = DEFAULT_COUNT_OF_UPPERCASE;
      m_min_special_case = DEFAULT_COUNT_OF_SPECIAL_CHARACTER;
      m_max_password_history = DEFAULT_COUNT_OF_PASSWORD_HISTORY;
      m_min_login_length = DEFAULT_LOGIN_LENGTH;
      m_change_in_n_days = DEFAULT_CHANGE_PASSWORD_IN_N_DAYS;
      m_max_days_without_login = DEFAULT_COUNT_OF_DAYS_WITHOUT_LOGIN;

      // Check Old RulesEnabled
      m_old_rules_enabled = GeneralParam.GetBoolean("User", "Password.OldRules", false);

      //Check PWD Policy Enabled
      if (int.TryParse(WSI.Common.GeneralParam.Value("User", "Password.RulesEnabled"), out _gp_value))
      {
        m_rules_enabled = (_gp_value == 1);
      }

      // MaxLoginAttempts is used always.
      if (int.TryParse(WSI.Common.GeneralParam.Value("User", "MaxLoginAttempts"), out _gp_value))
      {
        m_max_login_attempts = _gp_value;
      }

      if (m_rules_enabled)
      {
        if (int.TryParse(GeneralParam.Value("User", "Password.MinLength"), out _gp_value))
        {
          m_min_password_length = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MinDigits"), out _gp_value))
        {
          m_min_digit = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MinLowerCase"), out _gp_value))
        {
          m_min_lower_case = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MinUpperCase"), out _gp_value))
        {
          m_min_upper_case = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MinSpecialCase"), out _gp_value))
        {
          m_min_special_case = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.MaxPasswordHistory"), out _gp_value))
        {
          m_max_password_history = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Login.MinLength"), out _gp_value))
        {
          m_min_login_length = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "Password.ChangeInNDays"), out _gp_value))
        {
          m_change_in_n_days = _gp_value;
        }
        if (int.TryParse(GeneralParam.Value("User", "MaxDaysWithoutLogin"), out _gp_value))
        {
          m_max_days_without_login = _gp_value;
        }

      } // m_rules_enabled = true

    } // PasswordPolicy

    // PURPOSE: Check the password rules.
    //          The password don't should break the password policy rules.
    //
    //  PARAMS:
    //     - INPUT:
    //           - MachineName: string;
    //           - UserName:    string;
    //           - Password:    string;
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - bool: if check is ok then return "true" else "false".
    //
    public Boolean CheckPasswordRules(String MachineName,
                                      String UserName,
                                      Int32 UserId,
                                      String Password)
    {
      int _num_upper = 0;
      int _num_lower = 0;
      int _num_digit = 0;
      int _num_special = 0;

      // Check WhiteSpace at Begin or End of password
      if (Password.Trim() != Password)
      {
        return false;
      }

      // UserId = -1, it's a new user. Don't check the recently used passwords.
      if (UserId >= 0)
      {
        // Check password history
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (PasswordRecentlyUsed(UserId, Password, _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();
        }
      }

      // All Rules When Password Policy Disabled are checked.
      if (!RulesEnabled)
      {
        return true;
      }

      // Check Length
      if (Password.Length < MinPasswordLength)
      {
        return false;
      }

      // Should not contain the UserName
      if (Password.ToUpper().Contains(UserName.ToUpper()))
      {
        return false;
      }
      // Should not contain the MachineName
      if (Password.ToUpper().Contains(MachineName.ToUpper()))
      {
        return false;
      }

      // Count char types
      _num_upper = 0;
      _num_lower = 0;
      _num_digit = 0;
      _num_special = 0;
      foreach (char _char in Password.ToCharArray())
      {
        if (char.IsLetterOrDigit(_char))
        {
          if (char.IsDigit(_char))
          {
            _num_digit += 1;
          }
          else if (char.IsUpper(_char))
          {
            _num_upper += 1;
          }
          else
          {
            _num_lower += 1;
          }
        }
        else
        {
          _num_special += 1;
        }
      }

      // Check char types rules
      if (_num_upper < MinPasswordUpperCase)
      {
        return false;
      }
      if (_num_lower < MinPasswordLowerCase)
      {
        return false;
      }
      if (_num_digit < MinPasswordDigits)
      {
        return false;
      }
      if (_num_special < MinPasswordSpecialCase)
      {
        return false;
      }

      // Valid password
      return true;
    } // CheckPasswordRules

    // PURPOSE: Verify the current password.
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:      Int32;
    //           - Password:    string;
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - bool: if verification is ok then return "true" else "false".
    //
    public Boolean VerifyCurrentPassword(Int32 UserId, String Password, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Byte[] _bin_pwd;
      Int32 _effective_user_id;
      Boolean _is_valid_password;

      try
      {
        _is_valid_password = false;

        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   GU_PASSWORD ");
        _sb.AppendLine("       , ISNULL(GU_MASTER_ID, @pUserId) ");
        _sb.AppendLine("  FROM   GUI_USERS   ");
        _sb.AppendLine(" WHERE   GU_USER_ID = @pUserId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            _bin_pwd = _reader.GetSqlBytes(0).Buffer;
            _effective_user_id = _reader.GetInt32(1);
          }

          if (_bin_pwd[0] == 0)
          {
            Byte[] _in_pwd;

            _in_pwd = this.HashPassword(_effective_user_id, Password);
            _is_valid_password = ComparePassword(_in_pwd, _bin_pwd);

            if (!_is_valid_password)
            {
              if (_effective_user_id == 0) //If password is not validated, check if user is SuperUser and check again the password.
              {
                m_is_super_user = true;

                _in_pwd = this.HashPassword(_effective_user_id, Password);
                _is_valid_password = ComparePassword(_in_pwd, _bin_pwd);
              }
            }

            return _is_valid_password;
          }
          else
          {
            String _aux;

            _aux = Encoding.Default.GetString(_bin_pwd);
            _aux = _aux.Substring(0, _aux.IndexOf('\0'));

            return (PasswordEquals(Password, _aux));
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // VerifyCurrentPassword

    public Boolean PasswordRecentlyUsed(Int32 UserId, String Password, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      String _db_pwd_str;
      Byte[] _db_pwd_bin;
      String _in_pwd_str;
      Byte[] _in_pwd_bin;

      try
      {
        _in_pwd_str = CleanExtraCharsFromTrackdata(Password);
        _in_pwd_bin = HashPassword(UserId, Password);

        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   GU_PASSWORD    ");
        _sb.AppendLine("       , GU_PASSWORD_H1 ");
        _sb.AppendLine("       , GU_PASSWORD_H2 ");
        _sb.AppendLine("       , GU_PASSWORD_H3 ");
        _sb.AppendLine("       , GU_PASSWORD_H4 ");
        _sb.AppendLine("       , GU_PASSWORD_H5 ");
        _sb.AppendLine("  FROM   GUI_USERS      ");
        _sb.AppendLine(" WHERE   GU_USER_ID = @pUserId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              // Error reading, return password recently used
              return true;
            }

            for (int _idx_pwd = 0; _idx_pwd <= 5; _idx_pwd++)
            {
              if (_reader.IsDBNull(_idx_pwd))
              {
                continue;
              }

              _db_pwd_bin = _reader.GetSqlBytes(_idx_pwd).Buffer;
              if (_db_pwd_bin == null)
              {
                continue;
              }

              if (_db_pwd_bin[0] != 0)
              {
                _db_pwd_str = Encoding.Default.GetString(_db_pwd_bin);
                _db_pwd_str = _db_pwd_str.Substring(0, _db_pwd_str.IndexOf('\0'));
                if (_db_pwd_str.ToUpper() == _in_pwd_str.ToUpper())
                {
                  return true;
                }
                continue;
              }

              if (PasswordPolicy.ComparePassword(_in_pwd_bin, _db_pwd_bin))
              {
                return true;
              }
            }
          }
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return true;

    }// PasswordRecentlyUsed


    // PURPOSE: Update user's password.
    //   
    //    - INPUT:
    //       - UserId: Internal user identifier.
    //       - Password: Password to be checked.
    //       - SqlTrans: Database context.
    //
    //    - OUTPUT:
    //
    // RETURNS:
    //   - DB_ERROR
    //   - OK
    //
    // NOTES:

    public Boolean ChangeUserPassword(Int32 UserId, String Password, SqlTransaction Trx)
    {
      return ChangeUserPassword(UserId, Password, true, Trx);
    }

    public Boolean SetUserPassword(Int32 UserId, String Password, SqlTransaction Trx)
    {
      return ChangeUserPassword(UserId, Password, false, Trx);
    }


    private Boolean ChangeUserPassword(Int32 UserId,
                                       String Password,
                                       Boolean ResetChangeRequired,
                                       SqlTransaction Trx)
    {
      StringBuilder _sb;
      Byte[] _bin_pwd;

      try
      {
        _bin_pwd = HashPassword(UserId, Password);

        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   GUI_USERS");
        _sb.AppendLine("   SET   GU_PASSWORD     = @pPassword     ");
        _sb.AppendLine("       , GU_PASSWORD_H1  = GU_PASSWORD    ");
        _sb.AppendLine("       , GU_PASSWORD_H2  = GU_PASSWORD_H1 ");
        _sb.AppendLine("       , GU_PASSWORD_H3  = GU_PASSWORD_H2 ");
        _sb.AppendLine("       , GU_PASSWORD_H4  = GU_PASSWORD_H3 ");
        _sb.AppendLine("       , GU_PASSWORD_H5  = GU_PASSWORD_H4 ");
        _sb.AppendLine("       , GU_LAST_CHANGED = GETDATE()      ");
        _sb.AppendLine("       , GU_PWD_CHG_REQ  = CASE WHEN (@pReset = 1) THEN 0 ELSE GU_PWD_CHG_REQ END ");
        _sb.AppendLine(" WHERE   GU_USER_ID      = @pUserId      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _cmd.Parameters.Add("@pPassword", SqlDbType.Binary, 40).Value = _bin_pwd;
          _cmd.Parameters.Add("@pReset", SqlDbType.Bit).Value = ResetChangeRequired;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ChangeUserPassword

    // PURPOSE: Check the login rules.
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserName:    string;
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - bool: if check is ok then return "true" else "false".
    //
    public Boolean CheckLoginRules(String UserName)
    {
      if (!RulesEnabled)
      {
        return true;
      }

      return (UserName.Length >= MinLoginLength);

    }//End CheckLoginRules

    public String ParsePassword(String Password)
    {
      if (String.IsNullOrEmpty(Password))
      {
        return "";
      }

      if (!this.RulesEnabled && this.OldRulesEnabled)
      {
        return Password.ToUpper();
      }

      return Password;
    }

    public Boolean PasswordEquals(String Password1, String Password2)
    {
      String _pwd_1;
      String _pwd_2;

      _pwd_1 = CleanExtraCharsFromTrackdata(Password1);
      _pwd_2 = CleanExtraCharsFromTrackdata(Password2);

      if (String.IsNullOrEmpty(_pwd_1) || String.IsNullOrEmpty(_pwd_2))
      {
        return false;
      }

      if (RulesEnabled)
      {
        return (_pwd_1 == _pwd_2);
      }
      return (_pwd_1.ToUpper() == _pwd_2.ToUpper());
    }

    public String MessagePasswordPolicyInvalid()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      //La nueva clave debe cumplir las siguientes condiciones:\n
      _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_LINE_0"));
      _sb.AppendLine("");

      if (!RulesEnabled)
      {
        //    - La contrase�a no puede dejarse en blanco.\n
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_DISABLE_LINE_1"));
      }
      else
      {
        //     - Debe tener una longitud m�nima de @p0.\n
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_LONG", MinPasswordLength));

        if (MinPasswordDigits > 0)
        {
          //     - Debe tener un m�nimo de @p0 d�gitos.\n
          _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_NUM", MinPasswordDigits));
        }
        if (MinPasswordLowerCase > 0)
        {
          //     - Debe tener un m�nimo de @p0 caracteres en min�sculas.\n
          _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_LOW_CASE_CHARS", MinPasswordLowerCase));
        }
        if (MinPasswordUpperCase > 0)
        {
          //     - Debe tener un m�nimo de @p0 caracteres en mayusculas.\n
          _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_UP_CASE_CHARS", MinPasswordUpperCase));
        }
        if (MinPasswordSpecialCase > 0)
        {
          //     - Debe tener un m�nimo de @p0 caracteres especiales.\n
          _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_MIN_SPECIAL_CHARS", MinPasswordSpecialCase));
        }

        //       - No puede contener el nombre de usuario.
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_USER_CONTAINED"));
        //        - No puede contener el nombre del equipo.
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_COMPUTER_CONTAINED"));
        //        - No puede contener espacios en blanco al inicio ni al final de la palabra clave
        _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_BEGIN_END_WHITE_SPACE"));

      }

      //    - No debe haber sido utilizada anteriormente.
      _sb.AppendLine(Resource.String("STR_PASSWORD_POLICY_ENABLE_HISTORY"));

      return _sb.ToString();
    } // MessagePasswordPolicyInvalid

    public void DisableRule()
    {
      Int32 _gp_value;

      //Set default Values
      m_rules_enabled = false;
      m_max_login_attempts = DEFAULT_LOGIN_ATTEMPTS;
      m_min_password_length = DEFAULT_PASSWOD_LENGTH;
      m_min_digit = DEFAULT_COUNT_OF_DIGITS;
      m_min_lower_case = DEFAULT_COUNT_OF_LOWERCASE;
      m_min_upper_case = DEFAULT_COUNT_OF_UPPERCASE;
      m_min_special_case = DEFAULT_COUNT_OF_SPECIAL_CHARACTER;
      m_max_password_history = DEFAULT_COUNT_OF_PASSWORD_HISTORY;
      m_min_login_length = DEFAULT_LOGIN_LENGTH;
      m_change_in_n_days = DEFAULT_CHANGE_PASSWORD_IN_N_DAYS;
      m_max_days_without_login = DEFAULT_COUNT_OF_DAYS_WITHOUT_LOGIN;

      // MaxLoginAttempts is used always.
      if (int.TryParse(WSI.Common.GeneralParam.Value("User", "MaxLoginAttempts"), out _gp_value))
      {
        m_max_login_attempts = _gp_value;
      }
    }

    private String CleanExtraCharsFromTrackdata(String Password)
    {
      String _pwd;
      UInt64 _num;
      String _aux;

      if (Password == null)
      {
        return "";
      }

      if (Password.Length == 22 && Password.StartsWith("%") && Password.EndsWith("_"))
      {
        _pwd = Password.Substring(1, Password.Length - 2);

        if (UInt64.TryParse(_pwd, out _num))
        {
          _aux = _num.ToString().PadLeft(20, '0');

          if (_aux == _pwd)
          {
            return _pwd;
          }
        }
      }

      if (m_is_super_user || (!this.RulesEnabled && this.OldRulesEnabled)) // Password Policy not enabled & Old rules enabled. 
      {
        Password = Password.ToUpper();
      }

      return Password;

    }// CleanExtraCharsFromTrackdata


    // AJQ 13-FEB-2014
    private Byte[] HashPassword(Int32 UserId, String Password)
    {
      SHA256 _sha;
      Byte[] _buffer;
      Byte[] _hash;
      int _idx_w;

      Password = CleanExtraCharsFromTrackdata(Password);

      _sha = SHA256.Create();
      _buffer = Encoding.UTF8.GetBytes(UserId.ToString() + Password);
      _hash = _sha.ComputeHash(_buffer);

      _buffer = new Byte[40];
      _idx_w = 0;
      _buffer[_idx_w++] = 0;

      for (int _idx = 0; _idx < _hash.Length; _idx++)
      {
        _buffer[_idx_w++] = _hash[_idx];
      }

      return _buffer;
    } // HashPassword




    private static Boolean ComparePassword(Byte[] Pwd1, Byte[] Pwd2)
    {
      if (Pwd1.Length != Pwd2.Length)
      {
        return false;
      }

      if (Pwd1.Length != 40)
      {
        return false;
      }

      for (int _i = 0; _i < Pwd1.Length; _i++)
      {
        if (Pwd1[_i] != Pwd2[_i])
        {
          return false;
        }
      }

      return true;
    }
  }
}
