//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Stats.cs
// 
//   DESCRIPTION: Implements the stats features. 
//
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 27-SEP-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-SEP-2012 ACC    First release.
// 13-JUN-2013 HBB    Fixed Bug #849: If the name of the game is unknown the statistic manual adjustment doesn't show the data.
// 08-ABR-2015 FOS    Task 973: Add terminal name.
// 15-MAY-2017 FGB    BUG 27301: Ajuste Manual de Estadísticas no muestra datos de Sesiones cuando se ha cambiado de nombre el terminal
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  static public class Stats
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Get Stats terminal group by game
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId 
    //          - From 
    //          - To 
    //
    //      - OUTPUT :
    //          - StatsGames 
    //
    // RETURNS :
    //      - True: Select ok. False: Otherwise.
    //
    //   NOTES :
    //
    public static Boolean GetStatsGroupByGame(Int32 TerminalId, DateTime From, DateTime To, out DataTable StatsGames)
    {
      StringBuilder _sb;
      DateTime _today_opening;
      DateTime _2007_opening;

      StatsGames = new DataTable("STATS_GAMES");

      _today_opening = Misc.TodayOpening();
      _2007_opening = new DateTime(2007, 1, 1, _today_opening.Hour, _today_opening.Minute, 0);

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // 15-MAY-2017 FGB BUG 27301: We take the terminal name from the terminals table
          _sb = new StringBuilder();
          _sb.AppendLine("    SELECT  TE_NAME                               AS SPH_TERMINAL_NAME      ");
          //_sb.AppendLine("    SELECT  SPH_TERMINAL_NAME");
          _sb.AppendLine("          , DATEADD(DAY, DATEDIFF(HOUR, @p_2007_opening, SPH_BASE_HOUR) / 24, @p_2007_opening) AS BASE_DAY ");
          _sb.AppendLine("          , SPH_BASE_HOUR                         AS BASE_HOUR              ");
          _sb.AppendLine("          , SPH_GAME_ID                           AS GAME_ID                ");
          _sb.AppendLine("          , ISNULL(PG_GAME_NAME , 'UNKNOWN')      AS GAME_NAME              ");
          _sb.AppendLine("          , SPH_PLAYED_COUNT                      AS NUM_PLAYED             ");
          _sb.AppendLine("          , SPH_PLAYED_AMOUNT                     AS PLAYED                 ");
          _sb.AppendLine("          , SPH_WON_COUNT                         AS NUM_WON                ");
          _sb.AppendLine("          , SPH_WON_AMOUNT                        AS WON                    ");
          _sb.AppendLine("       FROM SALES_PER_HOUR WITH (INDEX (PK_SALES_PER_HOUR))                 ");
          _sb.AppendLine(" INNER JOIN TERMINAL_GAME_TRANSLATION ON (SPH_TERMINAL_ID = TGT_TERMINAL_ID AND SPH_GAME_ID = TGT_SOURCE_GAME_ID)");
          _sb.AppendLine("  LEFT JOIN PROVIDERS_GAMES   ON TGT_TRANSLATED_GAME_ID = PG_GAME_ID        ");
          _sb.AppendLine("  LEFT JOIN TERMINALS         ON TE_TERMINAL_ID = SPH_TERMINAL_ID           "); //15-MAY-2017 FGB
          _sb.AppendLine("      WHERE SPH_BASE_HOUR    >= @p_date_from                                ");
          _sb.AppendLine("        AND SPH_BASE_HOUR    < @p_date_to                                   ");
          _sb.AppendLine("        AND SPH_TERMINAL_ID  IN (                                           ");
          _sb.AppendLine("                                   SELECT   TE_TERMINAL_ID                  ");
          _sb.AppendLine("                                     FROM   TERMINALS                       ");
          _sb.AppendLine("                                    WHERE   TE_MASTER_ID = @p_terminal_id ) ");
          _sb.AppendLine("   ORDER BY BASE_HOUR, GAME_ID ASC                                          ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@p_2007_opening", SqlDbType.DateTime).Value = _2007_opening;
            _sql_cmd.Parameters.Add("@p_date_from", SqlDbType.DateTime).Value = From;
            _sql_cmd.Parameters.Add("@p_date_to", SqlDbType.DateTime).Value = To;
            _sql_cmd.Parameters.Add("@p_terminal_id", SqlDbType.Int).Value = TerminalId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(StatsGames);
            } // using
          } // using
        } // using

        StatsGames.Columns.Add("NETWIN").Expression = "([PLAYED] - [WON])";
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // GetStatsGroupByGame

    //------------------------------------------------------------------------------
    // PURPOSE : Group by hours Stats 
    //
    //  PARAMS :
    //      - INPUT :
    //          - StatsGames 
    //
    //      - OUTPUT :
    //          - StatsHours 
    //
    // RETURNS :
    //      - True: Select ok. False: Otherwise.
    //
    //   NOTES :
    //
    public static Boolean GetStatsGroupByHours(DataTable StatsGames, out DataTable StatsHours)
    {
      DataSetHelper _dsh;

      StatsHours = new DataTable("STATS_HOURS");

      try
      {
        _dsh = new DataSetHelper();

        StatsHours = _dsh.SelectGroupByInto("STATS_HOURS", StatsGames,
                                                  " SPH_TERMINAL_NAME TERMINAL_NAME" +
                                                  ",MIN(BASE_DAY) BASE_DAY" +
                                                  ",BASE_HOUR" +
                                                  ",MIN(GAME_ID) GAME_ID" +
                                                  ",MIN(GAME_NAME) GAME_NAME" +
                                                  ",SUM(NUM_PLAYED) NUM_PLAYED" +
                                                  ",SUM(PLAYED) PLAYED" +
                                                  ",SUM(NUM_WON) NUM_WON" +
                                                  ",SUM(WON) WON"
                                             , ""
                                             , "BASE_HOUR");

        StatsHours.Columns.Add("NETWIN").Expression = "([PLAYED] - [WON])";
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // GetStatsGroupByHours

    //------------------------------------------------------------------------------
    // PURPOSE : Group by days Stats 
    //
    //  PARAMS :
    //      - INPUT :
    //          - StatsHours 
    //
    //      - OUTPUT :
    //          - StatsDays 
    //
    // RETURNS :
    //      - True: Select ok. False: Otherwise.
    //
    //   NOTES :
    //
    public static Boolean GetStatsGroupByDays(DataTable StatsHours, out DataTable StatsDays)
    {
      DataSetHelper _dsh;

      StatsDays = new DataTable("STATS_DAYS");

      try
      {
        _dsh = new DataSetHelper();

        StatsDays = _dsh.SelectGroupByInto("STATS_DAYS", StatsHours,
                                                  " SPH_TERMINAL_NAME TERMINAL_NAME" +
                                                  ",BASE_DAY" +
                                                  ",MIN(BASE_HOUR) BASE_HOUR" +
                                                  ",MIN(GAME_ID) GAME_ID" +
                                                  ",MIN(GAME_NAME) GAME_NAME" +
                                                  ",SUM(NUM_PLAYED) NUM_PLAYED" +
                                                  ",SUM(PLAYED) PLAYED" +
                                                  ",SUM(NUM_WON) NUM_WON" +
                                                  ",SUM(WON) WON"
                                             , ""
                                             , "BASE_DAY");

        StatsDays.Columns.Add("NETWIN").Expression = "([PLAYED] - [WON])";
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // GetStatsGroupByDays

    //------------------------------------------------------------------------------
    // PURPOSE : Update / Insert Terminal Stats
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId 
    //          - DtTerminalStats 
    //          - Trx 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Select ok. False: Otherwise.
    //
    //   NOTES :
    //
    public static Boolean UpdateTerminalStats(Int32 TerminalId, DataTable DtTerminalStats, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_modified;
      Int32 _nr;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   SALES_PER_HOUR ");
        _sb.AppendLine("    SET   SPH_PLAYED_COUNT  = @p_played_count ");
        _sb.AppendLine("        , SPH_PLAYED_AMOUNT = @p_played_amount ");
        _sb.AppendLine("        , SPH_WON_COUNT     = @p_won_count ");
        _sb.AppendLine("        , SPH_WON_AMOUNT    = @p_won_amount ");
        _sb.AppendLine("  WHERE   SPH_BASE_HOUR     = @p_base_hour ");
        _sb.AppendLine("    AND   SPH_TERMINAL_ID   IN   (SELECT   TE_TERMINAL_ID ");
        _sb.AppendLine("                                    FROM   TERMINALS ");
        _sb.AppendLine("                                   WHERE   TE_MASTER_ID = @p_terminal_id)");
        _sb.AppendLine("    AND   SPH_GAME_ID       = @p_game_id ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@p_base_hour", SqlDbType.DateTime).SourceColumn = "BASE_HOUR";
          _sql_cmd.Parameters.Add("@p_terminal_id", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@p_game_id", SqlDbType.Int).SourceColumn = "GAME_ID";

          _sql_cmd.Parameters.Add("@p_played_count", SqlDbType.BigInt).SourceColumn = "NUM_PLAYED";
          _sql_cmd.Parameters.Add("@p_played_amount", SqlDbType.Money).SourceColumn = "PLAYED";
          _sql_cmd.Parameters.Add("@p_won_count", SqlDbType.BigInt).SourceColumn = "NUM_WON";
          _sql_cmd.Parameters.Add("@p_won_amount", SqlDbType.Money).SourceColumn = "WON";

          _sql_cmd.UpdatedRowSource = UpdateRowSource.None;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _num_rows_modified = DtTerminalStats.Select("", "", DataViewRowState.ModifiedCurrent).Length;

            _sql_da.UpdateCommand = _sql_cmd;
            _sql_da.DeleteCommand = null;
            _sql_da.SelectCommand = null;
            _sql_da.InsertCommand = null;
            _sql_da.UpdateBatchSize = 500;
            _sql_da.ContinueUpdateOnError = true;

            _nr = _sql_da.Update(DtTerminalStats);

            if (DtTerminalStats.HasErrors)
            {
              return false;
            }

            if (_nr != _num_rows_modified)
            {
              return false;
            }
          } // using
        } // using
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // UpdateTerminalStats

    //------------------------------------------------------------------------------
    // PURPOSE : Get Group by hours Terminal PlaySessions
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId 
    //          - From 
    //          - To 
    //
    //      - OUTPUT :
    //          - PlaySessionsHours 
    //
    // RETURNS :
    //      - True: Select ok. False: Otherwise.
    //
    //   NOTES :
    //
    public static Boolean GetPlaySessionsGroupByHours(Int32 TerminalId, DateTime From, DateTime To, out DataTable PlaySessionsHours)
    {
      StringBuilder _sb;
      DateTime _today_opening;
      DateTime _2007_opening;

      PlaySessionsHours = new DataTable("PLAY_SESSIONS_HOURS");

      _today_opening = Misc.TodayOpening();
      _2007_opening = new DateTime(2007, 1, 1, _today_opening.Hour, _today_opening.Minute, 0);

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("    SELECT  TE_NAME AS TERMINAL_NAME ");
          _sb.AppendLine("          , DATEADD(DAY, DATEDIFF(HOUR, @p_2007_opening, PS_FINISHED) / 24, @p_2007_opening) AS BASE_DAY ");
          _sb.AppendLine("          , DATEADD(HOUR, DATEDIFF(HOUR, @p_2007_opening, PS_FINISHED), @p_2007_opening) AS BASE_HOUR ");
          _sb.AppendLine("          , SUM(PS_PLAYED_COUNT)    AS PS_NUM_PLAYED ");
          _sb.AppendLine("          , SUM(PS_PLAYED_AMOUNT)   AS PS_PLAYED ");
          _sb.AppendLine("          , SUM(PS_WON_COUNT)       AS PS_NUM_WON ");
          _sb.AppendLine("          , SUM(PS_WON_AMOUNT)      AS PS_WON ");
          _sb.AppendLine("          , SUM(PS_TOTAL_CASH_IN)   AS PS_CASH_IN ");
          _sb.AppendLine("          , SUM(PS_TOTAL_CASH_OUT)  AS PS_CASH_OUT ");
          _sb.AppendLine("      FROM  PLAY_SESSIONS ");
          _sb.AppendLine(" LEFT JOIN  TERMINALS   ON TE_TERMINAL_ID = PS_TERMINAL_ID ");
          _sb.AppendLine("     WHERE  PS_TERMINAL_ID IN ( SELECT   TE_TERMINAL_ID       ");
          _sb.AppendLine("                                  FROM   TERMINALS            ");
          _sb.AppendLine("                                 WHERE   TE_MASTER_ID = @p_terminal_id )");
          _sb.AppendLine("       AND  PS_FINISHED    >= @p_date_from ");
          _sb.AppendLine("       AND  PS_FINISHED    < @p_date_to ");
          _sb.AppendLine("       AND  PS_STATUS      <> 0  ");
          _sb.AppendLine("       AND  PS_PROMO       = 0  ");
          _sb.AppendLine("  GROUP BY  TE_NAME ");
          //_sb.AppendLine("          , DATEDIFF(DAY,  @p_2007_opening, PS_FINISHED) ");    //Not used
          _sb.AppendLine("          , DATEDIFF(HOUR, @p_2007_opening, PS_FINISHED) ");
          _sb.AppendLine("  ORDER BY  BASE_HOUR ASC ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@p_2007_opening", SqlDbType.DateTime).Value = _2007_opening;
            _sql_cmd.Parameters.Add("@p_date_from", SqlDbType.DateTime).Value = From;
            _sql_cmd.Parameters.Add("@p_date_to", SqlDbType.DateTime).Value = To;
            _sql_cmd.Parameters.Add("@p_terminal_id", SqlDbType.Int).Value = TerminalId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(PlaySessionsHours);
            } // using
          } // using
        } // using

        PlaySessionsHours.Columns.Add("PS_NETWIN_PL_WO").Expression = "([PS_PLAYED] - [PS_WON])";
        PlaySessionsHours.Columns.Add("PS_NETWIN_CI_CO").Expression = "([PS_CASH_IN] - [PS_CASH_OUT])";
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // GetPlaySessionsGroupByHours

    //------------------------------------------------------------------------------
    // PURPOSE : Group by days PlaySessions 
    //
    //  PARAMS :
    //      - INPUT :
    //          - PlaySessionsHours 
    //
    //      - OUTPUT :
    //          - PlaySessionsDays 
    //
    // RETURNS :
    //      - True: Select ok. False: Otherwise.
    //
    //   NOTES :
    //
    public static Boolean GetPlaySessionsGroupByDays(DataTable PlaySessionsHours, out DataTable PlaySessionsDays)
    {
      DataSetHelper _dsh;

      PlaySessionsDays = new DataTable("PLAY_SESSIONS_DAYS");

      try
      {
        _dsh = new DataSetHelper();

        PlaySessionsDays = _dsh.SelectGroupByInto("PLAY_SESSIONS_DAYS", PlaySessionsHours,
                                                  " TERMINAL_NAME" +
                                                  ",BASE_DAY" +
                                                  ",MIN(BASE_HOUR) BASE_HOUR" +
                                                  ",SUM(PS_NUM_PLAYED) PS_NUM_PLAYED" +
                                                  ",SUM(PS_PLAYED) PS_PLAYED" +
                                                  ",SUM(PS_NUM_WON) PS_NUM_WON" +
                                                  ",SUM(PS_WON) PS_WON" +
                                                  ",SUM(PS_CASH_IN) PS_CASH_IN" +
                                                  ",SUM(PS_CASH_OUT) PS_CASH_OUT"
                                             , ""
                                             , "BASE_DAY");

        PlaySessionsDays.Columns.Add("PS_NETWIN_PL_WO").Expression = "([PS_PLAYED] - [PS_WON])";
        PlaySessionsDays.Columns.Add("PS_NETWIN_CI_CO").Expression = "([PS_CASH_IN] - [PS_CASH_OUT])";
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // GetPlaySessionsGroupByDays
  }
}
