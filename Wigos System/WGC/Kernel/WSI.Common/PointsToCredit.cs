//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PointsToCredit.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: JML
// 
// CREATION DATE: 08-MAY-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-MAY-2013 JML    First release.
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;

using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public class PointsToCredit
  {

    #region enums

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Enable/disable Gifts & promotions.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: Generation was correctly. False: Otherwise.   
    // 
    //   NOTES:
    // 

    public static Boolean EnableDisableGifts()
    {

      StringBuilder _sb;
      Boolean _enable;
      DayOfWeek _day_of_week;
      Int32 _closing_time;
      Boolean _is_day_of_week_in_schedule;
      DataTable _change_tables;
      DateTime _date_time_now;

      _sb = new StringBuilder();

      _closing_time = 0;

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (IsFirstTimeOfDay(_db_trx.SqlTransaction))
          {
            // Disable all the gifts that pertain to points_to_credit promotions 
            if (DisableAllGifts(_db_trx.SqlTransaction))
            {
              _db_trx.Commit();
            }
          }
        }   // Using DB_TRX
      }
      catch (SqlException _sql_ex)
      {
        Log.Message("Function EnableDisableGifts failed in function DisableAllGifts");
        Log.Exception(_sql_ex);

        return false;
      }
      catch (Exception _ex)
      {
        Log.Message("Function EnableDisableGifts failed in function DisableAllGifts");
        Log.Exception(_ex);

        return false;
      }

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {

          // Read all from table POINTS_TO_CREDIT
          if (!ReadPointsToCredit(out _change_tables, _db_trx.SqlTransaction))
          {
            Log.Message("Function EnableDisableGifts failed in function GetChangeTables");
            _db_trx.Rollback();

            return false;
          }

          if (_change_tables.Rows.Count == 0)
          {
            return true;
          }

          Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("WigosGUI", "ClosingTime"), out _closing_time);

          // Update Gifts and Promotions with appropriate values.
          foreach (DataRow _change_table in _change_tables.Rows)
          {
            _enable = false;

            _date_time_now = WGDB.Now;
            _day_of_week = Promotion.GetWeekDay(_date_time_now, _closing_time);
            _is_day_of_week_in_schedule = ScheduleDay.WeekDayIsInSchedule(_day_of_week, (Int32)_change_table["PTC_SCHEDULE_WEEKDAY"]);

            // Check for enable gifts
            // If change table is enable
            //    If day of week is enable
            //       If Schedule is enable
            if ((Boolean)_change_table["PTC_ENABLED"])
            {
              if (_is_day_of_week_in_schedule)
              {
                if (ScheduleDay.IsTimeInRange(_date_time_now.TimeOfDay, (Int32)_change_table["PTC_SCHEDULE1_TIME_FROM"], (Int32)_change_table["PTC_SCHEDULE1_TIME_TO"])
                    || ((Boolean)_change_table["PTC_SCHEDULE2_ENABLED"]
                        && ScheduleDay.IsTimeInRange(_date_time_now.TimeOfDay, (Int32)_change_table["PTC_SCHEDULE2_TIME_FROM"], (Int32)_change_table["PTC_SCHEDULE2_TIME_TO"])))
                {
                  _enable = true;
                }
              }
            }

            // Update the data if all gifts are not in the status where they belong. 
            //              Or if it has changed the data in the chanje_table since the last run of the thread.
            if ((_enable && (Int32)_change_table["NO_AVAILABLES"] > 0)
                 || (!_enable && (Int32)_change_table["AVAILABLES"] > 0)
                 || ((DateTime)_change_table["PTC_UPDATED"] > (DateTime)_change_table["PTC_UPDATED_BY_THREAD"]))
            {
              if (!UpdateTablesWithChangeTable(_change_table, _enable, _is_day_of_week_in_schedule))
              {
                Log.Message("Function EnableDisableGifts failed in function UpdateTablesWithChangeTable");
                _db_trx.Rollback();

                return false;
              }
            }
          }
          _db_trx.Commit();

        }   // Using DB_TRX

        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Error("Function EnableDisableGifts failed");
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Error("Function EnableDisableGifts failed");
        Log.Exception(_ex);
      }

      return false;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check if is the first time that procedure is execute today
    // 
    //  PARAMS:
    //      - INPUT:
    //            - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: Is the first time. False: Otherwise.   
    // 
    //   NOTES:
    // 

    private static Boolean IsFirstTimeOfDay(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _count;
      DateTime _date_now;

      _date_now = WGDB.Now.Date;
      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   COUNT(1) AS Changed_Today       ");
      _sb.AppendLine("  FROM   POINTS_TO_CREDITS               ");
      _sb.AppendLine(" WHERE   PTC_UPDATED_BY_THREAD >= @pToday ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pToday", SqlDbType.DateTime).Value = _date_now;
        _count = (Int32)_cmd.ExecuteScalar();

        if (_count > 0)
        {
          return false;
        }
        else
        {
          return true;
        }
      }
    }  // IsFirstTimeOfDay

    //------------------------------------------------------------------------------
    // PURPOSE: Disable all gifts from POINTS_TO_CREDITS
    // 
    //  PARAMS:
    //      - INPUT:
    //            - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: It was correctly. False: Otherwise. 
    // 
    //   NOTES:
    // 

    private static Boolean DisableAllGifts(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE   GIFTS                                ");
      _sb.AppendLine("    SET   GI_AVAILABLE = @pNewStatus           ");
      _sb.AppendLine("  WHERE   GI_POINTS_TO_CREDITS_ID IS NOT NULL  ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pNewStatus", SqlDbType.Bit).Value = false;

        return (_cmd.ExecuteNonQuery() > 0);

      }  // Using SqlCommand

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get change tables
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - Datatable with all change tables
    //
    // RETURNS :
    //      - True: Is the first time. False: Otherwise.
    //
    public static Boolean ReadPointsToCredit(out DataTable ChangeTables, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _day_of_week;
      ChangeTables = new DataTable();

      try
      {
        _day_of_week = GetDayOfWeekNumber(WGDB.Now);
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   PTC_POINTS_TO_CREDITS_ID ");
        _sb.AppendLine("       , PTC_NAME ");
        _sb.AppendLine("       , PTC_ENABLED ");
        _sb.AppendLine("       , PTC_CREDIT_TYPE ");
        _sb.AppendLine("       , PTC_UPDATED ");
        _sb.AppendLine("       , ISNULL(PTC_UPDATED_BY_THREAD, GETDATE()-1) AS PTC_UPDATED_BY_THREAD ");
        _sb.AppendLine("       , PTC_SCHEDULE_WEEKDAY ");
        _sb.AppendLine("       , PTC_SCHEDULE1_TIME_FROM ");
        _sb.AppendLine("       , PTC_SCHEDULE1_TIME_TO ");
        _sb.AppendLine("       , PTC_SCHEDULE2_ENABLED ");
        _sb.AppendLine("       , ISNULL(PTC_SCHEDULE2_TIME_FROM, 0) AS PTC_SCHEDULE2_TIME_FROM ");
        _sb.AppendLine("       , ISNULL(PTC_SCHEDULE2_TIME_TO, 0)   AS PTC_SCHEDULE2_TIME_TO ");
        _sb.AppendFormat("     , PTC_RESTRICTED_TO_TERMINAL_LIST_{0} AS PTC_RESTRICTED_TO_TERMINAL_LIST ", _day_of_week); // Calculate field
        _sb.AppendLine();
        _sb.AppendLine("       , ISNULL(AVAILABLES, 0) AS AVAILABLES ");
        _sb.AppendLine("       , ISNULL(NO_AVAILABLES, 0) AS NO_AVAILABLES ");
        _sb.AppendLine("  FROM   POINTS_TO_CREDITS ");
        _sb.AppendLine("  LEFT   JOIN ( SELECT   GI_POINTS_TO_CREDITS_ID  ");
        _sb.AppendLine("                       , SUM(CASE GI_AVAILABLE WHEN 1 THEN 1 ELSE 0 END) AS AVAILABLES  ");
        _sb.AppendLine("                       , SUM(CASE GI_AVAILABLE WHEN 0 THEN 1 ELSE 0 END) AS NO_AVAILABLES  ");
        _sb.AppendLine("                  FROM   GIFTS  WHERE GI_POINTS_TO_CREDITS_ID IS NOT NULL ");
        _sb.AppendLine("                 GROUP   BY GI_POINTS_TO_CREDITS_ID ) AS GIFS_A  ");
        _sb.AppendLine("    ON   GI_POINTS_TO_CREDITS_ID = PTC_POINTS_TO_CREDITS_ID ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(ChangeTables);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetChangeTables


    //------------------------------------------------------------------------------
    // PURPOSE : Update tables GIFTS, PROMOTIONS, POINTS_TO_CREDITS
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - Datatable with all change tables
    //
    // RETURNS :
    //      - True: It was correctly. False: Otherwise. 
    //
    public static Boolean UpdateTablesWithChangeTable(DataRow ChangeTable, Boolean Available, Boolean IsDayOfWeekInSchedule)
    {
      StringBuilder _sb;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          SqlTransaction _sql_trx;
          _sql_trx = _db_trx.SqlTransaction;

          _sb = new StringBuilder();
          _sb.AppendLine(" UPDATE   GIFTS  ");
          _sb.AppendLine("    SET   GI_AVAILABLE = @pAvalaible ");
          _sb.AppendLine("  WHERE   GI_POINTS_TO_CREDITS_ID = @pPtcId");
          _sb.AppendLine("    AND   GI_AVAILABLE <> @pAvalaible ");
          _sb.AppendLine();
          _sb.AppendLine(" UPDATE   PROMOTIONS  ");
          _sb.AppendLine("    SET   PM_RESTRICTED_TO_TERMINAL_LIST  = CASE WHEN (@pIsDayOfWeekInSchedule=1) THEN @pTerminalList ELSE PM_RESTRICTED_TO_TERMINAL_LIST END ");
          _sb.AppendLine("  WHERE   PM_POINTS_TO_CREDITS_ID    = @pPtcId");
          _sb.AppendLine();
          _sb.AppendLine(" UPDATE   POINTS_TO_CREDITS ");
          _sb.AppendLine("    SET   PTC_UPDATED_BY_THREAD = GETDATE() ");
          _sb.AppendLine("  WHERE   PTC_POINTS_TO_CREDITS_ID = @pPtcId ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx))
          {
            _sql_cmd.Parameters.Add("@pAvalaible", SqlDbType.Bit).Value = Available;
            _sql_cmd.Parameters.Add("@pIsDayOfWeekInSchedule", SqlDbType.Bit).Value = IsDayOfWeekInSchedule;
            _sql_cmd.Parameters.Add("@pTerminalList", SqlDbType.Xml).Value = ChangeTable["PTC_RESTRICTED_TO_TERMINAL_LIST"];
            _sql_cmd.Parameters.Add("@pPtcId", SqlDbType.BigInt).Value = ChangeTable["PTC_POINTS_TO_CREDITS_ID"];

            if (_sql_cmd.ExecuteNonQuery() < 1)
            {
              return false;
            }
          }
          _db_trx.Commit();
        }   // Using DB_TRX
      }
      catch (SqlException _sql_ex)
      {
        Log.Message("Function UpdateTablesWithChangeTable failed");
        Log.Exception(_sql_ex);

        return false;
      }
      catch (Exception _ex)
      {
        Log.Message("Function UpdateTablesWithChangeTable failed");
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // UpdateTablesWithChangeTable

    //------------------------------------------------------------------------------
    // PURPOSE : Return the day of the week in sql number
    //
    //  PARAMS :
    //      - INPUT:
    //            - DateTime
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Number of day of the week in sql
    //
    public static Int32 GetDayOfWeekNumber(DateTime CheckDateTime)
    {
      switch (CheckDateTime.DayOfWeek)
      {
        case DayOfWeek.Monday:
          return 1;
        case DayOfWeek.Tuesday:
          return 2;
        case DayOfWeek.Wednesday:
          return 3;
        case DayOfWeek.Thursday:
          return 4;
        case DayOfWeek.Friday:
          return 5;
        case DayOfWeek.Saturday:
          return 6;
        case DayOfWeek.Sunday:
          return 0;
      }
      return 0;
    }  // GetDayOfWeekNumber

  } // class PointsToCredit
}
