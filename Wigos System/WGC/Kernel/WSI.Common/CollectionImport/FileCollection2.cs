//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FileCollection.cs
// 
//   DESCRIPTION: Partial xlass to manage the collection of imported files
// 
//        AUTHOR: David Rigal
// 
// CREATION DATE: 28-MAY-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-MAY-2015 DRV   Initial version. Item 1314, task 1353
// 08-JUL-2015 FJC   Fixed Bug WIG-2556.
// 22-ABR-2016 JML   Product Backlog Item 9758:Gaming Tables (Fase 1): General cage report
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using WSI.Common;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common.CollectionImport
{
  public partial class SofCountData
  {
    private SqlDataAdapter m_da_cage_movements;
    private SqlDataAdapter m_da_money_collections;
    private Int32 m_user_id;
    private String m_user_name;
    private Int64 m_cage_session;

    //------------------------------------------------------------------------------
    // PURPOSE : Collects the specified Money Collections
    //
    //  PARAMS :
    //      - INPUT : List of MoneyCollections
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    //
    public Boolean CollectSoftCounts(List<Int64> MoneyCollections, Int32 UserID, String UserName, Int64 CageSessionID, DB_TRX _trx)
    {
      DataTable _collection_details;
      DataTable _cage_movements;
      DataTable _cage_stock;

      m_user_id = UserID;
      m_user_name = UserName;
      m_cage_session = CageSessionID;

      if (!UpdateSoftCountClass(MoneyCollections))
      {
        Log.Message("An error occurred in CollectSoftCounts() -UpdateSoftCountClass()-");

        return false;
      }
      if (!m_imported_soft_count_obj.SetCollectedStatus(_trx.SqlTransaction, UserID, CageSessionID, MoneyCollections))
      {
        Log.Message("An error occurred in CollectSoftCounts() -SetCollectedStatus()-");

        return false;
      }
      if (!UpdateCageMovements(MoneyCollections, out _cage_movements, _trx))
      {
        Log.Message("An error occurred in CollectSoftCounts() -UpdateCageMovements()-");

        return false;
      }
      if (!UpdateSoftCountsMoneyCollections(MoneyCollections, _trx))
      {
        Log.Message("An error occurred in CollectSoftCounts() -UpdateSoftCountsMoneyCollections()-");

        return false;
      }
      if (!GetAllCollectionDetails(MoneyCollections, out _collection_details, out _cage_stock))
      {
        Log.Message("An error occurred in CollectSoftCounts() -GetAllCollectionDetails()-");

        return false;
      }
      if (!InsertMoneyCollectionDetails(_collection_details, _trx.SqlTransaction))
      {
        Log.Message("An error occurred in CollectSoftCounts() -InsertMoneyCollectionDetails()-");

        return false;
      }
      if (!UpdateCollectedTickets(_collection_details, _cage_movements, _trx.SqlTransaction))
      {
        Log.Message("An error occurred in CollectSoftCounts() -UpdateCollectedTickets()-");

        return false;
      }
      if (!UpdateCageStock(_cage_stock, _trx.SqlTransaction))
      {
        Log.Message("An error occurred in CollectSoftCounts() -UpdateCageStock()-");

        return false;
      }
      if (!UpdateMobileBankCashierMovementsAndCageMeters(MoneyCollections, _trx.SqlTransaction))
      {
        Log.Message("An error occurred in CollectSoftCounts() -UpdateMobileBankCashierMovementsAndCageMeters()-");

        return false;
      }

      return true;
    }

    private Boolean UpdateSoftCountClass(List<Int64> MoneyCollections)
    {
      DataRow[] _rows;
      String _id;
      SoftCount _soft_count;

      try
      {
        foreach (Int64 _mc_id in MoneyCollections)
        {
          _rows = MergedCollectionSoftCount.Select(SQL_COLUMN_COLLECTION_ID + " = " + _mc_id);

          if (_rows.Length != 1)
          {
            return false;
          }

          _id = GetStackerByMC((Int64)_rows[0][SQL_COLUMN_COLLECTION_ID]);
          m_imported_soft_count_obj.GetSoftCount(_id, out _soft_count);
          _soft_count.m_target_collection_id = (Int64)_rows[0][SQL_COLUMN_COLLECTION_ID];
          _soft_count.m_target_stacker_id = Convert.ToInt64(_rows[0].IsNull(SQL_COLUMN_STACKER_ID) ? 0 : _rows[0][SQL_COLUMN_STACKER_ID]);
          _soft_count.m_target_terminal_id = Convert.ToInt64(_rows[0][SQL_COLUMN_TERMINAL_ID]);
          _soft_count.m_target_floor_id = _rows[0][SQL_COLUMN_FLOOR_ID].ToString();

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      return true;
    }

    #region CAGE MOVEMENTS BATCH UPDATE

    //------------------------------------------------------------------------------
    // PURPOSE : Gets money collection and cage movement data of the specified Money collections and insert or update cage movements
    //
    //  PARAMS :
    //      - INPUT : List of MoneyCollections, Transaction
    //      - OUTPUT : 
    // RETURNS : Datatable: with Money Collection and cage movement Data
    //
    //   NOTES : 
    //
    private Boolean UpdateCageMovements(List<Int64> MoneyCollections, out DataTable CageMovements, DB_TRX _trx)
    {
      if (GetSoftCountCageMovements(MoneyCollections, out CageMovements, _trx.SqlTransaction))
      {
        if (ProcessCageMovements(CageMovements, MoneyCollections))
        {
          if (UpdateCageMovements(CageMovements, _trx.SqlTransaction))
          {
            return true;
          }
        }
      }

      return false;
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Gets money collection and cage movement data of the specified Money collections
    //
    //  PARAMS :
    //      - INPUT : List of MoneyCollections, Transaction
    //      - OUTPUT : 
    // RETURNS : Datatable: with Money Collection and cage movement Data
    //
    //   NOTES : 
    //
    private Boolean GetSoftCountCageMovements(List<Int64> MoneyCollections, out DataTable CageMovements, SqlTransaction _sql_trx)
    {
      StringBuilder _sb_select;
      Int32 _counter;

      CageMovements = new DataTable();

      _sb_select = new StringBuilder();
      _sb_select.AppendLine("    SELECT   MC_COLLECTION_ID                        ");
      _sb_select.AppendLine("           , CGM_MOVEMENT_ID                         ");
      _sb_select.AppendLine("           , MC_CASHIER_SESSION_ID                   ");
      _sb_select.AppendLine("           , MC_TERMINAL_ID                          ");
      _sb_select.AppendLine("           , ISNULL( CGM_STATUS, -1) AS CGM_STATUS   ");
      _sb_select.AppendLine("           , -1 AS NEW_STATUS                        ");
      _sb_select.AppendLine("      FROM   MONEY_COLLECTIONS                       ");
      _sb_select.AppendLine(" LEFT JOIN   CAGE_MOVEMENTS                          ");
      _sb_select.AppendLine("        ON   CGM_MC_COLLECTION_ID = MC_COLLECTION_ID ");
      _sb_select.AppendLine("     WHERE   MC_COLLECTION_ID IN (                   ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb_select.ToString(), _sql_trx.Connection, _sql_trx))
        {
          _counter = 0;

          foreach (Int64 _money_collection_id in MoneyCollections)
          {
            if (_counter > 0)
            {
              _sb_select.Append(", ");
            }
            _sb_select.Append(String.Format("@pMoneyCollection{0}", _counter));
            _sql_cmd.Parameters.Add(String.Format("@pMoneyCollection{0}", _counter), SqlDbType.BigInt).Value = _money_collection_id;

            _counter++;
          }
          _sb_select.AppendLine(" ) ");

          _sql_cmd.CommandText = _sb_select.ToString();

          m_da_cage_movements = new SqlDataAdapter();
          m_da_cage_movements.SelectCommand = _sql_cmd;

          m_da_cage_movements.Fill(CageMovements);

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Prepare cage movements data to be updated to DB
    //
    //  PARAMS :
    //      - INPUT : Datatable of Cage Movements
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    //
    private Boolean ProcessCageMovements(DataTable CageMovements, List<Int64> MoneyCollections)
    {
      String _select = " CGM_STATUS IN ( " + (Int32)Cage.CageStatus.Canceled + " , " + (Int32)Cage.CageStatus.Sent + ", -1) ";
      Dictionary<Int64, Cage.CageStatus> _status;
      Cage.CageStatus _aux_status;

      try
      {
        //DRV: All rows must be collectable
        if (CageMovements.Select(_select).Length != CageMovements.Rows.Count)
        {
          return false;
        }

        if (!GetCollectedSoftCountWithStatus(MoneyCollections, out _status))
        {
          return false;
        }

        foreach (DataRow _row in CageMovements.Rows)
        {
          if (_status.TryGetValue((Int64)_row["MC_COLLECTION_ID"], out _aux_status))
          {
            _row["NEW_STATUS"] = _aux_status;
          }
          else
          {
            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Insert or updates cage movements
    //
    //  PARAMS :
    //      - INPUT : Datatable: Cage Movements
    //                Int32    : User Id
    //                Int64    : Cage Session
    //                SqlTransaction
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    //
    private Boolean UpdateCageMovements(DataTable Movements, SqlTransaction SqlTrx)
    {
      StringBuilder _sb_update;

      _sb_update = new StringBuilder();

      _sb_update.AppendLine(" IF (EXISTS( SELECT 1 FROM CAGE_MOVEMENTS WHERE CGM_MOVEMENT_ID = @pMovementId)) ");
      _sb_update.AppendLine(" BEGIN                                                          ");
      _sb_update.AppendLine("UPDATE   CAGE_MOVEMENTS                                         ");
      _sb_update.AppendLine("   SET   CGM_STATUS = @pStatus                                  ");
      _sb_update.AppendLine("       , CGM_USER_CAGE_ID = @pUserCage                          ");
      _sb_update.AppendLine("       , CGM_CAGE_SESSION_ID = @pCageSessionId                  ");
      _sb_update.AppendLine("       , CGM_MC_COLLECTION_ID = @pMcCollectionId                ");
      _sb_update.AppendLine("       , CGM_CANCELLATION_USER_ID = @pUserCage                  ");
      _sb_update.AppendLine("       , CGM_CANCELLATION_DATETIME = @pMovementDateTime         ");
      _sb_update.AppendLine(" WHERE   CGM_MOVEMENT_ID = @pMovementId                         ");
      _sb_update.AppendLine("   AND   CGM_STATUS = @pOldStatus                               ");
      _sb_update.AppendLine("DELETE   FROM CAGE_PENDING_MOVEMENTS                            ");
      _sb_update.AppendLine(" WHERE   CPM_MOVEMENT_ID = @pMovementId                         ");
      _sb_update.AppendLine(" END                                                            ");
      _sb_update.AppendLine(" ELSE ");
      _sb_update.AppendLine(" BEGIN ");
      _sb_update.AppendLine(" INSERT INTO   CAGE_MOVEMENTS           ");
      _sb_update.AppendLine("             ( CGM_USER_CAGE_ID         ");
      _sb_update.AppendLine("             , CGM_USER_CASHIER_ID      ");
      _sb_update.AppendLine("             , CGM_TERMINAL_CASHIER_ID  ");
      _sb_update.AppendLine("             , CGM_TYPE                 ");
      _sb_update.AppendLine("             , CGM_MOVEMENT_DATETIME    ");
      _sb_update.AppendLine("             , CGM_STATUS               ");
      _sb_update.AppendLine("             , CGM_CAGE_SESSION_ID      ");
      _sb_update.AppendLine("             , CGM_CASHIER_SESSION_ID   ");
      _sb_update.AppendLine("             , CGM_MC_COLLECTION_ID     ");
      _sb_update.AppendLine("             )                          ");
      _sb_update.AppendLine("      VALUES (                          ");
      _sb_update.AppendLine("               @pUserCage               ");
      _sb_update.AppendLine("             , @pUserCashier            ");
      _sb_update.AppendLine("             , @pTerminalCashier        ");
      _sb_update.AppendLine("             , @pOperationType          ");
      _sb_update.AppendLine("             , @pMovementDateTime       ");
      _sb_update.AppendLine("             , @pStatus                 ");
      _sb_update.AppendLine("             , @pCageSessionId          ");
      _sb_update.AppendLine("             , @pCashierSessionId       ");
      _sb_update.AppendLine("             , @pMcCollectionId         ");
      _sb_update.AppendLine("             )                          ");
      _sb_update.AppendLine(" SET @pMovementId = SCOPE_IDENTITY()    ");
      _sb_update.AppendLine(" END ");
      try
      {
        m_da_cage_movements.UpdateCommand = new SqlCommand(_sb_update.ToString(), SqlTrx.Connection, SqlTrx);

        m_da_cage_movements.UpdateCommand.Parameters.Add("@pMcCollectionId", SqlDbType.BigInt).SourceColumn = "MC_COLLECTION_ID";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pUserCage", SqlDbType.Int).Value = m_user_id;
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pUserCashier", SqlDbType.Int).Value = m_user_id;
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pTerminalCashier", SqlDbType.BigInt).SourceColumn = "MC_TERMINAL_ID";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pOperationType", SqlDbType.Int).Value = Cage.CageOperationType.FromTerminal;
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pMovementDateTime", SqlDbType.DateTime).Value = WGDB.Now;
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pStatus", SqlDbType.BigInt).SourceColumn = "NEW_STATUS";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = m_cage_session;
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).SourceColumn = "MC_CASHIER_SESSION_ID";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pMovementId", SqlDbType.BigInt).SourceColumn = "CGM_MOVEMENT_ID";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pOldStatus", SqlDbType.Int).SourceColumn = "CGM_STATUS";
        m_da_cage_movements.UpdateCommand.Parameters["@pMovementId"].Direction = ParameterDirection.InputOutput;

        m_da_cage_movements.Update(Movements);

        return true;
      }
      catch (DBConcurrencyException _ex)
      {
        Log.Exception(_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    #endregion

    #region MONEY COLLECTIONS BATCH UPDATE

    //------------------------------------------------------------------------------
    // PURPOSE : Gets money collection data of the specified Money collections and updates it
    //
    //  PARAMS :
    //      - INPUT : List of MoneyCollections, Transaction
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    //
    private Boolean UpdateSoftCountsMoneyCollections(List<Int64> MoneyCollections, DB_TRX _trx)
    {
      DataTable _dt_money_collections;

      if (GetSoftCountMoneyCollections(MoneyCollections, out _dt_money_collections, _trx.SqlTransaction))
      {
        if (ProcessMoneyCollections(MoneyCollections, _dt_money_collections))
        {
          if (UpdateMoneyCollections(_dt_money_collections, _trx.SqlTransaction))
          {
            return true;
          }
        }
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets money collection data of the specified Money collections
    //
    //  PARAMS :
    //      - INPUT : List of MoneyCollections, Transaction
    //      - OUTPUT : 
    // RETURNS : Datatable: with Money Collection Data
    //
    //   NOTES : 
    //
    private Boolean GetSoftCountMoneyCollections(List<Int64> MoneyCollections, out DataTable DTMoneyCollections, SqlTransaction _sql_trx)
    {
      StringBuilder _sb_select;
      Int32 _counter;

      DTMoneyCollections = new DataTable();

      _sb_select = new StringBuilder();
      _sb_select.AppendLine(" SELECT   MC_COLLECTION_ID                    ");
      _sb_select.AppendLine("        , MC_USER_ID                          ");
      _sb_select.AppendLine("        , MC_COLLECTION_DATETIME              ");
      _sb_select.AppendLine("        , MC_STATUS                           ");
      _sb_select.AppendLine("        , MC_CASHIER_SESSION_ID               ");
      _sb_select.AppendLine("        , MC_COLLECTED_BILL_AMOUNT            ");
      _sb_select.AppendLine("        , MC_COLLECTED_BILL_COUNT             ");
      _sb_select.AppendLine("        , MC_COLLECTED_TICKET_AMOUNT          ");
      _sb_select.AppendLine("        , MC_COLLECTED_TICKET_COUNT           ");
      _sb_select.AppendLine("        , MC_COLLECTED_RE_TICKET_AMOUNT       ");
      _sb_select.AppendLine("        , MC_COLLECTED_RE_TICKET_COUNT        ");
      _sb_select.AppendLine("        , MC_COLLECTED_PROMO_RE_TICKET_AMOUNT ");
      _sb_select.AppendLine("        , MC_COLLECTED_PROMO_RE_TICKET_COUNT  ");
      _sb_select.AppendLine("        , MC_COLLECTED_PROMO_NR_TICKET_AMOUNT ");
      _sb_select.AppendLine("        , MC_COLLECTED_PROMO_NR_TICKET_COUNT  ");
      _sb_select.AppendLine("        , 0 AS NEW_STATUS                     ");
      _sb_select.AppendLine("   FROM   MONEY_COLLECTIONS                   ");
      _sb_select.AppendLine("  WHERE   MC_COLLECTION_ID IN (               ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb_select.ToString(), _sql_trx.Connection, _sql_trx))
        {
          _counter = 0;

          foreach (Int64 _money_collection_id in MoneyCollections)
          {
            if (_counter > 0)
            {
              _sb_select.Append(", ");
            }
            _sb_select.Append(String.Format("@pMoneyCollection{0}", _counter));
            _sql_cmd.Parameters.Add(String.Format("@pMoneyCollection{0}", _counter), SqlDbType.BigInt).Value = _money_collection_id;

            _counter++;
          }
          _sb_select.AppendLine(" ) ");

          _sql_cmd.CommandText = _sb_select.ToString();

          m_da_money_collections = new SqlDataAdapter();
          m_da_money_collections.SelectCommand = _sql_cmd;

          m_da_money_collections.Fill(DTMoneyCollections);

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Process money collection data before update it to DB
    //
    //  PARAMS :
    //      - INPUT : Datatable
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    // 
    private Boolean ProcessMoneyCollections(List<Int64> MoneyCollectionIds, DataTable MoneyCollections)
    {
      DataRow[] _merged_row;
      SoftCount _soft_count;
      String _soft_count_id;
      Dictionary<Int64, Cage.CageStatus> _mc_status;
      Cage.CageStatus _cage_status;

      String _select = " MC_STATUS = " + (Int32)TITO_MONEY_COLLECTION_STATUS.PENDING;

      try
      {
        //All money collections must have status pending
        if (MoneyCollections.Select(_select).Length != MoneyCollections.Rows.Count)
        {
          return false;
        }

        if (!GetCollectedSoftCountWithStatus(MoneyCollectionIds, out _mc_status))
        {
          return false;
        }

        foreach (DataRow _row in MoneyCollections.Rows)
        {
          _merged_row = MergedCollectionSoftCount.Select("COLLECTION_ID = " + _row["MC_COLLECTION_ID"]);
          if (_merged_row.Length == 0)
          {
            return false;
          }

          switch (m_machine_id_type)
          {
            case ImportId.StackerId:
              _soft_count_id = _merged_row[0]["STACKER_ID"].ToString();
              break;
            case ImportId.EGMBaseName:
              _soft_count_id = _merged_row[0]["TERMINAL_ID"].ToString();
              break;
            case ImportId.EGMFloorId:
              _soft_count_id = _merged_row[0]["FLOOR_ID"].ToString();
              break;
            default:
              return false;
          }

          if (!m_imported_soft_count_obj.GetSoftCount(_soft_count_id, out _soft_count))
          {
            return false;
          }

          if (!_mc_status.TryGetValue((Int64)_row["MC_COLLECTION_ID"], out _cage_status))
          {
            return false;
          }

          _row["MC_USER_ID"] = m_user_id;
          _row["MC_COLLECTION_DATETIME"] = WGDB.Now;
          _row["NEW_STATUS"] = (Int32)ConvertCageStatusToMoneyCollectionStatus(_cage_status);
          _row["MC_COLLECTED_BILL_AMOUNT"] = _soft_count.TotalBillsAmount;
          _row["MC_COLLECTED_BILL_COUNT"] = _soft_count.TotalBillsCount;
          _row["MC_COLLECTED_TICKET_AMOUNT"] = _soft_count.TicketList.m_total_tickets_amount;
          _row["MC_COLLECTED_TICKET_COUNT"] = _soft_count.TicketList.m_total_tickets_count;
          _row["MC_COLLECTED_RE_TICKET_AMOUNT"] = _soft_count.TicketList.m_total_ticket_re_amount;
          _row["MC_COLLECTED_RE_TICKET_COUNT"] = _soft_count.TicketList.m_total_ticket_re_count;
          _row["MC_COLLECTED_PROMO_RE_TICKET_AMOUNT"] = _soft_count.TicketList.m_total_ticket_promo_re_amount;
          _row["MC_COLLECTED_PROMO_RE_TICKET_COUNT"] = _soft_count.TicketList.m_total_ticket_promo_re_count;
          _row["MC_COLLECTED_PROMO_NR_TICKET_AMOUNT"] = _soft_count.TicketList.m_total_ticket_promo_nr_amount;
          _row["MC_COLLECTED_PROMO_NR_TICKET_COUNT"] = _soft_count.TicketList.m_total_ticket_promo_nr_count;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    private TITO_MONEY_COLLECTION_STATUS ConvertCageStatusToMoneyCollectionStatus(Cage.CageStatus _cage_status)
    {
      switch (_cage_status)
      {
        case Cage.CageStatus.OK:
          return TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED;

        case Cage.CageStatus.OkAmountImbalance:
        case Cage.CageStatus.OkDenominationImbalance:
          return TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE;

        default:
          return TITO_MONEY_COLLECTION_STATUS.PENDING;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Updates money collection data
    //
    //  PARAMS :
    //      - INPUT : Datatable
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    // 
    private Boolean UpdateMoneyCollections(DataTable MoneyCollections, SqlTransaction SqlTrx)
    {
      StringBuilder _sb_update;

      _sb_update = new StringBuilder();

      _sb_update.AppendLine(" UPDATE   MONEY_COLLECTIONS                                           ");
      _sb_update.AppendLine("    SET   MC_USER_ID = @pUserId                                       ");
      _sb_update.AppendLine("        , MC_COLLECTION_DATETIME = @pCollectionDateTime               ");
      _sb_update.AppendLine("        , MC_STATUS = @pNewStatus                                     ");
      _sb_update.AppendLine("        , MC_COLLECTED_BILL_AMOUNT = @pBillAmount                     ");
      _sb_update.AppendLine("        , MC_COLLECTED_BILL_COUNT = @pBillCount                       ");
      _sb_update.AppendLine("        , MC_COLLECTED_TICKET_AMOUNT = @pTicketAmount                 ");
      _sb_update.AppendLine("        , MC_COLLECTED_TICKET_COUNT = @pTicketCount                   ");
      _sb_update.AppendLine("        , MC_COLLECTED_RE_TICKET_AMOUNT =  @pReTicketAmount           ");
      _sb_update.AppendLine("        , MC_COLLECTED_RE_TICKET_COUNT = @pReTicketCount              ");
      _sb_update.AppendLine("        , MC_COLLECTED_PROMO_RE_TICKET_AMOUNT = @pPromoReTicketAmount ");
      _sb_update.AppendLine("        , MC_COLLECTED_PROMO_RE_TICKET_COUNT = @pPromoReTicketCount   ");
      _sb_update.AppendLine("        , MC_COLLECTED_PROMO_NR_TICKET_AMOUNT = @pPromoNRTicketAmount ");
      _sb_update.AppendLine("        , MC_COLLECTED_PROMO_NR_TICKET_COUNT = @pPromoNRTicketCount   ");
      _sb_update.AppendLine(" OUTPUT   INSERTED.MC_COLLECTION_ID                                   ");
      _sb_update.AppendLine("        , INSERTED.MC_TERMINAL_ID                                     ");
      _sb_update.AppendLine("        , INSERTED.MC_CASHIER_SESSION_ID                              ");
      _sb_update.AppendLine("  WHERE   MC_COLLECTION_ID = @pCollectionId                           ");
      _sb_update.AppendLine("    AND   MC_STATUS = @pOldStatus                                     ");

      try
      {
        m_da_cage_movements.UpdateCommand = new SqlCommand(_sb_update.ToString(), SqlTrx.Connection, SqlTrx);

        m_da_cage_movements.UpdateCommand.Parameters.Add("@pUserId", SqlDbType.Int).Value = m_user_id;
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pCollectionDateTime", SqlDbType.DateTime).Value = WGDB.Now;
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pNewStatus", SqlDbType.Int).SourceColumn = "NEW_STATUS";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pBillAmount", SqlDbType.Decimal).SourceColumn = "MC_COLLECTED_BILL_AMOUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pBillCount", SqlDbType.Int).SourceColumn = "MC_COLLECTED_BILL_COUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pTicketAmount", SqlDbType.Decimal).SourceColumn = "MC_COLLECTED_TICKET_AMOUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pTicketCount", SqlDbType.Int).SourceColumn = "MC_COLLECTED_TICKET_COUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pReTicketAmount", SqlDbType.Decimal).SourceColumn = "MC_COLLECTED_RE_TICKET_AMOUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pReTicketCount", SqlDbType.Int).SourceColumn = "MC_COLLECTED_RE_TICKET_COUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pPromoReTicketAmount", SqlDbType.Decimal).SourceColumn = "MC_COLLECTED_PROMO_RE_TICKET_AMOUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pPromoReTicketCount", SqlDbType.Int).SourceColumn = "MC_COLLECTED_PROMO_RE_TICKET_COUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pPromoNRTicketAmount", SqlDbType.Decimal).SourceColumn = "MC_COLLECTED_PROMO_NR_TICKET_AMOUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pPromoNRTicketCount", SqlDbType.Int).SourceColumn = "MC_COLLECTED_PROMO_NR_TICKET_COUNT";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pCollectionId", SqlDbType.BigInt).SourceColumn = "MC_COLLECTION_ID";
        m_da_cage_movements.UpdateCommand.Parameters.Add("@pOldStatus", SqlDbType.Int).SourceColumn = "MC_STATUS";

        m_da_cage_movements.Update(MoneyCollections);

        return true;
      }
      catch (DBConcurrencyException _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    #endregion

    #region MONEY COLLECTION DETAILS BATCH UPDATE


    //------------------------------------------------------------------------------
    // PURPOSE : Gets Details datatable
    //
    //  PARAMS :
    //      - INPUT : List of money collection ids, sqltransaction
    //      - OUTPUT : Datatable
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    // 
    //private Boolean GetSoftCountCollectionsDetailForDB(List<Int64> MoneyCollections, out DataTable CollectionDetails)
    //{
    //  DataRow[] _merged_row;
    //  SoftCount _soft_count;
    //  String _soft_count_id;
    //  DataTable _soft_count_details;
    //  _soft_count = new SoftCount();

    //  CollectionDetails = _soft_count.CreateSoftCountDetailsDatatable();

    //  try
    //  {
    //    foreach (Int64 _money_collection_id in MoneyCollections)
    //    {
    //      _soft_count_id = GetStackerByMC(_money_collection_id);

    //      if (!m_imported_soft_count_obj.GetSoftCount(_soft_count_id, out _soft_count))
    //      {
    //        return false;
    //      }

    //      _soft_count.m_target_collection_id = _money_collection_id;
    //      _soft_count_details = _soft_count.SoftCountDetails.Copy();
    //      CollectionDetails.Merge(_soft_count_details);
    //    }

    //    return true;
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);
    //    return false;
    //  }
    //}

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts or updates money collection details
    //
    //  PARAMS :
    //      - INPUT : Datatable, transaction
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    // 
    private Boolean InsertMoneyCollectionDetails(DataTable CollectionDetails, SqlTransaction _sql_trx)
    {

      StringBuilder _sb_update;
      DataRow[] _details;
      SqlDataAdapter _da_collection_details;

      _sb_update = new StringBuilder();

      //UPDATE sentence must only be used by tickets. First ticket is inserted, and the other ones updates MCD_NUM_COLLECTED
      //Currencies just have to be inserted.
      _sb_update.AppendLine(" IF (EXISTS (SELECT 1 FROM MONEY_COLLECTION_DETAILS WHERE MCD_COLLECTION_ID = @pMoneyCollectionId AND MCD_FACE_VALUE = @pFaceValue )) ");
      _sb_update.AppendLine(" UPDATE   MONEY_COLLECTION_DETAILS                      ");
      _sb_update.AppendLine("    SET   MCD_NUM_COLLECTED = MCD_NUM_COLLECTED + 1     ");
      _sb_update.AppendLine("  WHERE   MCD_COLLECTION_ID = @pMoneyCollectionId AND MCD_FACE_VALUE = @pFaceValue  ");
      _sb_update.AppendLine(" ELSE                                                   ");
      _sb_update.AppendLine(" INSERT INTO   MONEY_COLLECTION_DETAILS ");
      _sb_update.AppendLine("             ( MCD_COLLECTION_ID        ");
      _sb_update.AppendLine("             , MCD_FACE_VALUE           ");
      _sb_update.AppendLine("             , MCD_NUM_EXPECTED         ");
      _sb_update.AppendLine("             , MCD_NUM_COLLECTED        ");
      _sb_update.AppendLine("             , MCD_CAGE_CURRENCY_TYPE   ");
      _sb_update.AppendLine("             )                          ");
      _sb_update.AppendLine("      VALUES ( @pMoneyCollectionId      ");
      _sb_update.AppendLine("             , @pFaceValue              ");
      _sb_update.AppendLine("             , ISNULL(@pExpected,0)     ");
      _sb_update.AppendLine("             , ISNULL(@pCollected,0)    ");
      _sb_update.AppendLine("             , @pType                   ");
      _sb_update.AppendLine("             )                          ");

      try
      {
        _da_collection_details = new SqlDataAdapter();

        _da_collection_details.InsertCommand = new SqlCommand(_sb_update.ToString(), _sql_trx.Connection, _sql_trx);
        _da_collection_details.InsertCommand.Parameters.Add("@pMoneyCollectionId", SqlDbType.Int).SourceColumn = "COLLECTION_ID";
        _da_collection_details.InsertCommand.Parameters.Add("@pFaceValue", SqlDbType.Decimal).SourceColumn = "DENOMINATION_NUMERIC"; //Tickets must be -200
        _da_collection_details.InsertCommand.Parameters.Add("@pExpected", SqlDbType.Int).SourceColumn = "EXPECTED_BILLS_DENOMINATION_QUANTITY"; //Also used for tickets
        _da_collection_details.InsertCommand.Parameters.Add("@pCollected", SqlDbType.Int).SourceColumn = "COLLECTED_BILLS_DENOMINATION_QUANTITY";
        _da_collection_details.InsertCommand.Parameters.Add("@pType", SqlDbType.Int).Value = (Int32)CageCurrencyType.Bill;

        _details = CollectionDetails.Select("( (TYPE = " + (Int32)TYPE_ROW_TICKET_BILL.BILL +
                                            ") OR (TYPE = " + (Int32)TYPE_ROW_TICKET_BILL.TICKET +
                                            " AND TICKET_STATUS = " + (Int32)STATUS_TICKET.TICKET_STATUS_OK + "))");

        _da_collection_details.Update(_details);

        return true;
      }
      catch (DBConcurrencyException _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    #endregion

    #region TICKET COLLECTION BATCH UPDATE

    private Boolean AddCageMovementToCollectedTickets(DataTable Tickets, DataTable CageMovements)
    {
      Dictionary<Int64, Int64> _dc_money_collection_cage_movement;
      Int64 _cage_movement;

      _dc_money_collection_cage_movement = new Dictionary<long, long>();
      try
      {
        foreach (DataRow _row in CageMovements.Rows)
        {
          _dc_money_collection_cage_movement.Add((Int64)_row["MC_COLLECTION_ID"], (Int64)_row["CGM_MOVEMENT_ID"]);
        }

        foreach (DataRow _row in Tickets.Rows)
        {
          if (!_dc_money_collection_cage_movement.TryGetValue((Int64)_row["COLLECTION_ID"], out _cage_movement))
          {
            return false;
          }
          _row["CAGE_MOVEMENT_ID"] = _cage_movement;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Updates Collected tickets
    //
    //  PARAMS :
    //      - INPUT : Datatable, transaction
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    // 
    private Boolean UpdateCollectedTickets(DataTable Tickets, DataTable CageMovements, SqlTransaction _sql_trx)
    {

      StringBuilder _sb_update;
      SqlDataAdapter _da_collection_details;
      DataRow[] _tickets;

      if (!AddCageMovementToCollectedTickets(Tickets, CageMovements))
      {
        return false;
      }

      _sb_update = new StringBuilder();

      _sb_update.AppendLine("UPDATE   TICKETS                                                   ");
      _sb_update.AppendLine("   SET   TI_COLLECTED_MONEY_COLLECTION = @CollectedMoneyCollection ");
      _sb_update.AppendLine("       , TI_CAGE_MOVEMENT_ID = @pMovementId                        ");
      _sb_update.AppendLine(" WHERE   TI_TICKET_ID = @pTicketId                                 ");
      _sb_update.AppendLine("   AND   TI_COLLECTED_MONEY_COLLECTION IS NULL                     ");

      try
      {
        _da_collection_details = new SqlDataAdapter();

        _da_collection_details.InsertCommand = new SqlCommand(_sb_update.ToString(), _sql_trx.Connection, _sql_trx);
        _da_collection_details.InsertCommand.Parameters.Add("@CollectedMoneyCollection", SqlDbType.Int).SourceColumn = "COLLECTION_ID";
        _da_collection_details.InsertCommand.Parameters.Add("@pMovementId", SqlDbType.Decimal).SourceColumn = "CAGE_MOVEMENT_ID";
        _da_collection_details.InsertCommand.Parameters.Add("@pTicketId", SqlDbType.Int).SourceColumn = "TICKET_ID";

        _da_collection_details.UpdateCommand = new SqlCommand(_sb_update.ToString(), _sql_trx.Connection, _sql_trx);
        _da_collection_details.UpdateCommand.Parameters.Add("@CollectedMoneyCollection", SqlDbType.Int).SourceColumn = "COLLECTION_ID";
        _da_collection_details.UpdateCommand.Parameters.Add("@pMovementId", SqlDbType.Decimal).SourceColumn = "CAGE_MOVEMENT_ID";
        _da_collection_details.UpdateCommand.Parameters.Add("@pTicketId", SqlDbType.Int).SourceColumn = "TICKET_ID";

        _tickets = Tickets.Select("TYPE = " + (Int32)TYPE_ROW_TICKET_BILL.TICKET + " AND TICKET_STATUS =" + (Int32)STATUS_TICKET.TICKET_STATUS_OK);

        _da_collection_details.Update(_tickets);

      }
      catch (DBConcurrencyException _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      return true;
    }

    #endregion

    #region CAGE STOCK BATCH UPDATE

    //------------------------------------------------------------------------------
    // PURPOSE : Process money collection data before update it to DB
    //
    //  PARAMS :
    //      - INPUT : Datatable
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    // 
    private Boolean UpdateCageStock(DataTable CageStock, SqlTransaction _sql_trx)
    {
      SqlDataAdapter _da_cage_stock;
      StringBuilder _sb_insert;

      _sb_insert = new StringBuilder();

      _sb_insert.AppendLine(" IF EXISTS ( SELECT   1                                         ");
      _sb_insert.AppendLine("               FROM   CAGE_STOCK                                ");
      _sb_insert.AppendLine("              WHERE   CGS_ISO_CODE       = @pIsoCode            ");
      _sb_insert.AppendLine("                AND   CGS_DENOMINATION       = @pDenomination   ");
      _sb_insert.AppendLine("                AND   CGS_CAGE_CURRENCY_TYPE = @pCurrencyType ) ");
      _sb_insert.AppendLine("  UPDATE   CAGE_STOCK                                           ");
      _sb_insert.AppendLine("     SET  CGS_QUANTITY        = CGS_QUANTITY + @pQuantity       ");
      _sb_insert.AppendLine("   WHERE  CGS_ISO_CODE        = @pIsoCode                       ");
      _sb_insert.AppendLine("     AND  CGS_DENOMINATION    = @pDenomination                  ");
      _sb_insert.AppendLine("     AND  CGS_CAGE_CURRENCY_TYPE = @pCurrencyType               ");
      _sb_insert.AppendLine(" ELSE                                                           ");
      _sb_insert.AppendLine("     INSERT INTO   CAGE_STOCK                                   ");
      _sb_insert.AppendLine("                 ( CGS_ISO_CODE                                 ");
      _sb_insert.AppendLine("                 , CGS_DENOMINATION                             ");
      _sb_insert.AppendLine("                 , CGS_QUANTITY                                 ");
      _sb_insert.AppendLine("                 , CGS_CAGE_CURRENCY_TYPE                       ");
      _sb_insert.AppendLine("                 )                                              ");
      _sb_insert.AppendLine("          VALUES                                                ");
      _sb_insert.AppendLine("                 ( @pIsoCode                                    ");
      _sb_insert.AppendLine("                 , @pDenomination                               ");
      _sb_insert.AppendLine("                 , @pQuantity                                   ");
      _sb_insert.AppendLine("                 , @pCurrencyType                               ");
      _sb_insert.AppendLine("                 )                                              ");

      try
      {
        _da_cage_stock = new SqlDataAdapter();
        _da_cage_stock.InsertCommand = new SqlCommand(_sb_insert.ToString(), _sql_trx.Connection, _sql_trx);
        _da_cage_stock.InsertCommand.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).SourceColumn = "ISO_CODE";
        _da_cage_stock.InsertCommand.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "DENOMINATION";
        _da_cage_stock.InsertCommand.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = CageCurrencyType.Bill;
        _da_cage_stock.InsertCommand.Parameters.Add("@pQuantity", SqlDbType.Decimal).SourceColumn = "QUANTITY";

        _da_cage_stock.Update(CageStock);

      }
      catch (DBConcurrencyException _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      return true;
    }

    #endregion

    #region MOBILE BANK, CASHIER MOVEMENTS, CAGE METERS

    private Boolean UpdateMobileBankCashierMovementsAndCageMeters(List<Int64> MoneyCollectionsIds, SqlTransaction Trx)
    {
      DataTable MBCMAndCageMetersData;

      if (!GetMBAndCMAndCageMetersData(MoneyCollectionsIds, out MBCMAndCageMetersData, Trx))
      {
        return false;
      }
      if (!FillMBAndCMData(MBCMAndCageMetersData))
      {
        return false;
      }
      if (!InsertMBAndCashierMovements(MBCMAndCageMetersData, Trx))
      {
        return false;
      }
      if (!UpdateCageMeters(MBCMAndCageMetersData, Trx))
      {
        return false;
      }
      if (!CloseAllCollectedCashierSessions(MBCMAndCageMetersData, Trx))
      {
        return false;
      }

      return true;
    }

    public Boolean GetMBAndCMAndCageMetersData(List<Int64> MoneyCollecionIds, out DataTable MBCMAndCageMetersData, SqlTransaction Trx)
    {
      String _mc_ids;
      StringBuilder _sb;

      _mc_ids = "";
      MBCMAndCageMetersData = new DataTable();

      if (MoneyCollecionIds.Count > 0)
      {
        foreach (Int64 _id in MoneyCollecionIds.ToArray())
        {
          _mc_ids = (String.IsNullOrEmpty(_mc_ids)) ? _id.ToString() : _mc_ids + "," + _id.ToString();

        }

        try
        {
          _sb = new StringBuilder();

          _sb.AppendLine("   SELECT   CS_SESSION_ID    AS SESSION_ID ");
          _sb.AppendLine("          , MC_TERMINAL_ID   AS TERMINAL_ID ");
          _sb.AppendLine("          , GU_USER_TYPE     AS USER_TYPE ");
          _sb.AppendLine("          , MC_COLLECTION_ID AS COLLECTION_ID ");
          _sb.AppendLine("          , 0.0              AS AMOUNT_TO_DEPOSIT ");
          _sb.AppendLine("          , 0.0              AS COLLECTED_BILL_AMOUNT");
          _sb.AppendLine("          , 0.0              AS COLLECTED_TICKET_AMOUNT ");
          _sb.AppendLine("   FROM MONEY_COLLECTIONS");
          _sb.AppendLine("        LEFT JOIN CASHIER_SESSIONS ON CS_SESSION_ID = MC_CASHIER_SESSION_ID");
          _sb.AppendLine("        LEFT JOIN GUI_USERS ON GU_USER_ID = CS_USER_ID");
          _sb.AppendLine("   WHERE   MC_COLLECTION_ID IN (" + _mc_ids + ")");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(MBCMAndCageMetersData);
            }
          }

          if (MBCMAndCageMetersData.Rows.Count != MoneyCollecionIds.Count)
          {
            return false;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          return false;
        }
      }
      return true;
    }

    private Boolean FillMBAndCMData(DataTable MBAndCMData)
    {
      SoftCount _soft_count;
      foreach (DataRow _row in MBAndCMData.Rows)
      {
        if (!m_imported_soft_count_obj.GetSoftCount(GetStackerByMC((Int64)_row["COLLECTION_ID"]), out _soft_count))
        {
          return false;
        }
        _row["AMOUNT_TO_DEPOSIT"] = _soft_count.TotalBillsAmount;
        _row["COLLECTED_BILL_AMOUNT"] = _soft_count.TotalBillsAmount;
        _row["COLLECTED_TICKET_AMOUNT"] = _soft_count.TicketList.m_total_tickets_amount;
      }
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts MB movements for each specified collection
    //
    //  PARAMS :
    //      - INPUT : Datatable, sqltransaction
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    // 
    private Boolean InsertMBAndCashierMovements(DataTable CollectionTotals, SqlTransaction Trx)
    {
      try
      {
        foreach (DataRow _row in CollectionTotals.Rows)
        {
          if (!NACashDeposit((Int64)_row["SESSION_ID"], (Int32)_row["TERMINAL_ID"], (GU_USER_TYPE)(Int16)_row["USER_TYPE"], (Decimal)_row["AMOUNT_TO_DEPOSIT"], Trx))
          {
            return false;
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts MB movements
    //
    //  PARAMS :
    //      - INPUT : CashierSessionId, TerminalId, UserType, Amount, sqltransaction
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    // 
    private Boolean NACashDeposit(Int64 CashierSessionId, Int32 TerminalId, GU_USER_TYPE UserType, Currency AmountToDeposit, SqlTransaction Trx)
    {
      Int64 _movement_id;
      Int32 _cashier_user_id;
      String _cashier_user_name;
      Int32 _cashier_terminal_id;
      String _cashier_terminal_name;
      Currency _cashier_balance;
      CashierSessionInfo _session_info;
      CashierMovementsTable _cashier_movements;
      String _terminal_name;
      MB_USER_TYPE _mb_user_type;
      Int64 _mb_account_id;
      String _mb_track_data;
      Int64 _dummy_mb_cashier_session_id;
      MBMovementData _mb_movement_Info;

      _movement_id = 0;
      _cashier_user_id = 0;
      _cashier_user_name = "";
      _cashier_terminal_id = 0;
      _cashier_terminal_name = "";
      _cashier_balance = 0;
      _terminal_name = "";
      _mb_track_data = "";

      try
      {
        if (TerminalId > 0)
        {
          if (!WSI.Common.Misc.GetTerminalName(TerminalId, out _terminal_name, Trx))
          {

            return false;
          }
        }
        switch (UserType)
        {
          case GU_USER_TYPE.SYS_ACCEPTOR:
            _mb_user_type = MB_USER_TYPE.SYS_ACCEPTOR;
            break;
          case GU_USER_TYPE.SYS_TITO:
            _mb_user_type = MB_USER_TYPE.SYS_TITO;
            break;
          default:
            _mb_user_type = MB_USER_TYPE.SYS_PROMOBOX;
            break;
        }

        if (!WSI.Common.Cashier.ReadMobileBankNoteUser(Trx, _mb_user_type, out _mb_account_id, out _mb_track_data, out _dummy_mb_cashier_session_id, _terminal_name))
        {
          return false;
        }

        if (!Cashier.GetCashierInfoBySession(CashierSessionId, Trx, out _cashier_user_id, out _cashier_user_name, out _cashier_terminal_id, out _cashier_terminal_name, out _cashier_balance))
        {
          return false;
        }

        _mb_movement_Info = new MBMovementData();
        _mb_movement_Info.CashierId = _cashier_terminal_id;
        _mb_movement_Info.CardId = _mb_account_id;
        _mb_movement_Info.Type = MBMovementType.DepositCash;
        _mb_movement_Info.CashierSessionId = CashierSessionId;
        _mb_movement_Info.InitialBalance = 0;
        _mb_movement_Info.SubAmount = 0;
        _mb_movement_Info.AddAmount = AmountToDeposit;
        _mb_movement_Info.FinalBalance = AmountToDeposit;
        _mb_movement_Info.CashierName = _terminal_name;

        MobileBank.DB_InsertMBCardMovement(_mb_movement_Info, out _movement_id, Trx);

        _session_info = new CashierSessionInfo();
        _session_info.CashierSessionId = CashierSessionId;
        _session_info.TerminalId = _cashier_terminal_id;
        _session_info.UserId = _cashier_user_id;
        _session_info.TerminalName = _cashier_terminal_name;
        _session_info.UserName = _cashier_user_name;
        _session_info.AuthorizedByUserId = _session_info.UserId;
        _session_info.AuthorizedByUserName = _session_info.UserName;

        _cashier_movements = new CashierMovementsTable(_session_info);
        _cashier_movements.Add(0, CASHIER_MOVEMENT.MB_CASH_IN, AmountToDeposit, _mb_account_id, _mb_track_data);

        if (!_cashier_movements.Save(Trx))
        {
          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Update Bills and tickets meters
    //
    //  PARAMS :
    //      - INPUT : Datatable, sqltransaction
    //      - OUTPUT : 
    // RETURNS : Boolean: TRUE if processed successfully
    //
    //   NOTES : 
    // 
    private Boolean UpdateCageMeters(DataTable MoneyCollections, SqlTransaction Trx)
    {
      Decimal _result;
      Decimal _result_session;

      try
      {
        foreach (DataRow _row in MoneyCollections.Rows)
        {
          if ((Decimal)_row["COLLECTED_BILL_AMOUNT"] > 0)
          {
            if (!CageMeters.UpdateCageMeters((Decimal)_row["COLLECTED_BILL_AMOUNT"]
                                           , 0
                                           , (Int64)_row["SESSION_ID"]
                                           , (Int32)CageMeters.CageSystemSourceTarget.Terminals
                                           , 0
                                           , GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
                                           , 0 // TODO JML 22-Abr-2016: add Cage currency type
                                           , CageMeters.UpdateCageMetersOperationType.Increment
                                           , CageMeters.UpdateCageMetersgetGetSessionMode.FromParameter
                                           , out _result
                                           , out _result_session
                                           , Trx))
            {
              return false;
            }
            if (!CageMeters.UpdateCageMeters((Decimal)_row["COLLECTED_BILL_AMOUNT"]
                                           , 0
                                           , (Int64)_row["SESSION_ID"]
                                           , (Int32)CageMeters.CageSystemSourceTarget.Terminals
                                           , 10
                                           , GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
                                           , 0 // TODO JML 22-Abr-2016: add Cage currency type
                                           , CageMeters.UpdateCageMetersOperationType.Increment
                                           , CageMeters.UpdateCageMetersgetGetSessionMode.FromParameter
                                           , out _result
                                           , out _result_session
                                           , Trx))
            {
              return false;
            }

          }

          if ((Decimal)_row["COLLECTED_TICKET_AMOUNT"] > 0)
          {
            if (!CageMeters.UpdateCageMeters((Decimal)_row["COLLECTED_TICKET_AMOUNT"]
                                           , 0
                                           , (Int64)_row["SESSION_ID"]
                                           , (Int32)CageMeters.CageSystemSourceTarget.Terminals
                                           , 0
                                           , GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
                                           , 0 // TODO JML 22-Abr-2016: add Cage currency type
                                           , CageMeters.UpdateCageMetersOperationType.Increment
                                           , CageMeters.UpdateCageMetersgetGetSessionMode.FromParameter
                                           , out _result
                                           , out _result_session
                                           , Trx))
            {
              return false;
            }
            if (!CageMeters.UpdateCageMeters((Decimal)_row["COLLECTED_TICKET_AMOUNT"]
                                            , 0
                                            , (Int64)_row["SESSION_ID"]
                                            , (Int32)CageMeters.CageSystemSourceTarget.Terminals
                                            , 9
                                            , GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
                                            , 0 // TODO JML 22-Abr-2016: add Cage currency type
                                            , CageMeters.UpdateCageMetersOperationType.Increment
                                            , CageMeters.UpdateCageMetersgetGetSessionMode.FromParameter
                                            , out _result
                                            , out _result_session
                                            , Trx))
            {
              return false;
            }

          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    private Boolean CloseAllCollectedCashierSessions(DataTable CashierSessions, SqlTransaction Trx)
    {
      foreach (DataRow _row in CashierSessions.Rows)
      {
        if (!CloseCashierSession((Int64)_row["SESSION_ID"], Trx))
        {
          return false;
        }
      }
      return true;
    }

    private Boolean CloseCashierSession(Int64 CashierSessionId, SqlTransaction Trx)
    {
      String _terminal_name;
      Int32 _terminal_id;
      CashierSessionInfo _session_info;
      CashierMovementsTable _cashier_movements;

      try
      {

        //Get terminal information to set cashier information used for Vouchers.
        _terminal_name = Environment.MachineName;
        _terminal_id = Cashier.ReadTerminalId(Trx, _terminal_name, GU_USER_TYPE.USER);

        if (_terminal_id == 0)
        {
          return false;
        }

        // Set the information as the following:
        //  - Machine: is the PC where the GUI is executed.
        //  - User: is the GUI user, not the Cashier user.
        WSI.Common.CommonCashierInformation.SetCashierInformation(CashierSessionId, m_user_id, m_user_name, _terminal_id, _terminal_name);

        // Set session information
        _session_info = CommonCashierInformation.CashierSessionInfo();
        // Set user type
        _session_info.UserType = GU_USER_TYPE.SYS_TITO;

        _cashier_movements = new CashierMovementsTable(_session_info);
        _cashier_movements.Add(0, CASHIER_MOVEMENT.CLOSE_SESSION, 0, 0, "");

        if (!_cashier_movements.Save(Trx))
        {
          return false;
        }

        // Close cashier session of system mobile bank
        if (!Cashier.CashierSessionCloseById(CashierSessionId, _terminal_id, _terminal_name, Trx))
        {
          return false;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      return true;
    }
    #endregion

  }
}
