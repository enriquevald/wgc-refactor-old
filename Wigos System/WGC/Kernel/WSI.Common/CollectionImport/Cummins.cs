//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Cummins.cs
// 
//   DESCRIPTION: Class to manage cummins imported files
// 
//        AUTHOR: David Rigal
// 
// CREATION DATE: 11-MAY-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAY-2015 DRV    Initial Version
// 11-JUN-2015 SGB    BUG 1898: No register the last error in import data.
// 20-OCT-2015 SGB    Backlog Item 5422: Insert new formats import.
// 23-MAY-2018 AGS    Bug 32756:WIGOS-10750 [Ticket #13898] Contadora Cummins 4115 Compatible Con el Sistema

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.CollectionImport
{
  class Cummins : IImportCollectionFile
  {
    private enum FileType
    {
      CurrencyFile = 0,
      TicketsFile = 1
    }

    private const int POS_COLUMN_STACKER_ID_CD3 = 0;
    private const int POS_COLUMN_STACKER_ID_CD8 = 5;
    private const int POS_COLUMN_STACKER_ID_CD11 = 1;
    private const int POS_COLUMN_CURRENCY_CD3 = 1;
    private const int POS_COLUMN_CURRENCY_CD8 = 6;
    private const int POS_COLUMN_CURRENCY_CD11 = 5;
    private const String BILL = "BILL";

    private ImportInfo m_import_info;
    private Int32 m_currency_num_items;
    private Decimal[] m_defined_denominations;
    private String[] m_validated_currency_lines;
    private String[] m_validated_ticket_lines;
    private ImportId m_imported_id_type;
    private SoftCountImportFormat m_import_format;
    private ImportedSoftCounts m_imported_softcounts;
    private Boolean m_error_gp;
    private String m_currency_iso_code;

    public event ImportedReadEventHandler ImportedReadEvent;

    //------------------------------------------------------------------------------
    // PURPOSE : Class constructor
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    //
    //   NOTES :
    //
    public Cummins(ImportInfo Info)
    {
      //ImportedError _error;
      String[] _denominations;
      m_import_info = Info;

      try
      {
        m_error_gp = false;

        m_currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
        m_import_format = (SoftCountImportFormat)GeneralParam.GetInt32("SoftCount", "Import.Format", 1);
        m_imported_id_type = (ImportId)GeneralParam.GetInt32("SoftCount", "Format" + ((int)m_import_format).ToString("000") + ".Identifier", 1); //Format001.Identifier, or Format002.Identifier ,...
        _denominations = GeneralParam.GetString("SoftCount", "Format" + ((int)m_import_format).ToString("000") + ".Denominations", "").Split(';'); //Format001.Denominations, or Format002.Denominations ,...
        m_currency_num_items = _denominations.Length;
        m_defined_denominations = new Decimal[_denominations.Length];
        m_imported_softcounts = new ImportedSoftCounts();
        for (Int32 _idx = 0; _idx < _denominations.Length; _idx++)
        {
          m_defined_denominations[_idx] = Decimal.Parse(_denominations[_idx]);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_gp = true;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Validate the format of the specified files
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    public void Validate()
    {
      ImportedEvent _event;
      ImportedError _error;

      if (m_error_gp == true)
      {
        _error = new ImportedError();
        _error.Line =  1;
        _error.Error = Resource.String("STR_SOFT_COUNT_CONFIGURATION_VALIDATION_ERROR");
        ImportedReadEvent(_error);

      }
      else
      {
        if (ValidateFile(m_import_info.File1, FileType.CurrencyFile))
        {
          _event = new ImportedEvent();
          _event.ImportEvent = ImportEvent.ValidationFinishedFile1;
          ImportedReadEvent(_event);
        }
        if (ValidateFile(m_import_info.File2, FileType.TicketsFile))
        {
          _event = new ImportedEvent();
          _event.ImportEvent = ImportEvent.ValidationFinishedFile2;
          ImportedReadEvent(_event);
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Imports the specified files and fills the soft count class
    //
    //  PARAMS :
    //      - INPUT : Denomination Count
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    public bool Import()
    {
      ImportedEvent _event;

      ImportCurrencyFile();
      if (m_validated_currency_lines != null)
      {
        _event = new ImportedEvent();
        _event.ImportEvent = ImportEvent.ImportationFinishedFile1;
        ImportedReadEvent(_event);
      }
      ImportTicketFile();

     if (m_validated_ticket_lines != null)
      {
        _event = new ImportedEvent();
        _event.ImportEvent = ImportEvent.ImportationFinishedFile2;
        ImportedReadEvent(_event);
      }
      ImportedReadEvent(m_imported_softcounts);

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Imports the ticket file and adds the tickets to the soft count class
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    private void ImportTicketFile()
    {
      String[] _line;
      SoftCount _sc;
      ImportedTicketNumber _ticket;
      ImportedError _error;
      ImportedEvent _event;

      if (m_validated_ticket_lines == null)
      {
        return;
      }

      _event = new ImportedEvent();
      _event.ImportEvent = ImportEvent.ImportationStartedFile2;
      ImportedReadEvent(_event);

      if (m_imported_softcounts == null)
      {
        m_imported_softcounts = new ImportedSoftCounts();
      }

      for (Int64 _idx = 0; _idx < m_validated_ticket_lines.Length; _idx++)
      {
        if (!String.IsNullOrEmpty(m_validated_ticket_lines[_idx]))
        {
          _ticket = new ImportedTicketNumber();
          _line = m_validated_ticket_lines[_idx].Split(',');
          if (!Int64.TryParse(_line[1], out _ticket.Number))
          {
            _error = new ImportedError();
            _error.Line = _idx + 1;
            _error.Error = Resource.String("STR_SOFT_COUNT_TICKET_PARSER_ERROR");
            ImportedReadEvent(_error); _error = new ImportedError();
            continue;
          }
          if (m_imported_softcounts.GetSoftCount(_line[0], out _sc))
          {
            _sc.AddTicket(_ticket);
          }
          else
          {
            _sc = new SoftCount();
            _sc.m_soft_sount_machine_id_type = m_imported_id_type;
            _sc.m_soft_count_machine_id = _line[0];
            _sc.AddTicket(_ticket);
            m_imported_softcounts.AddOrUpdateSoftCount(_sc, _sc.m_soft_count_machine_id);
          }
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Imports the currency file and adds each currency to the soft count class
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    private void ImportCurrencyFile()
    {
      SoftCount _sc;
      ImportedError _error;
      String _line;
      ImportedEvent _event;

      if (m_validated_currency_lines == null)
      {
        return;
      }

      _event = new ImportedEvent();
      _event.ImportEvent = ImportEvent.ImportationStartedFile1;
      ImportedReadEvent(_event);

      for (Int64 _idx = 0; _idx < m_validated_currency_lines.Length; _idx++)
      {
        _line = m_validated_currency_lines[_idx];

        if (!String.IsNullOrEmpty(_line))
        {
          if (ParseCurrencyLine(_line, out _sc))
          {
            if (!m_imported_softcounts.AddSoftCount(_sc, _sc.m_soft_count_machine_id))
            {
              _error = new ImportedError();
              _error.Line = _idx + 1;
              _error.Error = Resource.String("STR_SOFT_COUNT_DUPLICATED_COLLECTION_ERROR", _sc.m_soft_count_machine_id);
              ImportedReadEvent(_error); _error = new ImportedError();
            }
          }
          else
          {
            _error = new ImportedError();
            _error.Line = _idx + 1;
            _error.Error = Resource.String("STR_SOFT_COUNT_CURRENCY_PARSER_ERROR", _sc.m_soft_count_machine_id);
            ImportedReadEvent(_error); _error = new ImportedError();
          }
        }
      }
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Parses cummins text lines and creates the soft count
    //
    //  PARAMS :
    //      - INPUT : String line
    //      - OUTPUT : Softcount
    // RETURNS : True/false if completed succesfully or not
    //
    //   NOTES :
    //
    private Boolean ParseCurrencyLine(String Line, out SoftCount SC)
    {
      String[] _parsed_line;
      Decimal _amount;
      DenominationCount _denom;

      SC = new SoftCount();

      _parsed_line = Line.Split(',');
      SC.m_soft_sount_machine_id_type = m_imported_id_type;

      try
      {
        switch (m_import_format)
        {
          case SoftCountImportFormat.FORMAT_CD3:

            SC.m_soft_count_machine_id = _parsed_line[POS_COLUMN_STACKER_ID_CD3];

            for (Int32 _idx =0; _idx < m_currency_num_items; _idx++)
            {
              if (!Decimal.TryParse(_parsed_line[_idx + POS_COLUMN_CURRENCY_CD3], out _amount))
              {
                return false;
              }
              if (_amount % m_defined_denominations[_idx ] > 0)
              {
                return false;
              }
              _denom = new DenominationCount();
              _denom.Denomination = m_defined_denominations[_idx ];
              _denom.Count = Convert.ToInt64(_amount / m_defined_denominations[_idx ]);
              SC.AddBillDenomination(_denom);
            }
            break;

          case SoftCountImportFormat.FORMAT_CD8:

            SC.m_soft_count_machine_id = _parsed_line[POS_COLUMN_STACKER_ID_CD8];

            for (Int32 _idx = 0; _idx < m_currency_num_items; _idx++)
            {
              if (!Decimal.TryParse(_parsed_line[_idx + POS_COLUMN_CURRENCY_CD8 ], out _amount))
              {
                return false;
              }
              _denom = new DenominationCount();
              _denom.Denomination = m_defined_denominations[_idx ];
              _denom.Count = Convert.ToInt64(_amount);
              SC.AddBillDenomination(_denom);
            }
            break;

          case SoftCountImportFormat.FORMAT_CD11:

            SC.m_soft_count_machine_id = _parsed_line[POS_COLUMN_STACKER_ID_CD11];

            for (Int32 _idx = 0; _idx < m_currency_num_items; _idx++)
            {
              if (!Decimal.TryParse(_parsed_line[_idx + POS_COLUMN_CURRENCY_CD11 ], out _amount))
              {
                return false;
              }
              _denom = new DenominationCount();
              _denom.Denomination = m_defined_denominations[_idx ];
              _denom.Count = Convert.ToInt64(_amount);
              SC.AddBillDenomination(_denom);
            }
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Validates the specified file
    //
    //  PARAMS :
    //      - INPUT : String, Filepath
    //                Filetype, the type of the file
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    private Boolean ValidateFile(String FilePath, FileType Filetype)
    {
      ImportedError _error;
      ImportedEvent _event;
      String[] Lines;
      List<string> l = new List<string>();
      Int32 index;

      if (String.IsNullOrEmpty(FilePath))
      {
        return false;
      }

      if (Filetype == FileType.CurrencyFile)
      {
        _event = new ImportedEvent();
        _event.ImportEvent = ImportEvent.ValidationStartedFile1;
        ImportedReadEvent(_event);
      }

      if (Filetype == FileType.TicketsFile)
      {
        _event = new ImportedEvent();
        _event.ImportEvent = ImportEvent.ValidationStartedFile2;
        ImportedReadEvent(_event);
      }

      if (!ReadFileLines(FilePath, Filetype, out Lines))
      {
        _event = new ImportedEvent();
        _event.ImportEvent = ImportEvent.File2NotFound;
        if (Filetype == FileType.CurrencyFile)
        {
          _event.ImportEvent = ImportEvent.File1NotFound;
        }
        ImportedReadEvent(_event);
        return false;
      }
      if (Filetype == FileType.CurrencyFile)
      {
        m_validated_currency_lines = new String[Lines.Length];
      }
      else
      {
        m_validated_ticket_lines = new String[Lines.Length];
      }

      for (Int32 _idx = 0; _idx < Lines.Length; _idx++)
      {
        index = -1;
        if (Filetype == FileType.CurrencyFile)
        {
          ValidateFormat(Lines, _idx);
        }
        else
        {
          if (!ValidateTicketLine(Lines[_idx]))
          {
            _error = new ImportedError();
            _error.Line = _idx + 1;
            _error.Error = Resource.String("STR_SOFT_COUNT_TICKET_VALIDATION_ERROR");
            ImportedReadEvent(_error);
          }
          else
          {
            index = l.IndexOf(Lines[_idx].Substring(Lines[_idx].IndexOf(",") + 1));
            if (index >= 0)
            {
              _error = new ImportedError();
              _error.Line = _idx + 1;
              _error.Error = string.Format(Resource.String("STR_SOFT_COUNT_TICKET_VALIDATION_WARNING"), Lines[_idx], Lines[index]);
              ImportedReadEvent(_error);
            }
            m_validated_ticket_lines[_idx] = Lines[_idx];
            l.Add(Lines[_idx].Substring(Lines[_idx].IndexOf(",") + 1));
          }
        }
      }
      return true;
    }


    //------------------------------------------------------------------------------
    // PURPOSE : checks if the specified line is a total line
    //
    //  PARAMS :
    //      - INPUT : 
    //        - String Line
    //        - idx
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    private void ValidateFormat(String[] Lines, Int32 _idx)
    {
      ImportedError _error;
      Boolean _is_error;
      String[] _line_validate;

      _line_validate = Lines[_idx].Split(',');

      switch (m_import_format)
      {
        case SoftCountImportFormat.FORMAT_CD3:

          if (!ValidateCurrencyLine(Lines[_idx]))
          {
            if (!(_idx == Lines.Length - 1) || (_idx == Lines.Length - 1) && !(IsTotalLine(Lines[_idx])))
            {
              _error = new ImportedError();
              _error.Line = _idx + 1;
              _error.Error = Resource.String("STR_SOFT_COUNT_CURRENCY_VALIDATION_ERROR");
              ImportedReadEvent(_error);
            }
          }
          else
          {
            m_validated_currency_lines[_idx] = Lines[_idx];
          }

          break;

        case SoftCountImportFormat.FORMAT_CD8:

          _is_error = false;
          if (!ValidateCurrencyLine(Lines[_idx]))
          {
            _error = new ImportedError();
            _error.Line = _idx + 1;
            _error.Error = Resource.String("STR_SOFT_COUNT_FILE_VALIDATION_ERROR");
            ImportedReadEvent(_error);
            _is_error = true;
          }

          if (_line_validate[2] != m_currency_iso_code && _is_error == false)
          {
            _error = new ImportedError();
            _error.Line = _idx + 1;
            _error.Error = Resource.String("STR_SOFT_COUNT_FILE_VALIDATION_ERROR");
            ImportedReadEvent(_error);
            _is_error = true;
          }

          if (_line_validate[3].ToUpper() != BILL && _is_error == false)
          {
            _error = new ImportedError();
            _error.Line = _idx + 1;
            _error.Error = Resource.String("STR_SOFT_COUNT_FILE_VALIDATION_ERROR");
            ImportedReadEvent(_error);
            _is_error = true;
          }

          if (_is_error == false)
          {
            m_validated_currency_lines[_idx] = Lines[_idx];
          }

          break;

        case SoftCountImportFormat.FORMAT_CD11:
          //Not check anyting empty
          if (!ValidateCurrencyLine(Lines[_idx]))
          {
            _error = new ImportedError();
            _error.Line = _idx + 1;
            _error.Error = Resource.String("STR_SOFT_COUNT_FILE_VALIDATION_ERROR");
            ImportedReadEvent(_error);
            _is_error = false;
          }
          else
          {
            m_validated_currency_lines[_idx] = Lines[_idx];
          }


          break;

      }
    }


    //------------------------------------------------------------------------------
    // PURPOSE : checks if the specified line is a total line
    //
    //  PARAMS :
    //      - INPUT : String line
    //      - OUTPUT :
    // RETURNS : True if the specified line is a total line
    //
    //   NOTES :
    //
    private Boolean IsTotalLine(String Line)
    {
      try
      {
        if (Line.Contains("TOTAL"))
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Validates the currency line format
    //
    //  PARAMS :
    //      - INPUT : String line
    //      - OUTPUT :
    // RETURNS : true if the line format is ok
    //
    //   NOTES :
    //
    private Boolean ValidateCurrencyLine(String Line)
    {

      try
      {
        switch (m_import_format)
        {
          case SoftCountImportFormat.FORMAT_CD3:

            if (Line.Split(',').Length == POS_COLUMN_CURRENCY_CD3 + m_currency_num_items)
            {
              return true;
            }
            break;

          case SoftCountImportFormat.FORMAT_CD8:
            if (Line.Split(',').Length == POS_COLUMN_CURRENCY_CD8 + m_currency_num_items)
            {
              return true;
            }
            break;

          case SoftCountImportFormat.FORMAT_CD11:
            if (Line.Split(',').Length == POS_COLUMN_CURRENCY_CD11 + m_currency_num_items)
            {
              return true;
            }
            break;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Validates the ticket line format
    //
    //  PARAMS :
    //      - INPUT : String line
    //      - OUTPUT :
    // RETURNS : true if the line format is ok
    //
    //   NOTES :
    //
    private Boolean ValidateTicketLine(String Line)
    {
      try
      {
        if (Line.Split(',').Length == 2)
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Read the specified file and convert it to a string array
    //
    //  PARAMS :
    //      - INPUT : String Path
    //                Filetype
    //      - OUTPUT : String array 
    // RETURNS : true if the task is completed successfully
    //
    //   NOTES :
    //
    private Boolean ReadFileLines(string Path, FileType Filetype, out String[] Lines)
    {
      Lines = null;
      try
      {
        Lines = System.IO.File.ReadAllLines(Path);

        if (Filetype == FileType.CurrencyFile)
        {
          m_imported_softcounts.m_file1_binary = System.IO.File.ReadAllBytes(Path);
          m_imported_softcounts.m_file1_name = System.IO.Path.GetFileName(Path);
        }
        else
        {
          m_imported_softcounts.m_file2_binary = System.IO.File.ReadAllBytes(Path);
          m_imported_softcounts.m_file2_name = System.IO.Path.GetFileName(Path);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
  }
}
