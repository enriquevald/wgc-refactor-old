//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ImportInfo.cs
// 
//   DESCRIPTION: Class to specify the files that must be imported
// 
//        AUTHOR: David Rigal
// 
// CREATION DATE: 11-MAY-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAY-2015 DRV    Initial Version
// 20-OCT-2015 SGB    Backlog Item 5422: Insert new SoftCountImportFormat enum

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.CollectionImport
{
  public enum SoftCountImportFormat
  {
    FORMAT_CD3 = 1,  //Format type File to import
    FORMAT_CD8 = 2,
    FORMAT_CD11 = 3,
  }

  public enum SoftCountImportType
  {
    Cummins = 1,
  }

  public class ImportInfo
  {
    private String m_file_1;
    private String m_file_2;

    public String File1
    {
      get { return m_file_1; }
      set { m_file_1 = value; }
    }

    public String File2
    {
      get { return m_file_2; }
      set { m_file_2 = value; }
    }

    public SoftCountImportType FileType
    {
      get
      {
        Int32 _gp_import_format = 0;
        Int32 _gp_import_type = 0;

        try
        {
          _gp_import_format = GeneralParam.GetInt32("SoftCount", "Import.Format", 0);
          _gp_import_type = GeneralParam.GetInt32("SoftCount", "Format" + _gp_import_format.ToString("000") + ".Type", 0);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return (SoftCountImportType)_gp_import_type;
      }
    }
  }
}
