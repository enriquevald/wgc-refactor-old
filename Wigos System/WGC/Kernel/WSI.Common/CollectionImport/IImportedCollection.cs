//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ImportManager.cs
// 
//   DESCRIPTION: Interfaces and classes.
// 
//        AUTHOR: David Rigal
// 
// CREATION DATE: 11-MAY-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAY-2015 DRV    Initial Version

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.CollectionImport
{
  internal interface IImportCollectionFile
  {
    event ImportedReadEventHandler ImportedReadEvent;            //ImportedRead message received
    void Validate();                                   //Inits the reading process
    Boolean Import();                                     //Pauses reading
  }

  public interface IImported
  {
    ImportedType GetImportedType();

  }

  public class ImportedEvent : IImported
  {
    #region IImported Members
    private ImportEvent m_import_event;

    public ImportEvent ImportEvent
    {
      get { return m_import_event; }
      set { m_import_event = value; }
    }

    public ImportedType GetImportedType()
    {
      return ImportedType.Event;
    }

    #endregion
  }
  public class ImportedError : IImported
  {
    private Int64 m_line;
    private String m_error;

    public String Error
    {
      get { return m_error; }
      set { m_error = value; }
    }

    public Int64 Line
    {
      get { return m_line; }
      set { m_line = value; }
    }

    public ImportedType GetImportedType()
    {
      return ImportedType.Error;
    }

  }
}
