//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ImportManager.cs
// 
//   DESCRIPTION: Several Classes and enums to manage soft count import
// 
//        AUTHOR: David Rigal
// 
// CREATION DATE: 11-MAY-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAY-2015 DRV    Initial Version
// 20-OCT-2015 SGB    Backlog Item 5422: Insert new formats import.

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace WSI.Common.CollectionImport
{
  public class ImportManager
  {
    #region Fields
    public event ImportedReadEventHandler ImportedReceivedEvent;  //Notify events to GUI

    private ManualResetEvent ev_pending = new ManualResetEvent(false);     //Setted when there are pending events in the queue
    private ManualResetEvent ev_end_of_file = new ManualResetEvent(false); //Setted when file must be closed

    IImportCollectionFile m_import_file;                                   //File
    private WaitableQueue m_queue;                                         //Queue of events pending 

    private Thread m_thread;                                               //Dequeue events pending. 
    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Class constructor
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    //
    //   NOTES :
    //
    public ImportManager(ImportInfo _import_info)
    {
      Type _import_type;

      try
      {
        _import_type = Type.GetType("WSI.Common.CollectionImport." + _import_info.FileType);
        m_import_file = (IImportCollectionFile)Activator.CreateInstance(_import_type, _import_info);
        m_import_file.ImportedReadEvent += new ImportedReadEventHandler(NotifyEvent);
        m_queue = new WaitableQueue();
        m_thread = new Thread(ImportedThread);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Starts the import thread
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    //
    //   NOTES :
    //
    public void StartImporting()
    {
      m_thread.Start();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Stops the import thread
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    //
    //   NOTES :
    //
    public void StopImporting()
    {
      if (m_thread != null && m_thread.IsAlive)
      {
        m_thread.Abort();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : thread function that validates and imports the specified files
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    //
    //   NOTES :
    //
    public void ImportedThread()
    {
      try
      {
        m_import_file.Validate();
        m_import_file.Import();
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Throw events to the caller
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    //
    //   NOTES :
    //
    private void NotifyEvent(IImported IImported)
    {
      try
      {
        ImportedReadEventHandler _handler;
        if (ImportedReceivedEvent != null)
        {
          _handler = ImportedReceivedEvent;
          _handler(IImported);
        }
      }
      catch (ObjectDisposedException Ex)
      {
        Log.Error("Se cerr� la ventana de forma inesperada durante la inserci�n de elementos en UI: " + Ex.Message);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }
  }
}
