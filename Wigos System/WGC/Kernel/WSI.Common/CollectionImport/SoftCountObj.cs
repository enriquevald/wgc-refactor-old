//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SoftCountObj.cs
// 
//   DESCRIPTION:
// 
//        AUTHOR: Samuel Gonzalez
// 
// CREATION DATE: 16-JUN-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-JUN-2015 SGB    Initial Version
// 23-MAY-2018 AGS    Bug 32756:WIGOS-10750 [Ticket #13898] Contadora Cummins 4115 Compatible Con el Sistema

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;

namespace WSI.Common.CollectionImport
{

  public class SoftCount
  {

    #region " Members"

    public ImportId m_soft_sount_machine_id_type;
    public String m_soft_count_machine_id;
    public List<DenominationCount> m_bills_list;
    public List<DenominationCount> m_coins_list;
    public List<ImportedTicketNumber> m_tickets_list;
    public static List<Int64> m_dup_tickets;
    public Int64 m_target_terminal_id;
    public Int64 m_target_stacker_id;
    public String m_target_floor_id;
    public Int64 m_target_collection_id;
    [NonSerialized]
    private ValidatedTickets m_validated_tickets;
    [NonSerialized]
    private DataTable m_soft_count_details;
    private Decimal m_total_bills_amount;
    private Int64 m_total_bills_count;

    #endregion " Members"

    #region " Constructor"

    //------------------------------------------------------------------------------
    // TargetCollectionId : Class constructor
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    //
    //   NOTES :
    //
    public SoftCount()
    {
      m_bills_list = new List<DenominationCount>();
      m_coins_list = new List<DenominationCount>();
      m_tickets_list = new List<ImportedTicketNumber>();
      m_dup_tickets = new List<Int64>();
      m_total_bills_amount = 0;
      m_total_bills_count = 0;

    }
    #endregion " Constructor"

    #region " Propierties"

    [XmlIgnore]
    public DataTable SoftCountDetails
    {
      get
      {
        if (m_soft_count_details == null)
        {
          GetSoftCountDetailsDataTable();
        }
        return m_soft_count_details;
      }
      set { m_soft_count_details = value; }
    }

    public ValidatedTickets TicketList
    {
      get
      {
        if (m_validated_tickets == null)
        {
          ValidateTickets();
        }
        return m_validated_tickets;
      }
    }

    public Decimal TotalBillsAmount
    {
      get { return m_total_bills_amount; }
      set { m_total_bills_amount = value; }
    }

    public Int64 TotalBillsCount
    {
      get { return m_total_bills_count; }
      set { m_total_bills_count = value; }
    }

    #endregion " Propierties"

    #region " Public Functions"

    //------------------------------------------------------------------------------
    // PURPOSE : Return the Denomination count in the specified position of the list 
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS : DenominationCount
    //
    //   NOTES :
    //
    public DenominationCount GetBill(int Index)
    {
      try
      {
        return m_bills_list[Index];
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return new DenominationCount();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Search the Denomination count in the specified position of the list 
    //
    //  PARAMS :
    //      - INPUT : Denomination
    //      - OUTPUT : Denomination Count
    // RETURNS : Int, number of bills in the soft count
    //
    //   NOTES :
    //
    public Boolean GetBillByDenom(Decimal SearchDenom, out DenominationCount Denom)
    {
      Denom = new DenominationCount();

      foreach (DenominationCount _denom in m_bills_list)
      {
        if (_denom.Denomination == SearchDenom)
        {
          Denom = _denom;
          return true;
        }
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Search the Denomination count in the specified position of the list 
    //
    //  PARAMS :
    //      - INPUT : Denomination
    //      - OUTPUT : Denomination Count
    // RETURNS : Int, number of bills in the soft count
    //
    //   NOTES :
    //
    public Boolean GetTicketByValidationNumber(Int64 ValidationNumber, out ImportedTicketNumber Ticket)
    {
      Ticket = new ImportedTicketNumber();

      foreach (ImportedTicketNumber _ticket in m_tickets_list)
      {
        if (_ticket.Number == ValidationNumber)
        {
          Ticket = _ticket;
          return true;
        }
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Return the ticket in the specified position of the list 
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS : ImportedTicketNumber, the ticket in the specified index of the list
    //
    //   NOTES :
    //
    public ImportedTicketNumber GetTicket(int Index)
    {
      try
      {
        return m_tickets_list[Index];
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return new ImportedTicketNumber();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Adds a DenominationCount at the soft count 
    //
    //  PARAMS :
    //      - INPUT : Denomination Count
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    public void AddBillDenomination(DenominationCount Denom)
    {
      try
      {
        m_bills_list.Add(Denom);
        m_total_bills_amount += Denom.Count * Denom.Denomination;
        m_total_bills_count += Denom.Count;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Adds a ImportedTicketNumber at the soft count 
    //
    //  PARAMS :
    //      - INPUT : Denomination Count
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    public void AddTicket(ImportedTicketNumber Ticket)
    {
      try
      {
        m_tickets_list.Add(Ticket);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : 
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    public DataTable CreateSoftCountDetailsDatatable()
    {
      DataTable _soft_count_data_detail;

      _soft_count_data_detail = new DataTable();

      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_TYPE, Type.GetType("System.Int32"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_TICKET_NUMBER, Type.GetType("System.Decimal"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_TICKET_AMOUNT, Type.GetType("System.Decimal"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_TICKET_VALIDATION_TYPE, Type.GetType("System.Int16"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_TICKET_STATUS, Type.GetType("System.Int16"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_DENOMINATION, Type.GetType("System.String"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_COLLECTED_BILLS_QUANTITY, Type.GetType("System.Decimal"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_COLLECTED_BILLS_TOTAL, Type.GetType("System.Decimal"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_EXPECTED_BILLS_QUANTITY, Type.GetType("System.Decimal"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_EXPECTED_BILLS_TOTAL_AMOUNT, Type.GetType("System.Decimal"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_COLLECTION_ID, Type.GetType("System.Int64"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_STACKER_ID, Type.GetType("System.Int64"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_DENOMINATION_NUMERIC, Type.GetType("System.Decimal"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_TICKET_COLLECTED, Type.GetType("System.Int64"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_TICKET_ID, Type.GetType("System.Int64"));
      _soft_count_data_detail.Columns.Add(CollectionImport.SofCountData.SQL_COLUMN_CAGE_MOVEMENT_ID, Type.GetType("System.Int64"));

      return _soft_count_data_detail;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : 
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    public Boolean ValidateTickets()
    {
      StringBuilder _sb_validation_numbers;
      StringBuilder _sb;
      DataTable _dt_tickets;
      Int32 _counter;
      ImportedTicketNumber _ticket;

      _sb_validation_numbers = new StringBuilder();
      m_validated_tickets = new ValidatedTickets();
      _counter = 0;


      try
      {
        _sb = new StringBuilder();
        _dt_tickets = new DataTable();

        if (m_tickets_list.Count > 0)
        {
          _sb.AppendLine("    SELECT    TI_TICKET_ID         AS TICKET_ID            ");
          _sb.AppendLine("            , TI_VALIDATION_NUMBER AS TICKET_NUMBER        ");
          _sb.AppendLine("            , TI_AMOUNT            AS TICKET_AMOUNT        ");
          _sb.AppendLine("            , TI_TYPE_ID                                   ");
          _sb.AppendLine("      FROM    TICKETS                                      ");
          _sb.AppendLine("     WHERE    TI_VALIDATION_NUMBER IN(                     ");

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              foreach (ImportedTicketNumber _imp_ticket in m_tickets_list)
              {
                if (_counter > 0)
                {
                  _sb.Append(", ");
                }
                _sb.Append(String.Format("@pTicketNumber{0}", _counter));
                _sql_cmd.Parameters.Add(String.Format("@pTicketNumber{0}", _counter), SqlDbType.BigInt).Value = _imp_ticket.Number;

                _counter++;
              }
              _sb.AppendLine(" ) ORDER BY TI_VALIDATION_NUMBER ASC, ISNULL(TI_COLLECTED_MONEY_COLLECTION, -1) ASC, TI_TICKET_ID ASC");
              _sql_cmd.CommandText = _sb.ToString();

              using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
              {
                _da.Fill(_dt_tickets);
              }
            }
          }

          foreach (DataRow _dr in _dt_tickets.Rows)
          {
            if (!m_dup_tickets.Contains((Int64)_dr[CollectionImport.SofCountData.SQL_COLUMN_TICKET_NUMBER]))
            {
              _ticket = new ImportedTicketNumber();
              _ticket.TicketId = (Int64)_dr[CollectionImport.SofCountData.SQL_COLUMN_TICKET_ID];
              _ticket.Number = (Int64)_dr[CollectionImport.SofCountData.SQL_COLUMN_TICKET_NUMBER];
              _ticket.Amount = (Decimal)_dr[CollectionImport.SofCountData.SQL_COLUMN_TICKET_AMOUNT];
              _ticket.TicketType = (TITO_TICKET_TYPE)_dr["TI_TYPE_ID"];
              m_dup_tickets.Add(_ticket.Number);
              m_validated_tickets.AddTicket(_ticket);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion " Public Functions"

    #region " Private Functions"

    //------------------------------------------------------------------------------
    // PURPOSE : 
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES :
    //
    private void GetSoftCountDetailsDataTable()
    {
      DataRow _dr_softcount;
      ImportedTicketNumber _tickets_item;

      m_soft_count_details = CreateSoftCountDetailsDatatable();

      foreach (DenominationCount _bills_item in m_bills_list)
      {
        //Add Row to the Datatable
        _dr_softcount = m_soft_count_details.NewRow();

        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_TYPE] = TYPE_ROW_TICKET_BILL.BILL;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_DENOMINATION] = _bills_item.Denomination.ToString();
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_COLLECTED_BILLS_QUANTITY] = _bills_item.Count;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_COLLECTED_BILLS_TOTAL] = _bills_item.Denomination * _bills_item.Count;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_STACKER_ID] = m_target_stacker_id;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_COLLECTION_ID] = m_target_collection_id;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_DENOMINATION_NUMERIC] = _bills_item.Denomination;

        m_soft_count_details.Rows.Add(_dr_softcount);
      }


      // Read SoftCount Tickets
      for (Int32 _idx = 0; _idx < TicketList.Count; _idx++)
      {
        _tickets_item = TicketList.GetTicket(_idx);

        //Add Row to the Datatable
        _dr_softcount = m_soft_count_details.NewRow();
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_TYPE] = TYPE_ROW_TICKET_BILL.TICKET;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_TICKET_ID] = _tickets_item.TicketId;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_TICKET_NUMBER] = _tickets_item.Number;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_TICKET_AMOUNT] = _tickets_item.Amount;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_STACKER_ID] = m_target_stacker_id;
        _dr_softcount[CollectionImport.SofCountData.SQL_COLUMN_COLLECTION_ID] = m_target_collection_id;

        m_soft_count_details.Rows.Add(_dr_softcount);
      }
    }
    #endregion " Private Functions"

  }

  public class DenominationCount
  {
    public Decimal Denomination;
    public Int64 Count;

    public DenominationCount()
    {
      this.Denomination = 0;
      this.Count = 0;
    }
  }

  [Serializable]
  public class ValidatedTickets
  {

    #region " Members"

    public Decimal m_total_tickets_amount;
    public Decimal m_total_tickets_count;
    public Decimal m_total_ticket_re_amount;
    public Decimal m_total_ticket_re_count;
    public Decimal m_total_ticket_promo_re_amount;
    public Decimal m_total_ticket_promo_re_count;
    public Decimal m_total_ticket_promo_nr_amount;
    public Decimal m_total_ticket_promo_nr_count;
    private List<ImportedTicketNumber> m_ticket_list;

    #endregion " Members"

    #region " Constructor"

    public ValidatedTickets()
    {
      this.m_total_ticket_promo_nr_amount = 0;
      this.m_total_ticket_promo_nr_count = 0;
      this.m_total_ticket_promo_re_amount = 0;
      this.m_total_ticket_promo_re_count = 0;
      this.m_total_ticket_re_amount = 0;
      this.m_total_ticket_re_count = 0;
      this.m_total_tickets_amount = 0;
      this.m_total_tickets_count = 0;
      m_ticket_list = new List<ImportedTicketNumber>();
    }

    #endregion " Constructor"

    #region " Properties"

    public Int64 Count
    {
      get
      {
        return m_ticket_list.Count;
      }
    }
    #endregion "Propierties"

    #region " Public Functions"

    public ImportedTicketNumber GetTicket(Int32 ListIdx)
    {
      return m_ticket_list[ListIdx];
    }

    public void AddTicket(ImportedTicketNumber Ticket)
    {
      m_ticket_list.Add(Ticket);
      UpdateTotals(Ticket, true);
    }

    public void RemoveTicket(ImportedTicketNumber Ticket)
    {
      m_ticket_list.Remove(Ticket);
      UpdateTotals(Ticket, false);
    }
    #endregion "Public Functions"

    #region " Private Functions"

    private void UpdateTotals(ImportedTicketNumber Ticket, Boolean AddTicket)
    {
      Int32 _count;

      _count = AddTicket ? 1 : -1;

      m_total_tickets_amount += Ticket.Amount * _count;
      m_total_tickets_count += _count;

      switch (Ticket.TicketType)
      {
        case TITO_TICKET_TYPE.CASHABLE:
          m_total_ticket_re_amount += Ticket.Amount * _count;
          m_total_ticket_re_count += _count;
          break;
        case TITO_TICKET_TYPE.PROMO_NONREDEEM:
          m_total_ticket_promo_nr_amount += Ticket.Amount * _count;
          m_total_ticket_promo_nr_count += _count;
          break;
        case TITO_TICKET_TYPE.PROMO_REDEEM:
          m_total_ticket_promo_re_amount += Ticket.Amount * _count;
          m_total_ticket_promo_re_count += _count;
          break;
      }
    }

    #endregion " Private Functions"

  }

  public class ImportedTicketNumber
  {
    public Int64 TicketId;
    public Int64 Number;
    public Decimal Amount;
    public TITO_TICKET_TYPE TicketType;

    public ImportedTicketNumber()
    {
      this.TicketId = 0;
      this.Number = 0;
      this.Amount = 0;
    }
  }
}
