//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FileCollection.cs
// 
//   DESCRIPTION: Class to manager file collection
// 
//        AUTHOR: Samuel González
// 
// CREATION DATE: 07-MAY-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-MAY-2015 SGB   Initial version. Item 1314, task 1353
// 30-JUN-2015 FJC   Fixed Bug WIG-2499 (Change Stacker (Edit) ).
// 02-JUL-2015 FJC   Fixed Bug WIG-2526.
// 06-JUL-2015 FJC   Fixed Bug WIG-2538.
// 08-JUL-2015 FJC   Fixed Bug WIG-2556.
// 20-JUL-2015 FJC   Fixed Bug WIG-2511.
// 12-SEP-2016 JCA   Bug 17575:Recaudación de fichero: no se puede recaudar
// 24-JAN-2017 CCG   Bug 22703:Problemas al recaudar cuando se clona un terminal

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using WSI.Common;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common.CollectionImport
{
  #region " Enums "

  public enum ImportId
  {
    StackerId = 0,
    EGMBaseName = 1,
    EGMFloorId = 2
  }

  public enum TypeImportedFiles
  {
    SoftCount = 1
  }

  public enum TypeSoftCounts
  {
    ImportedFiles = 1
  }

  public enum SoftCountStatus
  {
    PendingCollection = 1,
    Collected = 2,
    Deleted = 3
  }

  #endregion " Enums"

  public class ImportedSoftCounts : IImported
  {
    public String m_file1_name;
    public Byte[] m_file1_binary;
    public String m_file2_name;
    public Byte[] m_file2_binary;
    public String m_name;
    public DateTime m_collection_datetime;
    public Int32 m_collection_user_id;
    public DateTime m_import_datetime;
    public Int32 m_import_user_id;
    public DateTime m_cancel_datetime;
    public Int32 m_cancel_user_id;
    public DateTime m_delete_datetime;
    public Int32 m_delete_user_id;
    public SoftCountStatus m_status;
    public DateTime m_working_day;
    public Int64 m_cage_session_id;
    public Int64 m_imported_soft_count_id;
    public Decimal m_total_bills;
    public Decimal m_total_tickets;

    private String m_xml_soft_count;
    private String m_xml_money_collections;
    private String m_xml_warning_errors;

    public Dictionary<String, SoftCount> m_soft_count;
    public Dictionary<Int32, DataTable> m_merged_details;
    public List<Int64> m_money_collection;
    public List<String> m_warning_errors;

    //------------------------------------------------------------------------------
    // PURPOSE : Class constructor
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    //
    //   NOTES :
    //
    public ImportedSoftCounts()
    {
      m_soft_count = new Dictionary<String, SoftCount>();
      m_merged_details = new Dictionary<int, DataTable>();
      m_money_collection = new List<Int64>();
      m_warning_errors = new List<string>();
      m_status = SoftCountStatus.PendingCollection;
    }

    public ImportedSoftCounts(Int64 SoftCountId)
    {
      m_imported_soft_count_id = SoftCountId;

      using (DB_TRX _trx = new DB_TRX())
      {
        LoadSoftCount(SoftCountId, _trx.SqlTransaction);
      }
    }
    #region PUBLIC METHODS

    //------------------------------------------------------------------------------
    // PURPOSE : Returns de IImported type
    //
    //  PARAMS :
    //      - OUTPUT : ImportedType enum
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public ImportedType GetImportedType()
    {
      return ImportedType.SoftCount;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Adds a Soft Count
    //
    //  PARAMS :
    //      - INPUT :
    //              - SoftCount
    //              - Id (Stacker, TerminalId or floorId) defined in GP
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if the operation goes well
    //
    //   NOTES :
    //
    public Boolean AddOrUpdateSoftCount(SoftCount Imported, String Id)
    {
      SoftCount _soft_count;
      try
      {
        if (m_soft_count == null)
        {
          m_soft_count = new Dictionary<string, SoftCount>();
        }

        if (m_soft_count.TryGetValue(Id, out _soft_count))
        {
          m_total_bills -= _soft_count.TotalBillsAmount;
          m_total_tickets -= _soft_count.TicketList.m_total_tickets_amount;
          _soft_count = Imported;
          m_total_bills += Imported.TotalBillsAmount;
          m_total_tickets += Imported.TicketList.m_total_tickets_amount;
        }
        else
        {
          m_soft_count.Add(Id, Imported);
          m_total_bills += Imported.TotalBillsAmount;
          m_total_tickets += Imported.TicketList.m_total_tickets_amount;
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    public Boolean AddSoftCount(SoftCount Imported, String Id)
    {
      SoftCount _soft_count;
      try
      {
        if (m_soft_count == null)
        {
          m_soft_count = new Dictionary<string, SoftCount>();
        }

        if (!m_soft_count.TryGetValue(Id, out _soft_count))
        {
          m_soft_count.Add(Id, Imported);
          m_total_bills += Imported.TotalBillsAmount;
          m_total_tickets += Imported.TicketList.m_total_tickets_amount;
        }
        else
        {
          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Return a specified Soft Count
    //
    //  PARAMS :
    //      - INPUT :
    //              - Id (Stacker, TerminalId or floorId) defined in GP
    //
    //      - OUTPUT :
    //              - SoftCount
    //
    // RETURNS :
    //      - True if the operation goes well
    //
    //   NOTES :
    //
    public Boolean GetSoftCount(String Id, out SoftCount Imported)
    {
      return m_soft_count.TryGetValue(Id, out Imported);

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets Soft count id list
    //
    //  PARAMS :
    //      - INPUT : NONE
    //
    //      - OUTPUT : NONE
    //
    // RETURNS :
    //      - A list whith all soft counts ids
    //
    //   NOTES :
    //
    public List<String> GetSoftCountIds()
    {
      List<String> _ids;

      _ids = new List<String>(m_soft_count.Keys);

      return _ids;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Prepare and insert all imported data
    //
    //  PARAMS :
    //      - INPUT :
    //              - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if the operation goes well
    //
    //   NOTES :
    //
    public Boolean SaveImported(SqlTransaction Trx, out string Xml)
    {
      Xml = String.Empty;
      try
      {

        this.m_xml_soft_count = this.SerializationSoftCount(this.m_soft_count);
        this.m_xml_money_collections = this.Serialize(this.m_money_collection);
        this.m_xml_warning_errors = this.Serialize(this.m_warning_errors);

        Xml = this.m_xml_soft_count;

        if (m_imported_soft_count_id != 0)
        {
          return UpdateSoftCount(Trx);
        }
        else
        {
          return InsertNewSoftCount(Trx, out m_imported_soft_count_id);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SaveImported

    public Boolean SetCollectedStatus(SqlTransaction Trx, Int32 UserId, Int64 CageSession, List<Int64> MoneyCollections)
    {
      m_money_collection = MoneyCollections;
      m_collection_user_id = UserId;
      m_cage_session_id = CageSession;
      m_status = SoftCountStatus.Collected;

      return SaveImported(Trx, out m_xml_soft_count);
    }

    #endregion

    #region PRIVATE METHODS

    //------------------------------------------------------------------------------
    // PURPOSE : Insert imported files an importation data
    //
    //  PARAMS :
    //      - INPUT :
    //              - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if the operation goes well
    //
    //   NOTES :
    //
    private Boolean InsertNewSoftCount(SqlTransaction Trx, out Int64 SoftCountID)
    {
      StringBuilder _sb;
      SqlParameter _param;
      Int32 _num_querys;

      SoftCountID = 0;
      _num_querys = 0;
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("DECLARE @pIdFile1 BIGINT;             ");
        _sb.AppendLine("DECLARE @pIdFile2 BIGINT;             ");
        _sb.AppendLine("                                      ");
        _sb.AppendLine("SET @pIdFile1 = NULL                  ");
        _sb.AppendLine("SET @pIdFile2 = NULL                  ");
        _sb.AppendLine("                                      ");
        if (!String.IsNullOrEmpty(this.m_file1_name))
        {
          _num_querys++;
          _sb.AppendLine("INSERT INTO  IMPORTED_FILES           ");
          _sb.AppendLine("   			    (IF_NAME                  ");
          _sb.AppendLine("   			   , IF_TYPE                  ");
          _sb.AppendLine("   			   , IF_IMPORT_DATETIME       ");
          _sb.AppendLine("   			   , IF_FILE_CONTENT)         ");
          _sb.AppendLine("   	 VALUES                           ");
          _sb.AppendLine("   			    (@pNameFile1              ");
          _sb.AppendLine("   			   , @pTypeFile1              ");
          _sb.AppendLine("   			   , GETDATE()                ");
          _sb.AppendLine("   			   , @pFile1)                 ");
          _sb.AppendLine("   	   SET @pIdFile1 = scope_identity()");

        }
        if (!String.IsNullOrEmpty(this.m_file2_name))
        {
          _num_querys++;
          _sb.AppendLine("INSERT INTO  IMPORTED_FILES           ");
          _sb.AppendLine("   			    (IF_NAME                  ");
          _sb.AppendLine("   			   , IF_TYPE                  ");
          _sb.AppendLine("   			   , IF_IMPORT_DATETIME       ");
          _sb.AppendLine("   			   , IF_FILE_CONTENT)         ");
          _sb.AppendLine("     VALUES                           ");
          _sb.AppendLine("   			    (@pNameFile2              ");
          _sb.AppendLine("   			   , @pTypeFile2              ");
          _sb.AppendLine("   			   , GETDATE()                ");
          _sb.AppendLine("   			   , @pFile2)                 ");
          _sb.AppendLine("   		 SET @pIdFile2 = scope_identity()");
        }
        _num_querys++;
        _sb.AppendLine("                                      ");
        _sb.AppendLine("INSERT INTO SOFT_COUNTS               ");
        _sb.AppendLine("           (SC_TYPE                   ");
        _sb.AppendLine("           ,SC_NAME                   ");
        _sb.AppendLine("           ,SC_IMPORT_DATETIME        ");
        _sb.AppendLine("           ,SC_IMPORT_USER_ID         ");
        _sb.AppendLine("           ,SC_STATUS                 ");
        _sb.AppendLine("           ,SC_FILE1_ID               ");
        _sb.AppendLine("           ,SC_FILE2_ID               ");
        _sb.AppendLine("           ,SC_WORKING_DAY            ");
        _sb.AppendLine("           ,SC_DATA                   ");
        _sb.AppendLine("           ,SC_MONEY_COLLECTIONS      ");
        _sb.AppendLine("           ,SC_WARNINGS_ERRORS)       ");
        _sb.AppendLine("     VALUES                           ");
        _sb.AppendLine("           (                          ");
        _sb.AppendLine("             @pType                   ");
        _sb.AppendLine("            ,@pName                   ");
        _sb.AppendLine("            ,@pImportDate             ");
        _sb.AppendLine("            ,@pImportUserId           ");
        _sb.AppendLine("            ,@pStatus                 ");
        _sb.AppendLine("            ,@pIdFile1                ");
        _sb.AppendLine("            ,@pIdFile2                ");
        _sb.AppendLine("            ,@pWorkingDay             ");
        _sb.AppendLine("            ,@pXmlData                ");
        _sb.AppendLine("            ,@pXmlMoneyCollections    ");
        _sb.AppendLine("            ,@pXmlWarning             ");
        _sb.AppendLine("           )                          ");
        _sb.AppendLine("         SET   @pSoftCountId = SCOPE_IDENTITY()   ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          if (!String.IsNullOrEmpty(this.m_file1_name))
          {
            _sql_cmd.Parameters.Add("@pNameFile1", SqlDbType.VarChar).Value = this.m_file1_name;
            _sql_cmd.Parameters.Add("@pTypeFile1", SqlDbType.Int).Value = TypeImportedFiles.SoftCount;
            _sql_cmd.Parameters.Add("@pFile1", SqlDbType.VarBinary).Value = this.m_file1_binary;
          }
          if (!String.IsNullOrEmpty(this.m_file2_name))
          {
            _sql_cmd.Parameters.Add("@pNameFile2", SqlDbType.VarChar).Value = this.m_file2_name;
            _sql_cmd.Parameters.Add("@pTypeFile2", SqlDbType.Int).Value = TypeImportedFiles.SoftCount;
            _sql_cmd.Parameters.Add("@pFile2", SqlDbType.VarBinary).Value = this.m_file2_binary;
          }
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = TypeSoftCounts.ImportedFiles;
          _sql_cmd.Parameters.Add("@pName", SqlDbType.VarChar).Value = this.m_name;
          _sql_cmd.Parameters.Add("@pImportUserId", SqlDbType.Int).Value = this.m_import_user_id;
          _sql_cmd.Parameters.Add("@pImportDate", SqlDbType.DateTime).Value = this.m_import_datetime;
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = this.m_status;
          _sql_cmd.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = this.m_working_day;
          _sql_cmd.Parameters.Add("@pXmlData", SqlDbType.Xml).Value = this.m_xml_soft_count;
          _sql_cmd.Parameters.Add("@pXmlMoneyCollections", SqlDbType.Xml).Value = this.m_xml_money_collections;
          _sql_cmd.Parameters.Add("@pXmlWarning", SqlDbType.Xml).Value = this.m_xml_warning_errors;

          _param = _sql_cmd.Parameters.Add("@pSoftCountId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_sql_cmd.ExecuteNonQuery() == _num_querys)
          {
            SoftCountID = Convert.ToInt64(_param.Value);
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    private Boolean UpdateSoftCount(SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   SOFT_COUNTS                                  ");
        _sb.AppendLine("   SET   SC_TYPE = @pType                             ");
        _sb.AppendLine("       , SC_NAME = @pName                             ");
        _sb.AppendLine("       , SC_STATUS = @pStatus                         ");
        _sb.AppendLine("       , SC_WORKING_DAY = @pWorkingDay                ");
        _sb.AppendLine("       , SC_DATA = @pXmlData                          ");
        _sb.AppendLine("       , SC_MONEY_COLLECTIONS = @pXmlMoneyCollections ");
        _sb.AppendLine("       , SC_WARNINGS_ERRORS = @pXmlWarning            ");
        _sb.AppendLine("       , SC_COLLECTION_DATETIME = GETDATE()           ");
        _sb.AppendLine("       , SC_COLLECTION_USER_ID = @pUserId             ");
        _sb.AppendLine("       , SC_CAGE_SESSION_ID = @pCageSession           ");
        _sb.AppendLine("       , SC_TOTAL_BILLS = @pTotalBills                ");
        _sb.AppendLine("       , SC_TOTAL_TICKETS = @pTotalTickets            ");
        _sb.AppendLine(" WHERE   SC_ID = @pSoftCountId                        ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = TypeSoftCounts.ImportedFiles;
          _sql_cmd.Parameters.Add("@pName", SqlDbType.VarChar).Value = this.m_name;
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = this.m_status;
          _sql_cmd.Parameters.Add("@pWorkingDay", SqlDbType.DateTime).Value = this.m_working_day;
          _sql_cmd.Parameters.Add("@pXmlData", SqlDbType.Xml).Value = this.m_xml_soft_count;
          _sql_cmd.Parameters.Add("@pXmlMoneyCollections", SqlDbType.Xml).Value = this.m_xml_money_collections;
          _sql_cmd.Parameters.Add("@pXmlWarning", SqlDbType.Xml).Value = this.m_xml_warning_errors;
          _sql_cmd.Parameters.Add("@pSoftCountId", SqlDbType.BigInt).Value = this.m_imported_soft_count_id;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = this.m_collection_user_id;
          _sql_cmd.Parameters.Add("@pCageSession", SqlDbType.BigInt).Value = this.m_cage_session_id;
          _sql_cmd.Parameters.Add("@pTotalBills", SqlDbType.Decimal).Value = this.m_total_bills;
          _sql_cmd.Parameters.Add("@pTotalTickets", SqlDbType.Decimal).Value = this.m_total_tickets;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }
    #endregion

    #region SERIALIZATION METHODS

    //------------------------------------------------------------------------------
    // PURPOSE : Serializes dictionary data
    //
    //  PARAMS :
    //      - INPUT :
    //              - SoftcountDictionary
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if the operation goes well
    //
    //   NOTES :
    //
    public String SerializationSoftCount(Dictionary<String, SoftCount> softCount)
    {

      try
      {
        List<SoftCount> _valueListSoftCount = new List<SoftCount>(softCount.Values);

        return Serialize(_valueListSoftCount);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return null;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Serializes the specified object
    //
    //  PARAMS :
    //      - INPUT :
    //              - Object
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Sring: Object Xml
    //
    //   NOTES :
    //
    public string Serialize(Object _value)
    {
      XmlSerializer _serializer = new XmlSerializer(_value.GetType());
      try
      {
        using (StringWriter _writer = new StringWriter())
        {
          _serializer.Serialize(_writer, _value);

          return _writer.ToString();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return null;
      }
    }

    public Boolean LoadSoftCount(Int64 SoftCountId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   SC_NAME                ");
        _sb.AppendLine("        , SC_IMPORT_DATETIME     ");
        _sb.AppendLine("        , SC_IMPORT_USER_ID      ");
        _sb.AppendLine("        , SC_COLLECTION_DATETIME ");
        _sb.AppendLine("        , SC_COLLECTION_USER_ID  ");
        _sb.AppendLine("        , SC_TOTAL_BILLS         ");
        _sb.AppendLine("        , SC_TOTAL_TICKETS       ");
        _sb.AppendLine("        , SC_DELETE_DATETIME     ");
        _sb.AppendLine("        , SC_DELETE_USER_ID      ");
        _sb.AppendLine("        , SC_STATUS              ");
        _sb.AppendLine("        , SC_WORKING_DAY         ");
        _sb.AppendLine("        , SC_CAGE_SESSION_ID     ");
        _sb.AppendLine("        , SC_DATA                ");
        _sb.AppendLine("        , SC_MONEY_COLLECTIONS   ");
        _sb.AppendLine("        , SC_WARNINGS_ERRORS     ");
        _sb.AppendLine("   FROM   SOFT_COUNTS            ");
        _sb.AppendLine("  WHERE   SC_ID = @pSoftCountId  ");


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {

          _sql_cmd.Parameters.Add("@pSoftCountId", SqlDbType.BigInt).Value = this.m_imported_soft_count_id;

          using (SqlDataReader _sql_read = _sql_cmd.ExecuteReader())
          {
            if (!_sql_read.Read())
            {
              return false;
            }

            this.m_name = _sql_read.IsDBNull(0) ? "" : _sql_read.GetString(0);
            this.m_import_datetime = _sql_read.IsDBNull(1) ? DateTime.MinValue : _sql_read.GetDateTime(1);
            this.m_import_user_id = _sql_read.IsDBNull(2) ? 0 : _sql_read.GetInt32(2);
            this.m_collection_datetime = _sql_read.IsDBNull(3) ? DateTime.MinValue : _sql_read.GetDateTime(3);
            this.m_collection_user_id = _sql_read.IsDBNull(4) ? 0 : _sql_read.GetInt32(4);
            this.m_total_bills = _sql_read.IsDBNull(5) ? 0 : _sql_read.GetDecimal(5);
            this.m_total_tickets = _sql_read.IsDBNull(6) ? 0 : _sql_read.GetDecimal(6);
            this.m_delete_datetime = _sql_read.IsDBNull(7) ? DateTime.MinValue : _sql_read.GetDateTime(7);
            this.m_delete_user_id = _sql_read.IsDBNull(8) ? 0 : _sql_read.GetInt32(8);
            this.m_status = _sql_read.IsDBNull(9) ? SoftCountStatus.PendingCollection : (SoftCountStatus)_sql_read.GetInt32(9);
            this.m_working_day = _sql_read.IsDBNull(10) ? DateTime.MinValue : _sql_read.GetDateTime(10);
            this.m_cage_session_id = _sql_read.IsDBNull(11) ? 0 : _sql_read.GetInt64(11);
            this.m_xml_soft_count = _sql_read.IsDBNull(12) ? "" : _sql_read.GetString(12).ToString();
            this.m_xml_money_collections = _sql_read.IsDBNull(13) ? "" : _sql_read.GetString(13).ToString();
            this.m_xml_warning_errors = _sql_read.IsDBNull(14) ? "" : _sql_read.GetString(14).ToString();

            Deserialization(SoftCountId);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Deserializes the imported soft counts xml
    //
    //  PARAMS :
    //      - INPUT :
    //              - XmlSoftCount: String with serialized data
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if the operation goes well
    //
    //   NOTES :
    //
    public Boolean Deserialization(Int64 SoftCountId)
    {
      System.IO.StringReader _stringReader;
      XmlSerializer _serializer;
      List<SoftCount> _valueListSoftCount;

      try
      {
        _valueListSoftCount = new List<SoftCount>();

        _stringReader = new System.IO.StringReader(m_xml_soft_count);
        _serializer = new XmlSerializer(_valueListSoftCount.GetType());
        _valueListSoftCount = _serializer.Deserialize(_stringReader) as List<SoftCount>;

        foreach (SoftCount _sofcount in _valueListSoftCount)
        {
          AddOrUpdateSoftCount(_sofcount, _sofcount.m_soft_count_machine_id);
        }
        m_imported_soft_count_id = SoftCountId;


        m_money_collection = new List<Int64>();
        _stringReader = new System.IO.StringReader(m_xml_money_collections);
        _serializer = new XmlSerializer(m_money_collection.GetType());
        m_money_collection = _serializer.Deserialize(_stringReader) as List<Int64>;


        m_warning_errors = new List<string>();
        _stringReader = new System.IO.StringReader(m_xml_warning_errors);
        _serializer = new XmlSerializer(m_warning_errors.GetType());
        m_warning_errors = _serializer.Deserialize(_stringReader) as List<string>;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    #endregion

  }  // ImportedSoftCounts

  public partial class SofCountData
  {

    #region " Constants "

    public const String SQL_COLUMN_COLLECTION_ID = "COLLECTION_ID";
    public const String SQL_COLUMN_STACKER_ID = "STACKER_ID";
    public const String SQL_COLUMN_TERMINAL_ID = "TERMINAL_ID";
    public const String SQL_COLUMN_FLOOR_ID = "FLOOR_ID";
    public const String SQL_COLUMN_COLLECTED_BILLS = "COLLECTED_BILLS";
    public const String SQL_COLUMN_COLLECTED_TICKETS = "COLLECTED_TICKETS";
    public const String SQL_COLUMN_EXPECTED_BILLS = "EXPECTED_BILLS";
    public const String SQL_COLUMN_EXPECTED_TICKETS = "EXPECTED_TICKETS";
    public const String SQL_COLUMN_NON_IMPORT = "NON_IMPORT";
    public const String SQL_COLUMN_CHECKED = "CHECKED";
    public const String SQL_COLUMN_IMPORT_INDEX = "IMPORT_INDEX";
    public const String SQL_COLUMN_TYPE = "TYPE";
    public const String SQL_COLUMN_BASE_NAME = "BASE_NAME";
    public const String SQL_COLUMN_DENOMINATION = "DENOMINATION";
    public const String SQL_COLUMN_DENOMINATION_NUMERIC = "DENOMINATION_NUMERIC";
    public const String SQL_COLUMN_COLLECTED_BILLS_QUANTITY = "COLLECTED_BILLS_DENOMINATION_QUANTITY";
    public const String SQL_COLUMN_COLLECTED_BILLS_TOTAL = "COLLECTED_BILLS_TOTAL_AMOUNT";
    public const String SQL_COLUMN_EXPECTED_BILLS_QUANTITY = "EXPECTED_BILLS_DENOMINATION_QUANTITY";
    public const String SQL_COLUMN_TICKET_NUMBER = "TICKET_NUMBER";
    public const String SQL_COLUMN_TICKET_COLLECTED = "TICKET_COLLECTED";
    public const String SQL_COLUMN_TICKET_STATUS = "TICKET_STATUS";
    public const String SQL_COLUMN_TICKET_VALIDATION_TYPE = "TICKET_VALIDATION_TYPE";
    public const String SQL_COLUMN_EXPECTED_BILL_COUNT = "EXPECTED_BILL_COUNT";
    public const String SQL_COLUMN_EXPECTED_TICKET_COUNT = "EXPECTED_TICKET_COUNT";
    public const String SQL_COLUMN_EXPECTED_RE_TICKET_AMOUNT = "EXPECTED_RE_TICKET_AMOUNT";
    public const String SQL_COLUMN_EXPECTED_RE_TICKET_COUNT = "EXPECTED_RE_TICKET_COUNT";
    public const String SQL_COLUMN_EXPECTED_PROMO_RE_TICKET_AMOUNT = "EXPECTED_PROMO_RE_TICKET_AMOUNT";
    public const String SQL_COLUMN_EXPECTED_PROMO_RE_TICKET_COUNT = "EXPECTED_PROMO_RE_TICKET_COUNT";
    public const String SQL_COLUMN_EXPECTED_PROMO_NR_TICKET_AMOUNT = "EXPECTED_PROMO_NR_TICKET_AMOUNT";
    public const String SQL_COLUMN_EXPECTED_PROMO_NR_TICKET_COUNT = "EXPECTED_PROMO_NR_TICKET_COUNT";
    public const String SQL_COLUMN_COLLECTED_BILL_AMOUNT = "COLLECTED_BILL_AMOUNT";
    public const String SQL_COLUMN_COLLECTED_BILL_COUNT = "COLLECTED_BILL_COUNT";
    public const String SQL_COLUMN_COLLECTED_TICKET_AMOUNT = "COLLECTED_TICKET_AMOUNT";
    public const String SQL_COLUMN_COLLECTED_TICKET_COUNT = "COLLECTED_TICKET_COUNT";
    public const String SQL_COLUMN_COLLECTED_RE_TICKET_AMOUNT = "COLLECTED_RE_TICKET_AMOUNT";
    public const String SQL_COLUMN_COLLECTED_RE_TICKET_COUNT = "COLLECTED_RE_TICKET_COUNT";
    public const String SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_AMOUNT = "COLLECTED_PROMO_RE_TICKET_AMOUNT";
    public const String SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_COUNT = "COLLECTED_PROMO_RE_TICKET_COUNT";
    public const String SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_AMOUNT = "COLLECTED_PROMO_NR_TICKET_AMOUNT";
    public const String SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_COUNT = "COLLECTED_PROMO_NR_TICKET_COUNT";
    public const String SQL_COLUMN_ISO_CODE = "ISO_CODE";
    public const String SQL_COLUMN_QUANTITY = "QUANTITY";
    public const String SQL_COLUMN_TICKET_ID = "TICKET_ID";
    public const String SQL_COLUMN_TICKET_AMOUNT = "TICKET_AMOUNT";
    public const String SQL_COLUMN_EXPECTED_BILLS_TOTAL_AMOUNT = "EXPECTED_BILLS_TOTAL_AMOUNT";
    public const String SQL_COLUMN_CAGE_MOVEMENT_ID = "CAGE_MOVEMENT_ID";
    public const String SQL_COLUMN_COLLECTION_STATUS = "COLLECTION_STATUS";


    #endregion " Constants "

    #region " Structs"

    public struct SOFT_COUNT_DATA_DETAIL
    {
      public Int64 StackerId;
      public Int32 TerminalId;
      public Int64 CollectionId;
      public Int32 ImportIndex;
    } // SOFT_COUNT_DATA_DETAIL

    public struct SOFT_COUNT_PARAMETERS
    {
      public COLLECTION_MODE CollectionMode;
      public String Xml;
      public DateTime SessionDay;
      public Int64 CageSessionId;
      public String CageSessionName;
      public Int64 SoftCountId;
      public String SoftCountName;
    } // SOFT_COUNT_PARAMETERS

    #endregion " Structs"

    #region " Members"

    private ImportedSoftCounts m_imported_soft_count_obj;   // Contains all XML in a object
    private DataTable m_soft_counts_detail;                 // Contains Tables CHASHIER_TERMINAL_MONEY && TICKETS IN MEMORY
    private DataTable m_soft_counts_terminals;              // Contains Table Terminals IN MEMORY
    private DataTable m_soft_counts_money_collection;       // Contains Table MOneyCollection (only pending)
    private ImportId m_machine_id_type;                     // Contains Mode: (By Stacker Id, by floorid, by basename)
    private DataTable m_merged_collection_soft_count;       // Contains Struc with DataTable (Grid Central info) and NumCollected and NumExpected
    private String m_soft_counts_xml;                       // Contains Xml with file import (xml)
    private COLLECTION_MODE m_mode_collected;               // Contains Mode ()
    private Int64 m_soft_count_id;
    private List<Int64> m_money_collection_central;

    #endregion " Members"

    #region " Properties"

    public DataTable MergedCollectionSoftCount
    {
      get
      {
        if (m_merged_collection_soft_count == null)
        {
          if (!GetSoftCountCollections())
          {

            Log.Message("An error occurred in constructor SofCountData (GetSoftCountCollections).");
          }

          switch (this.CollectionMode)
          {
            case COLLECTION_MODE.MODE_CANCEL:
              // FILL DataTable from (CASHIER_TERMINAL_MONEY && TICKETS) once
              if (!FillSoftCountMoneyCollectionDetail(m_imported_soft_count_obj.m_money_collection))
              {

                Log.Message("An error occurred in constructor SofCountData (GetSoftCountMoneyCollectionDetail).");
              }
              break;
            case COLLECTION_MODE.MODE_COLLECT:
              // FILL DataTable from (CASHIER_TERMINAL_MONEY && TICKETS) once
              if (!FillSoftCountMoneyCollectionDetail(this.m_money_collection_central))
              {

                Log.Message("An error occurred in constructor SofCountData (GetSoftCountMoneyCollectionDetail).");
              }
              break;
          }
        }

        return m_merged_collection_soft_count;
      }
      set { m_merged_collection_soft_count = value; }
    } // MergedCollectionSoftCount

    public ImportedSoftCounts SoftCountObject
    {
      get { return m_imported_soft_count_obj; }
      set { m_imported_soft_count_obj = value; }
    } // SoftCountObject

    public COLLECTION_MODE CollectionMode
    {
      get
      {
        return m_mode_collected;
      }
      set { m_mode_collected = value; }
    } // CollectionMode

    public String MergedField
    {
      get
      {
        switch (MachineIdType)
        {
          case ImportId.StackerId:
            return SQL_COLUMN_STACKER_ID;

          case ImportId.EGMFloorId:
          case ImportId.EGMBaseName:
            return SQL_COLUMN_TERMINAL_ID;

          default:
            return String.Empty;
        }
      }
    } // MergedField

    public ImportId MachineIdType
    {
      get
      {
        return m_machine_id_type;
      }
      set { m_machine_id_type = value; }
    } // MachineIdType

    #endregion " Properties"

    #region " Constructor"

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES :
    //
    public SofCountData()
    {

    } // SofCountData

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor
    //
    //  PARAMS :
    //      - INPUT : String SoftCountsXml
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES :
    //
    public SofCountData(String SoftCountsXml, Int64 SoftCountId)
    {
      SoftCountObject = new ImportedSoftCounts(SoftCountId);

      SoftCountObject.m_merged_details = new Dictionary<int, DataTable>();
      m_money_collection_central = new List<Int64>();

      MachineIdType = ImportId.StackerId;

      m_soft_counts_xml = SoftCountsXml;
      m_soft_count_id = SoftCountId;

      // FILL DataTable Terminals once
      if (!FillSoftCountAllTerminals())
      {

        Log.Message("An error occurred in constructor SofCountData (FillSoftCountAllTerminals).");
      }

    } // SofCountData

    #endregion " Constructor"

    #region " Section (center)"

    //------------------------------------------------------------------------------
    // PURPOSE : Get Data from Import File (Xml) and return int in a DataTable
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT : DataTable SoftCountData
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    private Boolean GetSoftCountImportFile(out DataTable SoftCountData)
    {
      DataRow _dr_softcount;
      ImportedSoftCounts _fillecolection;

      Boolean _once_one_type;
      Int64 _terminal_id;

      SoftCountData = new DataTable();

      try
      {
        //0. Initialize variables and make DataTable
        _once_one_type = true;
        _terminal_id = 0;
        _fillecolection = new ImportedSoftCounts();

        SoftCountData.Columns.Add(SQL_COLUMN_STACKER_ID, Type.GetType("System.Int64"));                         //StackerId
        SoftCountData.Columns.Add(SQL_COLUMN_FLOOR_ID, Type.GetType("System.String"));                          //FloorId
        SoftCountData.Columns.Add(SQL_COLUMN_TERMINAL_ID, Type.GetType("System.Int32"));                        //TerminalId
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_BILLS, Type.GetType("System.Decimal"));                  //Collected Bills
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_TICKETS, Type.GetType("System.Decimal"));                //Collected tickets
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_BILLS, Type.GetType("System.Decimal"));                   //Expected Bills
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_TICKETS, Type.GetType("System.Decimal"));                 //Expected Tickets
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTION_ID, Type.GetType("System.Int64"));                      //Collection Id
        SoftCountData.Columns.Add(SQL_COLUMN_NON_IMPORT, Type.GetType("System.Boolean"));                       //Non Import
        SoftCountData.Columns.Add(SQL_COLUMN_CHECKED, Type.GetType("System.Boolean"));                          //Checked
        SoftCountData.Columns.Add(SQL_COLUMN_IMPORT_INDEX, Type.GetType("System.Int32"));                       //Index of the import
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_BILL_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_TICKET_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_RE_TICKET_AMOUNT, Type.GetType("System.Decimal"));
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_RE_TICKET_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_PROMO_RE_TICKET_AMOUNT, Type.GetType("System.Decimal"));
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_PROMO_RE_TICKET_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_PROMO_NR_TICKET_AMOUNT, Type.GetType("System.Decimal"));
        SoftCountData.Columns.Add(SQL_COLUMN_EXPECTED_PROMO_NR_TICKET_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_BILL_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_TICKET_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_RE_TICKET_AMOUNT, Type.GetType("System.Decimal"));
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_RE_TICKET_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_AMOUNT, Type.GetType("System.Decimal"));
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_AMOUNT, Type.GetType("System.Decimal"));
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_COUNT, Type.GetType("System.Int32"));
        SoftCountData.Columns.Add(SQL_COLUMN_COLLECTION_STATUS, Type.GetType("System.Int32"));

        //1. DesSerialize into object
        SoftCountObject.Deserialization(m_soft_count_id);

        //2. Convert Object to DataTable

        //2.1 Read SoftCount by SoftCount
        foreach (KeyValuePair<String, SoftCount> _soft_count_item in SoftCountObject.m_soft_count)
        {
          if (_once_one_type)
          {
            MachineIdType = _soft_count_item.Value.m_soft_sount_machine_id_type;
            _once_one_type = false;
          }

          //Add Row to the Datatable
          _dr_softcount = SoftCountData.NewRow();

          switch (MachineIdType)
          {
            case ImportId.StackerId:    // Stacker Id

              _dr_softcount[SQL_COLUMN_STACKER_ID] = _soft_count_item.Value.m_soft_count_machine_id;

              break;
            case ImportId.EGMBaseName:  // Base Name
            case ImportId.EGMFloorId:   // Floor Id

              if (MachineIdType == ImportId.EGMBaseName)
              {
                _terminal_id = this.GetSoftCountOneTerminalId(String.Empty, (String)_soft_count_item.Value.m_soft_count_machine_id);
              }
              else
              {
                _terminal_id = this.GetSoftCountOneTerminalId((String)_soft_count_item.Value.m_soft_count_machine_id, String.Empty);
              }

              _dr_softcount[SQL_COLUMN_FLOOR_ID] = _soft_count_item.Value.m_soft_count_machine_id;
              _dr_softcount[SQL_COLUMN_TERMINAL_ID] = _terminal_id;

              break;

            default:

              break;

          }

          //*COLLECTED
          // Total Bills Amount
          _dr_softcount[SQL_COLUMN_COLLECTED_BILLS] = _soft_count_item.Value.TotalBillsAmount;

          // Total Bills Count
          _dr_softcount[SQL_COLUMN_COLLECTED_BILL_COUNT] = _soft_count_item.Value.TotalBillsCount;

          // Total Tickets Amount
          _dr_softcount[SQL_COLUMN_COLLECTED_TICKETS] = _soft_count_item.Value.TicketList.m_total_tickets_amount;

          // Total Tickets Count
          _dr_softcount[SQL_COLUMN_COLLECTED_TICKET_COUNT] = _soft_count_item.Value.TicketList.m_total_tickets_count;

          // Total Tickets RE Amount
          _dr_softcount[SQL_COLUMN_COLLECTED_RE_TICKET_AMOUNT] = _soft_count_item.Value.TicketList.m_total_ticket_re_amount;

          // Total Tickets RE Count
          _dr_softcount[SQL_COLUMN_COLLECTED_RE_TICKET_COUNT] = _soft_count_item.Value.TicketList.m_total_ticket_re_count;

          // Total Tickets PROMO RE Amount
          _dr_softcount[SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_AMOUNT] = _soft_count_item.Value.TicketList.m_total_ticket_promo_re_amount;

          // Total Tickets PROMO RE Count
          _dr_softcount[SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_COUNT] = _soft_count_item.Value.TicketList.m_total_ticket_promo_re_count;

          // Total Tickets PROMO NR Amount
          _dr_softcount[SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_AMOUNT] = _soft_count_item.Value.TicketList.m_total_ticket_promo_nr_amount;

          // Total Tickets PROMO NR Count
          _dr_softcount[SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_COUNT] = _soft_count_item.Value.TicketList.m_total_ticket_promo_nr_count;


          //*EXPECTED 
          // Total Bills Amount
          _dr_softcount[SQL_COLUMN_EXPECTED_BILLS] = 0;

          // Expected Tickets
          _dr_softcount[SQL_COLUMN_EXPECTED_TICKETS] = 0;

          SoftCountData.Rows.Add(_dr_softcount);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetSoftCountImportFile

    //------------------------------------------------------------------------------
    // PURPOSE : Get Data from Table Money Colletions and return int in a DataTable
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT : DataTable MoneyCollection
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    private Boolean GetSoftCountMoneyCollection(out DataTable MoneyCollection)
    {
      StringBuilder _sb;


      _sb = new StringBuilder();
      MoneyCollection = new DataTable();
      m_soft_counts_money_collection = new DataTable();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT   MC_STACKER_ID AS STACKER_ID                                              ");
        _sb.AppendLine("           , TE_FLOOR_ID AS FLOOR_ID                                                  ");
        _sb.AppendLine("           , MC_TERMINAL_ID AS TERMINAL_ID                                            ");
        _sb.AppendLine("           , 0.00 AS COLLECTED_BILLS                                                  ");
        _sb.AppendLine("           , 0.00 AS COLLECTED_TICKETS                                                ");
        _sb.AppendLine("           , MC_EXPECTED_BILL_AMOUNT AS EXPECTED_BILLS                                ");
        _sb.AppendLine("           , MC_EXPECTED_TICKET_AMOUNT AS EXPECTED_TICKETS                            ");
        _sb.AppendLine("           , MC_COLLECTION_ID AS COLLECTION_ID                                        ");
        _sb.AppendLine("           , 0 AS NON_IMPORT                                                          ");
        _sb.AppendLine("           , 1 AS CHECKED                                                             ");
        _sb.AppendLine("           , 0 AS IMPORT_INDEX                                                        ");
        _sb.AppendLine("           , MC_EXPECTED_BILL_COUNT AS EXPECTED_BILL_COUNT                            ");
        _sb.AppendLine("           , MC_EXPECTED_TICKET_COUNT AS EXPECTED_TICKET_COUNT                        ");
        _sb.AppendLine("           , MC_EXPECTED_RE_TICKET_AMOUNT AS EXPECTED_RE_TICKET_AMOUNT                ");
        _sb.AppendLine("           , MC_EXPECTED_RE_TICKET_COUNT AS EXPECTED_RE_TICKET_COUNT                  ");
        _sb.AppendLine("           , MC_EXPECTED_PROMO_RE_TICKET_AMOUNT AS EXPECTED_PROMO_RE_TICKET_AMOUNT    ");
        _sb.AppendLine("           , MC_EXPECTED_PROMO_RE_TICKET_COUNT AS EXPECTED_PROMO_RE_TICKET_COUNT      ");
        _sb.AppendLine("           , MC_EXPECTED_PROMO_NR_TICKET_AMOUNT AS EXPECTED_PROMO_NR_TICKET_AMOUNT    ");
        _sb.AppendLine("           , MC_EXPECTED_PROMO_NR_TICKET_COUNT AS EXPECTED_PROMO_NR_TICKET_COUNT      ");
        _sb.AppendLine("           , 0 AS COLLECTED_BILL_COUNT                                                ");
        _sb.AppendLine("           , 0 AS COLLECTED_TICKET_COUNT                                              ");
        _sb.AppendLine("           , 0.00 AS COLLECTED_RE_TICKET_AMOUNT                                       ");
        _sb.AppendLine("           , 0 AS COLLECTED_RE_TICKET_COUNT                                           ");
        _sb.AppendLine("           , 0.00 AS COLLECTED_PROMO_RE_TICKET_AMOUNT                                 ");
        _sb.AppendLine("           , 0 AS COLLECTED_PROMO_RE_TICKET_COUNT                                     ");
        _sb.AppendLine("           , 0.00 AS COLLECTED_PROMO_NR_TICKET_AMOUNT                                 ");
        _sb.AppendLine("           , 0 AS COLLECTED_PROMO_NR_TICKET_COUNT                                     ");
        _sb.AppendLine("           , MC_STATUS AS COLLECTION_STATUS                                           ");
        _sb.AppendLine("      FROM   MONEY_COLLECTIONS                                                        ");
        _sb.AppendLine("INNER JOIN   TERMINALS                                                                ");
        _sb.AppendLine("        ON   MONEY_COLLECTIONS.MC_TERMINAL_ID = TE_TERMINAL_ID                        ");
        if (CollectionMode == COLLECTION_MODE.MODE_COLLECT)
        {
          _sb.AppendLine("     WHERE   MC_STATUS = @pStatus1                                                  ");
        }
        else
        {
          //_sb.AppendLine("     WHERE   MC_STATUS IN (@pStatus1, @pStatus2)               ");
          _sb.AppendLine("     WHERE   MC_STATUS IN (@pStatus, @pStatus1, @pStatus2)                          ");
        }
        _sb.AppendLine("  ORDER BY   MC_COLLECTION_ID ASC                                                     ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING;
            if (CollectionMode == COLLECTION_MODE.MODE_COLLECT)
            {
              _sql_cmd.Parameters.Add("@pStatus1", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pStatus1", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED;
              _sql_cmd.Parameters.Add("@pStatus2", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE;
            }

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
            {
              _da.Fill(m_soft_counts_money_collection);
              MoneyCollection = m_soft_counts_money_collection.Copy();

            }
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetSoftCountMoneyCollection

    #endregion " Section (center)"

    #region " Section (detail)"

    //------------------------------------------------------------------------------
    // PURPOSE : Get Data (detail from one collectionId) from Import File (Xml)
    //
    //  PARAMS :
    //      - INPUT :   SOFT_COUNT_DATA_DETAIL DataDetailEntry
    //      - OUTPUT :  DataTable SoftCountDataDetail
    //                  SoftCount ProcessedSoftCount
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    private Boolean GetSoftCountImportFileDetail(SOFT_COUNT_DATA_DETAIL DataDetailEntry,
                                                 out DataTable SoftCountDataDetail,
                                                 out SoftCount ProcessedSoftCount)
    {
      Int64 _find_id;
      String _find_base;
      String _find_floorid;
      String _find_result;

      SoftCountDataDetail = new DataTable();
      _find_id = 0;
      _find_base = String.Empty;
      _find_floorid = String.Empty;
      _find_result = String.Empty;
      ProcessedSoftCount = new SoftCount();

      try
      {

        switch (this.MachineIdType)
        {
          case ImportId.StackerId:
            _find_result = DataDetailEntry.StackerId.ToString();

            break;

          case ImportId.EGMBaseName:
            _find_id = DataDetailEntry.TerminalId;

            if (GetSoftCountOneTerminalBaseNameFloorId(_find_id, out _find_base, out _find_floorid))
            {
              _find_result = _find_base;
            }

            break;

          case ImportId.EGMFloorId:
            _find_id = DataDetailEntry.TerminalId;

            if (GetSoftCountOneTerminalBaseNameFloorId(_find_id, out _find_base, out _find_floorid))
            {
              _find_result = _find_floorid;
            }

            break;
        }

        if (!SoftCountObject.m_soft_count.TryGetValue(_find_result, out ProcessedSoftCount))
        {
          ProcessedSoftCount = new SoftCount();
          SoftCountDataDetail = ProcessedSoftCount.CreateSoftCountDetailsDatatable();

          return false;
        }


        SoftCountDataDetail = ProcessedSoftCount.SoftCountDetails.Copy();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetSoftCountImportFileDetail

    //------------------------------------------------------------------------------
    // PURPOSE : Get Data (detail from one collectionId) from CASHIER_TERMINAL_MONEY and Tickets (DB)
    //
    //  PARAMS :
    //      - INPUT :   Int64 CollectionId
    //      - OUTPUT :  DataTable SoftCountDataDetail
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    private Boolean GetSoftCountMoneyCollectionDetailDataTable(Int64 CollectionId,
                                                               out DataTable SoftCountDetail)
    {
      DataRow[] _dr;
      DataRow _dr_aux;
      DataTable _dt_aux;

      SoftCountDetail = new DataTable();

      try
      {
        _dt_aux = m_soft_counts_detail.Clone();
        _dt_aux.Rows.Clear();

        _dr = m_soft_counts_detail.Select(SQL_COLUMN_COLLECTION_ID + " = " + CollectionId);

        foreach (DataRow _dr_aux_for in _dr)
        {
          _dr_aux = _dt_aux.NewRow();

          for (Int32 _idx = 0; _idx < m_soft_counts_detail.Columns.Count; _idx++)
          {
            _dr_aux[_idx] = _dr_aux_for[_idx];
          }

          _dt_aux.Rows.Add(_dr_aux);
        }

        SoftCountDetail = _dt_aux;

        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetSoftCountMoneyCollectionDetailDataTable

    #endregion " Section (detail)"

    #region " Private Functions"

    //------------------------------------------------------------------------------
    // PURPOSE : Get All Data from TICKETS and CASHIER_TERMINAL_MONEY to save in memory DataTable
    //
    //  PARAMS :
    //      - INPUT :   
    //      - OUTPUT :  
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    private Boolean FillSoftCountMoneyCollectionDetail()
    {
      return FillSoftCountMoneyCollectionDetail(new List<Int64>());
    }
    private Boolean FillSoftCountMoneyCollectionDetail(List<Int64> MoneyCollections)
    {
      StringBuilder _sb;
      StringBuilder _money_collection_where;
      Int32 _counter;

      _sb = new StringBuilder();
      m_soft_counts_detail = new DataTable();

      _money_collection_where = new StringBuilder();
      for (Int32 _idx = 0; _idx < MoneyCollections.Count; _idx++)
      {
        if (_idx > 0)
        {
          _money_collection_where.Append(" , ");
        }
        _money_collection_where.Append(String.Format("@pMoneyCollectionId{0}", _idx));
      }


      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("        SELECT   0                               AS TYPE                                  ");
        _sb.AppendLine("               , TI_VALIDATION_NUMBER            AS TICKET_NUMBER                         ");
        _sb.AppendLine("               , TI_AMOUNT                       AS TICKET_AMOUNT                         ");
        _sb.AppendLine("               , TI_VALIDATION_TYPE              AS TICKET_VALIDATION_TYPE                ");
        _sb.AppendLine("               , NULL                            AS TICKET_STATUS                         ");
        _sb.AppendLine("               , ''                              AS DENOMINATION                          ");
        _sb.AppendLine("               , NULL                            AS COLLECTED_BILLS_DENOMINATION_QUANTITY ");
        _sb.AppendLine("               , NULL                            AS COLLECTED_BILLS_TOTAL_AMOUNT          ");
        _sb.AppendLine("               , NULL                            AS EXPECTED_BILLS_DENOMINATION_QUANTITY  ");
        _sb.AppendLine("               , NULL                            AS EXPECTED_BILLS_TOTAL_AMOUNT           ");
        _sb.AppendLine("               , TI_MONEY_COLLECTION_ID          AS COLLECTION_ID                         ");
        _sb.AppendLine("               , NULL                            AS STACKER_ID                            ");
        _sb.AppendLine("               , NULL                            AS DENOMINATION_NUMERIC                  ");
        _sb.AppendLine("               , TI_COLLECTED_MONEY_COLLECTION   AS TICKET_COLLECTED                      ");
        _sb.AppendLine("               , TI_TICKET_ID                    AS TICKET_ID                             ");
        _sb.AppendLine("               , NULL                            AS CAGE_MOVEMENT_ID                      ");
        _sb.AppendLine("          FROM   TICKETS                                                                  ");
        if (MoneyCollections.Count > 0)
        {
          _sb.AppendLine("       WHERE  TICKETS.TI_MONEY_COLLECTION_ID IN (" + _money_collection_where.ToString() + ") ");
        }
        _sb.AppendLine("         UNION ALL                                                                        ");
        _sb.AppendLine("        SELECT   0                               AS TYPE                                  ");
        _sb.AppendLine("               , TI_VALIDATION_NUMBER            AS TICKET_NUMBER                         ");
        _sb.AppendLine("               , TI_AMOUNT                       AS TICKET_AMOUNT                         ");
        _sb.AppendLine("               , TI_VALIDATION_TYPE              AS TICKET_VALIDATION_TYPE                ");
        _sb.AppendLine("               , NULL                            AS TICKET_STATUS                         ");
        _sb.AppendLine("               , ''                              AS DENOMINATION                          ");
        _sb.AppendLine("               , NULL                            AS COLLECTED_BILLS_DENOMINATION_QUANTITY ");
        _sb.AppendLine("               , NULL                            AS COLLECTED_BILLS_TOTAL_AMOUNT          ");
        _sb.AppendLine("               , NULL                            AS EXPECTED_BILLS_DENOMINATION_QUANTITY  ");
        _sb.AppendLine("               , NULL                            AS EXPECTED_BILLS_TOTAL_AMOUNT           ");
        _sb.AppendLine("               , TI_MONEY_COLLECTION_ID          AS COLLECTION_ID                         ");
        _sb.AppendLine("               , NULL                            AS STACKER_ID                            ");
        _sb.AppendLine("               , NULL                            AS DENOMINATION_NUMERIC                  ");
        _sb.AppendLine("               , TI_COLLECTED_MONEY_COLLECTION   AS TICKET_COLLECTED                      ");
        _sb.AppendLine("               , TI_TICKET_ID                    AS TICKET_ID                             ");
        _sb.AppendLine("               , NULL                            AS CAGE_MOVEMENT_ID                      ");
        _sb.AppendLine("          FROM   TICKETS                                                                  ");
        if (MoneyCollections.Count > 0)
        {
          _sb.AppendLine("       WHERE  TICKETS.TI_COLLECTED_MONEY_COLLECTION IN (" + _money_collection_where.ToString() + ") ");
        }

        _sb.AppendLine("         UNION                                                                            ");
        _sb.AppendLine("        SELECT   1                       AS TYPE                                          ");
        _sb.AppendLine("               , NULL                    AS TICKET_NUMBER                                 ");
        _sb.AppendLine("               , NULL                    AS TICKET_AMOUNT                                 ");
        _sb.AppendLine("               , NULL                    AS TICKET_VALIDATION_TYPE                        ");
        _sb.AppendLine("               , NULL                    AS TICKET_STATUS                                 ");
        _sb.AppendLine("               , CAST(CTM_DENOMINATION AS NVARCHAR(50)) AS DENOMINATION                   ");
        _sb.AppendLine("               , NULL                    AS COLLECTED_BILLS_DENOMINATION_QUANTITY         ");
        _sb.AppendLine("               , NULL                    AS COLLECTED_BILLS_TOTAL_AMOUNT                  ");
        _sb.AppendLine("               , CTM_QUANTITY            AS EXPECTED_BILLS_DENOMINATION_QUANTITY          ");
        _sb.AppendLine("               , CTM_DENOMINATION * CTM_QUANTITY AS EXPECTED_BILLS_TOTAL_AMOUNT           ");
        _sb.AppendLine("               , CTM_MONEY_COLLECTION_ID AS COLLECTION_ID                                 ");
        _sb.AppendLine("               , NULL                    AS STACKER_ID                                    ");
        _sb.AppendLine("               , CTM_DENOMINATION        AS DENOMINATION_NUMERIC                          ");
        _sb.AppendLine("               , NULL                    AS TICKET_COLLECTED                              ");
        _sb.AppendLine("               , NULL                    AS TICKET_ID                                     ");
        _sb.AppendLine("               , NULL                    AS CAGE_MOVEMENT_ID                              ");
        _sb.AppendLine("          FROM   CASHIER_TERMINAL_MONEY                                                   ");
        _sb.AppendLine("     LEFT JOIN   MONEY_COLLECTIONS ON  CTM_MONEY_COLLECTION_ID = MC_COLLECTION_ID         ");

        if (this.CollectionMode == COLLECTION_MODE.MODE_CANCEL && MoneyCollections.Count > 0)
        {
          _sb.AppendLine(" WHERE MC_COLLECTION_ID IN (" + _money_collection_where.ToString() + ") ");
        }
        else
        {
          _sb.AppendLine("         WHERE   MC_STATUS = @pStatus                                                    ");
          _sb.AppendLine("            OR   CTM_MONEY_COLLECTION_ID IS NULL                                         ");
        }
        _sb.AppendLine("      GROUP BY   CTM_DENOMINATION, CTM_QUANTITY, CTM_MONEY_COLLECTION_ID                  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING;
            _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.VarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
            _counter = 0;
            foreach (Int64 _mc_id in MoneyCollections)
            {
              _sql_cmd.Parameters.Add(String.Format("@pMoneyCollectionId{0}", _counter), SqlDbType.BigInt).Value = _mc_id;
              _counter++;
            }

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
            {
              _da.Fill(m_soft_counts_detail);
            }
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // FillSoftCountMoneyCollectionDetail

    //------------------------------------------------------------------------------
    // PURPOSE : Get All Data from TERMINALS to save in memory DataTable
    //
    //  PARAMS :
    //      - INPUT :   
    //      - OUTPUT :  
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    private Boolean FillSoftCountAllTerminals()
    {

      StringBuilder _sb;


      _sb = new StringBuilder();
      m_soft_counts_terminals = new DataTable();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("      SELECT    TLC.TLC_TERMINAL_ID     AS TERMINAL_ID    ");
        _sb.AppendLine("              , TE.TE_FLOOR_ID          AS FLOOR_ID       ");
        _sb.AppendLine("              , TLC.TLC_BASE_NAME       AS BASE_NAME      ");
        _sb.AppendLine("        FROM    TERMINALS_LAST_CHANGED  AS TLC            ");
        _sb.AppendLine("  INNER JOIN    TERMINALS               AS TE             ");
        _sb.AppendLine("             ON TE.TE_TERMINAL_ID = TLC.TLC_TERMINAL_ID   ");
        _sb.AppendLine("    ORDER BY    TE.TE_TERMINAL_ID                         ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
            {
              _da.Fill(m_soft_counts_terminals);
            }
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // FillSoftCountAllTerminals


    //------------------------------------------------------------------------------
    // PURPOSE : Get Terminal_Id from one terminal
    //
    //  PARAMS :
    //      - INPUT :   String FloorId
    //                  String BaseName
    //      - OUTPUT :  
    //                  
    // RETURNS : 
    //
    //   NOTES : Terminal Id (Int64)
    //
    private Int64 GetSoftCountOneTerminalId(String FloorId,
                                            String BaseName)
    {
      DataRow[] _dr;
      try
      {
        if (FloorId != String.Empty)
        {
          _dr = m_soft_counts_terminals.Select(SQL_COLUMN_FLOOR_ID + " = '" + FloorId + "'");
          if (_dr != null && _dr.Length > 0)
          {

            return (Int32)_dr[0][SQL_COLUMN_TERMINAL_ID];
          }
        }
        if (BaseName != String.Empty)
        {
          _dr = m_soft_counts_terminals.Select(SQL_COLUMN_BASE_NAME + " = '" + BaseName + "'");
          if (_dr != null && _dr.Length > 0)
          {

            return (Int32)_dr[0][SQL_COLUMN_TERMINAL_ID];
          }
        }

        return 0;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return -1;
    } // GetSoftCountOneTerminalId


    //------------------------------------------------------------------------------
    // PURPOSE : Get BaseName And FloorId from one terminal
    //
    //  PARAMS :
    //      - INPUT :   Int64 TerminalId
    //      - OUTPUT :  String BaseName
    //                  String FloorId
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public Boolean GetSoftCountOneTerminalBaseNameFloorId(Int64 TerminalId,
                                                           out String BaseName,
                                                           out String FloorId)
    {
      DataRow[] _dr;

      BaseName = String.Empty;
      FloorId = String.Empty;
      try
      {

        _dr = m_soft_counts_terminals.Select(SQL_COLUMN_TERMINAL_ID + " = " + TerminalId);
        if (_dr != null && _dr.Length > 0)
        {
          if (!_dr[0].IsNull(SQL_COLUMN_FLOOR_ID))
          {
            FloorId = (String)(_dr[0][SQL_COLUMN_FLOOR_ID]);
          }

          if (!_dr[0].IsNull(SQL_COLUMN_BASE_NAME))
          {
            BaseName = (String)(_dr[0][SQL_COLUMN_BASE_NAME]);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetSoftCountOneTerminalBaseNameFloorId

    //------------------------------------------------------------------------------
    // PURPOSE : Return if MoneyCollection is Collected
    //
    //  PARAMS :
    //      - INPUT :   Int64 MoneyCollectionId
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    private Boolean IsMoneyCollectionCollected(Int64 MoneyCollectionId)
    {

      try
      {
        foreach (KeyValuePair<String, SoftCount> _soft_count_item in SoftCountObject.m_soft_count)
        {
          if (_soft_count_item.Value.m_target_collection_id == MoneyCollectionId)
          {
            return true;
          }
        }
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsMoneyCollectionCollected

    private STATUS_TICKET GetStatusTicket(DataRow DrDetailTicket, Int64 CollectionId)
    {
      try
      {
        if (DrDetailTicket.IsNull(SQL_COLUMN_COLLECTION_ID) && DrDetailTicket.IsNull(SQL_COLUMN_TICKET_COLLECTED))
        {

          return STATUS_TICKET.TICKET_STATUS_OK;
        }
        else if (!DrDetailTicket.IsNull(SQL_COLUMN_TICKET_COLLECTED) && (Int64)DrDetailTicket[SQL_COLUMN_TICKET_COLLECTED] == CollectionId)
        {

          return STATUS_TICKET.TICKET_STATUS_OK;
        }
        else if (!DrDetailTicket.IsNull(SQL_COLUMN_COLLECTION_ID) && (Int64)DrDetailTicket[SQL_COLUMN_COLLECTION_ID] == CollectionId && DrDetailTicket.IsNull(SQL_COLUMN_TICKET_COLLECTED))
        {

          return STATUS_TICKET.TICKET_STATUS_OK;
        }
        else
        {

          return STATUS_TICKET.TICKET_STATUS_ERROR;
        }
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return STATUS_TICKET.TICKET_STATUS_ERROR;
    }
    #endregion " Private Functions"

    #region " Public Functions "

    //------------------------------------------------------------------------------
    // PURPOSE : Return cashier session id
    //
    //  PARAMS :
    //      - INPUT :   Int64 MoneyCollectionId
    //      - OUTPUT :  Int64 CashierSessionId
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public Boolean GetCashierSessionId(Int64 MoneyCollectionId, out Int64 CashierSessionId)
    {

      StringBuilder _sb;

      CashierSessionId = 0;

      try
      {

        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT   MC_CASHIER_SESSION_ID                  ");
        _sb.AppendLine("      FROM   MONEY_COLLECTIONS                      ");
        _sb.AppendLine("     WHERE   MC_COLLECTION_ID = @pMoneyCollectionId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;

            using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
            {
              if (!_sql_rd.Read())
              {
                return false;
              }

              CashierSessionId = _sql_rd.GetInt64(0);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return false;
    } // GetCashierSessionId

    //------------------------------------------------------------------------------
    // PURPOSE : Merge Datatable (Import XML) with Datatable (CASHIER_TERMINAL_MONEY && Tickets)
    //
    //  PARAMS :
    //      - INPUT :   SOFT_COUNT_DATA_DETAIL DataDetailEntry
    //      - OUTPUT :  DataTable SoftCountDataBillsTicketsDetail
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public Boolean GetSoftCountCollectionsDetail(SOFT_COUNT_DATA_DETAIL DataDetailEntry,
                                                 Boolean RemoveWithOutCollectionId,
                                                 ref DataTable SoftCountDataBillsTicketsDetail)
    {

      DataTable _dt_import;
      DataTable _dt_money_collection_detail;
      DataRow[] _dr_aux_find;
      DataRow _dr_aux;
      SoftCount _soft_count;
      //DataTable _dt_detail_aux;

      Int64 _num_tickets_collected;
      Int64 _num_tickets_expected;
      Decimal _tmp_imp_denom;

      try
      {

        SoftCountDataBillsTicketsDetail = new DataTable();
        //_dt_detail_aux = new DataTable();

        _num_tickets_collected = 0;
        _num_tickets_expected = 0;

        // 1. Get Filter Data from XML
        GetSoftCountImportFileDetail(DataDetailEntry, out _dt_import, out _soft_count);

        // 1.1 Check if exists
        if (!RemoveWithOutCollectionId && SoftCountObject.m_merged_details.TryGetValue(DataDetailEntry.ImportIndex, out SoftCountDataBillsTicketsDetail))
        {

          return true;
        }

        // 2. Get Data from DataTable (memory)
        if (!GetSoftCountMoneyCollectionDetailDataTable(DataDetailEntry.CollectionId, out _dt_money_collection_detail))
        {

          Log.Message("An error occurred in function GetSoftCountCollectionsDetail (GetSoftCountMoneyCollectionDetailDataTable).");
          return false;
        }

        _num_tickets_expected = _dt_money_collection_detail.Select("TYPE = " + (Int32)TYPE_ROW_TICKET_BILL.TICKET).Length;

        // 3. Merge DataTables
        foreach (DataRow _dr_row in _dt_money_collection_detail.Rows)
        {

          // MERGE TICKETS
          if ((TYPE_ROW_TICKET_BILL)_dr_row[SQL_COLUMN_TYPE] == TYPE_ROW_TICKET_BILL.TICKET)
          {

            //Check if Tickets Exists in DB (tickets filtered by collection Id)
            _dr_aux_find = _dt_import.Select(SQL_COLUMN_TICKET_NUMBER + " = " + _dr_row[SQL_COLUMN_TICKET_NUMBER]);

            if (_dr_aux_find != null && _dr_aux_find.Length > 0)
            {

              //Check if ticket is collected or not Collected (Status: OK or ERROR)
              _dr_row[SQL_COLUMN_TICKET_STATUS] = GetStatusTicket(_dr_row, DataDetailEntry.CollectionId);

              _dr_row[SQL_COLUMN_COLLECTION_ID] = DataDetailEntry.CollectionId;
              _dr_row[SQL_COLUMN_DENOMINATION] = Cage.TICKETS_CODE;
              _dr_row[SQL_COLUMN_DENOMINATION_NUMERIC] = Cage.TICKETS_CODE;
              if (!RemoveWithOutCollectionId)
              {
                // For visualization in Grid
                _dr_row[SQL_COLUMN_EXPECTED_BILLS_QUANTITY] = 1;
                _dr_row[SQL_COLUMN_COLLECTED_BILLS_QUANTITY] = _num_tickets_expected;
              }
              else
              {
                //For Processing Collection
                _dr_row[SQL_COLUMN_EXPECTED_BILLS_QUANTITY] = _num_tickets_expected;
                _dr_row[SQL_COLUMN_COLLECTED_BILLS_QUANTITY] = 1;
              }

              _dt_import.Rows.Remove(_dr_aux_find[0]);
              _num_tickets_collected += 1;
            }
            else
            {
              //If ticket doesn' t exist in file (xml) is pending
              _dr_row[SQL_COLUMN_TICKET_STATUS] = STATUS_TICKET.TICKET_STATUS_PENDING;
              _dr_row[SQL_COLUMN_EXPECTED_BILLS_QUANTITY] = _num_tickets_expected;
              if (!RemoveWithOutCollectionId)
              {
                _dr_row[SQL_COLUMN_COLLECTED_BILLS_QUANTITY] = 0;
              }
              _dr_row[SQL_COLUMN_DENOMINATION] = Cage.TICKETS_CODE;
              _dr_row[SQL_COLUMN_DENOMINATION_NUMERIC] = Cage.TICKETS_CODE;
            }
          }

          // MERGE BILLS
          if ((TYPE_ROW_TICKET_BILL)_dr_row[SQL_COLUMN_TYPE] == TYPE_ROW_TICKET_BILL.BILL &&
               Decimal.TryParse(Format.LocalNumberToDBNumber(_dr_row[SQL_COLUMN_DENOMINATION_NUMERIC].ToString()), out _tmp_imp_denom))
          {
            _dr_row[SQL_COLUMN_DENOMINATION] = _tmp_imp_denom;
            _dr_aux_find = _dt_import.Select(SQL_COLUMN_DENOMINATION_NUMERIC + " = " + _tmp_imp_denom);
            if (_dr_aux_find != null && _dr_aux_find.Length > 0)
            {
              _dr_row[SQL_COLUMN_DENOMINATION_NUMERIC] = _dr_aux_find[0][SQL_COLUMN_DENOMINATION_NUMERIC];
              _dr_row[SQL_COLUMN_COLLECTED_BILLS_QUANTITY] = _dr_aux_find[0][SQL_COLUMN_COLLECTED_BILLS_QUANTITY];
              _dr_row[SQL_COLUMN_COLLECTED_BILLS_TOTAL] = _dr_aux_find[0][SQL_COLUMN_COLLECTED_BILLS_TOTAL];
              _dr_row[SQL_COLUMN_COLLECTION_ID] = DataDetailEntry.CollectionId;
              _dt_import.Rows.Remove(_dr_aux_find[0]);
            }
          }
        }

        // Add the rest of rows
        foreach (DataRow _dr_row in _dt_import.Rows)
        {

          // Only For Collection (BatchUpdate)
          if (Convert.ToInt64(_dr_row[SQL_COLUMN_COLLECTION_ID]) == 0 && RemoveWithOutCollectionId)
          {

            _dr_row[SQL_COLUMN_COLLECTION_ID] = DataDetailEntry.CollectionId;
          }

          _dr_aux = _dt_money_collection_detail.NewRow();

          for (Int32 _idx = 0; _idx < _dt_import.Columns.Count; _idx++)
          {
            _dr_aux[_idx] = _dr_row[_idx];
          }

          if ((Int32)_dr_row[SQL_COLUMN_TYPE] == (Int32)TYPE_ROW_TICKET_BILL.TICKET)
          {
            _dr_aux[SQL_COLUMN_TICKET_STATUS] = STATUS_TICKET.TICKET_STATUS_OK;
            _dr_aux[SQL_COLUMN_TICKET_VALIDATION_TYPE] = TITO_VALIDATION_TYPE.SYSTEM;
            _dr_aux[SQL_COLUMN_DENOMINATION] = Cage.TICKETS_CODE;
            _dr_aux[SQL_COLUMN_DENOMINATION_NUMERIC] = Cage.TICKETS_CODE;

            _dr_aux[SQL_COLUMN_EXPECTED_BILLS_QUANTITY] = _soft_count.TicketList.m_total_tickets_count;
            _dr_aux[SQL_COLUMN_COLLECTED_BILLS_QUANTITY] = 1;

            _num_tickets_collected += 1;
          }

          _dt_money_collection_detail.Rows.Add(_dr_aux);
        }

        // Add Line of Total Tickets
        if (!RemoveWithOutCollectionId)
        {
          _dr_aux = _dt_money_collection_detail.NewRow();
          _dr_aux[SQL_COLUMN_TYPE] = TYPE_ROW_TICKET_BILL.TOTAL_TICKET;
          _dr_aux[SQL_COLUMN_DENOMINATION] = Resource.String("STR_TITO_FILE_COLLECTION_TOTAL_TICKETS");
          _dr_aux[SQL_COLUMN_COLLECTED_BILLS_QUANTITY] = _num_tickets_collected;
          _dr_aux[SQL_COLUMN_EXPECTED_BILLS_QUANTITY] = _num_tickets_expected;
          _dr_aux[SQL_COLUMN_COLLECTION_ID] = DataDetailEntry.CollectionId;
          _dt_money_collection_detail.Rows.Add(_dr_aux);
        }
        //Order by Denomination
        _dt_money_collection_detail.DefaultView.Sort = SQL_COLUMN_DENOMINATION_NUMERIC + " DESC";

        _dt_money_collection_detail = _dt_money_collection_detail.DefaultView.ToTable();
        if (!RemoveWithOutCollectionId)
        {
          SoftCountObject.m_merged_details.Add(DataDetailEntry.ImportIndex, _dt_money_collection_detail);
        }
        SoftCountDataBillsTicketsDetail = _dt_money_collection_detail;

        return true;
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return false;
    } // GetSoftCountCollectionsDetail

    //------------------------------------------------------------------------------
    // PURPOSE : Merge Datatable (Xml) with Datatable (MoneyCollections)
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT : 
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public Boolean GetSoftCountCollections()
    {

      DataTable _dt_import;
      DataTable _dt_money_collection;
      DataRow[] _dr;
      DataRow _dr_aux;
      String _merge_field;
      Int32 _index_import;

      MergedCollectionSoftCount = new DataTable();
      _merge_field = String.Empty;
      _index_import = 0;

      try
      {

        if (!GetSoftCountImportFile(out _dt_import))
        {

          Log.Message("An error occurred in function GetSoftCountCollections (GetSoftCountImportFile).");
          return false;
        }

        if (!GetSoftCountMoneyCollection(out _dt_money_collection))
        {

          Log.Message("An error occurred in function GetSoftCountCollections (GetSoftCountMoneyCollection).");
          return false;
        }

        // Get MachineId Type
        _merge_field = MergedField;
        m_money_collection_central.Clear();
        // We search in DataTable MoneyCollections to find matching with _dt_import (XML)
        foreach (DataRow _dt_row in _dt_money_collection.Rows)
        {

          if (CollectionMode == COLLECTION_MODE.MODE_CANCEL &&
              !IsMoneyCollectionCollected((Int64)_dt_row[SQL_COLUMN_COLLECTION_ID]))
          {

            // Remove DataTable Row
            _dt_row.Delete();

            continue;
          }

          _dt_row[SQL_COLUMN_IMPORT_INDEX] = _index_import;
          _index_import++;

          if (_dt_row.IsNull(_merge_field))
          {
            _dt_row[SQL_COLUMN_NON_IMPORT] = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB;
            _dt_row[SQL_COLUMN_CHECKED] = false;

            continue;
          }

          _dr = _dt_import.Select(_merge_field + " = " + _dt_row[_merge_field]);

          if (_dr != null && _dr.Length > 0)
          {

            _dt_row[SQL_COLUMN_COLLECTED_BILLS] = _dr[0][SQL_COLUMN_COLLECTED_BILLS];
            _dt_row[SQL_COLUMN_COLLECTED_TICKETS] = _dr[0][SQL_COLUMN_COLLECTED_TICKETS];

            //COLLECTED
            _dt_row[SQL_COLUMN_COLLECTED_BILL_COUNT] = _dr[0][SQL_COLUMN_COLLECTED_BILL_COUNT];
            _dt_row[SQL_COLUMN_COLLECTED_TICKET_COUNT] = _dr[0][SQL_COLUMN_COLLECTED_TICKET_COUNT];
            _dt_row[SQL_COLUMN_COLLECTED_RE_TICKET_AMOUNT] = _dr[0][SQL_COLUMN_COLLECTED_RE_TICKET_AMOUNT];
            _dt_row[SQL_COLUMN_COLLECTED_RE_TICKET_COUNT] = _dr[0][SQL_COLUMN_COLLECTED_RE_TICKET_COUNT];
            _dt_row[SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_AMOUNT] = _dr[0][SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_AMOUNT];
            _dt_row[SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_COUNT] = _dr[0][SQL_COLUMN_COLLECTED_PROMO_RE_TICKET_COUNT];
            _dt_row[SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_AMOUNT] = _dr[0][SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_AMOUNT];
            _dt_row[SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_COUNT] = _dr[0][SQL_COLUMN_COLLECTED_PROMO_NR_TICKET_COUNT];

            _dt_import.Rows.Remove(_dr[0]);
          }
          else
          {

            _dt_row[SQL_COLUMN_NON_IMPORT] = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB;
            _dt_row[SQL_COLUMN_CHECKED] = false;
          }

          m_money_collection_central.Add((Int64)_dt_row[SQL_COLUMN_COLLECTION_ID]);
        }

        // Add the rest of rows (Rows that not merged with money_collection)
        if (CollectionMode == COLLECTION_MODE.MODE_COLLECT)
        {
          foreach (DataRow _dt_row in _dt_import.Rows)
          {
            _dr_aux = _dt_money_collection.NewRow();

            _dr_aux[SQL_COLUMN_STACKER_ID] = _dt_row[SQL_COLUMN_STACKER_ID];
            _dr_aux[SQL_COLUMN_FLOOR_ID] = _dt_row[SQL_COLUMN_FLOOR_ID];
            _dr_aux[SQL_COLUMN_TERMINAL_ID] = _dt_row[SQL_COLUMN_TERMINAL_ID];
            _dr_aux[SQL_COLUMN_COLLECTED_BILLS] = _dt_row[SQL_COLUMN_COLLECTED_BILLS];
            _dr_aux[SQL_COLUMN_COLLECTED_TICKETS] = _dt_row[SQL_COLUMN_COLLECTED_TICKETS];
            _dr_aux[SQL_COLUMN_EXPECTED_BILLS] = _dt_row[SQL_COLUMN_EXPECTED_BILLS];
            _dr_aux[SQL_COLUMN_EXPECTED_TICKETS] = _dt_row[SQL_COLUMN_EXPECTED_TICKETS];
            _dr_aux[SQL_COLUMN_COLLECTION_ID] = _dt_row[SQL_COLUMN_COLLECTION_ID];
            _dr_aux[SQL_COLUMN_NON_IMPORT] = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML;
            _dr_aux[SQL_COLUMN_CHECKED] = false;
            _dr_aux[SQL_COLUMN_IMPORT_INDEX] = _index_import;
            _index_import++;

            _dt_money_collection.Rows.Add(_dr_aux);
          }
        }
        else // Mode CANCEL
        {

          _dt_money_collection.AcceptChanges();
        }

        _dt_money_collection.DefaultView.Sort = SQL_COLUMN_STACKER_ID + " DESC, " + SQL_COLUMN_COLLECTION_ID + " DESC";
        MergedCollectionSoftCount = _dt_money_collection.DefaultView.ToTable();

        return true;
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return false;
    } // GetSoftCountCollections

    //------------------------------------------------------------------------------
    // PURPOSE : change softcount status
    //
    //  PARAMS :
    //      - INPUT :  
    //          -Int64 IdSoftCount
    //      - OUTPUT :
    //
    // RETURNS : 
    //      - SoftCountStatus
    //
    public SoftCountStatus GetSoftCountStatus(Int64 IdSoftCount, out String Xml)
    {
      StringBuilder _sb;

      Xml = String.Empty;
      _sb = new StringBuilder();

      try
      {
        _sb = new StringBuilder();
        //FJC & JCA 22/05/2015
        _sb.AppendLine("   SELECT   SC_STATUS    ");
        _sb.AppendLine("          , SC_DATA      ");
        _sb.AppendLine("     FROM   SOFT_COUNTS  ");
        _sb.AppendLine("    WHERE   SC_ID = @pId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = IdSoftCount;

            using (SqlDataReader _read = _sql_cmd.ExecuteReader())
            {
              if (_read.Read())
              {
                Xml = _read.GetString(1);
                return (SoftCountStatus)_read.GetInt32(0);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return SoftCountStatus.Deleted;
    } // GetSoftCountStatus

    //------------------------------------------------------------------------------
    // PURPOSE : delete softcount
    //
    //  PARAMS :
    //      - INPUT :   Int64 IdSoftCount
    //                  Int32 CurrentUserId
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public Boolean DeleteSoftCount(Int64 IdSoftCount, Int32 CurrentUserId)
    {
      StringBuilder _sb;
      Int32 _num_rows_updated;

      try
      {
        _sb = new StringBuilder();
        _num_rows_updated = 0;

        _sb.AppendLine("    UPDATE    SOFT_COUNTS                             ");
        _sb.AppendLine("       SET    SC_STATUS = @pStatus                    ");
        _sb.AppendLine("            , SC_DELETE_DATETIME = GETDATE()          ");
        _sb.AppendLine("            , SC_DELETE_USER_ID = @pScrDeletedUserId  ");
        _sb.AppendLine("     WHERE    SC_ID = @pScrId                         ");
        _sb.AppendLine("       AND    SC_STATUS = @pStatusPrev                ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pScrId", SqlDbType.BigInt).Value = IdSoftCount;
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = SoftCountStatus.Deleted;
            _sql_cmd.Parameters.Add("@pScrDeletedUserId", SqlDbType.Int).Value = CurrentUserId;
            _sql_cmd.Parameters.Add("@pStatusPrev", SqlDbType.Int).Value = SoftCountStatus.PendingCollection;

            _num_rows_updated = _sql_cmd.ExecuteNonQuery();
          }
          if (_num_rows_updated == 1)
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DeleteSoftCount

    //------------------------------------------------------------------------------
    // PURPOSE : cancel softcount
    //
    //  PARAMS :
    //      - INPUT :   Int64 IdSoftCount
    //                  string DataXml
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public Boolean CancelSoftCount(Int64 IdSoftCount, ImportedSoftCounts SoftCounts)
    {
      StringBuilder _sb;
      Int32 _num_rows_updated;
      List<Int64> _money_collection;

      try
      {
        // Change Object
        foreach (KeyValuePair<String, SoftCount> _soft_count_item in SoftCounts.m_soft_count)
        {
          _soft_count_item.Value.m_target_collection_id = 0;
          _soft_count_item.Value.m_target_terminal_id = 0;
          _soft_count_item.Value.m_target_stacker_id = 0;
        }

        // Save BD
        _sb = new StringBuilder();
        _num_rows_updated = 0;
        _money_collection = new List<Int64>();

        _sb.AppendLine("    UPDATE   SOFT_COUNTS                       ");
        _sb.AppendLine("       SET   SC_STATUS              = @pStatus ");
        _sb.AppendLine("           , SC_DATA                = @pData   ");
        _sb.AppendLine("           , SC_COLLECTION_DATETIME = NULL     ");
        _sb.AppendLine("           , SC_COLLECTION_USER_ID  = NULL     ");
        _sb.AppendLine("           , SC_TOTAL_BILLS         = NULL     ");
        _sb.AppendLine("           , SC_TOTAL_TICKETS       = NULL     ");
        _sb.AppendLine("           , SC_CAGE_SESSION_ID     = NULL     ");
        _sb.AppendLine("           , SC_MONEY_COLLECTIONS   = @pMoney  ");
        _sb.AppendLine("     WHERE   SC_ID = @pIdSoftCount             ");
        _sb.AppendLine("       AND   SC_STATUS = @pStatusPrev          ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pIdSoftCount", SqlDbType.BigInt).Value = IdSoftCount;
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = SoftCountStatus.PendingCollection;
            _sql_cmd.Parameters.Add("@pData", SqlDbType.Xml).Value = SoftCounts.SerializationSoftCount(SoftCounts.m_soft_count);
            _sql_cmd.Parameters.Add("@pMoney", SqlDbType.Xml).Value = SoftCounts.Serialize(_money_collection);
            _sql_cmd.Parameters.Add("@pStatusPrev", SqlDbType.Int).Value = SoftCountStatus.Collected;

            _num_rows_updated = _sql_cmd.ExecuteNonQuery();
          }
          if (_num_rows_updated == 1)
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CancelSoftCount

    //------------------------------------------------------------------------------
    // PURPOSE : cancel softcount
    //
    //  PARAMS :
    //      - INPUT :   Int64 IdSoftCount
    //                  string DataXml
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public Boolean UpdateSoftCount(Int64 IdSoftCount, ImportedSoftCounts SoftCounts)
    {
      StringBuilder _sb;
      Int32 _num_rows_updated;

      try
      {

        // Save BD
        _sb = new StringBuilder();
        _num_rows_updated = 0;

        _sb.AppendLine("    UPDATE   SOFT_COUNTS             ");
        _sb.AppendLine("       SET   SC_DATA = @pData        ");
        _sb.AppendLine("     WHERE   SC_ID   = @pIdSoftCount ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pIdSoftCount", SqlDbType.BigInt).Value = IdSoftCount;
            _sql_cmd.Parameters.Add("@pData", SqlDbType.Xml).Value = SoftCounts.SerializationSoftCount(SoftCounts.m_soft_count);

            _num_rows_updated = _sql_cmd.ExecuteNonQuery();
          }
          if (_num_rows_updated == 1)
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CancelSoftCount

    #endregion " Public Functions "

    #region " Section Edit"

    //------------------------------------------------------------------------------
    // PURPOSE : Change Id's by stacker or floorId when we are editing rows. 
    //           Changes are in Merged DataTable.
    //
    //  PARAMS :
    //      - INPUT :   String NewSoftCountId
    //                  Int32 Index (Index of the datatable row)
    //
    //      - OUTPUT :  DataTable DtResult
    //                  
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public SOFTCOUNT_EDIT_RESULT EditSoftCountMergedRows(String NewSoftCountId, Int32 Index, out DataTable DtResult)
    {
      DataTable DtOriginalValues;
      DataRow _dr_old;
      DataRow _dr_new;
      DataRow[] _dr_find;
      DataRow[] _dr_tmp_change_values;
      String _old_value;
      Int32 _import_index_new;
      STATUS_DATA_CENTRAL _status;
      Boolean _check_delete;

      DtResult = new DataTable();
      DtOriginalValues = new DataTable();
      _status = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK;
      _check_delete = false;

      try
      {
        _dr_old = null;
        _dr_new = null;
        _dr_tmp_change_values = null;
        _old_value = string.Empty;
        _import_index_new = -1;

        // Order by Collection Id Asc to math with the most older
        MergedCollectionSoftCount.DefaultView.Sort = SQL_COLUMN_COLLECTION_ID + " ASC";
        MergedCollectionSoftCount = MergedCollectionSoftCount.DefaultView.ToTable();

        DtOriginalValues = MergedCollectionSoftCount.Clone();

        //1. Check if NewSoftCountId are in BD (
        _dr_find = MergedCollectionSoftCount.Select(MergedField + " = " + NewSoftCountId);

        //2. If NewSoftcountId Exists, continue
        if (_dr_find != null && _dr_find.Length > 0)
        {
          DtOriginalValues.ImportRow(_dr_find[0]);
          _dr_new = DtOriginalValues.Rows[0];

          // Order by Import_Index to match Row Index with Import Index
          MergedCollectionSoftCount.DefaultView.Sort = SQL_COLUMN_IMPORT_INDEX + " ASC";
          MergedCollectionSoftCount = MergedCollectionSoftCount.DefaultView.ToTable();

          _dr_find = MergedCollectionSoftCount.Select(SQL_COLUMN_IMPORT_INDEX + " = " + Index);

          if (_dr_find != null && _dr_find.Length > 0)
          {
            if (!_dr_find[0].IsNull(MergedField))
            {

              DtOriginalValues.ImportRow(_dr_find[0]);
              _dr_old = DtOriginalValues.Rows[1];

              // Not available change only file with only file 
              if ((STATUS_DATA_CENTRAL)_dr_new[SQL_COLUMN_NON_IMPORT] == STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML
               && (STATUS_DATA_CENTRAL)_dr_old[SQL_COLUMN_NON_IMPORT] == STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML
                )
              {
                return SOFTCOUNT_EDIT_RESULT.EDIT_STATUS_ID_NOT_EXISTS;
              }

              _old_value = _dr_old[MergedField].ToString();

              _import_index_new = (Int32)_dr_new[SQL_COLUMN_IMPORT_INDEX];

              // Pass Modifieds to a DataTable (Only CloneTable)
              DtResult = MergedCollectionSoftCount.Clone();

              // * NEW VALUE
              _dr_tmp_change_values = MergedCollectionSoftCount.Select(SQL_COLUMN_IMPORT_INDEX + " = " + _dr_old[SQL_COLUMN_IMPORT_INDEX]);
              // Change Values
              _dr_tmp_change_values[0][SQL_COLUMN_EXPECTED_BILLS] = _dr_new[SQL_COLUMN_EXPECTED_BILLS];
              _dr_tmp_change_values[0][SQL_COLUMN_EXPECTED_TICKETS] = _dr_new[SQL_COLUMN_EXPECTED_TICKETS];
              _dr_tmp_change_values[0][SQL_COLUMN_TERMINAL_ID] = _dr_new[SQL_COLUMN_TERMINAL_ID];
              _dr_tmp_change_values[0][SQL_COLUMN_FLOOR_ID] = _dr_new[SQL_COLUMN_FLOOR_ID];
              _dr_tmp_change_values[0][SQL_COLUMN_COLLECTION_ID] = _dr_new[SQL_COLUMN_COLLECTION_ID];
              _dr_tmp_change_values[0][MergedField] = _dr_new[MergedField];

              // Set Status
              _status = GetNewStatusCollection(_dr_new[MergedField].ToString(), _dr_new[SQL_COLUMN_COLLECTION_ID].ToString(), (STATUS_DATA_CENTRAL)_dr_new[SQL_COLUMN_NON_IMPORT]);
              _dr_tmp_change_values[0][SQL_COLUMN_NON_IMPORT] = _status;
              _dr_tmp_change_values[0][SQL_COLUMN_CHECKED] = (_status == STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK) ? true : false;

              // Delete merged detail
              SoftCountObject.m_merged_details.Remove((Int32)_dr_old[SQL_COLUMN_IMPORT_INDEX]);

              // Pass Modified to a DataTable
              DtResult.ImportRow(_dr_tmp_change_values[0]);

              // * OLD VALUE
              _dr_tmp_change_values = MergedCollectionSoftCount.Select(SQL_COLUMN_IMPORT_INDEX + " = " + _dr_new[SQL_COLUMN_IMPORT_INDEX]);
              // Change Values
              _dr_tmp_change_values[0][SQL_COLUMN_EXPECTED_BILLS] = _dr_old[SQL_COLUMN_EXPECTED_BILLS];
              _dr_tmp_change_values[0][SQL_COLUMN_EXPECTED_TICKETS] = _dr_old[SQL_COLUMN_EXPECTED_TICKETS];
              _dr_tmp_change_values[0][SQL_COLUMN_TERMINAL_ID] = _dr_old[SQL_COLUMN_TERMINAL_ID];
              _dr_tmp_change_values[0][SQL_COLUMN_FLOOR_ID] = _dr_old[SQL_COLUMN_FLOOR_ID];
              _dr_tmp_change_values[0][SQL_COLUMN_COLLECTION_ID] = _dr_old[SQL_COLUMN_COLLECTION_ID];
              _dr_tmp_change_values[0][MergedField] = _dr_old[MergedField];

              //We change Id's (stackerId) in obj Softcount
              if (EditSoftCountObj(_old_value, NewSoftCountId))
              {

                _check_delete = true;
              }

              // Set Status
              _status = GetNewStatusCollection(_dr_old[MergedField].ToString(), _dr_old[SQL_COLUMN_COLLECTION_ID].ToString(), (STATUS_DATA_CENTRAL)_dr_old[SQL_COLUMN_NON_IMPORT]);
              _dr_tmp_change_values[0][SQL_COLUMN_NON_IMPORT] = _status;
              _dr_tmp_change_values[0][SQL_COLUMN_CHECKED] = (_status == STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK) ? true : false;

              // Delete merged detail
              SoftCountObject.m_merged_details.Remove((Int32)_dr_new[SQL_COLUMN_IMPORT_INDEX]);

              // Pass Modified to a DataTable
              DtResult.ImportRow(_dr_tmp_change_values[0]);

              // Set Order by Collection Id Asc
              MergedCollectionSoftCount.DefaultView.Sort = SQL_COLUMN_STACKER_ID + " DESC, " + SQL_COLUMN_COLLECTION_ID + " DESC";
              MergedCollectionSoftCount = MergedCollectionSoftCount.DefaultView.ToTable();

              if (_check_delete)
              {
                CheckToDeleteIfOnlyFileAndZeroValue(DtResult);
                return SOFTCOUNT_EDIT_RESULT.EDIT_STATUS_OK;
              }
            }
          }

          return SOFTCOUNT_EDIT_RESULT.EDIT_STATUS_ERROR;
        }
        else
        {
          return SOFTCOUNT_EDIT_RESULT.EDIT_STATUS_ID_NOT_EXISTS;
        }
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);

      }

      return SOFTCOUNT_EDIT_RESULT.EDIT_STATUS_ERROR;
    } // EditSoftCountMergedRows

    //------------------------------------------------------------------------------
    // PURPOSE : Change Id's by stacker or floorId when we are editing rows
    //           Changes are in SoftCount Object (Dictionary of SoftCounts).
    //  PARAMS :
    //      - INPUT :   String PreviousId
    //                  String NewId
    //      - OUTPUT :  
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public Boolean EditSoftCountObj(String PreviousId, String NewId)
    {

      SoftCount _sof_aux_return1;
      SoftCount _sof_aux_return2;

      try
      {
        _sof_aux_return1 = new SoftCount();
        _sof_aux_return2 = new SoftCount();

        if (SoftCountObject.m_soft_count.TryGetValue(NewId, out _sof_aux_return1) &&
            SoftCountObject.m_soft_count.TryGetValue(PreviousId, out _sof_aux_return2))
        {

          _sof_aux_return1.m_soft_count_machine_id = PreviousId;
          _sof_aux_return2.m_soft_count_machine_id = NewId;

          SoftCountObject.m_soft_count.Remove(NewId);
          SoftCountObject.m_soft_count.Remove(PreviousId);

          SoftCountObject.m_soft_count.Add(NewId, _sof_aux_return2);
          SoftCountObject.m_soft_count.Add(PreviousId, _sof_aux_return1);

        }
        else
        {
          if (SoftCountObject.m_soft_count.TryGetValue(PreviousId, out _sof_aux_return1))
          {

            _sof_aux_return1.m_soft_count_machine_id = NewId;
            SoftCountObject.m_soft_count.Remove(PreviousId);
            SoftCountObject.m_soft_count.Add(NewId, _sof_aux_return1);
          }
        }

        return true;
      }

      catch (Exception _ex)
      {

        Log.Exception(_ex);

      }

      return false;
    } // EditSoftCountObj

    //------------------------------------------------------------------------------
    // PURPOSE : Get Status (STATUS_DATA_CENTRAL) for one collection
    //           Changes are in SoftCount Object (Dictionary of SoftCounts).
    //  PARAMS :
    //      - INPUT :
    //          - String FileId (By Stacker Id or FloorId)
    //          - String CollectionId
    //          - STATUS_DATA_CENTRAL StatusOld
    //
    //      - OUTPUT :  
    // RETURNS : 
    //
    //   NOTES : STATUS_DATA_CENTRAL
    //
    private STATUS_DATA_CENTRAL GetNewStatusCollection(String FileId, String CollectionId, STATUS_DATA_CENTRAL StatusOld)
    {
      SoftCount _soft_count_result;

      try
      {
        if (String.IsNullOrEmpty(CollectionId))
        {
          return STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML;
        }

        if (StatusOld == STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB)
        {
          return STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK;
        }

        _soft_count_result = new SoftCount();
        if (this.SoftCountObject.m_soft_count.TryGetValue(FileId, out _soft_count_result))
        {

          return STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK;
        }
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB;
    } // GetNewStatusCollection

    //------------------------------------------------------------------------------
    // PURPOSE : when do changes, check to delete row when the register is only file and not in system
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable DtResult
    //
    //      - OUTPUT :  
    // RETURNS : 
    //
    //   NOTES :
    //
    private void CheckToDeleteIfOnlyFileAndZeroValue(DataTable DtResult)
    {
      DataRow[] _dr_old;
      _dr_old = null;

      try
      {
        foreach (DataRow _row in DtResult.Rows)
        {
          if ((STATUS_DATA_CENTRAL)_row[SQL_COLUMN_NON_IMPORT] == STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML)
          {
            if ((Convert.ToDecimal(_row[SQL_COLUMN_COLLECTED_BILLS]) + Convert.ToDecimal(_row[SQL_COLUMN_COLLECTED_TICKETS])
               + Convert.ToDecimal(_row[SQL_COLUMN_EXPECTED_BILLS]) + Convert.ToDecimal(_row[SQL_COLUMN_EXPECTED_TICKETS])) <= 0)
            {
              // To change on screen
              _row[SQL_COLUMN_NON_IMPORT] = STATUS_DATA_CENTRAL.STATUS_DELETED_ROW;

              // Delete row on merged table
              _dr_old = MergedCollectionSoftCount.Select(SQL_COLUMN_IMPORT_INDEX + " = " + _row[SQL_COLUMN_IMPORT_INDEX].ToString());
              if (_dr_old != null && _dr_old.Length > 0)
              {
                _dr_old[0].Delete();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // CheckToDeleteIfOnlyFileAndZeroValue

    #endregion " Section Edit"

    #region " Section Collect "

    //------------------------------------------------------------------------------
    // PURPOSE : Set List of collections id's and check if collection is balanced or unbalanced, 
    //           by total amount or by denomination.
    //
    //  PARAMS :
    //      - INPUT :   List<Int64> CollectionIdList
    //      - OUTPUT :  Dictionary<Int64, Cage.CageStatus>
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    public Boolean GetCollectedSoftCountWithStatus(List<Int64> CollectionIdList,
                                                   out Dictionary<Int64, Cage.CageStatus> CollectionForCollect)
    {
      SoftCount _soft_count;
      DataRow[] _dr_find;
      String _id_find_softcount;
      SOFT_COUNT_DATA_DETAIL _soft_count_data_detail;
      Cage.CageStatus _status_collection;

      CollectionForCollect = new Dictionary<long, Cage.CageStatus>();

      try
      {
        _soft_count = new SoftCount();
        _id_find_softcount = String.Empty;
        _soft_count_data_detail = new SOFT_COUNT_DATA_DETAIL();
        _status_collection = Cage.CageStatus.OK;

        foreach (Int32 _collection_index in CollectionIdList)
        {

          // Get Colllection in merged table by IMPORT_INDEX
          _dr_find = MergedCollectionSoftCount.Select(SQL_COLUMN_COLLECTION_ID + " = " + _collection_index);

          if (_dr_find != null && _dr_find.Length > 0)
          {
            // Get Id's 
            _soft_count_data_detail.CollectionId = Convert.ToInt64(_dr_find[0][SQL_COLUMN_COLLECTION_ID]);
            _soft_count_data_detail.StackerId = Convert.ToInt64(_dr_find[0].IsNull(SQL_COLUMN_STACKER_ID) ? 0 : _dr_find[0][SQL_COLUMN_STACKER_ID]);
            _soft_count_data_detail.TerminalId = Convert.ToInt32(_dr_find[0][SQL_COLUMN_TERMINAL_ID]);

            _id_find_softcount = _dr_find[0][MergedField].ToString();

            // Check Total Amounts (bills && tickets)
            if (Convert.ToDecimal(_dr_find[0][SQL_COLUMN_COLLECTED_BILLS]) != Convert.ToDecimal(_dr_find[0][SQL_COLUMN_EXPECTED_BILLS]) ||
                Convert.ToDecimal(_dr_find[0][SQL_COLUMN_COLLECTED_TICKETS]) != Convert.ToDecimal(_dr_find[0][SQL_COLUMN_EXPECTED_TICKETS]))
            {
              // if there are differences: Set Status OkAmountImbalance
              _status_collection = Cage.CageStatus.OkAmountImbalance;
            }
            else
            {

              // Check if balanced or unbalanced BY DETAIL (bills && tickets)
              if (!CheckBalanceUnBalanceDetail(_soft_count_data_detail, out _status_collection))
              {
                Log.Message("An error occurred in CheckBalanceUnBalanceByDenomination() function.");

                return false;
              }
            }

            // Fill Dictionary with relation (CollectionId - Status)
            CollectionForCollect.Add(_collection_index, _status_collection);
          }
        }

        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Check if a collection is balanced or unbalanced with detail (bydenomination && ticket status)
    //
    //  PARAMS :
    //      - INPUT :   List<Int64> CollectionIdList
    //      - OUTPUT :
    // RETURNS : 
    //
    //   NOTES : Boolean
    //
    private Boolean CheckBalanceUnBalanceDetail(SOFT_COUNT_DATA_DETAIL SoftCountDataDetail,
                                                out Cage.CageStatus _status_collection)
    {
      DataTable _result_detail;

      _status_collection = WSI.Common.Cage.CageStatus.OK;
      _result_detail = new DataTable();

      try
      {
        if (GetSoftCountCollectionsDetail(SoftCountDataDetail, false, ref _result_detail))
        {
          foreach (DataRow _dt_row in _result_detail.Rows)
          {

            // Check BILLS
            if (_dt_row[SQL_COLUMN_TYPE].ToString() == "1")
            {

              // If Expected is null and Collected is not null ==> Cage.CageStatus.OkDenominationImbalance
              if ((_dt_row.IsNull(SQL_COLUMN_EXPECTED_BILLS_QUANTITY) &&
                 !_dt_row.IsNull(SQL_COLUMN_COLLECTED_BILLS_QUANTITY) &&
                 Convert.ToInt32(_dt_row[SQL_COLUMN_COLLECTED_BILLS_QUANTITY]) > 0) ||
                 (_dt_row.IsNull(SQL_COLUMN_COLLECTED_BILLS_QUANTITY) &&
                 !_dt_row.IsNull(SQL_COLUMN_EXPECTED_BILLS_QUANTITY) &&
                 Convert.ToInt32(_dt_row[SQL_COLUMN_EXPECTED_BILLS_QUANTITY]) > 0))
              {
                _status_collection = Cage.CageStatus.OkDenominationImbalance;

                return true;
              }

              //If Collected >0 && Collected != Expected ==> Cage.CageStatus.OkDenominationImbalance
              if (Convert.ToInt32(_dt_row[SQL_COLUMN_COLLECTED_BILLS_QUANTITY]) > 0 &&
                  Convert.ToInt32(_dt_row[SQL_COLUMN_COLLECTED_BILLS_QUANTITY]) != Convert.ToInt32(_dt_row[SQL_COLUMN_EXPECTED_BILLS_QUANTITY]))
              {
                _status_collection = Cage.CageStatus.OkDenominationImbalance;

                return true;
              }
            }

            // Check TICKETS
            if (_dt_row[SQL_COLUMN_TYPE].ToString() == "0")
            {
              // Check TICKETS
              if ((STATUS_TICKET)_dt_row[SQL_COLUMN_TICKET_STATUS] != STATUS_TICKET.TICKET_STATUS_OK)
              {
                _status_collection = Cage.CageStatus.OkDenominationImbalance;

                return true;
              }
            }
          }
        }

        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    //FJC

    //------------------------------------------------------------------------------
    // PURPOSE : Check Stacker, or TerminalId by MoneyCollection
    //
    //  PARAMS :
    //      - INPUT :   Int64 MoneyCollectionID
    //      - OUTPUT :
    // RETURNS : String with Id
    //
    //   NOTES : 
    //
    private String GetStackerByMC(Int64 MoneyCollectionID)
    {
      DataRow[] _merged_row;
      String _soft_count_id;

      try
      {

        _soft_count_id = "-1";
        _merged_row = MergedCollectionSoftCount.Select(SQL_COLUMN_COLLECTION_ID + " = " + MoneyCollectionID);

        if (_merged_row.Length != 1)
        {
          return _soft_count_id;
        }

        switch (this.MachineIdType)
        {
          case ImportId.StackerId:
            _soft_count_id = _merged_row[0][SQL_COLUMN_STACKER_ID].ToString();
            break;
          case ImportId.EGMBaseName:
            _soft_count_id = _merged_row[0][SQL_COLUMN_TERMINAL_ID].ToString();
            break;
          case ImportId.EGMFloorId:
            _soft_count_id = _merged_row[0][SQL_COLUMN_FLOOR_ID].ToString();
            break;
          default:
            _soft_count_id = "-1";
            break;
        }

        return _soft_count_id;
      }

      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return String.Empty;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Check Stacker, or TerminalId by MoneyCollection
    //
    //  PARAMS :
    //      - INPUT :   Int64 MoneyCollectionID
    //      - OUTPUT :
    // RETURNS : String with Id
    //
    //   NOTES : 
    //
    private Int32 GetTerminalIdByMC(Int64 MoneyCollectionID)
    {
      DataRow[] _merged_row;

      try
      {
        _merged_row = MergedCollectionSoftCount.Select(SQL_COLUMN_COLLECTION_ID + " = " + MoneyCollectionID);

        if (_merged_row.Length != 1)
        {
          return -1;
        }

        return Convert.ToInt32(_merged_row[0][SQL_COLUMN_TERMINAL_ID]);
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return -1;

    }


    //------------------------------------------------------------------------------
    // PURPOSE : Get CollectionsDetails (Detail of collection && Cage Stock)
    //
    //  PARAMS :
    //      - INPUT :   List<Int64> Moneycollections
    //      - OUTPUT :  DataTable CollectionDetails
    //                  DataTable CageStock
    // RETURNS : Boolean
    //
    //   NOTES : 
    //
    public Boolean GetAllCollectionDetails(List<Int64> Moneycollections,
                                           out DataTable CollectionDetails,
                                           out DataTable CageStock)
    {
      SOFT_COUNT_DATA_DETAIL _soft_count_data_detail;
      DataTable _dt_detail_for_collect;

      CollectionDetails = new DataTable();
      _soft_count_data_detail = new SOFT_COUNT_DATA_DETAIL();
      _dt_detail_for_collect = new DataTable();
      CageStock = null;

      try
      {

        foreach (Int64 _mc in Moneycollections)
        {

          _soft_count_data_detail.CollectionId = _mc;
          switch (this.MachineIdType)
          {
            case ImportId.StackerId:
              _soft_count_data_detail.StackerId = Convert.ToInt64(GetStackerByMC(_mc));
              break;
            case ImportId.EGMBaseName:
            case ImportId.EGMFloorId:
              _soft_count_data_detail.TerminalId = GetTerminalIdByMC(_mc);
              break;
            default:
              _soft_count_data_detail.StackerId = -1;
              _soft_count_data_detail.TerminalId = -1;
              break;
          }

          if (!GetSoftCountCollectionsDetail(_soft_count_data_detail, true, ref _dt_detail_for_collect))
          {
            Log.Message("An error occurred in function GetSoftCountCollectionsDetail().");

            return false;
          }

          if (!GetAllSumCollectionDenom(_dt_detail_for_collect, ref CageStock))
          {
            Log.Message("An error occurred in function GetAllSumCollectionDenom().");

            return false;
          }

          CollectionDetails.Merge(_dt_detail_for_collect);

        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get Sum (Quantity) Denominations
    //
    //  PARAMS :
    //      - INPUT :   Int64 MoneyCollectionID
    //      - OUTPUT :
    // RETURNS : String with Id
    //
    //   NOTES : 
    //
    private Boolean GetAllSumCollectionDenom(DataTable Denominations,
                                             ref DataTable SumDenominations)
    {
      DataRow[] _dr;
      DataRow _dr_new_sum_denom;
      Int32 _idx_row;
      Int32 _quantity;

      Decimal _denom_find;

      try
      {
        _denom_find = 0;
        _quantity = 0;

        if (SumDenominations == null)
        {
          SumDenominations = new DataTable();
          SumDenominations.Columns.Add(SQL_COLUMN_ISO_CODE, Type.GetType("System.String"));
          SumDenominations.Columns.Add(SQL_COLUMN_DENOMINATION, Type.GetType("System.Decimal"));
          SumDenominations.Columns.Add(SQL_COLUMN_QUANTITY, Type.GetType("System.Decimal"));
        }

        foreach (DataRow _dr_aux in Denominations.Rows)
        {

          if (_dr_aux["TYPE"].ToString() == "1" || _dr_aux["TYPE"].ToString() == "0")
          {

            if ((_dr_aux["TYPE"].ToString() == "1" && !Decimal.TryParse(Format.LocalNumberToDBNumber(_dr_aux[SQL_COLUMN_DENOMINATION_NUMERIC].ToString()), out _denom_find)) ||
                 (_dr_aux["TYPE"].ToString() == "0" && !Decimal.TryParse(Format.LocalNumberToDBNumber(Cage.TICKETS_CODE.ToString()), out _denom_find)))
            {
              Log.Message("An error occurred in GetAllSumCollectionDenom().");

              return false;
            }

            _dr = SumDenominations.Select(SQL_COLUMN_DENOMINATION + " = " + _denom_find);
            if (_dr != null && _dr.Length > 0)
            {

              _idx_row = SumDenominations.Rows.IndexOf(_dr[0]);

            }
            else
            {
              _dr_new_sum_denom = SumDenominations.NewRow();
              _dr_new_sum_denom[SQL_COLUMN_ISO_CODE] = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
              _dr_new_sum_denom[SQL_COLUMN_DENOMINATION] = _denom_find;
              _dr_new_sum_denom[SQL_COLUMN_QUANTITY] = 0;
              SumDenominations.Rows.Add(_dr_new_sum_denom);

            }

            _dr = SumDenominations.Select(SQL_COLUMN_DENOMINATION + " = " + _denom_find);
            if (_dr != null && _dr.Length > 0)
            {

              _idx_row = SumDenominations.Rows.IndexOf(_dr[0]);

              if (_dr_aux["TYPE"].ToString() == "1") // BILLS
              {

                // BILLS
                _quantity = Convert.ToInt32(SumDenominations.Rows[_idx_row][SQL_COLUMN_QUANTITY]);
                if (!_dr_aux.IsNull(CollectionImport.SofCountData.SQL_COLUMN_COLLECTED_BILLS_QUANTITY))
                {
                  _quantity += Convert.ToInt32(_dr_aux[CollectionImport.SofCountData.SQL_COLUMN_COLLECTED_BILLS_QUANTITY]);
                }
              }
              else
              {
                // TICKETS
                _quantity += Convert.ToInt32(SumDenominations.Rows[_idx_row][SQL_COLUMN_QUANTITY]);
              }
              SumDenominations.Rows[_idx_row][SQL_COLUMN_QUANTITY] = _quantity;

            }
          }

          _quantity = 0;
        }
        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //FJC

    #endregion " Section Collect "

  } // SofCountData

} // WSI.Common.CollectionImport