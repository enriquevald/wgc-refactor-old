//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SoftCountDefinitions.cs
// 
//   DESCRIPTION: Several Classes and enums to manage soft count import
// 
//        AUTHOR: David Rigal
// 
// CREATION DATE: 11-MAY-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAY-2015 DRV    Initial Version
// 11-JUN-2015 SGB    Insert new importEvent enum

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.CollectionImport
{
  public delegate void ImportedReadEventHandler(IImported e);

  public enum ImportedType
  {
    Error = 0,
    SoftCount = 1,
    Event = 2,
  }

  public enum ImportEvent
  {
    File1NotFound = 1,
    File2NotFound = 2,
    ValidationFinishedFile1 = 3,
    ValidationFinishedFile2 = 4,
    ImportationFinishedFile1 = 5,
    ImportationFinishedFile2 = 6,
    ValidationStartedFile1 = 7,
    ValidationStartedFile2 = 8,
    ImportationStartedFile1 = 9,
    ImportationStartedFile2 = 10
  }

  public enum TYPE_ROW_TICKET_BILL
  {
    TICKET = 0,
    BILL = 1,
    TOTAL_TICKET = 2,
  } // TYPE_ROW_TICKET_BILL

  public enum STATUS_TICKET
  {
    TICKET_STATUS_OK = 0,       //EXISTS in DB and is not collected (Tickets) and EXISTS in XML
    TICKET_STATUS_ERROR = 1,    //EXISTS in XML and NOT EXISTS in DB (Tickets) or any error.
    TICKET_STATUS_PENDING = 2   //EXISTS in DB and not is Collected (Tickets) and NOT EXISTS in XML)
  } // STATUS_TICKET

  public enum STATUS_DATA_CENTRAL
  {
    STATUS_DATA_CENTRAL_OK = 0,
    STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB = 1,
    STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML = 2,
    STATUS_DATA_UNBALANCED = 3,
    STATUS_DELETED_ROW = 4
  } // STATUS_DATA_CENTRAL

  public enum COLLECTION_MODE
  {
    MODE_COLLECT = 0, //Defaul Mode (Show all)
    MODE_CANCEL = 1   //Cancel Mode (Show only collected)
  } // COLLECTION_MODE

  public enum SOFTCOUNT_EDIT_RESULT
  {
    EDIT_STATUS_OK,                 // All went Ok
    EDIT_STATUS_ID_NOT_EXISTS = 1,  // Stacker Id, FloorId, etc. Not exists in system (in DB)
    EDIT_STATUS_ERROR = 2           // Any error

  }
}
