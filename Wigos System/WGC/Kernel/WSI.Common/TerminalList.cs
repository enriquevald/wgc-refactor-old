//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalList.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Marcos Piedra Osuna
// 
// CREATION DATE: 21-AUG-2013
// 
// REVISION HISTORY:
// 
// Date        Author           Description
// ----------- ------           ------------------------------------------------
// 21-AUG-2013 MPO              Initial draft.
// 12-MAR-2014 MPO & JBC        Fixed Bug WIG-722: New function for export xml
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Data.SqlClient;

namespace WSI.Common
{
  public class TerminalList
  {
    public enum TerminalListType
    {
      All = 0,
      Listed = 1,
      NotListed = 2,
    }

    DataTable m_type = null;
    DataTable m_provider = null;
    DataTable m_zone = null;
    DataTable m_area = null;
    DataTable m_bank = null;
    DataTable m_terminal = null;
    DataTable m_group = null;

    DataSet m_ds = null;

    #region Constants

    public const String COLUMN_ID = "Id";
    public const String COLUMN_NAME = "Name";

    #endregion

    # region Constructors

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor for a TerminalList instance
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public TerminalList()
    {
      Init();
    } // TerminalList

    #endregion // Constructors

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Set the type of the TerminalList.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void SetListType(Int32 Type)
    {
      m_type.Rows[0][0] = (TerminalListType)Type;
    } // SetListType

    //------------------------------------------------------------------------------
    // PURPOSE: Get the type of the TerminalList.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - TerminalListType Type
    // 
    public TerminalListType ListType
    {
      get
      {
        return ((TerminalListType)m_type.Rows[0][0]);
      }
      set
      {
        m_type.Rows[0][0] = value;
        CleanList();
      }
    } // ListType

    //------------------------------------------------------------------------------
    // PURPOSE: Get the modifications of the TerminalList.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List
    // 
    public String AuditString(String All, String Listed, String Excluded, String None)
    {
      String _prefix;
      StringBuilder _list;

      switch ((TerminalListType)m_type.Rows[0][0])
      {
        case TerminalListType.All:
          return All;

        case TerminalListType.Listed:
          _prefix = Listed;
          break;

        case TerminalListType.NotListed:
          _prefix = Excluded;
          break;

        default:
          throw new Exception("Unknown TerminalListType: " + (Int32)m_type.Rows[0][0]);
      }


      _list = new StringBuilder();

      AuditStringList(m_group, _list);
      AuditStringList(m_provider, _list);
      AuditStringList(m_zone, _list);
      AuditStringList(m_area, _list);
      AuditStringList(m_bank, _list);
      AuditStringList(m_terminal, _list);

      return _prefix + ": " + (_list.ToString() != string.Empty ? _list.ToString() : None);

    } // AuditString

    //------------------------------------------------------------------------------
    // PURPOSE: Get the modifications of the type.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - All String: Title for type all
    //          - Listed String: Title for type selected in list
    //          - Excluded String: Title for type excluded in list
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    public String AuditStringType(String All, String Listed, String Excluded)
    {
      String _prefix;

      switch ((TerminalListType)m_type.Rows[0][0])
      {
        case TerminalListType.All:
          return All;

        case TerminalListType.Listed:
          _prefix = Listed;
          break;

        case TerminalListType.NotListed:
          _prefix = Excluded;
          break;

        default:
          throw new Exception("Unknown TerminalListType: " + (Int32)m_type.Rows[0][0]);
      }

      return _prefix;
    } // AuditStringType

    //------------------------------------------------------------------------------
    // PURPOSE: Get the modifications of a list of the TerminalList.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List
    // 
    public void AuditStringList(DataTable Dt, StringBuilder List)
    {
      foreach (DataRow _dr in Dt.Rows)
      {
        if (List.Length > 0)
        {
          List.Append(", ");
        }
        List.Append(((String)_dr[0]).ToUpper());
      }
    } // AuditStringList

    //------------------------------------------------------------------------------
    // PURPOSE:  Get the modifications of a list of elements.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Type GROUP_ELEMENT_TYPE: list type
    //          - None String: Title for no list
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    public String AuditStringList(WSI.Common.GROUP_ELEMENT_TYPE Type, String None)
    {
      DataTable _dt;
      StringBuilder _list;

      switch (Type)
      {
        case GROUP_ELEMENT_TYPE.GROUP:
          _dt = m_group;
          break;
        case GROUP_ELEMENT_TYPE.PROV:
          _dt = m_provider;
          break;
        case GROUP_ELEMENT_TYPE.ZONE:
          _dt = m_zone;
          break;
        case GROUP_ELEMENT_TYPE.AREA:
          _dt = m_area;
          break;
        case GROUP_ELEMENT_TYPE.BANK:
          _dt = m_bank;
          break;
        case GROUP_ELEMENT_TYPE.TERM:
          _dt = m_terminal;
          break;
        default:
          _dt = null;
          break;
      }

      _list = new StringBuilder();
      AuditStringList(_dt, _list);

      return _list.ToString() != string.Empty ? _list.ToString() : None;
    } // AuditStringList

    public List<String> AuditStringList(Dictionary<GROUP_ELEMENT_TYPE, String> TypeList)
    {
      List<String> _ls;
      DataTable _dt;
      DataColumn _dc;

      _ls = new List<String>();

      _dt = new DataTable();
      _dc = _dt.Columns.Add("Type", Type.GetType("System.Int32"));
      _dt.Columns.Add(COLUMN_ID, Type.GetType("System.Int64"));
      _dt.Columns.Add(COLUMN_NAME, Type.GetType("System.String"));

      try
      {
        _dc.DefaultValue = GROUP_ELEMENT_TYPE.GROUP;
        _dt.Merge(m_group);

        _dc.DefaultValue = GROUP_ELEMENT_TYPE.PROV;
        _dt.Merge(m_provider);

        _dc.DefaultValue = GROUP_ELEMENT_TYPE.ZONE;
        _dt.Merge(m_zone);

        _dc.DefaultValue = GROUP_ELEMENT_TYPE.AREA;
        _dt.Merge(m_area);

        _dc.DefaultValue = GROUP_ELEMENT_TYPE.BANK;
        _dt.Merge(m_bank);

        _dc.DefaultValue = GROUP_ELEMENT_TYPE.TERM;
        _dt.Merge(m_terminal);

        foreach (DataRow _row in _dt.Select("", "Type," + COLUMN_ID + "," + COLUMN_NAME))
        {
          _ls.Add(TypeList[(GROUP_ELEMENT_TYPE)_row["Type"]] + (String)_row[COLUMN_NAME]);
        }

      }
      catch { }

      return _ls;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Add a new element.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Terminal
    //          - Int32 Id
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void AddElement(String ElementName, Int32 ElementId, WSI.Common.GROUP_ELEMENT_TYPE Type)
    {
      DataRow _dr;
      DataTable _dt;

      if ((TerminalListType)m_type.Rows[0][0] == TerminalListType.All)
      {
        return;
      }

      if (SeekByType(ElementId, Type) != null)
      {
        return;
      }

      switch (Type)
      {
        case GROUP_ELEMENT_TYPE.GROUP:
          _dt = m_group;
          break;
        case GROUP_ELEMENT_TYPE.PROV:
          _dt = m_provider;
          break;
        case GROUP_ELEMENT_TYPE.ZONE:
          _dt = m_zone;
          break;
        case GROUP_ELEMENT_TYPE.AREA:
          _dt = m_area;
          break;
        case GROUP_ELEMENT_TYPE.BANK:
          _dt = m_bank;
          break;
        case GROUP_ELEMENT_TYPE.TERM:
          _dt = m_terminal;
          break;
        default:
          _dt = null;
          break;
      }

      if (_dt != null)
      {
        _dr = _dt.NewRow();
        _dr[0] = ElementName;
        _dr[1] = ElementId;
        _dt.Rows.Add(_dr);
      }
    } // AddElement

    //------------------------------------------------------------------------------
    // PURPOSE: Remove a element.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Terminal
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void RemoveElement(Int32 Element, WSI.Common.GROUP_ELEMENT_TYPE Type)
    {
      DataRow _dr;

      _dr = SeekByType(Element, Type);

      if (_dr != null)
      {
        _dr.Delete();

        switch (Type)
        {
          case GROUP_ELEMENT_TYPE.GROUP:
            m_group.AcceptChanges();
            break;
          case GROUP_ELEMENT_TYPE.PROV:
            m_provider.AcceptChanges();
            break;
          case GROUP_ELEMENT_TYPE.ZONE:
            m_zone.AcceptChanges();
            break;
          case GROUP_ELEMENT_TYPE.AREA:
            m_area.AcceptChanges();
            break;
          case GROUP_ELEMENT_TYPE.BANK:
            m_bank.AcceptChanges();
            break;
          case GROUP_ELEMENT_TYPE.TERM:
            m_terminal.AcceptChanges();
            break;
          default:
            break;
        }
      }
    } // RemoveElement

    //------------------------------------------------------------------------------
    // PURPOSE: Remove all elements.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Terminal
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void CleanList()
    {
      m_group.Clear();
      m_provider.Clear();
      m_zone.Clear();
      m_area.Clear();
      m_bank.Clear();
      m_terminal.Clear();
    } // RemoveElement

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if a Element is in the TerminalList.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Element
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: The Element is in the list. False: Otherwise.
    // 
    public Boolean Contains(Int32 ElementId, WSI.Common.GROUP_ELEMENT_TYPE Type)
    {
      switch ((TerminalListType)m_type.Rows[0][0])
      {
        case TerminalListType.All:
          return true;

        case TerminalListType.Listed:
          return (SeekByType(ElementId, Type) != null);

        case TerminalListType.NotListed:
          return (SeekByType(ElementId, Type) == null);

        default:
          return false;
      }
    } // Contains

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if exists an element of type Type.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - GROUP_ELEMENT_TYPE Type
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: The element is in the corresponding list. False: Otherwise.
    // 
    public Boolean ExistsElements(WSI.Common.GROUP_ELEMENT_TYPE Type)
    {
      switch (Type)
      {
        case GROUP_ELEMENT_TYPE.GROUP:
          return m_group.Rows.Count > 0;
        case GROUP_ELEMENT_TYPE.PROV:
          return m_provider.Rows.Count > 0;
        case GROUP_ELEMENT_TYPE.ZONE:
          return m_zone.Rows.Count > 0;
        case GROUP_ELEMENT_TYPE.AREA:
          return m_area.Rows.Count > 0;
        case GROUP_ELEMENT_TYPE.BANK:
          return m_bank.Rows.Count > 0;
        case GROUP_ELEMENT_TYPE.TERM:
          return m_terminal.Rows.Count > 0;
        default:
          return false;
      }
    } // Contains

    //------------------------------------------------------------------------------
    // PURPOSE: Get the list of elements of type Type.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Element
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: The Element is in the list. False: Otherwise.
    // 
    public DataTable GetElementsList(WSI.Common.GROUP_ELEMENT_TYPE Type)
    {
      switch (Type)
      {
        case GROUP_ELEMENT_TYPE.GROUP:
          return m_group;
        case GROUP_ELEMENT_TYPE.PROV:
          return m_provider;
        case GROUP_ELEMENT_TYPE.ZONE:
          return m_zone;
        case GROUP_ELEMENT_TYPE.AREA:
          return m_area;
        case GROUP_ELEMENT_TYPE.BANK:
          return m_bank;
        case GROUP_ELEMENT_TYPE.TERM:
          return m_terminal;
        default:
          return null;
      }
    } // Contains

    //------------------------------------------------------------------------------
    // PURPOSE: Get a XML representation of the element list.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    public String ToXml()
    {
      if ((TerminalListType)m_type.Rows[0][0] == TerminalListType.All)
      {
        return null;
      }

      return DataSetToXml(m_ds, XmlWriteMode.IgnoreSchema);
    } // ToXml

    public String ToXmlWithAll()
    {
      return DataSetToXml(m_ds, XmlWriteMode.IgnoreSchema);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Read a XML representation of the element list and store it into the DataSet.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void FromXml(String Xml)
    {
      if (String.IsNullOrEmpty(Xml))
      {
        ListType = TerminalListType.All;

        return;
      }

      m_type.Clear();
      CleanList();
      DataSetFromXml(m_ds, Xml, XmlReadMode.IgnoreSchema);

      DataRow[] _providers;
      Int32 _id;
      _providers = m_provider.Select(COLUMN_ID + " < 0");
      if (_providers.Length > 0)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          foreach (DataRow _provider in _providers)
          {
            if (!Groups.GetProviderId((String)_provider[COLUMN_NAME], out _id, _db_trx.SqlTransaction))
            {
              _provider.Delete();
            }
            else
            {
              _provider[COLUMN_ID] = _id;
            }
          }
        }
      }
    } // FromXml

    
    //------------------------------------------------------------------------------
    // PURPOSE: Get id terminal has gamegateway active.
    // 
    //  PARAMS:
    //      - INPUT:
    //              int Terminal_Id
    //      - OUTPUT:
    //
    // RETURNS: TRUE (if terminal has gamegateway active).
    // 
    public bool IsGameGatewayTerminalEnabled(int TerminalId)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   TG_TERMINAL_ID                  ");
        _sb.AppendLine("   FROM   TERMINAL_GROUPS                 ");
        _sb.AppendLine("  WHERE   TG_ELEMENT_TYPE = @pElementType ");
        _sb.AppendLine("    AND   TG_TERMINAL_ID  = @pTerminalId  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = (Int32)EXPLOIT_ELEMENT_TYPE.GAME_GATEWAY;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

            using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
            {
              if (_sql_reader.Read())
              {
                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsGameGatewayTerminalEnabled

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Init the properties of the class.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void Init()
    {
      DataRow _dr;
      TerminalListType _list_type;
      DataColumn _dc;

      _list_type = TerminalListType.All;

      m_ds = new DataSet("ProviderList");

      m_type = new DataTable("ListType");
      m_type.Columns.Add("Type", _list_type.GetType());
      _dr = m_type.NewRow();
      _dr[0] = TerminalListType.All;
      m_type.Rows.Add(_dr);

      m_provider = new DataTable("Provider");
      m_provider.Columns.Add(COLUMN_NAME, Type.GetType("System.String"));
      _dc = m_provider.Columns.Add(COLUMN_ID, Type.GetType("System.Int64"));
      _dc.AutoIncrement = true;
      _dc.AutoIncrementSeed = -1;
      _dc.AutoIncrementStep = -1;
      m_provider.PrimaryKey = new DataColumn[1] { m_provider.Columns[1] };

      m_terminal = new DataTable("Terminal");
      m_terminal.Columns.Add(COLUMN_NAME, Type.GetType("System.String"));
      m_terminal.Columns.Add(COLUMN_ID, Type.GetType("System.Int64"));
      m_terminal.PrimaryKey = new DataColumn[1] { m_terminal.Columns[1] };

      m_zone = new DataTable("Zone");
      m_zone.Columns.Add(COLUMN_NAME, Type.GetType("System.String"));
      m_zone.Columns.Add(COLUMN_ID, Type.GetType("System.Int64"));
      m_zone.PrimaryKey = new DataColumn[1] { m_zone.Columns[1] };

      m_area = new DataTable("Area");
      m_area.Columns.Add(COLUMN_NAME, Type.GetType("System.String"));
      m_area.Columns.Add(COLUMN_ID, Type.GetType("System.Int64"));
      m_area.PrimaryKey = new DataColumn[1] { m_area.Columns[1] };

      m_bank = new DataTable("Bank");
      m_bank.Columns.Add(COLUMN_NAME, Type.GetType("System.String"));
      m_bank.Columns.Add(COLUMN_ID, Type.GetType("System.Int64"));
      m_bank.PrimaryKey = new DataColumn[1] { m_bank.Columns[1] };

      m_group = new DataTable("Group");
      m_group.Columns.Add(COLUMN_NAME, Type.GetType("System.String"));
      m_group.Columns.Add(COLUMN_ID, Type.GetType("System.Int64"));
      m_group.PrimaryKey = new DataColumn[1] { m_group.Columns[1] };

      m_ds.Tables.Add(m_type);
      m_ds.Tables.Add(m_provider);
      m_ds.Tables.Add(m_terminal);
      m_ds.Tables.Add(m_zone);
      m_ds.Tables.Add(m_area);
      m_ds.Tables.Add(m_bank);
      m_ds.Tables.Add(m_group);
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Search for the element in the type list.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Key
    //          - GROUP_ELEMENT_TYPE Type
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DataRow: If found return the DataRow.
    // 
    private DataRow SeekByType(Int32 ElementId, WSI.Common.GROUP_ELEMENT_TYPE Type)
    {
      DataTable _search;
      Object[] _search_key;
      DataRow _dr_result;

      switch (Type)
      {
        case GROUP_ELEMENT_TYPE.GROUP:
          _search = m_group;
          break;
        case GROUP_ELEMENT_TYPE.PROV:
          _search = m_provider;
          break;
        case GROUP_ELEMENT_TYPE.ZONE:
          _search = m_zone;
          break;
        case GROUP_ELEMENT_TYPE.AREA:
          _search = m_area;
          break;
        case GROUP_ELEMENT_TYPE.BANK:
          _search = m_bank;
          break;
        case GROUP_ELEMENT_TYPE.TERM:
          _search = m_terminal;
          break;
        default:
          return null;
      }

      _search_key = new Object[] { ElementId };
      _dr_result = _search.Rows.Find(_search_key);

      return _dr_result;
    } // SeekByType

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String DataSetToXml(DataSet DataSet, XmlWriteMode WriteMode)
    {
      String _xml;

      _xml = "";
      using (StringWriter _sw = new StringWriter())
      {
        DataSet.WriteXml(_sw, WriteMode);
        _xml = _sw.ToString();
      }

      return _xml;
    } // DataSetToXml

    #endregion // Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private void DataSetFromXml(DataSet DataSet, String Xml, XmlReadMode ReadMode)
    {
      using (StringReader _sr = new StringReader(Xml))
      {
        DataSet.ReadXml(_sr, ReadMode);
      }
    } // DataSetFromXml

  } // TerminalList

}  // WSI.Common
