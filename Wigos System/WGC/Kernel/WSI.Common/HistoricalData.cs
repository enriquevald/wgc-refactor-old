//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: HistoricalData.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: Ram�n Moncl�s
// 
// CREATION DATE: 12-FEB-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- ------------------------------------------------------
// 12-FEB-2014 RMS        First release.
// 18-FEB-2014 RMS        Added thread capability.
// 01-APR-2014 RMS        Optimized for no timeouts.
//------------------------------------------------------------------------------ 

using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

namespace WSI.Common
{

  public class HistoricalData
  {

    private static Thread m_thread;

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize HistoricalData class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Init()
    {
      m_thread = new Thread(HistoricalDataThread);
      m_thread.Name = "HistoricalDataThread";

      m_thread.Start();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Thread process for historical data
    //
    //  PARAMS :
    //      - INPUT : 
    //                
    //      - OUTPUT : 
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void HistoricalDataThread()
    {
      Int32 _wait_hint;
      Int32 _time_start;
      Int64 _ellapsed;
      Int32 _updated_items;
      Int32 _pending_update;
      Int32 _timeouts_counter;

      _wait_hint = 60000;
      _timeouts_counter = 0;

      while (true)
      {
        Thread.Sleep(_wait_hint);

        // Set default wait time
        _wait_hint = 1000 * GeneralParam.GetInt32("HistoricalData", "PendingMovementsByHour.Step.WaitSeconds", 60, 10, 120);

        if (!Services.IsPrincipal("WCP"))
        {
          continue;
        }

        try
        {
          _time_start = Environment.TickCount;

          // Launch the stored
          // * Exceptions are captured by this method, if statement not necessary.
          HistoricalData.GenerateMovementsByHour(out _updated_items, out _pending_update);

          // Warning if duration greater than 40 seconds
          // Usually caused by multiple inserts on pending items.
          _ellapsed = Misc.GetElapsedTicks(_time_start);
          if (_ellapsed > 40000)
          {
            Log.Warning("HistoricalDataThread, Pending: " + _pending_update.ToString() + " Updated:" + _updated_items.ToString() + " Duration: " + _ellapsed.ToString() + " ms.");
          }

          if (_pending_update > 0)
          {
            // Thread don't sleep when there are pending items
            _wait_hint = 0;
          }
        }
        catch (SqlException _ex)
        {
          if (_ex.Number == -2) // SQL TimeOut
          {
            _timeouts_counter += 1;

            // If there are 10 consecutive errors we LOG the exception
            if (_timeouts_counter % 10 == 0)
            {
              Log.Exception(_ex);
              Log.Message("HistoricalDataThread: TimeOut Error Counter " + _timeouts_counter.ToString());

              // Make thread sleep for a while
              _wait_hint = 5000;
            }
          }
          else
          {
            // Other type exceptions must be reported
            Log.Exception(_ex);
            
            // Reset timeouts counter
            _timeouts_counter = 0;
          }
        }
        catch (Exception _ex)
        {
          // Other type exceptions must be reported
          Log.Exception(_ex);

          // Reset timeouts counter on other errors
          _timeouts_counter = 0;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Upate historical data about cashier and mobile bank movements
    //
    //  PARAMS :
    //      - INPUT : 
    //                
    //      - OUTPUT : 
    //          ItemsUpdated  Will contains the number of updated items      
    //          ItemsPending  Will contains the number of pending items before process start
    //
    // RETURNS :
    //    Boolean  (True if work is done)
    //
    //   NOTES :
    //    ** Exception is captured on caller routine to control number of errors. **
    //    ** Discovered LOCK by multiple inserts when trying to delete the pending items. **
    private static Boolean GenerateMovementsByHour(out Int32 ItemsUpdated, out Int32 ItemsPending)
    {
      Boolean _result;

      _result = true;

      ItemsUpdated = 0;
      ItemsPending = 0;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _cmd = new SqlCommand("SP_GenerateHistoricalMovements_ByHour"))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.CommandTimeout = 60; // 1 minute

          _cmd.Parameters.Add("@pMaxItemsPerLoop", SqlDbType.Int).Value = GeneralParam.GetInt32("HistoricalData", "PendingMovementsByHour.Step.MaxItems", 500);
          _cmd.Parameters.Add("@pCurrecyISOCode", SqlDbType.VarChar, 20).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

          using (SqlDataReader _reader = _db_trx.ExecuteReader(_cmd))
          {
            if (!_reader.Read())
            {
              _result = false;
            }
            else
            {
              ItemsUpdated = _reader.GetInt32(0);
              ItemsPending = _reader.GetInt32(1);
            }
          }
        }

        if (_result)
        {
          _db_trx.Commit();
        }

      }

      return _result;
    }


  }
}

