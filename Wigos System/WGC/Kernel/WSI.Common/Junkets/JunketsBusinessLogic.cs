﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Junkets.cs
// 
//   DESCRIPTION: Junkets
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 18-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-APR-2017 AMF    First release (Header).
// 04-MAY-2017 DPC    WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Junkets
{

  #region " Enums "

  public enum CommissionType
  {
    ByCostumer = 0,
    ByVisit = 1,
    ByEnrolled = 2,
    ByCoinIn = 3,
    ByTheoricalNetwin = 4,
    ByBuyIn = 5,
    ByPoints = 6
  }

  public enum Status
  {
    Pending = 0,
    Paid = 1
  }

  public enum ErrorCode
  {
    Valid = 0,
    NotExist = 1,
    Expired = 2,
    Applied = 3,
    HasAlredy = 4,
    Unknown = 99
  }

  #endregion // Enums

  public class JunketsBusinessLogic
  {

    #region " Members "

    private String m_text_popup;
    private String m_text_voucher;
    private String m_text_promotion;
    private String m_title_voucher;

    #endregion " Members "

    #region " Properties "

    public String FlyerCode { get; set; }
    public Int64? AccountId { get; set; }
    public Int64 OperationId { get; set; }
    public Int64 FlyerId { get; set; }
    public Int64 JunketID { get; set; }
    public Int64? PromotionID { get; set; }
    public Int64 FlagId { get; set; }
    public Boolean Reusable { get; set; }
    public Decimal Amount { get; set; }
    public Boolean FirstFlyer { get; set; }
    public Boolean ShowPopUp { get; set; }
    public Boolean PrintVoucher { get; set; }
    public Boolean PrintTextPromotion { get; set; }
    public String TextPopUp
    {
      get
      {
        if (ShowPopUp)
        {
          return m_text_popup;
        }

        return String.Empty;
      }
    }
    public String TextVoucher
    {
      get
      {
        if (PrintVoucher)
        {
          return m_text_voucher;
        }

        return String.Empty;
      }
    }
    public String TitleVoucher
    {
      get
      {
        if (PrintVoucher)
        {
          return m_title_voucher;
        }

        return String.Empty;
      }
    }

    public String TextPromotion
    {
      get
      {
        if (PrintTextPromotion)
        {
          return m_text_promotion;
        }

        return String.Empty;
      }
    }

    public ErrorCode ErrorFlyer { get; set; }

    #endregion " Properties "

    #region " Constructors "   

    /// <summary>
    /// Constructor
    /// </summary>
    public JunketsBusinessLogic()
    {
      FlyerId = -1;
    } // JunketsBusinessLogic

    #endregion " Constructors "

    #region " Public methods "

    /// <summary>
    /// Check if feature junkets is enabled
    /// </summary>
    /// <returns></returns>
    public static Boolean IsJunketsEnabled()
    {
      return GeneralParam.GetBoolean("Junkets", "Enabled", false);
    } // IsJunketsEnabled

    /// <summary>
    /// Process commissions with the info of buckets thread
    /// </summary>
    /// <param name="DrSession"></param>
    /// <param name="DtBuckets"></param>
    /// <param name="SqlTrx"></param>
    public static Boolean ProcessSessionCommissions(DataRow DrSession, DataTable DtBuckets, SqlTransaction SqlTrx)
    {
      JunketsCommissionsMovements _movements;
      DataRow[] _dt_buckets;
      Int64 _flyer_id;
      Int64 _junket;

      try
      {
        // Funcionality Enabled
        if (!IsJunketsEnabled())
        {
          return true;
        }

        // Get Flyer Id by AccountId
        if (!GetAccountValidFlyer((Int64)DrSession["ACCOUNT_ID"], SqlTrx, out _flyer_id, out _junket))
        {
          return true; // Don't have Flyer
        }

        // Create movements
        _movements = new JunketsCommissionsMovements();
        _movements.FlyerId = _flyer_id;
        _movements.AccountId = (Int64)DrSession["ACCOUNT_ID"];
        _movements.TerminalId = (Int32)DrSession["PPS_TERMINAL_ID"];
        if ((Decimal)DrSession["PPS_COIN_IN"] > 0m)
        {
          _movements.Commissions.Add(new Commission(CommissionType.ByCoinIn));
          _movements.Commissions.Add(new Commission(CommissionType.ByTheoricalNetwin));
          _movements.Amount = (Decimal)DrSession["PPS_COIN_IN"];
        }
        // Get points
        _dt_buckets = DtBuckets.Select(String.Format(" PPS_SESSION_ID = {0} AND BUCKET_ID = {1}", DrSession["PPS_SESSION_ID"], (Int16)Buckets.BucketId.RedemptionPoints));
        if (_dt_buckets.Length > 0)
        {
          if ((Decimal)_dt_buckets[0]["WONVALUE"] > 0m)
          {
            _movements.Commissions.Add(new Commission(CommissionType.ByPoints));
            _movements.Points = (Int32)(Decimal)_dt_buckets[0]["WONVALUE"];
          }
        }

        if (_movements.Commissions.Count > 0)
        {
          _movements.Save(SqlTrx);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.ProcessSessionCommissions -> AccountId: {0}", DrSession["ACCOUNT_ID"]));
        Log.Exception(_ex);
      }

      return false;
    } // ProcessSessionCommissions

    /// <summary>
    /// Comissions given when a flyer is entered (Customers submit a ticket or enrolled customers)
    /// </summary>
    /// <param name="sqlTrx"></param>
    /// <returns></returns>
    public Boolean ProcessCommissionsCustomerAndEnrolled(SqlTransaction sqlTrx)
    {
      JunketsCommissionsMovements _movements;
      Boolean _has_enrolled;
      Boolean _has_visits_today;
      Boolean _is_virtual;

      _movements = new JunketsCommissionsMovements()
      {
        AccountId = (Int64)this.AccountId,
        FlyerId = this.FlyerId,
        OperationId = this.OperationId
      };

      if (this.FirstFlyer)
      {
        _movements.Commissions.Add(new Commission(CommissionType.ByCostumer));

        if (!AccountEnrolledJunket(this.JunketID, (Int64)this.AccountId, out _has_enrolled, sqlTrx))
        {
          return false;
        }

        if (_has_enrolled)
        {
          _movements.Commissions.Add(new Commission(CommissionType.ByEnrolled));
        }
      }

      if (!HasVisitsToday((Int64)this.AccountId, WGDB.Now, sqlTrx, out _has_visits_today))
      {
        return false;
      }

      if (!IsAccountVirtual(out _is_virtual, sqlTrx))
      {
        return false;
      }

      if (!_has_visits_today || _is_virtual)
      {
        _movements.Commissions.Add(new Commission(CommissionType.ByVisit));
      }

      if (_movements.Commissions.Count > 0)
      {
        _movements.Save(sqlTrx);
      }

      return true;
    } // ProcessCommissionsCustomerAndEnrolled

    /// <summary>
    /// Set commission by enrolled
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Amount"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean ProcessCommissionEnrolled(Int64 AccountId, SqlTransaction SqlTrx)
    {

      Int64 _flyer_id;
      Boolean _has_enrolled;
      Boolean _has_enrolled_movement;
      JunketsCommissionsMovements _movements;
      Int64 _junket;

      try
      {

        if (!IsJunketsEnabled())
        {
          return true;
        }

        if (AccountId == 0)
        {
          return true;
        }

        // Get Flyer Id by AccountId
        if (!GetAccountValidFlyer(AccountId, SqlTrx, out _flyer_id, out _junket))
        {
          return true; // Don't have Flyer
        }

        if (!AccountEnrolledJunket(_junket, AccountId, out _has_enrolled, SqlTrx))
        {
          return false;
        }


        if (_has_enrolled)
        {
          if (!HasEnrolledMovement(AccountId, SqlTrx, out _has_enrolled_movement))
          {
            return false;
          }
          if (_has_enrolled_movement)
          {
            _movements = new JunketsCommissionsMovements();
            _movements.FlyerId = _flyer_id;
            _movements.AccountId = AccountId;
            _movements.Commissions.Add(new Commission(CommissionType.ByEnrolled));

            if (_movements.Commissions.Count > 0)
            {
              _movements.Save(SqlTrx);
            }
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.SetCommissionEnrolled -> AccountId: {0}", AccountId));
        Log.Exception(_ex);
      }

      return false;

    } // SetCommissionEnrolled

    /// <summary>
    /// Set commission by enrolled
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Amount"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean ProcessCommissionBuyIn(Int64 AccountId, Decimal Amount, SqlTransaction SqlTrx)
    {

      Int64 _flyer_id;
      JunketsCommissionsMovements _movements;
      Int64 _junket;

      try
      {

        if (!IsJunketsEnabled())
        {
          return true;
        }

        if (AccountId == 0)
        {
          return true;
        }

        // Get Flyer Id by AccountId
        if (!GetAccountValidFlyer(AccountId, SqlTrx, out _flyer_id, out _junket))
        {
          return true; // Don't have Flyer
        }

        _movements = new JunketsCommissionsMovements();
        _movements.FlyerId = _flyer_id;
        _movements.AccountId = AccountId;
        _movements.Amount = Amount;

        _movements.Commissions.Add(new Commission(CommissionType.ByBuyIn));

        if (_movements.Commissions.Count > 0)
        {
          _movements.Save(SqlTrx);
        }
        return true;

      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.SetCommissionEnrolled -> AccountId: {0}", AccountId));
        Log.Exception(_ex);
      }

      return false;

    } // SetCommissionEnrolled

    /// <summary>
    /// Process commissions pending generate in save of account movements
    /// </summary>
    /// <returns></returns>
    public static Boolean ProcessCommissionsPending()
    {
      // Enabled functionality
      if (!IsJunketsEnabled())
      {
        return true;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (CommissionsPending(_db_trx.SqlTransaction))
        {
          _db_trx.Commit();

          return true;
        }
        else
        {
          return false;
        }
      }
    } // ProcessCommissionsPending

    /// <summary>
    /// Get Junket data by Account Id and FlyerCode.
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetJunketData(SqlTransaction SqlTrx)
    {
      Boolean _is_virtual;
      Boolean _get_flyer;

      _get_flyer = GetJunketByFlyerCode(SqlTrx);
     
      if (!IsAccountVirtual(out _is_virtual, SqlTrx))
      {
        return false;
      }

      if (ErrorFlyer == ErrorCode.Valid && _is_virtual)
      {
        return true;
      }

      if (!_is_virtual)
      {
        if (AccountHasCurrentFlag(SqlTrx))
        {
          return false;
        }

        if (AccountHasValidFlyerFlag(false, SqlTrx))
        {
          return false;
        }
      }

      if (!_get_flyer)
      {
        GetFlyerByCode(SqlTrx);

        return false;
      }

      return true;

    } // GetJunketData

    /// <summary>
    /// Process to add flags to account.
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean AddFlagsToAccount(SqlTransaction SqlTrx)
    {
      DataTable _table;
      _table = AccountFlag.CreateAccountFlagsTable();

      if (this.AccountId == null)
      {
        return true;
      }

      //ETP Manual Flags added to junket:
      if (!GetFlagsTable(_table, SqlTrx))
      {
        return false;
      }

      AccountFlag.InsertAccountFlags(_table, SqlTrx);

      return true;
    } // AddFlagsToAccount

    #endregion // Public methods

    #region " Private methods "

    /// <summary>
    /// Get junket and flag  by Flyer code.
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean GetJunketByFlyerCode(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      ErrorFlyer = ErrorCode.NotExist;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT TOP 1   JF_ID                         ");
        _sb.AppendLine("                 , JF_JUNKET_ID                  ");
        _sb.AppendLine("                 , FL_FLAG_ID                    ");
        _sb.AppendLine("                 , JF_ALLOW_REUSE                ");
        _sb.AppendLine("                 , JF_SHOW_POP_UP                ");
        _sb.AppendLine("                 , JF_TEXT_POP_UP                ");
        _sb.AppendLine("                 , JF_PRINT_VOUCHER              ");
        _sb.AppendLine("                 , JF_TITLE_VOUCHER              ");
        _sb.AppendLine("                 , JF_TEXT_VOUCHER               ");
        _sb.AppendLine("                 , JF_PROMOTION_ID               ");
        _sb.AppendLine("                 , JF_PRINT_TEXT_PROMOTION       ");
        _sb.AppendLine("                 , JF_TEXT_PROMOTION             ");
        _sb.AppendLine("      FROM         JUNKETS_FLYERS                ");
        _sb.AppendLine("     INNER   JOIN  JUNKETS                       ");
        _sb.AppendLine("        ON         JF_JUNKET_ID =  JU_ID         ");
        _sb.AppendLine("       AND         JU_DATE_FROM <= @pDateTime    ");
        _sb.AppendLine("       AND         JU_DATE_TO > @pDateTime       ");
        _sb.AppendLine("     INNER   JOIN  FLAGS                         ");
        _sb.AppendLine("        ON         JF_ID =  FL_RELATED_ID        ");
        _sb.AppendLine("       AND         FL_RELATED_TYPE = @pFlyerType ");
        _sb.AppendLine("     WHERE         JF_CODE =  @pCode             ");
        _sb.AppendLine("  ORDER BY         JU_CREATION DESC              ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = this.FlyerCode;
          _cmd.Parameters.Add("@pFlyerType", SqlDbType.Bit).Value = AccountFlag.RelatedTypes.FLYER;
          _cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = WGDB.Now;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {

            if (_reader.Read())
            {
              FlyerId = (Int64)_reader["JF_ID"];
              JunketID = (Int64)_reader["JF_JUNKET_ID"];
              PromotionID = null;

              if ((_reader["JF_PROMOTION_ID"] != null) && (_reader["JF_PROMOTION_ID"] != DBNull.Value))
              {
                PromotionID = (Int64)_reader["JF_PROMOTION_ID"];
              }

              FlagId = (Int64)_reader["FL_FLAG_ID"];

              Reusable = (Boolean)_reader["JF_ALLOW_REUSE"];
              ShowPopUp = (Boolean)_reader["JF_SHOW_POP_UP"];
              m_text_popup = ShowPopUp ? (String)_reader["JF_TEXT_POP_UP"] : String.Empty;

              PrintVoucher = (Boolean)_reader["JF_PRINT_VOUCHER"];
              m_text_voucher = PrintVoucher ? (String)_reader["JF_TEXT_VOUCHER"] : String.Empty;

              PrintTextPromotion = (Boolean)_reader["JF_PRINT_TEXT_PROMOTION"];
              m_text_promotion = PrintTextPromotion ? (String)_reader["JF_TEXT_PROMOTION"] : String.Empty;

              m_title_voucher = PrintVoucher ? (String)_reader["JF_TITLE_VOUCHER"] : String.Empty;
              ErrorFlyer = ErrorCode.Valid;

              FirstFlyer = false;

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        ErrorFlyer = ErrorCode.Unknown;
        Log.Error(String.Format("JunketsBusinessLogic.GetJunketByFlyerCode -> Code: {0}", this.FlyerCode));
        Log.Exception(_ex);
      }

      return false;
    } // GetJunketByFlyerCode

    /// <summary>
    /// Get Flyer by Flyer Code.
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean GetFlyerByCode(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;

      if (ErrorFlyer == ErrorCode.Unknown)
      {
        return false;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT    TOP 1 JF_ID      ");
        _sb.AppendLine("   FROM    JUNKETS_FLYERS     ");
        _sb.AppendLine("  WHERE    JF_CODE =  @pCode  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = this.FlyerCode;

          _obj = _cmd.ExecuteScalar();

          if ((_obj != null) && (_obj != DBNull.Value))
          {
            ErrorFlyer = ErrorCode.Expired;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        ErrorFlyer = ErrorCode.Unknown;
        Log.Error(String.Format("JunketsBusinessLogic.GetFlyerByCode -> Code: {0}", this.FlyerCode));
        Log.Exception(_ex);
      }

      return false;
    } // GetFlyerByCode

    /// <summary>
    /// Get If account has current flyer flag.
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean AccountHasCurrentFlag(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT    TOP 1 AF_FLAG_ID             ");
        _sb.AppendLine("   FROM    ACCOUNT_FLAGS                ");
        _sb.AppendLine("  WHERE    AF_FLAG_ID  = @pFlagId       ");
        _sb.AppendLine("    AND    AF_ACCOUNT_ID = @pAccount_id ");
        _sb.AppendLine("    AND    AF_STATUS <> @pFlagStatus    ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pFlagId", SqlDbType.BigInt).Value = this.FlagId;
          _cmd.Parameters.Add("@pAccount_id", SqlDbType.BigInt).Value = this.AccountId;
          _cmd.Parameters.Add("@pFlagStatus", SqlDbType.Int).Value = ACCOUNT_FLAG_STATUS.CANCELLED;

          _obj = _cmd.ExecuteScalar();

          if ((_obj != null) && (_obj != DBNull.Value))
          {
            FirstFlyer = false;
            if (!Reusable)
            {
              ErrorFlyer = ErrorCode.Applied;
              return true;
            }

            FirstFlyer = false;
            return false;
          }

          FirstFlyer = true;
        }
      }
      catch (Exception _ex)
      {
        ErrorFlyer = ErrorCode.Unknown;
        Log.Error(String.Format("JunketsBusinessLogic.AccountHasCurrentFlag -> AccountId: {0}", this.AccountId));
        Log.Exception(_ex);
      }

      return false;
    } // AccountHasCurrentFlag

    /// <summary>
    /// Get If account has current enrolled during the current junked period
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean AccountEnrolledJunket(Int64 JunketId, Int64 AccountId, out Boolean _has_enrolled, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;
      _has_enrolled = false;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT    1 FROM ACCOUNTS                      ");
        _sb.AppendLine("INNER JOIN    JUNKETS                              ");
        _sb.AppendLine("        ON    JU_ID = @pJunket                     ");
        _sb.AppendLine("       AND    JU_DATE_FROM <= GETDATE()            ");
        _sb.AppendLine("       AND    JU_DATE_TO   >  GETDATE()            ");
        _sb.AppendLine("     WHERE    AC_CREATED > JU_DATE_FROM            ");
        _sb.AppendLine("       AND    AC_ACCOUNT_ID = @pAccountId          ");
        _sb.AppendLine("       AND    (AC_TYPE <>  @pAccountVirtualCashier ");
        _sb.AppendLine("       AND      ISNULL(AC_HOLDER_LEVEL, 0) > 0)    ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJunket", SqlDbType.BigInt).Value = JunketId;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pAccountVirtualCashier", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_CASHIER;


          _obj = _cmd.ExecuteScalar();

          if ((_obj != null) && (_obj != DBNull.Value))
          {
            _has_enrolled = true;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.AccountHasCurrentFlag -> AccountId: {0}", AccountId));
        Log.Exception(_ex);
      }

      return false;
    } // AccountEnrolledJunket


    private Boolean IsAccountVirtual(out Boolean IsVirtual, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      IsVirtual = false;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT  1                                        ");
        _sb.AppendLine("   FROM   ACCOUNTS                                ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId             ");
        _sb.AppendLine("  AND     AC_TYPE       = @pAccountVirtualCashier ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = this.AccountId;
          _cmd.Parameters.Add("@pAccountVirtualCashier", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_CASHIER;

          _obj = _cmd.ExecuteScalar();

        }

        if (_obj != null && _obj != DBNull.Value)
        {
          IsVirtual = true;
          this.FirstFlyer = true;
        }

        return true;
      }
      catch (Exception _ex)
      {
        ErrorFlyer = ErrorCode.Unknown;
        Log.Error(String.Format("JunketsBusinessLogic.IsAccountAnonymousOrVirtual -> AccountId: {0}", this.AccountId));
        Log.Exception(_ex);
      }

      return false;
    }


    /// <summary>
    /// Get if is needed to create a new comission movement
    /// </summary>
    /// <param name="ComissionType"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean HasMovementType(CommissionType ComissionType, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT  1                             ");
        _sb.AppendLine("      FROM JUNKETS_COMMISSIONS_MOVEMENTS  ");
        _sb.AppendLine("    WHERE  JCM_ACOUNT_ID = @pAccountId    ");
        _sb.AppendLine("       AND    JCM_FLYER_ID  = @pFlyerId   ");
        _sb.AppendLine("       AND    JCM_TYPE  = @pTypeMovement  ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pJunket", SqlDbType.BigInt).Value = this.JunketID;
          _cmd.Parameters.Add("@pAccount_id", SqlDbType.BigInt).Value = this.AccountId;
          _cmd.Parameters.Add("@pMovementType", SqlDbType.Int).Value = ComissionType;

          _obj = _cmd.ExecuteScalar();

          if ((_obj != null) && (_obj != DBNull.Value))
          {

          }
        }
      }
      catch (Exception _ex)
      {
        ErrorFlyer = ErrorCode.Unknown;
        Log.Error(String.Format("JunketsBusinessLogic.AccountHasCurrentFlag -> AccountId: {0}", this.AccountId));
        Log.Exception(_ex);
      }

      return false;
    } // HasMovementType

    /// <summary>
    /// Get If account has valid flyer flag.
    /// </summary>
    /// <param name="AddCurrent"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean AccountHasValidFlyerFlag(Boolean AddCurrent, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT    TOP 1 AF_FLAG_ID                ");
        _sb.AppendLine("       FROM    ACCOUNT_FLAGS                   ");
        _sb.AppendLine(" INNER JOIN    FLAGS                           ");
        _sb.AppendLine("         ON    FL_FLAG_ID = AF_FLAG_ID         ");
        _sb.AppendLine("        AND    FL_RELATED_TYPE = @pFlyerType   ");
        _sb.AppendLine("        AND    AF_STATUS      <> @pFlagStatus  ");
        _sb.AppendLine(" INNER JOIN    JUNKETS_FLYERS                  ");
        _sb.AppendLine("         ON    JF_ID         = FL_RELATED_ID   ");
        _sb.AppendLine(" INNER JOIN    JUNKETS                         ");
        _sb.AppendLine("         ON    JU_ID         =  JF_JUNKET_ID   ");
        _sb.AppendLine("        AND    JU_DATE_FROM  <= @pDateTime     ");
        _sb.AppendLine("        AND    JU_DATE_TO    >  @pDateTime     ");
        _sb.AppendLine("      WHERE    AF_ACCOUNT_ID =  @pAccount_id   ");

        if (!AddCurrent)
        {
          _sb.AppendLine("     AND      AF_FLAG_ID  <> @pFlagId        ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccount_id", SqlDbType.BigInt).Value = this.AccountId;
          _cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = WGDB.Now;

          _cmd.Parameters.Add("@pFlyerType", SqlDbType.Int).Value = AccountFlag.RelatedTypes.FLYER;
          _cmd.Parameters.Add("@pFlagStatus", SqlDbType.Int).Value = ACCOUNT_FLAG_STATUS.CANCELLED;

          if (!AddCurrent)
          {
            _cmd.Parameters.Add("@pFlagId", SqlDbType.BigInt).Value = FlagId;
          }

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              ErrorFlyer = ErrorCode.HasAlredy;

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        ErrorFlyer = ErrorCode.Unknown;
        Log.Error(String.Format("JunketsBusinessLogic.AccountHasValidFlyerFlag -> AccountId: {0}", this.AccountId));
        Log.Exception(_ex);
      }

      return false;
    } // AccountHasValidFlyerFlag

    /// <summary>
    /// Get Flyer valid by Account Id
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="FlyerId"></param>
    /// <returns></returns>
    private static Boolean GetAccountValidFlyer(Int64 AccountId, SqlTransaction SqlTrx, out Int64 FlyerId, out Int64 JunketId)
    {
      StringBuilder _sb;

      FlyerId = 0;
      JunketId = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   TOP 1 JF_ID                     ");
        _sb.AppendLine("            , JU_ID                           ");
        _sb.AppendLine("       FROM   ACCOUNT_FLAGS                   ");
        _sb.AppendLine(" INNER JOIN   FLAGS                           ");
        _sb.AppendLine("         ON   FL_FLAG_ID      = AF_FLAG_ID    ");
        _sb.AppendLine("        AND   FL_RELATED_TYPE = @pFlyerType   ");
        _sb.AppendLine("        AND   AF_ACCOUNT_ID   = @pAccountId   ");
        _sb.AppendLine("        AND   AF_STATUS      <>  @pFlagStatus ");
        _sb.AppendLine(" INNER JOIN   JUNKETS_FLYERS                  ");
        _sb.AppendLine("         ON   FL_RELATED_ID   = JF_ID         ");
        _sb.AppendLine(" INNER JOIN   JUNKETS                         ");
        _sb.AppendLine("         ON   JU_ID = JF_JUNKET_ID            ");
        _sb.AppendLine("        AND   JU_DATE_FROM   <= @pDateTime    ");
        _sb.AppendLine("        AND   JU_DATE_TO     >  @pDateTime    ");
        _sb.AppendLine("   ORDER BY   AF_ADDED_DATETIME DESC          ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pFlyerType", SqlDbType.Int).Value = AccountFlag.RelatedTypes.FLYER;
          _cmd.Parameters.Add("@pFlagStatus", SqlDbType.Int).Value = ACCOUNT_FLAG_STATUS.CANCELLED;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              FlyerId = (Int64)_reader["JF_ID"];
              JunketId = (Int64)_reader["JU_ID"];

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.GetAccountValidFlyer -> AccountId: {0}", AccountId));
        Log.Exception(_ex);
      }

      return false;
    } // GetAccountValidFlyer

    /// <summary>
    /// Create flags table
    /// </summary>
    /// <param name="Table"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean GetFlagsTable(DataTable Table, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      if (this.AccountId == null)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   JFF_FLAG_ID              ");
        _sb.AppendLine("        , JFF_FLAG_COUNT           ");
        _sb.AppendLine("   FROM   JUNKETS_FLYERS_FLAGS     ");
        _sb.AppendLine("  WHERE   JFF_FLYER_ID = @pFlyerId ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pFlyerId", SqlDbType.BigInt).Value = FlyerId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              if (!AddAccountFlag(Table, (Int64)_reader["JFF_FLAG_ID"], (Int32)_reader["JFF_FLAG_COUNT"]))
              {
                return false;
              }
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.AddManualFlagsTable -> AccountId: {0}", this.AccountId));
        Log.Exception(_ex);
      }

      return false;
    } // GetFlagsTable

    /// <summary>
    /// Undo Inserted Flags
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean UndoInsertedFlags(Int64 OperationId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _dummy_account_flags;
      DataTable _undo_table;

      _undo_table = new DataTable();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   AF_UNIQUE_ID                             ");
        _sb.AppendLine("        , AF_ACCOUNT_ID                            ");
        _sb.AppendLine("        , AF_STATUS                                ");
        _sb.AppendLine("   FROM   ACCOUNT_FLAGS                            ");
        _sb.AppendLine("  WHERE   AF_ADDED_BY_OPERATION_ID = @pOperationId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          using (SqlDataAdapter _adapter = new SqlDataAdapter(_cmd))
          {
            _adapter.Fill(_undo_table);

            if (AccountFlag.ChangeStatusFlags(_undo_table, ACCOUNT_FLAG_STATUS.CANCELLED, -1, out _dummy_account_flags) == _undo_table.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.AddManualFlagsTable -> OperationId: {0}", OperationId));
        Log.Exception(_ex);
      }

      return false;
    } // UndoInsertedFlags

    /// <summary>
    /// Add a n flags to table.
    /// </summary>
    /// <param name="Table"></param>
    /// <param name="AddedFlagId"></param>
    /// <param name="Count"></param>
    /// <returns></returns>
    private Boolean AddAccountFlag(DataTable Table, Int64 AddedFlagId, Int32 Count)
    {
      if (this.AccountId == null)
      {
        return true;
      }

      for (int i = 0; i < Count; i++)
      {
        if (!AccountFlag.AddFlagToAccount((Int64)this.AccountId
                                                , AddedFlagId
                                                , ACCOUNT_FLAG_STATUS.ACTIVE
                                                , -1
                                                , -1
                                                , Table
                                                , false
                                                , this.OperationId))
        {
          return false;
        }
      }

      return true;
    } // AddAccountFlag

    /// <summary>
    /// Check if the costumer has a visit in working day
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="VisitDate"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="HasVisits"></param>
    /// <returns></returns>
    private static Boolean HasVisitsToday(Int64 AccountId, DateTime VisitDate, SqlTransaction SqlTrx, out Boolean HasVisits)
    {

      StringBuilder _sb;
      DateTime _working_date;
      Int32 _num_visits;

      HasVisits = false;

      try
      {

        _working_date = Misc.Opening(VisitDate);

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   COUNT(1)                      ");
        _sb.AppendLine("   FROM   JUNKETS_COMMISSIONS_MOVEMENTS ");
        _sb.AppendLine("  WHERE   JCM_CREATION > @pDate         ");
        _sb.AppendLine("    AND   JCM_ACCOUNT_ID = @pAccountId  ");
        _sb.AppendLine("    AND   JCM_TYPE = @pType             ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = _working_date;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pType", SqlDbType.BigInt).Value = CommissionType.ByVisit;

          _num_visits = (Int32)_cmd.ExecuteScalar();
          
          HasVisits = (_num_visits % 2 != 0);
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.HasVisitsToday -> AccountId: {0}", AccountId));
        Log.Exception(_ex);
      }

      return false;

    } // HasVisitsToday

    /// <summary>
    /// Get, process and remove commissions pending
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean CommissionsPending(SqlTransaction SqlTrx)
    {
      DataTable _commissions_pending;

      if (!GetCommissionsPending(SqlTrx, out _commissions_pending))
      {
        return false;
      }

      if (_commissions_pending.Rows.Count == 0)
      {
        return true;
      }

      foreach (DataRow _commission in _commissions_pending.Rows)
      {
        if (!SetCommissionVisit((Int64)_commission["JCP_ACCOUNT_ID"], (DateTime)_commission["JCP_DATETIME"], SqlTrx))
        {
          return false;
        }
        if (!DeleteCommissionPendingProcessed((Int64)_commission["JCP_ID"], SqlTrx))
        {
          return false;
        }
      }

      return true;
    } // CommissionsPending

    /// <summary>
    /// Get pending commissions
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="CommissionsPending"></param>
    /// <returns></returns>
    private static Boolean GetCommissionsPending(SqlTransaction SqlTrx, out DataTable CommissionsPending)
    {
      StringBuilder _sb;

      CommissionsPending = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   TOP 500 JCP_ID              ");
        _sb.AppendLine("          , JCP_ACCOUNT_ID              ");
        _sb.AppendLine("          , JCP_DATETIME                ");
        _sb.AppendLine("     FROM   JUNKETS_COMMISSIONS_PENDING ");
        _sb.AppendLine(" ORDER BY   JCP_ID ASC                  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(CommissionsPending);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("JunketsBusinessLogic.GetCommissionsPending");
      }

      return false;
    } // GetCommissionsPending

    /// <summary>
    /// Remove pending commission processed
    /// </summary>
    /// <param name="CommissionId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DeleteCommissionPendingProcessed(Int64 CommissionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" DELETE FROM   JUNKETS_COMMISSIONS_PENDING ");
        _sb.AppendLine("       WHERE   JCP_ID = @pId               ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = CommissionId;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      Log.Error(String.Format("JunketsBusinessLogic.DeleteCommissionPendingProcessed -> CommissionId: {0}", CommissionId));

      return false;
    } // DeleteCommissionPendingProcessed

    /// <summary>
    /// Set commission by visit
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="VisitDate"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean SetCommissionVisit(Int64 AccountId, DateTime VisitDate, SqlTransaction SqlTrx)
    {

      Int64 _flyer_id;
      Boolean _has_visits_today;
      JunketsCommissionsMovements _movements;
      Int64 _junket;

      try
      {

        if (!IsJunketsEnabled())
        {
          return true;
        }

        // Get Flyer Id by AccountId
        if (!GetAccountValidFlyer(AccountId, SqlTrx, out _flyer_id, out _junket))
        {
          return true; // Don't have Flyer
        }

        if (!HasVisitsToday(AccountId, VisitDate, SqlTrx, out _has_visits_today))
        {
          return false;
        }

        if (!_has_visits_today)
        {
          _movements = new JunketsCommissionsMovements();
          _movements.FlyerId = _flyer_id;
          _movements.AccountId = AccountId;
          _movements.Commissions.Add(new Commission(CommissionType.ByVisit));

          if (_movements.Commissions.Count > 0)
          {
            _movements.Save(SqlTrx);
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.SetCommissionVisit -> AccountId: {0}", AccountId));
        Log.Exception(_ex);
      }

      return false;

    } // SetCommissionVisit



    /// <summary>
    /// Check if the costumer has a enrolled movement
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="HasEnrolled"></param>
    /// <returns></returns>
    private static Boolean HasEnrolledMovement(Int64 AccountId, SqlTransaction SqlTrx, out Boolean HasEnrolled)
    {

      StringBuilder _sb;
      DateTime _working_date;

      HasEnrolled = false;

      try
      {

        _working_date = Misc.TodayOpening();

        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   COUNT(1)                           ");
        _sb.AppendLine("     FROM   JUNKETS_COMMISSIONS_MOVEMENTS      ");
        _sb.AppendLine("    WHERE   JCM_ACCOUNT_ID = @pAccountId       ");
        _sb.AppendLine("      AND   JCM_TYPE = @pType                  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pType", SqlDbType.BigInt).Value = CommissionType.ByEnrolled;

          HasEnrolled = ((Int32)_cmd.ExecuteScalar() > 0);
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("JunketsBusinessLogic.HasEnrolledMovement -> AccountId: {0}", AccountId));
        Log.Exception(_ex);
      }

      return false;

    } // HasEnrolledMovement

    #endregion " Private methods "

  } // JunketsBusinessLogic

  public class Commission
  {

    #region " Properties "

    public Int64 Id { get; set; }
    public CommissionType Type { get; set; }
    public Decimal FixedValue { get; set; }
    public Decimal VariableValue { get; set; }
    public DateTime DateFrom { get; set; }
    public DateTime DateTo { get; set; }
    public DateTime Creation { get; set; }
    public DateTime Update { get; set; }
    public Decimal CommissionAmount { get; set; }

    #endregion " Properties "

    #region " Constructors "

    /// <summary>
    /// Constructor default
    /// </summary>
    public Commission()
    {
      this.Id = -1;
      this.FixedValue = 0;
      this.VariableValue = 0;
      this.Creation = WGDB.Now;
      this.Update = WGDB.Now;
    } // Commission

    /// <summary>
    /// Constructor with type
    /// </summary>
    /// <param name="Type"></param>
    public Commission(CommissionType Type)
    {
      this.Id = -1;
      this.Type = Type;
      this.FixedValue = 0;
      this.VariableValue = 0;
      this.Creation = WGDB.Now;
      this.Update = WGDB.Now;
    } // Commission

    #endregion // Constructors

  } // Commission
}
