﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JunketsCommissionsMovements.cs
// 
//   DESCRIPTION: Junkets commissions movements
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 18-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-APR-2017 AMF    First release (Header).
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.Junkets
{
  public class JunketsCommissionsMovements
  {

    #region " Members "

    private List<Commission> m_list_commissions;

    #endregion " Members "

    #region " Properties "

    public Int64 AccountId { get; set; }
    public Int64 FlyerId { get; set; }
    public Int64 OperationId { get; set; }
    public Int32 TerminalId { get; set; }
    public List<Commission> Commissions
    {
      get
      {
        if (m_list_commissions == null) m_list_commissions = new List<Commission>();

        return m_list_commissions;
      }
    }
    public Status Status { get; set; }
    public Decimal Amount { get; set; }
    public Int32 Points { get; set; }

    #endregion " Properties "

    #region " Constructor "

    /// <summary>
    /// Constructor
    /// </summary>
    public JunketsCommissionsMovements()
    {
      this.OperationId = 0;
      this.Amount = 0;
      this.Points = 0;
    } // JunketsCommissionsMovements

    #endregion " Constructor "

    #region " Public Methods "

    /// <summary>
    /// Save the movements
    /// </summary>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean Save(SqlTransaction SqlTrx)
    {
      try
      {
        if (!GetCommissionsByFlyerId(SqlTrx))
        {
          return false;
        }

        if (this.Commissions.Count == 0)
        {
          return true;
        }

        if (!InsertMovements(SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Save

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Get commissions of the junket by flyer Id
    /// </summary>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean GetCommissionsByFlyerId(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _dt_commissions;
      DataRow _dr_commission;
      DataRow[] _drs_commission;

      try
      {
        _dt_commissions = new DataTable();

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   JC_ID                                         ");
        _sb.AppendLine("            , JC_TYPE                                       ");
        _sb.AppendLine("            , JC_FIXED_VALUE                                ");
        _sb.AppendLine("            , JC_VARIABLE_VALUE                             ");
        _sb.AppendLine("            , JC_DATE_FROM                                  ");
        _sb.AppendLine("            , JC_DATE_TO                                    ");
        _sb.AppendLine("            , JC_CREATION                                   ");
        _sb.AppendLine("            , JC_UPDATE                                     ");
        _sb.AppendLine("       FROM   JUNKETS_COMMISSIONS                           ");
        _sb.AppendLine(" INNER JOIN   JUNKETS_FLYERS ON JC_JUNKET_ID = JF_JUNKET_ID ");
        _sb.AppendLine("        AND   JF_ID = @pFlyerId                             ");
        _sb.AppendLine("      WHERE   JC_FIXED_VALUE > 0                            ");
        _sb.AppendLine("         OR   JC_VARIABLE_VALUE > 0                         ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pFlyerId", SqlDbType.BigInt).Value = this.FlyerId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_dt_commissions);

            if (_dt_commissions.Rows.Count > 0)
            {
              foreach (Commission _commission in this.Commissions)
              {
                _drs_commission = _dt_commissions.Select(string.Format(" JC_TYPE = {0} ", (Int32)_commission.Type));

                if (_drs_commission.Length > 0)
                {
                  _dr_commission = _drs_commission[0];

                  _commission.Id = (Int64)_dr_commission["JC_ID"];
                  _commission.FixedValue = (Decimal)_dr_commission["JC_FIXED_VALUE"];
                  _commission.VariableValue = (Decimal)_dr_commission["JC_VARIABLE_VALUE"];
                  _commission.DateFrom = (DateTime)_dr_commission["JC_DATE_FROM"];
                  _commission.DateTo = (DateTime)_dr_commission["JC_DATE_TO"];
                  _commission.Creation = (DateTime)_dr_commission["JC_CREATION"];
                  _commission.Update = (DateTime)_dr_commission["JC_UPDATE"];

                  // Calculate commission amount
                  switch (_commission.Type)
                  {
                    case CommissionType.ByCostumer:
                    case CommissionType.ByVisit:
                    case CommissionType.ByEnrolled:
                      _commission.CommissionAmount = _commission.FixedValue;
                      break;

                    case CommissionType.ByCoinIn:
                    case CommissionType.ByBuyIn:
                      _commission.CommissionAmount = (_commission.VariableValue * this.Amount) / 100m;
                      break;

                    case CommissionType.ByTheoricalNetwin:
                      _commission.CommissionAmount = (_commission.VariableValue * GetTheoricalNetWin(SqlTrx)) / 100m;
                      break;

                    case CommissionType.ByPoints:
                      _commission.CommissionAmount = _commission.FixedValue * (Int32)this.Points;
                      break;
                  }
                }
              }
            }

            // Remove all not existing commissions
            this.Commissions.RemoveAll(_com => _com.Id == -1);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("JunketsCommissionsMovements.Save.GetCommissionsByFlyerId");
      }

      return false;
    } // GetCommissionsByFlyerId

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean InsertMovements(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _dt_commissions;
      Int32 _nr;

      try
      {
        _dt_commissions = Misc.ListToDataTable(this.Commissions);

        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   JUNKETS_COMMISSIONS_MOVEMENTS ");
        _sb.AppendLine("             ( JCM_ACCOUNT_ID                ");
        _sb.AppendLine("             , JCM_FLYER_ID                  ");
        _sb.AppendLine("             , JCM_OPERATION_ID              ");
        _sb.AppendLine("             , JCM_TYPE                      ");
        _sb.AppendLine("             , JCM_STATUS                    ");
        _sb.AppendLine("             , JCM_AMOUNT                    ");
        _sb.AppendLine("             , JCM_CREATION                  ");
        _sb.AppendLine("             )                               ");
        _sb.AppendLine("      VALUES                                 ");
        _sb.AppendLine("             ( @pAccountId                   ");
        _sb.AppendLine("             , @pFlyerId                     ");
        _sb.AppendLine("             , @pOperationId                 ");
        _sb.AppendLine("             , @pType                        ");
        _sb.AppendLine("             , @pStatus                      ");
        _sb.AppendLine("             , @pAmount                      ");
        _sb.AppendLine("             , @pCreation                    ");
        _sb.AppendLine("             )                               ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = this.AccountId;
          _sql_cmd.Parameters.Add("@pFlyerId", SqlDbType.BigInt).Value = this.FlyerId;
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = this.OperationId;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "TYPE";
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status.Pending;
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).SourceColumn = "COMMISSIONAMOUNT";
          _sql_cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = WGDB.Now;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;
            _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;

            _nr = _sql_da.Update(_dt_commissions);

            if (_nr != _dt_commissions.Rows.Count)
            {
              Log.Error("JunketsCommissionsMovements.Save.InsertMovements");

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertMovements

    /// <summary>
    /// Get Theorical netwin by terminal Id
    /// </summary>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Decimal GetTheoricalNetWin(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   ISNULL(TE_THEORETICAL_PAYOUT, @pTerminalDefaultPayout) ");
        _sb.AppendLine("       FROM   TERMINALS                                              ");
        _sb.AppendLine("      WHERE   TE_TERMINAL_ID = @pTerminalId                          ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalDefaultPayout", SqlDbType.Decimal).Value = GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout", 95) / 100m;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = this.TerminalId;

          _obj = _sql_cmd.ExecuteScalar();

          if ((_obj != null) && (_obj != DBNull.Value))
          {
            // Theorical Netwin = 100 % - Theorical Payout %
            return (this.Amount * (1 - (Decimal)_obj));
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("JunketsCommissionsMovements.Save.GetTheoricalNetWin");
      }

      // 95 % are the default theorical payout then, 5 % is the theorical netwin
      return (this.Amount * 5) / 100m;
    } // GetTheoricalNetWin

    /// <summary>
    /// Undo save operation
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean UndoSave(Int64 OperationId, SqlTransaction SqlTrx)
    {
      DataTable _data_table;

      if (!JunketsBusinessLogic.IsJunketsEnabled())
      {
        return true;
      }

      if (!GetUndoOperation(OperationId, out _data_table, SqlTrx))
      {
        return false;
      }

      if (!InsertUndoMovements(_data_table, SqlTrx))
      {
        return false;
      }

      if (!JunketsBusinessLogic.UndoInsertedFlags(OperationId, SqlTrx))
      {
        return false;
      }

      return true;
    } // UndoSave

    /// <summary>
    /// Get Undo Operation
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="Data"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean GetUndoOperation(Int64 OperationId, out DataTable Data, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      Data = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   JCM_ACCOUNT_ID                   ");
        _sb.AppendLine("        , JCM_FLYER_ID                     ");
        _sb.AppendLine("        , JCM_OPERATION_ID                 ");
        _sb.AppendLine("        , JCM_TYPE                         ");
        _sb.AppendLine("        , JCM_STATUS                       ");
        _sb.AppendLine("        , JCM_AMOUNT * -1   AS JCM_AMOUNT  ");
        _sb.AppendLine("        , JCM_CREATION                     ");
        _sb.AppendLine("   FROM   JUNKETS_COMMISSIONS_MOVEMENTS    ");
        _sb.AppendLine("  WHERE   JCM_OPERATION_ID = @pOperationId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.SelectCommand = _sql_cmd;
            _sql_da.Fill(Data);

            foreach (DataRow _row in Data.Rows)
            {
              _row.SetAdded();
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GetUndoOperation


    /// <summary>
    /// Insert Undo Movements
    /// </summary>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean InsertUndoMovements(DataTable Data, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _nr;

      try
      {

        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   JUNKETS_COMMISSIONS_MOVEMENTS ");
        _sb.AppendLine("             ( JCM_ACCOUNT_ID                ");
        _sb.AppendLine("             , JCM_FLYER_ID                  ");
        _sb.AppendLine("             , JCM_OPERATION_ID              ");
        _sb.AppendLine("             , JCM_TYPE                      ");
        _sb.AppendLine("             , JCM_STATUS                    ");
        _sb.AppendLine("             , JCM_AMOUNT                    ");
        _sb.AppendLine("             , JCM_CREATION                  ");
        _sb.AppendLine("             )                               ");
        _sb.AppendLine("      VALUES                                 ");
        _sb.AppendLine("             ( @pAccountId                   ");
        _sb.AppendLine("             , @pFlyerId                     ");
        _sb.AppendLine("             , @pOperationId                 ");
        _sb.AppendLine("             , @pType                        ");
        _sb.AppendLine("             , @pStatus                      ");
        _sb.AppendLine("             , @pAmount                      ");
        _sb.AppendLine("             , @pCreation                    ");
        _sb.AppendLine("             )                               ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "JCM_ACCOUNT_ID";
          _sql_cmd.Parameters.Add("@pFlyerId", SqlDbType.BigInt).SourceColumn = "JCM_FLYER_ID";
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "JCM_OPERATION_ID";
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "JCM_TYPE";
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = 0;
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).SourceColumn = "JCM_AMOUNT";
          _sql_cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = WGDB.Now;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;
            _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;

            _nr = _sql_da.Update(Data);

            if (_nr != Data.Rows.Count)
            {
              Log.Error("JunketsCommissionsMovements.Save.InsertUndoMovements");

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertMovements

    #endregion " Private Methods "

  }
}
