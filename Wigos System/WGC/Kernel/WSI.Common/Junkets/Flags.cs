﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public class Flag
  {
    public enum FlagTypes
    {
      Manual = 0,
      Automatic = 1
    }

    public enum RelatedTypes
    {
      NONE = 0,
      FLYER = 1
    }
  }
}
