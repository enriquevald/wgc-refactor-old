//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Coljuegos.cs
// 
//   DESCRIPTION: Coljuegos class
// 
//        AUTHOR: alberto Marcos
// 
// CREATION DATE: 15-AUG-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-AUG-2015 AMF    First version.
// 09-FEB-2016 JCA    PBI 9138: Release 20150930 Coljuegos y ChileSioc: Correcciones: Add GP for report End of previous day
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

using System.Text;

namespace WSI.Common
{
  static public class ColJuegos
  {
    public enum ENUM_COLJUEGOS_TECHNICAL_EVENTS
    {
      ENUM_COLJUEGOS_TECHNICAL_EVENTS_NONE = 0,
      ENUM_COLJUEGOS_TECHNICAL_EVENTS_HOST_DISCONNECTED = 1,
      ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_DISCONNECTED = 2,
      ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_MEMORY_FAIL = 3,
      ENUM_COLJUEGOS_TECHNICAL_EVENTS_HOST_MEMORY_FAIL = 4,
      ENUM_COLJUEGOS_TECHINCAL_EVENTS_MACHINE_GAME_CHANGE = 5,
      ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_RESTORING_ON_POWER_UP = 6,
      ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_METER_ROLLOVER = 7,
      ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_RESET_MEMORY = 8,
      ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_NUID_CHANGE = 9,
    }

    public static Boolean InsertColJuegosEvent(ENUM_COLJUEGOS_TECHNICAL_EVENTS Event, Int32 TerminalId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!InsertColJuegosEvent(Event, TerminalId, _db_trx.SqlTransaction))
        {
          return false;
        }
        _db_trx.Commit();
      }

      return true;
    }

    public static bool InsertColJuegosEvent(ENUM_COLJUEGOS_TECHNICAL_EVENTS Event, Int32 TerminalId, SqlTransaction _db_trx)
    {

      if (!GeneralParam.GetBoolean("Interface.Coljuegos", "Enabled", false))
      {
        return true;
      }

      try
      {
        using (SqlCommand _cmd = new SqlCommand("SP_GenerateColombiaEventRegister", _db_trx.Connection, _db_trx))
        {
          _cmd.CommandType = CommandType.StoredProcedure;

          _cmd.Parameters.Add("@pTerminal", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pEventId", SqlDbType.Int).Value = Event;
          if (GeneralParam.GetBoolean("Interface.Coljuegos", "RegisterEndOfTheDay", true))
          {
            _cmd.Parameters.Add("@pEndDayRegisterDatetime", SqlDbType.DateTime).Value = GetLastDayRegister();
          }

          if (_cmd.ExecuteNonQuery() == 0)
          {
            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert ColJuegos Technical event.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - NewMachineMeters
    //          - OldMachineMeters
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True if ok, false otherwise
    //
    public static Boolean InsertColJuegosMachineMetersEvent(Int32 TerminalId, MachineMeters OldMachineMeters, MachineMeters NewMachineMeters)
    {

      Boolean _new_is_zero;
      Boolean _old_is_zero;
      ENUM_COLJUEGOS_TECHNICAL_EVENTS _event;

      if (!NewMachineMeters.PlayWonAvalaible)
      {
        return true;
      }

      if (!GeneralParam.GetBoolean("Interface.Coljuegos", "Enabled", false))
      {
        return true;
      }

      try
      {
        _event = ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_NONE;
        _new_is_zero = (NewMachineMeters.PlayedCents == 0
                        && NewMachineMeters.WonCents == 0
                        && NewMachineMeters.PlayedQuantity == 0);

        _old_is_zero = (OldMachineMeters.PlayedCents == 0
                        && OldMachineMeters.WonCents == 0
                        && OldMachineMeters.PlayedQuantity == 0);

        if (_new_is_zero && !_old_is_zero)
        {
          _event = ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_RESET_MEMORY;
        }
        else
        {
          if (NewMachineMeters.PlayedCents < OldMachineMeters.PlayedCents
              || NewMachineMeters.WonCents < OldMachineMeters.WonCents
              || NewMachineMeters.JackpotCents < OldMachineMeters.JackpotCents)
          {
            _event = ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_METER_ROLLOVER;
          }
        }


        if (_event == ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_NONE)
        {
          return true;
        }

        return ColJuegos.InsertColJuegosEvent(_event, TerminalId);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertMachineMetersEvent

    private static DateTime GetLastDayRegister()
    {
      DateTime _date;
      string[] _gp_date;
      Int32 _hour;
      Int32 _minute;

      _gp_date = GeneralParam.GetString("Interface.Coljuegos", "ReportTime").Split(':');

      _date = WGDB.Now;

      if (!Int32.TryParse(_gp_date[0], out _hour))
      {
        _hour = 0;
      }
      if (!Int32.TryParse(_gp_date[1], out _minute))
      {
        _minute = 0;
      }
      _date = new DateTime(_date.Year, _date.Month, _date.Day, _hour, _minute, 0);
      _date = _date.AddSeconds(-1);

      return _date;
    }
  }
}
