﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GameCreatedRequest.cs
//// 
////      DESCRIPTION: Game created request for Web Service
//// 
////           AUTHOR: Samuel González
//// 
////    CREATION DATE: 15-OCT-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 15-OCT-2015 SGB    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;

namespace WSI.Common
{
  [Serializable, XmlRoot(ElementName = "request")]
  public class GameCreatedRequest
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "gameid")]
    public String GameId { get; set; }

    [XmlElementAttribute(ElementName = "gameinstanceid")]
    public String GameInstanceId { get; set; }

    [XmlElementAttribute(ElementName = "gamestarts")]
    public String GameStarts { get; set; }

    [XmlElementAttribute(ElementName = "firstprize")]
    public String FirstPrize { get; set; }

    [XmlElementAttribute(ElementName = "entrycost")]
    public String EntryCost { get; set; }

    [XmlElementAttribute(ElementName = "gamelogo")]
    public String GameLogo { get; set; }

    [XmlElementAttribute(ElementName = "gametitle")]
    public String GameTitle { get; set; }

    [XmlElementAttribute(ElementName = "jackpot")]
    public String Jackpot { get; set; }

    [XmlElementAttribute(ElementName = "authentication")]
    public Authentication Authentication { get; set; }
  }

  public class Authentication
  {
    [XmlElementAttribute(ElementName = "partnerid")]
    public String PartnerId { get; set; }

    [XmlElementAttribute(ElementName = "password")]
    public String Password { get; set; }
  }
}