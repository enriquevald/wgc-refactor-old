﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GameGateway.cs
//// 
////      DESCRIPTION: GameGateway
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 16-FEB-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 16-FEB-2016 AMF    First release.
//// 26-FEB-2016 AMF    PBI 10013: Refactor message error
//// 02-MAR-2016 AMF    PBI 10193: WCPMessage and InSession
//// 09-MAR-2016 JCA    BUG 10535: PimPamGO: Error actualizando balance reservado
//// 09-MAR-2016 AMF    PBI 10452: New Rollback Request
//// 16-MAR-2016 FJC    PBI 9105: BonoPlay: LCD: Cambios varios
//// 01-APR-2016 FJC    BUG 11286:PimPamGo: No se elimina el mensaje de "OK" de premio otorgado
//// 04-APR-2016 FJC    BUG 7585: No se insertan Movimientos de cuenta al reservar créditos desde el LCD
//// 01-JUN-2016 JCA    BUG 14059:PimPamGo: No se inserta el jugado/ganado en el redimible de sesiones de juego
//// 06-JUN-2016 JCA    Bug 14240:PimPamGo: Al jugar no genera puntos
//// 09-JUN-2016 JCA    Bug 14150:PimPamGo: en "reporte de sorteos" no salen las anulaciones
//// 13-JUN-2016 JCA    Bug 14165:PimPamGo: El jugado en movimientos de cuenta no cuadra con el jugado real
////------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public class GameGateway
  {

    #region " Constants "

    private const String ACCOUNT_HOLDER_NAME = "AC_HOLDER_NAME3";
    private const String ACCOUNT_BLOCKED = "AC_BLOCKED";
    private const String ACCOUNT_RESERVED = "AC_RE_RESERVED";

    private const String GB_USER_ID = "GB_ACCOUNT_ID";
    private const String GB_TOTAL_PRIZE = "GB_TOTAL_PRIZE";
    private const String GB_JACKPOT = "GB_JACKPOT_PRIZE";
    private const String GB_NUM_CREDITS = "GB_NUM_PRIZES";
    private const String GB_TOTAL_BET = "GB_TOTAL_BET";
    private const String GB_NUM_BETS = "GB_NUM_BETS";
    private const String GB_GAME_ID = "GB_GAME_ID";
    private const String GB_GAME_INSTANCE_ID = "GB_GAME_INSTANCE_ID";
    private const String GB_PARTNER_ID = "GB_PARTNER_ID";
    private const String GB_TRANSACTION_ID = "GB_TRANSACTION_ID";
    private const String GB_TRANSACTION_TYPE = "GB_TRANSACTION_TYPE";
    private const String GB_CREATED = "GB_CREATED";

    private const String GP_PROVIDER_TYPE_GAMEGATEWAY = "GameGateway";
    private const String GP_PROVIDER_TYPE_PARIPLAY = "PariPlay";

    private const String GAME_UKNOWN = "uknown";

    private const String GCM_TERMINAL_ID = "GCM_TERMINAL_ID";

    #endregion " Constants "

    #region " Enums "

    public enum RESPONSE_ERROR_CODE
    {
      OK = 0,
      NOT_ENOUGH_BALANCE = 1,
      ACCOUNT_BLOCKED = 2,
      ERROR = 3,

      // Generics (100 to 119)
      ERR_GENERAL = 100,
      ERR_NO_BODY = 101,
      ERR_NO_REQUESTTYPE = 102,
      ERR_INVALID_REQUESTTYPE = 103,
      ERR_NO_SESSIONID = 104,
      ERR_WRONG_AUTH = 105,
      ERR_NO_TRUSTEDIP = 106,
      ERR_TXNID_EXISTS = 107,
      ERR_TOKEN_EXPIRED = 108,
      ERR_TOKEN_INVALID = 109,

      // PariPlay (120 to 139)
      ERR_INVALID_NEGATIVE_AMOUNT = 120,
      ERR_UNKNOWN_TRANSACTIONID = 121,
      ERR_INVALID_GAME = 122,
      ERR_ROUND_INVALID = 123,
      ERR_INVALID_USER_ID = 124,
      ERR_TRANSACTION_ALREADY_SETTLED = 125,
      ERR_PLAYER_LIMIT_EXCEEDED_TURNOVER = 126,
      ERR_PLAYER_LIMIT_EXCEEDED_SESSION_TIME = 127,
      ERR_PLAYER_LIMIT_EXCEEDED_SESSION_STAKE = 128,
      ERR_ROUND_ALREADY_ENDED = 129,
      ERR_TRANSACTION_ALREADY_CANCELLED = 130,
      ERR_PLAYER_HAVE_OPENED_ROUNDS = 131,
      ERR_NO_ROUNDS_LEFT = 132,

      ERR_OP_GENERAL = 200,
      ERR_OP_AUTHENTICATION = 201,
      ERR_OP_ACCOUNTING = 202
    }

    public enum TRANSACTION_TYPE
    {
      BET = 1,
      BET_ROLLBACK = 2,
      PRIZE = 3,
      PRIZE_ROLLBACK = 4,
      PRIZE_PENDING = 5,
      BET_ROLLBACK_PENDING = 6,
      PRIZE_ROLLBACK_PENDING = 7
    }

    public enum TYPE_WCP
    {
      DRAW = 1,
      PRIZE = 2,
      DEBIT = 3
    }

    public enum PROVIDER_TYPE
    {
      PimPamGO = 1,
      PariPlay = 2
    }

    public enum LOG_MODE
    {
      NONE = 0,
      WCP = 1,
      BD = 2,
      Extended = 99
    }

    #endregion " Enums "

    #region " Public Methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieve all relevant data related to an account from the database
    //          using the account identifier.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - LCD_Session
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static void GetDataAccount(ref LCD_Session Session, SqlTransaction Trx)
    {
      GetDataAccount(ref Session, true, Trx);
    } // GetDataAccount

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieve all relevant data related to an account from the database
    //          using the account identifier.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - LCD_Session
    //          - CheckBlocked
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static void GetDataAccount(ref LCD_Session Session, Boolean CheckBlocked, SqlTransaction Trx)
    {
      DataTable _table;
      StringBuilder _sb;
      MultiPromos.AccountBalance _account_balance;

      try
      {
        Session.Balance = new MultiPromos.AccountBalance();
        _account_balance = new MultiPromos.AccountBalance();
        _sb = new StringBuilder();
        _table = new DataTable();

        _sb.AppendLine(" SELECT   ISNULL(ISNULL(AC_HOLDER_NAME3,'')+'-'+RIGHT('00'+RTRIM(DAY(AC_HOLDER_BIRTH_DATE)),2)+RIGHT('00'+RTRIM(MONTH(AC_HOLDER_BIRTH_DATE)),2),'@'+RTRIM(AC_ACCOUNT_ID)) AS AC_HOLDER_NAME3");
        _sb.AppendLine("        , AC_BLOCKED                                                                                                                                                      ");
        _sb.AppendLine("   FROM   ACCOUNTS                                                                                                                                                        ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId                                                                                                                                     ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Session.AccountId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_table);

            if (_table.Rows.Count != 1)
            {
              Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
              Session.ErrorMessage = "GetDataAccount. Incorrect AccountId: " + Session.AccountId.ToString();

              Log.Message(Session.ErrorMessage);

              return;
            }

            if (Boolean.Parse(_table.Rows[0][ACCOUNT_BLOCKED].ToString()) && CheckBlocked)
            {
              Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ACCOUNT_BLOCKED;
              Session.ErrorMessage = "GetDataAccount. Account blocked" + Session.AccountId.ToString();

              Log.Message(Session.ErrorMessage);

              return;
            }

            if (!MultiPromos.Trx_UpdateAccountBalance(Session.AccountId, MultiPromos.AccountBalance.Zero, out _account_balance, Trx))
            {
              Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
              Session.ErrorMessage = "GetDataAccount.Trx_UpdateAccountBalance. Error get balance AccountId: " + Session.AccountId.ToString();

              Log.Message(Session.ErrorMessage);

              return;
            }

            Session.Balance = _account_balance;
            Session.NickName = _table.Rows[0][ACCOUNT_HOLDER_NAME].ToString();
          }
        }
      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "GetDataAccount. Catch Exception AccountId: " + Session.AccountId.ToString();

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }
    } // GetDataAccount

    //------------------------------------------------------------------------------
    // PURPOSE: Get total prizes amount pending by AccountId
    // 
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Decimal
    //   NOTES:
    //
    public static Decimal GetGameGatewayTotalPrizesPendingAmount(Int64 AccountId)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        if (WSI.Common.GeneralParam.GetBoolean("GameGateway", "AwardPrizes", true))
        {
          return 0;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   SUM(GB_TOTAL_PRIZE)                          ");
        _sb.AppendLine("     FROM   GAMEGATEWAY_BETS                             ");
        _sb.AppendLine("    WHERE   GB_ACCOUNT_ID       = @pAccountId            ");
        _sb.AppendLine("      AND   GB_TRANSACTION_TYPE = @pTransactionTypePrize ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTransactionTypePrize", SqlDbType.Int).Value = TRANSACTION_TYPE.PRIZE_PENDING;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              return (Decimal)_obj;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return 0;
    } // GetGameGatewayTotalPrizesPendingAmount

    //------------------------------------------------------------------------------
    // PURPOSE: Manage GameGateway Bulk
    // 
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //   NOTES:
    //
    public static Boolean ManageGameGatewayBulk(Int64 AccountId)
    {
      DataTable _dt_bulk;

      _dt_bulk = GetGameGatewayBulk(null, AccountId);

      if (_dt_bulk.Rows.Count == 0)
      {
        return true;
      }

      if (ProcessGameGatewayBulk(_dt_bulk, true))
      {
        return true;
      }

      return false;
    } // ManageGameGatewayBulk

    //------------------------------------------------------------------------------
    // PURPOSE: Manage GameGateway Bulk
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TransactionId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //   NOTES:
    //
    public static Boolean ManageGameGatewayBulk(String TransactionId)
    {
      DataTable _dt_bulk;

      _dt_bulk = GameGateway.GetGameGatewayBulk(TransactionId, -1);

      if (_dt_bulk.Rows.Count == 0)
      {
        return true;
      }

      if (GameGateway.ProcessGameGatewayBulk(_dt_bulk, false))
      {
        return true;
      }

      return false;
    } // ManageGameGatewayBulk

    //------------------------------------------------------------------------------
    // PURPOSE: Manage GameGateway Bulk
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //   NOTES:
    //
    public static Boolean ManageGameGatewayBulk()
    {
      DataTable _dt_bulk;

      _dt_bulk = GetGameGatewayBulk(null, -1);

      if (_dt_bulk.Rows.Count == 0)
      {
        return true;
      }

      if (ProcessGameGatewayBulk(_dt_bulk, false))
      {
        return true;
      }

      return false;
    } // ManageGameGatewayBulk

    //------------------------------------------------------------------------------
    // PURPOSE: Get mode reserved of account
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    public static Boolean AccountModeReserved(Int64 AccountId)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   AC_MODE_RESERVED            ");
        _sb.AppendLine("   FROM   ACCOUNTS                    ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj == null || _obj == DBNull.Value)
            {
              return false;
            }
            else
            {
              return (Boolean)_obj;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // AccountModeReserved

    //------------------------------------------------------------------------------
    // PURPOSE: Check if PlaySessionId is correct
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    public static Boolean CheckPlaySession(LCD_Session Session)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   COUNT (PS_PLAY_SESSION_ID)           ");
        _sb.AppendLine("   FROM   PLAY_SESSIONS                        ");
        _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");
        _sb.AppendLine("    AND   PS_ACCOUNT_ID      = @pAccountId     ");
        _sb.AppendLine("    AND   PS_TERMINAL_ID     = @pTerminalId    ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = Session.PlaySessionId;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Session.AccountId;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Session.TerminalId;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj == null || _obj == DBNull.Value)
            {
              return false;
            }
            else
            {
              return (Int32)_obj == 1 ? true : false;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CheckPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE: Process Credit/Debit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - AddAmount
    //          - DecAmount
    //          - LCD_Session
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static void ProcessCreditDebit(Decimal AddAmount, Decimal SubAmount, ref LCD_Session Session, SqlTransaction Trx)
    {
      MultiPromos.AccountBalance _add_sub_balance;
      MultiPromos.AccountBalance _out_final_balance;
      AccountMovementsTable _account_movements;
      MovementType _movement_type;
      Int32 _terminal_id;
      String _terminal_name;
      Int64 _play_session_id;
      GameCreatedRequest _uknown;
      Boolean _duplicated;
      String _movement_detail;
      Decimal _sub_amount_tmp;
      Decimal _add_amount_tmp;

      String _msg;

      try
      {
        _add_sub_balance = new MultiPromos.AccountBalance();
        _terminal_name = String.Empty;
        _movement_detail = (Session.TerminalId == 0) ? String.Empty : String.Format("<egmterminalid>{0}</egmterminalid>", Session.TerminalId);
        _sub_amount_tmp = SubAmount;
        _add_amount_tmp = AddAmount;

        // Add extended Log
        if (GeneralParam.GetInt32(GetProviderGPGroupKey(Session.ProviderType), "Log.Mode", 1) == (Int32)LOG_MODE.Extended)
        {
          _msg = String.Format("ProcessCreditDebit(Decimal AddAmount[{0}], Decimal SubAmount[{1}], ref LCD_Session Session[{2}], SqlTransaction Trx)", AddAmount, SubAmount, Session);
          Log.Message(_msg);
          _msg = String.Format("Session.TotalAmount =[{0}], Session.Balance.Redeemable = [{1}], Session.Balance.TotalRedeemable = [{2}]", Session.TotalAmount, Session.Balance.Redeemable, Session.Balance.TotalRedeemable);
          Log.Message(_msg);
        }

        switch (Session.TransactionType)
        {
          case GameGateway.TRANSACTION_TYPE.BET:
            _movement_type = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? MovementType.GameGatewayBet
                                                                              : MovementType.PariPlayBet;

            Session.TotalAmount = _sub_amount_tmp * -1;

            if (Session.Balance.Redeemable >= _sub_amount_tmp)
            {
              _add_sub_balance.Redeemable = _sub_amount_tmp;
            }
            else
            {
              _add_sub_balance.Redeemable = Session.Balance.Redeemable;
              _sub_amount_tmp -= _add_sub_balance.Redeemable;
              _add_sub_balance.PromoRedeemable = _sub_amount_tmp;
            }

            break;

          case GameGateway.TRANSACTION_TYPE.PRIZE:
          case GameGateway.TRANSACTION_TYPE.PRIZE_PENDING:
            _movement_type = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? MovementType.GameGatewayPrize
                                                                              : MovementType.PariPlayPrize;
            Session.TotalAmount = _add_amount_tmp;
            _add_sub_balance.Redeemable = _add_amount_tmp * -1;

            break;

          case GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING:
            _movement_type = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? MovementType.GameGatewayBetRollback
                                                                              : MovementType.PariPlayBetRollback;
            Session.TotalAmount = _sub_amount_tmp;
            _add_sub_balance.Redeemable = _sub_amount_tmp;

            break;

          default:
            // Default
            _movement_type = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? MovementType.GameGatewayPrize
                                                                              : MovementType.PariPlayPrize;
            break;
        }


        if (!MultiPromos.Trx_UpdateAccountBalance(Session.AccountId, MultiPromos.AccountBalance.Negate(_add_sub_balance), out _out_final_balance, Trx))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
          Session.ErrorMessage = "ProcessCreditDebit.Trx_UpdateAccountBalance. Error update balance AccountId: " + Session.AccountId.ToString();

          Log.Message(Session.ErrorMessage);

          return;
        }

        // Add extended Log
        if (GeneralParam.GetInt32(GetProviderGPGroupKey(Session.ProviderType), "Log.Mode", 1) == (Int32)LOG_MODE.Extended)
        {
          _msg = String.Format("MultiPromos.Trx_UpdateAccountBalance(Session.AccountId, MultiPromos.AccountBalance.Negate(_add_sub_balance[{0}]), out _out_final_balance[{1}], Trx)", _add_sub_balance, _out_final_balance);
          Log.Message(_msg);
        }

        if (Session.ReservedCreditEnabled == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS
          || Session.ReservedCreditEnabled == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)
        {
          if (!UpdateReservedAccount(ref Session, _out_final_balance, Trx))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
            Session.ErrorMessage = "ProcessCreditDebit.UpdateReservedAccount AccountId: " + Session.AccountId.ToString();

            Log.Message(Session.ErrorMessage);

            return;
          }
        }

        if (!InsertGameGatewayPlaySession(Session, _out_final_balance, out _play_session_id, out _terminal_id, out _terminal_name, Trx))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
          Session.ErrorMessage = "ProcessCreditDebit.InsertGameGatewayPlaySession AccountId: " + Session.AccountId.ToString();

          Log.Message(Session.ErrorMessage);

          return;
        }

        if (GeneralParam.GetInt32(GetProviderGPGroupKey(Session.ProviderType), "Log.Mode", 1) == (Int32)LOG_MODE.Extended)
        {
          _msg = String.Format("InsertGameGatewayPlaySession(Session, _out_final_balance[{0}], out _play_session_id[{1}], out _terminal_id[{2}], out _terminal_name[{3}], Trx)",
                                  _out_final_balance, _play_session_id, _terminal_id, _terminal_name);
          Log.Message(_msg);
        }

        Session.GameTerminalId = _terminal_id;

        if (Session.ProviderType == PROVIDER_TYPE.PimPamGO)
        {
          _uknown = CreateGameCreatedRequestUknown(Session.GameId.ToString(), Session.GameInstanceId.ToString());

          if (!InsertGameGatewayGames(_uknown, ref Session, false, Trx))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
            Session.ErrorMessage = "ProcessCreditDebit.InsertGameGatewayGames GameId: " + Session.GameId.ToString() + ", GameInstaceId: " + Session.GameInstanceId.ToString();

            Log.Message(Session.ErrorMessage);

            return;
          }
        }

        if (!ManageGameGatewayGameMeters(Session, Trx))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
          Session.ErrorMessage = "ProcessCreditDebit.ManageGameGatewayGameMeters GameId: " + Session.GameId.ToString() + ", GameInstaceId: " + Session.GameInstanceId.ToString();

          Log.Message(Session.ErrorMessage);

          return;
        }

        _account_movements = new AccountMovementsTable(_terminal_id, _terminal_name, _play_session_id);

        if (!_account_movements.Add(0, Session.AccountId, _movement_type, Session.Balance.TotalBalance, SubAmount, AddAmount, _out_final_balance.TotalBalance, _movement_detail))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
          Session.ErrorMessage = "ProcessCreditDebit.AccountMovements.Add. AccountId: " + Session.AccountId.ToString();

          Log.Message(Session.ErrorMessage);

          return;
        }

        // Add extended Log
        if (GeneralParam.GetInt32(GetProviderGPGroupKey(Session.ProviderType), "Log.Mode", 1) == (Int32)LOG_MODE.Extended)
        {
          _msg = String.Format("_account_movements.Add(0, Session.AccountId[{0}], _movement_type[{1}], Session.Balance.TotalBalance[{2}], SubAmount[{3}], AddAmount[{4}], _out_final_balance.TotalBalance[{5}], _movement_detail[{6}])",
                                  Session.AccountId, _movement_type, Session.Balance.TotalBalance, SubAmount, AddAmount, _out_final_balance.TotalBalance, _movement_detail);
          Log.Message(_msg);
        }

        _account_movements.Save(Trx);

        Session.MovementId = _account_movements.LastSavedMovementId;

        Session.PlaySessionId = _play_session_id;

        if (Session.TransactionType != GameGateway.TRANSACTION_TYPE.PRIZE_PENDING && Session.TransactionType != GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING)
        {
          if (!InsertGameGatewayBets(Session, Trx, out _duplicated))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
            Session.ErrorMessage = "ProcessCreditDebit.InsertGameGatewayBets";

            if (_duplicated)
            {
              if (Session.ProviderType == PROVIDER_TYPE.PariPlay)
              {
                Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_TRANSACTION_ALREADY_SETTLED;
              }

              Session.ErrorMessage += ". Duplicated registers in TransactionId: ";
              Session.ErrorMessage += String.Format("[{0}]", Session.TransactionId);
            }

            Log.Message(Session.ErrorMessage);

            return;
          }
        }

        if (!UpdateGameGatewayGames(Session, Trx))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
          Session.ErrorMessage = "ProcessCreditDebit.UpdateGameGatewayGames GameId: " + Session.GameId.ToString() + ", GameInstaceId: " + Session.GameInstanceId.ToString();

          Log.Message(Session.ErrorMessage);

          return;
        }

        // Finally actions by provider type
        switch (Session.ProviderType)
        {
          case PROVIDER_TYPE.PimPamGO:
            GetAndInsertCommand_PimPamGO(ref Session, Trx);
            break;

          case PROVIDER_TYPE.PariPlay:
            Session.Balance = _out_final_balance;
            break;
        }

      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "ProcessCreditDebit. Catch Exception AccountId+ " + Session.AccountId.ToString();

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

    } // ProcessCreditDebit

    //------------------------------------------------------------------------------
    // PURPOSE: Create new instance of GameCreatedRequest, mode uknown
    // 
    //  PARAMS:
    //      - INPUT:
    //          - GameId
    //          - GameInstanceId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - GameCreatedRequest
    // 
    //   NOTES:
    //
    public static GameCreatedRequest CreateGameCreatedRequestUknown(String GameId, String GameInstanceId)
    {
      GameCreatedRequest _uknown;

      _uknown = new GameCreatedRequest();
      _uknown.GameId = GameId;
      _uknown.GameInstanceId = GameInstanceId;
      _uknown.GameTitle = GAME_UKNOWN;
      _uknown.GameStarts = ToEpochTime(WGDB.Now);
      _uknown.FirstPrize = "0.00";
      _uknown.EntryCost = "0.00";
      _uknown.Jackpot = "0.00";
      _uknown.GameLogo = String.Empty;

      return _uknown;
    } // CreateGameCreatedRequestUknown

    //------------------------------------------------------------------------------
    // PURPOSE: Insert in table gameGateway_games
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Request
    //        - Session
    //        - DoUpdate
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //
    //   NOTES:
    //
    public static Boolean InsertGameGatewayGames(GameCreatedRequest Request, ref LCD_Session Session, Boolean DoUpdate)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (InsertGameGatewayGames(Request, ref Session, DoUpdate, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return false;
    } // InsertGameGatewayGames

    //------------------------------------------------------------------------------
    // PURPOSE: Insert in table gameGateway_games
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Request
    //        - Session
    //        - DoUpdate
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //
    //   NOTES:
    //
    public static Boolean InsertGameGatewayGames(GameCreatedRequest Request, ref LCD_Session Session, Boolean DoUpdate, SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameterCollection _params;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" IF NOT EXISTS (SELECT   1                                       ");
        _sb.AppendLine("                  FROM   GAMEGATEWAY_GAMES                       ");
        _sb.AppendLine("                 WHERE   GG_GAME_ID = @pGameId                   ");
        _sb.AppendLine("                   AND   GG_PARTNER_ID = @pPartnerId)            ");
        _sb.AppendLine(" BEGIN                                                           ");
        _sb.AppendLine(" INSERT INTO GAMEGATEWAY_GAMES                                   ");
        _sb.AppendLine("             (                                                   ");
        _sb.AppendLine("               GG_GAME_ID                                        ");
        _sb.AppendLine("             , GG_PARTNER_ID                                     ");
        _sb.AppendLine("             , GG_NAME                                           ");
        _sb.AppendLine("             , GG_NAME_TRANSLATED                                ");
        _sb.AppendLine("             , GG_LOGO                                           ");
        _sb.AppendLine("             , GG_CREATED                                        ");
        _sb.AppendLine("             )                                                   ");
        _sb.AppendLine("     VALUES  (                                                   ");
        _sb.AppendLine("               @pGameId                                          ");
        _sb.AppendLine("             , @pPartnerId                                       ");
        _sb.AppendLine("             , @pName                                            ");
        _sb.AppendLine("             , @pNameTranslated                                  ");
        _sb.AppendLine("             , @pLogo                                            ");
        _sb.AppendLine("             , @pCreated                                         ");
        _sb.AppendLine("             )                                                   ");
        _sb.AppendLine(" END                                                             ");
        _sb.AppendLine(" ELSE                                                            ");
        _sb.AppendLine(" BEGIN                                                           ");
        _sb.AppendLine(" IF (1 = @pDoUpdate)                                             ");
        _sb.AppendLine(" UPDATE   GAMEGATEWAY_GAMES                                      ");
        _sb.AppendLine("    SET   GG_NAME            = @pName                            ");
        _sb.AppendLine("        , GG_NAME_TRANSLATED = @pNameTranslated                  ");
        _sb.AppendLine("        , GG_LOGO            = @pLogo                            ");
        _sb.AppendLine("  WHERE   GG_GAME_ID         = @pGameId                          ");
        _sb.AppendLine("    AND   GG_PARTNER_ID      = @pPartnerId                       ");
        _sb.AppendLine(" END                                                             ");
        _sb.AppendLine("                                                                 ");
        _sb.AppendLine(" IF NOT EXISTS (SELECT   1                                       ");
        _sb.AppendLine("                  FROM   GAMEGATEWAY_GAME_INSTANCES              ");
        _sb.AppendLine("                 WHERE   GGI_GAME_INSTANCE_ID = @pGameInstanceId ");
        _sb.AppendLine("                   AND   GGI_PARTNER_ID = @pPartnerId)           ");
        _sb.AppendLine(" BEGIN                                                           ");
        _sb.AppendLine(" INSERT INTO GAMEGATEWAY_GAME_INSTANCES                          ");
        _sb.AppendLine("             (                                                   ");
        _sb.AppendLine("               GGI_GAME_ID                                       ");
        _sb.AppendLine("             , GGI_PARTNER_ID                                    ");
        _sb.AppendLine("             , GGI_GAME_INSTANCE_ID                              ");
        _sb.AppendLine("             , GGI_FIRST_PRIZE                                   ");
        _sb.AppendLine("             , GGI_ENTRY_COST                                    ");
        _sb.AppendLine("             , GGI_JACKPOT                                       ");
        _sb.AppendLine("             , GGI_STARTS                                        ");
        _sb.AppendLine("             , GGI_CREATED                                       ");
        _sb.AppendLine("             )                                                   ");
        _sb.AppendLine("     VALUES  (                                                   ");
        _sb.AppendLine("               @pGameId                                          ");
        _sb.AppendLine("             , @pPartnerId                                       ");
        _sb.AppendLine("             , @pGameInstanceId                                  ");
        _sb.AppendLine("             , @pFirstPrize                                      ");
        _sb.AppendLine("             , @pEntryCost                                       ");
        _sb.AppendLine("             , @pJackpot                                         ");
        _sb.AppendLine("             , @pStarts                                          ");
        _sb.AppendLine("             , @pCreated                                         ");
        _sb.AppendLine("             )                                                   ");
        _sb.AppendLine(" END                                                             ");
        _sb.AppendLine(" ELSE                                                            ");
        _sb.AppendLine(" BEGIN                                                           ");
        _sb.AppendLine(" IF (1 = @pDoUpdate)                                             ");
        _sb.AppendLine(" UPDATE   GAMEGATEWAY_GAME_INSTANCES                             ");
        _sb.AppendLine("    SET   GGI_FIRST_PRIZE      = @pFirstPrize                    ");
        _sb.AppendLine("        , GGI_ENTRY_COST       = @pEntryCost                     ");
        _sb.AppendLine("        , GGI_JACKPOT          = @pJackpot                       ");
        _sb.AppendLine("        , GGI_STARTS           = @pStarts                        ");
        _sb.AppendLine("  WHERE   GGI_GAME_INSTANCE_ID = @pGameId                        ");
        _sb.AppendLine("    AND   GGI_PARTNER_ID       = @pPartnerId                     ");
        _sb.AppendLine(" END                                                             ");
        _sb.AppendLine("                                                                 ");
        _sb.AppendLine(" SELECT   @pShowLCDMessage = GG_SHOW_LCD_MESSAGE                 ");
        _sb.AppendLine("        , @pGameName       = GG_NAME                             ");
        _sb.AppendLine("   FROM   GAMEGATEWAY_GAMES                                      ");
        _sb.AppendLine("  WHERE   GG_GAME_ID = @pGameId                                  ");
        _sb.AppendLine("    AND   GG_PARTNER_ID = @pPartnerId                            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = Request.GameId;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = Session.PartnerId;
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar, 50).Value = String.IsNullOrEmpty(Request.GameTitle) ? String.Empty : Request.GameTitle;
          _sql_cmd.Parameters.Add("@pNameTranslated", SqlDbType.NVarChar, 50).Value = String.IsNullOrEmpty(Request.GameTitle) ? String.Empty : Request.GameTitle;
          _sql_cmd.Parameters.Add("@pStarts", SqlDbType.DateTime).Value = FromEpochTime(Int64.Parse(Request.GameStarts));
          _sql_cmd.Parameters.Add("@pFirstPrize", SqlDbType.Money).Value = Request.FirstPrize;
          _sql_cmd.Parameters.Add("@pEntryCost", SqlDbType.Money).Value = Request.EntryCost;
          _sql_cmd.Parameters.Add("@pJackpot", SqlDbType.Money).Value = Request.Jackpot;
          _sql_cmd.Parameters.Add("@pLogo", SqlDbType.NVarChar, 200).Value = Request.GameLogo;
          _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).Value = Request.GameInstanceId;
          _sql_cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = WGDB.Now;
          _sql_cmd.Parameters.Add("@pDoUpdate", SqlDbType.Int).Value = DoUpdate;

          _sql_cmd.Parameters.Add("@pShowLCDMessage", SqlDbType.Bit).Direction = ParameterDirection.Output;
          _sql_cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;

          _params = _sql_cmd.Parameters;

          _sql_cmd.ExecuteNonQuery();

          Session.ShowLCDMessage = Convert.ToBoolean(_params["@pShowLCDMessage"].Value);
          Session.GameName = Convert.ToString(_params["@pGameName"].Value);

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH;
      Session.ErrorMessage = String.Format("InsertGameGatewayGames. GameId: {0}, GameInstanceId: {1}", Request.GameId, Request.GameInstanceId);

      Log.Message(Session.ErrorMessage);

      return false;
    } // InsertGameGatewayGames

    //------------------------------------------------------------------------------
    // PURPOSE: Insert PariPlay game in GameGatewayGames
    //
    //  PARAMS:
    //     - INPUT:
    //          - GameId
    //          - SqlTrx
    //
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - True or False
    //
    public static Boolean InsertGameGatewayPariPlayGames(Int64 GameId)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (InsertGameGatewayPariPlayGames(GameId, _db_trx.SqlTransaction))
          {
            _db_trx.SqlTransaction.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InsertGameGatewayPariPlayGames
    public static Boolean InsertGameGatewayPariPlayGames(Int64 GameId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DECLARE @pGameName VARCHAR(100);             ");
        _sb.AppendLine(" SET @pGameName = '';                         ");

        _sb.AppendLine(" SELECT   @pGameName = ISNULL(GE_TITLE, '')   ");
        _sb.AppendLine("   FROM   MAPP_GAMES_EXTERNAL                 ");
        _sb.AppendLine("  WHERE   GE_GAME_EXTERNAL_ID = @pGameId     ");

        _sb.AppendLine(" INSERT INTO GAMEGATEWAY_GAMES                ");
        _sb.AppendLine("             (  GG_GAME_ID                    ");
        _sb.AppendLine("              , GG_PARTNER_ID                 ");
        _sb.AppendLine("              , GG_NAME                       ");
        _sb.AppendLine("              , GG_NAME_TRANSLATED            ");
        _sb.AppendLine("              , GG_CREATED			          )   ");
        _sb.AppendLine("     VALUES  (  @pGameId                      ");
        _sb.AppendLine("              , @pPartnerId                   ");
        _sb.AppendLine("              , @pGameName                    ");
        _sb.AppendLine("              , @pGameName                    ");
        _sb.AppendLine("              , GETDATE()                 )   ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Value = GameId;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = (Int32)PROVIDER_TYPE.PariPlay;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InsertGameGatewayPariPlayGames


    //------------------------------------------------------------------------------
    // PURPOSE: Check if the account is in a play session
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Session or AccountId
    //
    //      - OUTPUT:
    //        - WCPTerminal (Only i
    //
    // RETURNS:
    //        - True or False 
    //
    //   NOTES:
    //
    public static Boolean IsInPlaySession(ref LCD_Session Session)
    {
      DataTable _wcp_terminals;

      if (!IsInPlaySessionInternal(Session.AccountId, out _wcp_terminals))
      {
        return false;
      }

      Session.WcpTerminals = _wcp_terminals;

      return (Session.WcpTerminals.Rows.Count == 1);

    } // IsInPlaySession
    public static Boolean IsInPlaySession(Int64 AccountId)
    {
      DataTable _wcp_terminals;

      if (!IsInPlaySessionInternal(AccountId, out _wcp_terminals))
      {
        return false;
      }

      return (_wcp_terminals.Rows.Count == 1);

    } // IsInPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE: Get WCP parameter from WCP command
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Session
    //        - TypeWCP
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False 
    //
    //   NOTES:
    //
    public static Boolean GetCommandParameterGameGateway(ref LCD_Session Session, TYPE_WCP TypeWCP, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        // Dont need access BD
        if (TypeWCP == TYPE_WCP.DEBIT)
        {
          Session.WcpParameter = String.Format("<type>{0}</type><cmd_id>{1}</cmd_id><accountid>{2}</accountid><partnerid>{3}</partnerid>", (Int16)TypeWCP, Session.WCPId, Session.AccountId, Session.PartnerId.ToString("000"));

          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("     SELECT   ISNULL(GGI_JACKPOT, 0) AS JACKPOT             ");
        _sb.AppendLine("            , GG_LOGO                                       ");
        _sb.AppendLine("            , GG_SHOW_LCD_MESSAGE                           ");
        _sb.AppendLine("       FROM   GAMEGATEWAY_GAME_INSTANCES                    ");
        _sb.AppendLine(" INNER JOIN   GAMEGATEWAY_GAMES ON GG_GAME_ID = GGI_GAME_ID ");
        _sb.AppendLine("        AND   GG_PARTNER_ID = GGI_PARTNER_ID                ");
        _sb.AppendLine("      WHERE   GGI_GAME_INSTANCE_ID = @pGameInstanceId       ");
        _sb.AppendLine("        AND   GGI_PARTNER_ID       = @pPartnerId            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).Value = Session.GameInstanceId;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = Session.PartnerId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            switch (TypeWCP)
            {
              case TYPE_WCP.DRAW:
                Session.WcpParameter = String.Format("<type>{0}</type><jackpot>{1}</jackpot><logo>{2}</logo><partnerid>{3}</partnerid>", (Int16)TypeWCP, (Int32)(_reader.GetSqlMoney(0) * 100), _reader.GetSqlString(1), Session.PartnerId.ToString("000"));
                break;

              case TYPE_WCP.PRIZE:
                Session.WcpParameter = String.Format("<type>{0}</type><cmd_id>{1}</cmd_id><prize>{2}</prize><logo>{3}</logo><accountid>{4}</accountid><partnerid>{5}</partnerid>", (Int16)TypeWCP, Session.WCPId, (Int32)(Session.TotalAmount * 100), _reader.GetSqlString(1), Session.AccountId, Session.PartnerId.ToString("000"));
                break;
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetWcpParameterGameGateway

    //------------------------------------------------------------------------------
    // PURPOSE: Insert WCP commands GameGateway for each terminal with the active funcionality
    //
    //  PARAMS:
    //     - INPUT:
    //          - Session
    //          - SqlTransaction
    //
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - True or False
    //
    public static Boolean InsertGamegatewayCommandBatch(LCD_Session Session, TYPE_WCP TypeMessage, SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _dt_wcp;

      try
      {

        if (Session.WcpTerminals.Rows.Count == 0)
        {
          return true;
        }
        else
        {
          _dt_wcp = CreateCommandTerminalsTable(Session.WcpTerminals);
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO GAMEGATEWAY_COMMAND_MESSAGES ");
        _sb.AppendLine("             (                            ");
        _sb.AppendLine("               GCM_TERMINAL_ID            ");
        _sb.AppendLine("             , GCM_CODE                   ");
        _sb.AppendLine("             , GCM_TYPE                   ");
        _sb.AppendLine("             , GCM_STATUS                 ");
        _sb.AppendLine("             , GCM_PARAMETER              ");
        _sb.AppendLine("             )                            ");
        _sb.AppendLine("     VALUES  (                            ");
        _sb.AppendLine("               @pTerminalId               ");
        _sb.AppendLine("             , @pCode                     ");
        _sb.AppendLine("             , @pType                     ");
        _sb.AppendLine("             , 0                          ");
        _sb.AppendLine("             , @pParameter                ");
        _sb.AppendLine("             )                            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = GCM_TERMINAL_ID;
          _sql_cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = WCP_CommandCode.GameGateway;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = TypeMessage;
          _sql_cmd.Parameters.Add("@pParameter", SqlDbType.NVarChar).Value = Session.WcpParameter;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;
            _sql_da.UpdateBatchSize = 500;
            _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

            if (_sql_da.Update(_dt_wcp) == _dt_wcp.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertWcpCommandBatch

    //------------------------------------------------------------------------------
    // PURPOSE: Insert WCP command Message for one terminal
    //
    //  PARAMS:
    //     - INPUT:
    //          - Session
    //          - WCP_CommandCode 
    //          - GameGateway.TYPE_WCP
    //          - SqlTransaction
    //
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - True or False
    //
    public static Boolean InsertGamegatewayCommandMessage(ref LCD_Session Session, WCP_CommandCode CommandCode, GameGateway.TYPE_WCP TypeWcp, SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _param;
      DataTable _dt_wcp;

      _dt_wcp = null;

      try
      {

        if (Session.WcpTerminals.Rows.Count == 0)
        {
          return false;
        }
        else
        {
          _dt_wcp = CreateCommandTerminalsTable(Session.WcpTerminals);
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO GAMEGATEWAY_COMMAND_MESSAGES ");
        _sb.AppendLine("             (                            ");
        _sb.AppendLine("               GCM_TERMINAL_ID            ");
        _sb.AppendLine("             , GCM_CODE                   ");
        _sb.AppendLine("             , GCM_TYPE                   ");
        _sb.AppendLine("             , GCM_STATUS                 ");
        _sb.AppendLine("             )                            ");
        _sb.AppendLine("     VALUES  (                            ");
        _sb.AppendLine("               @pTerminalId               ");
        _sb.AppendLine("             , @pCode                     ");
        _sb.AppendLine("             , @pType                     ");
        _sb.AppendLine("             , 0                          ");
        _sb.AppendLine("             )                            ");
        _sb.AppendLine("                                          ");
        _sb.AppendLine(" SET @pWCPId = SCOPE_IDENTITY()           ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {

          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _dt_wcp.Rows[0][GCM_TERMINAL_ID]; //Only mst to be one record
          _sql_cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = CommandCode;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = TypeWcp;

          _param = _sql_cmd.Parameters.Add("@pWCPId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            Session.WCPId = (Int64)_param.Value;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertGamegatewayCommandMessage

    //------------------------------------------------------------------------------
    // PURPOSE: Update WCP commands
    //
    //  PARAMS:
    //     - INPUT:
    //          - Session
    //          - UpdateParameter
    //          - CreditStatus
    //          - SqlTransaction
    //
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - True or False
    //
    public static Boolean UpdateGamegatewayCommandMessage(LCD_Session Session, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GAMEGATEWAY_COMMAND_MESSAGES  ");
        _sb.AppendLine("    SET   GCM_PARAMETER = @pParameter   ");
        _sb.AppendLine("  WHERE   GCM_ID        = @pWCPId       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pParameter", SqlDbType.NVarChar).Value = Session.WcpParameter;
          _sql_cmd.Parameters.Add("@pWCPId", SqlDbType.BigInt).Value = Session.WCPId;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateGamegatewayCommandMessage

    //------------------------------------------------------------------------------
    // PURPOSE: Get Insert Bets String Builder
    // 
    //  PARAMS:
    //      - INPUT:
    //          - IsPrize
    //          - IsRollback
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - StringBuilder
    // 
    //   NOTES:
    //
    public static StringBuilder GetInsertBetsStringBuilder(Boolean IsPrize, Boolean IsRollback)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      if (IsRollback)
      {
        _sb.AppendLine(" IF NOT EXISTS (SELECT   1                                                                        ");
        _sb.AppendLine("                  FROM   GAMEGATEWAY_BETS                                                         ");
        _sb.AppendLine("                 WHERE   GB_GAME_ID          = @pGameId                                           ");
        _sb.AppendLine("                   AND   GB_GAME_INSTANCE_ID = @pGameInstanceId                                   ");
        _sb.AppendLine("                   AND   GB_TRANSACTION_TYPE = @pTransactionTypeBet                               ");
        _sb.AppendLine("                   AND   GB_TRANSACTION_ID   = @pTransactionId                                    ");
        _sb.AppendLine("                   AND   GB_NUM_BETS         = @pNumBets  * -1                                    ");
        _sb.AppendLine("                   AND   GB_TOTAL_BET        = @pTotalBet * -1                                    ");
        _sb.AppendLine("                   AND   GB_ACCOUNT_ID       = @pAccountId                                        ");
        _sb.AppendLine("                   AND   GB_PARTNER_ID       = @pPartnerId)                                       ");
        _sb.AppendLine(" BEGIN                                                                                            ");
        _sb.AppendLine(" SET @ErrorRegister = 1                                                                           ");
        _sb.AppendLine(" END                                                                                              ");
        _sb.AppendLine(" ELSE                                                                                             ");
        _sb.AppendLine(" BEGIN                                                                                            ");
      }

      _sb.AppendLine(" IF NOT EXISTS (SELECT   1                                                                          ");
      _sb.AppendLine("                  FROM   GAMEGATEWAY_BETS                                                           ");
      _sb.AppendLine("                 WHERE   GB_GAME_ID          = @pGameId                                             ");
      _sb.AppendLine("                   AND   GB_GAME_INSTANCE_ID = @pGameInstanceId                                     ");
      if (IsPrize)
      {
        _sb.AppendLine("                 AND   GB_TRANSACTION_TYPE IN (@pTransactionTypeExists, @pTransactionTypePending) ");
      }
      else
      {
        _sb.AppendLine("                 AND   GB_TRANSACTION_TYPE = @pTransactionTypeBet                                 ");
      }
      _sb.AppendLine("                   AND   GB_TRANSACTION_ID   = @pTransactionId                                      ");
      _sb.AppendLine("                   AND   GB_ACCOUNT_ID       = @pAccountId                                          ");
      _sb.AppendLine("                   AND   GB_PARTNER_ID       = @pPartnerId)                                         ");
      _sb.AppendLine(" BEGIN                                                                                              ");
      _sb.AppendLine(" SET @ErrorRegister = 0                                                                             ");
      _sb.AppendLine("                                                                                                    ");
      _sb.AppendLine(" INSERT INTO GAMEGATEWAY_BETS                                                                       ");
      _sb.AppendLine("             (                                                                                      ");
      _sb.AppendLine("               GB_GAME_ID                                                                           ");
      _sb.AppendLine("             , GB_GAME_INSTANCE_ID                                                                  ");
      _sb.AppendLine("             , GB_TRANSACTION_TYPE                                                                  ");
      _sb.AppendLine("             , GB_TRANSACTION_ID                                                                    ");
      _sb.AppendLine("             , GB_ACCOUNT_ID                                                                        ");
      _sb.AppendLine("             , GB_PARTNER_ID                                                                        ");
      _sb.AppendLine("             , GB_EGM_TERMINAL_ID                                                                   ");
      _sb.AppendLine("             , GB_CREATED                                                                           ");
      _sb.AppendLine("             , GB_BETS                                                                              ");
      _sb.AppendLine("             , GB_NUM_BETS                                                                          ");
      _sb.AppendLine("             , GB_TOTAL_BET                                                                         ");
      _sb.AppendLine("             , GB_NUM_PRIZES                                                                        ");
      _sb.AppendLine("             , GB_TOTAL_PRIZE                                                                       ");
      _sb.AppendLine("             , GB_JACKPOT_PRIZE                                                                     ");
      _sb.AppendLine("             , GB_RELATED_PS_ID                                                                     ");
      _sb.AppendLine("             , GB_RELATED_MV_ID                                                                     ");
      _sb.AppendLine("             )                                                                                      ");
      _sb.AppendLine("     VALUES  (                                                                                      ");
      _sb.AppendLine("               @pGameId                                                                             ");
      _sb.AppendLine("             , @pGameInstanceId                                                                     ");
      _sb.AppendLine("             , @pTransactionType                                                                    ");
      _sb.AppendLine("             , @pTransactionId                                                                      ");
      _sb.AppendLine("             , @pAccountId                                                                          ");
      _sb.AppendLine("             , @pPartnerId                                                                          ");
      _sb.AppendLine("             , @pEGMTerminalId                                                                      ");
      _sb.AppendLine("             , @pCreated                                                                            ");
      _sb.AppendLine("             , @pBets                                                                               ");
      _sb.AppendLine("             , @pNumBets                                                                            ");
      _sb.AppendLine("             , @pTotalBet                                                                           ");
      _sb.AppendLine("             , @pNumPrizes                                                                          ");
      _sb.AppendLine("             , @pTotalPrize                                                                         ");
      _sb.AppendLine("             , @pJackpotPrize                                                                       ");
      _sb.AppendLine("             , @pRelatedPsId                                                                        ");
      _sb.AppendLine("             , @pRelatedMvId                                                                        ");
      _sb.AppendLine("             )                                                                                      ");
      _sb.AppendLine(" END                                                                                                ");
      _sb.AppendLine(" ELSE                                                                                               ");
      _sb.AppendLine(" BEGIN                                                                                              ");
      _sb.AppendLine(" SET @ErrorRegister = 1                                                                             ");
      _sb.AppendLine(" END                                                                                                ");

      if (IsRollback)
      {
        _sb.AppendLine(" END                                                                                              ");
      }

      return _sb;
    } // GetInsertBetsStringBuilder

    /// <summary>
    /// Calling function for insert Account Movement (Gamegateway Reserve Credit)
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="TerminalId"></param>
    /// <param name="ResquestAmount"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean InsertAccountMovementGameGateway(Int64 AccountId, String TerminalId, Decimal InitialReservedCredit, Decimal ResquestAmount, SqlTransaction Trx)
    {
      //Account Movements
      CashierSessionInfo _cashier_session_info;
      AccountMovementsTable _account_movements;
      GU_USER_TYPE _user_type;
      TerminalTypes _terminal_type;
      WSI.Common.Terminal.TerminalInfo _terminal_info;
      String _terminal_name;
      String _terminal_external_id;
      Int32 _terminal_id;

      // Initialize Account Movement
      _user_type = GU_USER_TYPE.NOT_ASSIGNED;
      _terminal_name = String.Empty;
      _terminal_external_id = String.Empty;
      _terminal_id = 0;
      _terminal_type = TerminalTypes.UNKNOWN;

      // 1. Trx_GetTerminalInfoByExternalId
      try
      {
        if (!WSI.Common.Terminal.Trx_GetTerminalInfoByExternalId(TerminalId, out _terminal_info, Trx))
        {
          Log.Error(String.Format("InsertAccountMovementGameGateway -->  Trx_GetTerminalInfoByExternalId failed!!! -TerminalId: {0}; AccountId: {1}", TerminalId, AccountId));

          return false;
        }

        _terminal_id = _terminal_info.TerminalId;
        _terminal_name = _terminal_info.Name;
        _terminal_type = _terminal_info.TerminalType;

        // 2.1 Get User Type
        switch (_terminal_type)
        {
          case TerminalTypes.PROMOBOX:
            {
              _user_type = Misc.GetUserTypePromobox();
            }
            break;
          case TerminalTypes.SAS_HOST:
            {
              _user_type = Misc.IsNoteAcceptorEnabled() ? GU_USER_TYPE.SYS_ACCEPTOR : GU_USER_TYPE.SYS_SYSTEM;
            }
            break;
          default:
            {
              Log.Message(String.Format("WCP_MsgGameGatewayGetCredit --> Invalid Trx_GetTerminalInfoByExternalId(Terminal_Id: {0}; TerminalType: {1})", _terminal_id, _terminal_type));

              return false;
            }
        }

        // 2. Call to insert account movement
        _cashier_session_info = Cashier.GetSystemCashierSessionInfo(_user_type, Trx, _terminal_name);
        _account_movements = new AccountMovementsTable(_cashier_session_info, _user_type == GU_USER_TYPE.SYS_ACCEPTOR ? _terminal_id : 0, _terminal_name);

        if (!WSI.Common.GameGateway.InsertAccountMovementReserveCredit(_cashier_session_info,
                                                                      _account_movements,
                                                                      AccountId,
                                                                      ResquestAmount == -1 ? 0 : ResquestAmount,
                                                                      InitialReservedCredit,
                                                                      Trx))
        {
          Log.Message(String.Format("WCP_MsgGameGatewayGetCredit --> Invalid InsertAccountMovementReserveCredit (Terminal_Id: {0}; TerminalType: {1})", _terminal_id, _terminal_type));

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Inserts Account Movement when are reserving credit from GameGateway
    /// </summary>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="AccountId"></param>
    /// <param name="Amount"></param>
    /// <param name="InitialReservedCredit"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean InsertAccountMovementReserveCredit(CashierSessionInfo CashierSessionInfo,
                                                             AccountMovementsTable AccountMovements,
                                                             Int64 AccountId,
                                                             Decimal Amount,
                                                             Decimal InitialReservedCredit,
                                                             SqlTransaction Trx)
    {
      Int64 OperationId;
      Decimal _final_reserved_credit;
      Decimal _add_amount;
      Decimal _sub_amount;

      _sub_amount = 0;
      _add_amount = 0;

      try
      {
        //Insert Operation Id
        if (!Operations.DB_InsertOperation(OperationCode.GAMEGATEWAY_RESERVE_CREDIT, AccountId,
                                           CashierSessionInfo.CashierSessionId, 0, 0, 0, 0, 0, 0, string.Empty, out OperationId, Trx))
        {
          Log.Error("GameGateWay.cs --> InsertAccountMovementReserveCredit() --> Error Inserting Operation Id");

          return false;
        }

        //_account_movement = new AccountMovementsTable(CashierSessionInfo);

        _final_reserved_credit = Amount;

        if (_final_reserved_credit - InitialReservedCredit >= 0)
        {
          _add_amount = _final_reserved_credit - InitialReservedCredit;
        }
        else
        {
          _sub_amount = InitialReservedCredit - _final_reserved_credit;
        }

        //Insert Account Movement
        if (!AccountMovements.Add(OperationId, AccountId, MovementType.GameGatewayReserveCredit, InitialReservedCredit, _sub_amount, _add_amount, _final_reserved_credit))
        {
          Log.Error("GameGateWay.cs --> InsertAccountMovementReserveCredit() --> Error Inserting Account Movement");

          return false;
        }

        //Save Account Movement
        if (!AccountMovements.Save(Trx))
        {
          Log.Error("GameGateWay.cs --> InsertAccountMovementReserveCredit() --> Error Saving Account Movement");

          return false;
        }

        return true;
      }

      catch (Exception _ex)
      {
        Log.Error("GameGateWay.cs --> InsertAccountMovementReserveCredit() --> Error Generic");
        Log.Error(_ex.Message);
      }

      return false;

    } // InsertAccountMovementReserveCredit

    /// <summary>
    /// Get provider general param group key 
    /// </summary>
    /// <returns></returns>
    public static String GetProviderGPGroupKey(PROVIDER_TYPE ProviderType)
    {
      switch (ProviderType)
      {
        case PROVIDER_TYPE.PariPlay:
          {
            return GP_PROVIDER_TYPE_PARIPLAY;
          }

        case PROVIDER_TYPE.PimPamGO:
        default:
          {
            return GP_PROVIDER_TYPE_GAMEGATEWAY;
          }
      }
    } // GetProviderGPGroupKey


    #region " Manage Gateway Log Methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Manage Log mode
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Request
    //          - Response
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static void ManageLog(String Request, String Response, PROVIDER_TYPE ProviderType)
    {
      try
      {
        switch ((GameGateway.LOG_MODE)GeneralParam.GetInt32(GetProviderGPGroupKey(ProviderType), "Log.Mode", 0))
        {
          case GameGateway.LOG_MODE.WCP:
          case GameGateway.LOG_MODE.Extended:
            Log.Message(String.Format("Request: {0}", Request));
            Log.Message(String.Format("Response: {0}", Response));
            break;

          case GameGateway.LOG_MODE.BD:
            InsertGameGatewayLog(Request, Response);
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // ManageLog


    //------------------------------------------------------------------------------
    // PURPOSE: Insert in table gameGateway_log
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Request
    //        - Response
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static void InsertGameGatewayLog(String Request, String Response)
    {
      StringBuilder _sb;

      try
      {
        // Remove Tag <?xml version="1.0" encoding="utf-8"?>
        if (Request.Contains("?>"))
        {
          Request = Request.Substring(Request.IndexOf("?>") + 2);
        }
        if (Response.Contains("?>"))
        {
          Response = Response.Substring(Response.IndexOf("?>") + 2);
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO GAMEGATEWAY_LOG ");
        _sb.AppendLine("             (               ");
        _sb.AppendLine("               GL_REQUEST    ");
        _sb.AppendLine("             , GL_RESPONSE   ");
        _sb.AppendLine("             )               ");
        _sb.AppendLine("     VALUES  (               ");
        _sb.AppendLine("               @pRequest     ");
        _sb.AppendLine("             , @pResponse    ");
        _sb.AppendLine("             )               ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pRequest", SqlDbType.Xml).Value = Request;
            _sql_cmd.Parameters.Add("@pResponse", SqlDbType.Xml).Value = Response;

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Warning("Error InsertGameGatewayLog");

              return;
            }
          }

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    } // InsertGameGatewayLog

    #endregion " Manage Gateway Log Methods "

    #endregion " Public Methods "

    #region " Private Methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Get data from table gameGateway_bulk
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TransactionId
    //          - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DataTable
    //   NOTES:
    //
    private static DataTable GetGameGatewayBulk(String TransactionId, Int64 AccountId)
    {
      StringBuilder _sb;
      DataTable _table;

      _table = new DataTable();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   TOP 50                                                                     ");
        _sb.AppendLine("            GB_ACCOUNT_ID                                                              ");
        _sb.AppendLine("          , GB_TOTAL_PRIZE                                                             ");
        _sb.AppendLine("          , GB_JACKPOT_PRIZE                                                           ");
        _sb.AppendLine("          , GB_NUM_PRIZES                                                              ");
        _sb.AppendLine("          , GB_TOTAL_BET                                                               ");
        _sb.AppendLine("          , GB_NUM_BETS                                                                ");
        _sb.AppendLine("          , GB_GAME_ID                                                                 ");
        _sb.AppendLine("          , GB_GAME_INSTANCE_ID                                                        ");
        _sb.AppendLine("          , GB_PARTNER_ID                                                              ");
        _sb.AppendLine("          , GB_TRANSACTION_ID                                                          ");
        _sb.AppendLine("          , GB_TRANSACTION_TYPE                                                        ");
        _sb.AppendLine("          , GB_CREATED                                                                 ");
        _sb.AppendLine("     FROM   GAMEGATEWAY_BETS                                                           ");

        // BulkThread && ProcessRequest
        if (AccountId == -1 && !GeneralParam.GetBoolean("GameGateway", "AwardPrizes", true))
        {
          _sb.AppendLine("  WHERE   GB_TRANSACTION_TYPE = @pTransactionTypeRollback                            ");
        }
        else
        {
          _sb.AppendLine("  WHERE   GB_TRANSACTION_TYPE IN (@pTransactionTypePrize, @pTransactionTypeRollback) ");
        }

        // By TransactionId
        if (!String.IsNullOrEmpty(TransactionId))
        {
          _sb.AppendLine("    AND   GB_TRANSACTION_ID   = @pTransactionId                                      ");
        }

        // By AccountId
        if (AccountId > -1)
        {
          _sb.AppendLine("    AND   GB_ACCOUNT_ID       = @pAccountId                                          ");
        }

        _sb.AppendLine(" ORDER BY   GB_CREATED ASC                                                             ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (AccountId > -1 || GeneralParam.GetBoolean("GameGateway", "AwardPrizes", true))
            {
              _sql_cmd.Parameters.Add("@pTransactionTypePrize", SqlDbType.Int).Value = TRANSACTION_TYPE.PRIZE_PENDING;
            }

            _sql_cmd.Parameters.Add("@pTransactionTypeRollback", SqlDbType.Int).Value = TRANSACTION_TYPE.BET_ROLLBACK_PENDING;

            if (!String.IsNullOrEmpty(TransactionId))
            {
              _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.NVarChar, 40).Value = TransactionId;
            }

            if (AccountId > -1)
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            }

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_table);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _table;
    } // GetGameGatewayBulk



    //------------------------------------------------------------------------------
    // PURPOSE: Check if the account is in a play session
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - True or False 
    //
    //   NOTES:
    //
    private static Boolean IsInPlaySessionInternal(Int64 AccountId, out DataTable WCPTerminals)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      WCPTerminals = new DataTable();

      try
      {
        _sb.AppendLine(" SELECT   AC_CURRENT_TERMINAL_ID                 ");
        _sb.AppendLine("   FROM   ACCOUNTS                               ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId            ");
        _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID IS NOT NULL ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(WCPTerminals);

              if (WCPTerminals.Rows.Count == 1)
              {
                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // IsInPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE: Process GameGateway Bulk
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Credits
    //          - SendMessage
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //   NOTES:
    //
    private static Boolean ProcessGameGatewayBulk(DataTable Credits, Boolean SendMessage)
    {
      LCD_Session _session;
      Decimal _add_amount;
      Decimal _sub_amount;

      try
      {
        _session = new LCD_Session();

        foreach (DataRow _credit in Credits.Rows)
        {
          _session = new LCD_Session((Int64)_credit[GB_USER_ID]);

          if (_session.ErrorCode != RESPONSE_ERROR_CODE.OK)
          {
            return false;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            GetDataAccount(ref _session, false, _db_trx.SqlTransaction);

            if (_session.ErrorCode != RESPONSE_ERROR_CODE.OK)
            {
              return false;
            }

            _session.TotalBets = (Int32)_credit[GB_NUM_CREDITS] > 0 ? (Int32)_credit[GB_NUM_CREDITS] : (Int32)_credit[GB_NUM_BETS];
            _add_amount = (Decimal)_credit[GB_TOTAL_PRIZE];
            _sub_amount = (Decimal)_credit[GB_TOTAL_BET];
            _session.TotalJackpot = (Decimal)_credit[GB_JACKPOT];
            _session.GameId = (Int64)_credit[GB_GAME_ID];
            _session.GameInstanceId = (Int64)_credit[GB_GAME_INSTANCE_ID];
            _session.PartnerId = (Int32)_credit[GB_PARTNER_ID];
            _session.TransactionId = (String)_credit[GB_TRANSACTION_ID];
            _session.TransactionType = (TRANSACTION_TYPE)_credit[GB_TRANSACTION_TYPE];
            _session.Finished = (DateTime)_credit[GB_CREATED]; //When send Bulk the game was finished

            ProcessCreditDebit(_add_amount, _sub_amount, ref _session, _db_trx.SqlTransaction);

            if (_session.ErrorCode != RESPONSE_ERROR_CODE.OK)
            {
              return false;
            }

            if (!UpdateGameGatewayBulk(_session, _db_trx.SqlTransaction))
            {
              return false;
            }

            _db_trx.Commit();
          }
        }

        if (SendMessage && IsInPlaySession(ref _session))
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!InsertGamegatewayCommandMessage(ref _session, WCP_CommandCode.GameGateway, TYPE_WCP.PRIZE, _db_trx.SqlTransaction))
            {
              Log.Message(String.Format("Error ProcessGameGatewayBulk.InsertGamegatewayCommandMessage. AccountId: {0}", _session.AccountId));
            }
            else
            {
              _session.WcpParameter = String.Format("<type>{0}</type><cmd_id>{1}</cmd_id><prize>0</prize><logo></logo><accountid>{2}</accountid><partnerid>{3}</partnerid>", (Int16)TYPE_WCP.PRIZE, _session.WCPId, _session.AccountId, _session.PartnerId.ToString("000"));

              if (!UpdateGamegatewayCommandMessage(_session, _db_trx.SqlTransaction))
              {
                Log.Message(String.Format("Error ProcessGameGatewayBulk.UpdateGamegatewayCommandMessage. AccountId: {0}", _session.AccountId));
              }
              else
              {
                _db_trx.Commit();
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ProcessGameGatewayBulk

    //------------------------------------------------------------------------------
    // PURPOSE: Update execution table gameGateway_bulk
    // 
    //  PARAMS:
    //      - INPUT:
    //          - LCD_Session
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //   NOTES:
    //
    private static Boolean UpdateGameGatewayBulk(LCD_Session Session, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GAMEGATEWAY_BETS                           ");
        _sb.AppendLine("    SET   GB_TRANSACTION_TYPE = @pTransactionType    ");
        _sb.AppendLine("        , GB_RELATED_PS_ID    = @pRelatedPsId        ");
        _sb.AppendLine("        , GB_RELATED_MV_ID    = @pRelatedMvId        ");
        _sb.AppendLine("  WHERE   GB_GAME_ID          = @pGameId             ");
        _sb.AppendLine("    AND   GB_GAME_INSTANCE_ID = @pGameInstanceId     ");
        _sb.AppendLine("    AND   GB_TRANSACTION_TYPE = @pTransactionTypeOld ");
        _sb.AppendLine("    AND   GB_TRANSACTION_ID   = @pTransactionId      ");
        _sb.AppendLine("    AND   GB_ACCOUNT_ID       = @pAccountId          ");
        _sb.AppendLine("    AND   GB_PARTNER_ID       = @pPartnerId          ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTransactionType", SqlDbType.Int).Value = (Session.TransactionType == TRANSACTION_TYPE.PRIZE_PENDING) ? TRANSACTION_TYPE.PRIZE : TRANSACTION_TYPE.BET_ROLLBACK;
          _sql_cmd.Parameters.Add("@pRelatedPsId", SqlDbType.BigInt).Value = Session.PlaySessionId;
          _sql_cmd.Parameters.Add("@pRelatedMvId", SqlDbType.BigInt).Value = Session.MovementId;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = Session.GameId;
          _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).Value = Session.GameInstanceId;
          _sql_cmd.Parameters.Add("@pTransactionTypeOld", SqlDbType.Int).Value = Session.TransactionType;
          _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.NVarChar, 40).Value = Session.TransactionId;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Session.AccountId;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = Session.PartnerId;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }

          Log.Warning("Error UpdateGameGatewayBulk");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateGameGatewayBulk

    //------------------------------------------------------------------------------
    // PURPOSE: Update reserved account
    // 
    //  PARAMS:
    //      - INPUT:
    //          - LCD_Session
    //          - FinalBalance
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False 
    //
    //   NOTES:
    //
    private static Boolean UpdateReservedAccount(ref LCD_Session Session, MultiPromos.AccountBalance FinalBalance, SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameterCollection _params;
      Decimal _reserved_amount_output;
      Decimal _amount_to_decrease;


      try
      {
        _amount_to_decrease = Session.TotalAmount;


        //Calculate new values to decrease the reserved balance. Note:  UpdateBalance "reserved protection"
        if (Session.TransactionType == GameGateway.TRANSACTION_TYPE.BET)
        {

          _amount_to_decrease = Math.Max(0, (Session.TotalAmount * -1) - (Session.Balance.Reserved - FinalBalance.Reserved));

          if (_amount_to_decrease == 0)
          {
            Session.Balance.Reserved = FinalBalance.Reserved;

            return true;
          }

          _amount_to_decrease = _amount_to_decrease * -1;
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   ACCOUNTS                                                        ");
        _sb.AppendLine("    SET   AC_RE_RESERVED = dbo.Maximum_Money(AC_RE_RESERVED + @pAmount,0) ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID  = @pAccountId                                    ");

        _sb.AppendLine(" SELECT   @pReserved =  AC_RE_RESERVED                                    ");
        _sb.AppendLine("   FROM   ACCOUNTS                                                        ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID  = @pAccountId                                    ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Session.TransactionType == (GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING) ? _amount_to_decrease * -1 : _amount_to_decrease;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Session.AccountId;

          _sql_cmd.Parameters.Add("@pReserved", SqlDbType.Money).Direction = ParameterDirection.Output;
          _params = _sql_cmd.Parameters;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            _reserved_amount_output = (Decimal)_params["@pReserved"].Value;
            Session.Balance.Reserved = _reserved_amount_output;

            return true;
          }

          Log.Warning("Error UpdateReservedAccount");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateReservedAccount

    //------------------------------------------------------------------------------
    // PURPOSE: Insert play session for GameGateway
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LCD_Session
    //        - FinalBalance
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //        - PlaySessionId
    //        - TerminalId
    //        - GameName
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    private static Boolean InsertGameGatewayPlaySession(LCD_Session Session, MultiPromos.AccountBalance FinalBalance, out Int64 PlaySessionId, out Int32 TerminalId, out String TerminalName, SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameterCollection _params;
      PlaySessionStatus _ps_status;
      Decimal _bet_amount;
      Currency _nr_won_amount;
      Currency _re_won_amount;
      Int32 _won_count;
      Int32 _played_count;
      Boolean _insert_PlaySessionToPending;

      PlaySessionId = -1;
      TerminalId = 0;
      TerminalName = String.Empty;
      _insert_PlaySessionToPending = false;

      try
      {
        switch (Session.TransactionType)
        {
          case GameGateway.TRANSACTION_TYPE.BET:
            _ps_status = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? PlaySessionStatus.GameGatewayBet
                                                                          : PlaySessionStatus.PariPlayBet;
            _bet_amount = Session.TotalAmount * -1;
            _won_count = 0;
            _nr_won_amount = 0;
            _re_won_amount = 0;
            _played_count = Session.TotalBets;
            _insert_PlaySessionToPending = true;

            break;

          case GameGateway.TRANSACTION_TYPE.PRIZE:
          case GameGateway.TRANSACTION_TYPE.PRIZE_PENDING:
            _ps_status = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? PlaySessionStatus.GameGatewayPrize
                                                                          : PlaySessionStatus.PariPlayPrize;
            _bet_amount = 0;
            _won_count = Session.TotalBets;
            _nr_won_amount = 0;
            _re_won_amount = Session.TotalAmount;
            _played_count = 0;

            break;

          case GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING:
            _ps_status = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? PlaySessionStatus.GameGatewayBetRollback
                                                                          : PlaySessionStatus.PariPlayBetRollback;

            _bet_amount = Session.TotalAmount;
            _won_count = 0;
            _nr_won_amount = 0;
            _re_won_amount = 0;
            _played_count = Session.TotalBets;
            _insert_PlaySessionToPending = true;

            break;

          default:
            _ps_status = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? PlaySessionStatus.GameGatewayPrize
                                                                          : PlaySessionStatus.PariPlayPrize;
            _bet_amount = 0;
            _won_count = Session.TotalBets;
            _nr_won_amount = 0;
            _re_won_amount = Session.TotalAmount;
            _played_count = 0;

            break;
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   TOP 1                                 ");
        _sb.AppendLine("          @pTerminalId = TE_TERMINAL_ID         ");
        _sb.AppendLine("        , @pTerminalName = TE_BASE_NAME         ");
        _sb.AppendLine("   FROM   TERMINALS                             ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_TYPE = @pTerminalType     ");
        _sb.AppendLine("                                                ");
        _sb.AppendLine(" INSERT INTO   PLAY_SESSIONS                    ");
        _sb.AppendLine("             (                                  ");
        _sb.AppendLine("               PS_ACCOUNT_ID                    ");
        _sb.AppendLine("             , PS_TERMINAL_ID                   ");
        _sb.AppendLine("             , PS_TYPE                          ");
        _sb.AppendLine("             , PS_INITIAL_BALANCE               ");
        _sb.AppendLine("             , PS_REDEEMABLE_CASH_IN            ");
        _sb.AppendLine("             , PS_FINAL_BALANCE                 ");
        _sb.AppendLine("             , PS_PLAYED_COUNT                  ");
        _sb.AppendLine("             , PS_PLAYED_AMOUNT                 ");
        _sb.AppendLine("             , PS_WON_COUNT                     ");
        _sb.AppendLine("             , PS_WON_AMOUNT                    ");
        _sb.AppendLine("             , PS_CASH_IN                       ");
        _sb.AppendLine("             , PS_CASH_OUT                      ");
        _sb.AppendLine("             , PS_REDEEMABLE_WON                ");
        _sb.AppendLine("             , PS_REDEEMABLE_CASH_OUT           ");
        _sb.AppendLine("             , PS_NON_REDEEMABLE_WON            ");
        _sb.AppendLine("             , PS_NON_REDEEMABLE_CASH_OUT       ");
        _sb.AppendLine("             , PS_STATUS                        ");
        _sb.AppendLine("             , PS_STARTED                       ");
        _sb.AppendLine("             , PS_FINISHED                      ");
        _sb.AppendLine("             , PS_STAND_ALONE                   ");
        _sb.AppendLine("             , PS_PROMO                         ");
        _sb.AppendLine("             , PS_REDEEMABLE_PLAYED             ");
        _sb.AppendLine("             )                                  ");
        _sb.AppendLine("      VALUES                                    ");
        _sb.AppendLine("             (                                  ");
        _sb.AppendLine("               @pAccountId                      "); // PS_ACCOUNT_ID
        _sb.AppendLine("             , @pTerminalId                     "); // PS_TERMINAL_ID
        _sb.AppendLine("             , @pType                           "); // PS_TYPE                         
        _sb.AppendLine("             , @pInitialBalance                 "); // PS_INITIAL_BALANCE
        _sb.AppendLine("             , @pInitialBalance                 "); // PS_REDEEMABLE_CASH_IN                         
        _sb.AppendLine("             , @pFinalBalance                   "); // PS_FINAL_BALANCE
        _sb.AppendLine("             , @pPlayedCount                    "); // PS_PLAYED_COUNT
        _sb.AppendLine("             , @pBetAmount                      "); // PS_PLAYED_AMOUNT
        _sb.AppendLine("             , @pWonCount                       "); // PS_WON_COUNT
        _sb.AppendLine("             , (@pReWonAmount + @pNrWonAmount)  "); // PS_WON_AMOUNT
        _sb.AppendLine("             , 0                                "); // PS_CASH_IN
        _sb.AppendLine("             , 0                                "); // PS_CASH_OUT                         
        _sb.AppendLine("             , @pReWonAmount                    "); // PS_REDEEMABLE_WON
        _sb.AppendLine("             , @pInitialBalance + @pReWonAmount "); // PS_REDEEMABLE_CASH_OUT
        _sb.AppendLine("             , @pNrWonAmount                    "); // PS_NON_REDEEMABLE_WON
        _sb.AppendLine("             , @pNrWonAmount                    "); // PS_NON_REDEEMABLE_CASH_OUT                                                  
        _sb.AppendLine("             , @pStatus                         "); // PS_STATUS
        _sb.AppendLine("             , GETDATE()                        "); // PS_STARTED
        _sb.AppendLine("             , DATEADD(SECOND, 1, GETDATE())    "); // PS_FINISHED
        _sb.AppendLine("             , 0                                "); // PS_STAND_ALONE
        _sb.AppendLine("             , 0                                "); // PS_PROMO
        _sb.AppendLine("             , @pRePlayed                       "); // PS_REDEEMABLE_PLAYED
        _sb.AppendLine("             )                                  ");
        _sb.AppendLine(" SET @pPlaySessionId = SCOPE_IDENTITY()         ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Session.AccountId;
          _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? (Int32)TerminalTypes.GAMEGATEWAY : (Int32)TerminalTypes.PARIPLAY;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Session.ProviderType == PROVIDER_TYPE.PimPamGO) ? PlaySessionType.GAMEGATEWAY : PlaySessionType.PARIPLAY;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = _ps_status;
          _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = Session.Balance.TotalBalance;
          _cmd.Parameters.Add("@pBetAmount", SqlDbType.Money).Value = _bet_amount;
          _cmd.Parameters.Add("@pRePlayed", SqlDbType.Money).Value = _bet_amount;
          _cmd.Parameters.Add("@pWonCount", SqlDbType.Int).Value = _won_count;
          _cmd.Parameters.Add("@pReWonAmount", SqlDbType.Money).Value = _re_won_amount.SqlMoney;
          _cmd.Parameters.Add("@pNrWonAmount", SqlDbType.Money).Value = _nr_won_amount.SqlMoney;
          _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = FinalBalance.TotalBalance;
          _cmd.Parameters.Add("@pPlayedCount", SqlDbType.Int).Value = _played_count;

          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Direction = ParameterDirection.Output;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Direction = ParameterDirection.Output;
          _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 40).Direction = ParameterDirection.Output;

          _params = _cmd.Parameters;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          PlaySessionId = (Int64)_params["@pPlaySessionId"].Value;
          TerminalId = (Int32)_params["@pTerminalId"].Value;
          TerminalName = (String)_params["@pTerminalName"].Value;
        }

        MultiPromos.PlaySessionCloseParameters _ps_close_parameters;
        MultiPromos.EndSessionOutput _session_output;
        TerminalTypes _terminal_type;
        TimeSpan _timespan;

        _ps_close_parameters = new MultiPromos.PlaySessionCloseParameters();
        _timespan = new TimeSpan(0, 1, 0);
        _session_output = new MultiPromos.EndSessionOutput();
        _terminal_type = TerminalTypes.GAMEGATEWAY;

        _ps_close_parameters.PlaySessionId = PlaySessionId;
        _ps_close_parameters.AccountId = Session.AccountId;
        _ps_close_parameters.TerminalId = TerminalId;
        _ps_close_parameters.Played = new MultiPromos.AccountBalance(_bet_amount, 0, 0);
        _ps_close_parameters.NumPlayed = _played_count;
        _ps_close_parameters.BalanceMismatch = false;


        //JCA 2016-06-08 Create register to calculate points/buckets
        if (_insert_PlaySessionToPending && !MultiPromos.Trx_PlaySessionToPending(_timespan, _ps_close_parameters, _session_output, _terminal_type, Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertGameGatewayPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE: Get Epoch time from DateTime
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateTime
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    //
    //   NOTES:
    //
    private static String ToEpochTime(DateTime Date)
    {
      DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

      return Convert.ToInt64((Date - epoch).TotalMilliseconds).ToString();
    } // ToEpochTime

    //------------------------------------------------------------------------------
    // PURPOSE: Get DateTime from Epoch time
    // 
    //  PARAMS:
    //      - INPUT:
    //        - EpochTime
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DateTime
    //
    //   NOTES:
    //
    private static DateTime FromEpochTime(Int64 EpochTime)
    {
      DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

      return epoch.AddMilliseconds(EpochTime);
    } // FromEpochTime

    //------------------------------------------------------------------------------
    // PURPOSE: Insert games for GameGateway
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LCD_Session
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //        - GMGameId
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    private static Boolean InsertGameGatewayGames(LCD_Session Session, out Int32 GMGameId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _param;

      GMGameId = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" IF NOT EXISTS (SELECT   1                     ");
        _sb.AppendLine("                  FROM   GAMES                 ");
        _sb.AppendLine("                 WHERE   GM_NAME = @pGameName) ");
        _sb.AppendLine(" BEGIN                                         ");
        _sb.AppendLine(" INSERT INTO GAMES                             ");
        _sb.AppendLine("             (                                 ");
        _sb.AppendLine("               GM_NAME                         ");
        _sb.AppendLine("             )                                 ");
        _sb.AppendLine("     VALUES  (                                 ");
        _sb.AppendLine("               @pGameName                      ");
        _sb.AppendLine("             )                                 ");
        _sb.AppendLine(" END                                           ");
        _sb.AppendLine("                                               ");
        _sb.AppendLine(" SELECT   @pGameId = GM_GAME_ID                ");
        _sb.AppendLine("   FROM   GAMES                                ");
        _sb.AppendLine("  WHERE   GM_NAME = @pGameName                 ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar, 50).Value = Session.GameName;

          _param = _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int);
          _param.Direction = ParameterDirection.Output;

          _sql_cmd.ExecuteNonQuery();

          GMGameId = (Int32)_param.Value;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertGameGatewayGames

    //------------------------------------------------------------------------------
    // PURPOSE: Manage game meters for GameGateway
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LCD_Session
    //        - TerminalId
    //        - GameName
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    private static Boolean ManageGameGatewayGameMeters(LCD_Session Session, SqlTransaction Trx)
    {
      Int32 _gm_game_id;

      if (!InsertGameGatewayGames(Session, out _gm_game_id, Trx))
      {
        return false;
      }

      if (!InsertGameGatewayTerminalGameTranslation(Session, _gm_game_id, Trx))
      {
        return false;
      }

      if (!UpdateGameGatewayGameMeters(Session, Trx))
      {
        return false;
      }

      return true;
    } // ManageGameGatewayGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE: Insert bets for GameGateway
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LCD_Session
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //        - Boolean
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    private static Boolean InsertGameGatewayBets(LCD_Session Session, SqlTransaction Trx, out Boolean Duplicated)
    {
      StringBuilder _sb;
      SqlParameter _param;

      Duplicated = false;

      try
      {
        _sb = GetInsertBetsStringBuilder(Session.TotalAmount > 0, false);

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          if (Session.TotalAmount > 0)
          {
            _sql_cmd.Parameters.Add("@pTransactionTypeExists", SqlDbType.Int).Value = GameGateway.TRANSACTION_TYPE.PRIZE;
            _sql_cmd.Parameters.Add("@pTransactionTypePending", SqlDbType.Int).Value = GameGateway.TRANSACTION_TYPE.PRIZE_PENDING;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pTransactionTypeBet", SqlDbType.Int).Value = GameGateway.TRANSACTION_TYPE.BET;
          }
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = Session.GameId;
          _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).Value = Session.GameInstanceId;
          _sql_cmd.Parameters.Add("@pTransactionType", SqlDbType.Int).Value = Session.TransactionType;
          _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.NVarChar, 40).Value = Session.TransactionId;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Session.AccountId;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = Session.PartnerId;
          if (Session.TerminalId == 0)
          {
            _sql_cmd.Parameters.Add("@pEGMTerminalId", SqlDbType.Int).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pEGMTerminalId", SqlDbType.Int).Value = Session.TerminalId;

          }
          if (String.IsNullOrEmpty(Session.Bets))
          {
            _sql_cmd.Parameters.Add("@pBets", SqlDbType.Xml).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pBets", SqlDbType.Xml).Value = Session.Bets;
          }
          _sql_cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = WGDB.Now;
          _sql_cmd.Parameters.Add("@pNumBets", SqlDbType.Int).Value = (Session.TotalAmount > 0) ? 0 : Session.TotalBets;
          _sql_cmd.Parameters.Add("@pTotalBet", SqlDbType.Money).Value = (Session.TotalAmount > 0) ? 0 : Session.TotalAmount * -1;
          _sql_cmd.Parameters.Add("@pNumPrizes", SqlDbType.Int).Value = (Session.TotalAmount > 0) ? Session.TotalBets : 0;
          _sql_cmd.Parameters.Add("@pTotalPrize", SqlDbType.Money).Value = (Session.TotalAmount > 0) ? Session.TotalAmount : 0;
          _sql_cmd.Parameters.Add("@pJackpotPrize", SqlDbType.Money).Value = Session.TotalJackpot;
          _sql_cmd.Parameters.Add("@pRelatedPsId", SqlDbType.BigInt).Value = Session.PlaySessionId;
          _sql_cmd.Parameters.Add("@pRelatedMvId", SqlDbType.BigInt).Value = Session.MovementId;

          _param = _sql_cmd.Parameters.Add("@ErrorRegister", SqlDbType.Bit);
          _param.Direction = ParameterDirection.Output;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
          else
          {
            Duplicated = (Boolean)_param.Value;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertGameGatewayBets

    //------------------------------------------------------------------------------
    // PURPOSE: Update execution tables gameGateway_games and GameGateway_game_instances
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Session
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //   NOTES:
    //
    private static Boolean UpdateGameGatewayGames(LCD_Session Session, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Decimal _played;
      Decimal _won;
      Decimal _pending;
      Int32 _num_bets;
      Int32 _num_prizes;
      Int32 _num_pending;

      try
      {
        switch (Session.TransactionType)
        {
          case GameGateway.TRANSACTION_TYPE.BET:
            _played = Session.TotalAmount * -1;
            _won = 0;
            _pending = 0;
            _num_bets = Session.TotalBets;
            _num_prizes = 0;
            _num_pending = 0;

            break;

          case GameGateway.TRANSACTION_TYPE.PRIZE:
            _played = 0;
            _won = Session.TotalAmount;
            _pending = 0;
            _num_bets = 0;
            _num_prizes = Session.TotalBets;
            _num_pending = 0;

            break;

          // Only GameGateway
          case GameGateway.TRANSACTION_TYPE.PRIZE_PENDING:
            _played = 0;
            _won = Session.TotalAmount;
            _num_bets = 0;
            _num_prizes = Session.TotalBets;

            if (!GeneralParam.GetBoolean("GameGateway", "AwardPrizes", true))
            {
              _pending = Session.TotalAmount * -1;
              _num_pending = Session.TotalBets * -1;
            }
            else
            {
              _pending = 0;
              _num_pending = 0;
            }

            break;


          case GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING:
            _played = Session.TotalAmount;
            _won = 0;
            _pending = 0;
            _num_bets = Session.TotalBets;
            _num_prizes = 0;
            _num_pending = 0;

            break;

          default:
            _played = 0;
            _won = Session.TotalAmount;
            _pending = 0;
            _num_bets = 0;
            _num_prizes = Session.TotalBets;
            _num_pending = 0;

            break;
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GAMEGATEWAY_GAME_INSTANCES                                            ");
        _sb.AppendLine("    SET   GGI_PLAYED           = GGI_PLAYED      + @pPlayed                     ");
        _sb.AppendLine("        , GGI_WON              = GGI_WON         + @pWon                        ");
        _sb.AppendLine("        , GGI_WON_JACKPOT      = GGI_WON_JACKPOT + @pWonJackpot                 ");
        _sb.AppendLine("        , GGI_NUM_BETS         = GGI_NUM_BETS    + @pNumBets                    ");
        _sb.AppendLine("        , GGI_NUM_PRIZES       = GGI_NUM_PRIZES  + @pNumPrizes                  ");
        _sb.AppendLine("        , GGI_PENDING          = GGI_PENDING     + @pPending                    ");
        _sb.AppendLine("        , GGI_NUM_PENDING      = GGI_NUM_PENDING + @pNumPending                 ");
        if (Session.Finished != null)
        {
          _sb.AppendLine("        , GGI_FINISHED       = dbo.Maximum_Datetime(@pFinished, GGI_FINISHED) ");
        }
        if (Session.TransactionType == TRANSACTION_TYPE.BET_ROLLBACK_PENDING)
        {
          //JCALERO el parámetro @pNumBets viene negativo con lo que  --  es  +
          _sb.AppendLine("        , GGI_NUM_BETS_ROLLBACK  = GGI_NUM_BETS_ROLLBACK - @pNumBets          ");
        }

        _sb.AppendLine("  WHERE   GGI_GAME_INSTANCE_ID = @pGameInstanceId                               ");
        _sb.AppendLine("    AND   GGI_PARTNER_ID       = @pPartnerId                                    ");
        _sb.AppendLine("                                                                                ");
        _sb.AppendLine(" UPDATE   GAMEGATEWAY_GAMES                                                     ");
        _sb.AppendLine("    SET   GG_PLAYED       = GG_PLAYED      + @pPlayed                           ");
        _sb.AppendLine("        , GG_WON          = GG_WON         + @pWon                              ");
        _sb.AppendLine("        , GG_WON_JACKPOT  = GG_WON_JACKPOT + @pWonJackpot                       ");
        _sb.AppendLine("        , GG_NUM_BETS     = GG_NUM_BETS    + @pNumBets                          ");
        _sb.AppendLine("        , GG_NUM_PRIZES   = GG_NUM_PRIZES  + @pNumPrizes                        ");
        _sb.AppendLine("        , GG_PENDING      = GG_PENDING     + @pPending                          ");
        _sb.AppendLine("        , GG_NUM_PENDING  = GG_NUM_PENDING + @pNumPending                       ");
        _sb.AppendLine("  WHERE   GG_GAME_ID      = @pGameId                                            ");
        _sb.AppendLine("    AND   GG_PARTNER_ID   = @pPartnerId                                         ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pPlayed", SqlDbType.Money).Value = _played;
          _sql_cmd.Parameters.Add("@pWon", SqlDbType.Money).Value = _won;
          _sql_cmd.Parameters.Add("@pWonJackpot", SqlDbType.Money).Value = Session.TotalJackpot;
          _sql_cmd.Parameters.Add("@pNumBets", SqlDbType.Int).Value = _num_bets;
          _sql_cmd.Parameters.Add("@pNumPrizes", SqlDbType.Int).Value = _num_prizes;
          _sql_cmd.Parameters.Add("@pPending", SqlDbType.Money).Value = _pending;
          _sql_cmd.Parameters.Add("@pNumPending", SqlDbType.Int).Value = _num_pending;
          _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).Value = Session.GameInstanceId;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = Session.GameId;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = Session.PartnerId;
          if (Session.Finished != null)
          {
            _sql_cmd.Parameters.Add("@pFinished", SqlDbType.DateTime).Value = Session.Finished;
          }

          if (_sql_cmd.ExecuteNonQuery() == 2)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateGameGatewayGames

    //------------------------------------------------------------------------------
    // PURPOSE: Insert terminal game translation for GameGateway
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LCD_Session
    //        - GMGameId
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    private static Boolean InsertGameGatewayTerminalGameTranslation(LCD_Session Session, Int32 GMGameId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" IF NOT EXISTS (SELECT   1                                 ");
        _sb.AppendLine("                  FROM   TERMINAL_GAME_TRANSLATION         ");
        _sb.AppendLine("                 WHERE   TGT_TERMINAL_ID    = @pTerminalId ");
        _sb.AppendLine("                   AND   TGT_SOURCE_GAME_ID = @pGameId)    ");
        _sb.AppendLine(" BEGIN                                                     ");
        _sb.AppendLine(" INSERT INTO TERMINAL_GAME_TRANSLATION                     ");
        _sb.AppendLine("             (                                             ");
        _sb.AppendLine("               TGT_TERMINAL_ID                             ");
        _sb.AppendLine("             , TGT_SOURCE_GAME_ID                          ");
        _sb.AppendLine("             , TGT_TARGET_GAME_ID                          ");
        _sb.AppendLine("             )                                             ");
        _sb.AppendLine("     VALUES  (                                             ");
        _sb.AppendLine("               @pTerminalId                                ");
        _sb.AppendLine("             , @pGameId                                    ");
        _sb.AppendLine("             , @pGameId                                    ");
        _sb.AppendLine("             )                                             ");
        _sb.AppendLine(" END                                                       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Session.GameTerminalId;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Value = GMGameId;

          _sql_cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertGameGatewayTerminalGameTranslation

    //------------------------------------------------------------------------------
    // PURPOSE: Update game meters for GameGateway
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LCD_Session
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    private static Boolean UpdateGameGatewayGameMeters(LCD_Session Session, SqlTransaction Trx)
    {
      Int64 _delta_played_count;
      Decimal _delta_played_amount;
      Int64 _delta_won_count;
      Decimal _delta_won_amount;

      try
      {
        switch (Session.TransactionType)
        {
          case GameGateway.TRANSACTION_TYPE.BET:
            _delta_played_count = Session.TotalBets;
            _delta_played_amount = Session.TotalAmount * -1;
            _delta_won_count = 0;
            _delta_won_amount = 0;
            break;

          case GameGateway.TRANSACTION_TYPE.PRIZE:
          case GameGateway.TRANSACTION_TYPE.PRIZE_PENDING:
            _delta_played_count = 0;
            _delta_played_amount = 0;
            _delta_won_count = Session.TotalBets;
            _delta_won_amount = Session.TotalAmount;

            break;

          case GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING:
            _delta_played_count = Session.TotalBets;
            _delta_played_amount = Session.TotalAmount;
            _delta_won_count = 0;
            _delta_won_amount = 0;

            break;

          default:
            _delta_played_count = 0;
            _delta_played_amount = 0;
            _delta_won_count = Session.TotalBets;
            _delta_won_amount = Session.TotalAmount;

            break;
        }

        using (SqlCommand _cmd = Terminal.GetGameMetersUpdateCommand(Trx))
        {

          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Session.GameTerminalId;
          _cmd.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar, 50).Value = Session.GameName;
          _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).Value = 0.01;
          _cmd.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).Value = 0;
          _cmd.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).Value = _delta_played_count;
          _cmd.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).Value = _delta_played_amount;
          _cmd.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).Value = _delta_won_count;
          _cmd.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).Value = _delta_won_amount;
          _cmd.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).Value = Session.TotalJackpot;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateGameGatewayGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE: Create DataTable for GAMEGATEWAY_COMMAND_MESSAGES
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Terminals
    //
    //      - OUTPUT:
    //        - DataTable
    //
    // RETURNS:
    //
    //   NOTES:
    //
    private static DataTable CreateCommandTerminalsTable(DataTable Terminals)
    {
      DataTable _dt_terminals;
      DataRow _dr_terminal;

      _dt_terminals = new DataTable();
      _dt_terminals.Columns.Add(GCM_TERMINAL_ID, Type.GetType("System.Int32"));

      try
      {
        foreach (DataRow _terminal in Terminals.Rows)
        {
          _dr_terminal = _dt_terminals.NewRow();
          _dr_terminal[GCM_TERMINAL_ID] = (Int32)_terminal[0];

          _dt_terminals.Rows.Add(_dr_terminal);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_terminals;
    } // CreateCommandTerminalsTable

    //------------------------------------------------------------------------------
    // PURPOSE: Get and insert PimPamGO command
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Session
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Nothing
    // 
    //   NOTES:
    //
    private static void GetAndInsertCommand_PimPamGO(ref LCD_Session Session, SqlTransaction Trx)
    {

      if (Session.TransactionType == GameGateway.TRANSACTION_TYPE.PRIZE && IsInPlaySession(ref Session))
      {
        if (!GetCommandParameterGameGateway(ref Session, TYPE_WCP.PRIZE, Trx))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
          Session.ErrorMessage = String.Format("Error GetCommandParameterGameGateway. GameInstanceId: {0}", Session.GameInstanceId);

          Log.Message(Session.ErrorMessage);

          return;
        }

        if (!InsertGamegatewayCommandBatch(Session, TYPE_WCP.PRIZE, Trx))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
          Session.ErrorMessage = String.Format("Error InsertGamegatewayCommandBatch. GameInstanceId: {0}", Session.GameInstanceId);

          Log.Message(Session.ErrorMessage);

          return;
        }
      }
    } // AddPimPamGOCommand

    #endregion " Private Methods "

  } // GameGateway
} // WSI.Common
