﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: LCD_Session.cs
//// 
////      DESCRIPTION: LCD session
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 09-OCT-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 09-OCT-2015 AMF    First release.
//// 11-DIC-2015 JCA & FJC  BackLog Item 7311: Bonoplay=> Mixed Mode.
//// 15-JAN-2016 AMF    PBI 8267: Rollback
//// 18-JAN-2016 AMF    PBI 8260: Traceability
//// 19-FEB-2016 AMF    PBI 9434: Award prizes
//// 28-ABR-2017 DMT    PBI 26733:Wigos - Section List Template
//// 29-JAN-2018 AGS    Bug 31349:WIGOS-7723 [Ticket #11857] Error al Convertir el valor del paremtero de Int64 a Int32
////------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public class LCD_Session : ICloneable
  {

    #region " Constants "

    private String PROVIDER_ZERO = "000";
    private Int64 PARIPLAY_GAME_INSTANCE_INDEX = 100000;

    #endregion " Constants "

    #region " Members "

    private String m_session_id;
    private Int64 m_game_id;
    private Int64 m_game_instance_id;
    private String m_game_name;
    private Int32 m_game_terminal_id;
    private Boolean m_show_new_game_message;
    private Int64 m_play_session_id;
    private Int64 m_account_id;
    private String m_nickname;
    private Int32 m_terminal_id;
    private Int32 m_partner_id;
    private String m_password;
    private MultiPromos.AccountBalance m_balance;
    private Int32 m_credit_time_out;
    private String m_currency_iso_code;
    private String m_bets;
    private Int32 m_total_bets;
    private Decimal m_total_amount;
    private Decimal m_total_jackpot;
    private Int64 m_wcp_id;
    private String m_wcp_parameter;
    private DataTable m_wcp_terminals;
    private Boolean m_show_lcd_message;
    private GameGateway.RESPONSE_ERROR_CODE m_error_code;
    private String m_error_message;
    private MultiPromos.GAME_GATEWAY_RESERVED_MODE m_reserved_credit;
    private DateTime? m_finished;
    private GameGateway.TRANSACTION_TYPE m_transaction_type;
    private String m_transaction_id;
    private Int64 m_movement_id;
    private String m_token;
    private Decimal m_response_balance;
    private GameGateway.PROVIDER_TYPE m_provider_type = GameGateway.PROVIDER_TYPE.PimPamGO;

    #endregion " Members "

    #region " Properties "

    public String SessionId
    {
      get { return m_session_id; }
      set { m_session_id = value; }
    } // SessionId

    public Int64 GameId
    {
      get { return m_game_id; }
      set { m_game_id = value; }
    } // GameId

    public Int64 GameInstanceId
    {
      get { return m_game_instance_id; }
      set { m_game_instance_id = value; }
    } // GameInstanceId

    public String GameName
    {
      get { return m_game_name; }
      set { m_game_name = value; }
    } // GameName

    public Int32 GameTerminalId
    {
      get { return m_game_terminal_id; }
      set { m_game_terminal_id = value; }
    } // GameTerminalId

    public Boolean ShowNewGameMessage
    {
      get { return m_show_new_game_message; }
      set { m_show_new_game_message = value; }
    } // ShowNewGameMessage

    public Int64 PlaySessionId
    {
      get { return m_play_session_id; }
      set { m_play_session_id = value; }
    } // PlaySessionId

    public Int64 AccountId
    {
      get { return m_account_id; }
      set { m_account_id = value; }
    } // AccountId

    public String NickName
    {
      get { return m_nickname; }
      set { m_nickname = value; }

    } // NickName

    public Int32 TerminalId
    {
      get { return m_terminal_id; }
      set { m_terminal_id = value; }
    } // TerminalId

    public Int32 PartnerId
    {
      get { return m_partner_id; }
      set { m_partner_id = value; }
    } // PartnerId

    public String Password
    {
      get { return m_password; }
      set { m_password = value; }
    } // Password

    public MultiPromos.AccountBalance Balance
    {
      get { return m_balance; }
      set { m_balance = value; }
    } // Balance

    public Int32 CreditTimeOut
    {
      get { return m_credit_time_out; }
      set { m_credit_time_out = value; }
    } // CreditTimeOut

    public String CurrencyIsoCode
    {
      get { return m_currency_iso_code; }
      set { m_currency_iso_code = value; }
    } // CurrencyIsoCode

    public String Bets
    {
      get { return m_bets; }
      set { m_bets = value; }
    } // Bets

    public Int32 TotalBets
    {
      get { return m_total_bets; }
      set { m_total_bets = value; }
    } // TotalBets

    public Decimal TotalAmount
    {
      get { return m_total_amount; }
      set { m_total_amount = value; }
    } // TotalPrizes

    public Decimal TotalJackpot
    {
      get { return m_total_jackpot; }
      set { m_total_jackpot = value; }
    } // TotalJackpot

    public Int64 WCPId
    {
      get { return m_wcp_id; }
      set { m_wcp_id = value; }
    } // WCPId

    public String WcpParameter
    {
      get { return m_wcp_parameter; }
      set { m_wcp_parameter = value; }
    } // WcpParameter

    public DataTable WcpTerminals
    {
      get { return m_wcp_terminals; }
      set { m_wcp_terminals = value; }
    } // WcpTerminals

    public Boolean ShowLCDMessage
    {
      get { return m_show_lcd_message; }
      set { m_show_lcd_message = value; }
    } // ShowLCDMessage

    public GameGateway.RESPONSE_ERROR_CODE ErrorCode
    {
      get { return m_error_code; }
      set { m_error_code = value; }
    } // ErrorCode

    public String ErrorMessage
    {
      get { return m_error_message; }
      set { m_error_message = value; }
    } // ErrorMessage

    public MultiPromos.GAME_GATEWAY_RESERVED_MODE ReservedCreditEnabled
    {
      get { return m_reserved_credit; }
      set { m_reserved_credit = value; }
    } // ReservedCreditEnabled

    public DateTime? Finished
    {
      get { return m_finished; }
      set { m_finished = value; }
    } // Finished

    public GameGateway.TRANSACTION_TYPE TransactionType
    {
      get { return m_transaction_type; }
      set { m_transaction_type = value; }
    } // TransactionType

    public String TransactionId
    {
      get { return m_transaction_id; }
      set { m_transaction_id = value; }
    } // TransactionId

    public Int64 MovementId
    {
      get { return m_movement_id; }
      set { m_movement_id = value; }
    } // MovementId

    public String Token
    {
      get { return m_token; }
      set { m_token = value; }
    } // Token

    public Decimal ResponseBalance
    {
      get { return m_response_balance; }
      set { m_response_balance = value; }
    } // ResponseBalance

    public GameGateway.PROVIDER_TYPE ProviderType
    {
      get { return m_provider_type; }
      set { m_provider_type = value; }
    } // ProviderType

    #endregion " Properties "

    #region " PimPamGO "

    #region " Public Methods "

    //------------------------------------------------------------------------------
    // PURPOSE: New LCD_Session
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SessionPartnerId
    //          - SessionPassword
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public LCD_Session(String SessionPartnerId, String SessionPassword)
    {
      String _provider_gp_id = PROVIDER_ZERO;
      Int32 _mode_reserved;

      _provider_gp_id = GetProviderGPId(SessionPartnerId);
      _mode_reserved = 0;

      if (GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.PartnerId", _provider_gp_id), String.Empty) != SessionPartnerId ||
          GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.Password", _provider_gp_id), String.Empty) != SessionPassword ||
         !GeneralParam.GetBoolean("GameGateway", String.Format("Provider.{0}.Enabled", _provider_gp_id), false))
      {
        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH;
        m_error_message = "PartnerId : " + SessionPartnerId.ToString() + ", Password : " + SessionPassword;
      }
      else
      {
        m_partner_id = Int32.Parse(SessionPartnerId);
        m_password = SessionPassword;
      }

      m_credit_time_out = GeneralParam.GetInt32("GameGateway", String.Format("Provider.{0}.CreditTimeOut", _provider_gp_id), 20, 10, 300);

      m_currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN");

      m_show_new_game_message = GeneralParam.GetBoolean("GameGateway", String.Format("Provider.{0}.ShowNewGameMessage", _provider_gp_id), true);

      _mode_reserved = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);
      m_reserved_credit = (MultiPromos.GAME_GATEWAY_RESERVED_MODE)_mode_reserved;
    } // LCD_Session

    //------------------------------------------------------------------------------
    // PURPOSE: New LCD_Session
    // 
    //  PARAMS:
    //      - INPUT:
    //          - UserId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public LCD_Session(Int64 UserId)
    {
      Int32 _mode_reserved;

      _mode_reserved = (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS; ;

      try
      {
        m_error_code = GameGateway.RESPONSE_ERROR_CODE.OK;

        m_account_id = UserId;

        m_currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN");

        // Mode Reserved
        _mode_reserved = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);
        if (_mode_reserved == (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS && GameGateway.AccountModeReserved(m_account_id))
        {
          m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS;
        }
        else if (_mode_reserved == (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)
        {
          m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED;
        }
        else
        {
          m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_NO_SESSIONID;
        m_error_message = "User Id : " + SessionId;
      }
    } // LCD_Session

    //------------------------------------------------------------------------------
    // PURPOSE: New LCD_Session
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SessionId
    //          - SessionPartnerId
    //          - SessionPassword
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public LCD_Session(String SessionId, Int32 SessionPartnerId, String SessionPassword)
    {
      String _provider_gp_id = PROVIDER_ZERO;
      Int32 _mode_reserved;

      _mode_reserved = (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS;

      try
      {
        m_error_code = GameGateway.RESPONSE_ERROR_CODE.OK;

        m_session_id = SessionId;

        m_play_session_id = Int64.Parse(SessionId.Split('_')[0]);
        m_account_id = Int64.Parse(SessionId.Split('_')[1]);
        m_terminal_id = Int32.Parse(SessionId.Split('_')[2]);
        _provider_gp_id = SessionId.Split('_')[3];

        if (!GameGateway.CheckPlaySession(this))
        {
          m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_NO_SESSIONID;
          m_error_message = "Session Id : " + SessionId;

          return;
        }

        if (GeneralParam.GetInt32("GameGateway", String.Format("Provider.{0}.PartnerId", _provider_gp_id), 0) != SessionPartnerId ||
            GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.Password", _provider_gp_id), String.Empty) != SessionPassword ||
           !GeneralParam.GetBoolean("GameGateway", String.Format("Provider.{0}.Enabled", _provider_gp_id), false))
        {
          m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH;
          m_error_message = "PartnerId : " + SessionPartnerId.ToString() + ", Password : " + SessionPassword;
        }
        else
        {
          m_partner_id = SessionPartnerId;
          m_password = SessionPassword;
        }

        m_credit_time_out = GeneralParam.GetInt32("GameGateway", String.Format("Provider.{0}.CreditTimeOut", _provider_gp_id), 20, 10, 300);

        m_currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN");

        m_show_new_game_message = GeneralParam.GetBoolean("GameGateway", String.Format("Provider.{0}.ShowNewGameMessage", _provider_gp_id), true);

        // Mode Reserved
        _mode_reserved = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);
        if (_mode_reserved == (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS && GameGateway.AccountModeReserved(m_account_id))
        {
          m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS;
        }
        else if (_mode_reserved == (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)
        {
          m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED;
        }
        else
        {
          m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_NO_SESSIONID;
        m_error_message = "Session Id : " + SessionId;
      }
    } // LCD_Session

    //------------------------------------------------------------------------------
    // PURPOSE: New LCD_Session
    // 
    //  PARAMS:
    //      - INPUT:
    //          - UserId
    //          - SessionPartnerId
    //          - SessionPassword
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public LCD_Session(Int64 UserId, String SessionPartnerId, String SessionPassword)
    {
      String _provider_gp_id = PROVIDER_ZERO;
      Int32 _mode_reserved;

      _mode_reserved = (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS;

      try
      {
        m_error_code = GameGateway.RESPONSE_ERROR_CODE.OK;

        m_account_id = UserId;

        _provider_gp_id = GetProviderGPId(SessionPartnerId);

        if (GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.PartnerId", _provider_gp_id), String.Empty) != SessionPartnerId ||
            GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.Password", _provider_gp_id), String.Empty) != SessionPassword ||
           !GeneralParam.GetBoolean("GameGateway", String.Format("Provider.{0}.Enabled", _provider_gp_id), false))
        {
          m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH;
          m_error_message = "PartnerId : " + SessionPartnerId.ToString() + ", Password : " + SessionPassword;
        }
        else
        {
          m_partner_id = Int32.Parse(SessionPartnerId);
          m_password = SessionPassword;
        }

        m_credit_time_out = GeneralParam.GetInt32("GameGateway", String.Format("Provider.{0}.CreditTimeOut", _provider_gp_id), 20, 10, 300);

        m_currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN");

        m_show_new_game_message = GeneralParam.GetBoolean("GameGateway", String.Format("Provider.{0}.ShowNewGameMessage", _provider_gp_id), true);

        // Mode Reserved
        _mode_reserved = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);
        if (_mode_reserved == (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS && GameGateway.AccountModeReserved(m_account_id))
        {
          m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS;
        }
        else if (_mode_reserved == (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)
        {
          m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED;
        }
        else
        {
          m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_NO_SESSIONID;
        m_error_message = "User Id : " + UserId.ToString();
      }
    } // LCD_Session

    //------------------------------------------------------------------------------
    // PURPOSE: New LCD_Session
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public LCD_Session()
    {

    } // LCD_Session

    public Boolean IsSessionWinUp(String SessionId, int TerminalId)
    {
      TerminalTypes _terminal_type;
      _terminal_type = TerminalTypes.UNKNOWN;
  
      //SqlTransaction Trx;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (!WSI.Common.Terminal.Trx_GetTerminalType(TerminalId, _db_trx.SqlTransaction, out _terminal_type))
            return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      if (SessionId == "0" && _terminal_type == TerminalTypes.WIN_UP)
        return true;
      else
        return false;
    } //  IsSessionWinUp

    //------------------------------------------------------------------------------
    // PURPOSE: Clone funcionality
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Object
    // 
    //   NOTES:
    //
    public object Clone()
    {
      return this.MemberwiseClone();
    } // Clone

    #endregion

    #region " Privated Methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Get Provider GP Id by PartnerId
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SessionPartnerId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Provider Id
    // 
    //   NOTES:
    //
    private String GetProviderGPId(String SessionPartnerId)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   SUBSTRING(GP_SUBJECT_KEY,10,3)             ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS                             ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY   =    'GameGateway'          ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY LIKE 'Provider.%.PartnerId' ");
        _sb.AppendLine("    AND   GP_KEY_VALUE   =    @pPartnerId            ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.NVarChar, 3).Value = SessionPartnerId;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj == null || _obj == DBNull.Value)
            {
              return PROVIDER_ZERO;
            }
            else
            {
              return (String)_obj;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return PROVIDER_ZERO;

    } // GetProviderGPId

    #endregion " Private Methods "

    #endregion

    #region " PariPlay "

    #region " Public Methods "

    /// <summary>
    /// New LCD_Session (PariPlay)
    /// </summary>
    /// <param name="Token"></param>
    /// <param name="Username"></param>
    /// <param name="Password"></param>
    /// <param name="IsExpired"></param>
    public LCD_Session(String Token, String Username, String Password, out Boolean IsExpired)
    {
      PariPlay_WigosAuthentication(Token, Username, Password, out IsExpired);
    } // LCD_Session

    /// <summary>
    /// Manage PariPlay token
    /// </summary>
    /// <param name="Token"></param>
    /// <param name="IsAuthentication"></param>
    /// <returns></returns>
    public Boolean PariPlay_TokenManager(string Token, SqlTransaction SqlTrx)
    {
      return PariPlay_TokenManager(Token, false, SqlTrx);
    } // PariPlay_TokenManager
    public Boolean PariPlay_TokenManager(string Token, Boolean IsAuthentication, SqlTransaction SqlTrx)
    {
      // Start instance if called from Authentication request only!!!
      if (IsAuthentication)
      {
        return PariPlay_StartInstance(SqlTrx);
      }

      // Active instance for token. 
      if (GameInstanceId == 0)
      {
        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_AUTHENTICATION;
        m_error_message = String.Format("Error PariPlay_StartInstance: Token '{0}' has already active instance.", Token);

        return false;
      }

      return true;
    } // PariPlay_TokenManager

    /// <summary>
    /// End PariPlay Game Instance
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean PariPlay_EndGameInstance(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("  UPDATE    GAMEGATEWAY_GAME_INSTANCES                  ");
        _sb.AppendLine("     SET    GGI_FINISHED = GETDATE()                    ");
        _sb.AppendLine("    WHERE   GGI_GAME_INSTANCE_ID   = @pGameInstanceId   ");
        _sb.AppendLine("      AND   GGI_GAME_ID            = @pGameId           ");
        _sb.AppendLine("      AND   GGI_PARTNER_ID         = @pPartnerId        ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).Value = GameInstanceId;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = GameId;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = PartnerId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
            m_error_message = String.Format("EndPariPlayGameInstance: Error on close game instance {0}.", GameInstanceId);

            return false;
          }
        }

        return PariPlay_MarkTokenToExpired(SqlTrx);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        m_error_message = String.Format("EndPariPlayGameInstance: Error on close game instance {0}.", GameInstanceId);
      }

      return false;

    } // PariPlay_EndGameInstance

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// PariPlay Wigos Authentication
    /// </summary>
    /// <param name="Token"></param>
    /// <param name="Username"></param>
    /// <param name="Password"></param>
    /// <param name="IsExpired"></param>
    private void PariPlay_WigosAuthentication(String Token, String Username, String Password, out Boolean IsExpired)
    {
      IsExpired = true;
      m_error_code = GameGateway.RESPONSE_ERROR_CODE.OK;

      try
      {
        // LogIn
        if (!PariPlay_Login(Token, Username, Password))
        {
          return;
        }

        // Set session data
        m_total_bets = 1; // By default
        m_partner_id = (Int32)GameGateway.PROVIDER_TYPE.PariPlay;
        m_provider_type = GameGateway.PROVIDER_TYPE.PariPlay;
        m_reserved_credit = MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS;

        m_token = Token;
        m_password = Password;

        // Get PariPlay session data
        if (!PariPlay_GetSessionData(Token, out IsExpired))
        {
          m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_TOKEN_INVALID;
          m_error_message = String.Format("Token '{0}' is not valid", Token);
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_NO_SESSIONID;
        m_error_message = "PartnerId : " + m_partner_id.ToString();
      }

    } // PariPlay_WigosAuthentication

    /// <summary>
    /// PariPlay login
    /// </summary>
    /// <param name="Token"></param>
    /// <param name="Username"></param>
    /// <param name="Password"></param>
    /// <returns></returns>
    private bool PariPlay_Login(string Token, string Username, string Password)
    {
      // Internal Authentication
      if (GeneralParam.GetString("PariPlay", "UserName", String.Empty) != Username ||
          GeneralParam.GetString("PariPlay", "Password", String.Empty) != Password ||
         !GeneralParam.GetBoolean("PariPlay", "Enabled", false))
      {
        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH;
        m_error_message = "UserName : " + Username.ToString() + ", Password : " + Password;

        return false;
      }

      return true;

    } // PariPlay_Login

    /// <summary>
    /// Create new PariPlay game instance
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean PariPlay_StartInstance(SqlTransaction SqlTrx)
    {
      // GameInstanceId request in 'PariPlay_WigosAuthentication'
      // Check if exist active instance for the current token
      if (GameInstanceId != 0)
      {
        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_PLAYER_HAVE_OPENED_ROUNDS;
        m_error_message = String.Format("Error PariPlay_StartInstance: Token '{0}' has already active instance.", Token);

        return false;
      }

      // Get GameId from 'MAPP_GAMES_EXTERNAL' table
      if (!PariPlay_SetGameId(SqlTrx))
      {
        return false;
      }

      // Insert into 'GAMEGATEWAY_GAME_INSTANCES' table
      return PariPlay_InsertNewGameInstance(SqlTrx);

    } // PariPlay_StartInstance

    /// <summary>
    /// Set GameId by GameCode
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean PariPlay_SetGameId(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   GE_GAME_EXTERNAL_ID     ");
        _sb.AppendLine("   FROM   MAPP_GAMES_EXTERNAL     ");
        _sb.AppendLine("  WHERE   GE_CODE = @pGameCode    ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          // GameId and PartnerId setted in 'PariPlay_WigosAuthentication.PariPlay_GetSessionData()'
          _sql_cmd.Parameters.Add("@pGameCode", SqlDbType.VarChar).Value = m_game_name;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_INVALID_GAME;
              m_error_message = String.Format("Error PariPlay_SetGameId. The game not exist: {0}", m_game_instance_id);

              return false;
            }

            // Set GameId
            m_game_id = _reader.GetInt64(0);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        m_error_message = "Error PariPlay_InsertNewGameInstance: Error on execute query";
      }

      return false;

    } // PariPlay_SetGameId

    /// <summary>
    /// Insert new PariPlay game instance and Update External sessions
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean PariPlay_InsertNewGameInstance(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("  INSERT INTO GAMEGATEWAY_GAME_INSTANCES              ");
        _sb.AppendLine("              ( GGI_GAME_INSTANCE_ID                  ");
        _sb.AppendLine("              , GGI_PARTNER_ID                        ");
        _sb.AppendLine("              , GGI_GAME_ID                           ");
        _sb.AppendLine("              , GGI_FIRST_PRIZE                       ");
        _sb.AppendLine("              , GGI_ENTRY_COST                        ");
        _sb.AppendLine("              , GGI_JACKPOT                           ");
        _sb.AppendLine("              , GGI_STARTS                            ");
        _sb.AppendLine("              , GGI_CREATED          )                ");
        _sb.AppendLine("      VALUES  ( @pGameInstanceId                      ");
        _sb.AppendLine("              , @pPartnerId                           ");
        _sb.AppendLine("              , @pGameId                              ");
        _sb.AppendLine("              , 0                                     ");
        _sb.AppendLine("              , 0                                     ");
        _sb.AppendLine("              , 0                                     ");
        _sb.AppendLine("              , @pDate                                ");
        _sb.AppendLine("              , @pDate               )                ");
        _sb.AppendLine("                                                      ");
        _sb.AppendLine("  UPDATE   MAPP_GAMES_EXTERNAL_SESSIONS               ");
        _sb.AppendLine("     SET   EGS_GAME_ID          = @pGameId            ");
        _sb.AppendLine("         , EGS_GAME_INSTANCE_ID = @pGameInstanceId    ");
        _sb.AppendLine("    WHERE  EGS_TOKEN            = @pToken             ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          // Set new squence id
          if (!PariPlay_SetNewSequenceId(SqlTrx))
          {
            return false;
          }

          // GameId and PartnerId setted in 'PariPlay_WigosAuthentication.PariPlay_GetSessionData()'
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = m_game_id;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = m_partner_id;
          _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.NVarChar).Value = m_game_instance_id;
          _sql_cmd.Parameters.Add("@pToken", SqlDbType.VarChar).Value = Token;
          _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = WGDB.Now;

          if (_sql_cmd.ExecuteNonQuery() != 2) // 2 = Insert & Update
          {
            m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_PLAYER_HAVE_OPENED_ROUNDS;
            m_error_message = String.Format("Error PariPlay_InsertNewGameInstance. Exist active instance for this game: {0}", m_game_instance_id);

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        m_error_message = "Error PariPlay_InsertNewGameInstance: Error on execute query";
      }

      return false;

    } // PariPlay_InsertNewGameInstance

    /// <summary>
    /// Set new SequenceId
    /// </summary>
    private Boolean PariPlay_SetNewSequenceId(SqlTransaction SqlTrx)
    {
      try
      {
        if (Sequences.GetValue(SqlTrx, SequenceId.ParyPlay, out m_game_instance_id))
        {
          m_game_instance_id += PARIPLAY_GAME_INSTANCE_INDEX;
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        m_error_message = "Error PariPlay_SetNewSequenceId: Error gettin new game instance id.";
      }

      return false;

    } // PariPlay_SetNewSequenceId

    /// <summary>
    /// Start PariPlay Instance
    /// </summary>
    /// <param name="Token"></param>
    /// <param name="IsExpired"></param>
    /// <returns></returns>
    private Boolean PariPlay_GetSessionData(String Token, out Boolean IsExpired)
    {
      StringBuilder _sb;

      IsExpired = false;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   EGS_ACCOUNT_ID                ");
        _sb.AppendLine("        , EGS_GAME_CODE                 ");
        _sb.AppendLine("        , EGS_GAME_ID                   ");
        _sb.AppendLine("        , EGS_GAME_INSTANCE_ID          ");
        _sb.AppendLine("        , EGS_FINISHED                  ");
        _sb.AppendLine("   FROM   MAPP_GAMES_EXTERNAL_SESSIONS  ");
        _sb.AppendLine("  WHERE   EGS_TOKEN   = @pToken         ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pToken", SqlDbType.VarChar, 40).Value = Token;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_TOKEN_INVALID;
                m_error_message = String.Format("PariPlay_GetSessionData: Token '{0}' is not valid.", Token);

                return false;
              }

              // Set session data
              m_account_id = _reader.GetInt64(0);    // PlayerId
              m_game_name = _reader.GetString(1);   // GameCode

              // EGS_GAME_ID != 0: Exist active instance
              // EGS_GAME_ID == 0: No exist active instance
              m_game_id = (_reader[2] == DBNull.Value) ? 0 : _reader.GetInt64(2);       // GameId

              // EGS_GAME_INSTANCE_ID != 0: Exist active instance
              // EGS_GAME_INSTANCE_ID == 0: No exist active instance 
              m_game_instance_id = (_reader[3] == DBNull.Value) ? 0 : _reader.GetInt64(3); // GameInstanceId

              // EGS_FINISHED == 0 Token not expired
              // EGS_FINISHED == 1 Token expired
              IsExpired = _reader.GetBoolean(4);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        m_error_message = String.Format("Error PariPlayTokenIsValid. Token: {0}", Token);
      }

      return false;

    } // PariPlay_GetSessionData

    /// <summary>
    /// Mark Token to Expired
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean PariPlay_MarkTokenToExpired(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   MAPP_GAMES_EXTERNAL_SESSIONS ");
        _sb.AppendLine("   SET   EGS_FINISHED = @pExpired     ");
        _sb.AppendLine("  WHERE  EGS_TOKEN    = @pToken       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pExpired", SqlDbType.Bit).Value = true;
          _sql_cmd.Parameters.Add("@pToken", SqlDbType.VarChar, 40).Value = Token;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_TOKEN_INVALID;
            m_error_message = String.Format("MarkTokenToExpired: Token '{0}' is not valid.", Token);

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_code = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        m_error_message = String.Format("Error MarkTokenToExpired. Token: {0}", Token);
      }

      return false;

    } // PariPlay_MarkTokenToExpired

    #endregion

    #endregion
  }
}
