﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalDraw.cs
// 
//   DESCRIPTION: Class to calculate  TerminalDraw
// 
//        AUTHOR: Fernando Jimenez
// 
// CREATION DATE: 23-JAN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-JAN-2017 FJC    Initial version
// 30-MAY-2017 FJC    PBI 27761:Sorteo de máquina. Modificiones en la visualización de las pantallas (LCD y InTouch)). 
// 22-AUG-2017 FJC    Fixed Defect: WIGOS-4649 Terminal Draw: The Draw does not appear in the terminal and continue with start card session
// 29-AUG-2017 FJC    WIGOS-4771 Terminal Draw. Include GameId for each recharge
// 04-SEP-2017 FJC    PBI 29532:Terminal Draw. Apply retrocompatibility (18.315 & 18.319)
// 18-OCT-2017 FJC    WIGOS-5928 Terminal draw does not work neither in LCD nor 4 lines
// 16-FEB-2018 FJC    WIGOS-8082 [Ticket #12237] Sorteo en Terminal
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;

namespace WSI.Common.TerminalDraw.Persistence
{
  #region "Recharge Entities"

  public struct RechargeData
  {
    public Int64 TdrId;
    public Int64 AccountId;
    public Int64 TerminalId;
    public Int64 OperationId;
    public Int64 GameId;
    public Int64 GameType;
    public String GameUrl;
    public Decimal AmountTotalCashIn;
    public Decimal AmountReBet;
    public Decimal AmountNrBet;
    public Int64 AmountPointsBet;
    
    //First Screen
    public String FirstScreenMessageLine0;
    public String FirstScreenMessageLine1;
    public String FirstScreenMessageLine2;
    public Int64 FirstScreenTimeOut;
    public Boolean TimeOutExpiresParticipateInDraw;
    public Boolean ForceParticipateInDraw;

    //Second Screen
    public String SecondScreenMessageLine0;
    public String SecondScreenMessageLine1;
    public String SecondScreenMessageLine2;
    public String SecondScreenMessageLine3;
    public Int64 SecondScreenTimeOut;
    public Boolean ForceExecuteDraw;

    //PlaySessionId
    public Int64 PlaySessionId;

    //Total Bet Amount
    public Decimal TotalBetAmount;

    //Balance
    public Decimal BalanceAmount;
  }

  public class TerminalDrawAccountEntity
  {
    #region "Members"
    private Int64 m_account_id;
    private Int64 m_terminal_id;

    private List<TerminalDrawGamesEntity> m_games;
    #endregion

    #region "Properties"
    public Int64 AccountId
    {
      get { return m_account_id; }
      set { m_account_id = value; }
    }
    public Int64 TerminalId
    {
      get { return m_terminal_id; }
      set { m_terminal_id = value; }
    }

    public List<TerminalDrawGamesEntity> Games
    {
      get { return m_games; }
      set { m_games = value; }
    }
    #endregion

    #region "Public Methods"
    public void AddRecharge(RechargeData InputRechargeData)
    {
      TerminalDrawGamesEntity _terminal_draw_recharge_games;
      TerminalDrawRechargeEntity _terminal_draw_recharge_recharge;

      _terminal_draw_recharge_games = new TerminalDrawGamesEntity();
      _terminal_draw_recharge_recharge = new TerminalDrawRechargeEntity();
      
      this.AccountId = InputRechargeData.AccountId;
      this.TerminalId = InputRechargeData.TerminalId;
      _terminal_draw_recharge_recharge.TdrId = InputRechargeData.TdrId;
      _terminal_draw_recharge_recharge.OperationId = InputRechargeData.OperationId;
      _terminal_draw_recharge_games.GameUrl = InputRechargeData.GameUrl;

      // First Screen
      _terminal_draw_recharge_games.FirstScreenMessageLine0 = InputRechargeData.FirstScreenMessageLine0;
      _terminal_draw_recharge_games.FirstScreenMessageLine1 = InputRechargeData.FirstScreenMessageLine1;
      _terminal_draw_recharge_games.FirstScreenMessageLine2 = InputRechargeData.FirstScreenMessageLine2;
      _terminal_draw_recharge_games.FirstScreenTimeOut = InputRechargeData.FirstScreenTimeOut;
      _terminal_draw_recharge_games.TimeoutExpiresParticipateInDraw = InputRechargeData.TimeOutExpiresParticipateInDraw;
      _terminal_draw_recharge_games.ForceParticipateInDraw = InputRechargeData.ForceParticipateInDraw;
      
      // Second Screen
      _terminal_draw_recharge_games.SecondScreenMessageLine0 = InputRechargeData.SecondScreenMessageLine0;
      _terminal_draw_recharge_games.SecondScreenMessageLine1 = InputRechargeData.SecondScreenMessageLine1;
      _terminal_draw_recharge_games.SecondScreenMessageLine2 = InputRechargeData.SecondScreenMessageLine2;
      _terminal_draw_recharge_games.SecondScreenMessageLine3 = InputRechargeData.SecondScreenMessageLine3;
      _terminal_draw_recharge_games.SecondScreenTimeOut = InputRechargeData.SecondScreenTimeOut;
      _terminal_draw_recharge_games.ForceExecuteDraw = InputRechargeData.ForceExecuteDraw;
      
      // Recharge Data
      _terminal_draw_recharge_recharge.AmountTotalCashIn = InputRechargeData.AmountTotalCashIn;
      _terminal_draw_recharge_recharge.AmountReBet = InputRechargeData.AmountReBet;
      _terminal_draw_recharge_recharge.AmountNrBet = InputRechargeData.AmountNrBet;
      _terminal_draw_recharge_recharge.AmountPointsBet = InputRechargeData.AmountPointsBet;
      _terminal_draw_recharge_recharge.GameId = InputRechargeData.GameId;
      _terminal_draw_recharge_recharge.GameType = InputRechargeData.GameType;

      // Other
      _terminal_draw_recharge_games.PlaySessionId = InputRechargeData.PlaySessionId;
      _terminal_draw_recharge_games.TotalBetAmount = InputRechargeData.TotalBetAmount;
      _terminal_draw_recharge_games.BalanceAmount = InputRechargeData.BalanceAmount;

      // Add Recharge
      _terminal_draw_recharge_games.Recharges.Add(_terminal_draw_recharge_recharge);
      
      //Only for one game
      if (this.Games.Count <= 0)
      {
        this.Games.Add(_terminal_draw_recharge_games);
      }
      else
      {
        this.Games[0].Recharges.Add(_terminal_draw_recharge_recharge);
      }
    }

    #endregion

    #region "Constructor"

    public TerminalDrawAccountEntity()
    {
      m_games = new List<TerminalDrawGamesEntity>();
    }

    #endregion
  }

  public class TerminalDrawGamesEntity
  {
    #region "Members"
    private String m_game_url;
    
    //First Screen
    private String m_first_screen_message_line_0;
    private String m_first_screen_message_line_1;
    private String m_first_screen_message_line_2;
    private Int64 m_first_screen_time_out;
    private Boolean m_timeout_expires_participate_in_draw;
    private Boolean m_force_participate_in_draw;

    //Second Screen
    private String m_second_screen_message_line_0;
    private String m_second_screen_message_line_1;
    private String m_second_screen_message_line_2;
    private String m_second_screen_message_line_3;
    private Int64 m_second_screen_time_out;
    private Boolean m_force_execute_draw;

    //Play Session Id
    private Int64 m_play_session_id;

    //Recharges
    private List<TerminalDrawRechargeEntity> m_recharges;

    //Bet Amount
    private Decimal m_total_bet_amount;

    //Balance
    private Decimal m_balance_amount;

    #endregion

    #region "Properties"
    public String GameUrl
    {
      get { return m_game_url; }
      set { m_game_url = value; }
    }

    public String FirstScreenMessageLine0
    {
      get { return m_first_screen_message_line_0; }
      set { m_first_screen_message_line_0 = value; }
    }

    public String FirstScreenMessageLine1
    {
      get { return m_first_screen_message_line_1; }
      set { m_first_screen_message_line_1 = value; }
    }

    public String FirstScreenMessageLine2
    {
      get { return m_first_screen_message_line_2; }
      set { m_first_screen_message_line_2 = value; }
    }

    public Int64 FirstScreenTimeOut
    {
      get { return m_first_screen_time_out; }
      set { m_first_screen_time_out = value; }
    }

    public Boolean TimeoutExpiresParticipateInDraw
    {
      get { return m_timeout_expires_participate_in_draw; }
      set { m_timeout_expires_participate_in_draw = value; }
    }

    public Boolean ForceParticipateInDraw
    {
      get { return m_force_participate_in_draw; }
      set { m_force_participate_in_draw = value; }
    }

    public String SecondScreenMessageLine0
    {
      get { return m_second_screen_message_line_0; }
      set { m_second_screen_message_line_0 = value; }
    }

    public String SecondScreenMessageLine1
    {
      get { return m_second_screen_message_line_1; }
      set { m_second_screen_message_line_1 = value; }
    }

    public String SecondScreenMessageLine2
    {
      get { return m_second_screen_message_line_2; }
      set { m_second_screen_message_line_2 = value; }
    }

    public String SecondScreenMessageLine3
    {
      get { return m_second_screen_message_line_3; }
      set { m_second_screen_message_line_3 = value; }
    }

    public Int64 SecondScreenTimeOut
    {
      get { return m_second_screen_time_out; }
      set { m_second_screen_time_out = value; }
    }
    public Boolean ForceExecuteDraw
    {
      get { return m_force_execute_draw; }
      set { m_force_execute_draw = value; }
    }

    public Int64 PlaySessionId
    {
      get { return m_play_session_id; }
      set { m_play_session_id = value; }
    }

    public List<TerminalDrawRechargeEntity> Recharges
    {
      get { return m_recharges; }
      set { m_recharges = value; }
    }

    public Decimal TotalBetAmount
    {
      get { return m_total_bet_amount; }
      set { m_total_bet_amount = value; }
    }

    public Decimal BalanceAmount
    {
      get { return m_balance_amount; }
      set { m_balance_amount = value; }
    }

    #endregion

    #region "Constructor"
    public TerminalDrawGamesEntity()
    {
      m_recharges = new List<TerminalDrawRechargeEntity>();
      
      m_first_screen_message_line_0 = String.Empty;
      m_first_screen_message_line_1 = String.Empty;
      m_first_screen_message_line_2 = String.Empty;

      m_second_screen_message_line_0 = String.Empty;
      m_second_screen_message_line_1 = String.Empty;
      m_second_screen_message_line_2 = String.Empty;
      m_second_screen_message_line_3 = String.Empty;
    }
    #endregion
  }

  public class TerminalDrawRechargeEntity
  {
    #region "Members"

    private Int64 m_tdr_id;
    private Decimal m_amount_total_cash_in;
    private Decimal m_amount_re_bet;
    private Decimal m_amount_nr_bet;
    private Int64 m_amount_points_bet;
    private TerminalDraw.StatusTerminalDraw m_status;
    private Int64 m_operation_id;
    private Int64 m_game_id;
    private Int64 m_game_type;

    #endregion

    #region Properties
    public Int64 TdrId
    {
      get { return m_tdr_id; }
      set { m_tdr_id = value; }
    }

    public Decimal AmountTotalCashIn
    {
      get { return m_amount_total_cash_in; }
      set { m_amount_total_cash_in = value; }
    }
    public Decimal AmountReBet
    {
      get { return m_amount_re_bet; }
      set { m_amount_re_bet = value; }
    }
    public Decimal AmountNrBet
    {
      get { return m_amount_nr_bet; }
      set { m_amount_nr_bet = value; }
    }
    public Int64 AmountPointsBet
    {
      get { return m_amount_points_bet; }
      set { m_amount_points_bet = value; }
    }
    public TerminalDraw.StatusTerminalDraw Status
    {
      get { return m_status; }
      set { m_status = value; }
    }
    public Int64 OperationId
    {
      get { return m_operation_id; }
      set { m_operation_id = value; }
    }
    public Int64 GameId
    {
      get { return m_game_id; }
      set { m_game_id = value; }
    }
    public Int64 GameType
    {
      get { return m_game_type; }
      set { m_game_type = value; }
    }

    public Decimal MinimumPrize
    {
      get { return this.AmountTotalCashIn - (this.AmountReBet + this.AmountNrBet); }
    }

    #endregion
  }
  #endregion 

  #region "Prize Plan Entities"

  public struct PrizePlanData
  {
    public Int64 GameId;
    public Int64 GameType;
    public String TerminalGameUrl;
    public String TerminalGameName;
    
    //Winner
    public String ResultWinnerScreenMessageLine0;
    public String ResultWinnerScreenMessageLine1;
    public String ResultWinnerScreenMessageLine2;
    public String ResultWinnerScreenMessageLine3;

    //Looser
    public String ResultLooserScreenMessageLine0;
    public String ResultLooserScreenMessageLine1;
    public String ResultLooserScreenMessageLine2;
    public String ResultLooserScreenMessageLine3;

    public Boolean IsWinner;
    public Decimal AmountRePlayedDraw;
    public Decimal AmountRePlayedWon;
    public Decimal AmountNrPlayedDraw;
    public Decimal AmountNrPlayedWon;
    public Int64 AmountPointsPlayedDraw;
    public Int64 AmountPointsPlayedWon;
    public Int64 TerminalGameTimeOut; // TODO
    public Decimal AmountTotalBalance;
    public Int64 PlaySessionId;
    public Boolean Forced;
  }


  public class TerminalDrawPrizePlanGames
  {
    #region "Members"
    private List<TerminalDrawPrizePlanDetail> m_games;
    #endregion

    #region "Properties"
    public List<TerminalDrawPrizePlanDetail> Games
    {
      get { return m_games; }
      set { m_games = value; }
    }
    #endregion

    #region "Constructor"
    public TerminalDrawPrizePlanGames()
    {
      m_games = new List<TerminalDrawPrizePlanDetail>();
    }
    #endregion

    #region "Public Methods"
    public void AddPrizePlan(PrizePlanData InputPrizePlanData)
    {
      TerminalDrawPrizePlanDetail _terminal_draw_prize_plan;

      _terminal_draw_prize_plan = new TerminalDrawPrizePlanDetail();
      _terminal_draw_prize_plan.GameId = InputPrizePlanData.GameId;
      _terminal_draw_prize_plan.GameType = InputPrizePlanData.GameType;
      _terminal_draw_prize_plan.TerminalGameUrl = InputPrizePlanData.TerminalGameUrl;
      _terminal_draw_prize_plan.TerminalGameName = InputPrizePlanData.TerminalGameName;

      // Winner
      _terminal_draw_prize_plan.ResultWinnerScreenMessageLine0 = InputPrizePlanData.ResultWinnerScreenMessageLine0;
      _terminal_draw_prize_plan.ResultWinnerScreenMessageLine1 = InputPrizePlanData.ResultWinnerScreenMessageLine1;
      _terminal_draw_prize_plan.ResultWinnerScreenMessageLine2 = InputPrizePlanData.ResultWinnerScreenMessageLine2;
      _terminal_draw_prize_plan.ResultWinnerScreenMessageLine3 = InputPrizePlanData.ResultWinnerScreenMessageLine3;

      // Looser
      _terminal_draw_prize_plan.ResultLooserScreenMessageLine0 = InputPrizePlanData.ResultLooserScreenMessageLine0;
      _terminal_draw_prize_plan.ResultLooserScreenMessageLine1 = InputPrizePlanData.ResultLooserScreenMessageLine1;
      _terminal_draw_prize_plan.ResultLooserScreenMessageLine2 = InputPrizePlanData.ResultLooserScreenMessageLine2;
      _terminal_draw_prize_plan.ResultLooserScreenMessageLine3 = InputPrizePlanData.ResultLooserScreenMessageLine3;

      _terminal_draw_prize_plan.IsWinner = InputPrizePlanData.IsWinner;
      _terminal_draw_prize_plan.AmountRePlayedDraw = InputPrizePlanData.AmountRePlayedDraw;
      _terminal_draw_prize_plan.AmountRePlayedWon = InputPrizePlanData.AmountRePlayedWon;
      _terminal_draw_prize_plan.AmountNrPlayedDraw = InputPrizePlanData.AmountNrPlayedDraw;
      _terminal_draw_prize_plan.AmountNrPlayedWon = InputPrizePlanData.AmountNrPlayedWon;
      _terminal_draw_prize_plan.AmountPointsPlayedDraw = InputPrizePlanData.AmountPointsPlayedDraw;
      _terminal_draw_prize_plan.AmountPointsPlayedWon = InputPrizePlanData.AmountPointsPlayedWon;
      _terminal_draw_prize_plan.TerminalGameTimeOut = InputPrizePlanData.TerminalGameTimeOut;
      _terminal_draw_prize_plan.AmountTotalBalance = InputPrizePlanData.AmountTotalBalance;
      _terminal_draw_prize_plan.PlaySessionId = InputPrizePlanData.PlaySessionId;
      _terminal_draw_prize_plan.Forced = InputPrizePlanData.Forced;

      this.Games.Add(_terminal_draw_prize_plan);
    }
    #endregion
  }

  public class TerminalDrawPrizePlanDetail
  {
    #region "Members"
    private Int64 m_game_id;
    private Int64 m_game_type;
    private string m_terminal_game_url;
    private string m_terminal_game_name;

    //Result Winner 
    private string m_result_winner_screen_message_line_0;
    private string m_result_winner_screen_message_line_1;
    private string m_result_winner_screen_message_line_2;
    private string m_result_winner_screen_message_line_3;

    //Result Looser 
    private string m_result_looser_screen_message_line_0;
    private string m_result_looser_screen_message_line_1;
    private string m_result_looser_screen_message_line_2;
    private string m_result_looser_screen_message_line_3;

    private Boolean m_is_winner;
    private Decimal m_amount_re_played_draw;
    private Decimal m_amount_re_played_won;
    private Decimal m_amount_nr_played_draw;
    private Decimal m_amount_nr_played_won;
    private long m_amount_points_played_draw;
    private long m_amount_points_played_won;
    private long m_terminal_game_time_out;
    private Decimal m_amount_total_balance;
    private long m_play_session_id;
    private Boolean m_forced;

    #endregion

    #region "Properties"
    public Int64 GameId
    {
      get { return m_game_id; }
      set { m_game_id = value; }
    }

    public Int64 GameType
    {
      get { return m_game_type; }
      set { m_game_type = value; }
    }

    public string TerminalGameUrl
    {
      get { return m_terminal_game_url; }
      set { m_terminal_game_url = value; }
    }

    public string ResultWinnerScreenMessageLine0
    {
      get { return m_result_winner_screen_message_line_0; }
      set { m_result_winner_screen_message_line_0 = value; }
    }

    public string ResultWinnerScreenMessageLine1
    {
      get { return m_result_winner_screen_message_line_1; }
      set { m_result_winner_screen_message_line_1 = value; }
    }

    public string ResultWinnerScreenMessageLine2
    {
      get { return m_result_winner_screen_message_line_2; }
      set { m_result_winner_screen_message_line_2 = value; }
    }

    public string ResultWinnerScreenMessageLine3
    {
      get { return m_result_winner_screen_message_line_3; }
      set { m_result_winner_screen_message_line_3 = value; }
    }

    public string ResultLooserScreenMessageLine0
    {
      get { return m_result_looser_screen_message_line_0; }
      set { m_result_looser_screen_message_line_0 = value; }
    }

    public string ResultLooserScreenMessageLine1
    {
      get { return m_result_looser_screen_message_line_1; }
      set { m_result_looser_screen_message_line_1 = value; }
    }

    public string ResultLooserScreenMessageLine2
    {
      get { return m_result_looser_screen_message_line_2; }
      set { m_result_looser_screen_message_line_2 = value; }
    }

    public string ResultLooserScreenMessageLine3
    {
      get { return m_result_looser_screen_message_line_3; }
      set { m_result_looser_screen_message_line_3 = value; }
    }

    public string TerminalGameName
    {
      get { return m_terminal_game_name; }
      set { m_terminal_game_name = value; }
    }

    public Boolean IsWinner
    {
      get { return m_is_winner; }
      set { m_is_winner = value; }
    }

    public Decimal AmountRePlayedDraw
    {
      get { return m_amount_re_played_draw; }
      set { m_amount_re_played_draw = value; }
    }

    public Decimal AmountRePlayedWon
    {
      get { return m_amount_re_played_won; }
      set { m_amount_re_played_won = value; }
    }

    public Decimal AmountNrPlayedDraw
    {
      get { return m_amount_nr_played_draw; }
      set { m_amount_nr_played_draw = value; }
    }

    public Decimal AmountNrPlayedWon
    {
      get { return m_amount_nr_played_won; }
      set { m_amount_nr_played_won = value; }
    }

    public Int64 AmountPointsPlayedDraw
    {
      get { return m_amount_points_played_draw; }
      set { m_amount_points_played_draw = value; }
    }

    public Int64 AmountPointsPlayedWon
    {
      get { return m_amount_points_played_won; }
      set { m_amount_points_played_won = value; }
    }
    public Int64 TerminalGameTimeOut
    {
      get { return m_terminal_game_time_out; }
      set { m_terminal_game_time_out = value; }
    }
    public Decimal AmountTotalBalance
    {
      get { return m_amount_total_balance; }
      set { m_amount_total_balance = value; }
    }

    public long PlaySessionId
    {
      get { return m_play_session_id; }
      set { m_play_session_id = value; }
    }

    public Boolean Forced
    {
      get { return m_forced; }
      set { m_forced = value; }
    }

    #endregion

    #region "Constructor"
    public TerminalDrawPrizePlanDetail()
    {
      TerminalGameUrl = String.Empty;
      TerminalGameName = String.Empty;

      //Winner 
      ResultWinnerScreenMessageLine0 = String.Empty;
      ResultWinnerScreenMessageLine1 = String.Empty;
      ResultWinnerScreenMessageLine2 = String.Empty;
      ResultWinnerScreenMessageLine3 = String.Empty;

      //Looser
      ResultLooserScreenMessageLine0 = String.Empty;
      ResultLooserScreenMessageLine1 = String.Empty;
      ResultLooserScreenMessageLine2 = String.Empty;
      ResultLooserScreenMessageLine3 = String.Empty;
    }

    #endregion
  }
  #endregion 
}
