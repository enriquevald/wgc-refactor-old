﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalDraw.cs
// 
//   DESCRIPTION: Class to calculate  TerminalDraw
// 
//        AUTHOR: Fernando Jimenez
// 
// CREATION DATE: 23-JAN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-JAN-2017 FJC    Initial version
// 21-MAR-2017 FJC    Fixed Bug 25947:Sorteo de Máquina. Error al ejecutar sorteo con transferencia a otra cuenta.
// 30-MAY-2017 FJC    PBI 27761:Sorteo de máquina. Modificiones en la visualización de las pantallas (LCD y InTouch)). 
// 29-AUG-2017 FJC    WIGOS-4771 Terminal Draw. Include GameId for each recharge
// 28-SEP-2017 DPC    WIGOS-5357: [Ticket #5272] Cashier closing operation : Ticket not show correctly the correct concepts in E/S
// 08-NOV-2017 AMF    PBI 30550:[WIGOS-5561] Codere EGM Draw
// 08-NOV-2017 MS     PBI 30550:[WIGOS-5561] Codere EGM Draw
// 13-NOV-2017 MS     PBI 30550:[WIGOS-6394] Codere EGM Draw - Create draw voucher
// 13-DIC-2017 FJC    Fixed Bug: WIGOS-7158 Terminal Draw - draw doesn't appear in LCD InTouch (Url)
// 06-FEB-2018 DHA    Bug 31411:WIGOS-8056 Application crashes when leaving empty a configuration field in Terminal Draw screen
// 13-FEB-2018 FJC    Fixed Bug WIGOS-8056 Application crashes when leaving empty a configuration field in Terminal Draw screen
// 16-FEB-2018 FJC    WIGOS-8082 [Ticket #12237] Sorteo en Terminal

// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common.TerminalDraw.Persistence;
using WSI.Common.ExtentionMethods;
using WSI.Common.InTouch;
using System.Collections;

namespace WSI.Common.TerminalDraw
{
  public class TerminalDraw
  {
    public enum StatusTerminalDraw
    {
      PENDING = 0,
      PROCESSING = 1,
      DRAWN = 2
    }


    #region "Constants"

    public const Int64 WELCOME_DRAW_ID = 1;

    #endregion

    #region "members"

    #endregion

    /// <summary>
    /// Get minimum prize in TerminalDraw
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="GameId"></param>
    /// <returns></returns>
    public static Boolean GetMinimumPrizeTerminalDraw(Int64 AccountId, Int64 GameId, out Currency Price, out Currency MinimumPrize)
    {
      TerminalDrawAccountEntity _recharge_list;
      Decimal _total_charge_draw;

      Price = 0;
      MinimumPrize = 0;

      if (!TerminalDraw.GetPendingTerminalDrawRechargesByAccount(AccountId, GameId, out _recharge_list))
      {
        Log.Error(String.Format("TerminalDraw Error: GetTerminalDrawRechargesByAccount(): AccountId: {0} ", AccountId));

        return false;
      }

      // Calculate total charge draw
      if (!CalculateTotalChargeDraw(_recharge_list.Games, out _total_charge_draw))
      {
        Log.Error("TerminalDraw Error: CalculateTotalChargeDraw()");

        return false;
      }

      Price = _total_charge_draw;
      MinimumPrize = _recharge_list.Games.TotalMinimumPrize();

      return true;
    } // GetMinimumPrizeTerminalDraw

    /// <summary>
    /// Generate a Terminal Draw Recharge
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="AccountId"></param>
    /// <param name="SplitA"></param>
    /// <param name="SplitB"></param>
    /// <param name="RechargeErrorCode"></param>
    /// <param name="ErrorMessage"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean GenerateTerminalDrawRecharge(Int64 OperationId,
                                                       Int64 AccountId,
                                                       Currency SplitA,
                                                       Currency SplitB,
                                                       out RECHARGE_ERROR_CODE RechargeErrorCode,
                                                       out String ErrorMessage,
                                                       SqlTransaction Trx)
    {
      return GenerateTerminalDrawRecharge(OperationId, AccountId, SplitA, SplitB, String.Empty, WELCOME_DRAW_ID, 0, 0, out RechargeErrorCode, out ErrorMessage, Trx);
    } // GenerateTerminalDrawRecharge
    public static Boolean GenerateTerminalDrawRecharge(Int64 OperationId,
                                                       Int64 AccountId,
                                                       Currency SplitA,
                                                       Currency SplitB,
                                                       Int64 GameId,
                                                       Currency PromotionAmount,
                                                       PromoGame.GameType GameType,
                                                       SqlTransaction Trx)
    {
      RECHARGE_ERROR_CODE _recharge_error_code;
      String _error_message;

      return GenerateTerminalDrawRecharge(OperationId, AccountId, SplitA, SplitB, String.Empty, GameId, PromotionAmount, GameType, out _recharge_error_code, out _error_message, Trx);
    } // GenerateTerminalDrawRecharge
    public static Boolean GenerateTerminalDrawRecharge(Int64 OperationId,
                                                       Int64 AccountId,
                                                       Currency SplitA,
                                                       Currency SplitB,
                                                       String PyramizationXml,
                                                       Int64 GameId,
                                                       Currency PromotionAmount,
                                                       PromoGame.GameType GameType,
                                                       out RECHARGE_ERROR_CODE RechargeErrorCode,
                                                       out String ErrorMessage,
                                                       SqlTransaction Trx)
    {
      TerminalDrawAccountEntity _terminal_draw_recharge;
      RechargeData _recharge_data;
      PromoGame _game;
      Currency _total_amount_cash_in;
      Currency _draw_prize_re;
      Currency _draw_prize_nr;

      RechargeErrorCode = RECHARGE_ERROR_CODE.ERROR_GENERIC;
      ErrorMessage = String.Empty;
      _terminal_draw_recharge = new TerminalDrawAccountEntity();
      _recharge_data = new RechargeData();

      try
      {
        // Get Promogame
        _game = new PromoGame(GameId);

        #region " Prize Calculation Info "

        /////////////////////////////  PRIZE CALCULATION  ////////////////////////////
        //                                                                          //
        //  The prize of draws is calculated form table TERMINAL_DRAWS_RECHARGES:   //
        //    Calculation = (TDR_TOTAL_CASH_IN - TDR_NR_BET)                        //
        //                                                                          //
        //    WELCOME_DRAW EXAMPLE:                                                 //
        //      - TDR_TOTAL_CASH_IN is total amount recharged                       //
        //        · SplitA + SplitB                                                 //
        //      - TDR_NR_BET is total SplitA                                        //
        //        · SplitB                                                          //
        //      - TOTAL PRIZE = (SplitA + SplitB) - SplitB                          //
        //                                                                          //
        //    PROMO_GAME EXAMPLE:                                                   //
        //      - TDR_TOTAL_CASH_IN is total amount recharged                       //
        //        · SplitA + SplitB                                                 //
        //      - TDR_NR_BET is total PromotionAmount                               //
        //        · PromotionAmount                                                 //
        //      - TOTAL PRIZE = (SplitA + SplitB) - PromotionAmount                 //
        //                                                                          //
        //////////////////////////////////////////////////////////////////////////////

        #endregion

        // Total Amount CashIn
        if (_game.Type == PromoGame.GameType.Welcome)
        {
          GetDrawAmounts_WelcomeDraw(SplitA, SplitB, out _total_amount_cash_in, out _draw_prize_re, out _draw_prize_nr);
        }
        else
        {          
          GetDrawAmounts_PromoGame(PromotionAmount, _game.ReturnUnits, PyramizationXml, out _total_amount_cash_in, out _draw_prize_re, out _draw_prize_nr);
        }

        //Add Recharge Data
        _recharge_data.AccountId = AccountId;
        _recharge_data.OperationId = OperationId;
        _recharge_data.GameId = GameId;
        _recharge_data.GameType = (Int64)GameType;

        // Set amounts
        _recharge_data.AmountTotalCashIn = _total_amount_cash_in;
        _recharge_data.AmountReBet = _draw_prize_re;
        _recharge_data.AmountNrBet = _draw_prize_nr;

        //Insert Recharge
        _terminal_draw_recharge.AddRecharge(_recharge_data);

        // insert draw in local
        if (!DB_InsertTerminalDrawRecharge(_terminal_draw_recharge, OperationId, Trx))
        {
          Log.Error("SaveDraw failed!");
          RechargeErrorCode = RECHARGE_ERROR_CODE.ERROR_INSERT_CASH_DESK_DRAW_DB;
          ErrorMessage = "SaveDraw failed!";

          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return true;
    } // GenerateTerminalDrawRecharge

    /// <summary>
    /// Get Draw Amounts of WelcomeDraw
    /// </summary>
    /// <param name="SplitA"></param>
    /// <param name="SplitB"></param>
    /// <param name="_total_amount_cash_in"></param>
    /// <param name="_draw_prize_re"></param>
    /// <param name="_draw_prize_nr"></param>
    private static void GetDrawAmounts_WelcomeDraw(Currency SplitA, Currency SplitB, out Currency TotalAmountCashIn, out Currency DrawPrizeRE, out Currency DrawPrizeNR)
    {
      TotalAmountCashIn = (SplitA + SplitB);
      DrawPrizeRE = (TotalAmountCashIn - SplitB);
      DrawPrizeNR = 0;
    } // GetDrawAmounts_WelcomeDraw

    /// <summary>
    /// Get Draw Amounts of PromoGames
    /// </summary>
    /// <param name="SplitA"></param>
    /// <param name="SplitB"></param>
    /// <param name="TotalAmountCashIn"></param>
    /// <param name="DrawPrizeRE"></param>
    /// <param name="DrawPrizeNR"></param>
    private static void GetDrawAmounts_PromoGame(Currency PromotionAmount, PrizeType PrizeType, String PyramizationXml, out Currency TotalAmountCashIn, out Currency DrawPrizeRE, out Currency DrawPrizeNR)
    {
      Pyramization _pyramization;

      TotalAmountCashIn = 0;

      DrawPrizeRE = 0;
      DrawPrizeNR = 0;

      if (GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", false)
          && GeneralParam.GetBoolean("Features", "PyramidalDistributionReward", false)
            && !PyramizationXml.Equals(String.Empty))
      {
        _pyramization = new Pyramization(PromotionAmount, PyramizationXml);
        PromotionAmount += _pyramization.Calculate();
      }           

      switch (PrizeType)
      {
        case PrizeType.Redeemable:
          DrawPrizeRE = PromotionAmount;
          break;

        case PrizeType.NoRedeemable:
          DrawPrizeNR = PromotionAmount;
          break;

        default:
          Log.Warning("GetDrawAmounts_PromoGame(): Invalid PrizeType.");
          break;
      }

    } // GetDrawAmounts_PromoGame

    /// <summary>
    /// Get pending Terminal Draw Recharges by AccountId
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="TerminalDraw"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean GetPendingTerminalDrawRechargesByAccount(Int64 AccountId, Int64 GameId, out TerminalDrawAccountEntity TerminalDrawRechargeList)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetPendingTerminalDrawRechargesByAccount(AccountId, GameId, out TerminalDrawRechargeList, _db_trx.SqlTransaction);
      }
    } // GetPendingTerminalDrawRechargesByAccount
    public static Boolean GetPendingTerminalDrawRechargesByAccount(Int64 AccountId,
                                                                    Int64 GameId,
                                                                    out TerminalDrawAccountEntity TerminalDrawRechargeList,
                                                                    SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _dt_pending_draws;
      RechargeData _recharge_data;
      Int64 _last_game_type;

      TerminalDrawRechargeList = new TerminalDrawAccountEntity();
      _dt_pending_draws = new DataTable();
      _sb = new StringBuilder();
      _recharge_data = new RechargeData();

      try
      {
        // 1. Filter by Game id
        _sb.AppendLine("   DECLARE  @pGameId AS BIGINT                        ");

        if (GameId > 0)
        {
          _sb.AppendLine(String.Format("   SET      @pGameId = {0}   ", GameId));
        }
        else
        {
          _sb.AppendLine("    SELECT  TOP(1) @pGameId = TDR_GAME_ID           ");
          _sb.AppendLine("      FROM  TERMINAL_DRAWS_RECHARGES                ");
          _sb.AppendLine("     WHERE  TDR_ACCOUNT_ID  = @pAccountId           ");
          _sb.AppendLine("  ORDER BY  TDR_GAME_ID                             ");
        }

        _sb.AppendLine("SELECT TOP (@pNumberOfDraws) TDR_ACCOUNT_ID             ");
        _sb.AppendLine("           , TDR_GAME_ID                              ");
        _sb.AppendLine("           , TDR_OPERATION_ID                         ");
        _sb.AppendLine("           , TDR_TOTAL_CASH_IN      AS TOTAL_CASH_IN  ");
        _sb.AppendLine("           , TDR_RE_BET             AS RE_BET         ");
        _sb.AppendLine("           , TDR_NR_BET             AS NR_BET         ");
        _sb.AppendLine("           , TDR_POINTS_BET         AS POINTS_BET     ");
        _sb.AppendLine("           , TDR_GAME_TYPE          AS GAME_TYPE      ");
        _sb.AppendLine("    FROM     TERMINAL_DRAWS_RECHARGES                 ");
        _sb.AppendLine("   WHERE     TDR_ACCOUNT_ID =  @pAccountId            ");
        _sb.AppendLine("   AND       TDR_STATUS     =  @pStatus               ");
        _sb.AppendLine("   AND       TDR_GAME_ID    =  @pGameId               ");
        _sb.AppendLine("ORDER BY     TDR_ACCOUNT_ID                           ");
        _sb.AppendLine("           , TDR_GAME_ID                              ");
        _sb.AppendLine("           , TDR_GAME_TYPE                            ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = StatusTerminalDraw.PENDING;
          _sql_command.Parameters.Add("@pNumberOfDraws", SqlDbType.Int).Value = GetTerminalDrawGroups();

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_command))
          {
            _sql_da.Fill(_dt_pending_draws);
          }

          _last_game_type = -1;

          foreach (DataRow _dr in _dt_pending_draws.Rows)
          {
            if (_dr["RE_BET"] != DBNull.Value)
            {
              _recharge_data.AmountTotalCashIn = (_dr["TOTAL_CASH_IN"] == DBNull.Value) ? 0 : (Decimal)_dr["TOTAL_CASH_IN"];
              _recharge_data.AmountReBet = (_dr["RE_BET"] == DBNull.Value) ? 0 : (Decimal)_dr["RE_BET"];
              _recharge_data.AmountNrBet = (_dr["NR_BET"] == DBNull.Value) ? 0 : (Decimal)_dr["NR_BET"];
              _recharge_data.AmountPointsBet = (_dr["POINTS_BET"] == DBNull.Value) ? 0 : (Int64)_dr["POINTS_BET"];
            }

            _recharge_data.AccountId = AccountId;
            _recharge_data.TerminalId = 0;
            _recharge_data.OperationId = (Int64)_dr["TDR_OPERATION_ID"];
            _recharge_data.GameId = (Int64)_dr["TDR_GAME_ID"];
            _recharge_data.GameType = (Int64)_dr["GAME_TYPE"];

            // In this block we check if a PromoGame applies to a PlayCash Draw or a PlayPoint Draw 
            if (_last_game_type < 0 || _last_game_type == _recharge_data.GameType)
            {
              TerminalDrawRechargeList.AddRecharge(_recharge_data);
            }

            _last_game_type = _recharge_data.GameType;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // GetPendingTerminalDrawRechargesByAccount

    /// <summary>
    /// Process Pending TerminalDraw Recharges for TerminalDraw Execution
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="TerminalDraw"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean ProcessPendingTerminalDrawRechargesByAccount(Int64 AccountId,
                                                                        Int64 GameId,
                                                                        out TerminalDrawAccountEntity TerminalDrawRechargeList,
                                                                        SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _dt_pending_draws;

      TerminalDrawRechargeList = new TerminalDrawAccountEntity();
      RechargeData _recharge_data;
      _dt_pending_draws = new DataTable();
      _sb = new StringBuilder();
      _recharge_data = new RechargeData();

      try
      {
        // 1. Filter by Game id
        _sb.AppendLine("   DECLARE  @pGameId AS BIGINT                        ");

        if (GameId > 0)
        {
          _sb.AppendLine(String.Format("   SET      @pGameId = {0}   ", GameId));
        }
        else
        {
          _sb.AppendLine("    SELECT  TOP(1) @pGameId = TDR_GAME_ID     ");
          _sb.AppendLine("      FROM  TERMINAL_DRAWS_RECHARGES          ");
          _sb.AppendLine("     WHERE  TDR_ACCOUNT_ID = @pAccountId      ");
          _sb.AppendLine("  ORDER BY  TDR_GAME_ID                       ");
          _sb.AppendLine("          , TDR_GAME_TYPE                     ");
        }

        // 2. Temp Table
        _sb.AppendLine(" DECLARE @temp_terminalDraw                     ");
        _sb.AppendLine("   TABLE (                                      ");
        _sb.AppendLine("          TDR_ID            BIGINT NOT NULL,    ");
        _sb.AppendLine("          TDR_ACCOUNT_ID    BIGINT NOT NULL,    ");
        _sb.AppendLine("          TDR_GAME_ID       BIGINT NOT NULL,    ");
        _sb.AppendLine("          TDR_GAME_TYPE     BIGINT NOT NULL,    ");
        _sb.AppendLine("          TDR_OPERATION_ID  BIGINT NOT NULL,    ");
        _sb.AppendLine("          TDR_TOTAL_CASH_IN MONEY  NULL,        ");
        _sb.AppendLine("          TDR_RE_BET        MONEY  NULL,        ");
        _sb.AppendLine("          TDR_NR_BET        MONEY  NULL,        ");
        _sb.AppendLine("          TDR_POINTS_BET    MONEY  NULL)        ");

        // 3. Update Output
        _sb.AppendLine("  UPDATE  TOP (@pNumberOfDraws)  TERMINAL_DRAWS_RECHARGES      ");
        _sb.AppendLine("     SET   TDR_STATUS = @pStatus                ");
        _sb.AppendLine("  OUTPUT   INSERTED.TDR_ID                      ");
        _sb.AppendLine("         , INSERTED.TDR_ACCOUNT_ID              ");
        _sb.AppendLine("         , INSERTED.TDR_GAME_ID                 ");
        _sb.AppendLine("         , INSERTED.TDR_GAME_TYPE               ");
        _sb.AppendLine("         , INSERTED.TDR_OPERATION_ID            ");
        _sb.AppendLine("         , INSERTED.TDR_TOTAL_CASH_IN           ");
        _sb.AppendLine("         , INSERTED.TDR_RE_BET                  ");
        _sb.AppendLine("         , INSERTED.TDR_NR_BET                  ");
        _sb.AppendLine("         , INSERTED.TDR_POINTS_BET              ");
        _sb.AppendLine("    INTO   @temp_terminalDraw                   ");
        _sb.AppendLine("   WHERE   TDR_ACCOUNT_ID   = @pAccountId       ");
        _sb.AppendLine("     AND   TDR_GAME_ID      = @pGameId          ");

        // 4. Select 
        _sb.AppendLine("SELECT TOP (@pNumberOfDraws)                     ");
        _sb.AppendLine("             TDR_ID                              ");
        _sb.AppendLine("           , TDR_ACCOUNT_ID                      ");
        _sb.AppendLine("           , TDR_GAME_ID                         ");
        _sb.AppendLine("           , TDR_GAME_TYPE                       ");
        _sb.AppendLine("           , TDR_OPERATION_ID                    ");
        _sb.AppendLine("           , TDR_TOTAL_CASH_IN AS TOTAL_CASH_IN  ");
        _sb.AppendLine("           , TDR_RE_BET        AS RE_BET         ");
        _sb.AppendLine("           , TDR_NR_BET        AS NR_BET         ");
        _sb.AppendLine("           , TDR_POINTS_BET    AS POINTS_BET     ");
        _sb.AppendLine("    FROM     @temp_terminalDraw                  ");
        _sb.AppendLine("ORDER BY     TDR_OPERATION_ID ASC                ");
        _sb.AppendLine("           , TDR_ACCOUNT_ID                      ");
        _sb.AppendLine("           , TDR_GAME_ID                         ");
        _sb.AppendLine("           , TDR_GAME_TYPE                       ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = StatusTerminalDraw.PROCESSING;
          _sql_command.Parameters.Add("@pNumberOfDraws", SqlDbType.Int).Value = GetTerminalDrawGroups();

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_command))
          {
            _sql_da.Fill(_dt_pending_draws);
          }

          foreach (DataRow _dr in _dt_pending_draws.Rows)
          {
            if (_dr["RE_BET"] != DBNull.Value)
            {
              _recharge_data.AmountTotalCashIn = (_dr["TOTAL_CASH_IN"] != DBNull.Value) ? (Decimal)_dr["TOTAL_CASH_IN"] : 0;
              _recharge_data.AmountReBet = (_dr["RE_BET"] != DBNull.Value) ? (Decimal)_dr["RE_BET"] : 0;
              _recharge_data.AmountNrBet = (_dr["NR_BET"] != DBNull.Value) ? (Decimal)_dr["NR_BET"] : 0;
            }

            _recharge_data.TerminalId = 0;
            _recharge_data.AccountId = AccountId;
            _recharge_data.TdrId = (Int64)_dr["TDR_ID"];
            _recharge_data.OperationId = (Int64)_dr["TDR_OPERATION_ID"];
            _recharge_data.GameId = (Int64)_dr["TDR_GAME_ID"];
            _recharge_data.GameType = (Int64)_dr["TDR_GAME_TYPE"];

            TerminalDrawRechargeList.AddRecharge(_recharge_data);
          }

          if (!MultiPromos.Trx_ResetCancellableOperation(AccountId, Trx))
          {
            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // ProcessPendingTerminalDrawRechargesByAccount

    /// <summary>
    /// Delete terminal draw recharge by account id
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Status"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_DeleteTerminalDrawRechargeByAccountId(Int64 AccountId, StatusTerminalDraw Status, SqlTransaction Trx)
    {
      return DB_DeleteTerminalDrawRechargeByAccountId(AccountId, Status, false, Trx);
    } // DB_DeleteTerminalDrawRechargeByAccountId

    /// <summary>
    /// Delete terminal draw recharge by account id
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Status"></param>
    /// <param name="CheckDeleted"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_DeleteTerminalDrawRechargeByAccountId(Int64 AccountId, StatusTerminalDraw Status, Boolean CheckDeleted, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int64 _num_modified;

      _num_modified = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" DELETE                                 ");
        _sb.AppendLine("   FROM   TERMINAL_DRAWS_RECHARGES      ");
        _sb.AppendLine("  WHERE   TDR_ACCOUNT_ID = @pAccountId  ");
        _sb.AppendLine("    AND   TDR_STATUS     = @pStatus     ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

          _num_modified = _sql_command.ExecuteNonQuery();

          if ((CheckDeleted && _num_modified > 0) || (!CheckDeleted))
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_DeleteTerminalDrawRechargeByAccountId

    /// <summary>
    /// Deletes TerminalDraw Recharges by OperationId
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_DeleteTerminalDrawRechargeByOperationId(Int64 OperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" DELETE                                     ");
        _sb.AppendLine("   FROM   TERMINAL_DRAWS_RECHARGES          ");
        _sb.AppendLine("  WHERE   TDR_OPERATION_ID  = @pOperationId ");
        _sb.AppendLine("    AND   TDR_STATUS        = @pStatus      ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pOperationId", SqlDbType.Int).Value = OperationId;
          _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = StatusTerminalDraw.PENDING;

          _sql_command.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_DeleteTerminalDrawRechargeByOperationId

    /// <summary>
    /// Deletes Terminal Draw Recharge by Id
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_DeleteTerminalDrawRechargeById(Int64 TdrId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" DELETE                               ");
        _sb.AppendLine("   FROM   TERMINAL_DRAWS_RECHARGES    ");
        _sb.AppendLine("  WHERE   TDR_ID            = @pTdrId ");


        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pTdrId", SqlDbType.BigInt).Value = TdrId;

          if (_sql_command.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //DB_DeleteTerminalDrawRechargeByOperationId

    /// <summary>
    /// Update Account_promotion to bough
    /// </summary>
    /// <param name="OperationID"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean UpdatePromotionStatus(List<Int64> OperationsId, ACCOUNT_PROMO_STATUS PromoType, PromoGame.GameType GameType, SqlTransaction SqlTrx)
    {
      // Check if is necessary update account promotion
      if (!MustUpdateAccountPromotionStatus(PromoType, GameType))
      {
        return true;
      }

      // Update all operations
      foreach (Int64 _operation_id in OperationsId)
      {
        // Update AccountPromotion to Bought
        if (!DB_UpdatePromotionStatus(_operation_id, PromoType, SqlTrx))
        {
          Log.Error(String.Format("TerminalDraw Error: UpdatePromotionStatus(). Operation Id: {0}.", _operation_id));

          return false;
        }
      }

      return true;
    } // UpdatePromotionStatus

    /// <summary>
    /// Check if there's enough credit for terminal draw
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="GamesRecharge"></param>
    /// <param name="ChargeDraw"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean CheckIfEnoughBalanceForTerminalDraw(Int64 AccountId, List<TerminalDrawGamesEntity> Games, out Decimal ChargeDraw, SqlTransaction Trx)
    {
      MultiPromos.AccountBalance _account_balance;
      Decimal _total_charge_draw;

      _total_charge_draw = 0;
      ChargeDraw = 0;

      try
      {
        if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _account_balance, Trx))
        {
          Log.Error(String.Format("TerminalDraw Error. CheckIfEnoughBalanceForTerminalDraw(): Trx_UpdateAccountBalance(): AccountId: {1} ", AccountId));

          return false;
        }

        // Calculate total charge draw
        if (!CalculateTotalChargeDraw(Games, out _total_charge_draw))
        {
          Log.Error("TerminalDraw Error. CheckIfEnoughBalanceForTerminalDraw(): CalculateTotalChargeDraw()");

          return false;
        }

        ChargeDraw = _total_charge_draw;

        if (_account_balance.TotalRedeemable >= ChargeDraw)
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CheckIfEnoughBalanceForTerminalDraw

    /// <summary
    /// GetTerminalDrawRechargesAmountByOperationId
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="Amount"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_GetTerminalDrawRechargesAmountByOperationId(Int64 OperationId, out Currency Amount, SqlTransaction Trx)
    {
      StringBuilder _sb;

      Amount = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TDR_TOTAL_CASH_IN                 ");
        _sb.AppendLine("   FROM   TERMINAL_DRAWS_RECHARGES          ");
        _sb.AppendLine("  WHERE   TDR_OPERATION_ID  = @pOperationId ");
        _sb.AppendLine("    AND   TDR_STATUS        = @pStatus      ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pOperationId", SqlDbType.Int).Value = OperationId;
          _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = StatusTerminalDraw.PENDING;

          using (SqlDataReader _reader = _sql_command.ExecuteReader())
          {
            if (_reader.Read() && (!_reader.IsDBNull(0)))
            {
              Amount = (Currency)_reader.GetSqlMoney(0);
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }// DB_GetTerminalDrawRechargesAmountByOperationId

    /// <summary>
    /// TerminalDraw get pending draw
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="InternalId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean TerminalDrawGetPendingDraw(long AccountId, Int32 InternalId, Int64 GameId, out TerminalDrawAccountEntity AccountEntity)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (TerminalDrawGetPendingDraw(AccountId, InternalId, GameId, out AccountEntity, _db_trx.SqlTransaction))
        {
          return _db_trx.Commit();
        }
      }

      return false;
    } // TerminalDrawGetPendingDraw
    public static Boolean TerminalDrawGetPendingDraw(long AccountId, Int32 InternalId, out TerminalDrawAccountEntity AccountEntity, SqlTransaction SqlTrx)
    {
      return TerminalDrawGetPendingDraw(AccountId, InternalId, 0, out AccountEntity, SqlTrx);
    }
    public static Boolean TerminalDrawGetPendingDraw(long AccountId, Int32 InternalId, Int64 GameId, out TerminalDrawAccountEntity AccountEntity, SqlTransaction SqlTrx)
    {
      TerminalDrawAccountEntity _terminal_draw_recharge;
      Int64 _play_session_id;
      Int64 _terminal_id_out;
      CashDeskDraw _draw;
      Decimal _charge_draw;
      MultiPromos.AccountBalance _account_balance;
      Boolean _close_play_sessions_before_new_game;
      Int64 _game_id;

      _charge_draw = 0;
      AccountEntity = new TerminalDrawAccountEntity();
      _terminal_draw_recharge = new TerminalDrawAccountEntity();
      _close_play_sessions_before_new_game = GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "CloseOpenedSessionsBeforeNewGame", false);

      // Check if terminal draw is enabled
      if (!Misc.IsTerminalDrawEnabled())
      {
        return true;
      }

      if (!TerminalDraw.GetPendingTerminalDrawRechargesByAccount(AccountId, GameId, out _terminal_draw_recharge, SqlTrx))
      {
        Log.Error(String.Format("TerminalDraw Error: GetTerminalDrawRechargesByAccount(): AccountId: {0} ", AccountId));

        return false;
      }

      // Check account entity
      if (!CheckAccountEntity(_terminal_draw_recharge))
      {
        Log.Error("TerminalDraw Error: GetTerminalDrawRechargesByAccount(): Error on get Terminal Draw Recharges");

        return true;
      }

      // Set GameId
      _game_id = _terminal_draw_recharge.Games.Id();

      if (_terminal_draw_recharge.Games.Count > 0
        && TerminalDraw.CheckIfEnoughBalanceForTerminalDraw(AccountId, _terminal_draw_recharge.Games, out _charge_draw, SqlTrx))
      {
        AccountEntity = _terminal_draw_recharge;

        _draw = new CashDeskDraw((Int16)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02, _game_id);
        _draw.Parameters = new Parameters(_game_id);
        _draw.Result = new Result();
        _draw.Parameters.m_terminal_id = InternalId;
        _draw.Parameters.m_account_id = AccountId;
        _draw.Parameters.m_force_open_session = true;

        // Update Account Balance
        if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _account_balance, SqlTrx))
        {
          Log.Error(String.Format("TerminalDraw Error: Trx_UpdateAccountBalance(): TerminalId: {0}; AccountId: {1} ", InternalId, AccountId));

          return false;
        }

        // Close old PlaySessions
        if (_close_play_sessions_before_new_game && !CashDeskDrawBusinessLogic.Trx_CloseOpenPlaySessions(InternalId, SqlTrx))
        {
          Log.Error(String.Format("TerminalDraw Error: -WCP_MsgTerminalDrawGetPendingDraw- Trx_CloseOpenPlaySessions(): AccountId: {0}, TerminalId: {1} ", AccountId, InternalId));

          return false;
        }

        // Start PlaySession
        if (!CashDeskDrawBusinessLogic.Trx_InsertPlaySession(_draw, out _play_session_id, out _terminal_id_out, CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02, SqlTrx))
        {
          Log.Error(String.Format("TerminalDraw Error: -WCP_MsgTerminalDrawGetPendingDraw- Trx_InsertPlaySession(): AccountId: {0}, TerminalId: {1} ", AccountId, InternalId));

          return false;
        }

        PromoGameTypePlaySession _promogame_playSession = new PromoGameTypePlaySession();
        _promogame_playSession.PlaySessionId = _play_session_id;
        _promogame_playSession.GameType = _terminal_draw_recharge.Games.Type();

        if (!_promogame_playSession.Insert())
        {
          Log.Error(String.Format("TerminalDraw Error: -WCP_MsgTerminalDrawGetPendingDraw- Trx_InsertPlaySession(): AccountId: {0}, TerminalId: {1} ", AccountId, InternalId));
        }
        

        // Set Screen Messages (4 lines mode)
        SetAccountEntityParams_ScreenMessages(ref AccountEntity);

        // Set Account Params
        if (_game_id == WELCOME_DRAW_ID)
        {
          SetAccountEntityParams_WelcomeDraw(ref AccountEntity, _play_session_id, _account_balance.TotalBalance, _charge_draw);
        }
        else
        {
          SetAccountentityParams_PromoGame(ref AccountEntity, _game_id, _play_session_id, _account_balance.TotalBalance, _charge_draw);
        }
      }

      return true;
    } // TerminalDrawGetPendingDraw

    /// <summary>
    /// function to set status promotion bought (Only Start Card Session)
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="InternalId"></param>
    /// <param name="GameId"></param>
    /// <returns></returns>
    public static Boolean TerminalDrawSetPromotionStatusToBought(Int64 AccountId, Int32 InternalId, Int64 GameId)
    {
      TerminalDrawAccountEntity _account_entity;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (TerminalDrawSetPromotionStatusToBought(AccountId, InternalId, GameId, out _account_entity, _db_trx.SqlTransaction))
        {
          return _db_trx.Commit();
        }
      }

      return false;
    } // TerminalDrawSetPromotionStatusToBought
    public static Boolean TerminalDrawSetPromotionStatusToBought(Int64 AccountId, Int32 InternalId, Int64 GameId, out TerminalDrawAccountEntity AccountEntity, SqlTransaction SqlTrx)
    {
      AccountEntity = new TerminalDrawAccountEntity();

      // Only for PromoGames
      if (GameId <= WELCOME_DRAW_ID)
      {
        return true;
      }

      // Get AccountEntity
      if (!GetPendingTerminalDrawRechargesByAccount(AccountId, GameId, out AccountEntity, SqlTrx))
      {
        Log.Error(String.Format("TerminalDraw Error: GetTerminalDrawRechargesByAccount(): AccountId: {0} ", AccountId));

        return false;
      }

      // Check AccountEntity
      if (!CheckAccountEntity(AccountEntity))
      {
        Log.Error("TerminalDraw Error: Not CheckAccountEntity().");

        return false;
      }

      // Update account promotion status to bought
      if (!UpdatePromotionStatus(AccountEntity.Games.OperationsId(), ACCOUNT_PROMO_STATUS.BOUGHT, AccountEntity.Games.Type(), SqlTrx))
      {
        Log.Error("TerminalDraw Error: Not UpdatePromotionStatus().");

        return false;
      }

      return true;

    } // TerminalDrawSetPromotionStatusToBought

    /// <summary>
    /// Check terminal draw account entity
    /// </summary>
    /// <param name="AccountEntity"></param>
    /// <returns></returns>
    public static Boolean CheckAccountEntity(TerminalDrawAccountEntity AccountEntity)
    {
      // Check account entity
      return (AccountEntity.Games != null && AccountEntity.Games.CheckOperationId());
    } // CheckAccountEntity

    /// <summary>
    /// TerminalDraw Proces draw
    /// </summary>
    /// <param name="InternalId"></param>
    /// <param name="AccountEntity"></param>
    /// <param name="PrizePlanGames"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean TerminalDrawProcessDraw(Int32 InternalId, TerminalDrawAccountEntity AccountEntity, Int64 GameId, out TerminalDrawPrizePlanGames PrizePlanGames)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (TerminalDrawProcessDraw(InternalId, AccountEntity, GameId, out PrizePlanGames, _db_trx.SqlTransaction))
        {
          return _db_trx.Commit();
        }
      }

      return false;
    } // TerminalDrawProcessDraw
    public static Boolean TerminalDrawProcessDraw(Int32 InternalId, TerminalDrawAccountEntity AccountEntity, out TerminalDrawPrizePlanGames PrizePlanGames, SqlTransaction SqlTrx)
    {
      return TerminalDrawProcessDraw(InternalId, AccountEntity, 0, out PrizePlanGames, SqlTrx);
    } // TerminalDrawProcessDraw
    public static Boolean TerminalDrawProcessDraw(Int32 InternalId, TerminalDrawAccountEntity AccountEntity, Int64 GameId, out TerminalDrawPrizePlanGames PrizePlanGames, SqlTransaction SqlTrx)
    {
      RechargeOutputParameters _output_parameters;
      RechargeInputParameters _input_parameters;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      DataTable _promotions;
      Boolean _processed;
      Currency _amount_re_played;
      Currency _amount_re_won;
      Currency _amount_nr_won;
      Boolean _is_winner;
      Boolean _loser_voucher;
      MultiPromos.AccountBalance _account_balance;
      Int64 _play_session_mahine_id;
      TerminalDrawAccountEntity _terminal_draw_recharge_list;
      PrizePlanData _prize_plan_data;
      Currency _split_a;
      Currency _split_b;
      PromoGame _game;

      PrizePlanGames = new TerminalDrawPrizePlanGames();

      try
      {
        _processed = false;
        _amount_re_played = 0;
        _amount_re_won = 0;
        _amount_nr_won = 0;
        _is_winner = false;
        _play_session_mahine_id = 0;
        _prize_plan_data = new PrizePlanData();

        // Get Pending Draws
        if (!TerminalDraw.ProcessPendingTerminalDrawRechargesByAccount(AccountEntity.AccountId, GameId, out _terminal_draw_recharge_list, SqlTrx))
        {
          Log.Error(String.Format("TerminalDraw Error: ProcessPendingTerminalDrawRechargesByAccount(): TerminalId: {0}; AccountId: {1} ", InternalId, AccountEntity.AccountId));

          return false;
        }

        //If gameId is 0 and _terminal_draw_recharge has games and recharges
        if (GameId == 0 && CheckAccountEntity(_terminal_draw_recharge_list))
        {
          GameId = _terminal_draw_recharge_list.Games.Id();
        }

        _play_session_mahine_id = AccountEntity.Games.PlaySessionId();

        foreach (TerminalDrawGamesEntity _games in _terminal_draw_recharge_list.Games)
        {
          foreach (TerminalDrawRechargeEntity _recharges in _games.Recharges)
          {
            _promotions = AccountPromotion.CreateAccountPromotionTable();
            _input_parameters = new RechargeInputParameters();
            _account_movements = new AccountMovementsTable(new CashierSessionInfo());
            _cashier_movements = new CashierMovementsTable(new CashierSessionInfo());
            _output_parameters = new RechargeOutputParameters();
            _input_parameters.AccountId = AccountEntity.AccountId;
            _input_parameters.CashDeskDraw = CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02;
            _input_parameters.TerminalId = InternalId;

            if (!_processed)
            {
              _input_parameters.PlaySessionId = _play_session_mahine_id;
              _play_session_mahine_id = 0;
            }
                      
            _game = new PromoGame(GameId);

            // Total Amount CashIn
            if (_game.Type == PromoGame.GameType.Welcome)
            {
              _split_a = _recharges.AmountReBet;
              _split_b = _recharges.AmountTotalCashIn - _recharges.AmountReBet;
            }
            else
            {
              _split_a = 0;
              _split_b = (_game.ReturnUnits == PrizeType.Redeemable) ? _recharges.AmountReBet : _recharges.AmountNrBet;
            }

            // 1. Execution Draw
            if (!CashDeskDrawBusinessLogic.ConditionallyCashDeskDraw(_recharges.OperationId,
                                                                      _input_parameters,
                                                                      _recharges.AmountTotalCashIn,
                                                                      _promotions,
                                                                      _account_movements,
                                                                      _cashier_movements,
                                                                      SqlTrx,
                                                                      _split_a,
                                                                      _split_b,
                                                                      0,
                                                                      0,
                                                                      null,
                                                                      out  _output_parameters,
                                                                      out _play_session_mahine_id))
            {
              Log.Error(String.Format("TerminalDraw Error: ConditionallyCashDeskDraw(): TerminalId: {0}; AccountId: {1} ", InternalId, AccountEntity.AccountId));

              return false;
            }

            if (!_account_movements.Save(SqlTrx))
            {
              Log.Error(String.Format("TerminalDraw Error: AccountMovements.Save(): TerminalId: {0}; AccountId: {1} ", InternalId, AccountEntity.AccountId));

              return false;
            }

            if (!AccountPromotion.InsertAccountPromotions(_promotions, false, SqlTrx))
            {
              Log.Error(String.Format("TerminalDraw Error: InsertAccountPromotions(): TerminalId: {0}; AccountId: {1} ", InternalId, AccountEntity.AccountId));

              return false;
            }

            _output_parameters.CoverCouponAccountPromotionId = GetAccountPromotionId(_promotions, "ACP_PROMO_TYPE = " + (Int32)Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON);
            _output_parameters.AccountPromotionId = GetAccountPromotionId(_promotions, "ACP_PROMO_TYPE <> " + (Int32)Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE +
                                                                               " AND ACP_PROMO_TYPE <> " + (Int32)Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON);

            // Set promotion status to redeemed
            if (!AccountPromotion.Trx_ActivatePromotionsOnAccount(_promotions, _account_movements, _cashier_movements, true, SqlTrx, _input_parameters.CashDeskDraw))
            {
              Log.Error(String.Format("TerminalDraw Error: Trx_ActivatePromotionsOnAccount(): TerminalId: {0}; AccountId: {1} ", InternalId, AccountEntity.AccountId));

              return false;

            }

            _amount_re_played += _output_parameters.CashDeskDrawResult.m_participation_amount;
            _amount_re_won += _output_parameters.CashDeskDrawResult.m_re_awarded;
            _amount_nr_won += _output_parameters.CashDeskDrawResult.m_nr_awarded;

            if (_output_parameters.CashDeskDrawResult.m_is_winner)
            {
              _is_winner = _output_parameters.CashDeskDrawResult.m_is_winner;
            }

            //Update status to redemeed 
            if (!TerminalDraw.UpdatePromotionStatus(AccountEntity.Games.OperationsId(), ACCOUNT_PROMO_STATUS.REDEEMED, AccountEntity.Games.Type(), SqlTrx))
            {
              Log.Error("TerminalDraw Error: Updating Account promotions to Redeemed.");

              return false;
            }

            _loser_voucher = false;

            //Create Voucher
            VoucherBuilder.GetVoucherTerminalDraw(_input_parameters.VoucherAccountInfo
                                                  , _output_parameters.OperationId
                                                  , _input_parameters
                                                  , _output_parameters
                                                  , PrintMode.Print
                                                  , _is_winner
                                                  , _loser_voucher
                                                  , SqlTrx);


            if (!_is_winner)
            {

              _loser_voucher = true;

              //Create Loser Voucher
              VoucherBuilder.GetVoucherTerminalDraw(_input_parameters.VoucherAccountInfo
                                                    , _output_parameters.OperationId
                                                    , _input_parameters
                                                    , _output_parameters
                                                    , PrintMode.Print
                                                    , _is_winner
                                                    , _loser_voucher
                                                    , SqlTrx);


            }


            //Deletes Recharge
            if (!DB_DeleteTerminalDrawRechargeById(_recharges.TdrId, SqlTrx))
            {
              Log.Error(String.Format("TerminalDraw Error: DB_DeleteTerminalDrawRechargeById(): TerminalId: {0}; AccountId: {1} ", InternalId, AccountEntity.AccountId));

              return false;
            }

            _processed = true;

          } // foreach
        } // foreach

        if (_processed)
        {
          if (!MultiPromos.Trx_UpdateAccountBalance(AccountEntity.AccountId, MultiPromos.AccountBalance.Zero, out _account_balance, SqlTrx))
          {
            Log.Error(String.Format("TerminalDraw Error: Trx_UpdateAccountBalance(): TerminalId: {0}; AccountId: {1} ", InternalId, AccountEntity.AccountId));

            return false;
          }

          // WelcomeDraw configuration
          if (GameId <= WELCOME_DRAW_ID)
          {
            SetPrizePlanData_WelcomeDraw(ref _prize_plan_data, _play_session_mahine_id, _account_balance.TotalBalance, _amount_re_played, _amount_re_won, _amount_nr_won, _is_winner);
          }
          else
          {
            SetPrizePlanData_PromoGame(ref _prize_plan_data, GameId, _play_session_mahine_id, _account_balance.TotalBalance, _amount_re_played, _amount_re_won, _amount_nr_won, _is_winner);
          }

          //Add prize plan
          PrizePlanGames.AddPrizePlan(_prize_plan_data);

          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;

    } // TerminalDrawProcessDraw

    /// <summary>
    /// End terminal draw
    /// </summary>
    /// <param name="PlaySessionId"></param>
    /// <param name="TimeElapsed"></param>
    /// <param name="ClosePlaySession"></param>
    /// <param name="SqlTrx"></param>
    public static Boolean ProcessTerminalDrawEnd(Int64 PlaySessionId, Int64 TimeElapsed, Boolean ClosePlaySession)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (ProcessTerminalDrawEnd(PlaySessionId, TimeElapsed, ClosePlaySession, _db_trx.SqlTransaction))
        {
          return _db_trx.Commit();
        }
      }

      return false;
    } // ProcessTerminalDrawEnd
    public static Boolean ProcessTerminalDrawEnd(Int64 PlaySessionId, Int64 TimeElapsed, Boolean ClosePlaySession, SqlTransaction SqlTrx)
    {
      if (!CashDeskDrawBusinessLogic.Trx_UpdatePlaySessionFinished(PlaySessionId, TimeElapsed, ClosePlaySession, SqlTrx))
      {
        Log.Error(String.Format("TerminalDraw Error: Trx_UpdatePlaySessionFinished()"));

        return false;
      }

      return true;
    } // ProcessTerminalDrawEnd

    /// <summary>
    /// Add terminal draw recharge in session
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <param name="AccountId"></param>
    /// <param name="AccountLevel"></param>
    /// <param name="TotalPoints"></param>
    /// <param name="TrackData"></param>
    /// <param name="Pin"></param>
    /// <param name="TerminalName"></param>
    /// <param name="PromotionId"></param>
    /// <param name="GameId"></param>
    /// <param name="Amount"></param>
    /// <param name="PromotionAmount"></param>
    /// <param name="GameType"></param>
    /// <returns></returns>
    public static Boolean AddTerminalDrawRecharge_InSession(OperationCode OperationCode
                                                            , Int64 AccountId
                                                            , Int32 AccountLevel
                                                            , Points TotalPoints
                                                            , String TrackData
                                                            , String Pin
                                                            , String TerminalName
                                                            , Int64 PromotionId
                                                            , Int64 GameId
                                                            , Currency Amount
                                                            , Currency PromotionAmount
                                                            , PromoGame.GameType GameType)
    {
      DataTable _promotions;

      try
      {
        // Create new AccountPromotion DataTable.
        _promotions = AccountPromotion.CreateAccountPromotionTable();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          switch (GameType)
          {
            // Add PlayPoint promotion to account
            case PromoGame.GameType.PlayPoint:
              return ProcessGiftPlayerRequest(AccountId, AccountLevel, TrackData, Pin, TerminalName, PromotionId);

            // Link Account promotion to related terminal draw
            case PromoGame.GameType.PlayReward:
              return PlayRewardPromotionManager(OperationCode, AccountId, PromotionId, Amount, PromotionAmount, GameId, GameType, _db_trx.SqlTransaction);

            default:
              // NOTHING TODO
              break;
          }

          return _db_trx.Commit();
        }
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return false;

    } // AddTerminalDrawRecharge_InSession


    /// <summary>
    /// Check if an account have terminal draw
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    public static Boolean CheckExistsTerminalDrawRechargeByAccountId(Int64 AccountId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return CheckExistsTerminalDrawRechargeByAccountId(AccountId, _db_trx.SqlTransaction);
      }
    } // CheckExistsTerminalDrawRechargeByAccountId

    /// <summary>
    /// Check if an account have terminal draw 
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean CheckExistsTerminalDrawRechargeByAccountId(Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder(); 
        _sb.AppendLine(" SELECT   1                             ");
        _sb.AppendLine("   FROM   TERMINAL_DRAWS_RECHARGES      ");
        _sb.AppendLine("  WHERE   TDR_ACCOUNT_ID =  @pAccountId ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          _obj = _sql_command.ExecuteScalar();

          return (_obj != null && _obj != DBNull.Value);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CheckExistsTerminalDrawRechargeByAccountId


    #region "Private Methods"


    /// <summary>
    /// Calculate total charge draw
    /// </summary>
    /// <param name="Games"></param>
    /// <param name="TotalChargeDraw"></param>
    /// <returns></returns>
    private static Boolean CalculateTotalChargeDraw(List<TerminalDrawGamesEntity> Games, out Decimal TotalChargeDraw)
    {
      MultiPromos.AccountBalance _charge_draw;

      TotalChargeDraw = 0;

      // Games iteration
      foreach (TerminalDrawGamesEntity _game in Games)
      {
        // Recharges iteration
        foreach (TerminalDrawRechargeEntity _recharge in _game.Recharges)
        {
          if (!GetChargeDraw(_recharge, out _charge_draw))
          {
            return false;
          }

          TotalChargeDraw += _charge_draw.TotalBalance;
        }
      }

      return true;
    } // GetTotalChargeDraw

    /// <summary>
    /// Get charge draw
    /// </summary>
    /// <param name="Recharge"></param>
    /// <param name="AccountBalanceRecharge"></param>
    /// <returns></returns>
    private static Boolean GetChargeDraw(TerminalDrawRechargeEntity Recharge, out MultiPromos.AccountBalance AccountBalanceRecharge)
    {
      // TerminalDraw - WelcomeDraw
      if (Recharge.GameId == WELCOME_DRAW_ID)
      {
        return GetRecharge_WelcomeDraw(Recharge, out AccountBalanceRecharge);
      }

      return GetRecharge_PromoGame(Recharge, out AccountBalanceRecharge);
    } // GetChargeDraw

    /// <summary>
    /// Get ChargeDraw from WelcomDraw
    /// </summary>
    /// <param name="Recharge"></param>
    /// <param name="AccountBalanceRecharge"></param>
    /// <returns></returns>
    private static Boolean GetRecharge_WelcomeDraw(TerminalDrawRechargeEntity Recharge, out MultiPromos.AccountBalance AccountBalanceRecharge)
    {
      Decimal _reedemable;
      Decimal _participation_prize;
      Decimal _max_participation_prize;

      AccountBalanceRecharge = new MultiPromos.AccountBalance();

      try
      {
        _max_participation_prize = CashDeskDrawBusinessLogic.GetMaxParticipationPrice((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02);

        _participation_prize = CashDeskDrawBusinessLogic.GetParticipationPrice((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02);
        _participation_prize /= (Decimal)100;
        _participation_prize *= Recharge.AmountReBet;

        _reedemable = Math.Min(_participation_prize, _max_participation_prize);

        AccountBalanceRecharge = new MultiPromos.AccountBalance(_reedemable, 0, 0);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetRecharge_WelcomeDraw

    /// <summary>
    /// Get ChargeDraw from PromoGame
    /// </summary>
    /// <param name="Recharge"></param>
    /// <param name="AccountBalanceRecharge"></param>
    /// <returns></returns>
    private static Boolean GetRecharge_PromoGame(TerminalDrawRechargeEntity Recharge, out MultiPromos.AccountBalance AccountBalanceRecharge)
    {
      PromoGame _promo_game;

      AccountBalanceRecharge = new MultiPromos.AccountBalance();

      try
      {
        // TODO JBP: Revise if is correct instance AccountBalance with Price always redeemable.
        _promo_game = new PromoGame(Recharge.GameId);
        AccountBalanceRecharge = new MultiPromos.AccountBalance(_promo_game.Price, 0, 0);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetRecharge_PromoGame

    /// <summary>
    /// Insert terminal draw recharge
    /// </summary>
    /// <param name="TerminalDrawRecharge"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_InsertTerminalDrawRecharge(TerminalDrawAccountEntity TerminalDrawRecharge, Int64 OperationId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = InsertQuery();

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = TerminalDrawRecharge.AccountId;
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)TerminalDraw.StatusTerminalDraw.PENDING;
          _sql_cmd.Parameters.Add("@pTotalCashIn", SqlDbType.Money).Value = TerminalDrawRecharge.Games[0].Recharges[0].AmountTotalCashIn;
          _sql_cmd.Parameters.Add("@pReBet", SqlDbType.Money).Value = TerminalDrawRecharge.Games[0].Recharges[0].AmountReBet;
          _sql_cmd.Parameters.Add("@pNrBet", SqlDbType.Money).Value = TerminalDrawRecharge.Games[0].Recharges[0].AmountNrBet;

          //Future Indicate NR or Point Also
          _sql_cmd.Parameters.Add("@pPointsBet", SqlDbType.Money).Value = DBNull.Value;

          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = TerminalDrawRecharge.Games.Id();
          _sql_cmd.Parameters.Add("@pGameType", SqlDbType.BigInt).Value = TerminalDrawRecharge.Games.Type();

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_InsertTerminalDrawRecharge

    /// <summary>
    /// Function to insert a playcashdraw  register into TERMINAL_DRAW_RECHARGES 
    /// </summary>
    /// <returns>True:if Insert the register/False: otherwise</returns>
    private static Boolean DB_InsertPlayDrawRecharge(Int64 AccountId, Decimal TotalCashIn, Decimal ReBet, Decimal NrBet, Decimal PointsBet, Int64 OperationId, long GameId, PromoGame.GameType GameType, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = InsertQuery();

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)TerminalDraw.StatusTerminalDraw.PENDING;
          _sql_cmd.Parameters.Add("@pTotalCashIn", SqlDbType.Money).Value = TotalCashIn;

          _sql_cmd.Parameters.Add("@pReBet", SqlDbType.Money).Value = ReBet;
          if (ReBet == 0)
          {
            _sql_cmd.Parameters.Add("@pReBet", SqlDbType.Money).Value = DBNull.Value;
          }

          _sql_cmd.Parameters.Add("@pNrBet", SqlDbType.Money).Value = NrBet;
          if (NrBet == 0)
          {
            _sql_cmd.Parameters.Add("@pNrBet", SqlDbType.Money).Value = DBNull.Value;
          }

          _sql_cmd.Parameters.Add("@pPointsBet", SqlDbType.Money).Value = PointsBet;
          if (PointsBet == 0)
          {
            _sql_cmd.Parameters.Add("@pPointsBet", SqlDbType.Money).Value = DBNull.Value;
          }

          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = GameId;

          _sql_cmd.Parameters.Add("@pGameType", SqlDbType.BigInt).Value = (Int64)GameType;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_InsertPlayCashDrawRecharge

    /// <summary>
    /// Function that retuns insert query to Terminal_draws_recharge
    /// </summary>
    /// <returns></returns>
    private static StringBuilder InsertQuery()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.AppendLine("   INSERT INTO   TERMINAL_DRAWS_RECHARGES  ");
      _sb.AppendLine("               ( TDR_ACCOUNT_ID            ");
      _sb.AppendLine("               , TDR_STATUS                ");
      _sb.AppendLine("               , TDR_TOTAL_CASH_IN         ");
      _sb.AppendLine("               , TDR_RE_BET                ");
      _sb.AppendLine("               , TDR_NR_BET                ");
      _sb.AppendLine("               , TDR_POINTS_BET            ");
      _sb.AppendLine("               , TDR_OPERATION_ID          ");
      _sb.AppendLine("               , TDR_GAME_ID               ");
      _sb.AppendLine("               , TDR_GAME_TYPE)            ");
      _sb.AppendLine("        VALUES (                           ");
      _sb.AppendLine("                 @pAccountId               ");
      _sb.AppendLine("               , @pStatus                  ");
      _sb.AppendLine("               , @pTotalCashIn             ");
      _sb.AppendLine("               , @pReBet                   ");
      _sb.AppendLine("               , @pNrBet                   ");
      _sb.AppendLine("               , @pPointsBet               ");
      _sb.AppendLine("               , @pOperationId             ");
      _sb.AppendLine("               , @pGameId                  ");
      _sb.AppendLine("               , @pGameType)               ");

      return _sb;
    } // InsertQuery

    /// <summary>
    /// GetAccountPRomotionId
    /// </summary>
    /// <param name="Promotions"></param>
    /// <param name="Filter"></param>
    /// <returns></returns>
    private static Int64 GetAccountPromotionId(DataTable Promotions, String Filter)
    {
      DataRow[] _aux_rows;

      _aux_rows = Promotions.Select(Filter);

      if (_aux_rows.Length > 0)
      {

        return (Int64)_aux_rows[0]["ACP_UNIQUE_ID"];
      }

      return 0;

    } // GetAccountPromotionId

    /// <summary>
    /// Set AccountEntity Screen Messages
    /// </summary>
    /// <param name="AccountEntity"></param>
    private static void SetAccountEntityParams_ScreenMessages(ref TerminalDrawAccountEntity AccountEntity)
    {
      // First Screen 
      AccountEntity.Games[0].FirstScreenMessageLine0 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameFirstDrawScreenMessageLine0", string.Empty);
      AccountEntity.Games[0].FirstScreenMessageLine1 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameFirstDrawScreenMessageLine1", string.Empty);
      AccountEntity.Games[0].FirstScreenMessageLine2 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameFirstDrawScreenMessageLine2", string.Empty);

      // Second Screen
      AccountEntity.Games[0].SecondScreenMessageLine0 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameSecondDrawScreenMessageLine0", string.Empty);
      AccountEntity.Games[0].SecondScreenMessageLine1 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameSecondDrawScreenMessageLine1", string.Empty);
      AccountEntity.Games[0].SecondScreenMessageLine2 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameSecondDrawScreenMessageLine2", string.Empty);
      AccountEntity.Games[0].SecondScreenMessageLine3 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameSecondDrawScreenMessageLine3", string.Empty);

    } // SetAccountEntityParams_WelcomeDraw

    /// <summary>
    /// Set AccountEntity params for WelcomeDraw
    /// </summary>
    /// <param name="AccountEntity"></param>
    /// <param name="PlaySessionId"></param>
    /// <param name="TotalBalance"></param>
    /// <param name="TotalBetAmount"></param>
    private static void SetAccountEntityParams_WelcomeDraw(ref TerminalDrawAccountEntity AccountEntity, Int64 PlaySessionId, Decimal TotalBalance, Decimal TotalBetAmount)
    {

      // * First Screen
      AccountEntity.Games[0].FirstScreenTimeOut = GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameFirstDrawScreenTimeOut", 10);
      AccountEntity.Games[0].TimeoutExpiresParticipateInDraw = GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameFirsIfTimeOutExpiresParticipateInDraw", false);

      // Force PlayDraw: ShowTheBuyDialog is only for InTouchWeb. If ShowTheBuyDialog == true, not force play draw.
      AccountEntity.Games[0].ForceParticipateInDraw = (GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameFirstDrawScreenForceParticipateInDraw", false)
                                                     && !GeneralParam.GetBoolean("CashDesk.DrawConfiguration.02", "ShowTheBuyDialog", false));

      // * Second Screen
      AccountEntity.Games[0].SecondScreenTimeOut = GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameTimeout", 10);
      
      // Force Execute Draw in Terminal Draw (only for 4 LCD Lines)
      AccountEntity.Games[0].ForceExecuteDraw = GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameSecondDrawScreenForceExecuteDraw", false);

      // * Game Url
      AccountEntity.Games[0].GameUrl = Misc.ReplaceUrlParam(GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameUrl", String.Empty));

      // * Play Session Id
      AccountEntity.Games[0].PlaySessionId = PlaySessionId;

      // * Total Balance
      AccountEntity.Games[0].BalanceAmount = TotalBalance;

      // * Charge Draw (Bet Amount)
      AccountEntity.Games[0].TotalBetAmount = TotalBetAmount;

    } // SetAccountEntityParams_WelcomeDraw

    /// <summary>
    /// Set AccountEntity params for PromoGame
    /// </summary>
    /// <param name="AccountEntity"></param>
    /// <param name="PlaySessionId"></param>
    /// <param name="TotalBalance"></param>
    /// <param name="TotalBetAmount"></param>
    private static void SetAccountentityParams_PromoGame(ref TerminalDrawAccountEntity AccountEntity, Int64 GameId, Int64 PlaySessionId, Decimal TotalBalance, Decimal TotalBetAmount)
    {
      PromoGame _game;

      _game = new PromoGame(GameId);

      // Show buy dialog ( General_Params.ShowTheBuyDialog & Game.ShowBuyDialog )
      AccountEntity.Games[0].ForceParticipateInDraw = !(GeneralParam.GetBoolean("CashDesk.DrawConfiguration.02", "ShowTheBuyDialog", false) && _game.ShowBuyDialog);

      // TimeOuts
      AccountEntity.Games[0].FirstScreenTimeOut = _game.AutoCancel;
      AccountEntity.Games[0].SecondScreenTimeOut = _game.TransferTimeOut;
      AccountEntity.Games[0].TimeoutExpiresParticipateInDraw = false;

      // Game Url
      AccountEntity.Games[0].GameUrl = _game.GameURL;

      // Play Session Id
      AccountEntity.Games[0].PlaySessionId = PlaySessionId;

      // Total Balance
      AccountEntity.Games[0].BalanceAmount = TotalBalance;

      // Charge Draw (Bet Amount)
      AccountEntity.Games[0].TotalBetAmount = TotalBetAmount;
    } // SetAccountentityParams_PromoGame

    /// <summary>
    /// Set PrizePlanData for WelcomeDraw
    /// </summary>
    /// <param name="AccountEntity"></param>
    /// <param name="PlaySessionId"></param>
    /// <param name="TotalBalance"></param>
    /// <param name="TotalBetAmount"></param>
    private static void SetPrizePlanData_WelcomeDraw(ref PrizePlanData PrizePlanData, Int64 PlaySessionId, Decimal TotalBalance, Decimal AmountREPlayed, Decimal AmountREWon, Decimal AmountNRWon, Boolean IsWinner)
    {
      //2. Return Prize Plan
      //PrizePlanData.TerminalGameUrl = Misc.ReplaceUrlParam(GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameUrl", String.Empty));
      PrizePlanData.TerminalGameName = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameName", "Sorteo en Máquina");

      // Winner
      PrizePlanData.ResultWinnerScreenMessageLine0 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameResultWinnerDrawScreenMessageLine0", string.Empty);
      PrizePlanData.ResultWinnerScreenMessageLine1 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameResultWinnerDrawScreenMessageLine1", string.Empty);
      PrizePlanData.ResultWinnerScreenMessageLine2 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameResultWinnerDrawScreenMessageLine2", string.Empty);
      PrizePlanData.ResultWinnerScreenMessageLine3 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameResultWinnerDrawScreenMessageLine3", string.Empty);

      // Looser
      PrizePlanData.ResultLooserScreenMessageLine0 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameResultLooserDrawScreenMessageLine0", string.Empty);
      PrizePlanData.ResultLooserScreenMessageLine1 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameResultLooserDrawScreenMessageLine1", string.Empty);
      PrizePlanData.ResultLooserScreenMessageLine2 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameResultLooserDrawScreenMessageLine2", string.Empty);
      PrizePlanData.ResultLooserScreenMessageLine3 = GeneralParam.GetString("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameResultLooserDrawScreenMessageLine3", string.Empty);

      // TerminalGameTimeOut 
      PrizePlanData.TerminalGameTimeOut = GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameTimeout", 10, 10, Int32.MaxValue);

      // Force PlayDraw: ShowTheBuyDialog is only for InTouchWeb. If ShowTheBuyDialog == true, not force play draw.
      PrizePlanData.Forced = (GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameFirstDrawScreenForceParticipateInDraw", false)
                            && !GeneralParam.GetBoolean("CashDesk.DrawConfiguration.02", "ShowTheBuyDialog", false));

      PrizePlanData.IsWinner = IsWinner;
      PrizePlanData.AmountRePlayedDraw = AmountREPlayed;
      PrizePlanData.AmountRePlayedWon = AmountREWon;
      PrizePlanData.AmountNrPlayedWon = AmountNRWon;
      PrizePlanData.AmountTotalBalance = TotalBalance;
      PrizePlanData.PlaySessionId = PlaySessionId;
    } // SetPrizePlanData_WelcomeDraw

    /// <summary>
    /// Set PrizePlanData for PlayPromo
    /// </summary>
    /// <param name="AccountEntity"></param>
    /// <param name="PlaySessionId"></param>
    /// <param name="TotalBalance"></param>
    /// <param name="TotalBetAmount"></param>
    private static void SetPrizePlanData_PromoGame(ref PrizePlanData PrizePlanData, Int64 GameId, Int64 PlaySessionId, Decimal TotalBalance, Decimal AmountREPlayed, Decimal AmountREWon, Decimal AmountNRWon, Boolean IsWinner)
    {
      PromoGame _promo_game;

      _promo_game = new PromoGame(GameId);

      //2. Return Prize Plan
      PrizePlanData.TerminalGameUrl = _promo_game.GameURL;
      PrizePlanData.TerminalGameName = _promo_game.Name;

      // Data
      PrizePlanData.TerminalGameTimeOut = GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij((short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02), "TerminalGameTimeout", 10, 10, Int32.MaxValue);
      PrizePlanData.IsWinner = IsWinner;
      PrizePlanData.AmountRePlayedDraw = AmountREPlayed;
      PrizePlanData.AmountRePlayedWon = AmountREWon;
      PrizePlanData.AmountNrPlayedWon = AmountNRWon;
      PrizePlanData.AmountTotalBalance = TotalBalance;
      PrizePlanData.PlaySessionId = PlaySessionId;
    } // SetPrizePlanData_PlayPromo


    /// <summary>
    /// Check if is necessary update account promotion
    /// </summary>
    /// <param name="PromoType"></param>
    /// <param name="GameType"></param>
    /// <returns></returns>
    private static Boolean MustUpdateAccountPromotionStatus(ACCOUNT_PROMO_STATUS PromoType, PromoGame.GameType GameType)
    {
      switch (PromoType)
      {
        case ACCOUNT_PROMO_STATUS.BOUGHT:
          // Check if is necessary update AccountPromotion to Bought
          return MustUpdateAccountPromotionStatusToBought(GameType);

        case ACCOUNT_PROMO_STATUS.REDEEMED:
          // Check if is necessary update AccountPromotion to Redeemed
          return MustUpdateAccountPromotionStatusToRedeemed(GameType);
      }

      Log.Message("Error on MustUpdateAccountPromotionStatus(). Invalid ACCOUNT_PROMO_STATUS.");

      return false;
    } // MustUpdateAccountPromotionStatus

    /// <summary>
    /// Update Account_promotion 
    /// </summary>
    /// <param name="OperationID"></param>
    /// <param name="Type"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_UpdatePromotionStatus(Int64 OperationID, ACCOUNT_PROMO_STATUS Type, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS          ");
        _sb.AppendLine("    SET   ACP_STATUS = @pStatus       ");
        _sb.AppendLine("  WHERE   ACP_OPERATION_ID = @pTdrId  ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = Type;
          _sql_command.Parameters.Add("@pTdrId", SqlDbType.BigInt).Value = OperationID;

          if (_sql_command.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //DB_UpdatePromotionStatus

    /// <summary>
    /// Return if is necessary update AccountPromotion Status to Bought
    /// </summary>
    /// <returns></returns>
    private static Boolean MustUpdateAccountPromotionStatusToBought(PromoGame.GameType Type)
    {
      return (Type != PromoGame.GameType.Welcome);
    } // MustUpdateAccountPromotionStatusToBought

    /// <summary>
    /// Return if is necessary update AccountPromotion Status to Redeemed
    /// </summary>
    /// <returns></returns>
    private static Boolean MustUpdateAccountPromotionStatusToRedeemed(PromoGame.GameType Type)
    {
      return (Type != PromoGame.GameType.Welcome);
    } // MustUpdateAccountPromotionStatusToBought

    /// <summary>
    /// Function that Process the player gift request
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="AccountLevel"></param>
    /// <param name="TrackData"></param>
    /// <param name="Pin"></param>
    /// <param name="TerminalName"></param>
    /// <param name="PromotionId"></param>
    /// <returns></returns>
    private static Boolean ProcessGiftPlayerRequest(Int64 AccountId, Int32 AccountLevel, String TrackData, String Pin, String TerminalName, Int64 PromotionId)
    {

      WCPPlayerRequest _intouch_points;
      Int32 _response_code;
      String _data_account_level;
      ArrayList _voucher_list;
      Ticket _tickets;
      MultiPromos.PromoBalance _promo_balance;

      _data_account_level = String.Format("GIFTS.GI_POINTS_LEVEL{0}", AccountLevel);

      InTouchGift _gift = new InTouchGift(PromotionId, _data_account_level);

      TerminalName = TerminalName.Replace("[", "").Replace("]", "");

      _intouch_points = new WCPPlayerRequest(_gift.GiftId, TrackData, Pin, (_gift.GiftPoints * 100), 1, AccountId, TerminalName, 0, null);

      OperationVoucherParams.LoadData = true;

      if (!_intouch_points.ProcessPlayerRequestGift(_intouch_points, out  _voucher_list, out  _tickets, out _promo_balance, out _response_code))
      {
        return false;
      }

      return true;
    } // ProcessGiftPlayerRequest

    /// <summary>
    /// PlayReward promotion manager
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <param name="AccountId"></param>
    /// <param name="PromotionId"></param>
    /// <param name="Amount"></param>
    /// <param name="PromotionAmount"></param>
    /// <param name="GameId"></param>
    /// <param name="GameType"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean PlayRewardPromotionManager(OperationCode OperationCode
                                                      , Int64 AccountId
                                                      , Int64 PromotionId
                                                      , Currency Amount
                                                      , Currency PromotionAmount
                                                      , Int64 GameId
                                                      , PromoGame.GameType GameType
                                                      , SqlTransaction SqlTrx)
    {
      Int64 _operation_id;

      // Add operation id
      if (!Operations.DB_InsertOperation(OperationCode, AccountId, PromotionId, Amount, PromotionAmount, out _operation_id, SqlTrx))
      {
        Log.Message("Error on PlayDrawInsert(). Can't insert account operation.");

        return false;
      }

      // Update related AccountPromotion with TerminalDraw
      if (!new AccountPromotion().RelateAccountPromotionWithTerminalDraw(AccountId, PromotionId, _operation_id, SqlTrx))
      {
        Log.Message("Error on PlayRewardPromotionManager(). Update AccountPromotion failed.");

        return false;
      }
      
      // Insert Terminal draw recharge (Promotion "Pending Draw")
      if (!GenerateTerminalDrawRecharge(_operation_id, AccountId, PromotionAmount, 0, GameId, PromotionAmount, GameType, SqlTrx))
      {
        Log.Message("Error on PlayDrawInsert(). Can't insert terminal draw recharge. ");

        return false;
      }


      return true;
    } // PlayRewardPromotionManager

    public static Boolean ProcesRequest()
    {
      return true;
    }

    public static Int32 GetTerminalDrawGroups()
    {
      Int32 _number_of_draws;
      _number_of_draws = 1;

      _number_of_draws = GeneralParam.GetInt32("CashDesk.Draw.02", "GroupDraws", 1, 1, 20);

      return _number_of_draws;

    } //GetTerminalDrawGroups

    #endregion

  } // TerminalDraw
}
