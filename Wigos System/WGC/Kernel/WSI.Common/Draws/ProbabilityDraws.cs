﻿//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Probability.cs
// 
//   DESCRIPTION: Class to calculate Draws
// 
//        AUTHOR: Rodrigo Gonzalez Rodriguez
// 
// CREATION DATE: 26-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-SEP-2016  RGR    Initial version - PBI 17965: Televisa - Draw Cash (Cash prize) - Probability of prizes

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  internal class ProbabilityDraws
  {
    #region Const
    private const decimal totalProbability = 100;
    #endregion

    #region Members
    private SinglePrizeCategory[] m_prizes;
    private static Random m_random = new Random();
    #endregion

    #region Constructor
    internal ProbabilityDraws(IList<SinglePrizeCategory> prizes)
    {
      // AJQ: There is no need to sort the prizes
      m_prizes = SinglePrizeCategory.ToArray(prizes);
    }
    #endregion

    #region Internal Methods
    internal bool IsValid()
    {
      decimal _sum;

      _sum = 0;
      foreach (var _item in m_prizes)
      {
        _sum += _item.m_percent_probability;
      }

      return _sum == totalProbability ? true : false;
    }

    public SinglePrizeCategory Draw()
    {
      decimal _rnd_value;
      decimal _sum_prob;
      SinglePrizeCategory _default;

      lock (m_random)
      {
        _rnd_value = (decimal)(m_random.NextDouble() * 100);
      }

      _default =  new SinglePrizeCategory(0, 0, 0, 0, PrizeType.Unknown, false);
      _sum_prob = 0;

      foreach (SinglePrizeCategory _category in m_prizes)
      {
        if (_category.m_percent_probability <= 0) continue;

        if (_category.m_percent_probability > _default.m_percent_probability)
        {
          _default = _category;
        }

        _sum_prob = _sum_prob + _category.m_percent_probability;

        if (_sum_prob >= _rnd_value)
        {
          return _category;
        }
      }

      return _default;
    }


    #endregion

  }
}
