//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashDeskDrawBusinessLogic.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Daniel Moreno
// 
// CREATION DATE: 01-AUG-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2013  DMR    Initial version
// 05-NOV-2013  MMG    Modified ValidateCashDeskDrawConfiguration function
//                     Modified CalculateCashDeskDrawPrize function
// 26-JUN-2014  RCI    Fixed Bug WIG-1050: CashDesk Draw: Wrong calculate when IEJC tax.
// 08-OCT-2014  DHA    If cash desk draw is generate as play session, don't reset cancellable operation
// 17-AUG-2015  DCS    Fixed Bug: WIG-2181 The barcode is not printed when it hides the currency symbol
// 13-JAN-2015  JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 01-MAR-2016  JML    Product Backlog Item 10084: Winpot - Provisioning Tax: Cashdesk draw
// 03-MAR-2016  JML    Product Backlog Item 10085:Winpot - Tax Provisions: Include movement reports
// 14-SEP-2016  RGR    Product Backlog Item 17445:Televisa - Cashier draw (Cash prize) - Configuration
// 22-SEP-2016  LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 26-SEP-2016  RGR    PBI 17965: Televisa - Draw Cash (Cash prize) - Probability of prizes
// 29-SEP-2016  RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 04-OCT-2016  LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 04-OCT-2016  RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 05-OCT-2016  RGR    PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
// 10-OCT-2016  RGR    Bug 18880: Draw tables: the percentage configuration tables draw does not correspond to company A and B.
// 10-OCT-2016  LTC    Bug 18843:Cajero - Cuentas: Reintegro de pago de premio en "Efectivo en cuenta" muestra un error y no permite la operaci�n
// 11-OCT-2016  LTC    Bug 18883: Draw tables: Is not possible to sell chips with draw on gaming tables enabled
// 11-OCT-2016  EOR    Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cup�n promocional in the ticket
// 12-OCT-2016  RGR    Bug 18922: Draw box with cash prize (Televisa): the concept of the promotional coupon is not shown on the ticket cash desk
// 12-OCT-2016  EOR    Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cup�n promocional in the ticket
// 12-OCT-2016  LTC    Bug 18883: Draw tables: Is not possible to sell chips with draw on gaming tables enabled
// 13-OCT-2016  LTC    Bug 19012: Craw on Gaming tables: won amount is not given as gaming chips
// 13-OCT-2016  RGR    Bug 19051: Draw box with cash prize (Televisa) is performed as loser draw with idle question of access
// 20-OCT-2016  DLL    Bug 19398:Cajero: C�lculos incorrectos en los vouchers de sorteo
// 31-OCT-2016  LTC    Bug 19228:WigosGUI: Wrong values  for looser on cash desk draw report
// 09-NOV-2016  ETP    Fixed Bug 20160: Duplicated concept when not participating in GamingTables cashdeskdraw
// 15-NOV-2016  ETP    Fixed Bug Bug 20363: Can't recharge wihtout participate in cashdesk draw 
// 25-JAN-2017  FJC    PBI 22241:Sorteo de m�quina - Participaci�n - US 1 - Participaci�n
// 07-FEB-2017  ATB    Fixed Bug 24290: Cashier: Tickets TITO can't be created
// 08-FEB-2017 FAV     PBI 25268:Third TAX - Payment Voucher
// 03-MAR-2017  XGJ    Bug 25270 Sorteo de m�quina - Reporte de sesiones de juego no siemre registra todas las columnas
// 23-MAR-2017  XGJ & FJC & SJA Fixed Bug 26082:Sorteo de M�quina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado.
// 08-MAR-2017  FAV    PBI 25268: Third TAX - Payment Voucher
// 08-MAR-2017  FGB    PBI 25269: Third TAX - Cash session summary
// 15-MAR-2017  FGB    PBI 25268: Third TAX - Payment Voucher
// 03-MAR-2017  XGJ    Bug 25270 Sorteo de m�quina - Reporte de sesiones de juego no siemre registra todas las columnas
// 23-MAR-2017  XGJ & FJC & SJA Fixed Bug 26082:Sorteo de M�quina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado.
// 29-MAR-2017  FAV    PBI 25956:Third TAX - Cash desk draw and UNR TAX Configuration
// 06-APR-2017  FJC    PBI 26615:Sorteo de m�quina - Protecci�n para cerrar sesiones abiertas
// 23-MAY-2017  FJC    Fixed Bug 27604:Tickets perdodores de sorteo de caja generan sesiones de juego inconsistentes
// 17-OCT-2017  AMF    PBI 30297:[WIGOS-33] Cash draw with a configurable bet amount
// 20-OCT-2017  AMF    Bug 30346:[WIGOS-5957] Extra movement when Cash Desk draw
// 02-NOV-2017  AJQ & JCA WIGOS-6318: Tax on Prizes: different taxes for 'normal prizes' and 'cash-draw prizes'.
// 21-NOV-2017  RGR    Bug 30878:WIGOS-6654 WigosGUI is displaying auto generated movements in the screen "Cash movements" with amount 0$ and without any NLS 
// 28-DEC-2017  JML    Bug 31168:WIGOS-7352 [Ticket #11361] Sorteo de caja con un monto de apuesta configurable
// 15-FEB-2018  DHA    Bug 31537:WIGOS-8103 [Ticket #11369] Sessiones de sorteos - Sorteo Perdedor
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;

namespace WSI.Common
{
  public class CashDeskDrawBusinessLogic
  {
    private static int m_last_tick = WSI.Common.Misc.GetTickCount();
    private static int m_wait_hint = 0;

    #region enums

    public enum ErrorCode
    {
      Unknown = 0,
      WebServiceNoConnected = 1,
      RemoteServerNotConnected = 2,
      //---
    }

    public enum DrawGenerationStatus
    {
      Local = 0,
      LocalPendingToUpload = 10,
      LocalUploaded = 11,
      LocalUploadFailed = 12,
      Remote = 20,
    }

    public enum DrawUploadStatus
    {
      NotConfigured = 0,
      NothingToUpload = 1,
      SomethingUploaded = 2,
      ErrorUploading = 3,
      Error = 4,
    }

    public enum ActionOnServerDown
    {
      RechargeDisabled = 0,
      PrizeCoupon = 1,
      ComplementaryVoucher = 2,
    }

    public enum PrizeMode
    {
      Promotion = 0,
      PlayerSession = 1,
      CashPrize = 2,
      Terminal = 3
    }

    public enum ENUM_DRAW_CONFIGURATION
    {
      DRAW_00 = 0,  // CashDesk Draw
      DRAW_01 = 1,  // GamingTable Draw
      DRAW_02 = 2   // Terminal Draw
    }

    public enum ENUM_TAX_TYPE
    {
      TAX_DEFAULT = 0,
      TAX_CHIPS = 1,
      TAX_PROMO_RE = 2,
      TAX_DRAW_CASH_PRIZE = 3
    }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Get if cashdesk draws are enabled
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Draws are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsEnabledCashDeskDraws(Int16 CashDeskId = 0)
    {
      return GeneralParam.GetBoolean("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "Enabled", false);
    } //IsEnabledCashDeskDraws

    //------------------------------------------------------------------------------
    // PURPOSE: Get if cashdesk draw previews are enabled
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Preview Draws are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsEnabledPreviewCashDeskDraws(Int16 CashDeskId = 0)
    {
      return GeneralParam.GetBoolean("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "ShowCashDeskDraws", false);
    } //IsEnabledPreviewCashDeskDraws

    //------------------------------------------------------------------------------
    // PURPOSE: Get if cashdesk draws vouchers are enabled
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: vouchers are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsEnabledCashDeskDrawsVouchers(Int16 CashDeskId = 0)
    {
      return GeneralParam.GetBoolean("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "VoucherOnCashDeskDraws.Winner", false);
    } //IsEnabledCashDeskDrawsVouchers

    //------------------------------------------------------------------------------
    // PURPOSE: Get if cashdesk draws vouchers are enabled
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: vouchers are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsEnabledCashDeskDrawsComplementaryVouchers(Int16 CashDeskId = 0)
    {
      return GeneralParam.GetBoolean("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "VoucherOnCashDeskDraws.Loser", false);
    } //IsEnabledCashDeskDrawsComplementaryVouchers

    //------------------------------------------------------------------------------
    // PURPOSE: Get Draw Voucher Title
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: Voucher Title if set in General Parameters
    // 
    //   NOTES:
    //
    public static String GetCashDeskDrawVoucherTitle(Int16 CashDeskId = 0)
    {
      return GeneralParam.GetString("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "WinnerPrizeVoucherTitle");
    } //GetCashDeskDrawVoucherTitle

    //------------------------------------------------------------------------------
    // PURPOSE: Get if cashdesk draws Complementary Voucher Title are enabled
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: Complementary Voucher Title if set in General Parameters
    // 
    //   NOTES:
    //
    public static String GetCashDeskDrawVoucherComplementaryTitle(Int16 CashDeskId = 0)
    {
      return GeneralParam.GetString("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "LoserPrizeVoucherTitle");
    } //GetCashDeskDrawVoucherComplementaryTitle

    //------------------------------------------------------------------------------
    // PURPOSE: Get if cashdesk draws Loser Draws are enabled
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Loser Promotions are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsLoserPromotionEnabled(Int16 CashDeskId = 0)
    {
      return GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + GetCashDeskPostfij(CashDeskId), "LoserPrize1.Enabled", false);
    } //IsLoserPromotionEnabled

    //------------------------------------------------------------------------------
    // PURPOSE: Get the Prize Mode
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: PrizeMode
    // 
    //   NOTES:
    //
    public static PrizeMode GetPrizeMode(Int16 CashDeskId = 0)
    {
      PrizeMode _mode;

      _mode = (PrizeMode)GeneralParam.GetInt32("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "AccountingMode", 0);

      return _mode;
    } //GetPrizeMode

    //------------------------------------------------------------------------------
    // PURPOSE: Check if Player Session Draw Enabled
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Player Session Draw Enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static bool IsPlayerSessionCashDeskDraw(ENUM_DRAW_CONFIGURATION drawConfiguration)
    {
      return (CashDeskDrawBusinessLogic.GetPrizeMode((short)drawConfiguration) == PrizeMode.PlayerSession);
    } //IsPlayerSessionCashDeskDraw

    //------------------------------------------------------------------------------
    // PURPOSE: Check if Cash Prize Draw Enabled
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Cash Prize Draw Enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static bool IsCashPrizeCashDeskDraw(ENUM_DRAW_CONFIGURATION drawConfiguration)
    {
      return (CashDeskDrawBusinessLogic.GetPrizeMode((short)drawConfiguration) == PrizeMode.CashPrize);
    } //IsCashPrizeCashDeskDraw

    //------------------------------------------------------------------------------
    // PURPOSE: Check if Terminal Prize Draw
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Terminal Prize Draw Enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static bool IsTerminalDraw(ENUM_DRAW_CONFIGURATION DrawConfiguration)
    {
      return (CashDeskDrawBusinessLogic.GetPrizeMode((short)DrawConfiguration) == PrizeMode.Terminal);
    } //IsTerminalDraw

    //------------------------------------------------------------------------------
    // PURPOSE: Get the Maximum Paricipation Price
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: Maximum Participation Price
    // 
    //   NOTES:
    //
    public static decimal GetMaxParticipationPrice(Int16 CashDeskId = 0)
    {
      decimal _price;

      _price = GeneralParam.GetDecimal("CashDesk.DrawConfiguration" + GetCashDeskPostfij(CashDeskId), "ParticipationMaxPrice", 0);

      return _price;
    } //GetMaxParticipationPrice

    //------------------------------------------------------------------------------
    // PURPOSE: Get the Paricipation Price
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: Participation Price
    // 
    //   NOTES:
    //
    public static decimal GetParticipationPrice(Int16 CashDeskId = 0)
    {
      decimal _price;

      _price = GeneralParam.GetDecimal("CashDesk.DrawConfiguration" + GetCashDeskPostfij(CashDeskId), "ParticipationPrice", 0);

      return _price;
    } //GetParticipationPrice

    //------------------------------------------------------------------------------
    // PURPOSE: Get the Action on Server Down
    // 
    //  PARAMS:
    //      - INPUT : CashDeskID
    //
    //      - OUTPUT: 
    //
    // RETURNS: ActionOnServerDown
    // 
    //   NOTES:
    //
    static ActionOnServerDown GetActionOnServerDown(Int16 CashDeskId = 0)
    {
      Int32 _action;

      _action = GeneralParam.GetInt32("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "ActionOnServerDown");

      switch (_action)
      {
        case 0:
          return ActionOnServerDown.RechargeDisabled;

        case 1:
          return ActionOnServerDown.PrizeCoupon;

        default:
          // Do nothing -> RechargeDisabled
          break;
      }

      Log.Warning("Unknown CashDesk.Draw.ActionOnServerDown: " + _action.ToString() + ", -> RechargeDisabled");

      return ActionOnServerDown.RechargeDisabled;
    } //GetActionOnServerDown

    //------------------------------------------------------------------------------
    // PURPOSE: Get the value for the general param to ask for participation
    // 
    //  PARAMS:
    //      - INPUT: CashDeskId
    //
    //      - OUTPUT:
    //
    // RETURNS: True: Ask is enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean AskForParticipation(Int16 CashDeskId = 0)
    {
      return GeneralParam.GetBoolean("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "AskForParticipation", false);
    } //AskForParticipation

    //------------------------------------------------------------------------------
    // PURPOSE: Get the value for Cash Draw Voucher Title
    // 
    //  PARAMS:
    //      - INPUT: CashDeskId
    //
    //      - OUTPUT:
    //
    // RETURNS: Cash Draw Title
    // 
    //   NOTES:
    // 
    public static String GetCashDrawTitle(Int16 CashDeskId = 0)
    {
      return GeneralParam.GetString("CashDesk.Draw" + GetCashDeskPostfij(CashDeskId), "WinnerPrizeVoucherTitle").ToLower();
    } //GetCashDrawTitle

    //------------------------------------------------------------------------------
    // PURPOSE: Generate a cash desk draw
    // 
    //  PARAMS:
    //      - INPUT: OperationId
    //               AccountId
    //               Split A
    //               Split B
    //               Tax Provisions
    //
    //      - OUTPUT: CashDeskDraw
    //
    // RETURNS: True: Can generate cashdesk draws. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean GenerateCashdeskDraw(Int64 OperationId, Int64 AccountId, Currency SplitA, Currency SplitB, Currency TaxProvisions, out CashDeskDraw Draw, out RECHARGE_ERROR_CODE RechargeErrorCode, out String ErrorMessage, ENUM_DRAW_CONFIGURATION DrawMode, SqlTransaction Trx) // LTC 11-OCT-2016
    {
      RechargeErrorCode = RECHARGE_ERROR_CODE.ERROR_GENERIC;

      Draw = new CashDeskDraw((short)DrawMode);
      Draw.Parameters.m_total_cash_in = SplitA + SplitB;
      Draw.Parameters.m_tax_provisions = TaxProvisions;
      Draw.Parameters.m_account_id = AccountId;
      Draw.Parameters.m_operation_id = OperationId;

      // Bet Amount is only the A part (without CashIn tax).
      Draw.Parameters.m_bet_amount = SplitA;
      Draw.DrawId = (short)DrawMode;

      if (!CashDeskDrawBusinessLogic.ValidateCashDeskDrawConfiguration(Draw, out ErrorMessage))
      {
        Log.Error("Wrong Draw Configuration.");
        RechargeErrorCode = RECHARGE_ERROR_CODE.ERROR_RECHARGE_WITH_CASH_DESK_DRAW_ENABLED; // LTC 11-OCT-2016

        return false;
      }

      String _cash_desk_post_fij;
      _cash_desk_post_fij = GetCashDeskPostfij((short)DrawMode);

      if (!GeneralParam.GetBoolean("CashDesk.Draw" + _cash_desk_post_fij, "LocalServer") &&
         (!String.IsNullOrEmpty(GeneralParam.GetString("CashDesk.Draw" + _cash_desk_post_fij, "ServerAddress1"))
       || !String.IsNullOrEmpty(GeneralParam.GetString("CashDesk.Draw" + _cash_desk_post_fij, "ServerAddress2"))))
      {
        // Remote WebService
        if (!ExternalCashDeskDraw(Draw.Parameters, (short)DrawMode))
        {
          Log.Error("CashDeskDrawBusinessLogic.GenerateCashdeskDraw - Error in ExternalCashDeskDraw");
          RechargeErrorCode = RECHARGE_ERROR_CODE.ERROR_GENERATE_CASH_DESK_DRAW_EXTERNAL; // LTC 11-OCT-2016
          ErrorMessage = "Error in ExternalCashDeskDraw";

          return false;
        }
      }
      else
      {
        // Local Or LocalToUpload
        if (!Draw.Draw())
        {
          Log.Error("CashDeskDrawBusinessLogic.GenerateCashdeskDraw - Error in general params configuration");
          RechargeErrorCode = RECHARGE_ERROR_CODE.ERROR_CASH_DESK_DRAW_CONFIGURATION; // LTC 11-OCT-2016
          ErrorMessage = "Error in general params configuration";

          return false;
        }
      }

      // insert draw in local
      if (!SaveDraw(Draw, Trx))
      {
        Log.Error("SaveDraw failed!");
        RechargeErrorCode = RECHARGE_ERROR_CODE.ERROR_INSERT_CASH_DESK_DRAW_DB;
        ErrorMessage = "SaveDraw failed!";

        return false;
      }

      return true;
    } // GenerateCashdeskDraw

    //------------------------------------------------------------------------------
    // PURPOSE: Add Cash Prize
    // 
    //  PARAMS:
    //      - INPUT: IsNetPrize
    //               Prize
    //
    //      - OUTPUT: Gross
    //                Tax
    //
    // RETURNS: True: Cash Prize Added False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean AddPrizeCashPrize(Boolean IsNetPrize,
                                                 Currency Prize,
                                                 out Currency Gross,
                                                 out Currency Tax1,
                                            out Currency Tax2,
                                            out Currency Tax3)
    {
      Decimal _gross;
      Decimal _net;

      Decimal _tax1;
      Decimal _tax2;
      Decimal _tax3;
      Decimal _service_charge;
      Decimal _taxes;

      Percentage _tax1_pct;
      Percentage _tax2_pct;
      Percentage _tax3_pct;
      Percentage _service_charge_pct;
      Percentage _taxes_pct;
      TaxParameters _read_taxes;

      Gross = 0;
      Tax1 = 0;
      Tax2 = 0;
      Tax3 = 0;
      _read_taxes = TaxParameters.Read(ENUM_TAX_TYPE.TAX_DRAW_CASH_PRIZE);


      //This not apply because change the method to obtain the Taxes:
      //Create function TaxParameters.Read
      //// The Taxes on InKind Prizes must be explicitly set.
      //if (String.IsNullOrEmpty(GeneralParam.GetString("Cashier", "Tax.OnPrize.1.Pct"))
      // || String.IsNullOrEmpty(GeneralParam.GetString("Cashier", "Tax.OnPrize.2.Pct")))
      //{
      //  Log.Error("Tax.OnPrize.?.Pct not defined");

      //  return false;
      //}
      //if (String.IsNullOrEmpty(GeneralParam.GetString("Cashier.ServiceCharge", "OnWinner.PrizePct")))
      //{
      //  Log.Error("OnWinner.PrizePct not defined");

      //  return false;
      //}

      _tax1_pct = _read_taxes[0].TaxPct;//GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.1.Pct") / 100.0m;
      _tax2_pct = _read_taxes[1].TaxPct;//GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.2.Pct") / 100.0m;
      _tax3_pct = _read_taxes[2].TaxPct;//GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.3.Pct") / 100.0m;
      _service_charge_pct = GeneralParam.GetDecimal("Cashier.ServiceCharge", "OnWinner.PrizePct") / 100.0m;

      if (IsNetPrize)
      {
        _taxes_pct = (_tax1_pct + _tax2_pct + _tax3_pct + _service_charge_pct);

        if (_taxes_pct >= 100.0m)
        {
          Log.Error("Sum of Tax.OnPrize.?.Pct + ServiceCharge >= 100%");
          return false;
        }

        _gross = Math.Round(Prize / (1.0m - _taxes_pct), 2, MidpointRounding.AwayFromZero);
        _gross = _gross - 0.01m;

        while (true)
        {
          _tax1 = Math.Round(_gross * _tax1_pct, 2, MidpointRounding.AwayFromZero);
          _tax2 = Math.Round(_gross * _tax2_pct, 2, MidpointRounding.AwayFromZero);
          _tax3 = Math.Round(_gross * _tax3_pct, 2, MidpointRounding.AwayFromZero);
          _service_charge = Math.Round(_gross * _service_charge_pct, 2, MidpointRounding.AwayFromZero);
          _taxes = _tax1 + _tax2 + _tax3 + _service_charge;
          _net = _gross - _taxes;

          if (_net >= Prize)
          {
            break;
          }

          _gross += 0.01m;
        }
      }
      else
      {
        _gross = Prize;
        _tax1 = Math.Round(_gross * _tax1_pct, 2, MidpointRounding.AwayFromZero);
        _tax2 = Math.Round(_gross * _tax2_pct, 2, MidpointRounding.AwayFromZero);
        _tax3 = Math.Round(_gross * _tax3_pct, 2, MidpointRounding.AwayFromZero);
        _service_charge = Math.Round(Prize * _service_charge_pct, 2, MidpointRounding.AwayFromZero);

        _gross += _tax1 + _tax2 + _tax3;
      }

      Gross = _gross;
      Tax1 = _tax1;
      Tax2 = _tax2;
      Tax3 = _tax3;

      return true;
    } // AddPrizeCashPrize

    //------------------------------------------------------------------------------
    // PURPOSE: Add In Kind Prize
    // 
    //  PARAMS:
    //      - INPUT: IsNetPrize
    //               Prize
    //
    //      - OUTPUT: Gross
    //                Tax
    //
    // RETURNS: True: In Kind Prize Prize Added False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean AddPrizeUNR(Boolean IsNetPrize,
                                                 Currency Prize,
                                                 out Currency Gross,
                                                 out Currency Tax1,
                                      out Currency Tax2,
                                      out Currency Tax3)
    {
      Decimal _gross;
      Decimal _net;

      Decimal _tax1;
      Decimal _tax2;
      Decimal _tax3;
      Decimal _taxes;

      Percentage _tax1_pct;
      Percentage _tax2_pct;
      Percentage _tax3_pct;
      Percentage _taxex_pct;

      Gross = 0;
      Tax1 = 0;
      Tax2 = 0;
      Tax3 = 0;

      // The Taxes on InKind Prizes must be explicitly set.
      if (String.IsNullOrEmpty(GeneralParam.GetString("Cashier", "Tax.OnPrizeInKind.1.Pct"))
       || String.IsNullOrEmpty(GeneralParam.GetString("Cashier", "Tax.OnPrizeInKind.2.Pct"))
       || String.IsNullOrEmpty(GeneralParam.GetString("Cashier", "Tax.OnPrizeInKind.3.Pct")))
      {
        Log.Error("Tax.OnPrizeInKind.?.Pct not defined");

        return false;
      }

      _tax1_pct = GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind.1.Pct") / 100.0m;
      _tax2_pct = GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind.2.Pct") / 100.0m;
      _tax3_pct = GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind.3.Pct") / 100.0m;

      if (IsNetPrize)
      {
        _taxex_pct = (_tax1_pct + _tax2_pct + _tax3_pct);

        if (_taxex_pct >= 100.0m)
        {
          Log.Error("Sum of Tax.OnPrizeInKind.?.Pct >= 100%");
          return false;
        }

        _gross = Math.Round(Prize / (1.0m - (_taxex_pct / 100.0m)), 2, MidpointRounding.AwayFromZero);
        _gross = _gross - 0.01m;

        while (true)
        {
          _tax1 = Math.Round(_gross * _tax1_pct, 2, MidpointRounding.AwayFromZero);
          _tax2 = Math.Round(_gross * _tax2_pct, 2, MidpointRounding.AwayFromZero);
          _tax3 = Math.Round(_gross * _tax3_pct, 2, MidpointRounding.AwayFromZero);
          _taxes = _tax1 + _tax2 + _tax3;
          _net = _gross - _taxes;

          if (_net >= Prize)
          {
            break;
          }

          _gross += 0.01m;
        }
      }
      else
      {
        _gross = Prize;
        _tax1 = Math.Round(_gross * _tax1_pct, 2, MidpointRounding.AwayFromZero);
        _tax2 = Math.Round(_gross * _tax2_pct, 2, MidpointRounding.AwayFromZero);
        _tax3 = Math.Round(_gross * _tax3_pct, 2, MidpointRounding.AwayFromZero);
      }

      Gross = _gross;
      Tax1 = _tax1;
      Tax2 = _tax2;
      Tax3 = _tax3;

      return true;
    } //AddPrizeUNR

    //------------------------------------------------------------------------------
    // PURPOSE: Generate Movements UNR
    // 
    //  PARAMS:
    //      - INPUT: OperationID
    //               AccountID
    //               PrizeType
    //               Gross
    //               Tax1
    //               Tax2
    //               AccountMovements
    //               CashierMovements
    //               SqlTrx
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Movements Generated False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean GenerateMovementsUNR(Int64 OperationId, Int64 AccountId,
                                               AccountPromotion.PrizeType PrizeType, Decimal Gross, Decimal Tax1, Decimal Tax2, Decimal Tax3,
                                               AccountMovementsTable AccountMovements,
                                               CashierMovementsTable CashierMovements,
                                               SqlTransaction SqlTrx)
    {
      Decimal _x;
      Decimal _taxes;

      MultiPromos.AccountBalance _bal_incr;
      MultiPromos.AccountBalance _fin_balance;
      Decimal _fin_points;
      Boolean _generate_mov_tax1;
      Boolean _generate_mov_tax2;
      Boolean _generate_mov_tax3;

      // DHA 30-10-2013: Hide taxes if percentage is 0
      _generate_mov_tax1 = GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind.1.Pct", 0m) == 0 ? false : true;
      _generate_mov_tax2 = GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind.2.Pct", 0m) == 0 ? false : true;
      _generate_mov_tax3 = GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind.3.Pct", 0m) == 0 ? false : true;

      try
      {
        _taxes = Tax1 + Tax2 + Tax3;

        switch (PrizeType)
        {
          case AccountPromotion.PrizeType.InKind1_TaxesFromAccount:

            if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, 0, out _fin_balance, out _fin_points, SqlTrx))
            {
              return false;
            }
            _x = _fin_balance.TotalBalance;

            if (_generate_mov_tax1)
            {
              AccountMovements.Add(OperationId, AccountId, MovementType.TaxOnPrize1, _x, Tax1, 0, _x - Tax1);
            }

            _x = _x - Tax1;

            if (_generate_mov_tax2)
            {
              AccountMovements.Add(OperationId, AccountId, MovementType.TaxOnPrize2, _x, Tax2, 0, _x - Tax2);
            }

            _x = _x - Tax2;

            if (_generate_mov_tax3)
            {
              if (Tax3 > 0)
              {
                AccountMovements.Add(OperationId, AccountId, MovementType.TaxOnPrize3, _x, Tax3, 0, _x - Tax3);
              }
            }

            _x = _x - Tax3;

            _bal_incr = new MultiPromos.AccountBalance(-_taxes, 0, 0);
            if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, _bal_incr, 0, out _fin_balance, out _fin_points, SqlTrx))
            {
              return false;
            }

            CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS, Gross, AccountId, "");
            if (_generate_mov_tax1)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1, Tax1, AccountId, "");
            }

            if (_generate_mov_tax2)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2, Tax2, AccountId, "");
            }

            if (_generate_mov_tax3)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3, Tax3, AccountId, "");
            }

            break;

          case AccountPromotion.PrizeType.InKind2_TaxesFromSite:
            CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS, Gross, AccountId, "");

            if (_generate_mov_tax1)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1, Tax1, AccountId, "");
            }

            if (_generate_mov_tax2)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2, Tax2, AccountId, "");
            }

            if (_generate_mov_tax3)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3, Tax3, AccountId, "");
            }

            break;

          case AccountPromotion.PrizeType.None:
          default:
            Log.Error("Unknown PrizeType: " + PrizeType.ToString());

            return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GenerateMovementsUNR

    //------------------------------------------------------------------------------
    // PURPOSE: Generate Movements ReInKind
    // 
    //  PARAMS:
    //      - INPUT: OperationID
    //               AccountID
    //               PrizeType
    //               Gross
    //               Tax1
    //               Tax2
    //               AccountMovements
    //               CashierMovements
    //               SqlTrx
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Movements Generated False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean GenerateMovementsReInKind(Int64 OperationId,
                                                    Int64 AccountId,
                                                    AccountPromotion.PrizeType PrizeType,
                                                    Decimal Gross, Decimal Tax1, Decimal Tax2, Decimal Tax3,
                                           AccountMovementsTable AccountMovements,
                                           CashierMovementsTable CashierMovements,
                                           SqlTransaction SqlTrx)
    {
      Decimal _x;
      Decimal _taxes;

      MultiPromos.AccountBalance _bal_incr;
      MultiPromos.AccountBalance _fin_balance;
      Decimal _fin_points;
      Boolean _generate_mov_tax1;
      Boolean _generate_mov_tax2;
      Boolean _generate_mov_tax3;

      // Hide taxes if percentage is 0
      _generate_mov_tax1 = GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind.1.Pct", 0m) == 0 ? false : true;
      _generate_mov_tax2 = GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind.2.Pct", 0m) == 0 ? false : true;
      _generate_mov_tax3 = GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind.3.Pct", 0m) == 0 ? false : true;

      try
      {
        _taxes = Tax1 + Tax2 + Tax3;

        switch (PrizeType)
        {
          case AccountPromotion.PrizeType.InKind1_TaxesFromAccount:

            // Account Balance
            if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, 0, out _fin_balance, out _fin_points, SqlTrx))
            {
              return false;
            }
            _x = _fin_balance.TotalBalance;

            // Account movements
            if (_generate_mov_tax1)
            {
              AccountMovements.Add(OperationId, AccountId, MovementType.TaxOnPrize1, _x, Tax1, 0, _x - Tax1);
            }

            _x = _x - Tax1;

            if (_generate_mov_tax2)
            {
              AccountMovements.Add(OperationId, AccountId, MovementType.TaxOnPrize2, _x, Tax2, 0, _x - Tax2);
            }

            _x = _x - Tax2;

            if (_generate_mov_tax3)
            {
              if (Tax3 > 0)
              {
                AccountMovements.Add(OperationId, AccountId, MovementType.TaxOnPrize3, _x, Tax3, 0, _x - Tax3);
              }
            }

            _x = _x - Tax3;

            // Account Balance
            _bal_incr = new MultiPromos.AccountBalance(-_taxes, 0, 0);
            if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, _bal_incr, 0, out _fin_balance, out _fin_points, SqlTrx))
            {
              return false;
            }

            // Cashier movements
            CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS, Gross, AccountId, "");
            if (_generate_mov_tax1)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1, Tax1, AccountId, "");
            }

            if (_generate_mov_tax2)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2, Tax2, AccountId, "");
            }

            if (_generate_mov_tax3)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3, Tax3, AccountId, "");
            }
            break;

          case AccountPromotion.PrizeType.InKind2_TaxesFromSite:
            // Cashier movements
            CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS, Gross, AccountId, "");
            if (_generate_mov_tax1)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1, Tax1, AccountId, "");
            }

            if (_generate_mov_tax2)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2, Tax2, AccountId, "");
            }

            if (_generate_mov_tax3)
            {
              CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3, Tax3, AccountId, "");
            }
            break;

          case AccountPromotion.PrizeType.None:
          default:
            Log.Error("RE in Kind: Unknown PrizeType: " + PrizeType.ToString());

            return false;

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GenerateMovementsReInKind
    //------------------------------------------------------------------------------
    // PURPOSE: Generate Movements PrizeMode Cash Prize
    // 
    //  PARAMS:
    //      - INPUT: AccountID
    //               Recharge input parameters
    //               Recharge output parameters
    //               AccountMovements
    //               CashierMovements
    //               SqlTrx
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Movements Generated False: Otherwise.
    // 
    //   NOTES:
    //
    public static bool GenerateMovementsPrizeModeCashPrize(Int64 AccountId,
                                                           RechargeInputParameters InPutParams,
                                                           RechargeOutputParameters OutPutParams,
                                                           AccountMovementsTable AccountMovements,
                                                           CashierMovementsTable CashierMovements,
                                                           SqlTransaction SqlTrx)
    {
      MultiPromos.AccountBalance _initBalance;
      decimal _initBalance_aux;
      decimal _finalBalance;
      decimal _add;
      decimal _sub_promo_re;
      Currency _baseTax;
      Currency _tax;
      StringBuilder _sb;

      OutPutParams.CashDeskDrawResult.CashPrizeMode = GetOutCashPrizeMode(OutPutParams);
      Accounts.CalculateSplit2Tax(OutPutParams.CashDeskDrawResult.CashPrizeMode.ServiceCharge, out _baseTax, out _tax);

      if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _initBalance, SqlTrx))
      {
        return false;
      }

      _initBalance_aux = _initBalance.TotalBalance;
      _finalBalance = _initBalance_aux;
      _add = 0;
      _sub_promo_re = 0;

      if (OutPutParams.CashDeskDrawResult.m_is_winner)
      {
        _initBalance_aux = _initBalance.TotalBalance;
        _finalBalance = _initBalance_aux - OutPutParams.CashDeskDrawResult.CashPrizeMode.NetPrize;
        AccountMovements.Add(OutPutParams.OperationId, AccountId, MovementType.CashOut, _initBalance_aux, OutPutParams.CashDeskDrawResult.CashPrizeMode.GrossPrize, OutPutParams.CashDeskDrawResult.CashPrizeMode.GrossPrize - OutPutParams.CashDeskDrawResult.CashPrizeMode.NetPrize, _finalBalance);

        _initBalance_aux = _finalBalance;
        _finalBalance = _initBalance_aux - OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax1.TaxAmount;
        AccountMovements.Add(OutPutParams.OperationId, AccountId, MovementType.TaxOnPrize1, _initBalance_aux, OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax1.TaxAmount, 0, _finalBalance);

        _initBalance_aux = _finalBalance;
        _finalBalance = _initBalance_aux - OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax2.TaxAmount;
        AccountMovements.Add(OutPutParams.OperationId, AccountId, MovementType.TaxOnPrize2, _initBalance_aux, OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax2.TaxAmount, 0, _finalBalance);

        _initBalance_aux = _finalBalance;
        _finalBalance = _initBalance_aux - OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax3.TaxAmount;

        if (OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax3.TaxAmount > 0)
        {
          AccountMovements.Add(OutPutParams.OperationId, AccountId, MovementType.TaxOnPrize3, _initBalance_aux, OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax3.TaxAmount, 0, _finalBalance);
        }

        _initBalance_aux = _finalBalance;
        _finalBalance = _initBalance_aux - OutPutParams.CashDeskDrawResult.CashPrizeMode.ServiceCharge;
        AccountMovements.Add(OutPutParams.OperationId, AccountId, MovementType.ServiceCharge, _initBalance_aux, OutPutParams.CashDeskDrawResult.CashPrizeMode.ServiceCharge, 0, _finalBalance);

        _initBalance_aux = _finalBalance;
        _finalBalance = _initBalance_aux + OutPutParams.CashDeskDrawResult.CashPrizeMode.NetPrize;
        AccountMovements.Add(OutPutParams.OperationId, AccountId, MovementType.CashIn, _initBalance_aux, 0, OutPutParams.CashDeskDrawResult.CashPrizeMode.NetPrize, _finalBalance);

        if (OutPutParams.CashDeskDrawResult.m_is_winner)
        {
          _sub_promo_re = OutPutParams.CashDeskDrawResult.CashPrizeMode.GrossPrize;
        }

        _add = OutPutParams.CashDeskDrawResult.CashPrizeMode.NetPrize - OutPutParams.CashDeskDrawResult.CashPrizeMode.GrossPrize;
        _add += _sub_promo_re;

        _initBalance = new MultiPromos.AccountBalance(_add, -_sub_promo_re, 0);

        if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, _initBalance, out _initBalance, SqlTrx))
        {
          return false;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   ACCOUNTS ");
        _sb.AppendLine("   SET   AC_INITIAL_CASH_IN = AC_INITIAL_CASH_IN + @pDevolution ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID      = @pAccountId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pDevolution", SqlDbType.Money).Value = OutPutParams.CashDeskDrawResult.CashPrizeMode.NetPrize - OutPutParams.CashDeskDrawResult.m_participation_amount;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("Cash Prize mode: Update Initial CashIn. AccountId: " + AccountId);

            return false;
          }
        }

        CashierMovements.Add(OutPutParams.OperationId, CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER, OutPutParams.CashDeskDrawResult.CashPrizeMode.GrossPrize, AccountId, InPutParams.ExternalTrackData);

        if (OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax1.TaxPct > 0)
        {
          CashierMovements.Add(OutPutParams.OperationId, CASHIER_MOVEMENT.TAX_ON_PRIZE1, OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax1.TaxAmount, AccountId, InPutParams.ExternalTrackData);
        }

        if (OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax2.TaxPct > 0)
        {
          CashierMovements.Add(OutPutParams.OperationId, CASHIER_MOVEMENT.TAX_ON_PRIZE2, OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax2.TaxAmount, AccountId, InPutParams.ExternalTrackData);
        }

        if (OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax3.TaxAmount > 0)
        {
          CashierMovements.Add(OutPutParams.OperationId, CASHIER_MOVEMENT.TAX_ON_PRIZE3, OutPutParams.CashDeskDrawResult.CashPrizeMode.Tax3.TaxAmount, AccountId, InPutParams.ExternalTrackData);
        }

        if (GeneralParam.GetString("Cashier.ServiceCharge", "AttributedToCompany", "B").ToUpper() == "B")
        {
          CashierMovements.Add(OutPutParams.OperationId, CASHIER_MOVEMENT.SERVICE_CHARGE, OutPutParams.CashDeskDrawResult.CashPrizeMode.ServiceCharge, AccountId, InPutParams.ExternalTrackData, "", "", 0, 0, 0, _tax);
        }
        else
        {
          CashierMovements.Add(OutPutParams.OperationId, CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A, OutPutParams.CashDeskDrawResult.CashPrizeMode.ServiceCharge, AccountId, InPutParams.ExternalTrackData, "", "", 0, 0, 0, _tax);
        }

        CashierMovements.Add(OutPutParams.OperationId, CASHIER_MOVEMENT.CASH_IN_SPLIT1, OutPutParams.CashDeskDrawResult.CashPrizeMode.NetPrize, AccountId, InPutParams.ExternalTrackData);
      }

      return true;
    } //GenerateMovementsPrizeModeCashPrize

    //------------------------------------------------------------------------------
    // PURPOSE: Generate Movements PrizeMode Cash Prize
    // 
    //  PARAMS:
    //      - INPUT: Recharge output parameters
    //
    //      - OUTPUT: OutCashPrizeMode
    //
    // RETURNS: True: OutCashPrizeMode False: Otherwise.
    // 
    //   NOTES:
    //     EOR 12-OCT-2016
    //
    public static OutCashPrizeMode GetOutCashPrizeMode(RechargeOutputParameters OutPutParams)
    {
      OutCashPrizeMode _outCashPrize;
      DevolutionPrizeParts _prizeParts;

      _outCashPrize = new OutCashPrizeMode();

     
      if (OutPutParams.CashDeskDrawResult.m_is_winner)
      {
        _prizeParts = new DevolutionPrizeParts(ENUM_TAX_TYPE.TAX_DRAW_CASH_PRIZE);
        _outCashPrize.GrossPrize = OutPutParams.CashDeskDrawResult.m_re_gross;

        _prizeParts.Compute(OutPutParams.CashDeskDrawResult.m_re_gross, 0, true, true);

        _outCashPrize.Tax1 = _prizeParts.Tax1;
        _outCashPrize.Tax2 = _prizeParts.Tax2;
        _outCashPrize.Tax3 = _prizeParts.Tax3;

        _outCashPrize.ServiceCharge = _prizeParts.ServiceCharge;
      }
      else
      {
        if (OutPutParams.CashDeskDrawResult.m_re_gross > 0)
        {
          _outCashPrize.GrossPrize = Math.Max(OutPutParams.CashDeskDrawResult.m_re_gross, OutPutParams.CashDeskDrawResult.m_re_awarded);
        }
        else
        {
          _outCashPrize.GrossPrize = Math.Max(OutPutParams.CashDeskDrawResult.m_nr_gross, OutPutParams.CashDeskDrawResult.m_nr_awarded);
        }
      }

      _outCashPrize.NetPrize = _outCashPrize.GrossPrize - _outCashPrize.Tax1.TaxAmount - _outCashPrize.Tax2.TaxAmount - _outCashPrize.Tax3.TaxAmount - _outCashPrize.ServiceCharge;
      _outCashPrize.Awarded = _outCashPrize.NetPrize;

      //EOR 12-OCT-2016
      _outCashPrize.Winner = OutPutParams.CashDeskDrawResult.m_is_winner;

      return _outCashPrize;
    } // GetOutCashPrizeMode

    //------------------------------------------------------------------------------
    // PURPOSE: Generate Conditional Cashdesk Draw
    // 
    //  PARAMS:
    //      - INPUT: OperationID
    //               Recharge input parameters
    //               Promotions
    //               AccountMovements
    //               CashierMovements
    //               SqlTrx
    //               Split A
    //               Split B
    //               ProvisioningTax
    //               NR2
    //               AccountPromotion: Promotion selected
    //
    //      - OUTPUT: Recharge output parameters
    //                PlaySessionID
    //
    // RETURNS: True: Draw Generated False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean ConditionallyCashDeskDraw(Int64 OperationId,
                                                    RechargeInputParameters InputParameters,
                                                    Currency RedeemableAmountToAdd,
                                                    DataTable Promotions,
                                                    AccountMovementsTable AccountMovements,
                                                    CashierMovementsTable CashierMovements,
                                                    SqlTransaction SqlTrx,
                                                    Currency SplitA,
                                                    Currency SplitB,
                                                    Currency ProvisioningTax,
                                                    Currency NR2,
                                                    AccountPromotion AccountPromotion,
                                                    out RechargeOutputParameters OutputParameters,
                                                    out Int64 PlaySessionId)
    {
      CashDeskDraw _draw;
      AccountPromotion _aux_promotion;
      Promotion.PROMOTION_TYPE _promo_nr;
      Promotion.PROMOTION_TYPE _promo_re;
      Promotion.PROMOTION_TYPE _promo_prize_coupon;
      Boolean _continue;
      Currency _won_amount;
      bool _isEnabledCashDeskDraws;
      MultiPromos.AccountBalance _init_balance;
      MultiPromos.AccountBalance _charge_draw;
      MultiPromos.AccountBalance _final_balance;
      Currency _amount_awarded;
      Boolean _is_player_session;
      Boolean _is_terminal_draw;

      MultiPromos.AccountBalance _draw_prize;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _fin_balance;

      Int32 _cashdesk_draw_terminal_id;
      String _cashdesk_draw_terminal_name;

      Int64 _play_session_id;
      Decimal _fin_points;
      Int64 _terminal_id;

      PlaySessionId = 0;
      OutputParameters = new RechargeOutputParameters(OperationId);

      _is_player_session = false;
      _is_terminal_draw = false;

      _amount_awarded = 0;
      _ini_balance = null;
      _fin_balance = null;
      _cashdesk_draw_terminal_id = 0;
      _cashdesk_draw_terminal_name = null;
      _is_player_session = false;
      _is_terminal_draw = false;

      _draw = null;
      _promo_prize_coupon = Promotion.PROMOTION_TYPE.UNKNOWN;
      _continue = true;
      _charge_draw = MultiPromos.AccountBalance.Zero;
      _final_balance = MultiPromos.AccountBalance.Zero;
      _init_balance = MultiPromos.AccountBalance.Zero;

      _isEnabledCashDeskDraws = IsEnabledCashDeskDraws(Convert.ToInt16(InputParameters.CashDeskDraw));

      // RCI & DDM 01-JUL-2014: If not enabled, always return PRIZE_COUPON.
      if (!_isEnabledCashDeskDraws) //LTC 22-SEP-2016
      {
        _promo_prize_coupon = Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON;
      }
      else
      {
        if (InputParameters.ParticipateInCashDeskDraw == ParticipateInCashDeskDraw.No)
        {
          _promo_prize_coupon = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO;
        }
        else
        {
          // Draw Enabled & Player participates on the draw (choosed "Participated" or "Default" on MB, eBOX, etc.)
          if (!CashDeskDrawBusinessLogic.GenerateCashdeskDraw(OperationId, InputParameters.AccountId, SplitA, SplitB, ProvisioningTax, out _draw, out OutputParameters.ErrorCode, out OutputParameters.ErrorMessage, InputParameters.CashDeskDraw, SqlTrx)) // LTC 11-OCT-2016  
          {
            // Draw failed: Server Down
            switch (CashDeskDrawBusinessLogic.GetActionOnServerDown(Convert.ToInt16(InputParameters.CashDeskDraw))) //LTC 22-SEP-2016
            {
              case ActionOnServerDown.PrizeCoupon:
                _promo_prize_coupon = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN;
                break;

              case ActionOnServerDown.RechargeDisabled:
              default:
                Log.Warning("ConditionallyCashDeskDraw: ActionOnServerDown.RechargeDisabled");

                return false;
            }
          }
        }
      }

      if (_promo_prize_coupon != Promotion.PROMOTION_TYPE.UNKNOWN)
      {
        _continue = false;

        if (SplitB > 0)
        {
          if (!Promotion.ReadPromotionData(SqlTrx, _promo_prize_coupon, out _aux_promotion))
          {
            Log.Error(string.Format("ConditionallyCashDeskDraw: Promotion.ReadPromotionData(0). Promotion {0}", _promo_prize_coupon.ToString()));

            return false;
          }

          if (!AccountPromotion.InsertPromotionToAccount(OperationId, InputParameters.AccountId, InputParameters.AccountLevel, RedeemableAmountToAdd, 0, true, SplitB, _aux_promotion, AccountPromotion, Promotions))
          {
            Log.Error(string.Format("ConditionallyCashDeskDraw: AccountPromotion.InsertPromotionToAccount(0)." +
                                      "OperationId {0}; AccountId: {1} ",
                                       OperationId.ToString(), InputParameters.AccountId.ToString()));

            return false;
          }
        }
      }

      if (NR2 > 0)
      {
        if (!Promotion.ReadPromotionData(SqlTrx, Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON, out _aux_promotion))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: Promotion.ReadPromotionData(1). Promotion {0}", Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON.ToString()));

          return false;
        }

        if (CashDeskDrawBusinessLogic.IsCashPrizeCashDeskDraw(InputParameters.CashDeskDraw))
        {
          if (!_isEnabledCashDeskDraws || InputParameters.ParticipateInCashDeskDraw == ParticipateInCashDeskDraw.No)
          {
            if (!AccountPromotion.InsertPromotionToAccount(OperationId, InputParameters.AccountId, InputParameters.AccountLevel, RedeemableAmountToAdd, 0, true, NR2, _aux_promotion, AccountPromotion, Promotions))
            {
              Log.Error(string.Format("ConditionallyCashDeskDraw: AccountPromotion.InsertPromotionToAccount(1)." +
                                      "OperationId {0}; AccountId: {1}; IsCashPrizeCashDeskDraw: {2} ",
                                       OperationId.ToString(), InputParameters.AccountId.ToString(), "True"));

              return false;
            }
          }
        }
        else
        {
          if (!AccountPromotion.InsertPromotionToAccount(OperationId, InputParameters.AccountId, InputParameters.AccountLevel, RedeemableAmountToAdd, 0, true, NR2, _aux_promotion, AccountPromotion, Promotions))
          {
            Log.Error(string.Format("ConditionallyCashDeskDraw: AccountPromotion.InsertPromotionToAccount(2). " +
                                      "OperationId {0}; AccountId: {1}; IsCashPrizeCashDeskDraw: {2} ",
                                       OperationId.ToString(), InputParameters.AccountId.ToString(), "False"));

            return false;
          }
        }
      }

      if (!_continue)
      {
        return true;
      }

      // Draw performed!
      if (_draw == null)
      {
        Log.Error(string.Format("ConditionallyCashDeskDraw: Draw Null"));

        return false;
      }

      // 1: ->PlaySession or 3: -> Machine;  Others: -> Promotion
      _is_player_session = (CashDeskDrawBusinessLogic.GetPrizeMode(_draw.DrawId) == PrizeMode.PlayerSession);
      _is_terminal_draw = (CashDeskDrawBusinessLogic.GetPrizeMode(_draw.DrawId) == PrizeMode.Terminal);

      // Possible exception if _draw is null, added trap above
      OutputParameters.CashDeskDrawResult = _draw.Result;

      if (CashDeskDrawBusinessLogic.IsCashPrizeCashDeskDraw(InputParameters.CashDeskDraw) || CashDeskDrawBusinessLogic.IsPlayerSessionCashDeskDraw(InputParameters.CashDeskDraw))
      {
        _charge_draw = new MultiPromos.AccountBalance(CashDeskDrawBusinessLogic.GetParticipationPrice((short)InputParameters.CashDeskDraw) * -1, 0, 0);
      }
      else if (CashDeskDrawBusinessLogic.IsTerminalDraw(InputParameters.CashDeskDraw)) // calculate participation ciost percentage for terminal draw
      {
        _charge_draw = new MultiPromos.AccountBalance(Math.Min(((CashDeskDrawBusinessLogic.GetParticipationPrice((short)InputParameters.CashDeskDraw) / (Decimal)100) * SplitA), GetMaxParticipationPrice((short)InputParameters.CashDeskDraw)) * -1, 0, 0);
      }

      // DRV 01-AUG-2014: When ChipSaleRegister, don't insert cashier movements.
      if (!InputParameters.ChipSaleRegister)
      {
        CashierMovements.Add(OperationId, CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW, _charge_draw.TotalBalance * -1, InputParameters.AccountId, InputParameters.ExternalTrackData);
      }

      if (CashDeskDrawBusinessLogic.IsCashPrizeCashDeskDraw(InputParameters.CashDeskDraw) ||
          CashDeskDrawBusinessLogic.IsPlayerSessionCashDeskDraw(InputParameters.CashDeskDraw) ||
          CashDeskDrawBusinessLogic.IsTerminalDraw(InputParameters.CashDeskDraw))
      {
        if (!MultiPromos.Trx_UpdateAccountBalance(InputParameters.AccountId, MultiPromos.AccountBalance.Zero, out _init_balance, SqlTrx))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: MultiPromos.Trx_UpdateAccountBalance(1). " +
                                  "AccountId: {0}; BalanceRedeem: {1} BalanceToAdd: {2}",
                                  InputParameters.AccountId.ToString(), _init_balance.TotalRedeemable, MultiPromos.AccountBalance.Zero.ToString()));

          return false;
        }

        if (!MultiPromos.Trx_UpdateAccountBalance(InputParameters.AccountId, _charge_draw, out _final_balance, SqlTrx))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: MultiPromos.Trx_UpdateAccountBalance(2). " +
                                  "AccountId: {0}; BalanceRedeem: {1} BalanceToAdd: {2}",
                                  InputParameters.AccountId.ToString(), _final_balance.TotalRedeemable, _charge_draw.TotalBalance.ToString()));

          return false;
        }

        OutputParameters.CashDeskDrawResult.m_participation_amount = _charge_draw.TotalBalance * -1;

        // XGJ 24-FEB-2017
        if (Misc.IsTerminalDrawEnabled())
        {
          if (!Misc.GetTerminalName(InputParameters.TerminalId, out _cashdesk_draw_terminal_name, SqlTrx))
          {
            _cashdesk_draw_terminal_name = "";
          }

          AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.StartCardSession, _init_balance.TotalBalance, _charge_draw.TotalBalance * -1, 0, _final_balance.TotalBalance, "", InputParameters.TerminalId, _cashdesk_draw_terminal_name);
        }
        else
        {
        AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.CashdeskDrawParticipation, _init_balance.TotalBalance, _charge_draw.TotalBalance * -1, 0, _final_balance.TotalBalance);
      }
      }
      else
      {
        if (!Misc.IsTerminalDrawEnabled())
        {
        AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.CashdeskDrawParticipation, 0, 0, 0, 0);
      }
        else
        {
          AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.StartCardSession, 0, 0, 0, 0);
        }
      }

      #region LTC 22-SEP-2016
      //LTC 22-SEP-2016
      if (_is_player_session || _is_terminal_draw)
      {
        _draw.Parameters.m_total_cash_in_company_a = SplitA;

        if (_draw.DrawId == (short)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02)
        {
          _draw.Parameters.m_terminal_id = InputParameters.TerminalId;
          _draw.Parameters.m_play_session_id = InputParameters.PlaySessionId;
        }

        if (!Trx_InsertPlaySession(_draw, out _play_session_id, out _terminal_id, InputParameters.CashDeskDraw, SqlTrx))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: Trx_InsertPlaySession."));

          return false;
        }

        PlaySessionId = _play_session_id;

        // [XGJ][FJC][SJA] 23-MAR-2017 Fixed Bug 26082:Sorteo de M�quina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado.
        //if (_is_terminal_draw && !Trx_DecreaseInitialCashInBySplitB(InputParameters.AccountId, SplitB, SqlTrx))
        //{
        //  Log.Error("CashDeskDrawBusinessLogic.ConditionallyCashDeskDraw.Trx_DecreaseInitialCashInBySplitB failed!");

        //  return false;
        //}

        //Update CasDeskSraw Terminal, Session
        if (_draw != null)
        {
          Trx_UpdateCashDeskDraw(_draw, _play_session_id, _terminal_id, SqlTrx);
        }

        _won_amount = 0;
        if (_draw.Result.m_is_winner)
        {
          _won_amount = _draw.Result.m_nr_awarded + _draw.Result.m_re_awarded;
        }

        AccountMovements.SetPlaySessionId(_play_session_id);

        _draw_prize = new MultiPromos.AccountBalance(0, 0, 0);
        if (_draw.Result.m_is_winner && _draw.Result.m_re_awarded > 0)
        {
          if (_is_terminal_draw)
          {
            _draw_prize = new MultiPromos.AccountBalance(_draw.Result.m_re_awarded, 0, 0);
          }
          else
          {
            _draw_prize = new MultiPromos.AccountBalance(0, _draw.Result.m_re_awarded, 0);
          }
        }

        if (!MultiPromos.Trx_UpdateAccountBalance(InputParameters.AccountId, _draw_prize, 0, out _fin_balance, out _fin_points, SqlTrx))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: MultiPromos.Trx_UpdateAccountBalance(3). " +
                                  "AccountId: {0}; BalanceRedeem: {1} BalanceToAdd: {2}",
                                  InputParameters.AccountId.ToString(), _final_balance.TotalRedeemable, _draw_prize.TotalBalance.ToString()));

          return false;
        }

        if (_draw.Result.m_is_winner && _draw.Result.m_nr_awarded > 0)
        {
          // Create promotion not visible. Play session with no redeemable won!
          if (!Promotion.ReadPromotionData(SqlTrx, Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN, out _aux_promotion))
          {
            Log.Error(string.Format("ConditionallyCashDeskDraw: Promotion.ReadPromotionData(2). Promotion {0}", Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN.ToString()));

            return false;
          }

          if (!AccountPromotion.InsertPromotionToAccount(OperationId, InputParameters.AccountId, InputParameters.AccountLevel, RedeemableAmountToAdd, 0, true, _draw.Result.m_nr_awarded, _aux_promotion, AccountPromotion, Promotions))
          {
            Log.Error(string.Format("ConditionallyCashDeskDraw: AccountPromotion.InsertPromotionToAccount(3). " +
                                    "OperationId {0}; AccountId: {1}",
                                    OperationId.ToString(), InputParameters.AccountId.ToString()));

            return false;
          }

          _draw_prize = new MultiPromos.AccountBalance(0, 0, _draw.Result.m_nr_awarded);

          _fin_balance += _draw_prize;
        }

        _ini_balance = _fin_balance - _draw_prize;

        if (!GetCashDeskDrawTerminalData(TerminalTypes.CASHDESK_DRAW, out _cashdesk_draw_terminal_id, out _cashdesk_draw_terminal_name, SqlTrx))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: GetCashDeskDrawTerminalData()."));

          return false;
        }

        // Always if is player session or is winner
        if (_is_player_session || _draw.Result.m_is_winner)
        {
            //XGJ 24-FEB-2017
            if (CashDeskDrawBusinessLogic.GetPrizeMode(_draw.DrawId) == PrizeMode.Terminal)
            {

              if (!Misc.IsTerminalDrawEnabled())
              {
              AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.CashdeskDrawPlaySession,
                _ini_balance.TotalBalance, 0, _won_amount,
                _fin_balance.TotalBalance, "", _cashdesk_draw_terminal_id, _cashdesk_draw_terminal_name);
              }
              else
              {
                if (!Misc.GetTerminalName(InputParameters.TerminalId, out _cashdesk_draw_terminal_name, SqlTrx))
                {
                  _cashdesk_draw_terminal_name = "";
                }

                AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.EndCardSession,
                               _ini_balance.TotalBalance, 0, _won_amount,
                               _fin_balance.TotalBalance, "", InputParameters.TerminalId, _cashdesk_draw_terminal_name);
              }
            }
            else
            {
              if (!Misc.IsTerminalDrawEnabled())
              {
                if (!Misc.GetTerminalName(InputParameters.TerminalId, out _cashdesk_draw_terminal_name, SqlTrx))
                {
                  _cashdesk_draw_terminal_name = "";
                }

              AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.CashdeskDrawPlaySession,
                _ini_balance.TotalBalance, _draw.Parameters.m_bet_amount, _draw.Parameters.m_bet_amount + _won_amount,
                _fin_balance.TotalBalance, "", _cashdesk_draw_terminal_id, _cashdesk_draw_terminal_name);
          }
              else
              {
                AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.EndCardSession,
                       _ini_balance.TotalBalance, _draw.Parameters.m_bet_amount, _draw.Parameters.m_bet_amount + _won_amount,
                       _fin_balance.TotalBalance, "", InputParameters.TerminalId, _cashdesk_draw_terminal_name);
        }
            }
          }

        if (_draw.Result.m_is_winner)
        {
          return true;
        }

        // Loser: continue to award the loser promotion
      }
      #endregion

      if (_draw.Result.m_is_winner)
      {
        _promo_re = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE;
        _promo_nr = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR;

        if (_draw.Result.m_prize_category.m_prize_type == PrizeType.UNR
            || _draw.Result.m_prize_category.m_prize_type == PrizeType.NoRedeemable2)
        {
          _promo_nr = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND;
        }
        else if (_draw.Result.m_prize_category.m_prize_type == PrizeType.ReInKind)
        {
          _promo_re = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND;
        }
      }
      else
      {
        // Loser
        // The prize category is alredy set on losers.
        _promo_re = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE;
        _promo_nr = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR;

        if (_draw.Result.m_prize_category.m_prize_type == PrizeType.UNR
            || _draw.Result.m_prize_category.m_prize_type == PrizeType.NoRedeemable2)
        {
          _promo_nr = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND;
        }
      }

      if (_draw.Result.m_nr_awarded > 0)
      {
        if (!Promotion.ReadPromotionData(SqlTrx, _promo_nr, out _aux_promotion))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: Promotion.ReadPromotionData(3). " +
                                    "OperationId {0}; AccountId: {1}",
                                    OperationId.ToString(), InputParameters.AccountId.ToString()));
          
          return false;
        }

        if (!AccountPromotion.InsertPromotionToAccount(OperationId, InputParameters.AccountId,
            InputParameters.AccountLevel, RedeemableAmountToAdd, 0, true,
            _draw.Result.m_nr_awarded + (_is_terminal_draw ? Math.Abs(_charge_draw.Redeemable) : 0), _aux_promotion, AccountPromotion,
            Promotions))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: AccountPromotion.InsertPromotionToAccount(4). " +
                                  "OperationId {0}; AccountId: {1}; ChargeTerminalDraw: {2}",
                                  OperationId.ToString(), InputParameters.AccountId.ToString(),
                                  _charge_draw.Redeemable.ToString()));

          return false;
        }

        // Promo is inserted on first position of the table 
        _draw.Result.m_unr_tax1 = Promotions.Rows[0]["ACP_PRIZE_TAX1"] == DBNull.Value ? 0 : (Decimal)Promotions.Rows[0]["ACP_PRIZE_TAX1"];

        _draw.Result.m_unr_tax2 = Promotions.Rows[0]["ACP_PRIZE_TAX2"] == DBNull.Value ? 0 : (Decimal)Promotions.Rows[0]["ACP_PRIZE_TAX2"];

        _draw.Result.m_unr_tax3 = Promotions.Rows[0]["ACP_PRIZE_TAX3"] == DBNull.Value ? 0 : (Decimal)Promotions.Rows[0]["ACP_PRIZE_TAX3"];
      }

      if (_draw.Result.m_re_awarded > 0)
      {
        if (!Promotion.ReadPromotionData(SqlTrx, _promo_re, out _aux_promotion))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: Promotion.ReadPromotionData(4). " +
                                   "OperationId {0}; AccountId: {1}",
                                   OperationId.ToString(), InputParameters.AccountId.ToString()));
          
          return false;
        }

        _amount_awarded = _draw.Result.m_re_awarded;

        if ((CashDeskDrawBusinessLogic.GetPrizeMode(_draw.DrawId) == PrizeMode.CashPrize || _is_terminal_draw) && _draw.Result.m_is_winner)
        {
          _amount_awarded = _draw.Result.m_re_gross;
        }

        if (_is_terminal_draw)
        {
          _amount_awarded += Math.Min(((CashDeskDrawBusinessLogic.GetParticipationPrice((short)InputParameters.CashDeskDraw) / (Decimal)100) * SplitA), GetMaxParticipationPrice((short)InputParameters.CashDeskDraw));
        }

        if (!AccountPromotion.InsertPromotionToAccount(OperationId, InputParameters.AccountId, InputParameters.AccountLevel, RedeemableAmountToAdd, 0, true, _amount_awarded, _aux_promotion, AccountPromotion, Promotions))
        {
          Log.Error(string.Format("ConditionallyCashDeskDraw: AccountPromotion.InsertPromotionToAccount(5). " +
                                  "OperationId {0}; AccountId: {1}; AmountAwarded: {2}",
                                  OperationId.ToString(), InputParameters.AccountId.ToString(),
                                  _amount_awarded.ToString()));
          
          return false;
        }

        // Tax for redimible in kind
        if ((ACCOUNT_PROMO_CREDIT_TYPE)Promotions.Rows[0]["ACP_CREDIT_TYPE"] == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND)
        {
          _draw.Result.m_ure_tax1 = Promotions.Rows[0]["ACP_PRIZE_TAX1"] == DBNull.Value ? 0 : (Decimal)Promotions.Rows[0]["ACP_PRIZE_TAX1"];
          _draw.Result.m_ure_tax2 = Promotions.Rows[0]["ACP_PRIZE_TAX2"] == DBNull.Value ? 0 : (Decimal)Promotions.Rows[0]["ACP_PRIZE_TAX2"];
          _draw.Result.m_ure_tax3 = Promotions.Rows[0]["ACP_PRIZE_TAX3"] == DBNull.Value ? 0 : (Decimal)Promotions.Rows[0]["ACP_PRIZE_TAX3"];
        }
      }

      // Only add movements in terminal draw (loser)
      //  - In winner case, the execution does not reach this line
      if (_is_terminal_draw)
      {
        // Add Play session account movement. 

        // XGJ 24-02-2017
        if (!Misc.IsTerminalDrawEnabled())
        {
        AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.CashdeskDrawPlaySession, _ini_balance.TotalBalance, 0, 0, _ini_balance.TotalBalance, "", _cashdesk_draw_terminal_id, _cashdesk_draw_terminal_name);
        }
        else
        {
          if (!Misc.GetTerminalName(InputParameters.TerminalId, out _cashdesk_draw_terminal_name, SqlTrx))
          {
            _cashdesk_draw_terminal_name = "";
          }
          
          AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.EndCardSession, _ini_balance.TotalBalance, 0, 0, _ini_balance.TotalBalance, "", InputParameters.TerminalId, _cashdesk_draw_terminal_name);
        }
      }

      return true;
    } //ConditionallyCashDeskDraw

    /// <summary>
    /// Decrease Initial Cash in off account id whith SplitB
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="SplitB"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean Trx_DecreaseInitialCashInBySplitB(Int64 AccountId, Currency SplitB, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   ACCOUNTS                                            ");
        _sb.AppendLine("   SET   AC_INITIAL_CASH_IN = AC_INITIAL_CASH_IN - @pSplitB  ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID      = @pAccountId                    ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pSplitB", SqlDbType.Money).Value = (Decimal)SplitB;
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          if (_sql_command.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (SqlException _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return false;
    } // Trx_DecreaseInitialCashInBySplitB

    //------------------------------------------------------------------------------
    // PURPOSE: Establish the connexion with server address 1 or server address 2
    // 
    //  PARAMS:
    //      - INPUT: CashierDraw
    //               CashDeskId
    //
    //      - OUTPUT:
    //
    // RETURNS: True: Establish the connexion. False: Otherwise.
    // 
    //   NOTES:
    //
    //     LTC 22-SEP-2016
    private static Boolean ExternalCashDeskDraw(Parameters CashierDraw, Int16 CashDeskId)
    {
      String _cash_desk_post_fij;
      _cash_desk_post_fij = GetCashDeskPostfij(CashDeskId);

      if (String.IsNullOrEmpty(GeneralParam.GetString("CashDesk.Draw" + _cash_desk_post_fij, "ServerAddress1"))
       && String.IsNullOrEmpty(GeneralParam.GetString("CashDesk.Draw" + _cash_desk_post_fij, "ServerAddress2")))
      {
        return false;
      }

      //1 CONECT TO WS (ServerAddress1 Or ServerAddress2)
      return ConnectToWS(CashierDraw);
    } // ExternalCashDeskDraw

    /// <summary>
    /// Connect to external WS
    /// </summary>
    /// <param name="CashierDraw"></param>
    /// <returns></returns>
    private static Boolean ConnectToWS(Parameters CashierDraw)
    {
      //Not implemented
      Log.Warning("CashDeskDrawBusinessLogic.ConnectToWS - Not Implemented!!!!!");

      return false;
    } // ConnectToWS

    //------------------------------------------------------------------------------
    // PURPOSE: inserts the draw data on a table of an external DataBase
    // 
    //  PARAMS:
    //      - INPUT: ConnectionString
    //               CashDeskDraw
    //
    //      - OUTPUT:
    //
    // RETURNS: True: Inserted ok. False: Otherwise.
    // 
    //   NOTES:
    //
    private static Boolean SaveDrawToExternalDataBase(String ConnectionString, CashDeskDraw Draw)
    {
      // insert in remote server
      try
      {
        using (SqlConnection _sql_conn = new SqlConnection(ConnectionString))
        {
          _sql_conn.Open();

          using (SqlTransaction _trx = _sql_conn.BeginTransaction())
          {
            if (SaveDraw(Draw, _trx))
            {
              _trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SaveDrawToExternalDataBase

    //------------------------------------------------------------------------------
    // PURPOSE: insert Cashdesk_Draws
    // 
    //  PARAMS:
    //      - INPUT: CashierDraws
    //               SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS: - True: Inserted ok. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean SaveDraw(CashDeskDraw Draw, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int64 _draw_id;
      PrizeMode _prize_mode;
      long _terminal_id;

      _prize_mode = CashDeskDrawBusinessLogic.GetPrizeMode(Draw.DrawId); // LTC 04-OCT-2016
      _terminal_id = CashDeskDrawBusinessLogic.GetSorterTerminalID(Draw.DrawId, SqlTrx);

      try
      {
        if (Draw.Result.m_draw_id == 0)
        {
          if (!Sequences.GetValue(SqlTrx, SequenceId.CashDeskDraw, out _draw_id))
          {
            return false;
          }

          Draw.Result.m_draw_id = _draw_id * 1000 + GeneralParam.GetInt32("Site", "Identifier", 0) % 1000;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   CASHDESK_DRAWS     ");
        _sb.AppendLine("             ( CD_SITE_ID         ");
        _sb.AppendLine("             , CD_DRAW_ID         ");
        _sb.AppendLine("             , CD_OPERATION_ID    ");
        _sb.AppendLine("             , CD_ACCOUNT_ID      ");
        _sb.AppendLine("             , CD_DRAW_DATETIME   ");
        _sb.AppendLine("             , CD_COMBINATION_BET ");
        _sb.AppendLine("             , CD_COMBINATION_WON ");
        _sb.AppendLine("             , CD_NR_BET          ");
        _sb.AppendLine("             , CD_NR_WON          ");
        _sb.AppendLine("             , CD_RE_BET          ");
        _sb.AppendLine("             , CD_RE_WON          ");
        _sb.AppendLine("             , CD_PRIZE_ID        ");
        _sb.AppendLine("             , CD_UPLOAD_TO_EXT_DB");
        _sb.AppendLine("             , CD_TERMINAL        "); //LTC 22-SEP-2016
        _sb.AppendLine("             , CD_SESSION_ID     )");
        _sb.AppendLine("      VALUES                      ");
        _sb.AppendLine("             ( @pSiteId           ");
        _sb.AppendLine("             , @pDrawId      ");
        _sb.AppendLine("             , @pOperationId      ");
        _sb.AppendLine("             , @pAccountId        ");
        _sb.AppendLine("             , @pDrawDateTime     ");
        _sb.AppendLine("             , @pCombinationBet   ");
        _sb.AppendLine("             , @pCombinationWon   ");
        _sb.AppendLine("             , @pNrBet            ");
        _sb.AppendLine("             , @pNrWon            ");
        _sb.AppendLine("             , @pReBet            ");
        _sb.AppendLine("             , @pReWon            ");
        _sb.AppendLine("             , @pPrizeId          ");
        _sb.AppendLine("             , @pUploadToExtDB    "); //LTC 22-SEP-2016
        _sb.AppendLine("             , @pTerminal         ");
        _sb.AppendLine("             , @pSessionId)       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pDrawId", SqlDbType.BigInt).Value = Draw.Result.m_draw_id;
          _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Draw.Parameters.m_site_id;
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = Draw.Parameters.m_operation_id;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Draw.Parameters.m_account_id;
          _sql_cmd.Parameters.Add("@pDrawDateTime", SqlDbType.DateTime).Value = Draw.Result.m_date_draw;
          _sql_cmd.Parameters.Add("@pCombinationBet", SqlDbType.NVarChar).Value = Draw.Result.FormatBetNumbers();
          _sql_cmd.Parameters.Add("@pCombinationWon", SqlDbType.NVarChar).Value = Draw.Result.FormatWinningCombination();

          // do not like doing this here would prefer to modify the draw routine to take this into account
          // but time is tight, also would prefer to create a draw runner test harness to execute the draw
          // with concrete set of parameters for testing and verification purposes
          //if (_prize_mode == PrizeMode.CashPrize)
          //{
          //  _sql_cmd.Parameters.Add("@pReBet", SqlDbType.Money).Value = CashDeskDrawBusinessLogic.GetParticipationPrice((short)Draw.DrawId); // LTC 04-OCT-2016
          //}
          //else if (_prize_mode == PrizeMode.Terminal)
          //{
          //  SetDrawPrize_Terminal(Draw);
            
          //  _sql_cmd.Parameters.Add("@pReBet", SqlDbType.Money).Value = Draw.Parameters.m_re_bet.SqlMoney;
          //  _sql_cmd.Parameters.Add("@pNrBet", SqlDbType.Money).Value = Draw.Parameters.m_nr_bet.SqlMoney;
          //}
          //else
          //{
          //  _sql_cmd.Parameters.Add("@pReBet", SqlDbType.Money).Value = Draw.Parameters.m_bet_amount.SqlMoney;
          //}

          Draw.Parameters.m_re_bet = 0;
          Draw.Parameters.m_nr_bet = 0;
          Draw.Parameters.m_points_bet = 0;

          // Set Bet Parameters 
          SetDrawPrizeBet(Draw);

          _sql_cmd.Parameters.Add("@pReBet", SqlDbType.Money).Value = Draw.Parameters.m_re_bet.SqlMoney;
          _sql_cmd.Parameters.Add("@pNrBet", SqlDbType.Money).Value = Draw.Parameters.m_nr_bet.SqlMoney;

          //RGR 13-OCT-2016
          //LTC 22-SEP-2016
          if (_terminal_id == 0)
          {
            _sql_cmd.Parameters.Add("@pTerminal", SqlDbType.Int).Value = _terminal_id;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pTerminal", SqlDbType.Int).Value = DBNull.Value;
          }

          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.Money).Value = DBNull.Value;

          if (GeneralParam.GetBoolean("CashDesk.Draw" + GetCashDeskPostfij(Draw.DrawId), "IsCashDeskDraw"))
          {
            // "Sorteador"
            _sql_cmd.Parameters.Add("@pUploadToExtDB", SqlDbType.Bit).Value = DBNull.Value;
          }
          else
          {
            DrawGenerationStatus _status;

            _status = DrawGenerationStatus.Local;

            String _cash_desk_post_fij;
            _cash_desk_post_fij = GetCashDeskPostfij(Draw.DrawId);

            //LTC 22-SEP-2016
            if (!GeneralParam.GetBoolean("CashDesk.Draw" + _cash_desk_post_fij, "LocalServer") &&
               (!String.IsNullOrEmpty(GeneralParam.GetString("CashDesk.Draw" + _cash_desk_post_fij, "ServerAddress1"))
               || !String.IsNullOrEmpty(GeneralParam.GetString("CashDesk.Draw" + _cash_desk_post_fij, "ServerAddress2"))))
            {
              // WebService
              _status = DrawGenerationStatus.Remote;
            }
            else
            {
              if (!String.IsNullOrEmpty(GeneralParam.GetString("CashDesk.Draw" + _cash_desk_post_fij, "ServerBDConnectionString")))
              {
                _status = DrawGenerationStatus.LocalPendingToUpload;
              }
            }

            _sql_cmd.Parameters.Add("@pUploadToExtDB", SqlDbType.Int).Value = (int)_status;
          }

          if (Draw.Result.m_is_winner)
          {
            _sql_cmd.Parameters.Add("@pPrizeId", SqlDbType.Int).Value = Draw.Result.m_prize_category.m_prize_id;

            // do not like doing this here would prefer to modify the draw routine to take this into account
            // but time is tight, also would prefer to create a draw runner test harness to execute the draw
            // with concrete set of parameters for testing and verification purposes
            if (_prize_mode == PrizeMode.Terminal)
            {
              Draw.Result.m_re_awarded += Draw.Parameters.m_bet_amount.Decimal;
              _sql_cmd.Parameters.Add("@pReWon", SqlDbType.Money).Value = Draw.Result.m_re_awarded.SqlMoney;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pReWon", SqlDbType.Money).Value = Draw.Result.m_re_awarded.SqlMoney;
            }

            _sql_cmd.Parameters.Add("@pNrWon", SqlDbType.Money).Value = Draw.Result.m_nr_awarded.SqlMoney;
          }
          else
          {
            // Is loser
            _sql_cmd.Parameters.Add("@pPrizeId", SqlDbType.Int).Value = PrizeCategory.Prize_5; //LTC 31-OCT-2016
            _sql_cmd.Parameters.Add("@pReWon", SqlDbType.Money).Value = 0;
            _sql_cmd.Parameters.Add("@pNrWon", SqlDbType.Money).Value = 0;
          }

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SaveDraw

    /// <summary>
    /// Get redeemable bet 
    /// </summary>
    /// <returns></returns>
    private static void SetDrawPrizeBet(CashDeskDraw Draw)
    {
      switch (CashDeskDrawBusinessLogic.GetPrizeMode(Draw.Parameters.m_cash_desk_id))
      {
        case CashDeskDrawBusinessLogic.PrizeMode.CashPrize:
          SetDrawPrize_CashPrize(Draw);
          break;

        case CashDeskDrawBusinessLogic.PrizeMode.Terminal:
          SetDrawPrize_Terminal(Draw);
          break;

        default:
          Draw.Parameters.m_re_bet = Draw.Parameters.m_bet_amount;
          break;
      }

    } // SetDrawPrizeBet

    /// <summary>
    /// Set draw prize when draw is by CashPrize
    /// </summary>
    private static void SetDrawPrize_CashPrize(CashDeskDraw Draw)
    {
      Draw.Parameters.m_re_bet = CashDeskDrawBusinessLogic.GetParticipationPrice(Draw.Parameters.m_cash_desk_id);
    } // SetDrawPrize_CashPrize

    /// <summary>
    /// Set draw prize when draw is by Terminal
    /// </summary>
    private static void SetDrawPrize_Terminal(CashDeskDraw Draw)
    {
      if (Draw.GameId > TerminalDraw.TerminalDraw.WELCOME_DRAW_ID)
      {
        SetDrawPrize_GameId(Draw);
      }
      else
      {
        SetDrawPrize_WelcomeDraw(Draw);
      }
    } // SetDrawPrize_Terminal

    /// <summary>
    /// Set draw prize when draw is by GameId
    /// </summary>
    /// <param name="Draw"></param>
    private static void SetDrawPrize_GameId(CashDeskDraw Draw)
    {
      Draw.Parameters.m_re_bet = 0;
      Draw.Parameters.m_nr_bet = 0;
      Draw.Parameters.m_points_bet = 0;

      switch (Draw.Parameters.m_game.PriceUnits)
      {
        case PrizeType.Redeemable:
          Draw.Parameters.m_re_bet = Draw.Parameters.m_game.Price;
          break;

        case PrizeType.NoRedeemable:
          Draw.Parameters.m_nr_bet = Draw.Parameters.m_game.Price;
          break;
      }
    } // SetDrawPrize_GameId

    /// <summary>
    /// Set draw prize when draw is by WelcomeDraw
    /// </summary>
    private static void SetDrawPrize_WelcomeDraw(CashDeskDraw Draw)
    {
      Decimal _maxbet;

      _maxbet = CashDeskDrawBusinessLogic.GetMaxParticipationPrice((short)Draw.DrawId);
      Draw.Parameters.m_bet_amount = Math.Min((CashDeskDrawBusinessLogic.GetParticipationPrice(Draw.DrawId) / (Decimal)100) * Draw.Parameters.m_bet_amount.Decimal, _maxbet);

      Draw.Parameters.m_re_bet = Draw.Parameters.m_bet_amount;
    } // SetDrawPrize_WelcomeDraw

    //------------------------------------------------------------------------------
    // PURPOSE: Insert loser promotions
    // 
    //  PARAMS:
    //      - INPUT: CashierDraws
    //               AccountLevel
    //               RedeemableAmountToAdd
    //               PrizeCoupon
    //               SqlTrx
    //
    //      - OUTPUT: Promotions
    //
    // RETURNS: - List<VoucherDrawCashDesk>
    // 
    //   NOTES:
    //
    //LTC 22-SEP-2016
    public static List<VoucherDrawCashDesk> GenerateVoucherDrawCashDesk(String[] VoucherAccountInfo,
                                                                        Result CashDeskDrawResult,
                                                                        Int64 OperationId,
                                                                        OperationCode OperationCode,
                                                                        PrintMode Mode,
                                                                        SqlTransaction SQLTransaction,
                                                                        TYPE_SPLITS SplitInformation,
                                                                        ENUM_DRAW_CONFIGURATION CasDeskId)
    {
      List<VoucherDrawCashDesk> _voucher_draw_cash_desk;
      VoucherDrawCashDesk _temp_voucher_draw_cash_desk;

      _voucher_draw_cash_desk = new List<VoucherDrawCashDesk>();
      _temp_voucher_draw_cash_desk = null;

      if (IsEnabledCashDeskDraws(Convert.ToInt16(CasDeskId)) && CashDeskDrawResult != null)
      {
        if (IsEnabledCashDeskDrawsVouchers(Convert.ToInt16(CasDeskId)))
        {
          if (CashDeskDrawResult.m_bet_numbers != null && CashDeskDrawResult.m_winning_combination != null)
          {
            _temp_voucher_draw_cash_desk = new VoucherDrawCashDesk(VoucherAccountInfo,
                                                                  CashDeskDrawResult,
                                                                  OperationId,
                                                                  false,
                                                                  OperationCode,
                                                                  Mode,
                                                                  CasDeskId,
                                                                  SQLTransaction);

            // DHA 12-FEB-2014: Added general param to hide currency symbol
            if (GeneralParam.GetBoolean("CashDesk.Draw" + GetCashDeskPostfij(Convert.ToInt16(CasDeskId)), "Voucher.HideCurrencySymbol", false) &&
             !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
            {
              _temp_voucher_draw_cash_desk.ReplaceCurrencySimbol("");
            }

            if (Mode == PrintMode.Print)
            {
              _temp_voucher_draw_cash_desk.Save(OperationId, SQLTransaction);
            }

            _voucher_draw_cash_desk.Add(_temp_voucher_draw_cash_desk);
          }
        }

        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.Redeemable && !CashDeskDrawResult.m_is_winner && CashDeskDrawBusinessLogic.IsCashPrizeCashDeskDraw(CasDeskId))
        {
          return _voucher_draw_cash_desk;
        }

        // Generate DrawCashDesk Voucher Loser
        if (!CashDeskDrawResult.m_is_winner && (CashDeskDrawResult.m_re_awarded + CashDeskDrawResult.m_nr_awarded) > 0)
        {
          if (IsEnabledCashDeskDrawsComplementaryVouchers(Convert.ToInt16(CasDeskId)))
          {
            _temp_voucher_draw_cash_desk = new VoucherDrawCashDesk(VoucherAccountInfo,
                                                                 CashDeskDrawResult,
                                                                 OperationId,
                                                                 true,
                                                                 OperationCode,
                                                                 Mode,
                                                                 CasDeskId,
                                                                 SQLTransaction);

            // DHA 12-FEB-2014: Added general param to hide currency symbol
            if (GeneralParam.GetBoolean("CashDesk.Draw" + GetCashDeskPostfij(Convert.ToInt16(CasDeskId)), "Voucher.HideCurrencySymbol", false) &&
             !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
            {
              _temp_voucher_draw_cash_desk.ReplaceCurrencySimbol("");
            }

            if (Mode == PrintMode.Print)
            {
              _temp_voucher_draw_cash_desk.Save(OperationId, SQLTransaction);
            }

            _voucher_draw_cash_desk.Add(_temp_voucher_draw_cash_desk);
          }
        }
      }

      return _voucher_draw_cash_desk;
    } //GenerateVoucherDrawCashDesk

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate cash desk draw prizes
    // 
    //  PARAMS:
    //      - INPUT: SplitA
    //               SplitB
    //               TaxProvosion
    //               PrizeCategories
    //               CashDeskID
    //
    //      - OUTPUT: PrizeCategory
    //                RE
    //                NR
    //                ReGross
    //
    // RETURNS: TRUE: Successfully Calculated FALSE: If otherwise
    // 
    //   NOTES:
    //

    public static Boolean CalculateCashDeskDrawPrize(Currency SplitA,
                                                     Currency SplitB,
                                                     Currency TaxProvisions,
                                                     List<SinglePrizeCategory> PrizeCategories,
                                                     Int16 CashDeskId,
                                                     out SinglePrizeCategory PrizeCategory,
                                                     out Currency RE,
                                                     out Currency NR,
                                                     out Currency ReGross)
    {
      Currency _prize_amount;
      ProbabilityDraws _draw;
      Decimal _participation_price;
      Currency _gross;
      Currency _tax1;
      Currency _tax2;
      Currency _tax3;
      Currency _total_taxes;

      RE = 0;
      NR = 0;
      ReGross = 0;
      PrizeCategory = new SinglePrizeCategory(0, 0, 0, 0, PrizeType.Unknown, false);
      _participation_price = 0;

      if (PrizeCategories.Count == 0)
      {
        Log.Error("PrizeCategories is empty.");

        return false;
      }

      _draw = new ProbabilityDraws(PrizeCategories);
      PrizeCategory = _draw.Draw();

      if (GetPrizeMode(CashDeskId) == PrizeMode.CashPrize || GetPrizeMode(CashDeskId) == PrizeMode.PlayerSession)
      {
        _participation_price = GetParticipationPrice(CashDeskId);
      }

      _prize_amount = Math.Round(PrizeCategory.m_fixed + SplitB + _participation_price + (SplitA + SplitB + TaxProvisions) * PrizeCategory.m_percent / 100.00m, 2, MidpointRounding.AwayFromZero);

      if (Misc.IsTaxProvisionsEnabled())
      {
        _prize_amount += TaxProvisions;
      }

      switch (PrizeCategory.m_credit_type)
      {
        case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
        case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
        case ACCOUNT_PROMO_CREDIT_TYPE.NR3:
        case ACCOUNT_PROMO_CREDIT_TYPE.UNR1:
        case ACCOUNT_PROMO_CREDIT_TYPE.UNR2:

          NR = Math.Max(_prize_amount, 0);

          if (PrizeCategory.m_prize_type == PrizeType.UNR)
          {
            if (!CashDeskDrawBusinessLogic.AddPrizeUNR(true, NR, out _gross, out _tax1, out _tax2, out _tax3))
            {
              return false;
            }

            NR = _gross;
            _total_taxes = (_tax1 + _tax2 + _tax3);
            if (_total_taxes > SplitA)
            {
              NR = 0;

              Log.Warning("UNR Taxes on Gross are greater than SplitA, SplitA: " + SplitA.ToString() + ", Gross: " + _gross.ToString() + ", Taxes: " + _total_taxes.ToString());
            }
          }

          return true;

        case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:

          RE = Math.Max(_prize_amount, 0);

          if (!CashDeskDrawBusinessLogic.AddPrizeUNR(true, RE, out _gross, out _tax1, out _tax2, out _tax3))
          {
            return false;
          }

          RE = _gross;
          _total_taxes = (_tax1 + _tax2 + _tax3);
          if (_total_taxes > SplitA)
          {
            RE = 0;

            Log.Warning("RE Taxes on Gross are greater than SplitA, SplitA: " + SplitA.ToString() + ", Gross: " + _gross.ToString() + ", Taxes: " + _total_taxes.ToString());
          }

          return true;

        case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
          if (GetPrizeMode() == PrizeMode.CashPrize)
          {
            RE = Math.Max(_prize_amount, 0);

            if (!CashDeskDrawBusinessLogic.AddPrizeCashPrize(true, RE, out _gross, out _tax1, out _tax2, out _tax3))
            {
              return false;
            }

            ReGross = _gross;
            _total_taxes = (_tax1 + _tax2 + _tax3);
            if (_total_taxes > SplitA)
            {
              RE = 0;

              Log.Warning("RE Taxes on Gross are greater than SplitA, SplitA: " + SplitA.ToString() + ", Gross: " + _gross.ToString() + ", Taxes: " + _total_taxes.ToString());
            }
          }
          else
          {
            RE = Math.Max(_prize_amount, 0);
          }

          return true;

        default:
          Log.Error("PrizeCategory Unknown CreditType: " + PrizeCategory.m_credit_type.ToString());
          break;
      }

      return false;
    } // CalculateCashDeskDrawPrize

    //------------------------------------------------------------------------------
    // PURPOSE: Validate cash desk draw configuration
    // 
    //  PARAMS:
    //      - INPUT: CashDeskDraw
    //
    //      - OUTPUT: Error message (if Any)
    //
    // RETURNS: TRUE: If Validated FALSE: Otherwise
    // 
    //   NOTES:
    //
    //LTC 10-OCT-2016
    public static Boolean ValidateCashDeskDrawConfiguration(CashDeskDraw CashDeskDraw, out String ErrorMessage)
    {
      Decimal _total_percent_probability;
      ErrorMessage = "";

      if (CashDeskDraw.Parameters.total_balls_number <= CashDeskDraw.Parameters.balls_extracted
       || CashDeskDraw.Parameters.balls_extracted <= CashDeskDraw.Parameters.balls_of_participant
       || CashDeskDraw.Parameters.balls_of_participant <= 0)
      {
        ErrorMessage = Resource.String("STR_CASHDESK_DRAW_ERROR_MSG_01");
        Log.Error("ValidateCashDeskDrawConfiguration. Wrong Draw Configuration: wrong Keno configuration");

        return false;
      }

      if (CashDeskDraw.Parameters.m_winner_prizes.Count == 0)
      {
        ErrorMessage = Resource.String("STR_CASHDESK_DRAW_ERROR_MSG_02");
        Log.Error("ValidateCashDeskDrawConfiguration. Wrong Draw Configuration: winner prizes not defined");

        return false;
      }

      if (CashDeskDraw.Parameters.m_loser_prizes.Count == 0 && IsLoserPromotionEnabled(CashDeskDraw.DrawId))
      {
        ErrorMessage = Resource.String("STR_CASHDESK_DRAW_ERROR_MSG_03");
        Log.Error("ValidateCashDeskDrawConfiguration. Wrong Draw Configuration: loser prizes not defined");

        return false;
      }

      _total_percent_probability = 0;

      foreach (SinglePrizeCategory _prize in CashDeskDraw.Parameters.m_winner_prizes)
      {
        _total_percent_probability += _prize.m_percent_probability;
      }

      foreach (SinglePrizeCategory _prize in CashDeskDraw.Parameters.m_loser_prizes)
      {
        _total_percent_probability += _prize.m_percent_probability;
      }

      if (Math.Round(_total_percent_probability, 0) != 100)
      {
        ErrorMessage = Resource.String("STR_CASHDESK_DRAW_ERROR_MSG_04");
        Log.Error("ValidateCashDeskDrawConfiguration.  Wrong Draw Configuration: probability all winner and loser prizes not sum 100");

        return false;
      }

      return true;
    } //ValidateCashDeskDrawConfiguration

    //------------------------------------------------------------------------------
    // PURPOSE: Send the draw data to external DataBase & Update local table if correct
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: True: Send ok. False: Otherwise.
    // 
    //   NOTES:
    //
    public static void UploadDraws()
    {
      DrawUploadStatus _status;

      if (Misc.GetElapsedTicks(m_last_tick) < m_wait_hint)
      {
        return;
      }

      m_last_tick = Misc.GetTickCount();
      _status = DrawUploadStatus.Error;

      try
      {
        do
        {
          _status = UploadDrawsToExternalDataBase();

        } while (Misc.GetElapsedTicks(m_last_tick) <= 60000 && _status == DrawUploadStatus.SomethingUploaded);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        _status = DrawUploadStatus.Error;
      }

      m_last_tick = Misc.GetTickCount();

      switch (_status)
      {
        case DrawUploadStatus.SomethingUploaded:
          m_wait_hint = 0;
          break;

        case DrawUploadStatus.NothingToUpload:
          m_wait_hint = 1 * 60 * 1000;
          break;

        case DrawUploadStatus.ErrorUploading:
          m_wait_hint = 2 * 60 * 1000;
          break;

        case DrawUploadStatus.NotConfigured:
        case DrawUploadStatus.Error:
        default:
          m_wait_hint = 10 * 60 * 1000;
          break;
      }
    } // UploadDraws

    //------------------------------------------------------------------------------
    // PURPOSE: Upload Draws To External DB
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: DrawUploadStatus
    // 
    //   NOTES:
    //
    public static DrawUploadStatus UploadDrawsToExternalDataBase()
    {
      DataTable _dt_draws_pending;
      StringBuilder _sb_sql;
      Int32 _nr;
      String _connection_string;

      _connection_string = GeneralParam.GetString("CashDesk.Draw", "ServerBDConnectionString");

      if (String.IsNullOrEmpty(_connection_string))
      {
        return DrawUploadStatus.NotConfigured;
      }

      _dt_draws_pending = new DataTable();

      try
      {
        // read pending draws to send to remote server
        _sb_sql = new StringBuilder();
        _sb_sql.AppendLine("  SELECT   TOP 100                                      ");
        _sb_sql.AppendLine("           CD_SITE_ID                                   ");
        _sb_sql.AppendLine("         , CD_DRAW_ID                                   ");
        _sb_sql.AppendLine("         , CD_OPERATION_ID                              ");
        _sb_sql.AppendLine("         , CD_ACCOUNT_ID                                ");
        _sb_sql.AppendLine("         , CD_DRAW_DATETIME                             ");
        _sb_sql.AppendLine("         , CD_COMBINATION_BET                           ");
        _sb_sql.AppendLine("         , CD_COMBINATION_WON                           ");
        _sb_sql.AppendLine("         , CD_RE_BET                                    ");
        _sb_sql.AppendLine("         , CD_NR_BET                                    ");
        _sb_sql.AppendLine("         , CD_RE_WON                                    ");
        _sb_sql.AppendLine("         , CD_NR_WON                                    ");
        _sb_sql.AppendLine("         , CD_PRIZE_ID                                  ");
        _sb_sql.AppendLine("    FROM   CASHDESK_DRAWS WITH(INDEX(IX_cd_upload))     ");
        _sb_sql.AppendLine("   WHERE   CD_UPLOAD_TO_EXT_DB = @pLocalPendingToUpload ");
        _sb_sql.AppendLine("ORDER BY   CD_DRAW_ID                                   ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pLocalPendingToUpload", SqlDbType.Int).Value = DrawGenerationStatus.LocalPendingToUpload;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_dt_draws_pending);
            }
          }
        }

        if (_dt_draws_pending.Rows.Count <= 0)
        {
          // Nothing to upload
          return DrawUploadStatus.NothingToUpload;
        }

        foreach (DataRow _row in _dt_draws_pending.Rows)
        {
          _row.SetAdded();
        }

        // Insert draws into remote server
        _sb_sql.Length = 0;
        _sb_sql.AppendLine("IF EXISTS ( SELECT   1                       ");
        _sb_sql.AppendLine("              FROM   CASHDESK_DRAWS          ");
        _sb_sql.AppendLine("             WHERE   CD_SITE_ID = @pSiteId   ");
        _sb_sql.AppendLine("               AND   CD_DRAW_ID = @pDrawId ) ");
        _sb_sql.AppendLine("BEGIN                                        ");
        _sb_sql.AppendLine("  DELETE   FROM CASHDESK_DRAWS               ");
        _sb_sql.AppendLine("   WHERE   CD_SITE_ID = @pSiteId             ");
        _sb_sql.AppendLine("     AND   CD_DRAW_ID = @pDrawId             ");
        _sb_sql.AppendLine("END                                          ");
        _sb_sql.AppendLine("INSERT   INTO CASHDESK_DRAWS                 ");
        _sb_sql.AppendLine("       ( CD_SITE_ID                          ");
        _sb_sql.AppendLine("       , CD_DRAW_ID                          ");
        _sb_sql.AppendLine("       , CD_OPERATION_ID                     ");
        _sb_sql.AppendLine("       , CD_ACCOUNT_ID                       ");
        _sb_sql.AppendLine("       , CD_DRAW_DATETIME                    ");
        _sb_sql.AppendLine("       , CD_COMBINATION_BET                  ");
        _sb_sql.AppendLine("       , CD_COMBINATION_WON                  ");
        _sb_sql.AppendLine("       , CD_RE_BET                           ");
        _sb_sql.AppendLine("       , CD_NR_BET                           ");
        _sb_sql.AppendLine("       , CD_RE_WON                           ");
        _sb_sql.AppendLine("       , CD_NR_WON                           ");
        _sb_sql.AppendLine("       , CD_PRIZE_ID                         ");
        _sb_sql.AppendLine("       )                                     ");
        _sb_sql.AppendLine("VALUES ( @pSiteId                            ");
        _sb_sql.AppendLine("       , @pDrawId                            ");
        _sb_sql.AppendLine("       , @pOperationId                       ");
        _sb_sql.AppendLine("       , @pAccountID                         ");
        _sb_sql.AppendLine("       , @pDrawDatetime                      ");
        _sb_sql.AppendLine("       , @pCombinationBet                    ");
        _sb_sql.AppendLine("       , @pCombinationWon                    ");
        _sb_sql.AppendLine("       , @pReBet                             ");
        _sb_sql.AppendLine("       , @pNrBet                             ");
        _sb_sql.AppendLine("       , @pReWon                             ");
        _sb_sql.AppendLine("       , @pNrWon                             ");
        _sb_sql.AppendLine("       , @pPrizeId                           ");
        _sb_sql.AppendLine("       )                                     ");

        //Not use WGDB because connected to another DB
        using (SqlConnection _sql_conn = new SqlConnection(_connection_string))
        {
          _sql_conn.Open();

          using (SqlTransaction _db_rmt_trx = _sql_conn.BeginTransaction())
          {
            using (SqlCommand _sql_command = new SqlCommand(_sb_sql.ToString(), _db_rmt_trx.Connection, _db_rmt_trx))
            {
              _sql_command.Parameters.Add("@pSiteId", SqlDbType.Int).SourceColumn = "CD_SITE_ID";
              _sql_command.Parameters.Add("@pDrawId", SqlDbType.BigInt).SourceColumn = "CD_DRAW_ID";
              _sql_command.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "CD_OPERATION_ID";
              _sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt).SourceColumn = "CD_ACCOUNT_ID";
              _sql_command.Parameters.Add("@pDrawDatetime", SqlDbType.DateTime).SourceColumn = "CD_DRAW_DATETIME";
              _sql_command.Parameters.Add("@pCombinationBet", SqlDbType.NVarChar).SourceColumn = "CD_COMBINATION_BET";
              _sql_command.Parameters.Add("@pCombinationWon", SqlDbType.NVarChar).SourceColumn = "CD_COMBINATION_WON";
              _sql_command.Parameters.Add("@pReBet", SqlDbType.Money).SourceColumn = "CD_RE_BET";
              _sql_command.Parameters.Add("@pNrBet", SqlDbType.Money).SourceColumn = "CD_NR_BET";
              _sql_command.Parameters.Add("@pReWon", SqlDbType.Money).SourceColumn = "CD_RE_WON";
              _sql_command.Parameters.Add("@pNrWon", SqlDbType.Money).SourceColumn = "CD_NR_WON";
              _sql_command.Parameters.Add("@pPrizeId", SqlDbType.BigInt).SourceColumn = "CD_PRIZE_ID";

              using (SqlDataAdapter _sql_da = new SqlDataAdapter())
              {
                _sql_da.InsertCommand = _sql_command;
                _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                _sql_da.UpdateBatchSize = 500;

                _nr = _sql_da.Update(_dt_draws_pending);

                if (_nr != _dt_draws_pending.Rows.Count)
                {
                  Log.Error("SendDrawToExternalDataBase. Draws not inserted.");
                  _db_rmt_trx.Rollback();

                  return DrawUploadStatus.ErrorUploading;
                }

                _db_rmt_trx.Commit();
              }
            }
          }
        }

        foreach (DataRow _row in _dt_draws_pending.Rows)
        {
          _row.SetModified();
        }

        // Update local draws send to remote server
        _sb_sql.Length = 0;
        _sb_sql.AppendLine("UPDATE   CASHDESK_DRAWS                 ");
        _sb_sql.AppendLine("   SET   CD_UPLOAD_TO_EXT_DB = @pUpload ");
        _sb_sql.AppendLine(" WHERE   CD_DRAW_ID          = @pDrawId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pDrawId", SqlDbType.BigInt).SourceColumn = "CD_DRAW_ID";
            _sql_cmd.Parameters.Add("@pUpload", SqlDbType.Int).Value = DrawGenerationStatus.LocalUploaded;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.UpdateCommand = _sql_cmd;
              _sql_da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_da.UpdateBatchSize = 500;

              _nr = _sql_da.Update(_dt_draws_pending);

              if (_nr != _dt_draws_pending.Rows.Count)
              {
                Log.Error("SendDrawToExternalDataBase. Local draws not updated.");

                return DrawUploadStatus.Error;
              }

              _db_trx.Commit();
            }
          }
        }

        return DrawUploadStatus.SomethingUploaded;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return DrawUploadStatus.Error;
    } // UploadDrawsToExternalDataBase

    //------------------------------------------------------------------------------
    // PURPOSE: Get the Terminal Data of the Cash Draw
    // 
    //  PARAMS:
    //      - INPUT: TerminalType
    //               Trx
    //
    //      - OUTPUT: TerminalID
    //                TerminalName
    //
    // RETURNS: TRUE: Data Found  FALSE: Otherwise
    // 
    //   NOTES:
    //
    public static Boolean GetCashDeskDrawTerminalData(TerminalTypes TerminalType, out Int32 TerminalId, out String TerminalName, SqlTransaction Trx)
    {
      StringBuilder _sb;

      TerminalId = -1;
      TerminalName = "";

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TOP 1                             ");
        _sb.AppendLine("          TE_TERMINAL_ID                    ");
        _sb.AppendLine("        , TE_NAME                           ");
        _sb.AppendLine("   FROM   TERMINALS                         ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_TYPE = @pTerminalType ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TerminalType;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            TerminalId = _reader.GetInt32(0);
            TerminalName = _reader.GetString(1);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCashDeskDrawTerminalData

    //------------------------------------------------------------------------------
    // PURPOSE: Get TerminalID
    // 
    //  PARAMS:
    //      - INPUT: Draw
    //               Trx
    //
    //      - OUTPUT: 
    //
    // RETURNS: TerminalID
    // 
    //   NOTES:
    //
    //     RGR 13-OCT-2016
    public static long GetSorterTerminalID(short Draw, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int64 _terminal_id;

      _terminal_id = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CAST(ISNULL(MIN(TE_TERMINAL_ID), 0) AS BIGINT) ");
        _sb.AppendLine("   FROM   TERMINALS                                      ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_TYPE = @pTerminalType              ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          switch (Draw)
          {
            case (short)ENUM_DRAW_CONFIGURATION.DRAW_00:
              _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = (int)TerminalTypes.CASHDESK_DRAW;
              break;

            case (short)ENUM_DRAW_CONFIGURATION.DRAW_01:
              _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = (int)TerminalTypes.CASHDESK_DRAW_TABLE;
              break;

            case (short)ENUM_DRAW_CONFIGURATION.DRAW_02:
              _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = (int)TerminalTypes.TERMINAL_DRAW;
              break;
          }

          _terminal_id = (Int64)_cmd.ExecuteScalar();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _terminal_id;
    } // GetSorterTerminalID

    /// <summary>
    /// Get SQL for Insert PlaySession
    /// </summary>
    /// <param name="CashDeskId"></param>
    /// <param name="Draw"></param>
    /// <returns></returns>
    private static String GetSQLPlaySessionCashDeskDrawInsert(ENUM_DRAW_CONFIGURATION CashDeskId, CashDeskDraw Draw)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("DECLARE @_terminal_id INT                                                           ");

      switch (CashDeskId)
      {
        case ENUM_DRAW_CONFIGURATION.DRAW_00:
        case ENUM_DRAW_CONFIGURATION.DRAW_01:
          _sb.AppendLine("SET @_terminal_id = (SELECT TOP 1 TE_TERMINAL_ID FROM TERMINALS WHERE TE_TERMINAL_TYPE = @pTerminalType)");
          break;

        case ENUM_DRAW_CONFIGURATION.DRAW_02:
          _sb.AppendLine("SET @_terminal_id = @pTerminalId                                                ");
          break;
      }

      _sb.AppendLine("INSERT INTO PLAY_SESSIONS ( PS_ACCOUNT_ID                                           ");
      _sb.AppendLine("                          , PS_TERMINAL_ID                                          ");
      _sb.AppendLine("                          , PS_TYPE                                                 ");
      _sb.AppendLine("                          , PS_INITIAL_BALANCE                                      ");
      _sb.AppendLine("                          , PS_REDEEMABLE_CASH_IN                                   ");
      _sb.AppendLine("                          , PS_FINAL_BALANCE                                        ");
      _sb.AppendLine("                          , PS_PLAYED_COUNT                                         ");
      _sb.AppendLine("                          , PS_PLAYED_AMOUNT                                        ");
      _sb.AppendLine("                          , PS_REDEEMABLE_PLAYED                                    ");
      _sb.AppendLine("                          , PS_WON_COUNT                                            ");
      _sb.AppendLine("                          , PS_WON_AMOUNT                                           ");
      _sb.AppendLine("                          , PS_CASH_IN                                              ");
      _sb.AppendLine("                          , PS_CASH_OUT                                             ");
      _sb.AppendLine("                          , PS_REDEEMABLE_WON                                       ");
      _sb.AppendLine("                          , PS_REDEEMABLE_CASH_OUT                                  ");
      _sb.AppendLine("                          , PS_NON_REDEEMABLE_WON                                   ");
      _sb.AppendLine("                          , PS_NON_REDEEMABLE_CASH_OUT                              ");
      _sb.AppendLine("                          , PS_STATUS                                               ");
      _sb.AppendLine("                          , PS_STARTED                                              ");
      _sb.AppendLine("                          , PS_FINISHED                                             ");
      _sb.AppendLine("                          , PS_STAND_ALONE                                          ");
      _sb.AppendLine("                          , PS_PROMO                                                ");
      _sb.AppendLine("                          )                                                         ");
      _sb.AppendLine("                   VALUES ( @pAccountId                                             "); // PS_ACCOUNT_ID
      _sb.AppendLine("                          , @_terminal_id                                           "); // PS_TERMINAL_ID
      _sb.AppendLine("                          , @pType                                                  "); // PS_TYPE
      _sb.AppendLine("                          , @pInitialBalance                                        "); // PS_INITIAL_BALANCE
      _sb.AppendLine("                          , @pInitialBalance                                        "); // PS_REDEEMABLE_CASH_IN
      _sb.AppendLine("                          , @pFinalBalance                                          "); // PS_FINAL_BALANCE
      _sb.AppendLine("                          , @pPlayedCount                                           "); // PS_PLAYED_COUNT
      _sb.AppendLine("                          , @pBetAmount                                             "); // PS_PLAYED_AMOUNT
      _sb.AppendLine("                          , @pRedeemablePlayer                                      "); // PS_REDEEMABLE_PLAYER
      _sb.AppendLine("                          , @pWonCount                                              "); // PS_WON_COUNT
      _sb.AppendLine("                          , (@pReWonAmount + @pNrWonAmount)                         "); // PS_WON_AMOUNT
      _sb.AppendLine("                          , 0                                                       "); // PS_CASH_IN
      _sb.AppendLine("                          , 0                                                       "); // PS_CASH_OUT
      _sb.AppendLine("                          , @pReWonAmount                                           ");
      _sb.AppendLine("                          , @pInitialBalance + @pReWonAmount - @pRePlayedAmount     "); // PS_REDEEMABLE_CASH_OUT
      _sb.AppendLine("                          , @pNrWonAmount                                           "); // PS_NON_REDEEMABLE_WON
      _sb.AppendLine("                          , @pNrWonAmount                                           "); // PS_NON_REDEEMABLE_CASH_OUT
      _sb.AppendLine("                          , @pStatus                                                "); // PS_STATUS
      _sb.AppendLine("                          , GETDATE()                                               "); // PS_STARTED
      _sb.AppendLine("                          , DATEADD(SECOND, 5, GETDATE())                           "); // PS_FINISHED
      _sb.AppendLine("                          , 0                                                       "); // PS_STAND_ALONE
      _sb.AppendLine("                          , 0                                                       "); // PS_PROMO
      _sb.AppendLine("                          )                                                         ");
      _sb.AppendLine("                           SET @pPlaySessionId = SCOPE_IDENTITY()                   ");
      _sb.AppendLine("                           SET @pterminal_id_out = @_terminal_id                    "); //LTC 22-SEP-2016

      return _sb.ToString();
    }

    /// <summary>
    /// Get SQL for Update Play Session
    /// </summary>
    /// <returns></returns>
    private static String GetSQLPlaySessionCashDeskDrawUpdate()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("DECLARE @_terminal_id INT                                                                  ");
      _sb.AppendLine("UPDATE  PLAY_SESSIONS                                                                      ");
      _sb.AppendLine("   SET  PS_TYPE                    = @pType                                                ");
      _sb.AppendLine("      , PS_INITIAL_BALANCE         = @pInitialBalance                                      ");
      _sb.AppendLine("      , PS_REDEEMABLE_CASH_IN      = @pInitialBalance                                      ");
      _sb.AppendLine("      , PS_FINAL_BALANCE           = @pFinalBalance                                        ");
      _sb.AppendLine("      , PS_PLAYED_COUNT            = 1                                                     ");
      _sb.AppendLine("      , PS_PLAYED_AMOUNT           = @pBetAmount                                           ");
      _sb.AppendLine("      , PS_REDEEMABLE_PLAYED       = @pRedeemablePlayer                                    ");
      _sb.AppendLine("      , PS_WON_COUNT               = @pWonCount                                            ");
      _sb.AppendLine("      , PS_WON_AMOUNT              = (@pReWonAmount + @pNrWonAmount)                       ");
      _sb.AppendLine("      , PS_CASH_IN                 = 0                                                     ");
      _sb.AppendLine("      , PS_CASH_OUT                = 0                                                     ");
      _sb.AppendLine("      , PS_REDEEMABLE_WON          = @pReWonAmount                                         ");
      _sb.AppendLine("      , PS_REDEEMABLE_CASH_OUT     = @pInitialBalance + @pReWonAmount - @pRePlayedAmount   ");
      _sb.AppendLine("      , PS_NON_REDEEMABLE_WON      = @pNrWonAmount                                         ");
      _sb.AppendLine("      , PS_NON_REDEEMABLE_CASH_OUT = @pNrWonAmount                                         ");
      _sb.AppendLine("      , PS_STATUS                  = @pStatus                                              ");
      _sb.AppendLine("      , PS_FINISHED                = DATEADD(SECOND, 5, GETDATE())                         ");
      _sb.AppendLine("      , PS_STAND_ALONE             = 0                                                     ");
      _sb.AppendLine("      , PS_PROMO                   = 0                                                     ");
      _sb.AppendLine("WHERE   PS_PLAY_SESSION_ID         = @pPlaySessionId                                       ");
      //
      _sb.AppendLine("  SET   @pterminal_id_out          = @_terminal_id                                         "); //LTC 22-SEP-2016

      return _sb.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert Play Session
    // 
    //  PARAMS:
    //      - INPUT: Draw
    //               Trx
    //
    //      - OUTPUT: PlaysessionId
    //                TerminalID
    //
    // RETURNS: TRUE: Play session Successfully inserted FALSE: Otherwise
    // 
    //   NOTES:
    //
    //     LTC 22-SEP-2016
    //public static Boolean Trx_InsertPlaySession(CashDeskDraw Draw,
    //                                             out Int64 PlaySessionId,
    //                                             out Int64 TerminalID,
    //                                             ENUM_DRAW_CONFIGURATION CashDeskId,
    //                                             SqlTransaction Trx)
    //{
    //  return Trx_InsertPlaySession(0, Draw, out PlaySessionId, out TerminalID, CashDeskId, Trx);
    //}

    //------------------------------------------------------------------------------
    // PURPOSE: Insert Play Session
    // 
    //  PARAMS:
    //      - INPUT: Draw
    //               Trx
    //
    //      - OUTPUT: PlaysessionId
    //                TerminalID
    //
    // RETURNS: TRUE: Play session Successfully inserted FALSE: Otherwise
    // 
    //   NOTES:
    //
    //     LTC 22-SEP-2016
    public static Boolean Trx_InsertPlaySession(CashDeskDraw Draw,
                                                 out Int64 PlaySessionId,
                                                 out Int64 TerminalID,
                                                 ENUM_DRAW_CONFIGURATION CashDeskId,
                                                 SqlTransaction Trx)
    {
      String _sql_str;
      SqlParameter _param_play_session;
      SqlParameter _terminal_id;
      PlaySessionType _ps_type;
      PlaySessionStatus _ps_status;
      Decimal _bet_amount;
      Currency _nr_won_amount;
      Currency _re_won_amount;
      Currency _initial_balance_terminal_draw;
      Decimal _re_won_promotion;
      int _won_count;

      _param_play_session = null;
      _terminal_id = null;

      PlaySessionId = -1;
      TerminalID = -1;

      _ps_type = PlaySessionType.DRAW;
      _ps_status = PlaySessionStatus.DrawLoser;
      _bet_amount = Draw.Result.m_participation_amount;
      _won_count = 0;
      _nr_won_amount = 0;
      _re_won_amount = 0;
      _initial_balance_terminal_draw = 0;
      _re_won_promotion = 0;

      if (Draw.Result.m_is_winner)
      {
        _ps_status = PlaySessionStatus.DrawWinner;
        _won_count = 1;
        _nr_won_amount = Draw.Result.m_nr_awarded.SqlMoney;
        _re_won_amount = Draw.Result.m_re_awarded.SqlMoney;
      }
      else
      {
        _re_won_promotion = Draw.Result.m_re_awarded + Draw.Parameters.m_bet_amount;
      }

      try
      {
        if (CashDeskId == ENUM_DRAW_CONFIGURATION.DRAW_02 && Draw.Parameters.m_play_session_id > 0)
        {
          _sql_str = GetSQLPlaySessionCashDeskDrawUpdate();
        }
        else
        {
          _sql_str = GetSQLPlaySessionCashDeskDrawInsert(CashDeskId, Draw);
        }

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          if (Draw.Parameters.m_play_session_id <= 0)
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Draw.Parameters.m_account_id;
          }

          switch (CashDeskId)
          {
            case ENUM_DRAW_CONFIGURATION.DRAW_00:
              _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = (int)TerminalTypes.CASHDESK_DRAW;
              _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = Draw.Parameters.m_bet_amount.SqlMoney;
              _cmd.Parameters.Add("@pRedeemablePlayer", SqlDbType.Money).Value = _bet_amount;
              _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = (decimal)(_initial_balance_terminal_draw + Draw.Parameters.m_bet_amount + _re_won_amount + _nr_won_amount - _bet_amount);
              _cmd.Parameters.Add("@pRePlayedAmount", SqlDbType.Money).Value = _bet_amount;
              _cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).Value = 1;
              _cmd.Parameters.Add("@pReWonAmount", SqlDbType.Money).Value = _re_won_amount.SqlMoney;
              break;

            case ENUM_DRAW_CONFIGURATION.DRAW_02:
              _ps_type = PlaySessionType.TERMINAL_DRAW;

              if (Draw.Parameters.m_force_open_session && Draw.Parameters.m_play_session_id <= 0)
              {
                _ps_status = PlaySessionStatus.Opened;
                _cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).Value = 0;
              }
              else
              {
                if(!Misc.IsVoucherModeCodere())
                {
                  _ps_status = PlaySessionStatus.Closed;
                }
                _cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).Value = 1;
              }

              _bet_amount = Draw.Parameters.m_bet_amount;
              _initial_balance_terminal_draw = Draw.Parameters.m_total_cash_in_company_a.SqlMoney;
              TerminalID = Draw.Parameters.m_terminal_id;

              Decimal _winner_amount_re;
              Decimal _winner_amount_nr;

              _winner_amount_re = 0;
              _winner_amount_nr = 0;

              if (Draw.Result.m_is_winner || !Misc.IsVoucherModeCodere())
              {
                _winner_amount_re = (Decimal)_re_won_amount.SqlMoney + _re_won_promotion;
                _winner_amount_nr = _nr_won_amount;
              }

              _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = (int)TerminalTypes.TERMINAL_DRAW;
              _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = _initial_balance_terminal_draw.SqlMoney;
              _cmd.Parameters.Add("@pRedeemablePlayer", SqlDbType.Money).Value = _bet_amount;
              _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = (decimal)(_initial_balance_terminal_draw - Draw.Parameters.m_bet_amount + _winner_amount_re + _winner_amount_nr);
              _cmd.Parameters.Add("@pRePlayedAmount", SqlDbType.Money).Value = Draw.Parameters.m_bet_amount.SqlMoney;
              _cmd.Parameters.Add("pRePromotion", SqlDbType.Money).Value = _re_won_promotion;
              _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalID;
              _cmd.Parameters.Add("@pReWonAmount", SqlDbType.Money).Value = _winner_amount_re;

              break;

            default:
              _cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).Value = 1;
              _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = Draw.Parameters.m_bet_amount.SqlMoney;
              _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = (int)TerminalTypes.CASHDESK_DRAW_TABLE;
              _cmd.Parameters.Add("@pRedeemablePlayer", SqlDbType.Money).Value = 0;
              _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = (decimal)(_initial_balance_terminal_draw + Draw.Parameters.m_bet_amount + _re_won_amount);
              _cmd.Parameters.Add("@pRePlayedAmount", SqlDbType.Money).Value = 0;
              _cmd.Parameters.Add("@pReWonAmount", SqlDbType.Money).Value = _re_won_amount.SqlMoney;
              break;
          }

          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = _ps_type;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = _ps_status;
          _cmd.Parameters.Add("@pBetAmount", SqlDbType.Money).Value = _bet_amount;
          _cmd.Parameters.Add("@pWonCount", SqlDbType.Int).Value = _won_count;
          _cmd.Parameters.Add("@pNrWonAmount", SqlDbType.Money).Value = _nr_won_amount.SqlMoney;

          _terminal_id = _cmd.Parameters.Add("@pterminal_id_out", SqlDbType.BigInt); //LTC 22-SEP-2016
          _terminal_id.Direction = ParameterDirection.Output;

          if (Draw.Parameters.m_play_session_id <= 0)
          {
          _param_play_session = _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt);
          _param_play_session.Direction = ParameterDirection.Output;
          }
          else
          {
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = Draw.Parameters.m_play_session_id;
            PlaySessionId = Draw.Parameters.m_play_session_id;
          }

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          if (Draw.Parameters.m_play_session_id <= 0)
          {
          PlaySessionId = (Int64)_param_play_session.Value;
          TerminalID = (Int64)_terminal_id.Value;
        }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } // Trx_InsertPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE: Update PlaySession Finished
    // 
    //  PARAMS:
    //      - INPUT: PlaySessionID
    //               Milliseconds
    //               Trx
    //
    //      - OUTPUT: PlaysessionId
    //                TerminalID
    //
    // RETURNS: TRUE: Play session Successfully inserted FALSE: Otherwise
    // 
    //   NOTES:
    //
    public static Boolean Trx_UpdatePlaySessionFinished(Int64 PlaySessionId, Int64 Milliseconds, Boolean ClosePlaySession, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        // XGJ 03-MAR-2017
        Milliseconds = (Milliseconds < 1000) ? 1000 : Milliseconds;

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   PLAY_SESSIONS                                                 ");
        _sb.AppendLine("    SET   PS_FINISHED = (DATEADD(MILLISECOND, @pMillisecond,            ");
        _sb.AppendLine("                      (SELECT   PS_STARTED                              ");
        _sb.AppendLine("                         FROM   PLAY_SESSIONS                           ");
        _sb.AppendLine("                        WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId))) ");

        if (ClosePlaySession)
        {
          _sb.AppendLine("      , PS_STATUS = CASE WHEN PS_STATUS = @pStatusOpened THEN @pStatus ELSE PS_STATUS END ");
        }

        _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId                          ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pMillisecond", SqlDbType.BigInt).Value = Milliseconds;

          if (ClosePlaySession)
          {
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = PlaySessionStatus.Closed;
            _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          }

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } //Trx_UpdatePlaySessionFinished

    //------------------------------------------------------------------------------
    // PURPOSE: Update Cash Desk Draw
    // 
    //  PARAMS:
    //      - INPUT: Draw
    //               PlaySessionId
    //               TerminalId
    //               Trx
    //
    //      - OUTPUT: 
    //
    // RETURNS: TRUE: Cash Desk Draw Updated FALSE: Otherwise
    // 
    //   NOTES:
    //
    //     LTC 22-SEP-2016
    public static Boolean Trx_UpdateCashDeskDraw(CashDeskDraw Draw,
                                                 Int64 PlaySessionId,
                                                 Int64 TerminalId,
                                                 SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   CASHDESK_DRAWS             ");
        _sb.AppendLine("    SET   CD_TERMINAL   = @pTerminal ");
        _sb.AppendLine("        , CD_SESSION_ID = @pSession  ");
        _sb.AppendLine("  WHERE   CD_DRAW_ID    = @pDraw     ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminal", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pSession", SqlDbType.Int).Value = PlaySessionId;
          _cmd.Parameters.Add("@pDraw", SqlDbType.Int).Value = Draw.Result.m_draw_id;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    } // Trx_UpdateCashDeskDraw

    /// <summary>
    /// Get the Suffix of the Cash Table Draw param name
    /// </summary>
    /// <param name="CashDeskId"></param>
    /// <returns></returns>
    public static string GetCashDeskPostfij(Int16 CashDeskId)
    {
      String _postFij;

      _postFij = String.Empty;

      if (CashDeskId != 0)
      {
        _postFij = "." + CashDeskId.ToString("00").PadLeft(2);
      }

      return _postFij;
    } // GetCashDeskPostfij

    /// <summary>
    /// Terminal Draw: Close Opened PlaySessions
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean Trx_CloseOpenPlaySessions(Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   PLAY_SESSIONS                   ");
        _sb.AppendLine("    SET   PS_STATUS       = @pNewStatus   ");
        _sb.AppendLine("  WHERE   PS_TERMINAL_ID  = @pTerminalId  ");
        _sb.AppendLine("    AND   PS_STATUS       = @pStatus      ");
        _sb.AppendLine("    AND   PS_TYPE         = @pType        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          // Params "SET"
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = PlaySessionStatus.Closed;

          // Params "WHERE"
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = PlaySessionType.TERMINAL_DRAW;

          _cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }

      return false;
    }
  } // Trx_CloseOpenedPlaySessions
}
