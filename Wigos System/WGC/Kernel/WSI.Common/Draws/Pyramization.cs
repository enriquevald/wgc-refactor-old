﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace WSI.Common
{
  public class Pyramization
  {
    #region "Constants"

    private const Int32 DT_COLUMN_PROBABILITY = 0;
    private const Int32 DT_COLUMN_FIX = 1;
    private const Int32 DT_COLUMN_RECHARGE = 2;

    private const Int32 MinimumRandomRange = 1;
    private const Int32 MaximumRandomRange = 100;

    #endregion

    #region "Members"

    private DataTable DistributionTable;
    private List<PyramizationLevel> PyramizationDist;
    private Decimal Recharge;
    
    #endregion

    public Pyramization(Decimal Recharge, String Xml)
    {
      this.Recharge = Recharge;
      this.LoadPyramizationDistribution(Xml);
    }

    #region "Public methods"

    /// <summary>
    /// To get the pyramization value
    /// </summary>
    /// <returns></returns>
    public Decimal Calculate()
    {
      return this.Calculate(0);
    } // GetPyramizationValue
    public Decimal Calculate(Int32 LevelNumber)
    {
      return this.CalculateDistribution(LevelNumber);
    } // GetPyramizationValue

    #endregion

    #region "Private methods"

    /// <summary>
    /// To load a list of pyramization levels
    /// </summary>
    private void LoadPyramizationDistribution(String Xml)
    {
      this.LoadDistributionTable(Xml);
      this.PyramizationDist = new PyramizationLevel().GetPyramizationLevelList(this.DistributionTable);
    } // LoadPyramizationDistribution

    /// <summary>
    /// To load an XML into a DataTable
    /// </summary>
    /// <param name="Xml"></param>
    private void LoadDistributionTable(String Xml)
    {
      StringReader _reader;
      DataSet _ds;

      _reader = new StringReader(Xml);
      _ds = new DataSet();

      _ds.ReadXml(_reader);

      this.DistributionTable = _ds.Tables[0];
    } // LoadDistributionTable

    /// <summary>
    /// To calculate de total amount to add to the total prize
    /// </summary>
    /// <param name="Recharge"></param>
    /// <param name="Probability"></param>
    /// <returns></returns>
    private Decimal CalculateDistribution()
    {
      return this.CalculateDistribution(0);
    } // CalculateDistribution
    private Decimal CalculateDistribution(Int32 LevelNumber)
    {
      PyramizationLevel _level;
      Int32 _level_number;

      _level_number = (LevelNumber == 0 ? this.GenerateRandomNumber() : LevelNumber);
      _level = this.SearchPyramizationLevel(_level_number);

      return ((this.Recharge * _level.PctRecharge) / 100) + _level.Fix;
    } // CalculateDistribution

    /// <summary>
    /// To search the probability range level
    /// </summary>
    /// <param name="Probability"></param>
    /// <returns></returns>
    private PyramizationLevel SearchPyramizationLevel(Int32 Probability)
    {
      Int32 _pct_total;

      _pct_total = 0;

      foreach(PyramizationLevel _level in this.PyramizationDist)
      {
        _pct_total += _level.Probability;
        
        if(Probability <= _pct_total)
        {
          return _level;
        }
      }

      return null;
    } // SearchPyramizationLevel

    /// <summary>
    /// To generate a random number
    /// </summary>
    /// <returns></returns>
    private Int32 GenerateRandomNumber()
    {
      Random _rnd;
      Int32 _number;

      _rnd = new Random();
      _number = _rnd.Next(MinimumRandomRange, MaximumRandomRange);

      return _number;
    } // GenerateRandomNumber

    #endregion
  }
}
