//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashDeskDraw.cs
// 
//   DESCRIPTION: Class to calculate  CahsDeskDraw
// 
//        AUTHOR: Joaquin Calero & Daniel Dominguez
// 
// CREATION DATE: 01-AUG-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2013  JCA    Initial version
// 05-AUG-2013  DDM    Continue........
// 06-AUG-2013  DMR    Added GetCashDeskDrawsConfiguration method 
// 05-NOV-2013  MMG    Modified CalculateCashDeskDrawPrize Calls including a new parameter
// 01-MAR-2016  JML    Product Backlog Item 10084: Winpot - Provisioning Tax: Cashdesk draw
// 14-OCT-2015  SDS    New Draw Kind Redeemable (RE)
// 13-JAN-2016  JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 22-FEB-2016  JML    Bug 9788:Sorteo de caja: Error en la configuraci�n Sorteo
// 13-JAN-2015  JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 22-SEP-2016  LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 26-SEP-2016  RGR    PBI 17965: Televisa - Draw Cash (Cash prize) - Probability of prizes
// 28-SEP-2016  RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 28-SEP-2016  LTC    PBI 17448: Televisa - Cashdeskdraw (Cash desk) - Voucher
// 11-OCT-2016  EOR    Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cup�n promocional in the ticket
// 12-OCT-2016  RGR    Bug 18922: Draw box with cash prize (Televisa): the concept of the promotional coupon is not shown on the ticket cash desk
// 09-NOV-2016  ETP    Fixed Bug 20160: Duplicated concept when not participating in GamingTables cashdeskdraw
// 18-NOV-2016  ETP    Fixed Bug 20736: Cashdeskdraw. Total label only in cashprize and Televisa mode
// 08-FEB-2017  FAV    PBI 25268: Third TAX - Payment Voucher
// 15-MAR-2017  FGB    PBI 25268: Third TAX - Payment Voucher
// 21-MAR-2017 FAV     PBI 25735:Third TAX - Movement reports
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.Common
{

  public class CashDeskDraw
  {
    //static variable used by all CashDeskDraw instances
    private static Random m_random = null;

    private static Random RandomCashDeskDraw
    {
      get
      {
        if (m_random == null)
        {
          m_random = new Random();
        }

        return m_random;
      }
    }

    public Parameters Parameters;
    public Result Result;
    public Int16 DrawId;
    public Int64 GameId;

    #region Constructor

    public CashDeskDraw(Parameters CashierDraw)
    {
      this.Parameters = CashierDraw;
    }

    //LTC 22-SEP-2016
    public CashDeskDraw(Int16 CashDeskId = 0, Int64 Game = 0)
    {
      DrawId = CashDeskId;
      GameId = Game;
      Parameters = new Parameters(Game, CashDeskId);
    }
    #endregion // Constructor

    #region Public methods

    //------------------------------------------------------------------------------
    // PURPOSE: Generates the raffle numbers, the bet numbers and the prize of the draw
    // 
    //  PARAMS:
    //      - INPUT: Random
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public Boolean Draw()
    {
      Int32 _idx_combination_extracted;
      Int32 _idx_bet_combination;
      Int32 _random_extract_remaining;
      List<Int32> _copy_combination_won;
      List<Int32> _all_numbers;
      List<Int32> _remaining_numbers;
      Boolean _loser_number;
      Random _random;
      List<SinglePrizeCategory> _categories;

      _random = new Random();
      Result = new Result();
      Result.m_date_draw = WGDB.Now;
      Result.m_is_winner = false;
      _categories = new List<SinglePrizeCategory>();

      foreach (var _item in Parameters.m_winner_prizes)
      {
        _categories.Add(_item);
      }

      foreach (var _item in Parameters.m_loser_prizes)
      {
        _categories.Add(_item);
      }

      Currency _re_awarded = Result.m_re_awarded;

      // Compute Prize 
      if (!CashDeskDrawBusinessLogic.CalculateCashDeskDrawPrize(Parameters.m_bet_amount,
                                                                Parameters.m_total_cash_in - Parameters.m_bet_amount,
                                                                Parameters.m_tax_provisions,
                                                                _categories,
                                                                Parameters.m_cash_desk_id,
                                                                out Result.m_prize_category,
                                                                out Result.m_re_awarded,
                                                                out Result.m_nr_awarded,
                                                                out Result.m_re_gross))
      {
        return false;
      }

      Result.m_is_winner = Result.m_prize_category.m_is_winner_type;
      // Initialize numbers      
      _all_numbers = new List<Int32>();
      for (Int32 _number = 1; _number <= Parameters.total_balls_number; _number++)
      {
        _all_numbers.Add(_number);
      }

      //First extract all raffle numbers
      Result.m_winning_combination = new List<Int32>();
      for (Int32 _idx_number = 0; _idx_number < Parameters.balls_extracted; _idx_number++)
      {
        if (_all_numbers.Count == 0)
        {
          break;
        }
        _idx_combination_extracted = _random.Next(_all_numbers.Count);
        Result.m_winning_combination.Add(_all_numbers[_idx_combination_extracted]);
        _all_numbers.RemoveAt(_idx_combination_extracted);
      }

      //Second extract the BET numbers 
      _copy_combination_won = new List<Int32>(Result.m_winning_combination);
      _remaining_numbers = new List<Int32>(_all_numbers);
      Result.m_bet_numbers = new List<Int32>();
      _loser_number = true;

      _random_extract_remaining = 0;
      for (Int32 _idx_bet_number = 0; _idx_bet_number < Parameters.balls_of_participant; _idx_bet_number++)
      {
        if (!Result.m_is_winner && _loser_number && _idx_bet_number + 1 == Parameters.balls_of_participant)
        {
          _idx_bet_combination = _random.Next(0, _remaining_numbers.Count);
          Result.m_bet_numbers.Add(_remaining_numbers[_idx_bet_combination]);
          _remaining_numbers.RemoveAt(_idx_bet_combination);
          break;
        }

        // 0 --> Extract list
        // 1 --> Remaining list
        if (!Result.m_is_winner)
        {
          _random_extract_remaining = _random.Next(0, 2);
        }

        if (_random_extract_remaining == 0 || _remaining_numbers.Count == 0)
        {
          _idx_bet_combination = _random.Next(0, _copy_combination_won.Count);
          Result.m_bet_numbers.Add(_copy_combination_won[_idx_bet_combination]);
          _copy_combination_won.RemoveAt(_idx_bet_combination);
        }
        else
        {
          _idx_bet_combination = _random.Next(0, _remaining_numbers.Count);
          Result.m_bet_numbers.Add(_remaining_numbers[_idx_bet_combination]);
          _remaining_numbers.RemoveAt(_idx_bet_combination);
          _loser_number = false;
        }
      }

      //if (Result.m_prize_category.m_credit_type == ACCOUNT_PROMO_CREDIT_TYPE.UNR1 || Result.m_prize_category.m_credit_type == ACCOUNT_PROMO_CREDIT_TYPE.UNR2)
      //{
      //  Currency _gross;
      //  Currency _tax1;
      //  Currency _tax2;        

      //  if (!CashDeskDrawBusinessLogic.AddPrizeUNR(true, Result.m_nr_awarded, out _gross, out _tax1, out _tax2))
      //  {
      //    return false;
      //  }
      //  Result.m_unr_tax1 = _tax1;
      //  Result.m_unr_tax2 = _tax2;
      //  Result.m_nr_gross = _gross;

      //    // ADD gross in Result?
      //}

      return true;
    }

    #endregion //Public methods

  } // CashDeskDraw


  // PURPOSE: Class to obtain the parameters of a Draw
  // 
  public class Parameters
  {
    public int total_balls_number;
    public int balls_extracted;
    public int balls_of_participant;

    public int m_num_participants
    {
      get { return m_number_of_participants; }
      set
      {
        m_number_of_participants = value;
        this.m_num_losers = Math.Max(0, this.m_number_of_participants - m_number_of_winners);
      }
    }

    public int m_num_winners
    {
      get { return m_number_of_winners; }
      set
      {
        m_number_of_winners = value;
        this.m_num_losers = Math.Max(0, this.m_number_of_participants - m_number_of_winners);
      }
    }

    public int m_num_losers;

    public Int32 m_site_id;
    public Int64 m_account_id;
    public Int64 m_terminal_id;           // Only for Terminal Draw
    public Int64 m_play_session_id;       // Only for Terminal Draw
    public Boolean m_force_open_session;  // Only for Terminal Draw
    public Int64 m_operation_id;
    public Currency m_bet_amount;
    public Currency m_re_bet;
    public Currency m_nr_bet;
    public Currency m_points_bet;
    public Currency m_total_cash_in;
    public Currency m_total_cash_in_company_a;
    public Currency m_tax_provisions;
    public TYPE_SPLITS m_splits;
    public String _prize_id;

    public List<SinglePrizeCategory> m_winner_prizes;
    public List<SinglePrizeCategory> m_loser_prizes;

    public Int16 m_cash_desk_id;
    public Int64 m_game_id;
    public PromoGame m_game;

    private int m_number_of_participants;
    private int m_number_of_winners;

    private const Int32 MAX_PRIZES = 4;

    #region Constructor

    //LTC 22-SEP-2016
    public Parameters(Int64 GameId, Int16 CashDeskId = 0)
    {
      Int32 _prizes_enabled;
      Decimal _percentage_probability;
      Boolean _is_enabled;

      m_cash_desk_id = CashDeskId;
      m_game_id = GameId;
      
      m_site_id             = GeneralParam.GetInt32("Site", "Identifier", 0);
      m_num_participants    = GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), "NumberOfParticipants");
      m_num_winners         = GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), "NumberOfWinners");
      total_balls_number    = GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), "TotalsBallsNumber");
      balls_extracted       = GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), "BallsExtracted");
      balls_of_participant  = GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), "BallsOfParticipant");

      // Get Game configuration only when is PromoGame. 
      if (m_game_id > TerminalDraw.TerminalDraw.WELCOME_DRAW_ID)
      {
        m_game = new PromoGame(m_game_id);
      }

      m_num_winners = Math.Max(0, Math.Min(m_num_winners, m_num_participants));
      m_num_losers = Math.Max(0, m_num_participants - m_num_winners);

      m_winner_prizes = new List<SinglePrizeCategory>();
      m_loser_prizes = new List<SinglePrizeCategory>();

      _prizes_enabled = 0;
      _percentage_probability = 100;

      for (Int32 _idx_prize = 1; _idx_prize <= MAX_PRIZES; _idx_prize++)
      {

        _prize_id = "WinnerPrize" + _idx_prize.ToString();
        _is_enabled = GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(CashDeskId), _prize_id + ".Enabled");

        if (_is_enabled)
        {
          _prizes_enabled += 1;
        }
      }

      if (_prizes_enabled > 0)
      {
        _percentage_probability = Math.Round(100 / (Decimal)_prizes_enabled, 2, MidpointRounding.AwayFromZero);
      }

      for (Int32 _idx_prize = 1; _idx_prize <= MAX_PRIZES; _idx_prize++)
      {
        _prize_id = "WinnerPrize" + _idx_prize.ToString();
        _is_enabled = GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(CashDeskId), _prize_id + ".Enabled");

        if (_is_enabled)
        {
          if(GameId > TerminalDraw.TerminalDraw.WELCOME_DRAW_ID)
          {
            // Add game winner prize
            AddGameWinnerPrize(_idx_prize);
          }
          else
          {
          // Default WinnerPrize
          AddDefaultWinnerPrize(_percentage_probability, _prize_id, _idx_prize);
        }
        }
        else if (_idx_prize > 4)
        {
          break;
        }
      }

      _prizes_enabled = 0;

      for (Int32 _idx_prize = 1; _idx_prize <= MAX_PRIZES; _idx_prize++)
      {
        _prize_id   = "LoserPrize" + _idx_prize.ToString();
        _is_enabled = GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(CashDeskId), _prize_id + ".Enabled");

        if (_is_enabled)
        {
          _prizes_enabled += 1;
        }
      }

      if (_prizes_enabled > 0)
      {
        _percentage_probability = Math.Round(100 / (Decimal)_prizes_enabled, 2, MidpointRounding.AwayFromZero);
      }

      for (Int32 _idx_prize = 1; _idx_prize <= MAX_PRIZES; _idx_prize++)
      {
        _prize_id = "LoserPrize" + _idx_prize.ToString();
        _is_enabled = GeneralParam.GetBoolean("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(CashDeskId), _prize_id + ".Enabled");

        if (_is_enabled)
        {
          if (GameId > TerminalDraw.TerminalDraw.WELCOME_DRAW_ID)
          {
            // Add game loser prize
            AddGameLoserPrize(_idx_prize);
          }
          else
          {
          // Default WinnerPrize
          AddDefaultLoserPrize(_percentage_probability, _prize_id, _idx_prize);
        }
        }
        else if (_idx_prize > 1)
        {
          break;
        }
      }

      if (!Split.ReadSplitParameters(out m_splits))
      {
        return;
      }

      //m_is_winner = false;// TODO
    }

    #endregion // Constructor

    /// <summary>
    /// Add default winner prize
    /// </summary>
    /// <returns></returns>
    private void AddGameWinnerPrize(Int32 PrizeIdx)
    {
      // Add winner prize
      m_winner_prizes.Add(new SinglePrizeCategory ( 0, 100, m_game.PercentatgeCostReturn, PrizeIdx, m_game.ReturnUnits, true));
    } // AddGameWinnerPrize

    /// <summary>
    /// Add default winner prize
    /// </summary>
    /// <returns></returns>
    private void AddDefaultWinnerPrize(Decimal PercentageProbability, String PrizeId, Int32 PrizeIdx)
    {
      // Add winner prize
      m_winner_prizes.Add(new SinglePrizeCategory( GeneralParam.GetCurrency("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), _prize_id + ".Fixed", 0),
                                                   GeneralParam.GetDecimal("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), _prize_id + ".Percentage", 0),
                                                   GeneralParam.GetDecimal("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), _prize_id + ".Probability", PercentageProbability),
                                                   GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), _prize_id + ".PrizeId", PrizeIdx),
                                                   (PrizeType)GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), _prize_id + ".PrizeType", 0), true));

    } // AddDefaultWinnerPrize

    /// <summary>
    /// Add default winner prize
    /// </summary>
    /// <returns></returns>
    private void AddGameLoserPrize(Int32 PrizeIdx)
    {
      // Add loser prize
      m_loser_prizes.Add(new SinglePrizeCategory ( 0, 100, m_game.PercentatgeCostReturn, PrizeIdx, m_game.ReturnUnits, false));
    } // AddGameLoserPrize

    /// <summary>
    /// Add default winner prize
    /// </summary>
    /// <returns></returns>
    private void AddDefaultLoserPrize(Decimal PercentageProbability, String PrizeId, Int32 PrizeIdx)
    {
      // Add loser prize
      m_loser_prizes.Add(new SinglePrizeCategory(GeneralParam.GetCurrency("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), PrizeId + ".Fixed", 0),
                                                GeneralParam.GetDecimal("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), PrizeId + ".Percentage", 0),
                                                GeneralParam.GetDecimal("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), PrizeId + ".Probability", PercentageProbability),
                                                GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), PrizeId + ".PrizeId", PrizeIdx),
                                                (PrizeType)GeneralParam.GetInt32("CashDesk.DrawConfiguration" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(m_cash_desk_id), PrizeId + ".PrizeType", 1), false));

    } // AddDefaultLoserPrize

  } // Parameters

   
  // PURPOSE: Class to establish the Prize of a Draw
  // SDS 14-10-2015 New Draw Kind Redeemable (RE)

  public enum PrizeType
  {
    Unknown = 0,
    NoRedeemable = 1,
    NoRedeemable2 = 2,
    Redeemable = 3,
    UNR = 4,
    ReInKind = 5,
  }

  public class SinglePrizeCategory : IComparable<SinglePrizeCategory>
  {
    #region Members
    public Currency m_fixed;
    public Decimal m_percent;
    public Decimal m_percent_probability;
    public Int32 m_prize_id;
    public ACCOUNT_PROMO_CREDIT_TYPE m_credit_type;
    public PrizeType m_prize_type;
    public int m_position;
    public bool m_is_winner_type;
    #endregion

    #region Constructor

    public SinglePrizeCategory(Currency Fixed, Decimal Percent, Decimal PercentageProbability, Int32 PrizeId, PrizeType PrizeType, bool IsWinnerType)
    {
      m_fixed = Fixed;
      m_percent = Percent;
      m_percent_probability = PercentageProbability;
      m_prize_id = PrizeId;
      m_prize_type = PrizeType;
      m_is_winner_type = IsWinnerType;

      switch (m_prize_type)
      {
        case PrizeType.NoRedeemable:
          m_credit_type = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          break;

        case PrizeType.NoRedeemable2:
          m_credit_type = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          break;

        case PrizeType.Redeemable:
          m_credit_type = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE;
          break;

        case PrizeType.UNR:
          m_credit_type = ACCOUNT_PROMO_CREDIT_TYPE.UNR2;
          break;

        case PrizeType.ReInKind:
          m_credit_type = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND;
          break;

        case PrizeType.Unknown:
        default:
          m_prize_type = PrizeType.Unknown;
          m_credit_type = ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN;
          break;
      }
    }

    #endregion // Constructor

    #region Internal

    internal static SinglePrizeCategory[] ToArray(IList<SinglePrizeCategory> prizes)
    {
      SinglePrizeCategory[] _prizes;

      _prizes = new SinglePrizeCategory[prizes.Count];
      for (int _i = 0; _i < _prizes.Length; _i++)
      {
        _prizes[_i] = prizes[_i];
      }
      return _prizes;
    }

    internal SinglePrizeCategory Copy(int position)
    {
      SinglePrizeCategory _prize;

      _prize = new SinglePrizeCategory(this.m_fixed, this.m_percent, this.m_percent_probability, this.m_prize_id, this.m_prize_type, this.m_is_winner_type);
      _prize.m_position = position;

      return _prize;
    }

    #endregion

    #region IComparable
    public int CompareTo(SinglePrizeCategory other)
    {
      int _compare;

      if (other == null)
      {
        return 1;
      }
      if (this.m_position < other.m_position)
      {
        _compare = -1;
      }
      else if (this.m_position == other.m_position)
      {
        _compare = 0;
      }
      else
      {
        _compare = 1;
      }
      return _compare;
    }
    #endregion
  } // SinglePrize

  // PURPOSE: Class to obtain the result of a Draw
  // 
  public class Result
  {
    public DateTime m_date_draw;
    public List<int> m_winning_combination;
    public List<int> m_bet_numbers;
    public Int64 m_draw_id;

    public Boolean m_is_winner;
    public SinglePrizeCategory m_prize_category;
    // Prize
    public Currency m_re_awarded = 0;
    public Currency m_re_gross = 0; //LTC 28-SEP-2016

    public Currency m_nr_awarded = 0;
    public Currency m_nr_gross = 0;

    // Tax UNR
    public Currency m_unr_tax1 = 0;
    public Currency m_unr_tax2 = 0;
    public Currency m_unr_tax3 = 0;

    // Tax URE
    public Currency m_ure_tax1 = 0;
    public Currency m_ure_tax2 = 0;
    public Currency m_ure_tax3 = 0;

    public OutCashPrizeMode CashPrizeMode { get; set; }

    public decimal m_participation_amount;

    public bool IsCashPrize;  //ETP 18-NOV-2016

    public Result()
    {
      CashPrizeMode = new OutCashPrizeMode();
    }

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Establish the format of draw numbers
    // 
    //  PARAMS:
    //      - INPUT: List: list of the numbers
    //
    //      - OUTPUT:
    //
    // RETURNS: the numbers with the correct format
    // 
    //   NOTES: 
    //
    public string FormatNumbers(List<int> List)
    {
      String _str;

      _str = "";
      foreach (int _number in List)
      {
        _str += _number.ToString("00") + "-";
      }

      _str = _str.TrimEnd('-');
      return _str;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Establish the format for the winning combination
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: the winning combination of the draw with the correct format
    // 
    //   NOTES:
    //
    public string FormatWinningCombination()
    {
      return FormatNumbers(m_winning_combination);
    }

    //------------------------------------------------------------------------------
    // PURPOSE:  Establish the format for the bet numbers
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: the bet numbers of the draw with the correct format
    // 
    //   NOTES:
    //
    public string FormatBetNumbers()
    {
      return FormatNumbers(m_bet_numbers);
    }

    #endregion Public Methods

  } // Result

  public class OutCashPrizeMode
  {
    public decimal GrossPrize;

    public decimal NetPrize;

    public TaxParameter Tax1;

    public TaxParameter Tax2;

    public TaxParameter Tax3;

    public decimal ServiceCharge;

    public decimal Awarded; //EOR 11-OCT-2016

    public bool Winner;     //EOR 11-OCT-2016

    public OutCashPrizeMode()
    {
      Tax1 = new TaxParameter();
      Tax2 = new TaxParameter();
      Tax3 = new TaxParameter();
    }
  }

}
