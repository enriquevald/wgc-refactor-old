﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WSI.Common
{
  class PyramizationLevel
  {
    #region "Constants"

    private const Int32 DT_COLUMN_PROBABILITY = 0;
    private const Int32 DT_COLUMN_FIX = 1;
    private const Int32 DT_COLUMN_PCT_RECHARGE = 2;

    #endregion

    #region "Properties"

    public Int32 Probability { get; set; }
    public Int32 Fix { get; set; }
    public Int32 PctRecharge { get; set; }

    #endregion
    
    #region "Public methods"

    /// <summary>
    /// 
    /// </summary>
    /// <param name="PyramizationDistTable"></param>
    /// <returns></returns>
    public List<PyramizationLevel> GetPyramizationLevelList(DataTable PyramizationDistTable)
    {
      List<PyramizationLevel> _level_list;
      
      _level_list = new List<PyramizationLevel>();

      foreach (DataRow _row in PyramizationDistTable.Rows)
      {
        _level_list.Add(this.DataRowToObject(_row));
      }

      return _level_list;
    }

    #endregion

    #region "Private methods"

    private PyramizationLevel DataRowToObject(DataRow Row)
    {
      return new PyramizationLevel {
        Probability = Convert.ToInt32(Row.ItemArray[DT_COLUMN_PROBABILITY]),
        Fix = Convert.ToInt32(Row.ItemArray[DT_COLUMN_FIX]),
        PctRecharge = Convert.ToInt32(Row.ItemArray[DT_COLUMN_PCT_RECHARGE])
      };
    }

    #endregion
  }
}
