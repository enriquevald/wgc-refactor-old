//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Alarm.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: RCI
// 
// CREATION DATE: 15-JUL-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUL-2011 RCI    First release.
// 18-OCT-2011 JML    Fixed bug in DB_Save.
// 02-JUL-2012 ACC    Added Peru External Event.
// 03-MAR-2013 RBG    Added AlarmCode TerminalSystem_PlaySessionManualClosed.
// 02-MAY-2014 JML    Added Alarm Codes Dictionary
// 06-MAY-2014 AMF    Fixed Bug WIG-876: Generate custom alarm
// 08-JUL-2014 JPJ    Added multilanguage support
// 06-AUG-2014 DCS    Fixed Bug WIG-1134: Alarms associated with patterns only insert in one language
// 29-JUN-2015 FJC    Product Backlog Item 282.
// 07-SEP-2015 ETP    BUG 4195 FIXED. Exceptions in Log WCP 
// 11-NOV-2015 GMV    TFS 5275 : HighRollers Extended definition
// 30-MAY-2015 AMF    PBI 13378: CountR - Alarmas CountR
// 21-SEP-2017 RAB    PBI 29318 - WIGOS-3699 MES13 Specific alarms - Gaming table bets over
// 28-SEP-2017 RAB    PBI 29842: WIGOS-3757 MES13 Specific alarms - Gaming table player sitting time
// 29-SEP-2017 RAB    PBI 29841: WIGOS-3756 MES13 Specific alarms - Gaming table bet deviation
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

namespace WSI.Common
{
  public class Alarm
  {
    private const int READ_PERIOD = 60 * 1000;
    private static Boolean m_avoid_register = false;
    private static int m_tick = Misc.GetTickCount() - READ_PERIOD;

    // Alarms reserved for pattern alarms
    //0x00980000   -->  9961472
    // ...to....
    //0x01000000   --> 16777216
    public const Int64 PatternIDOffset = 9961472;
    public const Int32 PatternCategoryID = 44;

    // JML 5-MAY-2014
    public struct ALARMS_CATALOG
    {
      //Public GroupId As Integer
      public string GroupName;
      //Public CategoryId As Integer
      public string CategoryName;
      public int AlarmCode;
      public string AlarmName;
      public string AlarmDescription;
    }



    #region Public Methods

    public static String ResourceId(AlarmCode Code)
    {
      return ResourceId((UInt32)Code);
    }

    public static String ResourceId(UInt32 Code)
    {
      return "STR_EA_0x" + Code.ToString("X").PadLeft(8, '0');
    }

    public static String Description(AlarmCode Code)
    {
      return Resource.String(ResourceId(Code));
    }

    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   AlarmCode Code)
    {
      return Register(SourceCode, SourceId, SourceName, Code, null);
    }

    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   AlarmCode Code,
                                   String Description)
    {
      AlarmSeverity _severity;
      String _desc;

      if (String.IsNullOrEmpty(Description))
      {
        _desc = Alarm.Description(Code);
      }
      else
      {
        _desc = Description;
      }

      switch (Code)
      {
        case AlarmCode.TerminalSystem_WcpConnected:
          _severity = AlarmSeverity.Info;

          if (!PeruEvents.InsertTechnicalEvent((Int32)SourceId, false, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_HOST_DISCONNECTED, WGDB.Now))
          {
            Log.Error("InsertTechnicalEvent: return false. ");
          }

          if (!ColJuegos.InsertColJuegosEvent(ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_HOST_DISCONNECTED, (Int32)SourceId))
          {
            Log.Error("InsertColJuegosEvent: return false. ");
          }

          break;

        case AlarmCode.TerminalSystem_Wc2Connected:
        case AlarmCode.Service_Started:
        case AlarmCode.Service_Running:
        case AlarmCode.Service_NewVersionDownloaded:
        case AlarmCode.SystemPowerStatus_ResumeCritical:
        case AlarmCode.TerminalSystem_WwpConnectedToCenter:
        case AlarmCode.TerminalSystem_WwpSiteConnected:
        case AlarmCode.TerminalSystem_SasHostFirstTime:        
          _severity = AlarmSeverity.Info;
          break;

        case AlarmCode.TerminalSystem_PlaySessionWasManuallyClosed:
        case AlarmCode.TerminalSystem_GameMeterReset:
        case AlarmCode.TerminalSystem_GameMeterBigIncrement:
        case AlarmCode.TerminalSystem_HPCMeterBigIncrement:
        case AlarmCode.TerminalSystem_MachineMeterReset:
        case AlarmCode.TerminalSystem_MachineMeterBigIncrement:
        case AlarmCode.TerminalSystem_SasMeterReset:
        case AlarmCode.TerminalSystem_SasMeterBigIncrement:
        case AlarmCode.Service_Stopping:
        case AlarmCode.Service_StandBy:
        case AlarmCode.Service_LicenseWillExpireSoon:
        case AlarmCode.Service_NewVersionAvailable:
        case AlarmCode.User_MaxLoginAttemps:
        case AlarmCode.SystemPowerStatus_PowerStatusChange:
        case AlarmCode.SystemPowerStatus_BatteryLow:
        case AlarmCode.WWPClientPSA_ErrorGettingDataDailyReport:
        case AlarmCode.WWPClientPSA_ErrorGettingDataPlaySessions:
        case AlarmCode.WWPClientPSA_ErrorGettingDataStatistics:
        case AlarmCode.MultiSite_AccountWithNegativePoints:
        case AlarmCode.MultiSite_UnknownSiteTriedToConnect:
        case AlarmCode.TerminalSystem_MaxValueModified:
        case AlarmCode.User_GamingTables_BetsOverHandByCustomer:
        case AlarmCode.User_GamingTables_BetsVariationCustomerPerSession:
        case AlarmCode.User_GamingTables_PlayerSittingTime:
          _severity = AlarmSeverity.Warning;
          break;

        case AlarmCode.TerminalSystem_WcpDisconnected:
          _severity = AlarmSeverity.Error;

          if (!PeruEvents.InsertTechnicalEvent((Int32)SourceId, true, PeruEvents.ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_HOST_DISCONNECTED, WGDB.Now))
          {
            Log.Error("InsertTechnicalEvent: return false. ");
          }


          if (!ColJuegos.InsertColJuegosEvent(ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_HOST_DISCONNECTED, (Int32)SourceId))
          {
            Log.Error("InsertColJuegosEvent: return false. ");
          }

          break;

        case AlarmCode.TerminalSystem_Wc2Disconnected:
        case AlarmCode.Service_Stopped:
        case AlarmCode.Service_LicenseExpired:
        default:
          _severity = AlarmSeverity.Error;


          break;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Register(SourceCode, SourceId, SourceName, (UInt32)Code, _desc, _severity, DateTime.MinValue, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();

          return true;
        }
      }

      return false;
    } // Register

    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   UInt32 AlarmCode,
                                   String Description,
                                   AlarmSeverity Severity)
    {
      Boolean _result;

      _result = false;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Register(SourceCode, SourceId, SourceName, AlarmCode, Description, Severity, DateTime.MinValue, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();

          _result = true;
        }
      }

      return _result;
    }

    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   UInt32 AlarmCode,
                                   String Description,
                                   AlarmSeverity Severity,
                                   DateTime GeneratedDateTime,
                                   SqlTransaction SqlTrx)
    {
      Int64 _alarm_id;
      DateTime _dt = new DateTime();

      return Register(SourceCode, SourceId, SourceName, AlarmCode, Description, Severity, GeneratedDateTime, false, _dt, out _alarm_id, SqlTrx);
    } // Register


    //FJC
    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   UInt32 AlarmCode,
                                   String Description,
                                   AlarmSeverity Severity,
                                   DateTime GeneratedDateTime,
                               Int64 TechnicalAccountId,
                               SqlTransaction SqlTrx)
    {
      Int64 _alarm_id;
      DateTime _dt = new DateTime();

      return Register(SourceCode, SourceId, SourceName, AlarmCode, Description, Severity, GeneratedDateTime, false, _dt, out _alarm_id, TechnicalAccountId, SqlTrx);
    } // Register 

    //FJC
    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   UInt32 AlarmCode,
                                   String Description,
                                   AlarmSeverity Severity,
                                   DateTime GeneratedDateTime,
                                   Boolean InsertIfNotExist,
                                   DateTime IntervalInsertIfNotExist,
                                   out Int64 AlarmId,
                                   SqlTransaction SqlTrx)
    {
      AlarmId = 0;

      return Register(SourceCode, SourceId, SourceName, AlarmCode, Description, Severity, GeneratedDateTime, InsertIfNotExist, IntervalInsertIfNotExist, out AlarmId, -1, SqlTrx);
    } // Register

    public static Boolean Register(AlarmSourceCode SourceCode,
                                   Int64 SourceId,
                                   String SourceName,
                                   UInt32 AlarmCode,
                                   String Description,
                                   AlarmSeverity Severity,
                                   DateTime GeneratedDateTime,
                                   Boolean InsertIfNotExist,
                                   DateTime IntervalInsertIfNotExist,
                                   out Int64 AlarmId,
                                   Int64 TechnicalAccountId,
                                   SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_rows;
      SqlParameter _output_param;

      String _technician_user_name;
      Boolean _dummy_has_activity;
      DateTime _dummy_logged_in;
      String _dummy_logon_computer;
      DateTime _dummy_last_activity;
      TimeSpan _dummy_without_activity;
      String _dummy_last_action;
      Users.EXIT_CODE _dummy_exit_code;


      AlarmId = 0;
      _technician_user_name = String.Empty;

      if (Misc.GetElapsedTicks(m_tick) >= READ_PERIOD)
      {
        m_avoid_register = false;

        if (GeneralParam.GetBoolean("Site", "DisableNewAlarms", false))
        {
          m_avoid_register = true;
        }

        m_tick = Misc.GetTickCount();
      }

      if (m_avoid_register)
      {

        return true;
      }

      try
      {
        _sb = new StringBuilder();
        if (InsertIfNotExist)
        {
          _sb.AppendLine("IF NOT EXISTS ( SELECT   1                                                      ");
          _sb.AppendLine("                  FROM   ALARMS WITH (INDEX(IX_ALARMS_DATETIME_SEVERITY_SOURCE)) ");
          _sb.AppendLine("                 WHERE   AL_SOURCE_ID = @pSourceId                              ");
          _sb.AppendLine("                   AND   AL_SOURCE_CODE = @pSourceCode                          ");
          _sb.AppendLine("                   AND   AL_SEVERITY = @pSeverity                               ");
          _sb.AppendLine("                   AND   AL_ALARM_CODE = @pAlarmCode                            ");
          _sb.AppendLine("                   AND   AL_ALARM_DESCRIPTION = @pAlarmDescription              ");
          _sb.AppendLine("                   AND   AL_DATETIME >= @pInterval                              ");
          _sb.AppendLine("               )                                                                ");
        }
        _sb.AppendLine(" INSERT INTO  ALARMS (AL_SOURCE_CODE       ");
        _sb.AppendLine("                    , AL_SOURCE_ID         ");
        _sb.AppendLine("                    , AL_SOURCE_NAME       ");
        _sb.AppendLine("                    , AL_ALARM_CODE        ");
        _sb.AppendLine("                    , AL_ALARM_NAME        ");
        _sb.AppendLine("                    , AL_ALARM_DESCRIPTION ");
        _sb.AppendLine("                    , AL_SEVERITY          ");
        _sb.AppendLine("                    , AL_REPORTED          ");
        _sb.AppendLine("                    , AL_DATETIME          ");
        if (TechnicalAccountId > 0)
        {
          _sb.AppendLine("                  , AL_TECHNICIAN_ID     ");
          _sb.AppendLine("                  , AL_ACK_DATETIME      ");
          _sb.AppendLine("                  , AL_ACK_USER_ID       ");
          _sb.AppendLine("                  , AL_ACK_USER_NAME     ");

        }
        else if (TechnicalAccountId == 0)
        {
          _sb.AppendLine("                  , AL_TECHNICIAN_ID     ");
        }

        _sb.AppendLine("                     )                     ");
        _sb.AppendLine("              VALUES (@pSourceCode         ");
        _sb.AppendLine("                    , @pSourceId           ");
        _sb.AppendLine("                    , @pSourceName         ");
        _sb.AppendLine("                    , @pAlarmCode          ");
        _sb.AppendLine("                    , @pAlarmName          ");
        _sb.AppendLine("                    , @pAlarmDescription   ");
        _sb.AppendLine("                    , @pSeverity           ");
        _sb.AppendLine("                    , GETDATE ()           ");

        if (GeneratedDateTime == DateTime.MinValue)
        {
          _sb.AppendLine("                  , GETDATE ()           ");
        }
        else
        {
          _sb.AppendLine("                  , @pGenerated          ");
        }
        if (TechnicalAccountId > 0)
        {
          _sb.AppendLine("                  , @pTechnicalAccountId ");
          _sb.AppendLine("                  , @pDatetime           ");
          _sb.AppendLine("                  , @pUserId             ");
          _sb.AppendLine("                  , @pUserName           ");
        }
        else if (TechnicalAccountId == 0)
        {
          _sb.AppendLine("                  , @pTechnicalAccountId ");
        }

        _sb.AppendLine("                     )                     ");
        _sb.AppendLine(" SET @pAlarmId = SCOPE_IDENTITY()          ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          if (InsertIfNotExist)
          {
            _sql_cmd.Parameters.Add("@pInterval", SqlDbType.DateTime).Value = IntervalInsertIfNotExist;
          }
          _sql_cmd.Parameters.Add("@pSourceCode", SqlDbType.Int).Value = SourceCode;
          _sql_cmd.Parameters.Add("@pSourceId", SqlDbType.BigInt).Value = SourceId;
          _sql_cmd.Parameters.Add("@pSourceName", SqlDbType.NVarChar).Value = SourceName;
          _sql_cmd.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = AlarmCode;
          _sql_cmd.Parameters.Add("@pAlarmName", SqlDbType.NVarChar).Value = SourceCode.ToString();
          _sql_cmd.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar).Value = Description;
          _sql_cmd.Parameters.Add("@pSeverity", SqlDbType.Int).Value = Severity;
          if (GeneratedDateTime != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pGenerated", SqlDbType.DateTime).Value = GeneratedDateTime;
          }

          if (TechnicalAccountId > 0)
          {
            _sql_cmd.Parameters.Add("@pTechnicalAccountId", SqlDbType.Int).Value = TechnicalAccountId;
            _sql_cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now;
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = TechnicalAccountId;
            Users.GetActivityUser((Int32)TechnicalAccountId, out _dummy_has_activity, out _technician_user_name, out _dummy_logged_in, out _dummy_logon_computer, out _dummy_last_activity, out _dummy_without_activity, out _dummy_last_action, out _dummy_exit_code);
            _sql_cmd.Parameters.Add("@pUserName", SqlDbType.Text).Value = _technician_user_name;

          }
          else if (TechnicalAccountId == 0)
          {
            _sql_cmd.Parameters.Add("@pTechnicalAccountId", SqlDbType.Int).Value = TechnicalAccountId;
          }

          _output_param = new SqlParameter("@pAlarmId", SqlDbType.BigInt);


          _output_param.Direction = ParameterDirection.Output;

          _sql_cmd.Parameters.Add(_output_param);

          _num_rows = _sql_cmd.ExecuteNonQuery();

          if (_output_param.Value != DBNull.Value)
          {
            AlarmId = (Int64)_output_param.Value;
          }

          if (_num_rows != 1 && !InsertIfNotExist)
          {
            Log.Warning(String.Format(" Alarm.Register: Error inserting Alarm.  Rows affected {0}", _num_rows));
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Register

    public static Boolean Register(AlarmSourceCode SourceCode,
                               Int64 SourceId,
                               String SourceName,
                               UInt32 AlarmCode,
                               String Description,
                               Int64 OperadorId,
                               Int64 EstablecimientoId,
                               Int32 LineaId,
                               AlarmSeverity Severity,
                               DateTime GeneratedDateTime,
                               SqlTransaction SqlTrx)
    {
      StringBuilder _sql_str;



      if (Misc.GetElapsedTicks(m_tick) >= READ_PERIOD)
      {
        int _disabled;

        m_avoid_register = false;

        _disabled = 0;
        if (int.TryParse(Misc.ReadGeneralParams("Site", "DisableNewAlarms", SqlTrx), out _disabled))
        {
          if (_disabled != 0)
          {
            m_avoid_register = true;
          }
        }

        m_tick = Misc.GetTickCount();
      }

      if (m_avoid_register)
      {
        return true;
      }

      try
      {
        _sql_str = new StringBuilder();

        _sql_str.AppendLine("INSERT INTO ALARMS ( AL_SOURCE_CODE        ");
        _sql_str.AppendLine("                   , AL_SOURCE_ID          ");
        _sql_str.AppendLine("                   , AL_SOURCE_NAME        ");
        _sql_str.AppendLine("                   , AL_ALARM_CODE         ");
        _sql_str.AppendLine("                   , AL_ALARM_NAME         ");
        _sql_str.AppendLine("                   , AL_ALARM_DESCRIPTION  ");
        _sql_str.AppendLine("                   , AL_SEVERITY           ");
        _sql_str.AppendLine("                   , AL_REPORTED           ");
        _sql_str.AppendLine("                   , AL_DATETIME           ");
        _sql_str.AppendLine("                   )                       ");
        _sql_str.AppendLine("     VALUES        ( @pSourceCode          ");
        _sql_str.AppendLine("                   , @pSourceId            ");
        _sql_str.AppendLine("                   , @pSourceName          ");
        _sql_str.AppendLine("                   , @pAlarmCode           ");
        _sql_str.AppendLine("                   , @pAlarmName           ");
        _sql_str.AppendLine("                   , @pAlarmDescription    ");
        _sql_str.AppendLine("                   , @pSeverity            ");
        _sql_str.AppendLine("                   , GETDATE ()            ");

        if (GeneratedDateTime == DateTime.MinValue)
        {
          _sql_str.AppendLine("                 , GETDATE ()           ");
        }
        else
        {
          _sql_str.AppendLine("                 , @pGenerated          ");
        }
        _sql_str.AppendLine("                   )                      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSourceCode", SqlDbType.Int).Value = SourceCode;
          _sql_cmd.Parameters.Add("@pSourceId", SqlDbType.BigInt).Value = SourceId;
          _sql_cmd.Parameters.Add("@pSourceName", SqlDbType.NVarChar).Value = SourceName;
          _sql_cmd.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = AlarmCode;
          _sql_cmd.Parameters.Add("@pAlarmName", SqlDbType.NVarChar).Value = SourceCode.ToString();
          _sql_cmd.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar).Value = Description;


          _sql_cmd.Parameters.Add("@pSeverity", SqlDbType.Int).Value = Severity;

          if (GeneratedDateTime != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pGenerated", SqlDbType.DateTime).Value = GeneratedDateTime;
          }

          return (_sql_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Register

    // JML 5-MAY-2014
    public static Boolean GetAlarmsCatalog(Boolean IncludePersonalAlarms, Int64? NotIncludeThisAlarm, out Dictionary<int, ALARMS_CATALOG> AlarmCatalog, LanguageIdentifier Language)
    {
      StringBuilder _sb;
      ALARMS_CATALOG _alarm_struct;

      AlarmCatalog = new Dictionary<Int32, ALARMS_CATALOG>();

      try
      {
        _sb = new System.Text.StringBuilder();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb.AppendLine("SELECT   ALG_NAME AS 'GROUP' ");
          _sb.AppendLine("       , ALC_NAME AS 'CATEGORY' ");
          _sb.AppendLine("       , ALCG_ALARM_CODE AS 'ALARMCODE' ");
          _sb.AppendLine("       , ALCG_NAME AS 'ALARMNAME' ");
          _sb.AppendLine("       , ALCG_DESCRIPTION AS 'ALARMDESCRIPTION' ");
          _sb.AppendLine("  FROM   ALARM_GROUPS ");
          _sb.AppendLine("  LEFT   JOIN ALARM_CATEGORIES ON ALC_ALARM_GROUP_ID = ALG_ALARM_GROUP_ID ");
          _sb.AppendLine("  LEFT   JOIN ALARM_CATALOG_PER_CATEGORY ON ALCC_CATEGORY = ALC_CATEGORY_ID ");
          _sb.AppendLine("  LEFT   JOIN ALARM_CATALOG  ON ALCG_ALARM_CODE = ALCC_ALARM_CODE ");
          _sb.AppendLine(" WHERE   ALCG_VISIBLE = 1");
          _sb.AppendLine("   AND   ALG_VISIBLE = 1");
          _sb.AppendLine("   AND   ALCG_LANGUAGE_ID = @pLanguageID");
          _sb.AppendLine("   AND   ALC_LANGUAGE_ID = @pLanguageID");
          _sb.AppendLine("   AND   ALG_LANGUAGE_ID = @pLanguageID");
          if (!IncludePersonalAlarms)
          {
            _sb.AppendLine("   AND   ALCG_TYPE <> 1 ");
          }
          if (NotIncludeThisAlarm != null)
          {
            _sb.AppendLine("   AND   ALCG_ALARM_CODE <> @pAlarmCode ");
          }
          _sb.AppendLine(" ORDER   BY ALG_NAME, ALC_NAME, ALCG_NAME ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (NotIncludeThisAlarm != null)
            {
              _sql_cmd.Parameters.Add("@pAlarmCode", SqlDbType.BigInt).Value = NotIncludeThisAlarm;
            }
            _sql_cmd.Parameters.Add("@pLanguageID", SqlDbType.Int).Value = (Int32)Language;

            using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
            {
              while (_sql_rd.Read())
              {
                if(!AlarmCatalog.ContainsKey(_sql_rd.GetInt32(2)))
                {
                  _alarm_struct.GroupName = _sql_rd.GetString(0);
                  _alarm_struct.CategoryName = _sql_rd.GetString(1);
                  _alarm_struct.AlarmCode = _sql_rd.GetInt32(2);
                  _alarm_struct.AlarmName = _sql_rd.GetString(3);
                  _alarm_struct.AlarmDescription = _sql_rd.GetString(4);
                  AlarmCatalog.Add(_alarm_struct.AlarmCode, _alarm_struct);
                }                
              }
            }
          }
        }
        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetAlarmsCatalog

    public static bool InsertAlarmCatalog(UInt32 AlarmCode, String AlarmName, String AlarmDescription, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_rows;
      SqlParameter _sql_param;
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE ");
        _sb.AppendLine("                          , ALCG_LANGUAGE_ID ");
        _sb.AppendLine("                          , ALCG_TYPE       ");
        _sb.AppendLine("                          , ALCG_NAME       ");
        _sb.AppendLine("                          , ALCG_DESCRIPTION");
        _sb.AppendLine("                          , ALCG_VISIBLE    ");
        _sb.AppendLine("                          )                 ");
        _sb.AppendLine("                    VALUES (@pAlarmCode     ");
        _sb.AppendLine("                          , @pLanguageID    ");
        _sb.AppendLine("                          , @pType          ");
        _sb.AppendLine("                          , @pName          ");
        _sb.AppendLine("                          , @pDescription   ");
        _sb.AppendLine("                          , @pVisible       ");
        _sb.AppendLine("                          )                 ");

        //07-AUG-2014 DCS: Add second language for every insert in alarm catalog

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = AlarmCode;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = 1; // It's not a system alarm, it's a custom alarm
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar, 50).Value = AlarmName;
          _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar, 350).Value = AlarmDescription;
          _sql_cmd.Parameters.Add("@pVisible", SqlDbType.Int).Value = 1;
          _sql_param = _sql_cmd.Parameters.Add("@pLanguageID", SqlDbType.Int);

          _sql_param.Value = LanguageIdentifier.English;
          _num_rows = _sql_cmd.ExecuteNonQuery();

          _sql_param.Value = LanguageIdentifier.Spanish;
          _num_rows += _sql_cmd.ExecuteNonQuery();

          //07-AUG-2014 DCS: Update all languages - Rows updated are the num of languages inserted
          if (_num_rows != 2)
          {
            Log.Warning(String.Format(" InsertAlarmCatalog: Error inserting Alarm Catalog.  Rows affected {0}", _num_rows));

            return false;

          }

        }

        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE");
        _sb.AppendLine("                                       , ALCC_CATEGORY  ");
        _sb.AppendLine("                                       , ALCC_TYPE      ");
        _sb.AppendLine("                                       , ALCC_DATETIME  ");
        _sb.AppendLine("                                       )                ");
        _sb.AppendLine("                                 VALUES (@pAlarmCode    ");
        _sb.AppendLine("                                       , @pCategory     ");
        _sb.AppendLine("                                       , @pType         ");
        _sb.AppendLine("                                       , GETDATE()      ");
        _sb.AppendLine("                                       )                ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = AlarmCode;
          _sql_cmd.Parameters.Add("@pCategory", SqlDbType.Int).Value = PatternCategoryID;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = 1; // It's not a system alarm, it's a custom alarm

          _num_rows = _sql_cmd.ExecuteNonQuery();

          if (_num_rows != 1)
          {
            Log.Warning(String.Format(" InsertAlarmCatalog: Error inserting Alarm Catalog per category.  Rows affected {0}", _num_rows));

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InsertAlarmCatalog

    public static bool UpdateAlarmCatalog(UInt32 AlarmCode, String AlarmName, String AlarmDescription, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_rows;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   UPDATE ALARM_CATALOG SET   ALCG_NAME         = @pName");
        _sb.AppendLine("                            , ALCG_DESCRIPTION  = @pDescription");
        _sb.AppendLine("    WHERE ALCG_ALARM_CODE   = @pAlarmCode");
        //07-AUG-2014 DCS: Update all languages
        //_sb.AppendLine("      AND ALCG_LANGUAGE_ID  = @pLanguageID");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = AlarmCode;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = 0;
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar, 50).Value = AlarmName;
          _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar, 350).Value = AlarmDescription;
          _sql_cmd.Parameters.Add("@pVisible", SqlDbType.Int).Value = 1;
          //07-AUG-2014 DCS: Update all languages
          //_sql_cmd.Parameters.Add("@pLanguageID", SqlDbType.Int).Value = WSI.Common.Resource.LanguageId;

          _num_rows = _sql_cmd.ExecuteNonQuery();

          //07-AUG-2014 DCS: Update all languages - Rows updated are the num of languages inserted
          if (_num_rows != 2)
          {
            Log.Warning(String.Format(" UpdateAlarmCatalog: Error inserting Alarm Catalog.  Rows affected {0}", _num_rows));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // UpdateAlarmCatalog

    //------------------------------------------------------------------------------
    // PURPOSE :  Check if today is client's Birthday or soon (using Stored Procedure)
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - HolderName
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    //
    public static Boolean CheckBirthdayAlarm(Int64 AccountId, String HolderName, String TerminalName, TerminalTypes TerminalType)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!MultiPromos.CheckBirthdayAlarm(AccountId, HolderName, TerminalName, TerminalType, _db_trx.SqlTransaction))
        {

          return false;
        }

        _db_trx.Commit();
      }

      return true;
    }

    public static void GeneraAlarmByMeter(String AlarmName,
                                          AlarmCode AlarmBase,
                                          Int32 MeterCode,
                                          Int32 TerminalId,
                                          String ProviderId,
                                          String TerminalName,
                                          Int64 OldValue,
                                          Int64 NewValue)
    {
      StringBuilder _sb;
      String _meter_name;

      _meter_name = Resource.String_En(String.Format("STR_SAS_METER_DESC_{0:00000}", MeterCode));

      _sb = new StringBuilder();
      _sb.AppendLine(AlarmName);
      _sb.AppendLine(" Terminal: " + TerminalId.ToString());
      _sb.AppendLine("     Name: " + Misc.ConcatProviderAndTerminal(ProviderId, TerminalName));
      _sb.AppendLine(" Old meter values: " + _meter_name + ": " + OldValue.ToString());
      _sb.AppendLine(" New meter values: " + _meter_name + ": " + NewValue.ToString());

      Alarm.Register(AlarmSourceCode.TerminalSystem,
                     TerminalId,
                     Misc.ConcatProviderAndTerminal(ProviderId, TerminalName),
                     AlarmBase + MeterCode,
                     _sb.ToString());

      Log.Warning(_sb.ToString());
    } // GeneraAlarmByMeter

    #endregion // Public Methods

  } // Alarm

  public class AlarmList : List<Alarm>
  {
    #region Public Methods

    public static AlarmList Create(AlarmSourceCode AlarmSource)
    {
      AlarmList _alarm_list;

      _alarm_list = new AlarmList();
      //switch (AlarmSource)
      //{
      //  case AlarmSourceCode.GameTerminal:
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalTestModeEnter, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalTestModeExit, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalConfigChanged, AlarmSeverity.Info));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalPayoutChanged, AlarmSeverity.Info));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalMeterReset, AlarmSeverity.Warning));
      //    //_alarm_list.Add(new Alarm(AlarmCode.TerminalEventList, AlarmSeverity.Warning));
      //    break;
      //  case AlarmSourceCode.Cashier:
      //    break;
      //  case AlarmSourceCode.User:
      //    _alarm_list.Add(new Alarm(AlarmCode.MaxLoginAttemps, AlarmSeverity.Warning));
      //    break;
      //  default:          //.Default
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalTestModeEnter, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalTestModeExit, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalConfigChanged, AlarmSeverity.Info));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalPayoutChanged, AlarmSeverity.Info));
      //    _alarm_list.Add(new Alarm(AlarmCode.TerminalMeterReset, AlarmSeverity.Warning));
      //    //_alarm_list.Add(new Alarm(AlarmCode.TerminalEventList, AlarmSeverity.Warning));
      //    _alarm_list.Add(new Alarm(AlarmCode.MaxLoginAttemps, AlarmSeverity.Warning));
      //    break;
      //}


      return _alarm_list;
    } // Create

    #endregion // Public Methods

  } // AlarmList

} // WSI.Common
