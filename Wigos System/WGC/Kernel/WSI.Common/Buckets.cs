﻿//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Buckets.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Buckets
// 
//        AUTHOR: Sergi Martínez
// 
// CREATION DATE: 10-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-DEC-2015 SMN    First release (Header).
// 10-DEC-2015 SGB    Add new Enum's
// 11-DEC-2015 JRC    Add new Enum's
// 19-JAN-2016 JRC    Added all funcionality 
// 19-JAN-2016 FGB    Added new class BucketVoucher
// 20-JAN-2016 FGB    Added new method Buckets.BucketsEmpty
// 20-JAN-2016 FGB    Added new method Buckets.BucketsExpiration
// 25-JAN-2016 FGB    Now Buckets.ReadAndDeleteBucketsExpired returns the delete records sorted by customer.
// 26-JAN-2016 FGB    Renamed method Buckets.BucketsExpiration to Buckets.BucketsExpirationByInactivityDays
// 26-JAN-2016 FGB    Added new classes Bucket_Data and BucketsList
// 01-FEB-2016 FGB    Change all call to ELPLog to Log
// 02-FEB-2016 ETP    Now we control that the bucket is visible by mask parameter.
// 09-FEB-2016 FGB    Changed parameters in BucketUpdate.UpdateCustomerBucket
// 10-FEB-2016 FGB    Changed SQL generated at GetUpdateCustomerBucketSQL for SQL Server 2005 Compatibility
// 19-FEB-2016 ETP    Product Backlog Item 9748: Multiple buckets: Canje de buckets RE
// 29-FEB-2016 FGB    Added parameter UpdateCustomerBucket_Date to method BucketsUpdate.GetUpdateCustomerBucketSQL
// 02-MAR-2016 ETP    Fixed bug 10210: Multiple buckets: Trunc points.
// 18-MAR-2016 SMN    Product Backlog Item 10792:Multiple Buckets: Desactivar Acumulación de Buckets si SPACE is enabled
// 29-MAR-2016 FGB    Renamed Buckets.ReadAndDeleteBucketsExpired to Bucket.ReadBucketsToExpire and it processes a record at a time, to avoid locks.
// 22-MAR-2016 FGB    PBI 9781:Multiple Buckets: Refactorización -> Parameter TheoricHold renamed to TheoricPayout in BucketsUpdate.CalculateAccountBuckets
// 06-JUN-2016 SMN    Fixed Bug 14163:Buckets: no están caducando
// 07-JUN-2016 DLL    Fixed Bug 14302:Añadir/Fijar Puntos: no funciona para cuentas con ID > 2^31
// 06-JUN-2016 JCA    Bug 14240:PimPamGo: Al jugar no genera puntos
// 13-JUL-2016 SMN    Fixed Bug 14837:Mesa de juego (Cashless,TITO): No se pueden sacar a los jugadores cuando están jugando
// 02-AGO-2016 JRC    PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
// 30-AGO-2016 XGJ    Add parameter BucketMultiplier to CalcutaeAnount
// 22-SEP-2016 FJC    Fixed Bug 18019:PromoBOX: no se muestran los puntos negativos de la cuenta de un cliente
// 27-SEP-2016 SMN    Fixed Bug 18193:Buckets: El multiplicador horario aplica al bucket de nivel
// 30-SEP-2016 SMN    Fixed Bug 18356:Buckets: Se están otorgando puntos a pesar de estar configurados para no otorgarlos
// 04-OCT-2016 FJC    Fixed Bug 18264:Buckets: en movimientos de cuenta el valor inicial está a 0
// 11-OCT-2016 SMN    Fixed Bug 18945: Mesas de Juego - Buckets: Si SPACE está habilitado, se suman puntos desde Mesas de Juego
// 09-FEB-2017 ETP    Fixed Bug 24500: WCP: Exception en el WCP al jugar con una cuenta anonima
// 07-MAR-2017 RLO    Bug 25309:WCP: Buckets expiration throws an exception
// 19-APR-2017 AMF    WIGOS-706: Junkets - Technical story
// 16-OCT-2017 DHA    Bug 30253:WIGOS-5432 [Ticket #9123] Accounts movements - Points in tables v03.006.0023
// 07-MAY-2018 AGS    Bug 32587:WIGOS-10725 Error in log WWP_Center
// 19-JUL-2017 OMC    Added BucketsUpdateReason enum for Marina del Sol.
// 27-JUL-2017 FJC    WIGOS-2021 WSPoints - Method - UpdateBucket
// 16-MAY-2018 GDA    WIGOS-11802 -- [Ticket #14327] Ebox tardan en conectar
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Net;
using System.Runtime.InteropServices;
using System.Management;
using System.IO;
using System.Xml;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Data.SqlClient;
using System.IO.Compression;
using System.Diagnostics;
using System.Data.SqlTypes;
using System.Data;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace WSI.Common
{
  public static class Buckets
  {
    #region Enums

    public enum BucketSystemType
    {
      System = 1,
      User = 2
    }

    public enum BucketType
    {
      Unknown = 0,
      RedemptionPoints = 1,                //--PuntosCanje
      RankingLevelPoints = 2,              //--PuntosNivel
      Credit_NR = 3,                       //--NR     
      Credit_RE = 4,                       //--RE     
      Comp1_RedemptionPoints = 5,          //--Comp1Canje 
      Comp2_Credit_NR = 6,                 //--Comp2NR 
      Comp3_Credit_RE = 7,                 //--Comp3RE
      RankingLevelPoints_Generated = 8,      //--PuntosNivel Generados
      RankingLevelPoints_Discretional = 9,   //--PuntosNivel Discrecionales
    }

    public enum BucketId
    {
      RedemptionPoints = 1,                //--PuntosCanje
      RankingLevelPoints = 2,              //--PuntosNivel
      Credit_NR = 3,                       //--NR     
      Credit_RE = 4,                       //--RE     
      Comp1_RedemptionPoints = 5,          //--Comp1Canje 
      Comp2_Credit_NR = 6,                 //--Comp2NR 
      Comp3_Credit_RE = 7,                 //--Comp3RE 
      RankingLevelPoints_Generated = 8,      //--PuntosNivel Generados        (automaticos)
      RankingLevelPoints_Discretional = 9,   //--PuntosNivel Discrecionales   (manuales)
    }

    public enum BucketOperation
    {
      //DISCRETIONAL  = Manual changes 
      //GENERATED     = Automatic changes, at the end of play sessions 

      ADD_DISCRETIONAL = 1,
      SUB_DISCRETIONAL = 2,
      SET_DISCRETIONAL = 3,
      EXPIRE = 4,
      ADD_GENERATED = 5,
      SUB_GENERATED = 6,
      SET_GENERATED = 7,
      //ADD = 8,
      //SUB = 9,
      //SET = 10
    }

    //BucketVisibility
    //first 8 bits reserved for visibility in devices
    //next 24 bits reserved for visibility in reports

    public enum BucketVisibility
    {
      Cashier = 1,
      Promobox = 2,
      LCD = 4,
      // section future devices
      //empty = 8
      //empty = 16
      //empty = 32
      //empty = 64
      //empty = 128

      Report_BucketsByClient = 256
      //section future reports
      //empty = 512             reporte  2
      //empty = 1024            reporte  3
      //empty = 2048            reporte  4
      //empty = 4096            reporte  5
      //empty = 8192            reporte  6
      //empty = 16384           reporte  7
      //empty = 32768           reporte  8
      //empty = 65536           reporte  9
      //empty = 131072          reporte  10
      //empty = 262144          reporte  11
      //empty = 524288          reporte  12
      //empty = 1048576         reporte  13
      //empty = 2097152         reporte  14
      //empty = 4194304         reporte  15
      //empty = 8388608         reporte  16
      //empty = 16777216        reporte  17
      //empty = 33554432        reporte  18
      //empty = 67108864        reporte  19
      //empty = 134217728       reporte  20
      //empty = 268435456       reporte  21
      //empty = 536870912       reporte  22
      //empty = 1073741824      reporte  23
      //empty = 2147483648      reporte  24


    } // BucketVisibility


    public const int MAX_RECORDS_TO_EXPIRE = 500;
    public const ulong BaseBitFieldValue = 4294967288;
    //at this first stage, only the first 3 bits of BucketVisibility (devices) might be switched from 0 to 1
    //the remaining 29 bits will stay as 1.
    //the only possible values for the field will be

    // LCD promobox    cashier       field
    // 0       0           0       4294967288
    // 0       0           1       4294967289
    // 0       1           0       4294967290
    // 0       1           1       4294967291
    // 1       0           0       4294967292
    // 1       0           1       4294967293
    // 1       1           0       4294967294
    // 1       1           1       4294967295

    // so, in order to make calculations easier, we set a base value BaseBitFieldValue = 4294967288 and the final value of the field will be
    // BaseBitFieldValue or LCD
    // BaseBitFieldValue or Promobox
    // BaseBitFieldValue or Cashier


    public enum BucketUnits
    {
      Money = 1,
      Points = 2
    }

    #endregion Enums

    #region functions

    public static BucketUnits GetBucketUnits(BucketType Type)
    {
      switch (Type)
      {
        case BucketType.RedemptionPoints:
        case BucketType.RankingLevelPoints:
        case BucketType.RankingLevelPoints_Discretional:
        case BucketType.RankingLevelPoints_Generated:
          {
            return BucketUnits.Points;
          }
        case BucketType.Credit_NR:
        case BucketType.Credit_RE:
          {
            return BucketUnits.Money;
          }
        case BucketType.Comp1_RedemptionPoints:
          {
            return BucketUnits.Points;
          }
        case BucketType.Comp2_Credit_NR:
        case BucketType.Comp3_Credit_RE:
          {
            return BucketUnits.Money;
          }
        default:
          {
            return BucketUnits.Money;
          }
      }

    }

    public static Boolean ReadDeviceVisibility(Int64 DatabaseValue, BucketVisibility Device)
    {
      return ((Int32)Device & DatabaseValue) == (Int32)Device;
    }

    public static ulong SetDeviceVisibility(UInt64 BaseValue, BucketVisibility Device, Boolean Status)
    {
      UInt64 _result;

      _result = BaseValue;
      if (BaseValue == 0)
        BaseValue = BaseBitFieldValue;

      if (Status)
        _result = BaseValue | (UInt32)Device;
      else
        _result = BaseValue;

      return _result;
    } // SetDeviceVisibility

    /// <summary>
    /// Returns hardcoded properties of each bucket
    /// </summary>
    /// <param name="IdBucket">Bucket ID</param>
    /// <returns>Bucket_Properties</returns>
    public static Bucket_Properties GetProperties(BucketId IdBucket)
    {
      Bucket_Properties _bucket_properties = new Bucket_Properties();

      switch (IdBucket)
      {
        case BucketId.RedemptionPoints:
          _bucket_properties.can_disable = false;
          _bucket_properties.gui_can_add = true;
          _bucket_properties.gui_can_set = true;
          _bucket_properties.gui_can_sub = true;
          _bucket_properties.cashier_can_add = true;
          _bucket_properties.cashier_can_set = false;
          _bucket_properties.cashier_can_sub = true;
          _bucket_properties.visible_lcd_enabled = false;
          _bucket_properties.visible_promobox_enabled = false;
          _bucket_properties.gui_expiration_can_enabled = true;
          break;

        case BucketId.RankingLevelPoints:
          _bucket_properties.can_disable = false;
          _bucket_properties.gui_can_add = false;
          _bucket_properties.gui_can_set = false;
          _bucket_properties.gui_can_sub = false;
          _bucket_properties.cashier_can_add = false;
          _bucket_properties.cashier_can_set = false;
          _bucket_properties.cashier_can_sub = false;
          _bucket_properties.visible_lcd_enabled = false;
          _bucket_properties.visible_promobox_enabled = false;
          _bucket_properties.gui_expiration_can_enabled = false;
          break;

        case BucketId.Credit_NR:
          _bucket_properties.can_disable = true;
          _bucket_properties.gui_can_add = true;
          _bucket_properties.gui_can_set = true;
          _bucket_properties.gui_can_sub = true;
          _bucket_properties.cashier_can_add = true;
          _bucket_properties.cashier_can_set = false;
          _bucket_properties.cashier_can_sub = true;
          _bucket_properties.visible_lcd_enabled = false;
          _bucket_properties.visible_promobox_enabled = false;
          _bucket_properties.gui_expiration_can_enabled = true;
          break;

        case BucketId.Credit_RE:
          _bucket_properties.can_disable = true;
          _bucket_properties.gui_can_add = true;
          _bucket_properties.gui_can_set = true;
          _bucket_properties.gui_can_sub = true;
          _bucket_properties.cashier_can_add = true;
          _bucket_properties.cashier_can_set = false;
          _bucket_properties.cashier_can_sub = true;
          _bucket_properties.visible_lcd_enabled = false;
          _bucket_properties.visible_promobox_enabled = false;
          _bucket_properties.gui_expiration_can_enabled = true;
          break;

        case BucketId.Comp1_RedemptionPoints:
          _bucket_properties.can_disable = true;
          _bucket_properties.gui_can_add = true;
          _bucket_properties.gui_can_set = true;
          _bucket_properties.gui_can_sub = true;
          _bucket_properties.cashier_can_add = true;
          _bucket_properties.cashier_can_set = false;
          _bucket_properties.cashier_can_sub = true;
          _bucket_properties.visible_lcd_enabled = false;
          _bucket_properties.visible_promobox_enabled = false;
          _bucket_properties.gui_expiration_can_enabled = true;
          break;

        case BucketId.Comp2_Credit_NR:
          _bucket_properties.can_disable = true;
          _bucket_properties.gui_can_add = true;
          _bucket_properties.gui_can_set = true;
          _bucket_properties.gui_can_sub = true;
          _bucket_properties.cashier_can_add = true;
          _bucket_properties.cashier_can_set = false;
          _bucket_properties.cashier_can_sub = true;
          _bucket_properties.visible_lcd_enabled = false;
          _bucket_properties.visible_promobox_enabled = false;
          _bucket_properties.gui_expiration_can_enabled = true;
          break;

        case BucketId.Comp3_Credit_RE:
          _bucket_properties.can_disable = true;
          _bucket_properties.gui_can_add = true;
          _bucket_properties.gui_can_set = true;
          _bucket_properties.gui_can_sub = true;
          _bucket_properties.cashier_can_add = true;
          _bucket_properties.cashier_can_set = false;
          _bucket_properties.cashier_can_sub = true;
          _bucket_properties.visible_lcd_enabled = false;
          _bucket_properties.visible_promobox_enabled = false;
          _bucket_properties.gui_expiration_can_enabled = true;
          break;

        case BucketId.RankingLevelPoints_Discretional:
          _bucket_properties.can_disable = true;
          _bucket_properties.gui_can_add = false;
          _bucket_properties.gui_can_set = false;
          _bucket_properties.gui_can_sub = false;
          _bucket_properties.cashier_can_add = false;
          _bucket_properties.cashier_can_set = false;
          _bucket_properties.cashier_can_sub = false;
          _bucket_properties.visible_lcd_enabled = false;
          _bucket_properties.visible_promobox_enabled = false;
          _bucket_properties.gui_expiration_can_enabled = true;
          break;

        case BucketId.RankingLevelPoints_Generated:
          _bucket_properties.can_disable = true;
          _bucket_properties.gui_can_add = false;
          _bucket_properties.gui_can_set = false;
          _bucket_properties.gui_can_sub = false;
          _bucket_properties.cashier_can_add = false;
          _bucket_properties.cashier_can_set = false;
          _bucket_properties.cashier_can_sub = false;
          _bucket_properties.visible_lcd_enabled = false;
          _bucket_properties.visible_promobox_enabled = false;
          _bucket_properties.gui_expiration_can_enabled = true;
          break;

      }

      return _bucket_properties;
    } // GetProperties

    /// <summary>
    /// Delete the empty customer buckets
    /// </summary>
    /// <returns></returns>
    public static Boolean BucketsEmpty()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("   DELETE FROM CUSTOMER_BUCKET     ");
      _sb.AppendLine("    WHERE ISNULL(CBU_VALUE, 0) = 0 ");

      // 22-SEP-2016 FJC Fixed Bug 18019:PromoBOX: no se muestran los puntos negativos de la cuenta de un cliente
      // "WHERE ISNULL(CBU_VALUE, 0) <= 0 ";

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.ExecuteNonQuery();
          }

          _db_trx.Commit();
        }

        return true;
      }
      catch (Exception)
      {
        return false;
      }
    } // BucketsEmpty

    /// <summary>
    /// Delete the empty customer buckets when customer id = 0
    /// </summary>
    /// <returns></returns>
    public static Boolean BucketsCustomerIdEmpty()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("   DELETE  FROM CUSTOMER_BUCKET ");
      _sb.AppendLine("    WHERE  CBU_CUSTOMER_ID = 0  ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.ExecuteNonQuery();
          }

          _db_trx.Commit();
        }

        return true;
      }
      catch (Exception)
      {
        return false;
      }
    } // BucketsCustomerIdEmpty

    /// <summary>
    /// Returns the customer buckets to expire by inactivity days
    /// </summary>
    /// <param name="DtCustomerBucketsToExpire">DataTable with the records from table CUSTOMER_BUCKET to expire</param>
    /// <returns></returns>
    private static Boolean ReadBucketsToExpire(out DataTable DtCustomerBucketsToExpire)
    {
      StringBuilder _sb;

      DtCustomerBucketsToExpire = new DataTable("CUSTOMER_BUCKET");

      try
      {
        // Buscamos los Buckets:
        //  - Con dias inactividad superior a los dias indicados en el bucket (bu_expiration_days).
        _sb = new StringBuilder();
        _sb.AppendFormat(" SELECT   TOP {0}  c.CBU_BUCKET_ID                  ", Buckets.MAX_RECORDS_TO_EXPIRE); // Solo los n-primeros registros
        _sb.AppendLine("      ,   c.CBU_CUSTOMER_ID                                                   ");
        _sb.AppendLine("      ,   c.CBU_VALUE                                                         ");
        _sb.AppendLine("   FROM   CUSTOMER_BUCKET c                                                   ");
        _sb.AppendLine("   JOIN   BUCKETS b on b.bu_bucket_id = c.cbu_bucket_id                       ");
        _sb.AppendLine("  WHERE   (ISNULL(b.bu_expiration_days, 0) > 0)                               ");
        _sb.AppendLine(" 	  AND   (DATEDIFF(DAY, c.cbu_updated, GETDATE()) > b.bu_expiration_days)    ");
        _sb.AppendLine(" 	  AND   (ISNULL(c.cbu_value, 0) > 0)                                        ");
        //Devolvemos ordenado por CBU_CUSTOMER_ID y CBU_BUCKET_ID (los campos de la PK)
        _sb.AppendLine(" ORDER BY c.CBU_CUSTOMER_ID, c.CBU_BUCKET_ID                                  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(DtCustomerBucketsToExpire);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadBucketsToExpire

    /// <summary>
    /// Create movements for the buckets to expire
    ///    NOTES : Creates the Operation, Account movements, Cashier movements, ... 
    /// </summary>
    /// <param name="DtCustomerBucketsToExpire">DataTable with the buckets to expire</param>
    /// <returns></returns>
    private static Boolean ProcessBucketsToExpire(DataTable DtCustomerBucketsToExpire)
    {
      Buckets.BucketId _bucket_id;
      Int64 _customer_id;
      Int64 _old_customer_id = -1;
      long _operation_id = -1;

      Decimal _value_to_expire;
      Decimal _old_bucket_value;
      Decimal _new_bucket_value;

      OperationCode _operation_code;
      BucketsForm _BucketsForm;

      CashierSessionInfo _cashier_session;
      MovementType _movement_type;
      CASHIER_MOVEMENT _cashier_movement;
      String _expiration_reason = Resource.String("STR_BUCKET_HAS_EXPIRED");
      Boolean _exists_account;

      _BucketsForm = new BucketsForm();

      try
      {
        foreach (DataRow _row in DtCustomerBucketsToExpire.Rows)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!Accounts.ExistsAccount((Int64)_row["CBU_CUSTOMER_ID"], out _exists_account, _db_trx.SqlTransaction))
            {
              continue;
            }

            _value_to_expire = (Decimal)_row["CBU_VALUE"];

            if (_value_to_expire != 0)
            {
              _bucket_id = (Buckets.BucketId)(Int64)_row["CBU_BUCKET_ID"];
              _customer_id = (Int64)_row["CBU_CUSTOMER_ID"];
              _operation_code = OperationCode.MULTIPLE_BUCKETS_EXPIRED;

              _movement_type = MovementType.MULTIPLE_BUCKETS_Expired + (int)_bucket_id;
              _cashier_movement = CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED + (int)_bucket_id;

              _old_bucket_value = 0;
              if (!BucketsUpdate.UpdateCustomerBucket(_customer_id, _bucket_id, null, _value_to_expire, BucketOperation.EXPIRE, true, true
                                                      , out _old_bucket_value, out _new_bucket_value, _db_trx.SqlTransaction))
              {
                return false;
              }

              if (!GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
              {
                _cashier_session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
                if (_cashier_session == null)
              {
                return false;
              }

              if (_old_bucket_value != _value_to_expire)
              {
                //If the _old_bucket_value is different from _value_to_expire then another process 
                // has changed its value since we searched for the buckets to expire --> we do not do anything --> ignore record
                // It will make a Rollback
                continue;
              }

              if (_exists_account)
              {
                // Si cambiamos de customer (o account) creamos una Operation nueva
                if (_customer_id != _old_customer_id)
                {
                  if (!Operations.DB_InsertOperation(_operation_code,
                                                     _customer_id,                           // AccountId
                                                     _cashier_session.CashierSessionId,      // CashierSessionId
                                                     0,                                      // MbAccountId
                                                     0,                                      // Amount
                                                     0,                                      // PromotionId
                                                     0,                                      // NonRedeemable
                                                     0,                                      // NonRedeemableWonLock
                                                     0,                                      // OperationData 
                                                     0,
                                                     string.Empty,                           // Reason
                                                     out _operation_id,                      // OperationId
                                                       _db_trx.SqlTransaction))
                  {
                    return false;
                  }
                }

                if (_operation_id == -1)
                {
                  return false;
                }

                if (!_BucketsForm.CreateAccountMovements(_movement_type, _cashier_session, _operation_id, _value_to_expire, 0, _value_to_expire, 0, _customer_id, _expiration_reason, _db_trx.SqlTransaction))
                {
                  return false;
                }

                if (!_BucketsForm.CreateCashierMovements(_cashier_movement, _cashier_session, _operation_id, _value_to_expire, 0, String.Empty, _expiration_reason, _db_trx.SqlTransaction))
                {
                  return false;
                }
              }
              }
              else
              {
                _operation_id = 0;
                if (!_BucketsForm.CreateAccountMovements(_movement_type, _operation_id, _value_to_expire, 0, _value_to_expire, 0, _customer_id, _expiration_reason, _db_trx.SqlTransaction))
                {
                  return false;
                }
              }

              _old_customer_id = _customer_id;

              _db_trx.Commit();
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ProcessBucketsToExpire

    /// <summary>
    /// Delete the expired customer buckets by days of inactivity
    /// </summary>
    /// <returns></returns>
    public static Boolean BucketsExpirationByInactivityDays()
    {
      DataTable _dt_buckets_to_expire;

      try
      {
        if (!ReadBucketsToExpire(out _dt_buckets_to_expire))
        {
          Log.Error(" *** ReadBucketsToExpire failed.");

          return false;
        }

        if (_dt_buckets_to_expire == null)
        {
          return false;
        }

        if (_dt_buckets_to_expire.Rows.Count == 0)
        {
          return true;
        }

        if (!ProcessBucketsToExpire(_dt_buckets_to_expire))
        {
          Log.Error(" *** ProcessBucketsExpired failed.");

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // BucketsExpirationByInactivityDays

    public static Boolean BucketsHaveSameConfiguration(BucketId BucketId1, BucketId BucketId2, SqlTransaction SqlTrx)
    {
      Object _obj;
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("                                                            ");
      _sb.AppendLine(" DECLARE @RESULT BIT                                        ");
      _sb.AppendLine(" SET @RESULT = 0                                            ");
      _sb.AppendLine("                                                            ");
      _sb.AppendLine("                                                            ");
      _sb.AppendLine(" IF EXISTS (SELECT  1                                       ");
      _sb.AppendLine(" 		          FROM  BUCKETS BU1                             ");
      _sb.AppendLine(" 		              , BUCKETS BU2                             ");
      _sb.AppendLine(" 	           WHERE  BU1.BU_BUCKET_ID = @pBucket_id_1        ");
      _sb.AppendLine(" 	             AND  BU2.BU_BUCKET_ID = @pBucket_id_2        ");
      _sb.AppendLine(" 	             AND  BU1.BU_LEVEL_FLAGS = BU2.BU_LEVEL_FLAGS ");
      _sb.AppendLine(" 	             AND  BU1.BU_K_FACTOR = BU2.BU_K_FACTOR)      ");
      _sb.AppendLine(" BEGIN                                                      ");
      _sb.AppendLine(" 	      SELECT   @RESULT = 1                                ");
      _sb.AppendLine(" 	        FROM   BUCKET_LEVELS BL1                          ");
      _sb.AppendLine(" 	  INNER JOIN   BUCKET_LEVELS BL2                          ");
      _sb.AppendLine(" 	          ON   BL1.BUL_BUCKET_ID = @pBucket_id_1          ");
      _sb.AppendLine(" 	               AND BL2.BUL_BUCKET_ID = @pBucket_id_2      ");
      _sb.AppendLine(" 	               AND BL1.BUL_LEVEL_ID = BL2.BUL_LEVEL_ID    ");
      _sb.AppendLine(" 	       WHERE   BL1.BUL_A_FACTOR = BL2.BUL_A_FACTOR	      ");
      _sb.AppendLine(" 	               AND BL1.BUL_B_FACTOR = BL2.BUL_B_FACTOR	  ");
      _sb.AppendLine(" END                                                        ");
      _sb.AppendLine(" SELECT @RESULT                                             ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pBucket_id_1", SqlDbType.BigInt).Value = (int)BucketId1;
          _cmd.Parameters.Add("@pBucket_id_2", SqlDbType.BigInt).Value = (int)BucketId2;

          _obj = _cmd.ExecuteScalar();

          if ((_obj != null) && (_obj != DBNull.Value))
          {
            return (Boolean)_obj;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // BucketsHaveSameConfiguration

    public static Boolean InsertPromotionToAccount(MultiPromos.AccountInfo AccountInfo, Decimal BucketCredit, Decimal BucketCredit_InitialValue, BucketId BucketId,
                                                   Int32 TerminalId, String TerminalName, TerminalTypes TerminalType, SqlTransaction Trx)
    {
      CashierSessionInfo _cashier_session_info;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      GU_USER_TYPE _gu_user_type;
      Int64 _operation_id;
      RechargeInputParameters _input_params;
      RechargeOutputParameters _output_params;
      AccountPromotion _aux_promotion;

      if (BucketCredit <= 0)
      {
        return true;
      }

      switch (TerminalType)
      {
        case TerminalTypes.PROMOBOX:
          {
            _gu_user_type = Misc.GetUserTypePromobox();
          }
          break;
        case TerminalTypes.SAS_HOST:
          {
            _gu_user_type = Misc.IsNoteAcceptorEnabled() ? GU_USER_TYPE.SYS_ACCEPTOR : GU_USER_TYPE.SYS_SYSTEM;
          }
          break;
        default:
          {
            Log.Message(String.Format("Buckets.InsertPromotionToAccount: Invalid TerminalType {0}, TerminalId {1}, TerminalName {2}", TerminalType, TerminalId, TerminalName));

            return false;
          }
      }

      _cashier_session_info = Cashier.GetSystemCashierSessionInfo(_gu_user_type, Trx, TerminalName);
      if (_cashier_session_info == null)
      {
        Log.Error("Buckets.InsertPromotionToAccount: GetSystemCashierSessionInfo failed.");

        return false;
      }

      Promotion.PROMOTION_TYPE _Promotion;

      switch (BucketId)
      {
        case Buckets.BucketId.Credit_NR:
          _Promotion = Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_NR_CREDIT;
          break;

        case Buckets.BucketId.Credit_RE:
          _Promotion = Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_RE_CREDIT;
          break;

        default:
          Log.Error("not Promotions defined: Bucket Id = " + BucketId);
          return false;
      }

      if (!Promotion.ReadPromotionData(Trx, _Promotion, out _aux_promotion))
      {
        return false;
      }

      //Insert operation
      if (!Operations.DB_InsertOperation(OperationCode.NA_PROMOTION, AccountInfo.AccountId, _cashier_session_info.CashierSessionId, 0, 0,
                                         _aux_promotion.id, BucketCredit, 0, 0, 0, 0, string.Empty, out _operation_id, Trx))
      {
        return false;
      }

      _account_movements = new AccountMovementsTable(_cashier_session_info, _gu_user_type == GU_USER_TYPE.SYS_ACCEPTOR ? TerminalId : 0, TerminalName);
      _cashier_movements = new CashierMovementsTable(_cashier_session_info, TerminalName);

      _input_params = new RechargeInputParameters();
      _input_params.AccountId = AccountInfo.AccountId;
      _input_params.AccountLevel = AccountInfo.HolderLevel;
      _input_params.CardPaid = true;
      _input_params.CardPrice = 0;
      _input_params.GenerateVouchers = false;

      if (!Accounts.DB_AddPromotion(_input_params, _operation_id, _account_movements, _cashier_movements, _aux_promotion.id, BucketCredit,
                                    0, Trx, out _output_params))
      {
        Log.Error(String.Format("Buckets.InsertPromotionToAccount: Can't apply promotion. AccountId {0}, PromoId {1}.", AccountInfo.AccountId, _aux_promotion.id));

        return false;
      }

      if (!BucketsUpdate.UpdateCustomerBucket(AccountInfo.AccountId, BucketId, BucketCredit_InitialValue, BucketCredit, BucketOperation.SUB_GENERATED, true, true, Trx))
      {
        return false;
      }

      return true;
    } // InsertPromotionToAccount

    #endregion functions
  } // Buckets

  public class Bucket_Properties
  {
    public Boolean can_disable;
    public Boolean gui_can_add;
    public Boolean gui_can_sub;
    public Boolean gui_can_set;
    public Boolean cashier_can_add;
    public Boolean cashier_can_sub;
    public Boolean cashier_can_set;
    public Boolean visible_lcd_enabled;
    public Boolean visible_promobox_enabled;
    public Boolean gui_expiration_can_enabled;

  }

  // Class that contains the bucket data for expiration control
  public class Bucket_Data
  {
    public Int64 Id;
    public String Name;
    public Int32 ExpirationDays;
    public String ExpirationDate;
  }

  // List of Bucket_Data
  public class BucketsList : List<Bucket_Data>
  {
    #region Constants
    private const String EXCEPTION_BUCKETSLIST = "Exception in class BucketsList";
    private const String EXCEPTION_MESSAGE = ". Message: ";
    private const int SQL_COLUMN_BU_BUCKET_ID = 0;
    private const int SQL_COLUMN_BU_NAME = 1;
    private const int SQL_COLUMN_BU_EXPIRATION_DAYS = 2;
    private const int SQL_COLUMN_BU_EXPIRATION_DATE = 3;
    #endregion

    #region Public Methods

    /// <summary>
    /// Fills the list with the data of the buckets
    /// </summary>
    /// <returns></returns>
    public Boolean GetList()
    {
      String _sql_str;
      Bucket_Data Bucket;

      this.Clear();
      _sql_str = "SELECT BU_BUCKET_ID, BU_NAME, BU_EXPIRATION_DAYS, BU_EXPIRATION_DATE " +
                 "FROM BUCKETS " +
                 "ORDER BY BU_BUCKET_ID ";
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                Bucket = new Bucket_Data();
                Bucket.Id = (Int64)_reader[SQL_COLUMN_BU_BUCKET_ID];
                Bucket.Name = (String)_reader[SQL_COLUMN_BU_NAME].ToString();
                Bucket.ExpirationDays = _reader.IsDBNull(SQL_COLUMN_BU_EXPIRATION_DAYS) ? 0 : (Int32)_reader[SQL_COLUMN_BU_EXPIRATION_DAYS];
                Bucket.ExpirationDate = _reader.IsDBNull(SQL_COLUMN_BU_EXPIRATION_DATE) ? String.Empty : (String)_reader[SQL_COLUMN_BU_EXPIRATION_DATE].ToString();
                this.Add(Bucket);
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning(String.Format(EXCEPTION_BUCKETSLIST + ".GetList" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
        return false;
      }
    } // GetList

    /// <summary>
    /// Searches bucket by ID
    /// </summary>
    /// <param name="BucketId"></param>
    /// <returns>The bucket name</returns>
    public String GetBucketName(Int32 BucketId)
    {
      List<Bucket_Data> _bucket_data_list = this.FindAll(_element => _element.Id == BucketId);
      return _bucket_data_list.Count > 0 ? _bucket_data_list[0].Name : String.Empty;
    } // GetBucketName

    #endregion
  } // class BucketsList


  public class WonBucket
  {
    public Decimal WonValue;
    public Int64 PlaySessionId;
    public Int64 AccountId;
    public Int64 TerminalId;
    public String TerminalName;
    public Boolean ValueHaveToBeAwarded;
  } // class WonBucket


  public class WonBucketDict
  {
    private Dictionary<Buckets.BucketId, WonBucket> m_dict = new Dictionary<Buckets.BucketId, WonBucket>();

    public Dictionary<Buckets.BucketId, WonBucket> Dict
    {
      get { return m_dict; }
      set { m_dict = value; }
    }

    public void Add(Buckets.BucketId BucketId, Decimal WonValue, Int64 PlaySessionId, Int64 AccountId, Int64 TerminalId, Boolean ValueHaveToBeAwarded)
    {
      WonBucket _bucket = new WonBucket();
      _bucket.WonValue = WonValue;
      _bucket.AccountId = AccountId;
      _bucket.PlaySessionId = PlaySessionId;
      _bucket.TerminalId = TerminalId;
      _bucket.ValueHaveToBeAwarded = ValueHaveToBeAwarded;

      if (m_dict.ContainsKey(BucketId))
      {
        m_dict.Remove(BucketId);
      }

      m_dict.Add(BucketId, _bucket);
    } // Add
  } // class WonBucketDict

  public static class BucketsUpdate
  {
    #region Members

    private static DataTable m_Buckets;  /* los 7 buckes de la cuenta*/

    #endregion Members

    #region Enums

    #endregion Enums

    #region Constants
    private const String EXCEPTION_WCP_BUCKETS_UPDATE_TASK = "Exception into class WCP_BucketsUpdate.";
    private const String EXCEPTION_MESSAGE = ". Message: ";
    private const Int32 MAX_PENDING_SESSIONS = 10;
    private const Int32 WAIT_MANUAL_RESET_EVENT_MS = 1000;

    #endregion Constants

    private static ManualResetEvent m_ev_data_available = new ManualResetEvent(false);

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_BucketsUpdate class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void Init()
    {
      try
      {
        Thread _thread;
        _thread = new Thread(BucketsUpdateThread);
        _thread.Name = "BucketsUpdateThread";
        _thread.Start();
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "Init" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }
    } // Init

    #endregion Public Methods

    #region Private Methods

    /// <summary>
    /// Main thread. Process records from pending sessions calculating de buckets
    /// </summary>
    static private void BucketsUpdateThread()
    {
      Int32 _wait_hint = 0;
      Boolean _play_sessions_pending = true;
      Int32 _num_play_sessions_pending;

      CreateTableStructure();

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);

          if (!Services.IsPrincipal("WCP"))
          {
            _wait_hint = 1000;

            continue;
          }

          if (GeneralParam.GetBoolean("PlayerTracking.ExternalLoyaltyProgram", "Mode", false))
          {
            _wait_hint = 5 * 60 * 1000; // 5 minutes

            continue;
          }

          _wait_hint = 200;

          m_ev_data_available.WaitOne(WAIT_MANUAL_RESET_EVENT_MS);
          m_ev_data_available.Reset();

          // Process all pending sessions
          _play_sessions_pending = true;
          while (_play_sessions_pending)
          {
            DataTable _dtSessions;

            using (DB_TRX _db_trx = new DB_TRX())
            {
              // Get Pending sessions from [pending_play_sessions_to_player_tracking]
              if (!GetPendingSessions(_db_trx.SqlTransaction, out _dtSessions))
              {
                Log.Error(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "GetPendingSessions" + EXCEPTION_MESSAGE + "{0} .", WGDB.Now));
                continue;
              }
              _num_play_sessions_pending = _dtSessions.Rows.Count;

              // Not exists pending sessions
              if (_num_play_sessions_pending == 0)
              {
                _play_sessions_pending = false;

                continue;
              }

              m_Buckets.Clear();

              // Process buckets foreach play session
              foreach (DataRow _dr in _dtSessions.Rows)
              {
                ProcessPlaySession(_dr, _db_trx.SqlTransaction);
                // Junkets
                Junkets.JunketsBusinessLogic.ProcessSessionCommissions(_dr, m_Buckets, _db_trx.SqlTransaction);
              }

              // BatchUpdate all Buckets
              if (!BatchUpdateBuckets(m_Buckets, _db_trx.SqlTransaction))
              {
                Log.Error(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "BucketsUpdateThread" + EXCEPTION_MESSAGE + "{0} .", WGDB.Now));

                continue;
              }

              // Create Movements: Accounts & Cashier
              if (!CreateMovements(OperationCode.MULTIPLE_BUCKETS_ENDSESSION, MovementType.MULTIPLE_BUCKETS_EndSession, CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION, m_Buckets, _db_trx.SqlTransaction))
              {
                Log.Error(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "CreateAccountMovements" + EXCEPTION_MESSAGE + "{0} .", WGDB.Now));

                continue;
              }

              // Delete pending sessions processed
              if (!DeletePendingSessionProcessed(_dtSessions, _db_trx.SqlTransaction))
              {
                Log.Error(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "DeletePendingSessionProcessed" + EXCEPTION_MESSAGE + "{0} .", WGDB.Now));

                continue;
              }

              // Not exists more pending sessions
              if (_num_play_sessions_pending < MAX_PENDING_SESSIONS)
              {
                _play_sessions_pending = false;
              }

              _db_trx.Commit();
            }

            Thread.Sleep(_wait_hint);
            _wait_hint = 0;

          } // while
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "BucketsUpdateThread" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
        }
      }
    } // BucketsUpdateThread


    private static void CreateTableStructure()
    {
      m_Buckets = new DataTable();
      m_Buckets.Columns.Add("ACCOUNT_ID", Type.GetType("System.Int64"));
      m_Buckets.Columns.Add("BUCKET_ID", Type.GetType("System.Int64"));
      m_Buckets.Columns.Add("WONVALUE", Type.GetType("System.Decimal"));
      m_Buckets.Columns.Add("PPS_SESSION_ID", Type.GetType("System.Int64"));
      m_Buckets.Columns.Add("PPS_TERMINAL_ID", Type.GetType("System.Int64"));
      m_Buckets.Columns.Add("INITIAL_BALANCE", Type.GetType("System.Decimal")).DefaultValue = 0;
      m_Buckets.Columns.Add("TERMINAL_NAME", Type.GetType("System.String")).DefaultValue = "";
    }

    /// <summary>
    ///   retrieves the list of session pending to be processed from PENDING_PLAY_SESSIONS_TO_PLAYER_TRACKING
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="Dt">Datatable with the pending sessions</param>
    /// <returns></returns>
    private static Boolean GetPendingSessions(SqlTransaction SqlTrx, out DataTable Dt)
    {
      StringBuilder _sb;

      Dt = new DataTable();
      _sb = new StringBuilder();

      _sb.AppendLine("  UPDATE PENDING_PLAY_SESSIONS_TO_PLAYER_TRACKING            ");
      _sb.AppendLine("         SET PPS_COIN_IN = PPS_COIN_IN                       ");
      _sb.AppendLine("  OUTPUT INSERTED.PPS_ACCOUNT_ID ACCOUNT_ID                  ");
      _sb.AppendLine("  	   , INSERTED.PPS_COIN_IN                                ");
      _sb.AppendLine("  	   , INSERTED.PPS_SESSION_ID                             ");
      _sb.AppendLine("  	   , INSERTED.PPS_TERMINAL_ID                            ");
      _sb.AppendLine("  	   , INSERTED.PPS_DURATION_TOTAL_MINUTES                 ");
      _sb.AppendLine("  	   , INSERTED.PPS_PARAMS_NUMPLAYED                       ");
      _sb.AppendLine("  	   , INSERTED.PPS_PARAMS_BALANCE_MISMATCH                ");
      _sb.AppendLine("  	   , INSERTED.PPS_TERMINAL_TYPE                          ");
      _sb.AppendLine("   WHERE PPS_SESSION_ID IN                                   ");
      _sb.AppendLine("  			 (	SELECT  TOP (" + MAX_PENDING_SESSIONS + ")       ");
      _sb.AppendLine("  						      PPS_SESSION_ID                           ");
      _sb.AppendLine("  				    FROM  PENDING_PLAY_SESSIONS_TO_PLAYER_TRACKING ");
      _sb.AppendLine("  			 )                                                   ");

      try
      {
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(Dt);
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "GetPendingSessions" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }
      return false;
    }

    /// <summary>
    ///  Processes on session 
    /// </summary>
    /// <param name="DrSession"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean ProcessPlaySession(DataRow DrSession, SqlTransaction SqlTrx)
    {
      WonBucketDict _wonBucket = new WonBucketDict();

      Terminal.TerminalInfo _terminal_info;

      if (!Terminal.Trx_GetTerminalInfo((Int32)DrSession["PPS_TERMINAL_ID"], out _terminal_info, SqlTrx))
      {
        return false;
      }

      try
      {
        if (!ComputeWonBuckets((Int64)DrSession["ACCOUNT_ID"]
                             , _terminal_info
                             , (Decimal)DrSession["PPS_COIN_IN"]
                             , (Int64)DrSession["PPS_SESSION_ID"]
                             , ref _wonBucket
                             , SqlTrx))
        {
          return false;
        }

        if (SessionIsOk(DrSession, _wonBucket, SqlTrx))
        {
          foreach (KeyValuePair<Buckets.BucketId, WonBucket> _key_value in _wonBucket.Dict)
          {
            DataRow _new_row = m_Buckets.NewRow();
            DataRow _new_row2 = m_Buckets.NewRow();

            _new_row["BUCKET_ID"] = _key_value.Key;
            _new_row["WONVALUE"] = _key_value.Value.WonValue;

            _new_row["ACCOUNT_ID"] = DrSession["ACCOUNT_ID"];
            _new_row["PPS_SESSION_ID"] = DrSession["PPS_SESSION_ID"];
            _new_row["PPS_TERMINAL_ID"] = DrSession["PPS_TERMINAL_ID"];
            _new_row["TERMINAL_NAME"] = _terminal_info.Name;
            m_Buckets.Rows.Add(_new_row);

            // This method is intended to be receiving ADD_GENERATED operation ONLY
            // and based on that I'm adding the same information for the buket RankingLevelPoints_Generated = 8 PuntosCanje Generados (automaticos)  

            if (_key_value.Key == Buckets.BucketId.RankingLevelPoints)
            {
              _new_row2["BUCKET_ID"] = Buckets.BucketId.RankingLevelPoints_Generated;
              _new_row2["WONVALUE"] = _key_value.Value.WonValue;
              _new_row2["ACCOUNT_ID"] = DrSession["ACCOUNT_ID"];
              _new_row2["PPS_SESSION_ID"] = DrSession["PPS_SESSION_ID"];
              _new_row2["PPS_TERMINAL_ID"] = DrSession["PPS_TERMINAL_ID"];
              _new_row2["TERMINAL_NAME"] = _terminal_info.Name;
              m_Buckets.Rows.Add(_new_row2);
            }
          }

          return true;
        }
        else
        {
          // Save the play session. Table [invalid_play_sessions_to_player_tracking]
          if (!InsertInvalidPlaySession((Int64)DrSession["PPS_SESSION_ID"], (Int64)DrSession["ACCOUNT_ID"], (Decimal)DrSession["PPS_COIN_IN"], SqlTrx))
          {
            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Message("Error: WCP_BucketsUpdate.BucketsUpdate");
        Log.Exception(_ex);
      }

      return false;
    } // ProcessPlaySession

    private static Boolean InsertInvalidPlaySession(Int64 PlaySession, Int64 AccountId, Decimal CoinIn, SqlTransaction SqlTrx)
    {
      StringBuilder _sb = new StringBuilder();

      _sb.AppendLine(" INSERT INTO   INVALID_PLAY_SESSIONS_TO_PLAYER_TRACKING ");
      _sb.AppendLine("             ( IPS_PLAY_SESSION_ID                      ");
      _sb.AppendLine("             , IPS_ACCOUNT_ID                           ");
      _sb.AppendLine("             , IPS_COIN_IN )                            ");
      _sb.AppendLine("      VALUES ( @pPlaySessionId ,                        ");
      _sb.AppendLine("               @pAccountId ,                            ");
      _sb.AppendLine("               @pCoinIn )                               ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySession;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pCoinIn", SqlDbType.Decimal).Value = CoinIn;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Message("Error: BucketsUpdate.InsertInvalidPlaySession");
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean ComputeWonBuckets(Int64 AccountId
                                          , Terminal.TerminalInfo TerminalInfo
                                          , Decimal CoinIn
                                          , Buckets.BucketId BucketId
                                          , Int64 PlaySessionId
                                          , out Decimal WonBucket
                                          , out Boolean BucketHaveToBeAwarded
                                          , SqlTransaction SqlTrx)
    {
      Decimal _theoreticalPayout;
      WonBucketDict _won_bucket_dict = new WonBucketDict();

      WonBucket = 0;
      BucketHaveToBeAwarded = false;

      GetTheoreticalPayout(TerminalInfo, SqlTrx, out _theoreticalPayout);

      if (!ComputeWonBuckets(AccountId, TerminalInfo, CoinIn, _theoreticalPayout, PlaySessionId, ref _won_bucket_dict, SqlTrx))
      {
        Log.Error(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "ComputeWonBuckets. {0}.", WGDB.Now));

        return false;
      }

      if (_won_bucket_dict.Dict.ContainsKey(BucketId))
      {
        WonBucket = _won_bucket_dict.Dict[BucketId].WonValue;
        BucketHaveToBeAwarded = _won_bucket_dict.Dict[BucketId].ValueHaveToBeAwarded;
      }

      return true;

    } // ComputeWonBuckets

    public static Boolean ComputeWonBuckets(Int64 AccountId
                                    , Terminal.TerminalInfo TerminalInfo
                                    , Decimal CoinIn
                                    , Int64 PlaySessionId
                                    , ref WonBucketDict WonBucketDict // 3 columnas IdBucket | WonPoints | PointHaveToBeAwarded
                                    , SqlTransaction SqlTrx)
    {
      Decimal _theoretical_payout;
      GetTheoreticalPayout(TerminalInfo, SqlTrx, out _theoretical_payout);

      return ComputeWonBuckets(AccountId, TerminalInfo, CoinIn, _theoretical_payout, PlaySessionId, ref WonBucketDict, SqlTrx);
    }

    public static Boolean ComputeWonBuckets(Int64 AccountId
                                    , Terminal.TerminalInfo TerminalInfo
                                    , Decimal CoinIn
                                    , Decimal TheoreticalPayout
                                    , Int64 PlaySessionId
                                    , ref WonBucketDict WonBucketDict
                                    , SqlTransaction SqlTrx)
    {
      Int32 _elp_mode;
      DataTable _dt_buckets_definition_list;

      // If ExternalLoyaltyProgram is enabled, not compute any bucket and return TRUE
      _elp_mode = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0);
      if (_elp_mode != 0)
      {
        WonBucketDict = new WonBucketDict();

        return true;
      }

      if (!GetBucketsDataTableByAccount(AccountId
                                      , out _dt_buckets_definition_list
                                      , SqlTrx))
      {
        return false;
      }

      WonBucketDict = CalculateAccountBuckets(AccountId, PlaySessionId, CoinIn, TheoreticalPayout, _dt_buckets_definition_list, TerminalInfo.PvPointsMultiplier, TerminalInfo.CurrencyCode);

      return true;
    } // ComputeWonBuckets

    private static Boolean GetTheoreticalPayout(Terminal.TerminalInfo TerminalInfo, SqlTransaction SqlTrx, out Decimal TheoreticalPayout)
    {
      Object _obj;
      StringBuilder _sb1 = new StringBuilder();
      TheoreticalPayout = 0;

      _sb1.AppendLine(" DECLARE @_theoretical_payout  MONEY                                                                    ");
      _sb1.AppendLine(" SET @_theoretical_payout = ISNULL ( ( SELECT  CAST (GP_KEY_VALUE AS MONEY)                             ");
      _sb1.AppendLine("                                         FROM  GENERAL_PARAMS                                           ");
      _sb1.AppendLine("                                        WHERE  GP_GROUP_KEY   = 'PlayerTracking'                        ");
      _sb1.AppendLine("                                          AND  GP_SUBJECT_KEY = 'TerminalDefaultPayout' ), 0) / 100     ");
      _sb1.AppendLine(" SELECT                                                                                                 ");
      _sb1.AppendLine(" 		ISNULL(TE_THEORETICAL_PAYOUT, @_theoretical_payout)                                                ");
      _sb1.AppendLine(" FROM                                                                                                   ");
      _sb1.AppendLine(" 	  TERMINALS                                                                                          ");
      _sb1.AppendLine(" WHERE TE_TERMINAL_ID = @terminalID                                                                     ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb1.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@terminalID", SqlDbType.BigInt).Value = TerminalInfo.TerminalId;

          _obj = _cmd.ExecuteScalar();

          if ((_obj != null) && (_obj != DBNull.Value))
          {
            TheoreticalPayout = (Decimal)_obj;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "GetTheoreticalPayout" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    } //GetTheoreticalPayout

    private static Boolean GetBucketsDataTableByAccount(Int64 AccountId
                                                      , out DataTable DtBuckets
                                                      , SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DtBuckets = new DataTable();

      _sb = new StringBuilder();
      _sb.AppendLine("    SELECT   BU_BUCKET_ID                                                                          ");
      _sb.AppendLine("  	       , BUL_A_FACTOR                                                                          ");
      _sb.AppendLine("  	       , BUL_B_FACTOR                                                                          ");
      _sb.AppendLine("  	       , BU_K_FACTOR                                                                           ");
      _sb.AppendLine("  	       , VALUE = 0                                                                             ");
      _sb.AppendLine("      FROM  BUCKETS                                                                                ");
      _sb.AppendLine("INNER JOIN  BUCKET_LEVELS BYLEVEL ON [BUCKETS].[BU_BUCKET_ID] = BYLEVEL.[BUL_BUCKET_ID]            ");
      _sb.AppendLine("   		 AND  BUCKETS.[BU_LEVEL_FLAGS] = 1 /* VA POR NIVELES*/                                       ");
      _sb.AppendLine("  	 WHERE  BUL_LEVEL_ID IN ( SELECT   ISNULL(AC_HOLDER_LEVEL,0)                                   ");
      _sb.AppendLine("  	                            FROM   ACCOUNTS                                                    ");
      _sb.AppendLine("  	                           WHERE  AC_ACCOUNT_ID = @AccountId)                                  ");
      _sb.AppendLine("  	   AND  BU_ENABLED = 1                                                                         ");

      _sb.AppendLine("       AND  NOT ( BU_BUCKET_ID = " + (Int32)Buckets.BucketId.RankingLevelPoints_Generated);
      _sb.AppendLine("               OR BU_BUCKET_ID = " + (Int32)Buckets.BucketId.RankingLevelPoints_Discretional);
      _sb.AppendLine("                ) ");




      _sb.AppendLine("  UNION                                                                                            ");

      _sb.AppendLine("    SELECT   BU_BUCKET_ID                                                                          ");
      _sb.AppendLine("  	       , BUL_A_FACTOR                                                                          ");
      _sb.AppendLine("  	       , BUL_B_FACTOR                                                                          ");
      _sb.AppendLine("  	       , BU_K_FACTOR                                                                           ");
      _sb.AppendLine("  	       , VALUE = 0                                                                             ");
      _sb.AppendLine("      FROM  BUCKETS                                                                                ");
      _sb.AppendLine("INNER JOIN  BUCKET_LEVELS NOTBYLEVEL ON [BUCKETS].[BU_BUCKET_ID] = NOTBYLEVEL.[BUL_BUCKET_ID]      ");
      _sb.AppendLine("  	 	 AND  BUCKETS.[BU_LEVEL_FLAGS] = 0 /* NO VA POR NIVELES*/                                    ");
      _sb.AppendLine("     WHERE  BU_ENABLED = 1                                                                         ");
      _sb.AppendLine("       AND  NOT ( BU_BUCKET_ID = " + (Int32)Buckets.BucketId.RankingLevelPoints_Generated);
      _sb.AppendLine("               OR BU_BUCKET_ID = " + (Int32)Buckets.BucketId.RankingLevelPoints_Discretional);
      _sb.AppendLine("                )");

      try
      {
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = AccountId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(DtBuckets);
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "GetBucketsDataTableByAccount" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    }

    private static WonBucketDict CalculateAccountBuckets(Int64 AccountId, Int64 PlaySessionId, Decimal CoinIn, Decimal TheoricPayout, DataTable DtSessionBuckets, Decimal Terminal_PvPointsMultiplier, String Terminal_CurrencyCode)
    {
      Decimal _amount = 0;
      Decimal _param_a = 0;
      Decimal _param_b = 0;
      Int32 _factor_k = 0;
      Decimal _coin_in_national_currency;
      WonBucketDict _won_bucket_dict;
      int _account_level;
      //Boolean _value_have_to_be_awarded;

      _won_bucket_dict = new WonBucketDict();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _coin_in_national_currency = CurrencyExchange.GetExchange(CoinIn, Terminal_CurrencyCode, CurrencyExchange.GetNationalCurrency(), _db_trx.SqlTransaction);
          _account_level = GetAccountLevel(AccountId, _db_trx.SqlTransaction);
        }

        if (DtSessionBuckets != null
            && DtSessionBuckets.Rows.Count > 0)
        {
          foreach (DataRow _dr in DtSessionBuckets.Rows)
          {
            _param_a = (Decimal)_dr["BUL_A_FACTOR"];
            _param_b = (Decimal)_dr["BUL_B_FACTOR"];

            _factor_k = 0;
            if ((Boolean)_dr["BU_K_FACTOR"])
            {
              _factor_k = 1;
            }

            Buckets.BucketId _bucket_id = (Buckets.BucketId)(Int64)_dr["BU_BUCKET_ID"];

            _amount = CalculateAmount(_coin_in_national_currency, _param_a, _param_b, _factor_k, TheoricPayout, Terminal_PvPointsMultiplier, PlaySessionId, _bucket_id, _account_level);

            if (_won_bucket_dict.Dict.ContainsKey(_bucket_id))
            {
              _won_bucket_dict.Dict.Remove(_bucket_id);
            }

            //_value_have_to_be_awarded = (_amount > 0);
            //_won_bucket_dict.Add(_bucket_id, _amount, 0, 0, 0, _value_have_to_be_awarded);
            _won_bucket_dict.Add(_bucket_id, _amount, PlaySessionId, AccountId, 0, true);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "CalculateAccountBuckets" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return _won_bucket_dict;
    }

    private static Decimal CalculateAmount(Decimal CoinIn, Decimal Param_A, Decimal Param_B, Int32 Param_K, Decimal TheoreticalPayout, Decimal Terminal_PvPointsMultiplier, Int64 PlaysessionId, Buckets.BucketId BucketId, int AccountLevel)
    {
      Decimal _result;

      _result = 0;
      int _terminalID = 0;

      try
      {
        if (Param_B != 0)
        {
          _terminalID = GetterminalIdByPlaySessionId(PlaysessionId);
          _result = CoinIn * Param_A / Param_B * Terminal_PvPointsMultiplier * (1 - Param_K * TheoreticalPayout) * GetBucketMultiplier(_terminalID, BucketId, AccountLevel.ToString());

          if (_result < 0 && CoinIn > 0)
          {
            //Result only allow negative result with negative Coin 
            _result = 0;
          }
        }

        //este seria un nuevo modelo de calculo mas restrictivo.
        //if (CoinIn >= Param_B)
        //  _result = Param_A * Math.Truncate(CoinIn / Param_B) * Terminal_PvPointsMultiplier * (1 - Param_K * TheoreticalPayout); 
      }
      catch (Exception _ex)
      {
        _result = 0;
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "CalculateAmount" + EXCEPTION_MESSAGE, _ex.Message, WGDB.Now));
      }
      return _result;
    } // CalculateAmount


    private static decimal GetBucketMultiplier(int TerminalId, Buckets.BucketId BucketId, string Level)
    {
      decimal _multiplier;
      StringBuilder _sb;
      DataTable _dt;
      Buckets.BucketId? _bucket_id;

      _multiplier = 1;
      _bucket_id = null;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _dt = new DataTable();

          _sb.AppendLine("   SELECT  BMA_MULTIPLIER                                              ");
          _sb.AppendLine("         , BM_BUCKET_AFECTED_ID                                        ");
          _sb.AppendLine("     FROM  BUCKETS_MULTIPLIER_TO_APPLY                                 ");
          _sb.AppendLine("LEFT JOIN  BUCKETS_MULTIPLIER_SCHEDULE ON BMA_BUCKET_ID = BM_BUCKET_ID ");
          _sb.AppendLine("    WHERE  BMA_TERMINAL_ID = @terminal                                 ");
          _sb.AppendLine("    AND    BMA_LEVEL = @pLevel                                         ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@terminal", SqlDbType.BigInt).Value = TerminalId;
            _cmd.Parameters.Add("@pLevel", SqlDbType.NVarChar).Value = Level;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _db_trx.Fill(_da, _dt);

              if (_dt.Rows.Count == 0)
              {
                return 1; // Not exists a multiplier for this terminal
              }

              if (_dt.Rows[0].IsNull(1))
              {
                return 1; // Scheduling has been deleted
              }

              try
              {
                _bucket_id = (Buckets.BucketId)Int32.Parse(_dt.Rows[0][1].ToString());
              }
              catch
              {
                Log.Error(string.Format("GetBucketMultiplier. BucketId not exists. Bucket = {0}", _dt.Rows[0][1]));

                return 1;
              }

              if (_bucket_id == BucketId)
              {
                _multiplier = Decimal.Parse(_dt.Rows[0][0].ToString());

                return _multiplier;
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return 1;
    }

    private static int GetterminalIdByPlaySessionId(Int64 PlaySessionId)
    {
      int _terminalID = 0;
      StringBuilder _sb;
      object _result;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();

          _sb.AppendLine("SELECT TOP(1) PS_TERMINAL_ID FROM PLAY_SESSIONS ");
          _sb.AppendLine("WHERE PS_PLAY_SESSION_ID = @playsession");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@playsession", SqlDbType.BigInt).Value = PlaySessionId;

            _result=_cmd.ExecuteScalar(); 
            if (_result != null) _terminalID = Convert.ToInt32(_result);
          }
        }

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return _terminalID;
    }


    private static Boolean SessionIsOk(DataRow DrSession, WonBucketDict WonBucketList, SqlTransaction SqlTrx)
    {
      TerminalTypes _terminal_type = (TerminalTypes)DrSession["pps_terminal_type"];
      Int32 _duration_total_minutes = (Int32)DrSession["pps_duration_total_minutes"];
      Int64 _num_played = (Int64)DrSession["pps_params_numplayed"];
      Int64 _account_id = (Int64)DrSession["account_id"];
      Int32 _terminal_id = (Int32)DrSession["pps_terminal_id"];
      Boolean _balance_mismatch = (Boolean)DrSession["pps_params_balance_mismatch"];
      Int64 _play_session_id = (Int64)DrSession["pps_session_id"];
      MultiPromos.EndSessionOutput _output = new MultiPromos.EndSessionOutput();
      _output.SetError(MultiPromos.EndSessionStatus.ErrorRetry, String.Empty);

      if (WonBucketList.Dict.ContainsKey(Buckets.BucketId.RedemptionPoints))
      {
        return Trx_PlaySessionToRevise(_terminal_type,
                                       WonBucketList.Dict[Buckets.BucketId.RedemptionPoints].WonValue,
                                                _duration_total_minutes,
                                                _num_played,
                                                _balance_mismatch,
                                                _account_id,
                                                _terminal_id,
                                                _play_session_id,
                                                _output,
                                       ref WonBucketList.Dict[Buckets.BucketId.RedemptionPoints].ValueHaveToBeAwarded,
                                                SqlTrx);
      }

      return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="TerminalType"></param>
    /// <param name="ComputedPoints"></param>
    /// <param name="DurationTotalMinutes"></param>
    /// <param name="NumPlayed"></param>
    /// <param name="BalanceMismatch"></param>
    /// <param name="AccountId"></param>
    /// <param name="TerminalId"></param>
    /// <param name="PlaySessionId"></param>
    /// <param name="Output"></param>
    /// <param name="ValueAwarded"></param>
    /// <param name="SqlTrx"></param>
    /// <returns>True: 1)Check is ok AND 2)database command is ok  ; False: 1) Check is not ok OR 2) database command is not OK</returns>
    private static Boolean Trx_PlaySessionToRevise(TerminalTypes TerminalType,
                                                   Decimal ComputedPoints,
                                                   Int32 DurationTotalMinutes,
                                                   Int64 NumPlayed,
                                                   Boolean BalanceMismatch,
                                                   Int64 AccountId,
                                                   Int64 TerminalId,
                                                   Int64 PlaySessionId,
                                                   MultiPromos.EndSessionOutput Output,
                                                   ref Boolean ValueAwarded,
                                                   SqlTransaction SqlTrx)
    {
      Int32 _max_points;
      Int32 _max_points_per_minute;
      Int16 _has_zero_plays;
      Int16 _has_mismatch;
      AwardPointsStatus _awarded_points_status;
      Decimal _awarded_points;
      StringBuilder _warning;
      StringBuilder _warning_header;
      StringBuilder _sb;

      ValueAwarded = true;
      _awarded_points = ComputedPoints;

      _warning = new StringBuilder();
      _warning_header = new StringBuilder();
      _max_points = 0;
      _max_points_per_minute = 0;
      _has_zero_plays = 0;
      _has_mismatch = 0;

      _sb = new StringBuilder();
      _sb.Length = 0;
      _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
      _sb.AppendLine("   FROM   GENERAL_PARAMS ");
      _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'PlayerTracking.DoNotAwardPoints' ");
      _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'PlaySession.PointsAreGreaterThan'");
      _sb.AppendLine("  UNION ALL ");
      _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
      _sb.AppendLine("   FROM   GENERAL_PARAMS ");
      _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'PlayerTracking.DoNotAwardPoints' ");
      _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'PlaySession.PointsPerMinuteAreGreaterThan'");
      _sb.AppendLine("  UNION ALL ");
      _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
      _sb.AppendLine("   FROM   GENERAL_PARAMS ");
      _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'PlayerTracking.DoNotAwardPoints' ");
      _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'PlaySession.HasNoPlays' ");
      _sb.AppendLine("  UNION ALL ");
      _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
      _sb.AppendLine("   FROM   GENERAL_PARAMS ");
      _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'PlayerTracking.DoNotAwardPoints' ");
      _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'PlaySession.HasBalanceMismatch' ");
      _sb.AppendLine(" ;");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              if (!Int32.TryParse(_reader.GetString(0), out _max_points))
              {
                _max_points = 0;
              }
            }

            if (_reader.Read())
            {
              if (!Int32.TryParse(_reader.GetString(0), out _max_points_per_minute))
              {
                _max_points_per_minute = 0;
              }
            }

            if (_reader.Read())
            {
              if (!Int16.TryParse(_reader.GetString(0), out _has_zero_plays))
              {
                _has_zero_plays = 0;
              }
            }

            if (_reader.Read())
            {
              if (!Int16.TryParse(_reader.GetString(0), out _has_mismatch))
              {
                _has_mismatch = 0;
              }
            }
          }
        }

        if (ValueAwarded)
        {
          if (_max_points > 0 && _max_points < ComputedPoints)
          {
            ValueAwarded = false;
            _warning.AppendFormat("Computed Points: {0}", ComputedPoints.ToString());
            _warning.AppendFormat(", Points Are Greater Than: {0}", _max_points.ToString());
          }
        }

        if (ValueAwarded)
        {
          if (DurationTotalMinutes > 0)
          {
            if (_max_points_per_minute > 0 && _max_points_per_minute < (ComputedPoints / (Decimal)DurationTotalMinutes))
            {
              ValueAwarded = false;
              _warning.AppendFormat("Computed Points: {0}", ComputedPoints.ToString());
              _warning.AppendFormat(", Minutes Played: {0}", DurationTotalMinutes.ToString("0.##"));
              _warning.AppendFormat(", Points Per Minute: {0}", ((Decimal)(ComputedPoints / (Decimal)DurationTotalMinutes)).ToString("0.##"));
              _warning.AppendFormat(", Points Per Minute Are Greater Than: {0}", _max_points_per_minute.ToString());
            }
          }
        }

        if (ValueAwarded)
        {
          if (_has_zero_plays == 1 && NumPlayed == 0)
          {
            if (TerminalType == TerminalTypes.WIN)
            {
              // AJQ & MPO 31-OCT-2013
              //   - LKT WIN has the NumPlayed equal to 0 at this point, but has the right value on DB ...
              //   - Avoid the check till we update the code, till then continue awarding the points
            }
            else
            {
              ValueAwarded = false;
              _warning.AppendFormat("Num. Played: {0}", NumPlayed.ToString());
              _warning.AppendFormat(", Has No Plays: True");
            }
          }
        }

        if (ValueAwarded)
        {
          if (_has_mismatch == 1 && BalanceMismatch)
          {
            ValueAwarded = false;
            _warning.AppendFormat("Balance Mismatch: {0}", BalanceMismatch.ToString());
            _warning.AppendFormat(", Has Balance Mismatch: True");
          }
        }

        _awarded_points_status = AwardPointsStatus.CalculatedAndReward;
        if (!ValueAwarded)
        {
          _awarded_points = 0;
          _awarded_points_status = AwardPointsStatus.Pending;

          _warning_header.Append("Play to revise -> ");
          _warning_header.AppendFormat("AccountId: {0}", AccountId);
          _warning_header.AppendFormat(", TerminalId: {0}", TerminalId);
          _warning_header.Append(" Criteria -> ");

          _warning.Insert(0, _warning_header.ToString());

          Output.Warning(_warning.ToString());
        }

        _sb = new StringBuilder();
        _sb.AppendLine("   UPDATE   PLAY_SESSIONS ");
        _sb.AppendLine("      SET   PS_AWARDED_POINTS_STATUS = CASE WHEN PS_STATUS <> @pStateOpened THEN @pPointsStatus   ELSE PS_AWARDED_POINTS_STATUS END ");
        _sb.AppendLine("          , PS_COMPUTED_POINTS       = CASE WHEN PS_STATUS <> @pStateOpened THEN @pComputedPoints ELSE NULL END ");
        _sb.AppendLine("          , PS_AWARDED_POINTS        = CASE WHEN PS_STATUS <> @pStateOpened THEN @pAwardedPoints  ELSE NULL END ");
        _sb.AppendLine("    WHERE   PS_PLAY_SESSION_ID       = @pPlaySessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pStateOpened", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pPointsStatus", SqlDbType.Int).Value = (Int32)_awarded_points_status;
          _cmd.Parameters.Add("@pComputedPoints", SqlDbType.Money).Value = ComputedPoints;
          _cmd.Parameters.Add("@pAwardedPoints", SqlDbType.Money).Value = _awarded_points;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return ValueAwarded;
          }
        }
      }
      catch
      { ;}

      return false;
    } // Trx_PlaySessionToRevise

    public static DataTable FromBucketDictToTable(WonBucketDict WonBucketList)
    {
      DataTable _wonBucketsAsTable;

      if (m_Buckets == null)
      {
        CreateTableStructure();
      }
      _wonBucketsAsTable = m_Buckets.Clone();

      foreach (KeyValuePair<Buckets.BucketId, WonBucket> _key_value in WonBucketList.Dict)
      {
        DataRow _new_row = _wonBucketsAsTable.NewRow();
        _new_row["BUCKET_ID"] = _key_value.Key;
        _new_row["WONVALUE"] = _key_value.Value.WonValue;

        _new_row["ACCOUNT_ID"] = _key_value.Value.AccountId;
        _new_row["PPS_SESSION_ID"] = _key_value.Value.PlaySessionId;
        _new_row["PPS_TERMINAL_ID"] = _key_value.Value.TerminalId;
        _new_row["TERMINAL_NAME"] = _key_value.Value.TerminalName;
        _wonBucketsAsTable.Rows.Add(_new_row);
      }

      return _wonBucketsAsTable;
    } // FromBucketDictToTable

    /// <summary>
    /// BatchUpdates both CUSTOMER_BUCKET and CUSTOMER_BUCKET_BY_GAMING_DAY tables.
    /// </summary>
    /// <param name="Dt"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean BatchUpdateBuckets(DataTable Dt, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _sprm;

      int _row_number;

      if ((Dt != null) && (Dt.Rows.Count > 0))
      {
        try
        {
          _sb = new StringBuilder();
          _sb = GetUpdateCustomerBucketSQL(Buckets.BucketOperation.ADD_GENERATED, true, true, true);

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            //for CUSTOMER_BUCKET
            _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).SourceColumn = "ACCOUNT_ID";
            _sql_cmd.Parameters.Add("@pBucketID", SqlDbType.BigInt).SourceColumn = "BUCKET_ID";
            _sql_cmd.Parameters.Add("@pOperationAmount", SqlDbType.Money).SourceColumn = "WONVALUE";

            //for CUSTOMER_BUCKET_BY_GAMING_DAY
            _sql_cmd.Parameters.Add("@pGamingDay", SqlDbType.Int).Value = Int32.Parse(Misc.TodayOpening().ToString("yyyyMMdd"));

            //Output parameters
            _sql_cmd.Parameters.Add("@pRowsAffected", SqlDbType.BigInt).Direction = ParameterDirection.Output;
            _sql_cmd.Parameters.Add("@pCustomerBucketNewValue", SqlDbType.Money).Direction = ParameterDirection.Output;

            _sprm = _sql_cmd.Parameters.Add("@pCustomerBucketOldValue", SqlDbType.Money);
            _sprm.Direction = ParameterDirection.Output;
            _sprm.SourceColumn = "INITIAL_BALANCE";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.InsertCommand = _sql_cmd;
              _sql_da.InsertCommand.Connection = SqlTrx.Connection;
              _sql_da.InsertCommand.Transaction = SqlTrx;
              _sql_da.ContinueUpdateOnError = false;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _row_number = _sql_da.Update(Dt);
            }

            return true;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "BatchUpdateBucket" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
          return false;
        }
      }

      return true;

    } // BatchUpdateBuckets

    /// <summary>
    /// Generates the SQL for updating the CUSTOMER_BUCKET and/or CUSTOMER_BUCKET_BY_GAMING_DAY tables
    /// </summary>
    /// <param name="Operation">Bucket Operation to perform</param>
    /// <param name="UpdateCustomerBucket_Amount">Update field CBU_VALUE of table CUSTOMER_BUCKET</param>
    /// <param name="UpdateCustomerBucketByGamingDay">Update table CUSTOMER_BUCKET_BY_GAMING_DAY (History by Day)</param>
    /// <param name="UpdateCustomerBucket_Date">Update field CBU_UPDATED of table CUSTOMER_BUCKET, used at bucket expiration</param>
    /// <returns>StringBuilder</returns>
    private static StringBuilder GetUpdateCustomerBucketSQL(Buckets.BucketOperation Operation
                                                          , Boolean UpdateCustomerBucket_Amount
                                                          , Boolean UpdateCustomerBucketByGamingDay
                                                          , Boolean UpdateCustomerBucket_Date)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      try
      {
        //Output parameters
        _sb.AppendLine(" SET @pCustomerBucketOldValue = 0;                                      ");
        _sb.AppendLine(" SET @pCustomerBucketNewValue = 0;                                      ");
        _sb.AppendLine(" SET @pRowsAffected = 0;                                                ");
        _sb.AppendLine("                                                                        ");

        _sb.AppendLine(" SELECT   @pCustomerBucketOldValue = ISNULL(CBU_VALUE, 0)               ");
        _sb.AppendLine("   FROM   CUSTOMER_BUCKET                                               ");
        _sb.AppendLine("  WHERE   CBU_CUSTOMER_ID = @pAccountId                                 ");
        _sb.AppendLine("    AND   CBU_BUCKET_ID = @pBucketId;                                   ");
        _sb.AppendLine("                                                                        ");

        //Variables
        if (UpdateCustomerBucket_Amount)
        {
          _sb.AppendLine(" IF (EXISTS (SELECT CBU_VALUE                                         ");
          _sb.AppendLine("               FROM CUSTOMER_BUCKET                                   ");
          _sb.AppendLine("              WHERE CBU_CUSTOMER_ID = @pAccountId                     ");
          _sb.AppendLine("                AND CBU_BUCKET_ID   = @pBucketId))                    ");
          _sb.AppendLine(" BEGIN                                                                ");
          _sb.AppendLine("      UPDATE CUSTOMER_BUCKET                                          "); // Update

          switch (Operation)
          {
            case Buckets.BucketOperation.ADD_DISCRETIONAL:
            case Buckets.BucketOperation.ADD_GENERATED:
              _sb.AppendLine("      SET CBU_VALUE = (ISNULL(CBU_VALUE, 0) + @pOperationAmount)  ");
              break;
            case Buckets.BucketOperation.SUB_DISCRETIONAL:
            case Buckets.BucketOperation.SUB_GENERATED:
              _sb.AppendLine("      SET CBU_VALUE = (ISNULL(CBU_VALUE, 0) - @pOperationAmount)  ");
              break;
            case Buckets.BucketOperation.SET_DISCRETIONAL:
            case Buckets.BucketOperation.SET_GENERATED:
              _sb.AppendLine("      SET CBU_VALUE = @pOperationAmount                           ");
              break;
            case Buckets.BucketOperation.EXPIRE:
              //When expiring the bucket, we value of the bucket just before the expiration 
              _sb.AppendLine("      SET CBU_VALUE = CASE WHEN (ISNULL(CBU_VALUE, 0) - @pOperationAmount > 0) THEN ");
              _sb.AppendLine("                                (ISNULL(CBU_VALUE, 0) - @pOperationAmount)          ");
              _sb.AppendLine("                           ELSE                                                     ");
              _sb.AppendLine("                               0                                                    ");
              _sb.AppendLine("                      END                                                           ");
              break;
            default:
              throw new Exception(" UpdateBucketAmount. Invalid Operation value ");
          }

          //If we are updating the CUSTOMER_BUCKET, then if UpdateCustomerBucket_Date, update the CUSTOMER_BUCKET.CBU_UPDATED
          if (UpdateCustomerBucket_Date)
          {
            _sb.AppendLine("           , CBU_UPDATED = GetDate()                      ");
          }
          _sb.AppendLine("      WHERE CBU_CUSTOMER_ID = @pAccountId                 ");
          _sb.AppendLine("        AND CBU_BUCKET_ID = @pBucketId;                   ");
          _sb.AppendLine(" END                                                      ");
          _sb.AppendLine(" ELSE                                                     ");
          _sb.AppendLine(" BEGIN                                                    ");
          _sb.AppendLine("      INSERT INTO CUSTOMER_BUCKET                         "); //Insert si no existe.
          _sb.AppendLine("      (                                                   ");
          _sb.AppendLine("            CBU_CUSTOMER_ID                               ");
          _sb.AppendLine("          , CBU_BUCKET_ID                                 ");
          _sb.AppendLine("          , CBU_VALUE                                     ");
          _sb.AppendLine("          , CBU_UPDATED                                   ");
          _sb.AppendLine("      )                                                   ");
          _sb.AppendLine("      VALUES                                              ");
          _sb.AppendLine("      (                                                   ");
          _sb.AppendLine("            @pAccountId                                   ");
          _sb.AppendLine("          , @pBucketId                                    ");

          switch (Operation)
          {
            case Buckets.BucketOperation.ADD_DISCRETIONAL:
            case Buckets.BucketOperation.ADD_GENERATED:
            case Buckets.BucketOperation.SET_DISCRETIONAL:
            case Buckets.BucketOperation.SET_GENERATED:
              _sb.AppendLine("       , @pOperationAmount                            ");

              break;
            case Buckets.BucketOperation.SUB_DISCRETIONAL:
            case Buckets.BucketOperation.SUB_GENERATED:
              _sb.AppendLine("       , @pOperationAmount * (-1)                     ");

              break;
            case Buckets.BucketOperation.EXPIRE:
              _sb.AppendLine("       ,0                                             ");

              break;
            default:
              throw new Exception(" InsertBucketAmount. Invalid Operation value     ");
          }

          _sb.AppendLine("          , GetDate()                                     ");
          _sb.AppendLine("      );                                                  ");
          _sb.AppendLine(" END                                                      ");
          _sb.AppendLine("                                                          ");
          _sb.AppendLine(" SET @pRowsAffected = @pRowsAffected + @@ROWCOUNT;        ");
        }

        _sb.AppendLine(" SELECT   @pCustomerBucketNewValue = ISNULL(CBU_VALUE, 0)               ");
        _sb.AppendLine("   FROM   CUSTOMER_BUCKET                                               ");
        _sb.AppendLine("  WHERE   CBU_CUSTOMER_ID = @pAccountId                                 ");
        _sb.AppendLine("    AND   CBU_BUCKET_ID = @pBucketId                                    ");

        if (UpdateCustomerBucketByGamingDay)
        {
          _sb.AppendLine(" DECLARE @AddValue money;                                 ");
          _sb.AppendLine(" DECLARE @SubValue money;                                 ");
          _sb.AppendLine(" SET @AddValue = 0;                                       ");
          _sb.AppendLine(" SET @SubValue = 0;                                       ");
          _sb.AppendLine("                                                          ");

          //Buscamos el anterior y el nuevo valor del CUSTOMER_BUCKET
          if (!UpdateCustomerBucket_Amount)
          {
            //Si no hemos updatado CUSTOMER_BUCKET
            _sb.AppendLine(" SELECT TOP 1 @pCustomerBucketOldValue = ISNULL(CBUD_VALUE, 0)       ");
            _sb.AppendLine("   FROM CUSTOMER_BUCKET_BY_GAMING_DAY                                ");
            _sb.AppendLine("  WHERE CBUD_CUSTOMER_ID = @pAccountId                               ");
            _sb.AppendLine("    AND CBUD_BUCKET_ID = @pBucketId                                  ");
            _sb.AppendLine("  ORDER BY CBUD_GAMING_DAY DESC;                                     ");
          }

          //Calculamos la diferencia entre el valor actual y el anterior
          _sb.AppendLine("                                                                       ");
          _sb.AppendLine(" IF (@pCustomerBucketNewValue >= @pCustomerBucketOldValue)             ");
          _sb.AppendLine(" BEGIN                                                                 ");
          _sb.AppendLine("     SET @AddValue = (@pCustomerBucketNewValue - @pCustomerBucketOldValue); ");
          _sb.AppendLine("     SET @SubValue = 0;                                                ");
          _sb.AppendLine(" END                                                                   ");
          _sb.AppendLine(" ELSE                                                                  ");
          _sb.AppendLine(" BEGIN                                                                 ");
          _sb.AppendLine("     SET @AddValue = 0;                                                ");
          _sb.AppendLine("     SET @SubValue = (@pCustomerBucketOldValue - @pCustomerBucketNewValue); ");
          _sb.AppendLine(" END                                                                   ");

          _sb.AppendLine(" IF ( EXISTS(SELECT 1                                                  ");
          _sb.AppendLine("               FROM CUSTOMER_BUCKET_BY_GAMING_DAY                      ");
          _sb.AppendLine("              WHERE CBUD_CUSTOMER_ID = @pAccountId                     ");
          _sb.AppendLine("                AND CBUD_BUCKET_ID = @pBucketId                        ");
          _sb.AppendLine("                AND CBUD_GAMING_DAY = @pGamingDay))                    ");
          _sb.AppendLine(" BEGIN                                                                 ");

          _sb.AppendLine("     UPDATE CUSTOMER_BUCKET_BY_GAMING_DAY                              "); // Update
          _sb.AppendLine("     SET CBUD_VALUE = @pCustomerBucketNewValue                         ");
          _sb.AppendLine("       , CBUD_VALUE_ADDED = ISNULL(CBUD_VALUE_ADDED, 0) + @AddValue    ");
          _sb.AppendLine("       , CBUD_VALUE_SUBSTRACTED = ISNULL(CBUD_VALUE_SUBSTRACTED, 0) + @SubValue ");

          _sb.AppendLine("      WHERE CBUD_CUSTOMER_ID = @pAccountId                             ");
          _sb.AppendLine("        AND CBUD_BUCKET_ID = @pBucketId                                ");
          _sb.AppendLine("        AND CBUD_GAMING_DAY = @pGamingDay;                             ");

          _sb.AppendLine(" END                                                                   ");

          _sb.AppendLine(" ELSE                                                                  ");
          _sb.AppendLine(" BEGIN                                                                 ");

          _sb.AppendLine("     INSERT INTO CUSTOMER_BUCKET_BY_GAMING_DAY (                       "); //Insert si no existe.
          _sb.AppendLine("         CBUD_GAMING_DAY                                               ");
          _sb.AppendLine("       , CBUD_CUSTOMER_ID                                              ");
          _sb.AppendLine("       , CBUD_BUCKET_ID                                                ");
          _sb.AppendLine("       , CBUD_VALUE                                                    ");
          _sb.AppendLine("       , CBUD_VALUE_ADDED                                              ");
          _sb.AppendLine("       , CBUD_VALUE_SUBSTRACTED                                        ");
          _sb.AppendLine("       )                                                               ");
          //Insertamos NewValue, que es el valor que hemos puesto en customer_bucket.
          _sb.AppendLine("     VALUES                                                            ");
          _sb.AppendLine("     (                                                                 ");
          _sb.AppendLine("           @pGamingDay                                                 ");
          _sb.AppendLine("         , @pAccountId                                                 ");
          _sb.AppendLine("         , @pBucketID                                                  ");
          _sb.AppendLine("         , @pCustomerBucketNewValue                                    ");
          _sb.AppendLine("         , @AddValue                                                   ");
          _sb.AppendLine("         , @SubValue                                                   ");
          _sb.AppendLine("     );                                                                ");

          _sb.AppendLine(" END                                                                   ");

          _sb.AppendLine(" SET @pRowsAffected = @pRowsAffected + @@ROWCOUNT;                     ");

          // Delete row if values = 0
          _sb.AppendLine("DELETE FROM  CUSTOMER_BUCKET_BY_GAMING_DAY                             ");
          _sb.AppendLine("      WHERE  CBUD_GAMING_DAY                  = @pGamingDay            ");
          _sb.AppendLine("        AND  CBUD_CUSTOMER_ID                 = @pAccountId            ");
          _sb.AppendLine("        AND  CBUD_BUCKET_ID                   = @pBucketID             ");
          _sb.AppendLine("        AND  ISNULL(CBUD_VALUE,0)             = 0                      ");
          _sb.AppendLine("        AND  ISNULL(CBUD_VALUE_ADDED,0)       = 0                      ");
          _sb.AppendLine("        AND  ISNULL(CBUD_VALUE_SUBSTRACTED,0) = 0                      ");

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _sb;
    }


    public static Boolean UpdateCustomerBucket(Int64 AccountId, Buckets.BucketId BucketId, Decimal? InitialValue, Decimal Value, Buckets.BucketOperation Operation, Boolean UpdateCustomerBucket_Amount, Boolean UpdateCustomerBucketByGamingDay, SqlTransaction SqlTrx)
    {
      Decimal _old_value;
      Decimal _new_value;
      Boolean result;
      result = false;

      if (BucketId == Buckets.BucketId.RankingLevelPoints && Operation == Buckets.BucketOperation.ADD_DISCRETIONAL)   //viene de importacion de puntos
      {
        result = UpdateCustomerBucket(AccountId, Buckets.BucketId.RankingLevelPoints_Discretional, InitialValue, Value, Operation, UpdateCustomerBucket_Amount, UpdateCustomerBucketByGamingDay, out _old_value, out _new_value, SqlTrx);
      }
      return UpdateCustomerBucket(AccountId, BucketId, InitialValue, Value, Operation, UpdateCustomerBucket_Amount, UpdateCustomerBucketByGamingDay, out _old_value, out _new_value, SqlTrx);
    }

    /// <summary>
    /// Update the Customer Bucket
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="BucketId"></param>
    /// <param name="InitialValue">If not null, verifies that the bucket value before the operation is the same as this parameter (InitialValue) </param>
    /// <param name="Value">Value of the operation</param>
    /// <param name="Operation">Operation to perform with the bucket</param>
    /// <param name="UpdateCustomerBucket_Amount">Shall we update the AMOUNT field of the CUSTOMER_BUCKET table</param>
    /// <param name="UpdateCustomerBucketByGamingDay">Shall we update the CUSTOMER_BUCKET_BY_GAMING_DAY table</param>
    /// <param name="OldValue">Value of the bucket before the operation</param>
    /// <param name="NewValue">Value of the bucket after the operation</param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean UpdateCustomerBucket(Int64 AccountId
                                             , Buckets.BucketId BucketId
                                             , Decimal? InitialValue
                                             , Decimal Value
                                             , Buckets.BucketOperation Operation
                                             , Boolean UpdateCustomerBucket_Amount
                                             , Boolean UpdateCustomerBucketByGamingDay
                                             , out Decimal OldValue
                                             , out Decimal NewValue
                                             , SqlTransaction SqlTrx)
    {
      // cashier frm_add_amount_multiple_buckets gets here
      StringBuilder _sb = null;
      Int32 _gamingDay = Int32.Parse(Misc.TodayOpening().ToString("yyyyMMdd"));
      Int64 _rows_affected = 0;
      Int64 _num_rows_to_control = 0;

      OldValue = 0;
      NewValue = 0;

      try
      {
        _sb = GetUpdateCustomerBucketSQL(Operation, UpdateCustomerBucket_Amount, UpdateCustomerBucketByGamingDay, false);

        if (UpdateCustomerBucket_Amount)
        {
          _num_rows_to_control++;
        }

        if (UpdateCustomerBucketByGamingDay)
        {
          _num_rows_to_control++;
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _paramRowsAffected;
          SqlParameter _paramCustomerBucketOldValue;
          SqlParameter _paramCustomerBucketNewValue;

          //Input parameters
          if (UpdateCustomerBucketByGamingDay)
          {
            _sql_cmd.Parameters.Add("@pGamingDay", SqlDbType.Int).Value = _gamingDay;
          }

          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pBucketId", SqlDbType.Int).Value = BucketId;
          _sql_cmd.Parameters.Add("@pOperationAmount", SqlDbType.Money).Value = Value;

          //Output parameters
          _paramRowsAffected = _sql_cmd.Parameters.Add("@pRowsAffected", SqlDbType.BigInt);
          _paramRowsAffected.Direction = ParameterDirection.Output;
          _paramCustomerBucketOldValue = _sql_cmd.Parameters.Add("@pCustomerBucketOldValue", SqlDbType.Money);
          _paramCustomerBucketOldValue.Direction = ParameterDirection.Output;
          _paramCustomerBucketNewValue = _sql_cmd.Parameters.Add("@pCustomerBucketNewValue", SqlDbType.Money);
          _paramCustomerBucketNewValue.Direction = ParameterDirection.Output;

          //Procesamos los buckets
          _sql_cmd.ExecuteNonQuery();

          //Output parameter with the number of affected rows.
          if (_paramRowsAffected.Value != DBNull.Value)
          {
            _rows_affected = (Int64)_paramRowsAffected.Value;
          }

          //Output parameter with the old value of the bucket.
          if (_paramCustomerBucketOldValue.Value != DBNull.Value)
          {
            OldValue = (Decimal)_paramCustomerBucketOldValue.Value;
          }

          //Output parameter with the new value of the bucket.
          if (_paramCustomerBucketNewValue.Value != DBNull.Value)
          {
            NewValue = (Decimal)_paramCustomerBucketNewValue.Value;
          }

          //Si sabemos el valor inicial del bucket, comprobamos el valor al updatar.
          if (InitialValue != null)
          {
            //OK si hemos modificado un registro y el valor que habia no es el mismo valor que le hemos indicado
            // Esto es por si el bucket ha expirado mientras realizabamos la operacion
            return (OldValue == InitialValue);
          }

          //Dependiendo del valor de UpdateCustomerBucket_Amount y UpdateCustomerBucketByGamingDay tienen que ser 1 o 2 registros modificados
          return (_rows_affected == _num_rows_to_control);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean CreateMovements(OperationCode OperationCode, MovementType AccountMovementType, CASHIER_MOVEMENT CashierMovementType, WonBucketDict WonBucketList, CashierSessionInfo CashierSession, SqlTransaction SqlTrx)
    {
      return CreateMovements(OperationCode, AccountMovementType, CashierMovementType, FromBucketDictToTable(WonBucketList), CashierSession, SqlTrx);
    }

    public static Boolean CreateMovements(OperationCode OperationCode, MovementType AccountMovementType, CASHIER_MOVEMENT CashierMovementType, WonBucketDict WonBucketList, SqlTransaction SqlTrx)
    {
      CashierSessionInfo _cashier_session;

      _cashier_session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);

      return CreateMovements(OperationCode, AccountMovementType, CashierMovementType, FromBucketDictToTable(WonBucketList), _cashier_session, SqlTrx);
    }

    public static Boolean CreateMovements(OperationCode OperationCode, MovementType AccountMovementType, CASHIER_MOVEMENT CashierMovementType, DataTable Dt, SqlTransaction SqlTrx)
    {
      CashierSessionInfo _cashier_session;

      _cashier_session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);

      return CreateMovements(OperationCode, AccountMovementType, CashierMovementType, Dt, _cashier_session, SqlTrx);

    } // CreateMovements

    public static Boolean CreateMovements(OperationCode OperationCode, MovementType AccountMovementType, CASHIER_MOVEMENT CashierMovementType, DataTable Dt, CashierSessionInfo CashierSession, SqlTransaction SqlTrx)
    {
      int _bucketId;
      _bucketId = 0;
      try
      {
        if (Dt == null || Dt.Rows.Count == 0)
        {
          return true;
        }

        Int64 _operation_id;
        CashierSessionInfo _cashier_session;
        _cashier_session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
        AccountMovementsTable _ac_mov_table = new AccountMovementsTable(_cashier_session);
        CashierMovementsTable _cashier_movements_table = new CashierMovementsTable(_cashier_session);

        foreach (DataRow _dr in Dt.Rows)
        {
          _bucketId = (int)(long)_dr["BUCKET_ID"];
          // Movements for buckets 8,9 are not generated
          if (!(_bucketId == (Int32)BucketId.RankingLevelPoints_Discretional || _bucketId == (Int32)BucketId.RankingLevelPoints_Generated))
          {
            if (!Operations.DB_InsertOperation(OperationCode, 0, _cashier_session.CashierSessionId, 0, 0, (Currency)(Decimal)_dr["WONVALUE"], 0, 0, 0, string.Empty, out _operation_id, SqlTrx))
            {
              return false;
            }

            //account movement
            if (!_ac_mov_table.Add(_operation_id,
                                   (long)_dr["ACCOUNT_ID"],
                                   AccountMovementType + (int)(long)_dr["BUCKET_ID"],
                                   (Decimal)_dr["INITIAL_BALANCE"],
                                   0,
                                   (Decimal)_dr["WONVALUE"],
                                   (Decimal)_dr["INITIAL_BALANCE"] + (Decimal)_dr["WONVALUE"],
                                   "",
                                   (int)(long)_dr["PPS_TERMINAL_ID"],
                                   _dr.IsNull("TERMINAL_NAME") ? "" : (String)_dr["TERMINAL_NAME"],
                                   "",
                                   _dr.IsNull("PPS_SESSION_ID") ? 0 : (Int64)_dr["PPS_SESSION_ID"]))
            {
              return false;
            }

            // cashier movements
            if (!_cashier_movements_table.Add(_operation_id,
                                              CashierMovementType + (int)(long)_dr["BUCKET_ID"],
                                              (Decimal)_dr["WONVALUE"],
                                              (long)_dr["ACCOUNT_ID"],
                                              String.Empty))
            {
              return false;
            }

          }
        } // foreach

        if (!_ac_mov_table.Save(SqlTrx))
        {
          return false;
        }

        if (!_cashier_movements_table.Save(SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CreateMovements


    private static Boolean DeletePendingSessionProcessed(DataTable Dt, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _rows_deleted_gps;
      Int32 _rows_affected;

      _sb = new StringBuilder();
      _sb.AppendLine(" DELETE FROM PENDING_PLAY_SESSIONS_TO_PLAYER_TRACKING ");
      _sb.AppendLine(" WHERE PPS_SESSION_ID = @pPpsSessionID                ");

      try
      {
        if ((Dt != null) && (Dt.Rows.Count > 0))
        {
          foreach (DataRow _dr in Dt.Rows)
          {
            _dr.Delete();
          }

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pPpsSessionID", SqlDbType.BigInt).SourceColumn = "PPS_SESSION_ID";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.DeleteCommand = _sql_cmd;
              _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_da.DeleteCommand.Connection = SqlTrx.Connection;
              _sql_da.DeleteCommand.Transaction = SqlTrx;
              _sql_da.UpdateBatchSize = 500;
              _rows_deleted_gps = Dt.Rows.Count;

              _rows_affected = _sql_da.Update(Dt);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "DeleteRow" + EXCEPTION_MESSAGE, _ex.Message, WGDB.Now));

        return false;
      }
    } // DeletePendingSessionProcessed

    private static int GetAccountLevel(Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      int _accountLevel;
      Object _obj;

      _accountLevel = 0;
      _sb = new StringBuilder();
      _sb.Append("SELECT ISNULL(AC_HOLDER_LEVEL,0)");
      _sb.Append("FROM ACCOUNTS                   ");
      _sb.Append("WHERE AC_ACCOUNT_ID = @pAccount ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pAccount", SqlDbType.BigInt).Value = AccountId;

        _obj = _cmd.ExecuteScalar();

        if (_obj != null && _obj != DBNull.Value)
        {
          _accountLevel = (Int32)_obj;
        }
      }
      return _accountLevel;
    }


    #endregion Private Methods.

    #region Public Methods

    public static void SetDataAvailable()
    {
      m_ev_data_available.Set();
    } // SetManualResetEvent

    #endregion Public Methods

  }  // WCP_BucketsUpdate

  public class BucketsForm
  {
    #region Members

    #endregion Members

    #region Constructor

    #endregion Constructor

    #region Public Methods

    public Boolean GetBucketsID(out List<Int64> BucketsID, Boolean OnlyEnabledBuckets = false, Boolean IncludeBucketSubsetLevel = false)
    {
      //returns a list of bucket_id  for frm_account_movements, frm_accounts_adjustment, frm_cashier_movements, frm_accounts_adjustment_multisite
      //for filling buckets selectors

      StringBuilder _sb;

      _sb = new StringBuilder();
      BucketsID = new List<Int64>();

      try
      {
        _sb.AppendLine("SELECT BU_BUCKET_ID            ");
        _sb.AppendLine("  FROM BUCKETS                 ");


        if (OnlyEnabledBuckets || !IncludeBucketSubsetLevel)
          _sb.AppendLine("  WHERE                    ");

        if (OnlyEnabledBuckets)
        {
          _sb.AppendLine("  BU_ENABLED = 1       ");
          if (!IncludeBucketSubsetLevel)
            _sb.AppendLine("  AND                  ");

        }

        if (!IncludeBucketSubsetLevel)
          _sb.AppendLine("  (BU_BUCKET_ID <> 8 AND BU_BUCKET_ID <> 9)      ");

        _sb.AppendLine("  ORDER BY BU_ORDER_ON_REPORTS ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                BucketsID.Add((Int64)(_reader["BU_BUCKET_ID"]));
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetBucketsID

    public Boolean GetBucketsByUser(Int64 AccountId, Buckets.BucketVisibility Terminal, out DataTable DtBuckets)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtBuckets = new DataTable();

      try
      {
        _sb.AppendLine("SELECT BU_NAME                               ");
        _sb.AppendLine("     , ISNULL(CBU_VALUE,0) AS VALUE          ");
        _sb.AppendLine("     , BU_BUCKET_ID                          ");
        _sb.AppendLine("     , ' ' AS STR_VALUE                      ");
        _sb.AppendLine("     , BU_BUCKET_TYPE                        ");
        _sb.AppendLine("  FROM BUCKETS                               ");

        _sb.AppendLine("LEFT JOIN CUSTOMER_BUCKET                    ");
        _sb.AppendLine("       ON BU_BUCKET_ID = CBU_BUCKET_ID       ");
        _sb.AppendLine("      AND CBU_CUSTOMER_ID = @pAccountId      "); //ETP: 20160115 Añadido en el ON y no en el where para que retorne los buckets que no estan definidos en el usuario.
        _sb.AppendLine("    WHERE BU_ENABLED = 1                     ");
        _sb.AppendLine("    AND   BU_VISIBLE_FLAGS & @pTerminal != 0 ");
        _sb.AppendLine("  ORDER BY BU_ORDER_ON_REPORTS               ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            _sql_cmd.Parameters.Add("@pTerminal", SqlDbType.Int).Value = Terminal;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(DtBuckets);

              foreach (DataRow row in DtBuckets.Rows)
              {
                row["STR_VALUE"] = SetCurrencyValue(((Decimal)row["VALUE"]), (Buckets.BucketType)row["BU_BUCKET_TYPE"]);
              }

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GetBucketsByUser

    private String GetBucketNameSQL()
    {
      StringBuilder _sb = new StringBuilder();

      _sb.AppendLine("SELECT  ISNULL(BU_NAME, 'BUCKET ' + CONVERT(NVARCHAR, BU_BUCKET_ID)) AS BU_NAME   ");
      _sb.AppendLine("  FROM  BUCKETS                                                                   ");
      _sb.AppendLine(" WHERE  BU_BUCKET_ID = @pBucketId                                                 ");

      return _sb.ToString();
    } //GetBucketNameSQL

    private String GetBucketTypeSQL()
    {
      StringBuilder _sb = new StringBuilder();

      _sb.AppendLine("SELECT  BU_BUCKET_TYPE                                  ");
      _sb.AppendLine("  FROM  BUCKETS                                         ");
      _sb.AppendLine(" WHERE  BU_BUCKET_ID = @pBucketId                       ");

      return _sb.ToString();
    }

    private Int32 GetBucketId(Int32 Movement)
    {
      Int32 BucketId;
      Int32 BucketBase;

      BucketBase = Movement / 100;
      BucketBase = BucketBase * 100;
      BucketId = Movement - BucketBase;

      return BucketId;
    }

    private Buckets.BucketId GetBucketId(MovementType AccountMovement)
    {
      Int32 BucketId;

      BucketId = GetBucketId((Int32)AccountMovement);

      return (Buckets.BucketId)BucketId;
    }

    private Buckets.BucketId GetBucketId(CASHIER_MOVEMENT AccountMovement)
    {
      Int32 BucketId;

      BucketId = GetBucketId((Int32)AccountMovement);

      return (Buckets.BucketId)BucketId;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get bucket name from ID (substract MoventType and BucketRange
    //
    //  PARAMS :
    //      - INPUT : CashierMovement: Value from movement type
    //
    //      - OUTPUT : BucketName: Get bucket from DB
    //
    // RETURNS : False: Incorrecto get a bucket name
    //           True:  Correcto get a bucket name
    //    
    public Boolean GetBucketName(CASHIER_MOVEMENT CashierMovement, out String BucketName)
    {
      return GetBucketName(GetBucketId(CashierMovement), out BucketName);

    } // GetBucketName

    //------------------------------------------------------------------------------
    // PURPOSE : Get bucket name from ID (substract MoventType and BucketRange
    //
    //  PARAMS :
    //      - INPUT : AccountMovement: Value from movement type
    //
    //      - OUTPUT : BucketName: Get bucket from DB
    //
    // RETURNS : False: Incorrecto get a bucket name
    //           True:  Correcto get a bucket name
    //    
    public Boolean GetBucketName(MovementType AccountMovement, out String BucketName)
    {
      return GetBucketName(GetBucketId(AccountMovement), out BucketName);

    } //GetBucketName

    public Boolean GetBucketName(Buckets.BucketId BucketId, out String BucketName)
    {
      String _str;

      BucketName = String.Empty;
      try
      {
        _str = GetBucketNameSQL();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_str, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pBucketId", SqlDbType.Int).Value = BucketId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return false;
              }

              BucketName = (String)_reader["BU_NAME"];

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Message("Error: BucketsFrom.GetBucketName");
        Log.Exception(_ex);

        return false;
      }
    } //GetBucketName


    //------------------------------------------------------------------------------
    // PURPOSE : Get bucket type from ID (substract MoventType and BucketRange
    //
    //  PARAMS :
    //      - INPUT : Account_movement: Value from movement type
    //
    //      - OUTPUT : MovementType: Get bucket from DB
    //
    // RETURNS : False: Incorrecto get a bucket type
    //           True:  Correcto get a bucket type
    //    
    public Boolean GetBucketType(MovementType Account_Movement, out Buckets.BucketType BucketType)
    {
      return GetBucketType(GetBucketId(Account_Movement), out BucketType);
    } //GetBucketType


    //------------------------------------------------------------------------------
    // PURPOSE : Get bucket type from ID (substract MoventType and BucketRange
    //
    //  PARAMS :
    //      - INPUT : Account_movement: Value from movement type
    //
    //      - OUTPUT : MovementType: Get bucket from DB
    //
    // RETURNS : False: Incorrecto get a bucket type
    //           True:  Correcto get a bucket type
    //    
    public Boolean GetBucketType(CASHIER_MOVEMENT CashierMovement, out Buckets.BucketType BucketType)
    {
      return GetBucketType(GetBucketId(CashierMovement), out BucketType);
    } //GetBucketType


    //------------------------------------------------------------------------------
    // PURPOSE : Get bucket type from ID (substract MoventType and BucketRange
    //
    //  PARAMS :
    //      - INPUT : Account_movement: Value from movement type
    //
    //      - OUTPUT : MovementType: Get bucket from DB
    //
    // RETURNS : False: Incorrecto get a bucket type
    //           True:  Correcto get a bucket type
    //    
    public Boolean GetBucketType(Buckets.BucketId BucketId, out Buckets.BucketType BucketType)
    {
      String _str;

      BucketType = Buckets.BucketType.Unknown;

      try
      {
        _str = GetBucketTypeSQL();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_str, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pBucketId", SqlDbType.Int).Value = BucketId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                BucketType = (Buckets.BucketType)_reader["BU_BUCKET_TYPE"];
              }
              else
              {
                return false;
              }
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Message("Error: BucketsFrom.GetBucketType");
        Log.Exception(_ex);

        return false;
      }
    } // GetBucketType


    public String SetCurrencyValue(Decimal Value, Buckets.BucketType Type)
    {
      String _currency_str;
      Currency _currency;

      switch (Buckets.GetBucketUnits(Type))
      {
        case Buckets.BucketUnits.Points:
          _currency_str = Math.Truncate(Value).ToString();
          break;

        case Buckets.BucketUnits.Money:
          _currency = (Math.Truncate(Value * 100) / 100);
          _currency_str = _currency.ToString();
          break;

        default:
          _currency_str = String.Empty;
          break;
      }

      return _currency_str;
    } // SetCurrencyValue


    public Boolean CreateCashierMovements(CASHIER_MOVEMENT CashierMovement, CashierSessionInfo CashierSession,
                                          Int64 Operation_id, Decimal Amount, Int64 AccountId, String TrackData,
                                          String Reason, SqlTransaction SqlTrx)
    {
      CashierMovementsTable _cashier_movements_table;

      _cashier_movements_table = new CashierMovementsTable(CashierSession);

      _cashier_movements_table.Add(Operation_id, CashierMovement, Amount, AccountId, TrackData, Reason);

      if (!_cashier_movements_table.Save(SqlTrx))
      {
        return false;
      }

      return true;
    } // CreateCashierMovements

    public Boolean CreateAccountMovements(MovementType Movement_Type, CashierSessionInfo CashierSession,
                                          Int64 Operation_id, Decimal InitBalance, Decimal AddAmount, Decimal SubAmount,
                                          Decimal FinalBalance, Int64 AccountId, String Reason, SqlTransaction SqlTrx)
    {
      AccountMovementsTable _account_movements_table;

      _account_movements_table = new AccountMovementsTable(CashierSession);

      _account_movements_table.Add(Operation_id, AccountId, Movement_Type, InitBalance, SubAmount, AddAmount, FinalBalance, Reason);

      if (!_account_movements_table.Save(SqlTrx))
      {
        return false;
      }

      return true;
    } // CreateAccountMovements

    public Boolean CreateAccountMovements(MovementType Movement_Type,
                                          Int64 Operation_id, Decimal InitBalance, Decimal AddAmount, Decimal SubAmount,
                                          Decimal FinalBalance, Int64 AccountId, String Reason, SqlTransaction SqlTrx)
    {
      AccountMovementsTable _account_movements_table;

      _account_movements_table = new AccountMovementsTable(0, "", 0, "");

      _account_movements_table.Add(Operation_id, AccountId, Movement_Type, InitBalance, SubAmount, AddAmount, FinalBalance, Reason);

      if (!_account_movements_table.Save(SqlTrx))
      {
        return false;
      }

      return true;
    } // CreateAccountMovements

    /// <summary>
    /// Returns if there are buckets enabled
    /// </summary>
    /// <returns></returns>
    public Boolean HasBucketsEnabled()
    {
      List<Int64> _buckets_enabled_list;

      //Search the list of buckets enabled
      if (!GetBucketsID(out _buckets_enabled_list, true, false))
      {
        return false;
      }

      if (_buckets_enabled_list == null)
      {
        return false;
      }

      return (_buckets_enabled_list.Count > 0);
    }
    #endregion Public Methods
  } // BucketsForm

  //BucketVoucher
  //Pasamos String en lugar de Currency ya que no sabemos la moneda, ni si es importe o cantidad
  public class BucketVoucher
  {
    public String BucketName { get; set; }
    public String Initial { get; set; }
    public String Increment { get; set; }
    public String Final { get; set; }
    public String[] AccountInfo { get; set; }
    public String Details { get; set; }

    #region constructor
    //------------------------------------------------------------------------------
    // PURPOSE : constructor inicialize filters to default value
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public BucketVoucher(String[] AccountInfo, String BucketName, String Initial, String Increment, String Final, String Details)
    {
      this.BucketName = BucketName;
      this.Initial = Initial;
      this.Increment = Increment;
      this.Final = Final;
      this.AccountInfo = AccountInfo;
      this.Details = Details;
    }

    #endregion //constructor
  }
}
