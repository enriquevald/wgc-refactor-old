//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BU_SessionMsgReceived.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 02-JUL-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JUL-2010 RCI    First release.
// 27-OCT-2011 ACC    Add Alarms WCP & WC2 Connected & Disconected
//
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;

namespace WSI.Common.BatchUpdate
{
  public static class TerminalSession
  {

    #region Constants

    private const Int32 TIMEOUT_MINUTES = 20;
    private const Int32 TIME_TO_CHECK_IN_MS = TIMEOUT_MINUTES * 60 * 1000;

    private const Int16 COLUMN_TERMINAL_ID = 0;
    private const Int16 COLUMN_SESSION_ID = 1;
    private const Int16 COLUMN_SEQUENCE_ID = 2;
    private const Int16 COLUMN_TRANSACTION_ID = 3;
    private const Int16 COLUMN_PROVIDER_ID = 4;
    private const Int16 COLUMN_TERMINAL_NAME = 5;
    private const Int16 COLUMN_TERMINAL_TYPE = 6;
    private const Int16 COLUMN_STATUS = 7;
    private const Int16 COLUMN_NEW_SESSION = 8;

    #endregion // Constants

    #region Attributes

    private static ReaderWriterLock m_rw_lock = new ReaderWriterLock();
    private static DataTable m_dt_sessions = null;
    private static SqlDataAdapter m_sql_adap = null;
    private static DataTable m_dt_sessions_copy = null;

    private static Thread m_thread;

    private static String m_protocol;
    private static Int32 m_opened;
    private static Int32 m_abandoned;
    private static Int32 m_disconnected;
    private static Int32 m_timeout;

    private static String m_abandoned_update_cmd_txt;
    private static String m_disconnected_update_cmd_txt;
    private static String m_timeout_update_cmd_txt;

    private static String m_disconnected_select_cmd_txt;
    private static String m_timeout_select_cmd_txt;

    #endregion // Attributes

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init attributes of the static class:
    //           - String for the SQL Command to update sessions (depending on Protocol parameter).
    //           - DataTable for the Session Cache.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String Protocol
    //          - Int32 Opened
    //          - Int32 Abandoned
    //          - Int32 Disconnected
    //          - Int32 Timeout
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init(String Protocol,
                            Int32 Opened,
                            Int32 Abandoned,
                            Int32 Disconnected,
                            Int32 Timeout)
    {
      SqlCommand _sql_command;
      String _sql_update;
      String _table_prefix1;
      String _table_prefix2;
      String _column_prefix1;
      String _column_prefix2;

      Lock();

      try
      {
        if (m_dt_sessions != null)
        {
          // already init... return!
          Log.Warning("Init: BU_SessionMsgReceived already initialized.");
          
          return;
        }

        m_protocol = Protocol;
        m_opened = Opened;
        m_abandoned = Abandoned;
        m_disconnected = Disconnected;
        m_timeout = Timeout;

        _sql_update = "UPDATE   YYY_SESSIONS " +
                        " SET   XXX_LAST_SEQUENCE_ID    = CASE WHEN (@pSequenceId > XXX_LAST_SEQUENCE_ID) THEN @pSequenceId ELSE XXX_LAST_SEQUENCE_ID END " +
                          "   , XXX_LAST_TRANSACTION_ID = CASE WHEN (@pTransactionId > XXX_LAST_TRANSACTION_ID) THEN @pTransactionId ELSE XXX_LAST_TRANSACTION_ID END " +
                          "   , XXX_LAST_RCVD_MSG       = CASE WHEN (GETDATE() > XXX_LAST_RCVD_MSG) THEN GETDATE() ELSE XXX_LAST_RCVD_MSG END " +
                          "   , XXX_STATUS              = @pStatus " +
                      " WHERE   XXX_TERMINAL_ID         = @pTerminalId " +
                        " AND   XXX_SESSION_ID          = @pSessionId " +
                        " AND   XXX_SERVER_NAME         = @pServerName " +
                        " AND   XXX_STATUS              = @pOpenedStatus ";

        m_abandoned_update_cmd_txt = "";
        m_abandoned_update_cmd_txt += " UPDATE   YYY_SESSIONS ";
        m_abandoned_update_cmd_txt += "    SET   XXX_STATUS      = @pStatus ";
        m_abandoned_update_cmd_txt += "        , XXX_FINISHED    = GETDATE() ";
        m_abandoned_update_cmd_txt += "  WHERE   XXX_TERMINAL_ID = @pTerminalId ";
        m_abandoned_update_cmd_txt += "    AND   XXX_STATUS      = 0 ";
        m_abandoned_update_cmd_txt += "    AND   XXX_SESSION_ID  < @pSessionId ";

        m_disconnected_select_cmd_txt = "";
        m_disconnected_select_cmd_txt += " SELECT   XXX_SESSION_ID ";
        m_disconnected_select_cmd_txt += "   FROM   YYY_SESSIONS ";
        m_disconnected_select_cmd_txt += "  WHERE   XXX_STATUS      = 0 ";
        m_disconnected_select_cmd_txt += "    AND   XXX_SERVER_NAME = @pServerName ";

        m_disconnected_update_cmd_txt = "";
        m_disconnected_update_cmd_txt += " UPDATE   YYY_SESSIONS ";
        m_disconnected_update_cmd_txt += "    SET   XXX_STATUS      = @pStatus ";
        m_disconnected_update_cmd_txt += "        , XXX_FINISHED    = GETDATE() ";
        m_disconnected_update_cmd_txt += "  WHERE   XXX_SESSION_ID  = @pSessionId ";
        m_disconnected_update_cmd_txt += "    AND   XXX_STATUS      = 0 ";
        m_disconnected_update_cmd_txt += "    AND   XXX_SERVER_NAME = @pServerName ";

        m_timeout_select_cmd_txt = "";
        m_timeout_select_cmd_txt += " SELECT   TOP 10 XXX_SESSION_ID ";
        m_timeout_select_cmd_txt += "   FROM   YYY_SESSIONS ";
        m_timeout_select_cmd_txt += "  WHERE   XXX_STATUS      = 0 ";
        m_timeout_select_cmd_txt += "    AND   (XXX_SERVER_NAME IS NULL OR XXX_SERVER_NAME <> @pServerName) ";
        m_timeout_select_cmd_txt += "    AND   DATEDIFF(MINUTE ,XXX_LAST_RCVD_MSG, GETDATE()) >= @pElapsedMinutes ";

        m_timeout_update_cmd_txt = "";
        m_timeout_update_cmd_txt += " UPDATE   YYY_SESSIONS ";
        m_timeout_update_cmd_txt += "    SET   XXX_STATUS      = @pStatus ";
        m_timeout_update_cmd_txt += "        , XXX_FINISHED    = GETDATE() ";
        m_timeout_update_cmd_txt += "  WHERE   XXX_SESSION_ID  = @pSessionId ";
        m_timeout_update_cmd_txt += "    AND   XXX_STATUS      = 0 ";
        m_timeout_update_cmd_txt += "    AND   (XXX_SERVER_NAME IS NULL OR XXX_SERVER_NAME <> @pServerName) ";
        m_timeout_update_cmd_txt += "    AND   DATEDIFF(MINUTE ,XXX_LAST_RCVD_MSG, GETDATE()) >= @pElapsedMinutes ";

        _table_prefix1 = "YYY";
        _column_prefix1 = "XXX";

        if (Protocol == "WC2")
        {
          _table_prefix2 = "WC2";
          _column_prefix2 = "W2S";
        }
        else if (Protocol == "WCP")
        {
          _table_prefix2 = "WCP";
          _column_prefix2 = "WS";
        }
        else if (Protocol == "WWP")
        {
          _table_prefix2 = "WWP";
          _column_prefix2 = "WWS";
        }
        else
        {
          throw new Exception("BU_TerminalSession.Init: Protocol " + Protocol + " not supported.");
        }

        _sql_update = _sql_update.Replace(_table_prefix1, _table_prefix2).Replace(_column_prefix1, _column_prefix2);
        m_abandoned_update_cmd_txt = m_abandoned_update_cmd_txt.Replace(_table_prefix1, _table_prefix2).Replace(_column_prefix1, _column_prefix2);
        m_disconnected_select_cmd_txt = m_disconnected_select_cmd_txt.Replace(_table_prefix1, _table_prefix2).Replace(_column_prefix1, _column_prefix2);
        m_disconnected_update_cmd_txt = m_disconnected_update_cmd_txt.Replace(_table_prefix1, _table_prefix2).Replace(_column_prefix1, _column_prefix2);
        m_timeout_select_cmd_txt = m_timeout_select_cmd_txt.Replace(_table_prefix1, _table_prefix2).Replace(_column_prefix1, _column_prefix2);
        m_timeout_update_cmd_txt = m_timeout_update_cmd_txt.Replace(_table_prefix1, _table_prefix2).Replace(_column_prefix1, _column_prefix2);

        if (Protocol == "WWP")
        {
          _column_prefix1 = "WWS_TERMINAL_ID";
          _column_prefix2 = "WWS_SITE_ID";

          _sql_update = _sql_update.Replace(_column_prefix1, _column_prefix2);
          m_abandoned_update_cmd_txt = m_abandoned_update_cmd_txt.Replace(_column_prefix1, _column_prefix2);
          m_disconnected_select_cmd_txt = m_disconnected_select_cmd_txt.Replace(_column_prefix1, _column_prefix2);
          m_disconnected_update_cmd_txt = m_disconnected_update_cmd_txt.Replace(_column_prefix1, _column_prefix2);
          m_timeout_select_cmd_txt = m_timeout_select_cmd_txt.Replace(_column_prefix1, _column_prefix2);
          m_timeout_update_cmd_txt = m_timeout_update_cmd_txt.Replace(_column_prefix1, _column_prefix2);
        }

        // Create DataTable Columns

        m_dt_sessions = new DataTable("SESSIONS");

        // For WWP, SITE_ID is stored in field TERMINAL_ID.
        m_dt_sessions.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
        m_dt_sessions.Columns.Add("SESSION_ID", Type.GetType("System.Int64")).Unique  = true;
        m_dt_sessions.Columns.Add("SEQUENCE_ID", Type.GetType("System.Int64"));
        m_dt_sessions.Columns.Add("TRANSACTION_ID", Type.GetType("System.Int64"));
        m_dt_sessions.Columns.Add("PROVIDER_ID", Type.GetType("System.String"));
        m_dt_sessions.Columns.Add("TERMINAL_NAME", Type.GetType("System.String"));
        m_dt_sessions.Columns.Add("TERMINAL_TYPE", Type.GetType("System.Int16"));
        m_dt_sessions.Columns.Add("STATUS", Type.GetType("System.Int32"));
        m_dt_sessions.Columns.Add("NEW_SESSION", Type.GetType("System.Int16"));

        m_dt_sessions_copy = m_dt_sessions.Clone();

        // Create Update Command and SqlDataAdapter.

        _sql_command = new SqlCommand(_sql_update);
        _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
        _sql_command.Parameters.Add("@pSessionId", SqlDbType.BigInt).SourceColumn = "SESSION_ID";
        _sql_command.Parameters.Add("@pSequenceId", SqlDbType.BigInt).SourceColumn = "SEQUENCE_ID";
        _sql_command.Parameters.Add("@pTransactionId", SqlDbType.BigInt).SourceColumn = "TRANSACTION_ID";
        _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "STATUS";
        _sql_command.Parameters.Add("@pServerName", SqlDbType.NVarChar).Value = Environment.MachineName;
        _sql_command.Parameters.Add("@pOpenedStatus", SqlDbType.Int).Value = m_opened;

        m_sql_adap = new SqlDataAdapter();
        m_sql_adap.SelectCommand = null;
        m_sql_adap.InsertCommand = null;
        m_sql_adap.UpdateCommand = _sql_command;
        m_sql_adap.DeleteCommand = null;
        m_sql_adap.ContinueUpdateOnError = true;

        m_sql_adap.UpdateBatchSize = 500;
        m_sql_adap.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        m_thread = new Thread(new ThreadStart (TerminalSession.SessionThread));
        m_thread.Name = "TerminalSession.SessionThread";
        m_thread.Start();
      }
      finally
      {
        Unlock();
      }
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Add a session in the DataTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32  TerminalId
    //          - Int64  SessionId
    //          - Int64  SequenceId
    //          - Int64  TransactionId
    //          - String ProviderId
    //          - String Name
    //          - Int16  TerminalType
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If session has been added correctly.
    //
    public static Boolean AddSession(Int32 TerminalId,
                                     Int64 SessionId,
                                     Int64 SequenceId,
                                     Int64 TransactionId,
                                     String ProviderId,
                                     String Name,
                                     Int16 TerminalType)
    {
      String _filter;
      DataRow[] _dr_sessions;
      DataRow _session;

      Lock();

      try
      {
        if (m_dt_sessions == null)
        {
          Log.Error("Not initialized. BU.SessionMsgReceived.AddSession Terminal: " + TerminalId + ", Session: " + SessionId + ".");

          return false;
        }

        // Search Terminal.
        _filter = "TERMINAL_ID = " + TerminalId;
        _dr_sessions = m_dt_sessions.Select(_filter);

        // Mark as abandoned all previous sessions.
        foreach (DataRow _dr_session in _dr_sessions)
        {
          if ( (Int64) _dr_session[COLUMN_SESSION_ID] == SessionId )
          {
            // Same session ID found, remove and later insert a new one
            m_dt_sessions.Rows.Remove(_dr_session);

            continue;
          }
          _dr_session[COLUMN_STATUS] = m_abandoned;
        }

        // Insert TerminalId/SessionId
        _session = m_dt_sessions.NewRow();
        _session[COLUMN_TERMINAL_ID] = TerminalId;
        _session[COLUMN_SESSION_ID] = SessionId;
        _session[COLUMN_SEQUENCE_ID] = SequenceId;
        _session[COLUMN_TRANSACTION_ID] = TransactionId;
        _session[COLUMN_PROVIDER_ID] = ProviderId;
        _session[COLUMN_TERMINAL_NAME] = Name;
        _session[COLUMN_TERMINAL_TYPE] = TerminalType;
        _session[COLUMN_STATUS] = m_opened;
        _session[COLUMN_NEW_SESSION] = 1;
        m_dt_sessions.Rows.Add(_session);

        // AcceptChanges clear RowState, then set row as Modified.
        _session.AcceptChanges();
        _session.SetModified();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: BU.SessionMsgReceived.AddSession Terminal: " + TerminalId + ", Session: " + SessionId + ".");

        Log.Exception(_ex);
        
        return false;
      }
      finally 
      {
        Unlock();
      }
    } // AddSession

    //------------------------------------------------------------------------------
    // PURPOSE : Get data from a Terminal/Session of the DataTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32  TerminalId
    //          - Int64  SessionId
    //
    //      - OUTPUT :
    //          - String ProviderId
    //          - String Name
    //
    // RETURNS :
    //      - Boolean: If data has been retrieved.
    //
    public static Boolean GetData(Int32 TerminalId,
                                  Int64 SessionId,
                                  out String ProviderId,
                                  out String Name)
    {
      TerminalTypes _dummy;

      return GetData(TerminalId, SessionId, out ProviderId, out Name, out _dummy);
    }

    public static Boolean GetData(Int32 TerminalId,
                                  Int64 SessionId,
                                  out String ProviderId,
                                  out String Name,
                                  out TerminalTypes TerminalType)
    {
      String _filter;
      DataRow[] _dr_sessions;
      DataRow _dr_session;

      ProviderId = "";
      Name = "";
      TerminalType = TerminalTypes.UNKNOWN;

      m_rw_lock.AcquireReaderLock(Timeout.Infinite);

      try
      {
        if (m_dt_sessions == null)
        {
          Log.Error("GetData: BU_SessionMsgReceived not initialized. Call Init first!");
          return false;
        }

        // Seek TerminalId with Opened SessionId.
        _filter = "TERMINAL_ID = " + TerminalId + " AND SESSION_ID = " + SessionId + " AND STATUS = " + m_opened;
        _dr_sessions = m_dt_sessions.Select(_filter);

        if (_dr_sessions.Length == 0)
        {
          Log.Error("GetData: Session not found. Terminal: " + TerminalId + ", Session: " + SessionId + ".");
          return false;
        }
        if (_dr_sessions.Length > 1)
        {
          Log.Warning("GetData: Too many sessions for Terminal: " + TerminalId + ". Count: " + _dr_sessions.Length + ".");
        }

        _dr_session = _dr_sessions[0];

        ProviderId = (String)_dr_session[COLUMN_PROVIDER_ID];
        Name = (String)_dr_session[COLUMN_TERMINAL_NAME];
        TerminalType = (TerminalTypes)((Int16)_dr_session[COLUMN_TERMINAL_TYPE]);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: BU_SessionMsgReceived.GetData. Terminal: " + TerminalId + ", Session: " + SessionId + ".");

        Log.Exception(_ex);

        return false;
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }
    } // GetData

    //------------------------------------------------------------------------------
    // PURPOSE : Is (WCP/WC2) session active?
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 SessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If active.
    //
    public static Boolean IsActive(Int64 SessionId)
    {
      String _filter;
      DataRow[] _dr_sessions;

      m_rw_lock.AcquireReaderLock(Timeout.Infinite);

      try
      {
        if (m_dt_sessions == null)
        {
          Log.Error("IsActive: BU_SessionMsgReceived not initialized. Call Init first!");
          return false;
        }

        // Seek TerminalId with Opened SessionId.
        _filter = "SESSION_ID = " + SessionId + " AND STATUS = " + m_opened;
        _dr_sessions = m_dt_sessions.Select(_filter);

        return (_dr_sessions.Length > 0);
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: BU_SessionMsgReceived.IsActive. Session: " + SessionId + ".");
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }

    } // IsActive

    //------------------------------------------------------------------------------
    // PURPOSE : Modify a existing session (marking it as opened) for the terminal in DataTable,
    //           with the SequenceId parameter.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 TerminalId
    //          - Int64 SessionId
    //          - Int64 SequenceId
    //          - Int64 TransactionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If session has been modified correctly.
    //
    public static Boolean SessionMessageReceived(Int32 TerminalId, Int64 SessionId, Int64 SequenceId, Int64 TransactionId)
    {
      return SessionMessageReceived(TerminalId, SessionId, SequenceId, TransactionId, m_opened);
    } // SessionMessageReceived

    //------------------------------------------------------------------------------
    // PURPOSE : Close a session as Status.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 TerminalId
    //          - Int64 SessionId
    //          - Int32 Status
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If session has been deleted successfully.
    //
    public static Boolean CloseSession(Int32 TerminalId, Int64 SessionId, Int32 Status)
    {
      return SessionMessageReceived(TerminalId, SessionId, 0, 0, Status);
    } // CloseSession

    //------------------------------------------------------------------------------
    // PURPOSE : Modify DB according to the Cache in memory (DataTable).
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If Cache has been writen to DB correctly.
    //
    public static Boolean Update(SqlTransaction Trx)
    {
      DataRow[] _dr_modified;
      DataRow[] _dr_new_session;
      Boolean _error;
      SqlCommand _sql_cmd;
      DataRow[] _dr_error;
      Lock ();
      
      try
      {
        if (m_dt_sessions == null)
        {
          Log.Error("Update: BU_SessionMsgReceived not initialized. Call Init first!");
          return false;
        }

        // If no rows to update, return ok.
        if (m_dt_sessions.Rows.Count == 0)
        {
          return true;
        }

        _dr_modified = m_dt_sessions.Select("", "", DataViewRowState.ModifiedCurrent);
        // Really, no rows to update... Return ok.
        if (_dr_modified.Length <= 0)
        {
          return true;        
        }

        m_dt_sessions_copy.Clear();
        foreach (DataRow _r in _dr_modified)
        {
          m_dt_sessions_copy.ImportRow(_r);
          // Delete not opened sessions.
          if ((Int32)_r[COLUMN_STATUS] != m_opened)
          {
            _r.Delete();
          }
        }

        // Mark as not modified
        m_dt_sessions.AcceptChanges();
      }
      finally
      {
        Unlock();
      }

      _error = true;
      try
      {
        m_sql_adap.UpdateCommand.Connection = Trx.Connection;
        m_sql_adap.UpdateCommand.Transaction = Trx;

        // Update Modified rows to DB -- Time consuming
        if (m_sql_adap.Update(m_dt_sessions_copy) == m_dt_sessions_copy.Rows.Count)
        {
          _error = false;
        }
        else
        {
          DataRow[] _rows_with_errors;

          _rows_with_errors = m_dt_sessions_copy.GetErrors();

          if (_rows_with_errors.Length > 0)
          {
            Lock ();
            try
            {
              foreach (DataRow _row in _rows_with_errors)
              {
                _dr_error = m_dt_sessions.Select("SESSION_ID = " + _row[COLUMN_SESSION_ID]);
                if (_dr_error.Length > 0)
                {
                  _dr_error[0].Delete();
                }
              }
            }
            finally
            {
              m_dt_sessions.AcceptChanges();
              Unlock ();
            }
          }
        }

        // ACC 27-OCT-2011: Add Alarms WCP & WC2 Connected and Disconected
        if (m_protocol == "WCP" || m_protocol == "WC2" || m_protocol == "WWP")
        {
          AlarmCode _alarm_code;
          String _alarm_description;
          AlarmSourceCode _alarm_source_code;

          // Loop 1st for Not Connected
          // - These two loops can't be merged in a single one, first disconnections are registered then the connecions
          foreach (DataRow _row in m_dt_sessions_copy.Rows)
          {
            if ((Int32)_row[COLUMN_STATUS] != m_opened)
            {
              // Alarm Disconnected.
              _alarm_source_code = (m_protocol == "WWP" ? AlarmSourceCode.Service : AlarmSourceCode.TerminalSystem);
              _alarm_code = (m_protocol == "WCP" ? AlarmCode.TerminalSystem_WcpDisconnected : AlarmCode.TerminalSystem_Wc2Disconnected);
              _alarm_code = (m_protocol == "WWP" ? AlarmCode.TerminalSystem_WwpSiteDisconnected : _alarm_code);
              _alarm_description = Resource.String(Alarm.ResourceId(_alarm_code), Environment.MachineName);

              Alarm.Register(_alarm_source_code,
                             (Int32)_row[COLUMN_TERMINAL_ID],
                             Misc.ConcatProviderAndTerminal((String)_row[COLUMN_PROVIDER_ID], (String)_row[COLUMN_TERMINAL_NAME]),
                             _alarm_code,
                             _alarm_description);
            }
          }

          // Loop 2nd for Newly Connected
          // - These two loops can't be merged in a single one, first disconnections are registered then the connecions
          foreach (DataRow _row in m_dt_sessions_copy.Rows)
          {
            if ((Int32)_row[COLUMN_STATUS] == m_opened)
            {
              if ((Int16)_row[COLUMN_NEW_SESSION] == 1)
              {
                // Alarm Connected
                _alarm_source_code = (m_protocol == "WWP" ? AlarmSourceCode.Service : AlarmSourceCode.TerminalSystem);
                _alarm_code = (m_protocol == "WCP" ? AlarmCode.TerminalSystem_WcpConnected : AlarmCode.TerminalSystem_Wc2Connected);
                _alarm_code = (m_protocol == "WWP" ? AlarmCode.TerminalSystem_WwpSiteConnected : _alarm_code);
                _alarm_description = Resource.String(Alarm.ResourceId(_alarm_code), Environment.MachineName);

                Alarm.Register(_alarm_source_code,
                               (Int32)_row[COLUMN_TERMINAL_ID],
                               Misc.ConcatProviderAndTerminal((String)_row[COLUMN_PROVIDER_ID], (String)_row[COLUMN_TERMINAL_NAME]),
                               _alarm_code,
                               _alarm_description);
              }
            }
          }
        }

        // RCI & AJQ 08-NOV-2010: Update as Abandoned all previous sessions for the terminal
        foreach (DataRow _row in m_dt_sessions_copy.Rows)
        {
          if ((Int16)_row[COLUMN_NEW_SESSION] == 1)
          {
            Lock ();
            try
            {
              _dr_new_session = m_dt_sessions.Select("SESSION_ID = " + _row[COLUMN_SESSION_ID]);
              if (_dr_new_session.Length > 0)
              {
                _dr_new_session[0][COLUMN_NEW_SESSION] = 0;
                _dr_new_session[0].AcceptChanges();
              }
            }
            finally
            {
              Unlock();
            }

            _sql_cmd = new SqlCommand(m_abandoned_update_cmd_txt);
            _sql_cmd.Connection = Trx.Connection;
            _sql_cmd.Transaction = Trx;

            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = m_abandoned;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = (Int32)_row[COLUMN_TERMINAL_ID];
            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = (Int64)_row[COLUMN_SESSION_ID];

            _sql_cmd.ExecuteNonQuery();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: BU_SessionMsgReceived.Update.");
        Log.Exception(_ex);
      }

      // Don't care about not updated rows

      return !_error;
    } // Update

    //------------------------------------------------------------------------------
    // PURPOSE : Acquire the lock for sessions in WRITE mode.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void Lock()
    {
      m_rw_lock.AcquireWriterLock(Timeout.Infinite);
    } // Lock

    //------------------------------------------------------------------------------
    // PURPOSE : Release the WRITE mode lock for sessions.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void Unlock()
    {
      m_rw_lock.ReleaseWriterLock();
    } // Unlock

    //------------------------------------------------------------------------------
    // PURPOSE : Get a copy of the Opened Terminal Sessions.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public static DataTable GetOpenedTerminalSessions()
    {
      DataTable _term_sessions;
      DataRow _terminal;

      if (m_protocol != "WCP")
      {
        // It should also work for WC2 protocol, but for now only WCP is allowed. 
        Log.Error("GetOpenedTerminalSessions: Only implemented for WCP protocol.");

        return null;
      }

      m_rw_lock.AcquireReaderLock(Timeout.Infinite);

      try
      {
        if (m_dt_sessions == null)
        {
          Log.Error("GetOpenedTerminalSessions: BU_SessionMsgReceived not initialized. Call Init first!");

          return null;
        }

        _term_sessions = new DataTable();
        _term_sessions.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));

        foreach (DataRow _row in m_dt_sessions.Rows)
        {
          // Only Gaming Terminals (no site jackpot, no mobile bank)
          // TERMINAL_TYPE = 1 (WIN)
          // TERMINAL_TYPE = 5 (SAS-HOST)
          if ((Int32)_row[COLUMN_STATUS] == m_opened &&
              ((Int16)_row[COLUMN_TERMINAL_TYPE] == (Int16)TerminalTypes.WIN || (Int16)_row[COLUMN_TERMINAL_TYPE] == (Int16)TerminalTypes.SAS_HOST))
          {
            _terminal = _term_sessions.NewRow();
            _terminal[0] = (Int32)_row[COLUMN_TERMINAL_ID];
            _term_sessions.Rows.Add(_terminal);
          }
        }

        // Mark as not modified
        _term_sessions.AcceptChanges();

        return _term_sessions;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }
    } // GetOpenedTerminalSessions

    //------------------------------------------------------------------------------
    // PURPOSE : Get SessionId for the Opened TerminalId indicated.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 TerminalId
    //
    //      - OUTPUT :
    //          - Int64 SessionId
    //
    // RETURNS :
    //      - Boolean.
    //
    public static Boolean GetOpenedTerminalSession(Int32 TerminalId,
                                                   out Int64 SessionId)
    {
      String _filter;
      DataRow[] _dr_sessions;

      SessionId = 0;

      m_rw_lock.AcquireReaderLock(Timeout.Infinite);

      try
      {
        if (m_dt_sessions == null)
        {
          Log.Error("GetOpenedTerminalSession: BU_SessionMsgReceived not initialized. Call Init first!");
          return false;
        }

        // Seek TerminalId with Opened SessionId.
        _filter = "TERMINAL_ID = " + TerminalId + " AND STATUS = " + m_opened;
        _dr_sessions = m_dt_sessions.Select(_filter);

        if (_dr_sessions.Length > 0)
        {
          SessionId = (Int64)_dr_sessions[0][COLUMN_SESSION_ID];

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: BU_SessionMsgReceived.GetOpenedTerminalSession. Terminal: " + TerminalId + ".");
        Log.Exception(_ex);
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }

      return false;
    } // GetOpenedTerminalSession

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Modify a existing session for the terminal in DataTable, with the SequenceId parameter.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 TerminalId
    //          - Int64 SessionId
    //          - Int64 SequenceId
    //          - Int64 TransactionId
    //          - Int32 Status
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: If session has been modified correctly.
    //
    private static Boolean SessionMessageReceived(Int32 TerminalId, Int64 SessionId, Int64 SequenceId, Int64 TransactionId, Int32 Status)
    {
      String _filter;
      DataRow[] _dr_sessions;
      DataRow _dr_session;

      Lock ();

      try
      {
        // Terminal Unenrolled
        if (TerminalId == 0)
        {
          return true;
        }

        if (m_dt_sessions == null)
        {
          Log.Error("SessionMessageReceived: BU_SessionMsgReceived not initialized. Call Init first!");
          return false;
        }

        // Seek TerminalId.
        _filter = "TERMINAL_ID = " + TerminalId + " AND SESSION_ID = " + SessionId;
        _dr_sessions = m_dt_sessions.Select(_filter);

        if (_dr_sessions.Length == 0)
        {
          Log.Warning("SessionMessageReceived: Session not found. Terminal: " + TerminalId + ", Session: " + SessionId + ".");
          return false;
        }
        if (_dr_sessions.Length > 1)
        {
          Log.Warning("SessionMessageReceived: Too many sessions for Terminal: " + TerminalId + ". Count: " + _dr_sessions.Length + ".");
        }

        _dr_session = _dr_sessions[0];

        // If session is not opened, return false. Can't update a closed session.
        if ((Int32)_dr_session[COLUMN_STATUS] != m_opened)
        {
          return false;
        }

        _dr_session.BeginEdit();

        // On any change the row will be set as Modified.
        if (SequenceId > (Int64)_dr_session[COLUMN_SEQUENCE_ID])
        {
          _dr_session[COLUMN_SEQUENCE_ID] = SequenceId;
        }
        if (TransactionId > (Int64)_dr_session[COLUMN_TRANSACTION_ID])
        {
          _dr_session[COLUMN_TRANSACTION_ID] = TransactionId;
        }
        if (Status != m_opened)
        {
          // Set new status when the new one is different from Opened
          _dr_session[COLUMN_STATUS] = Status;
        }

        _dr_session.EndEdit();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception: BU.SessionMsgReceived. Terminal: " + TerminalId + ", Session: " + SessionId + ".");
        Log.Exception(_ex);
        
        return false;
      }
      finally
      {
        Unlock ();
      }
    } // SessionMessageReceived

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for Update to DB every second.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void SessionThread()
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Int64 _elapsed;
      Int32 _last_check;
      Int32 _sleep_ms;

      _sql_conn = null;

      while (true)
      {
        try
        {
          if (_sql_conn == null)
          {
            _sql_conn = WGDB.Connection();
          }

          // Mark all opened sessions from my server as disconnected.
          // - Don't exit until ALL are marked successfuly.
          if (UpdateStatusSessions(m_disconnected, _sql_conn))
          {
            break;
          }

          Thread.Sleep(1000);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          if (_sql_conn != null)
          {
            try { _sql_conn.Close(); }
            catch { }
            try { _sql_conn.Dispose(); }
            catch { }
            _sql_conn = null;
          }
        }
      } // while

      _sql_trx = null;

      _last_check = Misc.GetTickCount();
      _sleep_ms = 0;

      while (true)
      {
        try
        {
          Thread.Sleep(_sleep_ms);
          _sleep_ms = 1000;

          if (_sql_conn == null)
          {
            _sql_conn = WGDB.Connection();
          }

          _sql_trx = _sql_conn.BeginTransaction ();

          TerminalSession.Update (_sql_trx);

          _sql_trx.Commit ();

          _elapsed = Misc.GetElapsedTicks (_last_check);
          if (_elapsed >= TIME_TO_CHECK_IN_MS)
          {
            // Mark sessions from other servers as timeout if last msg received is older than indicated.
            UpdateStatusSessions (m_timeout, _sql_conn);

            _last_check = Misc.GetTickCount();
          }
        }
        catch
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              try { _sql_trx.Rollback (); }
              catch { }
            }
            try { _sql_trx.Dispose(); }
            catch { }
            
            _sql_trx = null;
          }

          if (_sql_conn != null)
          {
            try { _sql_conn.Close (); }
            catch { }
            try { _sql_conn.Dispose(); }
            catch { }
            _sql_conn = null;
          }
        }
      } // while
    } // SessionThread

    //------------------------------------------------------------------------------
    // PURPOSE : Update sessions status to Disconnected or Timeout.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 Status
    //          - SqlConnection SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: All sessions have been updated successfuly.
    //      - False: Otherwise.
    //
    private static Boolean UpdateStatusSessions (Int32 Status, SqlConnection SqlConn)
    {
      SqlTransaction _sql_trx;
      String _select_cmd_txt;
      String _update_cmd_txt;
      SqlCommand _sql_cmd_select;
      SqlCommand _sql_cmd_update;
      SqlDataAdapter _sql_da;
      DataTable _sql_dt_sessions;
      Int32 _num_rows_updated;
      Int32 _num_total_rows;
      Boolean _only_once;

      _only_once = true;

      if (Status == m_disconnected)
      {
        _select_cmd_txt = m_disconnected_select_cmd_txt;
        _update_cmd_txt = m_disconnected_update_cmd_txt;
      }
      else if (Status == m_timeout)
      {
        _select_cmd_txt = m_timeout_select_cmd_txt;
        _update_cmd_txt = m_timeout_update_cmd_txt;
      }
      else
      {
        Log.Error("ChangeSessions: Unknow status: " + Status + ".");

        return false;
      }

      _sql_dt_sessions = new DataTable ("SESSIONS");
      _sql_da = new SqlDataAdapter ();
      _sql_cmd_select = new SqlCommand (_select_cmd_txt);
      _sql_cmd_select.Connection = SqlConn;
      _sql_da.SelectCommand = _sql_cmd_select;

      _sql_cmd_select.Parameters.Add ("@pServerName", SqlDbType.NVarChar).Value  = Environment.MachineName;
      if (Status == m_timeout)
      {
        _sql_cmd_select.Parameters.Add ("@pElapsedMinutes", SqlDbType.Int).Value = TIMEOUT_MINUTES;
      }

      _sql_da.Fill (_sql_dt_sessions);

      _sql_cmd_update = new SqlCommand (_update_cmd_txt);
      _sql_cmd_update.Parameters.Add ("@pSessionId", SqlDbType.BigInt);
      _sql_cmd_update.Parameters.Add ("@pStatus", SqlDbType.Int).Value           = Status;
      _sql_cmd_update.Parameters.Add ("@pServerName", SqlDbType.NVarChar).Value  = Environment.MachineName;
      if (Status == m_timeout)
      {
        _sql_cmd_update.Parameters.Add ("@pElapsedMinutes", SqlDbType.Int).Value = TIMEOUT_MINUTES;
      }

      _num_total_rows = 0;

      foreach ( DataRow _dr_session in _sql_dt_sessions.Rows )
      {
        _sql_trx = null;

        try
        {
          _sql_trx = SqlConn.BeginTransaction ();

          _sql_cmd_update.Connection  = SqlConn;
          _sql_cmd_update.Transaction = _sql_trx;

          _sql_cmd_update.Parameters["@pSessionId"].Value = (Int64)_dr_session[0];

          _num_rows_updated = _sql_cmd_update.ExecuteNonQuery();

          _sql_trx.Commit ();

          _num_total_rows += _num_rows_updated;
        }
        catch ( Exception _ex )
        {
          if (_only_once)
          {
            Log.Exception(_ex);
            _only_once = false;
          }
        }
        finally
        {
          if ( _sql_trx != null )
          {
            if ( _sql_trx.Connection != null )
            {
              try { _sql_trx.Rollback (); }
              catch {}
            }
            _sql_trx.Dispose ();
            _sql_trx = null;
          }
        }
      } // foreach

      if (_sql_dt_sessions.Rows.Count > 0)
      {
        Log.Message("UpdateStatusSessions: Status: " + Status + ", updated " + _num_total_rows + " / " + _sql_dt_sessions.Rows.Count);
      }

      return (_num_total_rows == _sql_dt_sessions.Rows.Count);
    } // UpdateStatusSessions

    #endregion // Private Methods

  } // TerminalSession

} // WSI.Common.BatchUpdate
