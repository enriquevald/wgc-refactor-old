﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Card.cs
// 
//   DESCRIPTION: Class to hold data form Mobile bank & user info
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 13-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-NOV-2017 FGB    First release.
//------------------------------------------------------------------------------

using System;

namespace WSI.Common
{
  public class MobileBankUserInfoData
  {
    #region "Properties"

    public Int32 UserId { get; set; }

    public String UserName { get; set; }

    public String UserHolderName { get; set; }

    public Int64 MobileBankId { get; set; }

    public String MobileBankHolderName { get; set; }

    public String MobileInternalTrackData { get; set; }

    public String MobileExternalTrackData { get; set; }

    public Boolean MobileBlocked { get; set; }

    public String MobilePin { get; set; }

    public Boolean IsVirtualCard { get; set; }
    #endregion
  }
}
