﻿//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ReportToolConfigResponseFactory.cs
// 
//   DESCRIPTION: Create ReportToolConfigResponseFactory object
// 
//        AUTHOR: Ernesto Salazar
// 
// CREATION DATE: 03-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-JUN-2016 ESE    First release.
// 15-SEP-2016 ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
// 17-NOV-2017 LTC    Product Backlog Item 30849:Multisite: Reporte genérico terminales
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WSI.Common.GenericReport.Factories
{
  internal static class ReportToolConfigResponseFactory
  {
    //Create a ReportToolConfigResponseFactory object
    public static ReportToolConfigResponse Create(ReportToolConfigRequest request, List<ReportToolConfigDTO> _list_reports, Boolean IsMultiSite = false)
    {
      ReportToolConfigResponse _response;
      List<ReportToolConfigDTO> _list_response;

      _response = new ReportToolConfigResponse();
      _list_response = new List<ReportToolConfigDTO>();

      try
      {
        //Scan the _list_reports in order to chek the LocationMenu
        foreach (var _aux_row in _list_reports)
        {
          //If is the same location then add the item to the response
          if (IsMultiSite)
          {
            if (_aux_row.LocationMenuMultiSite == request.LocationMenuMultiSite)
            {
          _list_response.Add(_aux_row);
        }
          }
          else
          {
            if (_aux_row.LocationMenu == request.LocationMenu)
            {
              _list_response.Add(_aux_row);
            }
          }
        }

        //All the process was 'OK'
        _response.ReportToolConfigs = _list_response;
        _response.Code = 0;
        _response.Message = "ok";
        _response.Success = true;
      }
      catch (Exception ex)
      {
        //An exception ocurrs, set the message
        _response.ReportToolConfigs = _list_response;
        _response.Code = -1;
        _response.Message = ex.Message;
        _response.Success = false;

        Log.Exception(ex);
      }

      return _response;
    }

    internal static ReportToolConfigResponse Create(DataTable dataTable)
    {
      ReportToolConfigResponse _response;
      IList<ReportToolConfigDTO> _list;

      _list = ReportToolConfigDTOFactory.Create(dataTable);
      _response = new ReportToolConfigResponse();

      _response.ReportToolConfigs = _list;
      _response.Code = 1;
      _response.Message = "Transaction complete!";
      _response.Success = true ;

      return _response;
    }

    internal static ReportToolConfigResponse Create(int code, bool success, string message)
    {
      ReportToolConfigResponse _response;

      _response = new ReportToolConfigResponse();
      _response.ReportToolConfigs = new List<ReportToolConfigDTO>();
      _response.Code = code;
      _response.Message = message;
      _response.Success = success;

      return _response;
    }
  }
}
