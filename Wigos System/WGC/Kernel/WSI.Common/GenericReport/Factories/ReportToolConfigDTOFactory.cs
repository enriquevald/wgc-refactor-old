﻿//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ReportToolConfigDTOFactory.cs
// 
//   DESCRIPTION: Create ReportToolConfigDTO objects
// 
//        AUTHOR: Ernesto Salazar
// 
// CREATION DATE: 03-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-JUN-2016 ESE    First release.
// 30-JUN-2016 RGR    Add multilanguage in report name and sheet name
// 15-SEP-2016  ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
// 24-NOV-2016  EOR    PBI 19870: R.Franco: New report machines and games
// 28-SEP-2017  RGR    Bug 29898:WIGOS-4921 Accounting - Generic report Monthly report: the design is not the same than the story and the footer signings are missing
// 17-NOV-2017  LTC    Product Backlog Item 30849:Multisite: Reporte genérico terminales
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WSI.Common.Utilities;
using System.Xml;

namespace WSI.Common.GenericReport.Factories
{
  internal static class ReportToolConfigDTOFactory
  {
    //Create a List<ReportToolConfigDTO> from a DataTable    
    public static List<ReportToolConfigDTO> Create(DataTable dtReportTool, Boolean IsMultiSite = false)
    {
      List<ReportToolConfigDTO> _response;
      ReportToolConfigDTO _aux_obj_report;

      _response = new List<ReportToolConfigDTO>();

      try
      {
        //Scan the datatable and turn into a ReportToolConfigDTO object
        foreach (DataRow _aux_row in dtReportTool.Rows)
        {
          //Create an aux object
          _aux_obj_report = new ReportToolConfigDTO();

          //Set the members
          _aux_obj_report.ReportID = Int32.Parse(_aux_row["RTC_REPORT_TOOL_ID"].ToString());
          _aux_obj_report.FormID = Int32.Parse(_aux_row["RTC_FORM_ID"].ToString());
          _aux_obj_report.LocationMenu = (LocationMenu)Int32.Parse(_aux_row["RTC_LOCATION_MENU"].ToString());

          if (IsMultiSite)
          {
            _aux_obj_report.LocationMenuMultiSite = (LocationMenuMultiSite)Int32.Parse(_aux_row["RTC_LOCATION_MENU"].ToString());
          }
          else
          {
            _aux_obj_report.LocationMenu = (LocationMenu)Int32.Parse(_aux_row["RTC_LOCATION_MENU"].ToString());
          }
          
          _aux_obj_report.StoreName = _aux_row["RTC_STORE_NAME"].ToString();
          _aux_obj_report.Filters = XmlSerializationHelper.Deserialize<ReportToolDesignFilterDTO>(_aux_row["RTC_DESIGN_FILTER"].ToString());
          _aux_obj_report.Sheets = XmlSerializationHelper.Deserialize<ReportToolDesignSheetsDTO[]>(_aux_row["RTC_DESIGN_SHEETS"].ToString());
          _aux_obj_report.Language = XmlSerializationHelper.Deserialize<LanguageResources>(_aux_row["RTC_REPORT_NAME"].ToString());
          _aux_obj_report.ModeType = (ENUM_FEATURES)_aux_row["RTC_MODE_TYPE"];
          _aux_obj_report.IsExecute = (bool)_aux_row["RTC_IS_EXECUTE"];
          _aux_obj_report.Header = _aux_row["RTC_HTML_HEADER"] == null ? string.Empty : _aux_row["RTC_HTML_HEADER"].ToString();
          _aux_obj_report.Footer = _aux_row["RTC_HTML_FOOTER"] == null ? string.Empty : _aux_row["RTC_HTML_FOOTER"].ToString();
          //Add a new object into de list _response
          _response.Add(_aux_obj_report);
        }
      }
      catch (Exception ex)
      {

        Log.Exception(ex);
      }

      return _response;
    }
  }
}
