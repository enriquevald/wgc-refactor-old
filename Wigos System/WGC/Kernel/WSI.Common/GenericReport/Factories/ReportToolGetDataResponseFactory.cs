﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GenericReport
// 
//   DESCRIPTION: Factory create objects ReportToolGetDataResponse
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 08-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-JUN-2016 RGR    Initial Version
// 30-JUN-2016 RGR    Add multilanguage in report name and sheet name
// 11-JUL-2016 RGR    Add multilanguage in columns name
// 24-NOV-2016 LTC    PBI 19870:R.Franco: New report Machines and Gaming Tables
// 28-SEP-2017 RGR    Bug 29898:WIGOS-4921 Accounting - Generic report Monthly report: the design is not the same than the story and the footer signings are missing
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WSI.Common.GenericReport.Factories
{
  internal class ReportToolGetDataResponseFactory
  {
    internal static ReportToolGetDataResponse Create(GetDataBaseReportRequest request, DataSet dts)
    {
      ReportToolGetDataResponse _response;
      ReportToolDesignSheetsDataDTO _sheetData;

      _response = new ReportToolGetDataResponse();
      _response.MetaDatas = new List<ReportToolDesignSheetsDataDTO>();

      if (request.Report.Sheets == null || dts.Tables.Count != request.Report.Sheets.Length)
      {
        throw new System.IndexOutOfRangeException("The number of sheets is different to the result of the store");
      }

      // LTC 24-NOV-2016
      for (int i = 0; i < dts.Tables.Count; i++)
      {
        _sheetData = new ReportToolDesignSheetsDataDTO();
        _sheetData.DataReport = dts.Tables[i];
        SetLanguageColumns(request.Report.Sheets[i].Columns, _sheetData.DataReport);
        _sheetData.DataReport.TableName = request.Report.Sheets[i].SheetName;
        _sheetData.Columns = request.Report.Sheets[i].Columns;
        _sheetData.LanguageResources = request.Report.Sheets[i].LanguageResources;           

        if (dts.Tables[i].Columns.Count == 1 && dts.Tables[i].Rows.Count == 0)
        {
          continue;
        }

        _response.MetaDatas.Add(_sheetData);
      }
      
      // LTC 24-NOV-2016
      List<DataTable> _ListDtToRemove;
      foreach (ReportToolDesignSheetsDataDTO _dsg in _response.MetaDatas)
      {
        _ListDtToRemove = new List<DataTable>();
        foreach (DataTable _dt in _dsg.DataReport.DataSet.Tables)
        {
          if (_dt.Columns.Count == 1 && _dt.Rows.Count == 0)
          {
            _ListDtToRemove.Add(_dt);
          }
        }

        foreach (DataTable _dt in _ListDtToRemove)
        {
          _dsg.DataReport.DataSet.Tables.Remove(_dt);
        }
      }
            
      _response.Code = 1;
      _response.Success = true;
      _response.Message = "It was successfully obtained information";

      return _response;
    }

    internal static ReportToolGetDataResponse Create()
    {
      ReportToolGetDataResponse _response;

      // LTC 24-NOV-2016
      _response = new ReportToolGetDataResponse();
      _response.Code = 7763;
      _response.Success = false;
      _response.MetaDatas = null;
      _response.Message = "";

      return _response;
    }

    // LTC 24-NOV-2016
    internal static ReportToolGetDataResponse Create(String Message)
    {
      ReportToolGetDataResponse _response;

      _response = new ReportToolGetDataResponse();
      _response.Code = 7764;
      _response.Success = false;
      _response.MetaDatas = null;
      _response.Message = Message;

      return _response;
    }

    private static void SetLanguageColumns(ReportToolDesignColumn[] columns, DataTable dt)
    {
      if (columns == null)
      {
        return;
      }

      foreach (DataColumn item in dt.Columns)
      {
        item.ColumnName = SearchLabel(item.ColumnName, columns);
      }
    }

    private static string SearchLabel(string code, ReportToolDesignColumn[] columns)
    {
      string _label;
      bool _match;

      _label = code;
      _match = false;

      foreach (ReportToolDesignColumn column in columns)
      {
        if (column.LanguageResources == null)
        {
          continue;
        }

        if (column.LanguageResources.NLS09 == null || column.LanguageResources.NLS10 == null)
        {
          continue;
        }
        switch (column.EquityMatchType)
        {
          case EquityMatchType.Contains:
            if (code.Contains(column.Code))
            {
              _label = _label.Replace(column.Code, column.LanguageResources.GetLabel());
              _match = true;
            }
            break;
          case EquityMatchType.Equality:
          default:
            if (code.ToLower() == column.Code.ToLower())
            {
              _label = column.LanguageResources.GetLabel();
              _match = true;
            }
            break;
        }
        if (_match)
        {
          _match = false;
          break;
        }
      }
      return _label;
    }
  }
}
