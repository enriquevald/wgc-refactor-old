﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ToolPDF
// 
//   DESCRIPTION: Create pdf file in base generic report
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 18-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-SEP-2017 RGR    Initial Version
// ----------- ------ ----------------------------------------------------------

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace WSI.Common.GenericReport
{
  internal static class ToolPDF
  {
    #region Public Methods

    public static bool Create(CreatePDFRequest requestPDF)
    {
      StringBuilder _html;
      MemoryStream _htmlStream;
      MemoryStream _cssStream;

      _html = new StringBuilder();
      using (Document _doc = new Document(iTextSharp.text.PageSize.LETTER))
      {
        var _writer = PdfWriter.GetInstance(_doc,
            new FileStream(requestPDF.FileName, FileMode.Create));
        _doc.Open();

        _html.Append(HTMLReplace(requestPDF.Header, requestPDF));
        foreach (var _item in requestPDF.Sheets)
        {

          _html.Append(ToHTML(_item.DataReport));

        }
        _html.Append(HTMLReplace(requestPDF.Footer, requestPDF));

        _htmlStream = new MemoryStream(Encoding.UTF8.GetBytes(_html.ToString()));
        _cssStream = new MemoryStream(Encoding.UTF8.GetBytes(GetCSS()));

        XMLWorkerHelper.GetInstance().ParseXHtml(_writer, _doc, _htmlStream, _cssStream);

        _htmlStream.Dispose();
        _cssStream.Dispose();
      }

      return true;
    }

    #endregion

    #region Private Methods

    private static string ToHTML(DataTable data)
    {
      StringBuilder _html;

      _html = new StringBuilder();
      _html.Append("<div>                                                                      ");
      _html.Append("  <table>                                                                  ");
      _html.Append("    <tr>                                                                   ");

      foreach (DataColumn _column in data.Columns)
      {
        _html.Append(string.Format("      <th>{0}</th>              ", _column.ColumnName));
      }

      _html.Append("    </tr>                                                                  ");

      foreach (DataRow _row in data.Rows)
      {
        _html.Append("    <tr>                                                                 ");

        foreach (DataColumn _column in data.Columns)
        {
          _html.Append(GetFormat(_row, _column));
        }

        _html.Append("    </tr>                                                                ");
      }


      _html.Append("  </table>                                                                 ");
      _html.Append("</div>                                                                     ");


      return _html.ToString();
    }

    private static string GetFormat(DataRow row, DataColumn column)
    {
      string _strRow;

      _strRow = string.Empty;


      switch (Type.GetTypeCode(column.DataType))
      {
        case TypeCode.Int16:
        case TypeCode.Int32:
        case TypeCode.Int64:
          long _long;

          if (row[column] == null)
          {
            _long = 0;
          }

          if (!long.TryParse(row[column].ToString(), out _long))
          {
            _long = 0;
          }

          _strRow = string.Format("<td class='td-right'>{0}</td>", _long.ToString("0"));
          break;
        case TypeCode.Decimal:
          decimal _dec;

          if (row[column] == null)
          {
            _dec = 0;
          }

          if (!decimal.TryParse(row[column].ToString(), out  _dec))
          {
            _dec = 0;
          }

          if (_dec == 0)
          {
            _strRow = string.Format("<td class='td-right'>{0}</td>", _dec.ToString("0"));
          }
          else if (Math.Abs(_dec) < 100)
          {
            _strRow = string.Format("<td class='td-right'>{0}</td>", _dec.ToString("0.00"));
          }
          else
          {
            _strRow = string.Format("<td class='td-right'>{0}</td>", _dec.ToString("0,0.00"));
          }
          
          break;
        case TypeCode.Double:
          double _dou;

          if (row[column] == null)
          {
            _dou = 0;
          }

          if (!double.TryParse(row[column].ToString(), out  _dou))
          {
            _dou = 0;
          }

          if (_dou == 0)
          {
            _strRow = string.Format("<td class='td-right'>{0}</td>", _dou.ToString("0"));
          }
          else if (Math.Abs(_dou) < 100)
          {
            _strRow = string.Format("<td class='td-right'>{0}</td>", _dou.ToString("0.00"));
          }
          else
          {
            _strRow = string.Format("<td class='td-right'>{0}</td>", _dou.ToString("0,0.00"));
          }

          break;
        default:
          if (row[column] == null)
          {
            _strRow = string.Empty;
          }
          else
          {
            _strRow = row[column].ToString();
          }

          _strRow = string.Format("<td class='td-center'>{0}</td>", _strRow);
          break;
      }

      return _strRow;
    }

    private static string GetCSS()
    {
      string _css = @"
      <style type='text/css'>
          html {
            font-family:Lucida Sans Unicode;
           
          }         

          table {
            border-collapse: separate;
            border-spacing: 2px;
            width: 100%;
            border:none;           
          }

          th {
            border: 1px solid #d9d6d6;
            text-align :center;      
            font-weight:bold; 
            font-size:8px;
            padding: 5px;
            background-color :#d9d6d6;
          }

         td {                        
            font-size:8px;
            padding: 5px;
          }

          .td-right{
            text-align: right;
            border-bottom: 1px solid #ddd; 
          }

          .td-center{
            text-align: center;
            border-bottom: 1px solid #ddd; 
          }

          .td-left{
            text-align: left;
            border-bottom: 1px solid #ddd; 
          }

      </style>
      ";

      return _css;
    }

    private static MemoryStream GetMemoryStream(string uiHTML)
    {
      return new MemoryStream(Encoding.UTF8.GetBytes(uiHTML));
    }

    private static string HTMLReplace(string source, CreatePDFRequest request)
    {
      string _result;

      _result = source;

      if (request.Filters.ShowPrint && request.Filters.Variables != null)
      {
        foreach (var _item in request.Filters.Variables)
        {
          switch (_item.Type)
          {
            case VariableType.GeneralParam:
              _result = _result.Replace(_item.Name, GetGPValue(_item.MetaDataOne, _item.MetaDataTwo));
              break;
            case VariableType.CurrentUser:
              _result = _result.Replace(_item.Name, GetCurrentUserValue(_item.MetaDataOne, request));
              break;
            case VariableType.Parameter:
              _result = _result.Replace(_item.Name, GetParameterValue(_item.MetaDataOne, _item.MetaDataTwo, request));
              break;
          }
        }
      }

      return _result;
    }

    private static string GetGPValue(string group, string subject)
    {
      return GeneralParam.GetString(group, subject , string.Empty);
    }

    private static string GetCurrentUserValue(string property, CreatePDFRequest request)
    {
      string _result;

      _result = string.Empty;
      switch (property.ToLower())
      {
        case "username":
          _result = request.UserName;
          break;
        case "userfullname":
          _result = request.UserFullName;
          break;
      }

      return _result;
    }

    private static string GetParameterValue(string nameParameter, string format, CreatePDFRequest request)
    {
      string _result;
      ReportToolCustomFilter _filter;
      SqlParameter _param;

      _result = string.Empty;
      _param = null;
      _filter = (ReportToolCustomFilter)request.Filters;
      foreach (var _item in _filter.dbParameters)
      {
        if (_item.ParameterName == nameParameter)
        {
          _param = _item;
          break;
        }
      }

      if (_param == null || _param.Value == null)
      {
        return _result;
      }

      switch (Type.GetTypeCode(_param.Value.GetType()))
      {
        case TypeCode.Int16:
        case TypeCode.Int32:
        case TypeCode.Int64:
          long _long;

          if (!long.TryParse(_param.Value.ToString(), out _long))
          {
            break;
          }

          if (!string.IsNullOrEmpty(format))
          {
            _result = _long.ToString(format);
          }

          break;

        case TypeCode.Decimal:
         decimal _dec;

         if (!decimal.TryParse(_param.Value.ToString(), out _dec))
          {
            break;
          }

          if (!string.IsNullOrEmpty(format))
          {
            _result = _dec.ToString(format);
          }

          break;

        case TypeCode.Double:
           double _dou;

         if (!double.TryParse(_param.Value.ToString(), out _dou))
          {
            break;
          }

          if (!string.IsNullOrEmpty(format))
          {
            _result = _dou.ToString(format);
          }

          break;

        case TypeCode.String:
          _result = _param.Value.ToString();
          
          break;

        case TypeCode.DateTime:
          DateTime _dat;

          if (!DateTime.TryParse(_param.Value.ToString(), out _dat))
          {
            break;
          }

          if (!string.IsNullOrEmpty(format))
          {
            _result = _dat.ToString(format);
          }

          break;

        case TypeCode.Boolean:
          Boolean _bol;
         
          if (!Boolean.TryParse(_param.Value.ToString(), out _bol))
          {
            break;
          }

          if (_bol)
          {
            _result = Boolean.TrueString;
          }
          else
          {
            _result = Boolean.FalseString;
          }

          break;
      }

      return _result;
    }

    #endregion

  }
}
