﻿//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ReportToolConfigService.cs
// 
//   DESCRIPTION: ReportToolConfigService class, implements IReportToolConfigService interface
// 
//        AUTHOR: Ernesto Salazar
// 
// CREATION DATE: 03-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-JUN-2016 ESE    First release.
// 25-AGO-2016 ESE    Product Backlog Item 13581:Show all the reports, not only those whit permission
// 15-SEP-2016 ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
// 17-NOV-2016 EOR    Fixed Bug 18253: Multisite: Log Exception acess to Gui
// 08-DIC-2016 ESE    Bug 21294:WigosGUI: Hide generic reports to master profile
// 28-SEP-2017 RGR    Bug 29898:WIGOS-4921 Accounting - Generic report Monthly report: the design is not the same than the story and the footer signings are missing
// 17-NOV-2017  LTC    Product Backlog Item 30849:Multisite: Reporte genérico terminales
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.GenericReport.Factories;

namespace WSI.Common.GenericReport
{
  public class ReportToolConfigService : IReportToolConfigService
  {
    #region Members
    private DataTable _dt_res;
    private DB_TRX _connection;
    private List<ReportToolConfigDTO> _list_reports;
    private ConstructorRequest _request;

    public List<ReportToolConfigDTO> List_reports
    {
      get { return _list_reports; }
    }

    #endregion

    #region Constructor

    public ReportToolConfigService()
    {
      _connection = new DB_TRX();
    }

    public ReportToolConfigService(ConstructorRequest request)
    {
      SqlCommand _cmd;

      _request = request;
      _connection = new DB_TRX();
      _dt_res = new DataTable();

      try
      {
        using (_cmd = this.GetAllSqlCommand(request))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_dt_res);
          }
        }

      }
      catch (Exception Ex)
      {
        Log.Error(Ex.Message); //EOR 17-NOV-2016
      }
      
      _list_reports = Factories.ReportToolConfigDTOFactory.Create(_dt_res, Misc.IsMultisiteCenter());
    }

    #endregion

    #region IReportToolConfigService

    //Scan the List<ReportToolConfigDTO> to find the enum LocationMenu in the parameter
    public bool ExistLocationMenu(LocationMenu location)
    {
      if (_request == null)
      {
        return false;
      }

      foreach (var item in _list_reports)
      {
        if (item.LocationMenu == location)
          return true;
      }

      return false;
    }

    //Scan the List<ReportToolConfigDTO> to find the enum LocationMenuMultiSite in the parameter
    public bool ExistLocationMenu(LocationMenuMultiSite location)
    {
      if (_request == null)
      {
        return false;
      }

      foreach (var item in _list_reports)
      {
        if (item.LocationMenuMultiSite == location)
          return true;
      }

      return false;
    }

    //Scan the List<ReportToolConfigDTO> to find the enum LocationMenu in the parameter
    public bool GetChildReportsPermissions(LocationMenu location)
    {
      if (_request == null)
      {
        return false;
      }

      foreach (var item in _list_reports)
      {
        if (item.LocationMenu == location)
        {
          if (item.IsExecute)
          {
            return true;
          }
        }

      }
      return false;
    }

    //Scan the List<ReportToolConfigDTO> to find the enum LocationMenuMultiSite in the parameter
    public bool GetChildReportsPermissions(LocationMenuMultiSite location)
    {
      if (_request == null)
      {
        return false;
      }

      foreach (var item in _list_reports)
      {
        if (item.LocationMenuMultiSite == location)
        {
          if (item.IsExecute)
          {
            return true;
          }
        }

      }
      return false;
    }

    //Build ReportToolConfigResponse object
    public ReportToolConfigResponse GetMenuConfiguration(ReportToolConfigRequest request)
    {

      return ReportToolConfigResponseFactory.Create(request, _list_reports, Misc.IsMultisiteCenter());
    }

    public ReportToolConfigResponse GetReportToolConfig(int reportToolConfigID)
    {
      ReportToolConfigResponse _response;
      StringBuilder _sb;

      _dt_res = new DataTable();
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" SELECT RTC_REPORT_TOOL_ID, RTC_FORM_ID, RTC_LOCATION_MENU, RTC_REPORT_NAME, RTC_STORE_NAME, RTC_DESIGN_FILTER, RTC_DESIGN_SHEETS, RTC_MODE_TYPE, CAST(1 AS BIT) AS RTC_IS_EXECUTE, RTC_HTML_HEADER, RTC_HTML_FOOTER ");
        _sb.AppendLine(" FROM REPORT_TOOL_CONFIG ");
        _sb.AppendLine(" WHERE RTC_REPORT_TOOL_ID = @pReportToolID");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _connection.SqlTransaction.Connection, _connection.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pReportToolID", SqlDbType.Int).Value = reportToolConfigID;
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_dt_res);
          }
        }

        _response = ReportToolConfigResponseFactory.Create(_dt_res);
      }
      catch (Exception _ex)
      {
        _list_reports = new List<ReportToolConfigDTO>();
        _response = ReportToolConfigResponseFactory.Create(-1, false, _ex.Message);
        Log.Exception(_ex);
      }
      return _response;
    }

    #endregion

    #region IDisposable
    //Close conection
    public void Dispose()
    {
      if (this._connection != null)
      {
        this._connection.Dispose();
        this._connection = null;
      }
      if (this._dt_res != null)
      {
        this._dt_res.Dispose();
      }
      if (this._list_reports != null)
      {
        this._list_reports.Clear();
      }
    }
    #endregion

    #region Private Methods

    private SqlCommand GetAllSqlCommand(ConstructorRequest request)
    {
      StringBuilder _sb;
      SqlCommand _cmd;

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT                                                         ");
      _sb.AppendLine("    RTC_REPORT_TOOL_ID                                         ");
      _sb.AppendLine("   ,RTC_FORM_ID                                                ");
      _sb.AppendLine("   ,RTC_LOCATION_MENU                                          ");
      _sb.AppendLine("   ,RTC_REPORT_NAME                                            ");
      _sb.AppendLine("   ,RTC_STORE_NAME                                             ");
      _sb.AppendLine("   ,RTC_DESIGN_FILTER                                          ");
      _sb.AppendLine("   ,RTC_DESIGN_SHEETS                                          ");
      _sb.AppendLine("   ,RTC_MODE_TYPE                                              ");
      _sb.AppendLine("   ,RTC_HTML_HEADER                                            ");
      _sb.AppendLine("   ,RTC_HTML_FOOTER                                            ");

      if (request.ShowAll)
      {
        _sb.AppendLine(" ,CAST(1 AS BIT) AS RTC_IS_EXECUTE                           ");
      }
      else
      {
        _sb.AppendLine(" ,CAST(ISNULL(GPF_EXECUTE_PERM, 0) AS BIT) AS RTC_IS_EXECUTE ");
      }

      _sb.AppendLine("FROM REPORT_TOOL_CONFIG                                        ");

      if (!request.ShowAll)
      {
        _sb.AppendLine("LEFT JOIN GUI_PROFILE_FORMS ON RTC_FORM_ID = GPF_FORM_ID     ");
      }

      _sb.AppendLine("WHERE RTC_STATUS = @pStatus                                    ");

      if (!request.ShowAll)
      {
        _sb.AppendLine("AND GPF_READ_PERM = @pStatus                                   ");
      }
      if (_request != null && _request.ModeType != null && _request.ModeType.Count != 0)
      {
        _sb.AppendLine("      AND RTC_MODE_TYPE IN (" + EnumFeaturesToString(request.ModeType) + ")          ");
      }

      if (!request.ShowAll)
      {
        _sb.AppendLine("AND GPF_PROFILE_ID = @pProfileID                             ");
      }

      if (request.OnlyReportsMailing)
      {
        _sb.AppendLine("AND RTC_MAILING = @pOnlyMailing                              ");
      }

      if (request.OnlyReportsMailing && !request.ShowAll)
      {
        _sb.AppendLine("AND GPF_EXECUTE_PERM = @pExecutePerm                         ");
      }

      _cmd = new SqlCommand(_sb.ToString(), this._connection.SqlTransaction.Connection, this._connection.SqlTransaction);
      _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = 1;

      if (!request.ShowAll)
      {
        _cmd.Parameters.Add("@pProfileID ", SqlDbType.Int).Value = request.ProfileID;
      }

      if (request.OnlyReportsMailing)
      {
        _cmd.Parameters.Add("@pOnlyMailing", SqlDbType.Int).Value = 1;

      }

      if (request.OnlyReportsMailing && !request.ShowAll)
      {
        _cmd.Parameters.Add("@pExecutePerm", SqlDbType.Int).Value = true;
      }

      return _cmd;
    }

    private String EnumFeaturesToString(List<ENUM_FEATURES> Enum_Features)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      String _str_feature;

      foreach (ENUM_FEATURES _feature in Enum_Features)
      {

        _str_feature = ((Int32)_feature).ToString();
        _sb.Append(_str_feature);
        if (Enum_Features.IndexOf(_feature) != Enum_Features.Count - 1)
        {
          _sb.Append(",");
        }
      }

      return _sb.ToString();
    }


    #endregion
  }
}
