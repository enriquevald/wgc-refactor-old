﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GenericReport
// 
//   DESCRIPTION: Implementation Interface IReportToolGetDataService
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 08-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-JUN-2016 RGR    Initial Version
// 24-NOV-2016 EOR    PBI 19870: R.Franco: New report machines and games
// 28-SEP-2017 RGR    Bug 29898:WIGOS-4921 Accounting - Generic report Monthly report: the design is not the same than the story and the footer signings are missing
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.GenericReport.Factories;

namespace WSI.Common.GenericReport
{
  public class ReportToolGetDataService : IReportToolGetDataService
  {
    #region Members

    StringBuilder _sbMessages = null;

    #endregion

    #region IReportToolGetDataService
    //Get Data Report
    public ReportToolGetDataResponse GetDataBaseReport(GetDataBaseReportRequest request)
    {
      ReportToolGetDataResponse _reponse = null;

      switch (request.Report.Filters.FilterType)
      {
        case ReportToolFilterType.MonthYear:
          _reponse = this.GetDataBaseMonthYearReport(request);

          break;
        case ReportToolFilterType.FromToDate:
          _reponse = this.GetDataBaseFromToDateReport(request);

          break;
        case ReportToolFilterType.CustomFilters: //EOR 24-NOV-2016
          _reponse = this.GetDataBaseCustomReport(request);

          break;
      }
      return _reponse;
    }

    public bool IsCreatePDF(CreatePDFRequest request)
    {
      return ToolPDF.Create(request);
    }
  
    #endregion

    #region Private Methods

    private ReportToolGetDataResponse GetDataBaseMonthYearReport(GetDataBaseReportRequest request)
    {
      ReportToolGetDataResponse _response;
      ReportToolMonthYearFilter _filter;

      _response = null;
      _filter = (ReportToolMonthYearFilter)request.Report.Filters;
      _sbMessages = new StringBuilder();

      try
      {
        using (DB_TRX _cn = new DB_TRX())
        {
          _cn.SqlTransaction.Connection.InfoMessage += Connection_InfoMessage;

          using (SqlCommand _cmd = new SqlCommand())
          {
            _cmd.Connection = _cn.SqlTransaction.Connection;
            _cmd.Transaction = _cn.SqlTransaction;
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandText = request.Report.StoreName;
            _cmd.Parameters.Add("@pMonth", SqlDbType.Int).Value = _filter.Month;
            _cmd.Parameters.Add("@pYear", SqlDbType.Int).Value = _filter.Year;

            using (SqlDataAdapter _adap = new SqlDataAdapter(_cmd))
            {
              using (DataSet _dts = new DataSet())
              {
                _adap.Fill(_dts);
                if (_dts.Tables.Count > 0)
                {
                  _response = ReportToolGetDataResponseFactory.Create(request, _dts);
                }
                else
                {
                  _response = ReportToolGetDataResponseFactory.Create(this._sbMessages.ToString());
                }
              }
            }
          }
        }
      }
      catch (SqlException ex)
      {
        _response = ReportToolGetDataResponseFactory.Create();
        Log.Exception(ex);
      }
      return _response;
    }

    private ReportToolGetDataResponse GetDataBaseFromToDateReport(GetDataBaseReportRequest request)
    {
      ReportToolGetDataResponse _response;
      ReportToolFromToDateFilter _filter;

      _response = null;
      _filter = (ReportToolFromToDateFilter)request.Report.Filters;
      _sbMessages = new StringBuilder();

      try
      {
        using (DB_TRX _cn = new DB_TRX())
        {
          _cn.SqlTransaction.Connection.InfoMessage += Connection_InfoMessage;

          using (SqlCommand _cmd = new SqlCommand())
          {
            _cmd.Connection = _cn.SqlTransaction.Connection;
            _cmd.Transaction = _cn.SqlTransaction;
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandText = request.Report.StoreName;
            _cmd.Parameters.Add("@pFrom", SqlDbType.DateTime).Value = _filter.From;
            _cmd.Parameters.Add("@pTo", SqlDbType.DateTime).Value = _filter.To;

            using (SqlDataAdapter _adap = new SqlDataAdapter(_cmd))
            {
              using (DataSet _dts = new DataSet())
              {
                _adap.Fill(_dts);
                if (_dts.Tables.Count > 0)
                {
                  _response = ReportToolGetDataResponseFactory.Create(request, _dts);
                }
                else
                {
                  _response = ReportToolGetDataResponseFactory.Create(this._sbMessages.ToString());
                }
              }
            }
          }
        }
      }
      catch (SqlException ex)
      {
        _response = ReportToolGetDataResponseFactory.Create();
        Log.Exception(ex);
      }
      return _response;
    }

    //EOR 24-NOV-2016
    private ReportToolGetDataResponse GetDataBaseCustomReport(GetDataBaseReportRequest request)
    {
      ReportToolGetDataResponse _response;
      ReportToolCustomFilter _filter;

      _response = null;
      _filter = (ReportToolCustomFilter)request.Report.Filters;
      _sbMessages = new StringBuilder();

      try
      {
        using (DB_TRX _cn = new DB_TRX())
        {
          _cn.SqlTransaction.Connection.InfoMessage += Connection_InfoMessage;

          using (SqlCommand _cmd = new SqlCommand())
          {
            _cmd.Connection = _cn.SqlTransaction.Connection;
            _cmd.Transaction = _cn.SqlTransaction;
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandText = request.Report.StoreName;
            _cmd.Parameters.AddRange(_filter.dbParameters);

            using (SqlDataAdapter _adap = new SqlDataAdapter(_cmd))
            {
              using (DataSet _dts = new DataSet())
              {
                _adap.Fill(_dts);
                if (_dts.Tables.Count > 0)
                {
                  _response = ReportToolGetDataResponseFactory.Create(request, _dts);
                }
                else
                {
                  _response = ReportToolGetDataResponseFactory.Create(this._sbMessages.ToString());
                }
              }
            }
          }
        }
      }
      catch (SqlException ex)
      {
        _response = ReportToolGetDataResponseFactory.Create();
        Log.Exception(ex);
      }

      return _response;
    }

    //LTC 24-NOV-2016
    private void Connection_InfoMessage(object sender, SqlInfoMessageEventArgs e)
    {
      _sbMessages.AppendLine(e.Message);
    }

    #endregion
  }
}
