﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GenericReport
// 
//   DESCRIPTION: Domain entities for the module GenericReport
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 03-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-JUN-2016 RGR    Initial Version
// 30-JUN-2016 RGR    Add multilanguage in report name and sheet name
// 11-JUL-2016 RGR    Add multilanguage in columns name
// 17-AUG-2016 RGR    New models format colummn
// 25-AGO-2016 ESE    Product Backlog Item 13581:Show all the reports, not only those whit permission
// 15-SEP-2016 ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
// 24-NOV-2016 EOR    PBI 19870: R.Franco: New report machines and games
// 12-SEP-2017 EOR    Bug 29728: WIGOS-4977 Accounting - Generic report: The label "Months" or "Date" is changed by "Filters" on generic report form
// 28-SEP-2017 RGR    Bug 29898: WIGOS-4921 Accounting - Generic report Monthly report: the design is not the same than the story and the footer signings are missing
// 17-NOV-2017 LTC    Product Backlog Item 30849:Multisite: Reporte genérico terminales
// 30-NOV-2017 RGR    PBI 30932:WIGOS-6393 GUI - Menu: User Interface improvements
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml.Serialization;

namespace WSI.Common.GenericReport
{
  #region Enums

  //LTC 17-NOV-2017
  public enum TypeSourceItems
  {
    Items = 0,
    Query = 1
  }

  //EOR 24-NOV-2016
  public enum ReportToolTypeControl
  {
    uc_combo = 1,
    uc_date_picker = 2,
    uc_entry_field = 3,
    CheckBox = 4,
    RadioButton = 5
  }

  public enum LocationMenu
  {
    SystemMenu = 1,
    TerminalsMenu = 2,
    MonitorMenu = 3,
    AlarmsMenu = 4,
    StatisticsMenu = 5,
    CashMenu = 6,
    CageMenu = 7,
    AccountingMenu = 8,
    CustomerMenu = 9,
    MarketingMenu = 10,
    JackpotsMenu = 11,
    GamingTableMenu = 12,
    SmartFloorMenu = 13,
    GameGatewayMenu = 14
  }

  //LTC 17-NOV-2017
  public enum LocationMenuMultiSite
  {
    SystemMenu = 1,
    TerminalsMenu = 2,
    AuditMenu = 3,
    StatisticsMenu = 4,
    AccountingMenu = 5,
    CustomerMenu = 6,
    MonitorMenu = 7,
    JackpotsMenu = 8
  }

  public enum ReportToolFilterType
  {
    MonthYear = 1,
    FromToDate = 2,
    CustomFilters = 3 //EOR 24-NOV-2016
  }

  public enum EquityMatchType
  {
    Contains = 1,
    Equality = 2
  }

  public enum ReportToolFormatAlignment
  {   
    Left = 1,
    Center = 2,
    Right = 3
  }

  public enum ReportToolFormatType
  {
    Text = 1,
    Integer = 2,
    Currency = 3,
    CurrencyIso = 4,
    Percentage = 5,
    DateTime = 6
  }

  public enum VariableType
  {
    GeneralParam = 1,
    CurrentUser = 2,
    Parameter = 3
  }

 #endregion

  #region Language

  public abstract class NLS
  {
    public string Label { get; set; }
  }

  public class NLS09 : NLS { }
  public class NLS10 : NLS { }

  public class LanguageResources
  {
    public NLS09 NLS09 { get; set; }

    public NLS10 NLS10 { get; set; }

    public string GetLabel()
    {
      string _label;

      _label = string.Empty;
      switch (Language.GetWigosGUILanguageId())
      {
        case LanguageIdentifier.English:
          _label = NLS09.Label;
          break;
        case LanguageIdentifier.Spanish:
          _label = NLS10.Label;
          break;
      }
      return _label;
    }
  }

  #endregion

  #region Design Report

  public class ReportToolFormat
  {
    public int Width { get; set; }

    public ReportToolFormatAlignment Alignment { get; set; }

    public ReportToolFormatType DataType { get; set; }
  }

  //LTC 17-NOV-2017
  public class ReportToolDesignFilter
  {
    public ReportToolTypeControl TypeControl { get; set; }

    public ReporToolTextControl TextControl { get; set; }
        
    public ReportToolParameterStoredProcedure ParameterStoredProcedure { get; set; }

    public Property[] Propertys { get; set; }

    public Method[] Methods { get; set; }

    public TypeSourceItems TypeSource { get; set; }

    public string Query { get; set; }

    public Item[] Items { get; set; }

    public string Value { get; set; }
  }

  public class ReporToolTextControl
  {
    public string Label
    {
      get
      {
        return LanguageResources.GetLabel();
      }
    }

    public LanguageResources LanguageResources { get; set; }
  }

  public class ReportToolParameterStoredProcedure
  {
    public string Name { get; set; }

    public SqlDbType Type { get; set; }
  }

  public class Property
  {
    public string Name { get; set; }

    public string Value { get; set; }
  }

  public class Method
  {
    public string Name { get; set; }

    public string Parameters { get; set; }
  }

  public class Item
  {
    public int Id { get; set; }

    public ReporToolItemText Text { get; set; }
  }

  public class ReporToolItemText
  {
    public string Label
    {
      get
      {
        return LanguageResources.GetLabel();
      }
    }
    public LanguageResources LanguageResources { get; set; }
  }

  public class ReportToolDesignColumn
  {
    public string Code { get; set; }

    public ReportToolFormat Format { get; set; }

    public EquityMatchType EquityMatchType { get; set; }
   
    public string ColumnName
    {
      get
      {
        return LanguageResources.GetLabel();
      }
    }

    public LanguageResources LanguageResources { get; set; }
  }

  public class ReportToolDesignSheetsDTO
  {
    public string SheetName
    {
      get
      {
        return this.LanguageResources.GetLabel();
      }
    }

    public LanguageResources LanguageResources { get; set; }

    public ReportToolDesignColumn[] Columns { get; set; }
  }

  // EOR 12-SEP-2017
  public class ReportToolFilterText 
  {
    public string Label
    {
      get
      {
        return LanguageResources.GetLabel();
      }
    }

    public LanguageResources LanguageResources { get; set; }
  }

  #endregion

  #region Filter

  public class ReportToolDesignFilterDTO
  {
    public bool ShowPrint { get; set; }

    public VariableDTO[] Variables { get; set; }

    public ReportToolFilterType FilterType { get; set; }

    public ReportToolDesignFilter[] Filters { get; set; } //EOR 24-NOV-2016

    public ReportToolFilterText FilterText { get; set; }  //EOR 12-SEP-2017 
  }

  public class VariableDTO
  {
    public string Name { get; set; }

    public VariableType Type { get; set; }

    public string MetaDataOne { get; set; }

    public string MetaDataTwo { get; set; }
  }

  public class ReportToolMonthYearFilter : ReportToolCustomFilter
  {
    public int Month { get; set; }

    public int Year { get; set; }
  }

  public class ReportToolFromToDateFilter : ReportToolCustomFilter
  {
    public DateTime From { get; set; }

    public DateTime To { get; set; }
  }

  //EOR 24-NOV-2016
  public class ReportToolCustomFilter : ReportToolDesignFilterDTO
  {
    public System.Data.SqlClient.SqlParameter[]  dbParameters { get; set; }
  }

  #endregion  

  #region Report

  public class ReportToolConfigDTO
  {
    public int ReportID { get; set; }

    public int FormID { get; set; }

    public LocationMenu LocationMenu { get; set; }

    public LocationMenuMultiSite LocationMenuMultiSite { get; set; }

    public string ReportName
    {
      get
      {
        return Language.GetLabel();
      }
    }

    public string StoreName { get; set; }

    public LanguageResources Language { get; set; }

    public ReportToolDesignSheetsDTO[] Sheets { get; set; }

    public ReportToolDesignFilterDTO Filters { get; set; }

    public ENUM_FEATURES ModeType { get; set; }

    public bool IsExecute { get; set; }

    public string Header { get; set; }

    public string Footer { get; set; }
  }
 
  public class ReportToolDesignSheetsDataDTO : ReportToolDesignSheetsDTO
  {
    public DataTable DataReport { get; set; }
  }

  #endregion

  #region Request

  public class ReportToolConfigRequest
  {
    public LocationMenu LocationMenu { get; set; }

    public LocationMenuMultiSite LocationMenuMultiSite { get; set; }
  }

  public class GetDataBaseReportRequest
  {
    public ReportToolConfigDTO Report { get; set; }   
  }

  public class ConstructorRequest
  {
    public int ProfileID { get; set; }

    //TODO ETP ARRAY OF MODES.
    public List<ENUM_FEATURES> ModeType { get; set; }

    public bool ShowAll { get; set; }

    public bool OnlyReportsMailing { get; set; }
  }

  public class CreatePDFRequest
  {
    public string FileName { get; set; }

    public string UserName { get; set; }

    public string UserFullName { get; set; }

    public  IList<ReportToolDesignSheetsDataDTO> Sheets { get; set; }

    public ReportToolDesignFilterDTO Filters { get; set; }

    public string Header { get; set; }

    public string Footer { get; set; }
  }

  #endregion

  #region Response

  public abstract class ReportToolSuccessResponse
  {
    public string Message { get; set; }

    public int Code { get; set; }

    public bool Success { get; set; }
  }

  public class ReportToolConfigResponse : ReportToolSuccessResponse
  {
    public IList<ReportToolConfigDTO> ReportToolConfigs { get; set; }
  }

  public class ReportToolGetDataResponse : ReportToolSuccessResponse
  {
    public IList<ReportToolDesignSheetsDataDTO> MetaDatas { get; set; }
  }  

  #endregion

  #region Interfaces

  public interface IReportToolConfigService : IDisposable
  {
    List<ReportToolConfigDTO> List_reports { get; }

    bool ExistLocationMenu(LocationMenu menu);

    bool ExistLocationMenu(LocationMenuMultiSite menu);

    bool GetChildReportsPermissions(LocationMenu menu);

    bool GetChildReportsPermissions(LocationMenuMultiSite menu);

    ReportToolConfigResponse GetMenuConfiguration(ReportToolConfigRequest request);

    ReportToolConfigResponse GetReportToolConfig(int reportToolConfigID);
  }

  public interface IReportToolGetDataService 
  {
    ReportToolGetDataResponse GetDataBaseReport(GetDataBaseReportRequest request);

    bool IsCreatePDF(CreatePDFRequest request);

  }

  #endregion
}
