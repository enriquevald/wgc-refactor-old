//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: XML.cs
// 
//   DESCRIPTION: Some XML utils
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
// 29-OCT-2013 NMR    Added some methods for jobs with attributes
// 12-NOV-2013 NMR    Added method to retrieve values in other formats
// 03-DIC-2013 NMR    Improvement to prevent exception when value doesn't exists
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
 

namespace WSI.Common
{
  /// <summary>
  /// Provides a set of methods to facilitate the work with XML.
  /// </summary>
  public static class XML
  {
    #region PUBLIC

    /// <summary>
    /// Parses a 
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    static public double ParseDouble(string number)
    {
      double num;
      
      number = number.Replace(".", System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
      
      num = double.Parse(number);

      return num;

    }

    /// <summary>
    /// Parses a 
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    static public string DoubleToXmlValue(double number)
    {
      String num;

      num = number.ToString("0.00");
      num = num.Replace(System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator, ".");

      return num;
    }

    static public String CleanStringAttributeValue(String AttributeValue)
    {
      StringBuilder buffer = new StringBuilder(AttributeValue.Length);//This many chars at most

      foreach (char ch in AttributeValue)
        if (!Char.IsControl(ch))
        {
          buffer.Append(ch);//Only add to buffer if not a control char
        }
        else
        {
          buffer.Append(' ');
        }

      return buffer.ToString();
    }

    /// <summary>
    /// Parses a 
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    static public string StringToXmlValue(String Value)
    {
      String _xml;

      if (String.IsNullOrEmpty(Value))
      {
        return "";
      }

      _xml = CleanStringAttributeValue (Value);
      _xml = _xml.Replace("&", "&amp;");
      _xml = _xml.Replace("<", "&lt;");
      _xml = _xml.Replace(">", "&gt;");
      _xml = _xml.Replace("\"", "&quot;");
      _xml = _xml.Replace("'", "&apos;");

      return _xml;
    }
    

    /// <summary>
    /// Validates an XML document
    /// </summary>
    /// <param name="Xml">The xml string</param>
    /// <returns> - True:  on success
    ///           - False: otherwise
    /// </returns>
    static public bool TryParse(String Xml)
    {
      bool parsed;

      parsed = false;

      try
      {
        XmlDocument xml_document;

        xml_document = new XmlDocument();
        xml_document.LoadXml(Xml);

        parsed = true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return parsed;
    }

    /// <summary>
    /// Appends a child element to the node.
    /// </summary>
    /// <param name="Node">The parent</param>
    /// <param name="Name">Child name</param>
    /// <param name="Value">Child value</param>
    /// <returns>The child node</returns>
    public static XmlNode AppendChild(XmlNode Node, String Name, String Value)
    {
      XmlNode child;

      // Create the child node
      child = Node.OwnerDocument.CreateNode(XmlNodeType.Element, Name, "");
      // Set the value
      child.InnerText = Value;
      // Append the child to the node
      Node.AppendChild(child);
      // Return the child
      return child;
    }

    /// <summary>
    /// Insert a child element before the RefChild node.
    /// </summary>
    /// <param name="Node">The parent</param>
    /// <param name="RefChild">The new Child is placed before this node</param>
    /// <param name="Name">Child name</param>
    /// <param name="Value">Child value</param>
    /// <returns>The child node</returns>
    public static XmlNode InsertBefore(XmlNode Node, XmlNode RefChild, String Name, String Value)
    {
      XmlNode child;

      // Create the child node
      child = Node.OwnerDocument.CreateNode(XmlNodeType.Element, Name, "");
      // Set the value
      child.InnerText = Value;
      // Insert the new child before RefChild
      Node.InsertBefore(child, RefChild);
      // Return the child
      return child;
    }

    /// <summary>
    /// Retrieves the value of the child element named 'Name'
    /// </summary>
    /// <param name="Node">Parent node</param>
    /// <param name="Name">Child name</param>
    /// <returns>Child value</returns>
    public static String GetValue(XmlNode Node, String Name)
    {
      XmlNode child;

      child = Node.SelectSingleNode(Name);
      if (child != null)
      {
        return child.InnerXml;
      }
      return "";
    }

    /// <summary>
    /// Retrieves the value of the element named 'FieldName'
    /// </summary>
    /// <param name="XmlDoc">Xml string</param>
    /// <param name="FieldName">The field name</param>
    /// <returns>The field value</returns>
    public static String GetValue(String XmlDoc, String FieldName)
    {
      try
      {
        XmlDocument doc;
        XmlNode node;

        doc = new XmlDocument();
        doc.LoadXml(XmlDoc);

        node = doc.SelectSingleNode(FieldName);
        if (node == null)
        {
          return "";
        }
        return node.InnerXml;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return "";
      }
    }

    /// <summary>
    /// Retrieves the value of the element named 'FieldName' in sequencial mode, 
    /// getting it from the last possition read. This methots overrides the old
    /// with a XmlNode input param.
    /// </summary>
    /// <param name="XmlDoc">Xml reader</param>
    /// <param name="FieldName">The field name</param>
    /// <returns>The field value</returns>
    public static String GetValue(XmlReader XmlReader, String FieldName, Boolean IsOptional)
    {
      return GetValue(XmlReader, FieldName, IsOptional, false);
    }

    public static String ReadTagValue(XmlReader XmlReader, String FieldName, Boolean IsOptional)
    {
      return GetValue(XmlReader, FieldName, IsOptional, true);
    }
    public static String ReadTagValue(XmlReader XmlReader, String FieldName)
    {
      return ReadTagValue(XmlReader, FieldName, false);
    }

    public static String GetValue(XmlReader XmlReader, String FieldName, Boolean IsOptional, Boolean ReadEndTag)
    {
      String _value;

      _value = "";
      
      try
      {
        while ((XmlReader.Name != FieldName) || (XmlReader.NodeType != XmlNodeType.Element))
        {
          if (!XmlReader.Read())
          {
            if (!IsOptional)
            {
              throw new SystemException("Node not found");
            }

            return _value;
          }

        }

        // Found Element.Name = FieldName
        XmlReader.Read();

        if (XmlReader.HasValue)
        {
          _value = XmlReader.Value;

          if (ReadEndTag)
          {
            XmlReader.Read();
          }
        }

        return _value;
      }
      catch (Exception ex)
      {
        Log.Warning("GetValue Node not found -> FieldName: " + FieldName.ToString());
        Log.Exception(ex);

        return "";
      }
    }

    public static String GetValue(XmlReader XmlReader, String FieldName)
    {
      return GetValue(XmlReader, FieldName, false);
    }

    // retrieve the value as Int64 number; exception isn't captured here
    public static Int64 GetAsInt64(XmlReader XmlReader, String FieldName)
    {
      Int64 _result;
      String _str_value;

      _result = 0;

      if ((_str_value = GetValue(XmlReader, FieldName, false, false)) != "")
      {
        _result = Int64.Parse(_str_value);
      }

      return _result;
    }

    // retrieve the value as Int32 number; exception isn't captured here
    public static Int32 GetAsInt32(XmlReader XmlReader, String FieldName)
    {
      Int32 _result;
      String _str_value;

      _result = 0;

      if ((_str_value = GetValue(XmlReader, FieldName, false, false)) != "")
      {
        _result = Int32.Parse(_str_value);
      }

      return _result;
    }

    // retrieve the value as DateTime; exception isn't captured here
    public static DateTime GetAsDateTime(XmlReader XmlReader, String FieldName)
    {
      return DateTimeFromXmlDateTimeString(GetValue(XmlReader, FieldName, false, false));
    }

    public static DateTime GetAsDate(XmlReader XmlReader, String FieldName)
    {
      String _value;
      DateTime _dt;

      _value = GetValue(XmlReader, FieldName, false, false);
      _dt = DateTime.Parse(_value.Substring(0, 10));
      if (_dt.Year >= DateTime.MaxValue.Year)
      {
        return DateTime.MaxValue;
      }
      if (_dt.Year <= DateTime.MinValue.Year)
      {
        return DateTime.MinValue;
      }

      return _dt;
    }


    public static DateTime DateTimeFromXmlDateTimeString(String Value)
    {
      if (!String.IsNullOrEmpty(Value))
      {
        String _aux;
        int _year;
        int _idx;

        _idx = Value.IndexOf("-");

        if (_idx >= 0)
        {
          _aux = Value.Substring(0, _idx);

          if (int.TryParse(_aux, out _year))
          {
            if (_year >= DateTime.MaxValue.Year)
            {
              return DateTime.MaxValue;
            }
            if (_year <= DateTime.MinValue.Year)
            {
              return DateTime.MinValue;
            }
          }
        }
      }

      return DateTime.Parse(Value);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns an attribute value, presents in given node
    //
    //  PARAMS :
    //      - INPUT :
    //          - Node: node to be searched
    //          - AtributeName: names of find attribute
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String: value of attribute; could be empty
    //
    //   NOTES :
    //
    public static String GetAttributeValue(XmlNode Node, String AtributeName)
    {
      String _result;
      XmlNode _attribute;

      _result = "";

      _attribute = Node.Attributes.GetNamedItem(AtributeName);
      if (_attribute != null)
      {
        _result = _attribute.InnerText;
      }

      return _result;
    }

    /// <summary>
    /// Get XML formated datetime
    /// </summary>
    /// <param name="DateTime"></param>
    /// <returns></returns>
    public static String XmlDateTimeString(DateTime DateTime)
    {
      DateTime _utc;
      TimeSpan _ts;
      String _dt_str;

      _utc = DateTime.ToUniversalTime();
      _ts = DateTime.Subtract(_utc);

      _dt_str = DateTime.ToString("yyyy") + "-";
      _dt_str += DateTime.ToString("MM") + "-";
      _dt_str += DateTime.ToString("dd") + "T";
      _dt_str += DateTime.ToString("HH") + ":";
      _dt_str += DateTime.ToString("mm") + ":";
      _dt_str += DateTime.ToString("ss");

      if (_ts.TotalMinutes >= 0)
      {
        _dt_str += "+";
      }
      _dt_str += _ts.Hours.ToString("00") + ":";
      _dt_str += _ts.Minutes.ToString("00");

      return _dt_str;

    } // XmlDateTimeString

    public static String XmlDateString(DateTime DateTime)
    {
      String _dt_str;

      _dt_str = DateTime.ToString("yyyy") + "-";
      _dt_str += DateTime.ToString("MM") + "-";
      _dt_str += DateTime.ToString("dd");

      return _dt_str;

    } // XmlDateTimeString

    
    #endregion // PUBLIC
  }
}
