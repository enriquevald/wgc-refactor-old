//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SASMeters.cs
// 
//   DESCRIPTION: SASMeters updates
// 
//        AUTHOR: Humberto Braojos
// 
// CREATION DATE: 26-MAY-2014
// 
// REVISION HISTORY:
//  
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-MAY-2014 HBB    First release.
// 26-MAY-2014 HBB    Moved GetSasMeters from WCP_SASMeters.cs
// 26-MAY-2014 HBB    Moved ENUM_METERS_TYPE from WCP_SASMetersThread.cs
// 07-SEP-2015  MPO        TFS ITEM 2194: SAS16: Estadísticas multi-denominación: WCP
// 14-SEP-2015  DHA   Product Backlog Item 3705: Added coins from terminals
// 17-SEP-2015 JML    Maximun delta for coin acceptor

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common.TITO;

namespace WSI.Common
{
  public class SAS_Meter
  {

    public enum ENUM_METERS_TYPE
    {
      //BILLS = 0,
      //TICKETS = 1,
      BILLS_AND_TICKETS = 2,
      ALL = 3
    };

    public const Int32 TOTAL_COINS_ACCEPTED_METER = 8;
    public const Int32 MIN_TICKET_METER = 128;
    public const Int32 MAX_TICKET_METER = 139;
    public const Int32 MIN_BILL_METER = 64;
    public const Int32 MAX_BILL_METER = 87;
    public const Int32 TOTAL_CREDITS_BILLS_ACCEPTED_METER = 11;

    public Int32 Code;            // code of meters
    public Int32 GameId;          // not for TITO
    public Decimal DenomCents;    // not for TITO
    public Int64 Value;           // actual value of meters
    public Int64 MaxValue;        // meter cycle  (9..999 + 1)

    public Boolean IgnoreDeltas = false;  // flag used by PCD to not calculate meter increments on setting initial meter value

    // values read from DB
    public Int64 SequenceId;  // las stored sequence
    public DateTime TimeOfProcess;  // helper property; instant when the meters-group was processed

    public SAS_Meter()
    {
    }

    public SAS_Meter(Int32 Code, Int32 GameId, Decimal Denomination, Int64 Value, Int64 MaxValue)
    {
      this.Code = Code;
      this.GameId = GameId;
      this.DenomCents = Denomination;
      this.Value = Value;
      this.MaxValue = MaxValue;
    }

    [Flags]
    public enum METER_TYPE
    {
      AMOUNT = 1,
      QUANTITY = 2
    }

    public class MeterProperties
    {
      public Decimal Denomination;
      public METER_TYPE MeterType;
      public String SqlColumn;
      public String SqlParameter;
      public SqlDbType SqlType;
      public Type SystemType;

      public MeterProperties(Decimal Denomination, METER_TYPE MeterType, String SqlColumn, String SqlParameter, SqlDbType SqlType, Type SystemType)
      {
        this.Denomination = Denomination;
        this.MeterType = MeterType;
        this.SqlColumn = SqlColumn;
        this.SqlParameter = SqlParameter;
        this.SqlType = SqlType;
        this.SystemType = SystemType;
      }
    }

    private static Dictionary<Int32, MeterProperties> m_bills_meters_properties = null;
    private static Dictionary<Int32, MeterProperties> m_tickets_meters_properties = null;

    public static Dictionary<Int32, MeterProperties> GetTicketsMetersProperties()
    {
      if (m_tickets_meters_properties != null)
      {
        return m_tickets_meters_properties;
      }

      m_tickets_meters_properties = new Dictionary<Int32, MeterProperties>();

      m_tickets_meters_properties.Add(128, new MeterProperties(0, METER_TYPE.AMOUNT, "MCM_CASHABLE_AMOUNT", "@pCashableAmount", SqlDbType.Money, Type.GetType("System.Decimal")));
      m_tickets_meters_properties.Add(129, new MeterProperties(0, METER_TYPE.QUANTITY, "MCM_CASHABLE_NUM", "@pCashableNum", SqlDbType.BigInt, Type.GetType("System.Int64")));
      m_tickets_meters_properties.Add(130, new MeterProperties(0, METER_TYPE.AMOUNT, "MCM_PROMO_NR_AMOUNT", "@pPromoNRAmount", SqlDbType.Money, Type.GetType("System.Decimal")));
      m_tickets_meters_properties.Add(131, new MeterProperties(0, METER_TYPE.QUANTITY, "MCM_PROMO_NR_NUM", "@pPromoNRNum", SqlDbType.BigInt, Type.GetType("System.Int64")));
      m_tickets_meters_properties.Add(132, new MeterProperties(0, METER_TYPE.AMOUNT, "MCM_PROMO_RE_AMOUNT", "@pPromoREAmount", SqlDbType.Money, Type.GetType("System.Decimal")));
      m_tickets_meters_properties.Add(133, new MeterProperties(0, METER_TYPE.QUANTITY, "MCM_PROMO_RE_NUM", "@pPromoRENum", SqlDbType.BigInt, Type.GetType("System.Int64")));

      return m_tickets_meters_properties;
    }

    public static Dictionary<Int32, MeterProperties> GetBillsMetersProperties()
    {
      if (m_bills_meters_properties != null)
      {
        return m_bills_meters_properties;
      }

      m_bills_meters_properties = new Dictionary<Int32, MeterProperties>();
      // DHA: added coins
      m_bills_meters_properties.Add(8, new MeterProperties(0, METER_TYPE.AMOUNT, "MCM_TOTAL_COIN_AMOUNT", "@pTotalCoins", SqlDbType.Money, Type.GetType("System.Decimal")));
      m_bills_meters_properties.Add(64, new MeterProperties(1, METER_TYPE.QUANTITY, "MCM_1_BILL_NUM", "@p1BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(65, new MeterProperties(2, METER_TYPE.QUANTITY, "MCM_2_BILL_NUM", "@p2BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(66, new MeterProperties(5, METER_TYPE.QUANTITY, "MCM_5_BILL_NUM", "@p5BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(67, new MeterProperties(10, METER_TYPE.QUANTITY, "MCM_10_BILL_NUM", "@p10BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(68, new MeterProperties(20, METER_TYPE.QUANTITY, "MCM_20_BILL_NUM", "@p20BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(69, new MeterProperties(25, METER_TYPE.QUANTITY, "MCM_25_BILL_NUM", "@p25BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(70, new MeterProperties(50, METER_TYPE.QUANTITY, "MCM_50_BILL_NUM", "@p50BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(71, new MeterProperties(100, METER_TYPE.QUANTITY, "MCM_100_BILL_NUM", "@p100BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(72, new MeterProperties(200, METER_TYPE.QUANTITY, "MCM_200_BILL_NUM", "@p200BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(73, new MeterProperties(250, METER_TYPE.QUANTITY, "MCM_250_BILL_NUM", "@p250BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(74, new MeterProperties(500, METER_TYPE.QUANTITY, "MCM_500_BILL_NUM", "@p500BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(75, new MeterProperties(1000, METER_TYPE.QUANTITY, "MCM_1000_BILL_NUM", "@p1000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(76, new MeterProperties(2000, METER_TYPE.QUANTITY, "MCM_2000_BILL_NUM", "@p2000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(77, new MeterProperties(2500, METER_TYPE.QUANTITY, "MCM_2500_BILL_NUM", "@p2500BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(78, new MeterProperties(5000, METER_TYPE.QUANTITY, "MCM_5000_BILL_NUM", "@p5000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(79, new MeterProperties(10000, METER_TYPE.QUANTITY, "MCM_10000_BILL_NUM", "@p10000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(80, new MeterProperties(20000, METER_TYPE.QUANTITY, "MCM_20000_BILL_NUM", "@p20000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(81, new MeterProperties(25000, METER_TYPE.QUANTITY, "MCM_25000_BILL_NUM", "@p25000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(82, new MeterProperties(50000, METER_TYPE.QUANTITY, "MCM_50000_BILL_NUM", "@p50000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(83, new MeterProperties(100000, METER_TYPE.QUANTITY, "MCM_100000_BILL_NUM", "@p100000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(84, new MeterProperties(200000, METER_TYPE.QUANTITY, "MCM_200000_BILL_NUM", "@p200000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(85, new MeterProperties(250000, METER_TYPE.QUANTITY, "MCM_250000_BILL_NUM", "@p250000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(86, new MeterProperties(500000, METER_TYPE.QUANTITY, "MCM_500000_BILL_NUM", "@p500000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));
      m_bills_meters_properties.Add(87, new MeterProperties(1000000, METER_TYPE.QUANTITY, "MCM_1000000_BILL_NUM", "@p1000000BillNum", SqlDbType.Int, Type.GetType("System.Int32")));

      return m_bills_meters_properties;
    }

    public static MeterProperties GetBillsMeterProperties(Int32 MeterCode)
    {
      GetBillsMetersProperties();

      if (m_bills_meters_properties.ContainsKey(MeterCode))
      {
        return m_bills_meters_properties[MeterCode];
      }

      //Log.Error("GetBillsMeterProperties: Unexpected MeterCode: " + MeterCode.ToString());

      return null;
    }

    public static MeterProperties GetTicketsMeterProperties(Int32 MeterCode)
    {
      GetTicketsMetersProperties();

      if (m_tickets_meters_properties.ContainsKey(MeterCode))
      {
        return m_tickets_meters_properties[MeterCode];
      }

      //Log.Error("GetTicketsMeterProperties: Unexpected MeterCode: " + MeterCode.ToString());

      return null;
    }

    public static Decimal GetBillDenomination(Int32 MeterCode)
    {
      GetBillsMetersProperties();

      if (m_bills_meters_properties.ContainsKey(MeterCode))
      {
        return m_bills_meters_properties[MeterCode].Denomination;
      }

      //Log.Error("GetBillDenomination: Unexpected MeterCode: " + MeterCode.ToString());

      return 0;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Reads the meters required
    //
    //  PARAMS :
    //      - INPUT :
    //          - ENUM_METERS_TYPE
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //          - SasMeters
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: delta SAS meters Selected from SAS Meters.
    //      - false: error.
    //
    static public Boolean GetSasMeters(ENUM_METERS_TYPE MetersType,
                                       Int32 TerminalId,
                                       Boolean WithDenom,
                                       out DataTable SasMeters,
                                       SqlTransaction Trx)
    {
      StringBuilder _sb;
      StringBuilder _meters_required;

      SasMeters = new DataTable("DELTA_SAS_METERS");
      _meters_required = new StringBuilder("");
      _sb = new StringBuilder();

      try
      {

        switch (MetersType)
        {
          case ENUM_METERS_TYPE.BILLS_AND_TICKETS:
            {
              _sb.Length = 0;
              _sb.AppendLine("   SELECT   ISNULL(MC_CASHIER_SESSION_ID, 0) AS TSM_SESSION_ID ");
              _sb.AppendLine("          , TSM_TERMINAL_ID ");
              _sb.AppendLine("          , TSM_METER_CODE ");
              _sb.AppendLine("          , 0.0 AS TSM_BILL_DENOMINATION ");
              _sb.AppendLine("          , TSM_GAME_ID ");
              _sb.AppendLine("          , TSM_DENOMINATION ");
              _sb.AppendLine("          , TSM_RAW_DELTA_VALUE ");
              _sb.AppendLine("          , TSM_DELTA_VALUE ");
              _sb.AppendLine("          , ISNULL(TSM_COLLECTION_ID, 0) AS MC_COLLECTION_ID ");
              _sb.AppendLine("          , TSM_METER_VALUE ");
              _sb.AppendLine("          , ISNULL(MC_STATUS, 0) AS MC_STATUS ");
              _sb.AppendLine("          , CASE WHEN ISNULL(TE_SMIB2EGM_COMM_TYPE,0) = @pPulsesType THEN 1 ELSE 0 END AS TE_PCD_COMM ");
              _sb.AppendLine("          , TSM_LAST_REPORTED ");
              _sb.AppendLine("          , TSM_LAST_MODIFIED ");
              _sb.AppendLine("          , TE_SAS_ACCOUNTING_DENOM AS SAS_ACCOUNTING_DENOM ");
              _sb.AppendLine("     FROM ( ");
              _sb.AppendLine("          SELECT   TSM_TERMINAL_ID ");
              _sb.AppendLine("                 , TSM_METER_CODE ");
              _sb.AppendLine("                 , TSM_GAME_ID ");
              _sb.AppendLine("                 , TSM_DENOMINATION ");
              _sb.AppendLine("                 , TSM_RAW_DELTA_VALUE ");
              _sb.AppendLine("                 , TSM_DELTA_VALUE ");
              _sb.AppendLine("                 , ( SELECT MAX(MC_COLLECTION_ID) FROM MONEY_COLLECTIONS WHERE MC_TERMINAL_ID = TSM_TERMINAL_ID ) AS TSM_COLLECTION_ID ");
              _sb.AppendLine("                 , TSM_METER_VALUE ");
              _sb.AppendLine("                 , TSM_LAST_REPORTED ");
              _sb.AppendLine("                 , TSM_LAST_MODIFIED ");
              _sb.AppendLine("            FROM   TERMINAL_SAS_METERS ");
              _sb.AppendLine("           WHERE   ((TSM_METER_CODE >= @pMinBillMeterCode AND TSM_METER_CODE <= @pMaxBillMeterCode) ");
              _sb.AppendLine("                 OR (TSM_METER_CODE >= @pMinTicketMeterCode AND TSM_METER_CODE <= @pMaxTicketMeterCode) ");
              _sb.AppendLine("                 OR TSM_METER_CODE = @pTotalCreditsBillsAcceptedMeterCode ");
              _sb.AppendLine("                 OR TSM_METER_CODE = @pTotalCoinsAcceptedMeterCode ");
              _sb.AppendFormat("               {0} ", WithDenom ? "OR TSM_DENOMINATION > 0 )" : ")");
              _sb.AppendLine("             AND   TSM_DELTA_VALUE > 0 ");
              _sb.AppendLine("           ) x ");
              _sb.AppendLine(" INNER JOIN   TERMINALS ");
              _sb.AppendLine("         ON   TSM_TERMINAL_ID = TE_TERMINAL_ID ");
              _sb.AppendLine("  LEFT JOIN   MONEY_COLLECTIONS ");
              _sb.AppendLine("         ON   MC_COLLECTION_ID = TSM_COLLECTION_ID ");
              _sb.AppendLine("      WHERE   TE_TERMINAL_TYPE IN (" + WSI.Common.Misc.GamingTerminalTypeListToString() + ")");
              _sb.AppendLine("        AND   NOT (TE_STATUS = @pTerminalRetiredStatus AND TE_RETIREMENT_DATE < DATEADD(HOUR, -24 ,GETDATE()) ) ");
              if (TerminalId != 0)
              {
                _sb.AppendLine("      AND   TSM_TERMINAL_ID = @pTerminalId ");
              }
              _sb.AppendLine("   ORDER BY   TSM_TERMINAL_ID ");
              _sb.AppendLine("            , TSM_METER_CODE ");
              _sb.AppendLine("            , TSM_GAME_ID ");
              _sb.AppendLine("            , TSM_DENOMINATION ");

              break;
            }
          case ENUM_METERS_TYPE.ALL:
            {
              _sb.Length = 0;
              _sb.AppendLine("   SELECT   ISNULL(MC_CASHIER_SESSION_ID, 0) AS TSM_SESSION_ID ");
              _sb.AppendLine("          , TSM_TERMINAL_ID ");
              _sb.AppendLine("          , TSM_METER_CODE ");
              _sb.AppendLine("          , 0.0 AS TSM_BILL_DENOMINATION ");
              _sb.AppendLine("          , TSM_GAME_ID ");
              _sb.AppendLine("          , TSM_DENOMINATION ");
              _sb.AppendLine("          , TSM_RAW_DELTA_VALUE ");
              _sb.AppendLine("          , TSM_DELTA_VALUE ");
              _sb.AppendLine("          , ISNULL(TSM_COLLECTION_ID, 0) AS MC_COLLECTION_ID ");
              _sb.AppendLine("          , TSM_METER_VALUE ");
              _sb.AppendLine("          , ISNULL(MC_STATUS, 0) AS MC_STATUS ");
              _sb.AppendLine("          , CASE WHEN ISNULL(TE_SMIB2EGM_COMM_TYPE,0) = @pPulsesType THEN 1 ELSE 0 END AS TE_PCD_COMM ");
              _sb.AppendLine("          , TSM_LAST_REPORTED ");
              _sb.AppendLine("          , TSM_LAST_MODIFIED ");
              _sb.AppendLine("          , TE_SAS_ACCOUNTING_DENOM AS SAS_ACCOUNTING_DENOM ");
              _sb.AppendLine("     FROM ( ");
              _sb.AppendLine("          SELECT   TSM_TERMINAL_ID ");
              _sb.AppendLine("                 , TSM_METER_CODE ");
              _sb.AppendLine("                 , TSM_GAME_ID ");
              _sb.AppendLine("                 , TSM_DENOMINATION ");
              _sb.AppendLine("                 , TSM_RAW_DELTA_VALUE ");
              _sb.AppendLine("                 , TSM_DELTA_VALUE ");
              _sb.AppendLine("                 , CASE WHEN ((TSM_METER_CODE >= @pMinBillMeterCode AND TSM_METER_CODE <= @pMaxBillMeterCode) OR (TSM_METER_CODE >= @pMinTicketMeterCode  AND  TSM_METER_CODE <= @pMaxTicketMeterCode) OR TSM_METER_CODE = @pTotalCreditsBillsAcceptedMeterCode OR TSM_METER_CODE = @pTotalCoinsAcceptedMeterCode) THEN");
              _sb.AppendLine("                     ( SELECT MAX(MC_COLLECTION_ID) FROM MONEY_COLLECTIONS WHERE MC_TERMINAL_ID = TSM_TERMINAL_ID )");
              _sb.AppendLine("                   ELSE ");
              _sb.AppendLine("                     NULL ");
              _sb.AppendLine("                   END AS TSM_COLLECTION_ID ");
              _sb.AppendLine("                 , TSM_METER_VALUE ");
              _sb.AppendLine("                 , TSM_LAST_REPORTED ");
              _sb.AppendLine("                 , TSM_LAST_MODIFIED ");
              _sb.AppendLine("            FROM   TERMINAL_SAS_METERS ");
              _sb.AppendFormat("            {0} ", WithDenom ? "" : "WHERE TSM_DENOMINATION = 0");
              _sb.AppendLine("           ) x ");
              _sb.AppendLine(" INNER JOIN   TERMINALS ");
              _sb.AppendLine("         ON   TSM_TERMINAL_ID = TE_TERMINAL_ID ");
              _sb.AppendLine("  LEFT JOIN   MONEY_COLLECTIONS ");
              _sb.AppendLine("         ON   MC_COLLECTION_ID = TSM_COLLECTION_ID ");
              _sb.AppendLine("      WHERE   TE_TERMINAL_TYPE IN (" + WSI.Common.Misc.GamingTerminalTypeListToString() + ")");
              _sb.AppendLine("        AND   NOT (TE_STATUS = @pTerminalRetiredStatus AND TE_RETIREMENT_DATE < DATEADD(HOUR, -1,GETDATE()) ) ");
              _sb.AppendLine("   ORDER BY   TSM_TERMINAL_ID ");
              _sb.AppendLine("            , TSM_METER_CODE ");
              _sb.AppendLine("            , TSM_GAME_ID ");
              _sb.AppendLine("            , TSM_DENOMINATION ");

              break;
            }
          default:
            {
              Log.Error("Error on GetSasMeters");

              return false;
            }
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pTerminalRetiredStatus", SqlDbType.Int).Value = TerminalStatus.RETIRED;
          _cmd.Parameters.Add("@pMaxBillMeterCode", SqlDbType.Int).Value = SAS_Meter.MAX_BILL_METER;
          _cmd.Parameters.Add("@pMinBillMeterCode", SqlDbType.Int).Value = SAS_Meter.MIN_BILL_METER;
          _cmd.Parameters.Add("@pMaxTicketMeterCode", SqlDbType.Int).Value = SAS_Meter.MAX_TICKET_METER;
          _cmd.Parameters.Add("@pMinTicketMeterCode", SqlDbType.Int).Value = SAS_Meter.MIN_TICKET_METER;
          _cmd.Parameters.Add("@pTotalCreditsBillsAcceptedMeterCode", SqlDbType.Int).Value = SAS_Meter.TOTAL_CREDITS_BILLS_ACCEPTED_METER;
          _cmd.Parameters.Add("@pTotalCoinsAcceptedMeterCode", SqlDbType.Int).Value = SAS_Meter.TOTAL_COINS_ACCEPTED_METER;
          _cmd.Parameters.Add("@pPulsesType", SqlDbType.Int).Value = SMIB_COMMUNICATION_TYPE.PULSES;

          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _da.Fill(SasMeters);
          }
        }

        return true;

      }// try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetSasMeters

    static public Boolean SplitDTMachineDenomMetersWithValue(ref DataTable SasMeters,
                                                             ref DataTable SasMetersDenom)
    {
      DataRow _dr;
      Boolean _accept_changes;

      _accept_changes = false;

      if (SasMetersDenom.Columns.Count == 0)
      {
        SasMetersDenom = SasMeters.Clone();
      }

      SasMetersDenom.Rows.Clear();

      try
      {
        for (int _i = 0; _i < SasMeters.Rows.Count; _i++)
        {
          _dr = SasMeters.Rows[_i];

          if ((Decimal)_dr["TSM_DENOMINATION"] != 0)
          {
            if ((Int64)_dr["TSM_DELTA_VALUE"] != 0)
            {
              SasMetersDenom.Rows.Add(_dr.ItemArray);
            }
            _dr.Delete();

            _accept_changes = true;
          }
        }

        if (_accept_changes)
        {
          SasMeters.AcceptChanges();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get General Param BigIncrement
    //
    //  PARAMS :
    //      - INPUT :
    //          - MeterCode
    //
    //      - OUTPUT :
    //          - BigIncrement
    //
    //   NOTES :
    //
    public static void GetBigIncrement(Int32 MeterCode, out Int64 BigIncrement, Int32 TerminalId)
    {

      switch (MeterCode)
      {
        case 0: //Total coin in credits
        case 13: //Total SAS cashable ticket in, including nonrestricted tickets (cents) [same as meter 0080 + 0084]
        case 15: //Total SAS restricted ticket in (cents) [same as meter 0082]
        case 21: //Total ticket in, including cashable, nonrestricted and restricted tickets (credits)
        case 40: //Total cashable ticket in, including nonrestricted promotional tickets (credits)
        case 41: //Total regular cashable ticket in (credits)
        case 42: //Total restricted promotional ticket in (credits)
        case 43: //Total nonrestricted promotional ticket in (credits)
        case 128: //Regular cashable ticket in (cents)
        case 130: //Restricted ticket in (cents)
        case 132: //Nonrestricted ticket in (cents)
          BigIncrement = BigIncrementsData.CentsTicketIn(TerminalId);
          break;

        case 1: //Total coin out credits
        case 14: //Total SAS cashable ticket out, including debit tickets (cents) [same as meter 0086 + 008A]
        case 16: //Total SAS restricted ticket out (cents) [same as meter 0088]
        case 22: //Total ticket out, including cashable, nonrestricted, restricted and debit tickets (credits)
        case 44: //Total cashable ticket out, including debit tickets (credits)
        case 45: //Total restricted promotional ticket out (credits)
        case 134: //Regular cashable ticket out (cents)
        case 136: //Restricted ticket out (cents)
        case 138: //Debit ticket out (cents)
        case 168: //In-house cashable transfers to ticket (cents)
        case 170: //In-house restricted transfers to ticket (cents)
        case 172: //Debit transfers to ticket (cents)
          BigIncrement = BigIncrementsData.CentsTicketOut(TerminalId);
          break;

        case 2: //Total jackpot credits
        case 3: //Total hand paid cancelled credits
        case 4: //Total cancelled credits
        case 9: //Total credits paid from hopper
        case 10: //Total credits from coins to drop
        case 11: //Total credits from bills accepted
        case 12: //Current credits     
        case 28: //Total machine paid paytable win, not including progressive or external bonus amounts (credits)
        case 29: //Total machine paid progressive win (credits)
        case 30: //Total machine paid external bonus win (credits)
        case 31: //Total attendant paid paytable win, not including progressive or external bonus amounts (credits)
        case 32: //Total attendant paid progressive win (credits)
        case 33: //Total attendant paid external bonus win (credits)
        case 34: //Total won credits (sum of total coin out and total jackpot)
        case 35: //Total hand paid credits (sum of total hand paid cancelled credits and total jackpot)
        case 36: //Total drop, including but not limited to coins to drop, bills to drop, tickets to drop, and electronic in (credits)
        case 88: //Total credits from bills to drop
        case 99: //Total credits from bills diverted to hopper
        case 110: //Total credits from bills dispensed from hopper
        case 140: //Validated cancelled credit handpay, receipt printed (cents)
        case 142: //Validated jackpot handpay, receipt printed (cents)
        case 144: //Validated cancelled credit handpay, no receipt (cents)
        case 146: //Validated jackpot handpay, no receipt (cents)
          BigIncrement = BigIncrementsData.CentsWon(TerminalId);
          break;

        case 8: //Total credits from coin acceptor
          BigIncrement = BigIncrementsData.CentsCoinAcceptor(TerminalId);
          break;

        case 5: //Games played
        case 7: //Games lost
        case 37: //Games since last power reset
        case 38: //Games since slot door closure
          BigIncrement = BigIncrementsData.QuantityGamesPlayed(TerminalId);
          break;

        case 6: //Games won
          BigIncrement = BigIncrementsData.QuantityGamesWon(TerminalId);
          break;

        case 17: //Total SAS cashable ticket in, including nonrestricted tickets (quantity) [same as meter 0081 + 0085]
        case 19: //Total SAS restricted ticket in (quantity) [same as meter 0083]
        case 53: //Total regular cashable ticket in (quantity)
        case 54: //Total restricted promotional ticket in (quantity)
        case 55: //Total nonrestricted promotional ticket in (quantity)
        case 129: //Regular cashable ticket in (quantity)
        case 131: //Restricted ticket in (quantity)
        case 133: //Nonrestricted ticket in (quantity)
          BigIncrement = BigIncrementsData.QuantityTicketIn(TerminalId);
          break;

        case 18: //Total SAS cashable ticket out, including debit tickets (quantity) [same as meter 0087 + 008B]
        case 20: //Total SAS restricted ticket out (quantity)  [same as meter 0089]
        case 56: //Total cashable ticket out, including debit tickets (quantity)
        case 57: //Total restricted promotional ticket out (quantity)
        case 135: //Regular cashable ticket out (quantity)
        case 137: //Restricted ticket out (quantity)
        case 139: //Debit ticket out (quantity)
        case 141: //Validated cancelled credit handpay, receipt printed (quantity)
        case 143: //Validated jackpot handpay, receipt printed (quantity)
        case 145: //Validated cancelled credit handpay, no receipt (quantity)
        case 147: //Validated jackpot handpay, no receipt (quantity)
          BigIncrement = BigIncrementsData.QuantityTicketOut(TerminalId);
          break;

        case 23: //Total electronic transfers to gaming machine, including cashable, nonrestricted, restricted and debit, whether transfer is to credit meter or to ticket (credits)
        case 39: //Total credits from external coin acceptor
        case 46: //Electronic regular cashable transfers to gaming machine, not including external bonus awards (credits)
        case 47: //Electronic restricted promotional transfers to gaming machine, not including external bonus awards (credits)
        case 48: //Electronic nonrestricted promotional transfers to gaming machine, not including external bonus awards (credits)
        case 49: //Electronic debit transfers to gaming machine (credits)
        case 160: //In-house cashable transfers to gaming machine (cents)
        case 162: //In-house restricted transfers to gaming machine (cents)
        case 164: //In-house nonrestricted transfers to gaming machine (cents)
        case 166: //Debit transfers to gaming machine (cents)
        case 174: //Bonus cashable transfers to gaming machine (cents)
        case 176: //Bonus nonrestricted transfers to gaming machine (cents)
          BigIncrement = BigIncrementsData.CentsFTToEgm(TerminalId);
          break;

        case 24: //Total electronic transfers to host, including cashable, nonrestricted, restricted and win amounts (credits)
        case 50: //Electronic regular cashable transfers to host (credits)
        case 51: //Electronic restricted promotional transfers to host (credits)
        case 52: //Electronic nonrestricted promotional transfers to host (credits)
        case 184: //In-house cashable transfers to host (cents)
        case 186: //In-house restricted transfers to host (cents)
        case 188: //In-house nonrestricted transfers to host (cents)
          BigIncrement = BigIncrementsData.CentsFTFromEgm(TerminalId);
          break;

        case 25: //Total restricted amount played (credits)
        case 26: //Total nonrestricted amount played (credits)
        case 27: //Current restricted credits
          BigIncrement = BigIncrementsData.CentsPlayed(TerminalId);
          break;

        case 62: //Number of bills currently in the stacker (Issue exception 7B when this meter is reset)
        case 64: //Total number of $1.00 bills accepted
        case 65: //Total number of $2.00 bills accepted
        case 66: //Total number of $5.00 bills accepted
        case 67: //Total number of $10.00 bills accepted
        case 68: //Total number of $20.00 bills accepted
        case 69: //Total number of $25.00 bills accepted
        case 70: //Total number of $50.00 bills accepted
        case 71: //Total number of $100.00 bills accepted
        case 72: //Total number of $200.00 bills accepted
        case 73: //Total number of $250.00 bills accepted
        case 74: //Total number of $500.00 bills accepted
        case 75: //Total number of $1,000.00 bills accepted
        case 76: //Total number of $2,000.00 bills accepted
        case 77: //Total number of $2,500.00 bills accepted
        case 78: //Total number of $5,000.00 bills accepted
        case 79: //Total number of $10,000.00 bills accepted
        case 80: //Total number of $20,000.00 bills accepted
        case 81: //Total number of $25,000.00 bills accepted
        case 82: //Total number of $50,000.00 bills accepted
        case 83: //Total number of $100,000.00 bills accepted
        case 84: //Total number of $200,000.00 bills accepted
        case 85: //Total number of $250,000.00 bills accepted
        case 86: //Total number of $500,000.00 bills accepted
        case 87: //Total number of $1,000,000.00 bills accepted
        case 89: //Total number of $1.00 bills to drop
        case 90: //Total number of $2.00 bills to drop
        case 91: //Total number of $5.00 bills to drop
        case 92: //Total number of $10.00 bills to drop
        case 93: //Total number of $20.00 bills to drop
        case 94: //Total number of $50.00 bills to drop
        case 95: //Total number of $100.00 bills to drop
        case 96: //Total number of $200.00 bills to drop
        case 97: //Total number of $500.00 bills to drop
        case 98: //Total number of $1000.00 bills to drop
        case 100: //Total number of $1.00 bills diverted to hopper
        case 101: //Total number of $2.00 bills diverted to hopper
        case 102: //Total number of $5.00 bills diverted to hopper
        case 103: //Total number of $10.00 bills diverted to hopper
        case 104: //Total number of $20.00 bills diverted to hopper
        case 105: //Total number of $50.00 bills diverted to hopper
        case 106: //Total number of $100.00 bills diverted to hopper
        case 107: //Total number of $200.00 bills diverted to hopper
        case 108: //Total number of $500.00 bills diverted to hopper
        case 109: //Total number of $1000.00 bills diverted to hopper
        case 111: //Total number of $1.00 bills dispensed from hopper
        case 112: //Total number of $2.00 bills dispensed from hopper
        case 113: //Total number of $5.00 bills dispensed from hopper
        case 114: //Total number of $10.00 bills dispensed from hopper
        case 115: //Total number of $20.00 bills dispensed from hopper
        case 116: //Total number of $50.00 bills dispensed from hopper
        case 117: //Total number of $100.00 bills dispensed from hopper
        case 118: //Total number of $200.00 bills dispensed from hopper
        case 119: //Total number of $500.00 bills dispensed from hopper
        case 120: //Total number of $1000.00 bills dispensed from hopper
          BigIncrement = BigIncrementsData.QuantityBillIn(TerminalId);
          break;

        case 63: //Total value of bills currently in the stacker (credits) (Issue exception 7B when this meter is reset)        
          BigIncrement = BigIncrementsData.CentsBillIn(TerminalId);
          break;

        case 161: //In-House transfers to gaming machine that included cashable amounts (quantity)
        case 163: //In-house transfers to gaming machine that included restricted amounts (quantity)
        case 165: //In-house transfers to gaming machine that included nonrestricted amounts (quantity)     
        case 167: //Debit transfers to gaming machine (quantity)
        case 169: //In-house cashable transfers to ticket (quantity)
        case 171: //In-house restricted transfers to ticket (quantity)
        case 175: //Bonus transfers to gaming machine that included cashable amounts (quantity)
        case 177: //Bonus transfers to gaming machine that included nonrestricted amounts (quantity)
          BigIncrement = BigIncrementsData.QuantityFTToEgm(TerminalId);
          break;

        case 173: //Debit transfers to ticket (quantity)
        case 185: //In-house transfers to host that included cashable amounts (quantity)
        case 187: //In-house transfers to host that included restricted amounts (quantity)
        case 189: //In-house transfers to host that included nonrestricted amounts (quantity)
          BigIncrement = BigIncrementsData.QuantityFTFromEgm(TerminalId);
          break;

        //Cacharrito ports
        case 8192:
        case 8193:
        case 8194:
        case 8195:
        case 8196:
        case 8197:
        case 8198:
        case 8199:
        case 8200:
        case 8201:
        case 8202:
        case 8203:
        case 8204:
        case 8205:
        case 8206:
        case 8207:
          BigIncrement = BigIncrementsData.QuantityFTToEgm(TerminalId);//100000;
          break;

        default:
          BigIncrement = 0;
          break;
      }

    } // GetBigIncrement

    /// <summary>
    /// Clean register with denom empty
    /// </summary>
    /// <returns></returns>
    public static Boolean CleanDenomEmpty()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" DELETE FROM   TERMINAL_SAS_METERS              ");
        _sb.AppendLine("       WHERE   TSM_SAS_ACCOUNTING_DENOM IS NULL ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.ExecuteNonQuery();

            _db_trx.Commit();
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
  }
}
