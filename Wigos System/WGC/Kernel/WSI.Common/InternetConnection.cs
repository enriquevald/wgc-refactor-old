﻿//------------------------------------------------------------------------------
// Copyright © 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: InternetConnection.cs
// 
//   DESCRIPTION: Implements InternetConnection
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-NOV-2017 DHA     First release.
// 13-NOV-2017 DHA     Bug 30775:WIGOS-6110 [Ticket #9672] MTY I - Plaza Real - PinPad con error y autorizadas
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;

namespace WSI.Common
{
  public static class InternetConnection
  {

    public static Boolean RequiredInternetConnection 
    {
      get
      {
        return GeneralParam.GetBoolean("PinPad", "RequiredInternetConnection", false);
      }
     
    }
    public static bool CheckForInternetConnection()
    {

       if (!RequiredInternetConnection)
      {
        return true;
      }

      try
      {
        using (var client = new WebClient())
        {
          using (var stream = client.OpenRead("http://www.google.com"))
          {
            return true;
          }
        }
      }
      catch
      {
        return false;
      }
    }

    public static Boolean UpdateServiceInternetConnectionStatus(String ServiceName, Boolean HasInternetConnection)
    {
      StringBuilder _sb;

      if (!RequiredInternetConnection)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("IF NOT EXISTS (SELECT * FROM SERVICE_INTERNET_CONNECTION WHERE SIC_PROTOCOL = @pServiceName) ");
        _sb.AppendLine("BEGIN ");
        _sb.AppendLine("	INSERT INTO   SERVICE_INTERNET_CONNECTION ");
        _sb.AppendLine("                ( SIC_PROTOCOL ");
        _sb.AppendLine("                , SIC_HAS_INTERNET_CONNECTION ");
        _sb.AppendLine("                , SIC_LAST_UPDATE) ");
        _sb.AppendLine("         VALUES ");
        _sb.AppendLine("                ( @pServiceName ");
        _sb.AppendLine("                , @pHasInternetConnection ");
        _sb.AppendLine("                , GETDATE()) ");

        _sb.AppendLine("END ");
        _sb.AppendLine("ELSE ");
        _sb.AppendLine("BEGIN ");
        _sb.AppendLine("	UPDATE   SERVICE_INTERNET_CONNECTION ");
        _sb.AppendLine("	   SET   SIC_HAS_INTERNET_CONNECTION = @pHasInternetConnection ");
        _sb.AppendLine("		     , SIC_LAST_UPDATE = GETDATE() ");
        _sb.AppendLine("	 WHERE   SIC_PROTOCOL = @pServiceName ");
        _sb.AppendLine("END ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pHasInternetConnection", SqlDbType.Bit).Value = HasInternetConnection;
            _cmd.Parameters.Add("@pServiceName", SqlDbType.NVarChar).Value = ServiceName;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Warning("InternetConnection. Exception in function: UpdateServiceInternetConnectionStatus");
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    public static Boolean GetServiceInternetConnectionStatus(String ServiceName)
    {
      StringBuilder _sb;
      Object _obj;

      if (!RequiredInternetConnection)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   SIC_HAS_INTERNET_CONNECTION ");
        _sb.AppendLine("  FROM   SERVICE_INTERNET_CONNECTION ");
        _sb.AppendLine(" WHERE   SIC_PROTOCOL = @pServiceName ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pServiceName", SqlDbType.NVarChar).Value = ServiceName;

            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              return (Boolean)_obj;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Warning("InternetConnection. Exception in function: GetServiceInternetConnectionStatus");
        Log.Exception(_ex);
      }

      return false;
    }
  }
}
