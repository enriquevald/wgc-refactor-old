﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TriggersValidation.cs
// 
//   DESCRIPTION: Class for perform database triggers validation.
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 09-FEB-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-FEB-2018 OMC    First release.
// 12-FEB-2018 OMC    Change On WaitFor Delay from 10seconds to 5 minutes.
// 27-FEB-2018 AGS    Bug 31718:WIGOS-8183 [Ticket #12442] Tickets de fondos disponibles terminales
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;

namespace WSI.Common.TriggersValidation
{
  public static class TriggersValidation
  {

    #region Atributes
    private const string _stored_procedure_name = "EnableTriggers_By_Service";
    private const Int32 WAIT_FOR_NEXT_TRY = (5 * 60 * 1000);      // 5 minutes
    private static Thread m_thread;
    private static List<string> ServiceNames = new List<string>(){
        "InHouseAPI"
    }; // ServiceNames
    #endregion

    # region Public
    //------------------------------------------------------------------------------
    // PURPOSE : Initialize TriggersValidation class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Init()
    {
      m_thread = new Thread(TriggersChecker);
      m_thread.Name = "TriggersCheckerThread";

      m_thread.Start();
    }
    #endregion

    # region Private

    //------------------------------------------------------------------------------
    // PURPOSE : Method to check if we're on WWP_ServiceCenter or in WCP and if we're
    //           principal to perform or not the call to the Toggle method.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private static void TriggersChecker()
    {
      Int32 _wait_hint = 0;
      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);

          if (GeneralParam.GetBoolean("Multisite", "IsCenter", false) ? !Services.IsPrincipal("WWP CENTER") : !Services.IsPrincipal("WCP"))
          {
            _wait_hint = 1000;

            continue;
          }

          _wait_hint = WAIT_FOR_NEXT_TRY;

          ToggleTriggersByService();
        }

        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    } // TriggersChecker

    //------------------------------------------------------------------------------
    // PURPOSE : Method based on ServiceNames List of strings that contains the names 
    //           of the candidate services to toggle his related triggers on/off.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static void ToggleTriggersByService()
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          foreach (string _service_name in ServiceNames)
          {
            using (SqlCommand cmd = new SqlCommand(_stored_procedure_name, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@ServiceName", SqlDbType.VarChar).Value = _service_name;
              cmd.ExecuteNonQuery();
            }
          }
          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // ToggleTriggersByService
    #endregion
  } // TriggersValidation
} // WSI.Common.TriggersValidation
