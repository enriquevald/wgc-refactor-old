﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: BucketsExpirationDayMonth.cs
//
//   DESCRIPTION: BucketsExpirationDayMonth class
//
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 26-JAN-2016

// REVISION HISTORY:
//
// Date         Author  Description
// ------------ ------- ----------------------------------------------------------
// 26-JAN-2016  FGB     First release.
// 01-FEB-2016  FGB     Better expiration date comparition.
// 01-MAR-2016  FGB     Product Backlog Item 9781: Multiple Buckets: Refactorización
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace WSI.Common
{
  public static class BucketsExpirationDayMonth
  {
    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Buckets expiration to date (DD/MM)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    public static Boolean ProcessBucketsExpirationDayMonth()
    {
      int _num_PendingRows;
      BucketsList _bucketsList = new BucketsList();

      // Procesamos los registros expired pendientes de tratar
      ProcessExpiredList("Pending expired records", out _num_PendingRows);
      // Si hemos procesado registros pendientes, salimos.
      if (_num_PendingRows > 0)
      {
        return true;
      }

      // Buscamos la lista de buckets configurados    
      if (!GetBucketList(_bucketsList)) 
      {
        Log.Error("Error in BucketsExpirationDayMonth.GetBucketList");

        return false;
      }

      // Generamos la lista de registros expirados segun la configuración de los buckets
      if (!GenerateExpiratedList(_bucketsList))
      {
        return false;
      }

      // Procesamos los nuecos registros expirados
      ProcessExpiredList("New expired records", out _num_PendingRows);

      return true;

    } // ProcessBucketsExpirationDayMonth

    #endregion Public Methods

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Generates records in the Expired List
    //
    //  PARAMS :
    //      - INPUT :
    //            - BucketsList BucketsList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :     
    private static bool GenerateExpiratedList(BucketsList BucketsList)
    {
      DateTime _date_expiration;
      String _str_day_month;

      // Closing Time
      Int32 _closing_time_HH = GeneralParam.GetInt32("WigosGUI", "ClosingTime", 0, 0, 23);
      Int32 _closing_time_MM = GeneralParam.GetInt32("WigosGUI", "ClosingTimeMinutes", 0, 0, 59);

      // Buscamos si hay nuevos registros a expirar
      foreach (Bucket_Data _bucket_data in BucketsList)
      {
        if (CalculateDateExpirationDayMonth(_bucket_data, _closing_time_HH, _closing_time_MM, out _str_day_month, out _date_expiration))
        {
          if (WGDB.Now >= _date_expiration)
          {
            if (!GenerateBucketsExpiredList(_bucket_data.Id, _str_day_month, _date_expiration))
            {
              Log.Error("Error in BucketsExpirationDayMonth.GenerateBucketsExpiredList. DayMonth: " + _str_day_month + " Year: " + _date_expiration.Year.ToString());

              return false;
            }
          }
        }
      }

      return true;
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Process records at the Expired List
    //
    //  PARAMS :
    //      - INPUT :
    //            - String ErrorSubTitle
    //
    //      - OUTPUT :
    //            - int ExpiredCount
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :   
    private static Boolean ProcessExpiredList(String ErrorSubTitle, out int ExpiredCount)
    {
      DataTable _dt_expired_list;

      _dt_expired_list = GetBucketsExpiredList();

      // Expired records 
      ExpiredCount = _dt_expired_list.Rows.Count;
      if (ExpiredCount > 0)
      {
        if (ProcessBucketsExpiredList(_dt_expired_list))
        {
          if (!SetExecutionBucketsExpiredControl())
          {
            Log.Error("Error in BucketsExpirationDayMonth.SetExecutionBucketsExpiredControl" + " " + ErrorSubTitle);

            return false;
          }
        }
        else
        {
          Log.Error("Error in BucketsExpirationDayMonth.ProcessBucketsExpiredList" + " " + ErrorSubTitle);

          return false;
        }
      }

      return true;

    } // ProcessExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : process records from ACCOUNT_BUCKETS_EXPIRED_LIST
    //
    //  PARAMS :
    //      - INPUT :
    //            - DataTable ExpiredBucketsList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean ProcessBucketsExpiredList(DataTable ExpiredBucketsList)
    {
      Int64 _bucket_id;
      Int64 _account_id;
      int _expire_records = 0;
      
      foreach (DataRow _expired_row in ExpiredBucketsList.Rows)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (ExpireCustomerBucket(_expired_row, _db_trx.SqlTransaction))
            {
              _bucket_id = (Int64)_expired_row["ABEL_BUCKET_ID"];
              _account_id = (Int64)_expired_row["ABEL_ACCOUNT_ID"];

              if (DeleteCustomerBucketFromExpiredList(_bucket_id, _account_id, _db_trx.SqlTransaction))
              {
                _expire_records++;
                _db_trx.Commit();
              }
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          continue;
        }
      }

      return (ExpiredBucketsList.Rows.Count == _expire_records);

    } // ProcessBucketsExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate date expiration by dayMonth
    //
    //  PARAMS :
    //      - INPUT :
    //            - Bucket_Data BucketData
    //            - Int32 ClosingTime
    //      - OUTPUT :
    //            - String DayMonth
    //            - DateTime _date_expiration
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean CalculateDateExpirationDayMonth(Bucket_Data BucketData, Int32 ClosingTimeHH, Int32 ClosingTimeMM, out String DayMonth, out DateTime DateExpiration)
    {
      int _idx;
      Int32 _day;
      Int32 _month;

      //Get the ExpirationDate of the bucket
      DayMonth = BucketData.ExpirationDate;
      DateExpiration = WGDB.Now;

      if (!String.IsNullOrEmpty(DayMonth))
      {
        DayMonth = DayMonth.Replace("-", "/");
        DayMonth = DayMonth.Replace(".", "/");
        _idx = DayMonth.IndexOf("/");

        if (_idx > 0
          && Int32.TryParse(DayMonth.Substring(0, _idx), out _day)
          && Int32.TryParse(DayMonth.Substring(_idx + 1), out _month))
        {
          if ((_month >= 1 && _month <= 12) && (_day >= 1 && _day <= 31))
          {
            while (_day > 0)
            {
              try
              {
                DateExpiration = new DateTime(WGDB.Now.Year, _month, _day);
                DateExpiration = DateExpiration.AddHours(ClosingTimeHH);
                DateExpiration = DateExpiration.AddMinutes(ClosingTimeMM);
                DateExpiration = DateExpiration.AddDays(1);

                return true;
              }
              catch
              {
                _day = _day - 1;
              }
            }
          }
        }
        else
        {
          Log.Error(String.Format("BucketsExpirationDayMonth.CalculateDateExpirationDayMonth. Invalid Bucket.ExpirationDate: {0} for Bucket: {1} - {2}", DayMonth, BucketData.Id, BucketData.Name));
        }
      }

      return false;

    } // CalculateDateExpirationDayMonth

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the list of buckets
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - BucketsList
    //
    //   NOTES : 
    private static Boolean GetBucketList(BucketsList BucketsList)
    {
      if (BucketsList == null)
      {
        BucketsList = new BucketsList();
      }

      try
      {
        return BucketsList.GetList();
      }
      catch (Exception _ex)
      {
        Log.Error("Error in BucketsExpirationDayMonth.GetBucketList: " + _ex.Message.ToString());
      }

      return false;
    }
    
    //------------------------------------------------------------------------------
    // PURPOSE : Generate list of expired buckets
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64 BucketId
    //            - String DayMonth
    //            - DateTime _date_expiration
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean GenerateBucketsExpiredList(Int64 BucketId, String DayMonth, DateTime DateExpiration)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (InsertBucketsExpiredControl(BucketId, DayMonth, DateExpiration.Year, _db_trx.SqlTransaction))
          {
            if (SetBucketsExpiredList(BucketId, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();

              return true;
            }
          }

          return true; // has been processed previously
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GenerateBucketsExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : Bucket expiration for DayMonth
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataRow DataRowExpiredBucket
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean ExpireCustomerBucket(DataRow DataRowExpiredBucket, SqlTransaction SqlTrx)
    {
      Buckets.BucketId _bucket_id;
      Int64 _account_id;
      Decimal _value;
      Decimal _old_value;
      Decimal _new_value;
      long _operation_id;

      BucketsForm _buckets_form = new BucketsForm();

      _bucket_id = (Buckets.BucketId)(Int64)DataRowExpiredBucket["ABEL_BUCKET_ID"];
      _account_id = (Int64)DataRowExpiredBucket["ABEL_ACCOUNT_ID"];
      _value = (Decimal)DataRowExpiredBucket["ABEL_VALUE_TO_EXPIRE"];

      try
      {
        //Actualizamos el Customer Bucket y el History
        if (!BucketsUpdate.UpdateCustomerBucket(_account_id, _bucket_id, null, _value, Buckets.BucketOperation.EXPIRE, true, true, out _old_value, out _new_value, SqlTrx))
        {
          return false;
        }

        //Creamos los movimientos de Cuenta y Cajero
        _operation_id = 0;
        return CreateExpiredBucketMovements(_bucket_id, _account_id, _operation_id, _old_value, _new_value, _buckets_form, SqlTrx);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ExpireCustomerBucket


    //------------------------------------------------------------------------------
    // PURPOSE : Create Movements for the Expired Bucket
    //
    //  PARAMS :
    //      - INPUT :
    //          - Buckets.BucketId BucketId
    //          - Int64 AccountId
    //          - long OperationId
    //          - Decimal OldValue
    //          - Decimal NewValue
    //          - BucketsForm BucketsForm
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean CreateExpiredBucketMovements(Buckets.BucketId BucketId, Int64 AccountId, long OperationId, Decimal OldValue, Decimal NewValue, BucketsForm BucketsForm, SqlTransaction SqlTrx)
    {
      CashierSessionInfo _cashier_session; 
      MovementType _movement_type;
      CASHIER_MOVEMENT _cashier_movement;
      Decimal _diff_value = OldValue - NewValue; //Difference
      Boolean _multisite_is_center;

      _movement_type = MovementType.MULTIPLE_BUCKETS_Expired + Convert.ToInt16(BucketId);
      _multisite_is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", false);

      if (_multisite_is_center)
      {
        // Center: create 1) account movements
        if (!BucketsForm.CreateAccountMovements(_movement_type, OperationId, OldValue, 0, _diff_value, NewValue, AccountId, "", SqlTrx))
        {
          return false;
        }
      }
      else
      {
        // Site: Create 1) account movements
        //              2) cashier movements
        _cashier_session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
        if (_cashier_session == null)
        {
          return false;
        }

        if (!BucketsForm.CreateAccountMovements(_movement_type, _cashier_session, OperationId, OldValue, 0, _diff_value, NewValue, AccountId, "", SqlTrx))
        {
          return false;
        }
        _cashier_movement = CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED + Convert.ToInt16(BucketId);

        if (!BucketsForm.CreateCashierMovements(_cashier_movement, _cashier_session, OperationId, _diff_value, 0, "", "", SqlTrx))
        {
          return false;
        }
      }


      

      return true;
    } // CreateExpiredBucketMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Get records from ACCOUNT_BUCKETS_EXPIRED_LIST
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable: list of records to expire
    //      
    //   NOTES :
    private static DataTable GetBucketsExpiredList()
    {
      StringBuilder _sb;
      DataTable _dtBucketsExpiredList;

      _dtBucketsExpiredList = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   ABEL_ACCOUNT_ID               ");
        _sb.AppendLine("       , ABEL_BUCKET_ID                ");
        _sb.AppendLine("       , ABEL_VALUE_TO_EXPIRE          ");
        _sb.AppendLine("  FROM   ACCOUNT_BUCKETS_EXPIRED_LIST  ");
        _sb.AppendLine(" WHERE   ABEL_VALUE_TO_EXPIRE > 0      ");
        _sb.AppendLine("ORDER BY ABEL_ACCOUNT_ID               ");
        _sb.AppendLine("       , ABEL_BUCKET_ID                ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_dtBucketsExpiredList);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dtBucketsExpiredList;

    } // GetBucketsExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : Set records for ACCOUNT_BUCKETS_EXPIRED_LIST
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64 BucketId
    //            - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //      
    //   NOTES :
    private static Boolean SetBucketsExpiredList(Int64 BucketId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        //Si el Bucket ha caducado por Fecha, todos los registros de ese bucket se caducan.
        _sb = new StringBuilder();
        _sb.AppendLine("   INSERT INTO   ACCOUNT_BUCKETS_EXPIRED_LIST   ");
        _sb.AppendLine("               (                                ");
        _sb.AppendLine("                 ABEL_BUCKET_ID                 ");
        _sb.AppendLine("               , ABEL_ACCOUNT_ID                ");
        _sb.AppendLine("               , ABEL_VALUE_TO_EXPIRE           ");
        _sb.AppendLine("               , ABEL_DATETIME                  ");
        _sb.AppendLine("               )                                ");
        _sb.AppendLine("        SELECT   CBU_BUCKET_ID                  ");
        _sb.AppendLine("               , CBU_CUSTOMER_ID                ");
        _sb.AppendLine("               , CBU_VALUE                      ");
        _sb.AppendLine("               , GETDATE()                      ");
        _sb.AppendLine("          FROM   CUSTOMER_BUCKET                ");
        _sb.AppendLine("         WHERE   CBU_BUCKET_ID = @pBucketId     ");
        _sb.AppendLine("           AND   CBU_VALUE > 0                  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pBucketId", SqlDbType.BigInt).Value = BucketId;

          _sql_cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // SetBucketsExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : Delete from ACCOUNT_BUCKETS_EXPIRED_LIST for a Bucket and Account
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64 BucketId
    //            - Int64 AccountId
    //            - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean DeleteCustomerBucketFromExpiredList(Int64 BucketId, Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" DELETE FROM  ACCOUNT_BUCKETS_EXPIRED_LIST   ");
        _sb.AppendLine("       WHERE  ABEL_BUCKET_ID = @pBucketId    ");
        _sb.AppendLine("         AND  ABEL_ACCOUNT_ID = @pAccountId  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pBucketId", SqlDbType.BigInt).Value = BucketId;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DeleteCustomerBucketFromExpiredList

    //------------------------------------------------------------------------------
    // PURPOSE : Insert record in ACCOUNT_BUCKETS_EXPIRED_CONTROL
    //
    //  PARAMS :
    //      - INPUT :
    //            - Int64 BucketID
    //            - String DayMonth
    //            - int Year
    //            - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean InsertBucketsExpiredControl(Int64 BucketID, String DayMonth, int Year, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF NOT EXISTS ( SELECT   1                               ");
        _sb.AppendLine("                   FROM   ACCOUNT_BUCKETS_EXPIRED_CONTROL ");
        _sb.AppendLine("                  WHERE   ABEC_BUCKET_ID = @pBucketID     ");
        _sb.AppendLine("                    AND   ABEC_DAY_MONTH = @pDayMonth     ");
        _sb.AppendLine("                    AND   ABEC_YEAR      = @pYear )       ");
        _sb.AppendLine(" BEGIN                                                    ");
        _sb.AppendLine("   INSERT INTO   ACCOUNT_BUCKETS_EXPIRED_CONTROL          ");
        _sb.AppendLine("               (                                          ");
        _sb.AppendLine("                 ABEC_BUCKET_ID                           ");
        _sb.AppendLine("               , ABEC_DAY_MONTH                           ");
        _sb.AppendLine("               , ABEC_YEAR                                ");
        _sb.AppendLine("               )                                          ");
        _sb.AppendLine("        VALUES                                            ");
        _sb.AppendLine("               (                                          ");
        _sb.AppendLine("                 @pBucketID                               ");
        _sb.AppendLine("               , @pDayMonth                               ");
        _sb.AppendLine("               , @pYear                                   ");
        _sb.AppendLine("               )                                          ");
        _sb.AppendLine(" END                                                      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pBucketID", SqlDbType.Int).Value = BucketID;
          _sql_cmd.Parameters.Add("@pDayMonth", SqlDbType.NVarChar, 5).Value = DayMonth;
          _sql_cmd.Parameters.Add("@pYear", SqlDbType.Int).Value = Year;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InsertBucketsExpiredControl

    //------------------------------------------------------------------------------
    // PURPOSE : Set execution on ACCOUNT_BUCKETS_EXPIRED_CONTROL
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean SetExecutionBucketsExpiredControl()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNT_BUCKETS_EXPIRED_CONTROL ");
        _sb.AppendLine("    SET   ABEC_EXECUTION = GETDATE()      ");
        _sb.AppendLine("  WHERE   ABEC_EXECUTION IS NULL          ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (_sql_cmd.ExecuteNonQuery() > 0)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // SetExecutionBucketsExpiredControl

    #endregion Private Methods

  } // BucketsExpirationDayMonth
} // WSI.Common
