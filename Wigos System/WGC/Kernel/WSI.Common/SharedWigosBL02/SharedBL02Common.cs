//-------------------------------------------------------------------------------
// Copyright � 2015 Win Systems Ltd.
//-------------------------------------------------------------------------------
//
// MODULE NAME:   SharedBL02Common.cs
// DESCRIPTION:   
// AUTHOR:        Jos� Mart�nez L�pez
// CREATION DATE: 11-MAR-2015
//
// REVISION HISTORY:
// 
// Date         Author Description
// -----------  ------ ----------------------------------------------------------
// 11-MAR-2015  JML    Initial version (copied/moved from XMLReports.cs)
// 14-MAY-2015  JML    WIG-2326 Error in service charge
// 14-MAY-2015  JML    WIG-2335 Varius problems
// 09-OCT-2015  FAV    Bug 4261 Error about dates in the "Operations Purchase/Sale of chips" report
// 13-JAN-2015  JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 26-APR-2016  RAB    Product Backlog Item 9758: Table (Phase 1): Review reports table
// 21-JUL-2016  RAB    Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to MultiCurrency
// 19-SEP-2016  EOR    PBI 17468: Televisa - Service Charge: Cashier Movements
// 06-OCT-2016  PDM    PBI 17891: Constancias - WS - Job + SP
// 25-OCT-2016  JBP    PBI 19598:Constancias - Mejoras REVIEW Sprint 31
// 22-NOV-2016  JBP    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
// 29-NOV-2016  AMF    Bug 21097:Env�os al CFDI: est� doblando el valor de los impuestos
// 14-DEC-2016  FGB    Bug 5255:Operaciones de compra/venta de fichas: error en el log GUI tras realizar una b�squeda con rango de fecha
// 10-MAR-2017  FGB    PBI 25269: Third TAX - Cash session summary
// 14-JUL-2017  RAB    PBI 28424: WIGOS-2957 Rounding - Reports - Add Remining amount in two reports
//-------------------------------------------------------------------------------

using Microsoft.SqlServer.Server;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public static class SharedBL02Common
  {
    #region enums

    public enum SELECT_TYPE
    {
      by_operation = 0,
      by_qty_operations = 1,
      by_operations_list = 2,
      between_dates = 3
    }

    public enum CLAVE_TIPO_PAGO
    {
      undefined = 0,
      by_points = 1,
      by_cash = 2,
      by_check = 3,
      by_card = 4
    }

    #endregion // enums

    #region struct

    public struct COL_MOVS
    {
      public static readonly int OPERATION_ID = 0;
      public static readonly int DATETIME = 1;
      public static readonly int ACCOUNT_ID = 2;
      public static readonly int OPERATION_CODE = 3;
      public static readonly int CASH_IN_TOTAL = 4;
      public static readonly int CASH_IN_SPLIT1 = 5;
      public static readonly int CASH_IN_SPLIT2 = 6;
      public static readonly int CASH_IN_TAX1 = 7;
      public static readonly int CASH_IN_TAX2 = 8;
      public static readonly int CASH_OUT_TOTAL = 9;
      public static readonly int DEVOLUTION_AMOUNT = 10;
      public static readonly int PRIZE_AMOUNT = 11;
      public static readonly int TAX_ON_PRIZE1_AMOUNT = 12;
      public static readonly int TAX_ON_PRIZE2_AMOUNT = 13;

      public static readonly int CASH_OUT_MONEY = 14;
      public static readonly int CASH_OUT_CHECK = 15;

      public static readonly int HOLDER_RFC = 16;
      public static readonly int HOLDER_CURP = 17;
      public static readonly int HOLDER_NAME = 18;
      public static readonly int ADDRESS = 19;
      public static readonly int CITY = 20;
      public static readonly int ZIP = 21;
      public static readonly int EVIDENCE_HOLDER_RFC = 22;
      public static readonly int EVIDENCE_HOLDER_CURP = 23;
      public static readonly int EVIDENCE_HOLDER_NAME = 24;
      public static readonly int EVIDENCE_ADDRESS = 25;
      public static readonly int EVIDENCE_CITY = 26;
      public static readonly int EVIDENCE_ZIP = 27;
      public static readonly int EVIDENCE = 28;
      public static readonly int DOCUMENT_ID = 29;
      public static readonly int TAX_RETURNING = 30;
      public static readonly int SERVICE_CHARGE = 31;
      public static readonly int DECIMAL_ROUNDING = 32;
      public static readonly int CASHIER_SESSION = 33;
      public static readonly int PRIZE_IN_KIND_AMOUNT = 34;

      // Specific for XmlReportsPSA
      public static readonly int CASHIER_NAME = 35;
      public static readonly int PRIZE_EXPIRED = 36;
      public static readonly int VOUCHER_ID = 37;
      public static readonly int FOLIO = 38;
      public static readonly int PROMOTIONAL_BALANCE = 39;
      public static readonly int PAYMENT_TYPE_KEY = 40;
      public static readonly int PAYMENT_METHOD = 41;
      public static readonly int RECHARGE_FOR_POINTS_PRIZE = 42;

      public static readonly int OPENING_CM_DATE = 43;
      public static readonly int TRACK_DATA_EXTERNAL = 44;
      public static readonly int AC_HOLDER_NAMES = 45;
      public static readonly int AC_HOLDER_SURNAME = 46;

      public static readonly int CASHIER_SESSION_ID = 47;
      public static readonly int CASHIER_USER_ID = 48;
      public static readonly int CASHIER_FULL_NAME = 49;
      public static readonly int CASHIER_FULL_SURNAME = 50;
      public static readonly int CASHIER_CASHIER_ID = 51;
      public static readonly int CASHIER_CASHIER_NAME = 52;
      public static readonly int RECHARGE_TYPE_NUMERIC_KEY = 53;

      public static readonly int TIPO_OPERACION_DESCRIPCION = 54;
      public static readonly int FOLIO2 = 55;
      public static readonly int CASH_IN_TOTAL_WITHOUT_TAX = 56;

      public static readonly int BALANCE_A_FICHAS = 57;
      public static readonly int FICHAS_A_BALANCE = 58;
      public static readonly int CAGE_CURRENCY_TYPE = 59;
      public static readonly int CURRENCY_ISO_CODE = 60;
      public static readonly int PURCHASE_AMOUNT_CURRENCY = 61;
      public static readonly int SALES_AMOUNT_CURRENCY = 62;

      public static readonly int HOLDER_NATIONALITY = 63;
      public static readonly int HOLDER_ENT_FED = 64;
      public static readonly int HOLDER_EMAIL = 65;
      public static readonly int USER_TYPE = 66;

      public static readonly int CASH_IN_TAX3 = 67;
      public static readonly int TAX_ON_PRIZE3_AMOUNT = 68;
      public static readonly int RESIDUAL_AMOUNT = 69;
    }

    private struct COL_AM
    {
      public static readonly int OPERATION_ID = 0;
      public static readonly int DATETIME = 1;
      public static readonly int TYPE = 2;
      public static readonly int ADD_AMOUNT = 3;
      public static readonly int SUB_AMOUNT = 4;
      public static readonly int ACCOUNT_ID = 5;
      public static readonly int HOLDER_NAME = 6;
      public static readonly int RFC = 7;
      public static readonly int CURP = 8;
      public static readonly int HOLDER_ADDRESS = 9;
      public static readonly int HOLDER_CITY = 10;
      public static readonly int HOLDER_ZIP = 11;
      public static readonly int TRACK_DATA = 12;
      public static readonly int EVIDENCE_HOLDER_NAME = 13;
      public static readonly int EVIDENCE_RFC = 14;
      public static readonly int EVIDENCE_CURP = 15;
      public static readonly int EVIDENCE_DOCUMENT_ID1 = 16;
      public static readonly int EVIDENCE_DOCUMENT_ID2 = 17;
      public static readonly int EVIDENCE = 18;
      public static readonly int EVIDENCE_PLAYER_NAME1 = 19;
      public static readonly int EVIDENCE_PLAYER_NAME2 = 20;
      public static readonly int EVIDENCE_PLAYER_NAME3 = 21;
      public static readonly int CASHIER_SESSION = 22;
      public static readonly int CASHIER_NAME = 23;
      public static readonly int OPERATION_TYPE = 24;
      public static readonly int CASH_PAYMENT_AMOUNT = 25;
      public static readonly int CHECK_PAYMENT_AMOUNT = 26;

      public static readonly int OPENING_CM_DATE = 27;
      public static readonly int TRACK_DATA_EXTERNAL = 28;
      public static readonly int AC_HOLDER_NAMES = 29;
      public static readonly int AC_HOLDER_SURNAME = 30;
      public static readonly int CM_CAGE_CURRENCY_TYPE = 31;
      public static readonly int CM_CURRENCY_ISO_CODE = 32;
      public static readonly int INITIAL_BALANCE = 33;
      public static readonly int HOLDER_NATIONALITY = 34;
      public static readonly int HOLDER_ENT_FED = 35;
      public static readonly int HOLDER_EMAIL = 36;
      public static readonly int USER_TYPE = 37;
    }

    #endregion // struct

    #region const

    private const int COL_ACCS_ACCOUNT_ID = 0;
    private const int COL_ACCS_TOTAL_CASHIN = 1;
    private const int COL_ACCS_TOTAL_DEVOLUTION = 2;
    private const int COL_ACCS_TOTAL_PRIZE = 3;
    private const int COL_ACCS_TOTAL_ISR1 = 4;
    private const int COL_ACCS_TOTAL_ISR2 = 5;
    private const int COL_ACCS_RFC = 6;
    private const int COL_ACCS_CURP = 7;
    private const int COL_ACCS_NAME = 8;
    private const int COL_ACCS_ADDRESS = 9;
    private const int COL_ACCS_CITY = 10;
    private const int COL_ACCS_ZIP = 11;
    private const int COL_ACCS_TRACK_DATA = 12;
    private const int COL_ACCS_TOTAL_TAX_RETURNING = 13;
    private const int COL_ACCS_TOTAL_SERVICE_CHARGE = 14;
    private const int COL_ACCS_TOTAL_DECIMAL_ROUNDING = 15;
    private const int COL_ACCS_TOTAL_PRIZE_IN_KIND = 16;

    private const string DATASET_NAME = "MovimientosCuenta";
    private const string TABLE_NAME_MOVEMENT = "Movimiento";
    private const string TABLE_NAME_ACCOUNT = "Cuenta";
    private const string TABLE_NAME_TOTAL_MOVEMENTS = "TotalMovimientos";

    // Set default value for Tipo Pago.
    private const String TIPO_PAGO = "Efectivo";

    private const Int32 MAX_OPERATIONS = 1000;
    private const Char ARRAY_SEPARATOR = ',';

    #endregion // const

    //-------------------------------------------------------------------------------
    // PURPOSE: Generate DataTable with data from diferents metods
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      DataTable containing all data
    // NOTE:
    //     This 4th procedures only can call from sql server
    //-------------------------------------------------------------------------------
    [SqlProcedure]
    public static void GetSalesAndPaymentsByOperation(Int64 OperationId)
    {

      GetSalesAndPayments(SELECT_TYPE.by_operation, OperationId, MAX_OPERATIONS, null, DateTime.MinValue, DateTime.MaxValue);

    } // GetSalesAndPaymentsByOperation

    [SqlProcedure]
    public static void GetSalesAndPaymentsByQtyOperations(Int64 OperationId, Int32 NoOperations)
    {
      //TODO: If operationId is equal to zero or minor; the sql procedure return an error. This is a temporary solution. Pending to revised.
      if (OperationId <= 0)
      {
        OperationId = 1;
      }

      GetSalesAndPayments(SELECT_TYPE.by_qty_operations, OperationId, NoOperations, null, DateTime.MinValue, DateTime.MaxValue);
    } // GetSalesAndPaymentsByQtyOperations

    [SqlProcedure]
    public static void GetSalesAndPaymentsByOperationList(String OperationIds)
    {
      String[] _operation_ids;

      try
      {
        //_operation_ids = new List<string>();
        _operation_ids = OperationIds.Split(ARRAY_SEPARATOR);

        if (_operation_ids == null || _operation_ids.Length == 0 || _operation_ids.Length > MAX_OPERATIONS)
        {
          return;
        }
      }
      catch
      {
        return;
      }

      GetSalesAndPayments(SELECT_TYPE.by_operations_list, 0, MAX_OPERATIONS, OperationIds, DateTime.MinValue, DateTime.MaxValue);
    } // GetSalesAndPaymentsByOperationList

    [SqlProcedure]
    public static void GetSalesAndPaymentsBetweenDates(DateTime DateFrom, DateTime DateTo)
    {
      GetSalesAndPayments(SELECT_TYPE.between_dates, 0, MAX_OPERATIONS, null, DateFrom, DateTo);
    } // GetSalesAndPaymentsBetweenDates

    private static void SendDataTableOverPipe(DataTable tbl)
    {
      // Build our record schema
      List<SqlMetaData> OutputColumns = new List<SqlMetaData>(tbl.Columns.Count);
      foreach (DataColumn col in tbl.Columns)
      {
        if (col.DataType == typeof(String))
        {
          SqlMetaData OutputColumn = new SqlMetaData(col.ColumnName, GetSqlDbType(col.DataType.ToString()), 255);
          OutputColumns.Add(OutputColumn);
        }
        else if (col.DataType == typeof(Decimal))
        {
          SqlMetaData OutputColumn = new SqlMetaData(col.ColumnName, GetSqlDbType(col.DataType.ToString()), 18, 4);
          OutputColumns.Add(OutputColumn);
        }
        else
        {
          SqlMetaData OutputColumn = new SqlMetaData(col.ColumnName, GetSqlDbType(col.DataType.ToString()));
          OutputColumns.Add(OutputColumn);
        }
      }

      // Build our SqlDataRecord and start the results
      SqlDataRecord record = new SqlDataRecord(OutputColumns.ToArray());
      SqlContext.Pipe.SendResultsStart(record);

      // Now send all the rows
      foreach (DataRow row in tbl.Rows)
      {
        for (int col = 0; col < tbl.Columns.Count; col++)
        {
          record.SetValue(col, row.ItemArray[col]);
        }

        SqlContext.Pipe.SendResultsRow(record);
      }

      // And complete the results
      SqlContext.Pipe.SendResultsEnd();
    } // SendDataTableOverPipe

    private static SqlDbType GetSqlDbType(String Type)
    {
      switch (Type)
      {
        case "System.Boolean":
          return SqlDbType.Bit;
        case "System.DateTime":
          return SqlDbType.DateTime;
        case "System.Decimal":
          return SqlDbType.Decimal;
        case "System.Double":
          return SqlDbType.Float;
        case "System.Guid":
          return SqlDbType.UniqueIdentifier;
        case "System.Int16":
          return SqlDbType.SmallInt;
        case "System.Int32":
          return SqlDbType.Int;
        case "System.Int64":
          return SqlDbType.BigInt;
        case "System.String":
          return SqlDbType.NVarChar;
        case "System.Byte[]":
          return SqlDbType.VarBinary;
        default:
          return SqlDbType.Variant;
      }
    } // GetSqlDbType

    //-------------------------------------------------------------------------------
    // PURPOSE: Generate DataTable with data from diferents metods
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      DataTable containing all data
    // NOTE:
    //     This 4th procedures only can call from C#
    //-------------------------------------------------------------------------------

    public static Boolean GetSalesAndPaymentsByOperationDirect(Int64 OperationId, out DataTable DTSalesAndPays)
    {
      DTSalesAndPays = new DataTable();

#if !SQL_WIGOS_BL02
      DataTable DTSalesAndPayments;

      try
      {
        using (SqlConnection _conn = WGDB.Connection())
        {
          GetSalesAndPayments(SELECT_TYPE.by_operation, OperationId, MAX_OPERATIONS, null, DateTime.MinValue, DateTime.MaxValue, _conn, out DTSalesAndPayments);

          DTSalesAndPays = DTSalesAndPayments.Copy();
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#endif

      return false;
    } // GetSalesAndPaymentsByOperationDirect

    public static Boolean GetSalesAndPaymentsByQtyOperationsDirect(Int64 OperationId, Int32 NoOperations, out DataTable DTSalesAndPays)
    {
      DTSalesAndPays = new DataTable();

#if !SQL_WIGOS_BL02
      DataTable DTSalesAndPayments;

      try
      {
        using (SqlConnection _conn = WGDB.Connection())
        {
          GetSalesAndPayments(SELECT_TYPE.by_qty_operations, OperationId, NoOperations, null, DateTime.MinValue, DateTime.MaxValue, _conn, out DTSalesAndPayments);

          DTSalesAndPays = DTSalesAndPayments.Copy();
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#endif

      return false;
    } // GetSalesAndPaymentsByQtyOperationsDirect

    public static Boolean GetSalesAndPaymentsByOperationListDirect(String OperationIds, out DataTable DTSalesAndPays)
    {
      DTSalesAndPays = new DataTable();

#if !SQL_WIGOS_BL02

      String[] _operation_ids;
      DataTable DTSalesAndPayments;

      try
      {
        _operation_ids = OperationIds.Split(ARRAY_SEPARATOR);

        if (_operation_ids == null || _operation_ids.Length == 0 || _operation_ids.Length > MAX_OPERATIONS)
        {
          return false;
        }

        using (SqlConnection _conn = WGDB.Connection())
        {
          GetSalesAndPayments(SELECT_TYPE.by_operations_list, 0, MAX_OPERATIONS, OperationIds, DateTime.MinValue, DateTime.MaxValue, _conn, out DTSalesAndPayments);

          DTSalesAndPays = DTSalesAndPayments.Copy();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#endif

      return false;
    } // GetSalesAndPaymentsByOperationListDirect

    public static Boolean GetSalesAndPaymentsBetweenDatesDirect(DateTime DateFrom, DateTime DateTo, out DataTable DTSalesAndPays)
    {
      DTSalesAndPays = new DataTable();

#if !SQL_WIGOS_BL02

      DataTable DTSalesAndPayments;

      try
      {
        if (DateFrom == null || DateTo == null)
        {
          return false;
        }

        using (SqlConnection _conn = WGDB.Connection())
        {
          GetSalesAndPayments(SELECT_TYPE.between_dates, 0, MAX_OPERATIONS, null, DateFrom, DateTo, _conn, out DTSalesAndPayments);

          DTSalesAndPays = DTSalesAndPayments.Copy();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#endif

      return false;
    } // GetSalesAndPaymentsBetweenDatesDirect

    private static void GetSalesAndPayments(SELECT_TYPE SelectType, Int64 OperationId, Int32 NoOperations, String OperationIds, DateTime FromDate, DateTime ToDate)
    {
      DataTable DTSalesAndPayments;
      Int32 _last_desired_column;

      DTSalesAndPayments = null;

      SqlConnection _conn = new SqlConnection(@"context connection=true");
      _conn.Open();

      if (!GetSalesAndPayments(SelectType, OperationId, NoOperations, OperationIds, FromDate, ToDate, _conn, out DTSalesAndPayments))
      {
        return;
      }

      _conn.Close();

      _last_desired_column = DTSalesAndPayments.Columns.Count;

      if (SelectType == SELECT_TYPE.by_qty_operations)
      {
        DTSalesAndPayments.Columns["IdOperacion"].SetOrdinal(0);
        DTSalesAndPayments.Columns["Fecha"].SetOrdinal(1);
        DTSalesAndPayments.Columns["FechaHora"].SetOrdinal(2);
        DTSalesAndPayments.Columns["Folio"].SetOrdinal(3);
        DTSalesAndPayments.Columns["Folio2"].SetOrdinal(4);
        DTSalesAndPayments.Columns["TipoOperacion"].SetOrdinal(5);
        DTSalesAndPayments.Columns["TipoOperacionDescripcion"].SetOrdinal(6);

        DTSalesAndPayments.Columns["TotalRecarga"].SetOrdinal(7);
        DTSalesAndPayments.Columns["EmpresaA"].SetOrdinal(8);
        DTSalesAndPayments.Columns["EmpresaB"].SetOrdinal(9);
        DTSalesAndPayments.Columns["Impuesto1"].SetOrdinal(10);
        DTSalesAndPayments.Columns["Impuesto2"].SetOrdinal(11);
        DTSalesAndPayments.Columns["Impuesto3"].SetOrdinal(12);
        DTSalesAndPayments.Columns["RecargaPorPuntos"].SetOrdinal(13);

        DTSalesAndPayments.Columns["BalanceAFichas"].SetOrdinal(14);
        DTSalesAndPayments.Columns["FichasABalance"].SetOrdinal(15);

        DTSalesAndPayments.Columns["TotalRetiro"].SetOrdinal(16);
        DTSalesAndPayments.Columns["TotalRecargaSinImpuestos"].SetOrdinal(17);
        DTSalesAndPayments.Columns["Devolucion"].SetOrdinal(18);
        DTSalesAndPayments.Columns["Premio"].SetOrdinal(19);
        DTSalesAndPayments.Columns["ISR1"].SetOrdinal(20);
        DTSalesAndPayments.Columns["ISR2"].SetOrdinal(21);
        DTSalesAndPayments.Columns["ISR3"].SetOrdinal(22);

        DTSalesAndPayments.Columns["CargoPorServicio"].SetOrdinal(23);
        DTSalesAndPayments.Columns["RedondeoPorRetencion"].SetOrdinal(24);
        DTSalesAndPayments.Columns["RedondeoPorDecimales"].SetOrdinal(25);

        DTSalesAndPayments.Columns["PremioEspecie"].SetOrdinal(26);
        DTSalesAndPayments.Columns["RetiroEnEfectivo"].SetOrdinal(27);
        DTSalesAndPayments.Columns["RetiroEnCheque"].SetOrdinal(28);

        DTSalesAndPayments.Columns["TarjetaCliente"].SetOrdinal(29);
        DTSalesAndPayments.Columns["NoCuenta"].SetOrdinal(30);
        DTSalesAndPayments.Columns["Nombres"].SetOrdinal(31);
        DTSalesAndPayments.Columns["Apellidos"].SetOrdinal(32);
        DTSalesAndPayments.Columns["Shift_Id"].SetOrdinal(33);
        DTSalesAndPayments.Columns["Cajera_Id"].SetOrdinal(34);
        DTSalesAndPayments.Columns["CajaNombres"].SetOrdinal(35);
        DTSalesAndPayments.Columns["CajaApellidos"].SetOrdinal(36);
        DTSalesAndPayments.Columns["Caja_Id"].SetOrdinal(37);
        DTSalesAndPayments.Columns["Caja_Name"].SetOrdinal(38);

        _last_desired_column = 39;
      }
      else
      {
        _last_desired_column = 49;
      }

      while (DTSalesAndPayments.Columns.Count > _last_desired_column)
      {
        DTSalesAndPayments.Columns.RemoveAt(_last_desired_column);
      }

      SendDataTableOverPipe(DTSalesAndPayments);

    } // GetSalesAndPayments

    public static Boolean GetSalesAndPaymentsForCFDI(Int32 NoOperations, String OperationIds, SqlTransaction Trx, out DataTable DTSalesAndPayments)
    {

      DataTable _cashier_voucher;
      GeneralParam.Dictionary _general_param;

      try
      {
        DTSalesAndPayments = new DataTable();

        NoOperations = Math.Min(Math.Max(NoOperations, 0), MAX_OPERATIONS);

        _general_param = GeneralParam.Dictionary.Create(Trx);

        using (SqlCommand _sql_command = new SqlCommand())
        {
          _sql_command.Connection = Trx.Connection;
          _sql_command.Transaction = Trx;

          if (!GetSelect(SELECT_TYPE.by_operations_list, 0, NoOperations, OperationIds, DateTime.MinValue, DateTime.MaxValue, _sql_command, _general_param))
          {
            return false;
          }

          if (!GetVoucherFromCashierMovement(SELECT_TYPE.by_operations_list, 0, NoOperations, OperationIds, DateTime.MinValue, DateTime.MaxValue, out _cashier_voucher, Trx))
          {
            return false;
          }

          using (SqlDataReader _sql_reader = _sql_command.ExecuteReader())
          {
            if (!GetTranspose(NoOperations, _sql_reader, _sql_command, _cashier_voucher, _general_param, Trx, out DTSalesAndPayments))
            {
              return false;
            }
          }
        }

        return true;
      }

#if !SQL_WIGOS_BL02
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        DTSalesAndPayments = null;
        return false;
      }
#else
      catch
      {
        DTSalesAndPayments = null;
        return false;
      }
#endif
    }

    private static Boolean GetSalesAndPayments(SELECT_TYPE SelectType, Int64 OperationId, Int32 NoOperations, String OperationIds, DateTime FromDate, DateTime ToDate, SqlConnection _conn, out DataTable DTSalesAndPayments)
    {
      DataTable _cashier_voucher;
      GeneralParam.Dictionary _general_param;

      try
      {
        DTSalesAndPayments = new DataTable();

        NoOperations = Math.Min(Math.Max(NoOperations, 0), MAX_OPERATIONS);

        if (SelectType == SELECT_TYPE.between_dates)
        {
          NoOperations = int.MaxValue;
        }

        using (SqlTransaction _trx = _conn.BeginTransaction())
        {
          _general_param = GeneralParam.Dictionary.Create(_trx);

          using (SqlCommand _sql_command = new SqlCommand())
          {
            _sql_command.Connection = _trx.Connection;
            _sql_command.Transaction = _trx;

            if (!GetSelect(SelectType, OperationId, NoOperations, OperationIds, FromDate, ToDate, _sql_command, _general_param))
            {
              return false;
            }

            if (!GetVoucherFromCashierMovement(SelectType, OperationId, NoOperations, OperationIds, FromDate, ToDate, out _cashier_voucher, _trx))
            {
              return false;
            }

            using (SqlDataReader _sql_reader = _sql_command.ExecuteReader())
            {
              if (!GetTranspose(NoOperations, _sql_reader, _sql_command, _cashier_voucher, _general_param, _trx, out DTSalesAndPayments))
              {
                return false;
              }
            }
          }
        }

        return true;
      }

#if !SQL_WIGOS_BL02
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        DTSalesAndPayments = null;
        return false;
      }
#else
      catch
      {
        DTSalesAndPayments = null;
        return false;
      }
#endif
    }

    // PURPOSE: Return select command for data
    //
    //  PARAMS:
    //     - INPUT:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private static Boolean GetSelect(SELECT_TYPE SelectType, Int64 OperationId, Int32 NoOperations, String OperationIds, DateTime FromDate, DateTime ToDate, SqlCommand SqlCommand, GeneralParam.Dictionary GeneralParam)
    {
      StringBuilder _str_sql;
      StringBuilder _str_where_mov_types;

      _str_sql = new StringBuilder();
      _str_where_mov_types = new StringBuilder();

      try
      {
        _str_sql.AppendLine("SELECT    CM_OPERATION_ID ");         //col 0
        _str_sql.AppendLine("        , CM_DATE ");                 //col 1
        _str_sql.AppendLine("        , CM_TYPE ");                 //col 2
        _str_sql.AppendLine("        , CM_ADD_AMOUNT ");           //col 3
        _str_sql.AppendLine("        , CM_SUB_AMOUNT ");           //col 4          
        _str_sql.AppendLine("        , CM_ACCOUNT_ID                                          -- Cuenta ");                //col 5
        _str_sql.AppendLine("        , AC_HOLDER_NAME                                         -- Nombre ");                //col 6
        _str_sql.AppendLine("        , ISNULL (AC_HOLDER_ID1, AC_HOLDER_ID)  AC_HOLDER_ID1    -- RFC ");                   //col 7
        _str_sql.AppendLine("        , AC_HOLDER_ID2                                          -- CURP ");                  //col 8
        _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_01 + ' ' + AC_HOLDER_ADDRESS_02 + ' ' + AC_HOLDER_ADDRESS_03");   //col 9
        _str_sql.AppendLine("        , AC_HOLDER_CITY ");                                                                  //col 10
        _str_sql.AppendLine("        , AC_HOLDER_ZIP ");                                                                   //col 11
        _str_sql.AppendLine("        , AC_TRACK_DATA ");                                                                   //col 12
        _str_sql.AppendLine("        -- EVIDENCE --");
        _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME,  AC_HOLDER_NAME)                             EVIDENCE_AC_HOLDER_NAME   -- Constancia.Nombre ");     //col 13
        _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_ID1,   ISNULL (AC_HOLDER_ID1, AC_HOLDER_ID))       EVIDENCE_AC_HOLDER_ID1    -- Constancia.RFC ");        //col 14
        _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_ID2,   AC_HOLDER_ID2)                              EVIDENCE_AC_HOLDER_ID2    -- Constancia.CURP ");       //col 15
        _str_sql.AppendLine("        , AMP_DOCUMENT_ID1                                                                               -- Documento 37-A ISR1 ");   //col 16
        _str_sql.AppendLine("        , AMP_DOCUMENT_ID2                                                                               -- Documento 37-A ISR2 ");   //col 17
        _str_sql.AppendLine("        , convert (bit, case when AMP_OPERATION_ID is null then 0 else 1 end)  EVIDENCE                                         ");   //col 18
        _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME1, '') AMP_PLAYER_NAME1 ");       // col 19
        _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME2, '') AMP_PLAYER_NAME2 ");       // col 20
        _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME3, '') AMP_PLAYER_NAME3 ");       // col 21
        _str_sql.AppendLine("        , CM_SESSION_ID ");                                       // col 22
        _str_sql.AppendLine("        , CM_CASHIER_NAME");                                      // col 23
        _str_sql.AppendLine("        , AO_CODE");                                                                                              // col 24
        _str_sql.AppendLine("        , ISNULL(APO_CASH_PAYMENT, 0) APO_CASH_PAYMENT");                                                         // col 25
        _str_sql.AppendLine("        , ISNULL(APO_CHECK_PAYMENT, 0) APO_CHECK_PAYMENT");                                                       // col 26
        _str_sql.AppendLine("        , dbo.Opening(0, CM_DATE)  ");                             // col 27
        _str_sql.AppendLine("        , dbo.TrackDataToExternal(AC_TRACK_DATA) ");               // col 28
        _str_sql.AppendLine("        , ISNULL(AC_HOLDER_NAME3, '') + ' ' + ISNULL(AC_HOLDER_NAME4, '') ");              // col 29
        _str_sql.AppendLine("        , ISNULL(AC_HOLDER_NAME1, '') + ' ' + ISNULL(AC_HOLDER_NAME2, '') ");              // col 30
        _str_sql.AppendLine("        , CM_CAGE_CURRENCY_TYPE");                                // col 31
        _str_sql.AppendLine("        , CM_CURRENCY_ISO_CODE");                                 // col 32
        _str_sql.AppendLine("        , CM_INITIAL_BALANCE");                                   // col 33

        _str_sql.AppendLine("        , CO_ISO2");                                               // col 34
        _str_sql.AppendLine("        , FS_CFDI_ID");                                            // col 35
        _str_sql.AppendLine("        , AC_HOLDER_EMAIL_01");                                    // col 36
        _str_sql.AppendLine("        , AC_USER_TYPE");                                          // col 37

        _str_sql.AppendLine("  FROM    CASHIER_MOVEMENTS with (index (IX_cm_operation_id))");
        _str_sql.AppendLine("          INNER JOIN ACCOUNTS               ON CM_ACCOUNT_ID   = AC_ACCOUNT_ID         ");
        _str_sql.AppendLine("          LEFT  JOIN ACCOUNT_MAJOR_PRIZES   ON CM_OPERATION_ID = AMP_OPERATION_ID      ");
        _str_sql.AppendLine("          LEFT  JOIN ACCOUNT_OPERATIONS     ON CM_OPERATION_ID = AO_OPERATION_ID       ");
        _str_sql.AppendLine("          LEFT  JOIN ACCOUNT_PAYMENT_ORDERS ON CM_OPERATION_ID = APO_OPERATION_ID      ");
        _str_sql.AppendLine("          LEFT  JOIN COUNTRIES              ON CO_COUNTRY_ID   = AC_HOLDER_NATIONALITY AND CO_LANGUAGE_ID = 10 ");
        _str_sql.AppendLine("          LEFT  JOIN FEDERAL_STATES         ON FS_STATE_ID     = AC_HOLDER_FED_ENTITY  AND FS_COUNTRY_ISO_CODE2 = CO_ISO2 ");

        _str_sql.AppendLine(" WHERE    CM_OPERATION_ID IN ( SELECT  TOP (@pNoOperations) CM_OPERATION_ID ");

        SqlCommand.Parameters.Add("@pNoOperations", SqlDbType.BigInt).Value = NoOperations;

        switch (SelectType)
        {
          case SELECT_TYPE.by_operation:
            SqlCommand.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

            _str_sql.AppendLine("           FROM  CASHIER_MOVEMENTS WITH (INDEX (IX_cm_operation_id))   ");
            _str_sql.AppendLine("          WHERE  CM_OPERATION_ID = @pOperationId   ");

            break;
          case SELECT_TYPE.by_qty_operations:
            SqlCommand.Parameters.Add("@pOperationFrom", SqlDbType.BigInt).Value = OperationId;

            _str_sql.AppendLine("           FROM  CASHIER_MOVEMENTS WITH (INDEX (IX_cm_operation_id))   ");
            _str_sql.AppendLine("          WHERE  CM_OPERATION_ID >= @pOperationFrom  ");

            break;
          case SELECT_TYPE.by_operations_list:
            _str_sql.AppendLine("           FROM  CASHIER_MOVEMENTS WITH (INDEX (IX_cm_operation_id))   ");
            _str_sql.AppendLine("          WHERE  CM_OPERATION_ID in ( " + OperationIds + " )   ");

            break;
          case SELECT_TYPE.between_dates:
            SqlCommand.Parameters.Add("@pFromDate", SqlDbType.DateTime).Value = FromDate;
            SqlCommand.Parameters.Add("@pToDate", SqlDbType.DateTime).Value = ToDate;

            _str_sql.AppendLine("           FROM  CASHIER_MOVEMENTS WITH (INDEX (IX_cm_date_type))   ");
            _str_sql.AppendLine("          WHERE  CM_DATE >= @pFromDate ");
            _str_sql.AppendLine("            AND  CM_DATE < @pToDate ");

            break;
        }

        _str_sql.AppendLine("  #WHERE_ACCOUNT_MOVEMENTS# ");
        _str_sql.AppendLine("                             GROUP BY  CM_OPERATION_ID ) ");
        _str_sql.AppendLine("  #WHERE_ACCOUNT_MOVEMENTS# ");

        _str_sql.AppendLine(" ORDER BY CM_OPERATION_ID ");

        // Where for cashier movements 
        SqlCommand.Parameters.Add("@pTypeDevolutionSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.DEV_SPLIT1;
        SqlCommand.Parameters.Add("@pTypeCancelSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.CANCEL_SPLIT1;

        SqlCommand.Parameters.Add("@pTypeCashInSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_IN_SPLIT1;
        SqlCommand.Parameters.Add("@pTypeCashInMBSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1;
        SqlCommand.Parameters.Add("@pTypeCashInNASplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1;
        SqlCommand.Parameters.Add("@pTypeRechargeForPointsSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1;

        SqlCommand.Parameters.Add("@pTypeCashInSplit2", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_IN_SPLIT2;
        SqlCommand.Parameters.Add("@pTypeCashInNASplit2", SqlDbType.Int).Value = CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2;
        SqlCommand.Parameters.Add("@pTypeCashInMBSplit2", SqlDbType.Int).Value = CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2;
        SqlCommand.Parameters.Add("@pTypeRechargeForPointsSplit2", SqlDbType.Int).Value = CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2;

        SqlCommand.Parameters.Add("@pTypeCashInTaxSplit", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT1;
        SqlCommand.Parameters.Add("@pTypeCashInTaxSplit2", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT2;
        SqlCommand.Parameters.Add("@pTypeCashOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_OUT;

        SqlCommand.Parameters.Add("@pTypePrizes", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZES;
        SqlCommand.Parameters.Add("@pTypeTaxOnPrize1", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_ON_PRIZE1;
        SqlCommand.Parameters.Add("@pTypeTaxOnPrize2", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_ON_PRIZE2;
        SqlCommand.Parameters.Add("@pTypeTaxOnPrize3", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_ON_PRIZE3;

        SqlCommand.Parameters.Add("@pTypePrizeInKind1Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS;
        SqlCommand.Parameters.Add("@pTypeTax1OnPrizeKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1;
        SqlCommand.Parameters.Add("@pTypeTax2OnPrizeKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2;
        SqlCommand.Parameters.Add("@pTypeTax3OnPrizeKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3;

        SqlCommand.Parameters.Add("@pTypePrizeInKind2Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS;
        SqlCommand.Parameters.Add("@pTypeTax1OnPrizeKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1;
        SqlCommand.Parameters.Add("@pTypeTax2OnPrizeKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2;
        SqlCommand.Parameters.Add("@pTypeTax3OnPrizeKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3;

        SqlCommand.Parameters.Add("@pTypePrizeReInKind1Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS;
        SqlCommand.Parameters.Add("@pTypeTax1OnPrizeReKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1;
        SqlCommand.Parameters.Add("@pTypeTax2OnPrizeReKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2;
        SqlCommand.Parameters.Add("@pTypeTax3OnPrizeReKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3;

        SqlCommand.Parameters.Add("@pTypePrizeReInKind2Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS;
        SqlCommand.Parameters.Add("@pTypeTax1OnPrizeReKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1;
        SqlCommand.Parameters.Add("@pTypeTax2OnPrizeReKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2;
        SqlCommand.Parameters.Add("@pTypeTax3OnPrizeReKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3;

        // Input Payments types:
        SqlCommand.Parameters.Add("@pTypeBankCardPayment", SqlDbType.Int).Value = CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1;
        SqlCommand.Parameters.Add("@pTypeBankCardPaymentCredit", SqlDbType.Int).Value = CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1;
        SqlCommand.Parameters.Add("@pTypeBankCardPaymentDebit", SqlDbType.Int).Value = CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1;

        SqlCommand.Parameters.Add("@pCompanyBTypeBankCardPayment", SqlDbType.Int).Value = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1;
        SqlCommand.Parameters.Add("@pCompanyBTypeBankCardPaymentCredit", SqlDbType.Int).Value = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1;
        SqlCommand.Parameters.Add("@pCompanyBTypeBankCardPaymentDebit", SqlDbType.Int).Value = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1;

        SqlCommand.Parameters.Add("@pTypeCheckPayment", SqlDbType.Int).Value = CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1;
        SqlCommand.Parameters.Add("@pCompanyBTypeCheckPayment", SqlDbType.Int).Value = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1;

        // Output Payments types :
        SqlCommand.Parameters.Add("@pTypeCheckPaymentRefund", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHECK_PAYMENT;
        SqlCommand.Parameters.Add("@pTypeTaxReturning", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON;
        SqlCommand.Parameters.Add("@pTypeDecimalRounding", SqlDbType.Int).Value = CASHIER_MOVEMENT.DECIMAL_ROUNDING;
        SqlCommand.Parameters.Add("@pTypeRechargeForPointsPrize", SqlDbType.Int).Value = CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE;

        // DDM & JML 12-MAY-2015: DEFECT WIG-2326 - Error, service charge not include
        SqlCommand.Parameters.Add("@pTypeServiceCharge", SqlDbType.Int).Value = CASHIER_MOVEMENT.SERVICE_CHARGE;

        // EOR 19-SEP-2016
        SqlCommand.Parameters.Add("@pTypeServiceChargeA", SqlDbType.Int).Value = CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A;

        //SqlCommand.Parameters.Add("@pTypeOpenSession", SqlDbType.Int).Value = CASHIER_MOVEMENT.OPEN_SESSION;
        //SqlCommand.Parameters.Add("@pTypeFillerIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.FILLER_IN;
        //SqlCommand.Parameters.Add("@pTypeRedemPrizeExpired", SqlDbType.Int).Value = CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED;

        SqlCommand.Parameters.Add("@pTypePromotionRedeemable", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE;
        //SqlCommand.Parameters.Add("@pOperationUndoNone", SqlDbType.Int).Value = UndoStatus.None;

        // DHA 19-MAR-2015: Input chips types
        SqlCommand.Parameters.Add("@pChipsSaleTotal", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_TOTAL;
        SqlCommand.Parameters.Add("@pChipsRemainingAmount", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT;
        SqlCommand.Parameters.Add("@pChipsPurchaseTotal", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL;
        SqlCommand.Parameters.Add("@pChipsSaleTotalExchange", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE;
        SqlCommand.Parameters.Add("@pChipsPurchaseTotalExchange", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE;

        SqlCommand.Parameters.Add("@pTypeCashIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_IN;

        SqlCommand.Parameters.Add("@pTypeDevolutionSplit2", SqlDbType.Int).Value = CASHIER_MOVEMENT.DEV_SPLIT2;
        SqlCommand.Parameters.Add("@pTypeCancelSplit2", SqlDbType.Int).Value = CASHIER_MOVEMENT.CANCEL_SPLIT2;

        SqlCommand.Parameters.Add("@pChipsSaleRegisterTotal", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL;

        // Parameters for filter tables operations
        SqlCommand.Parameters.Add("@pOCodeChipsSale", SqlDbType.Int).Value = OperationCode.CHIPS_SALE;
        SqlCommand.Parameters.Add("@pOCodeChipsSaleWithRecharge", SqlDbType.Int).Value = OperationCode.CHIPS_SALE_WITH_RECHARGE;
        SqlCommand.Parameters.Add("@pOCodeChipsPurchase", SqlDbType.Int).Value = OperationCode.CHIPS_PURCHASE;
        SqlCommand.Parameters.Add("@pOCodeChipsPurchaseWithCashOut", SqlDbType.Int).Value = OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT;


        _str_where_mov_types.AppendLine(" AND    CM_TYPE IN( @pTypeDevolutionSplit1, @pTypeCancelSplit1        ");
        _str_where_mov_types.AppendLine("                   , @pTypeDevolutionSplit2, @pTypeCancelSplit2        ");
        _str_where_mov_types.AppendLine("                   , @pTypePrizes, @pTypeTaxOnPrize1, @pTypeTaxOnPrize2, @pTypeTaxOnPrize3 ");

        _str_where_mov_types.AppendLine("                   , @pTypeCashInSplit2, @pTypeCashInNASplit2 ");
        _str_where_mov_types.AppendLine("                   , @pTypeCashInMBSplit2, @pTypeRechargeForPointsSplit2 ");

        _str_where_mov_types.AppendLine("                   , @pTypeCashInTaxSplit, @pTypeCashInTaxSplit2 ");
        _str_where_mov_types.AppendLine("                   , @pTypeCashOut ");

        if (GeneralParam.GetBoolean("CashDesk.Draw", "ReportUNRFromSP_SalesAndPayment", true))
        {
          // Movements to report DrawCash
          _str_where_mov_types.AppendLine("         , @pTypePrizeInKind1Gross, @pTypeTax1OnPrizeKind1, @pTypeTax2OnPrizeKind1, @pTypeTax3OnPrizeKind1 ");
          _str_where_mov_types.AppendLine("         , @pTypePrizeInKind2Gross, @pTypeTax1OnPrizeKind2, @pTypeTax2OnPrizeKind2, @pTypeTax3OnPrizeKind2 ");

          _str_where_mov_types.AppendLine("         , @pTypePrizeReInKind1Gross, @pTypeTax1OnPrizeReKind1, @pTypeTax2OnPrizeReKind1, @pTypeTax3OnPrizeReKind1 ");
          _str_where_mov_types.AppendLine("         , @pTypePrizeReInKind2Gross, @pTypeTax1OnPrizeReKind2, @pTypeTax2OnPrizeReKind2, @pTypeTax3OnPrizeReKind2 ");
        }

        // Payment types.
        _str_where_mov_types.AppendLine("         , @pTypeBankCardPayment,         @pTypeBankCardPaymentCredit,         @pTypeBankCardPaymentDebit ");
        _str_where_mov_types.AppendLine("         , @pCompanyBTypeBankCardPayment, @pCompanyBTypeBankCardPaymentCredit, @pCompanyBTypeBankCardPaymentDebit ");

        _str_where_mov_types.AppendLine("         , @pTypeCheckPayment, @pCompanyBTypeCheckPayment, @pTypeCheckPaymentRefund ");

        _str_where_mov_types.AppendLine("         , @pTypeDecimalRounding ");

        // DDM & jml 12-MAY-2015: DEFECT WIG-2335 
        //StrSql.AppendLine("         , @pTypeRedemPrizeExpired ");
        _str_where_mov_types.AppendLine("         , @pTypeServiceCharge ");
        _str_where_mov_types.AppendLine("         , @pTypeServiceChargeA ");     // EOR 21-SEP-2016

        _str_where_mov_types.AppendLine("         , @pTypeRechargeForPointsPrize ");

        _str_where_mov_types.AppendLine("         , @pTypeCashInSplit1, @pTypeCashInMBSplit1, @pTypeCashInNASplit1 ");
        _str_where_mov_types.AppendLine("         , @pTypeRechargeForPointsSplit1, @pTypePromotionRedeemable ");
        _str_where_mov_types.AppendLine("         , @pTypeCashIn ");

        // Chips movements
        _str_where_mov_types.AppendLine("         , @pChipsSaleTotal, @pChipsPurchaseTotal, @pChipsSaleTotalExchange, @pChipsPurchaseTotalExchange  ");
        _str_where_mov_types.AppendLine("         , @pChipsRemainingAmount ");

        // Chips Sale Register 
        _str_where_mov_types.AppendLine("         , @pChipsSaleRegisterTotal ");

        // Include TaxReturning
        _str_where_mov_types.AppendLine("         , @pTypeTaxReturning ");
        _str_where_mov_types.AppendLine(" ) ");

        // se pone el where correcto reemplazando #WHERE_ACCOUNT_MOVEMENTS# situado en el select
        SqlCommand.CommandText = _str_sql.ToString().Replace("#WHERE_ACCOUNT_MOVEMENTS#", _str_where_mov_types.ToString());

        //// TODO A REVISAR QUE SE HACE
        //// Not include chips operations
        //StrSql.AppendLine(" AND ISNULL(AO_CODE, 0) NOT IN ( @pOCodeChipsSale,     @pOCodeChipsSaleWithRecharge, ");
        //StrSql.AppendLine("                                 @pOCodeChipsPurchase, @pOCodeChipsPurchaseWithCashOut ) ");

        return true;
      }
      catch
      {
        return false;
      }

    } // GetSelect

    private static Boolean GetTranspose(Int32 NoOperations, SqlDataReader SqlReader, SqlCommand _sql_command, DataTable _cashier_voucher, GeneralParam.Dictionary GeneralParam, SqlTransaction Trx, out DataTable DTOperations)
    {
      Int64 _am_operation_id;
      Int64 _cashier_session_id;
      Int64 _last_operation_id;
      Boolean _operation_has_unr_credit;
      DataRow _unr_row;

      DataTable _dt_movs;

      DataRow _row_movement;
      Decimal _devolution;
      Int64 _account_id;

      DateTime _aux_date;
      String _name;
      String _last_name1;
      String _last_name2;

      Int32 _amount_column;

#if !SQL_WIGOS_BL02
      Boolean _log_error_written;
#endif

      Boolean _is_reporting_redeemable;

      String _clave_tipo_pago_default_value;
      String _clave_tipo_pago_val;
      String _forma_pago_default_value;
      String _forma_pago_val;

      Int32 _num_operations;

      _last_operation_id = Int64.MinValue;
      _operation_has_unr_credit = false;

      _dt_movs = new DataTable();

      _row_movement = null;
      _devolution = 0;
      _num_operations = 0;

      CreateCashierMovementsColumns(ref _dt_movs);

      _clave_tipo_pago_default_value = String.Empty;
      _forma_pago_default_value = String.Empty;

      _is_reporting_redeemable = GeneralParam.GetBoolean("PSAClient", "ReportRedeemablePromotions", true);

#if !SQL_WIGOS_BL02
      _log_error_written = false;
#endif

      // Browse the gathered account movements to produce the required data operation by operation
      while (SqlReader.Read())
      {
        // Group all the movements related to the current operation
        _am_operation_id = (SqlReader.IsDBNull(COL_AM.OPERATION_ID) ? 0 : SqlReader.GetInt64(COL_AM.OPERATION_ID));
        _cashier_session_id = (SqlReader.IsDBNull(COL_AM.CASHIER_SESSION) ? 0 : SqlReader.GetInt64(COL_AM.CASHIER_SESSION));

        if (_last_operation_id != _am_operation_id)
        {
          _num_operations += 1;

          if (_num_operations > NoOperations)
          {
            break;
          }

          // If Last operation has UNR Credit
          // Will split register in two, one for recharge and other for UNR Prize
          if (_last_operation_id != Int64.MinValue
                    && _operation_has_unr_credit)
          {
            _unr_row = CreateRowForUNRCredit(_row_movement, GeneralParam);
            _dt_movs.Rows.Add(_unr_row);
          } //if (  _operation_has_unr_credit )

          // Save the devolution value before get new operation data
          if (_devolution != 0)
          {
            _row_movement[COL_MOVS.DEVOLUTION_AMOUNT] = _devolution;
          }

          // New operation
          _account_id = SqlReader.GetInt64(COL_AM.ACCOUNT_ID);

          _row_movement = _dt_movs.NewRow();
          _row_movement[COL_MOVS.OPERATION_ID] = _am_operation_id;

          _aux_date = SqlReader.GetDateTime(COL_AM.DATETIME);
          _row_movement[COL_MOVS.DATETIME] = new DateTime(_aux_date.Year,
                                                          _aux_date.Month,
                                                          _aux_date.Day,
                                                          _aux_date.Hour,
                                                          _aux_date.Minute,
                                                          _aux_date.Second);

          _row_movement[COL_MOVS.OPERATION_CODE] = SqlReader.IsDBNull(COL_AM.OPERATION_TYPE) ? -1 : SqlReader.GetInt32(COL_AM.OPERATION_TYPE);

          switch ((OperationCode)(SqlReader.IsDBNull(COL_AM.OPERATION_TYPE) ? -1 : SqlReader.GetInt32(COL_AM.OPERATION_TYPE)))
          {
            case OperationCode.CASH_IN:
            case OperationCode.PROMOTION:
            case OperationCode.MB_CASH_IN:
            case OperationCode.MB_PROMOTION:
            case OperationCode.GIFT_REDEEMABLE_AS_CASHIN:
            case OperationCode.GIFT_DELIVERY:
            case OperationCode.NA_CASH_IN:
            case OperationCode.NA_PROMOTION:
              _row_movement[COL_MOVS.TIPO_OPERACION_DESCRIPCION] = "Recarga";
              break;

            case OperationCode.CHIPS_SALE:
              _row_movement[COL_MOVS.TIPO_OPERACION_DESCRIPCION] = "Venta de fichas";
              break;

            case OperationCode.CHIPS_SALE_WITH_RECHARGE:
              _row_movement[COL_MOVS.TIPO_OPERACION_DESCRIPCION] = "Venta de fichas (con reintegro)";
              break;

            case OperationCode.CASH_OUT:
              _row_movement[COL_MOVS.TIPO_OPERACION_DESCRIPCION] = "Pago";
              break;

            case OperationCode.CHIPS_PURCHASE:
              _row_movement[COL_MOVS.TIPO_OPERACION_DESCRIPCION] = "Compra de fichas";
              break;

            case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
              _row_movement[COL_MOVS.TIPO_OPERACION_DESCRIPCION] = "Compra de fichas (con recarga)";
              break;
          }

          _row_movement[COL_MOVS.ACCOUNT_ID] = _account_id;
          _row_movement[COL_MOVS.HOLDER_NAME] = SqlReader.IsDBNull(COL_AM.HOLDER_NAME) ? null : SqlReader.GetString(COL_AM.HOLDER_NAME).Trim();
          _row_movement[COL_MOVS.HOLDER_RFC] = SqlReader.IsDBNull(COL_AM.RFC) ? null : SqlReader.GetString(COL_AM.RFC).Trim();
          _row_movement[COL_MOVS.HOLDER_CURP] = SqlReader.IsDBNull(COL_AM.CURP) ? null : SqlReader.GetString(COL_AM.CURP).Trim();
          _row_movement[COL_MOVS.ADDRESS] = SqlReader.IsDBNull(COL_AM.HOLDER_ADDRESS) ? null : SqlReader.GetString(COL_AM.HOLDER_ADDRESS).Trim();
          _row_movement[COL_MOVS.CITY] = SqlReader.IsDBNull(COL_AM.HOLDER_ZIP) ? null : SqlReader.GetString(COL_AM.HOLDER_ZIP).Trim();
          _row_movement[COL_MOVS.ZIP] = SqlReader.IsDBNull(COL_AM.HOLDER_ZIP) ? null : SqlReader.GetString(COL_AM.HOLDER_ZIP).Trim();
          _row_movement[COL_MOVS.HOLDER_NATIONALITY] = SqlReader.IsDBNull(COL_AM.HOLDER_NATIONALITY) ? String.Empty : SqlReader.GetString(COL_AM.HOLDER_NATIONALITY);
          _row_movement[COL_MOVS.HOLDER_ENT_FED] = SqlReader.IsDBNull(COL_AM.HOLDER_ENT_FED) ? -1 : SqlReader.GetInt32(COL_AM.HOLDER_ENT_FED);
          _row_movement[COL_MOVS.HOLDER_EMAIL] = SqlReader.IsDBNull(COL_AM.HOLDER_EMAIL) ? String.Empty : SqlReader.GetString(COL_AM.HOLDER_EMAIL);
          _row_movement[COL_MOVS.USER_TYPE] = SqlReader.IsDBNull(COL_AM.USER_TYPE) ? -1 : SqlReader.GetInt32(COL_AM.USER_TYPE);

          // EVIDENCE (CONSTANCIA) FIELDS

          // Full name is composed from  AMP_PLAYER_NAME1, AMP_PLAYER_NAME2 and AMP_PLAYER_NAME3. 
          _name = SqlReader.IsDBNull(COL_AM.EVIDENCE_PLAYER_NAME3) ? null : SqlReader.GetString(COL_AM.EVIDENCE_PLAYER_NAME3).Trim();
          _last_name1 = SqlReader.IsDBNull(COL_AM.EVIDENCE_PLAYER_NAME1) ? null : SqlReader.GetString(COL_AM.EVIDENCE_PLAYER_NAME1).Trim();
          _last_name2 = SqlReader.IsDBNull(COL_AM.EVIDENCE_PLAYER_NAME2) ? null : SqlReader.GetString(COL_AM.EVIDENCE_PLAYER_NAME2).Trim();
          _row_movement[COL_MOVS.EVIDENCE_HOLDER_NAME] = SharedFunctions.FormatFullName(FULL_NAME_TYPE.WITHHOLDING, _name, String.Empty, _last_name1, _last_name2, GeneralParam);
          _row_movement[COL_MOVS.EVIDENCE_HOLDER_RFC] = SqlReader.IsDBNull(COL_AM.EVIDENCE_RFC) ? null : SqlReader.GetString(COL_AM.EVIDENCE_RFC).Trim();
          _row_movement[COL_MOVS.EVIDENCE_HOLDER_CURP] = SqlReader.IsDBNull(COL_AM.EVIDENCE_CURP) ? null : SqlReader.GetString(COL_AM.EVIDENCE_CURP).Trim();

          _row_movement[COL_MOVS.EVIDENCE] = SqlReader.IsDBNull(COL_AM.EVIDENCE) ? false : SqlReader.GetBoolean(COL_AM.EVIDENCE);
          _row_movement[COL_MOVS.DOCUMENT_ID] = SqlReader.IsDBNull(COL_AM.EVIDENCE_DOCUMENT_ID1) ? DBNull.Value : SqlReader[COL_AM.EVIDENCE_DOCUMENT_ID1];

          _row_movement[COL_MOVS.CASHIER_SESSION] = _cashier_session_id;
          _row_movement[COL_MOVS.CASHIER_NAME] = !SqlReader.IsDBNull(COL_AM.CASHIER_NAME) ? SqlReader[COL_AM.CASHIER_NAME] : DBNull.Value;

          _row_movement[COL_MOVS.RECHARGE_FOR_POINTS_PRIZE] = 0;

          // Set 'Efectivo' as default payment type key and as default Payment method.
          // When examining operation movements change to apropiate if exist BankCard or Check movement.
          _forma_pago_default_value = GeneralParam.GetString("PSAClient", "FormaDePago.Efectivo", TIPO_PAGO);
          _row_movement[COL_MOVS.PAYMENT_METHOD] = _forma_pago_default_value;

          _clave_tipo_pago_default_value = GeneralParam.GetString("PSAClient", "ClaveTipoPago.Efectivo");
          if (String.IsNullOrEmpty(_clave_tipo_pago_default_value))
          {
            _clave_tipo_pago_default_value = GeneralParam.GetString("PSAClient", "ClaveTipoPago");
          }
          _row_movement[COL_MOVS.PAYMENT_TYPE_KEY] = _clave_tipo_pago_default_value;

          _row_movement[COL_MOVS.RECHARGE_TYPE_NUMERIC_KEY] = (Int32)CLAVE_TIPO_PAGO.undefined;

          _row_movement[COL_MOVS.OPENING_CM_DATE] = !SqlReader.IsDBNull(COL_AM.OPENING_CM_DATE) ? SqlReader[COL_AM.OPENING_CM_DATE] : DBNull.Value;
          _row_movement[COL_MOVS.TRACK_DATA_EXTERNAL] = !SqlReader.IsDBNull(COL_AM.TRACK_DATA_EXTERNAL) ? SqlReader[COL_AM.TRACK_DATA_EXTERNAL] : DBNull.Value;
          _row_movement[COL_MOVS.AC_HOLDER_NAMES] = !SqlReader.IsDBNull(COL_AM.AC_HOLDER_NAMES) ? SqlReader[COL_AM.AC_HOLDER_NAMES] : DBNull.Value;
          _row_movement[COL_MOVS.AC_HOLDER_SURNAME] = !SqlReader.IsDBNull(COL_AM.AC_HOLDER_SURNAME) ? SqlReader[COL_AM.AC_HOLDER_SURNAME] : DBNull.Value;

          String _filter_foucher;
          DataRow[] _rows_voucher;

          // Look for Voucher ( If exist )
          _filter_foucher = String.Format("CV_SESSION_ID = {0} and CV_ACCOUNT_ID = {1} and CV_OPERATION_ID = {2}",
                                  _cashier_session_id,
                                  _account_id,
                                  _am_operation_id);

          _rows_voucher = _cashier_voucher.Select(_filter_foucher);

          if (_rows_voucher != null && _rows_voucher.Length > 0)
          {
            if (_rows_voucher[0]["CV_VOUCHER_ID"] != DBNull.Value)
            {
              _row_movement[COL_MOVS.VOUCHER_ID] = (Int64)_rows_voucher[0]["CV_VOUCHER_ID"];
              _row_movement[COL_MOVS.FOLIO] = (Int64)_rows_voucher[0]["CV_VOUCHER_ID"];
            }

            if (_rows_voucher[0]["CV_SEQUENCE"] != DBNull.Value)
            {
              _row_movement[COL_MOVS.FOLIO2] = (Int64)_rows_voucher[0]["CV_SEQUENCE"];
            }

            if (_rows_voucher[0]["CV_SESSION_ID"] != DBNull.Value)
            {
              _row_movement[COL_MOVS.CASHIER_SESSION_ID] = (Int64)_rows_voucher[0]["CV_SESSION_ID"];
            }

            if (_rows_voucher[0]["CV_USER_ID"] != DBNull.Value)
            {
              _row_movement[COL_MOVS.CASHIER_USER_ID] = (Int32)_rows_voucher[0]["CV_USER_ID"];
            }

            if (_rows_voucher[0]["GU_FULL_NAME"] != DBNull.Value)
            {
              _row_movement[COL_MOVS.CASHIER_FULL_NAME] = (String)_rows_voucher[0]["GU_FULL_NAME"];
            }

            if (_rows_voucher[0]["CV_CASHIER_ID"] != DBNull.Value)
            {
              _row_movement[COL_MOVS.CASHIER_CASHIER_ID] = (Int32)_rows_voucher[0]["CV_CASHIER_ID"];
            }

            if (_rows_voucher[0]["CV_CASHIER_NAME"] != DBNull.Value)
            {
              _row_movement[COL_MOVS.CASHIER_CASHIER_NAME] = (String)_rows_voucher[0]["CV_CASHIER_NAME"];
            }
          }

          _devolution = 0;

          CheckEmptyFields(_row_movement);
          _dt_movs.Rows.Add(_row_movement);

          _last_operation_id = _am_operation_id;

          // Reset operation has unr credit flag.
          _operation_has_unr_credit = false;

        } // if _last_operation_id != _am_operation_id

        // Accumulate the account movement data into the operation 
        //    - Operation's movements
        //    - Account totals

        switch ((CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE])
        {
          case CASHIER_MOVEMENT.CASH_IN:
            if (!((OperationCode)SqlReader[COL_AM.OPERATION_TYPE] == OperationCode.CHIPS_PURCHASE
               || (OperationCode)SqlReader[COL_AM.OPERATION_TYPE] == OperationCode.CHIPS_SALE
               || (OperationCode)SqlReader[COL_AM.OPERATION_TYPE] == OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT
               || (OperationCode)SqlReader[COL_AM.OPERATION_TYPE] == OperationCode.CHIPS_SALE_WITH_RECHARGE))
            {
              _row_movement[COL_MOVS.CASH_IN_TOTAL_WITHOUT_TAX] = SqlReader[COL_AM.ADD_AMOUNT];
            }
            break;

          case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
          case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1:
          case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
          case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1:

            if ((CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE] == CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1)
            {
              _row_movement[COL_MOVS.RECHARGE_TYPE_NUMERIC_KEY] = (Int32)CLAVE_TIPO_PAGO.by_points;
            }
            else
            {
              if ((Int32)_row_movement[COL_MOVS.RECHARGE_TYPE_NUMERIC_KEY] == (Int32)CLAVE_TIPO_PAGO.undefined)
              {
                _row_movement[COL_MOVS.RECHARGE_TYPE_NUMERIC_KEY] = (Int32)CLAVE_TIPO_PAGO.by_cash;
              }
            }

            _row_movement[COL_MOVS.CASH_IN_SPLIT1] = SqlReader[COL_AM.ADD_AMOUNT];

            if (_row_movement[COL_MOVS.CASH_IN_TOTAL] == DBNull.Value)
            {
              _row_movement[COL_MOVS.CASH_IN_TOTAL] = SqlReader[COL_AM.ADD_AMOUNT];
            }
            else
            {
              _row_movement[COL_MOVS.CASH_IN_TOTAL] = (Decimal)_row_movement[COL_MOVS.CASH_IN_TOTAL] + (Decimal)SqlReader[COL_AM.ADD_AMOUNT];
            }

            break;

          case CASHIER_MOVEMENT.CHIPS_PURCHASE:
            _row_movement[COL_MOVS.CAGE_CURRENCY_TYPE] = SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE] == DBNull.Value ? 0 : (Int32)SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE];
            _row_movement[COL_MOVS.CURRENCY_ISO_CODE] = SqlReader[COL_AM.CM_CURRENCY_ISO_CODE] == DBNull.Value ? "" : (String)SqlReader[COL_AM.CM_CURRENCY_ISO_CODE];
            break;

          case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
            if (_row_movement[COL_MOVS.FICHAS_A_BALANCE] == DBNull.Value)
            {
              _row_movement[COL_MOVS.FICHAS_A_BALANCE] = SqlReader[COL_AM.ADD_AMOUNT];
            }
            else
            {
              _row_movement[COL_MOVS.FICHAS_A_BALANCE] = (Decimal)_row_movement[COL_MOVS.CASH_IN_TOTAL] + (Decimal)SqlReader[COL_AM.ADD_AMOUNT];
            }

            _row_movement[COL_MOVS.CAGE_CURRENCY_TYPE] = SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE] == DBNull.Value ? 0 : (Int32)SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE];
            _row_movement[COL_MOVS.PURCHASE_AMOUNT_CURRENCY] = SqlReader[COL_AM.ADD_AMOUNT] == DBNull.Value ? 0 : (Decimal)SqlReader[COL_AM.ADD_AMOUNT];
            _row_movement[COL_MOVS.CURRENCY_ISO_CODE] = SqlReader[COL_AM.CM_CURRENCY_ISO_CODE] == DBNull.Value ? "" : (String)SqlReader[COL_AM.CM_CURRENCY_ISO_CODE];

            break;

          case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE:
            if (_row_movement[COL_MOVS.FICHAS_A_BALANCE] == DBNull.Value)
            {
              _row_movement[COL_MOVS.FICHAS_A_BALANCE] = (Decimal)SqlReader[COL_AM.ADD_AMOUNT];
            }
            else
            {
              _row_movement[COL_MOVS.FICHAS_A_BALANCE] = (Decimal)_row_movement[COL_MOVS.CASH_IN_TOTAL] + (Decimal)SqlReader[COL_AM.ADD_AMOUNT];
            }

            _row_movement[COL_MOVS.CAGE_CURRENCY_TYPE] = SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE] == DBNull.Value ? 0 : (Int32)SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE];
            _row_movement[COL_MOVS.PURCHASE_AMOUNT_CURRENCY] = SqlReader[COL_AM.INITIAL_BALANCE] == DBNull.Value ? 0 : (Decimal)SqlReader[COL_AM.INITIAL_BALANCE];
            _row_movement[COL_MOVS.CURRENCY_ISO_CODE] = SqlReader[COL_AM.CM_CURRENCY_ISO_CODE] == DBNull.Value ? "" : (String)SqlReader[COL_AM.CM_CURRENCY_ISO_CODE];
            break;

          case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
          case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2:
          case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
          case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2:
            _row_movement[COL_MOVS.CASH_IN_SPLIT2] = SqlReader[COL_AM.ADD_AMOUNT];
            if (_row_movement[COL_MOVS.CASH_IN_TOTAL] == DBNull.Value)
            {
              _row_movement[COL_MOVS.CASH_IN_TOTAL] = SqlReader[COL_AM.ADD_AMOUNT];
            }
            else
            {
              _row_movement[COL_MOVS.CASH_IN_TOTAL] = (Decimal)_row_movement[COL_MOVS.CASH_IN_TOTAL] + (Decimal)SqlReader[COL_AM.ADD_AMOUNT];
            }

            break;

          case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT1:
            _row_movement[COL_MOVS.CASH_IN_TAX1] = SqlReader[COL_AM.ADD_AMOUNT];
            break;

          case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT2:
            _row_movement[COL_MOVS.CASH_IN_TAX2] = SqlReader[COL_AM.ADD_AMOUNT];
            break;

          case CASHIER_MOVEMENT.CASH_OUT:
            _row_movement[COL_MOVS.CASH_OUT_TOTAL] = SqlReader[COL_AM.SUB_AMOUNT];
            _row_movement[COL_MOVS.CASH_OUT_MONEY] = SqlReader[COL_AM.CASH_PAYMENT_AMOUNT];
            _row_movement[COL_MOVS.CASH_OUT_CHECK] = SqlReader[COL_AM.CHECK_PAYMENT_AMOUNT];
            break;

          case CASHIER_MOVEMENT.CHIPS_SALE:
            _row_movement[COL_MOVS.CAGE_CURRENCY_TYPE] = SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE] == DBNull.Value ? 0 : (Int32)SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE];
            _row_movement[COL_MOVS.CURRENCY_ISO_CODE] = SqlReader[COL_AM.CM_CURRENCY_ISO_CODE] == DBNull.Value ? "" : (String)SqlReader[COL_AM.CM_CURRENCY_ISO_CODE];
            break;

          case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
          case CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL: //14-DEC-2016 Bug 5255 FGB
            _row_movement[COL_MOVS.BALANCE_A_FICHAS] = SqlReader[COL_AM.SUB_AMOUNT];
            _row_movement[COL_MOVS.SALES_AMOUNT_CURRENCY] = SqlReader[COL_AM.SUB_AMOUNT] == DBNull.Value ? 0 : (Decimal)SqlReader[COL_AM.SUB_AMOUNT];
            _row_movement[COL_MOVS.CAGE_CURRENCY_TYPE] = SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE] == DBNull.Value ? 0 : (Int32)SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE];
            _row_movement[COL_MOVS.CURRENCY_ISO_CODE] = SqlReader[COL_AM.CM_CURRENCY_ISO_CODE] == DBNull.Value ? "" : (String)SqlReader[COL_AM.CM_CURRENCY_ISO_CODE];
            break;

          case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE:
            _row_movement[COL_MOVS.BALANCE_A_FICHAS] = (Decimal)SqlReader[COL_AM.SUB_AMOUNT];
            _row_movement[COL_MOVS.CAGE_CURRENCY_TYPE] = SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE] == DBNull.Value ? 0 : (Int32)SqlReader[COL_AM.CM_CAGE_CURRENCY_TYPE];
            _row_movement[COL_MOVS.SALES_AMOUNT_CURRENCY] = SqlReader[COL_AM.INITIAL_BALANCE] == DBNull.Value ? 0 : (Decimal)SqlReader[COL_AM.INITIAL_BALANCE];
            _row_movement[COL_MOVS.CURRENCY_ISO_CODE] = SqlReader[COL_AM.CM_CURRENCY_ISO_CODE] == DBNull.Value ? "" : (String)SqlReader[COL_AM.CM_CURRENCY_ISO_CODE];
            break;

          case CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT:
            _row_movement[COL_MOVS.RESIDUAL_AMOUNT] = SqlReader[COL_AM.SUB_AMOUNT] == DBNull.Value ? 0 : (Decimal)SqlReader[COL_AM.SUB_AMOUNT];
            break;

          case CASHIER_MOVEMENT.PRIZES:
            _row_movement[COL_MOVS.PRIZE_AMOUNT] = SqlReader[COL_AM.SUB_AMOUNT];
            break;

          case CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS:       // Premio Sorteo de Caja en Especie   (UNR)
          case CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS:       // Premio Sorteo de Caja en Promoci�n (UNR)
          case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS:    // Premio Sorteo de Caja en Especie   (Redeemeble)
          case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS:    // Premio Sorteo de Caja en Promoci�n (Redeemeble)

            if (_row_movement[COL_MOVS.PRIZE_IN_KIND_AMOUNT] == DBNull.Value)
            {
              _row_movement[COL_MOVS.PRIZE_IN_KIND_AMOUNT] = (Decimal)SqlReader[COL_AM.SUB_AMOUNT];
            }
            else
            {
              _row_movement[COL_MOVS.PRIZE_IN_KIND_AMOUNT] = (Decimal)_row_movement[COL_MOVS.PRIZE_IN_KIND_AMOUNT]
                                                              + (Decimal)SqlReader[COL_AM.SUB_AMOUNT];
            }

            _operation_has_unr_credit = true;
            break;

          case CASHIER_MOVEMENT.DEV_SPLIT1:
          case CASHIER_MOVEMENT.CANCEL_SPLIT1:
          case CASHIER_MOVEMENT.DEV_SPLIT2:
          case CASHIER_MOVEMENT.CANCEL_SPLIT2:
            _devolution = _devolution + (Decimal)SqlReader[COL_AM.SUB_AMOUNT];
            break;

          case CASHIER_MOVEMENT.TAX_ON_PRIZE1:                      // FEDERAL TAXES
          case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1:                // Premio Sorteo de Caja en Especie   - Impuesto Federal (UNR)
          case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1:                // Premio Sorteo de Caja en Promoci�n - Impuesto Federal (UNR)
          case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1:             // Premio Sorteo de Caja en Especie   - Impuesto Federal (RE in Kind)
          case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1:             // Premio Sorteo de Caja en Promoci�n - Impuesto Federal (RE in Kind)
            {
              if ((CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1 || (CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1)
              {
                _amount_column = COL_AM.SUB_AMOUNT;
              }
              else
              {
                _amount_column = COL_AM.ADD_AMOUNT;
              }

              if (_row_movement[COL_MOVS.TAX_ON_PRIZE1_AMOUNT] == DBNull.Value)
              {
                _row_movement[COL_MOVS.TAX_ON_PRIZE1_AMOUNT] = (Decimal)SqlReader[_amount_column];
              }
              else
              {
                _row_movement[COL_MOVS.TAX_ON_PRIZE1_AMOUNT] = (Decimal)_row_movement[COL_MOVS.TAX_ON_PRIZE1_AMOUNT]
                                                              + (Decimal)SqlReader[_amount_column];
              }

              break;
            }

          case CASHIER_MOVEMENT.TAX_ON_PRIZE2:                      // STATE TAXES
          case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2:                // Premio Sorteo de Caja en Especie   - Impuesto Estatal (UNR)
          case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2:                // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (UNR)
          case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2:             // Premio Sorteo de Caja en Especie   - Impuesto Estatal (RE in Kind)
          case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2:             // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (RE in Kind)
            {
              if ((CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2 || (CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2)
              {
                _amount_column = COL_AM.SUB_AMOUNT;
              }
              else
              {
                _amount_column = COL_AM.ADD_AMOUNT;
              }

              if (_row_movement[COL_MOVS.TAX_ON_PRIZE2_AMOUNT] == DBNull.Value)
              {
                _row_movement[COL_MOVS.TAX_ON_PRIZE2_AMOUNT] = (Decimal)SqlReader[_amount_column];
              }
              else
              {
                _row_movement[COL_MOVS.TAX_ON_PRIZE2_AMOUNT] = (Decimal)_row_movement[COL_MOVS.TAX_ON_PRIZE2_AMOUNT]
                                                              + (Decimal)SqlReader[_amount_column];
              }

              break;
            }

          case CASHIER_MOVEMENT.TAX_ON_PRIZE3:                      // MUNICIPAL TAXES
          case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3:                // Premio Sorteo de Caja en Especie   - Impuesto Municipal (UNR)
          case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3:                // Premio Sorteo de Caja en Promoci�n - Impuesto Municipal (UNR)
          case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3:             // Premio Sorteo de Caja en Especie   - Impuesto Municipal (RE in Kind)
          case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3:             // Premio Sorteo de Caja en Promoci�n - Impuesto Municipal (RE in Kind)
            {
              if ((CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3 || (CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3)
              {
                _amount_column = COL_AM.SUB_AMOUNT;
              }
              else
              {
                _amount_column = COL_AM.ADD_AMOUNT;
              }

              if (_row_movement[COL_MOVS.TAX_ON_PRIZE3_AMOUNT] == DBNull.Value)
              {
                _row_movement[COL_MOVS.TAX_ON_PRIZE3_AMOUNT] = (Decimal)SqlReader[_amount_column];
              }
              else
              {
                _row_movement[COL_MOVS.TAX_ON_PRIZE3_AMOUNT] = (Decimal)_row_movement[COL_MOVS.TAX_ON_PRIZE3_AMOUNT]
                                                              + (Decimal)SqlReader[_amount_column];
              }

              break;
            }

          // RRB 18-SEP-2012: TAX RETURNING, SERVICE CHARGE and DECIMAL ROUNDING
          case CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON:
            _row_movement[COL_MOVS.TAX_RETURNING] = SqlReader[COL_AM.SUB_AMOUNT];
            break;

          case CASHIER_MOVEMENT.SERVICE_CHARGE:
          case CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A:  //21-SEP-2016
            _row_movement[COL_MOVS.SERVICE_CHARGE] = SqlReader[COL_AM.ADD_AMOUNT];
            break;

          case CASHIER_MOVEMENT.DECIMAL_ROUNDING:
            _row_movement[COL_MOVS.DECIMAL_ROUNDING] = SqlReader[COL_AM.SUB_AMOUNT];
            break;

          case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE:

            // TODO: Only for PSA "FUTURE"
            //if (!_is_reporting_redeemable)
            //{
            //  // Not report promotional redeemable credit.
            //  break;
            //}

            if (_row_movement[COL_MOVS.PROMOTIONAL_BALANCE] != DBNull.Value)
            {
              _row_movement[COL_MOVS.PROMOTIONAL_BALANCE] = (Decimal)_row_movement[COL_MOVS.PROMOTIONAL_BALANCE] + (Decimal)SqlReader[COL_AM.ADD_AMOUNT];
            }
            else
            {
              _row_movement[COL_MOVS.PROMOTIONAL_BALANCE] = (Decimal)SqlReader[COL_AM.ADD_AMOUNT];
            }

            break;

          // Added to handle PrizeExpired ( DEVOLUTION )
          case CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED:

            _row_movement[COL_MOVS.PRIZE_EXPIRED] = SqlReader[COL_AM.SUB_AMOUNT];

            break;

          case CASHIER_MOVEMENT.CHECK_PAYMENT:
          case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:

            _clave_tipo_pago_val = GeneralParam.GetString("PSAClient", "ClaveTipoPago.Cheque", _clave_tipo_pago_default_value);
            _row_movement[COL_MOVS.PAYMENT_TYPE_KEY] = _clave_tipo_pago_val;

            _forma_pago_val = GeneralParam.GetString("PSAClient", "FormaDePago.Cheque", _forma_pago_default_value);
            _row_movement[COL_MOVS.PAYMENT_METHOD] = _forma_pago_val;

            if ((CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE] == CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1
              || (CASHIER_MOVEMENT)SqlReader[COL_AM.TYPE] == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1)
            {
              _row_movement[COL_MOVS.RECHARGE_TYPE_NUMERIC_KEY] = (Int32)CLAVE_TIPO_PAGO.by_check;
            }

            break;

          case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:

            _clave_tipo_pago_val = GeneralParam.GetString("PSAClient", "ClaveTipoPago.Tarjeta", _clave_tipo_pago_default_value);
            _row_movement[COL_MOVS.PAYMENT_TYPE_KEY] = _clave_tipo_pago_val;

            _forma_pago_val = GeneralParam.GetString("PSAClient", "FormaDePago.Tarjeta", _forma_pago_default_value);
            _row_movement[COL_MOVS.PAYMENT_METHOD] = _forma_pago_val;

            _row_movement[COL_MOVS.RECHARGE_TYPE_NUMERIC_KEY] = (Int32)CLAVE_TIPO_PAGO.by_card;

            break;

          case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE:
            _row_movement[COL_MOVS.RECHARGE_FOR_POINTS_PRIZE] = (Decimal)SqlReader[COL_AM.SUB_AMOUNT];

            break;
#if !SQL_WIGOS_BL02
          default:
            if (!_log_error_written)
            {
              Log.Error("DB_PSAReportesDiarios.AccountMovements. Unexpected movement type.  Operation_id: " + SqlReader.GetInt64(COL_AM.OPERATION_ID)
                      + ". Type:" + SqlReader[COL_AM.TYPE]);
              _log_error_written = true;
            }

            break;
#endif
        } // switch       

      } // while

      SqlReader.Close();

      // Save the devolution value before end the process
      if (_devolution != 0)
      {
        _row_movement[COL_MOVS.DEVOLUTION_AMOUNT] = _devolution;
      }

      // If last operation has unr_row will create row for this.
      if (_operation_has_unr_credit
         && _last_operation_id != Int64.MinValue)
      {
        _unr_row = CreateRowForUNRCredit(_row_movement, GeneralParam);
        _dt_movs.Rows.Add(_unr_row);
      } //if (  _operation_has_unr_credit )

      // Publish the produced data
      DTOperations = _dt_movs;

      // Commit the changes
      DTOperations.AcceptChanges();

      return true;

    } // GetTranspose

    private static DataTable CreateCashierMovementsColumns(ref DataTable CashierMovementsTable)
    {
      /**************************************************************************
       * JMM 25-JAN-2012
       * 
       * Columns names match the XML fields exported to the XML requiered by SAT
       * Please, DON'T CHANGE COLUMN NAMES
       * TODO: Load from NLS
       * COL_MOVS structure has columns indexes
       * ************************************************************************/
      CashierMovementsTable.TableName = TABLE_NAME_MOVEMENT;

      // AJQ 05-JUN-2014, The OperationId must be a Int64
      CashierMovementsTable.Columns.Add("IdOperacion", Type.GetType("System.Int64")).AllowDBNull = false;                  //col 0
      CashierMovementsTable.Columns.Add("FechaHora", Type.GetType("System.DateTime")).AllowDBNull = false;                 //col 1
      CashierMovementsTable.Columns.Add("NoCuenta", Type.GetType("System.Int64")).AllowDBNull = false;                     //col 2

      // Add 16-MAR-2015
      CashierMovementsTable.Columns.Add("TipoOperacion", Type.GetType("System.Int32")).AllowDBNull = false;                //col 3
      CashierMovementsTable.Columns.Add("TotalRecarga", Type.GetType("System.Decimal")).AllowDBNull = true;                //col 4	 	 	 
      CashierMovementsTable.Columns.Add("EmpresaA", Type.GetType("System.Decimal")).AllowDBNull = true;                    //col 5  Deposito
      CashierMovementsTable.Columns.Add("EmpresaB", Type.GetType("System.Decimal")).AllowDBNull = true;                    //col 6  Parte premio
      CashierMovementsTable.Columns.Add("Impuesto1", Type.GetType("System.Decimal")).AllowDBNull = true;                   //col 7	
      CashierMovementsTable.Columns.Add("Impuesto2", Type.GetType("System.Decimal")).AllowDBNull = true;                   //col 8					
      CashierMovementsTable.Columns.Add("TotalRetiro", Type.GetType("System.Decimal")).AllowDBNull = true;                 //col 9					

      CashierMovementsTable.Columns.Add("Devolucion", Type.GetType("System.Decimal")).AllowDBNull = true;                  //col 10
      CashierMovementsTable.Columns.Add("Premio", Type.GetType("System.Decimal")).AllowDBNull = true;                      //col 11
      CashierMovementsTable.Columns.Add("ISR1", Type.GetType("System.Decimal")).AllowDBNull = true;                        //col 12
      CashierMovementsTable.Columns.Add("ISR2", Type.GetType("System.Decimal")).AllowDBNull = true;                        //col 13

      CashierMovementsTable.Columns.Add("RetiroEnEfectivo", Type.GetType("System.Decimal")).AllowDBNull = true;            //col 14
      CashierMovementsTable.Columns.Add("RetiroEnCheque", Type.GetType("System.Decimal")).AllowDBNull = true;              //col 15

      CashierMovementsTable.Columns.Add("RFC", Type.GetType("System.String")).AllowDBNull = true;                          //col 16
      CashierMovementsTable.Columns.Add("CURP", Type.GetType("System.String")).AllowDBNull = true;                         //col 17
      CashierMovementsTable.Columns.Add("Nombre", Type.GetType("System.String")).AllowDBNull = true;                       //col 18
      CashierMovementsTable.Columns.Add("Domicilio", Type.GetType("System.String")).AllowDBNull = true;                    //col 19
      CashierMovementsTable.Columns.Add("Localidad", Type.GetType("System.String")).AllowDBNull = true;                    //col 20
      CashierMovementsTable.Columns.Add("CodigoPostal", Type.GetType("System.String")).AllowDBNull = true;                 //col 21        
      CashierMovementsTable.Columns.Add("Constancia.RFC", Type.GetType("System.String")).AllowDBNull = true;               //col 22
      CashierMovementsTable.Columns.Add("Constancia.CURP", Type.GetType("System.String")).AllowDBNull = true;              //col 23
      CashierMovementsTable.Columns.Add("Constancia.Nombre", Type.GetType("System.String")).AllowDBNull = true;            //col 24
      CashierMovementsTable.Columns.Add("Constancia.Domicilio", Type.GetType("System.String")).AllowDBNull = true;         //col 25
      CashierMovementsTable.Columns.Add("Constancia.Localidad", Type.GetType("System.String")).AllowDBNull = true;         //col 26
      CashierMovementsTable.Columns.Add("Constancia.CodigoPostal", Type.GetType("System.String")).AllowDBNull = true;      //col 27
      CashierMovementsTable.Columns.Add("Constancia", Type.GetType("System.Boolean")).AllowDBNull = true;                  //col 28      
      CashierMovementsTable.Columns.Add("DocumentID", Type.GetType("System.Int32")).AllowDBNull = true;                    //col 29
      CashierMovementsTable.Columns.Add("RedondeoPorRetencion", Type.GetType("System.Decimal")).AllowDBNull = true;        //col 30 
      CashierMovementsTable.Columns.Add("CargoPorServicio", Type.GetType("System.Decimal")).AllowDBNull = true;            //col 31
      CashierMovementsTable.Columns.Add("RedondeoPorDecimales", Type.GetType("System.Decimal")).AllowDBNull = true;        //col 32
      CashierMovementsTable.Columns.Add("SesionCaja", Type.GetType("System.Int64")).AllowDBNull = false;                   //col 33
      CashierMovementsTable.Columns.Add("PremioEspecie", Type.GetType("System.Decimal")).AllowDBNull = true;               //col 34

      //TODO: For PSA: Not match with RegistroLinea007, In PSA Client is NoRegistroCaja
      CashierMovementsTable.Columns.Add("CashierName", Type.GetType("System.String")).AllowDBNull = false;                 //col 35
      //TODO: For PSA: Not match with RegistroLinea007, Not in use.
      CashierMovementsTable.Columns.Add("PremioCaducado", Type.GetType("System.Decimal")).AllowDBNull = true;              //col 36
      //TODO: For PSA: Not match with RegistroLinea007, In PSA Client is  NoComprobante
      CashierMovementsTable.Columns.Add("VoucherId", Type.GetType("System.Int64")).AllowDBNull = true;                     //col 37
      CashierMovementsTable.Columns.Add("Folio", Type.GetType("System.Int64")).AllowDBNull = true;                         //col 38

      CashierMovementsTable.Columns.Add("SaldoPromocional", Type.GetType("System.Decimal")).AllowDBNull = true;            //col 39
      CashierMovementsTable.Columns.Add("ClaveTipoPago", Type.GetType("System.String")).AllowDBNull = true;                //col 40
      CashierMovementsTable.Columns.Add("FormaPago", Type.GetType("System.String")).AllowDBNull = true;                    //col 41
      CashierMovementsTable.Columns.Add("RecargaPorPuntos", Type.GetType("System.Decimal")).AllowDBNull = true;            //col 42

      CashierMovementsTable.Columns.Add("Fecha", Type.GetType("System.DateTime")).AllowDBNull = true;                      //col 43
      CashierMovementsTable.Columns.Add("TarjetaCliente", Type.GetType("System.String")).AllowDBNull = true;               //col 44
      CashierMovementsTable.Columns.Add("Nombres", Type.GetType("System.String")).AllowDBNull = true;                      //col 45
      CashierMovementsTable.Columns.Add("Apellidos", Type.GetType("System.String")).AllowDBNull = true;                    //col 46

      CashierMovementsTable.Columns.Add("Shift_Id", Type.GetType("System.Int64")).AllowDBNull = true;                      //col 47
      CashierMovementsTable.Columns.Add("Cajera_Id", Type.GetType("System.String")).AllowDBNull = true;                    //col 48
      CashierMovementsTable.Columns.Add("CajaNombres", Type.GetType("System.String")).AllowDBNull = true;                  //col 49
      CashierMovementsTable.Columns.Add("CajaApellidos", Type.GetType("System.String")).AllowDBNull = true;                //col 50
      CashierMovementsTable.Columns.Add("Caja_Id", Type.GetType("System.Int64")).AllowDBNull = true;                       //col 51
      CashierMovementsTable.Columns.Add("Caja_Name", Type.GetType("System.String")).AllowDBNull = true;                    //col 52
      CashierMovementsTable.Columns.Add("ClaveTipoPagoRecarga", Type.GetType("System.Int32")).AllowDBNull = true;          //col 53

      CashierMovementsTable.Columns.Add("TipoOperacionDescripcion", Type.GetType("System.String")).AllowDBNull = true;     //col 54
      CashierMovementsTable.Columns.Add("Folio2", Type.GetType("System.Int64")).AllowDBNull = true;                        //col 55
      CashierMovementsTable.Columns.Add("TotalRecargaSinImpuestos", Type.GetType("System.Decimal")).AllowDBNull = true;    //col 56

      CashierMovementsTable.Columns.Add("BalanceAFichas", Type.GetType("System.Decimal")).AllowDBNull = true;              //col 57
      CashierMovementsTable.Columns.Add("FichasABalance", Type.GetType("System.Decimal")).AllowDBNull = true;              //col 58
      CashierMovementsTable.Columns.Add("CageCurrencyType", Type.GetType("System.Int32")).AllowDBNull = true;              //col 59
      CashierMovementsTable.Columns.Add("CurrencyIsoCode", Type.GetType("System.String")).AllowDBNull = true;              //col 60
      CashierMovementsTable.Columns.Add("PurchaseCurrency", Type.GetType("System.Decimal")).AllowDBNull = true;            //col 61
      CashierMovementsTable.Columns.Add("SalesCurrency", Type.GetType("System.Decimal")).AllowDBNull = true;               //col 62

      CashierMovementsTable.Columns.Add("Nacionalidad", Type.GetType("System.String")).AllowDBNull = true;                 //col 63
      CashierMovementsTable.Columns.Add("EntidadFederativa", Type.GetType("System.Int32")).AllowDBNull = true;             //col 64
      CashierMovementsTable.Columns.Add("Email", Type.GetType("System.String")).AllowDBNull = true;                        //col 65
      CashierMovementsTable.Columns.Add("UserType", Type.GetType("System.Int32")).AllowDBNull = true;                      //col 66
      CashierMovementsTable.Columns.Add("Impuesto3", Type.GetType("System.Decimal")).AllowDBNull = true;                   //col 67
      CashierMovementsTable.Columns.Add("ISR3", Type.GetType("System.Decimal")).AllowDBNull = true;                        //col 68
      CashierMovementsTable.Columns.Add("ResidualAmount", Type.GetType("System.Decimal")).AllowDBNull = true;              //col 69

      // default value
      CashierMovementsTable.Columns["TotalRecarga"].DefaultValue = 0;
      CashierMovementsTable.Columns["EmpresaA"].DefaultValue = 0;
      CashierMovementsTable.Columns["EmpresaB"].DefaultValue = 0;
      CashierMovementsTable.Columns["Impuesto1"].DefaultValue = 0;
      CashierMovementsTable.Columns["Impuesto2"].DefaultValue = 0;
      CashierMovementsTable.Columns["TotalRetiro"].DefaultValue = 0;
      CashierMovementsTable.Columns["Devolucion"].DefaultValue = 0;
      CashierMovementsTable.Columns["Premio"].DefaultValue = 0;
      CashierMovementsTable.Columns["ISR1"].DefaultValue = 0;
      CashierMovementsTable.Columns["ISR2"].DefaultValue = 0;
      CashierMovementsTable.Columns["RetiroEnEfectivo"].DefaultValue = 0;
      CashierMovementsTable.Columns["RetiroEnCheque"].DefaultValue = 0;
      CashierMovementsTable.Columns["RedondeoPorRetencion"].DefaultValue = 0;
      CashierMovementsTable.Columns["CargoPorServicio"].DefaultValue = 0;
      CashierMovementsTable.Columns["RedondeoPorDecimales"].DefaultValue = 0;
      CashierMovementsTable.Columns["PremioEspecie"].DefaultValue = 0;
      CashierMovementsTable.Columns["PremioCaducado"].DefaultValue = 0;
      CashierMovementsTable.Columns["SaldoPromocional"].DefaultValue = 0;
      CashierMovementsTable.Columns["RecargaPorPuntos"].DefaultValue = 0;
      CashierMovementsTable.Columns["TotalRecargaSinImpuestos"].DefaultValue = 0;
      CashierMovementsTable.Columns["BalanceAFichas"].DefaultValue = 0;
      CashierMovementsTable.Columns["FichasABalance"].DefaultValue = 0;
      CashierMovementsTable.Columns["CageCurrencyType"].DefaultValue = 0;
      CashierMovementsTable.Columns["CurrencyIsoCode"].DefaultValue = "";
      CashierMovementsTable.Columns["PurchaseCurrency"].DefaultValue = 0;
      CashierMovementsTable.Columns["SalesCurrency"].DefaultValue = 0;
      CashierMovementsTable.Columns["Impuesto3"].DefaultValue = 0;
      CashierMovementsTable.Columns["ISR3"].DefaultValue = 0;
      CashierMovementsTable.Columns["ResidualAmount"].DefaultValue = 0;

      return CashierMovementsTable;
    }  // CreateCashierMovementsColumns

    //------------------------------------------------------------------------------
    // PURPOSE: Cleans the empty string replacing it with null values
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DataRow Row      
    //
    //      - OUTPUT:
    //
    // RETURNS:       
    // 
    private static void CheckEmptyFields(DataRow Row)
    {
      foreach (DataColumn _col in Row.Table.Columns)
      {
        if (_col.AllowDBNull && (Row[_col] != DBNull.Value))
        {
          if (Row[_col] is String)
          {
            if (String.IsNullOrEmpty(((String)Row[_col]).Trim()))
            {
              Row[_col] = null;
            }
          }
        }
      }
    }  // CheckEmptyFields

    //------------------------------------------------------------------------------
    // PURPOSE: Create RegistroL007 Row for UNR
    //          If the operation is a UNR credit movement, add a new record for the UNR part.
    //          
    //  PARAMS:
    //      - INPUT:
    //            - DataRow , Row with movement.
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      DataSet containing all the report data
    //    
    private static DataRow CreateRowForUNRCredit(DataRow RowMovement, GeneralParam.Dictionary GeneralParam)
    {
      DataRow _unr_row;
      String _clave_tipo_pago_val;
      String _forma_pago_val;

      _unr_row = RowMovement.Table.NewRow();

      // Clone row and set balances to 0 except PrizeInKind
      _unr_row.ItemArray = (Object[])RowMovement.ItemArray.Clone();

      _unr_row[COL_MOVS.CASH_IN_SPLIT1] = 0;
      _unr_row[COL_MOVS.DEVOLUTION_AMOUNT] = 0;
      _unr_row[COL_MOVS.PRIZE_AMOUNT] = 0;
      _unr_row[COL_MOVS.TAX_ON_PRIZE1_AMOUNT] = 0;
      _unr_row[COL_MOVS.TAX_ON_PRIZE2_AMOUNT] = 0;
      _unr_row[COL_MOVS.TAX_RETURNING] = 0;
      _unr_row[COL_MOVS.SERVICE_CHARGE] = 0;
      _unr_row[COL_MOVS.DECIMAL_ROUNDING] = 0;
      _unr_row[COL_MOVS.PRIZE_EXPIRED] = 0;
      _unr_row[COL_MOVS.PROMOTIONAL_BALANCE] = 0;
      _unr_row[COL_MOVS.PRIZE_IN_KIND_AMOUNT] = RowMovement[COL_MOVS.PRIZE_IN_KIND_AMOUNT];
      _unr_row[COL_MOVS.RECHARGE_FOR_POINTS_PRIZE] = 0;
      RowMovement[COL_MOVS.PRIZE_IN_KIND_AMOUNT] = 0;

      // Set ClaveTipoPago and Payment Method for UNR Register.
      _clave_tipo_pago_val = GeneralParam.GetString("PSAClient", "ClaveTipoPago.EnEspecie"
                                                              , "003");
      _unr_row[COL_MOVS.PAYMENT_TYPE_KEY] = _clave_tipo_pago_val;

      _forma_pago_val = GeneralParam.GetString("PSAClient", "FormaDePago.EnEspecie"
                                                          , "En Especie");
      _unr_row[COL_MOVS.PAYMENT_METHOD] = _forma_pago_val;

      return _unr_row;
    } // CreateRowForUNRCredit

    //------------------------------------------------------------------------------
    // PURPOSE: Get Cashier Voucher for a date range
    //
    //  PARAMS:
    //      - INPUT: 
    //          - DateFrom    
    //          - DateTo
    //          - Trx
    //
    //      - OUTPUT:
    //          - CashierVoucher 
    //
    // RETURNS:
    //          - True: correctly executed 
    //          - False: otherwise
    //
    //   NOTES:
    //
    private static Boolean GetVoucherFromCashierMovement(SELECT_TYPE SelectType, Int64 OperationId, Int32 NoOperations, String OperationIds, DateTime FromDate, DateTime ToDate, out DataTable CashierVoucher, SqlTransaction Trx)
    {
      CashierVoucher = new DataTable();
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT  TOP (" + NoOperations.ToString() + ")");
        _sb.AppendLine("          CV_VOUCHER_ID   ");
        _sb.AppendLine("        , CV_SEQUENCE     ");
        _sb.AppendLine("        , CV_SESSION_ID   ");
        _sb.AppendLine("        , CV_ACCOUNT_ID   ");
        _sb.AppendLine("        , CV_OPERATION_ID ");

        _sb.AppendLine("        , CV_SESSION_ID   ");
        _sb.AppendLine("        , CV_USER_ID      ");
        _sb.AppendLine("        , GU_FULL_NAME    ");
        _sb.AppendLine("        , CV_CASHIER_ID   ");
        _sb.AppendLine("        , CV_CASHIER_NAME ");

        if (SelectType == SELECT_TYPE.between_dates)
        {
          _sb.AppendLine("   FROM   CASHIER_VOUCHERS WITH (INDEX(IX_datetime)) ");
        }
        else
        {
          _sb.AppendLine("   FROM   CASHIER_VOUCHERS WITH (INDEX(IX_cv_operation_id_datetime)) ");
        }

        _sb.AppendLine("  INNER   JOIN GUI_USERS  ON GU_USER_ID  = CV_USER_ID ");

        switch (SelectType)
        {
          case SELECT_TYPE.by_operation:
            _sb.AppendLine("  WHERE   CV_OPERATION_ID = " + OperationId.ToString() + " ");

            break;
          case SELECT_TYPE.by_qty_operations:
            _sb.AppendLine("  WHERE   CV_OPERATION_ID >= " + OperationId.ToString() + " ");

            break;
          case SELECT_TYPE.by_operations_list:
            _sb.AppendLine("  WHERE   CV_OPERATION_ID in ( " + OperationIds + " ) ");

            break;
          case SELECT_TYPE.between_dates:
            _sb.AppendLine("  WHERE   CV_DATETIME >= @pFromDate ");
            _sb.AppendLine("    AND   CV_DATETIME <  @pToDate   ");

            break;
        }

        _sb.AppendLine("    AND   CV_TYPE IN (@pVoucherType_CashInA, @pVoucherType_CashOutA, @pVoucherType_ChipsBuy, @pVoucherType_ChipsSale)");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pVoucherType_CashInA", SqlDbType.Int).Value = CashierVoucherType.CashIn_A;
          _sql_cmd.Parameters.Add("@pVoucherType_CashOutA", SqlDbType.Int).Value = CashierVoucherType.CashOut_A;
          _sql_cmd.Parameters.Add("@pVoucherType_ChipsBuy", SqlDbType.Int).Value = CashierVoucherType.ChipsBuy;
          _sql_cmd.Parameters.Add("@pVoucherType_ChipsSale", SqlDbType.Int).Value = CashierVoucherType.ChipsSale;

          if (SelectType == SELECT_TYPE.between_dates)
          {
            _sql_cmd.Parameters.Add("@pFromDate", SqlDbType.DateTime).Value = FromDate;
            _sql_cmd.Parameters.Add("@pToDate", SqlDbType.DateTime).Value = ToDate;
          }

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(CashierVoucher);
          }
        }

        return true;
      }

#if !SQL_WIGOS_BL02
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch { }
#endif

      return false;

    } // GetVoucherFromCashierMovement

  }  // SharedBL02Common

}  // WSI.Common.SharedWigosBL02
