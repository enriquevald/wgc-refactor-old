using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;
using Microsoft.SqlServer.Server;

namespace WSI.Common
{
  public enum BarcodeType
  {
    Account = 0,
    Reserved = 1,
    Gift = 2,
    Handpay = 3,
    PayOrder = 4,
    Reception = 5,
    Flyer = 6,

    TitoStandardValidation       = 0x00030001,
    TitoSecureEnhancedValidation = 0x00030002,
    TitoSystemValidation         = 0x00030004,

    Unknown                      = 0x00FFFFFF,

    FilterAnyBarcode             = 0x00F1000FF,
    FilterAnyTito                = 0x00F2000FF,
    FilterBankCard               = 0x00F4000FF,
    FilterTrackData              = 0x00F8000FF,
    FilterAny                    = 0x011E000FF,
  }

  public class Barcode
  {
    // Pattern LCCCCSSSSNNNNNNNNNTT
    //      - L: 0       Leading mark (1 digit)
    //      - CCCC:      Checksum     (4 digits)
    //      - SSSS:      Site ID      (4 digits)
    //      - NNNNNNNNN: Sequence     (9 digits)
    //      - TT:        Type         (2 digits)
    // ----------------------- Total: 20 digits ----

    private readonly static Byte[] BARCODE_KEY = new byte[] { 35, 33, 94, 72, 71, 94, 36, 35, 70, 68, 83, 72, 74, 118, 98, 45 };
    private readonly static Byte[] BARCODE_IV = new byte[] { 1 };
    private readonly static int BARCODE_CHECKSUM_MODULE = 9973;

    protected Barcode()
    {
    }

    public Barcode(CardTrackData CardTrackData)
    {
      this.CardTrackData=CardTrackData;
      m_site_id = CardTrackData.SiteId;
      m_external_code = CardTrackData.ExternalTrackData;
    }

    protected String m_external_code;
    protected int m_site_id;
    protected int m_type;
    protected Int64 m_unique_id;


    public override string ToString()
    {
      BarcodeType _type;
      _type = (BarcodeType)m_type;
      return String.Format("Type:{0}, Site:{1}, UniqueId:{2}", _type, m_site_id, m_unique_id);
    }

    public String ExternalCode
    {
      get { return m_external_code; }
    }
    public int SiteId
    {
      get { return m_site_id; }
    }
    public BarcodeType Type
    {
      get { return (BarcodeType)m_type; }
    }
    public Int64 UniqueId
    {
      get { return m_unique_id; }
    }
    public String InternalCode
    {
      get { return String.Format("{0:D4}{1:D9}", m_site_id, m_unique_id); }
    }

    public CardTrackData CardTrackData
    {
      get;
      set;

    }
    public Barcode(Barcode Barcode)
    {
      m_external_code = Barcode.m_external_code;
      m_site_id = Barcode.m_site_id;
      m_type = Barcode.m_type;
      m_unique_id = Barcode.m_unique_id;
    }

#if !SQL_WIGOS_BL02
    public static Barcode Create(BarcodeType Type, Int64 UniqueId)
    {
      int _type;
      int _site_id;


      _type = (int)Type;
      _site_id = 0;

      if (_type >= 0 && _type < 100)
      {
        _site_id = GeneralParam.GetInt32("Site", "Identifier");
      }

      return Create(Type, UniqueId, _site_id);
    }
#endif

    public static Barcode Create(BarcodeType Type, Int64 UniqueId, Int32 SiteId)
    {
      String _external_code;
      int _type;

      _type = (int)Type;
      if (_type >= 0 && _type < 100)
      {
        if (!Barcode.Encode(SiteId, _type, UniqueId, out _external_code))
        {
          throw new Exception(String.Format("Unabled to create the Barcode. SiteId:{0}, Type:{1}, UniqueId:{2}", SiteId, Type, UniqueId));
        }

        return new Barcode(SiteId, _type, UniqueId, _external_code);
      }


      if (Type == BarcodeType.TitoStandardValidation
          || Type == BarcodeType.TitoSecureEnhancedValidation
          || Type == BarcodeType.TitoSystemValidation)
      {
        String _format;

        _format = "{0:D18}";
        if (Type == BarcodeType.TitoStandardValidation)
        {
          _format = "{0:D8}";
        }
        return new Barcode(0, (int)Type, UniqueId, String.Format(_format, UniqueId));
      }

      throw new Exception(String.Format("Unabled to create the Barcode. Type:{0}, UniqueId:{1}", Type, UniqueId));

    }

    public static Boolean TryParse(String ExternalCode, out Barcode Code)
    {
      UInt64 _number;
      int _site_id;
      int _type;
      Int64 _unique_id;
      BarcodeType _btype;

      
      Code = null;
      if (!IsValid(ExternalCode, out _number))
      {
        if (CardTrackData.AdmitAnyData() && CardTrackData.IsTrackData(ExternalCode))
        {
          Code = new Barcode(new ExternalTrackData(ExternalCode));

          return true;
        }
        return false;
      }

      _btype = BarcodeType.Unknown;
      switch  (ExternalCode.Length )
      {
        case 8: 
          _btype = BarcodeType.TitoStandardValidation; 
          break;
        
        case 16:
          _btype = BarcodeType.TitoSecureEnhancedValidation; 
          break;
        
        case 18:
          _btype = BarcodeType.TitoSecureEnhancedValidation;
          if (_number / 10000000000000000 > 0)
          {
            _btype = BarcodeType.TitoSystemValidation;
          }
          break;

        default:
          _btype = BarcodeType.Unknown;
          break;
      }

      if (_btype != BarcodeType.Unknown)
      {
        Code = Barcode.Create(_btype, (Int64)_number, 0);

        return true;
      }

      if (Barcode.Decode(ExternalCode, out _site_id, out _type, out _unique_id))
      {
        Code = Barcode.Create((BarcodeType)_type, _unique_id, _site_id);

        return true;
      }

      return false;
    }
    
    // DCS 18-03-015: Trasaladate class and adapt previous function for call in Sql Assembly
    //                If is all OK return Operation ID, otherwise return -1
    [SqlProcedure]
    public static Int32 GetOperationFromBarcode(String ExternalCode)
    {
      Barcode _output_barcode;

      if (TryParse(ExternalCode, out _output_barcode))
      {
        return (Int32)_output_barcode.m_unique_id;
      }
      else
      {
        return -1;
      }
    }

    public static Boolean IsValid(String PartialExternalCode)
    {
      UInt64 _dummy;

      return IsValid(PartialExternalCode, out _dummy);
    }
    public static Boolean IsValid(String PartialExternalCode, out UInt64 NumberValue)
    {
      NumberValue = 0;

      if (!UInt64.TryParse(PartialExternalCode, out NumberValue))
      {
        return false;
      }

      foreach (Char _d in PartialExternalCode)
      {
        if (!Char.IsDigit(_d))
        {
          return false;
        }
      }

      return true;
    }

    private Barcode(Int32 SiteId, Int32 Type, Int64 UniqueId, String ExternalCode)
    {
      m_external_code = ExternalCode;
      m_site_id = SiteId;
      m_type = Type;
      m_unique_id = UniqueId;
    }

    private static Boolean Encode(Int32 SiteId, Int32 Type, Int64 UniqueId, out String ExternalId)
    {
      String _internal;

      ExternalId = String.Empty;

      if (SiteId < 0 || SiteId >= 10000) return false;
      if (Type < 0 || Type >= 100) return false;
      if (UniqueId < 0 || UniqueId >= 1000000000) return false;

      _internal = String.Format("{0:D4}{1:D9}{2:D2}", SiteId, UniqueId, Type);
      _internal = PrefixWithChecksum(_internal);

      if (!Crypt(false, _internal, out ExternalId))
      {
        return false;
      }

      return true;
    }


    private static Boolean Decode(String ExternalId, out Int32 SiteId, out Int32 Type, out Int64 UniqueId)
    {
      String _internal;
      String _computed;

      SiteId = -1;
      Type = -1;
      UniqueId = -1;

      if (!Crypt(true, ExternalId, out _internal))
      {
        return false;
      }

      _computed = PrefixWithChecksum(_internal);

      if (_internal != _computed) return false;

      SiteId = Int32.Parse(_internal.Substring(5, 4));
      UniqueId = Int64.Parse(_internal.Substring(9, 9));
      Type = Int32.Parse(_internal.Substring(18, 2));

      return true;
    }


    private static String PrefixWithChecksum(String InternalExternal)
    {
      UInt64 _tmp_id;
      Byte[] _tmp_raw;
      int _chksum;
      String _internal;

      _internal = InternalExternal.Trim();
      _internal = _internal.PadLeft(15, '0');
      _internal = _internal.Substring(_internal.Length - 15);

      if (!UInt64.TryParse(_internal, out _tmp_id)) return String.Empty;
      _tmp_raw = BitConverter.GetBytes(_tmp_id);

      _chksum = 0;
      foreach (Byte _byte in _tmp_raw)
      {
        _chksum += _byte;
      }

      _chksum = _chksum % BARCODE_CHECKSUM_MODULE;

      _internal = String.Format("{0:D4}{1}", _chksum, _internal);
      _internal = _internal.PadLeft(20, '0');

      return _internal;
    }

    private static Boolean Crypt(Boolean Decrypt, String Input, out String Output)
    {
      Output = String.Empty;

      try
      {
        RC4CryptoServiceProviderExternal _rc4;
        ICryptoTransform _transform;
        UInt64 _tmp_code;
        Byte[] _tmp_raw;

        if (!UInt64.TryParse(Input, out _tmp_code)) return false;

        _rc4 = new RC4CryptoServiceProviderExternal();
        _rc4.KeySize = 64;

        if (Decrypt)
        {
          _transform = _rc4.CreateDecryptor(BARCODE_KEY, BARCODE_IV);
        }
        else
        {
          _transform = _rc4.CreateEncryptor(BARCODE_KEY, BARCODE_IV);
        }

        MemoryStream _ms = new MemoryStream();
        CryptoStream _cs = new CryptoStream(_ms, _transform, CryptoStreamMode.Write);

        _tmp_raw = BitConverter.GetBytes(_tmp_code);
        _cs.Write(_tmp_raw, 0, _tmp_raw.Length);
        _cs.FlushFinalBlock();

        _tmp_raw = _ms.ToArray();
        _tmp_code = BitConverter.ToUInt64(_tmp_raw, 0);

        Output = String.Format("{0:D20}", _tmp_code);

        return true;
      }
      catch
      {
        return false;
      }
    }

  }
}
