//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonDrawTicket.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. DrawTicket
//                2. DrawTicketLogic
//
//        AUTHOR: SSC
// 
// CREATION DATE: 30-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAY-2012 SSC    First release.
// 25-OCT-2013 RCI & LEM    Fixed Bug WIG-311: Draws are not shown for anonymous accounts
// 27-NOV-2013 DLL    Numbers separated per day instead per period
// 14-APR-2014 DLL    Added new functionality: VIP Account
// 19-FEB-2016 RAB    Product Backlog Item 9740:Cajero: impresi�n del n�mero de c�dula del cliente en los vouchers de sorteos
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Windows.Forms;

namespace WSI.Common
{
  public class DrawTicket
  {
    public const Int32 DEFAULT_MAX_NUMBERS_PER_VOUCHER = 300;

    DateTime m_creation_date;
    Int64 m_draw_id;
    DrawType m_draw_type;
    Int64 m_account_id;
    Int64 m_first_draw_num;
    Int64 m_last_draw_num;
    DrawTicketLogic.GetTicketCodes m_code;
    VoucherCardDraw m_voucher;
    Int64 m_ticket_number;
    String m_holder_name;
    String m_track_data;
    String m_draw_name;
    Int32 m_limit;
    Int32 m_limit_per_operation;
    Int64 m_max_number;
    CardData m_card;

    String m_header;
    String m_footer;
    String m_detail_1;
    String m_detail_2;
    String m_detail_3;

    // RCI 21-APR-2011
    Decimal m_number_price;
    Decimal m_number_points;


    Int64 m_max_numbers_available;
    Decimal m_min_played_pct;
    Decimal m_min_spent_pct;
    Decimal m_min_cash_in;
    Boolean m_first_cash_in_constrained;

    Int64 m_last_number;
    Int64 m_first_number;
    Int64 m_max_numbers_per_voucher;

    Int32 m_n;
    Int32 m_m;
    Int32 m_from1;
    Int32 m_from2;
    Int32 m_to1;
    Int32 m_to2;

    Boolean m_has_bingo_format;
    Boolean m_is_vip;

    public ProviderList ProviderList = new ProviderList();
    public DrawOfferList DrawOfferList = new DrawOfferList();

    // HBB 15-MAY-2013: Filter by Level
    Int32 m_level_filter;
    // RCI & JMM 07-JUN-2011: Filter by gender.
    DrawGenderFilter m_gender_filter;

    public void SetOffer(int DayMask, int N, int M, int From, int To, int From2, int To2)
    {
      m_n = N;
      m_m = M;
      m_from1 = From;
      m_from2 = From2;
      m_to1 = To;
      m_to2 = To2;
    }

    public Int64 MaxNumbersPerVoucher
    {
      get { return m_max_numbers_per_voucher; }
      set { m_max_numbers_per_voucher = value; }
    }

    public Int64 FirstNumber
    {
      get { return m_first_number; }
      set { m_first_number = value; }
    }

    public Int64 LastNumber
    {
      get { return m_last_number; }
      set { m_last_number = value; }
    }

    public VoucherCardDraw Voucher
    {
      get
      {
        return m_voucher;
      }
      set
      {
        m_voucher = value;
      }
    }

    public DateTime CreationDate
    {
      get
      {
        return m_creation_date;
      }
      set
      {
        m_creation_date = value;
      }
    }

    public Int64 DrawId
    {
      get
      {
        return m_draw_id;
      }
      set
      {
        m_draw_id = value;
      }
    }

    public DrawType DrawType
    {
      get
      {
        return m_draw_type;
      }
      set
      {
        m_draw_type = value;
      }
    }

    public Int64 AccountId
    {
      get
      {
        return m_account_id;
      }
      set
      {
        m_account_id = value;
      }
    }

    public Int64 FirstDrawNum
    {
      get
      {
        return m_first_draw_num;
      }
      set
      {
        m_first_draw_num = value;
      }
    }

    public Int64 LastDrawNum
    {
      get
      {
        return m_last_draw_num;
      }
      set
      {
        m_last_draw_num = value;
      }
    }

    public Int64 TicketNumber
    {
      get
      {
        return m_ticket_number;
      }
      set
      {
        m_ticket_number = value;
      }
    }

    public String HolderName
    {
      get
      {
        return m_holder_name;
      }
      set
      {
        m_holder_name = value;
      }
    }

    public String TrackData
    {
      get
      {
        return m_track_data;
      }
      set
      {
        m_track_data = value;
      }
    }

    public String DrawName
    {
      get
      {
        return m_draw_name;
      }
      set
      {
        m_draw_name = value;
      }
    }

    public DrawTicketLogic.GetTicketCodes CreationCode
    {
      get
      {
        return m_code;
      }
      set
      {
        m_code = value;
      }
    }

    public Int32 LimitPerDayAndPlayer
    {
      get
      {
        return m_limit;
      }
      set
      {
        m_limit = value;
      }
    }

    public Int32 LimitPerOperation
    {
      get
      {
        return m_limit_per_operation;
      }
      set
      {
        m_limit_per_operation = value;
      }
    }

    public Int64 MaxNumber
    {
      get
      {
        return m_max_number;
      }
      set
      {
        m_max_number = value;
      }
    }

    public CardData Card
    {
      get
      {
        return m_card;
      }
      set
      {
        m_card = value;
      }
    }

    public String Header
    {
      get
      {
        return m_header;
      }
      set
      {
        m_header = value;
      }
    }

    public String Footer
    {
      get
      {
        return m_footer;
      }
      set
      {
        m_footer = value;
      }
    }

    public String Detail1
    {
      get
      {
        return m_detail_1;
      }
      set
      {
        m_detail_1 = value;
      }
    }

    public String Detail2
    {
      get
      {
        return m_detail_2;
      }
      set
      {
        m_detail_2 = value;
      }
    }

    public String Detail3
    {
      get
      {
        return m_detail_3;
      }
      set
      {
        m_detail_3 = value;
      }
    }

    public Decimal NumberPrice
    {
      get { return m_number_price; }
      set { m_number_price = value; }
    }
    public Decimal NumberPoints
    {
      get { return m_number_points; }
      set { m_number_points = value; }
    }

    public Int64 MaxNumbersAvailable
    {
      get { return m_max_numbers_available; }
      set { m_max_numbers_available = value; }
    }
    public Decimal MinPlayedPct
    {
      get { return m_min_played_pct; }
      set { m_min_played_pct = value; }
    }
    public Decimal MinSpentPct
    {
      get { return m_min_spent_pct; }
      set { m_min_spent_pct = value; }
    }
    public Boolean FirstCashInConstrained
    {
      get { return m_first_cash_in_constrained; }
      set { m_first_cash_in_constrained = value; }
    }
    public Decimal MinCashIn
    {
      get { return m_min_cash_in; }
      set { m_min_cash_in = value; }
    }
    public Int32 LevelFilter
    {
      get { return m_level_filter; }
      set { m_level_filter = value; }
    }
    public DrawGenderFilter GenderFilter
    {
      get { return m_gender_filter; }
      set { m_gender_filter = value; }
    }
    public Boolean HasBingoFormat
    {
      get { return m_has_bingo_format; }
      set { m_has_bingo_format = value; }
    }

    public Boolean IsVIP
    {
      get { return m_is_vip; }
      set { m_is_vip = value; }
    }
  }

  public class DrawTicketLogic
  {
    public enum GetTicketCodes
    {
      OK = 0,
      ERROR = 1,
      NO_SALES = 2,
      NO_DRAW = 3,
      LAST_NUMBER_ERROR = 4,
      NUMBERS_ERROR = 5,
      TICKET_ERROR = 6,
      LIMIT_REACHED = 7,
      DRAW_MAX_REACHED = 8,
      PRESELECTED_DRAW_CHANGED = 9,
      PLAYED_OR_SPENT_REACHED = 10,
      MIN_CASH_IN_REACHED = 11,
      CASHIER_SESSION_CLOSED = 12
    }

    public enum DrawSearchType
    {
      BY_CREDITS = 0,
      BY_POINTS = 1,
      BY_CASH_IN = 2
    }

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Generates the Draw Voucher
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DrawTicket
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true or false
    // 
    //   NOTES:
    public static Boolean VoucherGeneration(DrawTicket DrawTicket,
                                            SqlTransaction SqlTrx)
    {
      Boolean _add_numbers;
      Boolean _hide_borders;
      Boolean _include_card_document;

      DrawTicket.Voucher = new VoucherCardDraw(PrintMode.Print, SqlTrx);

      _hide_borders = GeneralParam.GetBoolean("Cashier.DrawTicket", "BingoFormat.HideBorders", false);
      DrawTicket.Voucher.AddString("VOUCHER_CARD_DRAW_HIDE_BORDERS", _hide_borders ? "" : "border: solid 2px black;");

      DrawTicket.Voucher.AddString("VOUCHER_CARD_ACCOUNT_ID", DrawTicket.AccountId.ToString());
      DrawTicket.Voucher.AddString("VOUCHER_CARD_CARD_ID", DrawTicket.TrackData);
      DrawTicket.Voucher.AddString("VOUCHER_CARD_ACCOUNT_NAME", DrawTicket.HolderName);
      // RAB 19-FEB-2016: ShowDocumentId = 0 --> Hide client document.
      //                  ShowDocumentId = 1 --> Show client document.
      _include_card_document = GeneralParam.GetBoolean("Cashier.DrawTicket", "ShowDocumentId", false);
      DrawTicket.Voucher.AddMultipleLineString("VOUCHER_ACCOUNT_INFO", DrawTicket.Card.VoucherAccountInfo(_include_card_document));
      DrawTicket.Voucher.AddString("VOUCHER_CARD_DRAW_TICKET_NUMBER", DrawTicket.TicketNumber.ToString());

      // RCI 21-DEC-2011: Use Bingo format (or not) to add draw numbers
      _add_numbers = DrawTicket.Voucher.AddStringNumbers("VOUCHER_CARD_DRAW_NUMBERS",
                                DrawTicket.FirstDrawNum, DrawTicket.LastDrawNum,
                                DrawTicket.MaxNumber, DrawTicket.HasBingoFormat);

      DrawTicket.Voucher.AddMultipleLineString("VOUCHER_DRAW_HEADER", DrawTicket.Header);
      DrawTicket.Voucher.AddMultipleLineString("VOUCHER_DRAW_FOOTER", DrawTicket.Footer);
      DrawTicket.Voucher.AddMultipleLineString("VOUCHER_CARD_DRAW_DETAIL_1", DrawTicket.Detail1);
      DrawTicket.Voucher.AddMultipleLineString("VOUCHER_CARD_DRAW_DETAIL_2", DrawTicket.Detail2);
      DrawTicket.Voucher.AddMultipleLineString("VOUCHER_CARD_DRAW_DETAIL_3", DrawTicket.Detail3);

      return _add_numbers;
    } // VoucherGeneration

    //------------------------------------------------------------------------------
    // PURPOSE: Save the voucher ID
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Ticket
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true if update ok
    // 
    //   NOTES:
    public static Boolean SaveVoucherId(DrawTicket Ticket,
                                        SqlTransaction SqlTrx)
    {
      StringBuilder _sql_txt;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE   DRAW_TICKETS ");
        _sql_txt.AppendLine("   SET   DT_VOUCHER_ID = @VoucherId ");
        _sql_txt.AppendLine(" WHERE   DT_ID         = @TicketId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@VoucherId", SqlDbType.BigInt).Value = Ticket.Voucher.Id;
          _sql_cmd.Parameters.Add("@TicketId", SqlDbType.BigInt).Value = Ticket.TicketNumber;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("DrawTicket. Error updating DRAW_TICKETS.");

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // SaveVoucherId

    //------------------------------------------------------------------------------
    // PURPOSE: Get all the active draws
    // 
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - DrawId
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DataTable
    // 
    //   NOTES: 
    //
    public static List<DrawTicket> LockApplicableDraws(Int32 AccountLevel,
                                                       Int64 DrawId,
                                                       SqlTransaction SqlTrx)
    {
      SqlTransaction _sql_trx;
      SqlConnection _sql_conn;
      StringBuilder _sql_txt;
      Boolean _limited;
      DrawTicket _draw_ticket;
      List<DrawTicket> _list_draws;
      String _dr_points_col;
      String _dr_points_field;

      _list_draws = new List<DrawTicket>();

      _sql_conn = null;
      _sql_trx = null;

      _dr_points_col = "";
      _dr_points_field = "CAST(0 AS MONEY)";

      // Only use column DR_POINTS_LEVEL when account has a level.
      if (AccountLevel > 0)
      {
        _dr_points_col = "DR_POINTS_LEVEL" + AccountLevel;
        _dr_points_field = "ISNULL(DELETED." + _dr_points_col + ", 0)";
      }

      try
      {
        if (SqlTrx == null)
        {
          // Get Sql Connection 
          _sql_conn = WSI.Common.WGDB.Connection();
          _sql_trx = _sql_conn.BeginTransaction();
        }
        else
        {
          _sql_trx = SqlTrx;
        }

        _limited = false;

        //--
        //-- Lock applicable draws 
        //--
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE   DRAWS ");
        _sql_txt.AppendLine("   SET   DR_LAST_NUMBER = DR_LAST_NUMBER ");
        //-- Columns to retrieve 
        _sql_txt.AppendLine("OUTPUT   DELETED.DR_ID ");
        _sql_txt.AppendLine("       , DELETED.DR_CREDIT_TYPE ");
        _sql_txt.AppendLine("       , DELETED.DR_NUMBER_PRICE ");
        _sql_txt.AppendLine("       , DELETED.DR_LIMITED ");
        _sql_txt.AppendLine("       , DELETED.DR_LIMIT ");
        _sql_txt.AppendLine("       , DELETED.DR_LIMIT_PER_VOUCHER ");
        _sql_txt.AppendLine("       , DELETED.DR_MAX_NUMBER ");
        _sql_txt.AppendLine("       , ISNULL(DELETED.DR_HEADER, '') ");
        _sql_txt.AppendLine("       , ISNULL(DELETED.DR_FOOTER, '') ");
        _sql_txt.AppendLine("       , ISNULL(DELETED.DR_DETAIL1, '') ");
        _sql_txt.AppendLine("       , ISNULL(DELETED.DR_DETAIL2, '') ");
        _sql_txt.AppendLine("       , ISNULL(DELETED.DR_DETAIL3, '') ");
        _sql_txt.AppendLine("       , DELETED.DR_LAST_NUMBER ");
        _sql_txt.AppendLine("       , ISNULL(DELETED.DR_MIN_PLAYED_PCT, 0) ");
        _sql_txt.AppendLine("       , ISNULL(DELETED.DR_MIN_SPENT_PCT, 0) ");
        _sql_txt.AppendLine("       , ISNULL(DELETED.DR_MIN_CASH_IN, 0) ");
        _sql_txt.AppendLine("       , DELETED.DR_INITIAL_NUMBER ");
        _sql_txt.AppendLine("       , DELETED.DR_PROVIDER_LIST ");
        _sql_txt.AppendLine("       , DELETED.DR_OFFER_LIST    ");
        _sql_txt.AppendLine("       , DELETED.DR_NAME    ");
        _sql_txt.AppendLine("       , " + _dr_points_field);
        _sql_txt.AppendLine("       , DELETED.DR_LEVEL_FILTER ");
        _sql_txt.AppendLine("       , DELETED.DR_GENDER_FILTER ");
        _sql_txt.AppendLine("       , DELETED.DR_FIRST_CASH_IN_CONSTRAINED ");
        _sql_txt.AppendLine("       , DELETED.DR_LIMIT_PER_OPERATION ");
        _sql_txt.AppendLine("       , DELETED.DR_BINGO_FORMAT ");
        _sql_txt.AppendLine("       , ISNULL(DELETED.DR_VIP, 0) ");

        _sql_txt.AppendLine(" WHERE   DR_STARTING_DATE <= GETDATE () ");
        _sql_txt.AppendLine("   AND   DR_ENDING_DATE   >  GETDATE () ");
        _sql_txt.AppendLine("   AND   DR_STATUS        =  0 ");

        if (DrawId != 0)
        {
          if (AccountLevel > 0)
          {
            _sql_txt.AppendLine(" AND   " + _dr_points_col + "  >  0 ");
          }
          _sql_txt.AppendLine(" AND   DR_ID = @pDrawId ");
        }
        else
        {
          _sql_txt.AppendLine(" AND   DR_NUMBER_PRICE  >  0 ");
          _sql_txt.AppendLine(" AND   DR_CREDIT_TYPE IN ( @pDrawType1, @pDrawType2, @pDrawType3, @pDrawType4, @pDrawType5 ) ");
        }


        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), _sql_trx.Connection, _sql_trx))
        {
          _sql_cmd.Parameters.Add("@pDrawType1", SqlDbType.Int).Value = DrawType.BY_REDEEMABLE_SPENT;
          _sql_cmd.Parameters.Add("@pDrawType2", SqlDbType.Int).Value = DrawType.BY_PLAYED_CREDIT;
          _sql_cmd.Parameters.Add("@pDrawType3", SqlDbType.Int).Value = DrawType.BY_TOTAL_SPENT;
          _sql_cmd.Parameters.Add("@pDrawType4", SqlDbType.Int).Value = DrawType.BY_CASH_IN;
          _sql_cmd.Parameters.Add("@pDrawType5", SqlDbType.Int).Value = DrawType.BY_CASH_IN_POSTERIORI;
          _sql_cmd.Parameters.Add("@pDrawId", SqlDbType.BigInt).Value = DrawId;


          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _draw_ticket = new DrawTicket();

              _draw_ticket.CreationDate = WGDB.Now;
              _draw_ticket.DrawId = _sql_reader.GetInt64(0);
              _draw_ticket.DrawType = (DrawType)_sql_reader.GetInt32(1);
              _draw_ticket.NumberPrice = (Decimal)_sql_reader.GetSqlMoney(2);
              _draw_ticket.NumberPoints = (Decimal)_sql_reader.GetSqlMoney(20);

              _limited = _sql_reader.GetBoolean(3);
              _draw_ticket.LimitPerDayAndPlayer = _sql_reader.GetInt32(4);
              if (!_limited)
              {
                _draw_ticket.LimitPerDayAndPlayer = Int32.MaxValue;
              }
              _draw_ticket.MaxNumbersPerVoucher = Math.Min(_sql_reader.GetInt32(5), DrawTicket.DEFAULT_MAX_NUMBERS_PER_VOUCHER);
              _draw_ticket.MaxNumber = _sql_reader.GetInt64(6);
              _draw_ticket.Header = _sql_reader.GetString(7);
              _draw_ticket.Footer = _sql_reader.GetString(8);
              _draw_ticket.Detail1 = _sql_reader.GetString(9);
              _draw_ticket.Detail2 = _sql_reader.GetString(10);
              _draw_ticket.Detail3 = _sql_reader.GetString(11);
              _draw_ticket.LastNumber = _sql_reader.GetInt64(12);
              _draw_ticket.MinPlayedPct = _sql_reader.GetDecimal(13);
              _draw_ticket.MinSpentPct = _sql_reader.GetDecimal(14);
              _draw_ticket.MinCashIn = _sql_reader.GetDecimal(15);
              _draw_ticket.FirstNumber = _sql_reader.GetInt64(16);
              if (_draw_ticket.LastNumber != -1)
              {
                _draw_ticket.MaxNumbersAvailable = Math.Max(0, _draw_ticket.MaxNumber - _draw_ticket.LastNumber);
              }
              else
              {
                _draw_ticket.MaxNumbersAvailable = Math.Max(0, _draw_ticket.MaxNumber - _draw_ticket.FirstNumber + 1);
              }

              String _xml;

              _xml = _sql_reader.IsDBNull(17) ? null : _sql_reader.GetString(17);
              _draw_ticket.ProviderList.FromXml(_xml);

              _xml = _sql_reader.IsDBNull(18) ? null : _sql_reader.GetString(18);
              _draw_ticket.DrawOfferList.FromXml(_xml);

              _draw_ticket.DrawName = _sql_reader.GetString(19);

              // HBB 15-MAY-2013: Filter by Level
              _draw_ticket.LevelFilter = _sql_reader.GetInt32(21);

              // RCI & JMM 07-JUN-2011: Filter by gender.
              _draw_ticket.GenderFilter = (DrawGenderFilter)_sql_reader.GetInt32(22);

              _draw_ticket.FirstCashInConstrained = _sql_reader.GetBoolean(23);
              _draw_ticket.LimitPerOperation = _sql_reader.GetInt32(24);

              // RCI 21-DEC-2011: Bingo format flag
              _draw_ticket.HasBingoFormat = _sql_reader.GetBoolean(25);

              _draw_ticket.IsVIP = _sql_reader.GetBoolean(26);

              _list_draws.Add(_draw_ticket);
            } // while _sql_reader
          } // using _sql_reader
        } // using _sql_cmd
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _list_draws.Clear();
      }
      finally
      {
        if (SqlTrx == null)
        {
          if (_sql_trx != null)
          {
            _sql_trx.Dispose();
            _sql_trx = null;
          }

          // Close connection
          if (_sql_conn != null)
          {
            try { _sql_conn.Close(); }
            catch { }
            _sql_conn = null;
          }
        }
      }

      return _list_draws;
    } // LockApplicableDraws

    public static Boolean GetAccountDrawNumbers(Int64 AccountId,
                                                ref DrawNumberList AwardedNumbers,
                                                Boolean OnlyToday,
                                                SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _hours_before;
      DateTime _lower_limit_datetime;

      AwardedNumbers.Clear();

      try
      {
        _hours_before = 0;

        if (!OnlyToday)
        {
          _hours_before = GeneralParam.GetInt32("Cashier", "Draws.HoursBeforeTodayOpening");
        }

        // The openning date of the period
        _lower_limit_datetime = Misc.Opening(Misc.TodayOpening().AddHours(-_hours_before));

        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   DT_DRAW_ID                     ");
        _sb.AppendLine("          , DR_NAME                        ");
        _sb.AppendLine("          , DT_AWARDED_DAY                 ");
        _sb.AppendLine("          , DR_CREDIT_TYPE                 ");
        _sb.AppendLine("          , SUM (DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) ");
        _sb.AppendLine("          , SUM (CASE WHEN DT_PER_POINTS = 1 ");
        _sb.AppendLine("                      THEN DT_LAST_NUMBER - DT_FIRST_NUMBER + 1 ");
        _sb.AppendLine("                      ELSE 0 END)          ");
        _sb.AppendLine("          , ISNULL(DR_VIP, 0)   DR_VIP    ");
        _sb.AppendLine("     FROM   DRAW_TICKETS WITH (INDEX (IX_draw_id) )  ");
        _sb.AppendLine("          , DRAWS                          ");
        _sb.AppendLine("    WHERE   DT_ACCOUNT_ID = @pAccountId    ");
        _sb.AppendLine("      AND   DT_CREATED   >= @pLowerLimitDatetime ");
        _sb.AppendLine("      AND   DT_DRAW_ID    = DR_ID          ");
        _sb.AppendLine(" GROUP BY   DT_DRAW_ID, DR_NAME, DT_AWARDED_DAY, DR_CREDIT_TYPE, DR_VIP ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pLowerLimitDatetime", SqlDbType.DateTime).Value = _lower_limit_datetime;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              DrawTicket _draw;

              _draw = new DrawTicket();
              _draw.DrawId = _sql_reader.GetInt64(0);
              _draw.DrawName = _sql_reader.GetString(1);
              _draw.DrawType = (DrawType)_sql_reader.GetInt32(3);
              _draw.IsVIP = _sql_reader.GetBoolean(6);

              // Grouped by draw id and opening date
              AwardedNumbers.Add(new DrawNumbers(_draw, _sql_reader.GetDateTime(2), 0, 0, _sql_reader.GetInt64(4), _sql_reader.GetInt64(5)));
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        AwardedNumbers.Clear();

        return false;
      }

    } // GetAccountDrawNumbers

    #endregion

  }
}
