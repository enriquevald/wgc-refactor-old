﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: XMLDictionary.cs  
// 
//   DESCRIPTION: XMLDictionary class
//
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 18-AUG-2015
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- ------------------------------------------------------
// 18-AUG-2015 FAV        First release.
// 27-OCT-2015 FAV        Added two generic methods to convert dictionary to xml and xml to dictionary 
//------------------------------------------------------------------------------ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace WSI.Common
{
  public static class XMLDictionary
  {
    #region Public Methods

    /// <summary>
    /// Converts a dictionary to a XML string
    /// </summary>
    /// <param name="FillAmount"></param>
    /// <returns></returns>
    public static String DictionaryToXML<T1, T2>(Dictionary<T1, T2> Dictionary)
    {
      SerializableDictionary<T1, T2> _serializable_dictionary = ConvertDictionaryToSerializable(Dictionary);
      var _serializer = new XmlSerializer(typeof(SerializableDictionary<T1, T2>));
      string _xmlString;

      using (var _sw = new StringWriter())
      {
        using (var _writer = new XmlTextWriter(_sw))
        {
          _writer.Formatting = Formatting.Indented; // indent the Xml so it's human readable
          _serializer.Serialize(_writer, _serializable_dictionary);
          _writer.Flush();
          _xmlString = _sw.ToString();
        }
      }
      return _xmlString;
    }

    /// <summary>
    ///  Converts a XML string to a dictionary
    /// </summary>
    /// <param name="XMLString"></param>
    /// <returns></returns>
    public static Dictionary<T1, T2> XMLToDictionary<T1, T2>(string XMLString)
    {
      XmlNodeReader _reader = null;
      XmlDocument _doc = null;
      var _serializer = new XmlSerializer(typeof(SerializableDictionary<T1, T2>));

      _doc = new XmlDocument();
      _doc.LoadXml(XMLString);

      _reader = new XmlNodeReader(_doc.DocumentElement);

      return ConvertSerializableDictionaryToDictionary((SerializableDictionary<T1, T2>)_serializer.Deserialize(_reader));
    }

    /// <summary>
    /// Converts a sorted dictionary(CurrencyIsoType, Decimal) to a XML string
    /// </summary>
    /// <param name="FillAmount"></param>
    /// <returns></returns>
    public static string SortedDictionaryToXML(SortedDictionary<CurrencyIsoType, Decimal> FillAmount)
    {
      // TODO: Change to generic

      SerializableDictionary<CurrencyIsoType, Decimal> _serializable_dictionary = ConvertSortedDictionaryToSerializable(FillAmount);
      var _serializer = new XmlSerializer(typeof(SerializableDictionary<CurrencyIsoType, Decimal>));
      string _xmlString;

      using (var _sw = new StringWriter())
      {
        using (var _writer = new XmlTextWriter(_sw))
        {
          _writer.Formatting = Formatting.Indented; // indent the Xml so it's human readable
          _serializer.Serialize(_writer, _serializable_dictionary);
          _writer.Flush();
          _xmlString = _sw.ToString();
        }
      }
      return _xmlString;
    }

    /// <summary>
    ///  Converts a XML string to a sorted dictionary(CurrencyIsoType, Decimal)
    /// </summary>
    /// <param name="XMLString"></param>
    /// <returns></returns>
    public static SortedDictionary<CurrencyIsoType, Decimal> XMLToSortedDictionary(string XMLString)
    {
      // TODO: Change to generic

      XmlNodeReader _reader = null;
      XmlDocument _doc = null;
      var _serializer = new XmlSerializer(typeof(SerializableDictionary<CurrencyIsoType, Decimal>));

      _doc = new XmlDocument();
      _doc.LoadXml(XMLString);

      _reader = new XmlNodeReader(_doc.DocumentElement);

      return ConvertSerializableDictionaryToSorted((SerializableDictionary<CurrencyIsoType, Decimal>)_serializer.Deserialize(_reader));
    }

    #endregion

    #region Private Methods

    private static SerializableDictionary<CurrencyIsoType, Decimal> ConvertSortedDictionaryToSerializable(SortedDictionary<CurrencyIsoType, Decimal> SortedDictionary)
    {
      SerializableDictionary<CurrencyIsoType, Decimal> _serializable_dictionary = new SerializableDictionary<CurrencyIsoType, Decimal>();
      foreach (KeyValuePair<CurrencyIsoType, Decimal> entry in SortedDictionary)
      {
        _serializable_dictionary.Add(entry.Key, entry.Value);
      }

      return _serializable_dictionary;
    }

    private static SortedDictionary<CurrencyIsoType, Decimal> ConvertSerializableDictionaryToSorted(SerializableDictionary<CurrencyIsoType, Decimal> SerializableDictionary)
    {
      SortedDictionary<CurrencyIsoType, Decimal> _sorted_dictionary = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      foreach (KeyValuePair<CurrencyIsoType, Decimal> entry in SerializableDictionary)
      {
        _sorted_dictionary.Add(entry.Key, entry.Value);
      }

      return _sorted_dictionary;
    }

    private static SerializableDictionary<T1, T2> ConvertDictionaryToSerializable<T1, T2>(Dictionary<T1, T2> Dictionary)
    {
      SerializableDictionary<T1, T2> _serializable_dictionary = new SerializableDictionary<T1, T2>();
      foreach (KeyValuePair<T1, T2> entry in Dictionary)
      {
        _serializable_dictionary.Add(entry.Key, entry.Value);
      }

      return _serializable_dictionary;
    }

    private static Dictionary<T1, T2> ConvertSerializableDictionaryToDictionary<T1, T2>(SerializableDictionary<T1, T2> SerializableDictionary)
    {
      Dictionary<T1, T2> _sorted_dictionary = new Dictionary<T1, T2>();
      foreach (KeyValuePair<T1, T2> entry in SerializableDictionary)
      {
        _sorted_dictionary.Add(entry.Key, entry.Value);
      }

      return _sorted_dictionary;
    }

    #endregion
  }


  /// <summary>
  /// SerializableDictionary class, this class implements IXmlSerializable that is necesary to work with serialization
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  /// <typeparam name="TValue"></typeparam>
  [XmlRoot("dictionary")]
  public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
  {
    public System.Xml.Schema.XmlSchema GetSchema()
    {
      return null;
    }

    public void ReadXml(System.Xml.XmlReader reader)
    {
      XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
      XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));
      bool wasEmpty = reader.IsEmptyElement;
      reader.Read();
      if (wasEmpty)
        return;

      while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
      {
        reader.ReadStartElement("item");
        reader.ReadStartElement("key");
        TKey key = (TKey)keySerializer.Deserialize(reader);
        reader.ReadEndElement();
        reader.ReadStartElement("value");
        TValue value = (TValue)valueSerializer.Deserialize(reader);
        reader.ReadEndElement();
        this.Add(key, value);
        reader.ReadEndElement();
        reader.MoveToContent();
      }

      reader.ReadEndElement();
    }

    public void WriteXml(System.Xml.XmlWriter writer)
    {
      XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
      XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));
      foreach (TKey key in this.Keys)
      {
        writer.WriteStartElement("item");
        writer.WriteStartElement("key");
        keySerializer.Serialize(writer, key);
        writer.WriteEndElement();
        writer.WriteStartElement("value");
        TValue value = this[key];
        valueSerializer.Serialize(writer, value);
        writer.WriteEndElement();
        writer.WriteEndElement();
      }
    }
  }
}
