﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Catalog.cs
// 
//   DESCRIPTION: Catalog class
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 10-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-NOV-2015 FOS    First version.
// 16-MAR-2018 EOR    Bug 31963:WIGOS-8792 AGG - SITE - Catalogs: the 'Visible' checkbox does not work 
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public  class Catalog
  {
    StringBuilder m_sb;
    SqlTransaction m_trx;
    SqlConnection m_sql_conn;

    #region "Methods"
    public void GetCatalog(Int32 SystemType)
    {
      Int32 _catalogId;
      Object _obj;


      m_sql_conn = WSI.Common.WGDB.Connection();
      m_trx = m_sql_conn.BeginTransaction();

      _catalogId = 0;
      m_sb = new StringBuilder();

      m_sb.AppendLine("     SELECT   CAI_ID ");
      m_sb.AppendLine("       FROM   CATALOGS ");
      m_sb.AppendLine("      WHERE   CAT_SYSTEM_TYPE  =  @pSystemType ");

      using (SqlCommand _sql_cmd = new SqlCommand(m_sb.ToString(), m_trx.Connection, m_trx))
        {
          _sql_cmd.Parameters.Add("@pSystemType", SqlDbType.Int).Value = SystemType;
          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _catalogId = (Int32)_obj;
          }
        }
      m_trx.Commit();
      m_trx.Connection.Close();
      }

    public List<Catalog_Item> GetCatalogItems(Catalog.CatalogSystemType SystemType)
    {
      Object  _catalogId;
      List<Catalog_Item> list = new List<Catalog_Item>();


      m_sql_conn = WSI.Common.WGDB.Connection();
      m_trx = m_sql_conn.BeginTransaction();

      _catalogId = 0;
      m_sb = new StringBuilder();
      

      m_sb.AppendLine("     SELECT   CAI_ID,CAI_NAME, CAT_ID ");
      m_sb.AppendLine("       FROM   CATALOG_ITEMS");
      m_sb.AppendLine("       JOIN   CATALOGS");
      m_sb.AppendLine("         ON   CAT_ID = CAI_CATALOG_ID"); 
      m_sb.AppendLine("      WHERE   CAT_SYSTEM_TYPE  =  @pSystemType ");
      m_sb.AppendLine("        AND   CAI_ENABLED      =  @pEnabled ");

      using (SqlCommand _sql_cmd = new SqlCommand(m_sb.ToString(), m_trx.Connection, m_trx))
        {
          _sql_cmd.Parameters.Add("@pSystemType", SqlDbType.Int).Value = (int)SystemType;
          _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = true;


        SqlDataReader reader = _sql_cmd.ExecuteReader();
        
            Catalog_Item catalog_item;

        while (reader.Read())
            {
              catalog_item = new Catalog_Item();
          catalog_item.CatalogItemId = reader.GetInt64(0);
          catalog_item.Name =  reader.GetString(1); 
          catalog_item.CatalogId = reader.GetInt64(2);
              list.Add(catalog_item);
            }
          }

      m_trx.Connection.Close();
      return list;
    }
    #endregion

    public class Catalog_Item
    {

      #region "Constructors"

      public Catalog_Item() {
        this.CatalogId = 0;
        this.Name = string.Empty;
        this.Description = string.Empty;
        this.CatalogItemId  = 0;
      }

      public Catalog_Item (int CatalogId,string Name, string Description, Int64 ItemId)
      {
        this.CatalogId = CatalogId;
        this.Name = Name;
        this.Description = Description;
        this.CatalogItemId= ItemId ;
      }

      #endregion

      #region "Property"

      private Int64 catalog_id;
      private string name;
      private string description;
      private Int64 catalog_item_id;

      public Int64 CatalogItemId
      {
        get { return catalog_item_id; }
        set { catalog_item_id = value; }
      }

      
      public string Description
      {
        get { return description; }
        set { description = value; }
      }


      public string Name
      {
        get { return name; }
        set { name = value; }
      }


      public Int64 CatalogId
      {
        get { return catalog_id; }
        set { catalog_id = value; }
      }
      #endregion


    }

    public enum CatalogSystemType
    {
      Default = 0,
      ApplyTax = 1,
      RemoveTax = 2,
      // 01-APR-2016 LA   
      ContractType=3,
      AFIP_GamingTablesType = 4,
    }

  }
}
