//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Auditor.cs
// 
//   DESCRIPTION: Auditor class for Cashier
//
//        AUTHOR: MBF
// 
// CREATION DATE: 13-OCT-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-OCT-2011 MBF     First release.
// 05-APR-2012 APR     Added new functions to allow different audit levels.
// 21-JUL-2016 DLL     Fixed bug 13758 Unhandled exception
// 22-JUL-2016 ETP     Rollback Fixed bug 13758 Unhandled exception
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public class Auditor
  {
    #region Attributes

    private Int32 AuditCode;

    private ENUM_GUI GuiId;
    private Int32 UserId;
    private String Username;
    private String ComputerName;

    private DataTable m_auditor_lines;
    private Int32 m_order;

    private String m_sql_insert;

    #endregion

    #region Constructor

    public Auditor(Int32 AuditCode)
    {
      this.AuditCode = AuditCode;

      m_auditor_lines = new DataTable();
      m_auditor_lines.Columns.Add("Level", Type.GetType("System.Int32"));
      m_auditor_lines.Columns.Add("Order", Type.GetType("System.Int32"));
      m_auditor_lines.Columns.Add("NlsId", Type.GetType("System.Int32"));
      m_auditor_lines.Columns.Add("NlsParam01", Type.GetType("System.String"));
      m_auditor_lines.Columns.Add("NlsParam02", Type.GetType("System.String"));
      m_auditor_lines.Columns.Add("NlsParam03", Type.GetType("System.String"));
      m_auditor_lines.Columns.Add("NlsParam04", Type.GetType("System.String"));
      m_auditor_lines.Columns.Add("NlsParam05", Type.GetType("System.String"));

      m_order = 0;

      m_sql_insert = "";
      m_sql_insert += "INSERT INTO   GUI_AUDIT ";
      m_sql_insert += " ( GA_AUDIT_ID ";
      m_sql_insert += " , GA_GUI_ID ";
      m_sql_insert += " , GA_GUI_USER_ID ";
      m_sql_insert += " , GA_GUI_USERNAME ";
      m_sql_insert += " , GA_COMPUTER_NAME ";
      m_sql_insert += " , GA_DATETIME ";
      m_sql_insert += " , GA_AUDIT_CODE ";
      m_sql_insert += " , GA_ITEM_ORDER ";
      m_sql_insert += " , GA_AUDIT_LEVEL ";
      m_sql_insert += " , GA_NLS_ID ";
      m_sql_insert += " , GA_NLS_PARAM01 ";
      m_sql_insert += " , GA_NLS_PARAM02 ";
      m_sql_insert += " , GA_NLS_PARAM03 ";
      m_sql_insert += " , GA_NLS_PARAM04 ";
      m_sql_insert += " , GA_NLS_PARAM05 ";
      m_sql_insert += " ) ";
      m_sql_insert += " VALUES ";
      m_sql_insert += " ( @pAuditId ";
      m_sql_insert += " , @pGuiId ";
      m_sql_insert += " , @pUserId ";
      m_sql_insert += " , @pUsername ";
      m_sql_insert += " , @pComputerName ";
      m_sql_insert += " , GETDATE() ";
      m_sql_insert += " , @pAuditCode ";
      m_sql_insert += " , @pItemOrder ";
      m_sql_insert += " , @pAuditLevel ";
      m_sql_insert += " , @pNlsId ";
      m_sql_insert += " , @pParam01 ";
      m_sql_insert += " , @pParam02 ";
      m_sql_insert += " , @pParam03 ";
      m_sql_insert += " , @pParam04 ";
      m_sql_insert += " , @pParam05 ";
      m_sql_insert += " ) ";
    }

    #endregion

    #region Public Methods

    public static Boolean Audit(ENUM_GUI GuiId,
                                Int32 UserId,
                                String Username,
                                String ComputerName,
                                Int32 AuditCode,
                                Int32 NlsId,
                                String NlsParam01,
                                String NlsParam02,
                                String NlsParam03,
                                String NlsParam04,
                                String NlsParam05)
    {
      Auditor _auditor;

      _auditor = new Auditor(AuditCode);
      _auditor.SetName(NlsId, NlsParam01, NlsParam02, NlsParam03, NlsParam04, NlsParam05);

      return _auditor.Notify(GuiId, UserId, Username, ComputerName);

    } // Audit

    //------------------------------------------------------------------------------
    // PURPOSE : Sets a header for the audit entry.
    //
    //  PARAMS :
    //      - INPUT:
    //            - NlsId
    //            - NlsParam01
    //            - NlsParam02
    //            - NlsParam03
    //            - NlsParam04
    //            - NlsParam05
    //
    //      - OUTPUT :
    //            - None
    //
    public void SetName(Int32 NlsId,
                        String NlsParam01,
                        String NlsParam02,
                        String NlsParam03,
                        String NlsParam04,
                        String NlsParam05)
    {
      SetInternalField(NlsId, NlsParam01, NlsParam02, NlsParam03,
                       NlsParam04, NlsParam05, 0);
    } //SetName

    //------------------------------------------------------------------------------
    // PURPOSE : Sets a subline for the audit entry.
    //
    //  PARAMS :
    //      - INPUT:
    //            - NlsId
    //            - NlsParam01
    //            - NlsParam02
    //            - NlsParam03
    //            - NlsParam04
    //            - NlsParam05
    //
    //      - OUTPUT :
    //            - None
    //
    public void SetField(Int32 NlsId,
                         String NlsParam01,
                         String NlsParam02,
                         String NlsParam03,
                         String NlsParam04,
                         String NlsParam05)
    {
      SetInternalField(NlsId, NlsParam01, NlsParam02, NlsParam03,
                       NlsParam04, NlsParam05, 1);
    } //SetField

    //------------------------------------------------------------------------------
    // PURPOSE : Does the audit registration.
    //
    //  PARAMS :
    //      - INPUT:
    //            - GuiId
    //            - UserId
    //            - Username
    //            - ComputerName
    //
    //      - OUTPUT :
    //            - None
    //
    public Boolean Notify(ENUM_GUI GuiId,
                          Int32 UserId,
                          String Username,
                          String ComputerName)
    {
      this.GuiId = GuiId;
      this.UserId = UserId;
      this.Username = Username;
      this.ComputerName = ComputerName;

      return this.DB_Save();

    } // Notify

    public Boolean Notify(ENUM_GUI GuiId,
                          Int32 UserId,
                          String Username,
                          String ComputerName, SqlTransaction SqlTrx)
    {
      this.GuiId = GuiId;
      this.UserId = UserId;
      this.Username = Username;
      this.ComputerName = ComputerName;

      return this.DB_Save(SqlTrx);

    } // Notify

    public Int32 NumFields
    {
      get { return m_order; }
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Sets an audit entry field.
    //
    //  PARAMS :
    //      - INPUT:
    //            - NlsId
    //            - NlsParam01
    //            - NlsParam02
    //            - NlsParam03
    //            - NlsParam04
    //            - NlsParam05
    //            - Level
    //
    //      - OUTPUT :
    //            - None
    //
    private void SetInternalField(Int32 NlsId,
                                  String NlsParam01,
                                  String NlsParam02,
                                  String NlsParam03,
                                  String NlsParam04,
                                  String NlsParam05,
                                  Int32 Level)
    {
      DataRow _row;

      _row = m_auditor_lines.NewRow();
      _row["Level"] = Level;
      _row["Order"] = m_order;
      _row["NlsId"] = NlsId;
      _row["NlsParam01"] = NlsParam01;
      _row["NlsParam02"] = NlsParam02;
      _row["NlsParam03"] = NlsParam03;
      _row["NlsParam04"] = NlsParam04;
      _row["NlsParam05"] = NlsParam05;
      m_auditor_lines.Rows.Add(_row);

      m_order += 1;
    } // SetInternalField

    private Boolean DB_Save()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        bool success = this.DB_Save(_db_trx.SqlTransaction);
        _db_trx.Commit();

        return success;
      }
    }

    private Boolean DB_Save(SqlTransaction SqlTrx)
    {
      String _sql_str;
      Int64 _audit_id;
      Object _obj;

      _sql_str = " SELECT ISNULL(MAX(GA_AUDIT_ID), 0) + 1 FROM GUI_AUDIT ";

      _audit_id = 0;

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx))
        {
          _obj = _cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            _audit_id = (Int64)_obj;
          }
        }

        if (_audit_id == 0)
        {
          return false;
        }

        using (SqlCommand _cmd = new SqlCommand(m_sql_insert, SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAuditId", SqlDbType.BigInt).Value = _audit_id;
          _cmd.Parameters.Add("@pGuiId", SqlDbType.Int).Value = GuiId;
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _cmd.Parameters.Add("@pUsername", SqlDbType.NVarChar, 50).Value = Username;
          _cmd.Parameters.Add("@pComputerName", SqlDbType.NVarChar, 50).Value = ComputerName;
          _cmd.Parameters.Add("@pAuditCode", SqlDbType.Int).Value = AuditCode;
          _cmd.Parameters.Add("@pItemOrder", SqlDbType.Int).SourceColumn = "Order";
          _cmd.Parameters.Add("@pAuditLevel", SqlDbType.Int).SourceColumn = "Level";
          _cmd.Parameters.Add("@pNlsId", SqlDbType.Int).SourceColumn = "NlsId";
          _cmd.Parameters.Add("@pParam01", SqlDbType.NVarChar, 150).SourceColumn = "NlsParam01";
          _cmd.Parameters.Add("@pParam02", SqlDbType.NVarChar, 150).SourceColumn = "NlsParam02";
          _cmd.Parameters.Add("@pParam03", SqlDbType.NVarChar, 150).SourceColumn = "NlsParam03";
          _cmd.Parameters.Add("@pParam04", SqlDbType.NVarChar, 50).SourceColumn = "NlsParam04";
          _cmd.Parameters.Add("@pParam05", SqlDbType.NVarChar, 50).SourceColumn = "NlsParam05";

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            _da.InsertCommand = _cmd;
            _da.UpdateBatchSize = 500;
            _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

            if (_da.Update(m_auditor_lines) != m_auditor_lines.Rows.Count)
            {
              return false;
            }
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_Save

    #endregion

  }
}
