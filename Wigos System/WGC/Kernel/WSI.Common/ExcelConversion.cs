//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExcelConversion.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 27-DEC-2010
// 
// REVISION HISTORY:
// 
// Date        Author       Description
// ----------- -----------  ----------------------------------------------------------
// 27-DEC-2010 RCI          First release.
// 03-NOV-2011 RCI          GameStatisticsToExcel(): Game name now includes the theorical payout (without braces).
// 23-JAN-2012 JMM          DataTableToExcel(): Columns formatting.
// 24-JAN-2012 RCI          Added routines DataSetToExcel() and GetFullFileName().
// 25-JAN-2012 RCI & AJQ    In FillSheetConnectedTerminals(), compare Providers ignoring case,
//                          so "win" and "WIN" are considered the same provider.
// 13-MAR-2012 MPO          Added switch case for each type of cell in DataTableToSheet
//                          * "System.Decimal","System.DateTime","System.String","System.Integer"
// 14-MAR-2012 RCI          When DataTable has more rows thant allowed by Excel, make another sheet to fill them in.
// 15-MAR-2012 RCI          Added an event function to update a progress bar.
// 28-NOV-2012 RRB          Added routines TerminalsWithoutActivityToExcel() and FillTerminalsWithoutActivity.
// 10-DEC-2012 RRB          Added routines CashierSessionDetailsToExcel and FillSheetCashierSessionDetails.
// 17-DEC-2012 RRB          Added member decimal format, in DataTableToSheet added case when number have decimals.
// 20-FEB-2013 RRB          ConnectedTerminals have in account the terminal type (for the providers).
// 03-MAR-2013 RXM          Added Add ParamsToSheet function and called from all sheet generation points.
// 22-MAR-2013 RXM          Fixed Bug #644: Currency format placed in quotes to allow values of more than one character
// 01-JUL-2013 JAB          Change to new spreadSheetGear dll library.
// 27-AUG-2013 JBC          Added new type of EnumCashierSessionDetailsType
// 07-OCT-2013 RRR          Added WXP SAS Meters mailing.
// 14-OCT-2013 JAB          If we don't have permissions to create excel file, do nothing, show notification to user and write log.
// 16-OCT-2013 ACM          Added ExcelToDataTable function.
// 23-APR-2014 JAB          Added functionality: "REPORT_CASH_SUMMARY" can be printed with preview mode.
// 13-MAY-2014 JAB & RCI    Fixed Bug WIG-918: Error with Excel 2013.
// 14-JUL-2014 LEM          Use WSI.Common.Format.DateTimeCustomFormatString to set DateTime format.
// 19-AUG-2014 RRR          Added routine CageSessionReportToExcel
// 31-DEC-2014 JMV          Added methods for MachineAndGameReport
// 12-FEB-2015 DRV          Fixed Bug WIG-2042: Machine and game report is not beeing send
// 22-APR-2015 JMV          Fixed bug WIG-2233: Reporte diario de m�quinas: errores en el mail
// 28-APR-2015 JMV          Fixed bug TFS BI 1384: RFranco: Excel mailing no se visualiza bien desde m�vil
// 15-SEP-2015 ETP          Fixed bug 4150: New format for account movements, account movements (SAT) and cash movements.
// 20-OCT-2015 ECP          Added generic method (ConvertCells) to convert Range of Cells to a specific format. Backlog Item 1080 GUI - Improvements to Gaming Table Plays Sessions Import
// 20-OCT-2015 ECP          Added public method to Convert a Date in gregorian format to DateTime format. Backlog Item 1080 GUI - Improvements to Gaming Table Plays Sessions Import
// 24-NOV-2015 ECP          Backlog Item 6463 Garcia River-09 Cash Report Resume - Include Tickets Tito
// 16-MAY-2016 LTC          Bug 13164:Mejora - ReportsTool - Puerto Rico - Reset button missing
// 19-MAY-2016 EOR          Product Backlog Item 12810:(PHASE 2) Activity in Tables: Mailing
// 30-MAY-2016 EOR          Fixed Bug 13783:GUI, Resumen de caja - canje de fichas: Revisar el excel y la impresi�n
// 14-JUN-2016 EOR          Product Backlog Item 13672:Sprint Review 24 Winions - Mex
// 29-JUN-2016 RGR          Product Backlog Item 13573 : Generic Report: GUI generation Screen
// 17-AUG-2016 RGR          Bug 16711: Generic Reports - vault by currency movements from Mailing. Malformed Excel column Date
// 24-AUG-2016 EOR          Fixed Bug 16992: Mailing: The mailing of  Connected Terminals not work
// 11-OCT-2016 ETP          Fixed Bug 18820: Correct tax custody and tax provision values
// 19-OCT-2016 EOR          Fixed Bug 18486: Mailing Connected Incorrect data in the day one of the month
// 20-OCT-2016 EOR          Fixed Bug 18063: Mailing: The excel file receveided for email the type "Contadores recolecci�n a recolecci�n" without data
// 09-NOV-2016 EOR          PBI 19870: R.Franco: New report machines and games
// 25-ENE-2017  DPC         Bug 23052:Resumen de Caja: excel e impresi�n no incluyen algunos conceptos
// 13-JUL-2017 EOR          Bug 28701: WIGOS-3588 Wrong format in first row for a column in Excel file when exporting the Cash Mov. and Cash Mov. (SAT)
// 17-JUL-2017 EOR          Bug 28701: WIGOS-3588 Wrong format in first row for a column in Excel file when exporting the Cash Mov. and Cash Mov. (SAT)
// 24-JUL-2017 RAB          PBI 28627: WIGOS-3193 Score report - Report - Send by email
// 02-AUG-2017 RAB          Bug 29129:WIGOS-4131 Score report - The score report received by e-mail shows a incorrect Win/Loss amount
// 02-AUG-2017 RAB          Bug 29130:WIGOS-4132 Score report - Total amounts are missing for Win/Loss and Drop columns on the score report received by e-mail
// 03-AUG-2017 RAB          Bug 29168:WIGOS-4198 Score report - The amount of Final drop sum all drop amounts. It is the same than Total Drop
// 07-AUG-2017 RAB          Bug 29129:WIGOS-4131 Score report - The score report received by e-mail shows a incorrect Win/Loss amount
// 16-AUG-2017 RAB          Bug 29357:WIGOS-4431 [Ticket #7886] exportaci�n a excel
// 17-AUG-2017 RAB          Bug 29332:WIGOS-4150 Score report - Close gaming table amounts are not considered on WinLoss "Final" column
// 18-AUG-2017 RAB          Bug 29404:WIGOS-4613 Score report, columns Final and Total show values when session is not closed
// 27-SEP-2017 RAB          Bug 29490:WIGOS-4775 [Ticket #8366] Issue: Reporte Score por Hora - Formulas V03.06.0023
// 18-OCT-2017 RAB          Bug 29490:WIGOS-4775 [Ticket #8366] Issue: Reporte Score por Hora - Formulas V03.06.0023
// 19-DEC-2017 JML          Fixed Bug 29490:WIGOS-4775 [Ticket #8366] Issue: Reporte Score por Hora - Formulas V03.06.0023
// 05-JAN-2017 EOR          PBI 30849: Multisite: Reporte gen�rico terminales
// 08-MAR-2018 JML          Fixed Bug 31852:WIGOS-8719 Cut string in the "Movimientos de boveda por divisa" excel report
// 09-MAR-2018 AGS          Bug 31873:WIGOS-8796 string "Commissions" appears truncated when export Cash Summary report
// 25-APR-2018 RGR          Bug 32447: WIGOS-7345 Tables - Score report: The score report received by e-mail is diferent than the score report from windows form and excel file 
//------------------------------------------------------------------------------

#region Using
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using SpreadsheetGear;
using WSI.Common.GenericReport;
#endregion

namespace WSI.Common
{
  public delegate void ProgressBarEventHandler(Int32 Count, Int32 Max);

  public static class ExcelConversion
  {
    //Colors used -------------------------------------------------------------------------------
    //EXCEL_COLOR_INDEX_RED         --> SpreadsheetGear.Color.FromArgb(255, 000, 000);
    //EXCEL_COLOR_INDEX_YELLOW      --> SpreadsheetGear.Color.FromArgb(255, 255, 000);
    //EXCEL_COLOR_INDEX_LIGHT_BLUE 	--> SpreadsheetGear.Color.FromArgb(079, 129, 189);
    //EXCEL_COLOR_INDEX_DARK_BLUE 	--> SpreadsheetGear.Color.FromArgb(000, 051, 102);
    //EXCEL_COLOR_INDEX_WHITE       --> SpreadsheetGear.Color.FromArgb(255, 255, 255);
    //EXCEL_COLOR_INDEX_GRAY        --> SpreadsheetGear.Color.FromArgb(192, 192, 192);
    //-------------------------------------------------------------------------------------------

    #region Events

    public static event ProgressBarEventHandler ProgressBar;

    #endregion

    #region Enums

    public enum EXCEL_VERSION
    {
      BEFORE_2007,
      AFTER_2007
    }

    public enum EnumCashierSessionDetailsType
    {
      DEFAULT = 0,
      BOLD = 1,
      TOPBORDER = 2,
      TOPBORDER_BOLD = 3,
      YELLOW = 4,
      ALIGN_RIGHT = 5,
      TOPBORDER_ALIGN_RIGHT = 6,
      BOLD_ALIGN_RIGHT = 7,
    }

    public enum CalledFrom
    {
      WigosGUI,
      Mailing
    }


    #endregion

    #region Structs

    public class ExcelFileLocation
    {
      public String Filepath;
      public String Filename;
      public Int32 NlsId;
      public Boolean TypeMode;
      public Boolean ShowFile;
      public Boolean ExecutionSuccess;
      public Boolean ShowFileExportedForm;
      public Boolean HasPermissionsOverDirectory;

      public ExcelFileLocation()
      {
        HasPermissionsOverDirectory = ExcelConversion.HasPermissionsOverDirectory(ref Filepath);
      }
      public ExcelFileLocation(String Path)
      {
        HasPermissionsOverDirectory = ExcelConversion.HasPermissionsOverDirectory(ref Path);
        if (HasPermissionsOverDirectory)
        {
          Filepath = Path;
        }
      }
    }

    #endregion

    #region Properties

    public static Int32 ExcelMaxRows
    {
      get { return ExcelConversion.m_excel_max_rows; }
    }
    public static EXCEL_VERSION ExcelVersion
    {
      get { return ExcelConversion.m_excel_version; }
    }

    #endregion

    #region Members

    // MAX ROWS (EXCEL 2003)      --> 65.536
    // MAX ROWS (EXCEL 2007/2010) --> 1.048.576
    public const Int32 EXCEL_MAX_ROWS_BEFORE_2007 = 65000;
    public const Int32 EXCEL_MAX_ROWS_AFTER_2007 = 1048000;

    // For Games Statistics
    private const Int32 GAMES_HEADER_ROW = 4;
    private const Int32 ROW_OFFSET = 3; //5;  // LTC 16-MAY-2016
    private const Int32 COL_OFFSET = 0; //2;  // LTC 16-MAY-2016
    private const String OUTPUT_FOLDER = "Reports";
    // For Terminals Connected
    private const Int32 TERMS_HEADER_ROW = 4;

    private static EXCEL_VERSION m_excel_version = EXCEL_VERSION.AFTER_2007;
    private static Int32 m_excel_max_rows = EXCEL_MAX_ROWS_AFTER_2007;
    private static String m_currency_format;
    private static String m_number_format;
    private static String m_percent_format;
    private static String m_decimal_format;
    private static String m_currency_simbol;
    private static String m_currency_separator;
    private static String m_percent_simbol;
    private static String m_number_separator;
    private static String m_percent_separator;

    private static Boolean m_draw_spaces = false;
    private static Int32 GAMES_DATE;
    private static Int32 GAMES_PLAYED_AMOUNT;
    private static Int32 GAMES_WON_AMOUNT;
    private static Int32 GAMES_PAYOUT_PCT;
    private static Int32 GAMES_JACKPOT;
    private static Int32 GAMES_CONTRIBUTION_PCT;
    private static Int32 GAMES_CONTRIBUTION_AMOUNT;
    private static Int32 GAMES_NETWIN;
    private static Int32 GAMES_REAL_NETWIN;
    private static Int32 GAMES_REAL_PAYOUT_PCT;
    private static Int32 GAMES_PLAYS_COUNT;
    private static Int32 GAMES_WON_COUNT;
    private static Int32 GAMES_WON_PCT;
    private static Int32 GAMES_AVERAGE_PLAY;
    private static Int32 GAMES_FIRST_COLUMN;
    private static Int32 GAMES_LAST_COLUMN;
    private static Int32 GAMES_VLT;
    private static Int32 GAMES_NW_VLT;
    private static Int32 TERMS_DAY_START;
    private static Int32 TERMS_DAY_END;
    private static Int32 TERMS_PROVIDER;
    private static Int32 TERMS_TERMINAL_TYPE;
    private static Int32 TERMS_TOTAL;
    private static Int32 TERMS_LAST_COLUMN;

    // SCORE REPORT
    private static Int32 SCORE_REPORT_NUM_HEADERS = 2;

    // SCORE REPORT: Positions (rows and columns) of the title, working day, source data and table with the results
    private static Int32 SCORE_REPORT_TITLE_POS_ROW = 3;
    private static Int32 SCORE_REPORT_TITLE_POS_COLUMN = 3;
    private static Int32 SCORE_REPORT_SOURCE_DATA_POS_ROW = 5;
    private static Int32 SCORE_REPORT_SOURCE_DATA_POS_COLUMN = 3;
    private static Int32 SCORE_REPORT_WORKING_DAY_POS_ROW = 6;
    private static Int32 SCORE_REPORT_WORKING_DAY_POS_COLUMN = 3;
    private static Int32 SCORE_REPORT_INITIAL_POS_ROW = 10;
    private static Int32 SCORE_REPORT_INITIAL_POS_COLUMN = 3;
    private static Int32 SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN = 4;

    // SCORE REPORT: Column number where you will get the first hour of the game session
    private static Int32 SCORE_REPORT_SQL_COLUMN_FIRST_HOUR = 9;

    // SCORE REPORT: Number of columns per group and total of dynamic groups
    private static Int32 SCORE_REPORT_GAMBLING_TABLE_GROUP_COLUMNS = 2;
    private static Int32 SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS = 4;
    private static Int32 SCORE_REPORT_DYNAMIC_NUM_GROUPS = 24;

    // SCORE REPORT: Label to differentiate the different groups from the table
    private static String SCORE_REPORT_INITIAL_GROUP_LABEL = "INITIAL_GROUP";
    private static String SCORE_REPORT_TOTAL_GROUP_LABEL = "TOTAL_GROUP";
    private static String SCORE_REPORT_SUBTOTAL_LABEL = "SUBTOTAL";

    //For import Excel files
    private const int FIRST_DATA_ROW = 2;

    #endregion

    #region PublicFunctions

    //------------------------------------------------------------------------------
    // PURPOSE : Call ProgressBar Event.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 Count
    //          - Int32 Max
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - void: ---.
    //
    public static void OnProgressBar(Int32 Count, Int32 Max)
    {
      if (ProgressBar != null)
      {
        ProgressBar(Count, Max);
      }
    } // OnProgressBar

    //------------------------------------------------------------------------------
    // PURPOSE : Add the extension depending on the Excel version to the parameter Filename.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String Filename
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String: The full filename.
    //
    public static String GetFullFileName(String Filename)
    {
      CultureInfo _old_ci;
      Excel.Application _excel;

      _old_ci = Thread.CurrentThread.CurrentCulture;
      _excel = null;

      try
      {
        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        try
        {
          _excel = new Excel.ApplicationClass();
        }
        catch (Exception _ex)
        {
          _excel = null;
          Log.Exception(_ex);
        }

        if (_excel == null)
        {
          Log.Error("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.");
          Filename = Filename + ".xlsx";

          return Filename;
        }

        if (_excel.Version.CompareTo("12") >= 0)
        {
          Filename = Filename + ".xlsx";
        }
        else
        {
          Filename = Filename + ".xls";
        }

        return Filename;
      }
      finally
      {
        if (_excel != null)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }
        ReleaseComObject(_excel);

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // GetFullFileName

    //------------------------------------------------------------------------------
    // PURPOSE : To call DataSetToExcel without Boolean parameters HasMoreSheets and IncludeHeaderAndFormat.
    //
    //  PARAMS :
    //      - INPUT :
    //          - List<ReportToolDesignSheetsDataDTO>: metadaReport
    //          - String: Filename
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: Return true if execution is right.
    //
    public static Boolean DataSetToExcel(IList<ReportToolDesignSheetsDataDTO> metadataReport, String fileName, Dictionary<String, String> Params)
    {
      //EOR 22-NOV-2017
      Boolean _has_more_sheets;
      DataSet _dataSet;

      _dataSet = GetDataSet(metadataReport);

      return DataSetToExcel(_dataSet, fileName, out _has_more_sheets, Params, true, false);
    }

    private static DataSet GetDataSet(IList<ReportToolDesignSheetsDataDTO> metadataReport)
    {
      DataSet _dataSet;

      if (metadataReport.Count > 0)
      {
        _dataSet = metadataReport[0].DataReport.DataSet;
      }
      else
      {
        _dataSet = new DataSet();
      }

      return _dataSet;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : To call DataSetToExcel without Boolean parameters HasMoreSheets and IncludeHeaderAndFormat.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet: Set
    //          - String: Filename
    //          - Dictionary<String, String>: Params
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: Return true if execution is right.
    //
    public static Boolean DataSetToExcel(DataSet Set, String Filename, Dictionary<String, String> Params)
    {
      Boolean _has_more_sheets;

      return DataSetToExcel(Set, Filename, out _has_more_sheets, Params, true);
    } // DataSetToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : To call DataSetToExcel with Include Header and without HasMoreSheets
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet: Set
    //          - String: Filename
    //          - Dictionary<String, String>: Params
    //          - Boolean: IncludeHeaderAndFormat
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: Return true if execution is right.
    //
    public static Boolean DataSetToExcel(DataSet Set, String Filename, Dictionary<String, String> Params, Boolean IncludeHeaderAndFormat)
    {
      Boolean _has_more_sheets;

      return DataSetToExcel(Set, Filename, out _has_more_sheets, Params, IncludeHeaderAndFormat);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : To call DataSetToExcel with HasMoreSheets and without Include Header
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet: Set
    //          - String: Filename
    //          - Dictionary<String, String>: Params
    //
    //      - OUTPUT :
    //          - Boolean HasMoreSheets
    //
    // RETURNS :
    //      - Boolean: Return true if execution is right.
    //    
    public static Boolean DataSetToExcel(DataSet Set, String Filename, out Boolean HasMoreSheets, Dictionary<String, String> Params)
    {
      return DataSetToExcel(Set, Filename, out HasMoreSheets, Params, true);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Generic Conversion from DataSet to Excel file.
    //           Each Table from the DataSet to an Excel Sheet.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet Set
    //          - String Filename
    //          - Dictionary<String, String>: Params
    //          - Boolean: IncludeHeaderAndFormat
    //
    //      - OUTPUT :
    //          - Boolean HasMoreSheets
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean DataSetToExcel(DataSet Set, String Filename, out Boolean HasMoreSheets, Dictionary<String, String> Params, Boolean IncludeHeaderAndFormat, bool IncludeCurrencySimbol = true)
    {
      CultureInfo _old_ci;

      Excel.Application _excel;

      IWorkbook _book;
      IWorksheet _sheet;
      Int32 _idx_sheet;
      Int32 _num_needed_sheets;
      Int32 _idx_start_row;
      Int32 _num_sheets_per_table;
      Boolean _deleted_row;

      HasMoreSheets = false;

      _old_ci = Thread.CurrentThread.CurrentCulture;

      _excel = null;
      _book = null;
      _sheet = null;

      try
      {
        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        // Restore DateTime and Number Format.
        Thread.CurrentThread.CurrentCulture.DateTimeFormat = _old_ci.DateTimeFormat;
        Thread.CurrentThread.CurrentCulture.NumberFormat = _old_ci.NumberFormat;

        _excel = null;
        try
        {
          _excel = new Excel.ApplicationClass();
          _excel.DisplayAlerts = false;
        }
        catch
        {
          _excel = null;
        }

        SetFileExtensionAndMaxRows(_excel, ref Filename);

        // Calculate the needed sheets according to the maximum allowed rows.
        // Must be calculated after calling SetFileExtensionAndMaxRows().
        _num_needed_sheets = 0;
        foreach (DataTable _table in Set.Tables)
        {
          if (_table.Rows.Count > 0)
          {
            _num_sheets_per_table = (Int32)Math.Ceiling((Decimal)_table.Rows.Count / m_excel_max_rows);
          }
          else
          {
            _num_sheets_per_table = 1;
          }
          if (_num_sheets_per_table > 1)
          {
            HasMoreSheets = true;
          }
          _num_needed_sheets += _num_sheets_per_table;
        }

        // Initilize excel spreadsheetgear.
        _book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        // Add needed sheets.
        while (_num_needed_sheets - _book.Worksheets.Count > 0)
        {
          _book.Worksheets.Add();
        }
        // Or close unused sheets.
        while (_book.Worksheets.Count > _num_needed_sheets && _book.Worksheets.Count > 1)
        {
          _sheet = _book.Worksheets[_book.Worksheets.Count - 1];
          _sheet.Delete();
        }

        CalculateNumberFormats(IncludeCurrencySimbol);

        _idx_sheet = 0;
        foreach (DataTable _table in Set.Tables)
        {
          _deleted_row = false;

          // LTC 16-MAY-2016
          if (_table.Rows.Count == 0)
          {
            DataRow Dr = _table.NewRow();
            _table.Rows.Add(Dr);
            _deleted_row = true;
          }

          _idx_start_row = 0;
          _num_sheets_per_table = (Int32)Math.Ceiling((Decimal)_table.Rows.Count / m_excel_max_rows);
          OnProgressBar(0, _table.Rows.Count);

          for (Int32 _num_sheet = 0; _num_sheet < _num_sheets_per_table; _num_sheet++)
          {
            _sheet = _book.Worksheets[_idx_sheet];

            if (_deleted_row)
            {
              _table.Rows.RemoveAt(0);
            }

            DataTableToSheet(_sheet, _table, _idx_start_row, Params, IncludeHeaderAndFormat);

            _idx_sheet++;
            _idx_start_row += m_excel_max_rows;
          }
          OnProgressBar(_table.Rows.Count, _table.Rows.Count);
        }

        if (_num_needed_sheets > 1)
        {
          _sheet = _book.Worksheets[0];
          _sheet.Select();
        }

        FileStream _file = File.Create(Filename);
        switch (ExcelVersion)
        {
          case EXCEL_VERSION.AFTER_2007:
            _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
            break;

          case EXCEL_VERSION.BEFORE_2007:
            _book.SaveToStream(_file, FileFormat.Excel8);
            break;
        }
        _file.Close();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_excel != null)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }
        ReleaseComObject(_excel);

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // DataSetToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : To call DataTableToExcel without Boolean parameter.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable: Table
    //          - String: Filename
    //          - Dictionary<String, String>: Params
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: Return true if execution is right.
    //
    public static Boolean DataTableToExcel(DataTable Table, String Filename, Dictionary<String, String> Params)
    {
      Boolean _has_more_sheets;

      return DataTableToExcel(Table, Filename, out _has_more_sheets, Params);
    } // DataTableToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Generic Conversion from DataTable to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String Filename
    //
    //      - OUTPUT :
    //          - Boolean HasMoreSheets
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean DataTableToExcel(DataTable Table, String Filename, out Boolean HasMoreSheets, Dictionary<String, String> Params)
    {
      CultureInfo _old_ci;

      Excel.Application _excel;

      IWorkbook _book;
      IWorksheet _sheet;
      Int32 _idx_sheet;
      Int32 _num_needed_sheets;
      Int32 _idx_start_row;

      HasMoreSheets = false;

      _old_ci = Thread.CurrentThread.CurrentCulture;

      _excel = null;
      _book = null;
      _sheet = null;

      try
      {
        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        // Restore DateTime and Number Format.
        Thread.CurrentThread.CurrentCulture.DateTimeFormat = _old_ci.DateTimeFormat;
        Thread.CurrentThread.CurrentCulture.NumberFormat = _old_ci.NumberFormat;

        _excel = null;
        try
        {
          _excel = new Excel.ApplicationClass();
          _excel.DisplayAlerts = false;
        }
        catch
        {
          _excel = null;
        }

        SetFileExtensionAndMaxRows(_excel, ref Filename);

        // Calculate the needed sheets according to the maximum allowed rows.
        // Must be calculated after calling SetFileExtensionAndMaxRows().
        _num_needed_sheets = (Int32)Math.Ceiling((Decimal)Table.Rows.Count / m_excel_max_rows);
        if (_num_needed_sheets > 1)
        {
          HasMoreSheets = true;
        }

        // Initilize excel spreadsheetgear.
        _book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        // Add needed sheets.
        while (_num_needed_sheets - _book.Worksheets.Count > 0)
        {
          _book.Worksheets.Add();
        }
        // Or close unused sheets.
        while (_book.Worksheets.Count > _num_needed_sheets && _book.Worksheets.Count > 1)
        {
          _sheet = _book.Worksheets[_book.Worksheets.Count - 1];
          _sheet.Delete();
        }

        CalculateNumberFormats();

        _idx_sheet = 0;
        _idx_start_row = 0;
        OnProgressBar(0, Table.Rows.Count);

        for (Int32 _num_sheet = 0; _num_sheet < _num_needed_sheets; _num_sheet++)
        {
          _sheet = _book.Worksheets[_idx_sheet];

          DataTableToSheet(_sheet, Table, _idx_start_row, Params);

          _idx_sheet++;
          _idx_start_row += m_excel_max_rows;
        }
        OnProgressBar(Table.Rows.Count, Table.Rows.Count);

        if (_num_needed_sheets > 1)
        {
          _sheet = _book.Worksheets[0];
          _sheet.Select();
        }

        // Save excel file.
        FileStream _file = File.Create(Filename);
        switch (ExcelVersion)
        {
          case EXCEL_VERSION.AFTER_2007:
            _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
            break;

          case EXCEL_VERSION.BEFORE_2007:
            _book.SaveToStream(_file, FileFormat.Excel8);
            break;
        }
        _file.Close();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_excel != null)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // DataTableToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Generic Conversion from Excel file to DataTable
    //
    //  PARAMS :
    //      - INPUT :
    //          - String Filename
    //          - DataTable TableDefinition. Three Columns ->  0: String "ColumnName", 1: String "ColumnType", 2: Int32 "ColumnIndex"
    //
    //      - OUTPUT :
    //          - DataTable ImportedData
    //
    // RETURNS :
    //      - True: Imported Ok. False: Otherwise.
    //
    public static Boolean ExcelToDataTable(String ExcelFilePath, DataTable TableDefinition, out DataTable ImportedData)
    {
      return ExcelToDataTable(ExcelFilePath, TableDefinition, true, out ImportedData);
    }

    public static Boolean ExcelToDataTable(String ExcelFilePath, DataTable TableDefinition, Boolean ExcelUseCurrentRegion, out DataTable ImportedData)
    {
      ImportedData = new DataTable();

      if (!File.Exists(ExcelFilePath) || TableDefinition.Rows.Count == 0)
      {
        return false;
      }

      Dictionary<String, Int32> _columns_locations;

      _columns_locations = new Dictionary<String, Int32>();

      try
      {
        // TableDefinition 
        foreach (DataRow _row in TableDefinition.Rows)
        {
          ImportedData.Columns.Add(_row[0].ToString(), Type.GetType(_row[1].ToString()));
          _columns_locations.Add(_row[0].ToString(), (Int32)_row[2]);
        }

        //Initial SpredSheetGear objects
        SpreadsheetGear.IWorkbook _excel_book = null;
        SpreadsheetGear.IWorksheet _excel_sheet = null;
        SpreadsheetGear.IRange _excel_range = null;
        Object[,] _range_values;
        Int32 _num_rows;
        String _down_address;

        //Open book,sheet 
        _excel_book = Factory.GetWorkbook(ExcelFilePath);
        _excel_sheet = _excel_book.Worksheets[0];

        // DHA 01-APR-2015 set excel region to process
        if (ExcelUseCurrentRegion)
        {
          _excel_range = _excel_sheet.UsedRange.CurrentRegion;
        }
        else
        {
          _excel_range = _excel_sheet.UsedRange;
        }

        //select final row
        _down_address = _excel_range.GetAddress(false, false, ReferenceStyle.A1, false, null);
        //save range into array
        _excel_range = _excel_sheet.Range[_down_address];
        if (_excel_range.CellCount > 1)
        {
          _range_values = (object[,])_excel_range.Value; //Fill array with data from Excel Range.
          _num_rows = _range_values.GetLength(0); //get the num of rows  
        }
        else
        {
          //Just one cell filled
          _range_values = new object[1, 1];
          _range_values[0, 0] = _excel_range.Value;
          _num_rows = 1;
        }

        // For each row
        for (Int32 _idx_row = FIRST_DATA_ROW - 1; _idx_row < _num_rows; _idx_row++)
        {
          DataRow _new_row;

          _new_row = ImportedData.NewRow();

          //For each column
          foreach (DataColumn _column in ImportedData.Columns)
          {
            Object _cell_value;
            Int64 _cell_value_int_64;
            Int32 _cell_value_int_32;
            Decimal _cell_value_decimal;
            DateTime _cell_value_datetime;

            try
            {
              // Out of range column index
              if (_columns_locations[_column.ColumnName] >= _range_values.GetLength(1))
              {
                continue;
              }

              // Get cell value
              _cell_value = _range_values[_idx_row, _columns_locations[_column.ColumnName]];

              if (_cell_value == null)
              {
                continue;
              }

              // Convert to Column data type
              switch (Type.GetTypeCode(_column.DataType))
              {
                case TypeCode.String:
                  _cell_value = _cell_value.ToString();
                  break;

                case TypeCode.Decimal:
                  if (Decimal.TryParse(_cell_value.ToString(), out _cell_value_decimal))
                  {
                    _new_row[_column] = _cell_value_decimal;
                  }
                  continue;

                case TypeCode.Int64:
                  if (Int64.TryParse(_cell_value.ToString(), out _cell_value_int_64))
                  {
                    _new_row[_column] = _cell_value_int_64;
                  }
                  continue;

                case TypeCode.Int32:
                  if (Int32.TryParse(_cell_value.ToString(), out  _cell_value_int_32))
                  {
                    _new_row[_column] = _cell_value_int_32;
                  }
                  continue;

                case TypeCode.DateTime:
                  if (_cell_value != DBNull.Value && ConvertDateTime(_cell_value, out _cell_value_datetime))
                  {
                    _new_row[_column] = _cell_value_datetime;
                  }
                  continue;

                default:
                  Convert.ChangeType(_cell_value, _column.DataType);
                  break;
              }
            }
            catch (Exception)
            {
              continue;
            }
            _new_row[_column] = _cell_value;
          }

          ImportedData.Rows.Add(_new_row);
        }

        return true;
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return false;

    } // ExcelToDataTable

    //------------------------------------------------------------------------------
    // PURPOSE : Conversion from Game Statistics to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - String Filename
    //
    //      - OUTPUT :
    //          - String Filename
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean GameStatisticsToExcel(DataSet DataSet,
                                                DateTime FromDate,
                                                DateTime ToDate,
                                                ref String Filename,
                                                Dictionary<String, String> Params)
    {
      IWorkbook _book;
      IWorksheet _sheet;
      IRange _range;
      IFont _font;
      DataTable _dt_only_game_names;
      Int32 _num_games;
      Int32 _idx_sheet;
      DataSetHelper _dsh;
      DataTable _dt_games;
      DataTable _dt_connected;
      DataTable _total_sales;
      FileStream _file;

      _book = null;
      _sheet = null;

      try
      {
        _dt_games = DataSet.Tables["GAMES"];
        _dt_connected = DataSet.Tables["CONNECTED"];

        _dt_only_game_names = _dt_games.DefaultView.ToTable(true, "Game");
        _num_games = _dt_only_game_names.Rows.Count;

        if (Params == null || Params.Count == 0)
        {
          SetExcelColumnIndices(0);
        }
        else
        {
          SetExcelColumnIndices(3);
        }

        SetFileExtensionAndMaxRows(null, ref Filename);

        // Initilize excel spreadsheetgear.
        _book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        // If no games, send empty Excel file.
        if (_num_games == 0)
        {
          if (_book.Worksheets.Count == 0)
          {
            _book.Worksheets.Add();
          }
          while (_book.Worksheets.Count > 1)
          {
            _sheet = _book.Worksheets[_book.Worksheets.Count];
            _sheet.Delete();
          }

          _sheet = _book.Worksheets[0];
          _range = _sheet.Cells[3, 2];
          _font = _range.Font;

          _range.Value = "NO HAY DATOS";
          _font.Size = 16;
          _font.Name = "Calibri";
          _font.Bold = true;

          _file = File.Create(Filename);

          _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
          _file.Close();

          return true;
        }

        // Add needed sheets.
        while (_num_games - _book.Worksheets.Count > 0)
        {
          _book.Worksheets.Add();
        }
        // Or close unused sheets.
        while (_book.Worksheets.Count > _num_games)
        {
          _sheet = _book.Worksheets[_book.Worksheets.Count];
          _sheet.Delete();
        }

        CalculateNumberFormats();

        // RCI 07-APR-2011: Add TOTAL tab in first place.
        _sheet = _book.Worksheets[0];
        _book.Worksheets.AddBefore(_sheet);

        // Get a DataTable with the totals.
        _dsh = new DataSetHelper();
        _total_sales = _dsh.SelectGroupByInto("TOTAL", _dt_games,
                                              "SiteIdentifier,SiteName,Game,Date,sum(PlayedAmount) PlayedAmount,sum(WonAmount) WonAmount," +
                                              "sum(PlayedCount) PlayedCount,sum(WonCount) WonCount,sum(NumTerminals) NumTerminals," +
                                              "sum(Jackpot) Jackpot,JackpotContributionPct", "", "Date");

        _sheet = _book.Worksheets[0];
        _sheet.Select();
        _sheet.Name = "TOTAL";

        _sheet.WindowInfo.DisplayGridlines = false;

        FillSheetGameStatistics(_sheet, FromDate, ToDate, _total_sales.Select(), Params);
        FillSheetGameTotalVLT(_sheet, FromDate, ToDate, _dt_connected.Select(), Params);

        _idx_sheet = 1; // _idx_sheet = 2; (En SpreadSheetGear es uno menos) (antez 2)

        foreach (DataRow _game in _dt_only_game_names.Rows)
        {
          String _game_name;

          _game_name = (String)_game[0];
          _game_name = _game_name.Replace("WSI - ", "").Replace("[", "").Replace("]", "").Trim();

          _sheet = _book.Worksheets[_idx_sheet];
          _sheet.Select();
          _sheet.Name = _game_name;

          _sheet.WindowInfo.DisplayGridlines = false;

          FillSheetGameStatistics(_sheet, FromDate, ToDate, _dt_games.Select("Game = '" + Convert.ToString(_game[0]).Replace("'", "''") + "'"), Params);

          _idx_sheet++;
        }

        _sheet = _book.Worksheets[0];
        _sheet.Select();

        // Save excel file.
        _file = File.Create(Filename);

        _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
        _file.Close();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_book.Worksheets != null)
        {
          for (_idx_sheet = 1; _idx_sheet <= _book.Worksheets.Count - 1; _idx_sheet++)
          {
            _sheet = _book.Worksheets[_idx_sheet];
          }
        }

        if (_book != null)
        {
          try
          {
            _book.Close();
          }
          catch { }
        }
      }
    } // GameStatisticsToExcel

    /// <summary>
    /// Main function to get excel to be sent by email
    /// </summary>
    /// <param name="DataSet">Get the data to print in excel</param>
    /// <param name="FromDate">Start working day date</param>
    /// <param name="Filename">Name of the excel</param>
    /// <returns></returns>
    public static Boolean HourlyScoreReportToExcel(DataSet DataSet,
                                                   DateTime FromDate,
                                                   ref String Filename,
                                                   Dictionary<String, String> Params)
    {
      IWorkbook _book;
      IWorksheet _sheet;
      IRange _range;
      IFont _font;
      Int32 _idx_sheet;
      DataTable _dt_hourly_score_report;
      FileStream _file;
      Int32 _num_total_columns;
      Int32 _row_offset;

      _num_total_columns = SCORE_REPORT_GAMBLING_TABLE_GROUP_COLUMNS + SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS + (SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS * SCORE_REPORT_DYNAMIC_NUM_GROUPS) + (SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS * 2) - 1;
      _book = null;
      _sheet = null;
      _row_offset = SCORE_REPORT_SOURCE_DATA_POS_ROW;

      try
      {
        _dt_hourly_score_report = DataSet.Tables[Resource.String("STR_MAILING_HOURLY_SCORE_REPORT")];
        SetFileExtensionAndMaxRows(null, ref Filename);

        // Initilize excel spreadsheetgear
        _book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        // Get and print in Excel: site id, generate name, generate date
        AddParamsToSheet(_sheet, Params, SCORE_REPORT_SOURCE_DATA_POS_COLUMN, ref _row_offset);
        _range = _sheet.Cells[SCORE_REPORT_SOURCE_DATA_POS_ROW, SCORE_REPORT_SOURCE_DATA_POS_COLUMN,
                              SCORE_REPORT_SOURCE_DATA_POS_ROW + 3, SCORE_REPORT_SOURCE_DATA_POS_COLUMN + 1];
        _range.Borders.LineStyle = LineStyle.Continuous;

        // If no hourly score report data, send empty Excel file.
        if (_dt_hourly_score_report.Rows.Count == 0)
        {
          if (_book.Worksheets.Count == 0)
          {
            _book.Worksheets.Add();
          }
          while (_book.Worksheets.Count > 1)
          {
            _sheet = _book.Worksheets[_book.Worksheets.Count];
            _sheet.Delete();
          }

          // Create title SCORE REPORT in Excel
          _range = _sheet.Cells[SCORE_REPORT_TITLE_POS_ROW, SCORE_REPORT_TITLE_POS_COLUMN];
          _font = _range.Font;
          _range.Value = Resource.String("STR_MAILING_HOURLY_SCORE_REPORT");
          _font.Size = 24;
          _font.Name = "Calibri";
          _font.Bold = true;

          // Create WORKING DAY data in Excel
          _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN].Value = Resource.String("STR_GAMING_TABLE_REPORT_WORKING_DAY") + ":";
          _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN].ColumnWidth = 25;
          _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN + 1].NumberFormat = "@"; // Cell type to string
          _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN + 1].Value = FromDate.ToString();
          _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN + 1].HorizontalAlignment = HAlign.Left;
          _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN + 1].ColumnWidth = 30;
          _range = _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN,
                                SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN + 1];
          _range.Borders.LineStyle = LineStyle.Continuous;

          // Apply string type of range cells and set sheet name
          _sheet.Name = Resource.String("STR_MAILING_HOURLY_SCORE_REPORT");

          _range = _sheet.Cells[SCORE_REPORT_INITIAL_POS_ROW, SCORE_REPORT_INITIAL_POS_COLUMN];
          _font = _range.Font;

          _range.Value = Resource.String("STR_MAILING_NO_DATA");
          _font.Size = 16;
          _font.Name = "Calibri";
          _font.Bold = true;

          _file = File.Create(Filename);

          _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
          _file.Close();

          return true;
        }

        // Close unused sheets.
        while (_book.Worksheets.Count > 1)
        {
          _sheet = _book.Worksheets[_book.Worksheets.Count];
          _sheet.Delete();
        }

        // Create title SCORE REPORT in Excel
        _range = _sheet.Cells[SCORE_REPORT_TITLE_POS_ROW, SCORE_REPORT_TITLE_POS_COLUMN];
        _font = _range.Font;
        _range.Value = Resource.String("STR_MAILING_HOURLY_SCORE_REPORT");
        _font.Size = 24;
        _font.Name = "Calibri";
        _font.Bold = true;

        // Create WORKING DAY data in Excel
        _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN].Value = Resource.String("STR_GAMING_TABLE_REPORT_WORKING_DAY") + ":";
        _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN + 1].NumberFormat = "@"; // Cell type to string
        _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN + 1].Value = FromDate.ToString();
        _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN + 1].HorizontalAlignment = HAlign.Left;
        _range = _sheet.Cells[SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN,
                              SCORE_REPORT_WORKING_DAY_POS_ROW, SCORE_REPORT_WORKING_DAY_POS_COLUMN + 1];
        _range.Borders.LineStyle = LineStyle.Continuous;

        // Create headers columns and fill data in a table
        CreateScoreReportHeaders(_dt_hourly_score_report, _range, ref _sheet);
        FillScoreReportCells(_dt_hourly_score_report, _num_total_columns, _range, ref _sheet, FromDate);

        _sheet = _book.Worksheets[0];
        _sheet.Select();

        // Apply string type of range cells and set sheet name
        _sheet.Name = Resource.String("STR_MAILING_HOURLY_SCORE_REPORT");

        // Save excel file
        _file = File.Create(Filename);

        _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
        _file.Close();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_book.Worksheets != null)
        {
          for (_idx_sheet = 1; _idx_sheet <= _book.Worksheets.Count - 1; _idx_sheet++)
          {
            _sheet = _book.Worksheets[_idx_sheet];
          }
        }

        if (_book != null)
        {
          try
          {
            _book.Close();
          }
          catch { }
        }
      }
    } // HourlyScoreReportToExcel

    /// <summary>
    /// Function that inserts all the headers in the document. There are fixed headers and other dynamics ranging from the start working day to 24 hours ahead
    /// </summary>
    /// <param name="HourlyScoreReport">Get the data of hourly score report</param>
    /// <param name="Range">Range to apply various styles in this function</param>
    /// <param name="Sheet">Excel sheet to fill in the header data</param>
    private static void CreateScoreReportHeaders(DataTable HourlyScoreReport, IRange Range, ref IWorksheet Sheet)
    {
      List<String> _fixed_name_columns = new List<String> { Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_TYPE"),
                                                                Resource.String("STR_SESSION_SESSION"), 
                                                                Resource.String("STR_GAMING_TABLE_REPORT_DROP"), 
                                                                Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_WINLOSS"), 
                                                                Resource.String("STR_FLASH_REPORT_HOLD"),
                                                                Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION") + " %"
                                                              };
      String[] _score_hour_column; // Get dinamic hour of score report. POS0: Hour. POS1: column name (GT_DROP, GT_HOLD...)               
      Int32 _split_hour; // Index of split function
      Int32 _split_column_name; // Index of split function
      Int32 _score_hour; // Dinamic hour integer type
      Boolean _new_group; // Generate a new dinamic group?
      Int32 _idx_column;
      Int32 _idx_row;

      //Initial/default values
      _split_hour = 0;
      _split_column_name = 1;
      _new_group = true;
      _idx_column = SCORE_REPORT_INITIAL_POS_COLUMN;
      _idx_row = SCORE_REPORT_INITIAL_POS_ROW;

      //Header1. Group1: Gambling Table. This columns are merge cells.
      Range = Sheet.Cells[_idx_row, _idx_column,
                          _idx_row, _idx_column + (SCORE_REPORT_GAMBLING_TABLE_GROUP_COLUMNS - 1)];
      Range.MergeCells = true;
      Range.HorizontalAlignment = HAlign.Center;
      Range.Value = Resource.String("STR_UC_GAMING_TABLE_FILTER_TABLE_COLUMN_NAME");

      //Header1. Group2: Initial. This columns are merge cells.
      Range = Sheet.Cells[_idx_row, _idx_column + SCORE_REPORT_GAMBLING_TABLE_GROUP_COLUMNS,
                          _idx_row, _idx_column + (_fixed_name_columns.Count - 1)];
      Range.MergeCells = true;
      Range.HorizontalAlignment = HAlign.Center;
      Range.Value = Resource.String("STR_UC_BANK_INITIAL").ToUpper();

      //Header2. Columns defined in _fixed_name_columns list.
      for (Int32 _idx = 0; _idx < _fixed_name_columns.Count; _idx++)
      {
        Sheet.Cells[_idx_row + 1, _idx_column].Value = _fixed_name_columns[_idx];
        _idx_column += 1;
      }

      //Create a dinamic hours group of score report
      foreach (DataColumn _current_column in HourlyScoreReport.Columns)
      {
        _score_hour_column = _current_column.ColumnName.Split('-');
        if (Int32.TryParse(_score_hour_column[_split_hour], out _score_hour))
        {
          //If _new_group variable is TRUE then create a new group header. Header1: ... 05:00h or 06:00h or 07:00h ...
          if (_new_group)
          {
            Range = Sheet.Cells[_idx_row, _idx_column,
                                _idx_row, _idx_column + (SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS - 1)];
            Range.MergeCells = true;
            Range.HorizontalAlignment = HAlign.Center;

            //If _score_hour variable is 9999 change header label by FINAL name column
            if (_score_hour_column[_split_hour] == "9999")
            {
              Range.Value = Resource.String("STR_UC_BANK_FINAL").ToUpper();
            }
            else
            {
              Range.Value = _score_hour_column[_split_hour].Substring(0, 2) + ":" + _score_hour_column[_split_hour].Substring(2, 2) + "h";
            }

            _new_group = false;
          }

          //Header 2. Columns: Drop, W/L, Hold %, Occupation %
          switch (_score_hour_column[_split_column_name].ToUpper())
          {
            case "GT_DROP":
              Sheet.Cells[_idx_row + 1, _idx_column].Value += Resource.String("STR_GAMING_TABLE_REPORT_DROP");
              _new_group = false;
              break;
            case "GT_WIN_LOSS":
              Sheet.Cells[_idx_row + 1, _idx_column].Value += Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_WINLOSS");
              _new_group = false;
              break;
            case "GT_HOLD":
              Sheet.Cells[_idx_row + 1, _idx_column].Value += Resource.String("STR_FLASH_REPORT_HOLD");
              _new_group = false;
              break;
            case "GT_OCCUPATION_NUMBER":
              Sheet.Cells[_idx_row + 1, _idx_column].Value += Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION") + " %";
              _new_group = true;
              break;
            default:
              break;
          }

          _idx_column += 1;
        }
      }

      if (HourlyScoreReport.Columns.Count > 0)
      {
        //Create TOTAL header group (HEADER 1 and HEADER 2)
        Range = Sheet.Cells[_idx_row, _idx_column,
                            _idx_row, _idx_column + (SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS - 1)];
        Range.MergeCells = true;
        Range.HorizontalAlignment = HAlign.Center;
        Range.Value = Resource.String("STR_FRM_AMOUNT_TOTAL").ToUpper();

        Sheet.Cells[_idx_row + 1, _idx_column].Value += Resource.String("STR_GAMING_TABLE_REPORT_DROP");
        Sheet.Cells[_idx_row + 1, _idx_column + 1].Value += Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_WINLOSS");
        Sheet.Cells[_idx_row + 1, _idx_column + 2].Value += Resource.String("STR_FLASH_REPORT_HOLD");
        Sheet.Cells[_idx_row + 1, _idx_column + 3].Value += Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION") + " %";
      }
    } // CreateScoreReportHeaders

    /// <summary>
    /// Fill hourly score into the excel cells
    /// </summary>
    /// <param name="HourlyScoreReport">Get the data of hourly score report</param>
    /// <param name="TotalNumColumnsExcel">Number of columns of principal table in excel document</param>
    /// <param name="Range">Range to apply various styles in this function</param>
    /// <param name="Sheet">Excel sheet to fill in the header data</param>
    private static void FillScoreReportCells(DataTable HourlyScoreReport, Int32 TotalNumColumnsExcel, IRange Range, ref IWorksheet Sheet, DateTime FromDate)
    {
      Int32 _idx_row;
      Int32 _idx_column;
      String _current_type; // It allows us to know if a new subtotal
      String _subtotal_title; // Subtitle that corresponds to the type concatenated with the name of the session
      String _first_hour_row_to_string;
      Int32 _first_hour_row;

      // Dictionary of subtotals sessions and other dictionary of totals of total sessions
      Dictionary<String, Currency> _subtotals;
      Dictionary<String, Currency> _totals;

      // Number of seats in table. One variable correspons to total session an other variable correspons to total of total sessions
      Decimal _num_seats_in_table_session;
      Decimal _num_seats_in_table_total;

      // Calculate values to insert in a different cells
      Currency _drop;
      Currency _drop_total_session;
      Currency _win_loss;
      Currency _win_loss_total_session;
      Decimal _hold;
      Decimal _occupation;
      DateTime _gambling_table_opening_date;
      DateTime _gambling_table_closing_date;

      DateTime _first_time;
      DateTime _last_time;

      // Calculate occupation, subtotal occupation and totals occupations
      Decimal _occupation_session; // Subtotals of occupation (TOTAL group)
      Decimal _occupation_total_session; // Totals of occupation (TOTAL group)
      Decimal _occupation_final_column; // Subtotals of occupation (FINAL group)
      Decimal _occupation_total_final_column; // Totals of occupation (FINAL group)
      Decimal _num_hours_reported; // Hours reported per session
      Decimal _num_seats_hours_reported; // Calculation of occupancy * hours reported per session
      Decimal _num_sessions_by_types; // Number of sessions with the same type (for the calculation of SUBTOTAL)
      Decimal _last_occupation; // Get final occupation of gambling table session

      String _symbol;

      //Initial/default values
      _subtotals = new Dictionary<String, Currency>();
      _totals = new Dictionary<String, Currency>();
      _subtotal_title = String.Empty;
      _current_type = String.Empty;
      _idx_row = SCORE_REPORT_INITIAL_POS_ROW + SCORE_REPORT_NUM_HEADERS;
      _num_seats_in_table_session = 0;
      _num_seats_in_table_total = 0;
      _drop = 0;
      _drop_total_session = 0;
      _win_loss = 0;
      _hold = 0;
      _occupation = 0;
      _occupation_total_session = 0;
      _num_hours_reported = 0;
      _num_seats_hours_reported = 0;
      _occupation_session = 0;
      _num_sessions_by_types = 0;
      _occupation_final_column = 0;
      _occupation_total_final_column = 0;
      _last_occupation = 0;

      _first_time = FromDate;
      _last_time = new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day, WGDB.Now.Hour, WGDB.Now.Minute, WGDB.Now.Second);
      
      _symbol = WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol");

      foreach (DataRow _current_row in HourlyScoreReport.Rows)
      {
        _idx_column = SCORE_REPORT_INITIAL_POS_COLUMN;
        _win_loss_total_session = 0;

        if (_current_row["CLOSING_DATE"] == DBNull.Value)
        {
          _gambling_table_closing_date = new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day, WGDB.Now.Hour, WGDB.Now.Minute, WGDB.Now.Second);
          if (_gambling_table_closing_date > FromDate.AddDays(1))
          {
            _gambling_table_closing_date = FromDate.AddDays(1);
          }
        }
        else
        {
          _gambling_table_closing_date = (DateTime)_current_row["CLOSING_DATE"];
        }

        if (_current_row["OPENING_DATE"] == DBNull.Value)
        {
          _gambling_table_opening_date = FromDate;

        }
        else
        {
          _gambling_table_opening_date = (DateTime)_current_row["OPENING_DATE"];
        }

        _first_hour_row = Int32.Parse(HourlyScoreReport.Columns[SCORE_REPORT_SQL_COLUMN_FIRST_HOUR].ColumnName.Substring(0, 4));
        _first_hour_row_to_string = _first_hour_row.ToString().PadLeft(4, '0');

        // If _current_type is diferent to current row then create subtotal row.
        if (_current_type != String.Empty && _current_type != _current_row["TYPE_NAME"].ToString())
        {
          _subtotal_title = String.Format("{0} {1} {2}", Resource.String("STR_FRM_AMOUNT_TOTAL").ToUpper(),
                                                         Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_TYPE"),
                                                         _current_type);

          _subtotals["9999-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_SUBTOTAL_LABEL] = _occupation_final_column / _num_sessions_by_types;          
          _subtotals["1234-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_TOTAL_GROUP_LABEL] = _occupation_total_session / _num_sessions_by_types;
          CreateSubtotalOrTotalRow(ref Sheet, ref Range, ref _idx_row, TotalNumColumnsExcel, _subtotal_title, false, _subtotals, _num_seats_in_table_session, _num_sessions_by_types, FromDate, _first_time, _last_time);          
          _occupation_total_final_column += _occupation_final_column / _num_sessions_by_types;

          _num_sessions_by_types = 0;
          _occupation_final_column = 0;
          _occupation_total_session = 0;
          _last_occupation = 0;
        }
        _current_type = String.Format("{0}", _current_row["TYPE_NAME"]);

        Sheet.Cells[_idx_row, _idx_column].Value = String.Format("{0}", _current_row["TYPE_NAME"]);
        Sheet.Cells[_idx_row, _idx_column + 1].Value = String.Format("{0} - {1}", _current_row["TABLE_NAME"], _current_row["SESION_NAME"]);

        Sheet.Cells[_idx_row, _idx_column + 2].NumberFormat = "@"; // Cell type to string
        Sheet.Cells[_idx_row, _idx_column + 2].HorizontalAlignment = HAlign.Right;
        _drop = Currency.Zero();
        Sheet.Cells[_idx_row, _idx_column + 2].Value = String.Format("{0}", _drop);
        AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_DROP-" + SCORE_REPORT_INITIAL_GROUP_LABEL, _drop);

        Sheet.Cells[_idx_row, _idx_column + 3].NumberFormat = "@"; // Cell type to string
        Sheet.Cells[_idx_row, _idx_column + 3].HorizontalAlignment = HAlign.Right;
        _win_loss = Format.ParseCurrency(_current_row["INITIAL_BALANCE"].ToString());
        Sheet.Cells[_idx_row, _idx_column + 3].Value = String.Format("{0}", _win_loss);
        AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_WIN_LOSS-" + SCORE_REPORT_INITIAL_GROUP_LABEL, _win_loss);

        Sheet.Cells[_idx_row, _idx_column + 4].NumberFormat = "@"; // Cell type to string
        _hold = 0;
        Sheet.Cells[_idx_row, _idx_column + 4].Value = Currency.Format(_hold, String.Empty) + "%";
        Sheet.Cells[_idx_row, _idx_column + 4].HorizontalAlignment = HAlign.Right;
        AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_HOLD-" + SCORE_REPORT_INITIAL_GROUP_LABEL, _hold);

        Sheet.Cells[_idx_row, _idx_column + 5].NumberFormat = "@"; // Cell type to string
        _occupation = 0;
        Sheet.Cells[_idx_row, _idx_column + 5].Value = Currency.Format(_occupation, String.Empty) + "%";
        Sheet.Cells[_idx_row, _idx_column + 5].HorizontalAlignment = HAlign.Right;
        AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_INITIAL_GROUP_LABEL, _occupation);

        // Place the column index at the beginning of the dynamic groups of hours
        _idx_column += SCORE_REPORT_GAMBLING_TABLE_GROUP_COLUMNS + SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS;

        // Add 1 to reach the FINAL column
        for (int _idx = 0; _idx < SCORE_REPORT_DYNAMIC_NUM_GROUPS + 1; _idx++)
        {
          // If we reach the limit of the dynamic groups means that the next group is the FINAL (9999)
          if (_idx == SCORE_REPORT_DYNAMIC_NUM_GROUPS)
          {
            _first_hour_row_to_string = "9999";
            _drop = 0;
          }
          else
          {
            _drop = Format.ParseCurrency(_current_row[_first_hour_row_to_string + "-GT_DROP"].ToString());
            _drop_total_session += Format.ParseCurrency(_current_row[_first_hour_row_to_string + "-GT_DROP"].ToString());
            _win_loss_total_session += Format.ParseCurrency(_current_row[_first_hour_row_to_string + "-GT_WIN_LOSS"].ToString());
          }

          if (_first_hour_row_to_string == "9999" && _gambling_table_closing_date == DateTime.MinValue)
          {
            _drop = 0;
            _win_loss = 0;
          }

          Sheet.Cells[_idx_row, _idx_column].NumberFormat = "@"; // Cell type to string
          Sheet.Cells[_idx_row, _idx_column].HorizontalAlignment = HAlign.Right;
          Sheet.Cells[_idx_row, _idx_column].Value = String.Format("{0}", _drop);
          AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_DROP-" + SCORE_REPORT_SUBTOTAL_LABEL, _drop);

          Sheet.Cells[_idx_row, _idx_column + 1].NumberFormat = "@"; // Cell type to string
          Sheet.Cells[_idx_row, _idx_column + 1].HorizontalAlignment = HAlign.Right;
          _win_loss = Format.ParseCurrency(_current_row[_first_hour_row_to_string + "-GT_WIN_LOSS"].ToString());
          Sheet.Cells[_idx_row, _idx_column + 1].Value = String.Format("{0}", _win_loss);
          AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_WIN_LOSS-" + SCORE_REPORT_SUBTOTAL_LABEL, _win_loss);

          Sheet.Cells[_idx_row, _idx_column + 2].NumberFormat = "@"; // Cell type to string
          Sheet.Cells[_idx_row, _idx_column + 2].HorizontalAlignment = HAlign.Right;
          _hold = Decimal.Parse(_current_row[_first_hour_row_to_string + "-GT_HOLD"].ToString());
          Sheet.Cells[_idx_row, _idx_column + 2].Value = Currency.Format(_hold, String.Empty) + "%";
          AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_HOLD-" + SCORE_REPORT_SUBTOTAL_LABEL, 0); // Value is calculate in CreateSubtotalOrTotalRow function

          Sheet.Cells[_idx_row, _idx_column + 3].NumberFormat = "@"; // Cell type to string
          Sheet.Cells[_idx_row, _idx_column + 3].HorizontalAlignment = HAlign.Right;

          if (_first_hour_row_to_string == "9999")
          {
            _occupation = 0;
            _last_occupation = 0;
          }
          else
          {
            _occupation = (Decimal.Parse(_current_row[_first_hour_row_to_string + "-GT_OCCUPATION_NUMBER"].ToString()) /
                           Decimal.Parse(_current_row["NUM_SEATS_IN_TABLE"].ToString()));

            if (_first_hour_row_to_string != "9999" && _occupation > 0)
            {
              _num_hours_reported += 1;
              _occupation_session += Decimal.Parse(_current_row[_first_hour_row_to_string + "-GT_OCCUPATION_NUMBER"].ToString());
              if(_first_hour_row_to_string != "9999")
              {
                _last_occupation = Decimal.Parse(_current_row[_first_hour_row_to_string + "-GT_OCCUPATION_NUMBER"].ToString());
              }
              
            }
          }

          if(_first_hour_row_to_string == "9999")
          {                        
            if (_gambling_table_closing_date == DateTime.MinValue)
            {
              _occupation = 0;
            }
            else
            {
              _occupation = _last_occupation / Decimal.Parse(_current_row["NUM_SEATS_IN_TABLE"].ToString());
              _occupation_final_column += _last_occupation / Decimal.Parse(_current_row["NUM_SEATS_IN_TABLE"].ToString());
            }

            _last_occupation = 0;
          }

          Sheet.Cells[_idx_row, _idx_column + 3].Value = Currency.Format(_occupation * 100, String.Empty) + "%";
          AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_SUBTOTAL_LABEL, _occupation);
          
          // If it is different than time 9999 (FINAL column) we will add 4 to the index of the columns for the next group of hours
          if (_first_hour_row_to_string != "9999")
          {
            _idx_column += SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS;
          }

          // Change from 23:00h to 00:00h
          if (_first_hour_row == 2300)
          {
            _first_hour_row = 0000;
          }
          else
          {
            if (_first_hour_row_to_string != "9999")
            {
              // Increment 01:00h
              _first_hour_row += 100;
            }
          }

          // Formatted always to ####
          _first_hour_row_to_string = _first_hour_row.ToString().PadLeft(4, '0');
        }

        _idx_column += SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS; // We are in the column position of the total column
        _first_hour_row_to_string = "1234"; // Differentiate the rest of columns with a numbering different from the rest

        Sheet.Cells[_idx_row, _idx_column].NumberFormat = "@"; // Cell type to string
        Sheet.Cells[_idx_row, _idx_column].HorizontalAlignment = HAlign.Right;
        Sheet.Cells[_idx_row, _idx_column].Value = String.Format("{0}", _drop_total_session);
        AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_DROP-" + SCORE_REPORT_TOTAL_GROUP_LABEL, _drop_total_session);

        Sheet.Cells[_idx_row, _idx_column + 1].NumberFormat = "@"; // Cell type to string
        Sheet.Cells[_idx_row, _idx_column + 1].HorizontalAlignment = HAlign.Right;
        Sheet.Cells[_idx_row, _idx_column + 1].Value = String.Format("{0}", _win_loss_total_session);
        AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_WIN_LOSS-" + SCORE_REPORT_TOTAL_GROUP_LABEL, _win_loss_total_session);

        Sheet.Cells[_idx_row, _idx_column + 2].NumberFormat = "@"; // Cell type to string
        Sheet.Cells[_idx_row, _idx_column + 2].HorizontalAlignment = HAlign.Right;
        _hold = (_win_loss_total_session > 0 ? (_drop_total_session / _win_loss_total_session) * 100 : 0) - 0; // Initial hold is 0
        Sheet.Cells[_idx_row, _idx_column + 2].Value = Currency.Format(_hold, String.Empty) + "%";
        AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_HOLD-" + SCORE_REPORT_TOTAL_GROUP_LABEL, _hold);

        Sheet.Cells[_idx_row, _idx_column + 3].NumberFormat = "@"; // Cell type to string
        Sheet.Cells[_idx_row, _idx_column + 3].HorizontalAlignment = HAlign.Right;

        _num_seats_hours_reported += Decimal.Parse(_current_row["NUM_SEATS_IN_TABLE"].ToString()) * _num_hours_reported;
        
        Decimal _accumulate_num_seats_hours_reported;
        _accumulate_num_seats_hours_reported = Decimal.Parse(_current_row["NUM_SEATS_IN_TABLE"].ToString()) * _num_hours_reported;

        _occupation = _accumulate_num_seats_hours_reported > 0 ? (_occupation_session / _accumulate_num_seats_hours_reported) : 0;

        Sheet.Cells[_idx_row, _idx_column + 3].Value = Currency.Format(_occupation * 100, String.Empty) + "%";
        AccumulateSubtotalOrTotal(ref _subtotals, ref _totals, _first_hour_row_to_string + "-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_TOTAL_GROUP_LABEL, _occupation);

        _idx_row += 1;
        _drop_total_session = 0;

        _num_hours_reported = 0;
        _num_seats_in_table_session = 0;
        _occupation_session = 0;
        _occupation_total_session += _occupation;
        _num_sessions_by_types += 1;
      }

      //Create last subtotal row and total row.
      _subtotal_title = String.Format("{0} {1} {2}", Resource.String("STR_FRM_AMOUNT_TOTAL").ToUpper(),
                                                     Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_TYPE"),
                                                     _current_type);

      _subtotals["9999-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_SUBTOTAL_LABEL] = _occupation_final_column / _num_sessions_by_types;
      _subtotals["1234-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_TOTAL_GROUP_LABEL] = _occupation_total_session / _num_sessions_by_types;
      CreateSubtotalOrTotalRow(ref Sheet, ref Range, ref _idx_row, TotalNumColumnsExcel, _subtotal_title, false, _subtotals, _num_seats_in_table_session, _num_sessions_by_types, FromDate, _first_time, _last_time);          
     
      _totals["9999-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_SUBTOTAL_LABEL] = _totals["9999-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_SUBTOTAL_LABEL] / HourlyScoreReport.Rows.Count;
      _totals["1234-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_TOTAL_GROUP_LABEL] = _totals["1234-GT_OCCUPATION_NUMBER-" + SCORE_REPORT_TOTAL_GROUP_LABEL] / HourlyScoreReport.Rows.Count;
      CreateSubtotalOrTotalRow(ref Sheet, ref Range, ref _idx_row, TotalNumColumnsExcel, Resource.String("STR_FRM_AMOUNT_TOTAL").ToUpper(), true, _totals, _num_seats_in_table_total, HourlyScoreReport.Rows.Count, FromDate, _first_time, _last_time);          

      //Autofit all columns
      Range = Sheet.UsedRange; 

      foreach (IRange _current_range in Range.Columns)
      {
        if (_current_range.Width != 0)
        {
          _current_range.Columns.AutoFit();
          _current_range.Columns.ColumnWidth += 5;
        }
      }

      //Apply style
      StyleScoreReportExcel(ref Sheet, Range, TotalNumColumnsExcel, (_idx_row - SCORE_REPORT_INITIAL_POS_ROW - 1));
    } // FillScoreReportCells

    /// <summary>
    /// Function to accumulate values of subtotal and total dictionary
    /// </summary>
    /// <param name="SubTotals">Subtotal dictionary where you save the accumulated data</param>
    /// <param name="Totals">Total dictionary where you save the accumulated data</param>
    /// <param name="Key">Key of position dictionary</param>
    /// <param name="ValueToAccumulate">Value to accumulate or insert in a dictionary</param>
    private static void AccumulateSubtotalOrTotal(ref Dictionary<String, Currency> SubTotals, ref Dictionary<String, Currency> Totals, String Key, Decimal ValueToAccumulate)
    {
      if (SubTotals.ContainsKey(Key))
      {
        SubTotals[Key] += ValueToAccumulate;
        Totals[Key] += ValueToAccumulate;
      }
      else
      {
        SubTotals.Add(Key, ValueToAccumulate);
        if (!Totals.ContainsKey(Key))
        {
          Totals.Add(Key, ValueToAccumulate);
        }
        else
        {
          Totals[Key] += ValueToAccumulate;
        }
      }
    } // AccumulateSubtotalOrTotal

    /// <summary>
    /// Apply specific style to hourly score report
    /// </summary>
    /// <param name="Sheet">Excel sheet to fill in the header data</param>
    /// <param name="Range">Range to apply various styles in this function</param>
    /// <param name="TotalNumColumnsExcel">Number of total column to print a excel</param>
    /// <param name="TotalNumRowsExcel">Number of total rows to print a excel</param>
    private static void StyleScoreReportExcel(ref IWorksheet Sheet, IRange Range, Int32 TotalNumColumnsExcel, Int32 TotalNumRowsExcel)
    {
      // Apply bold font and gray backcolor
      Range = Sheet.Cells[SCORE_REPORT_INITIAL_POS_ROW, SCORE_REPORT_INITIAL_POS_COLUMN,
                          SCORE_REPORT_INITIAL_POS_ROW + SCORE_REPORT_NUM_HEADERS - 1, SCORE_REPORT_INITIAL_POS_COLUMN + TotalNumColumnsExcel];
      Range.Rows.Interior.Color = Colors.Gray;
      Range.Font.Bold = true;

      // Apply black border in a table
      Range = Sheet.Cells[SCORE_REPORT_INITIAL_POS_ROW, SCORE_REPORT_INITIAL_POS_COLUMN,
                          SCORE_REPORT_INITIAL_POS_ROW + TotalNumRowsExcel, SCORE_REPORT_INITIAL_POS_COLUMN + TotalNumColumnsExcel];
      Range.Cells.Borders.LineStyle = LineStyle.Continuous;
    }

    /// <summary>
    /// Printed in the document excel the subtotal or total row
    /// </summary>
    /// <param name="Sheet">Excel sheet to fill in the header data</param>
    /// <param name="Range">Range to apply various styles in this function</param>
    /// <param name="CurrentRow">Row where you insert the subtotal or total row</param>
    /// <param name="NumTotalColumns">Number of total column to print a excel</param>
    /// <param name="TitleRow">Title to insert of subtotal or total</param>
    /// <param name="IsTotal">Define if is total or subtotal row</param>
    /// <param name="Totals">Datatable where we find the data to be printed in the total or subtotal</param>
    /// <param name="NumSeats">Number of seats of the session or total of the session to calculate the occupied column</param>
    /// <param name="TotalRows">Number of rows for calculate the subtotals and totals rows</param>
    private static void CreateSubtotalOrTotalRow(ref IWorksheet Sheet, ref IRange Range, ref Int32 CurrentRow, Int32 NumTotalColumns, String TitleRow, Boolean IsTotal, Dictionary<String, Currency> Totals, Decimal NumSeats, Decimal TotalRows, DateTime DateFrom, DateTime FirstTime, DateTime LastTime)
    {
      String[] _score_report_columns;
      Int32 _split_column_name;
      Int32 _idx_column;

      // Calculate values to insert in a different cells
      Currency _win_loss;
      Currency _drop;
      Decimal _hold;
      Decimal _occupation;

      String _symbol;

      DateTime _date_time_from;

      _date_time_from = DateFrom;

      // Initial/default values
      _split_column_name = 1;
      _score_report_columns = null;
      _idx_column = 0;
      _win_loss = Currency.Zero();
      _drop = Currency.Zero();
      _hold = Decimal.MinValue;
      _occupation = Decimal.MinValue;
      _symbol = WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol");

      // Insert a title row of subtotal or total
      Range = Sheet.Cells[CurrentRow, SCORE_REPORT_INITIAL_POS_COLUMN,
                          CurrentRow, SCORE_REPORT_INITIAL_POS_COLUMN + NumTotalColumns];
      Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN].Value = String.Format("{0}", TitleRow);

      foreach (KeyValuePair<String, Currency> _total_item in Totals)
      {
        _score_report_columns = _total_item.Key.Split('-');
        _idx_column += 1;

        // Cell type to string
        Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column].NumberFormat = "@";

        switch (_score_report_columns[_split_column_name])
        {
          case "GT_DROP":
            _drop = _total_item.Value;
            Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column].HorizontalAlignment = HAlign.Right;
            Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column].Value = String.Format("{0}", _drop);
            break;
          case "GT_WIN_LOSS":
            _win_loss = _total_item.Value;
            Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column].HorizontalAlignment = HAlign.Right;
            Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column].Value = String.Format("{0}", _win_loss);
            break;
          case "GT_HOLD":
            _hold = 0;
            if (_win_loss != 0)
            {
              _hold = _drop > 0 ? ((_drop / _win_loss) * 100) : 0;
            }
            Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column].HorizontalAlignment = HAlign.Right;
            Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column].Value = Currency.Format(_hold, String.Empty) + "%";
            break;
          case "GT_OCCUPATION_NUMBER":
            if ((_total_item.Key.Contains("1234") || _total_item.Key.Contains("9999")) && (_total_item.Key.Contains(SCORE_REPORT_TOTAL_GROUP_LABEL) || _total_item.Key.Contains(SCORE_REPORT_SUBTOTAL_LABEL)))
            {
              //Final and Total group
              _occupation = _total_item.Value;
            }
            else if (_total_item.Key.Contains(SCORE_REPORT_INITIAL_GROUP_LABEL))
            {
              // Initial group
              _occupation = 0;
            }
            else
            {
              //0 - 24 hours group
              _occupation = _total_item.Value / TotalRows;
            }

            Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column].Value = Currency.Format(_occupation * 100, String.Empty) + "%";
            Sheet.Cells[CurrentRow, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column].HorizontalAlignment = HAlign.Right;
            break;
          default:
            break;
        }

        if (IsTotal && _score_report_columns[_split_column_name] == "GT_OCCUPATION_NUMBER"
           && (SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column) > (SCORE_REPORT_SQL_COLUMN_FIRST_HOUR)
           && (SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN + _idx_column) <= (SCORE_REPORT_SQL_COLUMN_FIRST_HOUR + (SCORE_REPORT_DYNAMIC_NUM_GROUPS * SCORE_REPORT_DYNAMIC_HOUR_GROUP_COLUMNS)))
        {
          IRange RangeColumns;

            
          if (_date_time_from < FirstTime || _date_time_from > LastTime)
          {
            RangeColumns = Range.Columns[1, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN - SCORE_REPORT_INITIAL_POS_COLUMN + _idx_column - 3,
                                         1, SCORE_REPORT_TOTAL_SUBTOTAL_POS_COLUMN - SCORE_REPORT_INITIAL_POS_COLUMN + _idx_column].Columns;
            RangeColumns.Hidden = true;
          }
          _date_time_from = _date_time_from.AddHours(1);
        }

      }

      // Set color of row. Total is YELLOW_00 (from WigosGUI) and subtotal is YELLOW_01 (from WigosGUI)
      if (IsTotal)
      {
        Range.Rows.Interior.Color = SpreadsheetGear.Color.FromArgb(255, 235, 94);
      }
      else
      {
        Range.Rows.Interior.Color = SpreadsheetGear.Color.FromArgb(255, 255, 208);
      }

      CurrentRow += 1;

      // Clean one to one subtotal or total accumulate to the next accumulation
      Totals.Clear();
    } // CreateSubtotalOrTotalRow

    //------------------------------------------------------------------------------
    // PURPOSE : Conversion from Connected Terminals to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet ConnectedTerminalsSet
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - String Filename
    //
    //      - OUTPUT :
    //          - String Filename
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean ConnectedTerminalsToExcel(DataSet ConnectedTerminalsSet,
                                                    DateTime FromDate,
                                                    DateTime ToDate,
                                                    ref String Filename,
                                                    Dictionary<String, String> Params,
                                                    CalledFrom CalledFrom)
    {
      CultureInfo _old_ci;
      Excel.Application _excel;

      IWorkbook _book;
      IWorksheet _sheet;
      IRange _range;
      IFont _font;
      Int32 _num_tables_in_set;
      Int32 _idx_sheet;
      DateTime _month_date;
      Int32 _last_day;
      FileStream _file;

      _old_ci = Thread.CurrentThread.CurrentCulture;

      _excel = null;
      _book = null;
      _sheet = null;

      try
      {
        _num_tables_in_set = ConnectedTerminalsSet.Tables.Count;

        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        // Restore DateTime and Number Format.
        Thread.CurrentThread.CurrentCulture.DateTimeFormat = _old_ci.DateTimeFormat;
        Thread.CurrentThread.CurrentCulture.NumberFormat = _old_ci.NumberFormat;

        _excel = null;

        if (CalledFrom == CalledFrom.WigosGUI)
        {
          try
          {
            _excel = new Excel.ApplicationClass();
            _excel.DisplayAlerts = false;
          }
          catch
          {
            _excel = null;
          }
        }

        SetFileExtensionAndMaxRows(_excel, ref Filename);

        // Initilize excel spreadsheetgear.
        _book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        // If no tables, send empty Excel file.
        if (_num_tables_in_set == 0)
        {
          if (_book.Worksheets.Count == 0)
          {
            _book.Worksheets.Add();
          }
          while (_book.Worksheets.Count > 1)
          {
            _sheet = _book.Worksheets[_book.Worksheets.Count];
            _sheet.Delete();

          }

          _sheet = _book.Worksheets[0];
          _range = _sheet.Cells[3, 2];
          _font = _range.Font;

          _range.Value = "NO HAY DATOS";
          _font.Size = 16;
          _font.Name = "Calibri";
          _font.Bold = true;

          // Save excel file.
          _file = File.Create(Filename);
          switch (ExcelVersion)
          {
            case EXCEL_VERSION.AFTER_2007:
              _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
              break;

            case EXCEL_VERSION.BEFORE_2007:
              _book.SaveToStream(_file, FileFormat.Excel8);
              break;
          }
          _file.Close();

          return true;
        }

        // Add needed sheets.
        while (_num_tables_in_set - _book.Worksheets.Count > 0)
        {
          _book.Worksheets.Add();
        }
        // Or close unused sheets.
        while (_book.Worksheets.Count > _num_tables_in_set)
        {
          _sheet = _book.Worksheets[_book.Worksheets.Count];
          _sheet.Delete();
        }

        CalculateNumberFormats();

        _idx_sheet = 0;
        _month_date = FromDate;


        foreach (DataTable _connected_terms in ConnectedTerminalsSet.Tables)
        {
          //EOR 19-OCT-2016
          if (_connected_terms.TableName.ToUpper().Contains("C_ACTIVITYTABLES") | _connected_terms.TableName == "CONNECTED_TERMINALS_CURRENT_MONTH")
          {
            _month_date = ToDate;
          }
          if (_connected_terms.TableName.ToUpper().Contains("P_ACTIVITYTABLES") | _connected_terms.TableName == "CONNECTED_TERMINALS_PREVIOUS_MONTH")
          {
            _month_date = FromDate;
          }


          _last_day = new DateTime(_month_date.Year, _month_date.Month, 1).AddMonths(1).AddDays(-1).Day;

          _sheet = _book.Worksheets[_idx_sheet];
          _sheet.Select();

          _sheet.WindowInfo.DisplayGridlines = false;

          //EOR 19-MAY-2016
          if (_connected_terms.TableName.ToUpper().Contains("ACTIVITYTABLES"))
          {
            FillSheetActivityTables(_sheet, _month_date, _last_day, _connected_terms, Params);
          }
          else
          {
            FillSheetConnectedTerminals(_sheet, _month_date, _last_day, _connected_terms, Params);
          }

          _idx_sheet++;
        }

        _sheet = _book.Worksheets[0];
        _sheet.Select();

        // Save excel file.
        _file = File.Create(Filename);
        switch (ExcelVersion)
        {
          case EXCEL_VERSION.AFTER_2007:
            _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
            break;

          case EXCEL_VERSION.BEFORE_2007:
            _book.SaveToStream(_file, FileFormat.Excel8);
            break;
        }
        _file.Close();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_book != null)
        {
          try
          {
            _book.Close();
          }
          catch { }
        }

        if (_excel != null)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }
        ReleaseComObject(_excel);

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // ConnectedTerminalsToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Conversion from Terminals Without Activity to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet TerminalsWithoutActivity
    //          - String Filename
    //
    //      - OUTPUT :
    //          
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean TerminalsWithoutActivityToExcel(DataSet TerminalsWithoutActivity,
                                                          ref String Filename,
                                                          Dictionary<String,
                                                          String> Params)
    {
      IWorkbook _book;
      IWorksheet _sheet;
      Int32 _idx_sheet;
      Int32 _num_needed_sheets;
      Int32 _idx_start_row;
      Int32 _num_sheets_per_table;

      _book = null;
      _sheet = null;

      try
      {
        SetFileExtensionAndMaxRows(null, ref Filename);

        // Calculate the needed sheets according to the maximum allowed rows.
        // Must be calculated after calling SetFileExtensionAndMaxRows().
        _num_needed_sheets = 0;
        foreach (DataTable _table in TerminalsWithoutActivity.Tables)
        {
          _num_sheets_per_table = (Int32)Math.Ceiling((Decimal)_table.Rows.Count / m_excel_max_rows);
          _num_needed_sheets += _num_sheets_per_table;
        }

        // Initilize excel spreadsheetgear.
        _book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        // Add needed sheets.
        while (_num_needed_sheets - _book.Worksheets.Count > 0)
        {
          _book.Worksheets.Add();
        }
        // Or close unused sheets.
        while (_book.Worksheets.Count > _num_needed_sheets && _book.Worksheets.Count > 1)
        {
          _sheet = _book.Worksheets[_book.Worksheets.Count];
          _sheet.Delete();
        }

        CalculateNumberFormats();

        // Setting Column Names
        TerminalsWithoutActivity.Tables["TERMINALS_WITHOUT_ACTIVITY"].TableName = Resource.String("STR_MAILING_TYPE_TERMINALS_WITHOUT_ACTIVITY");
        TerminalsWithoutActivity.Tables[Resource.String("STR_MAILING_TYPE_TERMINALS_WITHOUT_ACTIVITY")].Columns["TE_AREA"].ColumnName = Resource.String("STR_MAILING_TERMINALS_WITHOUT_ACTIVITY_COLUMN_01");
        TerminalsWithoutActivity.Tables[Resource.String("STR_MAILING_TYPE_TERMINALS_WITHOUT_ACTIVITY")].Columns["TE_BANK"].ColumnName = Resource.String("STR_MAILING_TERMINALS_WITHOUT_ACTIVITY_COLUMN_02");
        TerminalsWithoutActivity.Tables[Resource.String("STR_MAILING_TYPE_TERMINALS_WITHOUT_ACTIVITY")].Columns["TE_PROVIDER_ID"].ColumnName = Resource.String("STR_MAILING_TERMINALS_WITHOUT_ACTIVITY_COLUMN_03");
        TerminalsWithoutActivity.Tables[Resource.String("STR_MAILING_TYPE_TERMINALS_WITHOUT_ACTIVITY")].Columns["TE_NAME"].ColumnName = Resource.String("STR_MAILING_TERMINALS_WITHOUT_ACTIVITY_COLUMN_04");
        TerminalsWithoutActivity.Tables[Resource.String("STR_MAILING_TYPE_TERMINALS_WITHOUT_ACTIVITY")].Columns["TE_FLOOR_ID"].ColumnName = Resource.String("STR_MAILING_TERMINALS_WITHOUT_ACTIVITY_COLUMN_05");
        TerminalsWithoutActivity.Tables[Resource.String("STR_MAILING_TYPE_TERMINALS_WITHOUT_ACTIVITY")].Columns["TE_TERMINAL_TYPE"].ColumnName = Resource.String("STR_MAILING_TERMINALS_WITHOUT_ACTIVITY_COLUMN_06");
        TerminalsWithoutActivity.Tables[Resource.String("STR_MAILING_TYPE_TERMINALS_WITHOUT_ACTIVITY")].Columns["TE_LAST_ACTIVITY"].ColumnName = Resource.String("STR_MAILING_TERMINALS_WITHOUT_ACTIVITY_COLUMN_07");

        // Formatting columns: Terminal Type and Last Activity
        Int32 _hours_without_activity;
        Int32 _days_without_activity;

        foreach (DataRow _row in TerminalsWithoutActivity.Tables[Resource.String("STR_MAILING_TYPE_TERMINALS_WITHOUT_ACTIVITY")].Rows)
        {
          switch ((TerminalTypes)Convert.ToInt32(_row[5]))
          {
            case TerminalTypes.WIN:
              _row[5] = Resource.String("STR_MAILING_TERMINAL_TYPES_01");
              break;
            case TerminalTypes.T3GS:
              _row[5] = Resource.String("STR_MAILING_TERMINAL_TYPES_03");
              break;
            case TerminalTypes.SAS_HOST:
              _row[5] = Resource.String("STR_MAILING_TERMINAL_TYPES_05");
              break;
            default:
              _row[5] = Resource.String("STR_MAILING_TERMINAL_TYPES_00");
              break;
          }

          _hours_without_activity = Convert.ToInt32(_row[6]);

          if (_hours_without_activity <= 0)
          {
            _hours_without_activity = 168;
          }

          if (_hours_without_activity >= 168)
          {
            _row[6] = Resource.String("STR_MAILING_TERMINAL_LAST_ACTIVITY_WEEK");
          }
          else if (_hours_without_activity > 24)
          {
            _days_without_activity = (int)(_hours_without_activity / 24);
            _hours_without_activity = _hours_without_activity - (int)(_days_without_activity * 24);

            if (_hours_without_activity == 0)
            {
              _row[6] = Resource.String("STR_MAILING_TERMINAL_LAST_ACTIVITY_DAY", _days_without_activity);
            }
            else
            {
              _row[6] = Resource.String("STR_MAILING_TERMINAL_LAST_ACTIVITY_DAYS", _days_without_activity, _hours_without_activity);
            }
          }
          else if (_hours_without_activity <= 24)
          {
            _row[6] = Resource.String("STR_MAILING_TERMINAL_LAST_ACTIVITY_HOURS", _row[6]);
          }
        }

        _idx_sheet = 0;
        foreach (DataTable _table in TerminalsWithoutActivity.Tables)
        {
          _idx_start_row = 0;
          _num_sheets_per_table = (Int32)Math.Ceiling((Decimal)_table.Rows.Count / m_excel_max_rows);
          OnProgressBar(0, _table.Rows.Count);

          for (Int32 _num_sheet = 0; _num_sheet < _num_sheets_per_table; _num_sheet++)
          {
            _sheet = _book.Worksheets[_idx_sheet];

            FillTerminalsWithoutActivity(_sheet, _table, _idx_start_row, Params);

            _idx_sheet++;
            _idx_start_row += m_excel_max_rows;
          }
          OnProgressBar(_table.Rows.Count, _table.Rows.Count);
        }

        if (_num_needed_sheets > 1)
        {
          _sheet = _book.Worksheets[0];
          _sheet.Select();
        }

        FileStream _file = File.Create(Filename);
        switch (ExcelVersion)
        {
          case EXCEL_VERSION.AFTER_2007:
            _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
            break;

          case EXCEL_VERSION.BEFORE_2007:
            _book.SaveToStream(_file, FileFormat.Excel8);
            break;
        }
        _file.Close();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_book != null)
        {
          try
          {
            _book.Close();
          }
          catch { }
        }

      }
    } // TerminalsWithoutActivityToExcel



    //------------------------------------------------------------------------------
    // PURPOSE : Conversion from Machine and Game report to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet dsMachineAndGameReport
    //          - String Filename
    //
    //      - OUTPUT :
    //          
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean MachineAndGameReportToExcel(DataSet dsMachineAndGameReport,
                                                     ref String Filename,
                                                     Dictionary<String, String> Params,
                                                     DateTime DateFrom)
    {
      IWorkbookSet _bookset;
      IWorkbook _book;
      IWorksheet _sheet;
      Int32 _idx_sheet;
      Int32 _num_needed_sheets;
      Int32 _idx_start_row;
      Int32 _num_sheets_per_table;

      _book = null;
      _sheet = null;

      try
      {
        //SetFileExtensionAndMaxRows(null, ref Filename);

        // Calculate the needed sheets according to the maximum allowed rows.
        // Must be calculated after calling SetFileExtensionAndMaxRows().
        _num_needed_sheets = 0;
        foreach (DataTable _table in dsMachineAndGameReport.Tables)
        {
          _num_sheets_per_table = (Int32)Math.Ceiling((Decimal)_table.Rows.Count / m_excel_max_rows);
          _num_needed_sheets += _num_sheets_per_table;
        }

        // JMV 28/04/2015
        _bookset = Factory.GetWorkbookSet();
        _bookset.Experimental = "OleDbOpenXmlWorkaround";

        _book = _bookset.Workbooks.Add();

        // Initilize excel spreadsheetgear.
        //_book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        // Add needed sheets.
        while (_num_needed_sheets - _book.Worksheets.Count > 0)
        {
          _book.Worksheets.Add();
        }
        // Or close unused sheets.
        while (_book.Worksheets.Count > _num_needed_sheets && _book.Worksheets.Count > 1)
        {
          _sheet = _book.Worksheets[_book.Worksheets.Count];
          _sheet.Delete();
        }

        CalculateNumberFormats();

        _idx_sheet = 0;
        foreach (DataTable _table in dsMachineAndGameReport.Tables)
        {
          _idx_start_row = 0;
          _num_sheets_per_table = (Int32)Math.Ceiling((Decimal)_table.Rows.Count / m_excel_max_rows);
          OnProgressBar(0, _table.Rows.Count);

          for (Int32 _num_sheet = 0; _num_sheet < _num_sheets_per_table; _num_sheet++)
          {
            _sheet = _book.Worksheets[_idx_sheet];

            FillMachineAndGameReport(_sheet, _table, _idx_start_row, Params, DateFrom);

            _idx_sheet++;
            _idx_start_row += m_excel_max_rows;
          }
          OnProgressBar(_table.Rows.Count, _table.Rows.Count);
        }

        if (_num_needed_sheets > 1)
        {
          _sheet = _book.Worksheets[0];
          _sheet.Select();
        }

        FileStream _file = File.Create(Filename);
        switch (ExcelVersion)
        {
          case EXCEL_VERSION.AFTER_2007:
            _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
            break;

          case EXCEL_VERSION.BEFORE_2007:
            _book.SaveToStream(_file, FileFormat.Excel8);
            break;
        }
        _file.Close();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_book != null)
        {
          try
          {
            _book.Close();
          }
          catch { }
        }

      }
    } // MachineAndGameReportToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Fill an Excel sheet with machine and game report.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DataTable TerminalsWithoutActivity
    //          - Int32 IndexStartRow
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillMachineAndGameReport(IWorksheet Sheet, DataTable MachinesAndGames, Int32 IndexStartRow, Dictionary<String, String> Params, DateTime DateReport)
    {
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IRange _entire_column;
      IFont _font;
      IInterior _interior;
      Int32 _xls_current_row;
      Int32 _xls_current_col;
      Int32 _xls_cut_row;
      DataRow _row;
      Int32 _index;
      String _current_type;
      String _last_type;

      Sheet.Name = Resource.String("STR_MAILING_MACHINE_AND_GAME_REPORT_NAME");

      // REPORT TITLE
      _xls_current_row = 0;
      _range = Sheet.Cells[0, 0];
      _range.Value = Resource.String("STR_MAILING_MACHINE_AND_GAME_REPORT_TITLE");
      _cell1 = Sheet.Cells[0, 0];
      _cell2 = Sheet.Cells[0, 5];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      MergeRange(ref _range);
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _font = _range.Font;
      _font.Bold = true;
      _font.Size = 16;
      // SUBTITLE
      _xls_current_row++;
      _range = Sheet.Cells[1, 0];
      _range.Value = Resource.String("STR_MAILING_MACHINE_AND_GAME_REPORT_DATE") + ": " + DateReport.ToShortDateString();
      _cell1 = Sheet.Cells[1, 0];
      _cell2 = Sheet.Cells[1, 5];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      MergeRange(ref _range);
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _font = _range.Font;
      _font.Bold = true;
      _font.Size = 12;

      _xls_current_row = 3;

      _xls_current_col = 0;
      // COLUMN HEADERS
      for (_index = 1; _index < MachinesAndGames.Columns.Count; _index++)
      {
        _range = Sheet.Cells[_xls_current_row, _xls_current_col];
        if (_xls_current_col > 1)
        {
          _range.Value = MachinesAndGames.Columns[_index].Caption;
          _range.Font.Bold = true;
          _range.HorizontalAlignment = HAlign.Center;
        }
        else
        {
          _range.Value = "";
        }
        _xls_current_col++;
      }

      _xls_current_row++;
      _xls_cut_row = 0;
      _xls_current_col = 0;
      _last_type = "";
      _current_type = "";
      // ROW LOOP
      for (Int32 _idx_row = 0; _idx_row < m_excel_max_rows; _idx_row++)
      {
        if (_xls_current_row > m_excel_max_rows)
        {
          break;
        }
        if (_idx_row >= MachinesAndGames.Rows.Count)
        {
          break;
        }
        _row = MachinesAndGames.Rows[_idx_row];

        OnProgressBar(_idx_row, MachinesAndGames.Rows.Count);

        _current_type = (String)(_row.ItemArray[1]);
        if (_last_type != "")
        {
          if ((_last_type == Resource.String("STR_MAILING_MACHINE_AND_GAME_REPORT_BYGAME")) || (_last_type == Resource.String("STR_MAILING_MACHINE_AND_GAME_REPORT_BYMACHINE")))
          {
            if (_current_type != _last_type)
            {
              _xls_cut_row = _xls_current_row;
              _range = Sheet.Cells[_xls_current_row, 0, _xls_current_row, 5];
              _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;
              _range.Borders[BordersIndex.EdgeTop].Color = Colors.Black;
              _range = Sheet.Cells[5, 0, _xls_current_row - 1, 0];
              MergeRange(ref _range);
              _range.VerticalAlignment = VAlign.Center;
            }
          }
        }
        else
        {
          _range = Sheet.Cells[_xls_current_row, 0, _xls_current_row, 5];
          _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;
          _range.Borders[BordersIndex.EdgeTop].Color = Colors.Black;
          _range.Borders[BordersIndex.EdgeBottom].LineStyle = LineStyle.Continous;
          _range.Borders[BordersIndex.EdgeBottom].Color = Colors.Black;
        }
        _last_type = _current_type;
        // ITEM LOOP
        _xls_current_col = 0;

        for (_index = 1; _index < MachinesAndGames.Columns.Count; _index++)
        {
          _range = Sheet.Cells[_xls_current_row, _xls_current_col];
          _range.Value = _row.ItemArray[_index];
          // Formatting currency
          if (_xls_current_col > 1)
          {
            _range.NumberFormat = m_currency_format;
          }
          _xls_current_col++;
        } // ITEM LOOP

        _xls_current_row++;
      } // ROW LOOP

      // Line draws
      if (_xls_cut_row > 0)
      {
        _range = Sheet.Cells[_xls_cut_row, 0, _xls_current_row - 1, 0];
        MergeRange(ref _range);
        _range.VerticalAlignment = VAlign.Center;
      }

      _range = Sheet.Cells[_xls_current_row - 1, 0, _xls_current_row - 1, 5];
      _range.Borders[BordersIndex.EdgeBottom].LineStyle = LineStyle.Continous;
      _range.Borders[BordersIndex.EdgeBottom].Color = Colors.Black;

      _range = Sheet.Cells[4, 5, _xls_current_row - 1, 5];
      _range.Borders[BordersIndex.EdgeRight].LineStyle = LineStyle.Continous;
      _range.Borders[BordersIndex.EdgeRight].Color = Colors.Black;

      _cell1 = Sheet.Cells[4, 0];
      _cell2 = Sheet.Cells[4, 5];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;

      _font.Bold = true;
      _interior.Color = SpreadsheetGear.Color.FromArgb(192, 192, 192); // EXCEL_COLOR_INDEX_GRAY
      _interior.Pattern = Pattern.Solid; // 1;     // xlSolid
      _interior.PatternColorIndex = -1; // -4105;  // xlAutomatic      
      _entire_column.AutoFit();

      // Set column width manually
      Sheet.Range[0, 0].EntireColumn.ColumnWidth = 18;
      Sheet.Range[0, 1].EntireColumn.ColumnWidth = 36;
      Sheet.Range[0, 2, 0, 6].EntireColumn.ColumnWidth = 14;

    } // FillMachineAndGameReport


    //------------------------------------------------------------------------------
    // PURPOSE : Generic Conversion from CashierSessionDetails to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String Filename
    //
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean CashierSessionDetailsToExcel(DataTable Table, String Filename, Dictionary<String, String> Params, Boolean ShowFileExportedForm,
                                                       out ExcelFileLocation FileLocation, Boolean OnlyPreview, out IWorkbook Workbook)
    {
      IWorkbook _book;
      IWorksheet _sheet;
      Int32 _idx_sheet;
      Int32 _num_needed_sheets;
      Int32 _idx_start_row;
      Boolean _type_mode;

      FileLocation = new ExcelFileLocation();
      FileLocation.Filepath = "";
      FileLocation.Filename = "";
      FileLocation.NlsId = 0;
      FileLocation.ExecutionSuccess = false;
      FileLocation.ShowFileExportedForm = ShowFileExportedForm;

      _book = null;
      _sheet = null;
      Workbook = null;

      // False -> Open excel, True -> Save excel
      _type_mode = (Filename.Length > 0);

      try
      {
        // Initilize excel spreadsheetgear.
        _book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        // Add needed sheets.
        _num_needed_sheets = 3;
        while (_num_needed_sheets - _book.Worksheets.Count > 0)
        {
          _book.Worksheets.Add();
        }
        // Or close unused sheets.
        while (_book.Worksheets.Count > _num_needed_sheets && _book.Worksheets.Count > 1)
        {
          _sheet = _book.Worksheets[_book.Worksheets.Count];
          _sheet.Delete();
        }

        CalculateNumberFormats();

        _idx_sheet = 0;
        _idx_start_row = 0;
        OnProgressBar(0, Table.Rows.Count);

        _sheet = _book.Worksheets[_idx_sheet];

        FillSheetCashierSessionDetails(_sheet, Table, _idx_start_row, Params);

        _idx_sheet++;
        _idx_start_row += m_excel_max_rows;

        OnProgressBar(Table.Rows.Count, Table.Rows.Count);

        if (_num_needed_sheets > 1)
        {
          _sheet = _book.Worksheets[0];
          _sheet.Select();
        }

        if (OnlyPreview)
        {
          Workbook = _book;
          FileLocation.ExecutionSuccess = false;
        }
        else
        {
          WriteToFile(_book, _type_mode, -1, Filename, FileLocation);
          FileLocation.ExecutionSuccess = true;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        FileLocation.ExecutionSuccess = false;
        FileLocation.NlsId = 16000 + 8597;

        return false;
      }
      finally
      {
        if (!OnlyPreview)
        {
          if (_book != null)
          {
            try
            {
              _book.Close();
              _book = null;
            }
            catch { }
          }
        }
      }
    } // CashierSessionDetailsToExcel


    //------------------------------------------------------------------------------
    // PURPOSE : Generic Conversion from CageSessionReport to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String Filename
    //
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean CageSessionReportToExcel(DataTable Table, String Filename, Dictionary<String, String> Params, Boolean ShowFileExportedForm,
                                                       out ExcelFileLocation FileLocation, Boolean OnlyPreview, out IWorkbook Workbook)
    {
      IWorkbook _book;
      IWorksheet _sheet;
      Int32 _idx_sheet;
      Int32 _num_needed_sheets;
      Int32 _idx_start_row;
      Boolean _type_mode;

      FileLocation = new ExcelFileLocation();
      FileLocation.Filepath = "";
      FileLocation.Filename = "";
      FileLocation.NlsId = 0;
      FileLocation.ExecutionSuccess = false;
      FileLocation.ShowFileExportedForm = ShowFileExportedForm;

      _book = null;
      _sheet = null;
      Workbook = null;

      // False -> Open excel, True -> Save excel
      _type_mode = (Filename.Length > 0);

      try
      {
        // Initilize excel spreadsheetgear.
        _book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        // Add needed sheets.
        _num_needed_sheets = 3;
        while (_num_needed_sheets - _book.Worksheets.Count > 0)
        {
          _book.Worksheets.Add();
        }
        // Or close unused sheets.
        while (_book.Worksheets.Count > _num_needed_sheets && _book.Worksheets.Count > 1)
        {
          _sheet = _book.Worksheets[_book.Worksheets.Count];
          _sheet.Delete();
        }

        CalculateNumberFormats();

        _idx_sheet = 0;
        _idx_start_row = 0;
        OnProgressBar(0, Table.Rows.Count);

        _sheet = _book.Worksheets[_idx_sheet];

        FillSheetCageSessionReport(_sheet, Table, _idx_start_row, Params);

        _idx_sheet++;
        _idx_start_row += m_excel_max_rows;

        OnProgressBar(Table.Rows.Count, Table.Rows.Count);

        if (_num_needed_sheets > 1)
        {
          _sheet = _book.Worksheets[0];
          _sheet.Select();
        }

        if (OnlyPreview)
        {
          Workbook = _book;
          FileLocation.ExecutionSuccess = false;
        }
        else
        {
          WriteToFile(_book, _type_mode, -1, Filename, FileLocation);
          FileLocation.ExecutionSuccess = true;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        FileLocation.ExecutionSuccess = false;

        return false;
      }
      finally
      {
        if (!OnlyPreview)
        {
          if (_book != null)
          {
            try
            {
              _book.Close();
            }
            catch { }
          }
        }
      }
    } // CageSessionReportToExcel


    //------------------------------------------------------------------------------
    // PURPOSE : Generic Conversion from WxpMachineMeters to Excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String Filename
    //
    //
    // RETURNS :
    //      - True: Converted ok. False: Otherwise.
    //
    public static Boolean WxpMachineMetersToExcel(DataSet WxpMachineMeters,
                                                  ref String Filename,
                                                  Dictionary<String,
                                                  String> Params)
    {
      // Deletting unused columns
      WxpMachineMeters.Tables[0].Columns.Remove("TE_TERMINAL_ID");

      // Setting Names
      WxpMachineMeters.Tables[0].TableName = Resource.String("STR_MAILING_WXP_MACHINE_METERS_TITLE");

      WxpMachineMeters.Tables[0].Columns["TE_PROVIDER_ID"].ColumnName = Resource.String("STR_MAILING_WXP_MACHINE_METERS_PROVIDER");
      WxpMachineMeters.Tables[0].Columns["TE_NAME"].ColumnName = Resource.String("STR_MAILING_WXP_MACHINE_METERS_TERMINAL");
      WxpMachineMeters.Tables[0].Columns["TE_REGISTRATION_CODE"].ColumnName = Resource.String("STR_MAILING_WXP_MACHINE_METERS_REGISTRATION_CODE");
      WxpMachineMeters.Tables[0].Columns["MM_PLAYED_AMOUNT"].ColumnName = Resource.String("STR_MAILING_WXP_MACHINE_METERS_PLAYED");
      WxpMachineMeters.Tables[0].Columns["MM_WON_AMOUNT"].ColumnName = Resource.String("STR_MAILING_WXP_MACHINE_METERS_WON");
      WxpMachineMeters.Tables[0].Columns["MM_JACKPOT_AMOUNT"].ColumnName = Resource.String("STR_MAILING_WXP_MACHINE_METERS_JACKPOT");

      SetFileExtensionAndMaxRows(null, ref Filename);

      if (!ExcelConversion.DataSetToExcel(WxpMachineMeters, Filename, Params))
      {
        Log.Error("SendEmail: Error creating Excel file " + Filename + ".");

        return false;
      }

      return true;
    } // WxpMachineMetersToExcel

    //------------------------------------------------------------------------------
    // PURPOSE : To Save excel file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - IWorkbook:  Book
    //          - Boolean:    TypeMode
    //          - Int32:      NumRows
    //          - String:     Filename
    //
    // RETURNS :
    //      - ExcelFileLocation: structure with excel file information.
    //
    public static void WriteToFile(IWorkbook Book, Boolean TypeMode, Int32 NumRows, String Filename, ExcelFileLocation FileLocation)
    {
      CultureInfo _old_ci;
      Excel.Application _excel;
      Excel.Workbooks _books;
      Excel.Workbook _book_orig;
      Excel.Workbook _book_dest;
      Excel.Worksheet _sheet_dest;
      Excel.Worksheet _sheet_tmp;
      FileStream _file;

      _old_ci = Thread.CurrentThread.CurrentCulture;
      _excel = null;

      try
      {
        FileLocation.Filename = "ExcelFile";
        FileLocation.Filepath = "";
        FileLocation.NlsId = 0;
        FileLocation.TypeMode = TypeMode;

        if (Filename.Length > 0)
        {
          FileLocation.Filepath = Filename;
          FileLocation.Filename = Path.GetFileName(Filename);
        }

        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        // Restore DateTime and Number Format.
        Thread.CurrentThread.CurrentCulture.DateTimeFormat = _old_ci.DateTimeFormat;
        Thread.CurrentThread.CurrentCulture.NumberFormat = _old_ci.NumberFormat;

        _excel = null;
        try
        {
          // Start Excel and get Application object.
          _excel = new Excel.ApplicationClass();
          _excel.DisplayAlerts = false;
        }
        catch
        {
          _excel = null;

          //// If has to show Pop-up, this will be the message to show.
          //// 2017: Excel not installed.
          //FileLocation.NlsId = GLB_NLS_GUI_PLAYER_TRACKING.Id(2017);
          //FileLocation.NlsId = Resource.String("STR_EXPORT_FILE_EXCEL_NOT_INSTALLED");
          FileLocation.NlsId = 16000 + 2017;
        }

        SetFileExtensionAndMaxRows(_excel, ref FileLocation.Filename);

        // Create the output folder if it does not exist.
        FileLocation.Filepath = System.Environment.CurrentDirectory + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + OUTPUT_FOLDER;

        if (!Directory.Exists(FileLocation.Filepath))
        {
          Directory.CreateDirectory(FileLocation.Filepath);
        }

        FileLocation.Filename = FileLocation.Filepath + Path.DirectorySeparatorChar + FileLocation.Filename;
        _file = File.Create(FileLocation.Filename);
        FileLocation.Filepath = _file.Name;

        // If MS Excel is installed, the file can be shown.
        // Unless it's Excel 2003 and has more than 65,000 rows.
        FileLocation.ShowFile = (_excel != null);

        try
        {
          Int32 MAX_COLUMN_WIDTH;

          MAX_COLUMN_WIDTH = 100;
          Book.Worksheets[0].Range["C:C"].ColumnWidth = Math.Min(Book.Worksheets[0].Range["C:C"].ColumnWidth * 1.1, MAX_COLUMN_WIDTH);
          Book.Worksheets[0].Range["D:D"].ColumnWidth = Math.Min(Book.Worksheets[0].Range["D:D"].ColumnWidth * 1.1, MAX_COLUMN_WIDTH);

          switch (ExcelVersion)
          {
            case EXCEL_VERSION.AFTER_2007:
              Book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
              break;

            case EXCEL_VERSION.BEFORE_2007:
              if (NumRows <= EXCEL_MAX_ROWS_BEFORE_2007)
              {
                // Si el archivo tiene menos de 65000 registros, se guarda en .xls y se abre.
                Book.SaveToStream(_file, FileFormat.Excel8);
              }
              else
              {
                // Si el archivo tiene m�s de 65000 registros se guarda como .xlsx y se muestra el popup con los links.
                Book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);

                FileLocation.ShowFile = false;
                //// 2018: Excel installed but can't open because num rows exceeds Excel 2003 limit.
                //FileLocation.NlsId = GLB_NLS_GUI_PLAYER_TRACKING.Id(2018);
                //FileLocation.NlsId = Resource.String("STR_EXPORT_FILE_NOT_OPEN_BY_EXCEL_VERSION");
                FileLocation.NlsId = 16000 + 2018;
              }
              break;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          _file.Close();
        }

        // FileLocation.TypeMode == false; No guarda archivo. Lo visualiza.
        // FileLocation.TypeMode == True; Si guarda archivo. No visualiza.
        if (FileLocation.TypeMode)
        {
          // Mostrar el popUp indicando la ruta.
          FileLocation.ShowFile = false;
        }
        else
        {
          if (FileLocation.ShowFile)
          {
            // To create a new workbook and set his properties.
            _excel.SheetsInNewWorkbook = 1;
            _excel.Visible = false;
            _excel.DisplayAlerts = false;

            // Closes workbook after assign the value
            Book.Close();
            Book = null;

            _books = _excel.Workbooks;
            _book_dest = (Excel.Workbook)_books.Add(Type.Missing);
            _sheet_tmp = (Excel.Worksheet)_book_dest.Sheets[1];
            _sheet_tmp.Name = "_";

            ReleaseComObject(_sheet_tmp);


            // Open the excel file created with spreadSheetGear.
            _book_orig = _books.Open(FileLocation.Filepath.ToString(), Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing);

            foreach (Excel.Worksheet _sheet_orig in _book_orig.Sheets)
            {
              _sheet_dest = (Excel.Worksheet)_book_dest.Sheets[_book_dest.Sheets.Count];
              _sheet_orig.Move(_sheet_dest, Type.Missing);

              ReleaseComObject(_sheet_orig);
              ReleaseComObject(_sheet_dest);
            }

            _sheet_dest = (Excel.Worksheet)_book_dest.Sheets[_book_dest.Sheets.Count];
            _sheet_dest.Delete();
            ReleaseComObject(_sheet_dest);

            _sheet_tmp = (Excel.Worksheet)_book_dest.Sheets[1];
            _sheet_tmp.Select(Type.Missing);
            ReleaseComObject(_sheet_tmp);

            ReleaseComObject(_book_orig);
            ReleaseComObject(_book_dest);
            ReleaseComObject(_books);

            _excel.Visible = true;
            _excel.UserControl = true;
          }
        }
      }
      finally
      {
        if (_excel != null && !_excel.Visible)
        {
          try
          {
            _excel.Quit();
          }
          catch { }
        }
        ReleaseComObject(_excel);

        // Restore previous culture
        Thread.CurrentThread.CurrentCulture = _old_ci;
      }
    } // WriteToFile

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate the extension and the maximum rows allowed according to the Excel version.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Application Excel
    //          - String Filename
    //
    //      - OUTPUT :
    //          - String Filename
    //
    // RETURNS :
    //
    public static void SetFileExtensionAndMaxRows(Excel.Application Excel, ref String Filename)
    {
      SetFileExtensionAndMaxRows(Excel, ref Filename, 0);
    } // SetFileExtensionAndMaxRows

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate the extension and the maximum rows allowed according to the Excel version.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Application: Excel
    //          - String: Filename
    //          - Int32: NumRows
    //
    //      - OUTPUT :
    //          - String: Filename
    //
    // RETURNS :
    //
    public static void SetFileExtensionAndMaxRows(Excel.Application Excel, ref String Filename, Int32 NumRows)
    {
      String _ext;
      String _new_ext;

      m_excel_version = EXCEL_VERSION.AFTER_2007;
      m_excel_max_rows = EXCEL_MAX_ROWS_AFTER_2007;
      _new_ext = ".xlsx";

      if (Excel != null)
      {
        if (Excel.Version.CompareTo("12") < 0)
        {
          m_excel_version = EXCEL_VERSION.BEFORE_2007;

          // It's only an Excel 2003 if NumRows is less than 65,000.
          if (NumRows <= EXCEL_MAX_ROWS_BEFORE_2007)
          {
            m_excel_max_rows = EXCEL_MAX_ROWS_BEFORE_2007;
            _new_ext = ".xls";
          }
          else
          {
            m_excel_max_rows = EXCEL_MAX_ROWS_AFTER_2007;
            _new_ext = ".xlsx";
          }
        }

        // RXM 25-FEB-2013: Set default fileformat to avoid errors in some excel versions
        Excel.DefaultSaveFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault;
      }

      // Add the extension to the Filename if not defined.
      _ext = Path.GetExtension(Filename);
      if (_ext != ".xls" && _ext != ".xlsx")
      {
        Filename = Filename + _new_ext;
      }
    } // SetFileExtensionAndMaxRows

    //------------------------------------------------------------------------------
    // PURPOSE : Convert DataTable to CSV.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable: Table
    //          - String: Filename
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: Return true if execution is right.
    //
    public static Boolean DataTableToCSV(DataTable Table, String Filename)
    {
      StringBuilder _sb;
      String _value;
      String _separator;
      String _text_delimeter_0;
      String _text_delimeter_1;
      String _text_delimeter_2;

      _separator = "\t";
      _text_delimeter_0 = "";
      _text_delimeter_1 = "";
      _text_delimeter_2 = "'";

      if (Table == null)
      {
        return false;
      }
      if (Table.Columns.Count == 0)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder(Table.Columns.Count * 20);

        using (FileStream _fs = new FileStream(Filename, FileMode.Create, FileAccess.Write, FileShare.Read))
        {
          using (TextWriter _tw = new StreamWriter(_fs, Encoding.Unicode))
          {
            foreach (DataColumn _col in Table.Columns)
            {
              if (_sb.Length > 0)
              {
                _sb.Append(_separator);
              }
              _sb.Append(_text_delimeter_0 + _col.Caption + _text_delimeter_1);
            }
            _tw.WriteLine(_sb.ToString());

            foreach (DataRow _row in Table.Rows)
            {
              _sb.Length = 0;
              for (int _idx_col = 0; _idx_col < _row.Table.Columns.Count; _idx_col++)
              {
                if (_idx_col > 0)
                {
                  _sb.Append(_separator);
                }

                _value = _row.IsNull(_idx_col) ? "" : _row[_idx_col].ToString();

                if (IsCardNumber(_value))
                {
                  _sb.Append(_text_delimeter_0 + _text_delimeter_2 + _value + _text_delimeter_1);
                }
                else
                {
                  _sb.Append(_text_delimeter_0 + _value + _text_delimeter_1);
                }
              }
              _tw.WriteLine(_sb.ToString());
            }

            _tw.Close();
          }

          _fs.Close();
        }
        return true;
      }
      catch
      {
        return false;
      }
    } // DataTableToCSV

    //------------------------------------------------------------------------------
    // PURPOSE : Convert DataSet to CSV.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable: Table
    //          - String: Filename
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: Return true if execution is right.
    //
    public static Boolean DataSetToCSV(DataSet Set, String Filename)
    {
      String _filename;
      String _extension;
      String _path;
      String _base;
      Int32 _id;

      if (Set == null)
      {
        return false;
      }
      if (Set.Tables.Count == 0)
      {
        return true;
      }

      _id = 1;

      _path = System.IO.Path.GetFullPath(Filename);
      _filename = System.IO.Path.GetFileNameWithoutExtension(Filename);

      _extension = System.IO.Path.GetExtension(Filename);
      if (String.IsNullOrEmpty(_extension))
      {
        _base = Filename;
      }
      else
      {
        _base = Filename.Substring(0, Filename.LastIndexOf(_extension));
      }

      foreach (DataTable _table in Set.Tables)
      {
        _filename = _base;
        if (_id > 1)
        {
          _filename += "_" + _id.ToString();
        }
        _filename += _extension;

        if (!DataTableToCSV(_table, _filename))
        {
          return false;
        }
      }

      return true;
    } // DataSetToCSV


    //------------------------------------------------------------------------------
    // PURPOSE: This function says if you have permissions to create files in the given path.
    // 
    //  PARAMS:
    //     - INPUT:
    //         - PathToTest: String.
    // 
    //     - OUTPUT:
    // 
    // RETURNS:
    //       True if has permissions and file can be create
    // 
    //
    public static Boolean HasPermissionsOverDirectory(ref String PathToTest)
    {
      String _filename;
      FileStream _file;
      Boolean _has_permissions;
      Boolean _file_created;

      if (String.IsNullOrEmpty(PathToTest))
      {
        PathToTest = System.Environment.CurrentDirectory;
      }
      _file_created = false;
      _has_permissions = true;
      _filename = "ExcelFileTest";
      _filename = PathToTest + Path.DirectorySeparatorChar + _filename;

      try
      {
        _file = new FileStream(_filename, FileMode.Create, FileAccess.Write);
        _file.Close();
        _file_created = true;
        return _has_permissions;
      }
      catch (UnauthorizedAccessException)
      {
        _has_permissions = false;
        return _has_permissions;
      }
      finally
      {
        if (_has_permissions && _file_created)
        {
          File.Delete(_filename);
        }
      }

    } // HasPermissionsOverDirectory

    //------------------------------------------------------------------------------
    // PURPOSE : Generic Conversion Range of Cells (Column) to a specific format
    //
    //  PARAMS :
    //      - INPUT :
    //          - Iworksheet worksheet, Range to covert, DataTypes
    //          - Params  Dictionary with Two columns ->  0: String "ColumnName", 1: String "ColumnType"
    //
    //      - OUTPUT :
    //          - None, the range worksheet change to a specific format
    //
    // RETURNS:
    //       - void
    // 
    public static void ConvertCells(IWorksheet Sheet, IRange Range, Dictionary<String, String> Params)
    {
      object[,] _range_values;
      int _num_rows;
      int _num_cols;
      int _count = 0;

      IRange _cell1;
      IRange _cell2;
      IRange _range;

      _range_values = (object[,])Range.Value;
      _num_rows = _range_values.GetLength(0) - 1;
      _num_cols = _range_values.GetLength(1) - 1;

      foreach (KeyValuePair<String, String> _param in Params)
      {

        if (_param.Value == "System.DateTime")
        {
          _cell1 = Sheet.Cells[1, _count];
          _cell2 = Sheet.Cells[_num_rows, _count];
          _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
          _range.NumberFormat = Format.DateTimeCustomFormatString(true);
        }

        _count++;
      }


    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns True or False if the Value can be casted to DateTime
    //
    //  PARAMS:
    //      - INPUT:
    //          - Value: object to cast to DateTime
    //      - OUT:
    //          - ValueDateTime: casted Value
    //
    // RETURNS:
    //      - Boolean:
    //
    public static Boolean ConvertDateTime(Object Value, out DateTime ValueDateTime)
    {
      DateTime _date_time_value;
      Double _double_value;

      ValueDateTime = DateTime.MinValue;

      if (Double.TryParse(Value.ToString(), out _double_value))
      {
        if (_double_value < 0)
        {
          return false;
        }

        ValueDateTime = DateTime.FromOADate(_double_value);

        return true;
      }
      else if (DateTime.TryParse(Value.ToString(), out _date_time_value))
      {
        ValueDateTime = _date_time_value;

        return true;
      }

      return false;
    }  // ConvertDateTime

    #endregion

    #region PrivateFunctions

    //------------------------------------------------------------------------------
    // PURPOSE : Fill an Excel sheet with the game statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - DataRow[] GameRows
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillSheetGameStatistics(IWorksheet Sheet,
                                                DateTime FromDate,
                                                DateTime ToDate,
                                                DataRow[] GameRows,
                                                Dictionary<String, String> Params)
    {
      IRange _cells;
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IRange _entire_column;
      IRange _entire_row;
      IFont _font;
      IInterior _interior;

      Int32 _xls_row;
      Int32 _first_data_row;
      Int32 _last_data_row;
      Int32 _idx_row;
      Int32 _idx_col;
      DateTime _current_date;
      DateTime _stats_date;
      Int32 _col_offset;

      Int32 _max_column_width;
      Int32 _count_col_beg;
      Int32 _count_col_end;

      _cells = Sheet.Cells;

      _font = _cells.Font;
      _font.Size = 9;
      _font.Name = "Calibri";

      if (AddParamsToSheetOnLeft(Sheet, Params))
      {
        _col_offset = 3;
      }
      else
      {
        _col_offset = 0;
      }

      _range = Sheet.Cells[1, 2 + _col_offset];
      _range.Value = Resource.String("STR_GAME_STATISTICS");

      _range = Sheet.Cells[2, 2 + _col_offset];
      _range.Value = Sheet.Name;

      _cell1 = Sheet.Cells[1, 2 + _col_offset];
      _cell2 = Sheet.Cells[2, 2 + _col_offset];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _font.Size = 12;
      _font.Bold = true;

      _cell1 = Sheet.Cells[1, GAMES_PLAYED_AMOUNT];
      _cell2 = Sheet.Cells[1, GAMES_LAST_COLUMN];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _entire_row = _range.EntireRow;
      MergeRange(ref _range);
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _entire_row.RowHeight = 15.75;

      _cell1 = Sheet.Cells[2, GAMES_PLAYED_AMOUNT];
      _cell2 = Sheet.Cells[2, GAMES_LAST_COLUMN];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _entire_row = _range.EntireRow;
      MergeRange(ref _range);
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _entire_row.RowHeight = 15.75;

      // Column Headers
      _xls_row = GAMES_HEADER_ROW;

      _cell1 = Sheet.Cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      MergeRange(ref _range);
      _range.Value = "Amount in Mexican Pesos";

      _cell1 = Sheet.Cells[_xls_row, GAMES_PLAYS_COUNT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_WON_PCT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      MergeRange(ref _range);
      _range.Value = "Plays";

      _cell1 = Sheet.Cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_WON_PCT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_row = _range.EntireRow;

      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _font.Bold = true;
      _font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE
      _entire_row.RowHeight = 17.25;

      if (m_draw_spaces)
      {
        _xls_row++;

        _range = Sheet.Cells[_xls_row, GAMES_FIRST_COLUMN];
        _range.EntireRow.RowHeight = 5.25;
      }

      _xls_row++;

      _range = Sheet.Cells[_xls_row, GAMES_DATE];
      _range.Value = "DATE";

      _range = Sheet.Cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _range.Value = "Played";

      _range = Sheet.Cells[_xls_row, GAMES_WON_AMOUNT];
      _range.Value = "Won";

      _range = Sheet.Cells[_xls_row, GAMES_PAYOUT_PCT];
      _range.Value = "Payout %";

      _range = Sheet.Cells[_xls_row, GAMES_JACKPOT];
      _range.Value = "Jackpot";

      _cell1 = Sheet.Cells[_xls_row, GAMES_CONTRIBUTION_PCT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_CONTRIBUTION_AMOUNT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];

      MergeRange(ref _range);
      _range.Value = "Contribution";

      _range = Sheet.Cells[_xls_row, GAMES_NETWIN];
      _range.Value = "Netwin";

      _range = Sheet.Cells[_xls_row, GAMES_REAL_NETWIN];
      _range.Value = "REAL Netwin";

      _range = Sheet.Cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
      _range.Value = "Real Payout %";

      _range = Sheet.Cells[_xls_row, GAMES_PLAYS_COUNT];
      _range.Value = "Plays";

      _range = Sheet.Cells[_xls_row, GAMES_WON_COUNT];
      _range.Value = "Won";

      _range = Sheet.Cells[_xls_row, GAMES_WON_PCT];
      _range.Value = "Won %";

      _range = Sheet.Cells[_xls_row, GAMES_AVERAGE_PLAY];
      _range.Value = "Average Play";

      _cell1 = Sheet.Cells[_xls_row, GAMES_FIRST_COLUMN];
      _cell2 = Sheet.Cells[_xls_row, GAMES_LAST_COLUMN];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_row = _range.EntireRow;

      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _font.Bold = true;
      _font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE
      _entire_row.RowHeight = 27.75;

      // Data Rows

      _xls_row++;
      _first_data_row = _xls_row;
      _current_date = FromDate;

      foreach (DataRow _stats in GameRows)
      {
        if (_xls_row > m_excel_max_rows)
        {
          break;
        }

        _stats_date = (DateTime)_stats["Date"];

        while (_current_date < _stats_date)
        {
          SetEmptyRow(Sheet, _xls_row, _current_date);

          _xls_row++;
          _current_date = _current_date.AddDays(1);
        }
        _current_date = _stats_date.AddDays(1);

        if (_xls_row % 2 == (m_draw_spaces ? 1 : 0))
        {
          _cell1 = Sheet.Cells[_xls_row, GAMES_FIRST_COLUMN];
          _cell2 = Sheet.Cells[_xls_row, GAMES_LAST_COLUMN];
          _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
          _interior = _range.Interior;

          _interior.Color = SpreadsheetGear.Color.FromArgb(192, 192, 192); // EXCEL_COLOR_INDEX_GRAY

        }

        _range = Sheet.Cells[_xls_row, GAMES_DATE];
        _range.Value = _stats_date;

        _range = Sheet.Cells[_xls_row, GAMES_PLAYED_AMOUNT];
        _range.Value = (Decimal)_stats["PlayedAmount"];

        _range = Sheet.Cells[_xls_row, GAMES_WON_AMOUNT];
        _range.Value = (Decimal)_stats["WonAmount"];

        _range = Sheet.Cells[_xls_row, GAMES_PAYOUT_PCT];
        _range.Value = "=IF(" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + "> 0, " + ColName(GAMES_WON_AMOUNT + 1) + (_xls_row + 1) + "/" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + ", \"\")";

        _range = Sheet.Cells[_xls_row, GAMES_JACKPOT];
        _range.Value = (Decimal)_stats["Jackpot"];

        _range = Sheet.Cells[_xls_row, GAMES_CONTRIBUTION_PCT];
        _range.Value = (0.01m * (Decimal)_stats["JackpotContributionPct"]);

        _range = Sheet.Cells[_xls_row, GAMES_CONTRIBUTION_AMOUNT];
        _range.Value = "=" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + "*" + ColName(GAMES_CONTRIBUTION_PCT + 1) + (_xls_row + 1);

        _range = Sheet.Cells[_xls_row, GAMES_NETWIN];
        _range.Value = "=" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + "-" + ColName(GAMES_WON_AMOUNT + 1) + (_xls_row + 1);

        _range = Sheet.Cells[_xls_row, GAMES_REAL_NETWIN];
        _range.Value = "=" + ColName(GAMES_NETWIN + 1) + (_xls_row + 1) + "+" + ColName(GAMES_JACKPOT + 1) + (_xls_row + 1) + "-" + ColName(GAMES_CONTRIBUTION_AMOUNT + 1) + (_xls_row + 1);

        _range = Sheet.Cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
        _range.Value = "=IF(" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + "> 0, 1-" + ColName(GAMES_REAL_NETWIN + 1) + (_xls_row + 1) + "/" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + ", \"\")";

        _range = Sheet.Cells[_xls_row, GAMES_PLAYS_COUNT];
        _range.Value = _stats["PlayedCount"];

        _range = Sheet.Cells[_xls_row, GAMES_WON_COUNT];
        _range.Value = _stats["WonCount"];

        _range = Sheet.Cells[_xls_row, GAMES_WON_PCT];
        _range.Value = "=IF(" + ColName(GAMES_PLAYS_COUNT + 1) + (_xls_row + 1) + "> 0, " + ColName(GAMES_WON_COUNT + 1) + (_xls_row + 1) + "/" + ColName(GAMES_PLAYS_COUNT + 1) + (_xls_row + 1) + ", \"\")";

        _range = Sheet.Cells[_xls_row, GAMES_AVERAGE_PLAY];
        _range.Value = "=IF(" + ColName(GAMES_PLAYS_COUNT + 1) + (_xls_row + 1) + "> 0, " + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + "/" + ColName(GAMES_PLAYS_COUNT + 1) + (_xls_row + 1) + ", \"\")";

        _xls_row++;
      }

      while (_current_date < ToDate)
      {
        SetEmptyRow(Sheet, _xls_row, _current_date);

        _xls_row++;
        _current_date = _current_date.AddDays(1);
      }

      _last_data_row = _xls_row - 1;

      // Totals

      _range = Sheet.Cells[_xls_row, GAMES_DATE];
      _range.Value = "TOTAL:";
      _range.HorizontalAlignment = HAlign.Center;

      _range = Sheet.Cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _range.Value = "=SUM(" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_first_data_row + 1) + ":" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row - 1 + 1) + ")";

      _range = Sheet.Cells[_xls_row, GAMES_WON_AMOUNT];
      _range.Value = "=SUM(" + ColName(GAMES_WON_AMOUNT + 1) + (_first_data_row + 1) + ":" + ColName(GAMES_WON_AMOUNT + 1) + (_xls_row - 1 + 1) + ")";

      _range = Sheet.Cells[_xls_row, GAMES_PAYOUT_PCT];
      _range.Value = "=IF(" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + "> 0, " + ColName(GAMES_WON_AMOUNT + 1) + (_xls_row + 1) + "/" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + ", \"\")";

      _range = Sheet.Cells[_xls_row, GAMES_JACKPOT];
      _range.Value = "=SUM(" + ColName(GAMES_JACKPOT + 1) + (_first_data_row + 1) + ":" + ColName(GAMES_JACKPOT + 1) + (_xls_row - 1 + 1) + ")";

      _range = Sheet.Cells[_xls_row, GAMES_CONTRIBUTION_AMOUNT];
      _range.Value = "=SUM(" + ColName(GAMES_CONTRIBUTION_AMOUNT + 1) + (_first_data_row + 1) + ":" + ColName(GAMES_CONTRIBUTION_AMOUNT + 1) + (_xls_row - 1 + 1) + ")";

      _range = Sheet.Cells[_xls_row, GAMES_NETWIN];
      _range.Value = "=SUM(" + ColName(GAMES_NETWIN + 1) + (_first_data_row + 1) + ":" + ColName(GAMES_NETWIN + 1) + (_xls_row - 1 + 1) + ")";

      _range = Sheet.Cells[_xls_row, GAMES_REAL_NETWIN];
      _range.Value = "=SUM(" + ColName(GAMES_REAL_NETWIN + 1) + (_first_data_row + 1) + ":" + ColName(GAMES_REAL_NETWIN + 1) + (_xls_row - 1 + 1) + ")";

      _range = Sheet.Cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
      _range.Value = "=IF(" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + "> 0, 1-" + ColName(GAMES_REAL_NETWIN + 1) + (_xls_row + 1) + "/" + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + ", \"\")";

      _range = Sheet.Cells[_xls_row, GAMES_PLAYS_COUNT];
      _range.Value = "=SUM(" + ColName(GAMES_PLAYS_COUNT + 1) + (_first_data_row + 1) + ":" + ColName(GAMES_PLAYS_COUNT + 1) + (_xls_row - 1 + 1) + ")";

      _range = Sheet.Cells[_xls_row, GAMES_WON_COUNT];
      _range.Value = "=SUM(" + ColName(GAMES_WON_COUNT + 1) + (_first_data_row + 1) + ":" + ColName(GAMES_WON_COUNT + 1) + (_xls_row - 1 + 1) + ")";

      _range = Sheet.Cells[_xls_row, GAMES_WON_PCT];
      _range.Value = "=IF(" + ColName(GAMES_PLAYS_COUNT + 1) + (_xls_row + 1) + "> 0, " + ColName(GAMES_WON_COUNT + 1) + (_xls_row + 1) + "/" + ColName(GAMES_PLAYS_COUNT + 1) + (_xls_row + 1) + ", \"\")";

      _range = Sheet.Cells[_xls_row, GAMES_AVERAGE_PLAY];
      _range.Value = "=IF(" + ColName(GAMES_PLAYS_COUNT + 1) + (_xls_row + 1) + "> 0, " + ColName(GAMES_PLAYED_AMOUNT + 1) + (_xls_row + 1) + "/" + ColName(GAMES_PLAYS_COUNT + 1) + (_xls_row + 1) + ", \"\")";

      // Format Total Row
      _cell1 = Sheet.Cells[_xls_row, GAMES_FIRST_COLUMN];
      _cell2 = Sheet.Cells[_xls_row, GAMES_LAST_COLUMN];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_row = _range.EntireRow;

      _range.VerticalAlignment = VAlign.Center;
      _font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE
      _entire_row.RowHeight = 13.50;

      // Format Cells

      _cell1 = Sheet.Cells[_first_data_row, GAMES_DATE];
      _cell2 = Sheet.Cells[_last_data_row, GAMES_DATE];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _entire_row = _range.EntireRow;

      _range.HorizontalAlignment = HAlign.Center;
      _range.NumberFormat = "dddd, " + Format.DateTimeCustomFormatString(false);
      _entire_row.RowHeight = 14.25;
      _entire_row.VerticalAlignment = VAlign.Center;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_PLAYED_AMOUNT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_PLAYED_AMOUNT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_currency_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_WON_AMOUNT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_WON_AMOUNT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_currency_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_PAYOUT_PCT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_PAYOUT_PCT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_percent_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_JACKPOT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_JACKPOT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_currency_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_CONTRIBUTION_PCT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_CONTRIBUTION_PCT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_percent_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_CONTRIBUTION_AMOUNT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_CONTRIBUTION_AMOUNT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_currency_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_NETWIN];
      _cell2 = Sheet.Cells[_xls_row, GAMES_NETWIN];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_currency_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_REAL_NETWIN];
      _cell2 = Sheet.Cells[_xls_row, GAMES_REAL_NETWIN];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_currency_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_REAL_PAYOUT_PCT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_REAL_PAYOUT_PCT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_percent_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_PLAYS_COUNT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_PLAYS_COUNT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_number_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_WON_COUNT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_WON_COUNT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_number_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_WON_PCT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_WON_PCT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_percent_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_AVERAGE_PLAY];
      _cell2 = Sheet.Cells[_xls_row, GAMES_AVERAGE_PLAY];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_currency_format;

      // Auto Fit

      _cell1 = Sheet.Cells[GAMES_HEADER_ROW, GAMES_DATE];
      _cell2 = Sheet.Cells[GAMES_HEADER_ROW, GAMES_LAST_COLUMN];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _entire_column = _range.EntireColumn;
      _entire_column.AutoFit();

      _max_column_width = 100;
      _count_col_beg = GAMES_DATE;
      _count_col_end = GAMES_LAST_COLUMN;

      // Increment the insufficient Autofit to all columns shown.
      for (Int32 _count_current_col = _count_col_beg; _count_current_col <= _count_col_end; _count_current_col++)
      {
        Sheet.Range[GAMES_HEADER_ROW + 2, _count_current_col].EntireColumn.ColumnWidth = Math.Min(Sheet.Range[GAMES_HEADER_ROW + 2, _count_current_col].EntireColumn.ColumnWidth * 1.1, _max_column_width);
      }

      if (m_draw_spaces)
      {
        for (_idx_col = GAMES_FIRST_COLUMN + 1; _idx_col < GAMES_LAST_COLUMN; _idx_col += 2)
        {
          _idx_row = _first_data_row - 1;
          if (_idx_col == GAMES_CONTRIBUTION_PCT + 1)
          {
            _idx_row = _first_data_row;
          }
          _cell1 = Sheet.Cells[_idx_row, _idx_col];
          _cell2 = Sheet.Cells[_last_data_row, _idx_col];
          _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
          _interior = _range.Interior;
          _entire_column = _range.EntireColumn;

          _interior.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
          _entire_column.ColumnWidth = 0.58;

        }

        Int32[] _cols_in_white;

        _cols_in_white = new Int32[] { GAMES_PLAYS_COUNT - 1,
                                       GAMES_AVERAGE_PLAY - 1 };

        foreach (Int32 _col_idx in _cols_in_white)
        {
          _cell1 = Sheet.Cells[GAMES_HEADER_ROW, _col_idx];
          _cell2 = Sheet.Cells[GAMES_HEADER_ROW + 2, _col_idx];
          _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
          _interior = _range.Interior;
          _interior.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
        }
      }

      // Freeze first rows as they are the header.
      _range = Sheet.Cells[_first_data_row, 1];
      _range.Select();

      Sheet.WindowInfo.ScrollRow = 0;
      Sheet.WindowInfo.SplitRows = _range.Row;
      Sheet.WindowInfo.FreezePanes = true;

    } // FillSheetGameStatistics

    //------------------------------------------------------------------------------
    // PURPOSE : Fill an Excel sheet with the connected VLT info.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DateTime FromDate
    //          - DateTime ToDate
    //          - DataRow[] ConnectedRows
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillSheetGameTotalVLT(IWorksheet Sheet,
                                              DateTime FromDate,
                                              DateTime ToDate,
                                              DataRow[] ConnectedRows,
                                              Dictionary<String, String> Params)
    {
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IRange _entire_column;
      IFont _font;
      IInterior _interior;
      Int32 _xls_row;
      Int32 _first_data_row;
      Int32 _last_data_row;
      Int32 _idx_row;
      Int32 _idx_col;
      DateTime _current_date;
      DateTime _connected_date;

      Int32 _max_column_width;
      Int32 _count_col_beg;
      Int32 _count_col_end;

      // Column Headers
      _xls_row = GAMES_HEADER_ROW + (m_draw_spaces ? 2 : 1);

      _range = Sheet.Cells[_xls_row, GAMES_VLT];
      _range.Value = "VLT";

      _range = Sheet.Cells[_xls_row, GAMES_NW_VLT];
      _range.Value = "NW/VLT";

      _cell1 = Sheet.Cells[_xls_row, GAMES_VLT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_NW_VLT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;

      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _font.Bold = true;
      _font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE

      // Data Rows

      _xls_row++;
      _first_data_row = _xls_row;
      _current_date = FromDate;

      foreach (DataRow _connected in ConnectedRows)
      {
        if (_xls_row > m_excel_max_rows)
        {
          break;
        }

        _connected_date = (DateTime)_connected["Date"];

        while (_current_date < _connected_date)
        {
          _cell1 = Sheet.Cells[_xls_row, GAMES_VLT];
          _cell2 = Sheet.Cells[_xls_row, GAMES_NW_VLT];
          _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];

          _range.Value = 0;

          // RCI 01-AUG-2011
          if (_xls_row % 2 == (m_draw_spaces ? 1 : 0))
          {
            _interior = _range.Interior;
            _interior.Color = SpreadsheetGear.Color.FromArgb(192, 192, 192); // EXCEL_COLOR_INDEX_GRAY
          }

          _xls_row++;
          _current_date = _current_date.AddDays(1);
        }
        _current_date = _connected_date.AddDays(1);

        if (_xls_row % 2 == (m_draw_spaces ? 1 : 0))
        {
          _cell1 = Sheet.Cells[_xls_row, GAMES_VLT];
          _cell2 = Sheet.Cells[_xls_row, GAMES_NW_VLT];
          _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
          _interior = _range.Interior;

          _interior.Color = SpreadsheetGear.Color.FromArgb(192, 192, 192); // EXCEL_COLOR_INDEX_GRAY

        }

        _range = Sheet.Cells[_xls_row, GAMES_VLT];
        _range.Value = (Int32)_connected["NumTerminals"];

        _range = Sheet.Cells[_xls_row, GAMES_NW_VLT];
        _range.Value = "=IF(" + ColName(GAMES_VLT + 1) + (_xls_row + 1) + "> 0, " + ColName(GAMES_NETWIN + 1) + (_xls_row + 1) + "/" + ColName(GAMES_VLT + 1) + (_xls_row + 1) + ", \"\")";

        _xls_row++;
      }

      while (_current_date < ToDate)
      {
        _cell1 = Sheet.Cells[_xls_row, GAMES_VLT];
        _cell2 = Sheet.Cells[_xls_row, GAMES_NW_VLT];
        _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
        _range.Value = 0;

        // RCI 01-AUG-2011
        if (_xls_row % 2 == (m_draw_spaces ? 1 : 0))
        {
          _interior = _range.Interior;
          _interior.Color = SpreadsheetGear.Color.FromArgb(192, 192, 192); // EXCEL_COLOR_INDEX_GRAY
        }

        _xls_row++;
        _current_date = _current_date.AddDays(1);
      }

      _last_data_row = _xls_row - 1;

      // Totals
      _range = Sheet.Cells[_xls_row, GAMES_VLT];
      _range.Value = "=SUM(" + ColName(GAMES_VLT + 1) + (_first_data_row + 1) + ":" + ColName(GAMES_VLT + 1) + (_xls_row - 1 + 1) + ")";

      _range = Sheet.Cells[_xls_row, GAMES_NW_VLT];
      _range.Value = "=IF(" + ColName(GAMES_VLT + 1) + (_xls_row + 1) + "> 0, " + ColName(GAMES_NETWIN + 1) + (_xls_row + 1) + "/" + ColName(GAMES_VLT + 1) + (_xls_row + 1) + ", \"\")";

      // Format Total Row
      _cell1 = Sheet.Cells[_xls_row, GAMES_FIRST_COLUMN];
      _cell2 = Sheet.Cells[_xls_row, GAMES_NW_VLT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;

      _range.VerticalAlignment = VAlign.Center;
      _font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE

      // Format Cells
      _cell1 = Sheet.Cells[_first_data_row, GAMES_VLT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_VLT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_number_format;

      _cell1 = Sheet.Cells[_first_data_row, GAMES_NW_VLT];
      _cell2 = Sheet.Cells[_xls_row, GAMES_NW_VLT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.NumberFormat = m_currency_format;

      // Auto Fit
      _cell1 = Sheet.Cells[GAMES_HEADER_ROW, GAMES_VLT];
      _cell2 = Sheet.Cells[GAMES_HEADER_ROW, GAMES_NW_VLT];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _entire_column = _range.EntireColumn;
      _entire_column.AutoFit();

      _max_column_width = 100;
      _count_col_beg = GAMES_VLT;
      _count_col_end = GAMES_NW_VLT;

      // Increment the insufficient Autofit to all columns shown.
      for (Int32 _count_current_col = _count_col_beg; _count_current_col <= _count_col_end; _count_current_col++)
      {
        Sheet.Range[GAMES_HEADER_ROW + 2, _count_current_col].EntireColumn.ColumnWidth = Math.Min(Sheet.Range[GAMES_HEADER_ROW + 2, _count_current_col].EntireColumn.ColumnWidth * 1.1, _max_column_width);
      }

      if (m_draw_spaces)
      {
        for (_idx_col = GAMES_VLT - 1; _idx_col < GAMES_NW_VLT; _idx_col += 2)
        {
          _idx_row = _first_data_row - 1;
          _cell1 = Sheet.Cells[_idx_row, _idx_col];
          _cell2 = Sheet.Cells[_last_data_row, _idx_col];
          _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
          _interior = _range.Interior;
          _entire_column = _range.EntireColumn;

          _interior.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
          _entire_column.ColumnWidth = 0.58;

        }

        Int32[] _cols_in_white;

        _cols_in_white = new Int32[] { GAMES_VLT - 1,
                                       GAMES_NW_VLT - 1 };

        foreach (Int32 _col_idx in _cols_in_white)
        {
          _cell1 = Sheet.Cells[GAMES_HEADER_ROW, _col_idx];
          _cell2 = Sheet.Cells[GAMES_HEADER_ROW + 2, _col_idx];
          _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
          _interior = _range.Interior;
          _interior.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
        }
      }

    } // FillSheetGameTotalVLT

    //------------------------------------------------------------------------------
    // PURPOSE : Fill an Excel sheet with the terminals connected for the month.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DateTime MonthDate
    //          - Int32 LastDay
    //          - DataTable ConnectedTerms
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillSheetConnectedTerminals(IWorksheet Sheet,
                                                    DateTime MonthDate,
                                                    Int32 LastDay,
                                                    DataTable ConnectedTerms,
                                                    Dictionary<String, String> Params)
    {
      IRange _cells;
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IRange _entire_column;
      IRange _entire_row;
      IFont _font;
      IInterior _interior;
      Int32 _xls_row;
      Int32 _first_data_row;
      Int32 _last_data_row;
      Int32 _idx_col;
      Int32 _last_day_of_the_month;
      Int32 _idx_day;
      DateTime _day_date;
      String _last_provider;
      Int16 _last_terminal_type;
      Boolean _first_time_in_row;
      Int32 _col_offset;

      _last_day_of_the_month = MonthDate.AddMonths(1).AddDays(-1).Day;

      Sheet.Name = Resource.String("UC_TERMINALS_SEL_02").ToLower() + " " + MonthDate.ToString("MMMM yyyy"); //EOR 19-MAY-2016
      _cells = Sheet.Cells;
      _font = _cells.Font;
      _font.Size = 9;
      _font.Name = "Calibri";

      if (AddParamsToSheetOnLeft(Sheet, Params))
      {
        _col_offset = 2;
      }
      else
      {
        _col_offset = 0;
      }

      TERMS_PROVIDER = 1 + _col_offset;
      TERMS_TERMINAL_TYPE = 2 + _col_offset;
      TERMS_TOTAL = 3 + _col_offset;
      TERMS_DAY_START = 4 + _col_offset;
      TERMS_DAY_END = TERMS_DAY_START + _last_day_of_the_month - 1;
      TERMS_LAST_COLUMN = TERMS_DAY_END;

      _range = Sheet.Cells[1, TERMS_PROVIDER];
      // TableName in Mailing is fixed and can't be changed.
      // It starts with "CONNECTED_TERMINALS_".
      if (ConnectedTerms.TableName.StartsWith("CONNECTED_TERMINALS_"))
      {
        // Only for mailing
        if (Params == null || Params.Count == 0)
        {
          if (ConnectedTerms.Rows.Count > 0)
          {
            _range.Value = ConnectedTerms.Rows[0]["SiteName"];
          }
          else
          {
            _range.Value = "SALA DESCONOCIDA";
          }
        }
        else
        {
          _range.Value = Resource.String("STR_CONNECTED_TERMINALS");
        }
      }
      else
      {
        // GUI sets it's name correctly 
        _range.Value = ConnectedTerms.TableName;
      }

      _range = Sheet.Cells[2, TERMS_PROVIDER];
      _range.NumberFormat = "MMMM yyyy";
      _range.Value = MonthDate;

      // Column Headers
      _xls_row = TERMS_HEADER_ROW;
      _range = Sheet.Cells[_xls_row, TERMS_PROVIDER];
      _entire_column = _range.EntireColumn;
      _range.Value = "Proveedor";
      _entire_column.HorizontalAlignment = HAlign.Left;
      _entire_column.ColumnWidth = 40;

      _range = Sheet.Cells[_xls_row, TERMS_TERMINAL_TYPE];
      _entire_column = _range.EntireColumn;
      _range.Value = "Tipo de Terminal";
      _entire_column.HorizontalAlignment = HAlign.Left;
      _entire_column.ColumnWidth = 13;

      _range = Sheet.Cells[_xls_row, TERMS_TOTAL];
      _entire_column = _range.EntireColumn;
      _range.Value = "Total";
      _entire_column.HorizontalAlignment = HAlign.Right;
      _entire_column.ColumnWidth = 10;

      // Make these alignments after "Proveedor" alignments.
      _cell1 = Sheet.Cells[1, TERMS_PROVIDER];
      _cell2 = Sheet.Cells[2, TERMS_PROVIDER];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _entire_row = _range.EntireRow;

      _font.Size = 12;
      _font.Bold = true;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _entire_row.RowHeight = 15.75;

      _day_date = MonthDate;

      for (_idx_day = 1; _idx_day <= _last_day_of_the_month; _idx_day++)
      {
        _range = Sheet.Cells[_xls_row, TERMS_DAY_START + _idx_day - 1];
        _range.Value = _day_date;

        _day_date = _day_date.AddDays(1);
      }

      _cell1 = Sheet.Cells[_xls_row, TERMS_PROVIDER];
      _cell2 = Sheet.Cells[_xls_row, TERMS_TOTAL];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;

      _font.Bold = true;
      _font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE

      _cell1 = Sheet.Cells[_xls_row, TERMS_DAY_START];
      _cell2 = Sheet.Cells[_xls_row, TERMS_DAY_END];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;
      _entire_row = _range.EntireRow;

      _font.Bold = true;
      _font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE

      _interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE

      _range.NumberFormat = "dd mmm";
      _entire_column.HorizontalAlignment = HAlign.Right;
      _entire_column.ColumnWidth = 6;
      _entire_row.RowHeight = 17.25;
      _entire_row.VerticalAlignment = VAlign.Center;

      _first_data_row = _xls_row + 1;

      if (ConnectedTerms.Rows.Count > 0)
      {
        // Data Rows
        _xls_row++;

        _last_provider = (String)ConnectedTerms.Rows[0]["Provider"];
        _last_terminal_type = (Int16)ConnectedTerms.Rows[0]["TerminalType"];
        _first_time_in_row = true;

        foreach (DataRow _provider in ConnectedTerms.Rows)
        {
          // RCI & AJQ 25-JAN-2012: Compare Providers ignoring case, so "win" and "WIN" are considered the same provider.
          if (!_last_provider.Equals((String)_provider["Provider"], StringComparison.CurrentCultureIgnoreCase) ||
              (_last_terminal_type != (Int16)_provider["TerminalType"]))
          {
            _xls_row++;
            if (_xls_row % 2 == 0)
            {
              _cell1 = Sheet.Cells[_xls_row, 1 + _col_offset];
              _cell2 = Sheet.Cells[_xls_row, TERMS_DAY_END];
              _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
              _interior = _range.Interior;

              _interior.Color = SpreadsheetGear.Color.FromArgb(192, 192, 192); // EXCEL_COLOR_INDEX_GRAY

            }
            _first_time_in_row = true;
          }

          if (_xls_row > m_excel_max_rows)
          {
            break;
          }

          if (_first_time_in_row)
          {
            // Provider
            _range = Sheet.Cells[_xls_row, TERMS_PROVIDER];
            _range.Value = (String)_provider["Provider"];

            // Terminal Type
            _range = Sheet.Cells[_xls_row, TERMS_TERMINAL_TYPE];
            switch ((TerminalTypes)((Int16)_provider["TerminalType"]))
            {
              case TerminalTypes.WIN:
                _range.Value = Resource.String("STR_MAILING_TERMINAL_TYPES_01");
                break;

              case TerminalTypes.T3GS:
                _range.Value = Resource.String("STR_MAILING_TERMINAL_TYPES_03");
                break;

              case TerminalTypes.SAS_HOST:
                _range.Value = Resource.String("STR_MAILING_TERMINAL_TYPES_05");
                break;

              default:
                _range.Value = Resource.String("STR_MAILING_TERMINAL_TYPES_00");
                break;
            }

            _range = Sheet.Cells[_xls_row, TERMS_TOTAL];
            _range.Value = "=SUM(" + ColName(TERMS_DAY_START + 1) + (_xls_row + 1) + ":" + ColName(TERMS_DAY_END + 1) + (_xls_row + 1) + ")";
            _range.NumberFormat = m_number_format;

            _cell1 = Sheet.Cells[_xls_row, TERMS_DAY_START];
            _cell2 = Sheet.Cells[_xls_row, TERMS_DAY_START + LastDay - 1];
            _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
            _range.Value = 0;
            _range.NumberFormat = m_number_format;

            _first_time_in_row = false;
          }

          _idx_day = TERMS_DAY_START + ((DateTime)_provider["Date"]).Day - 1;
          _range = Sheet.Cells[_xls_row, _idx_day];
          _range.Value = (Int32)_provider["NumTerminals"];

          _last_provider = (String)_provider["Provider"];
          _last_terminal_type = (Int16)_provider["TerminalType"];
        }
      }

      _last_data_row = _xls_row;

      if (_last_data_row >= _first_data_row)
      {
        _cell1 = Sheet.Cells[_first_data_row, TERMS_PROVIDER];
        _cell2 = Sheet.Cells[_last_data_row, TERMS_PROVIDER];
        _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
        _range.EntireRow.RowHeight = 14.25;
        _range.EntireRow.VerticalAlignment = VAlign.Center;

      }

      // Totals
      _xls_row++;

      _range = Sheet.Cells[_xls_row, TERMS_PROVIDER];
      _range.Value = "TOTAL:";

      _cell1 = Sheet.Cells[_xls_row, TERMS_DAY_START];
      _cell2 = Sheet.Cells[_xls_row, TERMS_DAY_START + LastDay - 1];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];

      _range.Value = 0;
      _range.NumberFormat = m_number_format;

      if (_last_data_row >= _first_data_row)
      {
        _range = Sheet.Cells[_xls_row, TERMS_TOTAL];
        _range.Value = "=SUM(" + ColName(TERMS_TOTAL + 1) + (_first_data_row + 1) + ":" + ColName(TERMS_TOTAL + 1) + (_last_data_row + 1) + ")";
        _range.NumberFormat = m_number_format;

        for (_idx_day = 1; _idx_day <= LastDay; _idx_day++)
        {
          _idx_col = TERMS_DAY_START + _idx_day - 1;
          _range = Sheet.Cells[_xls_row, _idx_col];
          _range.Value = "=SUM(" + ColName(_idx_col + 1) + (_first_data_row + 1) + ":" + ColName(_idx_col + 1) + (_last_data_row + 1) + ")";
          _range.NumberFormat = m_number_format;
        }
      }

      _cell1 = Sheet.Cells[_xls_row, TERMS_PROVIDER];
      _cell2 = Sheet.Cells[_xls_row, TERMS_TOTAL];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;

      _font.Bold = true;
      _font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE

      _cell1 = Sheet.Range[_xls_row, TERMS_DAY_START];
      _cell2 = Sheet.Range[_xls_row, TERMS_DAY_END];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_row = _range.EntireRow;

      _font.Bold = true;
      _font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
      _entire_row.RowHeight = 17.25;
      _entire_row.VerticalAlignment = VAlign.Center;

      _cell1 = Sheet.Range[TERMS_HEADER_ROW, TERMS_PROVIDER];
      _cell2 = Sheet.Range[_xls_row, TERMS_TOTAL];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];

      _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;
      _range.Borders[BordersIndex.EdgeBottom].LineStyle = LineStyle.Continous;
      _range.Borders[BordersIndex.EdgeLeft].LineStyle = LineStyle.Continous;
      _range.Borders[BordersIndex.EdgeRight].LineStyle = LineStyle.Continous;

      _cell1 = Sheet.Range[TERMS_HEADER_ROW, TERMS_DAY_START];
      _cell2 = Sheet.Range[_xls_row, TERMS_DAY_END];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];

      _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;
      _range.Borders[BordersIndex.EdgeBottom].LineStyle = LineStyle.Continous;
      _range.Borders[BordersIndex.EdgeLeft].LineStyle = LineStyle.Continous;
      _range.Borders[BordersIndex.EdgeRight].LineStyle = LineStyle.Continous;
      _range.Borders[BordersIndex.InsideVertical].LineStyle = LineStyle.Continous;

      if (_last_data_row >= _first_data_row)
      {
        // Freeze first rows as they are the header.
        _range = Sheet.Range[_first_data_row, TERMS_DAY_START];
        _range.Select();

        Sheet.WindowInfo.ScrollRow = 0;
        Sheet.WindowInfo.SplitRows = _range.Row;

        Sheet.WindowInfo.ScrollColumn = 0;
        Sheet.WindowInfo.SplitColumns = _range.Column;

        Sheet.WindowInfo.FreezePanes = true;
      }

    } // FillSheetConnectedTerminals

    //EOR 19-MAY-2016
    //------------------------------------------------------------------------------
    // PURPOSE : Fill an Excel sheet with the Actitivy in Tables for the month.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DateTime MonthDate
    //          - Int32 LastDay
    //          - DataTable ConnectedTerms
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillSheetActivityTables(IWorksheet Sheet,
                                                    DateTime MonthDate,
                                                    Int32 LastDay,
                                                    DataTable ActivityTables,
                                                    Dictionary<String, String> Params)
    {
      IRange _cells;
      IRange _range;
      IFont _font;
      Int32 _xls_row;
      Int32 _xls_row_initial;
      Int32 _xls_row_terminal;
      Int32 _xls_col;
      Int32 _xls_col_initial;
      Int32 _last_day_of_the_month;
      Int32 _sql_column;
      DateTime _initial_day_of_month;

      _last_day_of_the_month = MonthDate.AddMonths(1).AddDays(-1).Day;

      Sheet.Name = Resource.String("STR_VOUCHER_GAMING_TABLES").ToLower() + " " + MonthDate.ToString("MMMM yyyy");
      _cells = Sheet.Cells;
      _font = _cells.Font;
      _font.Size = 9;
      _font.Name = "Calibri";

      AddParamsToSheetOnLeft(Sheet, Params);

      _xls_row = 1;
      _xls_col = 3;
      _range = Sheet.Cells[_xls_row, _xls_col];
      _range.Value = ActivityTables.TableName.Substring(16, ActivityTables.TableName.Length - 16).Trim();
      _range.Font.Bold = true;
      _range.Font.Size = 12;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _range.RowHeight = 15.75;
      _range.EntireColumn.ColumnWidth = 30;

      _xls_row++;
      _range = Sheet.Cells[_xls_row, _xls_col];
      _range.NumberFormat = "MMMM yyyy";
      _range.Value = MonthDate;
      _range.Font.Bold = true;
      _range.Font.Size = 12;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _range.RowHeight = 15.75;
      _range.EntireColumn.ColumnWidth = 30;

      // Column Headers
      _xls_row++;
      _range = Sheet.Cells[_xls_row + 1, _xls_col];
      _range.Value = Resource.String("STR_UC_GAMING_TABLE_FILTER_TYPE_COLUMN_NAME");
      _range.Font.Bold = true;
      _range.Font.Size = 9;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _range.Interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE
      _range.EntireColumn.ColumnWidth = 30;
      _range.Borders.LineStyle = LineStyle.Continous;

      _xls_col++;
      _range = Sheet.Cells[_xls_row + 1, _xls_col];
      _range.Value = Resource.String("STR_GTPS_IMPORT_TABLE_NAME");
      _range.Font.Bold = true;
      _range.Font.Size = 9;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _range.Interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE
      _range.EntireColumn.ColumnWidth = 30;
      _range.Borders.LineStyle = LineStyle.Continous;

      _xls_col++;
      _range = Sheet.Cells[_xls_row + 1, _xls_col];
      _range.Value = Resource.String("STR_MONTH");
      _range.Font.Bold = true;
      _range.Font.Size = 9;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _range.Interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE
      _range.Borders.LineStyle = LineStyle.Continous;



      _xls_col++;
      _range = Sheet.Cells[_xls_row, _xls_col, _xls_row, _xls_col + _last_day_of_the_month];
      _range.MergeCells = true;
      _range.Value = Resource.String("STR_ENABLED_TEXT");
      _range.Font.Bold = true;
      _range.Font.Size = 9;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _range.Interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
      _range.Borders.LineStyle = LineStyle.Continous;

      _xls_row++;
      _range = Sheet.Cells[_xls_row, _xls_col];
      _range.NumberFormat = m_number_format;
      _range.Value = Resource.String("STR_DAYS");
      _range.Font.Bold = true;
      _range.Font.Size = 9;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _range.Interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
      _range.Borders.LineStyle = LineStyle.Continous;

      _initial_day_of_month = new DateTime(MonthDate.Year, MonthDate.Month, 1);

      for (Int32 _xls_col_day = 1; _xls_col_day <= _last_day_of_the_month; _xls_col_day++)
      {
        _xls_col++;
        _range = Sheet.Cells[_xls_row, _xls_col];
        _range.NumberFormat = "dd MMM";
        _range.Value = _initial_day_of_month;
        _range.Font.Bold = true;
        _range.Font.Size = 9;
        _range.HorizontalAlignment = HAlign.Center;
        _range.VerticalAlignment = VAlign.Center;
        _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
        _range.Interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
        _range.Borders.LineStyle = LineStyle.Continous;

        _initial_day_of_month = _initial_day_of_month.AddDays(1);
      }

      _xls_col++;
      _range = Sheet.Cells[_xls_row - 1, _xls_col, _xls_row - 1, _xls_col + _last_day_of_the_month];
      _range.MergeCells = true;
      _range.Value = Resource.String("STR_UC_CARD_IN_USE");
      _range.Font.Bold = true;
      _range.Font.Size = 9;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _range.Interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
      _range.Borders.LineStyle = LineStyle.Continous;

      _range = Sheet.Cells[_xls_row, _xls_col];
      _range.Value = Resource.String("STR_DAYS");
      _range.NumberFormat = m_number_format;
      _range.Font.Bold = true;
      _range.Font.Size = 9;
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;
      _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
      _range.Interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
      _range.Borders.LineStyle = LineStyle.Continous;

      _initial_day_of_month = new DateTime(MonthDate.Year, MonthDate.Month, 1);

      for (Int32 _xls_col_day = 1; _xls_col_day <= _last_day_of_the_month; _xls_col_day++)
      {
        _xls_col++;
        _range = Sheet.Cells[_xls_row, _xls_col];
        _range.NumberFormat = "dd MMM";
        _range.Value = _initial_day_of_month;
        _range.Font.Bold = true;
        _range.Font.Size = 9;
        _range.HorizontalAlignment = HAlign.Center;
        _range.VerticalAlignment = VAlign.Center;
        _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
        _range.Interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
        _range.Borders.LineStyle = LineStyle.Continous;

        _initial_day_of_month = _initial_day_of_month.AddDays(1);
      }


      _xls_row++;
      _xls_col_initial = _xls_col - _last_day_of_the_month - _last_day_of_the_month - 4;
      _xls_row_initial = _xls_row + 1;

      foreach (DataRow _dr_register in ActivityTables.Rows)
      {
        _xls_col = _xls_col_initial;

        _sql_column = 1;
        _range = Sheet.Cells[_xls_row, _xls_col];
        _range.Value = _dr_register[_sql_column];

        _sql_column++;
        _xls_col++;
        _range = Sheet.Cells[_xls_row, _xls_col];
        _range.Value = _dr_register[_sql_column];

        _sql_column++;
        _xls_col++;
        _range = Sheet.Cells[_xls_row, _xls_col];
        _range.NumberFormat = m_number_format;
        _range.VerticalAlignment = VAlign.Center;
        _range.HorizontalAlignment = HAlign.Center;
        _range.Value = _dr_register[_sql_column];

        _sql_column++;
        _xls_col++;
        _range = Sheet.Cells[_xls_row, _xls_col];
        _range.NumberFormat = m_number_format;
        _range.VerticalAlignment = VAlign.Center;
        _range.HorizontalAlignment = HAlign.Center;
        _range.Font.Bold = true;
        _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
        _range.Interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
        _range.Borders[BordersIndex.EdgeLeft].LineStyle = LineStyle.Continous;
        _range.Borders[BordersIndex.EdgeRight].LineStyle = LineStyle.Continous;
        _range.Value = _dr_register[_sql_column];

        for (Int32 _xls_col_day = 1; _xls_col_day <= 31; _xls_col_day++)
        {
          if (_xls_col_day <= _last_day_of_the_month)
          {
            _sql_column++;
            _xls_col++;
            _range = Sheet.Cells[_xls_row, _xls_col];
            _range.NumberFormat = m_number_format;
            _range.VerticalAlignment = VAlign.Center;
            _range.HorizontalAlignment = HAlign.Center;
            _range.Value = _dr_register[_sql_column];
          }
        }

        _sql_column++;
        _xls_col++;
        _range = Sheet.Cells[_xls_row, _xls_col];
        _range.NumberFormat = m_number_format;
        _range.VerticalAlignment = VAlign.Center;
        _range.HorizontalAlignment = HAlign.Center;
        _range.Font.Bold = true;
        _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
        _range.Interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
        _range.Borders[BordersIndex.EdgeLeft].LineStyle = LineStyle.Continous;
        _range.Borders[BordersIndex.EdgeRight].LineStyle = LineStyle.Continous;
        _range.Value = _dr_register[_sql_column];

        for (Int32 _xls_col_day = 1; _xls_col_day <= 31; _xls_col_day++)
        {

          if (_xls_col_day <= _last_day_of_the_month)
          {
            _sql_column++;
            _xls_col++;
            _range = Sheet.Cells[_xls_row, _xls_col];
            _range.NumberFormat = m_number_format;
            _range.VerticalAlignment = VAlign.Center;
            _range.HorizontalAlignment = HAlign.Center;
            _range.Value = _dr_register[_sql_column];
          }
        }

        _xls_row++;
      }

      _xls_row_terminal = _xls_row;

      if (ActivityTables.Rows.Count > 0)
      {
        _xls_col = 3;
        _range = Sheet.Cells[_xls_row, _xls_col, _xls_row, _xls_col + 1];
        _range.MergeCells = true;
        _range.Value = Resource.String("STR_VOUCHER_RECEPTION_TOTAL") + ":";
        _range.Font.Bold = true;
        _range.Font.Size = 9;
        _range.HorizontalAlignment = HAlign.Center;
        _range.VerticalAlignment = VAlign.Center;
        _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE
        _range.Interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE
        _range.EntireColumn.ColumnWidth = 30;
        _range.Borders.LineStyle = LineStyle.Continous;
        _xls_col++;

        for (Int32 _xls_col_day = 1; _xls_col_day <= (_last_day_of_the_month + _last_day_of_the_month + 3); _xls_col_day++)
        {
          _xls_col++;
          _range = Sheet.Cells[_xls_row, _xls_col];
          _range.NumberFormat = m_number_format;
          _range.Font.Bold = true;
          _range.Font.Size = 9;
          _range.HorizontalAlignment = HAlign.Center;
          _range.VerticalAlignment = VAlign.Center;
          _range.Font.Color = SpreadsheetGear.Color.FromArgb(255, 255, 255); // EXCEL_COLOR_INDEX_WHITE 

          if (_xls_col_day == 1)
          {
            _range.Interior.Color = SpreadsheetGear.Color.FromArgb(0, 51, 102); // EXCEL_COLOR_INDEX_DARK_BLUE
          }
          else
          {
            _range.Interior.Color = SpreadsheetGear.Color.FromArgb(79, 129, 189); // EXCEL_COLOR_INDEX_LIGHT_BLUE
          }

          _range.Borders.LineStyle = LineStyle.Continous;
          _range.Value = "=SUM(" + ColName(_xls_col + 1) + _xls_row_initial.ToString() + ":" + ColName(_xls_col + 1) + _xls_row_terminal.ToString() + ")";
        }
      }
      _range = Sheet.Range[5, 6];
      _range.Select();

      Sheet.WindowInfo.ScrollRow = 0;
      Sheet.WindowInfo.SplitRows = _range.Row;

      Sheet.WindowInfo.ScrollColumn = 0;
      Sheet.WindowInfo.SplitColumns = _range.Column;

      Sheet.WindowInfo.FreezePanes = true;


    } // FillSheetActivityTables

    //------------------------------------------------------------------------------
    // PURPOSE : Fill an Excel sheet with the terminals without activity.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DataTable TerminalsWithoutActivity
    //          - Int32 IndexStartRow
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillTerminalsWithoutActivity(IWorksheet Sheet, DataTable TerminalsWithoutActivity, Int32 IndexStartRow, Dictionary<String, String> Params)
    {
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IRange _entire_column;
      IFont _font;
      IInterior _interior;
      Int32 _xls_row;
      Int32 _xls_col;
      DataRow _row;
      String _sheet_name;
      Int32 _index;
      Int32 _col_offset;
      Int32 _max_column_width;
      Int32 _count_col_beg;
      Int32 _count_col_end;

      if (AddParamsToSheetOnLeft(Sheet, Params))
      {
        _col_offset = 3;
      }
      else
      {
        _col_offset = 1;
      }

      if (TerminalsWithoutActivity.TableName.Contains("_"))
      {
        _sheet_name = Resource.String("STR_TERMINALS_WITHOUT_ACTIVITY"); // Only for mailing 
      }
      else
      {
        _sheet_name = TerminalsWithoutActivity.TableName; // GUI sets it's name correctly 
      }

      if (IndexStartRow > 0)
      {
        _index = (IndexStartRow / m_excel_max_rows) + 1;
        _sheet_name += " (" + _index + ")";
      }
      Sheet.Name = _sheet_name;

      _xls_row = ROW_OFFSET;
      _xls_col = _col_offset;

      foreach (DataColumn _col in TerminalsWithoutActivity.Columns)
      {
        _range = Sheet.Cells[_xls_row, _xls_col];
        _range.Value = String.IsNullOrEmpty(_col.Caption) ? _col.ColumnName : _col.Caption;

        _xls_col++;
      }

      _range = Sheet.Cells[ROW_OFFSET - 3, _col_offset];
      _range.Value = _sheet_name;

      _cell1 = Sheet.Cells[ROW_OFFSET - 3, _col_offset];
      _cell2 = Sheet.Cells[ROW_OFFSET - 2, _xls_col - 1];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];

      MergeRange(ref _range);
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;

      _font = _range.Font;
      _font.Bold = true;
      _font.Size = 20;

      _xls_row++;

      for (Int32 _idx_row = IndexStartRow; _idx_row < IndexStartRow + m_excel_max_rows; _idx_row++)
      {
        if (_xls_row > m_excel_max_rows + ROW_OFFSET)
        {
          break;
        }
        if (_idx_row >= TerminalsWithoutActivity.Rows.Count)
        {
          break;
        }
        _row = TerminalsWithoutActivity.Rows[_idx_row];

        OnProgressBar(_idx_row, TerminalsWithoutActivity.Rows.Count);

        _xls_col = _col_offset;
        foreach (String _item in _row.ItemArray)
        {
          _range = Sheet.Cells[_xls_row, _xls_col];

          if (_xls_col == 8 + _col_offset - COL_OFFSET)
          {
            _range.HorizontalAlignment = HAlign.Right;
          }

          switch (_item.GetType().ToString())
          {
            case "System.Decimal":
              _range.Value = _item;
              _range.NumberFormat = m_currency_format;
              break;

            case "System.DateTime":
              _range.Value = _item;
              _range.NumberFormat = Format.DateTimeCustomFormatString(true);
              break;

            case "System.String":
              _range.NumberFormat = "@";
              _range.Value = _item;
              break;

            case "System.Integer":
              _range.NumberFormat = m_number_format;
              _range.Value = _item;
              break;

            default:
              _range.Value = _item;
              break;
          }

          _xls_col++;
        } // foreach _item

        _xls_row++;
      } // for _idx_row

      _cell1 = Sheet.Cells[ROW_OFFSET, _col_offset];
      _cell2 = Sheet.Cells[ROW_OFFSET, TerminalsWithoutActivity.Columns.Count + _col_offset - 1];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;

      _font.Bold = true;
      _interior.Color = SpreadsheetGear.Color.FromArgb(192, 192, 192); // EXCEL_COLOR_INDEX_GRAY
      _interior.Pattern = Pattern.Solid; // 1;     // xlSolid
      _interior.PatternColorIndex = -1; // -4105;  // xlAutomatic
      _range.HorizontalAlignment = HAlign.Center;
      _entire_column.AutoFit();

      _max_column_width = 100;
      _count_col_beg = _col_offset;
      _count_col_end = TerminalsWithoutActivity.Columns.Count + _col_offset - 1;

      // Increment the insufficient Autofit to all columns shown.
      for (Int32 _count_current_col = _count_col_beg; _count_current_col <= _count_col_end; _count_current_col++)
      {
        Sheet.Range[ROW_OFFSET + 2, _count_current_col].EntireColumn.ColumnWidth = Math.Min(Sheet.Range[ROW_OFFSET + 2, _count_current_col].EntireColumn.ColumnWidth * 1.1, _max_column_width);
      }

    } // FillTerminalsWithoutActivity

    //------------------------------------------------------------------------------
    // PURPOSE : Put the rows (from IndexStartRow to IndexStartRow + m_excel_max_rows - 1) of the DataTable to a Excel Worksheet.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DataTable Table
    //          - Int32 IndexStartRow
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillSheetCashierSessionDetails(IWorksheet Sheet, DataTable Table, Int32 IndexStartRow, Dictionary<String, String> Params)
    {
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IRange _entire_column;
      IFont _font;
      IInterior _interior;
      Int32 _xls_row;
      Int32 _xls_col;
      DataRow _row;
      String _sheet_name;
      Int32 _index;
      Decimal _aux;
      NumberFormatInfo _nfi;
      Int32 _col_offset;
      Int32 _row_offset;

      _nfi = Thread.CurrentThread.CurrentCulture.NumberFormat;
      _sheet_name = Table.TableName;
      if (IndexStartRow > 0)
      {
        _index = (IndexStartRow / m_excel_max_rows) + 1;
        _sheet_name += " (" + _index + ")";
      }

      _aux = 0;

      // this code add params on left and moves data 2 columns to right
      //if (AddParamsToSheet(Sheet, Params))
      //{
      //  _col_offset = COL_OFFSET + 2;
      //}
      //else
      //{
      //  _col_offset = COL_OFFSET;
      //}

      _col_offset = COL_OFFSET + 1;
      _xls_col = _col_offset;

      _row_offset = ROW_OFFSET;

      AddParamsToSheet(Sheet, Params, _col_offset + 1, ref _row_offset);
      _xls_row = _row_offset;

      foreach (DataColumn _col in Table.Columns)
      {
        _xls_col++;
      }

      _range = Sheet.Cells[ROW_OFFSET - 3, _col_offset];
      _range.Value = _sheet_name;

      _cell1 = Sheet.Cells[ROW_OFFSET - 3, _col_offset];
      _cell2 = Sheet.Cells[ROW_OFFSET - 2, _xls_col - 2];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];

      // Merge by row only. See that if is necessary set a range to each row.
      MergeRange(ref _range);
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;

      _font = _range.Font;
      _font.Bold = true;
      _font.Size = 20;

      for (Int32 _idx_row = IndexStartRow; _idx_row < IndexStartRow + m_excel_max_rows; _idx_row++)
      {
        Int32 _aux_type;

        if (_xls_row > m_excel_max_rows + _row_offset)
        {
          break;
        }
        if (_idx_row >= Table.Rows.Count)
        {
          break;
        }
        _row = Table.Rows[_idx_row];

        OnProgressBar(_idx_row, Table.Rows.Count);

        _xls_col = _col_offset;

        foreach (Object _item in _row.ItemArray)
        {
          if (_xls_col == 4 + _col_offset - COL_OFFSET)
          {
            continue;
          }

          _range = Sheet.Cells[_xls_row, _xls_col];

          //Quantity Column
          if (_xls_col == (3 + _col_offset - COL_OFFSET) && (IsNumeric(_item.ToString()) || CurrencyToDouble(_item.ToString(), out _aux)))
          {
            _range.HorizontalAlignment = HAlign.Right;
            if (_aux < 0)
            {
              _font = _range.Font;
              _font.Color = SpreadsheetGear.Color.FromArgb(255, 0, 0);  // EXCEL_COLOR_INDEX_RED
            }
          }

          //Value Column
          if (_xls_col == (4 + _col_offset - COL_OFFSET) && (IsNumeric(_item.ToString()) || CurrencyToDouble(_item.ToString(), out _aux)))
          {
            _range.HorizontalAlignment = HAlign.Right;
            if (IsNumeric(_item.ToString(), out _aux))
            {
              if (_aux < 0)
              {
                _font = _range.Font;
                _font.Color = SpreadsheetGear.Color.FromArgb(255, 0, 0);  // EXCEL_COLOR_INDEX_RED

              }
            }
            else if (CurrencyToDouble(_item.ToString(), out _aux))
            {
              if (_aux < 0)
              {
                _font = _range.Font;
                _font.Color = SpreadsheetGear.Color.FromArgb(255, 0, 0);  // EXCEL_COLOR_INDEX_RED
              }
            }
          }


          _aux_type = _row.IsNull(4) ? 0 : Convert.ToInt32(_row[4]);

          switch ((EnumCashierSessionDetailsType)_aux_type)
          {
            case EnumCashierSessionDetailsType.BOLD:
              _range.Font.Bold = true;
              _range.Font.Size = 12;

              break;

            case EnumCashierSessionDetailsType.TOPBORDER:
              _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;

              break;

            case EnumCashierSessionDetailsType.TOPBORDER_BOLD:
              if (_xls_col == 4 + _col_offset - COL_OFFSET)
              {
                _range.HorizontalAlignment = HAlign.Right;
              }

              _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;
              _range.Font.Bold = true;
              _range.Font.Size = 12;

              break;

            case EnumCashierSessionDetailsType.YELLOW:
              _range.Interior.Color = SpreadsheetGear.Color.FromArgb(255, 255, 0); // EXCEL_COLOR_INDEX_YELLOW
              _range.Interior.Pattern = Pattern.Solid;     // xlSolid
              _range.Interior.PatternColorIndex = -1; // -4105;  // xlAutomatic

              break;

            case EnumCashierSessionDetailsType.ALIGN_RIGHT:
              if (_xls_col == 4 + _col_offset - COL_OFFSET)
              {
                _range.HorizontalAlignment = HAlign.Right;
              }

              break;

            case EnumCashierSessionDetailsType.TOPBORDER_ALIGN_RIGHT:
              if (_xls_col == 4 + _col_offset - COL_OFFSET)
              {
                _range.HorizontalAlignment = HAlign.Right;
              }

              _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;

              break;

            case EnumCashierSessionDetailsType.BOLD_ALIGN_RIGHT:
              if (_xls_col == 4 + _col_offset - COL_OFFSET)
              {
                _range.HorizontalAlignment = HAlign.Right;
              }
              _range.Font.Bold = true;
              _range.Font.Size = 12;

              break;

            default:
              break;
          }

          //ECMA-334
          switch (_item.GetType().ToString())
          {
            case "System.Decimal":
              _range.Value = _item;
              _range.NumberFormat = m_currency_format;
              break;

            case "System.DateTime":
              _range.Value = _item;
              _range.NumberFormat = Format.DateTimeCustomFormatString(true);
              break;

            case "System.String":
              if (IsNumeric((String)_item, out _aux))
              {
                if (!String.IsNullOrEmpty(_nfi.NumberDecimalSeparator) && _aux.ToString().Contains(_nfi.NumberDecimalSeparator))
                {
                  _range.NumberFormat = m_decimal_format;
                }
                else
                {
                  _range.NumberFormat = m_number_format;
                }
                _range.Value = _aux;
              }
              else if (CurrencyToDouble((String)_item, out _aux))
              {
                _range.NumberFormat = m_currency_format;
                _range.Value = _aux;
              }
              else
              {
                _range.NumberFormat = "@";
                _range.Value = _item;
              }
              break;

            case "System.Integer":
              _range.NumberFormat = m_number_format;
              _range.Value = _item;
              break;

            default:
              _range.Value = _item;
              break;
          }

          _xls_col++;
        } // foreach _item

        _xls_row++;
      } // for _idx_row

        _cell1 = Sheet.Cells[_row_offset - 1, _col_offset];
      _cell2 = Sheet.Cells[_row_offset - 1, Table.Columns.Count + _col_offset - 2];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;

      _range.HorizontalAlignment = HAlign.Center;
      _entire_column.AutoFit();

      _cell1 = Sheet.Cells[_row_offset - 1, _col_offset];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell1.Row, _cell1.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;

      _entire_column.ColumnWidth = 2.00;

      //Colum for Quantitys, third column in Table.Columns.Count
      _cell1 = Sheet.Cells[_row_offset - 1, _col_offset + 2];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell1.Row, _cell1.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;

      _entire_column.AutoFit();

    } // FillSheetCashierSessionDetails

    //------------------------------------------------------------------------------
    // PURPOSE : Put the rows (from IndexStartRow to IndexStartRow + m_excel_max_rows - 1) of the DataTable to a Excel Worksheet.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DataTable Table
    //          - Int32 IndexStartRow
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void FillSheetCageSessionReport(IWorksheet Sheet, DataTable Table, Int32 IndexStartRow, Dictionary<String, String> Params)
    {
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IRange _entire_column;
      IFont _font;
      IInterior _interior;
      Int32 _xls_row;
      Int32 _xls_col;
      DataRow _row;
      String _sheet_name;
      Int32 _index;
      Decimal _aux;
      NumberFormatInfo _nfi;
      Int32 _col_offset;
      Int32 _row_offset;

      _nfi = Thread.CurrentThread.CurrentCulture.NumberFormat;
      _sheet_name = Table.TableName;
      if (IndexStartRow > 0)
      {
        _index = (IndexStartRow / m_excel_max_rows) + 1;
        _sheet_name += " (" + _index + ")";
      }

      _col_offset = COL_OFFSET;
      _xls_col = _col_offset;

      _row_offset = ROW_OFFSET;

      AddParamsToSheet(Sheet, Params, _col_offset + 1, ref _row_offset);
      _xls_row = _row_offset;

      foreach (DataColumn _col in Table.Columns)
      {
        _xls_col++;
      }

      _range = Sheet.Cells[ROW_OFFSET - 3, _col_offset];
      _range.Value = _sheet_name;

      _cell1 = Sheet.Cells[ROW_OFFSET - 3, _col_offset];
      _cell2 = Sheet.Cells[ROW_OFFSET - 2, _xls_col - 2];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];

      // Merge by row only. See that if is necessary set a range to each row.
      MergeRange(ref _range);
      _range.HorizontalAlignment = HAlign.Center;
      _range.VerticalAlignment = VAlign.Center;

      _font = _range.Font;
      _font.Bold = true;
      _font.Size = 20;

      for (Int32 _idx_row = IndexStartRow; _idx_row < IndexStartRow + m_excel_max_rows; _idx_row++)
      {
        if (_xls_row > m_excel_max_rows + _row_offset)
        {
          break;
        }
        if (_idx_row >= Table.Rows.Count)
        {
          break;
        }
        _row = Table.Rows[_idx_row];

        OnProgressBar(_idx_row, Table.Rows.Count);

        _xls_col = _col_offset;
        foreach (Object _item in _row.ItemArray)
        {
          if (_xls_col == 8 + _col_offset - COL_OFFSET)
          {
            continue;
          }

          _range = Sheet.Cells[_xls_row, _xls_col];

          if (_xls_col == (6 + _col_offset - COL_OFFSET) && (IsNumeric(_item.ToString()) || CurrencyToDouble(_item.ToString(), out _aux)))
          {
            _range.HorizontalAlignment = HAlign.Right;

            if (IsNumeric(_item.ToString(), out _aux))
            {
              if (_aux < 0)
              {
                _font = _range.Font;
                _font.Color = SpreadsheetGear.Color.FromArgb(255, 0, 0);  // EXCEL_COLOR_INDEX_RED

              }
            }
            else if (CurrencyToDouble(_item.ToString(), out _aux))
            {
              if (_aux < 0)
              {
                _font = _range.Font;
                _font.Color = SpreadsheetGear.Color.FromArgb(255, 0, 0);  // EXCEL_COLOR_INDEX_RED
              }
            }
          }

          switch ((EnumCashierSessionDetailsType)Convert.ToInt32(_row[8]))
          {
            case EnumCashierSessionDetailsType.BOLD:
              _range.Font.Bold = true;
              _range.Font.Size = 12;

              break;

            case EnumCashierSessionDetailsType.TOPBORDER:
              _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;

              break;

            case EnumCashierSessionDetailsType.TOPBORDER_BOLD:
              if (_xls_col == 6 + _col_offset - COL_OFFSET)
              {
                _range.HorizontalAlignment = HAlign.Right;
              }

              _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;
              _range.Font.Bold = true;
              _range.Font.Size = 12;

              break;

            case EnumCashierSessionDetailsType.YELLOW:
              _range.Interior.Color = SpreadsheetGear.Color.FromArgb(255, 255, 0); // EXCEL_COLOR_INDEX_YELLOW
              _range.Interior.Pattern = Pattern.Solid;     // xlSolid
              _range.Interior.PatternColorIndex = -1; // -4105;  // xlAutomatic

              break;

            case EnumCashierSessionDetailsType.ALIGN_RIGHT:
              if (_xls_col == 6 + _col_offset - COL_OFFSET)
              {
                _range.HorizontalAlignment = HAlign.Right;
              }

              break;

            case EnumCashierSessionDetailsType.TOPBORDER_ALIGN_RIGHT:
              if (_xls_col == 6 + _col_offset - COL_OFFSET)
              {
                _range.HorizontalAlignment = HAlign.Right;
              }

              _range.Borders[BordersIndex.EdgeTop].LineStyle = LineStyle.Continous;

              break;

            case EnumCashierSessionDetailsType.BOLD_ALIGN_RIGHT:
              if (_xls_col == 6 + _col_offset - COL_OFFSET)
              {
                _range.HorizontalAlignment = HAlign.Right;
              }
              _range.Font.Bold = true;
              _range.Font.Size = 12;

              break;

            default:
              break;
          }

          //ECMA-334
          switch (_item.GetType().ToString())
          {
            case "System.Decimal":
              _range.Value = _item;
              _range.NumberFormat = m_currency_format;
              break;

            case "System.DateTime":
              _range.Value = _item;
              _range.NumberFormat = Format.DateTimeCustomFormatString(true);
              break;

            case "System.String":
              if (IsNumeric((String)_item, out _aux))
              {
                if (!String.IsNullOrEmpty(_nfi.NumberDecimalSeparator) && _aux.ToString().Contains(_nfi.NumberDecimalSeparator))
                {
                  _range.NumberFormat = m_decimal_format;
                }
                else
                {
                  _range.NumberFormat = m_number_format;
                }
                _range.Value = _aux;
              }
              else if (CurrencyToDouble((String)_item, out _aux))
              {
                _range.NumberFormat = m_currency_format;
                _range.Value = _aux;
              }
              else
              {
                _range.NumberFormat = "@";
                _range.Value = _item;
              }
              break;

            case "System.Integer":
              _range.NumberFormat = m_number_format;
              _range.Value = _item;
              break;

            default:
              _range.Value = _item;
              break;
          }

          _xls_col++;
        } // foreach _item

        _xls_row++;
      } // for _idx_row

      _cell1 = Sheet.Cells[_row_offset - 1, _col_offset];
      _cell2 = Sheet.Cells[_row_offset - 1, Table.Columns.Count + _col_offset - 1];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;

      _range.HorizontalAlignment = HAlign.Center;
      _entire_column.AutoFit();

      _cell1 = Sheet.Cells[_row_offset - 1, _col_offset];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell1.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;

      _entire_column.ColumnWidth = 2.00;

    } // FillSheetCageSessionReport

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate NumberFormat for numbers, currency and percentages.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void CalculateNumberFormats(bool IncludeCurrencySimbol = true)
    {
      NumberFormatInfo _nfi;
      NumberFormatInfo _nfi_eng;
      string _currencySymbol;

      _nfi = Thread.CurrentThread.CurrentCulture.NumberFormat;
      _nfi_eng = new CultureInfo("en-US", true).NumberFormat;

      m_number_format = "#" + _nfi_eng.NumberGroupSeparator + "##0";
      m_decimal_format = "#" + _nfi_eng.NumberGroupSeparator + "##0" + _nfi_eng.NumberDecimalSeparator + "00";

      m_percent_format = "#" + _nfi_eng.PercentGroupSeparator + "##0" + _nfi_eng.PercentDecimalSeparator + "00";
      switch (_nfi.PercentPositivePattern)
      {
        case 0:
          m_percent_format = m_percent_format + " " + _nfi.PercentSymbol;
          break;
        case 1:
          m_percent_format = m_percent_format + _nfi.PercentSymbol;
          break;
        case 2:
          m_percent_format = _nfi.PercentSymbol + m_percent_format;
          break;
        case 3:
          m_percent_format = _nfi.PercentSymbol + " " + m_percent_format;
          break;
        default:
          break;
      }

      _currencySymbol = (IncludeCurrencySimbol ? _nfi.CurrencySymbol : string.Empty);
      m_currency_format = "#" + _nfi_eng.CurrencyGroupSeparator + "##0" + _nfi_eng.CurrencyDecimalSeparator + "00";

      switch (_nfi.CurrencyPositivePattern)
      {
        case 0:
          m_currency_format = "\"" + _currencySymbol + "\"" + m_currency_format;
          break;
        case 1:
          m_currency_format = m_currency_format + "\"" + _currencySymbol + "\"";
          break;
        case 2:
          m_currency_format = "\"" + _currencySymbol + "\"" + " " + m_currency_format;
          break;
        case 3:
          m_currency_format = m_currency_format + " " + "\"" + _currencySymbol + "\"";
          break;
        default:
          break;
      }

      m_currency_simbol = _currencySymbol;
      m_currency_separator = _nfi.CurrencyGroupSeparator;
      m_percent_simbol = _nfi.PercentSymbol;
      m_number_separator = _nfi.NumberGroupSeparator;
      m_percent_separator = _nfi.PercentGroupSeparator;

    } // CalculateNumberFormats       

    //------------------------------------------------------------------------------
    // PURPOSE : Call DataTableToSheet without IncludeHeaderAndFormat
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DataTable Table
    //          - Int32 IndexStartRow
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void DataTableToSheet(IWorksheet Sheet, DataTable Table, Int32 IndexStartRow, Dictionary<String, String> Params)
    {
      DataTableToSheet(Sheet, Table, IndexStartRow, Params, true);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Put the rows (from IndexStartRow to IndexStartRow + m_excel_max_rows - 1) of the DataTable to a Excel Worksheet.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - DataTable Table
    //          - Int32 IndexStartRow
    //          - Boolean IncludeHeaderAndFormat
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void DataTableToSheet(IWorksheet Sheet, DataTable Table, Int32 IndexStartRow, Dictionary<String, String> Params, Boolean IncludeHeaderAndFormat)
    {
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IRange _entire_column;
      IFont _font;
      IInterior _interior;
      Int32 _xls_row;
      Int32 _xls_col;
      DataRow _row;
      Decimal _aux;
      String _sheet_name;
      Int32 _index;
      NumberFormatInfo _nfi;
      Int32 _col_offset;
      Int32 _row_offset;
      Int32 _max_column_width;
      Int32 _count_col_beg;
      Int32 _count_col_end;

      _nfi = Thread.CurrentThread.CurrentCulture.NumberFormat;
      _sheet_name = Table.TableName;

      if (IndexStartRow > 0)
      {
        _index = (IndexStartRow / m_excel_max_rows) + 1;
        _sheet_name += " (" + _index + ")";
      }

      if (_sheet_name.Length > 30)
      {
        Int32 _cut_position;
        _cut_position = _sheet_name.LastIndexOf(" ", 27);
        if (_cut_position >= 0)
        {
          Sheet.Name = _sheet_name.Substring(0, _cut_position) + " ...";
        }
        else
        {
          Sheet.Name = _sheet_name.Substring(0, 27) + " ...";
        }
      }
      else
      {
        Sheet.Name = _sheet_name;
      }
      _xls_row = ROW_OFFSET - (IncludeHeaderAndFormat ? 0 : 3);
      _row_offset = _xls_row;

      _xls_col = COL_OFFSET;
      _col_offset = _xls_col;

      if (AddParamsToSheet(Sheet, Params, COL_OFFSET, ref _row_offset))
      {
        _xls_row = _row_offset;
      }

      foreach (DataColumn _col in Table.Columns)
      {
        _range = Sheet.Cells[_xls_row, _xls_col];
        _range.Value = String.IsNullOrEmpty(_col.Caption) ? _col.ColumnName : _col.Caption;

        _xls_col++;
      }

      if (IncludeHeaderAndFormat)
      {
        _range = Sheet.Cells[ROW_OFFSET - 3, _col_offset];
        _range.Value = _sheet_name;

        _cell1 = Sheet.Cells[ROW_OFFSET - 3, _col_offset];
        _cell2 = Sheet.Cells[ROW_OFFSET - 2, _xls_col - 1];
        _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];

        MergeRange(ref _range);
        _range.HorizontalAlignment = HAlign.Center;
        _range.HorizontalAlignment = HAlign.Center;

        _font = _range.Font;
        _font.Bold = true;
        _font.Size = 26;
      }

      _xls_row++;

      _range = Sheet.Cells[_xls_row, _col_offset];
      _range.NumberFormat = "@";
      _range.Value = Resource.String("STR_NOT_FOUND_DATA_IN_INDICATED_FILTERS");

      for (Int32 _idx_row = IndexStartRow; _idx_row < IndexStartRow + m_excel_max_rows; _idx_row++)
      {
        if (_xls_row > m_excel_max_rows + ROW_OFFSET - (IncludeHeaderAndFormat ? 0 : 4))
        {
          break;
        }

        try
        {
          _row = Table.Rows[_idx_row];
        }
        catch
        {
          break;
        }

        OnProgressBar(_idx_row, Table.Rows.Count);

        _xls_col = _col_offset;
        foreach (Object _item in _row.ItemArray)
        {
          _range = Sheet.Cells[_xls_row, _xls_col];

          if (IncludeHeaderAndFormat)
          {
            //ECMA-334
            switch (_item.GetType().ToString())
            {
              case "System.Decimal":
                _range.Value = _item;
                _range.NumberFormat = m_currency_format;
                break;

              case "System.DateTime":
                _range.Value = _item;
                _range.NumberFormat = Format.DateTimeCustomFormatString(true);
                break;

              case "System.String":
                if (IsCardNumber((String)_item))
                {
                  _range.NumberFormat = "@";
                  _range.Value = _item;
                }
                else if (IsNumeric((String)_item, out _aux))
                {
                  if (_aux.ToString().Contains(_nfi.NumberDecimalSeparator))
                  {
                    _range.NumberFormat = m_decimal_format;
                  }
                  else
                  {
                    _range.NumberFormat = m_number_format;
                  }
                  _range.Value = _aux;
                }
                else if (PercentToDouble((String)_item, out _aux))
                {
                  _range.NumberFormat = m_percent_format;
                  _range.Value = _aux;
                }
                else if (CurrencyToDouble((String)_item, out _aux))
                {
                  _range.NumberFormat = m_currency_format;
                  _range.Value = _aux;
                }
                else
                {
                  _range.NumberFormat = "@";
                  _range.Value = _item;
                }
                break;

              case "System.Integer":
              case "System.Int32":  //EOR 13-JUL-2017
              case "System.Int64":
                _range.NumberFormat = m_number_format;
                _range.Value = _item;
                break;

              default:
                _range.Value = _item;
                break;
            }
          }
          else
          {
            _range.Value = _item;
          }

          _xls_col++;
        } // foreach _item

        _xls_row++;
      } // for _idx_row

      _max_column_width = 100;
      _count_col_beg = _col_offset;
      _count_col_end = Table.Columns.Count + _col_offset - 1;

      _cell1 = Sheet.Cells[_row_offset, _col_offset];
      _cell2 = Sheet.Cells[_row_offset, Table.Columns.Count + _col_offset - 1];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _font = _range.Font;
      _interior = _range.Interior;
      _entire_column = _range.EntireColumn;

      _font.Bold = true;
      _interior.Color = SpreadsheetGear.Color.FromArgb(192, 192, 192); // EXCEL_COLOR_INDEX_GRAY
      _interior.Pattern = Pattern.Solid;  // 1   // xlSolid
      _interior.PatternColorIndex = -1; // -4105;  // xlAutomatic
      _range.HorizontalAlignment = HAlign.Center;
      _entire_column.AutoFit();

      // Increment the insufficient Autofit to all columns shown.
      for (Int32 _count_current_col = _count_col_beg; _count_current_col <= _count_col_end; _count_current_col++)
      {
        Sheet.Range[ROW_OFFSET - (IncludeHeaderAndFormat ? 0 : 4) + 2, _count_current_col].EntireColumn.ColumnWidth = Math.Min(Sheet.Range[ROW_OFFSET - (IncludeHeaderAndFormat ? 0 : 4) + 2, _count_current_col].EntireColumn.ColumnWidth * 1.1, _max_column_width);
      }

    } // DataTableToSheet

    //------------------------------------------------------------------------------
    // PURPOSE : Add an empty row in the Excel.Worksheet when there is no data for the specific CurrentDate.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - Int32 XlsRow
    //          - DateTime CurrentDate
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void SetEmptyRow(IWorksheet Sheet,
                                    Int32 XlsRow,
                                    DateTime CurrentDate)
    {
      IRange _cells;
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IInterior _interior;

      _cells = Sheet.Cells;

      if (XlsRow % 2 == (m_draw_spaces ? 1 : 0))
      {
        _cell1 = Sheet.Cells[XlsRow, GAMES_FIRST_COLUMN];
        _cell2 = Sheet.Cells[XlsRow, GAMES_LAST_COLUMN];
        _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
        _interior = _range.Interior;

        _interior.Color = SpreadsheetGear.Color.FromArgb(192, 192, 192); // EXCEL_COLOR_INDEX_GRAY

      }

      _range = Sheet.Cells[XlsRow, GAMES_DATE];
      _range.Value = CurrentDate;

      _cell1 = Sheet.Cells[XlsRow, GAMES_PLAYED_AMOUNT];
      _cell2 = Sheet.Cells[XlsRow, GAMES_LAST_COLUMN];
      _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
      _range.Value = 0;

      _range = Sheet.Cells[XlsRow, GAMES_PAYOUT_PCT];
      _range.Value = "";

      _range = Sheet.Cells[XlsRow, GAMES_REAL_PAYOUT_PCT];
      _range.Value = "";

      _range = Sheet.Cells[XlsRow, GAMES_WON_PCT];
      _range.Value = "";

      _range = Sheet.Cells[XlsRow, GAMES_AVERAGE_PLAY];
      _range.Value = "";

    } // SetEmptyRow

    //------------------------------------------------------------------------------
    // PURPOSE : Set Excel column indices based on if column spaces is configured.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void SetExcelColumnIndices(Int32 ColOffset)
    {

      if (m_draw_spaces)
      {
        GAMES_DATE = 0;
        GAMES_PLAYED_AMOUNT = 2;
        GAMES_WON_AMOUNT = 4;
        GAMES_PAYOUT_PCT = 6;
        GAMES_JACKPOT = 8;
        GAMES_CONTRIBUTION_PCT = 10;
        GAMES_CONTRIBUTION_AMOUNT = 12;
        GAMES_NETWIN = 14;
        GAMES_REAL_NETWIN = 16;
        GAMES_REAL_PAYOUT_PCT = 18;
        GAMES_PLAYS_COUNT = 20;
        GAMES_WON_COUNT = 22;
        GAMES_WON_PCT = 24;
        GAMES_AVERAGE_PLAY = 26;

        GAMES_FIRST_COLUMN = 0;
        GAMES_LAST_COLUMN = 27;

        // Don't include VLT indexes values inside LAST_COLUMN.
        GAMES_VLT = 28;
        GAMES_NW_VLT = 30;
      }
      else
      {
        GAMES_DATE = 0;
        GAMES_PLAYED_AMOUNT = 1;
        GAMES_WON_AMOUNT = 2;
        GAMES_PAYOUT_PCT = 3;
        GAMES_JACKPOT = 4;
        GAMES_CONTRIBUTION_PCT = 5;
        GAMES_CONTRIBUTION_AMOUNT = 6;
        GAMES_NETWIN = 7;
        GAMES_REAL_NETWIN = 8;
        GAMES_REAL_PAYOUT_PCT = 9;
        GAMES_PLAYS_COUNT = 10;
        GAMES_WON_COUNT = 11;
        GAMES_WON_PCT = 12;
        GAMES_AVERAGE_PLAY = 13;

        GAMES_FIRST_COLUMN = 0;
        GAMES_LAST_COLUMN = 13;

        // Don't include VLT indexes values inside LAST_COLUMN.
        GAMES_VLT = 14;
        GAMES_NW_VLT = 15;
      }

      if (ColOffset > 0)
      {
        GAMES_DATE += ColOffset;
        GAMES_PLAYED_AMOUNT += ColOffset;
        GAMES_WON_AMOUNT += ColOffset;
        GAMES_PAYOUT_PCT += ColOffset;
        GAMES_JACKPOT += ColOffset;
        GAMES_CONTRIBUTION_PCT += ColOffset;
        GAMES_CONTRIBUTION_AMOUNT += ColOffset;
        GAMES_NETWIN += ColOffset;
        GAMES_REAL_NETWIN += ColOffset;
        GAMES_REAL_PAYOUT_PCT += ColOffset;
        GAMES_PLAYS_COUNT += ColOffset;
        GAMES_WON_COUNT += ColOffset;
        GAMES_WON_PCT += ColOffset;
        GAMES_AVERAGE_PLAY += ColOffset;
        GAMES_FIRST_COLUMN += ColOffset;
        GAMES_LAST_COLUMN += ColOffset;
        GAMES_VLT += ColOffset;
        GAMES_NW_VLT += ColOffset;
      }
    } // SetExcelColumnIndices


    //------------------------------------------------------------------------------
    // PURPOSE : Returns Excel column name from numeric position.
    //           e.g.: col_no 27 returns "AA"
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 ColNum
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static string ColName(int ColumnNumber)
    {
      int _dividend;
      string _column_name;
      int _modulo;

      _column_name = "";
      _dividend = ColumnNumber;

      while (_dividend > 0)
      {
        _modulo = (_dividend - 1) % 26;
        _column_name = Convert.ToChar(65 + _modulo).ToString() + _column_name;
        _dividend = (int)((_dividend - _modulo) / 26);
      }

      return _column_name;
    } // ColName

    //------------------------------------------------------------------------------
    // PURPOSE : Release a COM object.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object Obj
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void ReleaseComObject(object Obj)
    {
      Int32 _num_ref;
      try
      {
        if (Obj != null)
        {
          do
          {
            _num_ref = System.Runtime.InteropServices.Marshal.ReleaseComObject(Obj);
          } while (_num_ref > 0);
        }
        Obj = null;
      }
      catch
      {
        Obj = null;
      }
    } // ReleaseComObject

    //------------------------------------------------------------------------------
    // PURPOSE : Says if parameter is Numeric.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: Expression
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True if is numeric.
    //
    private static Boolean IsNumeric(String Expression)
    {
      Decimal _aux;

      return Decimal.TryParse(Expression, out _aux);
    } // IsNumeric

    //------------------------------------------------------------------------------
    // PURPOSE : Says if parameter is Numeric.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: Expression
    //
    //      - OUTPUT :
    //          - Decimal: Number
    //
    // RETURNS :
    //      - Boolean: True if is numeric.
    //
    private static Boolean IsNumeric(String Expression, out Decimal Number)
    {
      return Decimal.TryParse(Expression, out Number);
    } // IsNumeric

    //------------------------------------------------------------------------------
    // PURPOSE : Says if parameter is Numeric.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: Expression
    //
    //      - OUTPUT :
    //          - Decimal: Number
    //
    // RETURNS :
    //      - Boolean: True if is numeric.
    //
    private static Boolean IsCardNumber(String Expression)
    {

      if (Expression.Length != 20)
        return false;

      Regex _regexp;
      _regexp = new Regex("^[0-9]+$");
      if (_regexp.IsMatch(Expression))
      {
        return true;
      }
      else
      {
        return false;
      }

    } // IsCardNumber

    //------------------------------------------------------------------------------
    // PURPOSE : Says if parameter is Percent.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: Expression
    //
    //      - OUTPUT :
    //          - Decimal: Percent
    //
    // RETURNS :
    //      - Boolean: True if is Percent.
    //
    private static Boolean PercentToDouble(String Expression, out Decimal Percent)
    {
      String _aux;

      _aux = Expression;
      Percent = 0;

      if (!_aux.ToString().Contains(m_percent_simbol))
        return false;

      _aux = _aux.Replace(m_percent_simbol, "");
      _aux = _aux.Replace(m_percent_separator, "");

      if (IsNumeric(_aux))
      {
        Percent = Decimal.Parse(_aux);
        return true;
      }
      else
      {
        return false;
      }

    } // PercentToDouble

    //------------------------------------------------------------------------------
    // PURPOSE : Says if parameter is Currency.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: Expression
    //
    //      - OUTPUT :
    //          - Decimal: Currency
    //
    // RETURNS :
    //      - Boolean: True if is Currency.
    //
    private static Boolean CurrencyToDouble(String Expression, out Decimal Currency)
    {
      String _aux;

      _aux = Expression;
      Currency = 0;

      if (String.IsNullOrEmpty(m_currency_simbol))
      {
        return false;
      }
      if (!_aux.ToString().Contains(m_currency_simbol))
      {
        return false;
      }

      _aux = _aux.Replace(m_currency_simbol, "");
      _aux = _aux.Replace(m_currency_separator, "");

      if (IsNumeric(_aux))
      {
        Currency = Decimal.Parse(_aux);
        return true;
      }
      else
      {
        return false;
      }

    } // CurrencyToDouble

    //------------------------------------------------------------------------------
    // PURPOSE : Add params to Excel Worksheet on Left
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - Dictionary<String, String> Params 
    //
    // RETURNS :
    //      - True: Params added correctly. False: No params to add.
    //
    private static Boolean AddParamsToSheetOnLeft(IWorksheet Sheet, Dictionary<String, String> Params)
    {
      Int32 _row_offset;

      _row_offset = ROW_OFFSET;

      return AddParamsToSheet(Sheet, Params, 0, ref _row_offset);

    } // AddParamsToSheetOnLeft

    //------------------------------------------------------------------------------
    // PURPOSE : Add params to Excel Worksheet
    //
    //  PARAMS :
    //      - INPUT :
    //          - Excel.Worksheet Sheet
    //          - Dictionary<String, String> Params 
    //          - Int32 ColOffset
    //          - byref - Int32 RowOfset - updated with next usable row index
    //          
    // RETURNS :
    //      - True: Params added correctly. False: No params to add.
    //
    private static Boolean AddParamsToSheet(IWorksheet Sheet,
                                            Dictionary<String, String> Params,
                                            Int32 ColOffset,
                                            ref Int32 RowOffset)
    {
      IRange _cells;
      IRange _cell1;
      IRange _cell2;
      IRange _range;
      IRange _entire_column;
      IFont _font;
      Int32 _param_index;

      _param_index = 2;

      if (Params == null || Params.Count == 0)
      {
        return false;
      }

      _cells = Sheet.Cells;

      if (Params.Count > 0)
      {
        foreach (KeyValuePair<String, String> _param in Params)
        {
          if (_param.Key == "SITE")
          {
            _cell1 = Sheet.Cells[RowOffset, ColOffset];
            _cell1.NumberFormat = "@";
            _cell1.Value = _param.Value;
            _font = _cell1.Font;
            _font.Bold = true;

            _cell2 = Sheet.Cells[RowOffset, ColOffset + 1];
            _range = Sheet.Range[_cell1.Row, _cell1.Column, _cell2.Row, _cell2.Column];
            MergeRange(ref _range);
            _range.HorizontalAlignment = HAlign.Left;

          }
          else
          {
            _range = Sheet.Cells[RowOffset + _param_index, ColOffset];
            if (_param.Key != "")
            {
              _range.Value = _param.Key + ":"; // param label 
            }
            _range = Sheet.Cells[RowOffset + _param_index, ColOffset + 1]; // param value 
            _range.NumberFormat = "@";
            _range.Value = _param.Value;
            _range.HorizontalAlignment = HAlign.Left;

            _param_index++;
          }
        } // end for each

        Int32 MAX_COLUMN_WIDTH;
        MAX_COLUMN_WIDTH = 100;

        _range = Sheet.Cells[1, ColOffset];
        _entire_column = _range.EntireColumn;
        _entire_column.AutoFit();
        _range.ColumnWidth = Math.Min(_range.ColumnWidth * 1.1, MAX_COLUMN_WIDTH);
        //Book.Worksheets[0].Range["C:C"].ColumnWidth = Math.Min(Book.Worksheets[0].Range["C:C"].ColumnWidth * 1.1, MAX_COLUMN_WIDTH);

        _range = Sheet.Cells[1, ColOffset + 1];
        _entire_column = _range.EntireColumn;
        _entire_column.AutoFit();
        _range.ColumnWidth = Math.Min(_range.ColumnWidth * 1.1, MAX_COLUMN_WIDTH);
        //Book.Worksheets[0].Range["D:D"].ColumnWidth = Math.Min(Book.Worksheets[0].Range["D:D"].ColumnWidth * 1.1, MAX_COLUMN_WIDTH);
      }
      // intial rowoffset + params added + 1 blank at the end
      RowOffset = RowOffset + _param_index + 1;
      return true;
    } // addparamstosheet

    //------------------------------------------------------------------------------
    // PURPOSE : Merges the specified cells in the range
    //
    //  PARAMS :
    //      - INPUT :
    //          - Irange
    //          
    // RETURNS :
    //
    private static void MergeRange(ref IRange Range)
    {
      if (Range.CellCount > 1)
      {
        Range.Merge();
      }
    }
    #endregion

  } // ExcelConversion

} // WSI.Common
