//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:
// 
//   DESCRIPTION: Imports blacklist from excel documents.
//
//        AUTHOR: Scott Adamson  
// 
// CREATION DATE: 16-FEB-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-FEB-2016 SJA   First release
// 10-MAY-2016 SMN   PBI 12929: Visitas / Recepción: BlackList - Ampliar funcionalidad
// 06-JUN-2016 PDM   Recepción: GUI - Error al importar la lista de prohibidos. No aparece el detalle de los registros con error.
// 21-JUN-2016 PDM   PBI 14776: Visitas / Recepción: Blacklist - mejorar importación Excel
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using SpreadsheetGear;
using WSI.Common.Utilities.Extensions;

namespace WSI.Common
{
  public class ImportEventArgs : EventArgs
  {
    public int ProcessedErrors { get; set; }

    public int RowCount { get; set; }

    public int PendingRows { get; set; }

    public String Message { get; set; }

    public List<SqlException> ErrorsList { get; set; }

    public int ProcessedOk { get; set; }

    public TimeSpan Elapsed { get; set; }

    public List<ErrorDetail> ErrorsDetails { get; set; }

    public List<Warning> WarningList { get; set; }

    public ImportEventArgs()
    {
      ErrorsDetails = new List<ErrorDetail>();
      WarningList = new List<Warning>();
    }

    public override string ToString()
    {
      string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            Elapsed.Hours, Elapsed.Minutes, Elapsed.Seconds,
            Elapsed.Milliseconds / 10);
      return string.Format(Resource.String("STR_IMPORT_BLACKLIST_RESUME"), elapsedTime, RowCount, PendingRows,
        ProcessedOk, ProcessedErrors);
    }
  }

  public class ErrorDetail
  {
    public int RowNumber { get; set; }

    public string DescriptionError { get; set; }
  }

  public class Warning
  {
    public int RowNumber { get; set; }

    public string Message { get; set; }
  }

  /// <summary>
  /// Action delegate definition, required for enum transformation action string to int
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TResult"></typeparam>
  /// <param name="Arg"></param>
  /// <returns></returns>
  public delegate TResult Func<in T, out TResult>(T Arg);

  /// <summary>
  /// column definition type
  /// </summary>
  public class ColumnDefinition
  {
    public string ColumnName { get; set; }
    public string Alias { get; set; }
    public SqlDbType ColumnType { get; set; }
    public Func<string, int> EnumTranslation { get; set; }
    public bool Required { get; set; }
    public int MaxLength { get; set; }
    public object DefaultValue { get; set; }

  }

  /// <summary>
  /// column dictionary type
  /// </summary>
  public class ColumnDictionary : Dictionary<String, ColumnDefinition>
  {
    public new void Add(String Key, ColumnDefinition Value)
    {
      base.Add(Key.Trim().ToUpper(), Value);
    }
  }

  public abstract class ImportFromExcel
  {

    #region CONSTANTS

    public abstract String TableName { get; }

    #endregion

    #region members

    public abstract ColumnDictionary ColumnMatches { get; }

    #endregion

    #region events

    public delegate void RowUpdatedEvent(object Sender, ImportEventArgs E);

    public event RowUpdatedEvent OnRowUpdated;

    /// <summary>
    /// source excel file
    /// </summary>
    public string FilePath { get; set; }

    /// <summary>
    /// log file destination
    /// </summary>
    public string FileOut { get; set; }

    /// <summary>
    /// write to log file
    /// </summary>
    /// <param name="LogLine"></param>
    /// <returns></returns>
    public void WriteLog(string LogLine)
    {
      FileStream _fs_log;
      StreamWriter _sw_log;
      _fs_log = new FileStream(FileOut, FileMode.Append, FileAccess.Write);
      _sw_log = new StreamWriter(_fs_log);

      try
      {
        _sw_log.WriteLine("" + LogLine);
        _sw_log.Close();
        _fs_log.Close();
      }
      catch
      {
        _sw_log.Close();
        _fs_log.Close();
      }
    }

    # endregion



    /// <summary>
    /// Blacklist import routine, specify logfile
    /// </summary>
    /// <param name="LogFile"></param>
    protected ImportFromExcel(string LogFile)
      : this()
    {
      FileOut = LogFile;
    }

    /// <summary>
    /// Blacklist import routine
    /// </summary>
    protected ImportFromExcel()
    {
      FileOut = null;
    }



    /// <summary>
    /// main import routine
    /// </summary>
    /// <param name="FileName"></param>
    public void Import(string FileName = @"c:\Blacklist_Import.xlsx", int? Type = null)
    {
      if (FileName == null || !File.Exists(FileName))
      {
        return;
      }

      IWorkbook _excel_book;
      IWorksheet _excel_sheet;
      IRange _excel_range;
      int _excel_row_count;
      int _excel_column_count;
      int _current_row;
      ColumnDefinition[] _mapped_columns;
      string _sql_fields;
      string _sql_params;
      string _sql_where;
      bool _individual_action = false;
      ColumnDefinition _mapped_column;

      _excel_book = Factory.GetWorkbook(FileName);
      if (_excel_book == null)
        return;
      _excel_sheet = _excel_book.Worksheets[0];
      if (_excel_sheet == null)
        return;
      _excel_range = _excel_sheet.UsedRange.CurrentRegion;
      if (_excel_range == null)
        return;
      _current_row = 0;
      _excel_row_count = _excel_range.RowCount;
      _excel_column_count = _excel_range.ColumnCount;
      _mapped_columns = new ColumnDefinition[_excel_range.ColumnCount];
      _sql_fields = "";
      _sql_params = "";
      _sql_where = "";
      _mapped_column = null;

      for (int _current_column = 0; _current_column < _excel_column_count; _current_column++)
      {
        string _excel_column_name = _excel_range[_current_row, _current_column].Text.ToUpperInvariant();

        if (_excel_column_name.CompareTo(Resource.String("STR_FRM_BLACKLIST_IMPORT_EXCLUSION_ACTION")) == 0)
        {
          _individual_action = true;
          continue;
        }

        _mapped_column = ColumnMatches.ContainsKey(_excel_column_name)
          ? ColumnMatches[_excel_column_name]
          : null;

        _mapped_columns[_current_column] = _mapped_column;

        if (_mapped_column == null)
        {
          continue;
        }

        _sql_fields += _sql_fields.IsNullOrWhiteSpace() ? _mapped_column.ColumnName : "," + _mapped_column.ColumnName;
        _sql_params += _sql_params.IsNullOrWhiteSpace()
          ? "@" + _mapped_column.ColumnName
          : "," + "@" + _mapped_column.ColumnName;

        if (_mapped_column.Required)
        {
          _sql_where += _sql_where.IsNullOrWhiteSpace() ? _mapped_column.ColumnName + "=" : " AND " + _mapped_column.ColumnName + "=";
          _sql_where += " @" + _mapped_column.ColumnName;
        }

      }

      // Add list type
      if (Type != null)
      {
        _sql_fields += (_sql_fields.IsNullOrWhiteSpace() ? "" : ",") + "BLKF_ID_TYPE";
        _sql_params += (_sql_params.IsNullOrWhiteSpace() ? "" : ",") + Type;
      }

      if (!_individual_action)
      {
        ImportRowsNoAction(_excel_range, _mapped_columns, _sql_fields, _sql_params, Type);
      }
      else
      {
        ImportRowsByAction(_excel_range, _mapped_columns, _sql_fields, _sql_params, _sql_where, Type);
      }
    }

    /// <summary>
    /// Import excel without action
    /// </summary>
    /// <param name="Range"></param>
    /// <param name="MappedColumns"></param>
    /// <param name="SqlFields"></param>
    /// <param name="SqlParams"></param>
    /// <param name="Type"></param>
    private void ImportRowsNoAction(IRange Range, ColumnDefinition[] MappedColumns, string SqlFields, string SqlParams, int? Type = null)
    {
      DateTime _start_time;
      ImportEventArgs _response;
      int _excel_column_count;
      int _excel_row_count;
      int _current_row;
      string _error_message;
      string _sql_delete_all_statement;
      string _sql_insert_statement;
      ColumnDefinition _current_column_definition;
      object _val;
      object _cell_value;
      bool _cancel_operation;
      bool _error_row;

      _excel_column_count = Range.ColumnCount;
      _excel_row_count = Range.RowCount;
      _start_time = DateTime.Now;
      _response = new ImportEventArgs();
      _response.RowCount = Range.RowCount - 1;
      _response.PendingRows = Range.RowCount - 1;
      _response.ProcessedErrors = 0;
      _response.ProcessedOk = 0;
      _response.ErrorsList = new List<SqlException>();
      _error_message = "";
      _sql_delete_all_statement = "DELETE FROM {0} WHERE {1};";
      _sql_insert_statement = "INSERT INTO {0}({1})VALUES({2});";
      _sql_insert_statement = string.Format(_sql_insert_statement, TableName, SqlFields, SqlParams);
      _sql_delete_all_statement = string.Format(_sql_delete_all_statement, TableName, Type == null ? " 1=1 " : " ISNULL(BLKF_ID_TYPE, 0) = " + Type);
      _current_column_definition = null;
      _current_row = 0;
      _val = null;
      _cell_value = null;
      _cancel_operation = false;
      _error_row = false;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (OnRowUpdated != null)
          OnRowUpdated(this, _response); //Initial status

        using (SqlCommand _cmd_delete = new SqlCommand(_sql_delete_all_statement, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _cmd_delete.ExecuteNonQuery();
        }

        using (SqlCommand _cmd_insert = new SqlCommand(_sql_insert_statement, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          foreach (var _column in MappedColumns)
          {
            if (_column == null)
              continue;
            _cmd_insert.Parameters.Add(new SqlParameter(_column.ColumnName, _column.ColumnType));
          }

          for (_current_row = 1; _current_row < _excel_row_count; _current_row++)
          {

            _response.PendingRows--;

            for (int _current_column = 0; _current_column < _excel_column_count; _current_column++)
            {

              if (MappedColumns[_current_column] == null)
                continue;

              try
              {
                _cell_value = Range[_current_row, _current_column].Value;
                _current_column_definition = MappedColumns[_current_column];

                if (_cell_value == null && _current_column_definition.DefaultValue != null) _cell_value = _current_column_definition.DefaultValue;

                _val = GetValueFromCell(_current_column_definition, _cell_value);

                if (_val != null && _current_column_definition.ColumnType == SqlDbType.NVarChar)
                {
                  _val = _val.ToString().ToUpperInvariant();
                }

                _error_message = IsValid(_current_column_definition, _val);

                if (!String.IsNullOrEmpty(_error_message))
                {
                  _error_row = true;
                  _cancel_operation = true;
                  CancelImport(_response, _error_message, _current_row);
                  continue;
                }

                _cmd_insert.Parameters[MappedColumns[_current_column].ColumnName].Value = _val;

              }
              catch (Exception _exception)
              {
                _error_row = true;
                _cancel_operation = true;
                CancelImport(_response, _exception.Message, _current_row);
                continue;
              }

            }

            if (!_cancel_operation)
            {
              try
              {
                _cmd_insert.ExecuteNonQuery();
                _response.ProcessedOk++;
              }
              catch (SqlException _sqlex_exception)
              {
                _error_row = true;
                _cancel_operation = true;
                CancelImport(_response, _sqlex_exception.Message, _current_row);
              }
              catch (Exception _exception)
              {
                _error_row = true;
                _cancel_operation = true;
                CancelImport(_response, _exception.Message, _current_row);
              }
            }

            if (_error_row) _response.ProcessedErrors++;
            _response.Elapsed = DateTime.Now - _start_time;

            if (!string.IsNullOrEmpty(FileOut))
              WriteLog(_response.ToString());
            if (OnRowUpdated != null)
              OnRowUpdated(this, _response);

            _error_row = false;

          }
        }


        if (_cancel_operation)
          _db_trx.Rollback();
        else
          _db_trx.Commit();

        if (OnRowUpdated != null)
          OnRowUpdated(this, _response);

      }

    }

    /// <summary>
    /// Import excel with individuals action
    /// </summary>
    /// <param name="Range"></param>
    /// <param name="SqlFields"></param>
    /// <param name="SqlParams"></param>
    /// <param name="Type"></param>
    private void ImportRowsByAction(IRange Range, ColumnDefinition[] MappedColumns, string SqlFields, string SqlParams, string SqlWhere, int? Type = null)
    {
      DateTime _start_time;
      ImportEventArgs _response;
      int _excel_column_count = Range.ColumnCount;
      int _excel_row_count = Range.RowCount;
      int _current_row = 0;
      string _operation;
      object _cell_value_operation;
      SqlCommand _cmd_delete;
      SqlCommand _cmd_select;
      SqlCommand _cmd_insert;
      string _sql_delete_query;
      string _sql_select_query;
      string _sql_insert_query;
      string _error_message;
      object _result_query;
      Warning _warning;
      ColumnDefinition _current_column_definition;
      object _val;
      object _cell_value;
      bool _cancel_operation;
      bool _error_row;

      _error_message = "";
      _response = new ImportEventArgs();
      _response.RowCount = Range.RowCount - 1;
      _response.PendingRows = Range.RowCount - 1;
      _response.ProcessedErrors = 0;
      _response.ProcessedOk = 0;
      _start_time = DateTime.Now;
      _operation = "";
      _cell_value_operation = null;
      _cmd_delete = null;
      _cmd_insert = null;
      _cmd_select = null;
      _sql_delete_query = "DELETE FROM {0} WHERE {1};";
      _sql_delete_query = string.Format(_sql_delete_query, TableName, Type == null ? SqlWhere + " AND 1=1 " : SqlWhere + " AND ISNULL(BLKF_ID_TYPE, 0) = " + Type);
      _sql_select_query = "SELECT COUNT(*) FROM {0} WHERE {1}";
      _sql_select_query = string.Format(_sql_select_query, TableName, Type == null ? SqlWhere + " AND 1=1 " : SqlWhere + " AND ISNULL(BLKF_ID_TYPE, 0) = " + Type);
      _sql_insert_query = "INSERT INTO {0}({1})VALUES({2});";
      _sql_insert_query = string.Format(_sql_insert_query, TableName, SqlFields, SqlParams);
      _result_query = null;
      _val = null;
      _cell_value = null;
      _current_column_definition = null;
      _cancel_operation = false;
      _error_row = false;

      using (DB_TRX _db_trx = new DB_TRX())
      {

        if (OnRowUpdated != null)
          OnRowUpdated(this, _response); //Initial status

        for (_current_row = 1; _current_row < _excel_row_count; _current_row++)
        {
          _response.PendingRows--;

          for (int _current_column = 0; _current_column < _excel_column_count - 1; _current_column++)
          {

            //Verificar accion
            if (_current_column == 0)
            {
              _cell_value_operation = Range[_current_row, _excel_column_count - 1].Value;
              _operation = _cell_value_operation == null ? "" : _cell_value_operation.ToString().Trim().ToUpper();

              if (_operation.CompareTo("A") != 0 && _operation.CompareTo("B") != 0)
              {
                _response.Elapsed = DateTime.Now - _start_time;
                _cancel_operation = true;
                _error_row = true;
                CancelImport(_response, Resource.String("STR_FRM_BLACKLIST_IMPORT_NO_ACTION_DEFINED"), _current_row);
                continue;
              }
            }

            //Validations
            if (MappedColumns[_current_column] == null)
              continue;
            _current_column_definition = MappedColumns[_current_column];
            if (_current_column_definition == null) continue;
            if (_operation.CompareTo("B") == 0 && !_current_column_definition.Required) continue;
            //end validations

            try
            {
              _cell_value = Range[_current_row, _current_column].Value;

              if (_cell_value == null && _current_column_definition.DefaultValue != null) _cell_value = _current_column_definition.DefaultValue;

              _val = GetValueFromCell(_current_column_definition, _cell_value);

              if (_val != null && _current_column_definition.ColumnType == SqlDbType.NVarChar)
              {
                _val = _val.ToString().ToUpperInvariant();
              }

              _error_message = IsValid(_current_column_definition, _val);

              if (!String.IsNullOrEmpty(_error_message))
              {
                _cancel_operation = true;
                _error_row = true;
                CancelImport(_response, _error_message, _current_row);
                continue;
              }
            }
            catch (Exception _exception)
            {
              _cancel_operation = true;
              _error_row = true;
              CancelImport(_response, _exception.Message, _current_row);
              continue;
            }


            if (!_cancel_operation)
            {

              //If operation = B
              if (_operation.CompareTo("B") == 0)
              {

                if (_cmd_delete == null)
                {
                  _cmd_delete = new SqlCommand(_sql_delete_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction);

                  foreach (var _column in MappedColumns)
                  {
                    if (_column != null && _column.Required)
                      _cmd_delete.Parameters.Add(new SqlParameter(_column.ColumnName, _column.ColumnType));
                  }

                }

                _cmd_delete.Parameters[MappedColumns[_current_column].ColumnName].Value = _val;

              }

              //If operation = A
              if (_operation.CompareTo("A") == 0)
              {
                //cmd_select init
                if (_cmd_select == null)
                {
                  _cmd_select = new SqlCommand(_sql_select_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction);

                  foreach (var _column in MappedColumns)
                  {
                    if (_column != null && _column.Required)
                      _cmd_select.Parameters.Add(new SqlParameter(_column.ColumnName, _column.ColumnType));
                  }

                }//cmd_select init

                //cmd_insert init
                if (_cmd_insert == null)
                {
                  _cmd_insert = new SqlCommand(_sql_insert_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction);

                  foreach (var _column in MappedColumns)
                  {
                    if (_column != null)
                      _cmd_insert.Parameters.Add(new SqlParameter(_column.ColumnName, _column.ColumnType));
                  }

                }//cmd_insert init

                _cmd_insert.Parameters[MappedColumns[_current_column].ColumnName].Value = _val;
                if (_current_column_definition.Required) _cmd_select.Parameters[MappedColumns[_current_column].ColumnName].Value = _val;

              }//_operation.CompareTo("A") == 0

            }

          } //for columns

          if (!_cancel_operation)
          {
            try
            {

              if (_operation.CompareTo("B") == 0)
              {
                _result_query = _cmd_delete.ExecuteNonQuery();

                if ((int)_result_query == 0)
                {
                  _warning = new Warning();
                  _warning.RowNumber = _current_row + 1;
                  _warning.Message = Resource.String("STR_FRM_BLACKLIST_IMPORT_NOT_DELETED_NOTEXISTS");
                  _response.WarningList.Add(_warning);
                }
                else
                {
                  _response.ProcessedOk++;
                }

              }
              else if (_operation.CompareTo("A") == 0)
              {
                _result_query = _cmd_select.ExecuteScalar();

                if ((int)_result_query > 0)
                {
                  _warning = new Warning();
                  _warning.RowNumber = _current_row + 1;
                  _warning.Message = Resource.String("STR_FRM_BLACKLIST_IMPORT_NOT_ADDED_EXISTS");
                  _response.WarningList.Add(_warning);
                }
                else
                {
                  _result_query = _cmd_insert.ExecuteNonQuery();

                  if ((int)_result_query <= 0)
                  {
                    _warning = new Warning();
                    _warning.RowNumber = _current_row + 1;
                    _warning.Message = Resource.String("STR_FRM_BLACKLIST_IMPORT_NOT_ADDED");
                    _response.WarningList.Add(_warning);
                  }
                  else
                  {
                    _response.ProcessedOk++;
                  }
                }
              }
            }
            catch (SqlException _sqlex_exception)
            {
              _error_row = true;
              _cancel_operation = true;
              CancelImport(_response, _sqlex_exception.Message, _current_row);
            }
            catch (Exception _exception)
            {
              _error_row = true;
              _cancel_operation = true;
              CancelImport(_response, _exception.Message, _current_row);
            }

          } //!_cancel_operation


          if (_error_row) _response.ProcessedErrors++;
          _response.Elapsed = DateTime.Now - _start_time;

          if (!string.IsNullOrEmpty(FileOut))
            WriteLog(_response.ToString());
          if (OnRowUpdated != null)
            OnRowUpdated(this, _response);

          _error_row = false;

        } // for rows

        if (_cancel_operation)
          _db_trx.Rollback();
        else
          _db_trx.Commit();

        if (OnRowUpdated != null)
          OnRowUpdated(this, _response);

      }
    }

    /// <summary>
    /// Cancel the import
    /// </summary>
    /// <param name="MessageError"></param>
    /// <param name="RowNumber"></param>
    /// <param name="DB_TRX"></param>
    private void CancelImport(ImportEventArgs ImportEventArgs, string MessageError, int RowNumber)
    {
      ErrorDetail _error;
      _error = new ErrorDetail();
      _error.RowNumber = RowNumber + 1;
      _error.DescriptionError = MessageError;
      ImportEventArgs.ErrorsDetails.Add(_error);
      ImportEventArgs.ProcessedOk = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ColumnDefinition"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    private string IsValid(ColumnDefinition ColumnDefinition, Object Value)
    {

      if (String.IsNullOrEmpty(Value.ToString()) && ColumnDefinition.Required) return String.Format(Resource.String("STR_FRM_BLACKLIST_IMPORT_FIELD_REQUIRED"), ColumnDefinition.Alias.ToUpper());

      if (ColumnDefinition.MaxLength > 0 && Value != null && Value.ToString().Length > ColumnDefinition.MaxLength) return String.Format(Resource.String("STR_FRM_BLACKLIST_IMPORT_FIELD_TOO_LONG"), ColumnDefinition.Alias.ToUpper());

      return "";
    }

    /// <summary>
    /// get value in correct type from cell, expand as needed 
    /// </summary>
    /// <param name="CurrentColumn"></param>
    /// <param name="CellValue"></param>
    /// <returns></returns>
    private static object GetValueFromCell(ColumnDefinition CurrentColumn, object CellValue)
    {
      object _val;
      try
      {
        if (CurrentColumn.EnumTranslation != null)
        {
          _val = CurrentColumn.EnumTranslation((string)CellValue);
        }
        else
        {
          switch (CurrentColumn.ColumnType)
          {
            case SqlDbType.DateTimeOffset:
            case SqlDbType.DateTime2:
            case SqlDbType.DateTime:
              if (CellValue is double)
              {
                _val = DateTime.FromOADate((double)CellValue);
              }
              else
              {
                _val = DateTime.Parse((string)CellValue);
              }

              break;
            case SqlDbType.Text:
            case SqlDbType.Char:
            case SqlDbType.NChar:
            case SqlDbType.NText:
            case SqlDbType.VarChar:
            case SqlDbType.NVarChar:
              _val = CellValue ?? "";
              break;
            case SqlDbType.Int:
            case SqlDbType.SmallInt:
            case SqlDbType.TinyInt:
              _val = int.Parse(CellValue.ToString());
              break;
            case SqlDbType.BigInt:
              _val = long.Parse(CellValue.ToString());
              break;
            //case SqlDbType.Binary:
            //case SqlDbType.Bit:
            //case SqlDbType.Decimal:
            //case SqlDbType.Float:
            //case SqlDbType.Image:
            //case SqlDbType.Money:
            //case SqlDbType.Real:
            //case SqlDbType.UniqueIdentifier:
            //case SqlDbType.SmallDateTime:
            //case SqlDbType.SmallMoney:
            //case SqlDbType.Timestamp:
            //case SqlDbType.VarBinary:
            //case SqlDbType.Variant:
            //case SqlDbType.Xml:
            //case SqlDbType.Udt:
            //case SqlDbType.Structured:
            //case SqlDbType.Date:
            //case SqlDbType.Time:
            default:
              throw new ArgumentOutOfRangeException();
          }
        }
      }
      catch (Exception _exception)
      {
        throw new Exception(String.Format(Resource.String("STR_FRM_BLACKLIST_IMPORT_INVALID_FIELD"), CurrentColumn.Alias.ToUpper(), _exception.Message));
      }

      return _val;
    }
  }
}