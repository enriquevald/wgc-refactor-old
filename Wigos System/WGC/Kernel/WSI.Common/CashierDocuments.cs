//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CashierDocuments.cs
// 
//   DESCRIPTION : 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-FEB-2012 MPO    First release
// 02-MAY-2012 MPO    Added new document --> CreateDocument_ConstanciaEstatal
// 03-MAY-2012 MPO    Only one function for generate document "Estatal and Federal".
// 24-JAN-2013 MPO    Added new function for Order payment. --> CreateDocumentPaymentOrder
// 12-DEC-2013 RMS    In TITO mode: payment orders should not show the virtual accounts id.
// 12-FEB-2014 FBA    Added account holder id type control via CardData class IdentificationTypes
// 23-JUL-2014 SGB    Generate documents for PDF: If it is necessary hide fields for block 1 or block 2
// 30-APR-2015 DRV    Added new function for Statement --> CreateDocumentStatement Feature 1064
// 01-JUL-2015 YNM    Fixed Bug WIG-2456: CreateDocumentStatement function
// 20-JUN-2016 LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence
// 01-MAR-2017 FGB    Bug 24126: Withholding generation: The Excel data is incorrect
// 03-MAR-2017 DPC    Bug 25255: No se puede realizar el reintegro total con orden de pago
// 08-MAR-2017 FGB    PBI 25268: Third TAX - Payment Voucher
// 16-MAR-2017 ATB    PBI 25737: Third TAX - Report columns
// 27-MAR-2017 JMM    WIGOS-672: The "Informaci�n de sala" in the payment order is not displayed correctly
// 29-MAY-2017 DHA    Bug 27734: Background constancy generation error
// 29-MAY-2017 LTC    Bug 27739: Error witholding  - delete currency symbol in decimal fields
// 02-JUN-2017 FGB    Bug 27741: Error witholding document - "Folio" field does not show the required value
// 18-JUN-2017 FGB    Bug 27741: Error witholding document - "Folio" field does not show the required value
// 30-OCT-2017 JML    Fixed Bug 30451:WIGOS-5758 WinLoss Statement - Result shows incorrect sign
// 13-NOV-2017 LTC    Bug 30632:WIGOS-6159 WigosGui: Error occurred when the customer data is printed from customer base form
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace WSI.Common
{
  public static class CashierDocs
  {
    public enum EVIDENCE_TYPE
    {
      FEDERAL = 99,
      ESTATAL = 1,
      // LTC 20-JUN-2016
      ESTATAL_YUCATAN = 2,
      ESTATAL_CAMPECHE = 3,
      FEDERAL_CODERE = 4,
      COUNCIL = 5,
      NONE = 0
    }

    // LTC 20-JUN-2016
    /// <summary>
    /// Generic function for load a template and fill the evidence based on the parameter of "type of evidence".
    /// </summary>
    /// <param name="EvidenceType"></param>
    /// <param name="OperationId"></param>
    /// <param name="OpCode"></param>
    /// <param name="WitholdingTax"></param>
    /// <param name="PrizeDateTime"></param>
    /// <param name="PlayerId1"></param>
    /// <param name="PlayerId2"></param>
    /// <param name="PlayerFullName"></param>
    /// <param name="PlayerGivenName"></param>
    /// <param name="PlayerSourceName1"></param>
    /// <param name="PlayerSourceName2"></param>
    /// <param name="Prize"></param>
    /// <param name="BusinessId1"></param>
    /// <param name="BusinessId2"></param>
    /// <param name="BusinessName"></param>
    /// <param name="RepresentativeId1"></param>
    /// <param name="RepresentativeId2"></param>
    /// <param name="RepresentativeName"></param>
    /// <param name="Sign"></param>
    /// <param name="DocumentId"></param>
    /// <param name="Extension"></param>
    /// <returns></returns>
    public static MemoryStream CreateDocumentEvidence(EVIDENCE_TYPE EvidenceType,
                                                      long OperationId,
                                                      OperationCode OpCode,
                                                      String SiteId,
                                                      Currency WitholdingTax,
                                                      DateTime PrizeDateTime,
                                                      String PlayerId1,
                                                      String PlayerId2,
                                                      String PlayerFullName,
                                                      String PlayerGivenName,
                                                      String PlayerSourceName1,
                                                      String PlayerSourceName2,
                                                      Currency Prize,
                                                      String BusinessId1,
                                                      String BusinessId2,
                                                      String BusinessName,
                                                      String RepresentativeId1,
                                                      String RepresentativeId2,
                                                      String RepresentativeName,
                                                      Byte[] Sign,
                                                      long DocumentId,
                                                      out String Extension,
                                                      SqlTransaction SqlTrx)
    {
      Template _template_temp;
      MemoryStream _pdf_filled;

      _template_temp = null;
      _pdf_filled = null;

      Extension = String.Empty;

      Misc.WriteLog(String.Format("CashierDocuments - BEGIN CreateDocumentEvidence OperationId: {0} PlayerFullName: {1}", OperationId, PlayerFullName), Log.Type.Message);

      try
      {
        // Get Template (Common Fields)
        _template_temp = GetTemplate(EvidenceType);

        if (_template_temp == null)
        {
          return null;
        }

        // Specific Fields
        switch (EvidenceType)
        {
          case EVIDENCE_TYPE.FEDERAL:
            Misc.WriteLog(String.Format("CashierDocuments - CreateDocumentEvidence Switch FEDERAL"), Log.Type.Message);

            _template_temp.SetField("1", PrizeDateTime.Month.ToString());    // MES INICIAL
            _template_temp.SetField("2", PrizeDateTime.Month.ToString());    // MES FINAL
            _template_temp.SetField("3", PrizeDateTime.Year.ToString());     // EJERCICIO
            _template_temp.SetField("4", PlayerId1);                         // RFC JUGADOR
            _template_temp.SetField("5", PlayerId2);                         // CURP JUGADOR
            _template_temp.SetField("6", PlayerFullName);                    // NOMBRE JUGADOR
            _template_temp.SetField("19", Prize.ToString());                 // MONTO
            _template_temp.SetField("25", WitholdingTax.ToString());         // ISR
            _template_temp.SetField("28", BusinessId1);                      // RFC EMPRESA
            _template_temp.SetField("29", BusinessName);                     // NOMBRE EMPRESA
            _template_temp.SetField("30", BusinessId2);                      // CURP EMPRESA
            _template_temp.SetField("31", RepresentativeName);               // NOMBRE REPRESENTANTE
            _template_temp.SetField("32", RepresentativeId1);                // RFC REPRESENTANTE
            _template_temp.SetField("33", RepresentativeId2);                // CURP REPRESENTANTE

            _template_temp.SetField("36", "F1");                             // CLAVE DE PAGO -> F1=PREMIO

            break;

          case EVIDENCE_TYPE.ESTATAL:
            Misc.WriteLog(String.Format("CashierDocuments - CreateDocumentEvidence Switch ESTATAL"), Log.Type.Message);

            _template_temp.SetField("1", PrizeDateTime.Month.ToString());    // MES INICIAL
            _template_temp.SetField("2", PrizeDateTime.Month.ToString());    // MES FINAL
            _template_temp.SetField("3", PrizeDateTime.Year.ToString());     // EJERCICIO
            _template_temp.SetField("4", PlayerId1);                         // RFC JUGADOR
            _template_temp.SetField("5", PlayerId2);                         // CURP JUGADOR
            _template_temp.SetField("6", PlayerFullName);                    // NOMBRE JUGADOR
            _template_temp.SetField("19", Prize.ToString());                 // MONTO
            _template_temp.SetField("25", WitholdingTax.ToString());         // ISR
            _template_temp.SetField("28", BusinessId1);                      // RFC EMPRESA
            _template_temp.SetField("29", BusinessName);                     // NOMBRE EMPRESA
            _template_temp.SetField("30", BusinessId2);                      // CURP EMPRESA
            _template_temp.SetField("31", RepresentativeName);               // NOMBRE REPRESENTANTE
            _template_temp.SetField("32", RepresentativeId1);                // RFC REPRESENTANTE
            _template_temp.SetField("33", RepresentativeId2);                // CURP REPRESENTANTE

            break;

          case EVIDENCE_TYPE.ESTATAL_YUCATAN:
            Misc.WriteLog(String.Format("CashierDocuments - CreateDocumentEvidence Switch ESTATAL_YU"), Log.Type.Message);

            _template_temp.SetField("MES_26", PrizeDateTime.Month.ToString());              // MES INICIAL
            _template_temp.SetField("A�O_26", PrizeDateTime.Year.ToString());               // EJERCICIO
            _template_temp.SetField("APELLIDO PATERNO_13", PlayerSourceName1);              // APELLIDO PATERNO
            _template_temp.SetField("SEGUNDO MATERNO", PlayerSourceName2);                  // APELLIDO MATERNO
            _template_temp.SetField("NOMBRES_13", PlayerGivenName);                         // NOMBRE JUGADOR
            _template_temp.SetField("CLAVE �NICA DE REGISTRO DE POBLACI�N_13", PlayerId2);  // CURP JUGADOR
            _template_temp.SetField("REGISTRO FEDERAL DE CONTRIBUYENTES_13", PlayerId1);    // RFC JUGADOR
            _template_temp.SetField("DENOMINACI�N O RAZ�N SOCIAL_11", BusinessName);        // NOMBRE EMPRESA
            _template_temp.SetField("REGISTRO FEDERAL DE CONTRIBUYENTES_14", BusinessId1);  // RFC EMPRESA
            _template_temp.SetField("NOMBRES_15", RepresentativeName);                      // NOMBRE REPRESENTANTE
            _template_temp.SetField("CLAVE �NICA DE REGISTRO DE POBLACI�N_15", RepresentativeId2);  // CURP REPRESENTANTE
            _template_temp.SetField("REGISTRO FEDERAL DE CONTRIBUYENTES_15", RepresentativeId1);    // RFC REPRESENTANTE
            _template_temp.SetField("FECHA DE REALIZACI�N DEL EVENTO", PrizeDateTime.ToShortDateString().ToString());  // FECHA EVENTO
            _template_temp.SetField("MONTO DEL PREMIO OTORGADO", Prize.ToString());         // MONTO
            _template_temp.SetField("VALOR DE LOS SERVICIOS", WitholdingTax.ToString());    // ISR

            break;

          case EVIDENCE_TYPE.ESTATAL_CAMPECHE:
            Misc.WriteLog(String.Format("CashierDocuments - CreateDocumentEvidence Switch ESTATAL_CA"), Log.Type.Message);

            _template_temp.SetField("W5", PrizeDateTime.Month.ToString());                  // MES INICIAL
            _template_temp.SetField("Z5", PrizeDateTime.Year.ToString());                   // EJERCICIO
            _template_temp.SetField("D11", PlayerSourceName1);                              // APELLIDO PATERNO
            _template_temp.SetField("M11", PlayerSourceName2);                              // APELLIDO MATERNO
            _template_temp.SetField("U11", PlayerGivenName);                                // NOMBRE JUGADOR
            _template_temp.SetField("D13", PlayerId2);                                      // CURP JUGADOR
            _template_temp.SetField("M13", PlayerId1);                                      // RFC JUGADOR
            _template_temp.SetField("D17", BusinessName);                                   // NOMBRE EMPRESA
            _template_temp.SetField("M21", BusinessId1);                                    // RFC EMPRESA
            _template_temp.SetField("U25", RepresentativeName);                             // NOMBRE REPRESENTANTE
            _template_temp.SetField("D27", RepresentativeId2);                              // CURP REPRESENTANTE
            _template_temp.SetField("M27", RepresentativeId1);                              // RFC REPRESENTANTE
            _template_temp.SetField("D31", PrizeDateTime.ToShortDateString().ToString());   // FECHA EVENTO
            _template_temp.SetField("M31", Prize.ToString());                               // MONTO
            _template_temp.SetField("U31", WitholdingTax.ToString());                       // ISR

            break;

          case EVIDENCE_TYPE.FEDERAL_CODERE:
            Misc.WriteLog(String.Format("CashierDocuments - CreateDocumentEvidence Switch ESTATAL_CO"), Log.Type.Message);

            // LTC 2017-MAY-29
            _template_temp.SetField("A3", GetFolio(OperationId, OpCode, SiteId, SqlTrx));   // FOLIO
            _template_temp.SetField("B3", PrizeDateTime.ToShortDateString().ToString());    // FECHA EVENTO
            _template_temp.SetField("C3", "20");                                            // CLAVE RETENCI�N
            _template_temp.SetField("D3", "Obtenci�n de Premios.");                         // DESCRIPCI�N
            _template_temp.SetField("E3", BusinessId1);                                     // RFC EMPRESA
            _template_temp.SetField("F3", BusinessName);                                    // NOMBRE EMPRESA
            _template_temp.SetField("G3", GetTipoReceptor(PlayerId1));                      // TIPO RECEPTOR
            _template_temp.SetField("H3", PlayerId1);                                       // RFC JUGADOR
            _template_temp.SetField("I3", PlayerId2);                                       // CURP JUGADOR
            _template_temp.SetField("J3", PlayerFullName);                                  // NOMBRE JUGADOR
            _template_temp.SetField("K3", PrizeDateTime.Month.ToString());                  // MES INICIAL
            _template_temp.SetField("L3", PrizeDateTime.Month.ToString());                  // MES INICIAL
            _template_temp.SetField("M3", PrizeDateTime.Year.ToString());                   // EJERCICIO
            _template_temp.SetField("N3", Prize.ToStringWithoutSymbols());                  // MONTO
            _template_temp.SetField("O3", Prize.ToStringWithoutSymbols());                  // MONTO
            _template_temp.SetField("P3", "0");                                             // EXENTO
            _template_temp.SetField("Q3", WitholdingTax.ToStringWithoutSymbols());          // ISR
            _template_temp.SetField("R3", Prize.ToStringWithoutSymbols());                  // MONTO
            _template_temp.SetField("S3", GetCodigoImpuesto());                             // IMPUESTO
            _template_temp.SetField("T3", WitholdingTax.ToStringWithoutSymbols());          // ISR
            _template_temp.SetField("U3", "Pago Provisional");                              // TIPO PAGO

            break;
        }

        Misc.WriteLog(String.Format("CashierDocuments - FillSignature"), Log.Type.Message);

        //Fill the signature
        FillSignature(EvidenceType, Sign, _template_temp);

        _pdf_filled = (MemoryStream)_template_temp.GetStream();
        Extension = _template_temp.m_template_ext;

        // _template_pdf.Print();

        Misc.WriteLog(String.Format("CashierDocuments - Return PDF"), Log.Type.Message);

        return _pdf_filled;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
      finally
      {
        if (_template_temp != null)
        {
          _template_temp.Close();
        }

        Misc.WriteLog(String.Format("CashierDocuments - END CreateDocumentEvidence OperationId: {0} PlayerFullName: {1}", OperationId, PlayerFullName), Log.Type.Message);
      }
    } // CreateDocumentEvidence

    /// <summary>
    /// Return the 'Numero de Recibo'
    /// </summary>
    /// <param name="OperationId"></param>
    private static String GetNumeroReciboForFolioFromOperation(long OperationId, SqlTransaction SqlTrx)
    {
      //18-JUN-2017 FGB    Bug 27741: Error witholding document - "Folio" field does not show the required value
      Int64 _numero_recibo_for_folio;

      //Get numero recibo from operation = Min voucherid of operation
      if (!Operations.DB_GetNumeroReciboForFolioFromOperation(OperationId, out _numero_recibo_for_folio, SqlTrx))
      {
        _numero_recibo_for_folio = 0;
      }

      return _numero_recibo_for_folio.ToString();
    }

    // Return Folio
    private static String GetFolio(long OperationId, OperationCode OpCode, String SiteId, SqlTransaction SqlTrx)
    {
      // 02-JUN-2017 FGB Bug 27741: Error witholding document - "Folio" field does not show the required value
      //[ID SALA][T=Terminales || M = MESAS][Recibo = N�mero primer voucher de la operacion].
      return SiteId.ToString()
           + Operations.GetOperationTypeForFolio(OpCode)
           + GetNumeroReciboForFolioFromOperation(OperationId, SqlTrx);
    }

    // Return C�digo de impuesto
    private static String GetCodigoImpuesto()
    {
      return "01";
    }

    // Return Tipo Receptor
    private static String GetTipoReceptor(String PlayerId1)
    {
      return (PlayerId1 == ValidateFormat.RFC_GENERIC_02 ? "Extranjero" : "Nacional"); // TIPO RECEPTOR
    }

    /// <summary>
    /// Set signature image
    /// </summary>
    /// <param name="EvidenceType"></param>
    /// <param name="Sign"></param>
    /// <param name="TemplateTemp"></param>
    private static void FillSignature(EVIDENCE_TYPE EvidenceType, Byte[] Sign, Template TemplateTemp)
    {
      // Signature
      if (Sign != null)
      {
        switch (EvidenceType)
        {
          case EVIDENCE_TYPE.FEDERAL:
            TemplateTemp.SetImage(Sign, new Rectangle(47, 55, 160, 90));  // SIGNATURE IMAGE
            break;

          case EVIDENCE_TYPE.ESTATAL:
            TemplateTemp.SetImage(Sign, new Rectangle(63, 327, 160, 90)); // SIGNATURE IMAGE
            break;

          case EVIDENCE_TYPE.ESTATAL_YUCATAN:
            TemplateTemp.SetImage(Sign, new Rectangle(40, 240, 160, 90)); // SIGNATURE IMAGE
            break;

          case EVIDENCE_TYPE.ESTATAL_CAMPECHE:
            TemplateTemp.SetImage(Sign, new Rectangle(11, 422, 150, 85)); // SIGNATURE IMAGE
            break;

          case EVIDENCE_TYPE.FEDERAL_CODERE:
            break;

        }
      }
    }

    // PURPOSE: 
    //
    //  PARAMS:
    //     - INPUT:
    //           - PaymentOrder:
    //           - Logo:
    //           - PaymentOrderDoc:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public static Boolean CreateDocumentPaymentOrder(PaymentOrder PaymentOrder, Byte[] Logo, out MemoryStream PaymentOrderDoc)
    {
      Template _template_pdf;
      String _player_name;
      Dictionary<String, String> _payment_data;
      Dictionary<String, String> _payment_field;
      String _replace_value;
      String _block3;
      String _tax_str;
      String _doc_temp;
      String _doc_field;
      String _sign;
      String _check_payment_line;
      Int32 _doc_type;

      const Int32 BLOCK3_FIELD_NAME = 51;
      const Int32 BLOCK3_FIELD_VALUE = 17;

      PaymentOrderDoc = null;
      _player_name = PaymentOrder.HolderFullName;
      _payment_data = new Dictionary<String, String>();
      _payment_field = new Dictionary<String, String>();
      _block3 = "";

      try
      {
        using (MemoryStream _template_ms = WSI.Common.Resource.MemoryStream("PaymentOrder"))
        {
          if (_template_ms == null)
          {
            return false;
          }

          _payment_data.Add("@APO_DOC_REFERENCE", Misc.ReadGeneralParams("PaymentOrder", "DocReference"));
          _payment_data.Add("@PAYMENT_ORDER_TITLE", Misc.ReadGeneralParams("PaymentOrder", "DocumentTitle"));
          _payment_data.Add("@APO_DATETIME", PaymentOrder.OrderDate.ToLongDateString());

          if (WSI.Common.TITO.Utils.IsTitoMode() && WSI.Common.TITO.Utils.IsVirtualAccountById(PaymentOrder.AccountId))
          {
            // In TITO mode, virtual accounts should not appear
            _payment_data.Add("@APO_ACCOUNT_ID", "---");
          }
          else
          {
            _payment_data.Add("@APO_ACCOUNT_ID", PaymentOrder.AccountId.ToString());
          }

          _payment_data.Add("@APO_PLAYER_NAME_SIGN", _player_name);
          _payment_data.Add("@APO_PLAYER_NAME", PaymentOrder.FormatText(_player_name));

          _doc_temp = Resource.String("STR_ACCOUNT_PAYMENT_DOC").Trim().Trim(Environment.NewLine.ToCharArray());
          _doc_field = "";

          if (!String.IsNullOrEmpty(PaymentOrder.HolderId1Type))
          {
            if (Int32.TryParse(PaymentOrder.HolderId1Type, out _doc_type))
            {
              _doc_field = _doc_temp.Replace("@APO_PLAYER_TYPE_DOC", IdentificationTypes.DocIdTypeString(_doc_type));
            }
            else
            {
              _doc_field = _doc_temp.Replace("@APO_PLAYER_TYPE_DOC", PaymentOrder.HolderId1Type.PadRight(20));
            }

            _doc_field = _doc_field.Replace("@APO_PLAYER_DOC_ID", PaymentOrder.HolderId1);
          }

          if (!String.IsNullOrEmpty(PaymentOrder.HolderId2Type))
          {
            _doc_field += Environment.NewLine;

            if (Int32.TryParse(PaymentOrder.HolderId2Type, out _doc_type))
            {
              _doc_field += _doc_temp.Replace("@APO_PLAYER_TYPE_DOC", IdentificationTypes.DocIdTypeString(_doc_type));
            }
            else
            {
              _doc_field += _doc_temp.Replace("@APO_PLAYER_TYPE_DOC", PaymentOrder.HolderId2Type.PadRight(20));
            }

            _doc_field = _doc_field.Replace("@APO_PLAYER_DOC_ID", PaymentOrder.HolderId2);
          }

          _payment_data.Add("@APO_DOCS", _doc_field);

          _check_payment_line = PaymentOrder.CheckPayment.ToString();

          //TODO: Prepare PaymentOrderCurrencyString for accept others cultures
          if (Resource.CountryISOCode2 == "MX")
          {
            _check_payment_line += " (" + Common.NumberToLetter.PaymentOrderCurrencyString(PaymentOrder.CheckPayment) + ")";
          }

          _payment_data.Add("@APO_CHECK_PAYMENT_LINE", PaymentOrder.FormatText(_check_payment_line));
          _payment_data.Add("@APO_CHECK_PAYMENT", PaymentOrder.CheckPayment.ToString());
          _payment_data.Add("@APO_EFFECTIVE_DAYS", PaymentOrder.EffectiveDays);
          _payment_data.Add("@APO_APPLICATION_DATE", PaymentOrder.ApplicationDate.ToLongDateString());
          _payment_data.Add("@APO_PAYMENT_TYPE", PaymentOrder.PaymentType);
          _payment_data.Add("@APO_BANK_NAME", PaymentOrder.BankName);
          _payment_data.Add("@APO_BANK_CUSTOMER_NUMBER", PaymentOrder.BankCustomerNumber);
          _payment_data.Add("@APO_BANK_ACCOUNT_NUMBER", PaymentOrder.BankAccountNumber);

          _payment_data.Add("@APO_ACCOUNT_BALANCE", PaymentOrder.AccountBalance.ToString());
          _payment_data.Add("@APO_TOTAL_DEVOLUTION", PaymentOrder.CashRedeem.TotalDevolution.ToString());
          _payment_data.Add("@APO_PRIZE", PaymentOrder.CashRedeem.prize.ToString());
          _payment_data.Add("@APO_TAX1", PaymentOrder.CashRedeem.tax1.ToString());
          _payment_data.Add("@APO_TAX2", PaymentOrder.CashRedeem.tax2.ToString());

          if (PaymentOrder.CashRedeem.tax3_pct != 0)
          {
            _payment_data.Add("@APO_TAX3", PaymentOrder.CashRedeem.tax3.ToString());
          }
          else
          {
            _payment_data.Add("@APO_TAX3", String.Empty);
          }

          _payment_data.Add("@APO_TOTAL_PAYMENT", PaymentOrder.TotalPayment.ToString());
          _payment_data.Add("@APO_CASH_PAYMENT", PaymentOrder.CashPayment.ToString());
          _payment_data.Add("@APO_AUTHORIZED_BY_TITLE1", PaymentOrder.AuthorizedTitle1);
          _payment_data.Add("@APO_AUTHORIZED_BY_NAME1", PaymentOrder.AuthorizedName1);
          _payment_data.Add("@APO_AUTHORIZED_BY_TITLE2", PaymentOrder.AuthorizedTitle2);
          _payment_data.Add("@APO_AUTHORIZED_BY_NAME2", PaymentOrder.AuthorizedName2);

          _payment_field.Add("DocReference", Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DOC_REFERENCE"));
          _payment_field.Add("DocTitle", Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DOC_TITLE"));
          _payment_field.Add("TitleBlock1", Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DOC_BLOCK1_TITLE"));
          _payment_field.Add("Block1", Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DOC_BLOCK1"));
          _payment_field.Add("TitleBlock2", Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DOC_BLOCK2_TITLE"));
          _payment_field.Add("Block2", Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DOC_BLOCK2"));
          _payment_field.Add("TitleBlock3", Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DOC_BLOCK3_TITLE"));

          //Taxes
          _block3 = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DOC_BLOCK3");
          _tax_str = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name") + ":";
          _block3 = _block3.Replace("@TAX1_NAME", _tax_str.PadRight(BLOCK3_FIELD_NAME));
          _tax_str = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name") + ":";
          _block3 = _block3.Replace("@TAX2_NAME", _tax_str.PadRight(BLOCK3_FIELD_NAME));

          _tax_str = String.Empty;
          if (PaymentOrder.CashRedeem.tax3_pct != 0)
          {
            _tax_str = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name") + ":";
          }
          _block3 = _block3.Replace("@TAX3_NAME", _tax_str.PadRight(BLOCK3_FIELD_NAME));

          _payment_field.Add("Block3", _block3);

          //Signatures
          _payment_field.Add("Sign1", Resource.String("STR_ACCOUNT_PAYMENT_SIGN"));

          _sign = "";
          if (!String.IsNullOrEmpty(PaymentOrder.AuthorizedName1))
          {
            _sign = Resource.String("STR_ACCOUNT_PAYMENT_SIGN1");
          }
          _payment_field.Add("Sign2", _sign);

          _sign = "";
          if (!String.IsNullOrEmpty(PaymentOrder.AuthorizedName2))
          {
            _sign = Resource.String("STR_ACCOUNT_PAYMENT_SIGN2");
          }
          _payment_field.Add("Sign3", _sign);

          _template_pdf = Template.Create(_template_ms, Template.TYPE_TEMPLATE.PDF);

          foreach (KeyValuePair<String, String> _field in _payment_field)
          {
            _replace_value = _field.Value;
            _replace_value = _replace_value.TrimStart(Environment.NewLine.ToCharArray());

            foreach (KeyValuePair<String, String> _data in _payment_data)
            {
              if (_field.Key == "Block3")
              {
                _replace_value = _replace_value.Replace(_data.Key, _data.Value.PadLeft(BLOCK3_FIELD_VALUE));
              }
              else
              {
                _replace_value = _replace_value.Replace(_data.Key, _data.Value);
              }
            }

            _template_pdf.SetField(_field.Key, _replace_value);
          }

          if (Logo != null)
          {
            _template_pdf.SetImage(Logo, new Rectangle(83, 655, 100, 100));
          }

          PaymentOrderDoc = (MemoryStream)_template_pdf.GetStream();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }

      return false;
    } // CreateDocumentPaymentOrder

    // PURPOSE: Create a PDF with account data
    //
    //  PARAMS:
    //     - INPUT:
    //           - Cardata
    //           
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean CreateDocumentAccountData(CardData CardData, Boolean IncludeScannedDocs, out MemoryStream AccountData)
    {
      TemplatePDF _template_pdf;
      Dictionary<String, String> _acc_data;
      Dictionary<String, String> _acc_field;
      String _replace_value;
      String _doc_temp;
      String _doc_field;
      Int32 _pad_r_col1;
      Int32 _total_pages;
      String[] _message_params = { "", "" };
      Int32 _current_page;
      Dictionary<String, List<Byte[]>> _account_docs_img;
      Dictionary<String, List<Byte[]>> _ben_docs_img;
      String _doc_type;
      MemoryStream _mm_pdf;

      AccountData = null;
      _acc_data = new Dictionary<String, String>();
      _acc_field = new Dictionary<String, String>();

      _account_docs_img = new Dictionary<String, List<Byte[]>>();
      _ben_docs_img = new Dictionary<String, List<Byte[]>>();

      _pad_r_col1 = 20;
      _current_page = 1;

      try
      {
        using (MemoryStream _template_ms = WSI.Common.Resource.MemoryStream("AccountData"))
        {
          if (_template_ms == null)
          {
            return false;
          }

          _total_pages = 1;

          //
          // IMAGE DOCS
          //
          if (IncludeScannedDocs)
          {
            //
            // LOAD IMAGES
            //
            if (CardData.LoadAccountDocuments())
            {
              // Holder Documents
              _doc_type = "";
              foreach (IDocument _idoc in CardData.PlayerTracking.HolderScannedDocs)
              {
                if (!CardData.GetInfoDocName(_idoc.Name, out _doc_type))
                {
                  return false;
                }

                if (!_account_docs_img.ContainsKey(_doc_type))
                {
                  _account_docs_img[_doc_type] = new List<Byte[]>();
                }

                _account_docs_img[_doc_type].Add(_idoc.Content);
              }

              foreach (KeyValuePair<String, List<Byte[]>> _docs in _account_docs_img)
              {
                _total_pages += (Int32)Math.Ceiling((float)_docs.Value.Count / 2);
              }

              //Beneficiary Beneficiary Documents.
              if (CardData.PlayerTracking.HolderHasBeneficiary)
              {
                _doc_type = "";
                foreach (IDocument _idoc in CardData.PlayerTracking.BeneficiaryScannedDocs)
                {
                  if (!CardData.GetInfoDocName(_idoc.Name, out _doc_type))
                  {
                    return false;
                  }

                  if (!_ben_docs_img.ContainsKey(_doc_type))
                  {
                    _ben_docs_img[_doc_type] = new List<Byte[]>();
                  }

                  _ben_docs_img[_doc_type].Add(_idoc.Content);
                }

                foreach (KeyValuePair<String, List<Byte[]>> _docs in _ben_docs_img)
                {
                  _total_pages += (Int32)Math.Ceiling((float)_docs.Value.Count / 2);
                }
              }
            }
          } // IncludeScannedDocs

          _acc_data.Add("@APD_DATETIME", WGDB.Now.ToLongDateString());
          _acc_data.Add("@APD_ACCOUNT_ID", CardData.AccountId.ToString());
          _acc_data.Add("@APD_PLAYER_NAME", CardData.PlayerTracking.HolderName);

          _doc_temp = Resource.String("STR_ACCOUNT_PAYMENT_DOC").Trim();
          _doc_field = String.Empty;

          //Document (id)
          if ((!String.IsNullOrEmpty(CardData.PlayerTracking.HolderId)
            && (CardData.PlayerTracking.HolderIdType != ACCOUNT_HOLDER_ID_TYPE.RFC)
            && (CardData.PlayerTracking.HolderIdType != ACCOUNT_HOLDER_ID_TYPE.CURP)))
          {
            _doc_field = _doc_temp.Replace("@APO_PLAYER_TYPE_DOC", IdentificationTypes.DocIdTypeString(CardData.PlayerTracking.HolderIdType).PadRight(_pad_r_col1));
            _doc_field = _doc_field.Replace("@APO_PLAYER_DOC_ID", CardData.PlayerTracking.HolderId);
          }

          // RFC (id1)
          if (!String.IsNullOrEmpty(CardData.PlayerTracking.HolderId1))
          {
            if (!String.IsNullOrEmpty(_doc_field))
            {
              _doc_field += Environment.NewLine;
            }

            _doc_field += _doc_temp.Replace("@APO_PLAYER_TYPE_DOC", IdentificationTypes.DocIdTypeString(CardData.PlayerTracking.HolderId1Type).PadRight(_pad_r_col1));
            _doc_field = _doc_field.Replace("@APO_PLAYER_DOC_ID", CardData.PlayerTracking.HolderId1);
          }

          //CURP (id2)
          if (!String.IsNullOrEmpty(CardData.PlayerTracking.HolderId2))
          {
            if (!String.IsNullOrEmpty(_doc_field))
            {
              _doc_field += Environment.NewLine;
            }

            _doc_field += _doc_temp.Replace("@APO_PLAYER_TYPE_DOC", IdentificationTypes.DocIdTypeString(CardData.PlayerTracking.HolderId2Type).PadRight(_pad_r_col1));
            _doc_field = _doc_field.Replace("@APO_PLAYER_DOC_ID", CardData.PlayerTracking.HolderId2);
          }

          _acc_data.Add("@APD_DOCS", _doc_field);
          _acc_data.Add("@APD_GENDER", CardData.GenderString(CardData.PlayerTracking.HolderGender));
          _acc_data.Add("@APD_NATIONALITY", CardData.NationalitiesString((Int32)CardData.PlayerTracking.HolderNationality));
          _acc_data.Add("@APD_BIRTH_DATE", (CardData.PlayerTracking.HolderBirthDate != DateTime.MinValue ? CardData.PlayerTracking.HolderBirthDate.ToShortDateString() : String.Empty));
          _acc_data.Add("@APD_WEDDING_DATE", (CardData.PlayerTracking.HolderWeddingDate != DateTime.MinValue ? CardData.PlayerTracking.HolderWeddingDate.ToShortDateString() : String.Empty));
          _acc_data.Add("@APD_BIRTH_COUNTRY", CardData.CountriesString((Int32)CardData.PlayerTracking.HolderBirthCountry));
          _acc_data.Add("@APD_MARITAL_STATUS", CardData.MaritalStatusString((ACCOUNT_MARITAL_STATUS)CardData.PlayerTracking.HolderMaritalStatus - 1));
          _acc_data.Add("@APD_OCCUPATION", CardData.OccupationsDescriptionString((Int32)CardData.PlayerTracking.HolderOccupationId));

          // Scanned Doc.
          if (String.IsNullOrEmpty(CardData.PlayerTracking.HolderId3Type))
          {
            _acc_data.Add("@APD_ID3_TYPE_STR", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_SCANNED_DOC_NONE"));
          }
          else
          {
            _acc_data.Add("@APD_ID3_TYPE_STR", IdentificationTypes.DocIdTypeStringList(CardData.PlayerTracking.HolderId3Type, true));
          }

          _acc_data.Add("@APD_PHONE01", CardData.PlayerTracking.HolderPhone01);
          _acc_data.Add("@APD_PHONE02", CardData.PlayerTracking.HolderPhone02);
          _acc_data.Add("@APD_EMAIL01", CardData.PlayerTracking.HolderEmail01);
          _acc_data.Add("@APD_EMAIL02", CardData.PlayerTracking.HolderEmail02);
          _acc_data.Add("@APD_TWITTER", CardData.PlayerTracking.HolderTwitter);
          _acc_data.Add("@APD_ADDRESS01", CardData.PlayerTracking.HolderAddress01 + (CardData.PlayerTracking.HolderExtNumber != String.Empty ? ", " + CardData.PlayerTracking.HolderExtNumber : String.Empty));
          _acc_data.Add("@APD_ADDRESS02", CardData.PlayerTracking.HolderAddress02);
          _acc_data.Add("@APD_ADDRESS03", CardData.PlayerTracking.HolderAddress03);
          _acc_data.Add("@APD_CITY", CardData.PlayerTracking.HolderCity);
          _acc_data.Add("@APD_ZIP", CardData.PlayerTracking.HolderZip);
          _acc_data.Add("@APD_FED_ENTITY", CardData.FedEntitiesString((Int32)CardData.PlayerTracking.HolderFedEntity));

          // Beneficiary
          if (WSI.Common.Misc.HasBeneficiaryAndNotAMLExternalControl() && CardData.PlayerTracking.HolderHasBeneficiary)
          {
            StringBuilder _ben_block;

            _ben_block = new StringBuilder(Resource.String("STR_ACCOUNT_PERSONAL_DATA_BLOCK3").Trim(Environment.NewLine.ToCharArray()));

            _acc_data.Add("@APD_BEN_NAME", CardData.PlayerTracking.BeneficiaryName); // For CreateDocumentAccountImgDoc 
            _ben_block.Replace("@APD_BEN_NAME", CardData.PlayerTracking.BeneficiaryName);

            _doc_temp = Resource.String("STR_ACCOUNT_PAYMENT_DOC").Trim();
            _doc_field = String.Empty;

            // RFC
            if (!String.IsNullOrEmpty(CardData.PlayerTracking.BeneficiaryId1))
            {
              _doc_field = _doc_temp.Replace("@APO_PLAYER_TYPE_DOC", IdentificationTypes.DocIdTypeString(CardData.PlayerTracking.BeneficiaryId1Type).PadRight(_pad_r_col1));
              _doc_field = _doc_field.Replace("@APO_PLAYER_DOC_ID", CardData.PlayerTracking.BeneficiaryId1);
            }

            // CURP
            if (!String.IsNullOrEmpty(CardData.PlayerTracking.BeneficiaryId2))
            {
              if (!String.IsNullOrEmpty(CardData.PlayerTracking.BeneficiaryId1))
              {
                _doc_field += Environment.NewLine.PadRight(6);
              }

              _doc_field += _doc_temp.Replace("@APO_PLAYER_TYPE_DOC", IdentificationTypes.DocIdTypeString(CardData.PlayerTracking.BeneficiaryId2Type).PadRight(_pad_r_col1));
              _doc_field = _doc_field.Replace("@APO_PLAYER_DOC_ID", CardData.PlayerTracking.BeneficiaryId2);
            }

            _ben_block.Replace("@APD_BEN_DOCS", _doc_field);

            //NO DOCUMENT: Delete new line empty
            if (String.IsNullOrEmpty(_doc_field))
            {
              _doc_field = Regex.Replace(_ben_block.ToString(), @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
              _ben_block.Remove(0, _ben_block.Length);
              _ben_block.Append(_doc_field);
            }

            _ben_block.Replace("@APD_BEN_GENDER", CardData.GenderString(CardData.PlayerTracking.BeneficiaryGender));
            _ben_block.Replace("@APD_BEN_BIRTH_DATE", (CardData.PlayerTracking.BeneficiaryBirthDate != DateTime.MinValue ? CardData.PlayerTracking.BeneficiaryBirthDate.ToShortDateString() : String.Empty));
            _ben_block.Replace("@APD_BEN_OCCUPATION", CardData.OccupationsDescriptionString((Int32)CardData.PlayerTracking.BeneficiaryOccupationId));

            // Scanned Doc.
            if (!String.IsNullOrEmpty(CardData.PlayerTracking.BeneficiaryId3Type))
            {
              _acc_data.Add("@APD_BEN_ID3_TYPE_STR", IdentificationTypes.DocIdTypeStringList(CardData.PlayerTracking.BeneficiaryId3Type, true));
              _ben_block.Replace("@APD_BEN_ID3_TYPE_STR", IdentificationTypes.DocIdTypeStringList(CardData.PlayerTracking.BeneficiaryId3Type, true));
            }
            else
            {
              _ben_block.Replace("@APD_BEN_ID3_TYPE_STR", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_SCANNED_DOC_NONE"));
            }

            _acc_data.Add("@APD_BENEFICIARY", _ben_block.ToString());
          }
          else
          {
            _acc_data.Add("@APD_BENEFICIARY", String.Empty);
          }

          // Add blocks
          _acc_field.Add("TitleBlock1", Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DOC_BLOCK1_TITLE"));
          _acc_field.Add("Block1", AccountFieldsBlock1());
          _acc_field.Add("Block2", AccountFieldsBlock2());
          _message_params[0] = _current_page.ToString();
          _message_params[1] = _total_pages.ToString();
          _acc_field.Add("Page", Resource.String("STR_ACCOUNT_PERSONAL_DATA_PAGE", _message_params));

          _template_pdf = (TemplatePDF)Template.Create(_template_ms, Template.TYPE_TEMPLATE.PDF);

          foreach (KeyValuePair<String, String> _field in _acc_field)
          {
            _replace_value = _field.Value;
            _replace_value = _replace_value.TrimStart(Environment.NewLine.ToCharArray());

            foreach (KeyValuePair<String, String> _data in _acc_data)
            {
              _replace_value = _replace_value.Replace(_data.Key, _data.Value);
            }

            _template_pdf.SetField(_field.Key, _replace_value);
          }

          _template_pdf.Fill();
          // LTC 13-NOV-2017
          _template_pdf.SetIsChangedOff();

          // Merge pdf - Holder Scanned Doc.
          if (_account_docs_img.Count > 0)
          {
            foreach (KeyValuePair<String, List<Byte[]>> _docs in _account_docs_img)
            {
              _mm_pdf = null;

              if (CreateDocumentsForDocImages(_docs.Key, _acc_data, _docs.Value, false, _total_pages, _current_page, out _current_page, out _mm_pdf))
              {
                _template_pdf.Merge(_mm_pdf);
              }
              else
              {
                return false;
              }
            }
          }

          // Merge pdf - Beneficiary Scanned Doc.
          if (_ben_docs_img.Count > 0)
          {
            foreach (KeyValuePair<String, List<Byte[]>> _docs in _ben_docs_img)
            {
              _mm_pdf = null;

              if (CreateDocumentsForDocImages(_docs.Key, _acc_data, _docs.Value, true, _total_pages, _current_page, out _current_page, out _mm_pdf))
              {
                _template_pdf.Merge(_mm_pdf);
              }
              else
              {
                return false;
              }
            }
          }

          // Generate stream
          AccountData = (MemoryStream)_template_pdf.GetStream();
        }

        return true;
      }
      catch
      { }

      return false;
    }

    // PURPOSE: Return the account block 1  
    //
    //  PARAMS:
    //     - INPUT:
    //           
    //     - OUTPUT:
    //
    // RETURNS:
    //     - String of the block
    private static String AccountFieldsBlock1()
    {
      StringBuilder _sb;
      Int32 _pad_right;

      _pad_right = 21;

      _sb = new StringBuilder("");

      _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_DATE").PadRight(_pad_right, ' ') + "@APD_DATETIME");
      _sb.AppendLine("");
      _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_ACCOUNT_NUMBER").PadRight(_pad_right, ' ') + "@APD_ACCOUNT_ID");
      _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_PLAYER_NAME").PadRight(_pad_right, ' ') + "@APD_PLAYER_NAME");
      _sb.AppendLine("@APD_DOCS");
      _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_GENDER").PadRight(_pad_right, ' ') + "@APD_GENDER");

      if (GeneralParam.GetBoolean("Account.VisibleField", "Nationality", true))
      {
        _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_NACIONALITY").PadRight(_pad_right, ' ') + "@APD_NATIONALITY");
      }

      _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_DATE_BIRTH").PadRight(_pad_right, ' ') + "@APD_BIRTH_DATE");

      if (GeneralParam.GetBoolean("Account.VisibleField", "BirthCountry", true))
      {
        _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_BIRTH_COUNTRY").PadRight(_pad_right, ' ') + "@APD_BIRTH_COUNTRY");
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "WeddingDate", true))
      {
        _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_DATE_WEDDING").PadRight(_pad_right, ' ') + "@APD_WEDDING_DATE");
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "MaritalStatus", true))
      {
        _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_MARITAL_STATUS").PadRight(_pad_right, ' ') + "@APD_MARITAL_STATUS");
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "Occupation", true))
      {
        _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_OCCUPATION").PadRight(_pad_right, ' ') + "@APD_OCCUPATION");
      }

      _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_SCANNED_DOC").PadRight(_pad_right, ' ') + "@APD_ID3_TYPE_STR");

      return _sb.ToString();
    } // AccountFieldsBlock1

    // PURPOSE: Return the account block 2
    //
    //  PARAMS:
    //     - INPUT:
    //           
    //     - OUTPUT:
    //
    // RETURNS:
    //     - String of the block

    private static String AccountFieldsBlock2()
    {
      StringBuilder _sb;
      Int32 _pad_right;
      String _tab_left;

      _pad_right = 21;
      _sb = new StringBuilder("");
      _tab_left = new String(' ', 4);

      //subBlock Contact
      if (GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true)
        || GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true)
        || GeneralParam.GetBoolean("Account.VisibleField", "Email1", true)
        || GeneralParam.GetBoolean("Account.VisibleField", "Email2", true)
        || GeneralParam.GetBoolean("Account.VisibleField", "TwitterAccount", true))
      {
        _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_CONTACT"));
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true)
        && GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
      {
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_PHONE1").PadRight(_pad_right, ' ') + "@APD_PHONE01");
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_PHONE2").PadRight(_pad_right, ' ') + "@APD_PHONE02");
      }
      else
      {
        if (GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true))
        {
          _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_PHONE").PadRight(_pad_right, ' ') + "@APD_PHONE01");
        }

        if (GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
        {
          _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_PHONE").PadRight(_pad_right, ' ') + "@APD_PHONE02");
        }
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "Email1", true)
        && GeneralParam.GetBoolean("Account.VisibleField", "Email2", true))
      {
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_EMAIL1").PadRight(_pad_right, ' ') + "@APD_EMAIL01");
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_EMAIL2").PadRight(_pad_right, ' ') + "@APD_EMAIL02");
      }
      else
      {
        if (GeneralParam.GetBoolean("Account.VisibleField", "Email1", true))
        {
          _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_EMAIL").PadRight(_pad_right, ' ') + "@APD_EMAIL01");
        }

        if (GeneralParam.GetBoolean("Account.VisibleField", "Email2", true))
        {
          _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_EMAIL").PadRight(_pad_right, ' ') + "@APD_EMAIL02");
        }
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "TwitterAccount", true))
      {
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_TWITTER").PadRight(_pad_right, ' ') + "@APD_TWITTER");
      }

      //SubBlock Address
      _sb.AppendLine("");
      if (GeneralParam.GetBoolean("Account.VisibleField", "Address", true)
        || GeneralParam.GetBoolean("Account.VisibleField", "Address02", true)
        || GeneralParam.GetBoolean("Account.VisibleField", "Address03", true)
        || GeneralParam.GetBoolean("Account.VisibleField", "City", true)
        || GeneralParam.GetBoolean("Account.VisibleField", "Zip", true)
        || GeneralParam.GetBoolean("Account.VisibleField", "FedEntity", true))
      {
        _sb.AppendLine(Resource.String("STR_ACCOUNT_PERSONAL_DATA_ADDRESS_TITLE"));
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "Address", true))
      {
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_ADDRESS").PadRight(_pad_right, ' ') + "@APD_ADDRESS01");
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "Address02", true))
      {
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_ADDRESS2").PadRight(_pad_right, ' ') + "@APD_ADDRESS02");
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "Address03", true))
      {
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_ADDRESS3").PadRight(_pad_right, ' ') + "@APD_ADDRESS03");
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "City", true))
      {
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_CITY").PadRight(_pad_right, ' ') + "@APD_CITY");
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "Zip", true))
      {
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_DATA_ZIP").PadRight(_pad_right, ' ') + "@APD_ZIP");
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "FedEntity", true))
      {
        _sb.AppendLine(_tab_left + Resource.String("STR_ACCOUNT_PERSONAL_FED_ENTITY").PadRight(_pad_right, ' ') + "@APD_FED_ENTITY");
      }

      _sb.AppendLine("");
      _sb.AppendLine("@APD_BENEFICIARY");

      return _sb.ToString();
    } // CreateDocumentAccountData

    private static Boolean CreateDocumentsForDocImages(String TypeDoc,
                                                       Dictionary<String, String> AccData,
                                                       List<Byte[]> Imgs,
                                                       Boolean IsBeneficiary,
                                                       Int32 TotalPages,
                                                       Int32 InCurrentPage,
                                                       out Int32 OutCurrentPage,
                                                       out MemoryStream MmPdf)
    {
      MmPdf = null;
      OutCurrentPage = 0;

      try
      {
        TemplatePDF _template_pdf;
        Dictionary<String, String> _acc_field;
        List<Byte[]> _ls_img;
        Int32 _out_current_page;
        MemoryStream _pdf_generated;
        String _page;
        Object[] _params;

        _out_current_page = InCurrentPage;
        _template_pdf = null;
        _acc_field = new Dictionary<String, String>();
        _ls_img = new List<Byte[]>();

        //
        // Prepare blocks
        //
        if (IsBeneficiary)
        {
          _acc_field.Add("TitleBlock1", Resource.String("STR_ACCOUNT_PERSONAL_DATA_DOCUMENT_BEN"));
          _acc_field.Add("Block1", Resource.String("STR_ACCOUNT_PERSONAL_DATA_IMG_BEN"));
        }
        else
        {
          _acc_field.Add("TitleBlock1", Resource.String("STR_ACCOUNT_PERSONAL_DATA_DOCUMENT"));
          _acc_field.Add("Block1", Resource.String("STR_ACCOUNT_PERSONAL_DATA_IMG"));
        }

        if (AccData.ContainsKey("@APD_ID_TYPE_STR"))
        {
          AccData.Remove("@APD_ID_TYPE_STR");
        }

        AccData.Add("@APD_ID_TYPE_STR", IdentificationTypes.DocIdTypeStringList(TypeDoc, true));

        //
        // Prepare Images
        //
        _ls_img = new List<Byte[]>();
        for (Int16 _indx = 0; _indx < Imgs.Count; _indx++)
        {
          _out_current_page++;
          _params = new Object[] { "", "" };
          _params[0] = _out_current_page.ToString();
          _params[1] = TotalPages.ToString();
          _page = Resource.String("STR_ACCOUNT_PERSONAL_DATA_PAGE", _params);
          if (_acc_field.ContainsKey("Page"))
          {
            _acc_field.Remove("Page");
          }
          _acc_field.Add("Page", _page);

          _ls_img.Clear();
          _ls_img.Add(Imgs[_indx]);
          _indx++;
          if (_indx < Imgs.Count)
          {
            _ls_img.Add(Imgs[_indx]);
          }

          if (!CreateDocumentImage(_ls_img, _acc_field, AccData, out _pdf_generated))
          {
            return false;
          }

          if (_template_pdf == null)
          {
            _template_pdf = (TemplatePDF)Template.Create(_pdf_generated, Template.TYPE_TEMPLATE.PDF);
            _template_pdf.Fill();
            // LTC 13-NOV-2017
            _template_pdf.SetIsChangedOff();
          }
          else
          {
            _template_pdf.Merge(_pdf_generated);
          }
        }

        MmPdf = (MemoryStream)_template_pdf.GetStream();
        OutCurrentPage = _out_current_page;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("Exception in CreateDocumensForDocImages: " + _ex.Message);
      }

      return false;
    }

    private static Boolean CreateDocumentImage(List<Byte[]> ListImages,
                                               Dictionary<String, String> DataToReplace,
                                               Dictionary<String, String> AccData,
                                               out MemoryStream MmPdf)
    {
      MmPdf = null;

      try
      {
        TemplatePDF _template_pdf;
        String _replace_value;

        using (MemoryStream _template_ms = WSI.Common.Resource.MemoryStream("AccountDataImg"))
        {
          _template_pdf = (TemplatePDF)Template.Create(_template_ms, Template.TYPE_TEMPLATE.PDF);

          foreach (KeyValuePair<String, String> _field in DataToReplace)
          {
            _replace_value = _field.Value;
            _replace_value = _replace_value.TrimStart(Environment.NewLine.ToCharArray());

            foreach (KeyValuePair<String, String> _data in AccData)
            {
              _replace_value = _replace_value.Replace(_data.Key, _data.Value);
            }

            _template_pdf.SetField(_field.Key, _replace_value);
          }

          if (ListImages.Count == 1)
          {
            _template_pdf.SetScaleImage(ListImages[0], new RectangleF(1.5f, 1.5f, 26.5f, 16));
          }
          else if (ListImages.Count == 2)
          {
            _template_pdf.SetScaleImage(ListImages[0], new RectangleF(1.5f, 1.5f, 13, 16));
            _template_pdf.SetScaleImage(ListImages[1], new RectangleF(13.5f, 1.5f, 13, 16));
          }

          _template_pdf.Fill();
          _template_pdf.RotateAllPage();
          // LTC 13-NOV-2017
          _template_pdf.SetIsChangedOff();

          MmPdf = (MemoryStream)_template_pdf.GetStream();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("Exception in CreateDocumentImage: " + _ex.Message);
      }

      return false;
    }

    // PURPOSE: Generic function for load a template and fill the statement
    //
    //  PARAMS:
    //     - INPUT:

    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - MemoryStream: If not filled then null.
    public static MemoryStream CreateDocumentStatement(String AccountData,
                                                       String Title,
                                                       Byte[] Image,
                                                       Currency Played,
                                                       Currency Win,
                                                       Currency Netwin)
    {
      Template _template_pdf;
      MemoryStream _template_ms;
      MemoryStream _pdf_filled;

      _template_pdf = null;
      _pdf_filled = null;
      _template_ms = null;

      try
      {
        _template_ms = WSI.Common.Resource.MemoryStream("WinLossStatement");

        if (_template_ms == null)
        {
          return null;
        }

        _template_pdf = Template.Create(_template_ms, Template.TYPE_TEMPLATE.PDF);

        // Common Fields
        _template_pdf.SetField("Header", GeneralParam.GetString("WinLossStatement", "Header"));
        _template_pdf.SetField("Footer", GeneralParam.GetString("WinLossStatement", "Footer"));
        _template_pdf.SetField("Title", Title);
        _template_pdf.SetField("AccountData", AccountData);
        
        // Bug 30451:WIGOS-5758 WinLoss Statement - Result shows incorrect sign
        _template_pdf.SetField("CoinOutTitle", Resource.String("STR_WIN_LOSS_STATEMENT_PDF_COIN_IN"));
        _template_pdf.SetField("CoinOutValue", Played.ToString());
        _template_pdf.SetField("CoinInTitle", Resource.String("STR_WIN_LOSS_STATEMENT_PDF_COIN_OUT"));
        _template_pdf.SetField("CoinInValue", Win.ToString());

        _template_pdf.SetField("NetWinTitle", Resource.String("STR_WIN_LOSS_STATEMENT_PDF_NETWIN"));
        _template_pdf.SetField("NetWinValue", Netwin.ToString());

        if (Image != null && Image.Length > 0)
        {
          _template_pdf.SetImage(Image, new Rectangle(40, 700, 500, 100)); // TITLE IMAGE
        }

        _pdf_filled = (MemoryStream)_template_pdf.GetStream();

        return _pdf_filled;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
      finally
      {
        if (_template_pdf != null)
        {
          _template_pdf.Close();
        }
      }
    } // CreateDocumentEvidence

    // LTC 20-JUN-2016
    // PURPOSE: Get a list of EvidenceType
    //
    //  PARAMS:
    //     - INPUT:

    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - MemoryStream: If not filled then null.
    public static EVIDENCE_TYPE[] GetListEvidenceType()
    {
      return (EVIDENCE_TYPE[])Enum.GetValues(typeof(EVIDENCE_TYPE));
    }

    /// <summary>
    /// Get template for Evidence type
    /// </summary>
    /// <param name="EvidenceType"></param>
    /// <returns></returns>
    private static Template GetTemplate(EVIDENCE_TYPE EvidenceType)
    {
      Template _template_temp;
      Template.TYPE_TEMPLATE _template_type;
      String _template_name;

      _template_temp = null;
      _template_name = String.Empty;

      switch (EvidenceType)
      {
        case EVIDENCE_TYPE.FEDERAL:
          _template_type = Template.TYPE_TEMPLATE.PDF;
          _template_name = "FederalEvidence";

          _template_temp = GetTemplateFromMemoryStream(_template_type, _template_name);

          break;

        case EVIDENCE_TYPE.ESTATAL:
          _template_type = Template.TYPE_TEMPLATE.PDF;
          _template_name = "StateEvidence";

          _template_temp = GetTemplateFromMemoryStream(_template_type, _template_name);

          break;

        case EVIDENCE_TYPE.ESTATAL_YUCATAN:
          _template_type = Template.TYPE_TEMPLATE.PDF;
          _template_name = "StateEvidenceYucatan";

          _template_temp = GetTemplateFromMemoryStream(_template_type, _template_name);

          break;

        case EVIDENCE_TYPE.ESTATAL_CAMPECHE:
          _template_type = Template.TYPE_TEMPLATE.EXCEL;
          _template_name = "StateEvidenceCampeche";

          _template_temp = GetTemplateFromMemoryStream(_template_type, _template_name);

          break;

        case EVIDENCE_TYPE.FEDERAL_CODERE:
          _template_type = Template.TYPE_TEMPLATE.EXCEL;
          _template_name = "StateEvidenceCodere";

          _template_temp = GetTemplateFromMemoryStream(_template_type, _template_name);

          break;

        default:
          return null;
      }

      return _template_temp;
    }

    /// <summary>
    /// Get template from Template Type
    /// </summary>
    private static Template GetTemplateFromMemoryStream(Template.TYPE_TEMPLATE TemplateType, String TemplateResourceName)
    {
      Template _template_temp;
      MemoryStream _template_ms;

      _template_ms = WSI.Common.Resource.MemoryStream(TemplateResourceName);
      if (_template_ms == null)
      {
        return null;
      }

      _template_temp = Template.Create(_template_ms, TemplateType);

      _template_temp.m_template_ext = GetExtensionFromTemplateType(TemplateType);

      return _template_temp;
    }

    /// <summary>
    /// Get extension from Template Type
    /// </summary>
    private static String GetExtensionFromTemplateType(Template.TYPE_TEMPLATE TemplateType)
    {
      switch (TemplateType)
      {
        case Template.TYPE_TEMPLATE.PDF:
          return "pdf";

        case Template.TYPE_TEMPLATE.EXCEL:
          return "xlsx";

        default:
          throw new ArgumentException("GetExtensionFormTemplateType. Invalid Template type value.");
      }
    }
  } // CashierDocs
}