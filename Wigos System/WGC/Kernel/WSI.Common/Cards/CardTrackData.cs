﻿//------------------------------------------------------------------------------
// Copyright © 2008-2009 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CardTrackData.cs
// 
//   DESCRIPTION: Magnetic card track data analisys
// 
//        AUTHOR: Luis Reyes
// 
// CREATION DATE: 13-APR-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-APR-2018 LRS    First release.
//------------------------------------------------------------------------------
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace WSI.Common
{
	public class ExternalTrackData:CardTrackData
	{
		public ExternalTrackData(String TrackData):base(TrackData)
		{

		}
	}

	public class InternalTrackData:CardTrackData
	{
		public InternalTrackData(String TrackData, CardTrackData.CardsType CardType):base(TrackData, CardType)
		{ }
	
	}

	public class CardTrackData
	{
#region Constants

		private const Int32 CHECKSUM_MODULE = 9973;
		public const String INVALID_TRACKDATA = "* * * * * * * *";
		public const int MAX_TRACK_DATA_CHARS = 13;
		public const int RC4_KEY_SIZE = 64;

#endregion

#region Enums
		public enum CriptMode
		{
			NONE = 0,
			WCP = 1,
		}
		public enum TrackDataType
		{ 
			Internal,
			External
		}

		public enum CardsType:int
		{ 
			PLAYER,
			MOBILE_BANK,
		}
		

#endregion

#region Attributes
			private static byte[] TRACKDATA_CRYPTO_KEY = new byte[] { 35, 33, 94, 72, 71, 94, 36, 35, 70, 68, 83, 72, 74, 118, 98, 45 };
			private static byte[] TRACKDATA_CRYPTO_IV = new byte[] { 1 };
			private int _site_id = 0xFFFF;
      private bool _site_id_setup = false;
#endregion

#region Propertys
		public String InternalTrackData { get; set;}
		public String ExternalTrackData { get; set;}
		public  CardsType CardType { get; set; }
		public Int32 SiteId
		{
			get 
			{ 
        if (_site_id_setup)
        { 
        return _site_id;
        }
				if (AdmitAnyData())
				{ 
					return GeneralParam.GetInt32("Site","Identifier");
				} 
				else if (_site_id == 0xFFFF)
				{
					throw new Exception("Unknow Site Id");
				}
				
				return _site_id;
			}
			set
			{ 
        _site_id_setup = true;
				_site_id = value;
        IsCorrectTrackData = CheckMembers();
			}
		}

		public Int64 Sequence { get; set; }

		public bool IsCorrectTrackData { get; set;}

		public string VisibleTrackData(AccountType AccType)
		{
			
				if (String.IsNullOrEmpty( InternalTrackData ))
				{
					return INVALID_TRACKDATA;
				}

        if (AdmitAnyData())
        {
          return ExternalTrackData;
        }

				switch (AccType)
				{
					case AccountType.ACCOUNT_WIN:
						if ( IsCorrectTrackData)            
						{ 
							return ExternalTrackData;
						}
						break;
					case AccountType.ACCOUNT_VIRTUAL_CASHIER:
					case AccountType.ACCOUNT_VIRTUAL_TERMINAL:
						return "";
						
					default:
						return InternalTrackData;           
						
				}

				return INVALID_TRACKDATA;
		}
#endregion

#region Constructor
		public CardTrackData()
		{ }

		public CardTrackData(String TrackData)
		{ 

    String _decript_Data;
    String _decript_card_type;
				 
         IsCorrectTrackData =  CheckExternalTrackData(TrackData);
         if (! IsCorrectTrackData)
         { 
           return;
         }

         if (!AdmitAnyData())
         { 
				   DecriptExternalTrackData(TrackData, out _decript_Data, out _decript_card_type);
           ExtractDecriptedData (_decript_Data, _decript_card_type);
         }
         else
         { 
            ExternalTrackData=TrackData;
            InternalTrackData=TrackData;
         }

				 

		}

		public CardTrackData(String InternalTrackData, CardsType CardType)
		{

					this.CardType=CardType;
					this.InternalTrackData=InternalTrackData;
					IsCorrectTrackData = TrackDataToExternal();
					
 

		}

#endregion
		
#region Privates

    bool CheckMembers()
    {
      String _members_card_id;
      Int32 _site_id;
      Int32 _ls;
      Int32 _gt;

      _members_card_id = GeneralParam.GetString("MultiSite", "MembersCardId");
      if (String.IsNullOrEmpty(_members_card_id))
      {
        // No MemberCardId defined
        _site_id = GeneralParam.GetInt32("Site", "Identifier");

        return (_site_id == SiteId);
      }

      foreach (String _item in _members_card_id.Split(','))
      {
        if (_item.Contains("-"))
        {
          if (!Int32.TryParse(_item.Split('-')[0], out _ls))
          {

            continue;
          }
          if (!Int32.TryParse(_item.Split('-')[1], out _gt))
          {

            continue;
          }

          if (SiteId >= _ls && SiteId <= _gt)
          {

            return true;
          }
        }
        else
        {
          if (Int32.TryParse(_item, out _site_id))
          {
            if (_site_id == SiteId)
            {

              return true;
            }
          }
        }

      }
      return false;
    }

		bool CheckExternalTrackData(String TrackData)
		{ 
		
			String max_value_str;
      String _dummy;
			Regex _reg_ex;
      String _reg_ex_str;

			if (TrackData.Contains(CardData.VIRTUAL_FLAG_TRACK_DATA) || TrackData.Contains(CardData.RECYCLED_FLAG_TRACK_DATA))
			{
				InternalTrackData = TrackData;

				return true;
			}
			if (AdmitAnyData())
			{ 
			//First check regular expression

        _reg_ex_str = TrackDataRegEx();
        if ( _reg_ex_str == "" )
        {
          return true;
        }

        _reg_ex = new Regex(_reg_ex_str);
				if ( _reg_ex.IsMatch(TrackData))
				{ 
					return true;
				}
        return false;
			}
			// Check for old format compatibility
      if (TrackData.Length < MAX_TRACK_DATA_CHARS)
			{
				// Invalid card number
				return false;
			}

			// Check that incoming parameter is not too big for an UInt64
			max_value_str = UInt64.MaxValue.ToString();
			if (String.Compare(TrackData, max_value_str) > 0)
			{
				return false;
			} // if

			//Try to decript the external data
			if (!DecriptExternalTrackData(TrackData, out _dummy, out _dummy))
			{
				return false;
			}

			return true;
		}

		//------------------------------------------------------------------------------
		// PURPOSE: Receive the track data and split it into the card data components.
		// 
		//  PARAMS:
		//      - INPUT:
		//          - ExternalTrackData: External track data (encoded).
		//
		//      - OUTPUT:
		//          - CardId: internal card identifier. (the one stored in the database)
		//          - CardType: card type.
		//
		// RETURNS:
		//      - True: Incoming track data has been decrypted and the checksum has been 
		//              correctly verified. Output parameters contain the valid results
		//      - False: Incoming track data has some obvious format errors (length) or 
		//               it fails the checksum test.
		// 
		//   NOTES:
		//    Card pattern: (20 digits)
		//      LCCCCSSSSNNNNNNNNNTT
		//    Where:
		//    - L: must be 0 (1 digit)
		//    - CCCC: Checksum. (4 digits)
		//    - SSSSNNNNNNNNN: Internal TrackData. (13 digits)
		//      - SSSS:      Site ID.         (4 digits)
		//      - NNNNNNNNN: Sequence Number. (9 digits)
		//    - TT is the card type (2 digits)
		//
		bool DecriptExternalTrackData(String TrackData, out string DecriptData, out String DecripCardType)
		{ 
				RC4CryptoServiceProvider _rc4_csp;
				ICryptoTransform _decryptor;
				String _leading_digit_str;
				String _checksum_str;
				String _card_id_str;
				String _card_type_str;
				UInt64 _enc_input_data;
				UInt64 _output_data;
				byte[] _output_dec = new byte[8];
				string _external_track_data;
				byte[] _bytes_to_decrypt;
				MemoryStream _ms;
				CryptoStream _cs;
				UInt32 _calc_checksum;
				//String _calc_checksum_str;
				byte[] _checksum_data;
				UInt32 calc_checksum;
				String calc_checksum_str;      
				UInt64 _card_id;

				DecriptData = "";
        DecripCardType = ""; 
				try 
					{
						
						_rc4_csp = new RC4CryptoServiceProvider();
						_rc4_csp.KeySize = RC4_KEY_SIZE;

						// Decrypt card data
						//Get a decryptor that uses the same key and IV as the encryptor.
						_decryptor = _rc4_csp.CreateDecryptor(TRACKDATA_CRYPTO_KEY, TRACKDATA_CRYPTO_IV);
											 
						// Obtain encrypted data
						_enc_input_data = UInt64.Parse(TrackData);

						// Convert the int 64 into a byte array
						
						_bytes_to_decrypt = BitConverter.GetBytes(_enc_input_data);

						// Now decrypt the previously encrypted message using the decryptor
						// obtained in the above step.
						_ms = new MemoryStream(_bytes_to_decrypt);
						_cs = new CryptoStream(_ms, _decryptor, CryptoStreamMode.Read);

						_cs.Read(_output_dec, 0, 8);
						_output_data = BitConverter.ToUInt64(_output_dec, 0);
						// Obtain the 20 digit-long string
						_external_track_data = _output_data.ToString();
						_external_track_data = _external_track_data.PadLeft(20, '0');

						_leading_digit_str = _external_track_data.Substring(0, 1);
						_checksum_str = _external_track_data.Substring(1, 4);
						_card_id_str = _external_track_data.Substring(5, 13);
						_card_type_str = _external_track_data.Substring(18, 2);

								// Check parts
						if (_leading_digit_str != "0")
						{
							// Leading digit must be zero 
							return false;
						}

						_calc_checksum = 0;
						_card_id = UInt64.Parse(String.Concat(_card_id_str, _card_type_str));
						_checksum_data = BitConverter.GetBytes(_card_id);

						for (int i = 0; i < _checksum_data.Length; i++)
						{
							_calc_checksum += (Byte)(_checksum_data[i]);
						}

						calc_checksum = _calc_checksum % CHECKSUM_MODULE;
						calc_checksum_str = string.Format("{0:D4}", calc_checksum);

						if (_checksum_str != calc_checksum_str)
						{
							// Incorrect checksum
							return false;
						}
						

						if (!Enum.IsDefined(typeof(CardsType), int.Parse(_card_type_str)))
						{
							return false;
						}
            

            DecriptData = _card_id_str;
            DecripCardType = _card_type_str;
						return true;		
					}
					catch
					{		
						return false;
					}
	
		}

    void ExtractDecriptedData(String DecriptedData, String DecripCardType)
		{
      try 
      { 
			  // Prepare results
			  InternalTrackData = DecriptedData;
			  CardType = CardsType.PLAYER;
        if (int.Parse(DecripCardType) == (int)CardsType.MOBILE_BANK)
			  {
				  CardType = CardsType.MOBILE_BANK;
			  }

			  SplitInternalTrackData();
      }
      catch 
      { 
        throw new Exception ("Error decripting card track data");
      }
		}
		//------------------------------------------------------------------------------
		// PURPOSE: Split card number into its components:
		//          - Site Id.
		//          - Sequence number.
		// 
		// PARAMS:
		//      - INPUT:
		//          - CardNumber: Composed card internal id.
		//
		//      - OUTPUT:
		//          - SiteId: Site id.
		//          - Sequence: Sequence number.
		//
		// RETURNS:
		//
		// NOTES:
		//
		void SplitInternalTrackData()
		{

			SiteId = Convert.ToInt32(InternalTrackData.Substring(0, 4));
      IsCorrectTrackData = CheckMembers();
  		Sequence = Convert.ToInt64(InternalTrackData.Substring(4));

		} // SplitInternalTrackData


		bool TrackDataToExternal()
		{
			RC4CryptoServiceProvider _rc4_csp;
			String _str_aux;
			String _aux_checksum;
			UInt32 _checksum;
			UInt64 _i64_aux;
			byte[] _checksum_bytes;


			if (AdmitAnyData())
			{
				ExternalTrackData = InternalTrackData;        
				return true;
			}

			// Initialize
			ExternalTrackData = "";

			try
			{
				// RCI & XIT 19-DEC-2013: If the trackdata has the virtual flag, return ok.
				if (InternalTrackData.Contains(CardData.VIRTUAL_FLAG_TRACK_DATA))
				{
					ExternalTrackData = InternalTrackData;
					return false;
				}

				// Check incoming parameters
				if (InternalTrackData.Length != MAX_TRACK_DATA_CHARS)
				{
					return false;
				}

				// Non-supported card type
				if (!Enum.IsDefined(typeof(CardsType),CardType))        
				{    
					return false;
				}

				_rc4_csp = new RC4CryptoServiceProvider();
				_rc4_csp.KeySize = 64;

				// Get an encryptor.
				ICryptoTransform _rc4 = _rc4_csp.CreateEncryptor(TRACKDATA_CRYPTO_KEY, TRACKDATA_CRYPTO_IV);

				// Encrypt the data as an array of encrypted bytes in memory.
				MemoryStream _ms = new MemoryStream();
				CryptoStream _cs = new CryptoStream(_ms, _rc4, CryptoStreamMode.Write);

				// Calculate checksum
				_i64_aux = UInt64.Parse(String.Concat(InternalTrackData, String.Format("{0:D2}", (int)CardType)));
				_checksum_bytes = BitConverter.GetBytes(_i64_aux);

				_checksum = 0;
				for (int i = 0; i < _checksum_bytes.Length; i++)
				{
					_checksum += (Byte)(_checksum_bytes[i]);
				}

				_checksum = _checksum % CHECKSUM_MODULE;
				_aux_checksum = string.Format("{0:D4}", _checksum);

				// Build complete string
				_str_aux = String.Concat(_aux_checksum, InternalTrackData, String.Format("{0:D2}", (int)CardType));

				// Encrypt the formatted buffer
				_i64_aux = UInt64.Parse(_str_aux);

				// Convert the int 64 into a byte arrary
				byte[] _to_encrypt;
				_to_encrypt = BitConverter.GetBytes(_i64_aux);

				// Write all data to the crypto stream and flush it.
				_cs.Write(_to_encrypt, 0, _to_encrypt.Length);
				_cs.FlushFinalBlock();

				// Get the encrypted array of bytes.
				byte[] _ext_trackdata = _ms.ToArray();

				ExternalTrackData = BitConverter.ToUInt64(_ext_trackdata, 0).ToString();
				// Format number using 20 digits (add zero if necessary)
				ExternalTrackData = ExternalTrackData.PadLeft(20, '0');

				SplitInternalTrackData();

				return true;
			}
			catch
			{
				return false; 
			}
		}
#endregion

#region Statics
	/// <summary>
		/// Gets if the system actually admits any thada like Trackdata otherwise with normal crypted data
	/// </summary>
	/// <returns></returns>
		public static bool AdmitAnyData()
		{

			return GeneralParam.GetBoolean("Site", "AnyCard.Enabled",false);
		}

		/// <summary>
		/// Regular expression for validate external track data format
		/// </summary>
		/// <returns></returns>
		public static string TrackDataRegEx()
		{

			return GeneralParam.GetString("Site", "AnyCard.RegEx", "");
		}

    public static bool IsTrackData(String TrackData)
    { 
      ExternalTrackData _ctd;
      _ctd = new ExternalTrackData(TrackData);
      return _ctd.IsCorrectTrackData;
    }

    public static bool IsPartialTrackDataValid(String TrackNumber)
    { 
      Byte[] chars;

      if (AdmitAnyData())
      { 
        return true;
      }

      chars = null;

      try
      {
        chars = Encoding.ASCII.GetBytes(TrackNumber);
        foreach (byte b in chars)
        {
          if (b < '0')
          {
            return false;
          }
          if (b > '9')
          {
            return false;
          }
        }
      }
      finally
      {
        chars = null;
      }

      return true;
    
    }
    public static int MinimumSize()
    { 
      if (AdmitAnyData())
      {
        return GeneralParam.GetInt32("Site", "AnyCard.MinimumSize", 13);
      }
      else 
      { 
        return 20;
      }
    }

    public static int MaximumSize()
    {
      if (AdmitAnyData())
      {
        return 50;
      }
      else
      {
        return 20;
      }
    }

    public static String MakeInternalTrackData(int SiteId, Int64 Sequence)
    {
      return String.Concat(String.Format("{0:D4}", SiteId), String.Format("{0:D9}", Sequence));

    } // MakeInternalTrackData
#endregion
	}
}
