//------------------------------------------------------------------------------
// Copyright © 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashierOperations.cs
// 
//   DESCRIPTION: Class to manage an common cashier operations (Cash in/out...)
// 
//        AUTHOR: Agustí Poch
// 
// CREATION DATE: 23-SEP-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-SEP-2010 APB    First release.
// 22-NOV-2013 SMN    New routines:  CashierMovementsOldHistoryById & CashierMovementsOldHistoryByHour
// 20-MAR-2014 RRR    Added Liabilities old history process.
// 18-ABR-2016 DHA    Product Backlog Item 9950: added chips types
// 17-AUG-2016 RAB    Product Backlog Item 15914: Gaming tables (Phase 5): Order summary box and voucher currency
// 11-MAY-2017 RGR    Bug 26907: Added Cashier Movements Grouped Old History by SessionID
// 20-JUN-2018 AGS    Bug 33223:WIGOS-13078 Gambling tables: Close cash session unbalanced error "There has been an error. Please try again" is displayed in gambling table with integrated cashier when there are credit card and check operations.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using WSI.Common.Junkets;

namespace WSI.Common
{
  public class CashierOperations
  {
    public struct TYPE_CARD_CASH_IN
    {
      public Currency amount_to_pay;
      public Int32 num_tokens;
      public Int64 promotion_id;
      public Currency reward;
      public Currency won_lock;
      public Currency spent_used;
      public JunketsBusinessLogic junketinfo;
    };
  } // CashierOperations

  public static class CashierMovementsGrouped
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Cashier Movements Old History by SessionID
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static bool BySessionID(long sessionID, SqlTransaction trx)
    {
      bool _success;
      _success = false;

      try
      {
        using (SqlCommand _cmd = new SqlCommand("CashierMovementsGroupedPerSessionId", trx.Connection, trx))
        {
          _cmd.CommandTimeout = 0;
          _cmd.CommandType = CommandType.StoredProcedure;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = sessionID;
          _cmd.ExecuteNonQuery();
          _success = true;
        }
      }
      catch (Exception ex)
      {
        _success = false;
        Log.Exception(ex);
      }

      return _success;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Cashier Movements Old History by Id
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean CashierMovementsOldHistoryById(out Int32 ProcessedElements)
    {
      ProcessedElements = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("CashierMovementsOldHistoryById"))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter _param = new SqlParameter("@ElementsToProcess", SqlDbType.Int);
            _param.Direction = ParameterDirection.Output;
            _cmd.Parameters.Add(_param);

            _cmd.CommandTimeout = 15 * 60; // 15 minutes
            _db_trx.ExecuteNonQuery(_cmd);

            ProcessedElements = (Int32)_cmd.Parameters["@ElementsToProcess"].Value;
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }// CashierMovementsOldHistoryById

    //------------------------------------------------------------------------------
    // PURPOSE : Cashier Movements Old History by Hour
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean CashierMovementsOldHistoryByHour(out Int32 HoursToProcess)
    {
      HoursToProcess = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("CashierMovementsOldHistoryByHour"))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter _param = new SqlParameter("@HoursToProcess", SqlDbType.Int);
            _param.Direction = ParameterDirection.Output;
            _cmd.Parameters.Add(_param);

            _cmd.CommandTimeout = 15 * 60; // 15 minutes
            _db_trx.ExecuteNonQuery(_cmd);

            HoursToProcess = (Int32)_cmd.Parameters["@HoursToProcess"].Value;
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }// CashierMovementsOldHistoryByHour

    //------------------------------------------------------------------------------
    // PURPOSE : Liabilities Old History by Hour
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean LiabilitiesOldHistoryByHour(out Int32 HoursToProcess)
    {
      HoursToProcess = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("LiabilitiesOldHistoryByHour"))
          {
            _cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter _param = new SqlParameter("@HoursToProcess", SqlDbType.Int);
            _param.Direction = ParameterDirection.Output;
            _cmd.Parameters.Add(_param);

            _cmd.CommandTimeout = 15 * 60; // 15 minutes
            _db_trx.ExecuteNonQuery(_cmd);

            HoursToProcess = (Int32)_cmd.Parameters["@HoursToProcess"].Value;
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }// LiabilitiesOldHistoryByHour

    //------------------------------------------------------------------------------
    // PURPOSE : Cashier Short/Over Movements Old History
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean CashierShortOverMovementsOldHistory(out Boolean HasSesionsToProcess)
    {
      StringBuilder _sb;
      DataTable _dt_cashier_sessions;
      Int32 _sessions_by_cicle;
      Int64 _cashier_session_id;
      CashierSessionInfo _session_info;
      Cashier.TYPE_CASHIER_SESSION_STATS _cashier_session_data;
      CashierMovementsTable _cashier_movements;
      CurrencyIsoType _cit;
      Int32 _pay_type;
      Decimal _balance_value;
      Decimal _delivered_value;
      Decimal _diff;
      Boolean _calculate_diff;

      HasSesionsToProcess = true;
      _sessions_by_cicle = 1000;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   TOP (@pSessionsByCicle) ");
      _sb.AppendLine("          CS_SESSION_ID           ");
      _sb.AppendLine("        , CS_CASHIER_ID           ");
      _sb.AppendLine("        , CS_USER_ID              ");
      _sb.AppendLine("        , CT_NAME                 ");
      _sb.AppendLine("        , GU_USERNAME             ");
      _sb.AppendLine("        , CS_CLOSING_DATE         ");
      _sb.AppendLine("        , CS_BALANCE              ");
      _sb.AppendLine("        , CS_COLLECTED_AMOUNT     ");
      _sb.AppendLine("        , CAST(CASE WHEN GU_USER_TYPE = @pUserType  ");    // Cashier
      _sb.AppendLine("                AND CT_GAMING_TABLE_ID IS NULL      ");    // Not Integrated
      _sb.AppendLine("          THEN 1                                    ");    // Should be or not calculate
      _sb.AppendLine("          ELSE 0 END AS BIT) AS NEED_CALCULATE_DIFF ");    
      _sb.AppendLine("   FROM   CASHIER_SESSIONS WITH(INDEX(IX_CS_SHORT_OVER_HISTORY))  ");
      _sb.AppendLine("   LEFT   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID ");
      _sb.AppendLine("   LEFT   JOIN GUI_USERS ON CS_USER_ID = GU_USER_ID               ");
      _sb.AppendLine("  WHERE   CS_SHORT_OVER_HISTORY = 0  ");    // Not historified
      _sb.AppendLine("    AND   CS_STATUS = @pStatusClosed ");    // Close session
      _sb.AppendLine("  ORDER   BY CS_SHORT_OVER_HISTORY ASC ");
      _sb.AppendLine("        , CS_CLOSING_DATE DESC         ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pSessionsByCicle", SqlDbType.Int).Value = _sessions_by_cicle;
            _cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.CLOSED;
            _cmd.Parameters.Add("@pUserType", SqlDbType.SmallInt).Value = GU_USER_TYPE.USER;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _dt_cashier_sessions = new DataTable("CASHIER_SESSIONS");
              _sql_da.Fill(_dt_cashier_sessions);
            }
          }
        }

        foreach (DataRow _row in _dt_cashier_sessions.Rows)
        {
          _cashier_session_id = (Int64)_row["CS_SESSION_ID"];
          _calculate_diff = (Boolean)_row["NEED_CALCULATE_DIFF"];

          _cashier_session_data = new Cashier.TYPE_CASHIER_SESSION_STATS();
          _cashier_session_data.currencies_balance = new Dictionary<CurrencyIsoType, Decimal>();
          _cashier_session_data.collected = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
          _cashier_session_data.collected_diff = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());


          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (_calculate_diff)
              {
                _cit = new CurrencyIsoType();
                _cit.Type = CurrencyExchangeType.CURRENCY;
                _cit.IsoCode = CurrencyExchange.GetNationalCurrency();

                Cashier.ReadCashierSessionData(_db_trx.SqlTransaction, _cashier_session_id, ref _cashier_session_data);

                // National currency
                _balance_value = _cashier_session_data.final_balance;
                _delivered_value = !_row.IsNull("CS_COLLECTED_AMOUNT") ? (Decimal)_row["CS_COLLECTED_AMOUNT"] : 0;
                CalculateShortOver(_cit, _balance_value, _delivered_value, ref _cashier_session_data);

                // Rest of currencies (exchange currency, check, card, ...): It calls CalculateShortOver()
                FillCashierSessionCurrenciesDataFromSession(_cashier_session_id, ref _cashier_session_data);

                _session_info = new CashierSessionInfo();
                _session_info.CashierSessionId = _cashier_session_id;
                _session_info.TerminalId = (Int32)_row["CS_CASHIER_ID"];
                _session_info.TerminalName = (String)_row["CT_NAME"];
                _session_info.UserId = (Int32)_row["CS_USER_ID"];
                _session_info.UserName = (String)_row["GU_USERNAME"];
                _session_info.AuthorizedByUserId = (Int32)_row["CS_USER_ID"];
                _session_info.AuthorizedByUserName = (String)_row["GU_USERNAME"];
                //_session_info.GamingDay = CommonCashierInformation.GamingDay;
                _session_info.IsMobileBank = false;

                _cashier_movements = new CashierMovementsTable(_session_info);

                foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in _cashier_session_data.currencies_balance)
                {
                  _diff = _cashier_session_data.collected_diff.ContainsKey(_currency.Key) ? _cashier_session_data.collected_diff[_currency.Key] : 0;

                  _pay_type = 0;
                  switch (_currency.Key.Type)
                  {
                    case CurrencyExchangeType.CHECK:
                      _pay_type = Cage.CHECK_CODE;

                      break;
                    case CurrencyExchangeType.CARD:
                      _pay_type = Cage.BANK_CARD_CODE;

                      break;
                  }

                  if (_diff != 0)
                  {
                    if (_diff < 0)
                    {
                      // added new movement if there is cash short on closing cashier session
                      _cashier_movements.Add(0, CASHIER_MOVEMENT.CASH_CLOSING_SHORT, Math.Abs(_diff), 0, "", "", _currency.Key.IsoCode, _pay_type, 0, 0, -1, _currency.Key.Type, FeatureChips.ConvertToCageCurrencyType(_currency.Key.Type), null);
                    }
                    else if (_diff > 0)
                    {
                      // added new movement if there is cash over on closing cashier session
                      _cashier_movements.Add(0, CASHIER_MOVEMENT.CASH_CLOSING_OVER, _diff, 0, "", "", _currency.Key.IsoCode, _pay_type, 0, 0, -1, _currency.Key.Type, FeatureChips.ConvertToCageCurrencyType(_currency.Key.Type), null);
                    }
                    _cashier_movements.SetDateInLastMov((DateTime)_row["CS_CLOSING_DATE"]);
                    _cashier_movements.SetInitialAndFinalBalanceAmountInLastMov((Decimal)_row["CS_BALANCE"], (Decimal)_row["CS_BALANCE"]);
                  }

                } // foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in _cashier_session_stats.currencies_balance)

                if (_cashier_movements.RowsCount() != 0)
                {
                  _sb = new StringBuilder();
                  _sb.AppendLine(" UPDATE   CASHIER_SESSIONS                      ");
                  _sb.AppendLine("    SET   CS_STATUS           = @pStatusOpen    ");
                  _sb.AppendLine("  WHERE   CS_SESSION_ID       = @pSessionId     ");
                  _sb.AppendLine("    AND   CS_STATUS           = @pStatusClosed  ");

                  using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
                  {
                    _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = _cashier_session_id;
                    _cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.CLOSED;
                    _cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

                    try
                    {
                      if (_cmd.ExecuteNonQuery() == 0)
                      {
                        continue;
                      }
                    }
                    catch (Exception _ex)
                    {
                      Log.Exception(_ex);
                      continue;
                    }
                  }

                  if (!_cashier_movements.Save(_db_trx.SqlTransaction))
                  {
                    continue;
                  }
                } // if (_cashier_movements.RowsCount() != 0)
              } // if (_calculate_diff)

              _sb = new StringBuilder();
              _sb.AppendLine(" UPDATE   CASHIER_SESSIONS                      ");
              _sb.AppendLine("    SET   CS_STATUS           = @pStatusClosed  ");
              _sb.AppendLine("        , CS_SHORT_OVER_HISTORY = 1             ");    // Historified
              _sb.AppendLine("  WHERE   CS_SESSION_ID       = @pSessionId     ");
              //_sb.AppendLine("    AND   CS_STATUS           = @pStatusOpen    ");

              using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
              {
                _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = _cashier_session_id;
                _cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.CLOSED;
                _cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

                _cmd.ExecuteNonQuery();
              }

              _db_trx.Commit();

            } // using (DB_TRX _db_trx = new DB_TRX())
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
            continue;
          }

        } // foreach (DataRow _row in _dt_cashier_sessions.Rows)

        HasSesionsToProcess = (_dt_cashier_sessions.Rows.Count == _sessions_by_cicle);

        return true;

      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }  // CashierShortOverMovementsOldHistory

    //------------------------------------------------------------------------------
    // PURPOSE : Read Cashier Session Currencies
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES : this function is simmilar to Cashier.FillCashierSessionCurrenciesDataFromDt
    //
    public static void FillCashierSessionCurrenciesDataFromSession(Int64 _session_id, ref Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      StringBuilder _sb;
      DataTable _dt;
      CurrencyIsoType _cit;
      Decimal _balance_value;
      Decimal _delivered_value;

      _dt = new DataTable("CASHIER_SESSIONS_BY_CURRENCY");

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   CSC_ISO_CODE  ");
      _sb.AppendLine("       , CSC_TYPE      ");
      _sb.AppendLine("       , CSC_BALANCE   ");
      _sb.AppendLine("       , CSC_COLLECTED ");
      _sb.AppendLine("  FROM   CASHIER_SESSIONS_BY_CURRENCY  ");
      _sb.AppendLine(" WHERE   CSC_SESSION_ID =  @pSessionId ");
      _sb.AppendLine(" ORDER   BY CSC_ISO_CODE ");
      _sb.AppendLine("       , CSC_TYPE        ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = _session_id;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_dt);
            }
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      foreach (DataRow _dr in _dt.Rows)
      {
        //CSC_SESSION_ID, CSC_ISO_CODE, CSC_TYPE, CSC_BALANCE, CSC_COLLECTED
        _cit = new CurrencyIsoType();
        _cit.IsoCode = (String)_dr["CSC_ISO_CODE"];
        _cit.Type = (CurrencyExchangeType)_dr["CSC_TYPE"];

        _balance_value = !_dr.IsNull("CSC_BALANCE") ? (Decimal)_dr["CSC_BALANCE"] : 0;
        _delivered_value = !_dr.IsNull("CSC_COLLECTED") ? (Decimal)_dr["CSC_COLLECTED"] : 0;

        CalculateShortOver(_cit, _balance_value, _delivered_value, ref CashierSessionData);
      }

    } // FillCashierSessionCurrenciesDataFromDt

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate short/over
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    private static void CalculateShortOver(CurrencyIsoType Cit, Decimal Balance, Decimal Collected, ref Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {

      // Balance
      if (CashierSessionData.currencies_balance.ContainsKey(Cit))
      {
        CashierSessionData.currencies_balance[Cit] = Balance;
      }
      else
      {
        CashierSessionData.currencies_balance.Add(Cit, Balance);
      }

      // Collected
      if (CashierSessionData.collected.ContainsKey(Cit))
      {
        CashierSessionData.collected[Cit] = Collected;
      }
      else
      {
        CashierSessionData.collected.Add(Cit, Collected);
      }

      // Difference
      if (CashierSessionData.collected_diff.ContainsKey(Cit))
      {
        CashierSessionData.collected_diff[Cit] = Collected - Balance;
      }
      else
      {
        CashierSessionData.collected_diff.Add(Cit, Collected - Balance);
      }

    }

  } // CashierMovementsGrouped

  public class HistoricalCashierSessions
  {
    private const Int32 MAX_ELLAPSED_TIME_LOG_MS = 10000;
    private static Thread m_thread;

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize HistoricalData class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Init()
    {
      m_thread = new Thread(HistoricalCashierSessionsThread);
      m_thread.Name = "HistoricalCashierSessionsThread";

      m_thread.Start();
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Thread process for historical cashier sessions
    //
    //  PARAMS :
    //      - INPUT : 
    //                
    //      - OUTPUT : 
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void HistoricalCashierSessionsThread()
    {
      Int64 _ellapsed;
      Int32 _tick0;
      String _function;
      Int32 _num_processed;
      Int32 _num_hours_to_process;
      Boolean _has_sessions_to_process;
      Boolean _history_update_needed;
      Boolean _history_by_hour_update_needed;
      Boolean _liabilities_history_by_hour_update_needed;
      Boolean _short_over_history_create_needed;
      Int32 _wait_seconds;
      String _date_range;
      Int32 _date_range_limit_1;
      Int32 _date_range_limit_2;
      Int32 _now_hour;
      Boolean _slept;

      _function = "";
      _history_update_needed = true;
      _history_by_hour_update_needed = true;
      _liabilities_history_by_hour_update_needed = true;
      _short_over_history_create_needed = true;

      _wait_seconds = GeneralParam.GetInt32("HistoricalData", "OldCashierSessions.Step.WaitSeconds", 30, 1, 300); // [1s, 5m]
      _date_range = GeneralParam.GetString("HistoricalData", "OldCashierSessions.DateRange", "");

      //0-24
      try
      {
        _date_range_limit_1 = Int32.Parse(_date_range.Split('-')[0]);
        _date_range_limit_2 = Int32.Parse(_date_range.Split('-')[1]);

        if (_date_range_limit_1 > _date_range_limit_2)
        {
          _date_range_limit_1 = 0;
          _date_range_limit_2 = 24;
        }
      }
      catch
      {
        Log.Error("General Param: HistoricalData.OldCashierSessions.DateRange : Format Incorrect");
        _date_range_limit_1 = 0;
        _date_range_limit_2 = 24;
      }

      _slept = false;

      while (true)
      {
        try
        {
          if (!_history_update_needed && !_history_by_hour_update_needed && !_liabilities_history_by_hour_update_needed && !_short_over_history_create_needed)
          {
            break;
          }

          if (!_slept)
          {
            Thread.Sleep(_wait_seconds * 1000);
          }
          _slept = false;

          if (!Services.IsPrincipal("WCP"))
          {
            continue;
          }

          _now_hour = WGDB.Now.Hour;

          if (_now_hour < _date_range_limit_1 || _now_hour >= _date_range_limit_2)
          {
            continue;
          }

          // CashierMovementsOldHistoryById
          if (_history_update_needed)
          {
            _tick0 = Environment.TickCount;
            _function = "CashierMovementsOldHistoryById";

            if (!CashierMovementsGrouped.CashierMovementsOldHistoryById(out _num_processed))
            {
              Log.Warning(String.Format("HistoricalCashierSessionsThread. Exception in function: {0}", _function));
            }
            else
            {
              _history_update_needed = (_num_processed > 0);
            }

            _ellapsed = Misc.GetElapsedTicks(_tick0);

            if (_ellapsed > MAX_ELLAPSED_TIME_LOG_MS)
            {
              Log.Warning(String.Format("HistoricalCashierSessionsThread [{0}], Duration: {1} ms.", _function, _ellapsed));
            }

            Thread.Sleep(_wait_seconds * 1000);
            _slept = true;
          }

          // CashierMovementsOldHistoryByHour
          if (_history_by_hour_update_needed)
          {
            _tick0 = Environment.TickCount;
            _function = "CashierMovementsOldHistoryByHour";

            if (!CashierMovementsGrouped.CashierMovementsOldHistoryByHour(out _num_hours_to_process))
            {
              Log.Warning(String.Format("HistoricalCashierSessionsThread. Exception in function: {0}", _function));
            }
            else
            {
              _history_by_hour_update_needed = (_num_hours_to_process != 0);
            }

            _ellapsed = Misc.GetElapsedTicks(_tick0);

            if (_ellapsed > MAX_ELLAPSED_TIME_LOG_MS)
            {
              Log.Warning(String.Format("HistoricalCashierSessionsThread [{0}], Duration: {1} ms.", _function, _ellapsed));
            }

            Thread.Sleep(_wait_seconds * 1000);
            _slept = true;
          }

          // LiabilitiesOldHistoryByHour
          if (_liabilities_history_by_hour_update_needed)
          {
            _tick0 = Environment.TickCount;
            _function = "LiabilitiesOldHistoryByHour";

            if (!CashierMovementsGrouped.LiabilitiesOldHistoryByHour(out _num_hours_to_process))
            {
              Log.Warning(String.Format("HistoricalCashierSessionsThread. Exception in function: {0}", _function));
            }
            else
            {
              _liabilities_history_by_hour_update_needed = (_num_hours_to_process != 0);
            }

            _ellapsed = Misc.GetElapsedTicks(_tick0);

            if (_ellapsed > MAX_ELLAPSED_TIME_LOG_MS)
            {
              Log.Warning(String.Format("HistoricalCashierSessionsThread [{0}], Duration: {1} ms.", _function, _ellapsed));
            }

            Thread.Sleep(_wait_seconds * 1000);
            _slept = true;
          }

          // TODO BY WINIONS (20-08-2015): Revisar para la versión definitiva la historificación de sesiones de caja antiguas
          if (_short_over_history_create_needed)
          {
            _tick0 = Environment.TickCount;
            _function = "CashierShortOverMovementsOldHistory";

            if (!CashierMovementsGrouped.CashierShortOverMovementsOldHistory(out _has_sessions_to_process))
            {
              Log.Warning(String.Format("CashierShortOverMovementsOldHistory. Exception in function: {0}", _function));
            }
            else
            {
              _short_over_history_create_needed = _has_sessions_to_process;
            }

            _ellapsed = Misc.GetElapsedTicks(_tick0);

            if (_ellapsed > MAX_ELLAPSED_TIME_LOG_MS)
            {
              Log.Warning(String.Format("HistoricalCashierSessionsThread [{0}], Duration: {1} ms.", _function, _ellapsed));
            }

            Thread.Sleep(_wait_seconds * 1000);
            _slept = true;
          }

        }
        catch (Exception _ex)
        {
          Log.Warning(String.Format("HistoricalCashierSessionsThread. Exception in function: {0}", _function));
          Log.Exception(_ex);
        }

      } // while

      Log.Message("HistoricalCashierSessionThread has finished the job.");

    } // HistoricalCashierSessionsThread

  } // HistoricalCashierSessions
}




