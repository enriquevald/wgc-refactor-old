//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WinLossStatement.cs
// 
//   DESCRIPTION: Class to manage WinLossStatement
// 
//        AUTHOR: Fernando Jim�nez
// 
// CREATION DATE: 22-APR-2015
// 
// REVISION HISTORY:
// 
// Date        Author   Description
// ----------- -------- ----------------------------------------------------------
// 22-APR-2015 FJC      First release.
// 23-NOV-2015 FOS      Bug 6771:Payment tickets & handpays
// 27-OCT-2017 JML-RAB  Bug 30451:WIGOS-5758 WinLoss Statement - Result shows incorrect sign     
// 20-DEC-2017 DHA      Bug 31123:WIGOS-7299 Win loss statement document does not take into account the Jackpot amount
//--------------------------------------------------------------------------------

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Drawing.Printing;

namespace WSI.Common
{
  public partial class WinLossStatement
  {

    #region Attributes

    public struct WIN_LOSS_STATEMENT
    {
      public Int64 AccountId;
      public String TrackData;
      public String HolderName;
      public String Adress;
      public String City;
      public String Zip;
      public String State;
      public Int32 RequestYear;
      public DateTime DateFrom;
      public DateTime DateTo;
      public DateTime RequestDate;
      public Int32 RequestUserId;
      public DateTime PrintDate;
      public Int32 PrintUserId;
      public Currency PlayedAmount;
      public Currency WonAmount;
      public Currency JackpotAmount;
      public Currency NetWinLoss;
      public WinLossStatementStatus Status;
      public WinLossStatementStatus StatusPrev;
      public WinLossStatementStatusPrint StatusPrint;
      public WinLossStatementStatusPrint StatusPrintPrev;
      public Int64 DocumentId;
    } // WIN_LOSS_STATEMENT

    #endregion Attributes

    #region 'Enums'

    public enum WinLossStatementStatus
    {
      Pending = 0,
      InProgress = 1,
      Ready = 2,
      NotExists = 3
    }

    public enum WinLossStatementStatusPrint
    {
      NotPrinted = 0,
      Printed = 1
    }

    #endregion

    #region 'Public Methods'

    //------------------------------------------------------------------------------
    // PURPOSE : Insert WinLossStatement
    //
    //  PARAMS :
    //      - INPUT :
    //          - Statement
    //          - Trx
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True: If "WinLossStatement" has been created correctly
    //          - False: Otherwise
    //
    public static Boolean InsertWinLossStatement(WIN_LOSS_STATEMENT Statement,
                                                 SqlTransaction Trx)
    {
      StringBuilder _sb;

      DateTime _date_from;
      DateTime _date_to;

      try
      {
        _sb = new StringBuilder();


        //Calculate Dates
        if (!CalculateDates(Statement.RequestYear, out _date_from, out _date_to))
        {

          return false;
        }

        ////_sb.AppendLine(" INSERT INTO   WIN_LOSS_STATEMENTS ");
        ////_sb.AppendLine("             (                     ");
        ////_sb.AppendLine("               WLS_ACCOUNT_ID      ");
        ////_sb.AppendLine("             , WLS_DATE_FROM       ");
        ////_sb.AppendLine("             , WLS_DATE_TO         ");
        ////_sb.AppendLine("             , WLS_REQUEST_DATE    ");
        ////_sb.AppendLine("             , WLS_STATUS          ");
        ////_sb.AppendLine("             , WLS_REQUEST_USER_ID ");
        ////_sb.AppendLine("             )                     ");
        ////_sb.AppendLine("               VALUES              ");
        ////_sb.AppendLine("             (                     ");
        ////_sb.AppendLine("               @pAccountId         ");
        ////_sb.AppendLine("             , @pDateFrom          ");
        ////_sb.AppendLine("             , @pDateTo            ");
        ////_sb.AppendLine("             , GETDATE()           ");
        ////_sb.AppendLine("             , @pStatus            ");
        ////_sb.AppendLine("             , @pRequestUserId     ");
        ////_sb.AppendLine("             )                     ");

        _sb.AppendLine(" IF NOT EXISTS ( SELECT   WLS_ACCOUNT_ID                                  ");
        _sb.AppendLine("                   FROM   WIN_LOSS_STATEMENTS                             ");
        _sb.AppendLine("                  WHERE   WLS_ACCOUNT_ID = @pAccountId                    ");
        _sb.AppendLine("                    AND   WLS_DATE_FROM  = @pDateFrom                     ");
        _sb.AppendLine("                    AND   WLS_DATE_TO    = @pDateTo)                      ");
        _sb.AppendLine(" BEGIN                                                                    ");

        _sb.AppendLine("      INSERT INTO   WIN_LOSS_STATEMENTS                                   ");
        _sb.AppendLine("             (                                                            ");
        _sb.AppendLine("               WLS_ACCOUNT_ID                                             ");
        _sb.AppendLine("             , WLS_DATE_FROM                                              ");
        _sb.AppendLine("             , WLS_DATE_TO                                                ");
        _sb.AppendLine("             , WLS_REQUEST_DATE                                           ");
        _sb.AppendLine("             , WLS_STATUS                                                 ");
        _sb.AppendLine("             , WLS_REQUEST_USER_ID                                        ");
        _sb.AppendLine("             )                                                            ");
        _sb.AppendLine("               VALUES                                                     ");
        _sb.AppendLine("             (                                                            ");
        _sb.AppendLine("               @pAccountId                                                ");
        _sb.AppendLine("             , @pDateFrom                                                 ");
        _sb.AppendLine("             , @pDateTo                                                   ");
        _sb.AppendLine("             , GETDATE()                                                  ");
        _sb.AppendLine("             , @pStatus                                                   ");
        _sb.AppendLine("             , @pRequestUserId                                            ");
        _sb.AppendLine("             )                                                            ");
        _sb.AppendLine(" END                                                                      ");
        _sb.AppendLine(" ELSE                                                                     ");
        _sb.AppendLine("      UPDATE   WIN_LOSS_STATEMENTS                                        ");
        _sb.AppendLine("         SET   WLS_PRINT         = @pStatusPrint                          ");
        _sb.AppendLine("             , WLS_PRINT_USER_ID = @pUserPrint                            ");
        _sb.AppendLine("             , WLS_PRINT_DATE    = @pDatePrint                            ");
        _sb.AppendLine("       WHERE   WLS_ACCOUNT_ID    = @pAccountId                            ");
        _sb.AppendLine("         AND   WLS_DATE_FROM     = @pDateFrom                             ");
        _sb.AppendLine("         AND   WLS_DATE_TO       = @pDateTo                               ");
        _sb.AppendLine("         AND   WLS_STATUS        = @pStatusPrev                           ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Statement.AccountId;
          _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = _date_from;
          _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = _date_to;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Statement.Status;
          _cmd.Parameters.Add("@pStatusPrev", SqlDbType.Int).Value = Statement.StatusPrev;
          _cmd.Parameters.Add("@pStatusPrint", SqlDbType.Int).Value = WinLossStatementStatusPrint.NotPrinted;
          _cmd.Parameters.Add("@pUserPrint", SqlDbType.BigInt).Value = DBNull.Value;
          _cmd.Parameters.Add("@pDatePrint", SqlDbType.DateTime).Value = DBNull.Value;
          _cmd.Parameters.Add("@pRequestUserId", SqlDbType.BigInt).Value = Statement.RequestUserId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("Error on WCP_AccountsWinLossStatements.InsertWinLossStatement AccountId: " + Statement.AccountId.ToString() +
              " Period: " + Statement.DateFrom.ToString() + " - " + Statement.DateTo.ToString());

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on WCP_AccountsWinLossStatements.InsertWinLossStatement AccountId: " + Statement.AccountId.ToString() +
          " Period: " + Statement.DateFrom.ToString() + " - " + Statement.DateTo.ToString());
        Log.Exception(_ex);
      }

      return false;
    } // InsertWinLossStatement


    //------------------------------------------------------------------------------
    // PURPOSE : Return true if exists a WinLossStatement for one account id
    //
    //  PARAMS :
    //      - INPUT:
    //            - AccountId
    //            - RequestYear
    //            - RequestStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //public Boolean ExistsWinLossStatement(WIN_LOSS_STATEMENT Statement,
    //                                      SqlTransaction SqlTrx)
    //{
    //  StringBuilder _sql_str;
    //  Object _exists_win_loss_statement;

    //  try
    //  {
    //    //Search the last provided hour
    //    _sql_str = new StringBuilder();
    //    _sql_str.AppendLine("SELECT   WLS_ACCOUNT_ID                  ");
    //    _sql_str.AppendLine("  FROM   WIN_LOSS_STATEMENTS             ");
    //    _sql_str.AppendLine(" WHERE   WLS_ACCOUNT_ID    = @pAccountId ");
    //    _sql_str.AppendLine("   AND   WLS_FROM  = @pYear      ");
    //    _sql_str.AppendLine("   AND   WLS_STATUS        = @pStatus    ");

    //    using (SqlCommand _sql_cmd = new SqlCommand(_sql_str.ToString(), SqlTrx.Connection, SqlTrx))
    //    {
    //      _sql_cmd.Connection = SqlTrx.Connection;
    //      _sql_cmd.Transaction = SqlTrx;
    //      _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Statement.AccountId;
    //      _sql_cmd.Parameters.Add("@pRequestYear", SqlDbType.Int).Value = RequestYear;
    //      _sql_cmd.Parameters.Add("@pRequestStatus", SqlDbType.Int).Value = RequestStatus;

    //      _exists_win_loss_statement = _sql_cmd.ExecuteScalar();
    //    }
    //    if (_exists_win_loss_statement == DBNull.Value)
    //    {
    //      return false;
    //    }

    //    return true;
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);

    //    return false;
    //  }
    //} // ExistsWinLossStatement

    //------------------------------------------------------------------------------
    // PURPOSE : Return true if exists a WinLossStatement for one account id
    //
    //  PARAMS :
    //      - INPUT:
    //            - AccountId
    //            - RequestYear
    //            - RequestStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetWinLossStatement(ref WIN_LOSS_STATEMENT Statement,
                                              SqlTransaction SqlTrx)
    {
      StringBuilder _sql_str;
      DataTable _dt_win_loss_statement;
      DateTime _date_from;
      DateTime _date_to;

      try
      {

        _dt_win_loss_statement = new DataTable();
        Statement.DocumentId = -1;

        //Calculate Dates
        if (Statement.RequestYear > 0)
        {
          CalculateDates(Statement.RequestYear, out _date_from, out _date_to);
        }
        else
        {
          _date_from = Statement.DateFrom;
          _date_to = Statement.DateTo;
        }

        //Search the last provided hour
        _sql_str = new StringBuilder();
        _sql_str.AppendLine("    SELECT   WLS_ACCOUNT_ID                        ");
        _sql_str.AppendLine("           , AC_HOLDER_NAME                        ");
        _sql_str.AppendLine("           , AC_HOLDER_ADDRESS_01                  ");
        _sql_str.AppendLine("           , AC_HOLDER_CITY                        ");
        _sql_str.AppendLine("           , AC_HOLDER_ZIP                         ");
        _sql_str.AppendLine("           , FS_NAME                               ");
        _sql_str.AppendLine("           , WLS_DATE_FROM                         ");
        _sql_str.AppendLine("           , WLS_DATE_TO                           ");
        _sql_str.AppendLine("           , WLS_STATUS                            ");
        _sql_str.AppendLine("           , WLS_REQUEST_DATE                      ");
        _sql_str.AppendLine("           , WLS_REQUEST_USER_ID                   ");
        _sql_str.AppendLine("           , WLS_PRINT                             ");
        _sql_str.AppendLine("           , WLS_PLAYED_AMOUNT                     ");
        _sql_str.AppendLine("           , WLS_WON_AMOUNT                        ");
        _sql_str.AppendLine("           , WLS_JACKPOT_AMOUNT                    ");
        _sql_str.AppendLine("           , WLS_DOCUMENT_ID                       ");
        _sql_str.AppendLine("      FROM   ACCOUNTS                              ");
        _sql_str.AppendLine(" LEFT JOIN   FEDERAL_STATES                        ");
        _sql_str.AppendLine("        ON   AC_HOLDER_FED_ENTITY = FS_STATE_ID    ");
        _sql_str.AppendLine("INNER JOIN   WIN_LOSS_STATEMENTS                   ");
        _sql_str.AppendLine("        ON   AC_ACCOUNT_ID = WLS_ACCOUNT_ID        ");
        _sql_str.AppendLine("       AND   WLS_ACCOUNT_ID =  @pAccountId         ");
        _sql_str.AppendLine("       AND   WLS_DATE_FROM =   @pDateFrom          ");
        _sql_str.AppendLine("       AND   WLS_DATE_TO = @pDateTo                ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Connection = SqlTrx.Connection;
          _sql_cmd.Transaction = SqlTrx;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Statement.AccountId;
          _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = _date_from;
          _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = _date_to;

          // To optimize the query
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(_dt_win_loss_statement);
          }
        }

        if (_dt_win_loss_statement != null && _dt_win_loss_statement.Rows.Count > 0)
        {

          if (_dt_win_loss_statement.Rows[0]["AC_HOLDER_NAME"] != DBNull.Value)
          {
            Statement.HolderName = (String)_dt_win_loss_statement.Rows[0]["AC_HOLDER_NAME"];
          }

          if (_dt_win_loss_statement.Rows[0]["AC_HOLDER_ADDRESS_01"] != DBNull.Value)
          {
            Statement.Adress = (String)_dt_win_loss_statement.Rows[0]["AC_HOLDER_ADDRESS_01"];
          }

          if (_dt_win_loss_statement.Rows[0]["AC_HOLDER_CITY"] != DBNull.Value)
          {
            Statement.City = (String)_dt_win_loss_statement.Rows[0]["AC_HOLDER_CITY"];
          }

          if (_dt_win_loss_statement.Rows[0]["AC_HOLDER_ZIP"] != DBNull.Value)
          {
            Statement.Zip = (String)_dt_win_loss_statement.Rows[0]["AC_HOLDER_ZIP"];
          }

          if (_dt_win_loss_statement.Rows[0]["FS_NAME"] != DBNull.Value)
          {
            Statement.State = (String)_dt_win_loss_statement.Rows[0]["FS_NAME"];
          }

          Statement.DateFrom = (DateTime)_dt_win_loss_statement.Rows[0]["WLS_DATE_FROM"];
          Statement.DateTo = (DateTime)_dt_win_loss_statement.Rows[0]["WLS_DATE_TO"];
          Statement.Status = (WinLossStatementStatus)_dt_win_loss_statement.Rows[0]["WLS_STATUS"];
          Statement.RequestDate = (DateTime)_dt_win_loss_statement.Rows[0]["WLS_REQUEST_DATE"];
          Statement.RequestUserId = (Int32)_dt_win_loss_statement.Rows[0]["WLS_REQUEST_USER_ID"];
          Statement.StatusPrint = ((Boolean)_dt_win_loss_statement.Rows[0]["WLS_PRINT"]) ?
                                    WinLossStatement.WinLossStatementStatusPrint.Printed :
                                    WinLossStatement.WinLossStatementStatusPrint.NotPrinted;

          Statement.PlayedAmount = (Decimal)_dt_win_loss_statement.Rows[0]["WLS_PLAYED_AMOUNT"];
          Statement.WonAmount = (Decimal)_dt_win_loss_statement.Rows[0]["WLS_WON_AMOUNT"];
          Statement.JackpotAmount = (Decimal)_dt_win_loss_statement.Rows[0]["WLS_JACKPOT_AMOUNT"];
          Statement.NetWinLoss = Statement.PlayedAmount - Statement.WonAmount;

          if (_dt_win_loss_statement.Rows[0]["WLS_DOCUMENT_ID"] != DBNull.Value)
          {
            Statement.DocumentId = (Int64)_dt_win_loss_statement.Rows[0]["WLS_DOCUMENT_ID"];
          }


          return true;
        }
        else
        {
          //if not take last provider restard parameters in form win_loss_statement.

          Statement.Status = WinLossStatementStatus.NotExists;
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }

      return false;
    } // GetWinLossStatement

    //------------------------------------------------------------------------------
    // PURPOSE : Update win loss statement status
    //
    //  PARAMS :
    //      - INPUT :
    //          - Statement
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //
    static public Boolean UpdateWinLossStatementGenerate(WIN_LOSS_STATEMENT Statement)
    {

      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   UPDATE   WIN_LOSS_STATEMENTS                    ");
        _sb.AppendLine("      SET   WLS_STATUS          = @pStatus         ");
        _sb.AppendLine("          , WLS_GENERATION_DATE = @pGenerationDate ");
        _sb.AppendLine("    WHERE   WLS_ACCOUNT_ID = @pAccountId           ");
        _sb.AppendLine("      AND   WLS_DATE_FROM  = @pDateFrom            ");
        _sb.AppendLine("      AND   WLS_DATE_TO    = @pDateTo              ");
        _sb.AppendLine("      AND   WLS_STATUS     = @pStatusPrev          ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Statement.Status;//WinLossStatementStatus.InProgress;
            _cmd.Parameters.Add("@pGenerationDate", SqlDbType.DateTime).Value = WGDB.Now;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Statement.AccountId;
            _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Statement.DateFrom;
            _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Statement.DateTo;
            _cmd.Parameters.Add("@pStatusPrev", SqlDbType.Int).Value = Statement.StatusPrev;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error on WCP_AccountsWinLossStatements.UpdateWinLossStatementGenerate AccountId: " + Statement.AccountId.ToString() +
                  " Period: " + Statement.DateFrom.ToString() + " - " + Statement.DateTo.ToString());
        Log.Exception(_ex);
      }

      return false;
    } // UpdateWinLossStatementGenerate


    //------------------------------------------------------------------------------
    // PURPOSE : Return list of win_loss_statement is selected datesFilter and pending print
    //
    //  PARAMS :
    //      - INPUT:
    //            - DatesFilter
    //
    //      - OUTPUT :
    //            - WinLossStatements[]
    //
    // RETURNS :
    //      - True or False
    //
    public static Boolean GetWinLossStatementForPrint(List<String> DatesFilter, out List<WIN_LOSS_STATEMENT> WinLossStatements)
    {
      StringBuilder _sb;
      DataTable _dt;
      int _idx;
      WIN_LOSS_STATEMENT _statement_insert;

      WinLossStatements = new List<WIN_LOSS_STATEMENT>();

      try
      {

        _dt = new DataTable();

        //Search the last provided hour
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   WLS_ACCOUNT_ID                    ");
        _sb.AppendLine("           , AC_HOLDER_NAME                    ");
        _sb.AppendLine("           , AC_HOLDER_ADDRESS_01              ");
        _sb.AppendLine("           , AC_HOLDER_CITY                    ");
        _sb.AppendLine("           , AC_HOLDER_ZIP                     ");
        _sb.AppendLine("           , FS_NAME                           ");
        _sb.AppendLine("           , WLS_DATE_FROM                     ");
        _sb.AppendLine("           , WLS_DATE_TO                       ");
        _sb.AppendLine("           , WLS_STATUS                        ");
        _sb.AppendLine("           , WLS_REQUEST_DATE                  ");
        _sb.AppendLine("           , WLS_REQUEST_USER_ID               ");
        _sb.AppendLine("           , WLS_PLAYED_AMOUNT                 ");
        _sb.AppendLine("           , WLS_WON_AMOUNT                    ");
        _sb.AppendLine("           , WLS_JACKPOT_AMOUNT                ");
        _sb.AppendLine("           , WLS_DOCUMENT_ID                   ");
        _sb.AppendLine("      FROM   WIN_LOSS_STATEMENTS               ");
        _sb.AppendLine("INNER JOIN   ACCOUNTS                          ");
        _sb.AppendLine("        ON   AC_ACCOUNT_ID = WLS_ACCOUNT_ID    ");
        _sb.AppendLine(" LEFT JOIN   FEDERAL_STATES                    ");
        _sb.AppendLine("        ON   AC_HOLDER_FED_ENTITY = FS_STATE_ID");
        _sb.AppendLine("     WHERE   WLS_PRINT  = @pPrint              ");
        _sb.AppendLine("       AND   WLS_STATUS = @pStatus             ");
        _sb.AppendLine("       AND   WLS_DATE_FROM IN (                ");


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _idx = 0;

            //complete the query to dates selected
            foreach (String _dates in DatesFilter)
            {
              if (_idx > 0)
              {
                _sb.AppendLine(", ");
              }
              _sb.AppendLine(String.Format("@pDate{0}", _idx));
              _cmd.Parameters.Add(String.Format("@pDate{0}", _idx), SqlDbType.DateTime).Value = _dates;

              _idx++;
            }
            _sb.AppendLine(" ) ");

            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = WinLossStatementStatus.Ready;
            _cmd.Parameters.Add("@pPrint", SqlDbType.Int).Value = WinLossStatementStatusPrint.NotPrinted;

            _cmd.CommandText = _sb.ToString();

            // To optimize the query
            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _da.Fill(_dt);
            }
          }

          //Fill the object Win_loss_statement
          //WinLossStatements = new WIN_LOSS_STATEMENT[_dt.Rows.Count];

          for (_idx = 0; _idx < _dt.Rows.Count; _idx++)
          {
            _statement_insert = new WIN_LOSS_STATEMENT();

            _statement_insert.AccountId = (Int64)_dt.Rows[_idx]["WLS_ACCOUNT_ID"];

            if (_dt.Rows[_idx]["AC_HOLDER_NAME"] != DBNull.Value)
            {
              _statement_insert.HolderName = (String)_dt.Rows[_idx]["AC_HOLDER_NAME"];
            }

            if (_dt.Rows[_idx]["AC_HOLDER_ADDRESS_01"] != DBNull.Value)
            {
              _statement_insert.Adress = (String)_dt.Rows[_idx]["AC_HOLDER_ADDRESS_01"];
            }

            if (_dt.Rows[_idx]["AC_HOLDER_CITY"] != DBNull.Value)
            {
              _statement_insert.City = (String)_dt.Rows[_idx]["AC_HOLDER_CITY"];
            }

            if (_dt.Rows[_idx]["AC_HOLDER_ZIP"] != DBNull.Value)
            {
              _statement_insert.Zip = (String)_dt.Rows[_idx]["AC_HOLDER_ZIP"];
            }

            if (_dt.Rows[_idx]["FS_NAME"] != DBNull.Value)
            {
              _statement_insert.State = (String)_dt.Rows[_idx]["FS_NAME"];
            }

            _statement_insert.DateFrom = (DateTime)_dt.Rows[_idx]["WLS_DATE_FROM"];
            _statement_insert.DateTo = (DateTime)_dt.Rows[_idx]["WLS_DATE_TO"];
            _statement_insert.Status = (WinLossStatementStatus)_dt.Rows[_idx]["WLS_STATUS"];
            _statement_insert.RequestDate = (DateTime)_dt.Rows[_idx]["WLS_REQUEST_DATE"];
            _statement_insert.RequestUserId = (Int32)_dt.Rows[_idx]["WLS_REQUEST_USER_ID"];
            _statement_insert.RequestYear = ((DateTime)_dt.Rows[_idx]["WLS_DATE_FROM"]).Year;
            _statement_insert.StatusPrint = WinLossStatementStatusPrint.NotPrinted;
            _statement_insert.PlayedAmount = (Decimal)_dt.Rows[_idx]["WLS_PLAYED_AMOUNT"];
            _statement_insert.WonAmount = (Decimal)_dt.Rows[_idx]["WLS_WON_AMOUNT"];
            _statement_insert.JackpotAmount = (Decimal)_dt.Rows[_idx]["WLS_JACKPOT_AMOUNT"];
            _statement_insert.NetWinLoss = _statement_insert.WonAmount + _statement_insert.JackpotAmount - _statement_insert.PlayedAmount;

            if (_dt.Rows[_idx]["WLS_DOCUMENT_ID"] != DBNull.Value)
            {
              _statement_insert.DocumentId = (Int64)_dt.Rows[_idx]["WLS_DOCUMENT_ID"];
            }

            WinLossStatements.Add(_statement_insert);
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetWinLossStatement

    //------------------------------------------------------------------------------
    // PURPOSE : Update win loss statement status print
    //
    //  PARAMS :
    //      - INPUT :
    //          - Statement
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //
    static public Boolean UpdateWinLossStatementStatusPrint(WIN_LOSS_STATEMENT Statement)
    {

      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   UPDATE   WIN_LOSS_STATEMENTS             ");
        _sb.AppendLine("      SET   WLS_STATUS        = @pStatus    ");
        _sb.AppendLine("          , WLS_PRINT         = @pPrintNew  ");
        _sb.AppendLine("          , WLS_PRINT_DATE    = @pPrintDate ");
        _sb.AppendLine("          , WLS_PRINT_USER_ID = @pPrintUser ");
        _sb.AppendLine("    WHERE   WLS_ACCOUNT_ID    = @pAccountId ");
        _sb.AppendLine("      AND   WLS_DATE_FROM     = @pDateFrom  ");
        _sb.AppendLine("      AND   WLS_DATE_TO       = @pDateTo    ");
        _sb.AppendLine("      AND   WLS_PRINT         = @pPrintOld  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = WinLossStatement.WinLossStatementStatus.Ready;
            _cmd.Parameters.Add("@pPrintOld", SqlDbType.Int).Value = Statement.StatusPrintPrev;// PrintOld;// WinLossStatement.WinLosssStatementStatusPrint.NotPrinted;
            _cmd.Parameters.Add("@pPrintNew", SqlDbType.Int).Value = Statement.StatusPrint;// WinLossStatement.WinLosssStatementStatusPrint.Printed;
            _cmd.Parameters.Add("@pPrintDate", SqlDbType.DateTime).Value = DBNull.Value;
            _cmd.Parameters.Add("@pPrintUser", SqlDbType.Int).Value = DBNull.Value;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Statement.AccountId;
            _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Statement.DateFrom;
            _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Statement.DateTo;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error on WCP_AccountsWinLossStatements.UpdateWinLossStatementStatusPrint AccountId: " + Statement.AccountId.ToString() +
                  " Period: " + Statement.DateFrom.ToString() + " - " + Statement.DateTo.ToString());
        Log.Exception(_ex);
      }

      return false;
    } // UpdateWinLossStatementStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate fiscal period
    //
    //  PARAMS :
    //      - INPUT :
    //          - RequestYear
    //
    //      - OUTPUT :
    //          - DateFrom
    //          - DateTo
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //
    static public Boolean CalculateDates(Int32 RequestYear,
                                         out DateTime DateFrom,
                                         out DateTime DateTo)
    {
      DateTime _now;
      Boolean _current_fiscal_year;
      String FiscalYearStart;

      DateFrom = WGDB.Now.AddYears(-1);
      DateTo = WGDB.Now.AddYears(-1);
      FiscalYearStart = WSI.Common.GeneralParam.GetString("WinLossStatement", "FiscalYearStart.MMDDHH");
      _current_fiscal_year = (WGDB.Now.Year == RequestYear);

      try
      {
        _now = new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day, WGDB.Now.Hour, 0, 0);

        DateFrom = new DateTime(RequestYear,
                                Convert.ToInt32(FiscalYearStart.Substring(0, 2)),
                                Convert.ToInt32(FiscalYearStart.Substring(2, 2)),
                                Convert.ToInt32(FiscalYearStart.Substring(4, 2)),
                                0,
                                0);
        if (_current_fiscal_year)
        {
          DateTo = new DateTime(_now.Year,
                                _now.Month,
                                1,
                                Convert.ToInt32(FiscalYearStart.Substring(4, 2)),
                                0,
                                0);

          //JCA && AMF && FJC 28/04/2015
          //DateTo = DateTo.AddDays(-1);

          if (DateFrom > _now)
          {
            DateFrom = DateFrom.AddYears(-1);
          }
        }
        else
        {
          DateTo = DateFrom.AddYears(1);

          if (DateTo > _now)
          {
            DateTo = DateFrom;
            DateFrom = DateFrom.AddYears(-1);
          }
          else
          {
            DateTo = DateFrom.AddYears(1);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on WCP_AccountsWinLossStatements.CalculateDates RequestPeriodFiscal: " + RequestYear.ToString() +
                  " FiscalYearStart: " + FiscalYearStart.ToString());
        Log.Exception(_ex);
      }

      return false;
    } // CalculateDates

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Movements (Account & Cashier)
    //
    //  PARAMS :
    //      - INPUT :
    //          - WIN_LOSS_STATEMENT  Statement
    //          - CashierSessionInfo  CashierInf
    //          - SqlTransaction      Trx
    //
    //      - OUTPUT :
    //          - OperationId
    //          - DateTo
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //
    static public Boolean InsertAccountOperationMovements(WIN_LOSS_STATEMENT Statement,
                                                          CashierSessionInfo CashierInfo,
                                                          out Int64 OperationId,
                                                          SqlTransaction Trx)
    {
      Boolean _is_multisite;
      Int32 _num_rows_updated;
      Currency _current_balance;
      StringBuilder _sb;
      AccountMovementsTable _account_movement;
      CashierMovementsTable _cashier_movements;
      SqlParameter _balance;

      _num_rows_updated = 0;
      OperationId = 0;
      _sb = new StringBuilder();

      try
      {
        _is_multisite = GeneralParam.GetBoolean("MultiSite", "IsCenter");

        //1. GET Balance
        _sb.AppendLine(" SELECT   @pBalance     = AC_BALANCE                        ");
        _sb.AppendLine("   FROM   ACCOUNTS                                          ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ;                     ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Statement.AccountId;
          _balance = _cmd.Parameters.Add("@pBalance", SqlDbType.Money);
          _balance.Direction = ParameterDirection.Output;

          _num_rows_updated = _cmd.ExecuteNonQuery();
        }

        //2. INSERT Account Movement / Operation

        // If is multisite, It doesn't insert the operation
        if (!_is_multisite)
        {
          if (!Operations.DB_InsertOperation(OperationCode.WIN_LOSS_STATEMENT_REQUEST, Statement.AccountId,
                                             CashierInfo.CashierSessionId, 0, 0, 0, 0, 0,0,string.Empty, out OperationId, Trx))
          {
            Log.Error("InsertAccountOperationMovements. Account Operation not Inserted. Card: " + Statement.AccountId);

            return false;
          }
        }

        _current_balance = _balance.Value == null ? 0 : (System.Data.SqlTypes.SqlMoney)_balance.SqlValue;

        _account_movement = new AccountMovementsTable(CashierInfo);

        if (!_account_movement.Add(OperationId, Statement.AccountId, MovementType.WinLossStatementRequest, _current_balance, 0, 0, _current_balance))
        {
          Log.Error("InsertAccountOperationMovements. Account Movement not Inserted. Card: " + Statement.AccountId);

          return false;
        }
        if (!_account_movement.Save(Trx))
        {
          Log.Error("InsertAccountOperationMovements. Account Movement not Updated. Card: " + Statement.AccountId);

          return false;
        }

        //3. INSERT Cashier Movement
        _cashier_movements = new CashierMovementsTable(CashierInfo);

        //if (!_cashier_movements.Add(0, CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST, _current_balance, 0, ""))
        if (!_cashier_movements.Add(0, CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST, _current_balance, Statement.AccountId, ""))
        {
          Log.Error("InsertAccountOperationMovements. Cashier Movement not Inserted. Card: " + Statement.AccountId);

          return false;
        }

        if (!_cashier_movements.Save(Trx))
        {
          Log.Error("InsertAccountOperationMovements. Cashier Movement not Updated. Card: " + Statement.AccountId);

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InsertAccountOperationMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Update document Id of Win loss statement
    //
    //  PARAMS :
    //      - INPUT :
    //          - WIN_LOSS_STATEMENT Statement
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //
    static public Boolean UpdateWinLossStatementDocumentId(WIN_LOSS_STATEMENT Statement)
    {
      StringBuilder _sb;
      try
      {
        _sb = new StringBuilder();

        _sb.Length = 0;
        _sb.AppendLine("UPDATE   WIN_LOSS_STATEMENTS               ");
        _sb.AppendLine("   SET   WLS_DOCUMENT_ID   = @pDocumentId  ");
        _sb.AppendLine("       , WLS_PRINT_DATE    = @pPrintDate   ");
        _sb.AppendLine("       , WLS_PRINT_USER_ID = @pPrintUser   ");
        _sb.AppendLine(" WHERE   WLS_ACCOUNT_ID    = @pAccountId   ");
        _sb.AppendLine("   AND   WLS_DATE_FROM     = @pDateFrom    ");
        _sb.AppendLine("   AND   WLS_DATE_TO       = @pDateTo      ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pDocumentId", SqlDbType.BigInt).Value = Statement.DocumentId;
            _cmd.Parameters.Add("@pPrintDate", SqlDbType.DateTime).Value = Statement.PrintDate;
            _cmd.Parameters.Add("@pPrintUser", SqlDbType.Int).Value = Statement.PrintUserId;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Statement.AccountId;
            _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Statement.DateFrom;
            _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Statement.DateTo;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("UpdateWinLossStatementDocumentId DocumentId=" + Statement.DocumentId.ToString());
        Log.Exception(_ex);
      }

      return false;
    } // UpdateWinLossStatementDocumentId

    //------------------------------------------------------------------------------
    // PURPOSE : Get WinLossSatement into a PDF File (Get Pdf if exists or Create if not exists)
    //
    //  PARAMS :
    //      - INPUT :
    //          - WIN_LOSS_STATEMENT Statement
    //          - DocumentList WinLossStatementPdf
    //
    //      - OUTPUT :
    //          - WIN_LOSS_STATEMENT Statement
    //          - DocumentList WinLossStatementPdf
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //

    static public Boolean GetWinLossStatementPdfDocument(ref WIN_LOSS_STATEMENT Statement, ref DocumentList WinLossStatementPdf)
    {
      String _file_name;
      String _base_name;
      String _title;
      MemoryStream _ms_pdf;
      DOCUMENT_TYPE _doc_type;
      Byte[] _logo;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _file_name = String.Empty;
          _base_name = String.Empty;
          _title = String.Empty;
          WinLossStatementPdf = null;
          _logo = null;
          _doc_type = DOCUMENT_TYPE.STATEMENT_PDF;

          // Document exists
          if (Statement.DocumentId > 0)
          {
            // GET Existing Pdf File.
            if (!TableDocuments.Load(Statement.DocumentId, out _doc_type, out WinLossStatementPdf, _db_trx.SqlTransaction))
            {
              return false;
            }
          }
          else
          {
            // Create New Pdf File
            // 1. GET Logo
            TableDocuments.Load(DOCUMENT_TYPE.STATEMENT_LOGO, out WinLossStatementPdf, _db_trx.SqlTransaction);
            if (WinLossStatementPdf.Count > 0)
            {
              _logo = WinLossStatementPdf[0].Content;
            }
            // 2. CREATE Pdf file 
            if (!String.IsNullOrEmpty(Statement.HolderName))
            {
              _base_name = Statement.HolderName.ToString();
            }
            if (!String.IsNullOrEmpty(Statement.Adress))
            {
              _base_name += "\n" + Statement.Adress.ToString();
            }
            if (!String.IsNullOrEmpty(Statement.City))
            {
              _base_name += "\n" + Statement.City.ToString();
            }
            if (!String.IsNullOrEmpty(Statement.Zip))
            {
              _base_name += "\n" + Statement.Zip.ToString();
            }
            if (!String.IsNullOrEmpty(Statement.State))
            {
              _base_name += " - " + Statement.State.ToString();
            }
            _title = GeneralParam.GetString("WinLossStatement", "Title") +
                                            " (" + Resource.String("STR_WIN_LOSS_STATEMENT_PDF_TITLE") +
                                            " " + Statement.RequestYear + 
                                            ")";


            _ms_pdf = CashierDocs.CreateDocumentStatement(_base_name,
                                                          _title,
                                                          _logo,
                                                          Statement.PlayedAmount,
                                                          Statement.WonAmount + Statement.JackpotAmount,
                                                          Statement.NetWinLoss);
            if (_ms_pdf == null)
            {
              return false;
            }

            // 3. SAVE Document into Table DOCUMENTS
            WinLossStatementPdf = new DocumentList();
            _base_name = "WinLossStatement_" + Statement.AccountId.ToString() + "-" +
                         Statement.RequestDate.Year.ToString() +
                         Statement.RequestDate.Month.ToString("00") +
                         Statement.RequestDate.Day.ToString("00");

            _file_name = String.Format("{0}.PDF", _base_name);
            WinLossStatementPdf.Add(new ReadOnlyDocument(_file_name, _ms_pdf));

            if (!TableDocuments.Save(ref Statement.DocumentId, DOCUMENT_TYPE.STATEMENT_PDF, WinLossStatementPdf, _db_trx.SqlTransaction))
            {
              return false;
            }

            _db_trx.Commit();
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetWinLossStatementPdfDocument

    //------------------------------------------------------------------------------
    // PURPOSE : Change status statement and print statement document
    //
    //  PARAMS :
    //      - INPUT :
    //          - WIN_LOSS_STATEMENT Statement
    //          - PrinterName
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //
    static public Boolean RequestPrintWinLossStatementPdfDocument(WIN_LOSS_STATEMENT Statement, String PrinterName)
    {
      DocumentList _pdf_file;
      String _path;

      _pdf_file = null;
      _path = String.Empty;

      try
      {
        if (UpdateWinLossStatementStatusPrint(Statement))
        {
          if (GetWinLossStatementPdfDocument(ref Statement, ref _pdf_file))
          {

            _path = Path.GetTempPath();
            _pdf_file.Save(_path);

            if (PrintWinLossStatementPdfDocument(_path + _pdf_file[0].Name, PrinterName))
            {
              if (UpdateWinLossStatementDocumentId(Statement))
              {
                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // RequestPrintWinLossStatementPdfDocument

    //------------------------------------------------------------------------------
    // PURPOSE : Print document in PDF
    //
    //  PARAMS :
    //      - INPUT :
    //          - PdfFiles
    //          - PrinterName
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //
    static private Boolean PrintWinLossStatementPdfDocument(String PdfFiles, String PrinterName)
    {
      GhostScriptParameter _print_params;
      PageSize _page_size;
      PaperSize _paper_size;

      try
      {
        _page_size = (PageSize)GeneralParam.GetInt32("WinLossStatement", "PaperSize", 0);
        _paper_size = new PaperSize();

        // TODO SGB: Tama�os de los diferentes tipos
        switch (_page_size)
        {
          case PageSize.A4:
            _paper_size = new PaperSize("A4", 210, 297);
            break;

          case PageSize.A5:
            _paper_size = new PaperSize("A5", 148, 210);
            break;

          case PageSize.Letter:
            _paper_size = new PaperSize("Letter", 85, 110);
            break;

          default:
            break;
        }

        _print_params = new WSI.Common.GhostScriptParameter(PdfFiles, 1, PrinterName);

        if (!WSI.Common.GSPrint.Print(_print_params))
        {

          return false;
        }

        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // PrintWinLossStatementPdfDocument

    #endregion

    #region 'Private Methods'
    public static String DateTimeCustomFormatWithOutSeconds(DateTime DateTime)
    {
      DateTimeFormatInfo _dfi;
      String _date_format;
      try
      {
        _dfi = Thread.CurrentThread.CurrentCulture.DateTimeFormat;

        _date_format = _dfi.ShortDatePattern;
        _date_format += " HH" + _dfi.TimeSeparator + "mm"; ;

        return DateTime.ToString(_date_format);
      }
      catch
      { }

      return String.Empty;

    }

    #endregion

  }
}
