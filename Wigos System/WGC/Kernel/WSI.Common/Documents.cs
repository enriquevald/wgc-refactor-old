//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DocumentList.cs
// 
//   DESCRIPTION: Manage a Document List
// 
//        AUTHOR: Andreu Julia 
// 
// CREATION DATE: 14-FEB-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-FEB-2012 AJQ    First release.
// 02-MAY-2012 MPO    Added feature --> Delete/Save a doc_type.
// 30-AUG-2012 ANG    Fixed bug, WKTResources.Save() doesn't update file extension.
// 17-JUN-2013 JMM    Added ScannedDocuments class.
// 06-AUG-2013 ACM    Added ScannedDocUpgradeFormat function.
// 08-OCT-2013 ACM    Added ScannedDocUpdateDocType and UpdateToNewDocumentType functions.
// 25-JUL-2014 ACM    Added "LCDResources" class.
// 14-OCT-2016 RAB    PBI 18098: General params: Automatically add the GP to the system
// 01-MAR-2017 FGB    Bug 24126: Withholding generation: The Excel data is incorrect
// 29-MAY-2017 DHA    Bug 27734:Background constancy generation error
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;

namespace WSI.Common
{
  //------------------------------------------------------------------------------
  // PURPOSE : Interface to manage documents
  //
  //
  public interface IDocument : IEquatable<IDocument>
  {
    String Name { get; }

    Byte[] Content { get; }
  }

  //------------------------------------------------------------------------------
  // PURPOSE : ReadOnly document
  //
  //
  public class ReadOnlyDocument : IDocument
  {
    private readonly String m_name;

    private readonly Byte[] m_content;

    public ReadOnlyDocument(String Name, Byte[] Content)
    {
      m_name = Name;
      m_content = Content;
    }

    public ReadOnlyDocument(String Name, MemoryStream MemStream)
    {
      m_name = Name;
      MemStream.Seek(0, SeekOrigin.End);
      m_content = new Byte[MemStream.Position];
      Array.Copy(MemStream.GetBuffer(), m_content, m_content.Length);
    }

    public string Name
    {
      get { return m_name; }
    }

    public byte[] Content
    {
      get
      {
        if (m_content == null)
        {
          return new Byte[0];
        }

        return m_content;
      }
    }

    public bool Equals(IDocument other)
    {
      if (other == null)
      {
        return false;
      }

      return GetHashCode() == other.GetHashCode();
    }

    public override int GetHashCode()
    {
      int _hash = 17;
      
      unchecked
      {
        if (m_content != null)
        {
          _hash = _hash * 31 + m_content.GetHashCode();
        }

        if (m_name != null)
        {
          _hash = _hash * 31 + m_name.GetHashCode();
      }
      }

      return _hash;
    }
  }

  public class DocumentList : List<IDocument>
  {
    private enum CompressionMethod
    {
      None = 0,
      Deflate = 1,
    }

    private static Byte[] Compress(Byte[] ExpandedBuffer)
    {
      Byte[] _out;

      _out = null;

      using (MemoryStream _ms = new MemoryStream())
      {
        using (DeflateStream _dfs = new DeflateStream(_ms, CompressionMode.Compress, true))
        {
          _dfs.Write(ExpandedBuffer, 0, ExpandedBuffer.Length);
          _dfs.Close();
        }

        _out = new Byte[_ms.Position];
        Array.Copy(_ms.GetBuffer(), _out, _out.Length);
      }

      return _out;
    }

    private static Byte[] Expand(Byte[] CompressedBuffer)
    {
      Byte[] _tmp;
      Byte[] _out;
      int _read;

      _tmp = new Byte[64 * 1024];
      _out = null;

      using (MemoryStream _mso = new MemoryStream())
      {
        using (MemoryStream _msi = new MemoryStream(CompressedBuffer, 0, CompressedBuffer.Length))
        {
          using (DeflateStream _dfs = new DeflateStream(_msi, CompressionMode.Decompress))
          {
            do
            {
              _read = _dfs.Read(_tmp, 0, _tmp.Length);
              _mso.Write(_tmp, 0, _read);

            } while (_read == _tmp.Length);

            _dfs.Close();
          }
        }

        _out = new Byte[_mso.Position];
        Array.Copy(_mso.GetBuffer(), _out, _out.Length);
      }

      return _out;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Creates a document list from a binary array
    //
    //  PARAMS:
    //      - INPUT:
    //          - BinaryDocumentList
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      DocumentList
    public static DocumentList CreateFromBinary(Byte[] BinaryDocumentList)
    {
      BinaryFormatter _bin_formatter;
      DataSet _dataset;
      DataTable _table;
      DocumentList _doc_list;

      _doc_list = new DocumentList();

      using (MemoryStream _mem_stream = new MemoryStream(BinaryDocumentList))
      {
      _bin_formatter = new BinaryFormatter();

      _dataset = (DataSet)_bin_formatter.Deserialize(_mem_stream);
      }

      _table = _dataset.Tables["PackedFile"];

      foreach (DataRow _file in _table.Rows)
      {
        CompressionMethod _method;
        String _name;
        Byte[] _content;

        _name = (String)_file["Name"];
        _content = (Byte[])_file["Content"];
        _method = (CompressionMethod)_file["CompressionMethod"];

        if (_method == CompressionMethod.Deflate)
        {
          _content = Expand(_content);
        }

        _doc_list.Add(new ReadOnlyDocument(_name, _content));
      }

      return _doc_list;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Convert a document list to binary
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Byte []
    public Byte[] ConvertToBinary()
    {
      DataTable _table;
      DataSet _dataset;
      DataColumn _column;
      CompressionMethod _method;

      _method = CompressionMethod.None;

      _dataset = new DataSet("PackedFileList");
      _table = _dataset.Tables.Add("PackedFile");
      _column = _table.Columns.Add("Name", Type.GetType("System.String"));
      _column = _table.Columns.Add("Size", Type.GetType("System.Int64"));
      _column = _table.Columns.Add("CompressionMethod", _method.GetType());
      _column = _table.Columns.Add("CompressedSize", Type.GetType("System.Int64"));
      _column = _table.Columns.Add("Content", Type.GetType("System.Byte[]"));
      _column = _table.Columns.Add("SHA1", Type.GetType("System.Byte[]"));

      Byte[] _content;
      Byte[] _zip;
      Byte[] _hash;
      DataRow _row;
      SHA1 _csp;

      foreach (IDocument _doc in this)
      {
        _method = CompressionMethod.Deflate;
        _content = _doc.Content;
        _zip = Compress(_content);
        if (_zip.Length >= _content.Length)
        {
          _method = CompressionMethod.None;
          _zip = _content;
        }

        // Hash
        _csp = SHA1.Create();
        _hash = _csp.ComputeHash(_content);

        _row = _table.NewRow();
        _row["Name"] = _doc.Name;
        _row["Size"] = _content.LongLength;
        _row["CompressionMethod"] = _method;
        _row["CompressedSize"] = _zip.LongLength;
        _row["Content"] = _zip;
        _row["SHA1"] = _hash;

        _table.Rows.Add(_row);
      }

      BinaryFormatter _bin_formatter;
      SerializationFormat _serial_format;
      Byte[] _out;

      using (MemoryStream _mem_stream = new MemoryStream())
      {
      // Serialize 
      _bin_formatter = new BinaryFormatter();
      _serial_format = _dataset.RemotingFormat;
      _dataset.RemotingFormat = SerializationFormat.Binary;
      _bin_formatter.Serialize(_mem_stream, _dataset);

      _out = new Byte[_mem_stream.Position];

      Array.Copy(_mem_stream.GetBuffer(), _out, _out.Length);
      }

      return _out;
    }

    public Boolean Save(String Path)
    {

      Misc.WriteLog(String.Format("Documents.Save - BEGIN"), Log.Type.Message);
      try
      {
        foreach (IDocument _doc in this)
        {
          String _path;

          _path = System.IO.Path.Combine(Path, _doc.Name);

          try
          {
            Misc.WriteLog(String.Format("Documents.Save - write document _path: {0}", _path), Log.Type.Message);
            using (FileStream _fstream = new FileStream(_path, FileMode.Create, FileAccess.Write))
            {
              _fstream.Write(_doc.Content, 0, _doc.Content.Length);
              _fstream.Close();
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);

            return false;
          }
        }

        Misc.WriteLog(String.Format("Documents.Save - END OK"), Log.Type.Message);
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      Misc.WriteLog(String.Format("Documents.Save - END ERROR"), Log.Type.Message);
      return false;
    }

    public Boolean Find(String Name, out IDocument Document)
    {
      Document = null;

      foreach (IDocument _idoc in this)
      {
        if (_idoc.Name.Equals(Name))
        {
          Document = _idoc;

          return true;
        }
      }

      return false;
    }

    public Boolean Contains(String Name)
    {
      foreach (IDocument _idoc in this)
      {
        if (_idoc.Name.Contains(Name))
        {
          return true;
        }
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the total size in the Document List
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Int32
    public Int32 Length()
    {
      Int32 _total_length;

      _total_length = 0;

      if (this.Count > 0)
      {
        foreach (IDocument _document in this)
        {
          _total_length = _total_length + _document.Content.Length;
        }
      }

      return _total_length;
    }
  }


  public static class TableDocuments
  {

    public static Boolean Save(ref Int64 DocumentId, DOCUMENT_TYPE DocumentType, DocumentList DocList, SqlTransaction Trx)
    {
      String _sql_str;
      SqlParameter _parameter;

      Misc.WriteLog(String.Format("Documents.Save - BEGIN"), Log.Type.Message);

      try
      {
        if (DocumentId == 0)
        {
          _sql_str = "";
          _sql_str += "INSERT INTO DOCUMENTS ( DOC_TYPE ) ";
          _sql_str += "               VALUES ( @pDocumentType   ) ";
          _sql_str += "                  SET   @DocumentId = SCOPE_IDENTITY() ";

          using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DocumentType;
            _parameter = _cmd.Parameters.Add("@DocumentId", SqlDbType.BigInt);
            _parameter.Direction = ParameterDirection.Output;

            if (_cmd.ExecuteNonQuery() != 1)
            {

              Misc.WriteLog(String.Format("Documents.Save - END - ERROR INSERT   DOCUMENTS"), Log.Type.Message);
              return false;
            }

            DocumentId = Convert.ToInt64(_parameter.Value);

            if (DocList == null)
            {
              Misc.WriteLog(String.Format("Documents.Save - END - DocList NULL   OK"), Log.Type.Message);
              return true;
            }

            if (DocList.Count == 0)
            {
              Misc.WriteLog(String.Format("Documents.Save - END - DocList == 0    OK"), Log.Type.Message);
              return true;
            }
          }
        }

        if (DocumentId == 0)
        {
          Misc.WriteLog(String.Format("Documents.Save - END - DocumentId == 0   ERROR"), Log.Type.Message);
          return false;
        }

        _sql_str = "";
        _sql_str += "UPDATE DOCUMENTS SET   DOC_TYPE        = @pDocumentType ";
        _sql_str += "                     , DOC_MODIFIED    = GETDATE() ";
        _sql_str += "                     , DOC_DATA        = @pData ";
        _sql_str += "               WHERE   DOC_DOCUMENT_ID = @pDocumentId ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DocumentType;
          _sql_cmd.Parameters.Add("@pData", SqlDbType.VarBinary).Value = DocList.ConvertToBinary();
          _sql_cmd.Parameters.Add("@pDocumentId", SqlDbType.BigInt).Value = DocumentId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Misc.WriteLog(String.Format("Documents.Save - END - ERROR UPDATE DOCUMENTS"), Log.Type.Message);
            return false;
          }

          Misc.WriteLog(String.Format("Documents.Save - END - OK"), Log.Type.Message);
          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      Misc.WriteLog(String.Format("Documents.Save - END - ERROR"), Log.Type.Message);

      return false;
    }

    public static Boolean Save(DOCUMENT_TYPE DocumentType, DocumentList DocList, SqlTransaction Trx)
    {
      String _sql_str;
      Object _obj;
      Int64 _document_id;

      _document_id = 0;

      try
      {
        _sql_str = "";
        _sql_str += "SELECT TOP 1   DOC_DOCUMENT_ID ";
        _sql_str += "        FROM   DOCUMENTS ";
        _sql_str += "       WHERE   DOC_TYPE = @pDocumentType ";
        _sql_str += "    ORDER BY   DOC_DOCUMENT_ID DESC ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DocumentType;
          _obj = _cmd.ExecuteScalar();

          if (_obj == null)
          {
            //Only create a new ID
            if (!(Save(ref _document_id, DocumentType, null, Trx)))
            {
              return false;
            }
          }
          else
          {
            _document_id = Convert.ToInt64(_obj);
          }

          if (DocList == null)
          {
            return true;
          }

          if (DocList.Count == 0)
          {
            return true;
          }
        }

        return Save(ref _document_id, DocumentType, DocList, Trx);

      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Load(Int64 DocumentId, out DOCUMENT_TYPE DocumentType, out DocumentList DocList, SqlTransaction Trx)
    {
      String _sql_str;

      DocumentType = 0;
      DocList = new DocumentList();

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   DOC_TYPE  ";
        _sql_str += "       , DOC_DATA  ";
        _sql_str += "  FROM   DOCUMENTS ";
        _sql_str += " WHERE   DOC_DOCUMENT_ID = @pDocumentId ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDocumentId", SqlDbType.BigInt).Value = DocumentId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              DocumentType = (DOCUMENT_TYPE)_reader.GetInt32(0);
              DocList = DocumentList.CreateFromBinary((Byte[])_reader.GetSqlBinary(1));

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Load(DOCUMENT_TYPE DocumentType, out DocumentList DocList, SqlTransaction Trx)
    {
      String _sql_str;

      DocList = new DocumentList();

      Misc.WriteLog(String.Format("Documents.Load - BEGIN"), Log.Type.Message);

      try
      {
        _sql_str = "";
        _sql_str += "SELECT TOP 1   DOC_DATA  ";
        _sql_str += "        FROM   DOCUMENTS ";
        _sql_str += "       WHERE   DOC_TYPE = @pDocumentType ";
        _sql_str += "    ORDER BY   DOC_DOCUMENT_ID DESC ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DocumentType;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              DocList = DocumentList.CreateFromBinary((Byte[])_reader.GetSqlBinary(0));

              Misc.WriteLog(String.Format("Documents.Load - END - TRUE"), Log.Type.Message);
              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      Misc.WriteLog(String.Format("Documents.Load - END - FALSE"), Log.Type.Message);

      return false;
    }

    public static Boolean Delete(Int64 DocumentId, SqlTransaction Trx)
    {
      String _sql_str;
      Int32 _rows_affected;

      try
      {
        _sql_str = "";
        _sql_str += "DELETE   FROM DOCUMENTS";
        _sql_str += " WHERE   DOC_DOCUMENT_ID = @pDocumentId";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDocumentId", SqlDbType.BigInt).Value = DocumentId;

          _rows_affected = _cmd.ExecuteNonQuery();

          if (_rows_affected == 1)
          {
            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
  }

  public static class LCDResources
  {
    public static Boolean Load(out Image ResourceImage,
                               out Int64 ResourceId,
                               out String ResourceName,
                               SqlTransaction Trx
                               )
    {
      String _extension;
      Int32 _length;
      Byte[] _hash;
      Byte[] _resource_bytes;

      ResourceImage = null;

      if (Load(out _extension, out _length, out _hash, out _resource_bytes, out ResourceId, out ResourceName, 0, Trx))
      {
        using (MemoryStream _mm_image = new MemoryStream(_resource_bytes))
        {
        ResourceImage = Image.FromStream(_mm_image);
        }

        return true;
      }

      return false;
    }

    public static Boolean Load(out Int64 ResourceId,
                               out MemoryStream ResourceVideo,
                               out String Extension,
                               out String ResourceName,
                               SqlTransaction Trx
                               )
    {
      Int32 _length;
      Byte[] _hash;
      Byte[] _resource_bytes;

      ResourceVideo = null;

      if (Load(out Extension, out _length, out _hash, out _resource_bytes, out ResourceId, out ResourceName, 1, Trx))
      {
        ResourceVideo = new MemoryStream(_resource_bytes);

        return true;
      }

      return false;
    }

    public static Boolean Load(out String Extension,
                               out Int32 Length,
                               out Byte[] Hash,
                               out Byte[] ResourceBytes,
                               out Int64 ResourceId,
                               out String ResourceName,
                               Int64 ImageId,
                               SqlTransaction Trx)
    {
      String _sql_str;

      Hash = null;
      ResourceBytes = null;
      Extension = "";
      Length = 0;
      ResourceId = 0;
      ResourceName = "";

      try
      {
        _sql_str = "";
        _sql_str += "     SELECT   RES_RESOURCE_ID                                            ";
        _sql_str += "            , RES_DATA                                                   ";
        _sql_str += "            , RES_LENGTH                                                 ";
        _sql_str += "            , RES_EXTENSION                                              ";
        _sql_str += "            , RES_HASH                                                   ";
        _sql_str += "            , CIM_RESOURCE_NAME                                          ";
        _sql_str += "       FROM   WKT_RESOURCES                                              ";
        _sql_str += " INNER JOIN   LCD_IMAGES                                                 ";
        _sql_str += "         ON   CIM_RESOURCE_ID = RES_RESOURCE_ID                          ";
        _sql_str += "      WHERE	 RES_RESOURCE_ID = (SELECT   CIM_RESOURCE_ID                ";
        _sql_str += "                                   FROM   LCD_IMAGES                     ";
        _sql_str += "                                  WHERE   CIM_IMAGE_ID = @pCimImageId)   ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCimImageId", SqlDbType.BigInt).Value = ImageId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              ResourceId = (Int64)_reader.GetInt64(0);
              ResourceBytes = (Byte[])_reader.GetSqlBinary(1);
              Length = (Int32)_reader.GetInt32(2);
              Extension = (String)_reader.GetString(3);
              Hash = (Byte[])_reader.GetSqlBinary(4);
              ResourceName = (String)_reader.GetString(5);

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Save(ref Int64 ResourceId, Image ResourceImage, SqlTransaction Trx)
    {
      MemoryStream _ms;
      String _extension;
      Byte[] _img_buf;
      Int32 _img_len;

      _extension = String.Empty;
      if (ResourceImage == null)
      {
        return Save(ref ResourceId, _extension, 0, null, Trx);
      }

      _ms = new MemoryStream();
      ResourceImage.Save(_ms, ResourceImage.RawFormat);

      // AJQ 04-SEP-2012, The lenght of the image could be smaller than the size of the buffer
      _img_len = (Int32)_ms.Position;
      _img_buf = _ms.GetBuffer();

      _extension = ImageFormatFileExtensionProvider.GetFileExtensionForImageFormat(ResourceImage);

      return Save(ref ResourceId, _extension, _img_len, _img_buf, Trx);
    }

    public static Boolean Save(ref Int64 ResourceId, MemoryStream ResourceVideo, String Extension, SqlTransaction Trx)
    {
      Byte[] _vid_buf;
      Int32 _vid_len;

      if (ResourceVideo == null)
      {
        return Save(ref ResourceId, Extension, 0, null, Trx);
      }

      _vid_len = (Int32)ResourceVideo.Length;
      _vid_buf = ResourceVideo.GetBuffer();

      return Save(ref ResourceId, Extension, _vid_len, _vid_buf, Trx);
    }

    public static Boolean Save(ref Int64 ResourceId,
                               String Extension,
                               Int32 Length,
                               Byte[] ResourceBytes,
                               SqlTransaction Trx)
    {
      String _sql_str;
      SqlParameter _parameter;

      try
      {
        if (ResourceId == 0)
        {
          _sql_str = "";
          _sql_str += "INSERT INTO WKT_RESOURCES ( RES_EXTENSION, RES_LENGTH, RES_DATA, RES_HASH ) ";
          _sql_str += "                   VALUES ( @pExtension  , @pLength  , @pData  , @pHash   ) ";
          _sql_str += "                      SET   @ResourceId = SCOPE_IDENTITY() ";

          using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pExtension", SqlDbType.NVarChar).Value = Extension;
            _cmd.Parameters.Add("@pLength", SqlDbType.Int).Value = 0;
            _cmd.Parameters.Add("@pData", SqlDbType.Binary).Value = DBNull.Value;
            _cmd.Parameters.Add("@pHash", SqlDbType.Binary).Value = DBNull.Value;

            _parameter = _cmd.Parameters.Add("@ResourceId", SqlDbType.BigInt);
            _parameter.Direction = ParameterDirection.Output;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }

            ResourceId = Convert.ToInt64(_parameter.Value);

            if (ResourceBytes == null)
            {
              return true;
            }
          }
        }

        if (ResourceId == 0)
        {
          return false;
        }

        _sql_str = "";
        _sql_str += "UPDATE WKT_RESOURCES SET   RES_EXTENSION        = @pExtension ";
        _sql_str += "                         , RES_LENGTH           = @pLength ";
        _sql_str += "                         , RES_DATA             = @pData ";
        _sql_str += "                         , RES_HASH             = @pHash ";
        _sql_str += "                   WHERE   RES_RESOURCE_ID      = @pResourceId ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;

          if (ResourceBytes == null)
          {
            _sql_cmd.Parameters.Add("@pData", SqlDbType.VarBinary).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pData", SqlDbType.VarBinary).Value = ResourceBytes;
          }

          _sql_cmd.Parameters.Add("@pLength", SqlDbType.Int).Value = Length;

          if (ResourceBytes == null)
          {
            _sql_cmd.Parameters.Add("@pHash", SqlDbType.VarBinary).Value = DBNull.Value;
          }
          else
          {
            using (SHA1 _sha1 = SHA1.Create())
            {
              _sql_cmd.Parameters.Add("@pHash", SqlDbType.VarBinary).Value = _sha1.ComputeHash(ResourceBytes);
            }
          }

          if (ResourceBytes == null)
          {
            _sql_cmd.Parameters.Add("@pExtension", SqlDbType.NVarChar).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pExtension", SqlDbType.NVarChar).Value = Extension;
          }

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Delete(Int64 ResourceId, SqlTransaction Trx)
    {
      String _sql_str;
      Int32 _rows_affected;

      try
      {
        _sql_str = "";
        _sql_str += "DELETE   FROM WKT_RESOURCES ";
        _sql_str += " WHERE   RES_RESOURCE_ID = @pResourceId";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;

          _rows_affected = _cmd.ExecuteNonQuery();

          if (_rows_affected == 1)
          {
            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
  }

  public static class WKTResources
  {
    public static Boolean Load(Int64 ResourceId,
                               Int32 Start,
                               Int32 Offset,
                               out Byte[] ResourceBytes,
                               SqlTransaction Trx)
    {
      String _sql_str;

      ResourceBytes = null;

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   SUBSTRING(RES_DATA, @pStart, @pOffSet) ";
        _sql_str += "  FROM   WKT_RESOURCES ";
        _sql_str += " WHERE   RES_RESOURCE_ID = @pResourceId ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;
          _cmd.Parameters.Add("@pOffSet", SqlDbType.Int).Value = Offset;
          _cmd.Parameters.Add("@pStart", SqlDbType.Int).Value = Start;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              ResourceBytes = (Byte[])_reader.GetSqlBinary(0);

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Load(Int64 ResourceId,
                               Int32 Offset,
                               out String Extension,
                               out Int32 Length,
                               out Byte[] Hash,
                               out Byte[] ResourceBytes,
                               SqlTransaction Trx)
    {
      String _sql_str;

      Hash = null;
      ResourceBytes = null;
      Extension = "";
      Length = 0;

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   SUBSTRING(RES_DATA, 1, @pOffSet) ";
        _sql_str += "       , RES_LENGTH";
        _sql_str += "       , RES_EXTENSION ";
        _sql_str += "       , RES_HASH ";
        _sql_str += "  FROM   WKT_RESOURCES ";
        _sql_str += " WHERE   RES_RESOURCE_ID = @pResourceId ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;
          _cmd.Parameters.Add("@pOffSet", SqlDbType.Int).Value = Offset;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              ResourceBytes = (Byte[])_reader.GetSqlBinary(0);
              Length = (Int32)_reader.GetInt32(1);
              Extension = (String)_reader.GetString(2);
              Hash = (Byte[])_reader.GetSqlBinary(3);

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Load(Int64 ResourceId,
                               out String Extension,
                               out Int32 Length,
                               out Byte[] Hash,
                               out Byte[] ResourceBytes,
                               SqlTransaction Trx)
    {
      String _sql_str;

      Hash = null;
      ResourceBytes = null;
      Extension = "";
      Length = 0;

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   RES_DATA ";
        _sql_str += "       , RES_LENGTH";
        _sql_str += "       , RES_EXTENSION ";
        _sql_str += "       , RES_HASH ";
        _sql_str += "  FROM   WKT_RESOURCES ";
        _sql_str += " WHERE   RES_RESOURCE_ID = @pResourceId ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              ResourceBytes = (Byte[])_reader.GetSqlBinary(0);
              Length = (Int32)_reader.GetInt32(1);
              Extension = (String)_reader.GetString(2);
              Hash = (Byte[])_reader.GetSqlBinary(3);

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Load(Int64 ResourceId,
                               out Image ResourceImage,
                               SqlTransaction Trx)
    {
      String _extension;
      Int32 _length;
      Byte[] _hash;
      Byte[] _resource_bytes;

      ResourceImage = null;

      if (Load(ResourceId, out _extension, out _length, out _hash, out _resource_bytes, Trx))
      {
        using (MemoryStream _mm_image = new MemoryStream(_resource_bytes))
        {
        ResourceImage = Image.FromStream(_mm_image);
        }

        return true;
      }

      return false;
    }

    public static Boolean Save(ref Int64 ResourceId,
                               String Extension,
                               Int32 Length,
                               Byte[] ResourceBytes,
                               SqlTransaction Trx)
    {
      String _sql_str;
      SqlParameter _parameter;

      try
      {
        if (ResourceId == 0)
        {
          _sql_str = "";
          _sql_str += "INSERT INTO WKT_RESOURCES ( RES_EXTENSION, RES_LENGTH, RES_DATA, RES_HASH ) ";
          _sql_str += "                   VALUES ( @pExtension  , @pLength  , @pData  , @pHash   ) ";
          _sql_str += "                      SET   @ResourceId = SCOPE_IDENTITY() ";

          using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pExtension", SqlDbType.NVarChar).Value = Extension;
            _cmd.Parameters.Add("@pLength", SqlDbType.Int).Value = 0;
            _cmd.Parameters.Add("@pData", SqlDbType.Binary).Value = DBNull.Value;
            _cmd.Parameters.Add("@pHash", SqlDbType.Binary).Value = DBNull.Value;

            _parameter = _cmd.Parameters.Add("@ResourceId", SqlDbType.BigInt);
            _parameter.Direction = ParameterDirection.Output;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }

            ResourceId = Convert.ToInt64(_parameter.Value);

            if (ResourceBytes == null)
            {
              return true;
            }

          }
        }

        if (ResourceId == 0)
        {
          return false;
        }

        _sql_str = "";
        _sql_str += "UPDATE WKT_RESOURCES SET   RES_EXTENSION        = @pExtension ";
        _sql_str += "                         , RES_LENGTH           = @pLength ";
        _sql_str += "                         , RES_DATA             = @pData ";
        _sql_str += "                         , RES_HASH             = @pHash ";
        _sql_str += "                   WHERE   RES_RESOURCE_ID      = @pResourceId ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;

          if (ResourceBytes == null)
          {
            _sql_cmd.Parameters.Add("@pData", SqlDbType.VarBinary).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pData", SqlDbType.VarBinary).Value = ResourceBytes;
          }

          _sql_cmd.Parameters.Add("@pLength", SqlDbType.Int).Value = Length;

          if (ResourceBytes == null)
          {
            _sql_cmd.Parameters.Add("@pHash", SqlDbType.VarBinary).Value = DBNull.Value;
          }
          else
          {
            using (SHA1 _sha1 = SHA1.Create())
            {
              _sql_cmd.Parameters.Add("@pHash", SqlDbType.VarBinary).Value = _sha1.ComputeHash(ResourceBytes);
            }
          }

          if (ResourceBytes == null)
          {
            _sql_cmd.Parameters.Add("@pExtension", SqlDbType.NVarChar).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pExtension", SqlDbType.NVarChar).Value = Extension;
          }

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Save(ref Int64 ResourceId, Image ResourceImage, SqlTransaction Trx)
    {
      MemoryStream _ms;
      String _extension;
      Byte[] _img_buf;
      Int32 _img_len;

      _extension = String.Empty;
      if (ResourceImage == null)
      {
        return Save(ref ResourceId, _extension, 0, null, Trx);
      }

      _ms = new MemoryStream();
      ResourceImage.Save(_ms, ResourceImage.RawFormat);

      // AJQ 04-SEP-2012, The lenght of the image could be smaller than the size of the buffer
      _img_len = (Int32)_ms.Position;
      _img_buf = _ms.GetBuffer();

      _extension = ImageFormatFileExtensionProvider.GetFileExtensionForImageFormat(ResourceImage);

      return Save(ref ResourceId, _extension, _img_len, _img_buf, Trx);
    }

    public static Boolean Delete(Int64 ResourceId, SqlTransaction Trx)
    {
      String _sql_str;
      Int32 _rows_affected;

      try
      {
        _sql_str = "";
        _sql_str += "DELETE   FROM WKT_RESOURCES ";
        _sql_str += " WHERE   RES_RESOURCE_ID = @pResourceId";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).Value = ResourceId;

          _rows_affected = _cmd.ExecuteNonQuery();

          if (_rows_affected == 1)
          {
            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
  }

  public static class AccountDocuments
  {
    private const Int32 TIME_TO_CHECK = 60 * 1000;

    //------------------------------------------------------------------------------
    // PURPOSE : Update account documents from old format to new one.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean ScannedDocUpgradeFormat(Int64 AccountId, String HolderId3Type, String BeneficiaryId3Type, SqlTransaction Trx)
    {
      try
      {
        DocumentList _new_list;
        ReadOnlyDocument _new_doc;
        String _new_name;
        IDocument _doc_image_old;
        DocumentList _doc_list_old;

        _new_list = new DocumentList();

        //Load Existing Documents
        if (!Load(AccountId, out  _doc_list_old, Trx))
        {
          return false;
        }

        //Holder
        if (!String.IsNullOrEmpty(HolderId3Type))
        {
          foreach (String _id3 in new String[] { "holder_id3_1", "holder_id3_2" })
          {
            _doc_list_old.Find(_id3, out _doc_image_old);

            if (_doc_image_old != null)
            {
              // Set the new name
              if (!CardData.GenerateDocName(ACCOUNT_SCANNED_OWNER.HOLDER, HolderId3Type, out _new_name))
              {
                return false;
              }

              // Clone the content (Image)
              byte[] _new_content = new byte[_doc_image_old.Content.Length];
              _doc_image_old.Content.CopyTo(_new_content, 0);

              // Set the document
              _new_doc = new ReadOnlyDocument(_new_name, _new_content);

              // Add the document in the list
              _new_list.Add(_new_doc);
            }
          }
        }

        // Beneficiary
        if (!String.IsNullOrEmpty(BeneficiaryId3Type))
        {
          foreach (String _id3 in new String[] { "beneficiary_id3_1", "beneficiary_id3_2" })
          {
            _doc_list_old.Find(_id3, out _doc_image_old);
            if (_doc_image_old != null)
            {
              // Set the new name
              if (!CardData.GenerateDocName(ACCOUNT_SCANNED_OWNER.BENEFICIARY, BeneficiaryId3Type, out _new_name))
              {
                return false;
              }

              // Clone the content (Image)
              byte[] _new_content = new byte[_doc_image_old.Content.Length];
              _doc_image_old.Content.CopyTo(_new_content, 0);

              // Set the document
              _new_doc = new ReadOnlyDocument(_new_name, _new_content);

              // Add the document in the list
              _new_list.Add(_new_doc);
            }
          }
        }

        if (_new_list.Count > 0)
        {
          if (!Save(AccountId, _new_list, Trx))
          {
            return false;
          }
        }

        return true;
      }

      catch (Exception _ex)
      {
        Log.Error(String.Format("ScannedDocUpgradeFormat AccountId: {0} Error: {1}", AccountId, _ex.Message));
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Update account documents from one document type to another one.
    //
    //  PARAMS :
    //      - INPUT : Account Id, OldDocumentType and NewDocumentType (001:RFC, 002:CURP ...)
    //
    //      - OUTPUT :
    //
    // RETURNS : True if documents names are changed
    //
    public static Boolean ScannedDocUpdateDocType(Int64 AccountId, String OldDocumentType, String NewDocumenType, SqlTransaction Trx)
    {
      try
      {
        DocumentList _new_list;
        ReadOnlyDocument _new_doc;
        String _new_name;
        DocumentList _doc_list_old;

        _new_list = new DocumentList();

        //Load Existing Documents
        if (!Load(AccountId, out  _doc_list_old, Trx))
        {
          return false;
        }

        // Is there any document with the old document type?
        if (!_doc_list_old.Contains(String.Format("{0}_", OldDocumentType)))
        {
          return false;
        }

        foreach (IDocument _document_old in _doc_list_old)
        {
          if (_document_old.Name.Contains(String.Format("{0}_", OldDocumentType)))
          {
            // Set the new name
            _new_name = _document_old.Name.Replace(String.Format("{0}_", OldDocumentType), String.Format("{0}_", NewDocumenType));

            // Set the document with the new name
            _new_doc = new ReadOnlyDocument(_new_name, _document_old.Content);

            // Add the document in the list
            _new_list.Add(_new_doc);
          }
          else
          {
            // Same Document without changes
            _new_list.Add(_document_old);
          }
        }

        if (_new_list.Count > 0)
        {
          if (!Save(AccountId, _new_list, Trx))
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("ScannedDocUpdateDocType AccountId: {0} Error: {1}", AccountId, _ex.Message));
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Update account documents from an old document type to a new one.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void UpdateToNewDocumentType()
    {
      StringBuilder _sql_txt;
      DataTable _account_documents;
      Int64 _account_id;
      Dictionary<String, String> _doc_types_list;
      String _old_doc_type;
      String _new_doc_type;
      Int32 _last_check;
      Int64 _elapsed;

      _last_check = Misc.GetTickCount();

      try
      {
        _doc_types_list = new Dictionary<String, String>();

        switch (GeneralParam.GetInt32("Account", "DocumentType.SpecialUpdate", 0))
        {
          case 1:

            //RFC to Professional Certificate
            _old_doc_type = ((Int32)ACCOUNT_HOLDER_ID_TYPE.RFC).ToString().PadLeft(3, '0');
            _new_doc_type = "009"; // PROFESSIONAL_CERTIFICATE. Table IDENTIFICATION_TYPES
            _doc_types_list.Add(_old_doc_type, _new_doc_type);

            //CURP to Driving Livense
            _old_doc_type = ((Int32)ACCOUNT_HOLDER_ID_TYPE.CURP).ToString().PadLeft(3, '0'); ;
            _new_doc_type = "008"; // DRIVING_LICENSE. Table IDENTIFICATION_TYPES
            _doc_types_list.Add(_old_doc_type, _new_doc_type);

            break;

          case 0:
          case 2:
          default:
            return;

        }
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
        return;
      }

      // No documents type to update
      if (_doc_types_list.Count == 0)
      {
        return;
      }

      try
      {
        foreach (KeyValuePair<String, String> _doc_types_to_update in _doc_types_list)
        {
          // Accounts with the old document type scanned 
          _account_documents = new DataTable();

          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sql_txt = new StringBuilder();
            _sql_txt.AppendLine("    SELECT   AC_ACCOUNT_ID ");
            _sql_txt.AppendLine("      FROM   ACCOUNTS");
            _sql_txt.AppendLine("     WHERE   AC_HOLDER_ID3_TYPE LIKE '%' + @pId_Type + '%' OR ");
            _sql_txt.AppendLine("             AC_BENEFICIARY_ID3_TYPE LIKE '%' + @pId_Type + '%'");

            using (SqlCommand _cmd = new SqlCommand(_sql_txt.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _cmd.Parameters.Add("@pId_Type", SqlDbType.NVarChar, 3).Value = _doc_types_to_update.Key;
              using (SqlDataReader _sql_read = _cmd.ExecuteReader())
              {
                _account_documents.Load(_sql_read);
              }
            }
          }

          if (_account_documents.Rows.Count == 0)
          {
            continue;
          }

          // Update accounts_documents data with the new document type scanned
          foreach (DataRow _dr in _account_documents.Rows)
          {
            _account_id = (Int64)_dr[0];

            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (!ScannedDocUpdateDocType(_account_id, _doc_types_to_update.Key, _doc_types_to_update.Value, _db_trx.SqlTransaction))
              {
                Log.Error(String.Format("UpdateToNewDocumentsTypes: AccountId {0} not updated from {1} to {2} scanned documents.",
                                        _account_id, _doc_types_to_update.Key, _doc_types_to_update.Value));
                continue;
              }

              // Update accounts table with new document type scanned in fields: ac_holder_id3_type and ac_beneficiary_id3_type
              _sql_txt = new StringBuilder();
              _sql_txt.AppendLine("UPDATE   ACCOUNTS");
              _sql_txt.AppendLine("   SET   AC_HOLDER_ID3_TYPE = replace(AC_HOLDER_ID3_TYPE, @pOldDocType, @pNewDocType),");
              _sql_txt.AppendLine("         AC_BENEFICIARY_ID3_TYPE = replace(AC_BENEFICIARY_ID3_TYPE, @pOldDocType, @pNewDocType)");
              _sql_txt.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");

              using (SqlCommand _cmd = new SqlCommand(_sql_txt.ToString()))
              {
                _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _account_id;
                _cmd.Parameters.Add("@pOldDocType", SqlDbType.NVarChar, 3).Value = _doc_types_to_update.Key;
                _cmd.Parameters.Add("@pNewDocType", SqlDbType.NVarChar, 3).Value = _doc_types_to_update.Value;

                if (_db_trx.ExecuteNonQuery(_cmd) != 1)
                {
                  Log.Error(String.Format("UpdateToNewDocumentsTypes: AccountId {0} not updated in Accounts table. So not updated scanned documents.", _account_id));
                  continue;
                }

                _db_trx.Commit();
              }

              _elapsed = Misc.GetElapsedTicks(_last_check);
              if (_elapsed >= TIME_TO_CHECK)
              {
                return;
              }

            } // _db_trx          

          } // for each account

        }// for each document type

        //Set GP off
        using (DB_TRX _db_trx = new DB_TRX())
        {
          String _group_key;
          String _subject_key;
          String _value_key;

          _group_key = "Account";
          _subject_key = "DocumentType.SpecialUpdate";
          _value_key = "2";

          if (!GeneralParam.GenerateGeneralParam(_group_key, _subject_key, _value_key, _db_trx.SqlTransaction))
          {
            Log.Error("Error changing GP value --> " + _group_key + " | " + _subject_key + " = " + _value_key);

              return;
            }

            _db_trx.Commit();

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    } // UpdateToNewDocumentType

    //------------------------------------------------------------------------------
    // PURPOSE : Upgrade account documents from old format to new one.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void UpgradeToNewFormat()
    {
      StringBuilder _sql_txt;
      Int32 _aux;
      DataTable _account_documents;
      Int64 _account_id;
      Int32 _last_check;
      Int64 _elapsed;

      _last_check = Misc.GetTickCount();

      try
      {
        _account_documents = new DataTable();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("SELECT Count(*) FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACCOUNT_DOCUMENTS_TO_UPGRADE]') AND type in (N'U')");

          using (SqlCommand _cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _aux = (Int32)_db_trx.ExecuteScalar(_cmd);
          }

          if (_aux == 0)
          {
            return;
          }

          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("SELECT   AC_ACCOUNT_ID ");
          _sql_txt.AppendLine("       , AC_HOLDER_ID3_TYPE ");
          _sql_txt.AppendLine("       , AC_BENEFICIARY_ID3_TYPE ");
          _sql_txt.AppendLine("  FROM   ACCOUNT_DOCUMENTS_TO_UPGRADE ");

          using (SqlCommand _cmd = new SqlCommand(_sql_txt.ToString()))
          {
            using (SqlDataAdapter _sql_adap = new SqlDataAdapter(_cmd))
            {
              _db_trx.Fill(_sql_adap, _account_documents);
            }
          }
        }

        if (_account_documents.Rows.Count == 0)
        {
          return;
        }

        foreach (DataRow _dr in _account_documents.Rows)
        {
          _account_id = (Int64)_dr[0];

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (ScannedDocUpgradeFormat(_account_id, (String)_dr[1], (String)_dr[2], _db_trx.SqlTransaction))
            {
              _sql_txt = new StringBuilder();
              _sql_txt.AppendLine("DELETE   ACCOUNT_DOCUMENTS_TO_UPGRADE ");
              _sql_txt.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");

              using (SqlCommand _cmd = new SqlCommand(_sql_txt.ToString()))
              {
                _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _account_id;

                if (_db_trx.ExecuteNonQuery(_cmd) == 1)
                {
                  _db_trx.Commit();
                }
                else
                {
                  Log.Error(String.Format("AccountDocuments.Upgrade: AccountId {0} not deleted and should. So not upgraded.", _account_id));
                }
              }
            }
            else
            {
              Log.Error(String.Format("AccountDocuments.Upgrade: AccountId {0} not upgraded.", _account_id));
            }
          }

          _elapsed = Misc.GetElapsedTicks(_last_check);
          if (_elapsed >= TIME_TO_CHECK)
          {
            return;
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // UpgradeToNewFormat

    public static Boolean Save(Int64 AccountId, DocumentList DocList, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        if (AccountId <= 0)
        {
          return false;
        }

        if (DocList == null)
        {
          return false;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("SET NOCOUNT ON");
        _sb.AppendLine("IF NOT EXISTS (SELECT AD_ACCOUNT_ID FROM ACCOUNT_DOCUMENTS WHERE AD_ACCOUNT_ID = @pAccountId)");
        _sb.AppendLine("   BEGIN");
        _sb.AppendLine("      INSERT INTO ACCOUNT_DOCUMENTS ( AD_ACCOUNT_ID ) ");
        _sb.AppendLine("                             VALUES ( @pAccountId ) ");
        _sb.AppendLine("   END");
        _sb.AppendLine("SET NOCOUNT OFF");

        _sb.AppendLine("UPDATE ACCOUNT_DOCUMENTS SET   AD_MODIFIED    = GETDATE() ");
        _sb.AppendLine("                             , AD_DATA        = @pData ");
        _sb.AppendLine("                       WHERE   AD_ACCOUNT_ID  = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pData", SqlDbType.VarBinary).Value = DocList.ConvertToBinary();
          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }

      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Load(Int64 AccountId, out DocumentList DocList, SqlTransaction Trx)
    {
      StringBuilder _sb;

      DocList = new DocumentList();
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("SELECT   AD_DATA  ");
        _sb.AppendLine("  FROM   ACCOUNT_DOCUMENTS ");
        _sb.AppendLine(" WHERE   AD_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              DocList = DocumentList.CreateFromBinary((Byte[])_reader.GetSqlBinary(0));

              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean Delete(Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _rows_affected;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("DELETE FROM ACCOUNT_DOCUMENTS WHERE AD_ACCOUNT_ID = @pAccountId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          _rows_affected = _cmd.ExecuteNonQuery();

          if (_rows_affected == 1)
          {
            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
  }

  /// <summary>
  /// Class that returns the file extension for a image format
  /// </summary>
  public static class ImageFormatFileExtensionProvider
  {
    /// <summary>
    /// Returns the file extension for the image format
    /// </summary>
    /// <param name="ResourceImage"></param>
    /// <returns></returns>
    public static String GetFileExtensionForImageFormat(Image ResourceImage)
    {
      String _extension;
      _extension = String.Empty;

      if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Bmp.Guid)
      {
        _extension = ".bmp";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Gif.Guid)
      {
        _extension = ".gif";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Jpeg.Guid)
      {
        _extension = ".jpg";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Png.Guid)
      {
        _extension = ".png";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Tiff.Guid)
      {
        _extension = ".tif";
      }
      else if (ResourceImage.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Wmf.Guid)
      {
        _extension = ".wmf";
  }
      else
      {
        // AJQ 04-SEP-2012, Default extension
        _extension = ".bmp";
}

      return _extension;
    }
  }
}