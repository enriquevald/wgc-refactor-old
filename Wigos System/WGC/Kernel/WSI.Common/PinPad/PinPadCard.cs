﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.PinPad
{
  public class PinPadCard
  {
    public String CardNumber { get; set; }

    public String CardHolder { get; set; }

    public String CardBankName { get; set; }

    public String Track1 { get; set; }

    public String Track2 { get; set; }

    public String POSEntryMode { get; set; }

    public String TagsEMV { get; set; }
  }
}
