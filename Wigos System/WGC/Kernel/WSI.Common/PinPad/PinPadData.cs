﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : PinPadData.cs
// 
//   DESCRIPTION : PinPadData
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2016 ETP    First release
// 24-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge refactoring
// 21-MAY-2017 ETP    Fixed Bug WIGOS-2997: Added voucher data.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.PinPad
{
  #region " PinPadInput "
  public struct PinPadInput
  {
    public CurrencyExchangeResult Amount { get; set; }
    public CardData Card { get; set; }
    public Int64 OperationId { get; set; }
    public String TerminalName { get; set; }
    
  }
  #endregion " PinPadInput "
  
  #region " PinPadOutput "
  public struct PinPadOutput
  {
    public PinPadTransactions PinPadTransaction { get; set; }

    public String ExpirationDate;

    public String ProductType;

    public String CardType;

    public String AID;

    public String TVR;

    public String TSI;

    public String APN;

    public String AL;

    public String EntryMode;

    public Boolean WithSignature;
 
  }

  #endregion " PinPadOutput "
}
