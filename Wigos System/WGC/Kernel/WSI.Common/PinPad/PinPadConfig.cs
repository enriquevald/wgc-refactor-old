﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PinPadConfig.cs
//  
//   DESCRIPTION: Class that configurates PinPad for bank connect
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 27-JUN-2016
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2016 ETP     First release.
// 29-SEP-2016 ETP     Fixed bug 18281: Televisa: Localization issues 
// 21-MAY-2017 ETP     Fixed Bug WIGOS-2997: Added voucher data.
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;

namespace WSI.Common.PinPad
{
  public class PinPadConfig
  {
    #region " properties "
    public String Name { get; set; }
    public String MerchandId { get; set; }
    public String User { get; set; }
    public String Password { get; set; }
    public String URL { get; set; }
    public String Language { get; set; }
    public String ModeOperation { get; set; }

    public Dictionary<String, String> Atributes { get; set; }

    #endregion " properties "


    #region " constructor "

    public PinPadConfig(String Model)
    {
      Load(Model);
    }

    #endregion " constructor "

    #region " public methods "

    /// <summary>
    /// Load general Params
    /// </summary>
    /// <param name="Model"></param>
    public void Load(String Model)
    {
      String _subject;
      String _old_language;

      _subject = "Provider." + Model + ".{0}";

      Name = GeneralParam.GetString("PinPad", String.Format(_subject, "Name"), String.Empty);
      MerchandId = GeneralParam.GetString("PinPad", String.Format(_subject, "MerchandId"), String.Empty);
      User = GeneralParam.GetString("PinPad", String.Format(_subject, "User"), String.Empty);
      Password = GeneralParam.GetString("PinPad", String.Format(_subject, "Password"), String.Empty);
      URL = GeneralParam.GetString("PinPad", String.Format(_subject, "URL"), String.Empty);
      _old_language = Language;
      Language = GeneralParam.GetString("Cashier", "Language", "ES").ToUpper().Substring(0,2);
      if (_old_language == null)
      {
        InitAtributes();
      }
      ModeOperation = GetModeOperation(_subject);
    }

    #endregion " public methods "

    #region " private methods "
    /// <summary>
    /// Init Atributes
    /// </summary>
    private void InitAtributes()
    {
      if (Atributes == null)
      {
        Atributes = new Dictionary<String, String>();
      }
      switch (Language)
      {
        case "EN":
          //Input
          Atributes.Add("MERCHANT_ID", "MERCHANT_ID");
          Atributes.Add("USER", "USER");
          Atributes.Add("PASSWORD", "PASSWORD");

          Atributes.Add("TERMINAL_ID", "TERMINAL_ID");
          Atributes.Add("MODE", "MODE");
          Atributes.Add("RESPONSE_LANGUAGE", "RESPONSE_LANGUAGE");

          Atributes.Add("BANORTE_URL", "BANORTE_URL");
          Atributes.Add("TRANS_TIMEOUT", "TRANS_TIMEOUT");

          Atributes.Add("CMD_TRANS", "CMD_TRANS");
          Atributes.Add("CONTROL_NUMBER", "CONTROL_NUMBER");
          Atributes.Add("AMOUNT", "AMOUNT");

          Atributes.Add("PORT", "PORT");
          Atributes.Add("BAUD_RATE", "BAUD_RATE");
          Atributes.Add("PARITY", "PARITY");
          Atributes.Add("STOP_BITS", "STOP_BITS");
          Atributes.Add("DATA_BITS", "DATA_BITS");
          Atributes.Add("RESULT", "RESULT");
          Atributes.Add("AUTH", "AUTH");

          //Output
          Atributes.Add("SERIAL_NUMBER", "SERIAL_NUMBER");
          Atributes.Add("PAYW_RESULT", "PAYW_RESULT");
          Atributes.Add("AUTH_CODE", "AUTH_CODE");
          Atributes.Add("REFERENCE", "REFERENCE");
          Atributes.Add("CHIP_DECLINED", "CHIP_DECLINED");
          Atributes.Add("TEXT", "TEXT");
          Atributes.Add("CUST_RSP_DATE", "CUST_RSP_DATE");

          Atributes.Add("CARD_NUMBER", "CARD_NUMBER");
          Atributes.Add("CARD_HOLDER", "CARD_HOLDER");
          Atributes.Add("ISSUING_BANK", "ISSUING_BANK");
          Atributes.Add("ENTRY_MODE", "ENTRY_MODE");
          Atributes.Add("EMV_TAGS", "EMV_TAGS");

          Atributes.Add("TRACK1", "TRACK1");
          Atributes.Add("TRACK2", "TRACK2");
          Atributes.Add("EMV_DATA", "EMV_DATA");
          Atributes.Add("CAUSE", "CAUSE");

          Atributes.Add("CARD_EXP", "CARD_EXP");
          Atributes.Add("CARD_TYPE", "CARD_TYPE");
          Atributes.Add("CARD_BRAND", "CARD_BRAND");
          Atributes.Add("PIN_ENTRY", "PIN_ENTRY");
          
          Atributes.Add("AID", "AID");
          Atributes.Add("APN", "APN");
          Atributes.Add("AL", "AL");
          Atributes.Add("TVR", "TVR");
          Atributes.Add("TSI", "TSI");
          
          break;

        case "ES":
          //Input
          Atributes.Add("MERCHANT_ID", "ID_AFILIACION");
          Atributes.Add("USER", "USUARIO");
          Atributes.Add("PASSWORD", "CLAVE_USR");

          Atributes.Add("TERMINAL_ID", "ID_TERMINAL");
          Atributes.Add("MODE", "MODO");
          Atributes.Add("RESPONSE_LANGUAGE", "IDIOMA_RESPUESTA");

          Atributes.Add("BANORTE_URL", "URL_BANORTE");
          Atributes.Add("TRANS_TIMEOUT", "TIEMPO_MAX");

          Atributes.Add("CMD_TRANS", "CMD_TRANS");
          Atributes.Add("CONTROL_NUMBER", "NUMERO_CONTROL");
          Atributes.Add("AMOUNT", "MONTO");


          Atributes.Add("PORT", "PUERTO");
          Atributes.Add("BAUD_RATE", "VELOCIDAD");
          Atributes.Add("PARITY", "PARIDAD");
          Atributes.Add("STOP_BITS", "BITS_PARO");
          Atributes.Add("DATA_BITS", "BITS_DATOS");
          Atributes.Add("RESULT", "RESULTADO");

          //Output
          Atributes.Add("SERIAL_NUMBER", "NUMERO_SERIE");
          Atributes.Add("PAYW_RESULT", "RESULTADO_PAYW");
          Atributes.Add("AUTH_CODE", "CODIGO_AUT");
          Atributes.Add("REFERENCE", "REFERENCIA");
          Atributes.Add("CHIP_DECLINED", "DECLINADA_CHIP");
          Atributes.Add("TEXT", "TEXTO");
          Atributes.Add("CUST_RSP_DATE", "FECHA_RSP_CTE");

          Atributes.Add("CARD_NUMBER", "NUMERO_TARJETA");
          Atributes.Add("CARD_HOLDER", "TARJETAHABIENTE");
          Atributes.Add("ISSUING_BANK", "BANCO_EMISOR");
          Atributes.Add("ENTRY_MODE", "MODO_ENTRADA");
          Atributes.Add("EMV_TAGS", "TAGS_EMV");

          Atributes.Add("TRACK1", "TRACK1");
          Atributes.Add("TRACK2", "TRACK2");
          Atributes.Add("EMV_DATA", "DATOS_EMV");
          Atributes.Add("CAUSE", "CAUSA");
          Atributes.Add("AUTH", "VENTA");

          Atributes.Add("CARD_EXP", "FECHA_EXP");
          Atributes.Add("CARD_TYPE", "TIPO_TARJETA");
          Atributes.Add("CARD_BRAND", "MARCA_TARJETA");
          Atributes.Add("PIN_ENTRY", "CAPTURA_NIP");

          Atributes.Add("AID", "AID");
          Atributes.Add("APN", "APN");
          Atributes.Add("AL", "AL");
          Atributes.Add("TVR", "TVR");
          Atributes.Add("TSI", "TSI");


          break;
        default:
          Log.Warning("Language not suported by banorte API: " + Language);
          break;
      }
    }

/// <summary>
    /// Get Mode Operation
/// </summary>
/// <param name="Subject"></param>
/// <returns></returns>
    private static String GetModeOperation(String Subject)
    {
      // For production use "PRD". For Test use "AUT", "DEC" OR "RND"
      if (GeneralParam.GetBoolean("PinPad", String.Format(Subject, "Test.Enabled"), false))
      {
        return GeneralParam.GetString("PinPad", String.Format(Subject, "Test.Mode"), "AUT");
      }
      return "PRD";
    }
    
    #endregion " private methods "

  }
}
