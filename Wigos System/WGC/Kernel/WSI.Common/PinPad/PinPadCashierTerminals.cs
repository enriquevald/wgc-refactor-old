﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : PinPadCashierTerminals.cs
// 
//   DESCRIPTION : PinPad Cashier Terminals
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUN-2016 AMF    First release
// 21-FEB-2017 FGB    Bug 24801: WigosGUI: Incorrect collection in the cash closing, due to Gp BankCard.Conciliate
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.PinPad
{
  public class PinPadCashierTerminals
  {
    #region " Properties "

    public Int32 CashierId { get; set; }
    public Dictionary<Int32, PinPadCashierTerminal> PinPadTerminals { get; set; }
    public DataTable PinPads { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Insert PinPad cashier terminal
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _parameter;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  INSERT INTO  PINPAD_CASHIER_TERMINALS ");
        _sb.AppendLine("             ( PCT_CASHIER_ID           ");
        _sb.AppendLine("             , PCT_TYPE                 ");
        _sb.AppendLine("             , PCT_PORT                 ");
        _sb.AppendLine("             , PCT_ENABLED              ");
        _sb.AppendLine("             , PCT_LAST_MODIFIED )      ");
        _sb.AppendLine("      VALUES                            ");
        _sb.AppendLine("             ( @pCashierId              ");
        _sb.AppendLine("             , @pType                   ");
        _sb.AppendLine("             , @pPort                   ");
        _sb.AppendLine("             , @pEnabled                ");
        _sb.AppendLine("             , @pLastModified )         ");
        _sb.AppendLine("   SET  @pId = SCOPE_IDENTITY()         ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCashierId", SqlDbType.NVarChar, 50).Value = CashierId;
          //_cmd.Parameters.Add("@pType", SqlDbType.NVarChar, 200).Value = (Type == null) ? DBNull.Value : (Object)Type;
          //_cmd.Parameters.Add("@pPort", SqlDbType.NVarChar, 50).Value = (Port == null) ? DBNull.Value : (Object)Port;
          //_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Enabled;
          _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;

          _parameter = _cmd.Parameters.Add("@pId", SqlDbType.BigInt);
          _parameter.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            if (_parameter.Value != null)
            {
              // Id = (Int32)_parameter.Value;

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadCashierTerminals.DB_Insert -> CashierId: {0}", CashierId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Get enabled Pin Pad cashier terminals list
    /// </summary>
    /// <returns></returns>
    public List<PinPadCashierTerminal> DB_GetEnabledList()
    {
      StringBuilder _sb;

      List<PinPadCashierTerminal> _list = new List<PinPadCashierTerminal>();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   PCT_ID                      ");
        _sb.AppendLine("         , PCT_TYPE                    ");
        _sb.AppendLine("         , PCT_PORT                    ");
        _sb.AppendLine("         , PCT_ENABLED                 ");
        _sb.AppendLine("         , PCT_LAST_MODIFIED           ");
        _sb.AppendLine("    FROM   PINPAD_CASHIER_TERMINALS    ");
        _sb.AppendLine("   WHERE   PCT_CASHIER_ID = @pId       ");
        _sb.AppendLine("     AND   PCT_ENABLED = 1             ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = CashierId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                PinPadCashierTerminal _pinpadcashierterminal = new PinPadCashierTerminal();

                _pinpadcashierterminal.Id = (Int32)_reader[0];
                _pinpadcashierterminal.Type = (PinPadType)_reader[1];
                _pinpadcashierterminal.Port = (PinPadCashierTerminal.PINPAD_PORT)_reader[2];
                _pinpadcashierterminal.Enabled = (Boolean)_reader[3];
                _pinpadcashierterminal.LastModified = (DateTime)_reader[4];

                _list.Add(_pinpadcashierterminal);
              }

              return _list;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadCashierTerminals.DB_GetEnabledList -> CashierId: {0}", CashierId));
        Log.Exception(_ex);
      }

      return _list;
    } // DB_GetEnabledList

    /// <summary>
    /// Get Pin Pad cashier terminals by CashierId
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_GetByCashierId(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        PinPadTerminals = new Dictionary<int, PinPadCashierTerminal>();

        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   PCT_ID                      ");
        _sb.AppendLine("         , PCT_TYPE                    ");
        _sb.AppendLine("         , PCT_PORT                    ");
        _sb.AppendLine("         , PCT_ENABLED                 ");
        _sb.AppendLine("         , PCT_LAST_MODIFIED           ");
        _sb.AppendLine("    FROM   PINPAD_CASHIER_TERMINALS    ");
        _sb.AppendLine("   WHERE   PT_CASHIER_ID = @pId        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = CashierId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              PinPadTerminals.Add((Int32)_reader[0], new PinPadCashierTerminal());

              ((PinPadCashierTerminal)PinPadTerminals[(Int32)_reader[0]]).Id = (Int32)_reader[0];
              ((PinPadCashierTerminal)PinPadTerminals[(Int32)_reader[0]]).Type = (PinPadType)_reader[1];
              ((PinPadCashierTerminal)PinPadTerminals[(Int32)_reader[0]]).Port = (PinPadCashierTerminal.PINPAD_PORT)_reader[2];
              ((PinPadCashierTerminal)PinPadTerminals[(Int32)_reader[0]]).Enabled = (Boolean)_reader[3];
              ((PinPadCashierTerminal)PinPadTerminals[(Int32)_reader[0]]).LastModified = (DateTime)_reader[4];

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadCashierTerminals.DB_GetByCashierId -> CashierId: {0}", CashierId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetByCashierId

    /// <summary>
    /// Check if have some enabled by CashierId
    /// </summary>
    /// <returns></returns>
    public Boolean DB_CheckSomeEnabledByCashierId()
    {
      StringBuilder _sb;
      Int32 _count;

      try
      {
        PinPads = new DataTable();
        _count = 0;

        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   COUNT(1)                 ");
        _sb.AppendLine("    FROM   PINPAD_CASHIER_TERMINALS ");
        _sb.AppendLine("   WHERE   PCT_CASHIER_ID = @pId    ");
        _sb.AppendLine("     AND   PCT_ENABLED    = 1       ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = CashierId;

            _count = (Int32)_cmd.ExecuteScalar();

            if (_count > 0)
            {
              return true;
            }
            else
            {
              return false;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadCashierTerminals.DB_CheckSomeEnabledByCashierId -> CashierId: {0}", CashierId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_CheckSomeEnabledByCashierId

    /// <summary>
    /// Get Pin Pad cashier terminals DataTable by CashierId
    /// </summary>
    /// <returns></returns>
    public Boolean DB_GetDataTableByCashierId()
    {
      StringBuilder _sb;

      try
      {
        PinPads = new DataTable();

        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   PCT_ID                      ");
        _sb.AppendLine("         , PCT_TYPE                    ");
        _sb.AppendLine("         , '' AS PCT_TYPE_NAME         ");
        _sb.AppendLine("         , PCT_PORT                    ");
        _sb.AppendLine("         , '' AS PCT_PORT_NAME         ");
        _sb.AppendLine("         , PCT_ENABLED                 ");
        _sb.AppendLine("         , '' AS PCT_ENABLED_NAME      ");
        _sb.AppendLine("    FROM   PINPAD_CASHIER_TERMINALS    ");
        _sb.AppendLine("   WHERE   PCT_CASHIER_ID = @pId       ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = CashierId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(PinPads);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadCashierTerminals.DB_GetDataTableByCashierId -> CashierId: {0}", CashierId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetDataTableByCashierId

    /// <summary>
    /// Update by Id
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_UpdateById(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   PINPAD_TRANSACTIONS                ");
        _sb.AppendLine("    SET   PCT_PORT          = @pPort         ");
        _sb.AppendLine("        , PCT_ENABLED       = @pEnabled      ");
        _sb.AppendLine("        , PCT_LAST_MODIFIED = @pLastModified ");
        _sb.AppendLine("  WHERE   PCT_ID            = @pId           ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          // _cmd.Parameters.Add("@pPort", SqlDbType.Bit).Value = Port;
          //_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Enabled;
          _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;
          // _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Id;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        //Log.Error(String.Format("PinPadCashierTerminals.DB_UpdateById -> Id: {0}", Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateById

    #endregion " Public Methods "
  }

  public class PinPadCashierTerminal
  {

    #region " Enums "

    public enum PINPAD_PORT
    {
      USB = 0,
      COM1 = 1,
      COM2 = 2,
      COM3 = 3,
      COM4 = 4,
      COM5 = 5,
      COM6 = 6,
      COM7 = 7,
      COM8 = 8,
      COM9 = 9,
    }

    #endregion " Enums "

    #region " Properties "

    public Int32 Id { get; set; }
    public Int32 CashierId { get; set; }
    public PinPadType Type { get; set; }
    public PINPAD_PORT Port { get; set; }
    public Boolean Enabled { get; set; }
    public DateTime LastModified { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Insert PinPad cashier terminal
    /// </summary>
    /// <returns></returns>
    public Boolean DB_Insert()
    {
      StringBuilder _sb;
      SqlParameter _parameter;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  INSERT INTO  PINPAD_CASHIER_TERMINALS ");
        _sb.AppendLine("             ( PCT_CASHIER_ID           ");
        _sb.AppendLine("             , PCT_TYPE                 ");
        _sb.AppendLine("             , PCT_PORT                 ");
        _sb.AppendLine("             , PCT_ENABLED              ");
        _sb.AppendLine("             , PCT_LAST_MODIFIED )      ");
        _sb.AppendLine("      VALUES                            ");
        _sb.AppendLine("             ( @pCashierId              ");
        _sb.AppendLine("             , @pType                   ");
        _sb.AppendLine("             , @pPort                   ");
        _sb.AppendLine("             , @pEnabled                ");
        _sb.AppendLine("             , @pLastModified )         ");
        _sb.AppendLine("   SET  @pId = SCOPE_IDENTITY()         ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CashierId;
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Object)Type;
            _cmd.Parameters.Add("@pPort", SqlDbType.Int).Value = (Object)Port;
            _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Enabled;
            _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;

            _parameter = _cmd.Parameters.Add("@pId", SqlDbType.Int);
            _parameter.Direction = ParameterDirection.Output;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              if (_parameter.Value != null)
              {
                this.Id = (Int32)_parameter.Value;

                _db_trx.Commit();

                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadCashierTerminal.DB_Insert -> CashierId: {0}", CashierId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Get Pin Pad cashier terminals by Id
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_GetById(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   PCT_ID                      ");
        _sb.AppendLine("         , PCT_TYPE                    ");
        _sb.AppendLine("         , PCT_PORT                    ");
        _sb.AppendLine("         , PCT_ENABLED                 ");
        _sb.AppendLine("         , PCT_LAST_MODIFIED           ");
        _sb.AppendLine("    FROM   PINPAD_CASHIER_TERMINALS    ");
        _sb.AppendLine("   WHERE   PT_CASHIER_ID = @pId        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Id;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {

            if (_reader.Read())
            {
              Id = (Int32)_reader[0];
              Type = (PinPadType)_reader[1];
              Port = (PINPAD_PORT)_reader[2];
              Enabled = (Boolean)_reader[3];
              LastModified = (DateTime)_reader[4];

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadCashierTerminal.DB_GetByCashierId -> Id: {0}", Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetById

    /// <summary>
    /// Update by Id
    /// </summary>
    /// <returns></returns>
    public Boolean DB_UpdateById()
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   PINPAD_CASHIER_TERMINALS           ");
        _sb.AppendLine("    SET   PCT_PORT          = @pPort         ");
        _sb.AppendLine("        , PCT_ENABLED       = @pEnabled      ");
        _sb.AppendLine("        , PCT_LAST_MODIFIED = @pLastModified ");
        _sb.AppendLine("  WHERE   PCT_ID            = @pId           ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pPort", SqlDbType.Int).Value = Port;
            _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = Enabled;
            _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;
            _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = Id;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadCashierTerminal.DB_UpdateById -> Id: {0}", Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateById

    #endregion " Public Methods "
  }
}
