﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.PinPad
{
  public class PinPadResponse : ICloneable
  {
    public String AuthorizationCode { get; set; }

    public String Reference { get; set; }

    public String ResponseMessage { get; set; }

    public DateTime ResponseDateTime { get; set; }

    public PinPadCard PinPadCard { get; set; }

    public PinPadTransactions.STATUS StatusResponse { get; set; }

    public String EMVData { get; set; }

    public String ResponsePay { get; set; }

    public String ControlNumber { get; set; }

    public String MerchandId { get; set; }

    public String ExpirationDate { get; set; }

    public String ProductType { get; set; }
    
    public String CardType { get; set; }

    public String AID { get; set; }

    public String TVR { get; set; }

    public String TSI { get; set; }

    public String APN { get; set; }

    public String AL { get; set; }

    public Boolean WithPin { get; set; }

    public object Clone()
    {
      return this.MemberwiseClone();
    }
  }
}
