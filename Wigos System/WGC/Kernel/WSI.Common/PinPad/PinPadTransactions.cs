﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : PinPadTransactions.cs
// 
//   DESCRIPTION : PinPad Transactions
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-JUN-2016 AMF    First release
// 06-OCT-2016 FAV    Fixed Bug 18280:Televisa: Add 'Canceled' status for TPV transactions
// 24-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge refactoring
// 26-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge Wigos Rollback + Pinpad + Wigos Commit
// 21-MAY-2017 ETP    Fixed Bug WIGOS-2997: Added voucher data.
// 19-OCT-2017 JML    Fixed Bug 30321:WIGOS-5924 [Ticket #9450] Fórmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesión de caja
// 29-NOV-2017 RAB    Bug 30942:WIGOS-6509 PinPad: wrong cashier movements when some vouchers are withdrawn
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.PinPad
{

  public enum CARD_TYPE
  {
    NONE = 0,
    DEBIT = 1,
    CREDIT = 2,
  }

  public enum CARD_SCOPE
  {
    NONE = 0,
    NATIONAL = 1,
    INTERNATIONAL = 2,
  }

  public struct bank_card
  {
    #region " Atributes "

    public CARD_TYPE card_type;
    public CARD_SCOPE card_scope;

    #endregion " Atributes "

    #region " Functions "

    public void Initialize(CARD_TYPE Card_type, CARD_SCOPE Card_scope)
    {
      card_type = Card_type;
      card_scope = Card_scope;
    }
    #endregion " Functions "

    }


  public class PinPadTransactions
  {

    #region " Enums "



    public enum STATUS
    {
      NO_RESPONSE = 0,
      APPROVED = 1,
      DECLINED = 2,
      REJECTED = 3,
      CANCELED = 4,
      INITIALIZED = 5,
      PENDING_WIGOS = 6,
      ERROR = 7
    }

    #endregion " Enums "

    #region " Properties "

    public Int64 Id { get; set; }
    public String Transaction { get; set; }
    public Int32 UserId { get; set; }
    public Int64 AccountId { get; set; }
    public DateTime Created { get; set; }
    public String BankName { get; set; }
    public String CardNumber { get; set; }
    public bank_card CardType { get; set; }
    public String CardIsoCode { get; set; }
    public String CardHolder { get; set; }
    public Currency TransactionAmount { get; set; }
    public Currency CommissionAmount { get; set; }
    public Currency TotalAmount { get; set; }
    public STATUS StatusCode { get; set; }
    public String ErrorMessage { get; set; }
    public String ControlNumber { get; set; }
    public String Reference { get; set; }
    public Int64? OperationId { get; set; }
    public Int64? MerchantId { get; set; }
    public String AuthCode { get; set; }
    public Boolean Collected { get; set; }
    public DateTime LastModified { get; set; }
    public Int64 Retry { get; set; }
    public Int64 PinPadId { get; set; }
    public String TerminalName { get; set; }

    #endregion " Properties "

    #region " Constructor "

    public PinPadTransactions()
    {
      Id = -1;
      Transaction = String.Empty;
      CardNumber = String.Empty;
      CardHolder = String.Empty;
      ErrorMessage = String.Empty;
    }

    #endregion " Constructor "


    #region " Public Methods "

    /// <summary>
    /// Insert PinPad transaction
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _parameter;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("IF NOT EXISTS (SELECT PT_ID  FROM PINPAD_TRANSACTIONS WHERE PT_ID = @pOldId)  ");
        _sb.AppendLine("  BEGIN                                                                                               ");
        _sb.AppendLine("  INSERT INTO  PINPAD_TRANSACTIONS                                                                    ");
        _sb.AppendLine("             ( PT_TRANSACTION                                                                         ");
        _sb.AppendLine("             , PT_USER_ID                                                                             ");
        _sb.AppendLine("             , PT_ACCOUNT_ID                                                                          ");
        _sb.AppendLine("             , PT_CREATED                                                                             ");
        _sb.AppendLine("             , PT_BANK_NAME                                                                           ");
        _sb.AppendLine("             , PT_CARD_NUMBER                                                                         ");
        _sb.AppendLine("             , PT_CARD_TYPE                                                                           ");
        _sb.AppendLine("             , PT_CARD_ISO_CODE                                                                       ");
        _sb.AppendLine("             , PT_CARD_HOLDER                                                                         ");
        _sb.AppendLine("             , PT_TRANSACTION_AMOUNT                                                                  ");
        _sb.AppendLine("             , PT_COMMISSION_AMOUNT                                                                   ");
        _sb.AppendLine("             , PT_TOTAL_AMOUNT                                                                        ");
        _sb.AppendLine("             , PT_STATUS                                                                              ");
        _sb.AppendLine("             , PT_ERROR_MESSAGE                                                                       ");
        _sb.AppendLine("             , PT_CONTROL_NUMBER                                                                      ");
        _sb.AppendLine("             , PT_REFERENCE                                                                           ");
        _sb.AppendLine("             , PT_RETRY                                                                               ");
        _sb.AppendLine("             , PT_OPERATION_ID                                                                        ");
        _sb.AppendLine("             , PT_MERCHANT_ID                                                                         ");
        _sb.AppendLine("             , PT_AUTH_CODE                                                                           ");
        _sb.AppendLine("             , PT_COLLECTED                                                                           ");
        _sb.AppendLine("             , PT_CASHIER_NAME                                                                        ");
        _sb.AppendLine("             , PT_PINPAD_ID                                                                           ");
        _sb.AppendLine("             , PT_LAST_MODIFIED )                                                                     ");
        _sb.AppendLine("      VALUES                                                                                          ");
        _sb.AppendLine("             ( @pTransaction                                                                          ");
        _sb.AppendLine("             , @pUserId                                                                               ");
        _sb.AppendLine("             , @pAccountId                                                                            ");
        _sb.AppendLine("             , @pCreated                                                                              ");
        _sb.AppendLine("             , @pBankName                                                                             ");
        _sb.AppendLine("             , @pCardNumber                                                                           ");
        _sb.AppendLine("             , @pCardType                                                                             ");
        _sb.AppendLine("             , @pCardIsoCode                                                                          ");
        _sb.AppendLine("             , @pCardHolder                                                                           ");
        _sb.AppendLine("             , @pTransactionAmount                                                                    ");
        _sb.AppendLine("             , @pCommissionAmount                                                                     ");
        _sb.AppendLine("             , @pTotalAmount                                                                          ");
        _sb.AppendLine("             , @pStatus                                                                               ");
        _sb.AppendLine("             , @pErrorMessage                                                                         ");
        _sb.AppendLine("             , @pControlNumber                                                                        ");
        _sb.AppendLine("             , @pReference                                                                            ");
        _sb.AppendLine("             , @pRetry                                                                                ");
        _sb.AppendLine("             , @pOperationId                                                                          ");
        _sb.AppendLine("             , @pMerchantId                                                                           ");
        _sb.AppendLine("             , @pAuthCode                                                                             ");
        _sb.AppendLine("             , @pCollected                                                                            ");
        _sb.AppendLine("             , @pCashierName                                                                          ");
        _sb.AppendLine("             , @pPinPadId                                                                             ");
        _sb.AppendLine("             , @pLastModified )                                                                       ");
        _sb.AppendLine("   SET  @pId = SCOPE_IDENTITY()                                                                       ");
        _sb.AppendLine("   END                                                                                                ");
        _sb.AppendLine("ELSE                                                                                                  ");
        _sb.AppendLine("  BEGIN                                                                                               ");
        _sb.AppendLine("    UPDATE pinpad_transactions                                                                        ");
        _sb.AppendLine("     SET       PT_TRANSACTION          = @pTransaction                                                ");
        _sb.AppendLine("             , PT_USER_ID              = @pUserId                                                     ");
        _sb.AppendLine("             , PT_ACCOUNT_ID           = @pAccountId                                                  ");
        _sb.AppendLine("             , PT_CREATED              = @pCreated                                                    ");
        _sb.AppendLine("             , PT_BANK_NAME            = @pBankName                                                   ");
        _sb.AppendLine("             , PT_CARD_NUMBER          = @pCardNumber                                                 ");
        _sb.AppendLine("             , PT_CARD_TYPE            = @pCardType                                                   ");
        _sb.AppendLine("             , PT_CARD_ISO_CODE        = @pCardIsoCode                                                ");
        _sb.AppendLine("             , PT_CARD_HOLDER          = @pCardHolder                                                 ");
        _sb.AppendLine("             , PT_TRANSACTION_AMOUNT   = @pTransactionAmount                                          ");
        _sb.AppendLine("             , PT_COMMISSION_AMOUNT    = @pCommissionAmount                                           ");
        _sb.AppendLine("             , PT_TOTAL_AMOUNT         = @pTotalAmount                                                ");
        _sb.AppendLine("             , PT_STATUS               = @pStatus                                                     ");
        _sb.AppendLine("             , PT_ERROR_MESSAGE        = @pErrorMessage                                               ");
        _sb.AppendLine("             , PT_CONTROL_NUMBER       = @pControlNumber                                              ");
        _sb.AppendLine("             , PT_REFERENCE            = @pReference                                                  ");
        _sb.AppendLine("             , PT_RETRY                = @pRetry                                                      ");
        _sb.AppendLine("             , PT_OPERATION_ID         = @pOperationId                                                ");
        _sb.AppendLine("             , PT_MERCHANT_ID          = @pMerchantId                                                 ");
        _sb.AppendLine("             , PT_AUTH_CODE            = @pAuthCode                                                   ");
        _sb.AppendLine("             , PT_COLLECTED            = @pCollected                                                  ");
        _sb.AppendLine("             , PT_CASHIER_NAME         = @pCashierName                                                ");
        _sb.AppendLine("             , PT_PINPAD_ID            = @pPinPadId                                                   ");
        _sb.AppendLine("             , PT_LAST_MODIFIED        = @pLastModified                                               ");
        _sb.AppendLine("        WHERE  PT_ID = @pOldId                                                                        ");
        _sb.AppendLine("   SET  @pId = @pOldId                                                                                ");
        _sb.AppendLine("  END                                                                                                 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTransaction", SqlDbType.NVarChar, 50).Value = Transaction;
          _cmd.Parameters.Add("@pOldId", SqlDbType.NVarChar, 50).Value = Id;
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pBankName", SqlDbType.NVarChar, 20).Value = (BankName == null) ? String.Empty : BankName;
          _cmd.Parameters.Add("@pCardNumber", SqlDbType.NVarChar, 16).Value = (CardNumber == null) ? String.Empty : CardNumber;
          _cmd.Parameters.Add("@pCardType", SqlDbType.Int).Value = CardType.card_type;
          _cmd.Parameters.Add("@pCardIsoCode", SqlDbType.NVarChar, 3).Value = CardIsoCode;
          _cmd.Parameters.Add("@pCardHolder", SqlDbType.NVarChar, 50).Value = (CardHolder == null) ? String.Empty : CardHolder; ;
          _cmd.Parameters.Add("@pTransactionAmount", SqlDbType.Money).Value = (Decimal)TransactionAmount;
          _cmd.Parameters.Add("@pCommissionAmount", SqlDbType.Money).Value = (Decimal)CommissionAmount;
          _cmd.Parameters.Add("@pTotalAmount", SqlDbType.Money).Value = (Decimal)TotalAmount;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = StatusCode;
          _cmd.Parameters.Add("@pErrorMessage", SqlDbType.NVarChar, 200).Value = (String.IsNullOrEmpty(ErrorMessage)) ? DBNull.Value : (Object)ErrorMessage;
          _cmd.Parameters.Add("@pReference", SqlDbType.NVarChar, 50).Value = (Reference == null) ? DBNull.Value : (Object)Reference;
          _cmd.Parameters.Add("@pControlNumber", SqlDbType.NVarChar, 50).Value = ControlNumber;
          _cmd.Parameters.Add("@pRetry", SqlDbType.BigInt).Value = Retry;
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = (OperationId == null) ? DBNull.Value : (Object)OperationId;
          _cmd.Parameters.Add("@pMerchantId", SqlDbType.BigInt).Value = (MerchantId == null) ? DBNull.Value : (Object)MerchantId;
          _cmd.Parameters.Add("@pAuthCode", SqlDbType.NVarChar, 10).Value = (AuthCode == null) ? DBNull.Value : (Object)AuthCode;
          _cmd.Parameters.Add("@pCollected", SqlDbType.Bit).Value = Collected;
          _cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar).Value = TerminalName;
          _cmd.Parameters.Add("@pPinPadId", SqlDbType.BigInt).Value = PinPadId;
          _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;

          _parameter = _cmd.Parameters.Add("@pId", SqlDbType.BigInt);
          _parameter.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            if (_parameter.Value != null)
            {
              Id = (Int64)_parameter.Value;

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadTransactions.DB_Insert -> Transaction: {0}", Transaction));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update Cashier Session Has PinPad Operation
    /// </summary>
    /// <param name="Mode"></param>
    /// <returns></returns>
    public Boolean UpdateCashierSessionHasPinPadOperation(Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   CASHIER_SESSIONS                       ");
        _sb.AppendLine("   SET   CS_HAS_PINPAD_OPERATIONS = 1           ");
        _sb.AppendLine(" WHERE   CS_SESSION_ID            = @pSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateCashierSessionHasPinPadOperation

    /// <summary>
    /// Get Cashier Session Has PinPad Operation
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean GetCashierSessionHasPinPadOperation(Int64 CashierSessionId, String SessionQuery, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _has_pinpad_operation;
      
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   MAX(CS_HAS_PINPAD_OPERATIONS)             ");
        _sb.AppendLine("  FROM   CASHIER_SESSIONS                          ");
        
        if (!String.IsNullOrEmpty(SessionQuery) && CashierSessionId == 0)
        {
          _sb.AppendLine(" WHERE   CS_SESSION_ID               IN ( " + SessionQuery + ") ");
        }
        else
        {
          _sb.AppendLine(" WHERE   CS_SESSION_ID               = @pSessionId ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          if (CashierSessionId != 0 || String.IsNullOrEmpty(SessionQuery))
          {
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          }
          _has_pinpad_operation = _cmd.ExecuteScalar();

          if (_has_pinpad_operation == null || _has_pinpad_operation == DBNull.Value)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCashierSessionHasPinPadOperation

    /// <summary>
    /// Get Pin Pad Transaction by Id
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_GetById(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   PT_TRANSACTION        ");
        _sb.AppendLine("         , PT_USER_ID            ");
        _sb.AppendLine("         , PT_ACCOUNT_ID         ");
        _sb.AppendLine("         , PT_CREATED            ");
        _sb.AppendLine("         , PT_BANK_NAME          ");
        _sb.AppendLine("         , PT_CARD_NUMBER        ");
        _sb.AppendLine("         , PT_CARD_ISO_CODE      ");
        _sb.AppendLine("         , PT_CARD_TYPE          ");
        _sb.AppendLine("         , PT_CARD_HOLDER        ");
        _sb.AppendLine("         , PT_TRANSACTION_AMOUNT ");
        _sb.AppendLine("         , PT_COMMISSION_AMOUNT  ");
        _sb.AppendLine("         , PT_TOTAL_AMOUNT       ");
        _sb.AppendLine("         , PT_STATUS             ");
        _sb.AppendLine("         , PT_ERROR_MESSAGE      ");
        _sb.AppendLine("         , PT_CONTROL_NUMBER     ");
        _sb.AppendLine("         , PT_REFERENCE          ");
        _sb.AppendLine("         , PT_OPERATION_ID       ");
        _sb.AppendLine("         , PT_RETRY              ");
        _sb.AppendLine("         , PT_MERCHANT_ID        ");
        _sb.AppendLine("         , PT_AUTH_CODE          ");
        _sb.AppendLine("         , PT_COLLECTED          ");
        _sb.AppendLine("         , PT_LAST_MODIFIED      ");
        _sb.AppendLine("         , PT_CASHIER_NAME       ");
        _sb.AppendLine("         , PT_PINPAD_ID          ");
        _sb.AppendLine("    FROM   PINPAD_TRANSACTIONS   ");
        _sb.AppendLine("   WHERE   PT_ID = @pId          ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Id;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {

            if (_reader.Read())
            {
              Transaction = (String)_reader[0];
              UserId = (Int32)_reader[1];
              AccountId = (Int64)_reader[2];
              Created = (DateTime)_reader[3];
              BankName = _reader.IsDBNull(4) ? String.Empty : (String)_reader[4];
              CardNumber = _reader.IsDBNull(5) ? String.Empty : (String)_reader[5];

              
              CardIsoCode = _reader.IsDBNull(7) ? String.Empty : (String)_reader[6];

              CardType.Initialize((CARD_TYPE)_reader[7], CardIsoCode == CurrencyExchange.GetNationalCurrency() ? CARD_SCOPE.NATIONAL : CARD_SCOPE.INTERNATIONAL);

              CardHolder = _reader.IsDBNull(8) ? String.Empty : (String)_reader[8];
              TransactionAmount = (Currency)(Decimal)_reader[9];
              CommissionAmount = (Currency)(Decimal)_reader[10];
              TotalAmount = (Decimal)_reader[11];
              StatusCode = (STATUS)_reader[12];
              ErrorMessage = _reader.IsDBNull(13) ? String.Empty : (String)_reader[13];
              ControlNumber = _reader.IsDBNull(14) ? String.Empty : (String)_reader[14];
              Reference = _reader.IsDBNull(15) ? String.Empty : (String)_reader[15];
              Retry = (Int64)_reader[16];
              OperationId = _reader.IsDBNull(17) ? (Int64?)null : (Int64)_reader[17];
              MerchantId = _reader.IsDBNull(18) ? (Int64?)null : (Int64)_reader[18];
              AuthCode = _reader.IsDBNull(19) ? String.Empty : (String)_reader[19];
              Collected = (Boolean)_reader[20];
              LastModified = (DateTime)_reader[21];

              TerminalName = _reader.IsDBNull(22) ? String.Empty : (String)_reader[22];
              PinPadId = _reader.IsDBNull(23) ? 0 : (Int64)_reader[23];

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadTransactions.DB_GetById -> Id: {0}", Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetById

    /// <summary>
    /// Update Collected by Id
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_UpdateCollectedById(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   PINPAD_TRANSACTIONS               ");
        _sb.AppendLine("    SET   PT_COLLECTED     = @pCollected    ");
        _sb.AppendLine("        , PT_LAST_MODIFIED = @pLastModified ");
        _sb.AppendLine("  WHERE   PT_ID            = @pId           ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCollected", SqlDbType.Bit).Value = Collected;
          _cmd.Parameters.Add("@pLastModified", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Id;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadTransactions.DB_UpdateCollectedById -> Id: {0}", Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateCollectedById

    /// <summary>
    /// Get Retry By OperationId
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_GetRetryById(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   PT_RETRY AS   Retry             ");
        _sb.AppendLine("    FROM   PINPAD_TRANSACTIONS             ");
        _sb.AppendLine("   WHERE   PT_ID = @pID ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = Id;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              if (!_reader.IsDBNull(0))
              {
                Retry = (Int64)_reader[0];
                return true;
              }

            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PinPadTransactions.DB_GetRetryByOperationId -> OperationId: {0}", OperationId));
        Log.Exception(_ex);
      }

      return false;

    } //DB_GetRetryByOperationId



    /// <summary>
    /// Insert To Undo PinPad
    /// </summary>
    /// <param name="ControlNumber"></param>
    /// <param name="PinPadModel"></param>
    /// <param name="TerminalID"></param>
    /// <param name="MinutesToAdd"></param>
    /// <param name="SQLTrans"></param>
    /// <returns></returns>
    public static Boolean InsertToUndoPinPad(String ControlNumber, PinPadType PinPadModel, String TerminalID, Int32 MinutesToAdd, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DateTime _delete_after;

      try
      {
        _sb.AppendLine("INSERT INTO   UNDO_PIN_PAD_TRANSACTION   ");
        _sb.AppendLine("            ( UPPT_CONTROL_NUMBER        ");
        _sb.AppendLine("            , UPPT_PINPADMODEL           ");
        _sb.AppendLine("            , UPPT_TERMINAL_ID           ");
        _sb.AppendLine("            , UPPT_CANCEL_RETRIES        ");
        _sb.AppendLine("            , UPPT_LAST_UPDATE    )      ");
        _sb.AppendLine("     VALUES (  @pControlNumber           ");
        _sb.AppendLine("             , @pType                    ");
        _sb.AppendLine("             , @pTerminalId              ");
        _sb.AppendLine("             , 0                         ");
        _sb.AppendLine("             , @pNow      )              ");

        _delete_after = WGDB.Now.AddMinutes(MinutesToAdd);

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _cmd.Parameters.Add("@pControlNumber", SqlDbType.NVarChar).Value = ControlNumber;
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Int32)PinPadModel;
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.NVarChar).Value = TerminalID;
          _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = _delete_after;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              return true;
            }

          }
        }
      catch (Exception Ex)
      {
        Log.Error("InsertToUndoPinPad not saved Control Number = " + ControlNumber);
        Log.Exception(Ex);
      }
      return false;
    } //InsertToUndoPinPad

    /// <summary>
    /// Delete From Undo PinPad
    /// </summary>
    /// <param name="ControlNumber"></param>
    /// <param name="PinPadModel"></param>
    /// <param name="SQLTrans"></param>
    /// <returns></returns>
    public static Boolean DeleteFromUndoPinPad(String ControlNumber, SqlTransaction SQLTrans)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("DELETE FROM  UNDO_PIN_PAD_TRANSACTION                 ");
        _sb.AppendLine("      WHERE  UPPT_CONTROL_NUMBER  =  @pControlNumber  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SQLTrans.Connection, SQLTrans))
        {
          _cmd.Parameters.Add("@pControlNumber", SqlDbType.NVarChar).Value = ControlNumber;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception Ex)
      {
        Log.Error("DeleteFromUndoPinPad not saved Control Number = " + ControlNumber);
        Log.Exception(Ex);
      }

      return false;
    } //DeleteFromUndoPinPad

    public static Boolean UpdateDateUndoPinPad(String ControlNumber, SqlTransaction SQLTrans)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("UPDATE  UNDO_PIN_PAD_TRANSACTION                       ");
        _sb.AppendLine("   SET  UPPT_LAST_UPDATE = @pNow   ");
        _sb.AppendLine(" WHERE  UPPT_CONTROL_NUMBER    =  @pControlNumber      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SQLTrans.Connection, SQLTrans))
        {
          _cmd.Parameters.Add("@pControlNumber", SqlDbType.NVarChar).Value = ControlNumber;
          _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = WGDB.Now;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception Ex)
      {
        Log.Error("UpdateUndoPinPad not saved Control Number = " + ControlNumber);
        Log.Exception(Ex);
      }

      return false;
    } //DeleteFromUndoPinPad

    public Boolean CreateVoucher(CurrencyExchangeResult ExchangeResult, PinPadOutput VoucherData, SqlTransaction Trx, out ArrayList VoucherList)
    {
      VoucherCardAddPinPadTransaction _temp_voucher;
      VoucherCardAddPinPadTransaction _temp_voucher_copy;

      VoucherList = new ArrayList();
      try
      {
        if (this.OperationId == null)
        {

          return false;
        }

        if (GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.BankCard.HideVoucher", false))
        {
          return true;
        }

        _temp_voucher = new VoucherCardAddPinPadTransaction(ExchangeResult,
                                                             CurrencyExchange.GetNationalCurrency(),
                                                             PrintMode.Print,
                                                             false,
                                                             VoucherData,
                                                             Trx, 
                                                             VoucherCardAddPinPadTransaction.VoucherPinPadCopyType.COMPANY);
        VoucherList.Add(_temp_voucher);
        _temp_voucher.Save((long)this.OperationId, Trx);

        _temp_voucher_copy = new VoucherCardAddPinPadTransaction(ExchangeResult,
                                                     CurrencyExchange.GetNationalCurrency(),
                                                     PrintMode.Print,
                                                     false,
                                                     VoucherData,
                                                     Trx,
                                                     VoucherCardAddPinPadTransaction.VoucherPinPadCopyType.COSTUMER);

        VoucherList.Add(_temp_voucher_copy);
        _temp_voucher_copy.Save((long)this.OperationId, Trx);

        return true;
      }
      catch (Exception Ex)
      {
        Log.Error("PinPadTransactions.CreateVoucher = " + ControlNumber);
        Log.Exception(Ex);
      }
      return false;

    }


    #endregion " Public Methods "
  }
}
