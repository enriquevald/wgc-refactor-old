﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using WSI.Common.PinPad;


namespace WSI.Common.PinPad
{
  public interface IPinPad
  {
    Boolean InitDevice(PinPadCashierTerminal PinPadCashierTerminal);

    Boolean ReleaseDevice();

    void ResetDevice();

    void StartTransaction();

    void EndTransaction();

    Boolean IsAvailable();

    Boolean Pay(Decimal Amount, String ControlNumber, out PinPadResponse PinPadResponse);

    Boolean Read(out PinPadCard PinPadCard);

    Boolean Cancel(String ControlNumber, out PinPadResponse ResponseOut);

    Boolean Reverse(String ControlNumber, out PinPadResponse ResponseOut);

    Boolean SendTransaction(PinPadCard PinPadCard, Decimal Amount, String ControlNumber, out PinPadResponse PinPadResponse);

    Boolean NotifyResult(PinPadResponse ResponseIn, out PinPadResponse ResponseOut);

    PinPadType GetPinPadType();

    String GetPinPadMode();

    Boolean IsDriverInstalled();

    Boolean InstallDriver();

    String GetPinPadTerminalID();

    void SetPinPadTerminalID(String TerminalId);

    void Initialize();

  }
}
