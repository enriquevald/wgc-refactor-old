//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Partner.cs
// 
//   DESCRIPTION: Class to manage Partners
// 
//        AUTHOR: R. Dar�o Soria
// 
// CREATION DATE: 26-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-NOV-2015 RDS    First release.
// 04-OCT-2016 JBP    PBI 18464:PariPlay - Ajustar pantallas de BonoPlay
// 22-JUN-2017 FJC    WIGOS-3142 WS2S - Set / Retrieve Reserved Credit on the Cashier
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;
using System.Data;

namespace WSI.Common
{
  public static class Partner
  {
    #region Constants
    private const Int32 REFRESH_PERIOD = 2 * 60 * 1000; // 2 minutes
    private const Int32 ERROR_WAIT_HINT = 10 * 1000;    // 10 seconds 
    #endregion

    #region Members
    private static ManualResetEvent m_ev_values_available = new ManualResetEvent(false);
    private static Dictionary<Int32, String> m_partners = new Dictionary<Int32, String>();
    private static Boolean m_init = Partner.Init();
    private static Int64 m_num_read = 0; 
    #endregion

    #region Properties
    public static Dictionary<Int32, String> Partners
    {
      get
      {
        // Wait till the values have been retrieved from the DB
        m_ev_values_available.WaitOne();
        return Partner.m_partners;
      }
 
    } 
    #endregion


    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Creates the auto-refresh thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : This function is "automatically" called on the first call to Partners
    //
    private static Boolean Init()
    {
      Thread _thread = null;

      if (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("LKS_VC_DEV")))
      {
        if (System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv")
        {
          m_ev_values_available.Set();

          return true;
        }
      }

      _thread = new Thread(new ThreadStart(PartnerThread));
      _thread.Start();

      return true;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Automatic reload of the General Params
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //        The value of the selected General Param
    //
    //   NOTES :
    //
    private static void PartnerThread()
    {
      StringBuilder _sb;

      while (true)
      {
        try
        {
          // Ensure DB is initialized
          while (!WGDB.Initialized)
          {
            Thread.Sleep(250);
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sb = new StringBuilder();

            // Dynamic Providers GameGateway
            if (Misc.IsGameGatewayEnabled())
            {
              _sb.AppendLine("SELECT   CAST(PIDS.PID AS INT) AS PARTNER_ID                      ");
              _sb.AppendLine("       , PNAS.PNAME + ' (' + PIDS.PID + ')' AS PARTNER_NAME       ");
              _sb.AppendLine("  FROM                                                            ");
              _sb.AppendLine("     (SELECT   GP_KEY_VALUE AS PID                                ");
              _sb.AppendLine("        FROM   GENERAL_PARAMS                                     ");
              _sb.AppendLine("       WHERE   GP_GROUP_KEY  = 'GameGateway'                      ");
              _sb.AppendLine("         AND   GP_SUBJECT_KEY LIKE 'Provider.%.PartnerId') PIDS   ");
              _sb.AppendLine("  INNER JOIN                                                      ");
              _sb.AppendLine("     (SELECT   GP_KEY_VALUE AS PNAME                              ");
              _sb.AppendLine("             , GP_SUBJECT_KEY AS PNAMEKEY                         ");
              _sb.AppendLine("        FROM   GENERAL_PARAMS                                     ");
              _sb.AppendLine("       WHERE   GP_GROUP_KEY  = 'GameGateway'                      ");
              _sb.AppendLine("         AND   GP_SUBJECT_KEY LIKE 'Provider.%.Name') PNAS        ");
              _sb.AppendLine("          ON   PIDS.PID = SUBSTRING (PNAS.PNAMEKEY,10,3)          ");              
            }

            // Provider: ParyPlay.
            if( GeneralParam.GetBoolean("PariPlay", "Enabled", false) )
            {
              // Add union y GameGateway is enabled.
              if (Misc.IsGameGatewayEnabled())
              {
                _sb.AppendLine("            UNION                                               ");
              }

              _sb.AppendLine("-- PariPlay                                                       ");
              _sb.AppendLine("     ( SELECT   2	AS PARTNER_ID                                   ");
              _sb.AppendLine("              , ( SELECT  GP_KEY_VALUE                            ");
              _sb.AppendLine("                    FROM  GENERAL_PARAMS                          ");
              _sb.AppendLine("                   WHERE  GP_GROUP_KEY	= 'PariPlay'              ");
              _sb.AppendLine("                     AND  GP_SUBJECT_KEY	= 'Name'	 )            ");
              _sb.AppendLine("                   AS PARTNER_NAME )                              ");
            }

            _sb.AppendLine("    ORDER BY   PARTNER_ID ASC                                       ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
            {
              using (SqlDataReader _reader = _db_trx.ExecuteReader(_sql_cmd))
              {
                lock (m_partners)
                {
                  // Remove all the parameters & add the just read ones.
                  m_partners.Clear();
                  while (_reader.Read())
                  {
                    m_partners.Add(_reader.GetInt32(0), _reader.GetString(1));
                  }
                }
                // Notify 'Parameters Available'
                m_ev_values_available.Set();
              }
            }
          }
          Thread.Sleep(REFRESH_PERIOD);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Thread.Sleep(ERROR_WAIT_HINT);
        }
      }

    } // PartnerThread 
    #endregion

    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Return the value from the Partners (in memory)
    //
    //  PARAMS :
    //      - INPUT :
    //          - Id
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //        The value of the selected Key
    //
    //   NOTES :
    //
    public static String Value(Int32 Id)
    {
      String _value;

      // Wait till the values have been retrieved from the DB
      m_ev_values_available.WaitOne();

      // Get the value
      lock (m_partners)
      {
        if (!m_partners.TryGetValue(Id, out _value))
        {
          _value = String.Empty;
        }
      }

      Interlocked.Increment(ref m_num_read);

      return _value;
    } // Value
    
    #endregion
  }
}
