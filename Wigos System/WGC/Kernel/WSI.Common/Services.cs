using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public class Services
  {
    static public int CHECK_TIMEOUT = 5000; // Every CHECK_TIMEOUT miliseconds the thread will check for running timeouts
    static public int CHECK_RUNNING = 3000; // Every CHECK_RUNNING miliseconds the running status will be refreshed
    static public int SERVICE_TIMEOUT = 30; // If the Running has SERVICE_TIMEOUT seconds of inactivity it will be updated to UNKNOWN Status.
    static public int NUMBER_OF_RETRIES = 3; // It will determine the number of retries once the service detects that its not the unique.
    static public string m_status = "UNKNOWN";

    private const String EMPTY_IP = "0.0.0.0";

    /// <summary>
    /// Inserts a new Service, if the service already exists returns false
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="ServiceIpAddress"></param>
    /// <param name="Status"></param>
    /// <param name="ServiceVersion"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    static public bool DB_InsertService(String Protocol, String ServiceIpAddress, String Status, String ServiceVersion, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;

      //
      // Insert Service
      //
      sql_str = "INSERT INTO SERVICES (SVC_IP_ADDRESS, SVC_LAST_ACCESS, SVC_STATUS, SVC_PROTOCOL, SVC_MACHINE) " +
          "VALUES (@p1, GETDATE(), @p2 ,@p3, @p4) ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "SVC_IP_ADDRESS").Value = ServiceIpAddress;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 50, "SVC_STATUS").Value = Status;
      sql_command.Parameters.Add("@p3", SqlDbType.NVarChar, 50, "SVC_PROTOCOL").Value = Protocol;
      sql_command.Parameters.Add("@p4", SqlDbType.NVarChar, 50, "SVC_MACHINE").Value = Environment.MachineName;

      try
      {
        if (sql_command.ExecuteNonQuery() == 1)
        {
          return true;
        }
      }
      catch
      {
      }

      return false;
    } //DB_InsertService

    /// <summary>
    /// Checks and verify if the running service is not the only one in the DB
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    static public bool DB_CheckLivingService(String Protocol, SqlTransaction Trx)
    {
      Random rnd;
      String machine;
      int idx_retries;

      rnd = new Random();
      machine = Environment.MachineName;

      if (DB_GetRunningsButMe(machine, Protocol, Trx) > 0)
      {
        for (idx_retries = 0; idx_retries < NUMBER_OF_RETRIES; idx_retries++)
        {
          System.Threading.Thread.Sleep(rnd.Next(2000));

          if (DB_LockServices(Protocol, Trx))
          {
            if (DB_GetRunningsButMe(machine, Protocol, Trx) == 0)
            {
              return false;
            }
          }
        }

        return true;
      }

      return false;
    } //DB_CheckLivingService

    /// <summary>
    /// Updates the status of the current service.
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="ServiceIpAddress"></param>
    /// <param name="Status"></param>
    /// <param name="ServiceVersion"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    static public bool DB_UpdateService(String Protocol, String ServiceIpAddress, String Status, String ServiceVersion, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;

      //
      // Update Service
      //
      sql_str = "UPDATE SERVICES SET SVC_IP_ADDRESS     = @p1 " +
                               "   , SVC_LAST_ACCESS    = GETDATE() " +
                               "   , SVC_STATUS         = @p2 " +
                               "   , SVC_VERSION        = @p5 " +
                          " WHERE SVC_PROTOCOL          = @p3 " +
                          "   AND SVC_MACHINE           = @p4 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "SVC_IP_ADDRESS").Value = ServiceIpAddress;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 50, "SVC_STATUS").Value = Status;
      sql_command.Parameters.Add("@p3", SqlDbType.NVarChar, 50, "SVC_PROTOCOL").Value = Protocol;
      sql_command.Parameters.Add("@p4", SqlDbType.NVarChar, 50, "SVC_MACHINE").Value = Environment.MachineName;
      sql_command.Parameters.Add("@p5", SqlDbType.NVarChar, 50, "SVC_IP_ADDRESS").Value = ServiceVersion;

      try
      {
        if (sql_command.ExecuteNonQuery() == 1)
        {
          if (Status != m_status)
          {
            Log.Message(Environment.MachineName + @"\\" + Protocol + " service status changed: " + m_status + " --> " + Status);
            m_status = Status;
          }

          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } //DB_UpdateService

    /// <summary>
    /// Updates the status of the current service.
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    static public bool DB_LockServices(String Protocol, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;

      //
      // Update Service
      //
      sql_str = "UPDATE SERVICES SET SVC_STATUS         = SVC_STATUS " +
                          " WHERE SVC_PROTOCOL          = @p3 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p3", SqlDbType.NVarChar, 50, "SVC_PROTOCOL").Value = Protocol;

      try
      {
        sql_command.ExecuteNonQuery();

        return true;
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == -2) // Connection timeout
        {
          Log.Warning(" *** Sql Database Timeout: Locking services ...");
        }
        else
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } //DB_LockServices

    /// <summary>
    /// Updates the services running that are stopped more than SERVICE_TIMEOUT seconds
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    static public void DB_UpdateTimeOuts(String Protocol, SqlTransaction Trx)
    {
      SqlCommand _cmd;
      String _sql;

      //
      // Update Service
      //
      _sql = "UPDATE SERVICES SET SVC_LAST_ACCESS          = GETDATE() " +
                               "   , SVC_STATUS            = 'UNKNOWN' " +
                          "    WHERE SVC_PROTOCOL          = @p1 " +
                          "      AND (SVC_STATUS <> 'UNKNOWN') " +
                          "      AND (DATEDIFF(SECOND,SVC_LAST_ACCESS,GETDATE())) > @p2" +
                          "      AND SVC_MACHINE <> @p3 ";

      _cmd = new SqlCommand(_sql);
      _cmd.Connection = Trx.Connection;
      _cmd.Transaction = Trx;

      _cmd.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "SVC_PROTOCOL").Value = Protocol;
      _cmd.Parameters.Add("@p2", SqlDbType.Int, 8, "").Value = SERVICE_TIMEOUT;
      _cmd.Parameters.Add("@p3", SqlDbType.NVarChar, 50, "SVC_MACHINE").Value = Environment.MachineName;

      _cmd.CommandTimeout = 5; // 03-SEP-2012, Timeout after 5 seconds

      try
      {
        _cmd.ExecuteNonQuery();
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == -2) // command timeout
        {
          // Do nothing
        }
        else
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    } //DB_UpdateTimeOuts

    /// <summary>
    /// Returns the running services of the given protocol
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    static public int DB_GetRunnings(String Protocol, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      int runnings;

      runnings = 0;

      sql_str = "SELECT COUNT(SVC_STATUS) FROM SERVICES " +
                                       " WHERE SVC_STATUS = 'RUNNING' " +
                                       "   AND SVC_PROTOCOL = @p1 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "SVC_PROTOCOL").Value = Protocol;

      try
      {
        runnings = (int)sql_command.ExecuteScalar();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return runnings;
    } //DB_GetRunnings

    /// <summary>
    /// Returns the running services of the given protocol except the calling service
    /// </summary>
    /// <param name="Machine"></param>
    /// <param name="Protocol"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    static public int DB_GetRunningsButMe(String Machine, String Protocol, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      int runnings;

      runnings = 0;

      sql_str = "SELECT COUNT(SVC_STATUS) FROM SERVICES " +
                                       " WHERE SVC_STATUS = 'RUNNING' " +
                                       "   AND SVC_PROTOCOL = @p1 " +
                                       "   AND SVC_MACHINE <> @p2 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "SVC_PROTOCOL").Value = Protocol;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 50, "SVC_MACHINE").Value = Machine;

      try
      {
        runnings = (int)sql_command.ExecuteScalar();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return runnings;
    } //DB_GetRunnings

    /// <summary>
    /// Returns the status of the current service
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    static public String DB_GetStatus(String Protocol, SqlTransaction Trx)
    {
      String status;
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;
      String machine;

      reader = null;
      status = "";
      machine = Environment.MachineName;

      sql_str = "SELECT SVC_STATUS FROM SERVICES        " +
                                " WHERE SVC_PROTOCOL  = @p1 " +
                                "   AND SVC_MACHINE   = @p2 ";


      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "SVC_PROTOCOL").Value = Protocol;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 50, "SVC_MACHINE").Value = machine;

      try
      {
        reader = sql_command.ExecuteReader();

        if (reader.Read())
        {
          status = (String)reader.GetValue(0);
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == -2) // Connection timeout
        {
          Log.Warning(" *** Sql Database Timeout. Return Status RUNNING.");

          return "RUNNING";
        }
        else
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      reader.Close();
      reader = null;

      return status;
    } //DB_GetStatus

    /// <summary>
    /// Get principal service
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static String DB_GetPrincipalService(String Protocol, SqlTransaction Trx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine("DECLARE @Primary     NVARCHAR(50)");
        _sb.AppendLine();
        _sb.AppendLine("SELECT   TOP 1 @Primary = SVC_MACHINE ");
        _sb.AppendLine("  FROM   SERVICES");
        _sb.AppendLine(" WHERE   SVC_PROTOCOL = @Protocol");
        _sb.AppendLine("   AND   SVC_STATUS   = 'RUNNING'");
        _sb.AppendLine("   AND   SVC_MACHINE  = ServerProperty ('SERVERNAME')");
        _sb.AppendLine("   AND   DATEDIFF(SECOND, SVC_LAST_ACCESS, GETDATE ()) <= @Timeout");
        _sb.AppendLine(" ORDER BY SVC_MACHINE ASC");
        _sb.AppendLine();
        _sb.AppendLine("IF @Primary IS NULL ");
        _sb.AppendLine("SELECT   TOP 1 @Primary = SVC_MACHINE ");
        _sb.AppendLine("  FROM   SERVICES");
        _sb.AppendLine(" WHERE   SVC_PROTOCOL = @Protocol");
        _sb.AppendLine("   AND   SVC_STATUS   = 'RUNNING'");
        _sb.AppendLine("   AND   DATEDIFF(SECOND, SVC_LAST_ACCESS, GETDATE ()) <= @Timeout");
        _sb.AppendLine(" ORDER BY SVC_MACHINE ASC");
        _sb.AppendLine();
        _sb.AppendLine("SELECT @Primary");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          Object _obj;
          String _principal;

          _cmd.Parameters.Add("@Protocol", SqlDbType.NVarChar, 50).Value = Protocol;
          _cmd.Parameters.Add("@Timeout", SqlDbType.Int).Value = SERVICE_TIMEOUT;

          _obj = _cmd.ExecuteScalar();

          if (_obj == null || _obj is DBNull)
          {
            return String.Empty;
          }

          _principal = (String)_obj;

          return _principal;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return String.Empty;
    } // DB_GetPrincipalService

    /// <summary>
    /// Is principal?
    /// </summary>
    /// <param name="Protocol"></param>
    /// <returns></returns>
    public static Boolean IsPrincipal(String Protocol)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          return Environment.MachineName.Equals(DB_GetPrincipalService(Protocol, _db_trx.SqlTransaction),
                                                StringComparison.InvariantCultureIgnoreCase);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Gets a list of running services
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="Trx"></param>
    /// <param name="RunningServices"></param>
    /// <returns></returns>
    public static Boolean DB_GetRunningServices(String Protocol, SqlTransaction Trx, out List<String> RunningServices)
    {
      StringBuilder _sb;
      String _running_service;

      RunningServices = new List<String>();

      try
      {
        _sb = new StringBuilder();

        //Get list of services
        //Get first machine with ServerName as DB
        _sb.AppendLine("SELECT   SVC_MACHINE, SVC_IP_ADDRESS                                ");
        _sb.AppendLine("  FROM   SERVICES                                                   ");
        _sb.AppendLine(" WHERE   SVC_PROTOCOL = @Protocol                                   ");
        _sb.AppendLine("   AND   SVC_STATUS   = 'RUNNING'                                   ");
        _sb.AppendLine("   AND   SVC_MACHINE  = ServerProperty ('SERVERNAME')               ");
        _sb.AppendLine("   AND   DATEDIFF(SECOND, SVC_LAST_ACCESS, GETDATE ()) <= @Timeout  ");
        _sb.AppendLine();
        _sb.AppendLine("   UNION                                                            ");
        _sb.AppendLine();
        //Get then, rest of machines
        _sb.AppendLine("SELECT   SVC_MACHINE, SVC_IP_ADDRESS                                ");
        _sb.AppendLine("  FROM   SERVICES                                                   ");
        _sb.AppendLine(" WHERE   SVC_PROTOCOL = @Protocol                                   ");
        _sb.AppendLine("   AND   SVC_STATUS   = 'RUNNING'                                   ");
        _sb.AppendLine("   AND   SVC_MACHINE  <> ServerProperty ('SERVERNAME')              ");
        _sb.AppendLine("   AND   DATEDIFF(SECOND, SVC_LAST_ACCESS, GETDATE ()) <= @Timeout  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@Protocol", SqlDbType.NVarChar, 50).Value = Protocol;
          _cmd.Parameters.Add("@Timeout", SqlDbType.Int).Value = SERVICE_TIMEOUT;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            // If there are no services to analyze then we can exit
            if (!_reader.HasRows)
            {
              return false;
            }

            while (_reader.Read())
            {
              _running_service = _reader.GetString(1);    //Get IP

              if (!IsAValidIP(_running_service))          //Is 0.0.0.0?
              {
                _running_service = _reader.GetString(0);  //Get machine name
              }

              // Service is added to the list
              RunningServices.Add(_running_service);
            }

            return true;
          }
        }
      }  // Using SqlCommand
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetRunningServices

    /// <summary>
    /// Is a valid IP?
    /// </summary>
    /// <param name="IP"></param>
    /// <returns></returns>
    private static Boolean IsAValidIP(String IP)
    {
      if (String.IsNullOrEmpty(IP))
      {
        return false;
      }

      if (IP.Equals(EMPTY_IP))
      {
        return false;
      }

      return true;
    }
  }
}