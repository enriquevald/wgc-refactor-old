//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GamingTablesSessions.cs
//  
//   DESCRIPTION: 
// 
//        AUTHOR: Adri�n Cuartas
// 
// CREATION DATE: 23-DEC-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-DEC-2013 ACM    First release.
// 07-FEB-2014 SMN    Allow negative values in amount collected
// 02-DHA-2014 DHA    Fixed Bug #WIG-1327 & #WIG-1383: On open and close session if player tracking is enabled set to disable gaming table pause flag
// 31-OCT-2014 DLL    Fixed Bug #WIG-1613: Error updatin
// 01-MAR-2016 DHA    Product Backlog Item 9756: added chips types and setsg Gaming_table when gaming_table_id = -1
// 04-APR-2016 DHA    Product Backlog Item 9756: added chips (RE, NR and Color)
// 27-APR-2016 DHA    Product Backlog Item 10825: added chips types to cage concepts
// 24-MAY-2016 DHA    Product Backlog Item 9848: drop box gaming tables
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 07-JUL-2016 FOS    Product Backlog Item 14486 -Tables (Phase 1) Chips retro compatibility
// 28-JUN-2016 DHA    Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
// 28-SEP-2016 DHA    PBI 17747: cancel gaming tables operations
// 24-NOV-2016 DHA    Bug 20560: error initial and final chips amount on closing gaming tables sessions
// 25-NOV-2016 DHA    Bug 20560: added tips on closing sesions (cage concepts)
// 09-JAN-2017 DHA    Bug 21496: error on calculating gaming table "Win" for check and bank card
// 01-FEB-2017 RAB    Bug 16922: GamingTables: Error in the calculation of drop raised the cashier (automode)
// 05-ABR-2017 DHA    PBI 26401:MES10 Ticket validation - Ticket status change to cancelled
// 20-APR-2017 RAB    PBI 26809: Drop gaming table information - Actual drop
// 27-JUL-2017 RAB    Bug 28987: WIGOS-3985 Tables - Multicurrency - autocage: it is not allowd to close the gaming table with foreign chips
// 05-SEP-2017 DHA    PBI 28630:WIGOS-3324 MES22 - Gaming table chair assignment with linked gaming tables and card selection
// 21-MAR-2018 AGS    Bug 32007: WIGOS-9153 Error when opening gambling tables - Exception error: invalid column name
// 21-MAR-2018 AGS    Bug 32010: WIGOS-9318 Gambling table cannot be opened again after closing it and timeout log error
// 05-JUN-2018 AGS		Bug 32889:WIGOS-12142, WIGOS-12143, WIGOS-12146, WIGOS-12148 - Drop mesas
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public enum GTS_UPDATE_TYPE
  {
    FillIn = 0,
    FillOut = 1,
    ChipsOwnPurchase = 2,
    ChipsExternalPurchase = 3,
    ChipsOwnSales = 4,
    ChipsExternalSales = 5,
    CollectedAmount = 6,
    Tips = 7,
    ChipsTotalSales = 8,
    ChipsTotalPurchase = 9,
    Visits = 10,
    LastExternalSale = 11,
    LastExternalPurchase = 12,
    CollectedDropBoxAmount = 13,
    CollectedDropBoxChips = 14,
    CollectedDropBoxTickets = 15,
    OpenFillIn = 16,
    CloseFillOut = 17,
    ValidatedCopyDealerAmount = 18,
  }

  public class GamingTablesSessions
  {
    #region Members

    private Int64 m_gaming_table_session_id;
    private Int32 m_gaming_table_id;
    private Int64 m_cashier_session_id;
    private Currency m_fills_chips_amount;
    private Currency m_credits_chips_amount;
    private Currency m_initial_chips_amount;
    private Currency m_final_chips_amount;
    private Currency m_own_purchase_amount;
    private Currency m_external_purchase_amount;
    private Currency m_own_sales_amount;
    private Currency m_external_sales_amount;
    private Currency m_collected_amount;
    private Currency m_collected_dropbox_amount;
    private Currency m_collected_dropbox_chips_amount;
    private Currency m_collected_dropbox_tickets_amount;
    private Currency m_tips;
    private Decimal m_client_visits;
    private Currency m_total_sales_amount;
    private Currency m_total_purchase_amount;
    private Int64? m_last_external_sale_session_id;
    private Int64? m_last_external_purchase_session_id;
    private Boolean m_is_pre_closing;
    private Boolean m_is_first_deposit;
    private Currency m_validated_dealer_copy_amount;

    private SortedDictionary<CurrencyIsoType, GamingTablesSessionsCurrencyType> m_chips_amounts;

    #endregion

    #region Properties

    public Int64 GamingTableSessionId
    {
      get { return m_gaming_table_session_id; }
    }

    public Int32 GamingTableId
    {
      get { return m_gaming_table_id; }
    }

    public Int64 CashierSessionId
    {
      get { return m_cashier_session_id; }
    }

    public Currency FillChipsAmount
    {
      get { return m_fills_chips_amount; }
      set { m_fills_chips_amount = value; }
    }

    public Currency CreditChipsAmount
    {
      get { return m_credits_chips_amount; }
      set { m_credits_chips_amount = value; }
    }

    public Currency InitialChipsAmount
    {
      get { return m_initial_chips_amount; }
      set { m_initial_chips_amount = value; }
    }

    public Currency FinalChipsAmount
    {
      get { return m_final_chips_amount; }
      set { m_final_chips_amount = value; }
    }

    public Currency OwnPurchaseAmount
    {
      get { return m_own_purchase_amount; }
      set { m_own_purchase_amount = value; }
    }

    public Currency ExternalPurchaseAmount
    {
      get { return m_external_purchase_amount; }
      set { m_external_purchase_amount = value; }
    }

    public Currency OwnSalesAmount
    {
      get { return m_own_sales_amount; }
      set { m_own_sales_amount = value; }
    }

    public Currency ExternalSalesAmount
    {
      get { return m_external_sales_amount; }
      set { m_external_sales_amount = value; }
    }

    public Currency CollectedAmount
    {
      get { return m_collected_amount; }
      set { m_collected_amount = value; }
    }

    public Currency CollectedDropBoxAmount
    {
      get { return m_collected_dropbox_amount; }
      set { m_collected_dropbox_amount = value; }
    }

    public Currency CollectedDropBoxChipsAmount
    {
      get { return m_collected_dropbox_chips_amount; }
      set { m_collected_dropbox_chips_amount = value; }
    }

    public Currency CollectedDropBoxTicketsAmount
    {
      get { return m_collected_dropbox_tickets_amount; }
      set { m_collected_dropbox_tickets_amount = value; }
    }

    public Currency Tips
    {
      get { return m_tips; }
      set { m_tips = value; }
    }

    public Decimal ClientVisits
    {
      get { return m_client_visits; }
      set { m_client_visits = value; }
    }

    public Currency TotalSalesAmount
    {
      get { return m_total_sales_amount; }
      set { m_total_sales_amount = value; }
    }

    public Currency TotalPurchaseAmount
    {
      get { return m_total_purchase_amount; }
      set { m_total_purchase_amount = value; }
    }

    public Int64? LastExternalSaleSessionId
    {
      get { return m_last_external_sale_session_id; }
      set { m_last_external_sale_session_id = value; }
    }

    public Int64? LastExternalPurchaseSessionId
    {
      get { return m_last_external_purchase_session_id; }
      set { m_last_external_purchase_session_id = value; }
    }

    public SortedDictionary<CurrencyIsoType, GamingTablesSessionsCurrencyType> Amounts
    {
      get { return m_chips_amounts; }
      set { m_chips_amounts = value; }
    }

    public Boolean IsPreClosing
    {
      get { return m_is_pre_closing; }
      set { m_is_pre_closing = value; }
    }

    public Boolean IsFirstDeposit
    {
      get { return m_is_first_deposit; }
      set { m_is_first_deposit = value; }
    }

    public Currency ValidatedDealerCopyAmount
    {
      get { return m_validated_dealer_copy_amount; }
      set { m_validated_dealer_copy_amount = value; }
    }


    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Class Constructor
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public GamingTablesSessions(Int32 GamingTableId, Int64 CashierSessionId)
    {
      m_gaming_table_session_id = 0;
      m_gaming_table_id = GamingTableId;
      m_cashier_session_id = CashierSessionId;
      m_fills_chips_amount = 0;
      m_credits_chips_amount = 0;
      m_own_sales_amount = 0;
      m_external_sales_amount = 0;
      m_own_purchase_amount = 0;
      m_collected_amount = 0;
      m_tips = 0;
      m_client_visits = 0;
      m_is_pre_closing = false;
      m_initial_chips_amount = 0;
      m_final_chips_amount = 0;
      m_is_first_deposit = false;

      m_chips_amounts = new SortedDictionary<CurrencyIsoType, GamingTablesSessionsCurrencyType>(new CurrencyIsoType.SortComparer());
    } // GamingTablesSessions

    //------------------------------------------------------------------------------
    // PURPOSE : Look for existing GameTable and Get/Open a session
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean GetSession(out GamingTablesSessions GT_Session, Int64 GamingTableSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _game_table_id;
      Int64 _cashier_session_id;

      GT_Session = null;
      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   GTS_GAMING_TABLE_ID    ");
      _sb.AppendLine("       , GTS_CASHIER_SESSION_ID ");
      _sb.AppendLine("  FROM   GAMING_TABLES_SESSIONS ");
      _sb.AppendLine(" WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = GamingTableSessionId;

        // Check for existing session
        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          if (!_reader.Read())
          {
            return false;
          }
          _game_table_id = _reader.GetInt32(0);
          _cashier_session_id = _reader.GetInt64(1);
        }
      }

      return GetOrOpenSession(out GT_Session, _game_table_id, _cashier_session_id, Trx);
    }

    /// <summary>
    /// Reset gaming table drop box collection values from gaming table sessions
    /// </summary>
    /// <param name="CashierSessionId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean ResetGamblingTableSessionsDropbox(Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("DECLARE  @GamingTableSessionId BIGINT ");

        _sb.AppendLine(" UPDATE   GAMING_TABLES_SESSIONS ");
        _sb.AppendLine("    SET   GTS_COLLECTED_DROPBOX_AMOUNT = 0 ");
        _sb.AppendLine("        , GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT = 0 ");
        _sb.AppendLine("        , GTS_COLLECTED_DROPBOX_TICKETS_AMOUNT = 0 ");
        _sb.AppendLine("        , @GamingTableSessionId = GTS_GAMING_TABLE_SESSION_ID ");
        _sb.AppendLine("  WHERE   GTS_CASHIER_SESSION_ID = @CashierSessionId ");

        _sb.AppendLine(" UPDATE   GAMING_TABLES_SESSIONS_BY_CURRENCY ");
        _sb.AppendLine("    SET   GTSC_COLLECTED_DROPBOX_AMOUNT = 0 ");
        _sb.AppendLine("        , GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED = 0 ");
        _sb.AppendLine("  WHERE   GTSC_GAMING_TABLE_SESSION_ID = @GamingTableSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@CashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;

          if (_cmd.ExecuteNonQuery() <= 0)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Look for existing GameTable and Get/Open a session
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean GetOrOpenSession(out Boolean IsTheFirstDeposit, Int32 GamingTableId, Int64 CashierSessionId, SqlTransaction Trx)
    {
      GamingTablesSessions _gt_session;
      Boolean _return;

      IsTheFirstDeposit = false;

      _return = GetOrOpenSession(out _gt_session, GamingTableId, CashierSessionId, Trx);
      IsTheFirstDeposit = _gt_session.IsFirstDeposit;

      return _return;
    }

    public static Boolean GetOrOpenSession(out GamingTablesSessions GT_Session, Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      GT_Session = null;
      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   GT_GAMING_TABLE_ID   ");
      _sb.AppendLine("  FROM   GAMING_TABLES        ");
      _sb.AppendLine(" WHERE   GT_CASHIER_ID = (SELECT   CS_CASHIER_ID                      ");
      _sb.AppendLine("                            FROM   CASHIER_SESSIONS                   ");
      _sb.AppendLine("                           WHERE   CS_SESSION_ID = @pCashierSessionId)");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;

        _obj = _cmd.ExecuteScalar();
        // Check for existing Game Table
        if (_obj == null)
        {
          return false;
        }
      }

      return GetOrOpenSession(out GT_Session, (Int32)_obj, CashierSessionId, Trx);
    } // GetOrOpenSession()

    /// <summary>
    /// Get total BuyIn accumulated of gaming table session ID
    /// </summary>
    /// <param name="GamingTableSessionID">ID from gaming table session</param>
    /// <param name="Trx">Transaction</param>
    /// <returns></returns>
    public static Currency GetBuyInAccumulatedInGamingSessionID(Int64 GamingTableSessionID, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Dictionary<String, Currency> _total_sell_chips_by_currency;
      Currency _total_sell_chips;

      _sb = new StringBuilder();
      _total_sell_chips_by_currency = new Dictionary<String, Currency>();
      _total_sell_chips = 0;

      try
      {
        _sb.AppendLine(" SELECT GTPS_ISO_CODE                                        "); // 0
        _sb.AppendLine("      , SUM(GTPS_TOTAL_SELL_CHIPS)                           "); // 1
        _sb.AppendLine(" FROM GT_PLAY_SESSIONS                                       ");
        _sb.AppendLine(" WHERE GTPS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionID ");
        _sb.AppendLine(" GROUP BY GTPS_ISO_CODE                                      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableSessionID", SqlDbType.Int).Value = GamingTableSessionID;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _total_sell_chips_by_currency.Add(_reader.GetString(0), (Currency)_reader.GetSqlMoney(1));
            }
          }
        }

        foreach (KeyValuePair<String, Currency> _sell_chip in _total_sell_chips_by_currency)
        {
          if (_sell_chip.Key != CurrencyExchange.GetNationalCurrency())
          {
            _total_sell_chips += CurrencyExchange.GetExchange(_sell_chip.Value, _sell_chip.Key, CurrencyExchange.GetNationalCurrency(), Trx);
          }
          else
          {
            _total_sell_chips += _sell_chip.Value;
          }
        }

        return _total_sell_chips;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return 0;
    } //GetBuyInAccumulatedInGamingSessionID

    //------------------------------------------------------------------------------
    // PURPOSE : Open a Gaming Tables Session
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean GetOrOpenSession(out GamingTablesSessions GT_Session, Int32 GamingTableId, Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Boolean _session_exists;
      SqlParameter _sql_gt_session_id;

      _sb = new StringBuilder();
      _session_exists = false;
      GT_Session = new GamingTablesSessions(GamingTableId, CashierSessionId);

      try
      {
        // Check for existing GamingTablesSession
        _sb.AppendLine("SELECT   GTS_GAMING_TABLE_SESSION_ID  ");
        _sb.AppendLine("       , GTS_FILLS_CHIPS_AMOUNT  ");
        _sb.AppendLine("       , GTS_CREDITS_CHIPS_AMOUNT ");
        _sb.AppendLine("       , GTS_OWN_SALES_AMOUNT ");
        _sb.AppendLine("       , GTS_EXTERNAL_SALES_AMOUNT ");
        _sb.AppendLine("       , GTS_EXTERNAL_PURCHASE_AMOUNT ");
        _sb.AppendLine("       , GTS_OWN_PURCHASE_AMOUNT ");
        _sb.AppendLine("       , GTS_COLLECTED_AMOUNT ");
        _sb.AppendLine("       , GTS_TIPS ");
        _sb.AppendLine("       , GTS_CLIENT_VISITS ");
        _sb.AppendLine("       , GTS_TOTAL_SALES_AMOUNT ");
        _sb.AppendLine("       , GTS_TOTAL_PURCHASE_AMOUNT ");
        _sb.AppendLine("       , GTS_LAST_EXTERNAL_SALE_SESSION_ID ");
        _sb.AppendLine("       , GTS_LAST_EXTERNAL_PURCHASE_SESSION_ID ");
        _sb.AppendLine("       , GTS_PRE_CLOSING ");
        _sb.AppendLine("       , GTS_INITIAL_CHIPS_AMOUNT  ");
        _sb.AppendLine("       , GTS_FINAL_CHIPS_AMOUNT ");
        _sb.AppendLine("  FROM   GAMING_TABLES_SESSIONS ");
        _sb.AppendLine(" WHERE   GTS_GAMING_TABLE_ID    = @pGamingTableId");
        _sb.AppendLine("   AND   GTS_CASHIER_SESSION_ID = @pCashierSessionId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GT_Session.GamingTableId;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = GT_Session.CashierSessionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            // If Exists, Load values 
            if (_reader.Read())
            {
              GT_Session.m_gaming_table_session_id = _reader.GetInt64(0);
              GT_Session.m_fills_chips_amount = _reader.GetSqlMoney(1);
              GT_Session.m_credits_chips_amount = _reader.GetSqlMoney(2);
              GT_Session.m_own_sales_amount = _reader.GetSqlMoney(3);
              GT_Session.m_external_sales_amount = _reader.GetSqlMoney(4);
              GT_Session.m_external_purchase_amount = _reader.GetSqlMoney(5);
              GT_Session.m_own_purchase_amount = _reader.GetSqlMoney(6);
              GT_Session.m_collected_amount = _reader.GetSqlMoney(7);
              GT_Session.m_tips = _reader.GetSqlMoney(8);
              GT_Session.m_client_visits = _reader.GetDecimal(9);
              GT_Session.m_total_sales_amount = _reader.GetDecimal(10);
              GT_Session.m_total_purchase_amount = _reader.GetDecimal(11);
              GT_Session.m_last_external_sale_session_id = !_reader.IsDBNull(12) ? (Int64?)_reader[12] : null;
              GT_Session.m_last_external_purchase_session_id = !_reader.IsDBNull(13) ? (Int64?)_reader[13] : null;
              GT_Session.m_is_pre_closing = _reader.GetBoolean(14);
              GT_Session.m_initial_chips_amount = _reader.GetSqlMoney(15);
              GT_Session.m_final_chips_amount = _reader.GetSqlMoney(16);

              _session_exists = true;
            }
          }
        }

        // Insert a new GamingTablesSession
        if (!_session_exists)
        {
          _sb.Length = 0;
          _sb.AppendLine("INSERT INTO   GAMING_TABLES_SESSIONS ");
          _sb.AppendLine("            ( GTS_GAMING_TABLE_ID ");
          _sb.AppendLine("            , GTS_CASHIER_SESSION_ID ");
          _sb.AppendLine("            , GTS_FILLS_CHIPS_AMOUNT ");
          _sb.AppendLine("            , GTS_CREDITS_CHIPS_AMOUNT ");
          _sb.AppendLine("            , GTS_OWN_SALES_AMOUNT ");
          _sb.AppendLine("            , GTS_EXTERNAL_SALES_AMOUNT ");
          _sb.AppendLine("            , GTS_EXTERNAL_PURCHASE_AMOUNT ");
          _sb.AppendLine("            , GTS_OWN_PURCHASE_AMOUNT ");
          _sb.AppendLine("            , GTS_COLLECTED_AMOUNT ");
          _sb.AppendLine("            , GTS_TIPS ");
          _sb.AppendLine("            , GTS_CLIENT_VISITS ");
          _sb.AppendLine("            , GTS_TOTAL_SALES_AMOUNT ");
          _sb.AppendLine("            , GTS_TOTAL_PURCHASE_AMOUNT  ");
          _sb.AppendLine("            , GTS_INITIAL_CHIPS_AMOUNT ");
          _sb.AppendLine("            , GTS_FINAL_CHIPS_AMOUNT ) ");
          _sb.AppendLine("     VALUES ( @pGamingTableId ");
          _sb.AppendLine("            , @pCashierSessionId ");
          _sb.AppendLine("            , @pFillsChipsAmount ");
          _sb.AppendLine("            , @pCreditsChipsAmount ");
          _sb.AppendLine("            , @pOwnSalesAmount ");
          _sb.AppendLine("            , @pExternalSalesAmount ");
          _sb.AppendLine("            , @pExternalPurchaseAmount ");
          _sb.AppendLine("            , @pOwnPurchaseAmount ");
          _sb.AppendLine("            , @pCollectedAmount ");
          _sb.AppendLine("            , @pTips ");
          _sb.AppendLine("            , @pClientVisits ");
          _sb.AppendLine("            , @pChipsTotalSales ");
          _sb.AppendLine("            , @pChipsTotalPurchase ");
          _sb.AppendLine("            , @pInitialChipsAmount ");
          _sb.AppendLine("            , @pFinalChipsAmount )");
          _sb.AppendLine(" SET @pGamingTableSessionId = SCOPE_IDENTITY() ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pGamingTableId", SqlDbType.BigInt).Value = GT_Session.GamingTableId;
            _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = GT_Session.CashierSessionId;
            _cmd.Parameters.Add("@pFillsChipsAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pCreditsChipsAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pOwnSalesAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pExternalSalesAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pExternalPurchaseAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pOwnPurchaseAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pCollectedAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pTips", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pClientVisits", SqlDbType.Decimal).Value = 0;
            _cmd.Parameters.Add("@pChipsTotalSales", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pChipsTotalPurchase", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pInitialChipsAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pFinalChipsAmount", SqlDbType.Money).Value = 0;

            _sql_gt_session_id = _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt);
            _sql_gt_session_id.Direction = ParameterDirection.Output;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
            else
            {
              GT_Session.m_is_first_deposit = true;
            }
          }

          // DHA 02-OCT-2014 set gaming table pause to false
          if (GamingTableId > 0 && GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
          {
            _sb = new StringBuilder();

            _sb.AppendLine(" UPDATE   GAMING_TABLES                               ");
            _sb.AppendLine("    SET   GT_PLAYER_TRACKING_PAUSE = @pIsPaused       ");
            _sb.AppendLine("  WHERE   GT_GAMING_TABLE_ID =       @pGamingTableID  ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
            {
              _cmd.Parameters.Add("@pGamingTableID", SqlDbType.Int).Value = GamingTableId;
              _cmd.Parameters.Add("@pIsPaused", SqlDbType.Bit).Value = false;

              if (_cmd.ExecuteNonQuery() != 1)
              {
                return false;
              }
            }
          }

          GT_Session.m_gaming_table_session_id = (Int64)_sql_gt_session_id.Value;
        }
        else
        {
          // Get gaming sessions by currency
          _sb.Length = 0;
          _sb.AppendLine("SELECT   GTSC_ISO_CODE  "); // 0
          _sb.AppendLine("       , GTSC_TYPE "); // 1
          _sb.AppendLine("       , GTSC_FILLS_CHIPS_AMOUNT "); // 2
          _sb.AppendLine("       , GTSC_CREDITS_CHIPS_AMOUNT "); // 3
          _sb.AppendLine("       , GTSC_OWN_PURCHASE_AMOUNT "); // 4
          _sb.AppendLine("       , GTSC_TOTAL_PURCHASE_AMOUNT "); // 5
          _sb.AppendLine("       , GTSC_EXTERNAL_PURCHASE_AMOUNT "); // 6
          _sb.AppendLine("       , GTSC_EXTERNAL_SALES_AMOUNT "); // 7
          _sb.AppendLine("       , GTSC_OWN_SALES_AMOUNT "); // 8
          _sb.AppendLine("       , GTSC_TOTAL_SALES_AMOUNT "); // 9
          _sb.AppendLine("       , GTSC_COLLECTED_AMOUNT "); // 10
          _sb.AppendLine("       , GTSC_INITIAL_CHIPS_AMOUNT "); // 11
          _sb.AppendLine("       , GTSC_FINAL_CHIPS_AMOUNT "); // 12
          _sb.AppendLine("  FROM   GAMING_TABLES_SESSIONS_BY_CURRENCY ");
          _sb.AppendLine(" WHERE   GTSC_GAMING_TABLE_SESSION_ID    = @pGamingTableSessionId");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = GT_Session.GamingTableSessionId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              // If exists, Load values 
              while (_reader.Read())
              {
                CurrencyIsoType _key;
                GamingTablesSessionsCurrencyType _value;

                _key = new CurrencyIsoType();
                _value = new GamingTablesSessionsCurrencyType();

                _key.IsoCode = _reader.GetString(0);
                _key.Type = (CurrencyExchangeType)_reader.GetInt32(1);

                _value.FillsChipsAmount = _reader.GetSqlMoney(2);
                _value.CreditsChipsAmount = _reader.GetSqlMoney(3);
                _value.OwnPurchaseAmount = _reader.GetSqlMoney(4);
                _value.TotalPurchaseAmount = _reader.GetSqlMoney(5);
                _value.ExternalPurchaseAmount = _reader.GetSqlMoney(6);
                _value.ExternalSalesAmount = _reader.GetSqlMoney(7);
                _value.OwnSalesAmount = _reader.GetSqlMoney(8);
                _value.TotalSalesAmount = _reader.GetSqlMoney(9);
                _value.CollectedAmount = _reader.GetSqlMoney(10);
                _value.InitialChipsAmount = _reader.GetSqlMoney(11);
                _value.FinalChipsAmount = _reader.GetSqlMoney(12);

              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetOrOpenSession()

    //------------------------------------------------------------------------------
    // PURPOSE : Get opened Gaming Tables Session
    //
    //  PARAMS :
    //      - INPUT : 
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean GetOpenedSession(Int32 GamingTableId, out GamingTablesSessions GT_Session, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      GT_Session = new GamingTablesSessions(GamingTableId, 0);

      try
      {
        // Check for existing GamingTablesSession
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   TOP 1 ");
        _sb.AppendLine("         GTS_GAMING_TABLE_SESSION_ID  ");
        _sb.AppendLine("       , GTS_FILLS_CHIPS_AMOUNT  ");
        _sb.AppendLine("       , GTS_CREDITS_CHIPS_AMOUNT ");
        _sb.AppendLine("       , GTS_OWN_SALES_AMOUNT ");
        _sb.AppendLine("       , GTS_EXTERNAL_SALES_AMOUNT ");
        _sb.AppendLine("       , GTS_EXTERNAL_PURCHASE_AMOUNT ");
        _sb.AppendLine("       , GTS_OWN_PURCHASE_AMOUNT ");
        _sb.AppendLine("       , GTS_COLLECTED_AMOUNT ");
        _sb.AppendLine("       , GTS_TIPS ");
        _sb.AppendLine("       , GTS_CLIENT_VISITS ");
        _sb.AppendLine("       , GTS_TOTAL_SALES_AMOUNT ");
        _sb.AppendLine("       , GTS_TOTAL_PURCHASE_AMOUNT ");
        _sb.AppendLine("       , GTS_LAST_EXTERNAL_SALE_SESSION_ID ");
        _sb.AppendLine("       , GTS_LAST_EXTERNAL_PURCHASE_SESSION_ID ");
        _sb.AppendLine("       , CS_SESSION_ID ");
        _sb.AppendLine("       , GTS_PRE_CLOSING ");
        _sb.AppendLine("       , GTS_INITIAL_CHIPS_AMOUNT  ");
        _sb.AppendLine("       , GTS_FINAL_CHIPS_AMOUNT ");
        _sb.AppendLine("  FROM   GAMING_TABLES_SESSIONS ");
        _sb.AppendLine(" INNER   JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID ");
        _sb.AppendLine(" WHERE   GTS_GAMING_TABLE_ID    = @pGamingTableId");
        _sb.AppendLine("   AND   CS_STATUS = @pStatus");
        _sb.AppendLine("ORDER BY GTS_GAMING_TABLE_SESSION_ID DESC ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GT_Session.GamingTableId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            // If Exists, Load values 
            if (_reader.Read())
            {
              GT_Session.m_gaming_table_session_id = _reader.GetInt64(0);
              GT_Session.m_fills_chips_amount = _reader.GetSqlMoney(1);
              GT_Session.m_credits_chips_amount = _reader.GetSqlMoney(2);
              GT_Session.m_own_sales_amount = _reader.GetSqlMoney(3);
              GT_Session.m_external_sales_amount = _reader.GetSqlMoney(4);
              GT_Session.m_external_purchase_amount = _reader.GetSqlMoney(5);
              GT_Session.m_own_purchase_amount = _reader.GetSqlMoney(6);
              GT_Session.m_collected_amount = _reader.GetSqlMoney(7);
              GT_Session.m_tips = _reader.GetSqlMoney(8);
              GT_Session.m_client_visits = _reader.GetDecimal(9);
              GT_Session.m_total_sales_amount = _reader.GetDecimal(10);
              GT_Session.m_total_purchase_amount = _reader.GetDecimal(11);
              GT_Session.m_last_external_sale_session_id = !_reader.IsDBNull(12) ? (Int64?)_reader[12] : null;
              GT_Session.m_last_external_purchase_session_id = !_reader.IsDBNull(13) ? (Int64?)_reader[13] : null;
              GT_Session.m_cashier_session_id = _reader.GetInt64(14);
              GT_Session.m_is_pre_closing = _reader.GetBoolean(15);
              GT_Session.m_initial_chips_amount = _reader.GetSqlMoney(16);
              GT_Session.m_final_chips_amount = _reader.GetSqlMoney(17);
              GT_Session.IsFirstDeposit = false;
            }
            else
            {
              GT_Session = null;
            }
          }
        }

        if (GT_Session != null)
        {
          //Get buy-in accumulated in gaming table session id
          Currency _validated_dealer_copy_amount;
          _validated_dealer_copy_amount = GetBuyInAccumulatedInGamingSessionID(GT_Session.GamingTableSessionId, Trx);
          GT_Session.ValidatedDealerCopyAmount = _validated_dealer_copy_amount;

          // Get gaming sessions by currency
          _sb = new StringBuilder();

          _sb.AppendLine("SELECT   GTSC_ISO_CODE  "); // 0
          _sb.AppendLine("       , GTSC_TYPE "); // 1
          _sb.AppendLine("       , GTSC_FILLS_CHIPS_AMOUNT "); // 2
          _sb.AppendLine("       , GTSC_CREDITS_CHIPS_AMOUNT "); // 3
          _sb.AppendLine("       , GTSC_OWN_PURCHASE_AMOUNT "); // 4
          _sb.AppendLine("       , GTSC_TOTAL_PURCHASE_AMOUNT "); // 5
          _sb.AppendLine("       , GTSC_EXTERNAL_PURCHASE_AMOUNT "); // 6
          _sb.AppendLine("       , GTSC_EXTERNAL_SALES_AMOUNT "); // 7
          _sb.AppendLine("       , GTSC_OWN_SALES_AMOUNT "); // 8
          _sb.AppendLine("       , GTSC_TOTAL_SALES_AMOUNT "); // 9
          _sb.AppendLine("       , GTSC_COLLECTED_AMOUNT "); // 10
          _sb.AppendLine("       , GTSC_INITIAL_CHIPS_AMOUNT "); // 2
          _sb.AppendLine("       , GTSC_FINAL_CHIPS_AMOUNT "); // 3
          _sb.AppendLine("  FROM   GAMING_TABLES_SESSIONS_BY_CURRENCY ");
          _sb.AppendLine(" WHERE   GTSC_GAMING_TABLE_SESSION_ID    = @pGamingTableSessionId");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = GT_Session.GamingTableSessionId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              // If exists, Load values 
              while (_reader.Read())
              {
                CurrencyIsoType _key;
                GamingTablesSessionsCurrencyType _value;

                _key = new CurrencyIsoType();
                _value = new GamingTablesSessionsCurrencyType();

                _key.IsoCode = _reader.GetString(0);
                _key.Type = (CurrencyExchangeType)_reader.GetInt32(1);

                _value.FillsChipsAmount = _reader.GetSqlMoney(2);
                _value.CreditsChipsAmount = _reader.GetSqlMoney(3);
                _value.OwnPurchaseAmount = _reader.GetSqlMoney(4);
                _value.TotalPurchaseAmount = _reader.GetSqlMoney(5);
                _value.ExternalPurchaseAmount = _reader.GetSqlMoney(6);
                _value.ExternalSalesAmount = _reader.GetSqlMoney(7);
                _value.OwnSalesAmount = _reader.GetSqlMoney(8);
                _value.TotalSalesAmount = _reader.GetSqlMoney(9);
                _value.CollectedAmount = _reader.GetSqlMoney(10);
                _value.InitialChipsAmount = _reader.GetSqlMoney(11);
                _value.FinalChipsAmount = _reader.GetSqlMoney(12);

                GT_Session.Amounts.Add(_key, _value);
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetOrOpenSession()

    //------------------------------------------------------------------------------
    // PURPOSE : Update specific Gaming Tables Session fields
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public Boolean UpdateSession(GTS_UPDATE_TYPE UpdateType, SortedDictionary<CurrencyIsoType, Decimal> Amounts, SqlTransaction Trx)
    {
      foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in Amounts)
      {
        if (!UpdateSession(UpdateType, _amount.Value, _amount.Key, Trx))
        {
          return false;
        }
      }

      return true;

    }

    public Boolean UpdateSession(GTS_UPDATE_TYPE UpdateType, Currency Amount, CurrencyIsoType CurrencyIsoType, SqlTransaction Trx)
    {

      StringBuilder _sb;
      StringBuilder _sb_insert_params;
      Currency _final_amount;
      Currency _final_amount_total;
      Decimal _visits;
      CurrencyExchange _currency_exchange;
      CurrencyExchangeResult _exchange_result;
      Currency _national_currency_amount;
      Decimal _converted_amount;

      _final_amount = 0;
      _final_amount_total = 0;
      _visits = 0;
      _currency_exchange = null;
      _national_currency_amount = Amount;
      _converted_amount = 0;

      _sb = new StringBuilder();
      _sb_insert_params = new StringBuilder();

      // Avoid to update when type is a currency
      if (CurrencyIsoType.Type == CurrencyExchangeType.CURRENCY)
      {
        if (UpdateType == GTS_UPDATE_TYPE.ChipsExternalPurchase || UpdateType == GTS_UPDATE_TYPE.ChipsExternalSales || UpdateType == GTS_UPDATE_TYPE.ChipsOwnPurchase ||
          UpdateType == GTS_UPDATE_TYPE.ChipsOwnSales || UpdateType == GTS_UPDATE_TYPE.ChipsTotalPurchase || UpdateType == GTS_UPDATE_TYPE.ChipsTotalSales ||
          UpdateType == GTS_UPDATE_TYPE.FillIn || UpdateType == GTS_UPDATE_TYPE.FillOut || UpdateType == GTS_UPDATE_TYPE.OpenFillIn || UpdateType == GTS_UPDATE_TYPE.CloseFillOut
          || UpdateType == GTS_UPDATE_TYPE.ValidatedCopyDealerAmount)
        {
          return true;
        }
      }

      // Avoid to update when type is a chip
      if (FeatureChips.IsChipsType(CurrencyIsoType.Type) && UpdateType == GTS_UPDATE_TYPE.CollectedAmount)
      {
        return true;
      }

      if (CurrencyIsoType.Type != CurrencyExchangeType.CASINO_CHIP_COLOR && UpdateType != GTS_UPDATE_TYPE.CollectedAmount &&
        UpdateType != GTS_UPDATE_TYPE.CollectedDropBoxAmount && UpdateType != GTS_UPDATE_TYPE.CollectedDropBoxChips)
      {
        // Convert to national currency
        if (CurrencyIsoType.IsoCode != CurrencyExchange.GetNationalCurrency())
        {
          _national_currency_amount = CurrencyExchange.GetExchange(_national_currency_amount, CurrencyIsoType.IsoCode, CurrencyExchange.GetNationalCurrency(), Trx);
        }

        if (!UpdateSession(UpdateType, _national_currency_amount, Trx))
        {
          return false;
        }
      }

      try
      {
        _sb.AppendLine(" IF EXISTS ( SELECT   1  ");
        _sb.AppendLine("               FROM   GAMING_TABLES_SESSIONS_BY_CURRENCY ");
        _sb.AppendLine("              WHERE   GTSC_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId ");
        _sb.AppendLine("                AND   GTSC_ISO_CODE   = @pIsoCode ");
        _sb.AppendLine("                AND   GTSC_TYPE       = @pType) ");

        _sb.AppendLine(" UPDATE   GAMING_TABLES_SESSIONS_BY_CURRENCY  ");

        // Add Amount
        switch (UpdateType)
        {
          case GTS_UPDATE_TYPE.FillIn:
            _sb.AppendLine("   SET   GTSC_FILLS_CHIPS_AMOUNT = GTSC_FILLS_CHIPS_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_FILLS_CHIPS_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_FILLS_CHIPS_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.FillOut:
            _sb.AppendLine("   SET   GTSC_CREDITS_CHIPS_AMOUNT = GTSC_CREDITS_CHIPS_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_CREDITS_CHIPS_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_CREDITS_CHIPS_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.OpenFillIn:
            _sb.AppendLine("   SET   GTSC_INITIAL_CHIPS_AMOUNT = GTSC_INITIAL_CHIPS_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_INITIAL_CHIPS_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_INITIAL_CHIPS_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.CloseFillOut:
            _sb.AppendLine("   SET   GTSC_FINAL_CHIPS_AMOUNT = GTSC_FINAL_CHIPS_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_FINAL_CHIPS_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_FINAL_CHIPS_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.ChipsOwnPurchase:
            _sb.AppendLine("   SET   GTSC_OWN_PURCHASE_AMOUNT = GTSC_OWN_PURCHASE_AMOUNT + @pAmount ");
            _sb.AppendLine("       , GTSC_TOTAL_PURCHASE_AMOUNT = GTSC_TOTAL_PURCHASE_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_OWN_PURCHASE_AMOUNT ");
            _sb.AppendLine("       , INSERTED.GTSC_TOTAL_PURCHASE_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_OWN_PURCHASE_AMOUNT ");
            _sb_insert_params.AppendLine("                 , INSERTED.GTSC_TOTAL_PURCHASE_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.ChipsExternalPurchase:
            _sb.AppendLine("   SET   GTSC_EXTERNAL_PURCHASE_AMOUNT = GTSC_EXTERNAL_PURCHASE_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_EXTERNAL_PURCHASE_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_EXTERNAL_PURCHASE_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.ChipsExternalSales:
            _sb.AppendLine("   SET   GTSC_EXTERNAL_SALES_AMOUNT = GTSC_EXTERNAL_SALES_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_EXTERNAL_SALES_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_EXTERNAL_SALES_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.ChipsOwnSales:
            if (GamingTableBusinessLogic.IsEnableManualConciliation() && GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
            {
              _sb.AppendLine("   SET   GTSC_OWN_SALES_AMOUNT = GTSC_OWN_SALES_AMOUNT ");
              _sb.AppendLine("       , GTSC_TOTAL_SALES_AMOUNT = GTSC_TOTAL_SALES_AMOUNT ");
              _sb.AppendLine("OUTPUT   INSERTED.GTSC_OWN_SALES_AMOUNT ");
              _sb.AppendLine("       , INSERTED.GTSC_TOTAL_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_OWN_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("                 , INSERTED.GTSC_TOTAL_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("          VALUES    ");
              _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
              _sb_insert_params.AppendLine("                 , @pIsoCode ");
              _sb_insert_params.AppendLine("                 , @pType ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 ) ");
              break;
            }
            else
            {
              _sb.AppendLine("   SET   GTSC_OWN_SALES_AMOUNT = GTSC_OWN_SALES_AMOUNT + @pAmount ");
              _sb.AppendLine("       , GTSC_TOTAL_SALES_AMOUNT = GTSC_TOTAL_SALES_AMOUNT + @pAmount ");
              _sb.AppendLine("OUTPUT   INSERTED.GTSC_OWN_SALES_AMOUNT ");
              _sb.AppendLine("       , INSERTED.GTSC_TOTAL_SALES_AMOUNT ");

              _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_OWN_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("                 , INSERTED.GTSC_TOTAL_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("          VALUES    ");
              _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
              _sb_insert_params.AppendLine("                 , @pIsoCode ");
              _sb_insert_params.AppendLine("                 , @pType ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , @pAmount ");
              _sb_insert_params.AppendLine("                 , @pAmount ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 ) ");
              break;
            }

          case GTS_UPDATE_TYPE.CollectedAmount:
            _sb.AppendLine("   SET   GTSC_COLLECTED_AMOUNT = @pAmount ");
            _sb.AppendLine("       , GTSC_COLLECTED_AMOUNT_CONVERTED = @pAmountConverted ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_COLLECTED_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_COLLECTED_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , @pAmountConverted ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.CollectedDropBoxAmount:
          case GTS_UPDATE_TYPE.CollectedDropBoxChips:
            _sb.AppendLine("   SET   GTSC_COLLECTED_DROPBOX_AMOUNT = @pAmount ");
            _sb.AppendLine("       , GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED = @pAmountConverted ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_COLLECTED_DROPBOX_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_COLLECTED_DROPBOX_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , @pAmountConverted ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.Tips:
            _sb.AppendLine("   SET   GTSC_TIPS = GTSC_TIPS + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_TIPS ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_TIPS ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");

            /// TIPS
            break;

          case GTS_UPDATE_TYPE.ChipsTotalSales:
            if (GamingTableBusinessLogic.IsEnableManualConciliation() && GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
            {
              _sb.AppendLine("   SET   GTSC_TOTAL_SALES_AMOUNT = GTSC_TOTAL_SALES_AMOUNT ");
              _sb.AppendLine("OUTPUT   INSERTED.GTSC_TOTAL_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_TOTAL_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("          VALUES    ");
              _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
              _sb_insert_params.AppendLine("                 , @pIsoCode ");
              _sb_insert_params.AppendLine("                 , @pType ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 ) ");
            }
            else
            {
              _sb.AppendLine("   SET   GTSC_TOTAL_SALES_AMOUNT = GTSC_TOTAL_SALES_AMOUNT + @pAmount ");
              _sb.AppendLine("OUTPUT   INSERTED.GTSC_TOTAL_SALES_AMOUNT ");

              _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_TOTAL_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("          VALUES    ");
              _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
              _sb_insert_params.AppendLine("                 , @pIsoCode ");
              _sb_insert_params.AppendLine("                 , @pType ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , @pAmount ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 ) ");
            }

            break;

          case GTS_UPDATE_TYPE.ChipsTotalPurchase:
            _sb.AppendLine("   SET   GTSC_TOTAL_PURCHASE_AMOUNT = GTSC_TOTAL_PURCHASE_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTSC_TOTAL_PURCHASE_AMOUNT ");

            _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_TOTAL_PURCHASE_AMOUNT ");
            _sb_insert_params.AppendLine("          VALUES    ");
            _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
            _sb_insert_params.AppendLine("                 , @pIsoCode ");
            _sb_insert_params.AppendLine("                 , @pType ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , @pAmount ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 , 0 ");
            _sb_insert_params.AppendLine("                 ) ");
            break;

          case GTS_UPDATE_TYPE.ValidatedCopyDealerAmount:
            if (GamingTableBusinessLogic.IsEnableManualConciliation() && GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
            {
              _sb.AppendLine("   SET   GTSC_COPY_DEALER_VALIDATED_AMOUNT = GTSC_COPY_DEALER_VALIDATED_AMOUNT + @pAmount ");
              _sb.AppendLine("       , GTSC_EXTERNAL_SALES_AMOUNT = GTSC_EXTERNAL_SALES_AMOUNT + @pAmount ");
              _sb.AppendLine("OUTPUT   INSERTED.GTSC_COPY_DEALER_VALIDATED_AMOUNT ");
              _sb.AppendLine("       , INSERTED.GTSC_EXTERNAL_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_COPY_DEALER_VALIDATED_AMOUNT ");
              _sb_insert_params.AppendLine("                 , INSERTED.GTSC_EXTERNAL_SALES_AMOUNT ");
              _sb_insert_params.AppendLine("          VALUES    ");
              _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
              _sb_insert_params.AppendLine("                 , @pIsoCode ");
              _sb_insert_params.AppendLine("                 , @pType ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , @pAmount ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , @pAmount ");
              _sb_insert_params.AppendLine("                 ) ");
            }
            else
            {
              _sb.AppendLine("   SET   GTSC_COPY_DEALER_VALIDATED_AMOUNT = GTSC_COPY_DEALER_VALIDATED_AMOUNT + @pAmount ");
              _sb.AppendLine("OUTPUT   INSERTED.GTSC_COPY_DEALER_VALIDATED_AMOUNT ");

              _sb_insert_params.AppendLine("          OUTPUT   INSERTED.GTSC_COPY_DEALER_VALIDATED_AMOUNT ");
              _sb_insert_params.AppendLine("          VALUES    ");
              _sb_insert_params.AppendLine("                 ( @pGamingTableSessionId ");
              _sb_insert_params.AppendLine("                 , @pIsoCode ");
              _sb_insert_params.AppendLine("                 , @pType ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , 0 ");
              _sb_insert_params.AppendLine("                 , @pAmount ");
              _sb_insert_params.AppendLine("                 ) ");
            }
            break;

          default:

            return false;
        }

        _sb.AppendLine("   WHERE   GTSC_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId ");
        _sb.AppendLine("     AND   GTSC_ISO_CODE = @pIsoCode  ");
        _sb.AppendLine("     AND   GTSC_TYPE = @pType  ");
        _sb.AppendLine("  ELSE ");
        _sb.AppendLine("     INSERT INTO   GAMING_TABLES_SESSIONS_BY_CURRENCY ");
        _sb.AppendLine("                 ( GTSC_GAMING_TABLE_SESSION_ID ");
        _sb.AppendLine("                 , GTSC_ISO_CODE ");
        _sb.AppendLine("                 , GTSC_TYPE ");
        _sb.AppendLine("                 , GTSC_FILLS_CHIPS_AMOUNT ");
        _sb.AppendLine("                 , GTSC_CREDITS_CHIPS_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_OWN_PURCHASE_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_TOTAL_PURCHASE_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_EXTERNAL_PURCHASE_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_EXTERNAL_SALES_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_OWN_SALES_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_TOTAL_SALES_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_COLLECTED_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_COLLECTED_AMOUNT_CONVERTED  ");
        _sb.AppendLine("                 , GTSC_COLLECTED_DROPBOX_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED  ");
        _sb.AppendLine("                 , GTSC_INITIAL_CHIPS_AMOUNT ");
        _sb.AppendLine("                 , GTSC_FINAL_CHIPS_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_TIPS  ");
        _sb.AppendLine("                 , GTSC_COPY_DEALER_VALIDATED_AMOUNT  ");
        _sb.AppendLine("                 ) ");

        _sb.Append(_sb_insert_params.ToString());

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = this.GamingTableSessionId;
          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NChar).Value = CurrencyIsoType.IsoCode;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = CurrencyIsoType.Type;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount.SqlMoney;

          if (UpdateType == GTS_UPDATE_TYPE.CollectedAmount ||
              UpdateType == GTS_UPDATE_TYPE.CollectedDropBoxAmount ||
              UpdateType == GTS_UPDATE_TYPE.CollectedDropBoxChips)
          {
            _converted_amount = Amount;

            // Convert to national currency
            if (CurrencyIsoType.IsoCode != CurrencyExchange.GetNationalCurrency())
            {
              CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, CurrencyIsoType.IsoCode, out _currency_exchange, Trx);
              _currency_exchange.ApplyExchange(Amount, out _exchange_result);
              _converted_amount = _exchange_result.GrossAmount;
            }

            _cmd.Parameters.Add("@pAmountConverted", SqlDbType.Money).Value = _converted_amount;
          }

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            if (UpdateType == GTS_UPDATE_TYPE.Visits)
            {
              _visits = _reader.GetDecimal(0);
            }
            else
            {
              _final_amount = _reader.GetSqlMoney(0);

              if (UpdateType == GTS_UPDATE_TYPE.ChipsOwnSales || UpdateType == GTS_UPDATE_TYPE.ChipsOwnPurchase
                || (UpdateType == GTS_UPDATE_TYPE.ValidatedCopyDealerAmount && GamingTableBusinessLogic.IsEnableManualConciliation() && GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation()))
              {
                _final_amount_total = _reader.GetSqlMoney(1);
              }
            }
          }
        }

        if (!this.Amounts.ContainsKey(CurrencyIsoType))
        {
          this.Amounts.Add(CurrencyIsoType, new GamingTablesSessionsCurrencyType());
        }

        // Update GamingTableSession property value
        switch (UpdateType)
        {
          case GTS_UPDATE_TYPE.FillIn:
            this.Amounts[CurrencyIsoType].FillsChipsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.FillOut:
            this.Amounts[CurrencyIsoType].CreditsChipsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.OpenFillIn:
            this.Amounts[CurrencyIsoType].InitialChipsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.CloseFillOut:
            this.Amounts[CurrencyIsoType].FinalChipsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsOwnPurchase:
            this.Amounts[CurrencyIsoType].OwnPurchaseAmount = _final_amount;
            this.Amounts[CurrencyIsoType].TotalPurchaseAmount = _final_amount_total;
            break;

          case GTS_UPDATE_TYPE.ChipsExternalPurchase:
            this.Amounts[CurrencyIsoType].ExternalPurchaseAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsExternalSales:
            this.Amounts[CurrencyIsoType].ExternalSalesAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsOwnSales:
            this.Amounts[CurrencyIsoType].OwnSalesAmount = _final_amount;
            this.Amounts[CurrencyIsoType].TotalSalesAmount = _final_amount_total;
            break;

          case GTS_UPDATE_TYPE.CollectedAmount:
            this.Amounts[CurrencyIsoType].CollectedAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.CollectedDropBoxAmount:
          case GTS_UPDATE_TYPE.CollectedDropBoxChips:
            this.Amounts[CurrencyIsoType].CollectedDropBoxAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.Tips:
            this.Tips = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsTotalSales:
            this.Amounts[CurrencyIsoType].TotalSalesAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsTotalPurchase:
            this.Amounts[CurrencyIsoType].TotalPurchaseAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.Visits:
            this.ClientVisits = _visits;
            break;

          case GTS_UPDATE_TYPE.ValidatedCopyDealerAmount:
            this.Amounts[CurrencyIsoType].ValidatedDealerCopyAmount = _final_amount;
            if (GamingTableBusinessLogic.IsEnableManualConciliation() && GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
            {
              this.Amounts[CurrencyIsoType].ExternalSalesAmount = _final_amount_total;
            }
            break;

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
    public Boolean UpdateSession(GTS_UPDATE_TYPE UpdateType, Currency Amount, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Currency _final_amount;
      Currency _final_amount_total;
      Decimal _visits;

      _final_amount = 0;
      _final_amount_total = 0;
      _visits = 0;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("    UPDATE   GAMING_TABLES_SESSIONS ");

        // Add Amount
        switch (UpdateType)
        {
          case GTS_UPDATE_TYPE.FillIn:
            _sb.AppendLine("   SET   GTS_FILLS_CHIPS_AMOUNT = GTS_FILLS_CHIPS_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_FILLS_CHIPS_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.FillOut:
            _sb.AppendLine("   SET   GTS_CREDITS_CHIPS_AMOUNT = GTS_CREDITS_CHIPS_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_CREDITS_CHIPS_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.OpenFillIn:
            _sb.AppendLine("   SET   GTS_INITIAL_CHIPS_AMOUNT = GTS_INITIAL_CHIPS_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_INITIAL_CHIPS_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.CloseFillOut:
            _sb.AppendLine("   SET   GTS_FINAL_CHIPS_AMOUNT = GTS_FINAL_CHIPS_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_FINAL_CHIPS_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.ChipsOwnPurchase:
            _sb.AppendLine("   SET   GTS_OWN_PURCHASE_AMOUNT = GTS_OWN_PURCHASE_AMOUNT + @pAmount ");
            _sb.AppendLine("       , GTS_TOTAL_PURCHASE_AMOUNT = GTS_TOTAL_PURCHASE_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_OWN_PURCHASE_AMOUNT ");
            _sb.AppendLine("       , INSERTED.GTS_TOTAL_PURCHASE_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.ChipsExternalPurchase:
            _sb.AppendLine("   SET   GTS_EXTERNAL_PURCHASE_AMOUNT = GTS_EXTERNAL_PURCHASE_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_EXTERNAL_PURCHASE_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.ChipsExternalSales:
            _sb.AppendLine("   SET   GTS_EXTERNAL_SALES_AMOUNT = GTS_EXTERNAL_SALES_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_EXTERNAL_SALES_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.ChipsOwnSales:
            if (GamingTableBusinessLogic.IsEnableManualConciliation() && GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
            {
              _sb.AppendLine("   SET   GTS_OWN_SALES_AMOUNT = GTS_OWN_SALES_AMOUNT ");
              _sb.AppendLine("       , GTS_TOTAL_SALES_AMOUNT = GTS_TOTAL_SALES_AMOUNT ");
              _sb.AppendLine("OUTPUT   INSERTED.GTS_OWN_SALES_AMOUNT ");
              _sb.AppendLine("       , INSERTED.GTS_TOTAL_SALES_AMOUNT ");
            }
            else
            {
              _sb.AppendLine("   SET   GTS_OWN_SALES_AMOUNT = GTS_OWN_SALES_AMOUNT + @pAmount ");
              _sb.AppendLine("       , GTS_TOTAL_SALES_AMOUNT = GTS_TOTAL_SALES_AMOUNT + @pAmount ");
              _sb.AppendLine("OUTPUT   INSERTED.GTS_OWN_SALES_AMOUNT ");
              _sb.AppendLine("       , INSERTED.GTS_TOTAL_SALES_AMOUNT ");
            }
            break;

          case GTS_UPDATE_TYPE.CollectedAmount:
            _sb.AppendLine("   SET   GTS_COLLECTED_AMOUNT = @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_COLLECTED_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.CollectedDropBoxAmount:
            _sb.AppendLine("   SET   GTS_COLLECTED_DROPBOX_AMOUNT = @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_COLLECTED_DROPBOX_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.CollectedDropBoxChips:
            _sb.AppendLine("   SET   GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT = @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.CollectedDropBoxTickets:
            _sb.AppendLine("   SET   GTS_COLLECTED_DROPBOX_TICKETS_AMOUNT = @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_COLLECTED_DROPBOX_TICKETS_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.Tips:
            _sb.AppendLine("   SET   GTS_TIPS = GTS_TIPS + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_TIPS ");
            break;

          case GTS_UPDATE_TYPE.ChipsTotalSales:
            _sb.AppendLine("   SET   GTS_TOTAL_SALES_AMOUNT = GTS_TOTAL_SALES_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_TOTAL_SALES_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.ChipsTotalPurchase:
            _sb.AppendLine("   SET   GTS_TOTAL_PURCHASE_AMOUNT = GTS_TOTAL_PURCHASE_AMOUNT + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_TOTAL_PURCHASE_AMOUNT ");
            break;

          case GTS_UPDATE_TYPE.Visits:
            _sb.AppendLine("   SET   GTS_CLIENT_VISITS = GTS_CLIENT_VISITS + @pAmount ");
            _sb.AppendLine("OUTPUT   INSERTED.GTS_CLIENT_VISITS ");
            break;

          case GTS_UPDATE_TYPE.ValidatedCopyDealerAmount:
            if (GamingTableBusinessLogic.IsEnableManualConciliation() && GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
            {
              _sb.AppendLine("   SET   GTS_COPY_DEALER_VALIDATED_AMOUNT = GTS_COPY_DEALER_VALIDATED_AMOUNT + @pAmount ");
              _sb.AppendLine("       , GTS_EXTERNAL_SALES_AMOUNT = GTS_EXTERNAL_SALES_AMOUNT + @pAmount ");
              _sb.AppendLine("OUTPUT   INSERTED.GTS_COPY_DEALER_VALIDATED_AMOUNT ");
              _sb.AppendLine("       , INSERTED.GTS_EXTERNAL_SALES_AMOUNT ");
            }
            else
            {
              _sb.AppendLine("   SET   GTS_COPY_DEALER_VALIDATED_AMOUNT = GTS_COPY_DEALER_VALIDATED_AMOUNT + @pAmount ");
              _sb.AppendLine("OUTPUT   INSERTED.GTS_COPY_DEALER_VALIDATED_AMOUNT ");
            }
            break;

          default:

            return false;
        }

        _sb.AppendLine("     WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = this.GamingTableSessionId;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount.SqlMoney;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            if (UpdateType == GTS_UPDATE_TYPE.Visits)
            {
              _visits = _reader.GetDecimal(0);
            }
            else
            {
              _final_amount = _reader.GetSqlMoney(0);

              if (UpdateType == GTS_UPDATE_TYPE.ChipsOwnSales || UpdateType == GTS_UPDATE_TYPE.ChipsOwnPurchase
                || (UpdateType == GTS_UPDATE_TYPE.ValidatedCopyDealerAmount && GamingTableBusinessLogic.IsEnableManualConciliation() && GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation()))
              {
                _final_amount_total = _reader.GetSqlMoney(1);
              }
            }
          }
        }

        // Update GamingTableSession property value
        switch (UpdateType)
        {
          case GTS_UPDATE_TYPE.FillIn:
            this.FillChipsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.FillOut:
            this.CreditChipsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.OpenFillIn:
            this.InitialChipsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.CloseFillOut:
            this.FinalChipsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsOwnPurchase:
            this.OwnPurchaseAmount = _final_amount;
            this.TotalPurchaseAmount = _final_amount_total;
            break;

          case GTS_UPDATE_TYPE.ChipsExternalPurchase:
            this.ExternalPurchaseAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsExternalSales:
            this.ExternalSalesAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsOwnSales:
            this.OwnSalesAmount = _final_amount;
            this.TotalSalesAmount = _final_amount_total;
            break;

          case GTS_UPDATE_TYPE.CollectedAmount:
            this.CollectedAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.CollectedDropBoxAmount:
            this.CollectedDropBoxAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.CollectedDropBoxChips:
            this.CollectedDropBoxChipsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.CollectedDropBoxTickets:
            this.CollectedDropBoxTicketsAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.Tips:
            this.Tips = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsTotalSales:
            this.TotalSalesAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.ChipsTotalPurchase:
            this.TotalPurchaseAmount = _final_amount;
            break;

          case GTS_UPDATE_TYPE.Visits:
            this.ClientVisits = _visits;
            break;

          case GTS_UPDATE_TYPE.ValidatedCopyDealerAmount:
            this.m_validated_dealer_copy_amount = _final_amount;
            if (GamingTableBusinessLogic.IsEnableManualConciliation() && GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
            {
              this.m_external_sales_amount = _final_amount;
            }
            break;

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateSession()

    //------------------------------------------------------------------------------
    // PURPOSE : Update specific Gaming Tables Session
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public Boolean UpdateSessionLastSessionId(GTS_UPDATE_TYPE UpdateType, Int64? LastSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("    UPDATE   GAMING_TABLES_SESSIONS ");

        // Set External Session Id
        switch (UpdateType)
        {
          case GTS_UPDATE_TYPE.LastExternalSale:
            _sb.AppendLine("   SET   GTS_LAST_EXTERNAL_SALE_SESSION_ID = @pExternalSessionId ");
            break;

          case GTS_UPDATE_TYPE.LastExternalPurchase:
            _sb.AppendLine("   SET   GTS_LAST_EXTERNAL_PURCHASE_SESSION_ID = @pExternalSessionId ");
            break;

          default:

            return false;
        }

        _sb.AppendLine("     WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = this.GamingTableSessionId;
          if (LastSessionId == null)
          {
            _cmd.Parameters.Add("@pExternalSessionId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pExternalSessionId", SqlDbType.BigInt).Value = LastSessionId;
          }

          if (_cmd.ExecuteNonQuery() != 1)
          {

            return false;
          }
        }

        // Update GamingTableSession property value
        switch (UpdateType)
        {
          case GTS_UPDATE_TYPE.LastExternalSale:
            this.LastExternalSaleSessionId = LastSessionId;
            break;

          case GTS_UPDATE_TYPE.LastExternalPurchase:
            this.LastExternalPurchaseSessionId = LastSessionId;
            break;

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateSessionLastSessionId

    //------------------------------------------------------------------------------
    // PURPOSE : Close a Gaming Table Session
    //
    //  PARAMS :
    //      - INPUT :              
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public Boolean CloseSession(SortedDictionary<CurrencyIsoType, Decimal> CollectedAmounts, Decimal TotalCollectedNationalAmount, Decimal ClientVisits, SortedDictionary<CurrencyIsoType, Decimal> AmountsTips, SortedDictionary<CurrencyIsoType, Decimal> FinalAmount, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Decimal _final_amount_chips;
      CurrencyExchange _currency_exchange;
      CurrencyExchangeResult _exchange_result;

      _sb = new StringBuilder();
      _currency_exchange = null;
      _final_amount_chips = 0;

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in FinalAmount)
      {
        if (FeatureChips.IsChipsType(_amount.Key.Type) && _amount.Key.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          if (_amount.Key.IsoCode == CurrencyExchange.GetNationalCurrency())
          {
            _final_amount_chips += _amount.Value;
          }
          else
          {
            _final_amount_chips += CurrencyExchange.GetExchange(_amount.Value, _amount.Key.IsoCode, CurrencyExchange.GetNationalCurrency(), Trx);
          }

        }
      }

      try
      {
        _sb.AppendLine("UPDATE   GAMING_TABLES_SESSIONS ");
        _sb.AppendLine("   SET   GTS_COLLECTED_AMOUNT = @pCollectedAmount ");
        _sb.AppendLine("       , GTS_CLIENT_VISITS = @pClientVisits ");
        _sb.AppendLine("       , GTS_FINAL_CHIPS_AMOUNT = @pFinalChipsAmount ");
        _sb.AppendLine("       , GTS_TIPS = GTS_TIPS + @pTipsOfChips ");
        _sb.AppendLine("       , GTS_DROPBOX_ENABLED = @pDropBoxEnabled ");
        _sb.AppendLine("       , GTS_PRE_CLOSING = @pPreClosing ");
        _sb.AppendLine(" WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = this.GamingTableSessionId;
          _cmd.Parameters.Add("@pCollectedAmount", SqlDbType.Money).Value = TotalCollectedNationalAmount;
          _cmd.Parameters.Add("@pClientVisits", SqlDbType.Decimal).Value = ClientVisits;
          _cmd.Parameters.Add("@pFinalChipsAmount", SqlDbType.Money).Value = _final_amount_chips;
          _cmd.Parameters.Add("@pTipsOfChips", SqlDbType.Decimal).Value = CageMeters.CalculateTips(AmountsTips);
          _cmd.Parameters.Add("@pDropBoxEnabled", SqlDbType.Bit).Value = GamingTablesSessions.GamingTableInfo.HasDropbox;
          _cmd.Parameters.Add("@pPreClosing", SqlDbType.Bit).Value = 0;

          if (_cmd.ExecuteNonQuery() != 1)
          {

            return false;
          }
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" IF EXISTS ( SELECT   1  ");
        _sb.AppendLine("               FROM   GAMING_TABLES_SESSIONS_BY_CURRENCY ");
        _sb.AppendLine("              WHERE   GTSC_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId ");
        _sb.AppendLine("                AND   GTSC_ISO_CODE   = @pIsoCode ");
        _sb.AppendLine("                AND   GTSC_TYPE       = @pType) ");

        _sb.AppendLine("UPDATE   GAMING_TABLES_SESSIONS_BY_CURRENCY ");
        _sb.AppendLine("   SET   GTSC_FINAL_CHIPS_AMOUNT         = ISNULL(@pFinalChipsAmount, GTSC_FINAL_CHIPS_AMOUNT) ");
        _sb.AppendLine("       , GTSC_COLLECTED_AMOUNT           = ISNULL(@pCollectedAmount, GTSC_COLLECTED_AMOUNT) ");
        _sb.AppendLine("       , GTSC_COLLECTED_AMOUNT_CONVERTED = ISNULL(@pCollectedAmountConverted, GTSC_COLLECTED_AMOUNT_CONVERTED) ");
        _sb.AppendLine("       , GTSC_TIPS                       = GTSC_TIPS + @pTips ");
        _sb.AppendLine(" WHERE   GTSC_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId ");
        _sb.AppendLine("   AND   GTSC_ISO_CODE = @pIsoCode ");
        _sb.AppendLine("   AND   GTSC_TYPE = @pType ");

        _sb.AppendLine("  ELSE ");
        _sb.AppendLine("     INSERT INTO   GAMING_TABLES_SESSIONS_BY_CURRENCY ");
        _sb.AppendLine("                 ( GTSC_GAMING_TABLE_SESSION_ID ");
        _sb.AppendLine("                 , GTSC_ISO_CODE ");
        _sb.AppendLine("                 , GTSC_TYPE ");
        _sb.AppendLine("                 , GTSC_FILLS_CHIPS_AMOUNT ");
        _sb.AppendLine("                 , GTSC_CREDITS_CHIPS_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_OWN_PURCHASE_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_TOTAL_PURCHASE_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_EXTERNAL_PURCHASE_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_EXTERNAL_SALES_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_OWN_SALES_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_TOTAL_SALES_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_COLLECTED_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_COLLECTED_AMOUNT_CONVERTED  ");
        _sb.AppendLine("                 , GTSC_COLLECTED_DROPBOX_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED  ");
        _sb.AppendLine("                 , GTSC_INITIAL_CHIPS_AMOUNT ");
        _sb.AppendLine("                 , GTSC_FINAL_CHIPS_AMOUNT  ");
        _sb.AppendLine("                 , GTSC_TIPS  ");
        _sb.AppendLine("                 ) ");
        _sb.AppendLine("          VALUES    ");
        _sb.AppendLine("                 ( @pGamingTableSessionId ");
        _sb.AppendLine("                 , @pIsoCode ");
        _sb.AppendLine("                 , @pType ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , ISNULL(@pCollectedAmount, 0) ");
        _sb.AppendLine("                 , ISNULL(@pCollectedAmountConverted, 0) ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , 0 ");
        _sb.AppendLine("                 , ISNULL(@pFinalChipsAmount, 0) ");
        _sb.AppendLine("                 , ISNULL(@pTips, 0) ");
        _sb.AppendLine("                 ) ");

        // Fill final_chips_amount only for chips, with amounts on close session
        foreach (KeyValuePair<CurrencyIsoType, Decimal> _chips_amount in FinalAmount)
        {
          if (!FeatureChips.IsChipsType(_chips_amount.Key.Type))
          {
            continue;
          }
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = this.GamingTableSessionId;
            _cmd.Parameters.Add("@pFinalChipsAmount", SqlDbType.Money).Value = _chips_amount.Value;
            _cmd.Parameters.Add("@pCollectedAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pCollectedAmountConverted", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = _chips_amount.Key.IsoCode;
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = _chips_amount.Key.Type;
            _cmd.Parameters.Add("@pTips", SqlDbType.Money).Value = 0;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }
        }

        // Fill collected amount
        foreach (KeyValuePair<CurrencyIsoType, Decimal> _collected_amount in CollectedAmounts)
        {
          if (FeatureChips.IsChipsType(_collected_amount.Key.Type))
          {
            continue;
          }

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = this.GamingTableSessionId;
            _cmd.Parameters.Add("@pFinalChipsAmount", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pCollectedAmount", SqlDbType.Money).Value = _collected_amount.Value;
            _cmd.Parameters.Add("@pTips", SqlDbType.Money).Value = 0;

            // Convert to national currency
            CurrencyExchange.ReadCurrencyExchange(_collected_amount.Key.Type, _collected_amount.Key.IsoCode, out _currency_exchange, Trx);
            _currency_exchange.ApplyExchange(_collected_amount.Value, out _exchange_result);
            _cmd.Parameters.Add("@pCollectedAmountConverted", SqlDbType.Money).Value = _exchange_result.GrossAmount;
            _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = _collected_amount.Key.IsoCode;
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = _collected_amount.Key.Type;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }
        }

        // Tips
        foreach (KeyValuePair<CurrencyIsoType, Decimal> _tips_amount in AmountsTips)
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = this.GamingTableSessionId;
            _cmd.Parameters.Add("@pFinalChipsAmount", SqlDbType.Money).Value = DBNull.Value;
            _cmd.Parameters.Add("@pCollectedAmount", SqlDbType.Money).Value = DBNull.Value;
            _cmd.Parameters.Add("@pCollectedAmountConverted", SqlDbType.Money).Value = DBNull.Value;
            _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = _tips_amount.Key.IsoCode;
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = _tips_amount.Key.Type;
            _cmd.Parameters.Add("@pTips", SqlDbType.Money).Value = _tips_amount.Value;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }
        }

        // DHA 02-OCT-2014 set gaming table pause to false
        if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
        {
          _sb = new StringBuilder();

          _sb.AppendLine(" UPDATE   GAMING_TABLES                               ");
          _sb.AppendLine("    SET   GT_PLAYER_TRACKING_PAUSE = @pIsPaused       ");
          _sb.AppendLine("  WHERE   GT_GAMING_TABLE_ID =       @pGamingTableID  ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pGamingTableID", SqlDbType.Int).Value = this.GamingTableId;
            _cmd.Parameters.Add("@pIsPaused", SqlDbType.Bit).Value = false;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Gambling Table CloseSession failed. {0}", _ex.Message));
      }

      return false;
    } // CloseSession()

    public static Boolean SetGamingTableSessionPreClosing(Int64 GamingTableSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GAMING_TABLES_SESSIONS ");
        _sb.AppendLine("    SET   GTS_PRE_CLOSING = @pIsPreClosing ");
        _sb.AppendLine("  WHERE   GTS_GAMING_TABLE_SESSION_ID =  @pGamingTableSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.Int).Value = GamingTableSessionId;
          _cmd.Parameters.Add("@pIsPreClosing", SqlDbType.Bit).Value = true;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Gambling Table Pre-closing failed. {0}", _ex.Message));
      }

      return true;
    }

    public bool UpdateGamblingTableSessionsDropbox(CageDenominationsItems.CageDenominationsDictionary CollectedDropboxAmount, SqlTransaction SqlTrx)
    {

      Decimal _total;
      SortedDictionary<CurrencyIsoType, Decimal> _total_collected_amounts;
      SortedDictionary<CurrencyIsoType, Decimal> _total_collected_chips;
      String _iso_code = null;
      CurrencyExchangeType _currency_type;
      Currency _total_tickets_collected;
      CurrencyExchange _currency_exchange;
      CurrencyExchangeResult _exchange_result;
      Decimal _total_colleted_national;
      Decimal _total_colleted_chips_national;
      DataTable _currencies_amounts;

      _total_tickets_collected = 0;
      _currency_exchange = null;
      _exchange_result = null;
      _total_colleted_national = 0;
      _total_colleted_chips_national = 0;
      _total_collected_amounts = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _total_collected_chips = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currencies_amounts = new DataTable();

      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _item in CollectedDropboxAmount)
      {
        if (_currencies_amounts.Columns.Count == 0)
        {
          _currencies_amounts = _item.Value.ItemAmounts.Clone();
        }

        foreach (DataRow _row in _item.Value.ItemAmounts.Rows)
        {
          _currencies_amounts.Rows.Add(_row.ItemArray);
        }
      }

      foreach (DataRow _row in _currencies_amounts.Rows)
      {
        if (_row["CAGE_CURRENCY_TYPE_ENUM"] as String == String.Empty || _row["CAGE_CURRENCY_TYPE_ENUM"] == DBNull.Value ||
          !FeatureChips.IsChipsType((CageCurrencyType)_row["CAGE_CURRENCY_TYPE_ENUM"]) && ((CageCurrencyType)_row["CAGE_CURRENCY_TYPE_ENUM"]) != CageCurrencyType.Coin && ((CageCurrencyType)_row["CAGE_CURRENCY_TYPE_ENUM"]) != CageCurrencyType.Bill)
        {
          continue;
        }

        if (object.ReferenceEquals(_row["TOTAL"], DBNull.Value) || !Decimal.TryParse(_row["TOTAL"].ToString(), out _total))
        {
          continue;
        }

        _iso_code = _row["ISO_CODE"].ToString();
        _currency_type = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_row["CAGE_CURRENCY_TYPE_ENUM"]);

        // Avoid TITO Tickets
        if ((Decimal.Parse(_row["DENOMINATION"].ToString()) == Cage.TICKETS_CODE || (object.ReferenceEquals(_row["UN"], DBNull.Value) || Int32.Parse(_row["UN"].ToString()) <= 0)))
        {
          continue;
        }

        if (_total > 0)
        {
          if (_currency_type != CurrencyExchangeType.CURRENCY && !FeatureChips.IsChipsType(_currency_type))
          {
            continue;
          }

          if (!_total_collected_amounts.ContainsKey(new CurrencyIsoType(_iso_code, _currency_type)))
          {
            _total_collected_amounts.Add(new CurrencyIsoType(_iso_code, _currency_type), 0);
          }

          // Currency exchange must to convert to national currency to accumulate
          if (_iso_code != CurrencyExchange.GetNationalCurrency() && !FeatureChips.IsChipsType(_currency_type))
          {
            CurrencyExchange.ReadCurrencyExchange(_currency_type, _iso_code, out _currency_exchange, SqlTrx);
            _currency_exchange.ApplyExchange(_total, out _exchange_result);
            _total_colleted_national += _exchange_result.GrossAmount;
          }
          else if (!FeatureChips.IsChipsType(_currency_type))
          {
            _total_colleted_national += _total;
          }

          _total_collected_amounts[new CurrencyIsoType(_iso_code, _currency_type)] += _total;

        }
      }

      // Update total collected dropbox in national currency
      if (!this.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxAmount, _total_colleted_national, SqlTrx))
      {
        return false;
      }

      // Update total collected dropbox by currency
      if (!this.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxAmount, _total_collected_amounts, SqlTrx))
      {
        return false;
      }

      foreach (KeyValuePair<CurrencyIsoType, decimal> _amount in _total_collected_amounts)
      {
        if (_amount.Value > 0 && FeatureChips.IsChipsType(_amount.Key.Type) & !(_amount.Key.Type == CurrencyExchangeType.CASINO_CHIP_COLOR))
        {
          if (!_total_collected_chips.ContainsKey(new CurrencyIsoType(_amount.Key.IsoCode, _amount.Key.Type)))
          {
            _total_collected_chips.Add(new CurrencyIsoType(_amount.Key.IsoCode, _amount.Key.Type), 0);
          }

          // Currency exchange must to convert to national currency to accumulate
          if (_amount.Key.IsoCode != CurrencyExchange.GetNationalCurrency())
          {
            CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, _amount.Key.IsoCode, out _currency_exchange, SqlTrx);
            _currency_exchange.ApplyExchange(_amount.Value, out _exchange_result);
            _total_colleted_chips_national += _exchange_result.GrossAmount;
          }
          else if (FeatureChips.IsChipsType(_amount.Key.Type))
          {
            _total_colleted_chips_national += _amount.Value;
          }

          _total_collected_chips[new CurrencyIsoType(_amount.Key.IsoCode, _amount.Key.Type)] += _amount.Value;

        }
      }

      // Update total collected chips (RE & NR)
      if (!this.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxChips, _total_colleted_chips_national, SqlTrx))
      {
        return false;
      }

      if (!this.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxChips, _total_collected_chips, SqlTrx))
      {
        return false;
      }

      // Update total tickets collected dropbox 
      //if (!this.UpdateSession(GTS_UPDATE_TYPE.CollectedDropBoxTickets, _total_tickets_collected, SqlTrx))
      //{
      //  return false;
      //}

      //if (!this.UpdateSession(GTS_UPDATE_TYPE.Tips, GetChipTips, SqlTrx))
      //{
      //  return false;
      //}

      return true;

    }

    // 03-JAN-2014 DLL    
    public static class GamingTableInfo
    {
      public static Boolean IsGamingTable = false;
      public static Int32 GamingTableId;
      public static Boolean IsEnabled = false;
      public static Boolean HasDropbox = false;
      public static Int32 LinkedGamingTableId = 0;

    }

    public static Boolean GetGamingTableInfo(Int32 TerminalId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      GamingTableInfo.GamingTableId = 0;
      GamingTableInfo.IsGamingTable = false;
      GamingTableInfo.IsEnabled = false;
      GamingTableInfo.HasDropbox = false;
      GamingTableInfo.LinkedGamingTableId = 0;

      try
      {
        _sb.AppendLine("SELECT   GT_GAMING_TABLE_ID ");
        _sb.AppendLine("       , GT_ENABLED ");
        _sb.AppendLine("       , GT_DROPBOX_ENABLED ");
        _sb.AppendLine("       , GT_HAS_INTEGRATED_CASHIER ");
        _sb.AppendLine("       , 0 AS GT_LINKED_GAMING_TABLE_ID ");
        _sb.AppendLine("  FROM   GAMING_TABLES ");
        _sb.AppendLine(" WHERE   GT_CASHIER_ID = @pCashierId ");
        _sb.AppendLine(" UNION ");
        _sb.AppendLine("SELECT   0 AS GT_GAMING_TABLE_ID ");
        _sb.AppendLine("       , CAST(0 AS BIT) AS GT_ENABLED ");
        _sb.AppendLine("       , CAST(0 AS BIT) AS GT_DROPBOX_ENABLED ");
        _sb.AppendLine("       , CAST(0 AS BIT) AS GT_HAS_INTEGRATED_CASHIER ");
        _sb.AppendLine("       , CASE WHEN GT_LINKED_CASHIER_ID > 0 THEN GT_GAMING_TABLE_ID ELSE 0 END AS GT_LINKED_GAMING_TABLE_ID ");
        _sb.AppendLine("  FROM   GAMING_TABLES ");
        _sb.AppendLine(" WHERE   GT_LINKED_CASHIER_ID = @pCashierId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              GamingTableInfo.GamingTableId = _reader.GetInt32(0);
              GamingTableInfo.IsEnabled = _reader.GetBoolean(1);
              GamingTableInfo.HasDropbox = _reader.GetBoolean(2);
              GamingTableInfo.IsGamingTable = _reader.GetBoolean(3);
              GamingTableInfo.LinkedGamingTableId = _reader.GetInt32(4);
            }
          }
        }

        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetGamingTableInfo

    public Boolean IsGamingTableProcessing(Int32 GamingTableId)
    {
      StringBuilder _sb;
      Boolean _processing;

      _processing = false;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _sb = new StringBuilder();


        _sb.AppendLine("SELECT   GT_PROCESSING                      ");
        _sb.AppendLine("  FROM   GAMING_TABLES                      ");
        _sb.AppendLine(" WHERE   GT_GAMING_TABLE_ID = @gamingTableId");

        using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          sql_command.Parameters.Add("@gamingTableId", SqlDbType.BigInt).Value = GamingTableId;

          using (SqlDataReader _reader = sql_command.ExecuteReader())
          {
            if (_reader.Read())
            {
              _processing = _reader[0] == DBNull.Value ? false : (Boolean)_reader[0];
            }
          }
        }

      }

      return _processing;

    }

    public static Boolean SetGamingTableProcessing(Int32 GamingTableId, Boolean Processing, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int64 _num_updated_rows;

      _sb = new StringBuilder();


      _sb.AppendLine("UPDATE   GAMING_TABLES                      ");
      _sb.AppendLine("  SET    GT_PROCESSING = @Processing        ");
      _sb.AppendLine(" WHERE   GT_GAMING_TABLE_ID = @GamingTableId");

      if (Processing)
      {
        _sb.AppendLine(" AND   GT_PROCESSING = 0");
      }

      using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        sql_command.Parameters.Add("@GamingTableId", SqlDbType.BigInt).Value = GamingTableId;
        sql_command.Parameters.Add("@Processing", SqlDbType.Bit).Value = Processing;

        try
        {
          _num_updated_rows = sql_command.ExecuteNonQuery();

          if (_num_updated_rows == 1)
          {
            return true;
          }

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      return false;

    }


    //------------------------------------------------------------------------------
    // PURPOSE : Get gaming table name by gaming table id
    //
    //  PARAMS :
    //      - INPUT : 
    //          - Int32: GamingTableId             
    //      - OUTPUT : 
    //
    // RETURNS : 
    //          - String GamingTableName       
    //      
    //   NOTES :
    public static Boolean GetGamingTableName(Int32 GamingTableId, out String GamingTableName, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      _sb = new StringBuilder();
      _obj = null;
      GamingTableName = string.Empty;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   GT_NAME ");
        _sb.AppendLine("     FROM   GAMING_TABLES ");
        _sb.AppendLine("    WHERE   GT_GAMING_TABLE_ID = @pGamingTableId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GamingTableId;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            GamingTableName = (String)_obj;

            return true;
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }
    // GetGamingTableName

    //------------------------------------------------------------------------------
    // PURPOSE : Sum all total currencies amount in national currency excluding chips and Sum total chips amount 
    //
    //  PARAMS :
    //      - INPUT : 
    //                IsoAmount             
    //      - OUTPUT : 
    //                TotalChipsAmount
    //                TotalCurrenciesAmount
    //
    // RETURNS :  
    //
    //   NOTES :
    // 07-JAN-2014 DLL
    public static void AcumCurrenciesAndChips(SortedDictionary<CurrencyIsoType, Decimal> IsoAmount, out SortedDictionary<CurrencyIsoType, Decimal> TotalAmounts, out Decimal TotalNationalCurrency, SqlTransaction Trx)
    {
      CurrencyExchange _currency_exchange = null;
      CurrencyExchangeResult _exchange_result;

      TotalNationalCurrency = 0;

      TotalAmounts = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      TotalAmounts.Add(new CurrencyIsoType(CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CURRENCY), 0);
      TotalAmounts.Add(new CurrencyIsoType(CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CASINO_CHIP_RE), 0);

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _collected in IsoAmount)
      {
        if (!TotalAmounts.ContainsKey(_collected.Key))
        {
          TotalAmounts.Add(_collected.Key, 0);
        }

        switch (_collected.Key.Type)
        {
          case CurrencyExchangeType.CASINOCHIP:
          case CurrencyExchangeType.CASINO_CHIP_RE:
          case CurrencyExchangeType.CASINO_CHIP_NRE:
          case CurrencyExchangeType.CASINO_CHIP_COLOR:
            TotalAmounts[_collected.Key] += _collected.Value;
            break;

          case CurrencyExchangeType.CARD:
          case CurrencyExchangeType.CHECK:
            TotalAmounts[new CurrencyIsoType(CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CURRENCY)] += _collected.Value;
            TotalNationalCurrency += _collected.Value;
            break;

          case CurrencyExchangeType.CURRENCY:
            // DLL 08-JAN-2014
            if (_collected.Key.IsCasinoChip)
            {
              TotalAmounts[new CurrencyIsoType(CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CASINO_CHIP_RE)] = _collected.Value;

              continue;
            }

            TotalAmounts[_collected.Key] += _collected.Value;

            if (_collected.Key.IsoCode == CurrencyExchange.GetNationalCurrency())
            {
              TotalNationalCurrency += _collected.Value;
            }
            else
            {
              // Convert to national currency
              CurrencyExchange.ReadCurrencyExchange(_collected.Key.Type, _collected.Key.IsoCode, out _currency_exchange, Trx);
              _currency_exchange.ApplyExchange(_collected.Value, out _exchange_result);
              TotalNationalCurrency += _exchange_result.GrossAmount;
            }

            break;
        }
      }
    }

    public static void GetCollectedAmount(Cashier.TYPE_CASHIER_SESSION_STATS SessionStats,
                                          out SortedDictionary<CurrencyIsoType, Decimal> TotalAmounts, out Decimal TotalNationalAmount, SqlTransaction Trx)
    {
      Decimal _chips_netwin;

      GetCollectedAmount(SessionStats, out TotalAmounts, out TotalNationalAmount, out _chips_netwin, Trx);
    }

    public static void GetCollectedAmount(Cashier.TYPE_CASHIER_SESSION_STATS SessionStats, out SortedDictionary<CurrencyIsoType, Decimal> TotalAmounts, out Decimal TotalNationalAmount, out Decimal ChipsNetwin, SqlTransaction Trx)
    {
      Decimal _total_national_in;
      Decimal _total_national_out;
      Decimal _total_national_collected;
      SortedDictionary<CurrencyIsoType, Decimal> _total_amounts_in;
      SortedDictionary<CurrencyIsoType, Decimal> _total_amounts_out;
      SortedDictionary<CurrencyIsoType, Decimal> _total_amounts_collected;
      CurrencyIsoType _iso_type_national_currency_key;

      TotalAmounts = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      _total_national_in = 0;
      _total_national_out = 0;
      _total_national_collected = 0;

      _total_amounts_in = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _total_amounts_out = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _total_amounts_collected = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      _iso_type_national_currency_key = new CurrencyIsoType();
      _iso_type_national_currency_key.IsoCode = CurrencyExchange.GetNationalCurrency();
      _iso_type_national_currency_key.Type = CurrencyExchangeType.CURRENCY;

      // Sum inputs
      GamingTablesSessions.AcumCurrenciesAndChips(SessionStats.fill_in_balance, out _total_amounts_in, out _total_national_in, Trx);
      _total_amounts_in[_iso_type_national_currency_key] += SessionStats.deposits;
      _total_national_in += SessionStats.deposits;

      //Sum outputs
      GamingTablesSessions.AcumCurrenciesAndChips(SessionStats.fill_out_balance, out _total_amounts_out, out _total_national_out, Trx);
      _total_amounts_out[_iso_type_national_currency_key] += SessionStats.withdraws;
      _total_national_out += SessionStats.withdraws;

      GamingTablesSessions.AcumCurrenciesAndChips(SessionStats.collected, out _total_amounts_collected, out _total_national_collected, Trx);

      // Totals
      // TotalCurrencies = TotalOut + TotalColleted - TotalIn
      // TotalChips = TotalChipsOut + TotalChipsColleted
      TotalNationalAmount = _total_national_out + _total_national_collected - _total_national_in;

      TotalAmounts = _total_amounts_out;

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _item_collected in _total_amounts_collected)
      {
        if (!TotalAmounts.ContainsKey(_item_collected.Key))
        {
          TotalAmounts.Add(_item_collected.Key, _item_collected.Value);
        }
        else
        {
          TotalAmounts[_item_collected.Key] += _item_collected.Value;
        }
      }

      foreach (KeyValuePair<CurrencyIsoType, Decimal> _item_in in _total_amounts_in)
      {
        if (FeatureChips.IsChipsType(_item_in.Key.Type))
        {
          continue;
        }

        if (!TotalAmounts.ContainsKey(_item_in.Key))
        {
          TotalAmounts.Add(_item_in.Key, -_item_in.Value);
        }
        else
        {
          TotalAmounts[_item_in.Key] -= _item_in.Value;
        }
      }

      // TODO: Revisar DHA
      ChipsNetwin = SessionStats.chips_sale_total - SessionStats.chips_purchase_total;
    }

    #endregion
  }

  public class GamingTablesSessionsCurrencyType
  {
    public Currency FillsChipsAmount;
    public Currency CreditsChipsAmount;
    public Currency OwnPurchaseAmount;
    public Currency ExternalPurchaseAmount;
    public Currency OwnSalesAmount;
    public Currency ExternalSalesAmount;
    public Currency TotalSalesAmount;
    public Currency TotalPurchaseAmount;
    public Currency CollectedAmount;
    public Currency CollectedDropBoxAmount;
    public Currency InitialChipsAmount;
    public Currency FinalChipsAmount;
    public Currency ValidatedDealerCopyAmount;

  }
}
