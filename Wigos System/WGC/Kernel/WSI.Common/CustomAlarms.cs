//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CustomAlarms.cs
// 
//   DESCRIPTION: Class to manage custom alarms
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 09-APR-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-APR-2014 AMF    First release.
// 20-OCT-2015 GMV    TFS 5277 : HighRollers Alarm
// 11-NOV-2015 GMV    TFS 5275 : HighRollers Extended definition
// 03-MAR-2016 RAB    Bug 7608 : The amounts reported to generate alarms from HighRollers are not correct
// 23-MAY-2016 ETP    Bug 12917: The amounts reported has to be genereted in plays table not in play_session table
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class CustomAlarms
  {

    #region Enums

    public enum CustomAlarmsTypes
    {
      Payout = 1,
      Jackpot = 2,
      CancelledCredits = 3,
      Counterfeit = 4,
      Played = 5,
      Won = 6,
      Highroller = 7,
      HighrollerAnonymous = 8
    }

    #endregion // Enums

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Conditional register of alarm depend custom alarm type
    //
    //  PARAMS :
    //      - INPUT :
    //          - CustomAlarmsTypes
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    public static Boolean ConditionalRegister(CustomAlarmsTypes Type, Int32 TerminalId, SqlTransaction Trx, DataSet Data)
    {
      switch (Type)
      {
        case CustomAlarmsTypes.Payout:
          if (!Trx_PayoutAlarm(TerminalId, Trx))
          {
            return false;
          }
          break;

        case CustomAlarmsTypes.Jackpot:
          if (!Trx_JackpotAlarm(TerminalId, Trx))
          {

            return false;
          }
          break;

        case CustomAlarmsTypes.CancelledCredits:
          if (!Trx_CanceledCredit(TerminalId, Trx))
          {

            return false;
          }
          break;

        case CustomAlarmsTypes.Counterfeit:
          if (!Trx_CounterfeitAlarm(TerminalId, Trx))
          {

            return false;
          }
          break;

        case CustomAlarmsTypes.Played:
          if (!Trx_PlayedAlarm(TerminalId, Trx))
          {

            return false;
          }
          break;

        case CustomAlarmsTypes.Won:
          if (!Trx_WonAlarm(TerminalId, Trx))
          {

            return false;
          }
          break;

        case CustomAlarmsTypes.Highroller:
          if (!Trx_HighrollerAlarm(Data, Trx))
          {
            return false;
          }
          break;

        default:
          break;
      }

      return true;
    } // ConditionalRegister

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Set Jackpot alert for terminal.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    private static Boolean Trx_JackpotAlarm(Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _period_unit;
      Int32 _period_value;
      Int32 _quantity_limit;
      Decimal _amount_limit;
      Int32 _logical_operator;
      Int32 _jackpot_flags;
      String _provider_id;
      String _terminal_name;
      Decimal _jackpot_amount;
      Int32 _jackpot_quantity;
      Boolean _create_alarm;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "", "" };

      try
      {
        _create_alarm = false;

        // Alarm jackpot enabled
        if (!GeneralParam.GetBoolean("Alarms", "Jackpot.Enabled", false))
        {

          return true;
        }

        // Period of study unit
        _period_unit = GeneralParam.GetInt32("Alarms", "Jackpot.PeriodUnderStudy.Unit", 0, 0, 3);

        // Period of study value
        _period_value = GeneralParam.GetInt32("Alarms", "Jackpot.PeriodUnderStudy.Value", 0);

        // Jackpot quantity limit
        _quantity_limit = GeneralParam.GetInt32("Alarms", "Jackpot.Limit.Quantity", 0);

        // Jackpot amount limit
        _amount_limit = GeneralParam.GetDecimal("Alarms", "Jackpot.Limit.Amount", 0);

        // Logical operator
        _logical_operator = GeneralParam.GetInt32("Alarms", "Jackpot.Limit.LogicalOperator", 0, 0, 2);

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   ISNULL(TS_JACKPOT_FLAGS,0) AS JACKPOT_FLAGS ");
        _sb.AppendLine("            , TE_NAME ");
        _sb.AppendLine("            , TE_PROVIDER_ID ");
        _sb.AppendLine("            , ISNULL(SUM(HP_AMOUNT),0) AS JACKPOT_AMOUNT ");
        _sb.AppendLine("            , COUNT(HP_TERMINAL_ID) AS JACKPOT_QUANTITY ");
        _sb.AppendLine("       FROM   TERMINALS ");
        _sb.AppendLine("  LEFT JOIN   TERMINAL_STATUS ON   TE_TERMINAL_ID = TS_TERMINAL_ID ");
        _sb.AppendLine("  LEFT JOIN   HANDPAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine("        AND   HP_TYPE = @pSiteJackpotType ");
        _sb.AppendLine("        AND   HP_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        AND   HP_DATETIME > DATEADD(#UNIT#, -@pInterval, GETDATE()) ");
        _sb.AppendLine("      WHERE   TE_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("   GROUP BY   TS_JACKPOT_FLAGS, TE_NAME, TE_PROVIDER_ID ");

        _sb.Replace("#UNIT#", GetPeriodUnitString(_period_unit));

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pSiteJackpotType", SqlDbType.BigInt).Value = HANDPAY_TYPE.JACKPOT;
          _sql_cmd.Parameters.Add("@pInterval", SqlDbType.Int).Value = _period_value;

          using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
          {
            if (!_sql_rd.Read())
            {

              return false;
            }
            _jackpot_flags = _sql_rd.GetInt32(0);
            _terminal_name = _sql_rd.GetString(1);
            _provider_id = _sql_rd.GetString(2);
            _jackpot_amount = _sql_rd.GetDecimal(3);
            _jackpot_quantity = _sql_rd.GetInt32(4);

            switch (_logical_operator)
            {
              case 0:
                //ANY UNCHECKED
                if (_amount_limit == 0)
                {
                  if ((_jackpot_quantity >= _quantity_limit) || (_quantity_limit == 1))
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_JACKPOT_COUNT";
                    _alarm_message_param[0] = _jackpot_quantity.ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                else
                {
                  if (_jackpot_amount > _amount_limit)
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_JACKPOT_AMOUNT";
                    _alarm_message_param[0] = ((Currency)_jackpot_amount).ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                break;

              case 1:
                //AND
                if (((_jackpot_quantity >= _quantity_limit) || (_quantity_limit == 1)) &&
                    (_jackpot_amount > _amount_limit))
                {
                  _create_alarm = true;
                  _alarm_message = "STR_CUSTOM_ALARMS_JACKPOT_AMOUNT_COUNT";
                  _alarm_message_param[0] = _jackpot_quantity.ToString();
                  _alarm_message_param[1] = ((Currency)_jackpot_amount).ToString();
                  _alarm_message_param[2] = _period_value.ToString();
                  _alarm_message_param[3] = GetPeriodUnitDescription(_period_unit);
                }
                break;

              case 2:
                //OR
                if ((_jackpot_quantity >= _quantity_limit) || (_quantity_limit == 1))
                {
                  _create_alarm = true;
                  if (_jackpot_amount > _amount_limit)
                  {
                    _alarm_message = "STR_CUSTOM_ALARMS_JACKPOT_AMOUNT_COUNT";
                    _alarm_message_param[0] = _jackpot_quantity.ToString();
                    _alarm_message_param[1] = ((Currency)_jackpot_amount).ToString();
                    _alarm_message_param[2] = _period_value.ToString();
                    _alarm_message_param[3] = GetPeriodUnitDescription(_period_unit);
                  }
                  else
                  {
                    _alarm_message = "STR_CUSTOM_ALARMS_JACKPOT_COUNT";
                    _alarm_message_param[0] = _jackpot_quantity.ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                else
                {
                  if (_jackpot_amount > _amount_limit)
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_JACKPOT_AMOUNT";
                    _alarm_message_param[0] = ((Currency)_jackpot_amount).ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                break;

              default:
                break;
            }
          }
        }

        if (_create_alarm)
        {
          if (!Trx_SetStatusBitmaskAndInsertAlarm(CustomAlarmsTypes.Jackpot,
                                                  TerminalId,
                                                  _jackpot_flags,
                                                  true,
                                                  _provider_id,
                                                  _terminal_name,
                                                  _alarm_message,
                                                  _alarm_message_param,
                                                  Trx))
          {

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_JackpotAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Set canceled credit alert for terminal.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    private static Boolean Trx_CanceledCredit(Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _period_unit;
      Int32 _period_value;
      Int32 _quantity_limit;
      Decimal _amount_limit;
      Int32 _logical_operator;
      Int32 _canceled_credit_flags;
      String _provider_id;
      String _terminal_name;
      Decimal _canceled_credit_amount;
      Int32 _canceled_credit_quantity;
      Boolean _create_alarm;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "", "" };

      try
      {
        _create_alarm = false;

        // Alarm canceled credit enabled
        if (!GeneralParam.GetBoolean("Alarms", "CanceledCredit.Enabled", false))
        {

          return true;
        }

        // Period of study unit
        _period_unit = GeneralParam.GetInt32("Alarms", "CanceledCredit.PeriodUnderStudy.Unit", 0, 0, 3);

        // Period of study value
        _period_value = GeneralParam.GetInt32("Alarms", "CanceledCredit.PeriodUnderStudy.Value", 0);

        // Canceled credit quantity limit
        _quantity_limit = GeneralParam.GetInt32("Alarms", "CanceledCredit.Limit.Quantity", 0);

        // Canceled credit amount limit
        _amount_limit = GeneralParam.GetDecimal("Alarms", "CanceledCredit.Limit.Amount", 0);

        // Logical operator
        _logical_operator = GeneralParam.GetInt32("Alarms", "CanceledCredit.Limit.LogicalOperator", 0, 0, 2);

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   ISNULL(TS_JACKPOT_FLAGS,0) AS JACKPOT_FLAGS ");
        _sb.AppendLine("            , TE_NAME ");
        _sb.AppendLine("            , TE_PROVIDER_ID ");
        _sb.AppendLine("            , ISNULL(SUM(HP_AMOUNT),0) AS CANCELED_CREDIT_AMOUNT ");
        _sb.AppendLine("            , COUNT(HP_TERMINAL_ID) AS CANCELED_CREDIT_QUANTITY ");
        _sb.AppendLine("       FROM   TERMINALS ");
        _sb.AppendLine("  LEFT JOIN   TERMINAL_STATUS ON   TE_TERMINAL_ID = TS_TERMINAL_ID ");
        _sb.AppendLine("  LEFT JOIN   HANDPAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine("        AND   HP_TYPE = @pSiteCanceledCreditType ");
        _sb.AppendLine("        AND   HP_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        AND   HP_DATETIME > DATEADD(#UNIT#, -@pInterval, GETDATE()) ");
        _sb.AppendLine("      WHERE   TE_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("   GROUP BY   TS_JACKPOT_FLAGS, TE_NAME, TE_PROVIDER_ID ");

        _sb.Replace("#UNIT#", GetPeriodUnitString(_period_unit));

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pSiteCanceledCreditType", SqlDbType.BigInt).Value = HANDPAY_TYPE.CANCELLED_CREDITS;
          _sql_cmd.Parameters.Add("@pInterval", SqlDbType.Int).Value = _period_value;

          using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
          {
            if (!_sql_rd.Read())
            {

              return false;
            }
            _canceled_credit_flags = _sql_rd.GetInt32(0);
            _terminal_name = _sql_rd.GetString(1);
            _provider_id = _sql_rd.GetString(2);
            _canceled_credit_amount = _sql_rd.GetDecimal(3);
            _canceled_credit_quantity = _sql_rd.GetInt32(4);

            switch (_logical_operator)
            {
              case 0:
                //ANY UNCHECKED
                if (_amount_limit == 0)
                {
                  if ((_canceled_credit_quantity >= _quantity_limit) || (_quantity_limit == 1))
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_CANCELED_CREDIT_COUNT";
                    _alarm_message_param[0] = _canceled_credit_quantity.ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                else
                {
                  if (_canceled_credit_amount > _amount_limit)
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_CANCELED_CREDIT_AMOUNT";
                    _alarm_message_param[0] = ((Currency)_canceled_credit_amount).ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                break;

              case 1:
                //AND
                if (((_canceled_credit_quantity >= _quantity_limit) || (_quantity_limit == 1)) &&
                    (_canceled_credit_amount > _amount_limit))
                {
                  _create_alarm = true;
                  _alarm_message = "STR_CUSTOM_ALARMS_CANCELED_CREDIT_AMOUNT_COUNT";
                  _alarm_message_param[0] = _canceled_credit_quantity.ToString();
                  _alarm_message_param[1] = ((Currency)_canceled_credit_amount).ToString();
                  _alarm_message_param[2] = _period_value.ToString();
                  _alarm_message_param[3] = GetPeriodUnitDescription(_period_unit);
                }
                break;

              case 2:
                //OR
                if ((_canceled_credit_quantity >= _quantity_limit) || (_quantity_limit == 1))
                {
                  _create_alarm = true;
                  if (_canceled_credit_amount > _amount_limit)
                  {
                    _alarm_message = "STR_CUSTOM_ALARMS_CANCELED_CREDIT_AMOUNT_COUNT";
                    _alarm_message_param[0] = _canceled_credit_quantity.ToString();
                    _alarm_message_param[1] = ((Currency)_canceled_credit_amount).ToString();
                    _alarm_message_param[2] = _period_value.ToString();
                    _alarm_message_param[3] = GetPeriodUnitDescription(_period_unit);
                  }
                  else
                  {
                    _alarm_message = "STR_CUSTOM_ALARMS_CANCELED_CREDIT_COUNT";
                    _alarm_message_param[0] = _canceled_credit_quantity.ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                else
                {
                  if (_canceled_credit_amount > _amount_limit)
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_CANCELED_CREDIT_AMOUNT";
                    _alarm_message_param[0] = ((Currency)_canceled_credit_amount).ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                break;

              default:
                break;
            }
          }
        }

        if (_create_alarm)
        {
          if (!Trx_SetStatusBitmaskAndInsertAlarm(CustomAlarmsTypes.CancelledCredits,
                                                  TerminalId,
                                                  _canceled_credit_flags,
                                                  true,
                                                  _provider_id,
                                                  _terminal_name,
                                                  _alarm_message,
                                                  _alarm_message_param,
                                                  Trx))
          {

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_CanceledCredit

    //------------------------------------------------------------------------------
    // PURPOSE : Set Counterfeit alert for terminal.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    private static Boolean Trx_CounterfeitAlarm(Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _period_unit;
      Int32 _period_value;
      Int32 _quantity_limit;
      Int32 _bill_flags;
      String _provider_id;
      String _terminal_name;
      Int32 _bill_quantity;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "" };

      try
      {
        // Alarm Counterfeit enabled
        if (!GeneralParam.GetBoolean("Alarms", "Counterfeit.Enabled", false))
        {

          return true;
        }

        // Period of study unit
        _period_unit = GeneralParam.GetInt32("Alarms", "Counterfeit.PeriodUnderStudy.Unit", 0, 0, 3);

        // Period of study value
        _period_value = GeneralParam.GetInt32("Alarms", "Counterfeit.PeriodUnderStudy.Value", 0);

        // Counterfeit quantity limit
        _quantity_limit = GeneralParam.GetInt32("Alarms", "Counterfeit.Limit.Quantity", 0);

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   ISNULL(TS_BILL_FLAGS,0) AS BILL_FLAGS ");
        _sb.AppendLine("            , TE_NAME ");
        _sb.AppendLine("            , TE_PROVIDER_ID ");
        _sb.AppendLine("            , ISNULL(A1.COUNTERFEIT_EXCEEDED,0) AS COUNTERFEIT_EXCEEDED ");
        _sb.AppendLine("       FROM   TERMINALS ");
        _sb.AppendLine("  LEFT JOIN   TERMINAL_STATUS ON   TE_TERMINAL_ID = TS_TERMINAL_ID ");
        _sb.AppendLine("  LEFT JOIN ( ");
        _sb.AppendLine("            SELECT   AL_SOURCE_ID AS TERMINAL_ID ");
        _sb.AppendLine("                   , COUNT(1)     AS COUNTERFEIT_EXCEEDED ");
        _sb.AppendLine("              FROM   ALARMS ");
        _sb.AppendLine("             WHERE   AL_ALARM_CODE  = @pAlarmCode ");
        _sb.AppendLine("               AND   AL_SEVERITY    = @pAlarmSeverity ");
        _sb.AppendLine("               AND   AL_SOURCE_CODE = @pAlarmSourceCode ");
        _sb.AppendLine("               AND   AL_SOURCE_ID   = @pTerminalId ");
        _sb.AppendLine("               AND   AL_DATETIME    > DATEADD(#UNIT#, -@pInterval, GETDATE()) ");
        _sb.AppendLine("          GROUP BY   AL_SOURCE_ID ");
        _sb.AppendLine("          ) AS A1 ON A1.TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine("      WHERE   TE_TERMINAL_ID = @pTerminalId ");

        _sb.Replace("#UNIT#", GetPeriodUnitString(_period_unit));

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = 65580;
          _sql_cmd.Parameters.Add("@pAlarmSeverity", SqlDbType.Int).Value = (Int32)AlarmSeverity.Info;
          _sql_cmd.Parameters.Add("@pAlarmSourceCode", SqlDbType.Int).Value = (Int32)AlarmSourceCode.TerminalSASMachine;
          _sql_cmd.Parameters.Add("@pInterval", SqlDbType.Int).Value = _period_value;

          using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
          {
            if (!_sql_rd.Read())
            {

              return false;
            }
            _bill_flags = _sql_rd.GetInt32(0);
            _terminal_name = _sql_rd.GetString(1);
            _provider_id = _sql_rd.GetString(2);
            _bill_quantity = _sql_rd.GetInt32(3);
          }
        }

        if (_bill_quantity >= _quantity_limit)
        {
          _alarm_message = "STR_CUSTOM_ALARMS_COUNTERFEIT";
          _alarm_message_param[0] = _bill_quantity.ToString();
          _alarm_message_param[1] = _period_value.ToString();
          _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);

          if (!Trx_SetStatusBitmaskAndInsertAlarm(CustomAlarmsTypes.Counterfeit,
                                                  TerminalId,
                                                  _bill_flags,
                                                  true,
                                                  _provider_id,
                                                  _terminal_name,
                                                  _alarm_message,
                                                  _alarm_message_param,
                                                  Trx))
          {

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_CounterfeitAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Set Payout alert for terminal.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    private static Boolean Trx_PayoutAlarm(Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _period_unit;
      Int32 _period_value;
      Int32 _bill_flags;
      String _provider_id;
      String _terminal_name;
      Decimal _payout_theorical;
      Decimal _payout_played;
      Decimal _payout_won;
      Decimal _payout_calculated;
      Boolean _create_alarm;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "" };
      DataTable _account_flags;

      try
      {

        // Alarm payout enabled
        if (!GeneralParam.GetBoolean("Alarms", "Payout.Enabled", false))
        {

          return true;
        }

        // Period of study unit
        _period_unit = GeneralParam.GetInt32("Alarms", "Payout.PeriodUnderStudy.Unit", 0, 0, 3);

        // Period of study value
        _period_value = GeneralParam.GetInt32("Alarms", "Counterfeit.PeriodUnderStudy.Value", 0);

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   ISNULL(TS_PLAYED_WON_FLAGS,0) AS PLAYED_WON_FLAGS ");
        _sb.AppendLine("            , TE_NAME ");
        _sb.AppendLine("            , TE_PROVIDER_ID ");
        _sb.AppendLine("            , SUM(ISNULL(TE_THEORETICAL_PAYOUT, @pGPPayOut)) AS THEORETICAL_PAYOUT");
        _sb.AppendLine("            , SUM(ISNULL(A3.PLAYED,0)) AS CURRENT_PAYOUT_PLAYED ");
        _sb.AppendLine("            , SUM(ISNULL(A3.WON,0)) AS CURRENT_PAYOUT_WON");
        _sb.AppendLine("            , TS_TERMINAL_ID ");
        _sb.AppendLine("       FROM   TERMINALS ");
        _sb.AppendLine("  LEFT JOIN   TERMINAL_STATUS ON   TE_TERMINAL_ID = TS_TERMINAL_ID ");
        _sb.AppendLine("  LEFT JOIN ( ");
        _sb.AppendLine("            SELECT   TSMH_TERMINAL_ID     AS TERMINAL_ID ");
        _sb.AppendLine("                   , CAST(SUM(CASE WHEN TSMH_METER_CODE = 0 THEN TSMH_METER_INCREMENT ELSE 0 END)/100 AS DECIMAL(18,2)) AS PLAYED ");
        _sb.AppendLine("                   , CAST(SUM(CASE WHEN TSMH_METER_CODE = 1 THEN TSMH_METER_INCREMENT ELSE 0 END)/100 AS DECIMAL(18,2)) AS WON ");
        _sb.AppendLine("              FROM   TERMINAL_SAS_METERS_HISTORY ");
        _sb.AppendLine("             WHERE   TSMH_METER_CODE IN (0,1) ");
        _sb.AppendLine("               AND   TSMH_TYPE = @pTsmhType ");
        _sb.AppendLine("               AND   TSMH_DATETIME > DATEADD(HOUR, -@pInterval, GETDATE()) ");
        _sb.AppendLine("          GROUP BY   TSMH_TERMINAL_ID ");
        _sb.AppendLine("          ) AS A3 ON A3.TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine("   GROUP BY TS_PLAYED_WON_FLAGS, TE_NAME, TE_PROVIDER_ID, TS_TERMINAL_ID ");

        _sb.Replace("#UNIT#", GetPeriodUnitString(_period_unit));

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pGPPayOut", SqlDbType.Decimal).Value = GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout");
          _sql_cmd.Parameters.Add("@pTsmhType", SqlDbType.Int).Value = (Int32)ENUM_SAS_METER_HISTORY.TSMH_HOURLY;
          _sql_cmd.Parameters.Add("@pInterval", SqlDbType.Int).Value = _period_value;


          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _account_flags = new DataTable();
            _da.Fill(_account_flags);
          }
        }

        if (_account_flags.Rows.Count > 0)
        {
          foreach (DataRow _row in _account_flags.Rows)
          {
            _bill_flags = (Int32)_row[0]; //_sql_rd.GetInt32(0);
            _terminal_name = (String)_row[1]; // _sql_rd.GetString(1);
            _provider_id = (String)_row[2]; //_sql_rd.GetString(2);
            _payout_theorical = (Decimal)_row[3]; //_sql_rd.GetDecimal(3);
            _payout_played = (Decimal)_row[4]; //_sql_rd.GetDecimal(4);
            _payout_won = (Decimal)_row[5]; //_sql_rd.GetDecimal(5);
            _payout_calculated = 0;
            TerminalId = (Int32)_row[6]; //_sql_rd.GetInt32(6);

            if (_payout_played > 0)
            {
              _payout_calculated = Math.Round((Decimal)(_payout_won / _payout_played) * 100, 2);
            }

            if (_payout_calculated == 0)
            {
              _create_alarm = false;
            }
            else
            {
              //Calculated Payout must be out of the range and different from 0 to generate alarm.
              _create_alarm = _payout_calculated > _payout_theorical + GeneralParam.GetDecimal("Alarms", "Payout.Limit.Upper", 0)
                             || _payout_calculated < _payout_theorical - GeneralParam.GetDecimal("Alarms", "Payout.Limit.Lower", 0);
            }

            _alarm_message = "STR_CUSTOM_ALARMS_PAYOUT";
            _alarm_message_param[0] = _payout_calculated.ToString();
            _alarm_message_param[1] = _period_value.ToString();
            _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);

            if (!Trx_SetStatusBitmaskAndInsertAlarm(CustomAlarmsTypes.Payout,
                                                    TerminalId,
                                                    _bill_flags,
                                                    _create_alarm,
                                                    _provider_id,
                                                    _terminal_name,
                                                    _alarm_message,
                                                    _alarm_message_param,
                                                    Trx))
            {


              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_PayoutAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Set played alert for terminal.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    private static Boolean Trx_PlayedAlarm(Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _period_unit;
      Int32 _period_value;
      Int32 _quantity_limit;
      Decimal _amount_limit;
      Int32 _logical_operator;
      Int32 _played_flags;
      String _provider_id;
      String _terminal_name;
      Decimal _played_amount;
      Int32 _played_quantity;
      Boolean _create_alarm;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "", "" };

      try
      {
        _create_alarm = false;

        // Alarm Played enabled
        if (!GeneralParam.GetBoolean("Alarms", "Played.Enabled", false))
        {

          return true;
        }

        // Period of study unit
        _period_unit = GeneralParam.GetInt32("Alarms", "PlayedWon.PeriodUnderStudy.Unit", 0, 0, 3);

        // Period of study value
        _period_value = GeneralParam.GetInt32("Alarms", "PlayedWon.PeriodUnderStudy.Value", 0);

        // Played quantity limit
        _quantity_limit = GeneralParam.GetInt32("Alarms", "Played.Limit.Quantity", 0);

        // Played amount limit
        _amount_limit = GeneralParam.GetDecimal("Alarms", "Played.Limit.Amount", 0);

        // Logical operator
        _logical_operator = GeneralParam.GetInt32("Alarms", "Played.Limit.LogicalOperator", 0, 0, 2);

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   ISNULL(TS_PLAYED_WON_FLAGS,0) AS PLAYED_WON_FLAGS ");
        _sb.AppendLine("            , TE_NAME ");
        _sb.AppendLine("            , TE_PROVIDER_ID ");
        _sb.AppendLine("            , ISNULL(SUM(PS_PLAYED_AMOUNT),0) AS PLAYED_AMOUNT ");
        _sb.AppendLine("            , ISNULL(SUM(PS_PLAYED_COUNT),0) AS PLAYED_COUNT ");
        _sb.AppendLine("       FROM   TERMINALS ");
        _sb.AppendLine("  LEFT JOIN   TERMINAL_STATUS ON   TE_TERMINAL_ID = TS_TERMINAL_ID ");
        _sb.AppendLine("  LEFT JOIN   PLAY_SESSIONS ON PS_TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine("        AND   PS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        AND   PS_FINISHED > DATEADD(#UNIT#, -@pInterval, GETDATE()) ");
        _sb.AppendLine("      WHERE   TE_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("   GROUP BY   TS_PLAYED_WON_FLAGS, TE_NAME, TE_PROVIDER_ID ");

        _sb.Replace("#UNIT#", GetPeriodUnitString(_period_unit));

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pInterval", SqlDbType.Int).Value = _period_value;

          using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
          {
            if (!_sql_rd.Read())
            {

              return false;
            }
            _played_flags = _sql_rd.GetInt32(0);
            _terminal_name = _sql_rd.GetString(1);
            _provider_id = _sql_rd.GetString(2);
            _played_amount = _sql_rd.GetDecimal(3);
            _played_quantity = _sql_rd.GetInt32(4);

            switch (_logical_operator)
            {
              case 0:
                //ANY UNCHECKED
                if (_amount_limit == 0)
                {
                  if ((_played_quantity >= _quantity_limit) || (_quantity_limit == 1))
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_PLAYED_COUNT";
                    _alarm_message_param[0] = _played_quantity.ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                else
                {
                  if (_played_amount > _amount_limit)
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_PLAYED_AMOUNT";
                    _alarm_message_param[0] = ((Currency)_played_amount).ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                break;

              case 1:
                //AND
                if (((_played_quantity >= _quantity_limit) || (_quantity_limit == 1)) &&
                    (_played_amount > _amount_limit))
                {
                  _create_alarm = true;
                  _alarm_message = "STR_CUSTOM_ALARMS_PLAYED_AMOUNT_COUNT";
                  _alarm_message_param[0] = _played_quantity.ToString();
                  _alarm_message_param[1] = ((Currency)_played_amount).ToString();
                  _alarm_message_param[2] = _period_value.ToString();
                  _alarm_message_param[3] = GetPeriodUnitDescription(_period_unit);
                }
                break;

              case 2:
                //OR
                if ((_played_quantity >= _quantity_limit) || (_quantity_limit == 1))
                {
                  _create_alarm = true;
                  if (_played_amount > _amount_limit)
                  {
                    _alarm_message = "STR_CUSTOM_ALARMS_PLAYED_AMOUNT_COUNT";
                    _alarm_message_param[0] = _played_quantity.ToString();
                    _alarm_message_param[1] = ((Currency)_played_amount).ToString();
                    _alarm_message_param[2] = _period_value.ToString();
                    _alarm_message_param[3] = GetPeriodUnitDescription(_period_unit);
                  }
                  else
                  {
                    _alarm_message = "STR_CUSTOM_ALARMS_PLAYED_COUNT";
                    _alarm_message_param[0] = _played_quantity.ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                else
                {
                  if (_played_amount > _amount_limit)
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_PLAYED_AMOUNT";
                    _alarm_message_param[0] = ((Currency)_played_amount).ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                break;

              default:
                break;
            }
          }
        }

        if (_create_alarm)
        {
          if (!Trx_SetStatusBitmaskAndInsertAlarm(CustomAlarmsTypes.Played,
                                                  TerminalId,
                                                  _played_flags,
                                                  true,
                                                  _provider_id,
                                                  _terminal_name,
                                                  _alarm_message,
                                                  _alarm_message_param,
                                                  Trx))
          {

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_PlayedAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Set won alert for terminal.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    private static Boolean Trx_WonAlarm(Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _period_unit;
      Int32 _period_value;
      Int32 _quantity_limit;
      Decimal _amount_limit;
      Int32 _logical_operator;
      Int32 _won_flags;
      String _provider_id;
      String _terminal_name;
      Decimal _won_amount;
      Int32 _won_quantity;
      Boolean _create_alarm;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "", "" };

      try
      {
        _create_alarm = false;

        // Alarm Won enabled
        if (!GeneralParam.GetBoolean("Alarms", "Won.Enabled", false))
        {

          return true;
        }

        // Period of study unit
        _period_unit = GeneralParam.GetInt32("Alarms", "PlayedWon.PeriodUnderStudy.Unit", 0, 0, 3);

        // Period of study value
        _period_value = GeneralParam.GetInt32("Alarms", "PlayedWon.PeriodUnderStudy.Value", 0);

        // Played quantity limit
        _quantity_limit = GeneralParam.GetInt32("Alarms", "Won.Limit.Quantity", 0);

        // Played amount limit
        _amount_limit = GeneralParam.GetDecimal("Alarms", "Won.Limit.Amount", 0);

        // Logical operator
        _logical_operator = GeneralParam.GetInt32("Alarms", "Won.Limit.LogicalOperator", 0, 0, 2);

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   ISNULL(TS_PLAYED_WON_FLAGS,0) AS PLAYED_WON_FLAGS ");
        _sb.AppendLine("            , TE_NAME ");
        _sb.AppendLine("            , TE_PROVIDER_ID ");
        _sb.AppendLine("            , ISNULL(SUM(PS_WON_AMOUNT),0) AS WON_AMOUNT ");
        _sb.AppendLine("            , ISNULL(SUM(PS_WON_COUNT),0) AS WON_COUNT ");
        _sb.AppendLine("       FROM   TERMINALS ");
        _sb.AppendLine("  LEFT JOIN   TERMINAL_STATUS ON   TE_TERMINAL_ID = TS_TERMINAL_ID ");
        _sb.AppendLine("  LEFT JOIN   PLAY_SESSIONS ON PS_TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine("        AND   PS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        AND   PS_FINISHED > DATEADD(#UNIT#, -@pInterval, GETDATE()) ");
        _sb.AppendLine("      WHERE   TE_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("   GROUP BY   TS_PLAYED_WON_FLAGS, TE_NAME, TE_PROVIDER_ID ");

        _sb.Replace("#UNIT#", GetPeriodUnitString(_period_unit));

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pInterval", SqlDbType.Int).Value = _period_value;

          using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
          {
            if (!_sql_rd.Read())
            {

              return false;
            }
            _won_flags = _sql_rd.GetInt32(0);
            _terminal_name = _sql_rd.GetString(1);
            _provider_id = _sql_rd.GetString(2);
            _won_amount = _sql_rd.GetDecimal(3);
            _won_quantity = _sql_rd.GetInt32(4);

            switch (_logical_operator)
            {
              case 0:
                //ANY UNCHECKED
                if (_amount_limit == 0)
                {
                  if ((_won_quantity >= _quantity_limit) || (_won_quantity == 1))
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_WON_COUNT";
                    _alarm_message_param[0] = _won_quantity.ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                else
                {
                  if (_won_amount > _amount_limit)
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_WON_AMOUNT";
                    _alarm_message_param[0] = ((Currency)_won_amount).ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                break;

              case 1:
                //AND
                if (((_won_quantity >= _quantity_limit) || (_quantity_limit == 1)) &&
                    (_won_amount > _amount_limit))
                {
                  _create_alarm = true;
                  _alarm_message = "STR_CUSTOM_ALARMS_WON_AMOUNT_COUNT";
                  _alarm_message_param[0] = _won_quantity.ToString();
                  _alarm_message_param[1] = ((Currency)_won_amount).ToString();
                  _alarm_message_param[2] = _period_value.ToString();
                  _alarm_message_param[3] = GetPeriodUnitDescription(_period_unit);
                }
                break;

              case 2:
                //OR
                if ((_won_quantity >= _quantity_limit) || (_won_quantity == 1))
                {
                  _create_alarm = true;
                  if (_won_amount > _amount_limit)
                  {
                    _alarm_message = "STR_CUSTOM_ALARMS_WON_AMOUNT_COUNT";
                    _alarm_message_param[0] = _won_quantity.ToString();
                    _alarm_message_param[1] = ((Currency)_won_amount).ToString();
                    _alarm_message_param[2] = _period_value.ToString();
                    _alarm_message_param[3] = GetPeriodUnitDescription(_period_unit);
                  }
                  else
                  {
                    _alarm_message = "STR_CUSTOM_ALARMS_WON_COUNT";
                    _alarm_message_param[0] = _won_quantity.ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                else
                {
                  if (_won_amount > _amount_limit)
                  {
                    _create_alarm = true;
                    _alarm_message = "STR_CUSTOM_ALARMS_WON_AMOUNT";
                    _alarm_message_param[0] = ((Currency)_won_amount).ToString();
                    _alarm_message_param[1] = _period_value.ToString();
                    _alarm_message_param[2] = GetPeriodUnitDescription(_period_unit);
                  }
                }
                break;

              default:
                break;
            }
          }
        }

        if (_create_alarm)
        {
          if (!Trx_SetStatusBitmaskAndInsertAlarm(CustomAlarmsTypes.Won,
                                                  TerminalId,
                                                  _won_flags,
                                                  true,
                                                  _provider_id,
                                                  _terminal_name,
                                                  _alarm_message,
                                                  _alarm_message_param,
                                                  Trx))
          {

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_WonAlarm


    /// <summary>
    /// Get Amount For Highroller
    /// </summary>
    /// <param name="Play_Session_id">list of current play sessions</param>
    /// <param name="Filter">Alarm filter</param>
    /// <param name="Trx"></param>
    /// <returns>DataTable</returns>
    private static DataTable GetAmountForHighroller(List<String> Play_Session_id, String Filter, SqlTransaction Trx)
    {
      DataTable _amounts;
      DataTable _return_amounts;
      DataRow[] _row_amounts;
      StringBuilder _sb;
      Int32 _period_value;
      String _str_play_session_id;

      try
      {
        _str_play_session_id = String.Join(",", Play_Session_id.ToArray());
        if (_str_play_session_id == String.Empty)
        {
          return null;
        }

        _sb = new StringBuilder();

        _period_value = GeneralParam.GetInt32("Alarms", "HighRoller.PeriodUnderStudy.Value", 0);

        _sb.AppendLine("SELECT      PL_PLAY_SESSION_ID                       ");
        _sb.AppendLine("          , SUM(PL_PLAYED_AMOUNT) AS PS_PLAYED_AMOUNT");
        _sb.AppendLine("          , AVG(PL_PLAYED_AMOUNT) AS PS_AVERAGE_BET  ");
        _sb.AppendLine("      FROM  PLAYS                                    ");
        _sb.AppendFormat("   WHERE  PL_PLAY_SESSION_ID IN ({0})              ", _str_play_session_id);
        _sb.AppendLine("       AND  PL_DATETIME > @pDateStudied              ");
        _sb.AppendLine("  GROUP BY  PL_PLAY_SESSION_ID                       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {

          _sql_cmd.Parameters.Add("@pDateStudied", SqlDbType.DateTime).Value = WGDB.Now.AddMinutes(-_period_value);

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _amounts = new DataTable();
            _da.Fill(_amounts);
            _row_amounts = _amounts.Select(Filter);
            _return_amounts = _amounts.Clone();
            foreach (DataRow row in _row_amounts)
            {
              _return_amounts.ImportRow(row);
            }
            return _return_amounts;
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Set Highroller alert for terminal.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    private static Boolean Trx_HighrollerAlarm(DataSet _ds_customers_playing, SqlTransaction Trx) //, Boolean  ProcessAnonymous)
    {

      Int32 _period_unit;
      Int32 _period_value;
      Decimal _bet_average_limit;
      Int32 _played_won_flags;
      String _provider_name="";
      String _provider_id;
      Int32 _terminal_id;
      String _terminal_name;
      Decimal _bet_average;
      String _track_data;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "", "", "", "" };
      Boolean _process_anonymous;
      String _filter_plays;
      String _filter_play_sessions;
      Decimal _coin_in_limit;
      DataRow[] _drl_data;
      Decimal _coin_in;
      Int32 _user_type;
      String _card_track_data;
      Boolean rc;
      List<String> _play_session_list;
      DataTable _amounts;
      DataRow[] _row_amount;
      try
      {
        // Alarm Highroller enabled       
        if (!GeneralParam.GetBoolean("Alarms", "HighRoller.Enabled", false))
        {
          return true;
        }


        // Period of study unit
        _period_unit = GeneralParam.GetInt32("Alarms", "HighRoller.PeriodUnderStudy.Unit", 0, 0, 3);

        // Period of study value
        _period_value = GeneralParam.GetInt32("Alarms", "HighRoller.PeriodUnderStudy.Value", 0);

        // Highroller Limit BetAverage
        _bet_average_limit = GeneralParam.GetDecimal("Alarms", "HighRoller.Limit.BetAverage", 0);

        // Highroller Limit CoinIn
        _coin_in_limit = GeneralParam.GetDecimal("Alarms", "HighRoller.Limit.CoinIn", 0);

        // Highroller Process Anonymous
        _process_anonymous = GeneralParam.GetBoolean("Alarms", "HighRoller.ProcessAnonymous", false);


        if ((_bet_average_limit <= 0 && _coin_in_limit <= 0) || _period_unit == 0 || _period_value <= 0)
          return false;

        _alarm_message = (_coin_in_limit > 0) ? ((_bet_average_limit > 0) ? "STR_CUSTOM_ALARMS_HIGHROLLER" : "STR_CUSTOM_ALARMS_HIGHROLLER_COIN_IN") : "STR_CUSTOM_ALARMS_HIGHROLLER_BET_AVERAGE";

        DateTime _date_limit = WGDB.Now.AddMinutes(-_period_value);
        // RAB 03-MAR-2016: Old filter WD_AVERAGE_BET and WD_PLAYED_AMOUNT, change for PS_AVERAGE_BET and PD_PLAYED_AMOUNT

        _filter_plays = _bet_average_limit > 0 ? "PS_AVERAGE_BET > " + Format.LocalNumberToDBNumber(_bet_average_limit.ToString()) : "";
        _filter_plays = _coin_in_limit > 0 ? _filter_plays + (_bet_average_limit > 0 ? " AND " : "") + "PS_PLAYED_AMOUNT > " + Format.LocalNumberToDBNumber(_coin_in_limit.ToString()) : _filter_plays;
        _filter_play_sessions = String.Empty;

        if (_ds_customers_playing != null && _ds_customers_playing.Tables[0].Rows.Count > 0)
        {
          _filter_play_sessions = _filter_plays + (_ds_customers_playing.Tables[0].Rows[0]["PS_FINISHED"] != null ? " AND  PS_FINISHED >= '" : " AND  PS_STARTED >= '") + _date_limit.ToString("yyyy-MM-dd HH:mm:ss") + "'";
        }

        _drl_data = _ds_customers_playing.Tables[0].Select(_filter_play_sessions);
        _play_session_list = new List<String>();

        foreach (DataRow dr in _drl_data)
        {
          _play_session_list.Add(dr["PS_PLAY_SESSION_ID"].ToString());
        }

        _amounts = GetAmountForHighroller(_play_session_list, _filter_plays, Trx);

        if (_amounts != null && _amounts.Rows.Count > 0)
        {
          foreach (DataRow dr in _drl_data)
          {
            _user_type = (int)dr["AC_USER_TYPE"];

            if (_process_anonymous && _user_type != 0)
              continue;

            _row_amount = _amounts.Select("PL_PLAY_SESSION_ID = " + dr["PS_PLAY_SESSION_ID"].ToString());

            if (_row_amount.Length == 0)
              continue;

            _played_won_flags = (int)dr["PLAYED_WON_FLAGS"];
            _terminal_id = (int)dr["TE_TERMINAL_ID"];
            _terminal_name = dr["TE_NAME"].ToString();
            _provider_id = dr["TE_PROV_ID"].ToString();

            if (dr.Table.Columns.Contains("PV_NAME") && !dr.IsNull("PV_NAME"))
            {
              _provider_name = dr["PV_NAME"].ToString();
            }

            if (string.IsNullOrEmpty(_provider_name)) _provider_name = _provider_id;


            // RAB 03-MAR-2016
            // ETP 20-MAY-2016
            _bet_average = (Decimal)_row_amount[0]["PS_AVERAGE_BET"]; //Old value: WD_AVERAGE_BET
            _coin_in = (Decimal)_row_amount[0]["PS_PLAYED_AMOUNT"]; //Old value: WD_PLAYED_AMOUNT



            _track_data = Misc.IsCashlessMode() || WSI.Common.Misc.IsMico2Mode() ? dr["AC_TRACK_DATA"].ToString() : String.Empty;
            _card_track_data = "";

            if (!String.IsNullOrEmpty(_track_data))
              rc = CardNumber.TrackDataToExternal(out _card_track_data, _track_data, (int)CardTrackData.CardsType.PLAYER);

            _alarm_message_param[0] = (_bet_average_limit > 0) ? ((Currency)_bet_average).ToString() : " ";
            _alarm_message_param[1] = string.IsNullOrEmpty(_card_track_data) ? "" : ", " + _card_track_data;
            _alarm_message_param[2] = _period_value.ToString();
            _alarm_message_param[3] = GetPeriodUnitDescription(1);
            _alarm_message_param[4] = (_coin_in_limit > 0) ? ((Currency)_coin_in).ToString() : "";
            _alarm_message_param[5] = _user_type == 0 ? " " + Resource.String("STR_CUSTOM_ALARMS_HIGHROLLER_ANONYMOUS") : "";

            if (!Trx_SetStatusBitmaskAndInsertAlarm((_user_type == 0 ? CustomAlarmsTypes.HighrollerAnonymous : CustomAlarmsTypes.Highroller),
                                                    _terminal_id,
                                                    _played_won_flags,
                                                    true,
                                                    _provider_name,
                                                    _terminal_name,
                                                    _alarm_message,
                                                    _alarm_message_param,
                                                    Trx))
            {
              return false;
            }
          }

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_HighrollerAlarm



    //------------------------------------------------------------------------------
    // PURPOSE : Get the string with the period under study unit.
    //
    //  PARAMS :
    //      - INPUT :
    //          - PeriodUnit
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String GetPeriodUnitString(Int32 PeriodUnit)
    {
      switch (PeriodUnit)
      {
        case 1:
          return "MINUTE";

        case 2:
        default:
          return "HOUR";

        case 3:

          return "DAY";
      }
    } // GetPeriodUnitString

    //------------------------------------------------------------------------------
    // PURPOSE : Get the string with the description of the period under study unit.
    //
    //  PARAMS :
    //      - INPUT :
    //          - PeriodUnit
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String GetPeriodUnitDescription(Int32 PeriodUnit)
    {
      switch (PeriodUnit)
      {
        case 1:
          return Resource.String("STR_CUSTOM_ALARMS_MINUTE");

        case 2:
        default:
          return Resource.String("STR_CUSTOM_ALARMS_HOUR");

        case 3:
          return Resource.String("STR_CUSTOM_ALARMS_DAY");
      }
    } // GetPeriodUnitDescription

    //------------------------------------------------------------------------------
    // PURPOSE : Set Terminal_Status bitmask and Inserts an alarm if the flag changes from inactive to active.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AlarmType: Type of alarm.
    //          - TerminalId: Terminal Id to process.
    //          - Flags: Value of flags to process.
    //          - NewFlagStatus: Indicates if the alarm must be activated or deactivated.
    //          - ProviderId: String with provider id for the alarm.
    //          - TerminalName: String with terminal name for the alarm.
    //          - AlarmMessage: String with the resoure id for the alarm.                
    //          - AlarmMessageParam: String with param for alarm message.
    //          - SqlTransaction: Transaction.
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  Successfully
    //      - False: Otherwise
    //
    private static Boolean Trx_SetStatusBitmaskAndInsertAlarm(CustomAlarmsTypes AlarmType,
                                                             Int32 TerminalId,
                                                             Int32 Flags,
                                                             Boolean NewFlagStatus,
                                                             String ProviderId,
                                                             String TerminalName,
                                                             String AlarmMessage,
                                                             String[] AlarmMessageParam,
                                                             SqlTransaction Trx)
    {
      String _al_source_name;
      String _al_message;
      Int32 _ts_flag;
      Boolean _old_flag_status;
      Int32 _new_flag;
      Int64 _alarm_id = 0;
      AlarmCode _custom_alarm_code;
      DateTime _dt = new DateTime();

      switch (AlarmType)
      {
        case CustomAlarmsTypes.Payout:
          _ts_flag = (Int32)TerminalStatusFlags.PLAYS_FLAG.CURRENT_PAYOUT;
          _custom_alarm_code = AlarmCode.CustomAlarm_Payout;
          break;

        case CustomAlarmsTypes.Jackpot:
          _ts_flag = (Int32)TerminalStatusFlags.JACKPOT_FLAG.JACKPOTS;
          _custom_alarm_code = AlarmCode.CustomAlarm_Jackpot;
          break;

        case CustomAlarmsTypes.CancelledCredits:
          _ts_flag = (Int32)TerminalStatusFlags.JACKPOT_FLAG.CANCELED_CREDIT;
          _custom_alarm_code = AlarmCode.CustomAlarm_Canceled_Credit;
          break;

        case CustomAlarmsTypes.Counterfeit:
          _ts_flag = (Int32)TerminalStatusFlags.BILL_FLAG.COUNTERFEIT_EXCEEDED;
          _custom_alarm_code = AlarmCode.CustomAlarm_Counterfeit;
          break;

        case CustomAlarmsTypes.Played:
          _ts_flag = (Int32)TerminalStatusFlags.PLAYS_FLAG.PLAYED;
          _custom_alarm_code = AlarmCode.CustomAlarm_Played;
          break;

        case CustomAlarmsTypes.Won:
          _ts_flag = (Int32)TerminalStatusFlags.PLAYS_FLAG.WON;
          _custom_alarm_code = AlarmCode.CustomAlarm_Won;
          break;

        case CustomAlarmsTypes.Highroller:
          _ts_flag = (Int32)TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER;
          _custom_alarm_code = AlarmCode.CustomAlarm_HighRoller;
          break;

        case CustomAlarmsTypes.HighrollerAnonymous:
          _ts_flag = (Int32)TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER_ANONYMOUS;
          _custom_alarm_code = AlarmCode.CustomAlarm_HighRoller_Anonymous;
          break;

        default:
          return false;
      }

      _old_flag_status = TerminalStatusFlags.IsFlagActived(Flags, _ts_flag);
      _new_flag = TerminalStatusFlags.UpdateBitmask(Flags, _ts_flag, NewFlagStatus);

      try
      {
        //Inserts alarm if the status of the flag changes from inactive to active.
        if (!_old_flag_status && NewFlagStatus)
        {
          _al_source_name = Misc.ConcatProviderAndTerminal(ProviderId, TerminalName);
          _al_message = String.IsNullOrEmpty(AlarmMessageParam[0]) ? Resource.String(AlarmMessage) : Resource.String(AlarmMessage, AlarmMessageParam);

          if (!Alarm.Register(AlarmSourceCode.TerminalSASMachine, TerminalId, _al_source_name, (UInt32)_custom_alarm_code, _al_message, AlarmSeverity.Info, WGDB.Now, false, _dt, out _alarm_id, Trx))
          {

            return false;
          }
        }

        //Modify flag
        if ((_old_flag_status && _new_flag == 0) ||
           (!_old_flag_status && _new_flag != 0))
        {
          if (!TerminalStatusFlags.DB_SetValue(AlarmType, TerminalId, _new_flag, _alarm_id, Trx))
          {

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("Trx_SetStatusBitmaskAndInsertAlarm - Error generating Alarm '" + Resource.String(AlarmMessage) + "'");
        Log.Exception(_ex);

        return false;
      }
    } // Trx_SetStatusBitmaskAndInsertAlarm

    #endregion // Private Methods

  } // CustomAlarms
} // WSI.Common
