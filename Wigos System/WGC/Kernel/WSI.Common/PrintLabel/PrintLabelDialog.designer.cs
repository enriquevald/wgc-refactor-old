﻿namespace WSI.Labels
{
  partial class PrintLabelDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel1 = new System.Windows.Forms.Panel();
      this.btn_reset = new System.Windows.Forms.Button();
      this.btn_settings = new System.Windows.Forms.Button();
      this.btn_print = new System.Windows.Forms.Button();
      this.btn_close = new System.Windows.Forms.Button();
      this.llb_printer_title = new System.Windows.Forms.Label();
      this.lbl_printer = new System.Windows.Forms.Label();
      this.lbl_paper_source = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.label19 = new System.Windows.Forms.Label();
      this.UD_copies = new System.Windows.Forms.NumericUpDown();
      this.lblPageCount = new System.Windows.Forms.Label();
      this.groupBox3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.UD_copies)).BeginInit();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Location = new System.Drawing.Point(244, 71);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(110, 155);
      this.panel1.TabIndex = 1;
      this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
      this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
      this.panel1.MouseEnter += new System.EventHandler(this.panel1_MouseEnter);
      this.panel1.MouseLeave += new System.EventHandler(this.panel1_MouseLeave);
      this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
      this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
      // 
      // btn_reset
      // 
      this.btn_reset.Location = new System.Drawing.Point(128, 71);
      this.btn_reset.Name = "btn_reset";
      this.btn_reset.Size = new System.Drawing.Size(110, 29);
      this.btn_reset.TabIndex = 3;
      this.btn_reset.Text = "Resetar";
      this.btn_reset.UseVisualStyleBackColor = true;
      this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
      // 
      // btn_settings
      // 
      this.btn_settings.Location = new System.Drawing.Point(12, 232);
      this.btn_settings.Name = "btn_settings";
      this.btn_settings.Size = new System.Drawing.Size(110, 29);
      this.btn_settings.TabIndex = 4;
      this.btn_settings.Text = "Preferencias ...";
      this.btn_settings.UseVisualStyleBackColor = true;
      this.btn_settings.Click += new System.EventHandler(this.btn_settings_Click);
      // 
      // btn_print
      // 
      this.btn_print.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btn_print.Location = new System.Drawing.Point(128, 232);
      this.btn_print.Name = "btn_print";
      this.btn_print.Size = new System.Drawing.Size(110, 29);
      this.btn_print.TabIndex = 1;
      this.btn_print.Text = "Imprimir";
      this.btn_print.UseVisualStyleBackColor = true;
      this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
      // 
      // btn_close
      // 
      this.btn_close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_close.Location = new System.Drawing.Point(244, 232);
      this.btn_close.Name = "btn_close";
      this.btn_close.Size = new System.Drawing.Size(110, 29);
      this.btn_close.TabIndex = 2;
      this.btn_close.Text = "Cerrar";
      this.btn_close.UseVisualStyleBackColor = true;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // llb_printer_title
      // 
      this.llb_printer_title.AutoSize = true;
      this.llb_printer_title.Location = new System.Drawing.Point(12, 9);
      this.llb_printer_title.Name = "llb_printer_title";
      this.llb_printer_title.Size = new System.Drawing.Size(56, 13);
      this.llb_printer_title.TabIndex = 6;
      this.llb_printer_title.Text = "Impresora:";
      // 
      // lbl_printer
      // 
      this.lbl_printer.AutoSize = true;
      this.lbl_printer.Location = new System.Drawing.Point(77, 9);
      this.lbl_printer.Name = "lbl_printer";
      this.lbl_printer.Size = new System.Drawing.Size(56, 13);
      this.lbl_printer.TabIndex = 7;
      this.lbl_printer.Text = "Impresora:";
      // 
      // lbl_paper_source
      // 
      this.lbl_paper_source.AutoSize = true;
      this.lbl_paper_source.Location = new System.Drawing.Point(108, 31);
      this.lbl_paper_source.Name = "lbl_paper_source";
      this.lbl_paper_source.Size = new System.Drawing.Size(53, 13);
      this.lbl_paper_source.TabIndex = 9;
      this.lbl_paper_source.Text = "Impresora";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 31);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(87, 13);
      this.label2.TabIndex = 8;
      this.label2.Text = "Fuente de papel:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(241, 53);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(80, 13);
      this.label1.TabIndex = 10;
      this.label1.Text = "Primera página:";
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(9, 106);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(226, 57);
      this.label3.TabIndex = 11;
      this.label3.Text = "La imagen muestra las etiquetas disponibles en la primer página. Haga Click sobre" +
    " las etiquetas para cambiar entre disponible y ya utilizada.";
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.label19);
      this.groupBox3.Controls.Add(this.UD_copies);
      this.groupBox3.Location = new System.Drawing.Point(12, 163);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(223, 45);
      this.groupBox3.TabIndex = 0;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Copias";
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Location = new System.Drawing.Point(47, 19);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(96, 13);
      this.label19.TabIndex = 43;
      this.label19.Text = "Número de copias:";
      // 
      // UD_copies
      // 
      this.UD_copies.Location = new System.Drawing.Point(154, 17);
      this.UD_copies.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
      this.UD_copies.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.UD_copies.Name = "UD_copies";
      this.UD_copies.Size = new System.Drawing.Size(59, 20);
      this.UD_copies.TabIndex = 0;
      this.UD_copies.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.UD_copies.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.UD_copies.ValueChanged += new System.EventHandler(this.UD_copies_ValueChanged);
      // 
      // lblPageCount
      // 
      this.lblPageCount.AutoSize = true;
      this.lblPageCount.Location = new System.Drawing.Point(12, 213);
      this.lblPageCount.Name = "lblPageCount";
      this.lblPageCount.Size = new System.Drawing.Size(129, 13);
      this.lblPageCount.TabIndex = 12;
      this.lblPageCount.Text = "Número total de Páginas :";
      // 
      // PrintLabelDialog
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(361, 269);
      this.Controls.Add(this.lblPageCount);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.lbl_paper_source);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.lbl_printer);
      this.Controls.Add(this.llb_printer_title);
      this.Controls.Add(this.btn_print);
      this.Controls.Add(this.btn_close);
      this.Controls.Add(this.btn_settings);
      this.Controls.Add(this.btn_reset);
      this.Controls.Add(this.panel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "PrintLabelDialog";
      this.Text = "Impresión de etiquetas";
      this.Load += new System.EventHandler(this.Form2_Load);
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.UD_copies)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btn_reset;
    private System.Windows.Forms.Button btn_settings;
    private System.Windows.Forms.Button btn_print;
    private System.Windows.Forms.Button btn_close;
    private System.Windows.Forms.Label llb_printer_title;
    private System.Windows.Forms.Label lbl_printer;
    private System.Windows.Forms.Label lbl_paper_source;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.NumericUpDown UD_copies;
    private System.Windows.Forms.Label lblPageCount;
  }
}