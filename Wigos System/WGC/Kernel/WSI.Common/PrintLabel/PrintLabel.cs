﻿//------------------------------------------------------------------------------
// Copyright © 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PrintLabel.cs
// 
//   DESCRIPTION: Print Label utilities
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 29-AUG-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-AUG-2011 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;
using System.Diagnostics;
using Microsoft.Win32;
using System.Windows.Forms;
using WSI.Common;
using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;
using ThoughtWorks.QRCode.Codec.Util;


namespace WSI.Labels
{
  //------------------------------------------------------------------------------
  // PURPOSE: Print label manager
  //
  //   NOTES:
  //
  public class PrintLabelManager
  {
    Int32 COMMON_MODULE_GUI_PLAYER_TRACKING = 16000;

    private int NumRows = 3;
    public int NumCols = 2;
    private bool[,] Excluded;
    private PointF[,] Location;
    private PointF ClientOffset;

    private SizeF LabelSize;
    private SizeF LabelClientSize;
    private SizeF BarcodeSize;

    public delegate void PrintProgressEventHandler(object sender, ObjectPanelEventArgs e);
    public event PrintProgressEventHandler PrintProgress;


    private PrintLabelSettings Settings;

    public List<PrintLabel> Labels = new List<PrintLabel>();
    public List<int> PendingToPrint = new List<int>();
    public Boolean CancelRequested = false;
    public Boolean PauseRequested = false;

    public int PageCount;
    public int CurrentPage;

    public int GLB_CurrentUser_GuiId;
    public int GLB_CurrentUser_Id;
    public string GLB_CurrentUser_Name;
    public string GLB_Computer_Name;

    Auditor m_auditor;
    Int32 m_current_user_id = 0;
    String m_current_user_name = "";
    String m_current_computer_name = "";

   
    
    float Scale = 1.0f / 25.4f * 100.0f;

    private PrintDocument PrintDocument;

    public void OnPrintProgress(object sender, ObjectPanelEventArgs e)
    {
      if (PrintProgress != null)
      {
        PrintProgress(sender, e);
      }
    }



    //------------------------------------------------------------------------------
    // PURPOSE: Creates a Print Label Manager
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - PrintLabelManager
    //
    //   NOTES:
    //
    public static PrintLabelManager Create()
    {
      PrintLabelManager  _plm;
      PrintLabelSettings _pls;

      _pls = PrintLabelSettings.DefaultSettings;
      _plm = new PrintLabelManager(_pls.LabelSize, _pls.Rows, _pls.Colums, _pls.PrintOrigin, 1.0f - _pls.Padding / 100);

      return _plm;    
    } // PrintLabelManager


    //------------------------------------------------------------------------------
    // PURPOSE: Creates a Print Label Manager
    //
    //  PARAMS:
    //      - INPUT:
    //        - LabelSize
    //        - NumRows
    //        - NumCols
    //        - Offset
    //        - ClientArea
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - PrintLabelManager
    //
    //   NOTES:
    //
    private PrintLabelManager(SizeF LabelSize, int NumRows, int NumCols, PointF Offset, float ClientArea)
    {
      Update(LabelSize, NumRows, NumCols, Offset, ClientArea);
    } // PrintLabelManager


    //------------------------------------------------------------------------------
    // PURPOSE: Updates the Print Label Manager settings
    //
    //  PARAMS:
    //      - INPUT:
    //        - LabelSize
    //        - NumRows
    //        - NumCols
    //        - Offset
    //        - ClientArea
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    private void Update(SizeF LabelSize, int NumRows, int NumCols, PointF Offset, float ClientArea)
    {
      this.NumRows = NumRows;
      this.NumCols = NumCols;
      this.LabelSize = LabelSize;
      this.LabelClientSize = new SizeF(LabelSize.Width * ClientArea, LabelSize.Height * ClientArea);
      this.BarcodeSize = new SizeF(this.LabelClientSize.Width * 0.80f, this.LabelClientSize.Height * 0.20f);

      this.Excluded = new bool[NumRows, NumCols];
      this.Location = new PointF[NumRows, NumCols];

      this.ClientOffset = new PointF(0.5f * (this.LabelSize.Width - this.LabelClientSize.Width), 0.5f * (this.LabelSize.Height - this.LabelClientSize.Height));
      //
      for (int _idx_row = 0; _idx_row < NumRows; _idx_row++)
      {
        for (int _idx_col = 0; _idx_col < NumCols; _idx_col++)
        {
          Location[_idx_row, _idx_col] = new PointF(Offset.X + LabelSize.Width * _idx_col, Offset.Y + LabelSize.Height * _idx_row);
        }
      }

      // Apply _scale
      this.PrintDocument = new PrintDocument();
      this.PrintDocument.PrintController = new StandardPrintController();
      this.PrintDocument.PrintPage += new PrintPageEventHandler(this.PrintPage);
    
    } // Update

    //------------------------------------------------------------------------------
    // PURPOSE: Print all the labels currently added to the Print Label Manager 
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //

    public void Print()
    {
      PrintLabelDialog _dlg;
      Int32 _nls_id;

      _dlg = new PrintLabelDialog();
      _dlg.Preload();
      this.Settings = _dlg.Settings;

      this.Update(this.Settings.LabelSize, this.Settings.Rows, this.Settings.Colums, this.Settings.PrintOrigin, 1.0f - this.Settings.Padding / 100); 

      for (int _idx_row = 0; _idx_row < NumRows; _idx_row++)
      {
        for (int _idx_col = 0; _idx_col < NumCols; _idx_col++)
        {
          Excluded[_idx_row, _idx_col] = this.Settings.LabelUsed[_idx_row, _idx_col];
        }
      }

      PendingToPrint.Clear();
      for (int _idx_lbl = 0; _idx_lbl < this.Labels.Count; _idx_lbl++)
      {
        for (int _idx_copy = 0; _idx_copy < this.Settings.Copies; _idx_copy++)
        {
          PendingToPrint.Add(_idx_lbl);
        }
      }

      //pages calculation
      int LabelsPerPage = this.Settings.Rows * this.Settings.Colums;
      int _pendingToPrint = PendingToPrint.Count - NumExcluded();
      PageCount = 1;
      PageCount = PageCount + (int)(Math.Ceiling(((decimal)(_pendingToPrint) / (decimal)LabelsPerPage)));
      CurrentPage = 0;

      _dlg.countPages = PageCount;
      _dlg.PendingToPrint = this.Labels.Count;

      if (_dlg.ShowDialog() == DialogResult.OK)
        {


          PageCount = _dlg.countPages;
        //nuevo bloque
          this.Settings = _dlg.Settings;

          this.Update(this.Settings.LabelSize, this.Settings.Rows, this.Settings.Colums, this.Settings.PrintOrigin, 1.0f - this.Settings.Padding / 100);

          for (int _idx_row = 0; _idx_row < NumRows; _idx_row++)
          {
            for (int _idx_col = 0; _idx_col < NumCols; _idx_col++)
            {
              Excluded[_idx_row, _idx_col] = this.Settings.LabelUsed[_idx_row, _idx_col];
            }
          }

          PendingToPrint.Clear();
          for (int _idx_lbl = 0; _idx_lbl < this.Labels.Count; _idx_lbl++)
          {
            for (int _idx_copy = 0; _idx_copy < this.Settings.Copies; _idx_copy++)
            {
              PendingToPrint.Add(_idx_lbl);
            }
          }

        //nuevo bloque

        this.PrintDocument.PrinterSettings.PrinterName = this.Settings.PrinterName;
        foreach (PaperSource _paper in this.PrintDocument.PrinterSettings.PaperSources)
        {
          if (_paper.SourceName == this.Settings.PrinterPaperSource)
          {
            this.PrintDocument.DefaultPageSettings.PaperSource = _paper;

            break;
          }
        }

        foreach (PaperSize _size in this.PrintDocument.PrinterSettings.PaperSizes)
        {
          if (_size.PaperName == this.Settings.PaperSize)
          {
            this.PrintDocument.DefaultPageSettings.PaperSize = _size;

            break;
          }
        }

        // AUDIT_CODE_PRINT_QR_CODES =53
        m_auditor = new Auditor(53);

        try
        {
          this.PrintDocument.Print();
        }
        catch 
        {  
          // No permisions to write the file ...
        }

        if (CancelRequested)
        {
          return;
        }

        _nls_id = 6955; // "Imprimir códigos QR de máquinas"

        m_auditor.SetName(COMMON_MODULE_GUI_PLAYER_TRACKING + _nls_id, "", "", "", "", "");
        m_auditor.Notify(ENUM_GUI.WIGOS_GUI, m_current_user_id, m_current_user_name, m_current_computer_name);

        if (this.NumAvailable() == 0)
        {
          this.ClearPageLayout();
        }
        for (int _idx_row = 0; _idx_row < NumRows; _idx_row++)
        {
          for (int _idx_col = 0; _idx_col < NumCols; _idx_col++)
          {
            this.Settings.LabelUsed[_idx_row, _idx_col] = Excluded[_idx_row, _idx_col];
          }
        }

        this.Settings.Save();
        this.Labels.Clear();
      }
    } // Print

    //------------------------------------------------------------------------------
    // PURPOSE: Print all the labels currently added to the Print Label Manager 
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //

    public PrintLabel CreatePrintLabel()
    {
      return new PrintLabel(this);
    } // CreatePrintLabel


    //------------------------------------------------------------------------------
    // PURPOSE: Makes all positions available on the sheet
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    internal void ClearPageLayout()
    {
      for (int _idx_row = 0; _idx_row < NumRows; _idx_row++)
      {
        for (int _idx_col = 0; _idx_col < NumCols; _idx_col++)
        {
          Excluded[_idx_row, _idx_col] = false;
        }
      }
    } // ClearPageLayout


    //------------------------------------------------------------------------------
    // PURPOSE: Compute the number of available positions on the sheet
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    internal int NumAvailable()
    {
      int _available;
      _available = 0;
      for (int _idx_row = 0; _idx_row < NumRows; _idx_row++)
      {
        for (int _idx_col = 0; _idx_col < NumCols; _idx_col++)
        {
          if  (!Excluded[_idx_row, _idx_col] )
          {
            _available++;
          }
        }
      }
      return _available;
    } // NumAvailable

    //------------------------------------------------------------------------------
    // PURPOSE: Excludes the position from the sheet
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    internal void PageLayoutExclude(int Row, int Column)
    {
      Excluded[Row, Column] = true;
    } // PageLayoutExclude

    //------------------------------------------------------------------------------
    // PURPOSE: SetSize
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //

    internal void SetSize(PrintLabel Label)
    {
      Label.LabelSize = new SizeF(this.LabelClientSize.Width * this.Scale, this.LabelClientSize.Height * this.Scale);
      Label.BarcodeSize = new SizeF(this.BarcodeSize.Width * this.Scale, this.BarcodeSize.Height * this.Scale);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Adds the label to the list of labels to print
    //
    //  PARAMS:
    //      - INPUT:
    //        - Label
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //

    public void Add(PrintLabel Label)
    {
      if (Label.Manager != this)
      {
        throw new Exception("The label was not created by this manager.");
      }
      this.Labels.Add(Label);
    } // Add


    //------------------------------------------------------------------------------
    // PURPOSE: Prints a page
    //
    //  PARAMS:
    //      - INPUT:
    //        - Label
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //

    private void PrintPage(object sender, PrintPageEventArgs e)
    {
      Graphics _g;
      GraphicsState _state;
      Int32 _nls_id;

      _nls_id = 6991; // "Nombre: %1 Asset Number: %2"
      _g = e.Graphics;

      float _dpiX;
      float _dpiY;
      float _dpMX;
      float _dpMY;

      _dpiX = _g.DpiX;
      _dpiY = _g.DpiY;
      _dpMX = _dpiX / 25.4f;
      _dpMY = _dpiY / 25.4f;

      _dpiX = _g.DpiX;
      _dpiY = _g.DpiY;
      _dpMX = 100;
      _dpMY = 100;

      float _x;
      float _y;

      if (NumAvailable() <= 0)
      {
        ClearPageLayout();
      }


      if (PrintProgress != null)
      {

        while (CancelRequested || PauseRequested )
        {
          if (CancelRequested)
          {
            return;
          }
          int tick0 = Misc.GetTickCount();
          while (Misc.GetElapsedTicks(tick0) < 100)
          {
            System.Threading.Thread.Sleep(10);
            Application.DoEvents();
          }
        }
        
        CurrentPage++;  
        PrintProgress(this, new ObjectPanelEventArgs(null));
      }

      for (int _idx_row = 0; _idx_row < NumRows; _idx_row++)
      {
        for (int _idx_col = 0; _idx_col < NumCols; _idx_col++)
        {
          if (Excluded[_idx_row, _idx_col])
          {
            continue;
          }

          if (PendingToPrint.Count == 0)
          {
            continue;
          }

          PrintLabel _label;

          int _idx_label;

          _idx_label = PendingToPrint[0];
          PendingToPrint.Remove(_idx_label);
          _label = Labels[_idx_label];

          Excluded[_idx_row, _idx_col] = true;

          // Print the label on the current position
          _state = _g.Save();
          _x = Location[_idx_row, _idx_col].X * this.Scale;
          _y = Location[_idx_row, _idx_col].Y * this.Scale;

          _g.TranslateTransform(_x, _y);
          //_g.DrawRectangle(Pens.Black, 0f, 0f, this.LabelSize.Width * this.Scale, this.LabelSize.Height * this.Scale);
          _g.TranslateTransform(this.ClientOffset.X * this.Scale, this.ClientOffset.Y * this.Scale);

          //_g.DrawRectangle(Pens.Gray, 0f, 0f, this.LabelClientSize.Width * this.Scale, this.LabelClientSize.Height * this.Scale);

          _label.Print(_g);
          m_auditor.SetField(COMMON_MODULE_GUI_PLAYER_TRACKING + _nls_id, _label.Name, _label.AssetNumber, "", "", "");
          
          m_current_user_id =_label.CurrentUser_Id;
          m_current_user_name =_label.CurrentUser_Name;
          m_current_computer_name =_label.Computer_Name;

          _g.Restore(_state);
        }
      }


      e.HasMorePages = (PendingToPrint.Count > 0);
    } // PrintPage

    private int NumExcluded()
    {
      int _count;

      _count = 0;
      for (int _row = 0; _row < NumRows; _row++)
      {
        for (int _col = 0; _col < NumCols; _col++)
        {
          if ( Excluded[_row, _col])
          {
            _count++;
          }
        }
      }

      return _count;
    }

    private int NumTotal()
    {
      int _count;

      _count = 0;
      for (int _row = 0; _row < NumRows; _row++)
      {
        for (int _col = 0; _col < NumCols ; _col++)
        {
          _count++;
        }
      }

      return _count;
    }




  } // PrintLabelManager

  //------------------------------------------------------------------------------
  // PURPOSE: Print Label class
  //
  //   NOTES:
  //
  public class PrintLabel
  {
    internal String Barcode { get; private set; }
    internal String BarcodeText { get; private set; }
    internal SizeF BarcodeSize;
    internal SizeF LabelSize;
    internal  PrintLabelManager Manager;

    public String QR_Code { get; set; }
    public string QR_Footer { get; set; }
    public string AssetNumber { get; set; }
    public string Name { get; set; }

    //Manager.GLB_CurrentUser_Id, Manager.GLB_CurrentUser_Name, Manager.GLB_Computer_Name

    public int CurrentUser_Id
    {
      get
      {
        return Manager.GLB_CurrentUser_Id;
      }
    }
    public string CurrentUser_Name
    {
      get
      {
        return Manager.GLB_CurrentUser_Name;
      }
    }
    public string Computer_Name
    {
      get
      {
        return Manager.GLB_Computer_Name;
      }
    }

    internal class Tupla
    {
      public float _float ;
      public FontStyle _fontStyle;
      public string _string;
      public Tupla (float __float, FontStyle __fontStyle, string __string)
      {
        _float = __float;
        _fontStyle = __fontStyle;
        _string = __string;
      }
    }




    //private List<Tuple<float, FontStyle, String>> m_lines = new List<Tuple<float, FontStyle, string>>();
    private List<Tupla> m_lines = new List<Tupla>();

    public override string ToString()
    {
      String _str;

      _str = BarcodeText;
      if (WSI.Common.Misc.IsNullOrWhiteSpace(_str))
      {
        _str = Barcode; 
      }
      if (WSI.Common.Misc.IsNullOrWhiteSpace(_str))
      {
        _str = " No Barcode "; 
      }

      return _str;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Adds a text line to the label
    //
    //  PARAMS:
    //      - INPUT:
    //        - Style
    //        - FontSize
    //        - Text
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    public void WriteLine(float FontSize, FontStyle Style, String Text)
    {
      m_lines.Add(new Tupla(FontSize, Style, Text));
    } // WriteLine

    //------------------------------------------------------------------------------
    // PURPOSE: Creates a Print Label
    //
    //  PARAMS:
    //      - INPUT:
    //        - LabelSize
    //        - BarcodeSize
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    internal PrintLabel(SizeF LabelSize, SizeF BarcodeSize)
    {
      this.LabelSize = LabelSize;
      this.BarcodeSize = BarcodeSize;
    } // PrintLabel

    //------------------------------------------------------------------------------
    // PURPOSE: Creates a Print Label related with a PrintLabelManager
    //
    //  PARAMS:
    //      - INPUT:
    //        - Manager
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //

    internal PrintLabel(PrintLabelManager Manager)
    {
      this.Manager = Manager;
    } // PrintLabel

    //------------------------------------------------------------------------------
    // PURPOSE: Set the Barcode to the Print Label 
    //
    //  PARAMS:
    //      - INPUT:
    //        - Barcode
    //        - BarcodeText
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    public void SetBarcode(String Barcode, String BarcodeText)
    {
      this.Barcode = Barcode;
      this.BarcodeText = BarcodeText;
    } // SetBarcode

    //------------------------------------------------------------------------------
    // PURPOSE: Prints the Label on the given Graphics object
    //
    //  PARAMS:
    //      - INPUT:
    //        - G
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    internal void Print(System.Drawing.Graphics G)
    {

      SizeF _size;
      SizeF _size2;
      Font _font;
      float _y;
      float _th;
      SizeF _aux;
      float _fontsize;
      String _footerLine1;
      String _footerLine2;


      //Image newImage = Image.FromFile("C://Jorge Concheyro//winsystems//05 Reemplazo componentes//Wigos Cashier - Icons//icons//options//Terminal.png");

      


      if (this.Manager != null)
      {
        this.Manager.SetSize(this);
      }

      G.Clip = new Region(new RectangleF(0, 0, this.LabelSize.Width, this.LabelSize.Height));

      _y = 0;

      if (!String.IsNullOrEmpty(this.BarcodeText))
      {
        _font = new Font("Verdana", 10, FontStyle.Bold, GraphicsUnit.Point);
        _size = G.MeasureString(this.BarcodeText, _font);
        G.DrawString(this.BarcodeText, _font, Brushes.Black, (this.LabelSize.Width - _size.Width) / 2, _y);
        _y += _size.Height;
      }

      QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
      qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
      qrCodeEncoder.QRCodeScale = 1;
      qrCodeEncoder.QRCodeVersion = 7;
      qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;


      if (!String.IsNullOrEmpty(this.QR_Code))
      {
        // Proportional Printing. 5 posible columns 5 posible sizes.
        //switch (this.Manager.NumCols)
        //{
        //  case 5: qrCodeEncoder.QRCodeScale = 1;
        //    break;
        //  case 4: qrCodeEncoder.QRCodeScale = 2;
        //    break;
        //  case 3: qrCodeEncoder.QRCodeScale = 3;
        //    break;
        //  case 2: qrCodeEncoder.QRCodeScale = 4;
        //    break;
        //  case 1: qrCodeEncoder.QRCodeScale = 5;
        //    break;
        //}
        
        Bitmap image = qrCodeEncoder.Encode(this.QR_Code);
        qrCodeEncoder.QRCodeScale = 5;
        image = qrCodeEncoder.Encode(this.QR_Code);

        while ((image.Width / this.LabelSize.Width * 100f > 80f) || (image.Height  / this.LabelSize.Height * 100f > 60f))
        {
          qrCodeEncoder.QRCodeScale = qrCodeEncoder.QRCodeScale -1 ;
          image = qrCodeEncoder.Encode(this.QR_Code);
        }

        G.DrawImage(image, (this.LabelSize.Width - image.Width) / 2, _y);
        _y += image.Height;

        _y = _y + 12; //making some room between the QR code and the botton line

        _fontsize = 14;
        if (this.QR_Footer.IndexOf("\\n") > 0) 
        {
          _footerLine1 = this.QR_Footer.Substring(0, this.QR_Footer.IndexOf("\\n"));
          _footerLine2 = this.QR_Footer.Substring(this.QR_Footer.IndexOf("\\n") + 2);

          _font = new Font("Verdana", _fontsize, FontStyle.Bold, GraphicsUnit.Point);
          _size = G.MeasureString(_footerLine1, _font);

          //line 1
          // Proportional printing: start with a big font size, and make it smaller until it fits in the label
          while (_size.Width > this.LabelSize.Width)
          {
            _fontsize = _fontsize - (0.5f);
            _font = new Font("Verdana", _fontsize, FontStyle.Bold, GraphicsUnit.Point);
            _size = G.MeasureString(_footerLine1, _font);
          }

          // line 2
          _size2 = G.MeasureString(_footerLine2, _font);
          while (_size2.Width > this.LabelSize.Width)
          {
            _fontsize = _fontsize - (0.5f);
            _font = new Font("Verdana", _fontsize, FontStyle.Bold, GraphicsUnit.Point);
            _size2 = G.MeasureString(_footerLine2, _font);
          }

          if (_fontsize > 6)
          {
            G.DrawString(_footerLine1, _font, Brushes.Black, (this.LabelSize.Width - _size.Width) / 2, _y);
            _y = _y + _size.Height + 10;
            G.DrawString(_footerLine2, _font, Brushes.Black, (this.LabelSize.Width - _size2.Width) / 2, _y);
          }


        } 
        else
        { 
          _font = new Font("Verdana", _fontsize, FontStyle.Bold, GraphicsUnit.Point);
          _size = G.MeasureString(this.QR_Footer, _font);

          // Proportional printing: start with a big font size, and make it smaller until it fits in the label
          while (_size.Width > this.LabelSize.Width )
          {
            _fontsize = _fontsize - (0.5f);
            _font = new Font("Verdana", _fontsize, FontStyle.Bold, GraphicsUnit.Point); 
            _size = G.MeasureString(this.QR_Footer, _font);
          }

          if (_fontsize > 6 )
          { 
            G.DrawString(this.QR_Footer, _font, Brushes.Black, (this.LabelSize.Width - _size.Width) / 2, _y);
          }

        }
      }
      
      //if (!String.IsNullOrEmpty(this.QR_Code))
      //{
      // QrEncoder _qr_encoder;
      // QrCode _qr_code; 


        
      //_qr_encoder = new Gma.QrCodeNet.Encoding.QrEncoder(Gma.QrCodeNet.Encoding.ErrorCorrectionLevel.L); //TODO No sé el significado de esta sentencia
      //_qr_code = new Gma.QrCodeNet.Encoding.QrCode();
      //_qr_code = _qr_encoder.Encode("CCCCCCCCCCCCCCCCCpoijwpoijfpOIJWEPOFIJCXLKJVPIOJIJASDKJFEFNAIJRFAKNDFAOIERJAKMVOIOAREIMTGÑAOEMVÑOAERNGÑOAIJVTOJASETRGCC");

      //_temp_bmp = new Bitmap(_qr_code.Matrix.Width, _qr_code.Matrix.Height);


      //  for (int _x = 0; _x <= _qr_code.Matrix.Width - 1; _x++)
      //  {
      //    for (int _yy = 0; _yy <= _qr_code.Matrix.Height - 1; _yy++)
      //    {
      //      if ( _qr_code.Matrix.InternalArray[_x, _yy] ) 
      //      {
      //        _temp_bmp.SetPixel(_x, _yy, Color.Black); 
      //      }
      //      else
      //      {
      //        _temp_bmp.SetPixel(_x, _yy, Color.White);
      //      }
      //    }
      //  }
      //  G.DrawImage(_temp_bmp, 0, 0);
      //}



      // Is there any text line?
      if (m_lines.Count == 0)
      {
        return;
      }

      _th = this.LabelSize.Height - _y;
      G.TranslateTransform(0, _y);

      // Measure Strings ...
      _y = 0;
      _size = new SizeF();
      foreach (Tupla _line in m_lines)
      {
        _aux = new SizeF(this.LabelSize.Width, _th - _y);
        _font = new Font("Verdana", _line._float, _line._fontStyle, GraphicsUnit.Point);
        _size = G.MeasureString(_line._string, _font, _aux);
        _y += _size.Height;
      }
      _y += _size.Height / 2; // Add half height ...

      // Draw Strings
      if (_y > _th)
      {
        // Scale Height
        G.ScaleTransform(1.0f, _th / _y);
      }
      _aux = new SizeF(this.LabelSize.Width, _y);
      _y = 0;
      foreach (Tupla _line in m_lines)
      {
        _font = new Font("Verdana", _line._float , _line._fontStyle, GraphicsUnit.Point);
        _size = G.MeasureString(_line._string , _font, _aux);
        G.DrawString(_line._string , _font, Brushes.Black, new RectangleF(0.0f, _y, _aux.Width, _aux.Height));
        _y += _size.Height;
      }
    } // Print
  } // Print Label

  //------------------------------------------------------------------------------
  // PURPOSE: PrintLabelSettings
  //
  //   NOTES:
  //
  internal class PrintLabelSettings
  {
    public int Rows { get; private set; }
    public int Colums { get; private set; }
    public bool[,] LabelUsed { get; internal set; }
    public SizeF LabelSize { get; internal set; }
    public float Padding { get; internal set; }
    public String PrinterName { get; internal set; }
    public String PrinterPaperSource { get; internal set; }
    public String PaperSize { get; internal set; }
    public PointF PrintOrigin { get; internal set; }
    public int Copies { get; internal set; }
    public int DefaultCopies { get; internal set; }

    //------------------------------------------------------------------------------
    // PURPOSE: Sets the Layout
    //
    //  PARAMS:
    //      - INPUT:
    //        - RowCount
    //        - ColumnCount
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    internal void SetLayout(int RowCount, int ColumnCount)
    {
      Rows = RowCount;
      Colums = ColumnCount;
      LabelUsed = new bool[Rows, Colums];
    } // SetLayout

    //------------------------------------------------------------------------------
    // PURPOSE: Initial settings
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - PrintLabelSettings
    //
    //   NOTES:
    //
    internal static PrintLabelSettings InitialSettings
    {
      get
      {
        PrintLabelSettings _dflt;

        _dflt = new PrintLabelSettings();

        _dflt.SetLayout(3, 2);
        _dflt.LabelSize = new SizeF(105f, 99f);
        _dflt.Padding = 10.0f;
        _dflt.PrintOrigin = new PointF(0, 0);
        _dflt.PrinterName = "";
        _dflt.PrinterPaperSource = "";
        _dflt.PaperSize = "A4";
        _dflt.DefaultCopies = 1;
        _dflt.Copies = _dflt.DefaultCopies;

        try
        {
          PrintDocument _doc;
          _doc = new PrintDocument();
          _dflt.PrinterName = _doc.PrinterSettings.PrinterName;
          _dflt.PrinterPaperSource = _doc.PrinterSettings.PaperSources[0].SourceName;
        }
        catch
        {
        }

        return _dflt;
      }
    } // InitialSettings

    //------------------------------------------------------------------------------
    // PURPOSE: Default settings
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - PrintLabelSettings
    //
    //   NOTES:
    //
    public static PrintLabelSettings DefaultSettings
    {
      get
      {
        PrintLabelSettings _dflt;

        _dflt = new PrintLabelSettings();
        if (!_dflt.Load())
        {
          _dflt = PrintLabelSettings.InitialSettings;
        }

        return _dflt;
      }
    } // DefaultSettings


    //------------------------------------------------------------------------------
    // PURPOSE: Save settings
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    public void Save()
    {
      Byte[] _raw;
      Byte[] _itm;
      Byte[] _txt;
      int _idx;

      _raw = new Byte[2048];
      _idx = 0;
      // Record size
      _itm = BitConverter.GetBytes(_raw.Length);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // Rows
      _itm = BitConverter.GetBytes(Rows);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;


      // Columns
      _itm = BitConverter.GetBytes(Colums);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // Size.Width
      _itm = BitConverter.GetBytes(LabelSize.Width);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // Size.Height
      _itm = BitConverter.GetBytes(LabelSize.Height);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // Padding
      _itm = BitConverter.GetBytes(Padding);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // Origin.X
      _itm = BitConverter.GetBytes(PrintOrigin.X);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // Origin.Y
      _itm = BitConverter.GetBytes(PrintOrigin.Y);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;

      // Copies
      _itm = BitConverter.GetBytes(DefaultCopies);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;


      // PrintName.Length
      _txt = Encoding.Unicode.GetBytes(PrinterName);
      _itm = BitConverter.GetBytes(_txt.Length);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // PrintName.Value
      _itm = _txt;
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // PrinterPaperSource.Length
      _txt = Encoding.Unicode.GetBytes(PrinterPaperSource);
      _itm = BitConverter.GetBytes(_txt.Length);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // PrinterPaperSource.Value
      _itm = _txt;
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;

      // PaperSize.Length
      _txt = Encoding.Unicode.GetBytes(PaperSize);
      _itm = BitConverter.GetBytes(_txt.Length);
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
      // PaperSize.Value
      _itm = _txt;
      Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;


      // LabelUsed
      for (int i = 0; i < Rows; i++)
      {
        for (int j = 0; j < Colums; j++)
        {
          _itm = BitConverter.GetBytes(LabelUsed[i, j]);
          Array.Copy(_itm, 0, _raw, _idx, _itm.Length); _idx += _itm.Length;
        }
      }


      Registry.CurrentUser.SetValue(@"PrintLabelSettings\000", _raw, RegistryValueKind.Binary);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load settings
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //

    private Boolean Load()
    {
      try
      {
        Byte[] _raw;
        int _idx;
        int _l;
        int _m;
        float _d0;
        float _d1;

        _raw = (Byte[])Registry.CurrentUser.GetValue(@"PrintLabelSettings\000");
        _idx = 0;
        // Record size
        _l = BitConverter.ToInt32(_raw, _idx); _idx += 4;
        if (_l != 2048)
        {
          return false;
        }
        // Rows
        _l = BitConverter.ToInt32(_raw, _idx); _idx += 4;
        _m = BitConverter.ToInt32(_raw, _idx); _idx += 4;
        SetLayout(_l, _m);
        // Label size
        _d0 = BitConverter.ToSingle(_raw, _idx); _idx += 4;
        _d1 = BitConverter.ToSingle(_raw, _idx); _idx += 4;
        LabelSize = new SizeF(_d0, _d1);
        // Padding
        Padding = BitConverter.ToSingle(_raw, _idx); _idx += 4;

        // Origin.X
        _d0 = BitConverter.ToSingle(_raw, _idx); _idx += 4;
        _d1 = BitConverter.ToSingle(_raw, _idx); _idx += 4;
        PrintOrigin = new PointF(_d0, _d1);

        // Copies
        DefaultCopies = BitConverter.ToInt32(_raw, _idx); _idx += 4;
        DefaultCopies = Math.Max(1, DefaultCopies);
        Copies = DefaultCopies;

        // PrintName
        _l = BitConverter.ToInt32(_raw, _idx); _idx += 4;
        PrinterName = Encoding.Unicode.GetString(_raw, _idx, _l); _idx += _l;
        // PrinterPaperSource
        _l = BitConverter.ToInt32(_raw, _idx); _idx += 4;
        PrinterPaperSource = Encoding.Unicode.GetString(_raw, _idx, _l); _idx += _l;

        // PaperSize
        _l = BitConverter.ToInt32(_raw, _idx); _idx += 4;
        PaperSize = Encoding.Unicode.GetString(_raw, _idx, _l); _idx += _l;
        if (WSI.Common.Misc.IsNullOrWhiteSpace(PaperSize))
        {
          PaperSize = "A4";
        }

        // LabelUsed
        for (int i = 0; i < Rows; i++)
        {
          for (int j = 0; j < Colums; j++)
          {
            LabelUsed[i, j] = BitConverter.ToBoolean(_raw, _idx); _idx += 1;
          }
        }

        return true;
      }
      catch (Exception)
      {
        return false;
      }
    } // Load
  } // PrintLabelSettings
}

