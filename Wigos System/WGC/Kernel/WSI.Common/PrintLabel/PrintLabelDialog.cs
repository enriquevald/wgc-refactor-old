﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using WSI.Common;   



namespace WSI.Labels
{
  internal partial class PrintLabelDialog : Form
  {
    private float m_scale = 0.5f;
    private int num_rows = 4;
    private int num_cols = 2;
    private SizeF label_size = new SizeF(105.0f, 296.0f / 4);
    private bool [,]excluded;//  = new bool [num_rows, num_cols];

    private bool mouse_down = false;
    private int num_changes = 0;
    private bool excluding = true;

    public int countPages = 0;
    public int PendingToPrint = 0;

    PrintLabelSettings m_settings;
    PrintLabelSettings m_user_settings = null;

    public PrintLabelSettings Settings
    {
      get
      {
        return m_user_settings;
      }
    }

    public PrintLabelDialog()
    {
      InitializeComponent();

      this.btn_reset.Text = Resource.String("PRINT_LABEL_RESET");//"Reset";

      this.btn_settings.Text = Resource.String("PRINT_LABEL_PREFERENCES") + "...";//"Preferences ...";
      this.btn_print.Text = Resource.String("PRINT_LABEL_PRINT");//"Print";
      this.btn_close.Text = Resource.String("PRINT_LABEL_CLOSE");//"Close";

      this.llb_printer_title.Text = Resource.String("PRINT_LABEL_PRINTER") + ":";//"Printer:";
      this.lbl_printer.Text = Resource.String("PRINT_LABEL_PRINTER") + ":";//"Printer:";
      this.lbl_paper_source.Text = Resource.String("PRINT_LABEL_PRINTER") + ":";//"Printer:";

      this.label2.Text = Resource.String("PRINT_LABEL_PAPER_SOURCE") + ":";//"Paper source:";
      this.label1.Text = Resource.String("PRINT_LABEL_FIRST_PAGE") + ":";//"First page:";

      this.label3.Text = Resource.String("PRINT_LABEL_AVAILABLE_LABELS");//"The picture indicates the available labels on the first page. Click over the labels to toggle between available and already used.";
      this.groupBox3.Text = Resource.String("PRINT_LABEL_COPIES");//"Copies";
      this.label19.Text = Resource.String("PRINT_LABEL_NUMBER_OF_COPIES") + ":";//"Number of copies:";

      this.Text = Resource.String("PRINT_LABEL_FORM_TEXT");//"Print Label";
      excluded = new bool [num_rows, num_cols];

    }

    ////////public PrintLabelManager CreateLabels()
    ////////{
    ////////  Labels.PrintLabelManager.PrintLabel _lbl;

    ////////  PrintLabelManager _plm;

    ////////  //_plm = new PrintLabelManager(new SizeF(105.0f, 98.67f), 3, 2, new PointF(), 0.90f);
    ////////  _plm = new PrintLabelManager(new SizeF(105.0f, 74.00f), 4, 2, new PointF(-5.0f, -5.0f), 0.90f);
    ////////  //_plm = new PrintLabelManager(new SizeF(105.0f, 59.20f), 5, 2, new PointF(), 0.90f);


    ////////  _lbl = _plm.CreatePrintLabel();
    ////////  _lbl.SetBarcode("12345678ABCD", "1234-5678-ABCD");

    ////////  _lbl.WriteLine(9, FontStyle.Regular, "ADRESSEE:");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "Стоимость СЕТА лотерейного билета");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "Билет состоит из 2-х частей (СЕТ лотерейных билетов), объединенных перфорационной линией по горизонтали. ");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "65th Enrique Granados");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "Barcelona");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "08008");

    ////////  _lbl.WriteLine(9, FontStyle.Regular, " ");
    ////////  _lbl.WriteLine(9, FontStyle.Regular, "SENDER:");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "DISTRIBUTOR 100");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "Attn: Vladimir Putov");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "65th Enrique Granados");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "Barcelona");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "08008");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "RUSSIAN FEDERATION");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "RUSSIAN FEDERATION - 1");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "RUSSIAN FEDERATION - 2");

    ////////  _plm.Add(_lbl);

    ////////  //
    ////////  //
    ////////  _lbl = _plm.CreatePrintLabel();
    ////////  _lbl.SetBarcode("123412341234", "9876-5432-1234");

    ////////  _lbl.WriteLine(9, FontStyle.Regular, "ADRESSEE:");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "Стоимость СЕТА лотерейного билета");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "Билет состоит из 2-х частей (СЕТ лотерейных билетов), объединенных перфорационной линией по горизонтали. ");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "65th Enrique Granados");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "Barcelona");
    ////////  _lbl.WriteLine(9, FontStyle.Bold, "08008");

    ////////  _lbl.WriteLine(9, FontStyle.Regular, " ");
    ////////  _lbl.WriteLine(9, FontStyle.Regular, "SENDER:");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "DISTRIBUTOR 100");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "Attn: Vladimir Putov");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "65th Enrique Granados");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "Barcelona");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "08008");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "RUSSIAN FEDERATION");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "RUSSIAN FEDERATION - 1");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, "RUSSIAN FEDERATION - 2");
    ////////  _lbl.WriteLine(8, FontStyle.Regular, " ");


    ////////  _plm.Add(_lbl);


    ////////  ////////int _ii = 1;

    ////////  ////////// 1
    ////////  ////////_lbl = _plm.CreatePrintLabel();
    ////////  ////////_lbl.SetBarcode(_ii.ToString("00000000"), _ii.ToString("00000000"));
    ////////  ////////_plm.Add(_lbl);
    ////////  ////////_ii++;
    ////////  ////////// 2
    ////////  ////////_lbl = _plm.CreatePrintLabel();
    ////////  ////////_lbl.SetBarcode(_ii.ToString("00000000"), _ii.ToString("00000000"));
    ////////  ////////_plm.Add(_lbl);
    ////////  ////////_ii++;
    ////////  ////////// 3
    ////////  ////////_lbl = _plm.CreatePrintLabel();
    ////////  ////////_lbl.SetBarcode(_ii.ToString("00000000"), _ii.ToString("00000000"));
    ////////  ////////_plm.Add(_lbl);
    ////////  ////////_ii++;
    ////////  ////////// 4
    ////////  ////////_lbl = _plm.CreatePrintLabel();
    ////////  ////////_lbl.SetBarcode(_ii.ToString("00000000"), _ii.ToString("00000000"));
    ////////  ////////_plm.Add(_lbl);
    ////////  ////////_ii++;
    ////////  ////////// 5
    ////////  ////////_lbl = _plm.CreatePrintLabel();
    ////////  ////////_lbl.SetBarcode(_ii.ToString("00000000"), _ii.ToString("00000000"));
    ////////  ////////_plm.Add(_lbl);
    ////////  ////////_ii++;
    ////////  ////////// 6
    ////////  ////////_lbl = _plm.CreatePrintLabel();
    ////////  ////////_lbl.SetBarcode(_ii.ToString("00000000"), _ii.ToString("00000000"));
    ////////  ////////_plm.Add(_lbl);
    ////////  ////////_ii++;

    ////////  //_lbl = _plm.CreatePrintLabel();
    ////////  //_lbl.SetBarcode(_ii.ToString("00000000"), _ii.ToString("00000000"));
    ////////  //_plm.Add(_lbl);
    ////////  //_ii++;
    ////////  //_lbl = _plm.CreatePrintLabel();
    ////////  //_lbl.SetBarcode(_ii.ToString("00000000"), _ii.ToString("00000000"));
    ////////  //_plm.Add(_lbl);
    ////////  //_ii++;





    ////////  return _plm;


    ////////  //printPreviewDialog2.Document = _plm.Document();


    ////////  //_plm.PageLayoutExclude(0, 1);
    ////////  //_plm.PageLayoutExclude(1, 0);
    ////////  //_plm.PageLayoutExclude(2, 1);

    ////////  _plm.Document().Print();

    ////////}

    private void panel1_Paint(object sender, PaintEventArgs e)
    {
      float _scale;
      _scale = m_scale;
      int _id;

      e.Graphics.ScaleTransform(_scale, _scale);

      _id = 1;
      for (int _row = 0; _row < num_rows; _row++)
      {
        for (int _col = 0; _col < num_cols; _col++)
        {
          Brush _brush;

          _brush = excluded[_row, _col]?Brushes.LightGray:Brushes.White;
          e.Graphics.FillRectangle(_brush, _col * label_size.Width, _row * label_size.Height, label_size.Width, label_size.Height);

          if (excluded[_row, _col])
          {
            e.Graphics.DrawLine(Pens.Red, _col * label_size.Width, _row * label_size.Height, (_col + 1) * label_size.Width, (_row + 1) * label_size.Height);
            e.Graphics.DrawLine(Pens.Red, _col * label_size.Width, (_row + 1) * label_size.Height, (_col + 1) * label_size.Width, _row * label_size.Height);
          }
          else
          {
            e.Graphics.DrawString(_id.ToString(), new Font("Verdana", 16), Brushes.DarkGreen, (_col +0.1f) * label_size.Width, (_row+0.1f) * label_size.Height);
            _id++;
          
          }
          e.Graphics.DrawRectangle(Pens.Black, _col * label_size.Width, _row * label_size.Height, label_size.Width, label_size.Height);


        }
      }
    }



    private void btn_reset_Click(object sender, EventArgs e)
    {
      for (int _row = 0; _row < num_rows; _row++)
      {
        for (int _col = 0; _col < num_cols; _col++)
        {
          excluded[_row, _col] = false;
        }
      }
      UD_copies.Value = 1;
      RefreshPreview();
    }



    private void panel1_MouseDown(object sender, MouseEventArgs e)
    {
      if (e.Button == System.Windows.Forms.MouseButtons.None)
      {
        return;
      }
      
      int _row;
      int _col;

      if (!CheckXY(e, out _row, out _col))
      {
        return;
      }

      excluding = !excluded[_row, _col];
      mouse_down = true;
      num_changes = 0;
      panel1_MouseMove(sender, e);
    }




    private bool CheckXY (MouseEventArgs e, out int Row, out int Col)
    {
      float _x;
      float _y;

      _x = e.X * (1.0f / m_scale);
      _y = e.Y * (1.0f / m_scale);

      Row = -1;
      Col = -1;

      for (int _row = 0; _row < num_rows; _row++)
      {
        for (int _col = 0; _col < num_cols; _col++)
        {
          RectangleF _rect;

          _rect = new RectangleF(_col * label_size.Width, _row * label_size.Height, label_size.Width, label_size.Height);

          if (_rect.Left <= _x && _x < _rect.Right)
          {
            if (_rect.Top <= _y && _y < _rect.Bottom)
            {
              Row = _row;
              Col = _col;

              return true;
            }
          }
        }
      }

      return false;





    }

    private int NumExcluded()
    {
      int _count;

      _count = 0;
      for (int _row = 0; _row < num_rows; _row++)
      {
        for (int _col = 0; _col < num_cols; _col++)
        {
          if (excluded[_row, _col])
          {
            _count++;
          }
        }
      }

      return _count;
    }

    private int NumTotal()
    {
      int _count;

      _count = 0;
      for (int _row = 0; _row < num_rows; _row++)
      {
        for (int _col = 0; _col < num_cols; _col++)
        {
          _count++;
        }
      }

      return _count;
    }



    private void panel1_MouseMove(object sender, MouseEventArgs e)
    {
      if (!mouse_down)
      {
        return;
      }

      int _row;
      int _col;


      if ( CheckXY(e, out _row, out _col) )
      {
        if (excluded[_row, _col] != excluding)
        {
          excluded[_row, _col] = excluding;

          if (NumExcluded() == NumTotal())
          { 
            // All excluded ...
            excluded[_row, _col] = !excluding;

            return;
          }

          num_changes++;
          RefreshPreview();
        }
      }
    }

    private void panel1_MouseUp(object sender, MouseEventArgs e)
    {
      mouse_down = false;
      if (num_changes > 0)
      {
        RefreshPreview();
      }
    }

    private void RefreshPreview()
    {
      btn_reset.Enabled = (NumExcluded() > 0);
      this.panel1.Refresh();
      Refresh_Pages();
    }


    private void panel1_MouseEnter(object sender, EventArgs e)
    {
      mouse_down = false;
    }

    private void panel1_MouseLeave(object sender, EventArgs e)
    {
      mouse_down = false;
    }

    private void btn_settings_Click(object sender, EventArgs e)
    {
      PrintLabelSettingsDialog _dlg;
      _dlg = new PrintLabelSettingsDialog();
      _dlg.SetCopies(this.UD_copies.Value);

      DialogResult _dlg_result = _dlg.ShowDialog();

      if (_dlg_result == DialogResult.OK)
      {
        PrintLabelSettings x_settings = PrintLabelSettings.InitialSettings;
        this.UD_copies.Value = x_settings.Copies;
        
      }
      // Refresh, settings could be changed (without 'OK' result)
      Refresh();
    }


    private void Form2_Load(object sender, EventArgs e)
    {
      Refresh();
      RefreshPreview();
    }


    public void Preload()
    {
      m_user_settings = PrintLabelSettings.DefaultSettings;
    }



    public override void Refresh() 
    {
      
      m_settings = PrintLabelSettings.DefaultSettings;

      this.lbl_printer.Text = m_settings.PrinterName;
      this.lbl_paper_source.Text = m_settings.PrinterPaperSource;
      this.num_rows = m_settings.Rows;
      this.num_cols = m_settings.Colums;
      this.label_size = m_settings.LabelSize;
      this.excluded  = new bool [num_rows, num_cols];
      this.UD_copies.Value = (int)m_settings.Copies;

      for (int _row = 0; _row < num_rows; _row++)
      {
        for (int _col = 0; _col < num_cols; _col++)
        {
          excluded[_row, _col] = m_settings.LabelUsed[_row, _col];
        }
      }

      float _sx = 210.0f / ( this.num_cols * this.label_size.Width);
      float _sy = 297.0f / (this.num_rows * this.label_size.Height);
      _sx = Math.Max(_sx, _sy);
      m_scale = (1 / _sx)* 0.5f;


      Refresh_Pages();
      RefreshPreview();
    }

    private void btn_close_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void btn_print_Click(object sender, EventArgs e)
    {
      m_user_settings = m_settings;
      m_user_settings.Copies = (int) this.UD_copies.Value;
      for (int _row = 0; _row < num_rows; _row++)
      {
        for (int _col = 0; _col < num_cols; _col++)
        {
          m_user_settings.LabelUsed[_row, _col] = excluded[_row, _col];
        }
      }
      this.Close();
    }

    private void UD_copies_ValueChanged(object sender, EventArgs e)
    {
      Refresh_Pages();
    }

    private void Refresh_Pages()
    {
      //pages calculation
      int LabelsPerPage = m_settings.Rows * m_settings.Colums;
      int totalToPrint = PendingToPrint * (int)this.UD_copies.Value;
      totalToPrint = totalToPrint - (this.NumTotal() - this.NumExcluded()); 
      countPages = 1;
      countPages = countPages + (int)(Math.Ceiling(((decimal)(totalToPrint) / (decimal)LabelsPerPage)));
      lblPageCount.Text = Resource.String("PRINT_LABEL_NUMBER_OF_PAGES") + ":" + countPages.ToString();
    }



  }
}
