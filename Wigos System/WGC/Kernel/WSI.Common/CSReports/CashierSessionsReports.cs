﻿//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashierSessionsReports.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: 27-FEB-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-FEB-2014 RMS    First Release 
// 01-APR-2014 DLL    Added functionality to substract cancelation amount at input and output (if GP Cashier - CancellationAsUndoOperation is enabled)
// 09-APR-2014 DLL    Fixed Bug WIG-800: incorrect Cash Result because sum national game profit
// 09-APR-2014 RMS    Fixed Bug WIG-813: Cashier Difference in positive when collected is less than expected.
// 25-APR-2014 DHA & JML Rename/Added TITO Cashier/Account Movements
// 22-APR-2014 DLL    Added new functionality for print specific voucher for the Dealer
// 10-JUN-2014 DLL    Fixed Bug WIG-1024: Incorrect messagen when cash balance = 0 and game profit > 0
// 08-AUG-2014 RMS    Added supoort for Specific Bank Card Types
// 18-SEP-2014 RMS    Added support for cash advance
// 06-OCT-2014 JBC    Fixed Bug WIG-1415: Movements never used error.
// 15-DEC-2014 JBC, DLL, JPJ    New Company B commission movements
// 19-DEC-2014 JBC& DLL & JPJ Fixed Bug WIG-1879: Company B
// 12-FEB-2015 DLL    Fixed Bug WIG-2050: Flash Report incorrect values
// 24-FEB-2015 OPC    Fixed Bug WIG-2050: Fixed some errors.
// 13-MAR-2015 YNM    Fixed Bug WIG-2050: Fixed some errors.
// 13-MAR-2015 YNM    Fixed Bug WIG-2162: The card cash advance is incompatible with recharge with currency
// 23-MAR-2015 YNM    Fixed Bug WIG-2050: Fixed some errors.
// 10-APR-2015 RMS    Added Gaming day to Concept Data Table
// 20-MAY-2015 DLL    Fixed Bug WIG-2372: IEJC Tax mismatch with summary cash
// 15-JUN-2015 FAV    WIG-2381: QTT column changed by Terminals_Connected column and added Terminals_Played column
// 06-JUL-2015 JML    Card deposit
// 20-Jul-2015 JML    Short & over amount 
// 08-AUG-2015 YNM    TFS-2089: Changes in Flash Report look&feel and some formulas. 
// 24-AUG-2015 FAV    TFS-3257: Transfer between cashier sessions.
// 02-SEP-2015 FAV    Fixed Bug TFS-4082: Errors in the tags of the Report.
// 09-SEP-2015 YNM    Fixed Bug 4324: Card Replacement with cost = 0 Subtract commision amount
// 10-SEP-2015 YNM    Fixed Bug 4325: Card Replacement amount with check as payment metod is not updated properly 
// 22-SEP-2015 YNM    Fixed Bug TFS-4636:check comissions are not included on Check operations
// 19-NOV-2015 ECP    Backlog Item 6463 Garcia River-09 Cash Report Resume & Voucher Closing Cash - Include Tickets Tito
// 20-NOV-2015 FAV    PBI 6048: Report to Promobox movements about print promotions not redeemable
// 04-DEC-2015 ETP    Fixed Bug 7143: Duplicated credt cards recharge.
// 03-DIC-2015 FAV    PBI 6048: Report to Promobox movements about print promotions redeemable
// 12-JAN-2016 ETP    Add amount values for cash summary.
// 13-JAN-2015 JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 20-JAN-2016 RAB    Product Backlog Item 7941:Multiple buckets: Cajero: Añadir/Restar valores a los buckets
// 25-JAN-2016 YNM    Product Backlog Item 6656:Visitas / Recepción: Crear ticket de entrada
// 01-FEB-2016 AVZ    Product Backlog Item 8673:Visitas / Recepción: Configuración de Modo de Recepción
// 09-FEB-2016 DHA    Product Backlog Item 9042: Floor Dual Currency
// 16-FEB-2016 FAV    Fixed Bug 9347, Added total of the machine tickets voided
// 26-FEB-2016 RAB    Bug 9981:Sesiones de Caja: Al realizar una búsqueda, se graba error en los logs del Gui
// 02-MAR-2016 DHA    Product Backlog Item 10085:Winpot - Added Tax Provisions movements
// 03-MAR-2016 JML    Product Backlog Item 10085:Winpot - Tax Provisions: Include movement reports
// 09-MAR-2016 EOR    Product Backlog Item 10235:TITA: Reporte de tickets
// 08-APR-2016 EOR    Fixed Bug 11626: TITA - Incorrect Cash Summary
// 19-APR-2016 RGR    Product Backlog Item 11297: Codere - Tax Custody: Reports and cash movements and account
// 27-APR-2016 RGR    Product Backlog Item 11298: Codere - Tax Custody: Modify reports GUI Income and Return
// 10-MAY-2016 LTC    Fixed Bug 12996: Tax Custody: Circulating Redeemable Unbalanced
// 10-MAY-2016 EOR    Fixed Bug 12997: Tax Custody: Result of Cashier wrong amount
// 22-MAR-2016 YNM    Bug 10759: Reception error paying with currency
// 04-ABR-2016 YNM    Bug 11282: Reception error paying with currency
// 04-APR-2016 DHA    Product Backlog Item 9756: added chips (RE, NR and Color)
// 13-APR-2016 DHA    Product Backlog Item 9950: added chips operation (sales and purchases)
// 27-APR-2016 DHA    Product Backlog Item 10825: added chips types to cage concepts
// 28-APR-2016 FAV    Fixed Bug 12323: The player card shows a valid value for company B. 
// 03-MAY-2016 FAV    Product Backlog Item 12653:Cajero: Currency exchange for multiple denominations (Mallorca)
// 24-MAY-2016 DHA    Product Backlog Item 9848: drop box gaming tables
// 30-MAY-2016 ETP    Fixed Bug 12548:Flash Report: no aparecen los terminales conectados con SQL Server 2005
// 01-MAY-2016 ETP    Fixed Bugs 14045 & 14060: associate card not defiened.
// 03-JUN-2016 FAV    Fixed Bug 11718: Changes in Cash Closing voucher to shows Cash Excess and Cash Shortfall for foreign exchanges.
// 06-JUN-2016 DLL    Bug 14133:Incremento de Circulante Redimible: decrementa el valor al realizar un pago de tarjeta
// 08-JUN-2016 EOR    Fixed Bug 13783: GUI & Cashier, Cashier Summary Incorrect Total Tickets
// 09-JUN-2016 DHA    Product Backlog Item 13402: Rolling & Fixed closing stock
// 13-JUN-2016 FAV    Fixed Bug 12538: Logrand - Daily Report with wrong machines number
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 07-JUL-2016 FOS    Product Backlog Item 14486 -Tables (Phase 1) Chips retro compatibility
// 07-JUN-2016 JBP    PBI 13253:C2GO - Implementación métodos WS Wigos
// 19-JUL-2016 FAV    Fixed Bug 7317: Flash report with new formules
// 01-JUN-2016 JMV    Product Backlog Item 14927:TITO Hold vs Win: Update
// 26-JUL-2016 ETP    Product Backlog Item 14927: TITO Hold vs Win: Update - Handpay amount is 0 
// 29-JUL-2016 DLL    Bug 15184:FlashReport: no está calculando correctamente la "Diferencia"
// 01-AUG-2016 RAB    Product Backlog Item 15614: TPV Televisa: Undo last operation on bank card
// 17-AUG-2016 RAB    Product Backlog Item 15914: Gaming tables (Phase 5): Order summary box and voucher currency
// 22-AUG-2016 RAB    Bug 16892: The payment with card of credit cancelled is are broken down in box
// 26-AUG-2016 AMF    Bug 17074:Version Sprint - Cajero: Aparece errores en el log del cajero después de instalar la versión de sprint Release 20160825 0930 -- MS
// 19-SEP-2016 EOR    PBI 17469: Televisa - Service Charge: Cash Summary
// 27-SEP-2016 RAB    Bug 17875: TPV Televisa: Shortages of balance to cancel a credit card operation. Problems with the price of the player card
// 27-SEP-2016 DHA    Fixed Bug 17847: added gaming profit for check and bank card
// 03-OCT-2016 RGR    PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
// 04-OCT-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 11-OCT-2016 ETP    Fixed Bug 18901:Multisite: Errors in cash summary in multisite mode.
// 11-OCT-2016 ETP    Fixed Bug 18423: Company b incorrect card values.
// 12-OCT-2016 RGR    Bug 18922: Draw box with cash prize (Televisa): the concept of the promotional coupon is not shown on the ticket cash desk
// 17-OCT-2016 ETP    Bug 18894: Cashier: Cash desk prize is not reported.
// 19-JAN-2017 RGR    PBI 22282: Winpot - Split cash summary
// 13-FEB-2017 DPC    Bug 24430:Precio de la tarjeta: se imputa siempre a la empresa B
// 16-FEB-2017 RAB    Bug 24330: Session summary cashdesk: missing and excess appear in some types of cash, chips, check and currency
// 08-FEB-2017 FAV    PBI 25268:Third TAX - Payment Voucher
// 23-FEB-2017 RGR    PBI 22282: Winpot - Split cash summary
// 15-MAR-2017 RAB    Bug 24773: The introduced amount doesn't match the expected
// 28-MAR-2017 XGJ    Bug 26302:Voucher de Cierre de Caja-Empresa B: no refleja correctamente las cancelaciones 
// 11-APR-2017 FJC    Bug 26082:Sorteo de Máquina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado. (Reopened)
// 21-MAR-2017 ETP    PBI 25788: Credit Line - Get a Marker
// 05-JUL-2017 RAB    Bug 28557:WIGOS-3403 Undo sale chips operation with gaming tables draw played the log registered an error
// 06-JUL-2017 JML    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 13-JUN-2018 MS     PBI 33018: [WIGOS-5327][Ticket #8911][Change Request] Daily report Provider/Cashier - Promotion error V03.06.0023
// 12-JUN-2018 AGS    Bug 33002: WIGOS-5329 [Ticket #8912] Daily report Provider/Cashier - Netwin Error
// 26-JUN-2018 FOS    Bug 33335:WIGOS-13162 Gambling tables: Credit card movement is not showed correctly when a close partial withdraw has been done.
// 26-JUN-2018 DLM    Bug 33319: WIGOS-13186 Gambling tables: It is not possible to close the cash session balanced when there sale chips operation.
// 28-JUN-2018 FOS    Bug 33319: WIGOS-13186 Gambling tables: It is not possible to close the cash session balanced when there sale chips operation.
// 26-JUN-2018 FOS    Bug 33335: WIGOS-13162 Gambling tables:(Edited) After resolved the defect, QA detect other similar error, that didn't reproduced before solved previos error
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.Entrances;

namespace WSI.Common.CSReports
{
  public static class CashierSessionsReports
  {
    private static DataTable CreateByConceptDataTable()
    {
      DataTable _result;

      _result = new DataTable("CASHIER_MOVEMENTS_GROUPED");

      _result.Columns.Add("SESS_ID", Type.GetType("System.Int64")).AllowDBNull = false;
      _result.Columns.Add("SESS_NAME", Type.GetType("System.String")).AllowDBNull = false;
      _result.Columns.Add("SESS_USER_ID", Type.GetType("System.Int32")).AllowDBNull = false;
      _result.Columns.Add("SESS_FULL_NAME", Type.GetType("System.String")).AllowDBNull = false;
      _result.Columns.Add("SESS_OPENING_DATE", Type.GetType("System.DateTime")).AllowDBNull = false;
      _result.Columns.Add("SESS_CLOSING_DATE", Type.GetType("System.DateTime")).AllowDBNull = true;
      _result.Columns.Add("SESS_CASHIER_ID", Type.GetType("System.Int32")).AllowDBNull = false;
      _result.Columns.Add("SESS_CT_NAME", Type.GetType("System.String")).AllowDBNull = false;
      _result.Columns.Add("SESS_STATUS", Type.GetType("System.Int32")).AllowDBNull = false;
      _result.Columns.Add("SESS_CS_LIMIT_SALE", Type.GetType("System.Decimal")).AllowDBNull = true;
      _result.Columns.Add("SESS_MB_LIMIT_SALE", Type.GetType("System.Decimal")).AllowDBNull = true;
      _result.Columns.Add("SESS_COLLECTED_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = true;

      _result.Columns.Add("CGS_CAGE_SESSION_ID", Type.GetType("System.Int64")).AllowDBNull = true;
      _result.Columns.Add("CGS_SESSION_NAME", Type.GetType("System.String")).AllowDBNull = true;

      _result.Columns.Add("SESS_GAMING_DAY", Type.GetType("System.DateTime")).AllowDBNull = true;

      //ESE 28-SEP-2016
      _result.Columns.Add("SESS_MONEY_RECHARGE_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = true;
      _result.Columns.Add("SESS_BANK_CARD_RECHARGE_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = true;
      _result.Columns.Add("SESS_TOTAL_VOUCHERS", Type.GetType("System.Int32")).AllowDBNull = true;
      _result.Columns.Add("SESS_DELIVERED_VOUCHERS", Type.GetType("System.Int32")).AllowDBNull = true;
      _result.Columns.Add("SESS_TOTAL_VOUCHERS_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = true;

      // The balance is calculated with movements
      //_result.Columns.Add("SESS_BALANCE", Type.GetType("System.Decimal"));

      _result.Columns.Add("SESS_INFORMATION", typeof(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS));

      _result.PrimaryKey = new DataColumn[] { _result.Columns["SESS_ID"] };

      return _result;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the data referred to a list of sessions information and detail
    //          
    //  PARAMS:
    //      - INPUT:
    //          - Sessions            A DataTable with the sessions
    //          - MovementsGroupById  A DataTable with the movements of the list of 
    //                                sessions passed in Sessions
    //
    //      - OUTPUT:
    //          - ByConcept           DataTable with by sesión detailed data 
    // RETURNS: 
    // 
    public static void BuildGroupByConcept(DataTable Sessions, DataTable MovementsGroupById, DataTable CurrenciesInfo, out DataTable ByConcept)
    {
      Cashier.TYPE_CASHIER_SESSION_STATS _session_by_concept;
      DataRow _session_data_row;

      ByConcept = CreateByConceptDataTable();

      foreach (DataRow _session in Sessions.Rows)
      {
        _session_by_concept = null;

        BuildGroupByConcept((Int64)_session["SESS_ID"], MovementsGroupById, CurrenciesInfo, out _session_by_concept);

        if (_session_by_concept == null)
        {
          // Session has no movements
          continue;
        }

        _session_data_row = ByConcept.NewRow();

        _session_data_row["SESS_ID"] = (Int64)_session["SESS_ID"];
        _session_data_row["SESS_NAME"] = (String)_session["SESS_NAME"];
        _session_data_row["SESS_USER_ID"] = (Int32)_session["SESS_USER_ID"];
        _session_data_row["SESS_FULL_NAME"] = (String)_session["SESS_FULL_NAME"];
        _session_data_row["SESS_OPENING_DATE"] = (DateTime)_session["SESS_OPENING_DATE"];

        _session_data_row["SESS_CLOSING_DATE"] = _session.IsNull("SESS_CLOSING_DATE") ? DBNull.Value : _session["SESS_CLOSING_DATE"];

        _session_data_row["SESS_CASHIER_ID"] = (Int32)_session["SESS_CASHIER_ID"];
        _session_data_row["SESS_CT_NAME"] = (String)_session["SESS_CT_NAME"];
        _session_data_row["SESS_STATUS"] = (Int32)_session["SESS_STATUS"];

        _session_data_row["SESS_CS_LIMIT_SALE"] = _session.IsNull("SESS_CS_LIMIT_SALE") ? DBNull.Value : _session["SESS_CS_LIMIT_SALE"];

        _session_data_row["SESS_MB_LIMIT_SALE"] = _session.IsNull("SESS_MB_LIMIT_SALE") ? DBNull.Value : _session["SESS_MB_LIMIT_SALE"];

        _session_data_row["SESS_COLLECTED_AMOUNT"] = _session.IsNull("SESS_COLLECTED_AMOUNT") ? DBNull.Value : _session["SESS_COLLECTED_AMOUNT"];

        if (_session.IsNull("CGS_CAGE_SESSION_ID"))
        {
          _session_data_row["CGS_CAGE_SESSION_ID"] = DBNull.Value;
          _session_data_row["CGS_SESSION_NAME"] = DBNull.Value;
        }
        else
        {
          _session_data_row["CGS_CAGE_SESSION_ID"] = (Int64)_session["CGS_CAGE_SESSION_ID"];
          _session_data_row["CGS_SESSION_NAME"] = (String)_session["CGS_SESSION_NAME"];
        }

        _session_data_row["SESS_GAMING_DAY"] = _session.IsNull("SESS_GAMING_DAY") ? DBNull.Value : _session["SESS_GAMING_DAY"];

        //ESE 28-SEP-2016
        _session_data_row["SESS_MONEY_RECHARGE_AMOUNT"] = _session.IsNull("SESS_MONEY_RECHARGE_AMOUNT") ? 0 : _session["SESS_MONEY_RECHARGE_AMOUNT"];
        _session_data_row["SESS_BANK_CARD_RECHARGE_AMOUNT"] = _session.IsNull("SESS_BANK_CARD_RECHARGE_AMOUNT") ? 0 : _session["SESS_BANK_CARD_RECHARGE_AMOUNT"];
        _session_data_row["SESS_TOTAL_VOUCHERS"] = _session.IsNull("SESS_TOTAL_VOUCHERS") ? 0 : _session["SESS_TOTAL_VOUCHERS"];
        _session_data_row["SESS_DELIVERED_VOUCHERS"] = _session.IsNull("SESS_DELIVERED_VOUCHERS") ? 0 : _session["SESS_DELIVERED_VOUCHERS"];
        _session_data_row["SESS_TOTAL_VOUCHERS_AMOUNT"] = _session.IsNull("SESS_TOTAL_VOUCHERS_AMOUNT") ? 0 : _session["SESS_TOTAL_VOUCHERS_AMOUNT"];

        //_session_data_row["SESS_BALANCE"] = (Decimal)_session["SESS_BALANCE"];

        _session_data_row["SESS_INFORMATION"] = _session_by_concept;

        ByConcept.Rows.Add(_session_data_row);
      }

      return;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the data referred to a session information and detail
    //          
    //  PARAMS:
    //      - INPUT:
    //          - SessionId           The session id of the movements passed
    //          - MovementsGroupById  
    //
    //      - OUTPUT:
    //          - ByConcept           Object with the calculated data of the session 
    // RETURNS: 
    // 
    public static void BuildGroupByConcept(Int64 SessionId, DataTable MovementsGroupById, DataTable CurrenciesInfo, out Cashier.TYPE_CASHIER_SESSION_STATS SessionByConcept)
    {
      DataRow[] _session_movements;
      DataRow[] _session_currencies;

      SessionByConcept = null;

      _session_movements = MovementsGroupById.Select("CM_SESSION_ID = " + SessionId.ToString());

      // Only process if there are movements
      if (_session_movements.Length == 0)
      {
        return;
      }

      SessionByConcept = new Cashier.TYPE_CASHIER_SESSION_STATS();

      FillCashierSessionDataFromDt(_session_movements, ref SessionByConcept);

      if (CurrenciesInfo == null)
      {
        return;
      }

      _session_currencies = CurrenciesInfo.Select("CSC_SESSION_ID = " + SessionId.ToString());

      FillCashierSessionCurrenciesDataFromDt(_session_currencies, ref SessionByConcept);
    }

    // -----------------------------------------------------------------------------------------------------------------------------------------------

    private static void FillCashierSessionCurrenciesDataFromDt(DataRow[] SessionCurrenciesData, ref Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      CurrencyIsoType _cit;

      if (SessionCurrenciesData.Length == 0 || CashierSessionData == null)
      {
        return;
      }

      foreach (DataRow _dr in SessionCurrenciesData)
      {
        //CSC_SESSION_ID, CSC_ISO_CODE, CSC_TYPE, CSC_BALANCE, CSC_COLLECTED
        _cit = new CurrencyIsoType();
        _cit.IsoCode = (String)_dr["CSC_ISO_CODE"];
        _cit.Type = (CurrencyExchangeType)_dr["CSC_TYPE"];

        // Balance
        if (CashierSessionData.currencies_balance.ContainsKey(_cit))
        {
          CashierSessionData.currencies_balance[_cit] = (Decimal)_dr["CSC_BALANCE"];
        }
        else
        {
          CashierSessionData.currencies_balance.Add(_cit, (Decimal)_dr["CSC_BALANCE"]);
        }

        // Collected
        if (CashierSessionData.collected.ContainsKey(_cit))
        {
          CashierSessionData.collected[_cit] = (Decimal)_dr["CSC_COLLECTED"];
        }
        else
        {
          CashierSessionData.collected.Add(_cit, (Decimal)_dr["CSC_COLLECTED"]);
        }

        // Difference
        if (CashierSessionData.collected_diff.ContainsKey(_cit))
        {
          CashierSessionData.collected_diff[_cit] = (Decimal)_dr["CSC_COLLECTED"] - (Decimal)_dr["CSC_BALANCE"];
        }
        else
        {
          CashierSessionData.collected_diff.Add(_cit, (Decimal)_dr["CSC_COLLECTED"] - (Decimal)_dr["CSC_BALANCE"]);
        }
      }
    }

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // TODO : Use FillCashierSessionDataFromDt from Cashier.cs converting it to public

    //------------------------------------------------------------------------------
    // PURPOSE: Fill CashierSessionData structure from DataTable
    //
    //  PARAMS:
    //      - INPUT:
    //          - DataTable Dt_CashierMovementsGrouped
    //
    //      - OUTPUT:
    //          - ref TYPE_CASHIER_SESSION_STATS CashierSessionData
    //
    // RETURNS:
    //
    public static void FillCashierSessionDataFromDt(DataRow[] CashierMovementsGrouped, ref Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      #region "Variable declaration"
      //Currency cash_in_amount = 0;
      Currency cash_in_split1 = 0;
      Currency cash_in_split2 = 0;
      Currency cash_in_card_deposit = 0;

      Currency participation_in_draw = 0;

      // Currency cash_out_amount = 0;
      Currency dev_split1 = 0;
      Currency dev_split2 = 0;
      Currency cancel_split1 = 0;
      Currency cancel_split2 = 0;

      Currency cash_out_card_deposit = 0;
      Currency cash_out_card_deposit_split1 = 0;
      Currency cash_out_card_deposit_split2 = 0;

      Currency filler_in_amount = 0;
      Currency filler_out_amount = 0;

      Currency cashier_opening_amount = 0;
      Currency pending_amount = 0;

      Currency _promo_nr = 0;
      Currency _promo_re = 0;
      Currency _cancel_promo_nr = 0;
      Currency _cancel_promo_re = 0;

      Currency _redeemable_dev_expired = 0;
      Currency _redeemable_prize_expired = 0;
      Currency _not_redeemable_expired = 0;

      Currency promo_amount = 0;
      Currency _promo_nr_to_re = 0;

      Currency total_amount = 0;
      Currency tax_on_prize_1 = 0;
      Currency tax_on_prize_2 = 0;
      Currency tax_on_prize_3 = 0;
      Currency tax_returning_on_prize_coupon = 0;
      // RRB 10-SEP-2012
      Currency decimal_rounding = 0;
      Currency service_charge = 0;

      Currency service_charge_a = 0;
      Currency service_charge_b = 0;

      Currency sub_total_amount = 0;
      Currency mb_transfered_amount = 0;
      Currency mb_deposit_amount = 0;
      Currency mb_total_balance_lost = 0;

      // TJG 02-DEC-2009
      // Split the cash subtotal to correctly show the subtotal in the new Cash Close voucher layout.
      // Cash Close voucher has a different distribution of figures than the Cash Close screen.
      // Card deposits shgould be excluded from the voucher Cash Close subtotal.
      Currency sub_total_card_deposit = 0;

      Currency sub_balance = 0;
      Currency final_balance = 0;
      Currency prizes_amount = 0;
      Currency prizes_gross_other = 0;

      Currency handpays_amount = 0;
      Currency handpays_cancel_amount = 0;
      Decimal _payments_order_amount_cheque = 0;

      SortedDictionary<CurrencyIsoType, Decimal> _chips_purchase = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      SortedDictionary<CurrencyIsoType, Decimal> _chips_sale = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      // ICS 04-JUL-2013
      SortedDictionary<CurrencyIsoType, Decimal> _currencies_balance;
      String _national_currency;
      String _currency;

      Decimal _cash_commissions = 0;
      Decimal _company_b_cash_commissions = 0;

      Decimal _bank_card_net_amount_split1 = 0;
      Decimal _bank_card_net_amount_split2 = 0;
      Decimal _bank_card_commissions = 0;

      // RMS 08-AUG-2014
      Decimal _bank_credit_card_net_amount_split1 = 0;
      Decimal _bank_credit_card_net_amount_split2 = 0;
      Decimal _bank_credit_card_commissions = 0;
      Decimal _bank_debit_card_net_amount_split1 = 0;
      Decimal _bank_debit_card_net_amount_split2 = 0;
      Decimal _bank_debit_card_commissions = 0;

      Decimal _currency_exchange_net_amount_split1 = 0;
      Decimal _currency_exchange_net_amount_split2 = 0;
      Decimal _currency_exchange_commissions = 0;

      Decimal _points_as_cashin_split1 = 0;
      Decimal _points_as_cashin_split2 = 0;
      Decimal _points_as_cashin_prize = 0;

      Decimal _company_b_bank_card_commissions = 0;
      Decimal _company_b_credit_bank_commissions = 0;
      Decimal _company_b_debit_bank_commissions = 0;
      Decimal _company_b_check_commissions = 0;
      Decimal _company_b_currency_exchange_commissions = 0;


      // RCI 02-JAN-2014
      Decimal _cash_in_tax_split1 = 0;
      // DRV 28-JUL-2014
      Decimal _cash_in_tax_split2 = 0;

      // RGR 18-APR-2016
      Decimal _tax_custody = 0;

      // RGR 26-APR-2016
      Decimal _tax_custody_return = 0;

      // JML 03-MAR-2016
      Decimal _tax_provisions = 0;

      // DLL 05-AUG-2013
      SortedDictionary<CurrencyIsoType, Decimal> _fill_in_balance;
      SortedDictionary<CurrencyIsoType, Decimal> _fill_out_balance;

      //DLL 10-SEP-2013
      Decimal _check_net_amount_split1 = 0;
      Decimal _check_net_amount_split2 = 0;
      Decimal _check_commissions = 0;

      CurrencyIsoType _iso_type;

      // ICS 18-SEP-2013
      SortedDictionary<CurrencyIsoType, Decimal> _currency_cash_in;
      SortedDictionary<CurrencyIsoType, Decimal> _currency_cash_in_exchange;
      SortedDictionary<CurrencyIsoType, Decimal> _currency_cash_in_commissions;
      SortedDictionary<CurrencyIsoType, Decimal> _currency_game_profit;

      Decimal _amount;

      //Tickets Info
      Decimal _tickets_created_amount_jackpot;
      Decimal _tickets_created_amount_handpay;

      Decimal _tickets_cancelled_amount_jackpot;
      Decimal _tickets_cancelled_amount_handpay;
      Decimal _handpay_tito_hold;

      Decimal _tickets_cancelled_amount;
      Decimal _tickets_offline_amount;
      Decimal _tickets_online_discarded_amount;
      Int32 _tickets_created;
      Int32 _tickets_cancelled;
      Decimal _national_game_profit;

      // DHA 25-APR-2014 Added ticket TITO detail
      Decimal _tickets_cashier_printed_cashable_amount;
      Decimal _tickets_cashier_printed_promo_redeemable_amount;
      Decimal _tickets_cashier_printed_promo_no_redeemable_amount;
      Decimal _tickets_machine_printed_cashable_amount;
      Decimal _tickets_machine_printed_promo_no_redeemable_amount;
      Decimal _tickets_cashier_paid_cashable_amount;
      Decimal _tickets_cashier_paid_promo_redeemable_amount;
      Decimal _tickets_machine_played_cashable_amount;
      Decimal _tickets_machine_played_promo_redeemable_amount;
      Decimal _tickets_machine_played_promo_no_redeemable_amount;
      Decimal _tickets_expired_redeemed;

      //EOR 09-MAR-2016
      Decimal _tickets_swap_chips;

      //JBC 21-FEB-2014
      Decimal _tickets_expired_amount_promo_redeemable;
      Decimal _tickets_expired_amount_promo_no_redeemable;
      Decimal _tickets_expired_amount_redeemable;

      //DLL 22-APR-2014
      Boolean _chips_sale_mode;

      //SMN 10-SEP-2014
      Currency _concepts_amount_in = 0;
      Currency _concepts_amount_out = 0;
      SortedDictionary<CurrencyIsoType, Decimal> _concepts_currency_amount_in;
      SortedDictionary<CurrencyIsoType, Decimal> _concepts_currency_amount_out;

      //YNM 25-JAN-2016
      Currency _entrances_amount_in = 0;
      Currency _entrances_amount_in_currency = 0;
      Currency _entrances_amount_in_card = 0;
      Currency _entrances_amount_in_debit_card = 0;
      Currency _entrances_amount_in_credit_card = 0;
      Currency _entrances_amount_in_check = 0;
      SortedDictionary<CurrencyIsoType, Decimal> _entrances_currency_amount_in;

      // RMS 18-SEP-2014
      Currency _cash_advance_credit_card;
      Currency _cash_advance_debit_card;
      Currency _cash_advance_generic_card;
      Currency _cash_advance_check;
      Currency _cash_advance_credit_card_comissions;
      Currency _cash_advance_debit_card_comissions;
      Currency _cash_advance_generic_card_comissions;
      Currency _cash_advance_check_comissions;

      // DLL 18-DEC-2014
      Currency _company_b_cash_advance_credit_card_comissions;
      Currency _company_b_cash_advance_debit_card_comissions;
      Currency _company_b_cash_advance_generic_card_comissions;
      Currency _company_b_cash_advance_check_comissions;

      //JBC 22-OCT-2014
      Currency _mb_cash_over;
      Currency _mb_shortfall_cash;
      Boolean _gp_pending_cash_partial;

      //JML 06-JUL-2015
      Currency _bank_card_cash_in_card_deposit_split1 = 0;
      Currency _bank_credit_card_cash_in_card_deposit_split1 = 0;
      Currency _bank_debit_card_cash_in_card_deposit_split1 = 0;
      Currency _check_cash_in_card_deposit_split1 = 0;
      Currency _currency_cash_in_card_deposit_split1 = 0;
      Currency _cash_cash_in_card_deposit_split1 = 0;
      Currency _bank_card_cash_in_card_deposit_split2 = 0;
      Currency _bank_credit_card_cash_in_card_deposit_split2 = 0;
      Currency _bank_debit_card_cash_in_card_deposit_split2 = 0;
      Currency _check_cash_in_card_deposit_split2 = 0;
      Currency _currency_cash_in_card_deposit_split2 = 0;
      Currency _cash_cash_in_card_deposit_split2 = 0;
      Currency _tmp_amount = 0;

      SortedDictionary<CurrencyIsoType, Decimal> _currency_cash_in_card_deposit;

      // FAV 24-AUG-2015
      Currency _session_transfers_amount_in = 0;
      Currency _session_transfers_amount_out = 0;
      SortedDictionary<CurrencyIsoType, Decimal> _session_transfers_currency_amount_in;
      SortedDictionary<CurrencyIsoType, Decimal> _session_transfers_currency_amount_out;

      //ECP 23-NOV-2015 Tickets Info
      Decimal _tickets_created_amount;
      Decimal _tickets_played_machine_amount;
      Decimal _tickets_created_machine_amount;
      Int32 _tickets_played_machine_count;
      Int32 _tickets_created_machine_count;

      //ECP 19-NOV-2015 Added ticket accounts TITO detail
      Int32 _tickets_created_redeemable_handpay_count;
      Int32 _tickets_created_redeemable_jackpot_count;
      Int32 _tickets_cancelled_redeemable_handpay_count;
      Int32 _tickets_cancelled_redeemable_jackpot_count;

      Int32 _tickets_offline_count;
      Int32 _tickets_online_discarded_count;
      Int32 _tickets_cashier_printed_cashable_count;
      Int32 _tickets_cashier_printed_promo_redeemable_count;
      Int32 _tickets_cashier_printed_promo_no_redeemable_count;
      Int32 _tickets_machine_printed_cashable_count;
      Int32 _tickets_machine_printed_promo_no_redeemable_count;
      Int32 _tickets_cashier_paid_cashable_count;
      Int32 _tickets_cashier_paid_promo_redeemable_count;
      Int32 _tickets_machine_played_cashable_count;
      Int32 _tickets_machine_played_promo_redeemable_count;
      Int32 _tickets_machine_played_promo_no_redeemable_count;
      Int32 _tickets_cashier_expired_redeemed_count;

      // EOR 20-MAY-2016
      Int32 _tickets_swap_chips_count;

      Int32 _tickets_expired_promo_redeemable_count;
      Int32 _tickets_expired_promo_no_redeemable_count;
      Int32 _tickets_expired_redeemable_count;

      Currency _refill_amount_to_cage;
      Currency _collect_amount_to_cage;
      Currency _refill_amount_to_cashier;
      Currency _collect_amount_to_cashier;
      SortedDictionary<CurrencyIsoType, Decimal> _refill_currency_amount_to_cage;
      SortedDictionary<CurrencyIsoType, Decimal> _collect_currency_amount_to_cage;
      SortedDictionary<CurrencyIsoType, Decimal> _refill_currency_amount_to_cashier;
      SortedDictionary<CurrencyIsoType, Decimal> _collect_currency_amount_to_cashier;

      CageCurrencyType _cage_currency_type;

      // FAV 03-MAY-2016 (Exchange currency - multiple denominations)
      SortedDictionary<CurrencyIsoType, Decimal> _currency_exchange_cash_in;
      SortedDictionary<CurrencyIsoType, Decimal> _currency_exchange_cash_in_exchange;
      SortedDictionary<CurrencyIsoType, Decimal> _currency_exchange_cash_in_commissions;
      SortedDictionary<CurrencyIsoType, Decimal> _currency_exchange_cash_out;
      SortedDictionary<CurrencyIsoType, Decimal> _currency_exchange_cash_out_exchange;
      SortedDictionary<CurrencyIsoType, Decimal> _currency_exchange_cash_out_commissions;
      Currency _cash_in_currency_exchange = 0;
      Currency _cash_out_currency_exchange = 0;

      _mb_cash_over = 0;
      _mb_shortfall_cash = 0;
      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      _cash_advance_credit_card = 0;
      _cash_advance_debit_card = 0;
      _cash_advance_generic_card = 0;
      _cash_advance_check = 0;
      _cash_advance_credit_card_comissions = 0;
      _cash_advance_debit_card_comissions = 0;
      _cash_advance_generic_card_comissions = 0;
      _cash_advance_check_comissions = 0;

      _company_b_cash_advance_credit_card_comissions = 0;
      _company_b_cash_advance_debit_card_comissions = 0;
      _company_b_cash_advance_generic_card_comissions = 0;
      _company_b_cash_advance_check_comissions = 0;

      _refill_amount_to_cage = 0;
      _collect_amount_to_cage = 0;
      _refill_amount_to_cashier = 0;
      _collect_amount_to_cashier = 0;

      CASHIER_MOVEMENT _cm_type;

      _chips_sale_mode = GeneralParam.GetBoolean("GamingTables", "Cashier.Mode");

      //JBC 04-12-2014
      Split_Tax_Amount _total_tax;
      Split_Tax_Amount _card_tax;
      Split_Tax_Amount _company_b_tax;
      Dictionary<Split_Taxes_Amount, Split_Tax_Amount> _b_taxes;

      _total_tax = new Split_Tax_Amount();
      _card_tax = new Split_Tax_Amount();
      _company_b_tax = new Split_Tax_Amount();
      _b_taxes = new Dictionary<Split_Taxes_Amount, Split_Tax_Amount>();

      _tickets_cancelled = 0;
      _tickets_created = 0;
      _tickets_created_amount_handpay = 0;
      _tickets_created_amount_jackpot = 0;

      _tickets_cancelled_amount_handpay = 0;
      _tickets_cancelled_amount_jackpot = 0;
      _handpay_tito_hold = 0;

      _tickets_cancelled_amount = 0;
      _tickets_offline_amount = 0;
      _tickets_online_discarded_amount = 0;
      _national_game_profit = 0;
      _tickets_expired_redeemed = 0;

      _tickets_expired_amount_promo_redeemable = 0;
      _tickets_expired_amount_promo_no_redeemable = 0;
      _tickets_expired_amount_redeemable = 0;

      // DHA 17-APR-2014 added ticket TITO detail
      _tickets_cashier_printed_cashable_amount = 0;
      _tickets_cashier_printed_promo_redeemable_amount = 0;
      _tickets_cashier_printed_promo_no_redeemable_amount = 0;
      _tickets_machine_printed_cashable_amount = 0;
      _tickets_machine_printed_promo_no_redeemable_amount = 0;
      _tickets_cashier_paid_cashable_amount = 0;
      _tickets_cashier_paid_promo_redeemable_amount = 0;
      _tickets_machine_played_cashable_amount = 0;
      _tickets_machine_played_promo_redeemable_amount = 0;
      _tickets_machine_played_promo_no_redeemable_amount = 0;

      // EOR 09-MAR-2016
      _tickets_swap_chips = 0;

      //ECP 23-NOV-2015 Tickets Info
      _tickets_created_amount = 0;
      _tickets_played_machine_amount = 0;
      _tickets_created_machine_amount = 0;
      _tickets_played_machine_count = 0;
      _tickets_created_machine_count = 0;

      //ECP 19-NOV-2015 Added ticket accounts TITO detail
      _tickets_created_redeemable_handpay_count = 0;
      _tickets_created_redeemable_jackpot_count = 0;
      _tickets_cancelled_redeemable_handpay_count = 0;
      _tickets_cancelled_redeemable_jackpot_count = 0;

      _tickets_offline_count = 0;

      _tickets_online_discarded_count = 0;

      _tickets_cashier_printed_cashable_count = 0;
      _tickets_cashier_printed_promo_redeemable_count = 0;
      _tickets_cashier_printed_promo_no_redeemable_count = 0;
      _tickets_machine_printed_cashable_count = 0;
      _tickets_machine_printed_promo_no_redeemable_count = 0;
      _tickets_cashier_paid_cashable_count = 0;
      _tickets_cashier_paid_promo_redeemable_count = 0;
      _tickets_machine_played_cashable_count = 0;
      _tickets_machine_played_promo_redeemable_count = 0;
      _tickets_machine_played_promo_no_redeemable_count = 0;
      _tickets_cashier_expired_redeemed_count = 0;

      // EOR 20-MAY-2016
      _tickets_swap_chips_count = 0;

      _tickets_expired_promo_redeemable_count = 0;
      _tickets_expired_promo_no_redeemable_count = 0;
      _tickets_expired_redeemable_count = 0;

      //JML 20-JUL-2015
      SortedDictionary<CurrencyIsoType, Decimal> _short_currency;
      _short_currency = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      //JML 05-AUG-2015
      SortedDictionary<CurrencyIsoType, Decimal> _over_currency;
      _over_currency = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      // FAV 16-FEB-2016
      Currency _machine_tickets_voided = 0;
      Currency _total_credit_line_payback;
      Currency _total_credit_line_marker;

      _total_credit_line_payback = 0;
      _total_credit_line_marker = 0;

      CashierSessionData.collected = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      CashierSessionData.collected_diff = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _iso_type = new CurrencyIsoType();
      _national_currency = CurrencyExchange.GetNationalCurrency();
      _currencies_balance = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _fill_in_balance = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _fill_out_balance = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currency_cash_in = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currency_cash_in_exchange = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currency_cash_in_commissions = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currency_game_profit = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _entrances_currency_amount_in = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _concepts_currency_amount_in = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _concepts_currency_amount_out = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _entrances_currency_amount_in = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      _currency_cash_in_card_deposit = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      _session_transfers_currency_amount_in = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _session_transfers_currency_amount_out = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      _refill_currency_amount_to_cage = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _collect_currency_amount_to_cage = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _refill_currency_amount_to_cashier = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _collect_currency_amount_to_cashier = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      _currency_exchange_cash_in = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currency_exchange_cash_in_exchange = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currency_exchange_cash_in_commissions = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currency_exchange_cash_out = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currency_exchange_cash_out_exchange = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _currency_exchange_cash_out_commissions = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      CashierSessionData.chips_purchase_total = 0;
      CashierSessionData.chips_sale_total = 0;
      CashierSessionData.chips_sale_register_total = 0;

      CashierSessionData.current_card_withdrawal_total = 0;

      #endregion

      // JMV: 01-JUN-2016
      String _cash_promos_gp;
      List<string> _cash_promos_list;

      _cash_promos_list = new List<string>();
      _cash_promos_gp = GeneralParam.GetString("WigosGUI", "CashPromos", String.Empty);

      if (_cash_promos_gp != String.Empty)
      {
        _cash_promos_list = new List<string>(_cash_promos_gp.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
      }

      // Pseudo column 'SUB_TYPE' is introduced to distinguish movements
      // coming from each part of the union

      // AJQ 05-NOV-2012: Added the hint to force the index
      //                  Added "Union ALL" because all the registers are different (subtype)

      _amount = 0;
      foreach (DataRow _dr in CashierMovementsGrouped)
      {
        _amount = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]) + Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
        _cage_currency_type = (CageCurrencyType)(_dr["CM_CAGE_CURRENCY_TYPE"]);

        #region
        switch ((int)(_dr["CM_SUB_TYPE"]))
        {
          case 0:
            _cm_type = (CASHIER_MOVEMENT)_dr["CM_TYPE"];

            Boolean _stop_case = false;
            //Operaciones que no tienen nada que hacer
            _stop_case = MovementsWithoutActions(_cm_type);
            if (!_stop_case)
            {
              #region
              switch (_cm_type)
              {
                // AJQ & DDM 11-OCT-2013, UNR's
                case CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS: CashierSessionData.unr_k1_gross += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1: CashierSessionData.unr_k1_tax1 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2: CashierSessionData.unr_k1_tax2 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3: CashierSessionData.unr_k1_tax3 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS: CashierSessionData.unr_k2_gross += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1: CashierSessionData.unr_k2_tax1 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2: CashierSessionData.unr_k2_tax2 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3: CashierSessionData.unr_k2_tax3 += _amount; break;

                // JML 22-DEC-2015, RE in kind
                case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS: CashierSessionData.ure_k1_gross += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1: CashierSessionData.ure_k1_tax1 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2: CashierSessionData.ure_k1_tax2 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3: CashierSessionData.ure_k1_tax3 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS: CashierSessionData.ure_k2_gross += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1: CashierSessionData.ure_k2_tax1 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2: CashierSessionData.ure_k2_tax2 += _amount; break;
                case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3: CashierSessionData.ure_k2_tax3 += _amount; break;

                case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX: CashierSessionData.prize_taxes_1_pr_re += _amount; break;
                case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX: CashierSessionData.prize_taxes_2_pr_re += _amount; break;
                case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX: CashierSessionData.prize_taxes_3_pr_re += _amount; break;

                case CASHIER_MOVEMENT.OPEN_SESSION:
                  {
                    #region
                    _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();
                    if (_currency == "" || _currency == _national_currency)
                    {

                      cashier_opening_amount += _amount;


                      sub_total_amount += _amount;


                      total_amount += _amount;
                    }
                    else
                    {

                      // Update currencies balance
                      _iso_type = new CurrencyIsoType();
                      _iso_type.IsoCode = _currency;
                      _iso_type.Type = CurrencyExchangeType.CURRENCY;
                      if (_currency == Cage.CHIPS_ISO_CODE)
                      {
                        _iso_type.Type = CurrencyExchangeType.CASINOCHIP;
                      }

                      if (_currencies_balance.ContainsKey(_iso_type))
                      {
                        _currencies_balance[_iso_type] += _amount;
                      }
                      else
                      {
                        _currencies_balance.Add(_iso_type, _amount);
                      }

                      // Update fill in balance
                      if (_fill_in_balance.ContainsKey(_iso_type))
                      {
                        _fill_in_balance[_iso_type] += _amount;
                      }
                      else
                      {
                        _fill_in_balance.Add(_iso_type, _amount);
                      }
                    }
                    #endregion
                  }
                  break;

                case CASHIER_MOVEMENT.FILLER_IN:
                // DHA 09-JUN-2016
                case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
                  {
                    #region
                    _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();
                    if ((_currency == "" || _currency == _national_currency) && !FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                    {
                      filler_in_amount += _amount;
                      sub_total_amount += _amount;
                      total_amount += _amount;
                    }
                    else
                    {
                      // Update fill in balance
                      _iso_type = new CurrencyIsoType();
                      _iso_type.IsoCode = _currency;
                      _iso_type.Type = CurrencyExchangeType.CURRENCY;

                      if (_currency == Cage.CHIPS_ISO_CODE)
                      {
                        // Old Chips with X01 Iso code must be adapted to new chips definition
                        _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
                        _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;
                      }
                      else if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                      {
                        _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                      }

                      if (_fill_in_balance.ContainsKey(_iso_type))
                      {
                        _fill_in_balance[_iso_type] += _amount;
                      }
                      else
                      {
                        _fill_in_balance.Add(_iso_type, _amount);
                      }

                      // Update currencies balance
                      if (_currencies_balance.ContainsKey(_iso_type))
                      {
                        _currencies_balance[_iso_type] += _amount;
                      }
                      else
                      {
                        _currencies_balance.Add(_iso_type, _amount);
                      }
                    }
                    #endregion
                  }
                  break;

                case CASHIER_MOVEMENT.FILLER_OUT:
                  {
                    #region
                    _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();

                    // filler out movement needs to be differenced for check/card (colletion withdrawal for gaming tables from cage)
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;
                    _iso_type.Type = CurrencyExchangeType.CURRENCY;

                    switch ((Int32)(Decimal)(_dr["CM_CURRENCY_DENOMINATION"]))
                    {
                      case Cage.BANK_CARD_CODE:
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        break;
                      case Cage.CHECK_CODE:
                        _iso_type.Type = CurrencyExchangeType.CHECK;

                        break;
                      case Cage.TICKETS_CODE:
                        _tmp_amount = 0;

                        break;
                    }

                    if (FeatureChips.IsChipsType(_cage_currency_type))
                    {
                      _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                    }

                    if ((_currency == "" || _currency == _national_currency) && _iso_type.Type == CurrencyExchangeType.CURRENCY)
                    {
                      filler_out_amount += _amount;
                      sub_total_amount -= _amount;
                      total_amount -= _amount;
                    }
                    else
                    {
                      // Update fill out balance
                      if (_currency == Cage.CHIPS_ISO_CODE)
                      {
                        // Old Chips with X01 Iso code must be adapted to new chips definition
                        _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
                        _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;
                      }

                      if (_fill_out_balance.ContainsKey(_iso_type))
                      {
                        _fill_out_balance[_iso_type] += _amount;
                      }
                      else
                      {
                        _fill_out_balance.Add(_iso_type, _amount);
                      }

                      // Update currencies balance
                      if (_currencies_balance.ContainsKey(_iso_type))
                      {
                        _currencies_balance[_iso_type] -= _amount;
                      }
                      else
                      {
                        _currencies_balance.Add(_iso_type, -_amount);
                      }

                      if (_iso_type.Type == CurrencyExchangeType.CARD)
                      {
                        CashierSessionData.current_card_withdrawal_total += _amount;
                      }
                   
                    }
                    #endregion
                  }
                  break;

                case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
                  #region
                  _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();

                  // Update fill out balance
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CHECK;

                  if (_fill_out_balance.ContainsKey(_iso_type))
                  {
                    _fill_out_balance[_iso_type] += _amount;
                  }
                  else
                  {
                    _fill_out_balance.Add(_iso_type, _amount);
                  }

                  // Update currencies balance
                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] -= _amount;
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, -_amount);
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1:
                  cash_in_split1 += _amount;
                  _points_as_cashin_split1 += _amount;

                  break;

                case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2:
                  cash_in_split2 += _amount;
                  _points_as_cashin_split2 += _amount;

                  break;

                case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE:
                  prizes_amount += _amount;
                  _points_as_cashin_prize += _amount;

                  break;

                case CASHIER_MOVEMENT.CASH_IN:
                  {
                    //cash_in_amount += _amount;
                    total_amount += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.CASH_OUT:
                  {
                    //cash_out_amount += _amount;
                    total_amount -= _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.DEV_SPLIT1:
                case CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO:
                  {
                    dev_split1 += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.DEV_SPLIT2:
                  {
                    dev_split2 += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.CANCEL_SPLIT1:
                  {
                    cancel_split1 += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.CANCEL_SPLIT2:
                  {
                    cancel_split2 += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.MB_CASH_IN:
                  {
                    // cash_in_amount += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1:
                  {
                    cash_in_split1 += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
                case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
                  {
                    cash_in_split1 += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW:
                  cash_in_split1 -= _amount;
                  participation_in_draw += _amount;
                  break;

                case CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY:
                  {
                    cash_in_split1 += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  break;

                case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2:
                case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
                case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
                  {
                    #region
                    cash_in_split2 += _amount;
                    _company_b_tax.GrossAmount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);

                    if (_dr["CM_AUX_AMOUNT"] != DBNull.Value)
                    {
                      _company_b_tax.Tax += Convert.ToDecimal(_dr["CM_AUX_AMOUNT"]);
                    }
                    #endregion
                  }
                  break;

                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK:
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT:
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT:
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC:
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_UNDO:
                // Charge to company B
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_UNDO:
                  {
                    #region
                    // Card deposit from customers is considered a Cash In

                    cash_in_card_deposit += _amount;

                    total_amount += _amount;
                    _card_tax.GrossAmount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);

                    if (_dr["CM_AUX_AMOUNT"] != DBNull.Value)
                    {
                      _card_tax.Tax += Convert.ToDecimal(_dr["CM_AUX_AMOUNT"]);
                    }

                    // Find if already exist in _currencies_balance
                    _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                    // Update currencies balance
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    switch ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]))
                    {
                      case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
                        _cash_cash_in_card_deposit_split1 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CURRENCY;

                        break;
                      case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK:
                        _check_cash_in_card_deposit_split1 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CHECK;

                        break;
                      case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
                        _currency_cash_in_card_deposit_split1 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CURRENCY;

                        break;
                      case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT:
                        _bank_credit_card_cash_in_card_deposit_split1 += _amount;
                        _bank_card_cash_in_card_deposit_split1 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        break;
                      case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT:
                        _bank_debit_card_cash_in_card_deposit_split1 += _amount;
                        _bank_card_cash_in_card_deposit_split1 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        break;
                      case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC:
                        _bank_card_cash_in_card_deposit_split1 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        break;
                      case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_UNDO:
                        _bank_card_cash_in_card_deposit_split1 += _amount;
                        _bank_card_net_amount_split1 += -(_amount);
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_UNDO:
                        _bank_card_cash_in_card_deposit_split2 += _amount;
                        _bank_card_net_amount_split2 += -(_amount);
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN:
                        _cash_cash_in_card_deposit_split2 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CURRENCY;

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
                        _check_cash_in_card_deposit_split2 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CHECK;

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
                        _currency_cash_in_card_deposit_split2 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CURRENCY;

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
                        _bank_credit_card_cash_in_card_deposit_split2 += _amount;
                        _bank_card_cash_in_card_deposit_split2 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
                        _bank_debit_card_cash_in_card_deposit_split2 += _amount;
                        _bank_card_cash_in_card_deposit_split2 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
                        _bank_card_cash_in_card_deposit_split2 += _amount;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        break;
                    }

                    if (_currency_cash_in_card_deposit.ContainsKey(_iso_type))
                    {
                      _currency_cash_in_card_deposit[_iso_type] += _amount;
                    }
                    else
                    {
                      _currency_cash_in_card_deposit.Add(_iso_type, _amount);
                      if (!_currencies_balance.ContainsKey(_iso_type) && _iso_type.IsoCode != _national_currency)
                      {
                        _currencies_balance.Add(_iso_type, 0);
                      }
                    }

                    #endregion
                  }
                  break;

                case CASHIER_MOVEMENT.CARD_DEPOSIT_OUT:
                  {
                    // Card deposit returned to customers is considered a Cash Out
                    cash_out_card_deposit += _amount;
                    cash_out_card_deposit_split1 += _amount;
                    total_amount -= _amount;
                  }
                  break;

                // Charge to company B
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT:
                  {
                    // Card deposit returned to customers is considered a Cash Out
                    cash_out_card_deposit += _amount;
                    cash_out_card_deposit_split2 += _amount;
                    total_amount -= _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.CARD_ASSOCIATE:
                case CASHIER_MOVEMENT.CARD_REPLACEMENT:
                case CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK:
                case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT:
                case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT:
                case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC:
                // Charge to company B
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC:
                  {
                    #region
                    Decimal _sub_comission = 0;

                    // Find if already exist in _currencies_balance
                    _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                    // Update currencies balance
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    switch ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]))
                    {
                      case CASHIER_MOVEMENT.CARD_REPLACEMENT:
                      case CASHIER_MOVEMENT.CARD_ASSOCIATE:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _cash_cash_in_card_deposit_split1 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CURRENCY;

                        // Commissions
                        _cash_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;
                      case CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _check_cash_in_card_deposit_split1 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CHECK;

                        // Commissions
                        _check_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;
                      case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _bank_credit_card_cash_in_card_deposit_split1 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _bank_card_cash_in_card_deposit_split1 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        // Commissions
                        _bank_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _bank_credit_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;
                      case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _bank_debit_card_cash_in_card_deposit_split1 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _bank_card_cash_in_card_deposit_split1 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        // Commissions
                        _bank_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _bank_debit_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;
                      case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _bank_card_cash_in_card_deposit_split1 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        // Commissions
                        _bank_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;

                      case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _cash_cash_in_card_deposit_split2 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CURRENCY;

                        // Commissions
                        _company_b_cash_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _check_cash_in_card_deposit_split2 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CHECK;

                        // Commissions
                        _company_b_check_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _bank_credit_card_cash_in_card_deposit_split2 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _bank_card_cash_in_card_deposit_split2 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        // Commissions
                        _company_b_credit_bank_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _bank_debit_card_cash_in_card_deposit_split2 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _bank_card_cash_in_card_deposit_split2 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        // Commissions
                        _company_b_debit_bank_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;
                      case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC:
                        _sub_comission = Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                        _bank_card_cash_in_card_deposit_split2 += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                        _iso_type.Type = CurrencyExchangeType.CARD;

                        // Commissions
                        _company_b_bank_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                        break;

                    }

                    // Peronal Card replacements are considered Cash input
                    cash_in_card_deposit += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                    total_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                    _card_tax.GrossAmount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;

                    if (_dr["CM_AUX_AMOUNT"] != DBNull.Value)
                    {
                      _card_tax.Tax += Convert.ToDecimal(_dr["CM_AUX_AMOUNT"]);
                    }

                    // Peronal Card replacement by pay & currency
                    if (_currency_cash_in_card_deposit.ContainsKey(_iso_type))
                    {
                      _currency_cash_in_card_deposit[_iso_type] += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission;
                    }
                    else
                    {
                      _currency_cash_in_card_deposit.Add(_iso_type, Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]) - _sub_comission);
                    }

                    // Commissions
                    if (_currency_cash_in_commissions.ContainsKey(_iso_type))
                    {
                      _currency_cash_in_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                    }
                    else
                    {
                      _currency_cash_in_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                    }

                    // Update currencies balance
                    if ((CASHIER_MOVEMENT)_dr["CM_TYPE"] != CASHIER_MOVEMENT.CARD_REPLACEMENT &&
                        (CASHIER_MOVEMENT)_dr["CM_TYPE"] != CASHIER_MOVEMENT.CARD_ASSOCIATE &&
                        (CASHIER_MOVEMENT)_dr["CM_TYPE"] != CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT)
                    {
                      if (_currencies_balance.ContainsKey(_iso_type))
                      {
                        _currencies_balance[_iso_type] += _amount - _sub_comission;
                      }
                      else
                      {
                        _currencies_balance.Add(_iso_type, _amount - _sub_comission);
                      }
                    }

                    // Currency Cash in Balance
                    if (_currency_cash_in.ContainsKey(_iso_type))
                    {
                      _currency_cash_in[_iso_type] += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                    }
                    else
                    {
                      _currency_cash_in.Add(_iso_type, Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]));
                    }
                    #endregion
                  }
                  break;

                case CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE:
                // RCI 17-FEB-2012: Add new NonRedeemable2(CoverCoupon) amount in the same variable non_redeemable_amount.
                case CASHIER_MOVEMENT.CASH_IN_COVER_COUPON:
                //SSC 08-MAR-2012: NOTE ACCEPTOR
                case CASHIER_MOVEMENT.NA_CASH_IN_NOT_REDEEMABLE:
                // JMM 29-FEB-2012: Add new MB_CASH_IN_NOT_REDEEMABLE movement amount in the same variable non_redeemable_amount.
                case CASHIER_MOVEMENT.MB_CASH_IN_NOT_REDEEMABLE:
                //SSC 08-MAR-2012: NOTE ACCEPTOR
                case CASHIER_MOVEMENT.NA_CASH_IN_COVER_COUPON:
                // JMM 29-FEB-2012: Add new MB_CASH_IN_COVER_COUPON movement amount in the same variable non_redeemable_amount.
                case CASHIER_MOVEMENT.MB_CASH_IN_COVER_COUPON:
                //SSC 13-JUN-2012: Added new movements for Gifts/Points
                case CASHIER_MOVEMENT.POINTS_TO_NOT_REDEEMABLE:
                  _promo_nr += _amount;
                  break;

                // ACC 09-JUL-2012 Add Cancel Promo and Expiration movements.
                case CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE:
                case CASHIER_MOVEMENT.PROMO_CANCEL:
                  _cancel_promo_nr += _amount;
                  break;

                case CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE:
                  _cancel_promo_re += _amount;
                  break;

                case CASHIER_MOVEMENT.REDEEMABLE_DEV_EXPIRED:
                  _redeemable_dev_expired += _amount;
                  break;

                case CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED:
                  _redeemable_prize_expired += _amount;
                  break;

                case CASHIER_MOVEMENT.NOT_REDEEMABLE_EXPIRED:
                  _not_redeemable_expired += _amount;
                  break;

                case CASHIER_MOVEMENT.PROMO_CREDITS:
                  promo_amount += _amount;
                  break;

                //SSC 13-JUN-2012: Added new movements for Gifts/Points
                case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE:
                case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE:
                  {
                    _promo_re += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.PROMO_TO_REDEEMABLE:
                  {
                    _promo_nr_to_re += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.TAX_ON_PRIZE1:
                  {
                    tax_on_prize_1 += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.TAX_ON_PRIZE2:
                  {
                    tax_on_prize_2 += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.TAX_ON_PRIZE3:
                  {
                    tax_on_prize_3 += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON:
                  {
                    tax_returning_on_prize_coupon += _amount;
                  }
                  break;

                // RRB 10-SEP-2012
                case CASHIER_MOVEMENT.DECIMAL_ROUNDING:
                  {
                    decimal_rounding += _amount;
                  }
                  break;

                // RRB 12-SEP-2012
                case CASHIER_MOVEMENT.SERVICE_CHARGE:
                case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE:
                  {
                    service_charge += _amount;
                    service_charge_b += _amount;
                  }
                  break;

                // EOR 19-SEP-2016
                case CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A:
                case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A:
                  {
                    service_charge += _amount;
                    service_charge_a += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.PRIZES:
                  {
                    prizes_amount += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER:
                  {
                    prizes_gross_other += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.HANDPAY:
                case CASHIER_MOVEMENT.MANUAL_HANDPAY:
                  _handpay_tito_hold += _amount;
                  handpays_amount += _amount;
                  break;

                case CASHIER_MOVEMENT.HANDPAY_CANCELLATION:
                case CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION:

                  handpays_cancel_amount += _amount;
                  break;

                case CASHIER_MOVEMENT.CHECK_PAYMENT:
                  {
                    _payments_order_amount_cheque += _amount;
                  }
                  break;

                case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
                  #region
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = (String)_dr["CM_CURRENCY_ISO_CODE"];
                  _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;

                  if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                  {
                    _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                  }

                  if (_chips_purchase.ContainsKey(_iso_type))
                  {
                    _chips_purchase[_iso_type] += _amount;
                  }
                  else
                  {
                    _chips_purchase.Add(_iso_type, _amount);
                  }
                  CashierSessionData.chips_purchase_total += _amount;

                  // Update currencies balance
                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += _amount;
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, _amount);
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE:
                  #region
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = (String)_dr["CM_CURRENCY_ISO_CODE"];
                  _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;

                  _amount = Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);

                  if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                  {
                    _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                  }

                  if (_chips_purchase.ContainsKey(_iso_type))
                  {
                    _chips_purchase[_iso_type] += _amount;
                  }
                  else
                  {
                    _chips_purchase.Add(_iso_type, _amount);
                  }
                  if (_chips_purchase.ContainsKey(_iso_type))
                  {
                    _chips_purchase[_iso_type] += _amount;
                  }

                  // Update currencies balance
                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += _amount;
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, _amount);
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
                  #region
                  // DLM 26-JUN-2018  
                  if (_chips_sale_mode)
                  {
                    //continue;
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = (String)_dr["CM_CURRENCY_ISO_CODE"];
                    _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;

                    if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                    {
                      _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                    }

                    if (_chips_sale.ContainsKey(_iso_type))
                    {
                      _chips_sale[_iso_type] += 0;
                    }
                    else
                    {
                      _chips_sale.Add(_iso_type, 0);
                    }
                    CashierSessionData.chips_sale_total += 0;

                    // Update currencies balance
                    if (_currencies_balance.ContainsKey(_iso_type))
                    {
                      _currencies_balance[_iso_type] += 0;
                    }
                    else
                    {
                      _currencies_balance.Add(_iso_type, +0);
                    }
                  }
                  // DLM 26-JUN-2018
                  else
                  {

                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = (String)_dr["CM_CURRENCY_ISO_CODE"];
                  _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;

                  if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                  {
                    _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                  }



                  if (_chips_sale.ContainsKey(_iso_type))
                  {
                    _chips_sale[_iso_type] += _amount;
                  }
                  else
                  {
                    _chips_sale.Add(_iso_type, _amount);
                  }
                  CashierSessionData.chips_sale_total += _amount;

                  // Update currencies balance
                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] -= _amount;
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, -_amount);
                  }
                  }
                  #endregion
                  

                  break;

                case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE:
                  #region
                  //if (_chips_sale_mode)
                  //{
                  //  continue;
                  //}
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = (String)_dr["CM_CURRENCY_ISO_CODE"];
                  _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;

                  _amount = Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);

                  if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                  {
                    _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                  }

                  if (_chips_sale.ContainsKey(_iso_type))
                  {
                    _chips_sale[_iso_type] += _amount;
                  }
                  else
                  {
                    _chips_sale.Add(_iso_type, _amount);
                  }
                  CashierSessionData.chips_sale_total += _amount;

                  // Update currencies balance
                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] -= _amount;
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, -_amount);
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1:
                  #region
                  _currency_exchange_net_amount_split1 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);

                  // Find if already exist in _currencies_balance
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  // Update currencies balance
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CURRENCY;

                  // Currency Balance
                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Commissions
                  if ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]) == CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1)
                  {
                    _currency_exchange_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _company_b_currency_exchange_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  if (_currency_cash_in_commissions.ContainsKey(_iso_type))
                  {
                    _currency_cash_in_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _currency_cash_in_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                  }

                  // Currency Cash in Balance
                  if (_currency_cash_in.ContainsKey(_iso_type))
                  {
                    _currency_cash_in[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currency_cash_in.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Currency Cash In Exchange
                  if (_national_currency != _iso_type.IsoCode)
                  {
                    if (_currency_cash_in_exchange.ContainsKey(_iso_type))
                    {
                      _currency_cash_in_exchange[_iso_type] += (_amount
                                                                - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                    }
                    else
                    {
                      _currency_cash_in_exchange.Add(_iso_type, (_amount
                                                                - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"])));
                    }
                  }

                  break;

                case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY:
                  // FAV 03-MAY-2016

                  // Currency exchange
                  _currency_exchange_net_amount_split1 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);

                  // Find if already exist in _currencies_balance
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  // Update currencies balance
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CURRENCY;

                  // Currency balance
                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Comision balance
                  _currency_exchange_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);


                  // Currency exchange in foreign currency ('Cambio de divisas')
                  if (_currency_exchange_cash_in.ContainsKey(_iso_type))
                  {
                    _currency_exchange_cash_in[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currency_exchange_cash_in.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Currency exchange comission ('Comision:')
                  if (_currency_exchange_cash_in_commissions.ContainsKey(_iso_type))
                  {
                    _currency_exchange_cash_in_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _currency_exchange_cash_in_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                  }

                  // Currency exchange equivalent in national currency ('Equivale a:')
                  if (_national_currency != _iso_type.IsoCode)
                  {
                    if (_currency_exchange_cash_in_exchange.ContainsKey(_iso_type))
                    {
                      _currency_exchange_cash_in_exchange[_iso_type] += (_amount - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                    }
                    else
                    {
                      _currency_exchange_cash_in_exchange.Add(_iso_type, (_amount - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"])));
                    }
                  }
                  break;
                case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY:
                  // FAV 03-MAY-2016

                  _currency_exchange_net_amount_split1 += 0;

                  // Find if already exist in _currencies_balance
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  // Update currencies balance
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CURRENCY;

                  // Currency balance
                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] -= Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, -Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Comision balance
                  _currency_exchange_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);


                  // Currency exchange in foreign currency ('Devolucion de divisas')
                  if (_currency_exchange_cash_out.ContainsKey(_iso_type))
                  {
                    _currency_exchange_cash_out[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currency_exchange_cash_out.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Currency exchange comission ('Comision:')
                  if (_currency_exchange_cash_out_commissions.ContainsKey(_iso_type))
                  {
                    _currency_exchange_cash_out_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _currency_exchange_cash_out_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                  }

                  // Currency exchange equivalent in national currency ('Equivale a:')
                  if (_national_currency != _iso_type.IsoCode)
                  {
                    if (_currency_exchange_cash_out_exchange.ContainsKey(_iso_type))
                    {
                      _currency_exchange_cash_out_exchange[_iso_type] += (_amount - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                    }
                    else
                    {
                      _currency_exchange_cash_out_exchange.Add(_iso_type, (_amount - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"])));
                    }
                  }
                  break;

                case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN:
                  // FAV 03-MAY-2016
                  _cash_in_currency_exchange += _amount;
                  break;
                case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN_CHANGE:
                  // FAV 10-MAY-2016
                  _cash_in_currency_exchange -= _amount;
                  break;
                case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_OUT:
                  // FAV 03-MAY-2016
                  _cash_out_currency_exchange += _amount;
                  #endregion
                  break;

                // RMS 08-AUG-2014
                case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
                // DLL 15-DEC-2014
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
                // ETP 28-MAR-2017  
                case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE:
                case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE:
                case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE:
                  #region
                  // Update separated movement type values
                  switch ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]))
                  {
                    case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
                    case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE:
                      _bank_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      break;
                    case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                    case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE:
                      _bank_credit_card_net_amount_split1 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                      _bank_credit_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      _bank_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      break;
                    case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                    case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE:
                      _bank_debit_card_net_amount_split1 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                      _bank_debit_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      _bank_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      break;
                    // DLL 15-DEC-2014: Commission company B
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
                      _company_b_bank_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      break;
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                      _bank_debit_card_net_amount_split1 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                      _company_b_debit_bank_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      break;
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                      _bank_credit_card_net_amount_split1 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                      _company_b_credit_bank_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      break;
                  }

                  _bank_card_net_amount_split1 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  // Update currencies balance

                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CARD;

                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Commissions
                  if (_currency_cash_in_commissions.ContainsKey(_iso_type))
                  {
                    _currency_cash_in_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _currency_cash_in_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                  }

                  // Currency Cash in Balance
                  if (_currency_cash_in.ContainsKey(_iso_type))
                  {
                    _currency_cash_in[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currency_cash_in.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Currency Cash In Exchange
                  if (_national_currency != _iso_type.IsoCode)
                  {
                    if (_currency_cash_in_exchange.ContainsKey(_iso_type))
                    {
                      _currency_cash_in_exchange[_iso_type] += (_amount
                                                                - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                    }
                    else
                    {
                      _currency_cash_in_exchange.Add(_iso_type, (_amount
                                                                - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"])));
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:
                case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE:
                  #region
                  _check_net_amount_split1 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                  // Find if already exist in _currencies_balance
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  // Update currencies balance
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CHECK;

                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Commissions
                  if ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]) == CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1 || (CASHIER_MOVEMENT)(_dr["CM_TYPE"]) == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE)
                  {
                    _check_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _company_b_check_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }

                  if (_currency_cash_in_commissions.ContainsKey(_iso_type))
                  {
                    _currency_cash_in_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _currency_cash_in_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                  }

                  // Currency Cash in Balance
                  if (_currency_cash_in.ContainsKey(_iso_type))
                  {
                    _currency_cash_in[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currency_cash_in.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Currency Cash In Exchange
                  if (_national_currency != _iso_type.IsoCode)
                  {
                    if (_currency_cash_in_exchange.ContainsKey(_iso_type))
                    {
                      _currency_cash_in_exchange[_iso_type] += (_amount
                                                                - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                    }
                    else
                    {
                      _currency_cash_in_exchange.Add(_iso_type, (_amount
                                                                - Convert.ToDecimal(_dr["CM_SUB_AMOUNT"])));
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2:
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2:
                  _currency_exchange_net_amount_split2 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                  break;

                // RMS 08-AUG-2014
                case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
                case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
                case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
                // DLL 15-DEC-2014
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2:
                  #region

                  switch ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]))
                  {
                    case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
                      _bank_credit_card_net_amount_split2 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                      break;
                    case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
                      _bank_debit_card_net_amount_split2 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                      break;
                  }

                  _bank_card_net_amount_split2 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                  #endregion
                  break;

                // RAB 01-AUG-2016
                case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO:
                case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2_UNDO:
                  if ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]) == CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO)
                  {
                    _bank_card_net_amount_split1 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]) - Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  }
                  else if ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]) == CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2_UNDO)
                  {
                    _bank_card_net_amount_split2 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                  }

                  _bank_card_commissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                  if (_currency_cash_in_commissions.ContainsKey(_iso_type))
                  {
                    _currency_cash_in_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _currency_cash_in_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                  }
                  break;

                case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
                // DLL 17-DEC-2014
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2:
                  _check_net_amount_split2 += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                  break;

                case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT1:
                  _cash_in_tax_split1 += _amount;
                  break;

                case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT2:
                  _cash_in_tax_split2 += _amount;
                  break;

                // RGR 18-APR-2016
                case CASHIER_MOVEMENT.TAX_CUSTODY:
                  _tax_custody += _amount;
                  break;

                // RGR 26-APR-2016
                case CASHIER_MOVEMENT.TAX_RETURNING_CUSTODY:
                  _tax_custody_return += _amount;
                  break;

                // JML 03-MAR-2016 
                case CASHIER_MOVEMENT.TAX_PROVISIONS:
                  _tax_provisions += _amount;
                  break;

                //TITO Movements
                #region
                // DHA 25-APR-2014 modified/added TITO tickets movements
                case CASHIER_MOVEMENT.TITO_TICKET_COUNTR_PRINTED_CASHABLE:
                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_CASHABLE:
                  _tickets_created += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_cashable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_cashable_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _tickets_created_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  //EOR 08-APR-2016 If is TITA Add CM_SUB_AMOUNT
                  if (GeneralParam.GetBoolean("TITA", "Enabled", false))
                  {
                    _tickets_cashier_printed_cashable_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                    //EOR 08-JUN-2016 
                    _tickets_created_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE:
                  _tickets_created += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_promo_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_promo_redeemable_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _tickets_created_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE:
                  _tickets_created += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_promo_no_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_promo_no_redeemable_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _tickets_created_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE:
                  // FAV 03-NOV-2015
                  _tickets_created += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_promo_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_promo_redeemable_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _tickets_created_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE:
                  // FAV 20-NOV-2015
                  _tickets_created += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_promo_no_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_printed_promo_no_redeemable_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _tickets_created_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;
                case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_CASHABLE:
                  _tickets_created_machine_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_printed_cashable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_printed_cashable_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _tickets_created_machine_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE:
                  _tickets_created_machine_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_printed_promo_no_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_printed_promo_no_redeemable_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _tickets_created_machine_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;
                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE:
                  _tickets_cancelled += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_paid_cashable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_paid_cashable_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  _tickets_cancelled_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE:
                  _tickets_cancelled += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_expired_redeemed_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_expired_redeemed += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  _tickets_cancelled_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE:
                  _tickets_cancelled += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_paid_promo_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_paid_promo_redeemable_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  _tickets_cancelled_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE:
                  _tickets_cancelled += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cashier_expired_redeemed_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_expired_redeemed += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  _tickets_cancelled_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_HANDPAY:
                  _tickets_created_machine_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_created_redeemable_handpay_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_created_amount_handpay += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _tickets_created_machine_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_JACKPOT:
                  _tickets_created_machine_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_created_redeemable_jackpot_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_created_amount_jackpot += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _tickets_created_machine_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_HANDPAY:
                  _tickets_cancelled += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cancelled_redeemable_handpay_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cancelled_amount_handpay += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  handpays_amount += _amount;
                  _tickets_cancelled_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_HANDPAY:
                  _tickets_cancelled += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  handpays_amount += _amount;
                  _tickets_cashier_expired_redeemed_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_expired_redeemed += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  _tickets_cancelled_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_JACKPOT:
                  _tickets_cancelled += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cancelled_redeemable_jackpot_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_cancelled_amount_jackpot += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  handpays_amount += _amount;
                  _tickets_cancelled_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                //case CASHIER_MOVEMENT.HANDPAY_WITHHOLDING:
                //    _tickets_cancelled += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                //    _handpay_tito_hold += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                //    handpays_amount += _amount;
                //    break;


                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_JACKPOT:
                  _tickets_cancelled += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  handpays_amount += _amount;
                  _tickets_cashier_expired_redeemed_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_expired_redeemed += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  _tickets_cancelled_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_CASHABLE:
                  _tickets_played_machine_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_played_cashable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_played_cashable_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  _tickets_played_machine_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE:
                  _tickets_played_machine_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_played_promo_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_played_promo_redeemable_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  _tickets_played_machine_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE:
                  _tickets_played_machine_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_played_promo_no_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_machine_played_promo_no_redeemable_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  _tickets_played_machine_amount += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CREATE_CASHABLE_OFFLINE:
                  _tickets_offline_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_offline_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_REDEEMABLE:
                  _tickets_expired_promo_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_expired_amount_promo_redeemable += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_NOT_REDEEMABLE:
                  _tickets_expired_promo_no_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_expired_amount_promo_no_redeemable += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_REDEEMABLE:
                  _tickets_expired_redeemable_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_expired_amount_redeemable += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  break;
                #endregion

                //EOR 05-APR-2016
                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_SWAP_FOR_CHIPS:
                  _tickets_swap_chips += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  // EOR 20-MAY-2016
                  _tickets_swap_chips_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  break;
                case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN:
                case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE:
                  #region

                  // generate game profit for check and card
                  _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CURRENCY;

                  switch ((Int32)(Decimal)(_dr["CM_CURRENCY_DENOMINATION"]))
                  {
                    case Cage.BANK_CARD_CODE:
                      _iso_type.Type = CurrencyExchangeType.CARD;

                      break;
                    case Cage.CHECK_CODE:
                      _iso_type.Type = CurrencyExchangeType.CHECK;

                      break;
                    case Cage.TICKETS_CODE:
                      _tmp_amount = 0;

                      break;
                  }

                  if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                  {
                    _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                  }

                  if (_currency == _national_currency && _iso_type.Type == CurrencyExchangeType.CURRENCY)
                  {
                    _national_game_profit += _amount;
                  }
                  else
                  {
                    if (_currency == Cage.CHIPS_ISO_CODE)
                    {
                      // Old Chips with X01 Iso code must be adapted to new chips definition
                      _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
                      _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;
                    }

                    if (_currency_game_profit.ContainsKey(_iso_type))
                    {
                      _currency_game_profit[_iso_type] += _amount;
                    }
                    else
                    {
                      if (_amount != 0)
                      {
                        _currency_game_profit.Add(_iso_type, _amount);
                      }
                    }

                    // update currency balance
                    if (_currencies_balance.ContainsKey(_iso_type))
                    {
                      _currencies_balance[_iso_type] += _amount;
                    }
                    else
                    {
                      if (_amount != 0)
                      {
                        _currencies_balance.Add(_iso_type, _amount);
                      }
                    }

                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHABLE_REDEEMED_OFFLINE:
                  _tickets_online_discarded_count += Convert.ToInt32(_dr["CM_TYPE_COUNT"]);
                  _tickets_online_discarded_amount += Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  break;

                case CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL:
                  CashierSessionData.chips_sale_register_total += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]); ;
                  break;

                // RMS 18-SEP-2014
                case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
                case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  // Update currencies balance
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;

                  _cash_advance_check += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                  _iso_type.Type = CurrencyExchangeType.CHECK;

                  if ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]) == CASHIER_MOVEMENT.CASH_ADVANCE_CHECK)
                  {
                    _cash_advance_check_comissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _company_b_cash_advance_check_comissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }

                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  // Currency Cash Comissions
                  if (_currency_cash_in_commissions.ContainsKey(_iso_type))
                  {
                    _currency_cash_in_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _currency_cash_in_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                  }

                  // Currency Cash in Balance
                  if (_currency_cash_in.ContainsKey(_iso_type))
                  {
                    _currency_cash_in[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currency_cash_in.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
                case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
                case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
                case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
                case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
                case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
                  #region
                  switch ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]))
                  {
                    case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
                    case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
                      _cash_advance_credit_card += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);

                      if ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]) == CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD)
                      {
                        _cash_advance_credit_card_comissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      }
                      else
                      {
                        _company_b_cash_advance_credit_card_comissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      }

                      break;

                    case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
                    case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
                      _cash_advance_debit_card += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);

                      if ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]) == CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD)
                      {
                        _cash_advance_debit_card_comissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      }
                      else
                      {
                        _company_b_cash_advance_debit_card_comissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      }
                      break;

                    case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
                    case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
                      _cash_advance_generic_card += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);

                      if ((CASHIER_MOVEMENT)(_dr["CM_TYPE"]) == CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD)
                      {
                        _cash_advance_generic_card_comissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      }
                      else
                      {
                        _company_b_cash_advance_generic_card_comissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                      }
                      break;
                  }

                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = (String)_dr["CM_CURRENCY_ISO_CODE"];
                  _iso_type.Type = CurrencyExchangeType.CARD;

                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }

                  if (_currency_cash_in_commissions.ContainsKey(_iso_type))
                  {
                    _currency_cash_in_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _currency_cash_in_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                  }

                  // Currency Cash in Balance
                  if (_currency_cash_in.ContainsKey(_iso_type))
                  {
                    _currency_cash_in[_iso_type] += Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]);
                  }
                  else
                  {
                    _currency_cash_in.Add(_iso_type, Convert.ToDecimal(_dr["CM_INITIAL_BALANCE"]));
                  }
                  #endregion
                  break;

                // RAB 02-AUG-2016
                case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE_UNDO:
                case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD_UNDO:
                case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD_UNDO:
                case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD_UNDO:
                  _bank_card_net_amount_split2 = -Convert.ToDecimal(_dr["CM_ADD_AMOUNT"]);
                  _cash_advance_generic_card += Convert.ToDecimal(_dr["CM_FINAL_BALANCE"]);
                  _cash_advance_generic_card_comissions += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);

                  if (_currency_cash_in_commissions.ContainsKey(_iso_type))
                  {
                    _currency_cash_in_commissions[_iso_type] += Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]);
                  }
                  else
                  {
                    _currency_cash_in_commissions.Add(_iso_type, Convert.ToDecimal(_dr["CM_SUB_AMOUNT"]));
                  }
                  break;

                case CASHIER_MOVEMENT.CASH_CLOSING_OVER:
                  #region
                  _tmp_amount = _amount;

                  _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CURRENCY;

                  switch ((Int32)(Decimal)(_dr["CM_CURRENCY_DENOMINATION"]))
                  {
                    case Cage.BANK_CARD_CODE:
                      _iso_type.Type = CurrencyExchangeType.CARD;

                      break;
                    case Cage.CHECK_CODE:
                      _iso_type.Type = CurrencyExchangeType.CHECK;

                      break;
                    case Cage.TICKETS_CODE:
                      _tmp_amount = 0;

                      break;
                  }

                  if (_currency == Cage.CHIPS_ISO_CODE)
                  {
                    // Old Chips with X01 Iso code must be adapted to new chips definition
                    _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
                    _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;
                  }
                  else if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                  {
                    _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                  }

                  if (_over_currency.ContainsKey(_iso_type))
                  {
                    _over_currency[_iso_type] += _tmp_amount;
                  }
                  else
                  {
                    _over_currency.Add(_iso_type, _tmp_amount);
                  }

                  if (CashierSessionData.collected_diff != null &&
                    (_iso_type.IsoCode != _national_currency || _iso_type.IsoCode == _national_currency && _iso_type.Type != CurrencyExchangeType.CURRENCY))
                  {
                    if (CashierSessionData.collected_diff.ContainsKey(_iso_type))
                    {
                      CashierSessionData.collected_diff[_iso_type] += _tmp_amount;
                    }
                    else
                    {
                      CashierSessionData.collected_diff.Add(_iso_type, _tmp_amount);
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CASH_CLOSING_SHORT:
                  #region
                  _tmp_amount = _amount;
                  _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CURRENCY;
                  if (_currency == Cage.CHIPS_ISO_CODE)
                  {
                    // Old Chips with X01 Iso code must be adapted to new chips definition
                    _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
                    _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;
                  }
                  else if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                  {
                    _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                  }

                  switch ((Int32)(Decimal)(_dr["CM_CURRENCY_DENOMINATION"]))
                  {
                    case Cage.BANK_CARD_CODE:
                      _iso_type.Type = CurrencyExchangeType.CARD;

                      break;
                    case Cage.CHECK_CODE:
                      _iso_type.Type = CurrencyExchangeType.CHECK;

                      break;
                    case Cage.TICKETS_CODE:
                      _tmp_amount = 0;

                      break;
                  }
                  if (_short_currency.ContainsKey(_iso_type))
                  {
                    _short_currency[_iso_type] += _tmp_amount;
                  }
                  else
                  {
                    _short_currency.Add(_iso_type, _tmp_amount);
                  }

                  if (CashierSessionData.collected_diff != null &&
                    (_iso_type.IsoCode != _national_currency || _iso_type.IsoCode == _national_currency && _iso_type.Type != CurrencyExchangeType.CURRENCY))
                  {
                    if (CashierSessionData.collected_diff.ContainsKey(_iso_type))
                    {
                      CashierSessionData.collected_diff[_iso_type] += -_tmp_amount;
                    }
                    else
                    {
                      CashierSessionData.collected_diff.Add(_iso_type, -_tmp_amount);
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE:
                case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CHECK:
                case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD:
                case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD:
                case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CARD:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;

                  if (_cm_type == CASHIER_MOVEMENT.CUSTOMER_ENTRANCE)
                  {
                    _iso_type.Type = CurrencyExchangeType.CURRENCY;
                  }
                  else if (_cm_type == CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CHECK)
                  {
                    _iso_type.Type = CurrencyExchangeType.CHECK;
                    _entrances_amount_in_check += _amount;

                  }
                  else if (_cm_type == CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD ||
                    _cm_type == CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD ||
                    _cm_type == CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CARD)
                  {
                    _iso_type.Type = CurrencyExchangeType.CARD;
                    _entrances_amount_in_card += _amount;
                    _entrances_amount_in_credit_card += (_cm_type == CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD) ? _amount : 0;
                    _entrances_amount_in_debit_card += (_cm_type == CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD) ? _amount : 0;
                  }

                  if ((_currency == "" || _currency == _national_currency))
                  {
                    _entrances_amount_in += _amount;
                  }
                  else
                  {
                    _entrances_amount_in += Convert.ToDecimal(_dr["CM_AUX_AMOUNT"]);

                    if (_entrances_currency_amount_in.ContainsKey(_iso_type))
                      _entrances_currency_amount_in[_iso_type] += _amount;
                    else
                      _entrances_currency_amount_in.Add(_iso_type, _amount);

                    if (_iso_type.Type == CurrencyExchangeType.CURRENCY)
                    {
                      _entrances_amount_in_currency += Convert.ToDecimal(_dr["CM_AUX_AMOUNT"]);
                    }
                  }

                  if ((_currency == _national_currency && _iso_type.Type != CurrencyExchangeType.CURRENCY) || _currency != _national_currency)
                  {
                    if (_currencies_balance.ContainsKey(_iso_type))
                      _currencies_balance[_iso_type] += _amount;
                    else
                      _currencies_balance.Add(_iso_type, _amount);
                  }

                  if (_currency == _national_currency)
                  {
                    if (_currency_cash_in.ContainsKey(_iso_type))
                      _currency_cash_in[_iso_type] += _amount;
                    else
                      _currency_cash_in.Add(_iso_type, _amount);
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  if ((_currency == "" || _currency == _national_currency) && !FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                  {
                    _concepts_amount_in += _amount;
                  }
                  else
                  {
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    //DLL11111
                    _iso_type.Type = CurrencyExchangeType.CURRENCY;
                    if (_iso_type.IsoCode == Cage.CHIPS_ISO_CODE)
                    {
                      // Old Chips with X01 Iso code must be adapted to new chips definition
                      _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
                      _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;
                    }
                    else if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                    {
                      _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                    }

                    if (_concepts_currency_amount_in.ContainsKey(_iso_type))
                    {
                      _concepts_currency_amount_in[_iso_type] += _amount;
                    }
                    else
                    {
                      _concepts_currency_amount_in.Add(_iso_type, _amount);
                    }
                    // Update currencies balance
                    if (_currencies_balance.ContainsKey(_iso_type))
                    {
                      _currencies_balance[_iso_type] += _amount;
                    }
                    else
                    {
                      _currencies_balance.Add(_iso_type, _amount);
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  if (_currency == "" || _currency == _national_currency && !FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                  {
                    _concepts_amount_out += _amount;
                  }
                  else
                  {
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    _iso_type.Type = CurrencyExchangeType.CURRENCY;
                    if (_iso_type.IsoCode == Cage.CHIPS_ISO_CODE)
                    {
                      // Old Chips with X01 Iso code must be adapted to new chips definition
                      _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
                      _iso_type.Type = CurrencyExchangeType.CASINOCHIP;
                    }
                    else if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type))
                    {
                      _iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_currency_type);
                    }

                    if (_concepts_currency_amount_out.ContainsKey(_iso_type))
                    {
                      _concepts_currency_amount_out[_iso_type] += _amount;
                    }
                    else
                    {
                      _concepts_currency_amount_out.Add(_iso_type, _amount);
                    }

                    // Update currencies balance
                    if (_currencies_balance.ContainsKey(_iso_type))
                    {
                      _currencies_balance[_iso_type] -= _amount;
                    }
                    else
                    {
                      _currencies_balance.Add(_iso_type, _amount);
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  if (_currency == "" || _currency == _national_currency)
                  {
                    _session_transfers_amount_in += _amount;
                  }
                  else
                  {
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    //DLL11111
                    _iso_type.Type = CurrencyExchangeType.CURRENCY;
                    if (_iso_type.IsoCode == Cage.CHIPS_ISO_CODE)
                    {
                      _iso_type.Type = CurrencyExchangeType.CASINOCHIP;
                    }

                    if (_session_transfers_currency_amount_in.ContainsKey(_iso_type))
                    {
                      _session_transfers_currency_amount_in[_iso_type] += _amount;
                    }
                    else
                    {
                      _session_transfers_currency_amount_in.Add(_iso_type, _amount);
                    }
                    // Update currencies balance
                    if (_currencies_balance.ContainsKey(_iso_type))
                    {
                      _currencies_balance[_iso_type] += _amount;
                    }
                    else
                    {
                      _currencies_balance.Add(_iso_type, _amount);
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  if (_currency == "" || _currency == _national_currency)
                  {
                    _session_transfers_amount_out += _amount;
                  }
                  else
                  {
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    //DLL11111
                    _iso_type.Type = CurrencyExchangeType.CURRENCY;
                    if (_iso_type.IsoCode == Cage.CHIPS_ISO_CODE)
                    {
                      _iso_type.Type = CurrencyExchangeType.CASINOCHIP;
                    }

                    if (_session_transfers_currency_amount_out.ContainsKey(_iso_type))
                    {
                      _session_transfers_currency_amount_out[_iso_type] += _amount;
                    }
                    else
                    {
                      _session_transfers_currency_amount_out.Add(_iso_type, _amount);
                    }

                    // Update currencies balance
                    if (_currencies_balance.ContainsKey(_iso_type))
                    {
                      _currencies_balance[_iso_type] -= _amount;
                    }
                    else
                    {
                      _currencies_balance.Add(_iso_type, -_amount);
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  if (_currency == "" || _currency == _national_currency)
                  {
                    _collect_amount_to_cage += _amount;
                  }
                  else
                  {
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    _iso_type.Type = CurrencyExchangeType.CURRENCY;
                    if (_iso_type.IsoCode == Cage.CHIPS_ISO_CODE)
                    {
                      _iso_type.Type = CurrencyExchangeType.CASINOCHIP;
                    }

                    if (_collect_currency_amount_to_cage.ContainsKey(_iso_type))
                    {
                      _collect_currency_amount_to_cage[_iso_type] += _amount;
                    }
                    else
                    {
                      _collect_currency_amount_to_cage.Add(_iso_type, _amount);
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_CASHABLE:
                case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_CASHABLE:
                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE:
                  _machine_tickets_voided += _amount;
                  break;

                case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL_CAGE:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  if (_currency == "" || _currency == _national_currency)
                  {
                    _refill_amount_to_cage += _amount;
                  }
                  else
                  {
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    _iso_type.Type = CurrencyExchangeType.CURRENCY;
                    if (_iso_type.IsoCode == Cage.CHIPS_ISO_CODE)
                    {
                      _iso_type.Type = CurrencyExchangeType.CASINOCHIP;
                    }

                    if (_refill_currency_amount_to_cage.ContainsKey(_iso_type))
                    {
                      _refill_currency_amount_to_cage[_iso_type] += _amount;
                    }
                    else
                    {
                      _refill_currency_amount_to_cage.Add(_iso_type, _amount);
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  if (_currency == "" || _currency == _national_currency)
                  {
                    _collect_amount_to_cashier += _amount;
                  }
                  else
                  {
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    _iso_type.Type = CurrencyExchangeType.CURRENCY;
                    if (_iso_type.IsoCode == Cage.CHIPS_ISO_CODE)
                    {
                      _iso_type.Type = CurrencyExchangeType.CASINOCHIP;
                    }

                    if (_collect_currency_amount_to_cashier.ContainsKey(_iso_type))
                    {
                      _collect_currency_amount_to_cashier[_iso_type] += _amount;
                    }
                    else
                    {
                      _collect_currency_amount_to_cashier.Add(_iso_type, _amount);
                    }

                    // Update currencies balance
                    if (_currencies_balance.ContainsKey(_iso_type))
                    {
                      _currencies_balance[_iso_type] += _amount;
                    }
                    else
                    {
                      _currencies_balance.Add(_iso_type, _amount);
                    }
                  }
                  #endregion
                  break;
                case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL:
                  #region
                  _currency = (String)_dr["CM_CURRENCY_ISO_CODE"];

                  if (_currency == "" || _currency == _national_currency)
                  {
                    _refill_amount_to_cashier += _amount;
                  }
                  else
                  {
                    _iso_type = new CurrencyIsoType();
                    _iso_type.IsoCode = _currency;

                    _iso_type.Type = CurrencyExchangeType.CURRENCY;
                    if (_iso_type.IsoCode == Cage.CHIPS_ISO_CODE)
                    {
                      _iso_type.Type = CurrencyExchangeType.CASINOCHIP;
                    }

                    if (_refill_currency_amount_to_cashier.ContainsKey(_iso_type))
                    {
                      _refill_currency_amount_to_cashier[_iso_type] += _amount;
                    }
                    else
                    {
                      _refill_currency_amount_to_cashier.Add(_iso_type, _amount);
                    }

                    // Update currencies balance
                    if (_currencies_balance.ContainsKey(_iso_type))
                    {
                      _currencies_balance[_iso_type] -= _amount;
                    }
                    else
                    {
                      _currencies_balance.Add(_iso_type, -_amount);
                    }
                  }
                  #endregion
                  break;

                case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE:
                case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE:
                  _machine_tickets_voided += 0;
                  break;

                case CASHIER_MOVEMENT.ACCOUNT_ADD_BLACKLIST:
                case CASHIER_MOVEMENT.ACCOUNT_REMOVE_BLACKLIST:
                  break;

                case CASHIER_MOVEMENT.CREDIT_LINE_MARKER:
                  _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CREDITLINE;
                  _total_credit_line_marker += _amount;

                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += _amount;
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, _amount);
                  }
                  break;

                case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK:
                  _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CREDITLINE;
                  _total_credit_line_payback += _amount;
                  cash_in_split1 += _amount;


                  _amount = -_amount;
                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] -= _amount;
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, -_amount);
                  }

                  break;

                case CASHIER_MOVEMENT.CREDIT_LINE_MARKER_CARD_REPLACEMENT:
                  _currency = _dr["CM_CURRENCY_ISO_CODE"].ToString();
                  _iso_type = new CurrencyIsoType();
                  _iso_type.IsoCode = _currency;
                  _iso_type.Type = CurrencyExchangeType.CREDITLINE;

                  cash_in_card_deposit += _amount;
                  _total_credit_line_marker += _amount;

                  if (_currencies_balance.ContainsKey(_iso_type))
                  {
                    _currencies_balance[_iso_type] += _amount;
                  }
                  else
                  {
                    _currencies_balance.Add(_iso_type, _amount);
                  }
                  
                  break;
                
                case CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT:
                case CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT:
                  break;

                default:
                  #region
                  if ((_cm_type > CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN && _cm_type < CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT)
                    || (_cm_type > CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT && _cm_type <= CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_LAST))
                  {
                    if (_cash_promos_list.Contains(_cm_type.ToString()))
                    {
                      CashierSessionData.cash_promos += _amount;
                    }
                    break;
                  }

                  // RAB 20-JAN-2016: Range 1100 to 1599
                  if (_cm_type >= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION && _cm_type <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET_LAST)
                  {
                    break;
                  }

                  Log.Error("ReadCashierSessionData. Error in operation type (Cashier): " + Convert.ToInt64(_dr["CM_TYPE"]));
                  #endregion
                  break;
              } // switch

              #endregion
            }
            break;

          case 1:
            #region
            switch ((MBMovementType)(_dr["CM_TYPE"]))
            {
              case MBMovementType.ManualChangeLimit:
              case MBMovementType.AutomaticChangeLimit:
                // Nothing to do with these types of movement
                break;

              case MBMovementType.TransferCredit:
                {
                  mb_transfered_amount += _amount;
                }
                break;

              // JMM 29-FEB-2012: As this movements are also inserted on CASHIER_MOVEMENTS, 
              //                  it will be added on switch's case CM_SUB_TYPE=0  
              //case MBMovementType.CreditNonRedeemable:
              //// RCI 20-FEB-2012: Add new NonRedeemable2(CoverCoupon) amount in the same variable non_redeemable_amount.
              //case MBMovementType.CreditCoverCoupon:
              //  {

              //    non_redeemable_amount += _amount;
              //  }
              //  break;
              case MBMovementType.CloseSessionWithCashLost:
                {
                  mb_total_balance_lost += _amount;
                  total_amount -= _amount;
                }
                break;
              case MBMovementType.DepositCash:
                {
                  mb_deposit_amount += _amount;
                  total_amount += _amount;
                }
                break;

              case MBMovementType.CashExcess:
                _mb_cash_over += _amount;
                break;

              case MBMovementType.CashShortFall:
                _mb_shortfall_cash += _amount;
                total_amount -= _amount;
                break;

              case MBMovementType.CreditNonRedeemable:
              case MBMovementType.CloseSessionWithExcess:
              case MBMovementType.CreditCoverCoupon:
              case MBMovementType.CreditNonRedeemable2:
              case MBMovementType.CloseSession:
                // Nothing to do with this type of movement
                break;

              default:
                Log.Error("ReadCashierSessionData. Error in operation type (Mobile Bank): " + Convert.ToInt64(_dr["CM_TYPE"]));
                break;
            }
            #endregion
            break;

          case 2:  // Liabilities
            // RCI 09-APR-2014: Nothing to do with this type of sub type movement
            break;

          default:
            Log.Error("ReadCashierSessionData. Error in operation cm_sub_type: " + Convert.ToInt64(_dr["CM_SUB_TYPE"]));
            break;
        } // switch
        #endregion
      } // while

      #region "Fill Values"
      tax_on_prize_1 += CashierSessionData.prize_taxes_1_pr_re;
      tax_on_prize_2 += CashierSessionData.prize_taxes_2_pr_re;
      tax_on_prize_3 += CashierSessionData.prize_taxes_3_pr_re;

      // RMS 18-SEP-2014
      CashierSessionData.cash_advance_check = _cash_advance_check;
      CashierSessionData.cash_advance_check_comissions = _cash_advance_check_comissions;
      CashierSessionData.company_b_cash_advance_check_comissions = _company_b_cash_advance_check_comissions;

      CashierSessionData.cash_advance_credit_card = _cash_advance_credit_card;
      CashierSessionData.cash_advance_credit_card_comissions = _cash_advance_credit_card_comissions;
      CashierSessionData.company_b_cash_advance_credit_card_comissions = _company_b_cash_advance_credit_card_comissions;

      CashierSessionData.cash_advance_debit_card = _cash_advance_debit_card;
      CashierSessionData.cash_advance_debit_card_comissions = _cash_advance_debit_card_comissions;
      CashierSessionData.company_b_cash_advance_debit_card_comissions = _company_b_cash_advance_debit_card_comissions;

      CashierSessionData.cash_advance_generic_card = _cash_advance_generic_card;
      CashierSessionData.cash_advance_generic_card_comissions = _cash_advance_generic_card_comissions;
      CashierSessionData.company_b_cash_advance_generic_card_comissions = _company_b_cash_advance_generic_card_comissions;

      // SMN 16-SEP-2014
      CashierSessionData.concepts_input = _concepts_amount_in;
      CashierSessionData.concepts_output = _concepts_amount_out;
      CashierSessionData.concepts_currency_input = _concepts_currency_amount_in;
      CashierSessionData.concepts_currency_output = _concepts_currency_amount_out;

      //AVZ 01-FEB-2016
      #region
      if (Entrance.GetReceptionMode() == Entrances.Enums.ENUM_RECEPTION_MODE.TicketWithPrice || _entrances_amount_in != 0)
      {
        //YNM 25-JAN-2015
        CashierSessionData.entrances_input = _entrances_amount_in;
        CashierSessionData.entrances_input_currency = _entrances_amount_in_currency;
        CashierSessionData.entrances_currency_input = _entrances_currency_amount_in;
        CashierSessionData.entrances_input_check = _entrances_amount_in_check;
        CashierSessionData.entrances_input_card = _entrances_amount_in_card;
        CashierSessionData.entrances_input_debit_card = _entrances_amount_in_debit_card;
        CashierSessionData.entrances_input_credit_card = _entrances_amount_in_credit_card;
      }
      #endregion

      #region
      if (Misc.IsGamingHallMode())
      {
        CashierSessionData.refill_amount_to_cage = _refill_amount_to_cage;
        CashierSessionData.collect_amount_to_cage = _collect_amount_to_cage;
        CashierSessionData.refill_amount_to_cashier = _refill_amount_to_cashier;
        CashierSessionData.collect_amount_to_cashier = _collect_amount_to_cashier;
        CashierSessionData.refill_currency_amount_to_cage = _refill_currency_amount_to_cage;
        CashierSessionData.collect_currency_amount_to_cage = _collect_currency_amount_to_cage;
        CashierSessionData.refill_currency_amount_to_cashier = _refill_currency_amount_to_cashier;
        CashierSessionData.collect_currency_amount_to_cashier = _collect_currency_amount_to_cashier;
      }
      #endregion

      // JBC 12-NOV-2014 Shortfall_cash value added 
      pending_amount = (mb_transfered_amount + _mb_cash_over) - mb_deposit_amount - mb_total_balance_lost;

      //JBC 04-DEC-2014
      _total_tax.GrossAmount = _card_tax.GrossAmount + _company_b_tax.GrossAmount;
      _total_tax.Tax = _card_tax.Tax + _card_tax.Tax;

      _b_taxes.Add(Split_Taxes_Amount.TOTAL, _total_tax);
      _b_taxes.Add(Split_Taxes_Amount.CARD, _card_tax);
      _b_taxes.Add(Split_Taxes_Amount.COMPANY_B, _company_b_tax);

      CashierSessionData.split_tax_amounts = _b_taxes;

      if (_gp_pending_cash_partial)
      {
        pending_amount -= _mb_shortfall_cash;
        _mb_cash_over -= _mb_shortfall_cash;
      }

      CashierSessionData.total_cash_over = _mb_cash_over;

      //sub_balance = total_amount + pending_amount - cashier_opening_amount - filler_in_amount + filler_out_amount;
      //final_balance = sub_balance - (tax_on_prize_1 + tax_on_prize_2);
      CashierSessionData.mb_total_balance_lost = mb_total_balance_lost;

      // DEPOSIT + INITIAL
      CashierSessionData.deposits = cashier_opening_amount + filler_in_amount;

      // WITHDRAWS
      CashierSessionData.withdraws = filler_out_amount;

      // CASH IN
      CashierSessionData.a_total_in = cash_in_split1;
      CashierSessionData.participation_in_draw = participation_in_draw;
      CashierSessionData.b_total_in = cash_in_split2;
      CashierSessionData.cards_usage = cash_in_card_deposit;
      CashierSessionData.cards_usage_a = _cash_cash_in_card_deposit_split1 + _check_cash_in_card_deposit_split1 + _bank_card_cash_in_card_deposit_split1 + _bank_credit_card_cash_in_card_deposit_split1 + _bank_debit_card_cash_in_card_deposit_split1;
      CashierSessionData.cards_usage_b = _cash_cash_in_card_deposit_split2 + _check_cash_in_card_deposit_split2 + _bank_card_cash_in_card_deposit_split2 + _bank_credit_card_cash_in_card_deposit_split2 + _bank_debit_card_cash_in_card_deposit_split2;

      CashierSessionData.service_charge = service_charge;

      //EOR 22-SEP-2016
      CashierSessionData.service_charge_a = service_charge_a;
      CashierSessionData.service_charge_b = service_charge_b;

      CashierSessionData.commissions_currency_exchange = _currency_exchange_commissions;
      CashierSessionData.company_b_commissions_currency_exchange = _company_b_currency_exchange_commissions;
      CashierSessionData.total_commission_currency_exchange = _currency_exchange_commissions + _company_b_currency_exchange_commissions;
      CashierSessionData.commissions_bank_card = _bank_card_commissions;
      CashierSessionData.company_b_commissions_bank_card = _company_b_bank_card_commissions;
      CashierSessionData.company_b_commissions_debit_card = _company_b_debit_bank_commissions;
      CashierSessionData.company_b_commissions_credit_card = _company_b_credit_bank_commissions;
      CashierSessionData.total_commission_bank_card = _bank_card_commissions + _company_b_bank_card_commissions
                                                    + _company_b_debit_bank_commissions + _company_b_credit_bank_commissions;
      CashierSessionData.commissions_check = _check_commissions;
      CashierSessionData.company_b_commissions_check = _company_b_check_commissions;
      CashierSessionData.total_commission_check = _check_commissions + _company_b_check_commissions;
      //CashierSessionData.total_commission = _currency_exchange_commissions + _credit_card_commissions + _check_commissions;

      // FAV 24-AUGUST-2015
      CashierSessionData.session_transfers_input = _session_transfers_amount_in;
      CashierSessionData.session_transfers_output = _session_transfers_amount_out;
      CashierSessionData.session_transfers_currency_input = _session_transfers_currency_amount_in;
      CashierSessionData.session_transfers_currency_output = _session_transfers_currency_amount_out;

      CashierSessionData.total_cash_in = cash_in_split1 + cash_in_split2 + cash_in_card_deposit + participation_in_draw + service_charge + CashierSessionData.total_commission + CashierSessionData.entrances_input
                                        + _cash_in_tax_split1 + _cash_in_tax_split2 + _tax_custody + _tax_provisions + CashierSessionData.concepts_input + CashierSessionData.session_transfers_input
                                        + (Misc.IsGamingHallMode() ? (CashierSessionData.collect_amount_to_cashier - CashierSessionData.refill_amount_to_cashier) : 0);

      CashierSessionData.bank_card_amount_only_a = _bank_card_net_amount_split1 + _bank_card_cash_in_card_deposit_split1 + _bank_card_commissions;
      CashierSessionData.bank_card_amount_only_b = _bank_card_net_amount_split2 + _company_b_bank_card_commissions + _company_b_debit_bank_commissions +
                                                   _company_b_credit_bank_commissions + _bank_card_cash_in_card_deposit_split2;
      CashierSessionData.total_bank_card = CashierSessionData.bank_card_amount_only_a + CashierSessionData.bank_card_amount_only_b + CashierSessionData.entrances_input_card;

      CashierSessionData.total_bank_card_with_cash_advance = CashierSessionData.total_bank_card + CashierSessionData.total_cash_advance_bank_card;

      //JML 06-JUL-2015
      CashierSessionData.bank_card_cash_in_card_deposit_split1 = _bank_card_cash_in_card_deposit_split1 + _bank_credit_card_cash_in_card_deposit_split1 + _bank_debit_card_cash_in_card_deposit_split1;
      CashierSessionData.bank_credit_card_cash_in_card_deposit_split1 = _bank_credit_card_cash_in_card_deposit_split1;
      CashierSessionData.bank_debit_card_cash_in_card_deposit_split1 = _bank_debit_card_cash_in_card_deposit_split1;
      CashierSessionData.check_cash_in_card_deposit_split1 = _check_cash_in_card_deposit_split1;
      CashierSessionData.currency_cash_in_card_deposit_split1 = _currency_cash_in_card_deposit_split1;

      CashierSessionData.bank_card_cash_in_card_deposit_split2 = _bank_card_cash_in_card_deposit_split2 + _bank_credit_card_cash_in_card_deposit_split2 + _bank_debit_card_cash_in_card_deposit_split2;
      CashierSessionData.bank_credit_card_cash_in_card_deposit_split2 = _bank_credit_card_cash_in_card_deposit_split2;
      CashierSessionData.bank_debit_card_cash_in_card_deposit_split2 = _bank_debit_card_cash_in_card_deposit_split2;
      CashierSessionData.check_cash_in_card_deposit_split2 = _check_cash_in_card_deposit_split2;
      CashierSessionData.currency_cash_in_card_deposit_split2 = _currency_cash_in_card_deposit_split2;

      CashierSessionData.currency_cash_in_card_deposit = _currency_cash_in_card_deposit;

      // FAV 16-02-2016
      CashierSessionData.total_machine_ticket_voided = _machine_tickets_voided;

      // EOR 09-MAR-2016
      CashierSessionData.tickets_swap_chips = _tickets_swap_chips;
      // EOR 20-MAY-2016
      CashierSessionData.tickets_swap_chips_count = _tickets_swap_chips_count;

      // RMS 08-AUG-2014
      CashierSessionData.bank_credit_card_amount_only_a = _bank_credit_card_net_amount_split1 + _bank_credit_card_commissions + _bank_credit_card_cash_in_card_deposit_split1;
      CashierSessionData.bank_credit_card_amount_only_b = _bank_credit_card_net_amount_split2 + _company_b_credit_bank_commissions + _bank_credit_card_cash_in_card_deposit_split2;
      CashierSessionData.total_bank_credit_card = CashierSessionData.bank_credit_card_amount_only_a + CashierSessionData.bank_credit_card_amount_only_b + CashierSessionData.entrances_input_credit_card;

      CashierSessionData.bank_debit_card_amount_only_a = _bank_debit_card_net_amount_split1 + _bank_debit_card_commissions + _bank_debit_card_cash_in_card_deposit_split1;
      CashierSessionData.bank_debit_card_amount_only_b = _bank_debit_card_net_amount_split2 + _company_b_debit_bank_commissions + _bank_debit_card_cash_in_card_deposit_split2;
      CashierSessionData.total_bank_debit_card = CashierSessionData.bank_debit_card_amount_only_a + CashierSessionData.bank_debit_card_amount_only_b + CashierSessionData.entrances_input_debit_card;

      CashierSessionData.check_amount_only_a = _check_net_amount_split1 + _check_cash_in_card_deposit_split1;
      CashierSessionData.check_amount_only_b = _check_net_amount_split2 + _company_b_cash_advance_check_comissions + _check_cash_in_card_deposit_split2 + _company_b_check_commissions;
      CashierSessionData.total_check = CashierSessionData.check_amount_only_a + CashierSessionData.check_amount_only_b + CashierSessionData.commissions_check + CashierSessionData.entrances_input_check;
      if (!WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
      {
        CashierSessionData.currencies_balance = CurrencyExchange.OrderCurrenciesFromCurrencyConfiguration(_currencies_balance);
      }
      CashierSessionData.currency_exchange_amount_only_a = _currency_exchange_net_amount_split1 + _currency_exchange_commissions + _currency_cash_in_card_deposit_split1;
      CashierSessionData.currency_exchange_amount_only_b = _currency_exchange_net_amount_split2 + _company_b_currency_exchange_commissions + _currency_cash_in_card_deposit_split2;
      CashierSessionData.total_currency_exchange = CashierSessionData.currency_exchange_amount_only_a + CashierSessionData.currency_exchange_amount_only_b + CashierSessionData.entrances_input_currency;

      CashierSessionData.points_as_cashin_only_a = _points_as_cashin_split1;
      CashierSessionData.points_as_cashin_only_b = _points_as_cashin_split2;
      CashierSessionData.points_as_cashin_prize = _points_as_cashin_prize;
      CashierSessionData.points_as_cashin = CashierSessionData.points_as_cashin_only_a + CashierSessionData.points_as_cashin_only_b;

      // DLL 05-AUG-2013
      CashierSessionData.fill_in_balance = _fill_in_balance;
      CashierSessionData.fill_out_balance = _fill_out_balance;

      // ICS 18-SEP-2013
      CashierSessionData.currency_cash_in = _currency_cash_in;
      CashierSessionData.currency_cash_in_commissions = _currency_cash_in_commissions;

      CashierSessionData.currency_cash_in_exchange = _currency_cash_in_exchange;

      // FAV 03-MAY-2016
      CashierSessionData.currency_exchange_cash_in = _currency_exchange_cash_in;
      CashierSessionData.currency_exchange_cash_out = _currency_exchange_cash_out;
      CashierSessionData.cash_in_currency_exchange = _cash_in_currency_exchange;
      CashierSessionData.cash_out_currency_exchange = _cash_out_currency_exchange;

      // JAB 07-JUL-2012: get the value "result_cashier_only_a"
      // CashierSessionData.total_cash_in_only_a = cash_in_split1 + cash_in_card_deposit + CashierSessionData.total_commission_a + _cash_in_tax_split1;
      CashierSessionData.total_cash_in_only_a = cash_in_split1 + _check_cash_in_card_deposit_split1 + _bank_card_cash_in_card_deposit_split1 +
                                                _bank_credit_card_cash_in_card_deposit_split1 + _bank_debit_card_cash_in_card_deposit_split1 + CashierSessionData.total_commission_a +
                                                _cash_in_tax_split1 + _tax_custody + _tax_provisions + service_charge_a + CashierSessionData.cards_usage_a;

      // CASH OUT
      CashierSessionData.a_total_dev = dev_split1 + cancel_split1;
      CashierSessionData.b_total_dev = dev_split2 + cancel_split2;
      CashierSessionData.refund_cards_usage = cash_out_card_deposit;
      CashierSessionData.a_refund_cards_usage = cash_out_card_deposit_split1;
      CashierSessionData.b_refund_cards_usage = cash_out_card_deposit_split2;

      CashierSessionData.prize_payment = (prizes_amount + prizes_gross_other) - (tax_on_prize_1 + tax_on_prize_2 + tax_on_prize_3) + tax_returning_on_prize_coupon;

      //RGR 26-APR-2016
      CashierSessionData.tax_custody_return = _tax_custody_return;

      // RRB 10-SEP-2012
      CashierSessionData.decimal_rounding = decimal_rounding;

      CashierSessionData.total_cash_out = CashierSessionData.a_total_dev +
                                          CashierSessionData.b_total_dev +
                                          CashierSessionData.refund_cards_usage +
                                          CashierSessionData.prize_payment +
                                          CashierSessionData.tax_custody_return +
                                          CashierSessionData.decimal_rounding +
                                          CashierSessionData.concepts_output +
                                          CashierSessionData.session_transfers_output;

      // JAB 07-JUL-2012: get the value "result_cashier_only_a"
      CashierSessionData.total_cash_out_only_a = CashierSessionData.a_total_dev +
                                                 CashierSessionData.a_refund_cards_usage +
                                                 CashierSessionData.prize_payment +
                                                 CashierSessionData.tax_custody_return - service_charge;

      // TOTAL CASH OUT
      CashierSessionData.a_total_cash_out = CashierSessionData.a_total_dev + CashierSessionData.prize_payment + CashierSessionData.tax_custody_return;
      CashierSessionData.b_total_cash_out = CashierSessionData.b_total_dev;

      CashierSessionData.national_game_profit = _national_game_profit;
      CashierSessionData.currency_game_profit = _currency_game_profit;

      // TOTAL IN CASHIER
      CashierSessionData.total_in_cashier = CashierSessionData.deposits
                                          - CashierSessionData.withdraws
                                          + CashierSessionData.total_cash_in
                                          - CashierSessionData.total_cash_out;

      // JAB 11-JUL-2012: get the value "result_cashier_only_a"
      CashierSessionData.total_in_cashier_only_a = CashierSessionData.deposits
                                                 - CashierSessionData.withdraws
                                                 + CashierSessionData.total_cash_in_only_a
                                                 - CashierSessionData.total_cash_out_only_a;

      CashierSessionData.prize_gross = prizes_amount;
      CashierSessionData.prize_gross_other = prizes_gross_other;
      CashierSessionData.prize_taxes_1 = tax_on_prize_1;
      CashierSessionData.prize_taxes_2 = tax_on_prize_2;
      CashierSessionData.prize_taxes_3 = tax_on_prize_3;
      CashierSessionData.prize_taxes = CashierSessionData.prize_taxes_1 + CashierSessionData.prize_taxes_2 + CashierSessionData.prize_taxes_3;
      CashierSessionData.tax_returning_on_prize_coupon = tax_returning_on_prize_coupon;

      CashierSessionData.promo_re = _promo_re;
      CashierSessionData.promo_nr = _promo_nr;
      CashierSessionData.promo_nr_to_re = _promo_nr_to_re;

      // ACC 09-JUL-2012 Add Cancel Promo movements.
      CashierSessionData.cancel_promo_nr = _cancel_promo_nr;
      CashierSessionData.cancel_promo_re = _cancel_promo_re;

      // ACC 09-JUL-2012 Add Expiration movements.
      CashierSessionData.redeemable_dev_expired = _redeemable_dev_expired;
      CashierSessionData.redeemable_prize_expired = _redeemable_prize_expired;
      CashierSessionData.not_redeemable_expired = _not_redeemable_expired;

      CashierSessionData.handpay_payment = handpays_amount;
      CashierSessionData.handpay_canceled = handpays_cancel_amount;

      CashierSessionData.mb_pending = pending_amount;
      CashierSessionData.mb_deposit_amount = mb_deposit_amount;

      CashierSessionData.cash_in_tax_split1 = _cash_in_tax_split1;
      CashierSessionData.cash_in_tax_split2 = _cash_in_tax_split2;

      CashierSessionData.tax_custody = _tax_custody;
      CashierSessionData.tax_provisions = _tax_provisions;

      CashierSessionData.total_tax1 = CashierSessionData.prize_taxes_1 + CashierSessionData.unr_k1_tax1 + CashierSessionData.unr_k2_tax1;
      CashierSessionData.total_tax2 = CashierSessionData.prize_taxes_2 + CashierSessionData.unr_k1_tax2 + CashierSessionData.unr_k2_tax2;
      CashierSessionData.total_tax3 = CashierSessionData.prize_taxes_3 + CashierSessionData.unr_k1_tax3 + CashierSessionData.unr_k2_tax3;

      // DLL 01-APR-2014: Substract cancelation amount at input and output
      #region
      if (GeneralParam.GetBoolean("Cashier", "CancellationAsUndoOperation", false))
      {
        CashierSessionData.a_total_in -= cancel_split1;
        CashierSessionData.a_total_dev -= cancel_split1;

        //// XGJ 28/03/2017
        if (Misc.GetVoucherMode() == 8 && Misc.IsTerminalDrawEnabled())
        {
          CashierSessionData.total_in_b = CashierSessionData.b_total_in;
        }

        CashierSessionData.b_total_in -= cancel_split2;
        CashierSessionData.b_total_dev -= cancel_split2;

        CashierSessionData.total_cash_in -= (cancel_split1 + cancel_split2);
        CashierSessionData.total_cash_out -= (cancel_split1 + cancel_split2);
      }
      #endregion

      CashierSessionData.result_cashier = CashierSessionData.total_in_cashier
                                        - CashierSessionData.deposits
                                        + CashierSessionData.withdraws
                                        - ((CashierSessionData.total_tax1 + CashierSessionData.total_tax2 + CashierSessionData.total_tax3)
                                            + CashierSessionData.cash_in_tax_split1 + CashierSessionData.cash_in_tax_split2
                                            + (CashierSessionData.tax_custody - CashierSessionData.tax_custody_return) //EOR 10-MAY-2016
                                            + CashierSessionData.tax_provisions);


      // JAB 07-JUL-2012: get the value "result_cashier_only_a"
      CashierSessionData.result_cashier_only_a = CashierSessionData.total_in_cashier_only_a
                                               - CashierSessionData.deposits
                                               + CashierSessionData.withdraws
                                               - ((CashierSessionData.total_tax1 + CashierSessionData.total_tax2 + CashierSessionData.total_tax3)
                                                   + CashierSessionData.cash_in_tax_split1
                                                   + (CashierSessionData.tax_custody - CashierSessionData.tax_custody_return) //EOR 10-MAY-2016
                                                   + CashierSessionData.tax_provisions);

      // WITHDRAWS CHEQUE
      CashierSessionData.payment_orders = _payments_order_amount_cheque;

      // CREDITLINES
      CashierSessionData.total_credit_line_marker = _total_credit_line_marker;
      CashierSessionData.total_credit_line_payback = _total_credit_line_payback;

      // RCI 29-JAN-2013: Use cash_net when calculating final_balance
      CashierSessionData.cash_net = CashierSessionData.total_in_cashier
                                  + CashierSessionData.payment_orders
                                  - CashierSessionData.total_currency_exchange
                                  - CashierSessionData.total_bank_card
                                  - CashierSessionData.total_check
        // - CashierSessionData.points_as_cashin
                                  + CashierSessionData.national_game_profit
                                  + CashierSessionData.cash_in_currency_exchange;

      CashierSessionData.cash_net_only_a = CashierSessionData.total_in_cashier_only_a
                                         + CashierSessionData.participation_in_draw
                                         + CashierSessionData.payment_orders
                                         - CashierSessionData.currency_exchange_amount_only_a
                                         - CashierSessionData.bank_card_amount_only_a
                                         - CashierSessionData.check_amount_only_a;
      //- CashierSessionData.points_as_cashin_only_a


      CashierSessionData.final_balance = CashierSessionData.cash_net
        - pending_amount - mb_total_balance_lost
        + _mb_cash_over
        - CashierSessionData.total_cash_advance_bank_card
        - CashierSessionData.cash_advance_check
        - CashierSessionData.cash_advance_check_comissions
        - CashierSessionData.total_credit_line_marker;

      // JAB 11-JUL-2012: get the value "result_cashier_only_a"
      CashierSessionData.final_balance_only_a = CashierSessionData.cash_net_only_a - pending_amount - mb_total_balance_lost;

      // 11-APR-2017  FJC Bug 26082:Sorteo de Máquina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado. (Reopened)
      if (Misc.IsVoucherModeTV())
      {
        CashierSessionData.final_balance_only_a = CashierSessionData.final_balance_only_a
                                                - CashierSessionData.decimal_rounding
                                                - CashierSessionData.service_charge;
      }

      CashierSessionData.final_balance_only_b = CashierSessionData.b_total_in
                                              + CashierSessionData.cards_usage_b
                                              + CashierSessionData.total_commission_b
                                              - CashierSessionData.b_total_cash_out
                                              - CashierSessionData.currency_exchange_amount_only_b
                                              - CashierSessionData.bank_card_amount_only_b
                                              - CashierSessionData.check_amount_only_b
                                              + CashierSessionData.service_charge_b;

      CashierSessionData.report_a_total_in_gross = CashierSessionData.a_total_in + CashierSessionData.cash_in_tax_split1 + CashierSessionData.tax_custody + CashierSessionData.tax_provisions;
      CashierSessionData.report_a_total_in_net = Math.Round(CashierSessionData.a_total_in / (1 + (CashierSessionData.split_a_tax_pct / 100)), 2, MidpointRounding.AwayFromZero);
      CashierSessionData.report_a_total_in_taxes = Math.Round(CashierSessionData.report_a_total_in_net * (CashierSessionData.split_a_tax_pct / 100), 2, MidpointRounding.AwayFromZero);
      CashierSessionData.report_b_total_in_dev_net = Math.Round((CashierSessionData.b_total_in - CashierSessionData.b_total_dev) / (1 + (CashierSessionData.split_b_tax_pct / 100)), 2, MidpointRounding.AwayFromZero);
      CashierSessionData.report_b_total_in_dev_taxes = Math.Round(CashierSessionData.report_b_total_in_dev_net * (CashierSessionData.split_b_tax_pct / 100), 2, MidpointRounding.AwayFromZero);
      CashierSessionData.report_a_total_dev_gross = CashierSessionData.a_total_dev;
      CashierSessionData.report_a_total_dev_net = Math.Round(CashierSessionData.report_a_total_dev_gross / (1 + (CashierSessionData.split_a_tax_pct / 100)), 2, MidpointRounding.AwayFromZero);
      CashierSessionData.report_a_total_dev_taxes = Math.Round(CashierSessionData.report_a_total_dev_net * (CashierSessionData.split_a_tax_pct / 100), 2, MidpointRounding.AwayFromZero);
      CashierSessionData.report_prize_gross = CashierSessionData.prize_gross;
      CashierSessionData.report_prize_taxes_1 = CashierSessionData.prize_taxes_1;
      CashierSessionData.report_prize_taxes_2 = CashierSessionData.prize_taxes_2;
      CashierSessionData.report_prize_taxes_3 = CashierSessionData.prize_taxes_3;
      CashierSessionData.report_a_base_taxes = CashierSessionData.report_a_total_in_net
                                             - CashierSessionData.report_a_total_dev_net
                                             - CashierSessionData.prize_gross;
      CashierSessionData.report_a_taxes = Math.Round(CashierSessionData.report_a_base_taxes * (CashierSessionData.split_a_tax_pct / 100), 2, MidpointRounding.AwayFromZero);
      CashierSessionData.report_a_total_in_cashier = CashierSessionData.report_a_total_in_net
                                                   + CashierSessionData.report_a_total_in_taxes
                                                   - CashierSessionData.report_a_total_dev_gross
                                                   - CashierSessionData.report_prize_gross
                                                   + CashierSessionData.report_prize_taxes_1
                                                   + CashierSessionData.report_prize_taxes_2
                                                   + CashierSessionData.report_prize_taxes_3;
      CashierSessionData.report_b_total_in_cashier = CashierSessionData.report_b_total_in_dev_net
                                                   + CashierSessionData.report_b_total_in_dev_taxes;
      CashierSessionData.report_total_in_cashier = CashierSessionData.report_a_total_in_cashier
                                                 + CashierSessionData.report_b_total_in_cashier;
      CashierSessionData.report_funds_non_redeemable_promotion = CashierSessionData.report_a_total_in_taxes
                                                               + CashierSessionData.report_b_total_in_dev_net
                                                               + CashierSessionData.report_b_total_in_dev_taxes;
      CashierSessionData.chips_purchase = _chips_purchase;
      CashierSessionData.chips_sale = _chips_sale;
      //CashierSessionData.national_game_profit = _national_game_profit;

      //// JML 01-SEP-2015: Collected
      //CashierSessionData.collected = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      //foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency_balance in _collected)
      //{

      //}

      // JML 20-JUL-2015: Short amounts
      CashierSessionData.short_currency = _short_currency;

      // JML 05-AUG-2015: Over amounts
      CashierSessionData.over_currency = _over_currency;

      CashierSessionData.total_cash_in_aux = CashierSessionData.total_cash_in - CashierSessionData.b_total_in;

      CashierSessionData.b_total_dev_only_a = CashierSessionData.total_cash_out - CashierSessionData.b_total_dev;

      //Tickets Info
      #region Tickets Tito
      if (TITO.Utils.IsTitoMode())
      {
        CashierSessionData.tickets_created_count = _tickets_created;
        // ECP 23-NOV-2015 Added Tickets TITO Detail
        CashierSessionData.tickets_created_amount = _tickets_created_amount;
        CashierSessionData.tickets_cancelled_count = _tickets_cancelled;
        CashierSessionData.tickets_cancelled_amount = _tickets_cancelled_amount;

        CashierSessionData.tickets_played_machine_count = _tickets_played_machine_count;
        CashierSessionData.tickets_played_machine_amount = _tickets_played_machine_amount;
        CashierSessionData.tickets_created_machine_count = _tickets_created_machine_count;
        CashierSessionData.tickets_created_machine_amount = _tickets_created_machine_amount;

        // IN
        CashierSessionData.tickets_created_amount_redeemable_handpay = _tickets_created_amount_handpay;
        CashierSessionData.tickets_created_amount_redeemable_jackpot = _tickets_created_amount_jackpot;
        CashierSessionData.tickets_created_redeemable_handpay_count = _tickets_created_redeemable_handpay_count;
        CashierSessionData.tickets_created_redeemable_jackpot_count = _tickets_created_redeemable_jackpot_count;

        //OUT
        CashierSessionData.tickets_cancelled_redeemable_handpay = _tickets_cancelled_amount_handpay;
        CashierSessionData.tickets_cancelled_redeemable_jackpot = _tickets_cancelled_amount_jackpot;
        CashierSessionData.tickets_cancelled_redeemable_handpay_count = _tickets_cancelled_redeemable_handpay_count;
        CashierSessionData.tickets_cancelled_redeemable_jackpot_count = _tickets_cancelled_redeemable_jackpot_count;
        CashierSessionData.handpay_tito_hold = _handpay_tito_hold;

        CashierSessionData.tickets_canceled_or_redeemed_count = _tickets_cancelled + _tickets_offline_count;
        CashierSessionData.tickets_canceled_or_redeemed_amount = _tickets_cancelled_amount + _tickets_offline_amount;

        CashierSessionData.tickets_expired_promo_redeemable = _tickets_expired_amount_promo_redeemable;
        CashierSessionData.tickets_expired_promo_no_redeemable = _tickets_expired_amount_promo_no_redeemable;
        CashierSessionData.tickets_expired_redeemable = _tickets_expired_amount_redeemable;
        CashierSessionData.tickets_expired_promo_redeemable_count = _tickets_expired_promo_redeemable_count;
        CashierSessionData.tickets_expired_promo_no_redeemable_count = _tickets_expired_promo_no_redeemable_count;
        CashierSessionData.tickets_expired_redeemable_count = _tickets_expired_redeemable_count;

        // DHA 25-APR-204 Added Tickets TITO Detail
        CashierSessionData.tickets_cashier_printed_cashable = _tickets_cashier_printed_cashable_amount;
        CashierSessionData.tickets_cashier_printed_promo_redeemable = _tickets_cashier_printed_promo_redeemable_amount;
        CashierSessionData.tickets_cashier_printed_promo_no_redeemable = _tickets_cashier_printed_promo_no_redeemable_amount;
        CashierSessionData.tickets_machine_printed_cashable = _tickets_machine_printed_cashable_amount;
        CashierSessionData.tickets_machine_printed_promo_no_redeemable = _tickets_machine_printed_promo_no_redeemable_amount;
        CashierSessionData.tickets_cashier_paid_cashable = _tickets_cashier_paid_cashable_amount;
        CashierSessionData.tickets_cashier_paid_promo_redeemable = _tickets_cashier_paid_promo_redeemable_amount;
        CashierSessionData.tickets_machine_played_cashable = _tickets_machine_played_cashable_amount;
        CashierSessionData.tickets_machine_played_promo_redeemable = _tickets_machine_played_promo_redeemable_amount;
        CashierSessionData.tickets_machine_played_promo_no_redeemable = _tickets_machine_played_promo_no_redeemable_amount;
        CashierSessionData.tickets_cashier_expired_redeemed = _tickets_expired_redeemed;

        // ECP 20-NOV-2015 Added Tickets TITO Detail
        CashierSessionData.tickets_cashier_printed_cashable_count = _tickets_cashier_printed_cashable_count;
        CashierSessionData.tickets_cashier_printed_promo_redeemable_count = _tickets_cashier_printed_promo_redeemable_count;
        CashierSessionData.tickets_cashier_printed_promo_no_redeemable_count = _tickets_cashier_printed_promo_no_redeemable_count;
        CashierSessionData.tickets_machine_printed_cashable_count = _tickets_machine_printed_cashable_count;
        CashierSessionData.tickets_machine_printed_promo_no_redeemable_count = _tickets_machine_printed_promo_no_redeemable_count;
        CashierSessionData.tickets_cashier_paid_cashable_count = _tickets_cashier_paid_cashable_count;
        CashierSessionData.tickets_cashier_paid_promo_redeemable_count = _tickets_cashier_paid_promo_redeemable_count;
        CashierSessionData.tickets_machine_played_cashable_count = _tickets_machine_played_cashable_count;
        CashierSessionData.tickets_machine_played_promo_redeemable_count = _tickets_machine_played_promo_redeemable_count;
        CashierSessionData.tickets_machine_played_promo_no_redeemable_count = _tickets_machine_played_promo_no_redeemable_count;
        CashierSessionData.tickets_cashier_expired_redeemed_count = _tickets_cashier_expired_redeemed_count;

        // ICS & JML 12-FEB-2014 Cash report: Calculate tickets offline amount
        CashierSessionData.tickets_offline_amount = _tickets_online_discarded_amount - _tickets_offline_amount;

        // ECP 23-NOV-2015 Cash report: Calculate tickets offline count
        CashierSessionData.tickets_offline_count = _tickets_online_discarded_count - _tickets_offline_count;

      }
      #endregion // Tickets Tito
      #endregion

    } // FillCashierSessionDataFromDt

    //// -----------------------------------------------------------------------------------------------------------------------------------------------


    private static Boolean MovementsWithoutActions(CASHIER_MOVEMENT cm_type)
    {
      Boolean _stop_switch = false;

      switch (cm_type)
      {
        case CASHIER_MOVEMENT.CLOSE_SESSION:
        case CASHIER_MOVEMENT.CLOSE_SESSION_CHECK:
        case CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD:
        case CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT:
        case CASHIER_MOVEMENT.PROMO_EXPIRED:
        case CASHIER_MOVEMENT.PROMO_START_SESSION:
        case CASHIER_MOVEMENT.PROMO_END_SESSION:
        case CASHIER_MOVEMENT.DRAW_TICKET_PRINT:
        case CASHIER_MOVEMENT.POINTS_TO_DRAW_TICKET_PRINT:
        case CASHIER_MOVEMENT.CARD_CREATION:
        case CASHIER_MOVEMENT.CARD_PERSONALIZATION:
        case CASHIER_MOVEMENT.PRIZE_COUPON:
        case CASHIER_MOVEMENT.NA_PRIZE_COUPON:
        case CASHIER_MOVEMENT.MB_PRIZE_COUPON:
        case CASHIER_MOVEMENT.PROMOTION_POINT:
        case CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT:
        case CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE:
        // QMP 26-APR-2013: Added account PIN random
        case CASHIER_MOVEMENT.ACCOUNT_PIN_RANDOM:
        case CASHIER_MOVEMENT.CARD_RECYCLED:
        case CASHIER_MOVEMENT.MB_CASH_LOST:
        case CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT:
        case CASHIER_MOVEMENT.REQUEST_TO_TICKET_REPRINT:
        // QMP 02-MAY-2013: Session pending closing
        case CASHIER_MOVEMENT.CASH_PENDING_CLOSING:
        // RCI 27-JUN-2013: Block/Unblock accounts
        case CASHIER_MOVEMENT.ACCOUNT_BLOCKED:
        case CASHIER_MOVEMENT.ACCOUNT_UNBLOCKED:
        // SMN 05-SEP-2013

        case CASHIER_MOVEMENT.CHIPS_PURCHASE:
        case CASHIER_MOVEMENT.CHIPS_SALE:
        // LEM 02-JAN-2014
        case CASHIER_MOVEMENT.TITO_TICKET_REISSUE:
        // FBA 03-JAN-2014: Cage movements
        case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION:
        case CASHIER_MOVEMENT.CAGE_FILLER_IN:
        case CASHIER_MOVEMENT.CAGE_FILLER_OUT:
        case CASHIER_MOVEMENT.CAGE_LAST_MOVEMENT:
        case CASHIER_MOVEMENT.CAGE_OPEN_SESSION:
        case CASHIER_MOVEMENT.CHIPS_CHANGE_IN:
        case CASHIER_MOVEMENT.CHIPS_CHANGE_OUT:
        case CASHIER_MOVEMENT.TRANSFER_CREDIT_IN:
        case CASHIER_MOVEMENT.TRANSFER_CREDIT_OUT:
        // ICS 10-FEB-2014: TITO Movements
        case CASHIER_MOVEMENT.TITO_TICKET_PLAYABLE_REDEEMED_OFFLINE:
        // RCI & DRV 30-JUL-2014
        case CASHIER_MOVEMENT.CHIPS_SALE_WITH_CASH_IN:
        case CASHIER_MOVEMENT.CHIPS_PURCHASE_WITH_CASH_OUT:
        // JBC 02-NOV-2014
        case CASHIER_MOVEMENT.CASH_ADVANCE_CASH:
        case CASHIER_MOVEMENT.HANDPAY_AUTHORIZED:
        case CASHIER_MOVEMENT.HANDPAY_UNAUTHORIZED:
        case CASHIER_MOVEMENT.HANDPAY_VOIDED:
        case CASHIER_MOVEMENT.HANDPAY_VOIDED_UNDONE:
        case CASHIER_MOVEMENT.NOT_PLAYED_RECHARGE_REFUNDED:
        case CASHIER_MOVEMENT.MB_EXCESS:
        case CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST:
        case CASHIER_MOVEMENT.CASH_CLOSING_OVER_DEPRECATED:
        case CASHIER_MOVEMENT.CASH_CLOSING_SHORT_DEPRECATED:
        case CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE:
        case CASHIER_MOVEMENT.HANDPAY_WITHHOLDING:
        case CASHIER_MOVEMENT.TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY:
        case CASHIER_MOVEMENT.TITO_TICKETS_PLAYED_EXCHANGE_DUAL_CURRENCY:
        // RAB 26-FEB-2016
        case CASHIER_MOVEMENT.TITO_VALIDATE_TICKET:
        case CASHIER_MOVEMENT.CAGE_CASHIER_REFILL_HOPPER_TERMINAL:
        case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL:
        case CASHIER_MOVEMENT.CASHIER_CHANGE_STACKER_TERMINAL:
        case CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER:
        // DHA 13-MAY-2016
        case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE:
        case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE_DETAIL:
        // DHA 09-JUN-2016
        case CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK:
        case CASHIER_MOVEMENT.REOPEN_CASHIER:
          // RCI 04-NOV-2010: Nothing to do with these types of movement
          _stop_switch = true;
          break;

        case CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW:
          if (CashDeskDrawBusinessLogic.IsCashPrizeCashDeskDraw(CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00))
          {
            _stop_switch = false;
          }
          else
          {
            _stop_switch = true;
          }

          break;
      }

      return _stop_switch;
    }
  }

  public static class FlashReport
  {
    private static DataTable m_dt_flash_report;
    private static DataTable m_provider_info;
    private static Decimal m_site_payment;
    private static FlashReportValues m_flash_report;

    public enum RowColor
    {
      NONE = 0,
      YELLOW = 1,
      ORANGE = 2,
    }

    public class InputValues
    {
      public List<String> Header;
      public RowColor Row_Color;
      public String Element;
      public String Detail;

      // FAV 15-JUN-2015
      //public Int32? Quantity;
      public Int32? TerminalsConnected;
      public Int32? TerminalsPlayed;

      public Decimal? Cash_In;
      public Decimal? Cash_Out;
      public Decimal? Hold;
      public String Identifier;
      public Decimal Amount;
    }

    public class FlashReportValues
    {
      private Cashier.TYPE_CASHIER_SESSION_STATS m_cashier_session_stats;

      public String TaxName;
      public String CompanyAName;
      public String CompanyBName;
      public String Tax1Name;
      public String Tax2Name;
      public String Tax3Name;

      //Inputs
      public Decimal A1;
      public Decimal A2;
      public Decimal A3;
      public Decimal A4;
      public Decimal A5;
      public Decimal A6;

      public Decimal A0
      {
        get { return A1 + A2 + A3 + A4 + A5; }
      }

      public Decimal A
      {
        get { return A0 - (A1 + A5 + A4); }
      }

      //Outputs
      public Decimal B1;
      public Decimal B2;
      public Decimal B3;
      public Decimal B4;
      public Decimal B5;

      public Decimal B6; //Tax1
      public Decimal B7; //Tax3 
      public Decimal B8; //Tax3

      private Decimal SumOfTaxes
      {
        get { return (B6 + B7 + B8); }
      }

      public Decimal B0
      {
        get { return B1 + B2 + B3 + B4 + B5; }
      }

      public Decimal B
      {
        get { return B0 - (SumOfTaxes); }
      }

      //HandPays
      public Decimal C;

      //Lapsed Reedem
      public Decimal D;

      //Passive
      public Decimal G1;
      public Decimal G2;
      public Decimal G
      {
        get { return G2 - G1; }
      }

      //Cash Movement
      public Decimal H1;
      public Decimal H2;

      public Decimal H
      {
        //get { return (A0) - (B + A6); }
        get { return A0 - A6 - B + (H1 - H2); }
      }
      public Decimal I
      {
        get { return A - B - D; }
      }

      public Decimal J
      {
        //get { return I - (B6 + B7); }
        get { return A - B0; }
      }

      public Decimal K;

      //Pago de sala
      public Decimal L;

      //Promotions      
      public Decimal M1;
      public Decimal M2;
      public Decimal M3;
      public Decimal N;

      // Totals
      public Decimal M
      {
        get { return M1 - (M2 + M3); }
      }
      public Decimal O
      {
        //get { return K - L + G - M; }
        get { return K + G + D - (L + Q + B4); }
      }
      public Decimal P
      {
        get { return J - O; }
      }
      public Decimal Q
      {
        get { return M + N - (A3 + B5); }
      }

      public FlashReportValues(Cashier.TYPE_CASHIER_SESSION_STATS _cashier_session_stats)
      {
        m_cashier_session_stats = _cashier_session_stats;

        AssignValues();
      }

      private void AssignValues()
      {
        TaxName = GeneralParam.GetString("Cashier", "CashInTax.Name", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX"));

        CompanyAName = m_cashier_session_stats.split_a_name;
        CompanyBName = m_cashier_session_stats.split_b_name;
        Tax1Name = m_cashier_session_stats.prize_taxes_1_name;
        Tax2Name = m_cashier_session_stats.prize_taxes_2_name;
        Tax3Name = m_cashier_session_stats.prize_taxes_3_name;

        //Inputs
        A1 = m_cashier_session_stats.cash_in_tax_split1 + m_cashier_session_stats.cash_in_tax_split2;
        A2 = m_cashier_session_stats.a_total_in;
        A3 = m_cashier_session_stats.b_total_in;
        A4 = m_cashier_session_stats.cards_usage + m_cashier_session_stats.cards_usage_b
              + m_cashier_session_stats.cards_usage_b - m_cashier_session_stats.refund_cards_usage;
        A5 = m_cashier_session_stats.service_charge + m_cashier_session_stats.total_commission;
        A6 = m_cashier_session_stats.bank_card_amount_only_a + m_cashier_session_stats.bank_card_amount_only_b // Debit/Credit Recharges without discriminations
              + m_cashier_session_stats.check_amount_only_a + m_cashier_session_stats.check_amount_only_b; // Check Recharges

        //Outputs
        B1 = m_cashier_session_stats.a_total_dev;
        B2 = m_cashier_session_stats.prize_gross;
        B3 = m_cashier_session_stats.tax_returning_on_prize_coupon;
        B4 = m_cashier_session_stats.decimal_rounding;
        B5 = m_cashier_session_stats.b_total_dev;

        //Taxes
        B6 = m_cashier_session_stats.total_tax1;
        B7 = m_cashier_session_stats.total_tax2;
        B8 = m_cashier_session_stats.total_tax3;

        //HandPays
        C = m_cashier_session_stats.handpay_payment;

        //Lapsed Reedem
        D = m_cashier_session_stats.redeemable_dev_expired + m_cashier_session_stats.redeemable_prize_expired;

        //Passive
        G1 = m_cashier_session_stats.liabilities_from.promo_re_balance + m_cashier_session_stats.liabilities_from.promo_nr_balance + m_cashier_session_stats.liabilities_from.re_balance;
        G2 = m_cashier_session_stats.liabilities_to.promo_re_balance + m_cashier_session_stats.liabilities_to.promo_nr_balance + m_cashier_session_stats.liabilities_to.re_balance;

        //H1 = m_cashier_session_stats.deposits_only;
        H2 = m_cashier_session_stats.withdraws;

        //Pagos de sala
        L = m_cashier_session_stats.tax_returning_on_prize_coupon + m_cashier_session_stats.decimal_rounding +
            m_cashier_session_stats.points_as_cashin_prize + m_site_payment;

        //Promotions        
        M1 = m_cashier_session_stats.promo_nr;
        M2 = m_cashier_session_stats.not_redeemable_expired;
        M3 = m_cashier_session_stats.cancel_promo_nr;
        N = m_cashier_session_stats.promo_re - m_cashier_session_stats.cancel_promo_re; //-(A3 + B5);
      }
    }

    public const String COLUMN_HEADER = "CABECERA";
    public const String COLUMN_COLOR = "COLOR";
    public const String COLUMN_ELEMENT = "ELEMENTOS";
    public const String COLUMN_DETAIL = "DETALLE";

    // FAV 15-JUN-2015
    //public const String COLUMN_QUANTITY = "CANTIDAD";
    public const String COLUMN_TERMINALS_CONNECTED = "TERMINALS_CONNECTED";
    public const String COLUMN_TERMINALS_PLAYED = "TERMINALS_PLAYED";

    public const String COLUMN_CASH_IN = "CASH_IN";
    public const String COLUMN_CASH_OUT = "CASH_OUT";
    public const String COLUMN_HOLD = "HOLD";
    public const String COLUMN_IDENTIFIER = "ID";
    public const String COLUMN_AMOUNT = "AMOUNT";

    public const Int32 SQL_COLUMN_PROVIDER = 0;

    // FAV 15-JUN-2015
    //public const Int32 SQL_COLUMN_QUANTITY = 1;
    public const Int32 SQL_COLUMN_TERMINALS_CONNECTED = 1;
    public const Int32 SQL_COLUMN_TERMINALS_PLAYED = 2;

    public const Int32 SQL_COLUMN_CASH_IN = 3;
    public const Int32 SQL_COLUMN_CASH_OUT = 4;
    public const Int32 SQL_COLUMN_HOLD = 5;
    public const Int32 SQL_COLUMN_NETWIN = 6;

    //public static DataTable GetFlashReport(DateTime DateFrom)
    public static DataTable GetFlashReport(Int64 DateFrom)
    {
      DateTime _date_from = new DateTime(DateFrom);
      Decimal _deposit_expected = 0;

      Cashier.TYPE_CASHIER_SESSION_STATS _cashier_session_stats = new Cashier.TYPE_CASHIER_SESSION_STATS();
      m_dt_flash_report = CreateColumnsDataTable();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Misc.IsGamingDayEnabled())
        {
          String _sessions_query = String.Empty;
          _sessions_query = "";
          _sessions_query += "SELECT   CS_SESSION_ID    ";
          _sessions_query += "  FROM   CASHIER_SESSIONS ";
          _sessions_query += " WHERE   CS_GAMING_DAY >= '" + _date_from.ToString("yyyy-MM-dd'T'HH:mm:ss") + "' ";
          _sessions_query += "   AND   CS_GAMING_DAY <  '" + _date_from.AddDays(1).ToString("yyyy-MM-dd'T'HH:mm:ss") + "' ";
          Cashier.ReadCashierSessionData(_db_trx.SqlTransaction, CashierMovementsFilter.GAMING_DAY_RANGE, _sessions_query, _date_from, _date_from.AddDays(1), ref _cashier_session_stats);
        }
        else
        {
          Cashier.ReadCashierSessionData(_db_trx.SqlTransaction, _date_from, _date_from.AddDays(1), ref _cashier_session_stats);
        }

        m_provider_info = GetProviderInfo(_date_from, _db_trx.SqlTransaction);

        m_site_payment = GetSitePayments(_date_from, _db_trx.SqlTransaction);

        // The expected deposit is obtained from the cage movement
        Cage.GetTotalDepositExpected(out _deposit_expected, _date_from, _db_trx.SqlTransaction);
      }

      m_flash_report = new FlashReportValues(_cashier_session_stats);
      m_flash_report.H1 = _deposit_expected;

      AddSales();

      AddPayments();

      AddTotalCash();

      AddHandpaysAndPassive();

      AddProviders();

      AddPromotions();

      AddFinalBalance();

      return m_dt_flash_report;
    }

    private static void AddSales()
    {
      InputValues _input_values;

      //IEJC
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FLASH_REPORT_TAX_NAME");
      if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled")
      || (!String.IsNullOrEmpty(m_flash_report.TaxName) && m_flash_report.A1 >= 0))
      {
        _input_values.Detail = m_flash_report.TaxName;
      }

      _input_values.Identifier = "A1";
      _input_values.Amount = m_flash_report.A1;
      AddRow(_input_values);

      //Deposito de juego
      _input_values = new InputValues();
      _input_values.Detail = m_flash_report.CompanyAName;
      _input_values.Identifier = "A2";
      _input_values.Amount = m_flash_report.A2;
      AddRow(_input_values);

      //Acceso a Maquinas Tragamonedas
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_COMPANY_B_CONCEPT_CONFIG");
      if (GeneralParam.GetBoolean("Cashier", "Split.B.Enabled"))
      {
        _input_values.Detail = m_flash_report.CompanyBName;
      }

      _input_values.Identifier = "A3";
      _input_values.Amount = m_flash_report.A3;
      AddRow(_input_values);

      //Depósito de tarjetatotal_tax1
      _input_values = new InputValues();
      _input_values.Detail = WSI.Common.Misc.GetCardPlayerConceptName();
      _input_values.Identifier = "A4";
      _input_values.Amount = m_flash_report.A4;
      AddRow(_input_values);

      //Otros
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_UC_BANK_OTHER");
      _input_values.Identifier = "A5";
      _input_values.Amount = m_flash_report.A5;
      AddRow(_input_values);

      //Entradas Totales
      _input_values = new InputValues();
      _input_values.Element = Resource.String("STR_FLASH_REPORT_TOTAL_INPUTS");
      _input_values.Detail = "A0 = A1 + A2 + A3 + A4 + A5";
      _input_values.Identifier = "A0";
      _input_values.Amount = m_flash_report.A0;
      AddRow(_input_values);

      //Recargas con tarjetas/cheque
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FLASH_REPORT_CARD_CHECK_CARD");
      _input_values.Identifier = "A6";
      _input_values.Amount = m_flash_report.A6;
      AddRow(_input_values);

      //Total Ventas netas
      _input_values = new InputValues();
      _input_values.Row_Color = RowColor.YELLOW;
      _input_values.Element = Resource.String("STR_FLASH_REPORT_TOTAL_NET_SALES");
      _input_values.Detail = "A = A0 - (A1 + A4 + A5)";
      _input_values.Identifier = "A";
      _input_values.Amount = m_flash_report.A;
      AddRow(_input_values);
    }

    private static void AddPayments()
    {
      InputValues _input_values;

      //Depósito de juego
      _input_values = new InputValues();
      _input_values.Detail = m_flash_report.CompanyAName;
      _input_values.Identifier = "B1";
      _input_values.Amount = m_flash_report.B1;
      AddRow(_input_values);

      //Pago de Premios ( Premios)
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_UC_BANK_PRIZES");
      _input_values.Identifier = "B2";
      _input_values.Amount = m_flash_report.B2;
      AddRow(_input_values);

      //Redondeo por retención
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FRM_LAST_MOVEMENTS_TAX_RETURNING_ON_PRIZE_COUPON");
      _input_values.Identifier = "B3";
      _input_values.Amount = m_flash_report.B3;
      AddRow(_input_values);

      //Redondeo por decimales
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FRM_LAST_MOVEMENTS_DECIMAL_ROUNDING");
      _input_values.Identifier = "B4";
      _input_values.Amount = m_flash_report.B4;
      AddRow(_input_values);

      //Acceso a Maquinas Tragamonedas
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_COMPANY_B_CONCEPT_CONFIG");
      if (GeneralParam.GetBoolean("Cashier", "Split.B.Enabled"))
      {
        _input_values.Detail = m_flash_report.CompanyBName;
      }

      _input_values.Identifier = "B5";
      _input_values.Amount = m_flash_report.B5;
      AddRow(_input_values);

      //Impuesto I.S.R.
      _input_values = new InputValues();
      _input_values.Detail = m_flash_report.Tax1Name;
      _input_values.Identifier = "B6";
      _input_values.Amount = m_flash_report.B6;
      AddRow(_input_values);

      //Impuesto Estatal
      _input_values = new InputValues();
      _input_values.Detail = m_flash_report.Tax2Name;
      _input_values.Identifier = "B7";
      _input_values.Amount = m_flash_report.B7;
      AddRow(_input_values);

      //Third Tax
      _input_values = new InputValues();
      _input_values.Detail = m_flash_report.Tax3Name;
      _input_values.Identifier = "B8";
      _input_values.Amount = m_flash_report.B8;
      AddRow(_input_values);

      //Salidas Totales
      _input_values = new InputValues();
      _input_values.Element = Resource.String("STR_FLASH_REPORT_TOTAL_OUTPUTS");
      _input_values.Detail = "B0 = B1 + B2 + B3 + B4 + B5";
      _input_values.Identifier = "B0";
      _input_values.Amount = m_flash_report.B0;
      AddRow(_input_values);

      //Total Pagos Neta
      _input_values = new InputValues();
      _input_values.Row_Color = RowColor.YELLOW;
      _input_values.Element = Resource.String("STR_FLASH_REPORT_TOTAL_NET_PAYMENTS");
      _input_values.Detail = "B = B0 - (B6 + B7 + B8)";
      _input_values.Identifier = "B";
      _input_values.Amount = m_flash_report.B;
      AddRow(_input_values);
    }

    private static void AddHandpaysAndPassive()
    {
      InputValues _input_values;

      //Pagos Manuales
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_UC_BANK_HANDPAYS");
      _input_values.Identifier = "C";
      _input_values.Amount = m_flash_report.C;
      AddRow(_input_values);

      //Reedimible Caducado (Devolución + Premio)
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FRM_LAST_MOVEMENTS_021");
      _input_values.Identifier = "D";
      _input_values.Amount = m_flash_report.D;
      AddRow(_input_values);

      //Pasivo (Redimible + No Redimible) Inicial
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FLASH_REPORT_LIABILITIES_INITIAL");
      _input_values.Identifier = "G1";
      _input_values.Amount = m_flash_report.G1;
      AddRow(_input_values);

      //Pasivo (Redimible + No Redimible) Final
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FLASH_REPORT_LIABILITIES_FINAL");
      _input_values.Identifier = "G2";
      _input_values.Amount = m_flash_report.G2;
      AddRow(_input_values);

      //Cambio de pasivo
      _input_values = new InputValues();
      _input_values.Row_Color = RowColor.YELLOW;
      _input_values.Element = Resource.String("STR_FLASH_REPORT_LIABILITIES_CHANGE");
      _input_values.Detail = "G = G2 - G1";
      _input_values.Identifier = "G";
      _input_values.Amount = m_flash_report.G;
      AddRow(_input_values);
    }

    private static void AddTotalCash()
    {
      InputValues _input_values;

      // Deposito de caja esperado
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FLASH_REPORT_CASH_DESK_DEPOSIT_EXPECTED");
      _input_values.Identifier = "H1";
      _input_values.Amount = m_flash_report.H1;
      AddRow(_input_values);

      // Retiro de caja esperado
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FLASH_REPORT_CASH_DESK_WITHDRAWAL_EXPECTED");
      _input_values.Identifier = "H2";
      _input_values.Amount = m_flash_report.H2;
      AddRow(_input_values);

      //Flujo de Efectivo
      _input_values = new InputValues();
      _input_values.Element = Resource.String("STR_FLASH_REPORT_CASH_MOVEMENT");
      //_input_values.Detail = "H = A0 - A6 - B";
      _input_values.Detail = "H = A0 - A6 - B + (H1 - H2)";
      _input_values.Identifier = "H";
      _input_values.Amount = m_flash_report.H;
      AddRow(_input_values);

      //Resultado de Caja
      _input_values = new InputValues();
      _input_values.Element = Resource.String("STR_FLASH_REPORT_CASH_PROFIT");
      _input_values.Detail = "I = A - B - D";
      _input_values.Identifier = "I";
      _input_values.Amount = m_flash_report.I;
      AddRow(_input_values);

      //Resultado de Caja Neta
      _input_values = new InputValues();
      _input_values.Row_Color = RowColor.YELLOW;
      _input_values.Element = Resource.String("STR_FLASH_REPORT_CASH_NET_PROFIT");
      //_input_values.Detail = "J = I - (B6 + B7)";
      _input_values.Detail = "J = A - B0";
      _input_values.Identifier = "J";
      _input_values.Amount = m_flash_report.J;
      AddRow(_input_values);
    }

    private static void AddProviders()
    {
      InputValues _input_values;
      List<String> _header_list;

      // FAV 15-JUN-2015
      //Int32 _quantity;
      Int32 _terminalsConnected;
      Int32 _terminalsPlayed;

      Decimal _cash_in;
      Decimal _cash_out;

      // FAV 15-JUN-2015
      //_quantity = 0;
      _terminalsConnected = 0;
      _terminalsPlayed = 0;

      _cash_in = 0;
      _cash_out = 0;

      m_flash_report.K = 0;

      //Header
      _input_values = new InputValues();

      _header_list = new List<String>();
      _header_list.Add(" ");
      _header_list.Add(Resource.String("STR_TERMINAL_REPORT_PROVIDER"));

      // FAV 15-JUN-2015
      //_header_list.Add(Resource.String("STR_CAGE_TEXT_COLUMN_UNITS"));
      _header_list.Add(Resource.String("STR_FLASH_REPORT_CONNECTED"));
      _header_list.Add(Resource.String("STR_FLASH_REPORT_PLAYED"));

      _header_list.Add(Resource.String("STR_MAILING_MACHINE_AND_GAME_REPORT_COLUMN_04"));
      _header_list.Add(Resource.String("STR_MAILING_MACHINE_AND_GAME_REPORT_COLUMN_05"));
      _header_list.Add(Resource.String("STR_FLASH_REPORT_HOLD"));
      _header_list.Add(" ");
      _header_list.Add(Resource.String("STR_MAILING_NETWIN_LABEL"));

      _input_values.Header = _header_list;
      AddRow(_input_values);

      foreach (DataRow _dr in m_provider_info.Rows)
      {
        _input_values = new InputValues();
        _input_values.Detail = (String)_dr[SQL_COLUMN_PROVIDER];

        // FAV 15-JUN-2015
        //_input_values.Quantity = (Int32)_dr[SQL_COLUMN_QUANTITY];
        _input_values.TerminalsConnected = (Int32)_dr[SQL_COLUMN_TERMINALS_CONNECTED];
        _input_values.TerminalsPlayed = (Int32)_dr[SQL_COLUMN_TERMINALS_PLAYED];

        _input_values.Cash_In = (Decimal)_dr[SQL_COLUMN_CASH_IN];
        _input_values.Cash_Out = (Decimal)_dr[SQL_COLUMN_CASH_OUT];
        _input_values.Hold = (Decimal)_dr[SQL_COLUMN_HOLD];
        _input_values.Amount = (Decimal)_dr[SQL_COLUMN_NETWIN];
        AddRow(_input_values);

        // FAV 15-JUN-2015
        //_quantity += (Int32)_input_values.Quantity;
        _terminalsConnected += (Int32)_input_values.TerminalsConnected;
        _terminalsPlayed += (Int32)_input_values.TerminalsPlayed;

        _cash_in += (Decimal)_input_values.Cash_In;
        _cash_out += (Decimal)_input_values.Cash_Out;

        m_flash_report.K += _input_values.Amount;
      }

      //Totales
      _input_values = new InputValues();
      _input_values.Row_Color = RowColor.YELLOW;
      _input_values.Element = Resource.String("STR_FLASH_REPORT_PROVIDERS_TOTAL");
      _input_values.Identifier = "K";

      // FAV 15-JUN-2015
      //_input_values.Quantity = _quantity;
      _input_values.TerminalsConnected = _terminalsConnected;
      _input_values.TerminalsPlayed = _terminalsPlayed;

      _input_values.Cash_In = _cash_in;
      _input_values.Cash_Out = _cash_out;

      if (_input_values.Cash_In != 0)
      {
        _input_values.Hold = ((_input_values.Cash_In - _input_values.Cash_Out) / _input_values.Cash_In) * 100;
      }
      else
      {
        _input_values.Hold = (-_input_values.Cash_Out) * 100;
      }

      _input_values.Amount = m_flash_report.K;
      AddRow(_input_values);

      //Pagos de sala
      _input_values = new InputValues();
      _input_values.Element = Resource.String("STR_SITE_PAYMENTS");
      _input_values.Identifier = "L";
      _input_values.Amount = m_flash_report.L;
      AddRow(_input_values);
    }

    private static void AddPromotions()
    {
      InputValues _input_values;

      //Promo No Redimible Total
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FLASH_REPORT_PROMO_NON_REDEEMABLE_TOTAL");
      _input_values.Identifier = "M1";
      _input_values.Amount = m_flash_report.M1;
      AddRow(_input_values);

      //Promo No Redimible Caducado
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FLASH_REPORT_PROMO_EXPIRED_NON_REDEEMABLE");
      _input_values.Identifier = "M2";
      _input_values.Amount = m_flash_report.M2;
      AddRow(_input_values);

      //Promo No Redimible Cancelado
      _input_values = new InputValues();
      _input_values.Detail = Resource.String("STR_FLASH_REPORT_PROMO_CANCELLED_NON_REDEEMABLE");
      _input_values.Identifier = "M3";
      _input_values.Amount = m_flash_report.M3;
      AddRow(_input_values);

      //Promoción NR
      _input_values = new InputValues();
      _input_values.Element = Resource.String("STR_UC_CARD_BALANCE_PROMOTION_NON_REDEEMABLE");
      _input_values.Detail = "M = M1 - (M2 + M3)";
      _input_values.Identifier = "M";
      _input_values.Amount = m_flash_report.M;
      AddRow(_input_values);

      //Promo Redimibles
      _input_values = new InputValues();
      _input_values.Element = Resource.String("STR_UC_CARD_BALANCE_PROMOTION_REDEEMABLE");
      _input_values.Identifier = "N";
      _input_values.Amount = m_flash_report.N;
      AddRow(_input_values);

      //Total Promociones
      _input_values = new InputValues();
      _input_values.Row_Color = RowColor.YELLOW;
      _input_values.Element = Resource.String("STR_FLASH_REPORT_TOTAL_PROMO");
      _input_values.Detail = "Q = M + N - (A3 + B5)";
      //_input_values.Detail = "Q = M + N";
      _input_values.Identifier = "Q";
      //_input_values.Amount = m_flash_report.Q;
      _input_values.Amount = m_flash_report.Q;
      AddRow(_input_values);
    }

    private static void AddFinalBalance()
    {
      InputValues _input_values;

      //Resultado Neto
      _input_values = new InputValues();
      _input_values.Element = Resource.String("STR_FLASH_REPORT_NET_PROFIT");
      //_input_values.Detail = "O = K + G - L - M ";
      _input_values.Detail = "O = K + G + D -(L + Q + B4)";
      _input_values.Identifier = "O";
      _input_values.Amount = m_flash_report.O;
      AddRow(_input_values);

      //Diferencia
      _input_values = new InputValues();
      _input_values.Row_Color = RowColor.ORANGE;
      _input_values.Element = Resource.String("STR_FLASH_REPORT_DIFFERENCE");
      _input_values.Detail = "P = J - O";
      _input_values.Identifier = "P";
      _input_values.Amount = m_flash_report.P;
      AddRow(_input_values);
    }

    private static DataTable CreateColumnsDataTable()
    {
      DataTable _dt;

      _dt = new DataTable();

      _dt.Columns.Add(COLUMN_HEADER, typeof(List<String>));
      _dt.Columns.Add(COLUMN_COLOR, System.Type.GetType("System.Int32"));
      _dt.Columns.Add(COLUMN_ELEMENT, System.Type.GetType("System.String"));
      _dt.Columns.Add(COLUMN_DETAIL, System.Type.GetType("System.String"));

      // FAV 15-JUN-2015
      //_dt.Columns.Add(COLUMN_QUANTITY, System.Type.GetType("System.Int32"));
      _dt.Columns.Add(COLUMN_TERMINALS_CONNECTED, System.Type.GetType("System.Int32"));
      _dt.Columns.Add(COLUMN_TERMINALS_PLAYED, System.Type.GetType("System.Int32"));

      _dt.Columns.Add(COLUMN_CASH_IN, System.Type.GetType("System.Decimal"));
      _dt.Columns.Add(COLUMN_CASH_OUT, System.Type.GetType("System.Decimal"));
      _dt.Columns.Add(COLUMN_HOLD, System.Type.GetType("System.Decimal"));
      _dt.Columns.Add(COLUMN_IDENTIFIER, System.Type.GetType("System.String"));
      _dt.Columns.Add(COLUMN_AMOUNT, System.Type.GetType("System.Decimal"));

      return _dt;
    }

    private static void AddRow(InputValues InputValues)
    {
      DataRow _row;
      _row = m_dt_flash_report.NewRow();

      _row[COLUMN_HEADER] = InputValues.Header;
      _row[COLUMN_COLOR] = InputValues.Row_Color;
      _row[COLUMN_ELEMENT] = InputValues.Element;
      _row[COLUMN_DETAIL] = InputValues.Detail;

      // FAV 15-JUN-2015
      //if (InputValues.Quantity != null)
      //{
      //  _row[COLUMN_QUANTITY] = InputValues.Quantity;
      //}

      if (InputValues.TerminalsConnected != null)
      {
        _row[COLUMN_TERMINALS_CONNECTED] = InputValues.TerminalsConnected;
      }

      if (InputValues.TerminalsPlayed != null)
      {
        _row[COLUMN_TERMINALS_PLAYED] = InputValues.TerminalsPlayed;
      }

      if (InputValues.Cash_In != null)
      {
        _row[COLUMN_CASH_IN] = InputValues.Cash_In;
      }

      if (InputValues.Cash_Out != null)
      {
        _row[COLUMN_CASH_OUT] = InputValues.Cash_Out;
      }

      if (InputValues.Hold != null)
      {
        _row[COLUMN_HOLD] = InputValues.Hold;
      }

      _row[COLUMN_IDENTIFIER] = InputValues.Identifier;

      _row[COLUMN_AMOUNT] = InputValues.Amount;

      m_dt_flash_report.Rows.Add(_row);
    }

    private static DataTable GetProviderInfo(DateTime DateFrom, SqlTransaction SQLTransaction)
    {
      StringBuilder _sb;
      DataTable _dt;

      _dt = new DataTable();

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT TE_PROVIDER_ID ");
      _sb.AppendLine("     , COUNT(X.TC_TERMINAL_ID)");
      _sb.AppendLine("     , SUM(PLAYED)");
      _sb.AppendLine("     , SUM(TTL_IN)");
      _sb.AppendLine("     , SUM(TTL_OUT)");
      _sb.AppendLine("     , (((SUM(TTL_IN) - SUM(TTL_OUT)) / CASE WHEN SUM(TTL_IN) <> 0 THEN SUM(TTL_IN) ELSE 1 END) * 100)");
      _sb.AppendLine("     , SUM(TTL_IN) - SUM(TTL_OUT)");
      _sb.AppendLine("FROM (SELECT   TC_TERMINAL_ID, TE_PROVIDER_ID ");
      _sb.AppendLine("             , ISNULL(SUM(PS_TOTAL_CASH_IN), 0)   AS TTL_IN ");
      _sb.AppendLine("             , ISNULL(SUM(PS_TOTAL_CASH_OUT), 0)  AS TTL_OUT");
      _sb.AppendLine("             , MAX(CASE WHEN PS_PLAY_SESSION_ID IS NOT NULL  THEN 1 ELSE 0 END) AS PLAYED ");
      _sb.AppendLine("      FROM   TERMINALS_CONNECTED ");
      _sb.AppendLine("        INNER JOIN TERMINALS AS TT ON TC_MASTER_ID = TT.TE_MASTER_ID AND TE_TERMINAL_TYPE IN (@pWin, @pT3gs, @pSasHost, @pCashDeskDraw, @pBonoPlus, @pPariPlay)");
      _sb.AppendLine("        LEFT JOIN PLAY_SESSIONS ON   TE_TERMINAL_ID = PS_TERMINAL_ID ");
      _sb.AppendLine("                               AND   PS_FINISHED >= @pDateFrom");
      _sb.AppendLine("                               AND   PS_FINISHED  < @pDateTo");
      _sb.AppendLine("       WHERE  TC_DATE = @pDate");
      _sb.AppendLine("         AND  TC_CONNECTED = 1");
      _sb.AppendLine("       GROUP  BY TC_TERMINAL_ID, TE_PROVIDER_ID) AS X ");
      _sb.AppendLine("GROUP BY  TE_PROVIDER_ID");
      _sb.AppendLine("ORDER BY  TE_PROVIDER_ID ASC");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SQLTransaction.Connection, SQLTransaction))
        {
          _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = DateFrom.Date;
          _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom;
          _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateFrom.AddDays(1);
          _cmd.Parameters.Add("@pWin", SqlDbType.Int).Value = TerminalTypes.WIN;
          _cmd.Parameters.Add("@pT3gs", SqlDbType.Int).Value = TerminalTypes.T3GS;
          _cmd.Parameters.Add("@pSasHost", SqlDbType.Int).Value = TerminalTypes.SAS_HOST;
          _cmd.Parameters.Add("@pCashDeskDraw", SqlDbType.Int).Value = TerminalTypes.CASHDESK_DRAW;
          _cmd.Parameters.Add("@pBonoPlus", SqlDbType.Int).Value = TerminalTypes.GAMEGATEWAY;
          _cmd.Parameters.Add("@pPariPlay", SqlDbType.Int).Value = TerminalTypes.PARIPLAY;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_dt);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt;
    }

    private static Decimal GetSitePayments(DateTime DateFrom, SqlTransaction SQLTransaction)
    {
      StringBuilder _sb;
      Object _obj;

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   SUM(ISNULL(PS_REDEEMABLE_CASH_OUT, 0) - ISNULL(PS_REDEEMABLE_CASH_IN, 0) ) ");
      _sb.AppendLine("  FROM   PLAY_SESSIONS ");
      _sb.AppendLine("     ,   TERMINALS ");
      _sb.AppendLine(" WHERE   TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID ");
      _sb.AppendLine("   AND   TE_TERMINAL_TYPE IN ( @pSite, @pJackpot) ");
      _sb.AppendLine("   AND   PS_STATUS <> 0 ");
      _sb.AppendLine("   AND   PS_PROMO = 0  ");
      _sb.AppendLine("   AND   PS_FINISHED >= @pDateFrom ");
      _sb.AppendLine("   AND   PS_FINISHED <  @pDateTo");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SQLTransaction.Connection, SQLTransaction))
        {
          _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom;
          _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateFrom.AddDays(1);
          _cmd.Parameters.Add("@pSite", SqlDbType.Int).Value = TerminalTypes.SITE;
          _cmd.Parameters.Add("@pJackpot", SqlDbType.Int).Value = TerminalTypes.SITE_JACKPOT;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            return (Decimal)_obj;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return 0;
    }
  }
}
