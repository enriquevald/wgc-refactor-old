//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GamingTablesDesign.cs
//  
//   DESCRIPTION: 
// 
//        AUTHOR: David Hernández
// 
// CREATION DATE: 23-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAY-2014 DHA    First release.
// 22-JUL-2014 DHA    Fixed Bug #WIG-1111: error when AutoLocationSeats is disabled, seat can't be moved
// 23-JUL-2014 DHA    Fixed Bug #WIG-1115: avoid table with and height with 0 value
// 04-AGU-2014 DHA    Fixed Bug #WIG-1115: reopen - error when table is type3 with heigh/width 1 and have some sides
// 22-SEP-2014 DHA    Fixed Bug #WIG-1253: error when create a new gaming table
// 22-OCT-2014 DHA    Fixed Bug #WIG-1559: error when an user start a new play session, current bet value is not seted yet
// 18-JUL-2016 DHA    Product Backlog Item 15065: Multicurrency on gaming table player tracking
// 29-NOV-2016 ETP    Fixed Bug 21111: Error in fill gaming tables.
// 16-DIC-2016 FOS    Fixed Bug 1705: Control the number of sides when a gamingtable is designed
// 27-JAN-2017 CCG    Fixed Bug 23612:Cajero - Error en la pantalla de mesas de juego
// 06-JUN-2017 DHA    PBI 27760:WIGOS-257: MES21 - Gaming table chair assignment with card selection
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Reflection;
using System.Globalization;
using System.Text.RegularExpressions;

namespace WSI.Common
{
  public enum SEAT_STATUS_PLAYER_TRACKING
  {
    NONE = 0,
    DISABLED = 1,
    FREE = 2,
    OCUPATED = 3,
    WALK = 4,
  }
  public class GamingTableDesign
  {
    public const Int32 BottomMarginGamingTable = 50;
    public const Int32 PanelWidth = 870;
    public const Int32 PanelHeight = 400;
    public const Int32 TableMaxWidht = 550;
    public const Int32 TableMaxHeight = 260;
    public const Int32 TableMinWidht = 50;
    public const Int32 TableMinHeight = 50;
    public const Int32 SeatPositionMinValue = 0;
    public const Int32 SeatPositionMaxValue = 9999;

    public static Color ColorTimerLastUpdatedSeat = Color.White;

    public enum GT_Design_Type
    {
      Type1 = 0,
      Type2 = 1,
      Type3 = 2,
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create default gambling table design
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Int32: NumSeats
    //
    //      - OUTPUT:
    //          - String: TableDesignXml
    //
    // RETURNS:
    //          - Boolean
    //
    //   NOTES:
    public static Boolean DefaultGamingTableXml(Int32 NumSeats, out String TableDesignXml)
    {
      List<IDrawGamingTablePanelObject> _panel_objects;

      TableDesignXml = String.Empty;
      _panel_objects = new List<IDrawGamingTablePanelObject>();

      _panel_objects.Add(new GamingTableObject(null));

      for (Int32 _idx = 0; _idx < NumSeats; _idx++)
      {
        GamingSeatObject _seat = new GamingSeatObject(null);
        _seat.SeatProperties.SeatPosition = _idx + 1;
        _panel_objects.Add(_seat);
      }

      UtilsDesign.AutoLocationSeats(_panel_objects, PanelWidth, PanelHeight);
      UtilsDesign.GetXMLGamingTable(_panel_objects, out TableDesignXml);

      return true;

    }
  }

  public interface IDrawGamingTablePanelObject
  {
    GamingTablePanel Parent
    {
      get;
      set;
    }

    Guid Id
    {
      get;
      set;
    }

    Boolean IsSelected
    {
      get;
      set;
    }

    void DrawObject(Graphics gr, Matrix TransformMatrix);

  }

  public class GamingTablePanel : Panel
  {
    private Boolean m_dragging;
    private Boolean m_can_move;
    private Boolean m_move_seats;
    private Boolean m_mode_design;
    private List<Int32> m_seats_positions;
    private Timer m_timer;
    private Matrix m_transform_matrix;
    private Boolean m_read_only;

    public delegate void SelectedObjectChangedEventHandler(object sender, ObjectPanelEventArgs e);
    public event SelectedObjectChangedEventHandler SelectedObjectChanged;

    public delegate void SelectedSeatEventHandler(Int64? IdSeat, ref Boolean Cancel);
    public event SelectedSeatEventHandler SelectedSeat;

    private List<IDrawGamingTablePanelObject> m_panel_objects = new List<IDrawGamingTablePanelObject>();
    private List<Control> m_panel_controls = new List<Control>();
    public IDrawGamingTablePanelObject SelectedObject = null;
    private Image m_background_picture = null;

    public Image BackgroundPicture
    {
      get
      {
        return m_background_picture;
      }
      set
      {
        m_background_picture = value;
        AdjustSrollSize();
        this.Invalidate();
      }
    }

    public Boolean ReadOnly
    {
      get
      {
        return m_read_only;
      }
      set
      {
        m_read_only = value;
      }
    }

    public Boolean CanMoveSeats
    {
      get
      {
        return m_move_seats;
      }
      set
      {
        m_move_seats = value;
      }
    }

    public Boolean ModeDesign
    {
      get
      {
        return m_mode_design;
      }
      set
      {
        m_mode_design = value;
      }
    }

    public Boolean DoubleBufferred
    {
      get
      {
        return base.DoubleBuffered;
      }
      set
      {
        base.DoubleBuffered = value;
      }
    }

    public Int32 NumSeats
    {
      get
      {
        return GetNumSeats();
      }
    }

    public Boolean IsAutolocationSeatsEnabled
    {
      get
      {
        return GetAutolocationSeatsProperty();
      }
    }

    public GamingTablePanel()
    {
      base.BackColor = Color.DarkGray;
      base.DoubleBuffered = true;
      base.AutoScroll = true;
      base.AutoScrollMinSize = this.Size;
      base.Anchor = (AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom);
      CanMoveSeats = false;
      ModeDesign = false;

      m_timer = new Timer();
      m_timer.Enabled = !m_mode_design;
      m_timer.Interval = 500;
      m_timer.Tick += new EventHandler(Timer_Tick);
    }

    public void Clean()
    {
      m_panel_objects.Clear();
    }

    void Timer_Tick(object sender, EventArgs e)
    {
      m_timer.Stop();
      m_timer.Start();

      // Set color from label current value of seat
      GamingTableDesign.ColorTimerLastUpdatedSeat = GamingTableDesign.ColorTimerLastUpdatedSeat == Color.White ? Color.Orange : Color.White;

      this.Invalidate();
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      float _scale_rate;
      float _translate_x;
      float _translate_y;

      e.Graphics.TranslateTransform(this.AutoScrollPosition.X, this.AutoScrollPosition.Y);

      // Scale Control
      _scale_rate = Math.Min((float)Math.Round(this.Width / (float)GamingTableDesign.PanelWidth, 2), (float)Math.Round(this.Height / (float)GamingTableDesign.PanelHeight, 2));
      _translate_x = (this.Width - (float)GamingTableDesign.PanelWidth * (_scale_rate)) / 2;
      _translate_y = (this.Height - (float)GamingTableDesign.PanelHeight * (_scale_rate)) / 2;
      m_transform_matrix = new Matrix();
      m_transform_matrix.Translate(_translate_x, _translate_y);
      m_transform_matrix.Scale(_scale_rate, _scale_rate);
      e.Graphics.MultiplyTransform(m_transform_matrix);

      // Background
      if (this.m_background_picture != null) e.Graphics.DrawImage(this.m_background_picture, 0, 0);

      // Draw gaming table components
      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        // Set default name on edit mode
        if (m_mode_design && _object.GetType() == typeof(GamingSeatObject))
        {
          ((GamingSeatObject)_object).SeatProperties.AccountName = Resource.String("STR_CASHIER_GAMING_TABLE_PLAYER_UNKNOWN_BUTTON");
        }
        _object.DrawObject(e.Graphics, m_transform_matrix);
      }

      // Set seats auto location
      AutoLocationSeats();

      if (!this.Enabled && SelectedObject != null)
      {
        Boolean _cancel_selection = false;
        SelectedObject = null;
        SetSelectedObject(null);
        SelectedSeat(null, ref _cancel_selection);
      }

      if (ReadOnly)
      {
        SelectedObject = null;
        SetSelectedObject(SelectedObject);
      }

      base.OnPaint(e);
    }

    private void AdjustSrollSize()
    {
      if (m_background_picture == null) this.AutoScrollMinSize = new Size(0, 0);
      else
      {
        Size size = m_background_picture.Size;
        using (Graphics gr = this.CreateGraphics())
        {
          size.Width = (int)(size.Width * gr.DpiX / m_background_picture.HorizontalResolution);
          size.Height = (int)(size.Height * gr.DpiY / m_background_picture.VerticalResolution);
        }
        this.AutoScrollMinSize = size;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Add object to panel
    //
    //  PARAMS:
    //      - INPUT: 
    //          - IDrawGamingTablePanelObject: NewObject
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void AddDrawableObject(IDrawGamingTablePanelObject NewObject)
    {
      m_panel_objects.Add(NewObject);

      if (NewObject.GetType() == typeof(GamingTableObject))
      {
        m_can_move = !((GamingTableObject)NewObject).TableProperties.AutoLocationSeats;
      }
      else if (NewObject.GetType() == typeof(GamingSeatObject))
      {
        if (!((GamingSeatObject)NewObject).SeatProperties.SeatPosition.HasValue)
        {
          Int32 _max_position = 1;

          if (m_seats_positions.Count != 0)
          {
            m_seats_positions.Sort();
            _max_position = m_seats_positions[m_seats_positions.Count - 1] + 1;
          }

          ((GamingSeatObject)NewObject).SeatProperties.SeatPosition = _max_position;
          m_seats_positions.Add(_max_position);
        }

      }

      this.Invalidate();
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Update object
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Object Properties
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void UpdateObject(Object Properties)
    {
      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        if (_object.GetType() == typeof(GamingTableObject) && Properties.GetType() == typeof(GamingTableProperties))
        {
          if (((GamingTableProperties)Properties).Id == ((GamingTableObject)_object).TableProperties.Id)
          {
            ((GamingTableObject)_object).TableProperties = (GamingTableProperties)Properties;
            m_can_move = !((GamingTableProperties)Properties).AutoLocationSeats;
            break;
          }
        }
        else if (_object.GetType() == typeof(GamingSeatObject) && Properties.GetType() == typeof(GamingSeatProperties))
        {
          if (((GamingSeatProperties)Properties).Id == ((GamingSeatObject)_object).SeatProperties.Id)
          {
            ((GamingSeatObject)_object).SeatProperties = (GamingSeatProperties)Properties;

            if (((GamingSeatObject)_object).SeatProperties.Enabled)
            {
              ((GamingSeatObject)_object).SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.NONE;
            }
            else
            {
              ((GamingSeatObject)_object).SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.DISABLED;
            }

            // Check limits for new location
            Point MPosition = new Point();

            MPosition = UtilsDesign.CheckMaxPosition(((GamingSeatObject)_object).SeatProperties, GamingTableDesign.PanelWidth, GamingTableDesign.PanelHeight);

            ((GamingSeatObject)_object).SeatProperties.LocationX = MPosition.X;
            ((GamingSeatObject)_object).SeatProperties.LocationY = MPosition.Y;

            UtilsDesign.SetSeatColorByStatus(((GamingSeatObject)_object).SeatProperties);
            break;
          }
        }
      }

      if (m_panel_objects.Count > 0)
      {
        AutoLocationSeats();
      }

      this.Invalidate();
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      base.OnEnabledChanged(e);

      m_timer.Enabled = this.Enabled;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Num seats
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    private Int32 GetNumSeats()
    {
      Int32 _num_seats;

      _num_seats = 0;

      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        if (_object.GetType() == typeof(GamingSeatObject))
        {
          _num_seats++;
        }
      }
      return _num_seats;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Is autolocation seats enabled
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    private Boolean GetAutolocationSeatsProperty()
    {
      Boolean _is_autolocation_seats;

      _is_autolocation_seats = false;

      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        if (_object.GetType() == typeof(GamingTableObject))
        {
          _is_autolocation_seats = ((GamingTableObject)_object).TableProperties.AutoLocationSeats;
          break;
        }
      }
      return _is_autolocation_seats;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update gaming table data
    //
    //  PARAMS:
    //      - INPUT: 
    //          - GamingTable: GamingTable
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void UpdateGamingTableAndSeatInfo(GamingTable GamingTable)
    {
      if (GamingTable != null)
      {
        UpdateTableInfo(GamingTable);
      }

      if (GamingTable != null && GamingTable.Seats != null && GamingTable.Seats.Count > 0)
      {
        foreach (Seat _seat in GamingTable.Seats.Values)
        {
          if (_seat.PlayerType != GTPlayerType.Unknown && _seat.PlaySession != null && _seat.PlaySession.PlaySessionId > 0)
          {
            UpdateSeatInfo(_seat, GamingTable.TableIdlePlays, _seat.PlaySession.CurrentBet, GamingTable.TableAceptedIsoCode);
          }
          else
          {
            UpdateSeatInfo(_seat, GamingTable.TableIdlePlays);
          }
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update seat data
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Seat: Seat
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void UpdateSeatInfo(Seat Seat)
    {
      UpdateSeatInfo(Seat, null);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update seat data
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Seat: Seat
    //          - Int32: GamingTableIdlePlays
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void UpdateSeatInfo(Seat Seat, Int32? GamingTableIdlePlays)
    {
      UpdateSeatInfo(Seat, null, null, null);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update seat data
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Seat: Seat
    //          - Int32: GamingTableIdlePlays
    //          - Currency: CurrentBet
    //          - Currency: TableMinBet
    //          - Currency: TableMaxBet
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void UpdateSeatInfo(Seat Seat, Int32? GamingTableIdlePlays, Currency? CurrentBet, TableLimitsDictionary TableLimitsDic)
    {
      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        if (_object.GetType() == typeof(GamingSeatObject) && Seat.SeatId == ((GamingSeatObject)_object).SeatProperties.SeatId)
        {
          ((GamingSeatObject)_object).SeatProperties.AccountName = Seat.FirstName;
          ((GamingSeatObject)_object).SeatProperties.BottomLabelFirstLine = Currency.Format(Seat.PlaySession.CurrentBet, Seat.PlaySession.PlayerIsoCode);
          ((GamingSeatObject)_object).SeatProperties.BottomLabelSecondLine = String.Format("{0} x {1}", Currency.Format(Seat.PlaySession.PlayedAvg, Seat.PlaySession.PlayerIsoCode), Seat.PlaySession.PlaysCount);
          ((GamingSeatObject)_object).SeatProperties.Plays = Seat.PlaySession.PlaysCount;
          ((GamingSeatObject)_object).SeatProperties.InSessionLastPlay = Seat.PlaySession.LastPlaysCount;

          if (GamingTableIdlePlays.HasValue)
          {
            ((GamingSeatObject)_object).SeatProperties.TableIdlePlays = GamingTableIdlePlays.Value;
          }

          if (CurrentBet.HasValue && TableLimitsDic != null && TableLimitsDic.ContainsKey(Seat.PlaySession.PlayerIsoCode))
          {
            ((GamingSeatObject)_object).SeatProperties.CurrentBet = CurrentBet.Value;
            ((GamingSeatObject)_object).SeatProperties.TableMinBet = TableLimitsDic[Seat.PlaySession.PlayerIsoCode].MinBet;
            ((GamingSeatObject)_object).SeatProperties.TableMaxBet = TableLimitsDic[Seat.PlaySession.PlayerIsoCode].MaxBet;
          }

          if (!((GamingSeatObject)_object).SeatProperties.Enabled)
          {
            ((GamingSeatObject)_object).SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.DISABLED;
          }
          else if (Seat.PlayerType == GTPlayerType.Unknown)
          {
            ((GamingSeatObject)_object).SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.FREE;
          }
          else if (Seat.PlaySession.Started_walk != null)
          {
            ((GamingSeatObject)_object).SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.WALK;
          }
          else
          {
            ((GamingSeatObject)_object).SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.OCUPATED;
          }

          UtilsDesign.SetSeatColorByStatus(((GamingSeatObject)_object).SeatProperties);
          break;
        }
      }

      this.Invalidate();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update table data
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Seat: Seat
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void UpdateTableInfo(GamingTable GamingTable)
    {
      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        if (_object is GamingTableObject)
        {
          ((GamingTableObject)_object).TableProperties.TableIdlePlays = GamingTable.TableIdlePlays;
          break;
        }
      }

      this.Invalidate();
    }

    public void OnSelectedObjectChanged(object sender, ObjectPanelEventArgs e)
    {
      if (SelectedObjectChanged != null)
      {
        SelectedObjectChanged(sender, e);
      }
    }

    public void OnSelectedSeat(Int64? SeatId, ref Boolean Cancel)
    {
      if (SelectedSeat != null)
      {
        SelectedSeat(SeatId, ref Cancel);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Auto location seats
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void AutoLocationSeats()
    {
      UtilsDesign.AutoLocationSeats(m_panel_objects, GamingTableDesign.PanelWidth, GamingTableDesign.PanelHeight);
    }

    public void SetGamingTableAsSelectedObject()
    {
      IDrawGamingTablePanelObject _selected_object;

      _selected_object = SelectedObject;

      for (Int32 _idx_object = 0; _idx_object <= m_panel_objects.Count - 1; _idx_object++)
      {
        if (m_panel_objects[_idx_object] is GamingTableObject && ModeDesign)
        {
          _selected_object = m_panel_objects[_idx_object];
          break;
        }
      }

      SelectedObject = _selected_object;
      SetSelectedObject(SelectedObject);

      Timer_Tick(null, null);

      // Selected object
      if (SelectedObjectChanged != null)
      {
        SelectedObjectChanged(this, new ObjectPanelEventArgs(SelectedObject));
      }
    }

    /// <summary>
    /// Set to gaming table panel selected seat
    /// </summary>
    /// <param name="SeatId"></param>
    public void SetSelectedSeatAtGamingTable(Int64 SeatId)
    {
      IDrawGamingTablePanelObject _selected_object;

      _selected_object = SelectedObject;

      for (Int32 _idx_object = 0; _idx_object <= m_panel_objects.Count - 1; _idx_object++)
      {
        if (m_panel_objects[_idx_object] is GamingSeatObject && ((GamingSeatObject)m_panel_objects[_idx_object]).SeatProperties.SeatId == SeatId)
        {
          _selected_object = m_panel_objects[_idx_object];
          break;
        }
      }

      SelectedObject = _selected_object;
      SetSelectedObject(SelectedObject);

      Timer_Tick(null, null);

      // Selected object
      if (SelectedObjectChanged != null)
      {
        SelectedObjectChanged(this, new ObjectPanelEventArgs(SelectedObject));
      }
    }

    /// <summary>
    ///  Clear seat selection on gaming table
    /// </summary>
    public void CleanSeatSelection()
    {
      SelectedObject = null;
      SetSelectedObject(null);

    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      Region _object_region;
      Boolean _cancel_selection;
      IDrawGamingTablePanelObject _selected_object;

      _selected_object = SelectedObject;
      _cancel_selection = false;

      if (ReadOnly)
      {
        return;
      }

      for (Int32 _idx_object = m_panel_objects.Count - 1; _idx_object >= 0; _idx_object--)
      {
        if (m_panel_objects[_idx_object].GetType() == typeof(GamingTableObject) && ModeDesign)
        {
          if (((GamingTableObject)m_panel_objects[_idx_object]).TableProperties.Path != null)
          {
            _object_region = new Region(((GamingTableObject)m_panel_objects[_idx_object]).TableProperties.Path);
            if (_object_region.IsVisible(new Point(((MouseEventArgs)e).X, ((MouseEventArgs)e).Y)))
            {
              _selected_object = m_panel_objects[_idx_object];
            }
          }
        }
        else if (m_panel_objects[_idx_object].GetType() == typeof(GamingSeatObject))
        {
          if (((GamingSeatObject)m_panel_objects[_idx_object]).SeatProperties.Path != null)
          {
            _object_region = new Region(((GamingSeatObject)m_panel_objects[_idx_object]).SeatProperties.Path);
            if (_object_region.IsVisible(new Point(((MouseEventArgs)e).X, ((MouseEventArgs)e).Y)))
            {
              if (!ModeDesign && ((GamingSeatObject)m_panel_objects[_idx_object]).SeatProperties.Enabled)
              {
                _selected_object = m_panel_objects[_idx_object];
              }
              else if (ModeDesign)
              {
                _selected_object = m_panel_objects[_idx_object];
              }
              break;
            }
          }
        }
      }

      // Selected Seat event
      if (SelectedSeat != null && _selected_object != null && _selected_object.GetType() == typeof(GamingSeatObject) && ((GamingSeatObject)_selected_object).SeatProperties.Enabled)
      {
        SelectedSeat(((GamingSeatObject)_selected_object).SeatProperties.SeatId, ref _cancel_selection);
      }

      if (!_cancel_selection)
      {
        // Disable selected color for seat
        if (SelectedObject != _selected_object)
        {
          if (SelectedObject != null && SelectedObject is GamingSeatObject)
          {
            ((GamingSeatObject)SelectedObject).SeatProperties.BorderOuterColor = Color.White;
            UtilsDesign.SetSeatColorByStatus(((GamingSeatObject)SelectedObject).SeatProperties);
          }
          SelectedObject = _selected_object;
          SetSelectedObject(SelectedObject);

          Timer_Tick(null, null);

          // Selected object
          if (SelectedObjectChanged != null)
          {
            SelectedObjectChanged(this, new ObjectPanelEventArgs(SelectedObject));
          }
        }
      }

      // Move Seat
      if (CanMoveSeats && e.Button == MouseButtons.Left && SelectedObject != null && m_can_move && SelectedObject.GetType() == typeof(GamingSeatObject))
      {
        m_dragging = true;
      }

      this.Invalidate();
    }

    protected override void OnMouseMove(MouseEventArgs e)
    {

      if (CanMoveSeats && m_dragging && SelectedObject != null && SelectedObject.GetType() == typeof(GamingSeatObject))
      {

        Point MPosition = new Point();

        MPosition = this.PointToClient(MousePosition);

        MPosition.X -= ((GamingSeatObject)SelectedObject).SeatProperties.Width / 2;
        MPosition.Y -= ((GamingSeatObject)SelectedObject).SeatProperties.Height / 2;

        ((GamingSeatObject)SelectedObject).SeatProperties.LocationX = MPosition.X;
        ((GamingSeatObject)SelectedObject).SeatProperties.LocationY = MPosition.Y;

        // Check limits for new location
        MPosition = UtilsDesign.CheckMaxPosition(((GamingSeatObject)SelectedObject).SeatProperties, GamingTableDesign.PanelWidth, GamingTableDesign.PanelHeight);

        ((GamingSeatObject)SelectedObject).SeatProperties.LocationX = MPosition.X;
        ((GamingSeatObject)SelectedObject).SeatProperties.LocationY = MPosition.Y;

        this.Invalidate();
      }
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
      if (CanMoveSeats && m_dragging && SelectedObject != null)
      {
        m_dragging = false;
      }
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      this.SuspendLayout();
      RecalculateSeatsLocationOnResize();
      base.OnSizeChanged(e);
      this.ResumeLayout();
    }

    public void RecalculateSeatsLocationOnResize()
    {
      GamingTableProperties _table_properties;

      _table_properties = null;

      foreach (IDrawGamingTablePanelObject _object in m_panel_objects)
      {
        if (_object.GetType() == typeof(GamingTableObject))
        {
          _table_properties = ((GamingTableObject)_object).TableProperties;
          _table_properties = UtilsDesign.AdjustTableProperties(_table_properties, GamingTableDesign.PanelWidth, GamingTableDesign.PanelHeight);
          break;
        }
      }

      foreach (IDrawGamingTablePanelObject _object in m_panel_objects)
      {
        if (_table_properties != null && _object.GetType() == typeof(GamingSeatObject))
        {
          UtilsDesign.CalculateSeatsRelativePositionToPanel(_table_properties, ((GamingSeatObject)_object).SeatProperties);
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load gambling table
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Int32: GamingTableId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - Boolean
    //
    //   NOTES:
    public Boolean LoadGamingTable(Int32 GamingTableId)
    {
      this.SelectedObject = null;
      return LoadGamingTable(GamingTableId, false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load gambling table
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Int32: GamingTableId
    //          - Boolean: DefaultTable
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - Boolean
    //
    //   NOTES:
    public Boolean LoadGamingTable(Int32 GamingTableId, Boolean DefaultTable)
    {
      String _xml_design;
      List<Seat> _seats;

      m_seats_positions = new List<int>();
      m_panel_objects.Clear();

      _seats = new List<Seat>();
      this.SelectedObject = null;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!GamingTableBusinessLogic.ReadTableDesign(GamingTableId, out _xml_design, _db_trx.SqlTransaction) && !DefaultTable)
        {
          return false;
        }

        if (!GamingTableBusinessLogic.GetTableSeats(GamingTableId, out _seats, _db_trx.SqlTransaction))
        {
          return false;
        }
      }

      if (!LoadGamingTable(_xml_design, _seats))
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load gambling table
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String: XMLGamingTable
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - Boolean
    //
    //   NOTES:
    public Boolean LoadGamingTable(String XMLGamingTable)
    {
      return LoadGamingTable(XMLGamingTable, null);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load gambling table
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String: XMLGamingTable
    //          - List<Seat>: Seats
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - Boolean
    //
    //   NOTES:
    private Boolean LoadGamingTable(String XMLGamingTable, List<Seat> Seats)
    {
      XmlDocument _xml_design;
      XmlNodeList _xml_nodes;
      object _object_seat;
      object _object_table;
      Int32 _seat_id;

      _xml_design = new XmlDocument();
      _object_table = new GamingTableProperties();
      _seat_id = 0;
      m_seats_positions = new List<int>();

      this.m_panel_objects.Clear();
      if (XMLGamingTable != null && !String.IsNullOrEmpty(XMLGamingTable))
      {
        // Load gaming table
        _xml_design.LoadXml(XMLGamingTable);
        _xml_nodes = _xml_design.GetElementsByTagName("GamingTableProperties");

        if (_xml_nodes.Count > 0)
        {
          if (!UtilsDesign.DeserializeXml(_xml_nodes[0].OuterXml.ToString(), ref _object_table))
          {
            return false;
          }
          AddDrawableObject(new GamingTableObject((GamingTableProperties)_object_table, this));
        }

        // Load Seats
        _xml_nodes = _xml_design.GetElementsByTagName("GamingSeatProperties");
        if ((Seats == null || Seats.Count >= _xml_nodes.Count) && _xml_nodes.Count > 0)
        {
          foreach (XmlNode _seat in _xml_nodes)
          {
            _object_seat = new GamingSeatProperties();
            // Load XML Gaming Table
            if (!UtilsDesign.DeserializeXml(_seat.OuterXml.ToString(), ref _object_seat))
            {
              return false;
            }

            if (Seats != null)
            {
              ((GamingSeatProperties)_object_seat).SeatId = Seats[_seat_id].SeatId;
            }
            _seat_id++;

            UtilsDesign.CalculateSeatsRelativePositionToPanel((GamingTableProperties)_object_table, (GamingSeatProperties)_object_seat);

            AddDrawableObject(new GamingSeatObject((GamingSeatProperties)_object_seat, this));
          }
        }
      }
      else
      {
        AddDrawableObject(new GamingTableObject(this));

        // DHA 22-09-2014 Validate object seat is not null
        if (Seats != null && Seats.Count > 0)
        {
          foreach (Seat _seat in Seats)
          {
            if (_seat.Enabled)
            {
              _object_seat = new GamingSeatProperties();

              ((GamingSeatProperties)_object_seat).SeatId = Seats[_seat_id].SeatId;
              _seat_id++;

              AddDrawableObject(new GamingSeatObject((GamingSeatProperties)_object_seat, this));
            }
          }
          AutoLocationSeats();
        }
      }
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get XML gambling table design
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //          - String: XMLTableDesign
    //
    // RETURNS:
    //        - Boolean
    //
    //   NOTES:
    public Boolean GetXMLGamingTable(out String XMLTableDesign)
    {
      return UtilsDesign.GetXMLGamingTable(m_panel_objects, out XMLTableDesign);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get seats location
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //          - List<Point>: SeatsLocation
    //
    // RETURNS:
    //        - Boolean
    //
    //   NOTES:
    public Boolean GetSeatsLocation(out List<Point> SeatsLocation)
    {
      GamingTableProperties _table_properties;
      Point _seat_location;

      SeatsLocation = new List<Point>();
      _seat_location = new Point();
      _table_properties = new GamingTableProperties();

      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        if (_object.GetType() == typeof(GamingTableObject))
        {
          _table_properties = ((GamingTableObject)_object).TableProperties;
          break;
        }
      }

      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        if (_object.GetType() == typeof(GamingSeatObject))
        {
          _seat_location = new Point(((GamingSeatObject)_object).SeatProperties.LocationX - _table_properties.TableCenterPoint.X, ((GamingSeatObject)_object).SeatProperties.LocationY - _table_properties.TableCenterPoint.X);
          SeatsLocation.Add(_seat_location);
        }
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set seats
    //
    //  PARAMS:
    //      - INPUT: 
    //        - Int32: NumSeats
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - Boolean
    //
    //   NOTES:
    public Boolean SetSeatsTable(Int32 NumSeats)
    {
      Int32 _seats_to_set;
      IDrawGamingTablePanelObject _table;

      if (NumSeats < 0)
      {
        return false;
      }

      _seats_to_set = NumSeats;
      _table = new GamingTableObject(this);

      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        if (_object.GetType() == typeof(GamingTableObject))
        {
          _table = _object;
          break;
        }
      }

      this.m_panel_objects.Clear();
      this.m_panel_objects.Add(_table);

      for (Int32 _seat_id = 0; _seat_id < _seats_to_set; _seat_id++)
      {
        this.AddDrawableObject(new GamingSeatObject(this));
      }

      if (m_panel_objects.Count > 0)
      {
        AutoLocationSeats();
      }

      this.Invalidate();

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update seats
    //
    //  PARAMS:
    //      - INPUT: 
    //        - Int32: NumSeats
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - Boolean
    //
    //   NOTES:
    public Boolean UpdateSeatsTable(Int32 NumSeats)
    {
      Int32 _seats_required;
      Int32 _currents_seats;

      _seats_required = 0;
      _currents_seats = 0;

      if (NumSeats < 0)
      {
        return false;
      }

      m_seats_positions = new List<int>();

      foreach (IDrawGamingTablePanelObject _object in this.m_panel_objects)
      {
        if (_object.GetType() == typeof(GamingSeatObject))
        {
          m_seats_positions.Add(((GamingSeatObject)_object).SeatProperties.SeatPosition.Value);
          _currents_seats++;
        }
      }

      // Add seats
      if (NumSeats > _currents_seats)
      {
        _seats_required = NumSeats - _currents_seats;

        for (Int32 _seat_id = 0; _seat_id < _seats_required; _seat_id++)
        {
          this.AddDrawableObject(new GamingSeatObject(this));
        }

      }
      // Delete Seats
      else if (NumSeats < _currents_seats)
      {
        _seats_required = _currents_seats - NumSeats;

        while (_seats_required > 0)
        {
          if (m_panel_objects[_currents_seats].GetType() == typeof(GamingSeatObject))
          {
            m_seats_positions.Remove(_currents_seats);
            m_panel_objects.Remove(m_panel_objects[_currents_seats]);
            _seats_required--;
          }
          _currents_seats--;
        }
      }

      if (m_panel_objects.Count > 0)
      {
        AutoLocationSeats();
      }

      this.Invalidate();

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set selected objecto on panel
    //
    //  PARAMS:
    //      - INPUT: 
    //        - Object: Object
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    private void SetSelectedObject(Object Object)
    {
      foreach (IDrawGamingTablePanelObject _object in m_panel_objects)
      {
        if (Object != null && _object.Id == ((IDrawGamingTablePanelObject)Object).Id)
        {
          if ((Object.GetType() == typeof(GamingSeatObject) && !((GamingSeatObject)Object).SeatProperties.Enabled) && !ModeDesign)
          {
            _object.IsSelected = false;
          }
          else
          {
            _object.IsSelected = true;
          }
        }
        else
        {
          _object.IsSelected = false;
        }
      }
    }

  }

  public class GamingTableObject : IDrawGamingTablePanelObject
  {
    private GamingTablePanel m_parent = null;
    private GamingTableProperties m_table_properties;
    private Guid m_id;
    private Boolean m_is_selected;

    [TypeConverter(typeof(ExpandableObjectConverter))]
    public GamingTableProperties TableProperties
    {
      get { return m_table_properties; }
      set { m_table_properties = value; }
    }

    public GamingTablePanel Parent
    {
      get
      {
        return this.m_parent;
      }
      set
      {
        this.m_parent = value;
      }
    }

    public Guid Id
    {
      get
      {
        return this.m_id;
      }
      set
      {
        this.m_id = value;
      }
    }

    public Boolean IsSelected
    {
      get
      {
        return this.m_is_selected;
      }
      set
      {
        this.m_is_selected = value;
      }
    }

    public GamingTableObject(GamingTablePanel Parent)
    {
      this.Parent = Parent;
      TableProperties = new GamingTableProperties();
      Id = TableProperties.Id;
      TableProperties = UtilsDesign.AdjustTableProperties(TableProperties, Parent);
    }

    public GamingTableObject(GamingTableProperties TableProperties, GamingTablePanel Parent)
    {
      Id = Guid.NewGuid();
      this.Parent = Parent;

      this.TableProperties = TableProperties;
      this.TableProperties.Id = Id;
      this.TableProperties.TableIdlePlays = TableProperties.TableIdlePlays;

      this.TableProperties = UtilsDesign.AdjustTableProperties(TableProperties, Parent);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Draw object (GamingTable)
    //
    //  PARAMS:
    //      - INPUT: 
    //        - Graphics: gr
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void DrawObject(Graphics gr, Matrix TransformMatrix)
    {
      Pen _border_middle;
      Pen _border_interior;
      Pen _border_outer;
      Pen _border_transparent;
      PathGradientBrush _pthGrBrush;
      GraphicsPath _path;

      try
      {

        gr.SmoothingMode = SmoothingMode.AntiAlias;

        TableProperties.LocationX = (int)((GamingTableDesign.PanelWidth - TableProperties.Width) / 2);
        TableProperties.LocationY = (int)((GamingTableDesign.PanelHeight - TableProperties.Height) / 2);
        TableProperties.TableCenterPoint = new Point(GamingTableDesign.PanelWidth / 2, GamingTableDesign.PanelHeight / 2);

        _path = new GraphicsPath();

        TableProperties = UtilsDesign.AdjustTableProperties(TableProperties, Parent);

        switch (TableProperties.TableType)
        {
          case GamingTableDesign.GT_Design_Type.Type2:

            Rectangle rectangleC1 = new Rectangle(new Point(TableProperties.LocationX, TableProperties.LocationY), new Size(TableProperties.Height, TableProperties.Height));
            Rectangle rectangleC2 = new Rectangle(new Point(TableProperties.Width - TableProperties.Height + TableProperties.LocationX, TableProperties.LocationY), new Size(TableProperties.Height, this.TableProperties.Height));

            _path.StartFigure();
            _path.AddArc(rectangleC1, 90, 180);
            _path.AddLine(new Point(this.TableProperties.Height / 2 + TableProperties.LocationX, TableProperties.LocationY), new Point(TableProperties.Width - TableProperties.Height + TableProperties.LocationY, TableProperties.LocationY));
            _path.AddArc(rectangleC2, -90, 180);
            _path.CloseFigure();

            // Create image.
            //Image _logoC = Image.FromFile("logo2.png", true);
            //e.Graphics.DrawImage(_logoC, this.Width / 2 - _logoC.Width / 2, this.Height / 2 - _logoC.Height / 2);

            break;

          case GamingTableDesign.GT_Design_Type.Type3:

            List<Point> pointsPol = new List<Point>();
            double _angle_interval = 0;
            double _rotation_angle = UtilsDesign.DegreeToRadians(TableProperties.RotateAngle);

            _path.StartFigure();

            if (TableProperties.NumSides > 2 && TableProperties.NumSides < 50)
            {
              _angle_interval = 2 * Math.PI / TableProperties.NumSides;

              for (int _side = 0; _side < TableProperties.NumSides; _side++)
              {
                pointsPol.Add(new Point((int)(TableProperties.Width / 2 + TableProperties.Width / 2 * Math.Cos(_angle_interval * _side + _rotation_angle)) + TableProperties.LocationX, (int)(TableProperties.Height / 2 + TableProperties.Height / 2 * Math.Sin(_angle_interval * _side + _rotation_angle) + TableProperties.LocationY)));
              }

              _path.AddPolygon(pointsPol.ToArray());
            }
            else
            {
              _path.AddEllipse(new Rectangle(new Point(TableProperties.LocationX, TableProperties.LocationY), new Size(TableProperties.Width, TableProperties.Height)));
            }

            _path.CloseFigure();

            // Create image.
            //Image _logoC = Image.FromFile("logo2.png", true);
            //e.Graphics.DrawImage(_logoC, this.Width / 2 - _logoC.Width / 2, this.Height / 2 - _logoC.Height / 2);

            break;

          case GamingTableDesign.GT_Design_Type.Type1:

            TableProperties.LocationX = (int)((GamingTableDesign.PanelWidth - TableProperties.Width) / 2);
            TableProperties.LocationY = (int)(GamingTableDesign.PanelHeight - TableProperties.Height - GamingTableDesign.BottomMarginGamingTable);
            TableProperties.TableCenterPoint = new Point(GamingTableDesign.PanelWidth / 2, GamingTableDesign.PanelHeight - GamingTableDesign.BottomMarginGamingTable);

            Rectangle rectangleB1 = new Rectangle(new Point(TableProperties.LocationX, TableProperties.LocationY), new Size(this.m_table_properties.Width, 2 * this.m_table_properties.Height));

            _path.StartFigure();
            _path.AddArc(rectangleB1, -180, 180);
            _path.CloseFigure();

            break;

        }

        _border_outer = new Pen(TableProperties.BorderOuterColor);
        _border_outer.Width = TableProperties.BorderOuterSize;

        _border_middle = new Pen(TableProperties.BorderMiddleColor);
        _border_middle.Width = TableProperties.BorderMiddleSize + TableProperties.BorderOuterSize;

        _border_interior = new Pen(TableProperties.BorderInteriorColor);
        _border_interior.Width = TableProperties.BorderInteriorSize + TableProperties.BorderMiddleSize + TableProperties.BorderOuterSize;

        _border_transparent = new Pen(this.m_parent.BackColor);
        _border_transparent.Width = 1;

        _pthGrBrush = new PathGradientBrush(_path);
        _pthGrBrush.CenterColor = TableProperties.GradientCenterColor;
        _pthGrBrush.CenterPoint = new Point(TableProperties.Width / 2 + TableProperties.LocationX, TableProperties.Height + TableProperties.LocationY);

        Color[] colorsC = { TableProperties.GradientOuterColor };
        _pthGrBrush.SurroundColors = colorsC;

        TableProperties.Path = (GraphicsPath)_path.Clone();
        TableProperties.Path.Transform(TransformMatrix);

        gr.Clip = new Region(_path);
        gr.FillPath(_pthGrBrush, _path);
        gr.DrawPath(_border_interior, _path);
        gr.DrawPath(_border_middle, _path);
        gr.DrawPath(_border_outer, _path);
        gr.DrawPath(_border_transparent, _path);

        _border_middle.Dispose();
        _border_interior.Dispose();
        _border_outer.Dispose();
        _pthGrBrush.Dispose();
      }
      catch (Exception _ex)
      {
        Log.Message("GamingTableObject.DrawObject Error: " + _ex.Message);
      }
    }

    private void UpdateParent()
    {
      if (this.m_parent != null)
      {
        this.m_parent.Invalidate();
      }
    }
  }

  public class GamingSeatObject : IDrawGamingTablePanelObject
  {
    private GamingTablePanel m_parent = null;
    private GamingSeatProperties m_seat_propierties;
    private Guid m_id;
    private Boolean m_is_selected;

    [TypeConverter(typeof(ExpandableObjectConverter))]
    public GamingSeatProperties SeatProperties
    {
      get { return m_seat_propierties; }
      set { m_seat_propierties = value; }
    }

    public GamingTablePanel Parent
    {
      get
      {
        return this.m_parent;
      }
      set
      {
        this.m_parent = value;
      }
    }

    public Guid Id
    {
      get
      {
        return this.m_id;
      }
      set
      {
        this.m_id = value;
      }
    }

    public Boolean IsSelected
    {
      get
      {
        return this.m_is_selected;
      }
      set
      {
        this.m_is_selected = value;
      }
    }

    public GamingSeatObject(GamingTablePanel Parent)
    {
      this.Parent = Parent;
      SeatProperties = new GamingSeatProperties();

      if (!SeatProperties.Enabled)
      {
        SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.DISABLED;
      }
      else
      {
        SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.NONE;
      }
      Id = SeatProperties.Id;
    }

    public GamingSeatObject(GamingSeatProperties SeatProperties, GamingTablePanel Parent)
    {
      Id = Guid.NewGuid();
      this.Parent = Parent;

      this.SeatProperties = SeatProperties;
      this.SeatProperties.Id = Id;

      if (!SeatProperties.Enabled)
      {
        SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.DISABLED;
      }
      else
      {
        SeatProperties.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.NONE;
      }

      UtilsDesign.SetSeatColorByStatus(SeatProperties);

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Draw object (GamingSeat)
    //
    //  PARAMS:
    //      - INPUT: 
    //        - Graphics: gr
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public void DrawObject(Graphics gr, Matrix TransformMatrix)
    {
      UtilsDesign.DrawSeatObject(gr, SeatProperties, IsSelected, TransformMatrix);
    }

    private void UpdateParent()
    {
      if (this.m_parent != null)
      {
        this.m_parent.Invalidate();
      }
    }
  }

  [Serializable, XmlRoot(ElementName = "GamingTableProperties"), TypeConverter(typeof(PropertyTypeConverter))]
  public class GamingTableProperties
  {
    private GamingTableDesign.GT_Design_Type m_table_type;
    private Guid m_id;
    private GraphicsPath m_path;
    private Point m_table_center_point;

    private int m_width;
    private int m_height;

    private int m_location_x;
    private int m_location_y;

    private Color m_border_outer_color;
    private Color m_border_middle_color;
    private Color m_border_interior_color;

    private float m_border_outer_size;
    private float m_border_middle_size;
    private float m_border_interior_size;

    private Color m_gradient_center_color;
    private Color m_gradient_outer_color;

    private int m_num_sides;
    private int m_rotate_angle;

    private int m_seat_distance;
    private bool m_auto_location_seats;
    private int m_rotate_angle_seats;
    private int m_dealer_angle;
    private Int32 m_table_idle_plays;

    #region

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Guid Id
    {
      get
      {
        return m_id;
      }
      set
      {
        m_id = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public GraphicsPath Path
    {
      get
      {
        return m_path;
      }
      set
      {
        m_path = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Point TableCenterPoint
    {
      get
      {
        return m_table_center_point;
      }
      set
      {
        m_table_center_point = value;
      }
    }

    [XmlElement(ElementName = "TableType"), RefreshProperties(System.ComponentModel.RefreshProperties.All)]
    public GamingTableDesign.GT_Design_Type TableType
    {
      get
      {
        return m_table_type;
      }
      set
      {
        m_table_type = value;
        if (value != GamingTableDesign.GT_Design_Type.Type3)
        {
          UtilsDesign.SetPropertiesReadOnly("NumSides", true, this);
        }
        else {
          UtilsDesign.SetPropertiesReadOnly("NumSides", false, this);
        }
      }
    }

    [XmlElement(ElementName = "Width"), Category("Size"), TypeConverter(typeof(CustomTypeConverter))]
    public int Width
    {
      get
      {
        return m_width;
      }

      set
      {
        m_width = value;
      }
    }

    [XmlElement(ElementName = "Height"), Category("Size"), TypeConverter(typeof(CustomTypeConverter))]
    public int Height
    {
      get
      {
        return m_height;
      }

      set
      {
        m_height = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public int LocationX
    {
      get
      {
        return m_location_x;
      }

      set
      {
        m_location_x = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public int LocationY
    {
      get
      {
        return m_location_y;
      }

      set
      {
        m_location_y = value;
      }
    }

    [XmlElement(ElementName = "BorderOuterColor", Type = typeof(XmlColor)), Category("Outer Border"), TypeConverter(typeof(CustomTypeConverter))]
    public Color BorderOuterColor
    {
      get
      {
        return m_border_outer_color;
      }

      set
      {
        m_border_outer_color = value;
      }
    }

    [XmlElement(ElementName = "BorderMiddleColor", Type = typeof(XmlColor)), Category("Middle Border"), TypeConverter(typeof(CustomTypeConverter))]
    public Color BorderMiddleColor
    {
      get
      {
        return m_border_middle_color;
      }

      set
      {
        m_border_middle_color = value;
      }
    }

    [XmlElement(ElementName = "BorderInteriorColor", Type = typeof(XmlColor)), Category("Interior Border"), TypeConverter(typeof(CustomTypeConverter))]
    public Color BorderInteriorColor
    {
      get
      {
        return m_border_interior_color;
      }

      set
      {
        m_border_interior_color = value;
      }
    }

    [XmlElement(ElementName = "BorderOuterSize"), Category("Outer Border"), TypeConverter(typeof(CustomTypeConverter))]
    public float BorderOuterSize
    {
      get
      {
        return m_border_outer_size;
      }

      set
      {
        m_border_outer_size = value;
      }
    }

    [XmlElement(ElementName = "BorderMiddleSize"), Category("Middle Border"), TypeConverter(typeof(CustomTypeConverter))]
    public float BorderMiddleSize
    {
      get
      {
        return m_border_middle_size;
      }

      set
      {
        m_border_middle_size = value;
      }
    }

    [XmlElement(ElementName = "BorderInteriorSize"), Category("Interior Border"), TypeConverter(typeof(CustomTypeConverter))]
    public float BorderInteriorSize
    {
      get
      {
        return m_border_interior_size;
      }

      set
      {
        m_border_interior_size = value;
      }
    }

    [XmlElement(ElementName = "GradientCenterColor", Type = typeof(XmlColor)), Category("Gradient"), TypeConverter(typeof(CustomTypeConverter))]
    public Color GradientCenterColor
    {
      get
      {
        return m_gradient_center_color;
      }

      set
      {
        m_gradient_center_color = value;
      }
    }

    [XmlElement(ElementName = "GradientOuterColor", Type = typeof(XmlColor)), Category("Gradient"), TypeConverter(typeof(CustomTypeConverter))]
    public Color GradientOuterColor
    {
      get
      {
        return m_gradient_outer_color;
      }

      set
      {
        m_gradient_outer_color = value;
      }
    }

    [XmlElement(ElementName = "NumSides"), TypeConverter(typeof(CustomTypeConverter)), ReadOnly(false)]
    public int NumSides
    {
      get
      {
        return m_num_sides;
      }

      set
      {
        m_num_sides = value;
      }
    }

    [XmlElement(ElementName = "RotateAngle"), TypeConverter(typeof(CustomTypeConverter))]
    public int RotateAngle
    {
      get
      {
        return m_rotate_angle;
      }

      set
      {
        m_rotate_angle = value;
      }
    }

    [XmlElement(ElementName = "SeatsDistance"), Category("Seats"), TypeConverter(typeof(CustomTypeConverter)), ReadOnly(false)]
    public int SeatsDistance
    {
      get
      {
        return m_seat_distance;
      }

      set
      {
        m_seat_distance = value;
      }
    }

    [XmlElement(ElementName = "AutoLocationSeats"), Category("Seats"), RefreshProperties(System.ComponentModel.RefreshProperties.All)]
    public bool AutoLocationSeats
    {
      get
      {
        return m_auto_location_seats;
      }

      set
      {
        m_auto_location_seats = value;
        UtilsDesign.SetPropertiesReadOnly("SeatsDistance", !value, this);
        UtilsDesign.SetPropertiesReadOnly("DealerAngle", !value, this);
        UtilsDesign.SetPropertiesReadOnly("RotateAngleSeats", !value, this);
      }
    }

    [XmlElement(ElementName = "DealerAngle"), Category("Seats"), TypeConverter(typeof(CustomTypeConverter)), ReadOnly(false)]
    public int DealerAngle
    {
      get
      {
        return m_dealer_angle;
      }

      set
      {
        m_dealer_angle = value;
      }
    }

    [XmlElement(ElementName = "RotateAngleSeats"), Category("Seats"), TypeConverter(typeof(CustomTypeConverter)), ReadOnly(false)]
    public int RotateAngleSeats
    {
      get
      {
        return m_rotate_angle_seats;
      }

      set
      {
        m_rotate_angle_seats = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public int TableIdlePlays
    {
      get
      {
        return m_table_idle_plays;
      }

      set
      {
        m_table_idle_plays = value;
      }
    }

    #endregion

    public GamingTableProperties()
    {
      this.Id = Guid.NewGuid();
      this.Width = 500;
      this.Height = 250;
      this.BorderMiddleColor = Color.White;
      this.BorderInteriorColor = Color.White;
      this.BorderOuterColor = Color.White;
      this.BorderOuterSize = 3F;
      this.BorderMiddleSize = 4F;
      this.BorderInteriorSize = 3F;
      this.GradientCenterColor = Color.FromArgb(33, 139, 46);
      this.GradientOuterColor = Color.FromArgb(33, 139, 46);
      this.SeatsDistance = 50;
      this.AutoLocationSeats = true;
      this.DealerAngle = 0;
      this.RotateAngleSeats = 0;
      this.LocationX = (int)((GamingTableDesign.PanelWidth - this.Width) / 2);
      this.LocationY = (int)((GamingTableDesign.PanelHeight - this.Height) / 2);
      this.TableIdlePlays = GeneralParam.GetInt32("GamingTable.PlayerTracking", "CurrentBetWarningElapsedPlays", 5);
      this.TableCenterPoint = new Point(GamingTableDesign.PanelWidth / 2, GamingTableDesign.PanelHeight / 2);
    }
  }

  [Serializable, XmlRoot(ElementName = "GamingSeatProperties"), TypeConverter(typeof(PropertyTypeConverter))]
  public class GamingSeatProperties
  {
    private Guid m_id;
    private Int64 m_seat_id;
    private GraphicsPath m_path;

    private int m_width;
    private int m_height;

    private int m_location_x;
    private int m_location_y;

    private int m_relative_location_x;
    private int m_relative_location_y;

    private Color m_border_outer_color;
    private Color m_border_middle_color;
    private Color m_border_interior_color;

    private float m_border_outer_size;
    private float m_border_middle_size;
    private float m_border_interior_size;

    private Color m_gradient_center_color;
    private Color m_gradient_outer_color;

    private float m_rotate_angle;
    private Boolean m_enabled;

    private String m_account_name;
    private String m_bottom_label_first_line;
    private String m_bottom_label_second_line;
    private SEAT_STATUS_PLAYER_TRACKING m_seat_status;

    private Int32? m_seat_position;
    private Int64? m_plays;
    private Int64? m_in_session_last_play;
    private Int32 m_table_idle_plays;
    private Currency? m_current_bet;
    private Currency? m_table_min_bet;
    private Currency? m_table_max_bet;

    #region

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Guid Id
    {
      get
      {
        return m_id;
      }
      set
      {
        m_id = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Int64 SeatId
    {
      get
      {
        return m_seat_id;
      }
      set
      {
        m_seat_id = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public GraphicsPath Path
    {
      get
      {
        return m_path;
      }
      set
      {
        m_path = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public int Width
    {
      get
      {
        return m_width;
      }

      set
      {
        m_width = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public int Height
    {
      get
      {
        return m_height;
      }

      set
      {
        m_height = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, XmlElement(ElementName = "LocationX"), Category("Location"), TypeConverter(typeof(CustomTypeConverter)), ReadOnly(false)]
    public int LocationX
    {
      get
      {
        return m_location_x;
      }

      set
      {
        m_location_x = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, XmlElement(ElementName = "LocationY"), Category("Location"), TypeConverter(typeof(CustomTypeConverter)), ReadOnly(false)]
    public int LocationY
    {
      get
      {
        return m_location_y;
      }

      set
      {
        m_location_y = value;
      }
    }

    [XmlElement(ElementName = "RelativeLocationX"), Category("RelativeLocation"), Browsable(false)]
    public int RelativeLocationX
    {
      get
      {
        return m_relative_location_x;
      }

      set
      {
        m_relative_location_x = value;
      }
    }

    [XmlElement(ElementName = "RelativeLocationY"), Category("RelativeLocation"), Browsable(false)]
    public int RelativeLocationY
    {
      get
      {
        return m_relative_location_y;
      }

      set
      {
        m_relative_location_y = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Color BorderOuterColor
    {
      get
      {
        return m_border_outer_color;
      }

      set
      {
        m_border_outer_color = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Color BorderMiddleColor
    {
      get
      {
        return m_border_middle_color;
      }

      set
      {
        m_border_middle_color = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Color BorderInteriorColor
    {
      get
      {
        return m_border_interior_color;
      }

      set
      {
        m_border_interior_color = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public float BorderOuterSize
    {
      get
      {
        return m_border_outer_size;
      }

      set
      {
        m_border_outer_size = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public float BorderMiddleSize
    {
      get
      {
        return m_border_middle_size;
      }

      set
      {
        m_border_middle_size = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public float BorderInteriorSize
    {
      get
      {
        return m_border_interior_size;
      }

      set
      {
        m_border_interior_size = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Color GradientCenterColor
    {
      get
      {
        return m_gradient_center_color;
      }

      set
      {
        m_gradient_center_color = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Color GradientOuterColor
    {
      get
      {
        return m_gradient_outer_color;
      }

      set
      {
        m_gradient_outer_color = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public float RotateAngle
    {
      get
      {
        return m_rotate_angle;
      }

      set
      {
        m_rotate_angle = value;
      }
    }

    [XmlElement(ElementName = "Enabled")]
    public Boolean Enabled
    {
      get
      {
        return m_enabled;
      }

      set
      {
        m_enabled = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public String AccountName
    {
      get
      {
        return m_account_name;
      }

      set
      {
        m_account_name = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public String BottomLabelFirstLine
    {
      get
      {
        return m_bottom_label_first_line;
      }

      set
      {
        m_bottom_label_first_line = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public String BottomLabelSecondLine
    {
      get
      {
        return m_bottom_label_second_line;
      }

      set
      {
        m_bottom_label_second_line = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public SEAT_STATUS_PLAYER_TRACKING SeatStatus
    {
      get
      {
        return m_seat_status;
      }

      set
      {
        m_seat_status = value;
      }
    }

    [XmlElement(ElementName = "SeatPosition"), TypeConverter(typeof(CustomTypeConverter))]
    public Int32? SeatPosition
    {
      get
      {
        return m_seat_position;
      }

      set
      {
        if (GamingTableDesign.SeatPositionMinValue <= value && value <= GamingTableDesign.SeatPositionMaxValue)
        {
          m_seat_position = value;
        }
        if (m_seat_position == null)
        {
          m_seat_position = GamingTableDesign.SeatPositionMaxValue;
        }
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Int64? Plays
    {
      get
      {
        return m_plays;
      }
      set
      {
        m_plays = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Int64? InSessionLastPlay
    {
      get
      {
        return m_in_session_last_play;
      }
      set
      {
        m_in_session_last_play = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Int32 TableIdlePlays
    {
      get
      {
        return m_table_idle_plays;
      }
      set
      {
        m_table_idle_plays = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Currency? CurrentBet
    {
      get
      {
        return m_current_bet;
      }
      set
      {
        m_current_bet = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Currency? TableMinBet
    {
      get
      {
        return m_table_min_bet;
      }
      set
      {
        m_table_min_bet = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore, Browsable(false)]
    public Currency? TableMaxBet
    {
      get
      {
        return m_table_max_bet;
      }
      set
      {
        m_table_max_bet = value;
      }
    }

    #endregion

    public GamingSeatProperties()
    {
      this.Id = Guid.NewGuid();
      this.Width = 50;
      this.Height = 50;
      this.BorderMiddleColor = Color.White;
      this.BorderInteriorColor = Color.White;
      this.BorderOuterColor = Color.White;
      this.BorderOuterSize = 3F;
      this.BorderMiddleSize = 3F;
      this.BorderInteriorSize = 4F;
      this.GradientCenterColor = Color.Black;
      this.GradientOuterColor = Color.Black;
      this.AccountName = String.Empty;
      this.TableIdlePlays = GeneralParam.GetInt32("GamingTable.PlayerTracking", "CurrentBetWarningElapsedPlays", 5);
      this.Enabled = true;

    }
  }

  public class ObjectPanelEventArgs : EventArgs
  {
    private object m_object_panel;

    public ObjectPanelEventArgs(object ObjectPanel)
    {
      this.m_object_panel = ObjectPanel;
    }

    public object ObjectPanel
    {
      get { return this.m_object_panel; }
    }
  }

  class CustomTypeConverter : TypeConverter
  {
    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
    {
      if (sourceType == typeof(string))
      {
        return true;
      }

      return base.CanConvertFrom(context, sourceType);
    }


    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
    {
      if (context.PropertyDescriptor.PropertyType == typeof(Int32))
      {
        Int32 _value;

        if (!Int32.TryParse(value.ToString(), out _value))
        {
          return ((GridItem)context).Value;
        }

        return _value;
      }
      else if (context.PropertyDescriptor.PropertyType == typeof(Int32?))
      {
        Int32 _value;

        if (!Int32.TryParse(value.ToString(), out _value))
        {
          return ((GridItem)context).Value;
        }
        return _value;
      }
      else if (context.PropertyDescriptor.PropertyType == typeof(String))
      {
        return value.ToString();
      }

      else if (context.PropertyDescriptor.PropertyType == typeof(Boolean))
      {
        Boolean _value;

        if (!Boolean.TryParse(value.ToString(), out _value))
        {
          return ((GridItem)context).Value;
        }
        return _value;
      }

      else if (context.PropertyDescriptor.PropertyType == typeof(float))
      {
        float _value;

        if (!float.TryParse(value.ToString(), out _value))
        {
          return ((GridItem)context).Value;
        }
        return _value;
      }

      else if (context.PropertyDescriptor.PropertyType == typeof(Color))
      {
        return ((GridItem)context).Value;
      }

      return base.ConvertFrom(context, culture, value);
    }
  }

  class PropertyTypeConverter : ExpandableObjectConverter
  {
    public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
    {
      PropertyDescriptorCollection props = base.GetProperties(context, value, attributes);
      List<PropertyDescriptor> list = new List<PropertyDescriptor>(props.Count);

      foreach (PropertyDescriptor prop in props)
      {
        switch (prop.Name)
        {
          case "LocationX":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_LOCATION_X"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_LOCATION")));
            break;
          case "LocationY":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_LOCATION_Y"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_LOCATION")));
            break;
          case "Enabled":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_ENABLED"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS")));
            break;
          case "SeatPosition":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_SEATPOSITION"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS")));
            break;
          case "TableType":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_TABLETYPE"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS")));
            break;
          case "Width":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_WIDTH"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SIZE")));
            break;
          case "Height":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_HEIGHT"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SIZE")));
            break;
          case "BorderOuterColor":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_COLOR"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OUTER_BORDER")));
            break;
          case "BorderMiddleColor":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_COLOR"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_MIDDLE_BORDER")));
            break;
          case "BorderInteriorColor":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_COLOR"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_INNER_BORDER")));
            break;
          case "BorderOuterSize":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_SIZE"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OUTER_BORDER")));
            break;
          case "BorderMiddleSize":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_SIZE"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_MIDDLE_BORDER")));
            break;
          case "BorderInteriorSize":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_SIZE"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_INNER_BORDER")));
            break;
          case "GradientCenterColor":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_GRADIENT_CENTER_COLOR"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_GRADIENT")));
            break;
          case "GradientOuterColor":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_GRADIENT_OUTER_COLOR"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_GRADIENT")));
            break;
          case "NumSides":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_NUM_SIDES"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS")));
            break;
          case "RotateAngle":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_ROTATE_ANGLE"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS")));
            break;
          case "SeatsDistance":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_SEATS_DISTANCE"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SEATS")));
            break;
          case "AutoLocationSeats":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_AUTO_LOCATION_SEATS"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SEATS")));
            break;
          case "DealerAngle":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_DEALER_ANGLE"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SEATS")));
            break;
          case "RotateAngleSeats":
            list.Add(new GamingTablePropertyDescriptor(prop, Resource.String("GAMING_TABLE_PROPERTY_ROTATE_ANGLE_SEATS"), "", Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SEATS")));
            break;
          default:
            list.Add(prop);
            break;
        }
      }
      return new PropertyDescriptorCollection(list.ToArray(), true);
    }
  }

  class GamingTablePropertyDescriptor : PropertyDescriptor
  {
    private readonly string displayName;
    private readonly string description;
    private readonly string category;
    private readonly PropertyDescriptor parent;
    public GamingTablePropertyDescriptor(
      PropertyDescriptor parent, string displayName, string description, string category)
      : base(parent)
    {
      this.displayName = displayName;
      this.description = description;
      this.category = category;
      this.parent = parent;
    }
    public override string DisplayName
    { get { return displayName; } }

    public override bool ShouldSerializeValue(object component)
    { return parent.ShouldSerializeValue(component); }

    public override void SetValue(object component, object value)
    {
      parent.SetValue(component, value);
    }
    public override object GetValue(object component)
    {
      return parent.GetValue(component);
    }
    public override void ResetValue(object component)
    {
      parent.ResetValue(component);
    }
    public override bool CanResetValue(object component)
    {
      return parent.CanResetValue(component);
    }
    public override bool IsReadOnly
    {
      get { return parent.IsReadOnly; }
    }
    public override void AddValueChanged(object component, EventHandler handler)
    {
      parent.AddValueChanged(component, handler);
    }
    public override void RemoveValueChanged(object component, EventHandler handler)
    {
      parent.RemoveValueChanged(component, handler);
    }
    public override bool SupportsChangeEvents
    {
      get { return parent.SupportsChangeEvents; }
    }
    public override Type PropertyType
    {
      get { return parent.PropertyType; }
    }
    public override TypeConverter Converter
    {
      get { return parent.Converter; }
    }
    public override Type ComponentType
    {
      get { return parent.ComponentType; }
    }
    public override string Description
    {
      get { return description; }
    }
    public override PropertyDescriptorCollection GetChildProperties(object instance, Attribute[] filter)
    {
      return parent.GetChildProperties(instance, filter);
    }
    public override string Name
    {
      get { return parent.Name; }
    }
    public override string Category
    {
      get { return category; }
    }
  }

  public static class UtilsDesign
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Degree to radians
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Degree
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Radians
    //
    //   NOTES:
    public static float DegreeToRadians(int Degree)
    {
      return (float)(Degree * Math.PI / 180);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Radians to degree
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Radians
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Degree
    //
    //   NOTES:
    public static float DegreeToRadians(float Degree)
    {
      return (float)(Degree * Math.PI / 180);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Adjust gaming table properties
    //
    //  PARAMS:
    //      - INPUT: 
    //          - GamingTableProperties
    //          - Control Parent
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - GamingTableProperties
    //
    //   NOTES:
    public static GamingTableProperties AdjustTableProperties(GamingTableProperties TableProperties, Control Parent)
    {
      return AdjustTableProperties(TableProperties, GamingTableDesign.PanelWidth, GamingTableDesign.PanelHeight);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Adjust gaming table properties
    //
    //  PARAMS:
    //      - INPUT: 
    //          - GamingTableProperties
    //          - Width parent
    //          - Height parent
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - GamingTableProperties
    //
    //   NOTES:
    public static GamingTableProperties AdjustTableProperties(GamingTableProperties TableProperties, Int32 PanelWidth, Int32 PanelHeight)
    {
      if (GamingTableDesign.TableMaxWidht < TableProperties.Width)
      {
        TableProperties.Width = GamingTableDesign.TableMaxWidht;
      }

      if (GamingTableDesign.TableMaxHeight < TableProperties.Height)
      {
        TableProperties.Height = GamingTableDesign.TableMaxHeight;
      }

      if (GamingTableDesign.TableMinWidht > TableProperties.Width)
      {
        TableProperties.Width = GamingTableDesign.TableMinWidht;
      }

      if (GamingTableDesign.TableMinHeight > TableProperties.Height)
      {
        TableProperties.Height = GamingTableDesign.TableMinHeight;
      }

      switch (TableProperties.TableType)
      {
        case GamingTableDesign.GT_Design_Type.Type2:

          //if (TableProperties.Height > TableProperties.Width)
          //{
          //  TableProperties.Height = TableProperties.Width;
          //}

          TableProperties.LocationX = (int)((PanelWidth - TableProperties.Width) / 2);
          TableProperties.LocationY = (int)((PanelHeight - TableProperties.Height) / 2);

          TableProperties.TableCenterPoint = new Point(PanelWidth / 2, PanelHeight / 2);

          break;
        case GamingTableDesign.GT_Design_Type.Type3:

          //if (TableProperties.Height > TableProperties.Width)
          //{
          //  TableProperties.Height = TableProperties.Width;
          //}
          //else
          //{
          //  TableProperties.Width = TableProperties.Height;
          //}

          TableProperties.LocationX = (int)((PanelWidth - TableProperties.Width) / 2);
          TableProperties.LocationY = (int)((PanelHeight - TableProperties.Height) / 2);

          TableProperties.TableCenterPoint = new Point(PanelWidth / 2, PanelHeight / 2);

          break;

        case GamingTableDesign.GT_Design_Type.Type1:

          TableProperties.LocationX = (int)((PanelWidth - TableProperties.Width) / 2);
          TableProperties.LocationY = (int)(PanelHeight - TableProperties.Height - GamingTableDesign.BottomMarginGamingTable);

          if (TableProperties.DealerAngle < 180)
          {
            TableProperties.DealerAngle = 180;
          }

          TableProperties.TableCenterPoint = new Point(PanelWidth / 2, PanelHeight - GamingTableDesign.BottomMarginGamingTable);

          break;
      }

      return TableProperties;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Object to Xml
    //
    //  PARAMS:
    //      - INPUT: 
    //          - XmlStrt
    //
    //      - OUTPUT:
    //          - Reporte
    //
    // RETURNS:
    //          - True: Validate OK 
    //          - False: otherwise
    //
    //   NOTES:
    //
    public static Boolean DeserializeXml(String XmlStr, ref Object ObjectDeserialize)
    {
      System.Xml.Serialization.XmlSerializer _xml_ser;
      System.IO.StringReader _string_reader;
      try
      {
        if (ObjectDeserialize == null)
        {
          return false;
        }
        _string_reader = new System.IO.StringReader(XmlStr);
        _xml_ser = new System.Xml.Serialization.XmlSerializer(ObjectDeserialize.GetType());   
        ObjectDeserialize = _xml_ser.Deserialize(_string_reader);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DeserializeXml

    //------------------------------------------------------------------------------
    // PURPOSE: Get XML Serialization from Object
    //
    //  PARAMS:
    //      - INPUT: 
    //          - XmlObject
    //
    //      - OUTPUT:
    //          - XmlStr
    //
    // RETURNS:
    //          - True: Validate OK 
    //          - False: otherwise
    //
    //   NOTES:
    public static Boolean ObjectSerializableToXml(Object XmlObject, out String XmlStr)
    {
      System.Xml.Serialization.XmlSerializer _xml_ser;
      StreamWriter _stream_writer;
      StreamReader _stream_reader;

      XmlStr = "";

      try
      {
        XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        ns.Add(String.Empty, String.Empty);

        using (MemoryStream _memory_stream = new System.IO.MemoryStream())
        {
          _stream_writer = new System.IO.StreamWriter(_memory_stream);
          _stream_reader = new System.IO.StreamReader(_memory_stream);

          _xml_ser = new System.Xml.Serialization.XmlSerializer(XmlObject.GetType());
          _xml_ser.Serialize(_stream_writer, XmlObject, ns);

          _memory_stream.Position = 0;
          XmlStr = _stream_reader.ReadToEnd();
        }

        return true;
      }
      catch (System.Xml.XmlException _ex)
      {
        Console.WriteLine(_ex.Message);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ObjectSerializableToXml

    //------------------------------------------------------------------------------
    // PURPOSE: Get serializable properties
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Type
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - List<String>
    //
    //   NOTES:
    public static Dictionary<String, String> GetSerializableProperties(Type Properties)
    {
      Dictionary<String, String> serializable_properties = new Dictionary<String, String>();
      IList<PropertyInfo> props = new List<PropertyInfo>(Properties.GetProperties());

      if (Properties.IsDefined(typeof(SerializableAttribute), false))
      {
        foreach (PropertyInfo prop in props)
        {
          if (!prop.IsDefined(typeof(XmlIgnoreAttribute), false))
          {
            serializable_properties.Add(prop.Name, GetPropertyResource(prop.Name));
          }
        }
      }

      return serializable_properties;
    }

    public static String GetPropertyResource(String PropertyName)
    {
      String _resource;

      _resource = PropertyName;
      switch (PropertyName)
      {
        case "RelativeLocationX":
        case "LocationX":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_LOCATION") + "." + Resource.String("GAMING_TABLE_PROPERTY_LOCATION_X");
          break;
        case "RelativeLocationY":
        case "LocationY":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_LOCATION") + "." + Resource.String("GAMING_TABLE_PROPERTY_LOCATION_Y");
          break;
        case "Enabled":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS") + "." + Resource.String("GAMING_TABLE_PROPERTY_ENABLED");
          break;
        case "SeatPosition":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS") + "." + Resource.String("GAMING_TABLE_PROPERTY_SEATPOSITION");
          break;
        case "TableType":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS") + "." + Resource.String("GAMING_TABLE_PROPERTY_TABLETYPE");
          break;
        case "Width":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SIZE") + "." + Resource.String("GAMING_TABLE_PROPERTY_WIDTH");
          break;
        case "Height":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SIZE") + "." + Resource.String("GAMING_TABLE_PROPERTY_HEIGHT");
          break;
        case "BorderOuterColor":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OUTER_BORDER") + "." + Resource.String("GAMING_TABLE_PROPERTY_COLOR");
          break;
        case "BorderMiddleColor":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_MIDDLE_BORDER") + "." + Resource.String("GAMING_TABLE_PROPERTY_COLOR");
          break;
        case "BorderInteriorColor":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_INNER_BORDER") + "." + Resource.String("GAMING_TABLE_PROPERTY_COLOR");
          break;
        case "BorderOuterSize":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OUTER_BORDER") + "." + Resource.String("GAMING_TABLE_PROPERTY_SIZE");
          break;
        case "BorderMiddleSize":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_MIDDLE_BORDER") + "." + Resource.String("GAMING_TABLE_PROPERTY_SIZE");
          break;
        case "BorderInteriorSize":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_INNER_BORDER") + "." + Resource.String("GAMING_TABLE_PROPERTY_SIZE");
          break;
        case "GradientCenterColor":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_GRADIENT") + "." + Resource.String("GAMING_TABLE_PROPERTY_GRADIENT_CENTER_COLOR");
          break;
        case "GradientOuterColor":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_GRADIENT") + "." + Resource.String("GAMING_TABLE_PROPERTY_GRADIENT_OUTER_COLOR");
          break;
        case "NumSides":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS") + "." + Resource.String("GAMING_TABLE_PROPERTY_NUM_SIDES");
          break;
        case "RotateAngle":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_OTHERS") + "." + Resource.String("GAMING_TABLE_PROPERTY_ROTATE_ANGLE");
          break;
        case "SeatsDistance":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SEATS") + "." + Resource.String("GAMING_TABLE_PROPERTY_SEATS_DISTANCE");
          break;
        case "AutoLocationSeats":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SEATS") + "." + Resource.String("GAMING_TABLE_PROPERTY_AUTO_LOCATION_SEATS");
          break;
        case "DealerAngle":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SEATS") + "." + Resource.String("GAMING_TABLE_PROPERTY_DEALER_ANGLE");
          break;
        case "RotateAngleSeats":
          _resource = Resource.String("GAMING_TABLE_PROPERTY_CATEGORY_SEATS") + "." + Resource.String("GAMING_TABLE_PROPERTY_ROTATE_ANGLE_SEATS");
          break;
        default:
          _resource = PropertyName;
          break;
      }
      return _resource;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Autolocation seats
    //
    //  PARAMS:
    //      - INPUT: 
    //          - List<IDrawGamingTablePanelObject>: PanelObjects
    //          - Int32: PanelWidth
    //          - Int32: PanelHeight
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - GamingTableProperties
    //
    //   NOTES:
    public static void AutoLocationSeats(List<IDrawGamingTablePanelObject> PanelObjects, Int32 PanelWidth, Int32 PanelHeight)
    {
      GamingTableProperties _table_properties;
      List<GamingSeatProperties> _seats_properties;
      List<Point> _seats_points;
      Double _angle_interval;
      Double _angle_dealer;
      Double _initial_angle;
      Point _center_seats;
      Double _radio_width;
      Double _radio_height;

      _seats_properties = new List<GamingSeatProperties>();
      _table_properties = null;
      _seats_points = new List<Point>();
      _angle_dealer = 0;
      _angle_interval = 0;
      _center_seats = new Point(0, 0);
      _radio_width = 0;
      _radio_height = 0;

      try
      {
        foreach (IDrawGamingTablePanelObject _object in PanelObjects)
        {
          if (_object.GetType() == typeof(GamingTableObject))
          {
            _table_properties = ((GamingTableObject)_object).TableProperties;
            _table_properties = UtilsDesign.AdjustTableProperties(_table_properties, PanelWidth, PanelHeight);

            UtilsDesign.SetPropertiesReadOnly("SeatsDistance", !_table_properties.AutoLocationSeats, _table_properties);
            UtilsDesign.SetPropertiesReadOnly("DealerAngle", !_table_properties.AutoLocationSeats, _table_properties);
            UtilsDesign.SetPropertiesReadOnly("RotateAngleSeats", !_table_properties.AutoLocationSeats, _table_properties);
            UtilsDesign.SetPropertiesReadOnly("LocationX", _table_properties.AutoLocationSeats, new GamingSeatProperties());
            UtilsDesign.SetPropertiesReadOnly("LocationY", _table_properties.AutoLocationSeats, new GamingSeatProperties());
           }
           if (_object.GetType() == typeof(GamingSeatObject))
           {
             _seats_properties.Add(((GamingSeatObject)_object).SeatProperties);
           }
        }

        if (_table_properties != null && _table_properties.AutoLocationSeats)
        {
          if (_table_properties.TableType == GamingTableDesign.GT_Design_Type.Type1)
          {
            _center_seats = new Point(PanelWidth / 2, PanelHeight - GamingTableDesign.BottomMarginGamingTable);
            _radio_width = _table_properties.Width / 2;
            _radio_height = _table_properties.Height;

            if (_table_properties.DealerAngle < 180)
            {
              _table_properties.DealerAngle = 180;
            }
          }
          else if (_table_properties.TableType == GamingTableDesign.GT_Design_Type.Type3 || _table_properties.TableType == GamingTableDesign.GT_Design_Type.Type2)
          {
            _center_seats = new Point(PanelWidth / 2, PanelHeight / 2);
            _radio_width = _table_properties.Width / 2;
            _radio_height = _table_properties.Height / 2;
          }
          _initial_angle = -UtilsDesign.DegreeToRadians(90) - UtilsDesign.DegreeToRadians(_table_properties.RotateAngleSeats);

          _angle_interval = UtilsDesign.DegreeToRadians(360);

          if (_table_properties.DealerAngle > 0)
          {
            _angle_dealer = UtilsDesign.DegreeToRadians(_table_properties.DealerAngle);
            _initial_angle -= _angle_dealer / 2;
            _angle_interval -= _angle_dealer;
            if (_seats_properties.Count > 1)
            {
              _angle_interval = _angle_interval / (_seats_properties.Count - 1);
            }
          }
          else
          {
            if (_seats_properties.Count > 0)
            {
              _angle_interval = _angle_interval / _seats_properties.Count;
            }
          }

          int _idx_seat = 0;

          foreach (GamingSeatProperties _seat in _seats_properties)
          {
            _seat.LocationX = (int)(_center_seats.X + (_table_properties.SeatsDistance + _radio_width) * Math.Cos(_angle_interval * _idx_seat - _initial_angle));
            _seat.LocationY = (int)(_center_seats.Y + (_table_properties.SeatsDistance + _radio_height) * Math.Sin(_angle_interval * _idx_seat - _initial_angle));

            _seat.LocationX -= _seat.Width / 2;
            _seat.LocationY -= _seat.Height / 2;

            // Check limits for new location
            Point MPosition = new Point();

            MPosition = UtilsDesign.CheckMaxPosition(_seat, PanelWidth, PanelHeight);

            _seat.LocationX = MPosition.X;
            _seat.LocationY = MPosition.Y;

            _idx_seat++;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Message("GamingTable.AutoLocationSeats() Error:" + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get XML gaming table
    //
    //  PARAMS:
    //      - INPUT: 
    //          - List<IDrawGamingTablePanelObject>: PanelObjects
    //
    //      - OUTPUT:
    //        - String: XMLTableDesign
    //
    // RETURNS:
    //          - True: Validate OK 
    //          - False: otherwise
    //
    //   NOTES:
    public static Boolean GetXMLGamingTable(List<IDrawGamingTablePanelObject> PanelObjects, out String XMLTableDesign)
    {
      String _xml;
      XmlDocument _xml_child;
      GamingTableProperties _table_properties;

      XMLTableDesign = String.Empty;
      _xml = String.Empty;
      _xml_child = new XmlDocument();
      _table_properties = null;

      XmlNode _xml_node;
      XmlDocument _table_design = new XmlDocument();
      _table_design.LoadXml(@"<TableDesign></TableDesign>");

      foreach (IDrawGamingTablePanelObject _object in PanelObjects)
      {
        if (_object.GetType() == typeof(GamingTableObject))
        {
          if (!UtilsDesign.ObjectSerializableToXml(((GamingTableObject)_object).TableProperties, out _xml))
          {
            return false;
          }

          // Load xml
          _xml_child.LoadXml(_xml);

          // Remove XML declaration
          if (_xml_child.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
          {
            _xml_child.RemoveChild(_xml_child.FirstChild);
          }

          // Add xml fragment
          _xml_node = _table_design.ImportNode(_xml_child.FirstChild, true);
          _table_design.DocumentElement.AppendChild(_xml_node);

          _table_properties = ((GamingTableObject)_object).TableProperties;

          break;
        }
      }

      foreach (IDrawGamingTablePanelObject _object in PanelObjects)
      {
        if (_object.GetType() == typeof(GamingSeatObject))
        {
          CalculateSeatsRelativePositionFromPanel(_table_properties, ((GamingSeatObject)_object).SeatProperties);
          if (!UtilsDesign.ObjectSerializableToXml(((GamingSeatObject)_object).SeatProperties, out _xml))
          {
            return false;
          }

          // Load xml
          _xml_child.LoadXml(_xml);

          // Remove XML declaration
          if (_xml_child.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
          {
            _xml_child.RemoveChild(_xml_child.FirstChild);
          }
          // Add xml fragment
          _xml_node = _table_design.ImportNode(_xml_child.FirstChild, true);
          _table_design.DocumentElement.AppendChild(_xml_node);
        }
      }

      XMLTableDesign = _table_design.InnerXml.ToString();

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate seats relative position from panel
    //
    //  PARAMS:
    //      - INPUT: 
    //          - GamingTableProperties: Table
    //          - GamingSeatProperties: Seat
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public static void CalculateSeatsRelativePositionFromPanel(GamingTableProperties Table, GamingSeatProperties Seat)
    {
      Point _location;

      _location = new Point(-Table.TableCenterPoint.X + Seat.LocationX, -Table.TableCenterPoint.Y + Seat.LocationY);

      Seat.RelativeLocationX = _location.X;
      Seat.RelativeLocationY = _location.Y;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate seats relative position to panel
    //
    //  PARAMS:
    //      - INPUT: 
    //          - GamingTableProperties: Table
    //          - GamingSeatProperties: Seat
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public static void CalculateSeatsRelativePositionToPanel(GamingTableProperties Table, GamingSeatProperties Seat)
    {
      Point _location;

      _location = new Point(Table.TableCenterPoint.X + Seat.RelativeLocationX, Table.TableCenterPoint.Y + Seat.RelativeLocationY);

      Seat.LocationX = _location.X;
      Seat.LocationY = _location.Y;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set seat color by status
    //
    //  PARAMS:
    //      - INPUT: 
    //          - GamingSeatProperties SeatProperties
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    public static void SetSeatColorByStatus(GamingSeatProperties SeatProperties)
    {
      if (SeatProperties.SeatStatus == SEAT_STATUS_PLAYER_TRACKING.DISABLED)
      {
        SeatProperties.BorderInteriorColor = ColorTranslator.FromHtml("#898989");
        SeatProperties.GradientCenterColor = ColorTranslator.FromHtml("#898989");
        SeatProperties.GradientOuterColor = ColorTranslator.FromHtml("#898989");
      }
      else if (SeatProperties.SeatStatus == SEAT_STATUS_PLAYER_TRACKING.FREE)
      {
        SeatProperties.BorderInteriorColor = ColorTranslator.FromHtml("#278C47");
        SeatProperties.GradientCenterColor = ColorTranslator.FromHtml("#278C47");
        SeatProperties.GradientOuterColor = ColorTranslator.FromHtml("#278C47");
      }
      else if (SeatProperties.SeatStatus == SEAT_STATUS_PLAYER_TRACKING.OCUPATED)
      {

        SeatProperties.BorderInteriorColor = ColorTranslator.FromHtml("#23568B");
        SeatProperties.GradientCenterColor = ColorTranslator.FromHtml("#23568B");
        SeatProperties.GradientOuterColor = ColorTranslator.FromHtml("#23568B");
      }
      else if (SeatProperties.SeatStatus == SEAT_STATUS_PLAYER_TRACKING.WALK)
      {
        SeatProperties.BorderInteriorColor = ColorTranslator.FromHtml("#E06818 ");
        SeatProperties.GradientCenterColor = ColorTranslator.FromHtml("#E06818 ");
        SeatProperties.GradientOuterColor = ColorTranslator.FromHtml("#E06818 ");
      }
      else
      {
        SeatProperties.BorderInteriorColor = ColorTranslator.FromHtml("#FF0000");
        SeatProperties.GradientCenterColor = ColorTranslator.FromHtml("#FF0000");
        SeatProperties.GradientOuterColor = ColorTranslator.FromHtml("#FF0000");
      }
    }

    public static void SetPropertiesReadOnly(String Property, Boolean ReadOnly, Object Type)
    {
      PropertyDescriptor descriptor = TypeDescriptor.GetProperties(Type.GetType())[Property];
      ReadOnlyAttribute attribute = (ReadOnlyAttribute)descriptor.Attributes[typeof(ReadOnlyAttribute)];
      FieldInfo fieldToChange = attribute.GetType().GetField("isReadOnly", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

      fieldToChange.SetValue(attribute, ReadOnly);
    }

    public static void DrawSeatObject(Graphics Gr, GamingSeatProperties SeatProperties, Boolean IsSelected, Matrix TransformMatrix)
    {
      try
      {
        using (GraphicsPath _path = new GraphicsPath())
        {
          Pen _border_interior;
          Pen _border_outer;
          PathGradientBrush _pthGrBrush;
          GraphicsPath _path_circle;
          Size _label_top_size;
          Size _label_bottom_size;
          Rectangle _rect_circle;
          Point _location_to_draw;
          List<Point> _pointsPol;
          RectangleF _top_label;
          RectangleF _bottom_label_1;
          RectangleF _bottom_label_2;
          StringFormat _string_format;
          double _rotation_angle;

          Gr.SmoothingMode = SmoothingMode.AntiAlias;
          _path_circle = new GraphicsPath();
          _label_top_size = new Size(120, 12);
          _label_bottom_size = new Size(120, 12);
          _location_to_draw = new Point();
          _pointsPol = new List<Point>();
          _rotation_angle = (45 + SeatProperties.RotateAngle) * (Math.PI / 180);
          _string_format = new StringFormat();

          UtilsDesign.SetSeatColorByStatus(SeatProperties);

          if (SeatProperties.Height > SeatProperties.Width)
          {
            SeatProperties.Height = SeatProperties.Width;
          }
          else
          {
            SeatProperties.Width = SeatProperties.Height;
          }

          // Draw seat
          _rect_circle = new Rectangle(new Point(SeatProperties.LocationX, SeatProperties.LocationY), new Size(SeatProperties.Width, SeatProperties.Height));
          _path_circle.StartFigure();
          _path_circle.AddEllipse(_rect_circle);
          _path_circle.CloseFigure();

          // If seat is selected change the outer border to yellow
          if (IsSelected)
          {
            _border_outer = new Pen(Color.Black);
          }
          else
          {
            _border_outer = new Pen(SeatProperties.BorderOuterColor);
          }

          _border_outer.Width = SeatProperties.BorderOuterSize;

          _border_interior = new Pen(SeatProperties.BorderInteriorColor);
          _border_interior.Width = SeatProperties.BorderInteriorSize;

          _pthGrBrush = new PathGradientBrush(_path_circle);
          _pthGrBrush.CenterColor = SeatProperties.GradientCenterColor;
          _pthGrBrush.CenterPoint = new Point(SeatProperties.Width / 2 + SeatProperties.LocationX, SeatProperties.Height / 2 + SeatProperties.LocationY);

          Color[] colorsPol = { SeatProperties.GradientOuterColor };
          _pthGrBrush.SurroundColors = colorsPol;

          _location_to_draw.X = SeatProperties.LocationX - _label_top_size.Width / 2 + SeatProperties.Height / 2;
          _location_to_draw.Y = SeatProperties.LocationY - 14;

          // Create the labels of the seat
          _top_label = new RectangleF(_location_to_draw, new Size(_label_top_size.Width, _label_top_size.Height));

          _location_to_draw.Y = SeatProperties.LocationY + SeatProperties.Height + 3;
          _bottom_label_1 = new RectangleF(_location_to_draw, new Size(_label_bottom_size.Width, _label_bottom_size.Height));
          _location_to_draw.Y += _label_bottom_size.Height;
          _bottom_label_2 = new RectangleF(_location_to_draw, new Size(_label_bottom_size.Width, _label_bottom_size.Height));

          _path.StartFigure();
          _path.AddEllipse(new Rectangle(new Point((int)(SeatProperties.LocationX - _border_outer.Width), (int)(SeatProperties.LocationY - _border_outer.Width)), new Size((int)(SeatProperties.Width + _border_outer.Width * 2), (int)(SeatProperties.Height + _border_outer.Width * 2))));
          _path.CloseFigure();

          _path.AddRectangle(_top_label);
          _path.AddRectangle(_bottom_label_1);
          _path.AddRectangle(_bottom_label_2);

          Gr.SetClip(_path);
          Gr.FillEllipse(_pthGrBrush, new Rectangle(new Point((int)(SeatProperties.LocationX + _border_interior.Width / 2), (int)(SeatProperties.LocationY + _border_interior.Width / 2)), new Size((int)(SeatProperties.Width - _border_interior.Width), (int)(SeatProperties.Height - _border_interior.Width))));
          Gr.DrawEllipse(_border_interior, new Rectangle(new Point((int)(SeatProperties.LocationX + _border_interior.Width / 2), (int)(SeatProperties.LocationY + _border_interior.Width / 2)), new Size((int)(SeatProperties.Width - _border_interior.Width), (int)(SeatProperties.Height - _border_interior.Width))));
          Gr.DrawEllipse(_border_outer, new Rectangle(new Point(SeatProperties.LocationX, SeatProperties.LocationY), new Size(SeatProperties.Width, SeatProperties.Height)));

          SeatProperties.Path = (GraphicsPath)_path.Clone();

          if (TransformMatrix != null)
          {
            SeatProperties.Path.Transform(TransformMatrix);
          }

          _string_format.LineAlignment = StringAlignment.Center;
          _string_format.Alignment = StringAlignment.Center;
          _string_format.FormatFlags = StringFormatFlags.NoWrap;
          _string_format.Trimming = StringTrimming.EllipsisWord;

          // Draw position label of the seat
          using (GraphicsPath _gp_position = new GraphicsPath())
          {
            _gp_position.AddString(SeatProperties.SeatPosition.ToString(), FontFamily.GenericSansSerif, (int)FontStyle.Bold, 16f, _rect_circle, _string_format);
            //Gr.DrawPath(new Pen(Color.White, 2f), _gp_position);
            Gr.FillPath(Brushes.White, _gp_position);
          }

          // Draw top label of the seat
          if (!String.IsNullOrEmpty(SeatProperties.AccountName))
          {
            using (GraphicsPath _gp_name = new GraphicsPath())
            {
              _gp_name.AddString(SeatProperties.AccountName, FontFamily.GenericSansSerif, (int)FontStyle.Bold, 12f, _top_label, _string_format);
              //Gr.DrawPath(new Pen(Color.White, 3f), _gp_name);
              Gr.FillPath(Brushes.Black, _gp_name);
            }
          }

          // Draw bottom label 1 of the seat
          if (!String.IsNullOrEmpty(SeatProperties.BottomLabelFirstLine) && (SeatProperties.SeatStatus == SEAT_STATUS_PLAYER_TRACKING.OCUPATED || SeatProperties.SeatStatus == SEAT_STATUS_PLAYER_TRACKING.WALK))
          {
            using (GraphicsPath _gp_bottom_label_1 = new GraphicsPath())
            {
              _gp_bottom_label_1.AddString(SeatProperties.BottomLabelFirstLine, FontFamily.GenericSansSerif, (int)FontStyle.Bold, 10f, _bottom_label_1, _string_format);

              if ((SeatProperties.Plays.HasValue && SeatProperties.InSessionLastPlay.HasValue &&
                  SeatProperties.Plays.Value - SeatProperties.InSessionLastPlay.Value > SeatProperties.TableIdlePlays) ||
                 (SeatProperties.CurrentBet.HasValue && SeatProperties.TableMinBet.HasValue && SeatProperties.TableMaxBet.HasValue &&
                 ((SeatProperties.CurrentBet.Value < SeatProperties.TableMinBet.Value) || (SeatProperties.TableMaxBet.Value != 0 && SeatProperties.CurrentBet.Value > SeatProperties.TableMaxBet.Value))))
              {
                if (GamingTableDesign.ColorTimerLastUpdatedSeat != Color.White)
                {
                  Gr.DrawPath(new Pen(GamingTableDesign.ColorTimerLastUpdatedSeat, 1f), _gp_bottom_label_1);
                }
              }
              else
              {
                //Gr.DrawPath(new Pen(Color.White, 3f), _gp_bottom_label_1);
              }
              Gr.FillPath(Brushes.Black, _gp_bottom_label_1);
            }
          }

          // Draw bottom label 2 of the seat
          if (!String.IsNullOrEmpty(SeatProperties.BottomLabelSecondLine) && (SeatProperties.SeatStatus == SEAT_STATUS_PLAYER_TRACKING.OCUPATED || SeatProperties.SeatStatus == SEAT_STATUS_PLAYER_TRACKING.WALK))
          {
            using (GraphicsPath _gp_bottom_label_2 = new GraphicsPath())
            {
              _gp_bottom_label_2.AddString(SeatProperties.BottomLabelSecondLine, FontFamily.GenericSansSerif, (int)FontStyle.Bold, 10f, _bottom_label_2, _string_format);

              //Gr.DrawPath(new Pen(Color.White, 3f), _gp_bottom_label_2);
              Gr.FillPath(Brushes.Black, _gp_bottom_label_2);
            }
          }

          _string_format.Dispose();
          _path.Dispose();
          _path_circle.Dispose();
          _border_interior.Dispose();
          _border_outer.Dispose();
          _pthGrBrush.Dispose();
        }
      }
      catch (Exception _ex)
      {
        Log.Message("GamingTableObject.DrawObject Error: " + _ex.Message);
      }
    }

    public static Bitmap GetDrawSeatImage(GamingSeatProperties SeatProperties)
    {
      Bitmap _bm;

      _bm = new Bitmap(SeatProperties.Width, SeatProperties.Height);

      using (Graphics _gr = Graphics.FromImage(_bm))
      {
        UtilsDesign.DrawSeatObject(_gr, SeatProperties, false, null);
      }

      return _bm;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check position is in the table limits
    //
    //  PARAMS:
    //      - INPUT: 
    //            - Point to check
    //            - GamingSeatProperties Properties seat 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - Point into the limits
    //
    //   NOTES:
    public static Point CheckMaxPosition(GamingSeatProperties SeatProps, int width, int height)
    {
      int _object_size_rad;
      int _object_width;
      int _object_height;
      Point _position;
      Point _position_original;

      _position = new Point();
      _position_original = new Point();

      _position.X = SeatProps.LocationX;
      _position.Y = SeatProps.LocationY;
      _position_original.X = SeatProps.LocationX;
      _position_original.Y = SeatProps.LocationY;

      _object_width = SeatProps.Width;
      _object_height = SeatProps.Height;
      _object_size_rad = (_object_width / 2);

      //_position.X -= _object_width / 2;
      //_position.Y -= _object_height / 2;

      if (_position.X < _object_size_rad * 2)
        _position.X = _object_size_rad * 2;
      if (_position.X > width - _object_size_rad * 4)
        _position.X = width - _object_size_rad * 4;
      if (_position.Y < _object_size_rad)
        _position.Y = _object_size_rad;
      if (_position.Y > height - _object_size_rad * 3 - (int)SeatProps.BorderOuterSize)
        _position.Y = height - _object_size_rad * 3 - (int)SeatProps.BorderOuterSize;

      if (_position != _position_original)
      {
        return _position;
      }
      else
      {
        return _position_original;
      }

    } // CheckMaxPosition

  }

  public class XmlColor
  {
    private Color m_color = Color.Black;

    public XmlColor() { }
    public XmlColor(Color c) { m_color = c; }


    public Color ToColor()
    {
      return m_color;
    }

    public void FromColor(Color c)
    {
      m_color = c;
    }

    public static implicit operator Color(XmlColor x)
    {
      return x.ToColor();
    }

    public static implicit operator XmlColor(Color c)
    {
      return new XmlColor(c);
    }

    [XmlText]
    public string Web
    {
      get { return ColorTranslator.ToHtml(m_color); }
      set
      {
        try
        {
          m_color = ColorTranslator.FromHtml(value);
        }
        catch (Exception)
        {
          m_color = Color.Black;
        }
      }
    }
  }

  public class uc_transparent_panel : Panel
  {
    Image m_image;
    Int32 m_opacity;

    public delegate void ButtonClickEventHandler(object sender);
    public event ButtonClickEventHandler ButtonClick;

    public Image Image
    {
      get
      {
        return m_image;
      }
      set
      {
        m_image = value;
      }
    }

    public Int32 Opacity
    {
      get
      {
        return m_opacity;
      }
      set
      {
        m_opacity = value;
      }
    }

    public uc_transparent_panel()
    {
      this.DoubleBuffered = true;
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(m_opacity, BackColor)), new RectangleF(0, 0, this.Width, this.Height));

      if (m_image != null)
      {
        e.Graphics.DrawImage(m_image, (Int32)((this.Width - m_image.Width) / 2), (Int32)((this.Height - m_image.Height) / 2), m_image.Width, m_image.Height);
      }
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      Region _button_region;
      GraphicsPath _gp;

      if (m_image != null) 
      {
        _gp = new GraphicsPath();
        _gp.AddEllipse(new Rectangle((Int32)((this.Width - m_image.Width) / 2), (Int32)((this.Height - m_image.Height) / 2), m_image.Width, m_image.Height));
        _button_region = new System.Drawing.Region(_gp);

        if (_button_region.IsVisible(new Point(((MouseEventArgs)e).X, ((MouseEventArgs)e).Y)))
        {
          if (ButtonClick != null)
          {
            ButtonClick(this);
          }
        }
      }    

      base.OnMouseDown(e);
    }
  }


}
