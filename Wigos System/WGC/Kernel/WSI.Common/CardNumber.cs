//------------------------------------------------------------------------------
// Copyright � 2008-2009 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CardNumber.cs
// 
//   DESCRIPTION: Magnetic card conversion functions
// 
//        AUTHOR: Agust� Poch
// 
// CREATION DATE: 08-OCT-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-OCT-2008 APB    First release.
// 18-MAR-2016 YNM    Fixed Bug 10744: With GP requiereNewCard a "0" new account, trackdata is showed like "000-Recycled-Virtual.."
//------------------------------------------------------------------------------
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace WSI.Common
{
  public class CardNumber
  {

    #region Methods

    public static bool TrackDataToInternal(String ExternalTrackData, out String InternalTrackData, out int CardType)
    {
      CardTrackData _card_track_data;

      _card_track_data = new   ExternalTrackData(ExternalTrackData);
      InternalTrackData = _card_track_data.InternalTrackData;
      CardType = (int)_card_track_data.CardType;
      return _card_track_data.IsCorrectTrackData;

    } // TrackDataToInternal

    public static bool TrackDataToInternal(String ExternalTrackData, out String InternalTrackData)
        {
      CardTrackData _card_track_data;

      _card_track_data = new ExternalTrackData(ExternalTrackData);
      InternalTrackData = _card_track_data.InternalTrackData;

      return _card_track_data.IsCorrectTrackData;

    } // TrackDataToInternal

    //------------------------------------------------------------------------------
    // PURPOSE: Receive and convert the internal card data to an external track data (encoded)
    // to be recorded
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId: internal card identifier (the one stored in the database)
    //          - CardType: card type.
    //
    //      - OUTPUT:
    //          - TrackData: External card data (encoded).
    //
    // RETURNS:
    //      - True: Track data has been built using the incoming parameters
    //      - False: CardId format error or CardType not supported
    // 
    //   NOTES:
    //    Card pattern: (20 digits)
    //      LCCCCSSSSNNNNNNNNNTT
    //    Where:
    //    - L: must be 0 (1 digit)
    //    - CCCC: Checksum. (4 digits)
    //    - SSSSNNNNNNNNN: Internal TrackData. (13 digits)
    //      - SSSS:      Site ID.         (4 digits)
    //      - NNNNNNNNN: Sequence Number. (9 digits)
    //    - TT is the card type (2 digits)
    //
    public static bool TrackDataToExternal(out String ExternalTrackData, String InternalTrackData, int CardType)
    {
      CardTrackData _car_track_Data;
      _car_track_Data = new InternalTrackData(InternalTrackData, (CardTrackData.CardsType)CardType);//  CardType
      ExternalTrackData = _car_track_Data.ExternalTrackData;
      return _car_track_Data.IsCorrectTrackData;
    } // TrackDataToExternal

    public static bool VisibleTrackData(out String VisibleTrackData, String InternalTrackData, Int32 CardType, AccountType AccType)
    {

      CardTrackData _card_track_data;

      _card_track_data = new InternalTrackData(InternalTrackData, (CardTrackData.CardsType)CardType);      
      VisibleTrackData = _card_track_data.VisibleTrackData(AccType);
      return _card_track_data.IsCorrectTrackData;

    } // VisibleTrackData

    //------------------------------------------------------------------------------
    // PURPOSE: Compose the card internal id. 
    // 
    // PARAMS:
    //      - INPUT:
    //          - SiteId: Site id.
    //          - Sequence: Sequence number.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Card number.
    // 
    // NOTES:
    //


    //------------------------------------------------------------------------------
    // PURPOSE: Split card number into its components:
    //          - Site Id.
    //          - Sequence number.
    // 
    // PARAMS:
    //      - INPUT:
    //          - CardNumber: Composed card internal id.
    //
    //      - OUTPUT:
    //          - SiteId: Site id.
    //          - Sequence: Sequence number.
    //
    // RETURNS:
    //
    // NOTES:
    //
    public static void SplitInternalTrackData(String InternalTrackData, out Int32 SiteId, out Int64 Sequence)
    {
      CardTrackData _card_track_data;

      _card_track_data = new InternalTrackData(InternalTrackData, CardTrackData.CardsType.PLAYER);
      
      SiteId = _card_track_data.SiteId;
      Sequence = _card_track_data.Sequence;

    } // SplitInternalTrackData

    //------------------------------------------------------------------------------
    // PURPOSE: Check if track data pertains to the specified site id.
    // 
    // PARAMS:
    //      - INPUT:
    //          - SiteId: Id of the site to check if track data pertains to it.
    //          - ExternalTrackData 
    //
    // RETURNS: True: Track data pertains to the specified site id, False : Otherwise.
    //
    // NOTES:
    //
    public static Boolean ExternalTrackDataBelongToSite(int SiteId, String ExternalTrackData)
    {
      CardTrackData _card_track_data;

      _card_track_data = new ExternalTrackData(ExternalTrackData);

      return (_card_track_data.SiteId == SiteId);
    } // ExternalTrackDataBelongToSite

    //------------------------------------------------------------------------------
    // PURPOSE: Convert track data to a db storable string 
    // 
    // PARAMS:
    //      - INPUT:
    //          - Mode: Mode used to encrypt the track data.
    //          - ReceivedTrackData: Received Track data.
    //
    //      - OUTPUT:
    //          - DBTrackData: Track data stored on db.
    //
    // RETURNS:
    //
    // NOTES:
    //
    public static Boolean ConvertTrackData(CardTrackData.CriptMode Mode, String ReceivedTrackData, out String DBTrackData)
    {
      DBTrackData = ReceivedTrackData;

      if (Mode == CardTrackData.CriptMode.WCP)
      {
        int card_type;

        return TrackDataToInternal(ReceivedTrackData, out DBTrackData, out card_type);
      }

      return true;

    }// ConvertTrackData

    public static String MakeInternalTrackData(int SiteId, Int64 Sequence)
    {
      return CardTrackData.MakeInternalTrackData(SiteId, Sequence);

    } // MakeInternalTrackData

    #endregion
  } // class CardNumber
}
