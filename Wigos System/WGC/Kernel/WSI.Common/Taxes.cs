//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   Taxes.cs
// DESCRIPTION:   
// AUTHOR:        Andreu Juli�
// CREATION DATE: 27-JUN-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 27-JUN-2012  AJQ    Initial version
// 07-APR-2014  AJQ    Added a configurable tolerance for TaxReturning
// 10-APR-2014  RMS    Fixed Bug WIG-805: Service Charge + IEJC problem
// 20-JUN-2014  MPO    // AJQ 18-JUN-2014, Service Charge Apply Ratio on Recharge
// 28-AUG-2014  AJQ    Fixed Bug WIG-1186: Codere: Return also ServiceCharge if return taxes are all the taxes.
// 14-APR-2015  YNM    Fixed Bug TFS-1088: Ticket Handpay/Ticket CashOut: Tax breakdown on Tax Detail/Deductions. 
// 17-APR-2015  YNM    Fixed Bug WIG-2222: Ticket Handpay error calculating taxes.
// 23-APR-2015  YNM    Fixed Bug WIG-2240: Payment Order + Chips Buy take cashier taxes
// 11-NOV-2015  FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 17-NOV-2015  DLL    Backlog Item 6505: Apply tax to all withdrawal. Without Devolution.
// 17-NOV-2015  FAV    Backlog Item 6629: Custom reimbursement voucher (Migration to MICO)
// 20-APR-2016  EOR    Product Backlog Item 11296: Codere - Custody Tax: Implement Devolution Tax Custody
// 23-NOV-2015  FOS    Bug 6771:Payment tickets & handpays
// 01-DEC-2015  FOS    Fixed Bug 7205: Protected taxWaiverMode and renamed function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015  FOS    Fixed Bug 7208: Incorrect Tax Value on Handpays
// 23-NOV-2015  FOS    Bug 6771:Payment tickets & handpays
// 11-NOV-2015  FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 17-NOV-2015  DLL    Backlog Item 6505: Apply tax to all withdrawal. Without Devolution.
// 17-NOV-2015  FAV    Backlog Item 6629: Custom reimbursement voucher (Migration to MICO)
// 18-DEC-2015  FOS    Fixed Bug 6769: It doesn't delete taxes when TITO tickets are paid
// 25-JAN-2016  FOS    Fixed Bug 8594: Using correctly the param Handpay.Machine.applyTaxesByDefault
// 21-APR-2016  FOS    Fixed Bug 11698: Jackpot by level
// 31-MAY-2016  EOR    Fixed Bug 12945: Custoudy Wrong Calculate for redeem without prize
// 04-OCT-2016  LTC    Product Backlog Item 17448:Televisa - Cash desk draw (Cash) - Voucher
// 07-FEB-2017 XGJ&ETP PBI 22249: Machine Draw
// 08-FEB-2017  JML    BUG 25253: Errors on Custody in case Type Machines (1)
// 08-MAR-2017  FAV    PBI 25268: Third TAX - Payment Voucher
// 09-MAR-2017  FGB    PBI 25269: Third TAX - Cash session summary
// 15-MAR-2017  FGB    PBI 25268: Third TAX - Payment Voucher
// 05-APR-2017  JML    Bug 26568:Custodia - add new general param to configure tax custody for gaming tables operations
// 20-APR-2017  JML    Fixed Bug 26807: Custody - Incorrect custody amount to be refunded
// 19-JUN-2017  RGR    Fixed Bug 28185: WIGOS-2609 Wrong calculation in tax withholding when buying chips
// 16-OCT-2017  DHA    Bug 30269:WIGOS-5804 Error in rounding applied to Gambling tables
// 02-NOV-2017  AJQ & JCA WIGOS-6318: Tax on Prizes: different taxes for 'normal prizes' and 'cash-draw prizes'.
// 11-JAN-2017  JML & DHA Fixed Bug 31240:WIGOS-7338 [Ticket #11369] Errores en cargo por servicio y sorteo en terminal
// 21-FEB-2018  DHA   Bug 31619:WIGOS-8109 [Ticket #12335] Custodia
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public class Tax
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Get the General param value, that define if use tax Collection.
    //
    //  PARAMS :
    //      - INPUT :
    //          
    //      - OUTPUT :
    //            
    //
    // RETURNS :
    //      - Boolean value
    //
    //   NOTES :
    public static Boolean EnableTitoTaxWaiver(Boolean ParamEnabled)
    {
      if (!TITO.Utils.IsTitoMode())
      {
        return false;
      }
      else
      {
        return (ParamEnabled && GeneralParam.GetInt32("Cashier.TITOTaxWaiver", "Mode") > 0);
      }
    }

    public static Boolean ApplyTaxCollectionByHandPay(HANDPAY_TYPE HandPayType, int HandPay_level = 0)
    {
      string _handPay_type;

      switch (HandPayType)
      {
        case HANDPAY_TYPE.JACKPOT:
          _handPay_type = "Handpay.Machine.ApplyTaxesByDefault";
          break;

        case HANDPAY_TYPE.MANUAL_JACKPOT:
          _handPay_type = "Handpay.Manual.Jackpot.ApplyTaxesByDefault";

          if (HandPay_level > 0 && HandPay_level < 64)
          {
            _handPay_type = "Handpay.Manual.ProgJackpot.ApplyTaxesByDefault";
          }
          else if (HandPay_level == 64)
          {
            _handPay_type = "Handpay.Manual.MaxPrize.ApplyTaxesByDefault";
          }

          break;

        case HANDPAY_TYPE.CANCELLED_CREDITS:
          _handPay_type = "Handpay.Machine.ApplyTaxesByDefault";
          break;

        case HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS:
          _handPay_type = "Handpay.Manual.CancelCredits.ApplyTaxesByDefault";
          break;

        case HANDPAY_TYPE.MANUAL_OTHERS:
          _handPay_type = "Handpay.Manual.Other.ApplyTaxesByDefault";
          break;

        case HANDPAY_TYPE.SITE_JACKPOT:
        case HANDPAY_TYPE.UNKNOWN:
          _handPay_type = "Handpay.Manual.Jackpot.ApplyTaxesByDefault";
          break;

        default:
          _handPay_type = string.Empty;
          break;
      }

      return GeneralParam.GetInt32("Cashier.TITOTaxWaiver", _handPay_type) > 0;
    }

    public static Boolean ApplyTaxCollectionByMovementType(CASHIER_MOVEMENT CashierMovement)
    {
      Boolean _is_apply_tax;
      Boolean _is_enabled_apply_tax;

      _is_enabled_apply_tax = false;
      _is_apply_tax = EnableTitoTaxWaiver(true);

      switch (CashierMovement)
      {
        case CASHIER_MOVEMENT.CASH_IN:
        case CASHIER_MOVEMENT.CASH_OUT:
        case CASHIER_MOVEMENT.PRIZES:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE1:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE2:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE3:
        case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
        case CASHIER_MOVEMENT.CARD_DEPOSIT_OUT:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE:
        case CASHIER_MOVEMENT.MB_EXCESS:
          break;

        case CASHIER_MOVEMENT.HANDPAY:
        case CASHIER_MOVEMENT.HANDPAY_CANCELLATION:
        case CASHIER_MOVEMENT.MANUAL_HANDPAY:
        case CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION:
        case CASHIER_MOVEMENT.HANDPAY_AUTHORIZED:
        case CASHIER_MOVEMENT.HANDPAY_UNAUTHORIZED:
        case CASHIER_MOVEMENT.HANDPAY_VOIDED:
        case CASHIER_MOVEMENT.HANDPAY_VOIDED_UNDONE:
          _is_enabled_apply_tax = ApplyTaxCollectionByHandPay(HANDPAY_TYPE.MANUAL_JACKPOT);
          break;

        case CASHIER_MOVEMENT.CHIPS_SALE:
        case CASHIER_MOVEMENT.CHIPS_PURCHASE:
        case CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO:
        case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
        case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
          _is_enabled_apply_tax = GeneralParam.GetInt32("Cashier.TITOTaxWaiver", "TicketPayment.ApplyTaxesByDefault") > 0;
          break;

        default:
          break;
      }

      return _is_apply_tax && _is_enabled_apply_tax;
    }

    public static Boolean ApplyTaxCollectionTicketPayment()
    {
      return GeneralParam.GetInt32("Cashier.TITOTaxWaiver", "TicketPayment.ApplyTaxesByDefault") > 0;
    }

    public static Boolean ApplyTaxChipsPurchase()
    {
      return GeneralParam.GetInt32("Cashier.TITOTaxWaiver", "ChipsPurchase.ApplyTaxesByDefault") > 0;
    }

    public static Boolean ApplyTaxButtonEnabled(Decimal TotalPaid)
    {
      return TotalPaid > GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.1.Threshold");
    }

    //LTC 04-OCT-2016
    public static Boolean ServiceChageOnlyCancellable { get; set; }
  }

  public enum ServiceChargeMode
  {
    None = 0,
    EnabledPrizeComputedOnDeposit = 1,
    EnabledPrizeComputedOnRecharge = 2,
  }

  public enum TaxReturnMode
  {
    None = 0,
    Partial = 1,
    Total = 2,
  }

  public enum Split_Taxes_Amount
  {
    TOTAL = 0,
    CARD = 1,
    COMPANY_B = 2,
  }

  public class DevolutionPrizeParts
  {
    public Currency Devolution = 0;
    public Currency Prize = 0;
    public Currency DevolutionOnlyCancellable = 0; // LTC 04-OCT-2016

    public TaxParameters Taxes
    {
      get
      {
        return m_taxes;
      }
    }

    public Boolean IsPromotionWithTaxes
    {
      get
      {
        return m_promotion_with_taxes;
      }
    }

    public TaxReturnMode TaxReturnMode = TaxReturnMode.None;
    public Decimal TaxReturnTolerance = 0.0m;
    public Boolean PrizeCouponEnabled = false;
    public Decimal PrizeCouponRatio = 0;
    public Decimal RechargeRatio = 1.0m;
    public Currency TaxReturn = 0;

    public Boolean DecimalRoundingEnabled = false;
    public Currency DecimalRounding = 0;
    public Int64 RoundToCents = 0;

    public ServiceChargeMode ServiceChargeMode = ServiceChargeMode.None;
    public Decimal ServiceCharge = 0;

    private TaxParameters m_taxes = null;
    private Boolean m_promotion_with_taxes;

    private Decimal m_service_charge_on_winner_prize = 0;
    private Decimal m_service_charge_on_winner_refund = 0;
    private Decimal m_service_charge_on_loser_refund = 0;
    // AJQ 18-JUN-2014, Service Charge Apply Ratio on Recharge
    private Decimal m_service_charge_recharge_ratio = 0;

    //EOR 19-APR-2016
    private Boolean m_tax_custody_enabled = false;
    private Decimal m_tax_custody_pct = 0;
    public Currency DevolutionCustody;
    public Currency MaxDevolutionCustody = 0;
    private Misc.TaxCustodyType m_tax_custody_type = 0;

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate a value.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Balance
    //          - MaxDevolution
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - A Decimal value 
    public Decimal CalculateDevolutionCustody(Currency Balance, Currency MaxDevolution, Boolean IsChipsPurchaseWithCashOut)
    {
      // TODO AJQ & DDM: Pendiente de validar los par�metros de entrada..
      Decimal _value_custody;
      Decimal _dev;

      Decimal _pct;
      _value_custody = 0;

      if (!m_tax_custody_enabled)
      {
        return _value_custody;
      }

      switch (m_tax_custody_type)
      {
        case Misc.TaxCustodyType.OnlyMachines:
          if (IsChipsPurchaseWithCashOut)
          {
            return _value_custody;
          }

          break;
        case Misc.TaxCustodyType.OnlyGamingTables:
          if (!IsChipsPurchaseWithCashOut)
          {
            return _value_custody;
          }

          break;
        case Misc.TaxCustodyType.Both:
          //OK

          break;
        default:
          // Error: Not configured
          return _value_custody;
      }

      _pct = m_tax_custody_pct / 100;
      switch (Misc.GetCustodyMode())
      {
        case Misc.TaxCustodyMode.TaxOnSplit1:
          Decimal _provision;
          Decimal _diff;
          _provision = CalculateDevolutionProvision(MaxDevolution);
          MaxDevolution = MaxDevolution + _provision;
          MaxDevolution = Math.Max(Math.Min(MaxDevolution, Balance), 0);
          _value_custody = Math.Round(MaxDevolution * (1 / (1 - _pct)) * _pct, 2);
          _diff = MaxDevolution - Balance;

          if (_diff > 0)
          {
            _value_custody = _value_custody - (_diff * _pct);
          }

          break;

        case Misc.TaxCustodyMode.TaxOnDeposit:
        default:
          _dev = Math.Max(Math.Min(MaxDevolution, Balance), 0);
          _value_custody = Math.Round(_dev * _pct, 2);

          break;
      }

      _value_custody = Math.Max(Math.Min(_value_custody, this.MaxDevolutionCustody), 0);

      return _value_custody;
    } // CalcuteDevolutionCustody

    private Decimal CalculateDevolutionProvision(Currency MaxDevolution)
    {
      Decimal _pct;
      Decimal _value_provision;

      _value_provision = 0;

      if (Misc.IsTaxProvisionsEnabled())
      {
        _pct = Misc.TaxProvisionsPct() / 100;
        _value_provision = Math.Round(MaxDevolution * (1 / (1 - _pct)) * _pct, 2);
      }

      return _value_provision;
    } // CalculateDevolutionProvision

    //------------------------------------------------------------------------------
    // PURPOSE: Inflate a value by the cash in tax percentages when cash in tax enabled.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Base
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - A Decimal value with the Base inflated by Cash In Tax percentages or same value if cash in tax is not enabled
    //
    public static Decimal InflateDueToCashInTax(Decimal Base)
    {
      Decimal _value;
      Decimal _base_pct;
      Decimal _pct;

      _value = Base;

      // Check for Tax IEJC enabled
      if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled"))
      {
        _base_pct = GeneralParam.GetDecimal("Cashier", "CashInTax.BasePct");
        _pct = GeneralParam.GetDecimal("Cashier", "CashInTax.Pct");

        // Calculate the real RechargeRatio
        // We don't round the value to get a more accurate value
        _value = (10000 * Base) / (10000 - (_base_pct * _pct));
      }

      return _value;
    } // InflateDueToCashInTax

    public DevolutionPrizeParts()
      : this(false)
    {
    }

    public DevolutionPrizeParts(Boolean IsChipsOperation)
    {
      Init(IsChipsOperation ? CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_CHIPS : CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_DEFAULT);
    }
    public DevolutionPrizeParts(CashDeskDrawBusinessLogic.ENUM_TAX_TYPE TaxType)
    {
      Init(TaxType);
    }

    public void Init(CashDeskDrawBusinessLogic.ENUM_TAX_TYPE TaxType)
    {
      TYPE_SPLITS _splits;
      Boolean IsChipsOperation;

      IsChipsOperation = (TaxType == CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_CHIPS);

      m_taxes = TaxParameters.Read(TaxType);
      Boolean _is_enabled_cash_desk_02;

      _is_enabled_cash_desk_02 = CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws((Int16)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02);

      if (!Split.ReadSplitParameters(out _splits))
      {
        return;
      }

      //EOR 19-APR-2016
      if (Misc.IsTaxCustodyEnabled())
      {
        m_tax_custody_enabled = true;
        if (IsChipsOperation)
        {
          m_tax_custody_pct = Misc.GamingTablesTaxCustodyPct();
        }
        else
        {
          m_tax_custody_pct = Misc.TaxCustodyPct();
        }
        m_tax_custody_type = (Misc.TaxCustodyType)Misc.GetTaxCustodyType();
      }

      RechargeRatio = 1.0m;
      PrizeCouponRatio = 0;
      PrizeCouponEnabled = false;
      TaxReturnMode = TaxReturnMode.None;

      // We need allways calculated Recharge Ratio except for Televisa in mode terminal draw (Service charge)
      if (_splits.prize_coupon && (!_is_enabled_cash_desk_02 || !Misc.IsVoucherModeTV()))
      {
        if (_splits.company_a.cashin_pct > 0)
        {
          PrizeCouponEnabled = true;

          switch (Misc.ApplyCustodyConfigurationCalculateRatio(IsChipsOperation))
          {
            case Misc.TaxCustodyMode.TaxOnSplit1: // CodereMode
              if (IsChipsOperation)
              {
                RechargeRatio = 100 / (_splits.company_a.chips_sale_pct - (_splits.company_a.chips_sale_pct * m_tax_custody_pct / 100));
                PrizeCouponRatio = _splits.company_b.chips_sale_pct / (_splits.company_a.chips_sale_pct - (_splits.company_a.chips_sale_pct * m_tax_custody_pct / 100));
              }
              else
              {
                RechargeRatio = 100 / (_splits.company_a.cashin_pct - (_splits.company_a.cashin_pct * m_tax_custody_pct / 100));
                PrizeCouponRatio = _splits.company_b.cashin_pct / (_splits.company_a.cashin_pct - (_splits.company_a.cashin_pct * m_tax_custody_pct / 100));
              }
              break;

            case Misc.TaxCustodyMode.TaxOnDeposit:
              if (IsChipsOperation)
              {
                RechargeRatio = 100.00m / (_splits.company_a.chips_sale_pct / (1 + m_tax_custody_pct / 100));
                PrizeCouponRatio = _splits.company_b.chips_sale_pct / (_splits.company_a.chips_sale_pct / (1 + m_tax_custody_pct / 100));
              }
              else
              {
                RechargeRatio = 100.00m / (_splits.company_a.cashin_pct / (1 + m_tax_custody_pct / 100));
                PrizeCouponRatio = _splits.company_b.cashin_pct / (_splits.company_a.cashin_pct / (1 + m_tax_custody_pct / 100));
              }
              break;

            default:
            case Misc.TaxCustodyMode.None:
              if (IsChipsOperation)
              {
                RechargeRatio = 100.00m / _splits.company_a.chips_sale_pct;
                PrizeCouponRatio = _splits.company_b.chips_sale_pct / _splits.company_a.chips_sale_pct;
              }
              else
              {
                RechargeRatio = 100.00m / _splits.company_a.cashin_pct;
                PrizeCouponRatio = _splits.company_b.cashin_pct / _splits.company_a.cashin_pct;
              }
              break;
          }

          if (PrizeCouponRatio > 0)
          {
            try
            {
              TaxReturnMode = (TaxReturnMode)GeneralParam.GetInt32("Cashier", "ReturnTaxesOnPrizeCoupon", (Int32)(TaxReturnMode.None));

              if (TaxReturnMode != TaxReturnMode.None)
              {
                TaxReturnTolerance = 0.01m * GeneralParam.GetInt32("Cashier", "ReturnTaxesOnPrizeCoupon.ToleranceCents", 0);
              }
            }
            catch (Exception _ex)
            {
              Log.Exception(_ex);
            }
          }
        }
      }

      DecimalRoundingEnabled = false;
      RoundToCents = GeneralParam.GetInt64("Cashier", "MinCashableCents", 1);

      if (RoundToCents <= 0)
      {
        RoundToCents = 1;
      }
      else if (RoundToCents > 1)
      {
        DecimalRoundingEnabled = GeneralParam.GetBoolean("Cashier", "DecimalRoundingEnabled");
      }

      ServiceChargeMode = ServiceChargeMode.None;
      m_service_charge_on_winner_prize = 0;
      m_service_charge_on_winner_refund = 0;
      m_service_charge_on_loser_refund = 0;
      // AJQ 18-JUN-2014, Service Charge Apply Ratio on Recharge
      m_service_charge_recharge_ratio = 0;

      if (IsServiceChargeEnabled(IsChipsOperation))
      {
        GetServiceCharge(IsChipsOperation);
      }
    }

    public Boolean IsServiceChargeEnabled(Boolean IsChipsOperation)
    {
      String _group_key;

      _group_key = "Cashier.ServiceCharge";

      if (IsChipsOperation)
      {
        _group_key = "GamingTables.ServiceCharge";
      }

      return GeneralParam.GetBoolean(_group_key, "Enabled");
    }

    public void GetServiceCharge(Boolean IsChipsOperation)
    {
      String _group_key;

      _group_key = "Cashier.ServiceCharge";

      if (IsChipsOperation)
      {
        _group_key = "GamingTables.ServiceCharge";
      }

      try
      {
        ServiceChargeMode = (ServiceChargeMode)GeneralParam.GetInt32(_group_key, "Mode", (int)ServiceChargeMode.None);

        if (ServiceChargeMode != ServiceChargeMode.None)
        {
          m_service_charge_on_loser_refund = Math.Max(0, Math.Round(0.01m * GeneralParam.GetDecimal(_group_key, "OnLoser.RefundPct"), 4, MidpointRounding.AwayFromZero));
          m_service_charge_on_winner_refund = Math.Max(0, Math.Round(0.01m * GeneralParam.GetDecimal(_group_key, "OnWinner.RefundPct"), 4, MidpointRounding.AwayFromZero));
          m_service_charge_on_winner_prize = Math.Max(0, Math.Round(0.01m * GeneralParam.GetDecimal(_group_key, "OnWinner.PrizePct"), 4, MidpointRounding.AwayFromZero));
          // AJQ 18-JUN-2014, Service Charge Apply Ratio on Recharge
          m_service_charge_recharge_ratio = Math.Max(0, Math.Round(0.01m * GeneralParam.GetDecimal(_group_key, "OnRecharge.BasePct", 100m), 4, MidpointRounding.AwayFromZero));

          RechargeRatio = InflateDueToCashInTax(RechargeRatio);
        }
      }
      catch
      {
      }
    }

    public DevolutionPrizeParts(Boolean IsChipsOperation, OperationCode OpCode)
      : this(IsChipsOperation)
    {
      if (OpCode == OperationCode.PROMOTION_WITH_TAXES
          || OpCode == OperationCode.PRIZE_PAYOUT
          || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
      {
        m_promotion_with_taxes = true;
        m_taxes = TaxParameters.ReadReedemablePromotionTaxes();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Compute the Balance parts.
    //          The balace is splitted as:
    //          - Non Redeemable
    //          - Redeemable
    //
    //  PARAMS:
    //      - INPUT:
    //          - Balance
    //          - InitialCashIn
    //          - NonRedeemable1
    //          - NonRedeemable2
    //          - WonLock
    //          - SqlTrx            -- NOT USED
    //
    //      - OUTPUT:
    //          - BalancePartsOutput
    //
    // RETURNS:

    public void Compute(Currency TotalRedeemable, Currency MaxDevolution, Boolean IsApplyTaxes, Boolean IsParamEnabled)
    {
      this.Compute(TotalRedeemable, MaxDevolution, IsApplyTaxes, IsParamEnabled, false);
    }

    public void Compute(Currency TotalRedeemable, Currency MaxDevolution, Boolean IsApplyTaxes, Boolean IsParamEnabled, Boolean IsChipsPurchaseWithCashOut)
    {
      Currency _tmp_balance;
      Currency _ini_balance;
      Decimal _total_payable;
      Int64 _cents;
      Int64 _mod;
      Int64 _idx;
      Int64 _round;
      Int64 _max_tries;
      Boolean _prize_computation_without_devolution;

      _prize_computation_without_devolution = Misc.PrizeComputationWithoutDevolution();

      _ini_balance = TotalRedeemable;

      _max_tries = Math.Max(100, 100 + (Int64)(TotalRedeemable * 100));
      _round = Math.Max(1, this.RoundToCents);

      for (_idx = 0; _idx < _max_tries; _idx++)
      {
        // Redeemable
        //   Devolution
        _tmp_balance = Math.Max(0, _ini_balance);

        if (Tax.EnableTitoTaxWaiver(true) && IsParamEnabled)
        {
          this.Devolution = Math.Min(_tmp_balance, Math.Max(0, MaxDevolution));
        }
        else
        {
          if (_prize_computation_without_devolution || m_promotion_with_taxes)
          {
            this.Devolution = 0;
          }
          else
          {
            this.Devolution = Math.Min(_tmp_balance, Math.Max(0, MaxDevolution));
          }
        }

        _tmp_balance -= this.Devolution;

        //EOR 25-APR-2016
        this.DevolutionCustody = this.CalculateDevolutionCustody(_ini_balance, MaxDevolution, IsChipsPurchaseWithCashOut);

        // Prize
        this.Prize = _tmp_balance;
        // Taxes (0 must be set when prize is zero)

        foreach (TaxParameter _tax in this.Taxes)
        {
          if (Tax.EnableTitoTaxWaiver(IsApplyTaxes) && IsParamEnabled)
          {
            _tax.TaxAmount = Math.Round(this.Prize * _tax.TaxPct, 2, MidpointRounding.AwayFromZero);
          }
          else
          {
            _tax.TaxAmount = (this.Prize > _tax.PrizeThreshold) ? Math.Round(this.Prize * _tax.TaxPct, 2, MidpointRounding.AwayFromZero) : 0;
          }
        }

        this.TaxReturn = 0;

        if (this.TaxReturnMode != TaxReturnMode.None)
        {
          Decimal _total_prize_coupon;
          Decimal _prize_coupon;
          Decimal _prize_coupon_taxes;

          _total_prize_coupon = Math.Round(this.Devolution * this.PrizeCouponRatio, 2, MidpointRounding.AwayFromZero);
          _total_prize_coupon += this.TaxReturnTolerance;

          switch (this.TaxReturnMode)
          {
            case TaxReturnMode.Total:
              _prize_coupon = Math.Min(this.Prize, _total_prize_coupon);
              _prize_coupon_taxes = 0;
              foreach (TaxParameter _tax in this.Taxes)
              {
                _prize_coupon_taxes += Math.Round(_total_prize_coupon * _tax.TaxPct, 2, MidpointRounding.AwayFromZero);
              }
              _prize_coupon_taxes = Math.Min(_prize_coupon_taxes, this.TotalTaxesAmount);

              this.TaxReturn = _prize_coupon_taxes;

              break;

            case TaxReturnMode.Partial:
              if (this.Prize <= _total_prize_coupon)
              {
                this.TaxReturn = this.TotalTaxesAmount;
              }
              else
              {
                this.TaxReturn = 0;
              }
              break;

            case TaxReturnMode.None:
            default:
              this.TaxReturn = 0;
              break;
          } // switch (this.TaxReturnMode)
        }

        this.ServiceCharge = 0;
        if (this.ServiceChargeMode != ServiceChargeMode.None)
        {
          Currency _base_prize;
          Currency _base_dev;

          switch (this.ServiceChargeMode)
          {
            case ServiceChargeMode.EnabledPrizeComputedOnRecharge:
              Currency _total_recharged;
              Currency _total_refund;
              Currency _Devolution_tmp; // LTC 04-OCT-2016

              _Devolution_tmp = 0;
              _Devolution_tmp = (this.Devolution - this.DevolutionOnlyCancellable);

              _total_recharged = Math.Round(_Devolution_tmp * this.RechargeRatio, 2, MidpointRounding.AwayFromZero);
              // AJQ 18-JUN-2014, Service Charge Apply Ratio on Recharge
              _total_recharged = Math.Round(_total_recharged * m_service_charge_recharge_ratio, 2);
              _total_refund = _Devolution_tmp + this.Prize;
              _base_prize = Math.Max(0, _total_refund - _total_recharged);
              _base_dev = Math.Max(0, _total_refund - _base_prize);
              break;

            case ServiceChargeMode.EnabledPrizeComputedOnDeposit:
              _base_prize = this.Prize;
              _base_dev = this.Devolution;
              break;

            default:
              _base_prize = 0;
              _base_dev = 0;
              break;
          }

          if (_base_prize > 0)
          {
            // Winner
            this.ServiceCharge = Math.Round(_base_prize * this.m_service_charge_on_winner_prize +
                                            _base_dev * this.m_service_charge_on_winner_refund, 2, MidpointRounding.AwayFromZero);
          }
          else
          {
            // Loser
            this.ServiceCharge = Math.Round(_base_dev * this.m_service_charge_on_loser_refund, 2, MidpointRounding.AwayFromZero);
          }

          // AJQ 28-AUG-2014, CODERE: When the returned tax amount is the total tax on the prize, they also return the Service Charge
          if (this.TaxReturn > 0 && this.TaxReturn == this.TotalTaxesAmount)
          {
            this.TaxReturn += this.ServiceCharge;
          }
        }

        this.DecimalRounding = 0;

        _total_payable = TotalPayable;
        _cents = (Int64)Math.Round(_total_payable * 100, 0, MidpointRounding.AwayFromZero);
        _mod = _cents % _round;

        if (this.DecimalRoundingEnabled)
        {
          if (_mod >= _round / 2)
          {
            _mod = (_round - _mod);
          }
          else
          {
            _mod = -_mod;
          }

          DecimalRounding = Math.Round((Decimal)_mod / 100.0m, 2, MidpointRounding.AwayFromZero);

          break;
        }
        else
        {
          if (_mod == 0)
          {
            break;
          }

          if (_ini_balance == 0)
          {
            break;
          }

          _ini_balance = _ini_balance - 0.01m;
        }
      } // for 

    } // Compute

    public static String GetTaxesHtml(Decimal Amount, Boolean ThirdCol, Boolean Pct, Boolean IsChipsOperation, Boolean IsApplyTaxes, OperationCode OperationCode)
    {
      DevolutionPrizeParts _dev = new DevolutionPrizeParts(IsChipsOperation, OperationCode);
      return _dev.TaxesHtml(Amount, ThirdCol, Pct, IsApplyTaxes);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Contruct HTML to show Taxes on Vouchers.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Amount
    //          - ThirdCol
    //          - Pct
    //
    //      - OUTPUT:
    //          - String Html_Taxes
    //
    // RETURNS:
    //
    public String TaxesHtml(Decimal Amount, Boolean ThirdCol, Boolean Pct, Boolean IsApplyTaxes)
    {
      String Html_Taxes;
      List<TaxParameter> _Taxes;

      Html_Taxes = "";
      this.Prize = Amount;
      _Taxes = new List<TaxParameter>();

      foreach (TaxParameter _tax in this.Taxes)
      {
        if (_tax.TaxPct > 0)
        {
          if (!IsApplyTaxes)
          {
            _tax.TaxAmount = 0;
          }
          else
          {
            if (Tax.EnableTitoTaxWaiver(true))
            {
              _tax.TaxAmount = Math.Round(this.Prize * _tax.TaxPct, 2, MidpointRounding.AwayFromZero);
            }
            else
            {
              _tax.TaxAmount = (this.Prize > _tax.PrizeThreshold) ? Math.Round(this.Prize * _tax.TaxPct, 2, MidpointRounding.AwayFromZero) : 0;
            }
          }

          _Taxes.Add(_tax);
        }
      }

      foreach (TaxParameter _tax in _Taxes)
      {
        if (_Taxes.Count > 1)
        {
          _tax.TaxAmount = -_tax.TaxAmount;

          Html_Taxes += "<tr>";
          if (ThirdCol)
          {
            Html_Taxes += "  <td width='5%'></td>";
          }

          Html_Taxes += "    <td align='left' width='65%'>" + _tax.TaxName + " (" + _tax.TaxPct + "):</td>";
          Html_Taxes += "    <td align='right' width='30%'>" + _tax.TaxAmount.ToString() + "</td>";
          Html_Taxes += "</tr>";
        }
        else
        {
          Html_Taxes += "<tr>";

          if (ThirdCol)
          {
            Html_Taxes += "  <td width='5%'></td>";
          }

          if (Pct)
          {
            Html_Taxes += "    <td align=\"left\"  width=\"65%\">" + Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING") + " (" + _tax.TaxPct + "):</td>";
          }
          else
          {
            Html_Taxes += "    <td align=\"left\"  width=\"65%\">" + Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING") + ":</td>";
          }

          Html_Taxes += "    <td align=\"right\" width=\"30%\">" + _tax.TaxAmount.ToString() + "</td>";
          Html_Taxes += "</tr>";
        }
      }

      return Html_Taxes;
    }

    public Currency TotalTaxesAmount
    {
      get
      {
        Currency _total;
        _total = 0;
        foreach (TaxParameter _tax in m_taxes)
        {
          _total += _tax.TaxAmount;
        }

        return _total;
      }
    }

    public Boolean PrizeThresholdDefined
    {
      get
      {
        foreach (TaxParameter _tax in m_taxes)
        {
          if (_tax.PrizeThreshold > 0)
          {
            return true;
          }
        }

        return false;
      }
    }

    public TaxParameter Tax1
    {
      get
      {
        return m_taxes[0];
      }
    }

    public TaxParameter Tax2
    {
      get
      {
        return m_taxes[1];
      }
    }

    public TaxParameter Tax3
    {
      get
      {
        return m_taxes[2];
      }
    }

    public Currency TotalPayable
    {
      get
      {
        return this.Devolution + this.Prize - this.TotalTaxesAmount + this.TaxReturn - this.ServiceCharge + this.DecimalRounding + this.DevolutionCustody;
      }
    }

    public Currency TotalPayableWithoutTaxes
    {
      get
      {
        return this.Devolution + this.Prize + this.TaxReturn - this.ServiceCharge + this.DecimalRounding;
      }
    }
  }

  public class TaxParameter
  {
    public String TaxName;
    public Percentage TaxPct = 0;
    public Currency PrizeThreshold;
    public Currency TaxAmount;

    public Boolean TaxEnabled
    {
      get { return (TaxPct != 0); }
    }
  }

  public class TaxParameters : List<TaxParameter>
  {
    private const int NUM_TAXES = 3;

    private TaxParameters()
      : base()
    {
    }

    //internal static TaxParameters Read()
    //{
    //  return Read(false);
    //}
    internal static TaxParameters Read()
    {
      return Read(WSI.Common.CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_DEFAULT);
    }
    internal static TaxParameters Read(WSI.Common.CashDeskDrawBusinessLogic.ENUM_TAX_TYPE TaxType)
    {
      TaxParameter _tax;
      TaxParameters _taxes;
      Decimal _value;

      _taxes = new TaxParameters();

      for (int _tax_id = 1; _tax_id <= NUM_TAXES; _tax_id++)
      {
        _tax = new TaxParameter();

        switch (TaxType)
        {
          case CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_CHIPS:

            _tax.TaxName = GeneralParam.GetString("Cashier", string.Format("Tax.OnPrize.{0}.Name", _tax_id));
            _value = GeneralParam.GetDecimal("GamingTables", string.Format("Tax.OnPrize.{0}.Pct", _tax_id));
            _tax.TaxPct = _value / 100;
            _tax.PrizeThreshold = GeneralParam.GetDecimal("Cashier", string.Format("Tax.OnPrize.{0}.Threshold", _tax_id));

            break;


          case CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_DRAW_CASH_PRIZE:
            _tax.TaxName = GeneralParam.GetString("Cashier", string.Format("Tax.OnPrize.{0}.Name", _tax_id));

            _value = GeneralParam.GetDecimal("Draw.CashPrize", string.Format("Tax.OnPrize.{0}.Pct", _tax_id), -1);
            if (_value == -1)
            {
              // Doesn't exist, so use the default tax value
              _value = GeneralParam.GetDecimal("Cashier", string.Format("Tax.OnPrize.{0}.Pct", _tax_id));
            }
            _tax.TaxPct = _value / 100;
            _tax.PrizeThreshold = GeneralParam.GetDecimal("Cashier", string.Format("Tax.OnPrize.{0}.Threshold", _tax_id));

            break;


          case CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_PROMO_RE:
            _tax.TaxName = GeneralParam.GetString("Cashier", string.Format("Tax.OnPrize.{0}.Name", _tax_id));
            _value = GeneralParam.GetDecimal("Cashier", string.Format("Promotions.RE.Tax.{0}.Pct", _tax_id));
            _tax.TaxPct = _value / 100;
            _tax.PrizeThreshold = GeneralParam.GetDecimal("Cashier", string.Format("Promotions.RE.Tax.{0}.Threshold", _tax_id));

            break;

          case CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_DEFAULT:
          default:

            _tax.TaxName = GeneralParam.GetString("Cashier", string.Format("Tax.OnPrize.{0}.Name", _tax_id));
            _value = GeneralParam.GetDecimal("Cashier", string.Format("Tax.OnPrize.{0}.Pct", _tax_id));
            _tax.TaxPct = _value / 100;
            _tax.PrizeThreshold = GeneralParam.GetDecimal("Cashier", string.Format("Tax.OnPrize.{0}.Threshold", _tax_id));

            break;
        }

        _taxes.Add(_tax);
      }

      return _taxes;
    }

    internal static TaxParameters Read(Boolean IsChipsOperation)
    {
      return Read(IsChipsOperation ? WSI.Common.CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_CHIPS : WSI.Common.CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_DEFAULT);
    }

    internal static TaxParameters ReadReedemablePromotionTaxes()
    {
      return Read(WSI.Common.CashDeskDrawBusinessLogic.ENUM_TAX_TYPE.TAX_PROMO_RE);
    }
  }

  public class Split_Tax_Amount
  {
    private Currency m_tax;
    private Currency m_gross_amount;

    public Split_Tax_Amount()
    {
      m_tax = 0;
      m_gross_amount = 0;
    }

    public Currency Base
    {
      get
      {
        return m_gross_amount - m_tax;
      }
    }

    public Currency Tax
    {
      get
      {
        return m_tax;
      }
      set
      {
        m_tax = value;
      }
    }

    public Currency GrossAmount
    {
      get
      {
        return m_gross_amount;
      }

      set
      {
        m_gross_amount = value;
      }
    }

  }

}
