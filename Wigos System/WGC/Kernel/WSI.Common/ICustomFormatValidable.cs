using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public interface ICustomFormatValidable
  {
    String Text { get; set; }
    Boolean ValidateFormat();
  }
}
