//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMultisite.cs
// 
//   DESCRIPTION: Class with common operations for Multisite.
// 
//        AUTHOR: Artur Nebot
// 
// CREATION DATE: 25-FEB-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-FEB-2013 ANG    First release.
// 27-FEB-2013 JML    Sincronización de datos personales
// 13-JAN-2016 JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public enum WWP_SessionStatus
  {
    Opened = 0
  , Closed = 1
  , Abandoned = 2
  , Timeout = 3
  , Disconnected = 4
  }

  public enum PlayerTracking_Mode
  {
    Multisite = 0,
    Site = 1,
    ByGroups = 2,
  }

  public class CommonMultiSite
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Force account synchronize.
    //
    // call without Transaction
    public static Boolean ForceSynchronization_Accounts(String TrackData)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (ForceSynchronization_Accounts(TrackData, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ForceSynchronization_Accounts
    
    //------------------------------------------------------------------------------
    // PURPOSE: Insert account to synchronize.
    //
    //  PARAMS:
    //      - INPUT:
    //          
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True inserted account to synchronize
    //      - False otherwise.
    //
    public static Boolean ForceSynchronization_Accounts(String TrackData, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      // Synchronize Account when the Site belongs to a MultiSite.
      if (GeneralParam.GetInt32("Site", "MultiSiteMember") == 0)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" IF NOT EXISTS (SELECT 1 FROM MS_SITE_SYNCHRONIZE_ACCOUNTS ");
        _sb.AppendLine("                WHERE SSA_TRACK_DATA = @pTrackData)");
        _sb.AppendLine("   BEGIN");
        _sb.AppendLine("     INSERT INTO   MS_SITE_SYNCHRONIZE_ACCOUNTS");
        _sb.AppendLine("                 ( SSA_TRACK_DATA");
        _sb.AppendLine("                 , SSA_TODAY)");
        _sb.AppendLine("          VALUES (  @pTrackData");
        _sb.AppendLine("                 ,  @pTodayOpening)");
        _sb.AppendLine("   END");
        _sb.AppendLine(" ELSE");
        _sb.AppendLine("   BEGIN");
        _sb.AppendLine("     UPDATE   MS_SITE_SYNCHRONIZE_ACCOUNTS");
        _sb.AppendLine("        SET   SSA_TODAY      = @pTodayOpening");
        _sb.AppendLine("      WHERE   SSA_TRACK_DATA = @pTrackData");
        _sb.AppendLine("   END");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = Misc.TodayOpening();
          _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).Value = TrackData;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ForceSynchronization_Accounts

    //------------------------------------------------------------------------------
    // PURPOSE: Wwp Connection Timeout
    //
    //  PARAMS:
    //      - INPUT:
    //          
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public static Int32 WwpConnectionTimeout()
    {
      return SecureTcpClientConnection.Timeout;
    } // WwpConnectionTimeout

    //------------------------------------------------------------------------------
    // PURPOSE: Insert an account movement.
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId : Play session id.
    //          - CardId : Account id.
    //          - TerminalId : Terminal id.
    //          - SequenceId : Sequence id.
    //          - TransactionId :
    //          - Type :
    //              PointsExpired = 40,
    //              HolderLevelChanged = 43
    //          - InitialBalance :
    //          - SubAmount :
    //          - AddAmount :
    //          - FinalBalance :
    //          - MovementId :
    //          - Trx :
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static void DB_InsertMovement(Int64 OperationID,
                                         Int64 PlaySessionId,
                                         Int64 CardId,
                                         Int32 TerminalId,
                                         Int64 SequenceId,
                                         Int64 TransactionId,
                                         MovementType Type,
                                         Decimal InitialBalance,
                                         Decimal SubAmount,
                                         Decimal AddAmount,
                                         Decimal FinalBalance,
                                         SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_inserted;
      String _terminal_name;
      Object _obj;
      Boolean _is_multisite;

      _terminal_name = "";
      _is_multisite = GeneralParam.GetBoolean("MultiSite", "IsCenter", false);

      if (TerminalId > 0 && !_is_multisite)
      {
        sql_str = " SELECT  TE_NAME " +
                  "   FROM  TERMINALS " +
                  "   WHERE TE_TERMINAL_ID = @p1 ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "TE_TERMINAL_ID").Value = TerminalId;

        try
        {
          _obj = _sql_command.ExecuteScalar();
          if (_obj != null)
          {
            _terminal_name = (String)_obj;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      if (_is_multisite)
      {
        sql_str = "  DECLARE   @pSequence12Value AS BIGINT " +
                   "   UPDATE   SEQUENCES " +
                   "      SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 " +
                   "    WHERE   SEQ_ID         = @pSeqId12 " +
                   "   SELECT   @pSequence12Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = @pSeqId12; " +
                   " INSERT INTO ACCOUNT_MOVEMENTS (AM_PLAY_SESSION_ID " +
                                   ", AM_ACCOUNT_ID " +
                                   ", AM_TERMINAL_ID " +
                                   ", AM_WCP_SEQUENCE_ID " +
                                   ", AM_WCP_TRANSACTION_ID " +
                                   ", AM_TYPE " +
                                   ", AM_INITIAL_BALANCE " +
                                   ", AM_SUB_AMOUNT " +
                                   ", AM_ADD_AMOUNT " +
                                   ", AM_FINAL_BALANCE " +
                                   ", AM_DATETIME " +
                                   ", AM_TERMINAL_NAME " +
                                   ", AM_OPERATION_ID " +
                                   ", AM_MOVEMENT_ID) " +
                             "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, GETDATE(), @p11, @OperationID, @pSequence12Value) ";
      }
      else
      {
        sql_str = "INSERT INTO ACCOUNT_MOVEMENTS (AM_PLAY_SESSION_ID " +
                                   ", AM_ACCOUNT_ID " +
                                   ", AM_TERMINAL_ID " +
                                   ", AM_WCP_SEQUENCE_ID " +
                                   ", AM_WCP_TRANSACTION_ID " +
                                   ", AM_TYPE " +
                                   ", AM_INITIAL_BALANCE " +
                                   ", AM_SUB_AMOUNT " +
                                   ", AM_ADD_AMOUNT " +
                                   ", AM_FINAL_BALANCE " +
                                   ", AM_DATETIME " +
                                   ", AM_TERMINAL_NAME " +
                                   ", AM_OPERATION_ID) " +
                             "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, GETDATE(), @p11, @OperationID) ";
      }

      _sql_command = new SqlCommand(sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      if (PlaySessionId == 0)
      {
        _sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "AM_PLAY_SESSION_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "AM_PLAY_SESSION_ID").Value = PlaySessionId;
      }

      if (CardId == 0)
      {
        _sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "AM_ACCOUNT_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "AM_ACCOUNT_ID").Value = CardId;
      }

      if (TerminalId == 0)
      {
        _sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "AM_TERMINAL_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "AM_TERMINAL_ID").Value = TerminalId;
      }

      if (SequenceId == 0)
      {
        _sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "AM_WCP_SEQUENCE_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "AM_WCP_SEQUENCE_ID").Value = SequenceId;
      }

      if (TransactionId == 0)
      {
        _sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "AM_WCP_TRANSACTION_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "AM_WCP_TRANSACTION_ID").Value = TransactionId;
      }

      _sql_command.Parameters.Add("@p6", SqlDbType.Int, 4, "AM_TYPE").Value = (Int32)Type;
      _sql_command.Parameters.Add("@p7", SqlDbType.Money, 8, "AM_INITIAL_BALANCE").Value = InitialBalance;
      _sql_command.Parameters.Add("@p8", SqlDbType.Money, 8, "AM_SUB_AMOUNT").Value = SubAmount;
      _sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "AM_ADD_AMOUNT").Value = AddAmount;
      _sql_command.Parameters.Add("@p10", SqlDbType.Money, 8, "AM_FINAL_BALANCE").Value = FinalBalance;

      if (_terminal_name == "")
      {
        _sql_command.Parameters.Add("@p11", SqlDbType.NVarChar, 50, "AM_TERMINAL_NAME").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p11", SqlDbType.NVarChar, 50, "AM_TERMINAL_NAME").Value = _terminal_name;
      }

      if (OperationID == 0)
      {
        _sql_command.Parameters.Add("@OperationID", SqlDbType.BigInt, 8, "AM_OPERATION_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@OperationID", SqlDbType.BigInt, 8, "AM_OPERATION_ID").Value = OperationID;
      }

      if (_is_multisite)
      {
        _sql_command.Parameters.Add("@pSeqId12", SqlDbType.Int, 4, "AM_MOVEMENT_ID").Value = (Int32)WSI.Common.SequenceId.AccountMovements;
      }
      else
      {
        p = _sql_command.Parameters.Add("@p12", SqlDbType.BigInt, 8, "AM_MOVEMENT_ID");
        p.Direction = ParameterDirection.Output;
      }


      num_rows_inserted = _sql_command.ExecuteNonQuery();

      if (num_rows_inserted < 1)
      {
        Log.Warning("Exception throw: inserting movements.Mov. PlaySessionId: " + PlaySessionId.ToString() + " TerminalId: " + TerminalId.ToString());
        //throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MOVEMENT Not Inserted.");
      }

    } // DB_InsertMovement

    //------------------------------------------------------------------------------
    // PURPOSE : Where the loyalty program is configured?
    //           Value: 0 is configured/procesed in the MS
    //                  1 is configured/procesed in the Site
    //                  2 is mode by groups (future: configured in MS, procesed in site) 
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static PlayerTracking_Mode GetPlayerTrackingMode()
    {
      PlayerTracking_Mode _pt_mode;
      Int32 _pt_mode_default;

      _pt_mode_default = (Int32)PlayerTracking_Mode.Site;

      _pt_mode = (PlayerTracking_Mode)GeneralParam.GetInt32("MultiSite", "PlayerTracking.Mode", _pt_mode_default);

      return _pt_mode;
    } // GetPlayerTrackingMode

  }
}


