//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GamingTables.cs
//  
//   DESCRIPTION: 
// 
//        AUTHOR: Dani Dom�nguez
// 
// CREATION DATE: 15-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-MAY-2014 DDM    First release.
// 11-JUL-2016 JML    Product Backlog Item 15065: Gaming tables (Fase 4): Player tracking. Multicurrency
// 20-APR-2017 RAB    PBI 26809: Drop gaming table information - Actual drop
// 15-JUN-2017 JML    PBI 27998: WIGOS-2695 - Rounding - Apply rounding according to the minimum chip
// 06-JUL-2017 JML    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Xml;

namespace WSI.Common
{

  public class LinkedGamingTable
  {
    public Int32 TypeId;
    public String TypeName;
    public Int32 TableId;
    public String TableName;
    public Int64 CashierSession;
    public Int64 GamingTableSession;

    public static LinkedGamingTable UnknownSession()
    {
      LinkedGamingTable _unknown_session;

      _unknown_session = new LinkedGamingTable();

      _unknown_session.TableId = -1;
      _unknown_session.TypeId = -1;
      _unknown_session.GamingTableSession = -1;
      _unknown_session.CashierSession = -1;

      return _unknown_session;
    }
  }

  public class GamingTable
  {
    #region Members
    private Int32 m_gaming_table_id;
    private String m_gaming_table_name;
    private Int32 m_gaming_table_type;
    private Int32 m_user_id;
    private Currency m_bet_min;
    private Currency m_bet_max;
    private Dictionary<Int64, Seat> m_seats;
    private Int32 m_current_speed;
    private GTPlayerTrackingSpeed m_selected_speed;
    private Dictionary<GTPlayerTrackingSpeed, Int32> m_table_speeds;
    private GamingTablesSessions m_gaming_table_session;
    private Boolean m_gaming_table_paused;
    private Int32? m_cashier_id;
    private Int64 m_sequence;
    private Int32 m_table_idle_plays; // plays number for visual notification!
    private DataTable m_cashier_session_movements;    //Datatable with all cashier movements associated to that gaming table session
    private DataTable m_cashier_session_summary;    //Datatable with cashier sumary calculated which all movements associated to that gaming table session
    private Currency m_cashier_session_drop;          //Drop  Cashier associated to that gaming table session
    private DateTime m_cashier_session_opening;       //Opening  Cashier associated to that gaming table session
    private Boolean m_is_integrated;
    private Decimal m_theoric_hold;
    private TableLimitsDictionary m_table_acepted_iso_codes;
    private Currency m_validated_dealer_copy_amount;

    #endregion

    #region Properties

    public Int32 GamingTableId
    {
      get { return m_gaming_table_id; }
      set { m_gaming_table_id = value; }
    }
    public String GamingTableName
    {
      get { return m_gaming_table_name; }
      set { m_gaming_table_name = value; }
    }
    public Int32 GamingTableType
    {
      get { return m_gaming_table_type; }
      set { m_gaming_table_type = value; }
    }
    public Int32 UserId
    {
      get { return m_user_id; }
      set { m_user_id = value; }
    }
    public Int32 CurrentSpeed
    {
      get
      {
        if (m_selected_speed == GTPlayerTrackingSpeed.Unknown)
        {
          return 0;
        }
        else
        {
          return m_table_speeds[m_selected_speed];
        }
      }
      set { m_current_speed = value; }
    }
    public GTPlayerTrackingSpeed SelectedSpeed
    {
      get { return m_selected_speed; }
      set { m_selected_speed = value; }
    }
    public Dictionary<GTPlayerTrackingSpeed, Int32> TableSpeeds
    {
      get { return m_table_speeds; }
      set { m_table_speeds = value; }
    }
    public Currency BetMin
    {
      get { return m_bet_min; }
      set { m_bet_min = value; }
    }
    public Currency BetMax
    {
      get { return m_bet_max; }
      set { m_bet_max = value; }
    }

    public Dictionary<Int64, Seat> Seats
    {
      get { return m_seats; }
      set { m_seats = value; }
    }

    public GamingTablesSessions GamingTableSession
    {
      get { return m_gaming_table_session; }
      set { m_gaming_table_session = value; }
    }

    public Boolean GamingTablePaused
    {
      get { return m_gaming_table_paused; }
      set { m_gaming_table_paused = value; }
    }

    public Int32? CashierId
    {
      get { return m_cashier_id; }
      set { m_cashier_id = value; }
    }

    public Int64 Sequence
    {
      get { return m_sequence; }
      set { m_sequence = value; }
    }

    public Int32 TableIdlePlays
    {
      get { return m_table_idle_plays; }
      set { m_table_idle_plays = value; }
    }

    public Currency CashierSessionDrop
    {
      get { return m_cashier_session_drop; }
      set { m_cashier_session_drop = value; }
    }

    public DataTable CashierSessionMovements
    {
      get { return m_cashier_session_movements; }
      set { m_cashier_session_movements = value; }
    }

    public DataTable CashierSessionSummary
    {
      get { return m_cashier_session_summary; }
      set { m_cashier_session_summary = value; }
    }

    public DateTime CashierSessionOpening
    {
      get { return m_cashier_session_opening; }
      set { m_cashier_session_opening = value; }
    }

    public Boolean IsIntegrated
    {
      get { return m_is_integrated; }
      set { m_is_integrated = value; }
    }

    public Decimal TheoricHold
    {
      get { return m_theoric_hold; }
      set { m_theoric_hold = value; }
    }

    public TableLimitsDictionary TableAceptedIsoCode
    {
      get { return m_table_acepted_iso_codes; }
      set { m_table_acepted_iso_codes = value; }
    }
    public Currency ValidatedDealerCopyAmount
    {
      get { return m_validated_dealer_copy_amount; }
      set { m_validated_dealer_copy_amount = value; }
    }

    #endregion

    #region Functions

    #endregion
  }

  public class Seat
  {
    #region Members

    private Int64 m_seat_id;
    private Int32 m_gaming_table_id;
    private Int64 m_gaming_table_session_id;
    private Int64 m_account_id;
    // TODO JML && DDM Account Info (Display info)
    private Int32 m_holder_level;
    private Int32 m_holder_gender;
    private String m_holder_name;
    private String m_first_name;
    private Points m_points;
    private String m_track_data;
    private GTPlayerType m_player_type = GTPlayerType.Unknown;

    private Int32 m_virtual_terminal_id;
    private String m_virtual_terminal_name;
    private Boolean m_enabled;
    private DateTime? m_enabled_date;
    private DateTime? m_disabled_date;
    private TablePlaySession m_play_session;
    private Boolean m_has_pending_changes;
    private Int32? m_seat_position;
    private Decimal m_theoric_hold;


    #endregion

    #region Properties

    public Int64 SeatId
    {
      get { return m_seat_id; }
      set { m_seat_id = value; }
    }
    public Int32 GamingTableId
    {
      get { return m_gaming_table_id; }
      set { m_gaming_table_id = value; }
    }
    public Int64 GamingTableSessionId
    {
      get { return m_gaming_table_session_id; }
      set { m_gaming_table_session_id = value; }
    }
    public Int64 AccountId
    {
      get { return m_account_id; }
      set { m_account_id = value; }
    }
    public Int32 HolderLevel
    {
      get { return m_holder_level; }
      set { m_holder_level = value; }
    }
    public Int32 HolderGender
    {
      get { return m_holder_gender; }
      set { m_holder_gender = value; }
    }
    public String HolderName
    {
      get { return m_holder_name; }
      set { m_holder_name = value; }
    }
    public String FirstName
    {
      get { return m_first_name; }
      set { m_first_name = value; }
    }
    public Points Points
    {
      get { return m_points; }
      set { m_points = value; }
    }
    public String TrackData
    {
      get { return m_track_data; }
      set { m_track_data = value; }
    }

    public GTPlayerType PlayerType
    {
      get { return m_player_type; }
      set { m_player_type = value; }
    }
    public Int32 VirtualTerminalId
    {
      get { return m_virtual_terminal_id; }
      set { m_virtual_terminal_id = value; }
    }
    public String VirtualTerminalName
    {
      get { return m_virtual_terminal_name; }
      set { m_virtual_terminal_name = value; }
    }
    public Boolean Enabled
    {
      get { return m_enabled; }
      set { m_enabled = value; }
    }
    public DateTime? EnabledDate
    {
      get { return m_enabled_date; }
      set { m_enabled_date = value; }
    }
    public DateTime? DisabledDate
    {
      get { return m_disabled_date; }
      set { m_disabled_date = value; }
    }
    public TablePlaySession PlaySession
    {
      get { return m_play_session; }
      set { m_play_session = value; }
    }
    public Boolean HasPendingChanges
    {
      get { return m_has_pending_changes; }
      set { m_has_pending_changes = value; }
    }

    public Int32? SeatPosition
    {
      get { return m_seat_position; }
      set { m_seat_position = value; }
    }
    public Decimal TheoricHold
    {
      get { return m_theoric_hold; }
      set { m_theoric_hold = value; }
    }

    #endregion

    #region Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Seat
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :  
    //      - Boolean
    //------------------------------------------------------------------------------
    public Boolean Init()
    {
      try
      {
        this.PlayerType = GTPlayerType.Unknown;
        this.AccountId = 0; // _gaming_table.Seats[this.SeatId].AccountId;
        this.HolderGender = 0; // _gaming_table.Seats[this.SeatId].HolderGender;
        this.HolderLevel = 0; // _gaming_table.Seats[this.SeatId].HolderLevel;
        this.HolderName = ""; // _gaming_table.Seats[this.SeatId].HolderName;
        this.FirstName = "";
        this.Points = 0; // _gaming_table.Seats[this.SeatId].Points;
        this.TrackData = ""; // _gaming_table.Seats[this.SeatId].TrackData;
        this.PlaySession = new TablePlaySession();
        this.TheoricHold = 0;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    #endregion
  }

  public class TablePlaySession
  {
    #region ENUMS

    public enum GT_REGISTER_MODE
    {
      UNKOWN = 0,
      BY_TOTAL = 1,
      BY_AVERAGE = 2,
    }

    #endregion

    #region Members

    private Int64 m_play_session_id;
    private Int64 m_account_id;
    private Int32 m_gaming_table_id;
    private Int64 m_seat_id;
    private Int32 m_terminal_id;
    private DateTime m_started;
    private Int32 m_plays_count;
    private Currency m_played_amount;
    private Currency m_played_min;
    private Currency m_played_max;
    private Currency m_played_avg;
    private Currency m_net_win;
    private GTPlaySessionPlayerSkill m_player_skill;
    private GTPlayerTrackingSpeed m_player_speed;
    private Points m_awarded_points;
    private Points m_awarded_points_calculated;
    private Int64 m_walk;
    private DateTime? m_started_walk;
    private Int64 m_sequence;
    private Int32 m_current_table_speed;

    private GT_REGISTER_MODE m_register_mode;
    private Currency m_current_bet;
    private Currency m_total_sell_chips;
    private Currency m_chips_out;
    private Currency m_chips_in;
    private DateTime? m_updated;
    private DateTime? m_updated_played_current;
    private Currency m_last_played_amount;
    private Int32 m_last_plays_count;
    private String m_player_iso_code;

    #endregion

    #region Properties

    public Int64 PlaySessionId
    {
      get { return m_play_session_id; }
      set { m_play_session_id = value; }
    }
    public Int64 AccountId
    {
      get { return m_account_id; }
      set { m_account_id = value; }
    }
    public Int32 GamingTableId
    {
      get { return m_gaming_table_id; }
      set { m_gaming_table_id = value; }
    }
    public Int64 SeatId
    {
      get { return m_seat_id; }
      set { m_seat_id = value; }
    }
    public Int32 TerminalId
    {
      get { return m_terminal_id; }
      set { m_terminal_id = value; }
    }
    public DateTime Started
    {
      get { return m_started; }
      set { m_started = value; }
    }
    public Int32 PlaysCount
    {
      get
      {
        //if (CurrentTableSpeed > 0)
        //{
        //  return Math.Ceiling((Decimal)GameTime / (Decimal)CurrentTableSpeed);
        //}
        //else
        //{
        //  return 0;
        //}
        return m_plays_count;
      }
      set { m_plays_count = value; }
    }
    public Currency PlayedAmount
    {
      get { return m_played_amount; }
      set { m_played_amount = value; }
    }
    public Currency PlayedMin
    {
      get { return m_played_min; }
      set { m_played_min = value; }
    }
    public Currency PlayedMax
    {
      get { return m_played_max; }
      set { m_played_max = value; }
    }
    public Currency PlayedAvg
    {
      get { return m_played_avg; }
      set { m_played_avg = value; }
    }
    public Currency NetWin
    {
      get { return m_net_win; }
      set { m_net_win = value; }
    }
    public GTPlaySessionPlayerSkill PlayerSkill
    {
      get { return m_player_skill; }
      set { m_player_skill = value; }
    }
    public GTPlayerTrackingSpeed PlayerSpeed
    {
      get { return m_player_speed; }
      set { m_player_speed = value; }
    }
    public Points AwardedPoints
    {
      get { return m_awarded_points; }
      set { m_awarded_points = value; }
    }
    public Points AwardedPointsCalculated
    {
      get { return m_awarded_points_calculated; }
      set { m_awarded_points_calculated = value; }
    }
    public Int64 Walk
    {
      get
      {
        TimeSpan _time_interval;

        _time_interval = new TimeSpan(0, 0, (Int32)m_walk);

        if (m_started_walk != null)
        {
          _time_interval = _time_interval + (WGDB.Now - (DateTime)m_started_walk);
        }
        return (Int64)_time_interval.TotalSeconds;
      }
      set { m_walk = value; }
    }
    public Int64 ActualWalk
    {
      get
      {
        TimeSpan _time_interval;

        _time_interval = new TimeSpan(0, 0, 0);

        if (m_started_walk != null)
        {
          _time_interval = (WGDB.Now - (DateTime)m_started_walk);
        }
        return (Int64)_time_interval.TotalSeconds;
      }
    }
    public DateTime? Started_walk
    {
      get
      {
        return m_started_walk;
      }
      set
      {
        // TODO: Esto se debe hacer al guardar en base de datos cuando el cliente vuelve. m_walk = m_walk + ime_interval_sec
        //////if (value == null && m_started_walk != null)
        //////{
        //////  TimeSpan _time_interval;

        //////  if (! m_started_walk.HasValue)
        //////  {

        //////  }
        //////  else
        //////  {
        //////    _time_interval = WGDB.Now - m_started_walk;
        //////  }

        //////  m_walk = m_walk + Convert.ToInt32(_time_interval.TotalSeconds);
        //////}
        m_started_walk = value;
      }
    }
    public Double GameTime
    {
      get
      {
        TimeSpan _time_interval;
        Int32 _time_walk;

        _time_walk = (Int32)m_walk;

        if (m_started_walk != null)
        {
          _time_interval = m_started_walk.Value - m_started - new TimeSpan(0, 0, (Int32)_time_walk);
        }
        else
        {
          _time_interval = WGDB.Now - m_started - new TimeSpan(0, 0, (Int32)_time_walk);
        }

        return _time_interval.TotalSeconds;
      }
    }
    public Int64 Sequence
    {
      get
      {
        return m_sequence;
      }
      set
      {
        m_sequence = value;
      }
    }
    public Int32 CurrentTableSpeed
    {
      get
      {
        return m_current_table_speed;
      }
      set
      {
        m_current_table_speed = value;
      }
    }
    public GT_REGISTER_MODE RegisterMode
    {
      get { return m_register_mode; }
      set { m_register_mode = value; }
    }
    public Currency CurrentBet
    {
      get { return m_current_bet; }
      set { m_current_bet = value; }
    }
    public Currency TotalSellChips
    {
      get { return m_total_sell_chips; }
      set { m_total_sell_chips = value; }
    }
    public Currency ChipsOut
    {
      get { return m_chips_out; }
      set { m_chips_out = value; }
    }
    public Currency ChipsIn
    {
      get { return m_chips_in; }
      set { m_chips_in = value; }
    }
    public DateTime? Updated
    {
      get { return m_updated; }
      set { m_updated = value; }
    }
    public DateTime? UpdatedPlayedCurrent
    {
      get { return m_updated_played_current; }
      set { m_updated_played_current = value; }
    }
    public Currency LastPlayedAmount
    {
      get { return m_last_played_amount; }
      set { m_last_played_amount = value; }
    }
    public Int32 LastPlaysCount
    {
      get { return m_last_plays_count; }
      set { m_last_plays_count = value; }
    }
    public String PlayerIsoCode
    {
      get { return m_player_iso_code; }
      set { m_player_iso_code = value; }
    }

    #endregion

    #region Functions
    #endregion
  }

  public class TableLimitsDictionary : Dictionary<String, TableLimitsDictionary.TableLimits>
  {
    #region Constructors
    public TableLimitsDictionary(Int64 GamingTableId, SqlTransaction SqlTrx)
      : base()
    {
      DataTable _iso_code_limits;

      _iso_code_limits = new DataTable();

      _iso_code_limits = FeatureChips.ChipsSets.GetCurrenciesAccepted(GamingTableId, SqlTrx);

      foreach (DataRow _limit in _iso_code_limits.Rows)
      {
        this.Add((String)_limit["ISO_CODE"], new TableLimits((Int64)_limit["ORDEN"], (Decimal)_limit["MIN_BET"], (Decimal)_limit["MAX_BET"]));
      }

    }

    #endregion Constructors

    public Dictionary<String, TableLimitsDictionary.TableLimits> Create(Int64 GamingTableId)
    {
      Dictionary<String, TableLimitsDictionary.TableLimits> _dic_limit;
      DataTable _iso_code_limits;

      _dic_limit = new Dictionary<string, TableLimits>();
      _iso_code_limits = new DataTable();

      foreach (DataRow _limit in _iso_code_limits.Rows)
      {
        _dic_limit.Add((String)_limit["ISO_CODE"], new TableLimits((Int64)_limit["ORDEN"], (Currency)_limit["MIN_BET"], (Currency)_limit["MAX_BET"]));
      }

      return _dic_limit;
    }

    public class TableLimits
    {
      private Int64 m_order;
      private Currency m_min_bet;
      private Currency m_max_bet;

      public Int64 Order
      {
        get { return m_order; }
        set { m_order = value; }
      }

      public Currency MinBet
      {
        get { return m_min_bet; }
        set { m_min_bet = value; }
      }

      public Currency MaxBet
      {
        get { return m_max_bet; }
        set { m_max_bet = value; }
      }

      public TableLimits(Int64 Order, Currency MinBet, Currency MaxBet)
      {
        this.m_order = Order;
        this.m_min_bet = MinBet;
        this.m_max_bet = MaxBet;
      }

    }
  }

  public class RoundingSaleChips
  {
    public enum GT_CALCULATE_ROUNDED_AMOUNT_RESULT
    {
      UNDEFINED = 0,
      OK = 1,
      AMOUNT_LESS_THAN_MINIMUM_DENOMINATION = 2,
      ROUND_NOT_APPLIED = 3,
    }

    private Currency m_input_amount_minus_taxes;
    private Currency m_rounded_amount;
    private Currency m_remaining_amount;
    private Currency m_minimium_denomination;
    private GT_CALCULATE_ROUNDED_AMOUNT_RESULT m_calculate_result;

    public Currency InputAmountMinusTaxes
    {
      get { return m_input_amount_minus_taxes; }
      set { m_input_amount_minus_taxes = value; }
    }
    public Currency RoundedAmount
    {
      get { return m_rounded_amount; }
      set { m_rounded_amount = value; }
    }
    public Currency RemainingAmount
    {
      get { return m_remaining_amount; }
      set { m_remaining_amount = value; }
    }
    public Currency MinimiumDenomination
    {
      get { return m_minimium_denomination; }
      set { m_minimium_denomination = value; }
    }
    public GT_CALCULATE_ROUNDED_AMOUNT_RESULT CalculateResult
    {
      get { return m_calculate_result; }
      set { m_calculate_result = value; }
    }

    public RoundingSaleChips(Decimal AmountMinusTaxes, Boolean CouldCalculateRounded)
    {
      this.InputAmountMinusTaxes = AmountMinusTaxes;
      this.RoundedAmount = AmountMinusTaxes;
      this.RemainingAmount = 0;
      this.MinimiumDenomination = 0;
      this.CalculateResult = GT_CALCULATE_ROUNDED_AMOUNT_RESULT.UNDEFINED;

      if (CouldCalculateRounded)
      {
        GetRoundingSaleChipsValue();
      }
      else
      {
        this.CalculateResult = GT_CALCULATE_ROUNDED_AMOUNT_RESULT.ROUND_NOT_APPLIED;
      }
    }

    /// <summary>
    /// Get value of "Minimum chip value" for rounding in the sale of chips with recharge
    /// </summary>
    /// <returns>GP value</returns>
    private RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT GetRoundingSaleChipsValue()
    {
      try
      {
        if (GamingTableBusinessLogic.IsEnableRoundingSaleChips())
        {
          if (this.MinimiumDenomination <= 0)
          {
            this.MinimiumDenomination = GamingTableBusinessLogic.GetDefaultMinimumDenominationForRoundingSaleChips();
          }

          if (this.MinimiumDenomination > this.InputAmountMinusTaxes)
          {
            this.CalculateResult = RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.AMOUNT_LESS_THAN_MINIMUM_DENOMINATION;
            this.RoundedAmount = 0;
            this.RemainingAmount = this.InputAmountMinusTaxes;
            return this.CalculateResult;
          }

          if (this.MinimiumDenomination != 0)
          {
            this.RemainingAmount = this.InputAmountMinusTaxes % this.MinimiumDenomination;
          }
          else
          {
            this.RemainingAmount = 0;
          }
          this.RoundedAmount = this.InputAmountMinusTaxes - this.RemainingAmount;
          this.CalculateResult = RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.OK;
          return this.CalculateResult;
        }
        else
        {
          this.MinimiumDenomination = 0.01M;
          this.RemainingAmount = 0;
          this.RoundedAmount = this.InputAmountMinusTaxes;
          this.CalculateResult = RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.OK;
          return this.CalculateResult;
        }
      }
      catch
      {
        return this.CalculateResult;
      }
    }
  }


}


