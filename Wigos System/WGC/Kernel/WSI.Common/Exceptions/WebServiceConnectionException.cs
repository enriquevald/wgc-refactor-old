﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Customer.cs
// 
//   DESCRIPTION : Class to manage customers
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-JUN-2016  PDM    Creation
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.Exceptions
{
  public class WebServiceConnectionException : Exception
  {
    public WebServiceConnectionException()
    {
    }

    public WebServiceConnectionException(string message)
        : base(message)
    {
    }

    public WebServiceConnectionException(string message, Exception inner)
        : base(message, inner)
    {
    }
  }
}
