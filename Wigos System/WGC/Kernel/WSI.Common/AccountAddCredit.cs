//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AccountAddCredit.cs
// 
//   DESCRIPTION: Class to add credit to accounts
// 
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 23-FEB-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAY-2012 RCI    Fixed Bug #302: When adding credit, don't keep counting when adding a promotion with REAL WonLock (included "Candado 0").
// 06-JUL-2012 JAB    Added Function (CheckAddNotRedeemable). Not redeemable credit control.
// 07-AUG-2012 JCM    Add Cashier Limit Sales. Added flag LimitSaleReached, and RemainingCreditAllowCashierSale value of remain authorized sale credit 
// 03-MAY-2013 JCA    Added function ProcessRecharge and promotion "SPACE".
// 14-JUN-2013 RCI    Fixed Bug #861.
// 02-JUL-2013 DLL    Add Currency Exchange support
// 27-JUL-2013 RCI    Added Anti-Money Laundering support in DB_Recharge.
// 12-AUG-2013 RCI & LEM    ErrorCode output for recharges failed (necessary for AntiMoneyLaundering control).
// 28-AUG-2013 RCI    Added support for Recharge for points. GP Gifts.RedeemableCredits - AsCashIn
// 03-SEP-2013 RCI    Fixed Bug WIG-183: Recharge for points: invalid cashier movements
// 10-OCT-2013 LEM    If TITO is activated: Entire amount is considered as prize in DB_Recharge
// 11-NOV-2013 DHA    Fixed Bug WIG-399 Re-order 'Card Deposit In' movement to first position
// 12-NOV-2013 JML    Fixed Bug WIG-403: Recharge failed to reach the sales limit.
// 31-DEC-2013 RCI    Added CashIn tax support.
// 29-ENE-2014 LJM    Recharges payed with credit card or check, should not be accounted for AML
// 25-MAR-2014 XIT & LEM & DRV Fixed Bug WIGOSTITO-1162: Currency exchange promotions doesn't update his status when ticket is generated 
// 27-MAR-2014 JBC     Added new functionality. Check First Recharge to Anonymous Accounts
// 25-JUN-2014 RCI    Fixed Bug WIG-1037: Account movement for IEJC tax is wrong. It has not to affect account balance and for undo operation, it does.
// 01-JUL-2014 AMF    Personal Card Replacement
// 29-MAY-2014 DRV    Fixed Bug WIGOSTITO-1229: Wrong title on chips sale voucher
// 19-SEP-2014 SGB & RCI    Fixed Bug WIG-1246: Don't allow a note acceptor recharge when cage is disabled.
// 19-SEP-2014 SGB & RCI    Fixed Bug WIG-1231: Check if exceeds the allowable limit amount.
// 22-SEP-2014 LEM    Fixed Bug WIG-1247: Wrong provider statistics SPACE-TITO (ProcessRecharge)
// 02-OCT-2014 RCI    Fixed Bug WIG-1231: Check if exceeds the allowable limit amount (Only check if it's a real Mobile Bank!).
// 16-OCT-2014 DRV    Fixed Bug WIGOSTITO-1254: If cupon Cover it's enabled it's ticket is not generated.
// 18-NOV-2014 JML    Fixed Bug WIG-1707 MB: When checking the daily maximum limit per customer, recharging can not be performed when you have not exceeded the limit.
// 24-NOV-2014 MPO    LOGRAND CON02: Added IVA calculated (when needed) and input params class for cashin.
// 26-NOV-2014 OPC    Fixed Bug WIG-1757: Incorrect deposit when it's a custom card.
// 26-JAN-2015 JBC    Fixed Bug WIG-1968: AML Max Limit on BM recharges don't work.
// 28-Jan-2014 JML    Product Backlog Item 145: Reset cost promotions when recharging in the following cases: extra ball or the amount NR less than GP
// 12-FEB-2015 YNM    WIG-1887: Fixed error in Register Sell of Chips. The card's payment was working incorrectly.
// 09-MAR-2015 DRV    Fixed Bug WIG-2143: Error on cash in voucher when integrated chips operations are enabled
// 15-APR-2015 JBC & RMS   Cannot recharge when cashier session is out of gaming day
// 15-APR-2015 JBC    Fixed Bug WIG-2306: BM Error Message
// 30-JUN-2015 MPO    WIG-2492: Get and set account promo id for calculate cost (Refactor vars)
// 30-JUN-2015 DHA    Added card casino payment movement
// 05-AUG-2015 YNM    TFS-3109: Annonymous account min allowed cash in amount recharge
// 02-SEP-2015 DCS    Fixed Bug 4171: For card player payment the CASHIER_MOVEMENT it's the same for company A and B
// 23-JUL-2015 MPO    WIG-2599: Write a log when the current gaming day isn't open in DB_Recharge.
// 10-SEP-2015 YNM    Fixed BUg 4170: External PDL. Annonymous and personalized account allow reacharges 
// 25-SEP-2015 DHA    Fixed Bug TFS-4687: recharge with check wrong total
// 25-SEP-2015 DHA    Fixed Bug TFS-4688: recharge with check wrong transaction type
// 13-OCT-2015 YNM    Fixed Bug TFS-4679: Mico: Bills are not transferred by the acceptor
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 27-NOV-2015 SGB    Product Backlog Item 4713: Prerare ticket to print
// 29-FEB-2016 AVZ    Product Backlog Item 9363:Promo RE impuestos: Kernel: Promociones Redimibles: Aplicar impuestos
// 01-MAR-2016 DHA    Product Backlog Item 10083: Winpot - Provisioning Tax
// 01-MAR-2016 JML    Product Backlog Item 10084: Winpot - Provisioning Tax: Cashdesk draw
// 13-APR-2016 RGR    Product Backlog Item 11295: Codere - Custody Tax: Implement tax
// 14-ABR-2016 LTC    Product Backlog Item 11295: Codere - Custody Tax: Implement tax
// 14-APR-2016 RGR    Product Backlog Item 11295: Codere - Custody Tax: Implement tax
// 15-MAR-2016 ETP    Fixed Bug 10608: PromoBox: Error in points promotions
// 06-MAY-2016 AVZ GDA  Bug 12915: Al realizar una recarga con divisa obteniendo promo re con impuestos, el efectivo en caja queda en negativo -Task 12949:No se puede realizar una recarga de $0 de promoci�n RE con impuestos
// 13-JUN-2016 GA  SDS  Bug 13981:CardPrice
// 20-JUL-2016 RGR    Product Backlog Item 15318: Table-21: Sales of chips A and B now
// 02-AUG-2016 EOR    Product Backlog Item 16149: Codere - Tax Custody: Add tax custody concept in ticket recharge
// 01-SEP-2016 EOR    Product Backlog Item 15248: TPV Televisa: Cashier, Vouchers and tickets
// 07-SEP-2016 EOR    Product Backlog Item 15335: TPV Televisa: Cash Summary
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 22-SEP-2016 LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 28-SEP-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 03-OCT-2016 FAV    Fixed Bug 18440: Unhandled exception in refund of prizes
// 05-OCT-2016 RGR    PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
// 07-OCT-2016 LTC    PBI 17960: Televisa - Draw Cash for gaming tables - Configuration
// 10-OCT-2016 LTC    Bug 18843:Cajero - Cuentas: Reintegro de pago de premio en "Efectivo en cuenta" muestra un error y no permite la operaci�n
// 12-OCT-2016 RGR    Bug 18922: Draw box with cash prize (Televisa): the concept of the promotional coupon is not shown on the ticket cash desk
// 12-OCT-2016 EOR    Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cup�n promocional in the ticket
// 13-OCT-2016 LTC    Bug 19012: Craw on Gaming tables: won amount is not given as gaming chips
// 13-OCT-2016 RGR    Bug 19051: Draw box with cash prize (Televisa) is performed as loser draw with idle question of access
// 13-OCT-2016 EOR    Fixed Bug 19051: Draw box with cash prize (Televisa) is performed as loser draw with idle question of access
// 10-OCT-2016 LTC    Bug 18843:Cajero - Cuentas: Reintegro de pago de premio en "Efectivo en cuenta" muestra un error y no permite la operaci�n
// 12-OCT-2016 RGR    Bug 18922: Draw box with cash prize (Televisa): the concept of the promotional coupon is not shown on the ticket cash desk
// 12-OCT-2016 EOR    Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cup�n promocional in the ticket
// 03-OCT-2016 FAV    Fixed Bug 18440: Unhandled exception in refund of prizes
// 05-OCT-2016 RGR    PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
// 07-OCT-2016 LTC    PBI 17960: Televisa - Draw Cash for gaming tables - Configuration
// 18-NOV-2016 ETP    Fixed Bug 20736: Cashdeskdraw. Total label only in cashprize and Televisa mode
// 13-DIC-2016 FJC    Bug 21271:Promobox: No se muestra correctamente el terminal en "Reporte de tickets"
// 19-DEC-2016 FAV    Bug 21719:CountR: Invalid tax when Cashier.TITOTaxWaiver is enabled
// 25-JAN-2017 FJC    PBI 22241:Sorteo de m�quina - Participaci�n - US 1 - Participaci�n
// 07-FEB-2017 XGJ&ETP PBI 22249: Machine Draw
// 14-ENE-2017 DPC    Bug 24589:Pago de tarjeta: se tiene en cuenta el impuesto de la empresa B
// 24-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 29-MAR-2017 FAV    PBI 25956:Third TAX - Cash desk draw and UNR TAX Configuration
// 23-MAR-2017 XGJ&FJC&SJA Fixed Bug 26082:Sorteo de M�quina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado.
// 05-APR-2017 JML    Bug 26568:Custodia - add new general param to configure tax custody for gaming tables operations
// 20-APR-2017 JML    Fixed Bug 26807: Custody - Incorrect custody amount to be refunded
// 04-MAY-2017 DPC    WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
// 16-JUN-2017 ATB    PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 20-JUN-2017 ATB    PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 26-JUL-2017 DPC    WIGOS-3990: Creditline: two messages appear when selling chips using expired credit line
// 03-AGU-2017 DHA    Bug 29186:WIGOS-4155 The cashier reports an error message when the user try to "Sell" chips by "Recharge"
// 20-SEP-2017 JML    Bug 29854:WIGOS-5291 [Ticket #8927] Fallo: Impuesto en Reintegro [Pago de Fichas] - Custodia V03.06.0023
// 26-SEP-2017 JML    PBI 29319:MES13 Specific alarms: Purchase: Card
// 21-FEB-2018 DHA    Bug 31619:WIGOS-8109 [Ticket #12335] Custodia
// 07-FEB-2018 AGS    Fixed Bug 31619:WIGOS-8109 [Ticket #12335] Custodia
//------------------------------------------------------------------------------
using System;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using WSI.Common.ExternalPaymentAndSaleValidation;
using WSI.Common.CreditLines;
using WSI.Common.Junkets;

namespace WSI.Common
{
  public enum RECHARGE_ERROR_CODE
  {
    ERROR_GENERIC = 1,
    ERROR_AML_RECHARGE_NOT_AUTHORIZED = 2,
    ERROR_GENERATE_CASH_DESK_DRAW_EXTERNAL = 3,
    ERROR_INSERT_CASH_DESK_DRAW_EXTERNAL_DB = 4,
    ERROR_INSERT_CASH_DESK_DRAW_DB = 5,
    ERROR_CASH_DESK_DRAW_CONFIGURATION = 6,
    //ERROR_ANONYMOUS_FIRST_RECHARGE = 7,    // JML 2-DIC-2014: Deprecated
    ERROR_PRIZE_TOO_BIG = 8,
    ERROR_RECHARGE_LIMIT_EXCEEDED = 9,
    ERROR_NOTE_ACCEPTOR_WITH_CAGE_DISABLED = 10,
    ERROR_DAILY_RECHARGE_LIMIT_EXCEEDED = 11,
    ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED = 12,
    ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED = 13,
    ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED = 14,
    ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED = 15,
    ERROR_RECHARGE_OUT_GAMING_DAY = 16,
    ERROR_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT = 17,
    //LTC 10-OCT-2016
    ERROR_RECHARGE_WITH_WITHOLDING = 18,
    ERROR_RECHARGE_WITH_CASH_DESK_DRAW_ENABLED = 19,
    ERROR_RECHARGE_WITH_CASH_CLOSED = 20,
    ERROR_RECHARGE_WITH_CREDIT_LINE = 21,
  }

  public class RechargeInputParameters
  {
    public Int64 AccountId;
    public Int32 AccountLevel;
    public Boolean CardPaid;
    public Currency CardPrice;
    public String ExternalTrackData;
    public String[] VoucherAccountInfo;
    public Boolean GenerateVouchers = true;
    public Boolean NoMoney = false;
    public ParticipateInCashDeskDraw ParticipateInCashDeskDraw = ParticipateInCashDeskDraw.Default;
    public Boolean IsChipsOperation = false;
    public Boolean ChipSaleRegister = false;
    public Boolean OnlyPromotion = false;
    public TerminalTypes TerminalType;
    public WithholdData WithholdData;
    public CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskDraw; //LTC 22-SEP-2016  
    public Int32 TerminalId;
    public Int64 PlaySessionId;
    public CreditLine CreditLineData;
  }

  public class RechargeOutputParameters
  {
    public MultiPromos.AccountBalance InitialBalance = MultiPromos.AccountBalance.Zero;
    public MultiPromos.AccountBalance FinalBalance = MultiPromos.AccountBalance.Zero;
    public ArrayList VoucherList = new ArrayList();
    public Boolean SalesLimitReached = false;
    public Currency RemainingSalesLimit = 0;
    public RECHARGE_ERROR_CODE ErrorCode = RECHARGE_ERROR_CODE.ERROR_GENERIC;
    public Result CashDeskDrawResult = null;
    public Int64 AccountPromotionId = 0;
    public Int64 OperationId = 0;
    public Int64 CoverCouponAccountPromotionId = 0;
    public Currency CoverCouponAmount = 0;
    public Ticket TicketCreated = null;
    public String ErrorMessage = String.Empty;
    public CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskDraw; //LTC 13-OCT-2016  

    //Contructor
    public RechargeOutputParameters()
    {
    }

    //Contructor with parameter
    public RechargeOutputParameters(Int64 OperationId)
    {
      this.OperationId = OperationId;
    }
  }

  public partial class Accounts
  {

    #region "Constants"

    public const Int32 DB_PROMOTION_GAME_ID = 1;

    #endregion

    public static Boolean DB_AddPromotion(RechargeInputParameters InputParameters,
                                          Int64 OperationId,
                                          AccountMovementsTable AccountMovements,
                                          CashierMovementsTable CashierMovements,
                                          Int64 PromotionId,
                                          Currency PromotionReward,
                                          Currency SpentUsed,
                                          SqlTransaction Trx,
                                          out RechargeOutputParameters OutputParameters)
    {
      OperationCode opCode = EvalOperationCode(PromotionId, Trx);

      if (opCode == OperationCode.PROMOTION_WITH_TAXES)
      {
        return DB_CardCreditAdd(InputParameters, OperationId, AccountMovements, CashierMovements, 0, PromotionId, PromotionReward, 0,
                              SpentUsed, null, ParticipateInCashDeskDraw.No, opCode, Trx, null, out OutputParameters);
      }
      else
      {
        return DB_CardCreditAdd(InputParameters, OperationId, AccountMovements, CashierMovements, 0, PromotionId, PromotionReward, 0,
                              SpentUsed, null, ParticipateInCashDeskDraw.No, OperationCode.NOT_SET, Trx, null, out OutputParameters);
      }
    }

    public static Boolean DB_CardCreditAdd(RechargeInputParameters InputParameters,
                                           Int64 OperationId,
                                           AccountMovementsTable AccountMovements,
                                           CashierMovementsTable CashierMovements,
                                           Currency RedeemableAmountToAdd,
                                           Int64 PromotionId,
                                           Currency NotRedeemableAmount,
                                           Currency NotRedeemableWonLock,
                                           Currency SpentUsed,
                                           CurrencyExchangeResult ExchangeResult,
                                           ParticipateInCashDeskDraw ParticipateInCashDraw,
                                           OperationCode OpCode,
                                           SqlTransaction Trx,
                                           JunketsBusinessLogic JunketInfo,
                                           out RechargeOutputParameters OutputParameters)
    {
      DataTable _promos;
      AccountPromotion _promo;
      String _str_message;
      Currency _tito_ticket_amount;
      TITO_TICKET_TYPE _tito_ticket_type;
      Boolean _create_ticket;
      CASHIER_MOVEMENT _cashier_movement;
      Boolean _is_Tito_PromoBOX_promotion;
      ParamsTicketOperation _params_ticket_operation;
      Ticket _ticket;
      Boolean _need_print_redeemable;
      Boolean _need_print_no_redeemable;
      String _error_msg;
      Currency _total_taxes;

      OutputParameters = new RechargeOutputParameters(OperationId);

      try
      {
        _promos = AccountPromotion.CreateAccountPromotionTable();
        _promo = new AccountPromotion();

        _is_Tito_PromoBOX_promotion = WSI.Common.TITO.Utils.IsTitoMode()
                                    && InputParameters.TerminalType == TerminalTypes.PROMOBOX
                                    && PromotionId > 0
                                    && NotRedeemableAmount > 0;

        InputParameters.OnlyPromotion = _is_Tito_PromoBOX_promotion;

        if (PromotionId > 0 && NotRedeemableAmount > 0)
        {
          if (!Promotion.ReadPromotionData(Trx, PromotionId, _promo))
          {
            Log.Error("DB_CardAddCredit. Promotion not found: " + PromotionId + ".");

            return false;
          }

          if (!AccountPromotion.AddPromotionToAccount(OperationId, InputParameters.AccountId, InputParameters.AccountLevel,
                                                      RedeemableAmountToAdd, 0, true, NotRedeemableAmount, _promo, _promos))
          {
            return false;
          }

          if (SpentUsed > 0)
          {
            if (!Promotion.UpdateSpentUsedInPromotion(SpentUsed, InputParameters.AccountId, _promo, Trx, out _str_message))
            {
              Log.Error("DB_CardAddCredit. " + _str_message);

              return false;
            }
          } // if (SpentUsed > 0)
        }

        _need_print_redeemable = (ACCOUNT_PROMO_CREDIT_TYPE)_promo.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
                                    && GeneralParam.GetBoolean("WigosKiosk", "Promotions.RE.PrintTITOTicket", false);

        _need_print_no_redeemable = ((ACCOUNT_PROMO_CREDIT_TYPE)_promo.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.NR1
                                        || (ACCOUNT_PROMO_CREDIT_TYPE)_promo.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.NR2)
                                    && GeneralParam.GetBoolean("WigosKiosk", "Promotions.NR.PrintTITOTicket", false);

        if (!(_is_Tito_PromoBOX_promotion && (_need_print_redeemable || _need_print_no_redeemable)))
        {
          if (!Accounts.DB_Recharge(OperationId, InputParameters, RedeemableAmountToAdd, _promos, AccountMovements, CashierMovements, OpCode,
                                      Trx, ExchangeResult, null, _promo, JunketInfo, out OutputParameters))
          {
            return false;
          }
        }

        if (OutputParameters.SalesLimitReached)
        {
          // 12-NOV-2013 JML    Fixed Bug WIG-403: Recharge failed to reach the sales limit.
          return false;
        }

        // 27-NOV-2015 SGB SMN Calculate if create ticket for WKT
        _create_ticket = false;
        _tito_ticket_amount = 0;
        _tito_ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
        _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE;

        // If we have a promotion and it's a PromoBOX in TITO...
        if (_is_Tito_PromoBOX_promotion)
        {
          switch (_promo.credit_type)
          {
            case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
              if (!GeneralParam.GetBoolean("WigosKiosk", "Promotions.RE.PrintTITOTicket", false))
              {
                // None RE credit in tito accounts.
                Log.Error("AccountAddCredit. Promotion RE selected in PromoBOX but not configuration General Param WigosKiosk - Promotions.RE.PrintTITOTicket.");

                return false;
              }

              _create_ticket = true;
              _tito_ticket_amount = NotRedeemableAmount;
              _tito_ticket_type = TITO_TICKET_TYPE.PROMO_REDEEM;
              _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE;
              break;

            case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
            case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
              if (GeneralParam.GetBoolean("WigosKiosk", "Promotions.NR.PrintTITOTicket", false))
              {
                _create_ticket = true;
                _tito_ticket_amount = NotRedeemableAmount;
                _tito_ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
                _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE;
              }
              break;

            default:
              // Do nothing.
              break;
          }
        }

        _params_ticket_operation = new ParamsTicketOperation();

        if (_create_ticket)
        {
          CardData _card;

          _card = new CardData();

          _card.TrackData = InputParameters.ExternalTrackData;
          _card.AccountId = InputParameters.AccountId;

          _params_ticket_operation.out_promotion_id = PromotionId;
          _params_ticket_operation.out_cash_amount = _tito_ticket_amount;
          _params_ticket_operation.ticket_type = _tito_ticket_type;
          _params_ticket_operation.in_account = _card;
          _params_ticket_operation.session_info = CashierMovements.CashierSessionInfo;
          _params_ticket_operation.out_operation_id = OperationId;
          _params_ticket_operation.cashier_terminal_id = CashierMovements.CashierSessionInfo.TerminalId;
          _params_ticket_operation.terminal_types = InputParameters.TerminalType;
          _params_ticket_operation.terminal_id = InputParameters.TerminalId;
          _params_ticket_operation.in_cash_amt_0 = _tito_ticket_amount;
          _params_ticket_operation.in_cash_cur_0 = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

          if (!Accounts.DB_Recharge(OperationId, InputParameters, RedeemableAmountToAdd, _promos, AccountMovements, CashierMovements, OpCode,
                            Trx, ExchangeResult, /*_ticket.ValidationNumber.ToString()*/null, _promo, null, out OutputParameters))
          {
            return false;
          }

          _params_ticket_operation.out_account_promo_id = OutputParameters.AccountPromotionId;

          if (!CashierMovements.Add(OperationId, _cashier_movement, _tito_ticket_amount, _card.AccountId, _card.TrackData))
          {
            Log.Error("AccountAddCredit. Error Add cashier movement for TITO ticket printed from PromoBOX.");

            return false;
          }
        }

        //AVZ 29-FEB-2016
        if (OpCode == OperationCode.PROMOTION_WITH_TAXES ||
            OpCode == OperationCode.PRIZE_PAYOUT ||
            OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE
           )
        {
          if (_promo.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)
          {
            //Do redeem of the entire promotion
            ArrayList _voucher_list;
            Boolean _witholding_needed;
            Boolean _redeem_success;
            PaymentOrder _order = new PaymentOrder();
            Currency _recharge;

            //Do Recharge of the promotion with taxes
            DevolutionPrizeParts _parts = new DevolutionPrizeParts(false, OpCode);
            _parts.Compute(NotRedeemableAmount, 0, true, false);

            if (!AccountsRedeem.DB_CardCreditRedeem(InputParameters.AccountId, InputParameters.WithholdData, _order, NotRedeemableAmount, CASH_MODE.PARTIAL_REDEEM, null,
                                                    CreditRedeemSourceOperationType.DEFAULT, null, OpCode, Trx, true, AccountMovements, CashierMovements,
                                                    out _total_taxes, out _voucher_list, out _witholding_needed, out _redeem_success, out _error_msg, ref OperationId))
            {
              OutputParameters.ErrorMessage = _error_msg;

              if (_witholding_needed)
                OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_RECHARGE_WITH_WITHOLDING;

              return false;
            }

            if (OutputParameters.VoucherList == null)
              OutputParameters.VoucherList = _voucher_list;
            else
              OutputParameters.VoucherList.AddRange(_voucher_list);

            MultiPromos.AccountBalance LastBalance = OutputParameters.FinalBalance - OutputParameters.InitialBalance;
            LastBalance.PromoRedeemable -= NotRedeemableAmount;

            if (OpCode != OperationCode.PRIZE_PAYOUT)
            {
              RechargeOutputParameters _output_parameters;
              DataTable _empty_promos = AccountPromotion.CreateAccountPromotionTable();

              // AVZ GDA 06-05-2016 
              // GA  SDS 13-06-2016 Bug 13981:CardPrice
              _recharge = _parts.TotalPayable;
              if (OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
                _recharge -= InputParameters.CardPrice;

              InputParameters.CardPaid = true;

              if (!Accounts.DB_Recharge(OperationId, InputParameters, _recharge, _empty_promos, AccountMovements, CashierMovements, LastBalance, OpCode,
                                      Trx, null, null, _promo, JunketInfo, out _output_parameters))
              {
                OutputParameters = _output_parameters; //LTC 10-OCT-2016
                return false;
              }

              OutputParameters.CashDeskDrawResult = _output_parameters.CashDeskDrawResult;
              OutputParameters.FinalBalance = _output_parameters.FinalBalance;
              OutputParameters.InitialBalance = _output_parameters.InitialBalance;
              OutputParameters.RemainingSalesLimit = _output_parameters.RemainingSalesLimit;
              OutputParameters.SalesLimitReached = _output_parameters.SalesLimitReached;
              OutputParameters.VoucherList.AddRange(_output_parameters.VoucherList);
            }
          }
        }

        //Save accounts and cashier movements
        if (!AccountMovements.Save(Trx))
        {
          return false;
        }

        if (!CashierMovements.Save(Trx))
        {
          return false;
        }

        if (_create_ticket)
        {

          if (!WSI.Common.Cashier.Common_CreateTicket(_params_ticket_operation, true, out _ticket, Trx))
          {
            Log.Error("AccountAddCredit: Common_createTicket. Error creating promotional ticket.");

            return false;
          }

          OutputParameters.TicketCreated = _ticket;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_CardCreditAdd

    public static Boolean DB_Recharge(Int64 OperationId,
                                      RechargeInputParameters InputParameters,
                                      Currency RedeemableAmountToAdd,
                                      DataTable Promotions,
                                      AccountMovementsTable AccountMovements,
                                      CashierMovementsTable CashierMovements,
                                      SqlTransaction SqlTrx,
                                      out RechargeOutputParameters OutputParameters)
    {
      return DB_Recharge(OperationId, InputParameters, RedeemableAmountToAdd, Promotions, AccountMovements, CashierMovements, OperationCode.NOT_SET, SqlTrx, null, null, null, null, out OutputParameters);
    }


    public static Boolean DB_Recharge(Int64 OperationId,
                                      RechargeInputParameters InputParameters,
                                      Currency RedeemableAmountToAdd,
                                      DataTable Promotions,
                                      AccountPromotion AccountPromotion,
                                      AccountMovementsTable AccountMovements,
                                      CashierMovementsTable CashierMovements,
                                      SqlTransaction SqlTrx,
                                      out RechargeOutputParameters OutputParameters)
    {
      return DB_Recharge(OperationId, InputParameters, RedeemableAmountToAdd, Promotions, AccountMovements, CashierMovements, OperationCode.NOT_SET, SqlTrx, null, null, AccountPromotion, null, out OutputParameters);
    }

    public static Boolean DB_Recharge(Int64 OperationId,
                                      RechargeInputParameters InputParameters,
                                      Currency RedeemableAmountToAdd,
                                      DataTable Promotions,
                                      AccountMovementsTable AccountMovements,
                                      CashierMovementsTable CashierMovements,
                                      OperationCode OpCode,
                                      SqlTransaction SqlTrx,
                                      CurrencyExchangeResult ExchangeResult,
                                      String PromotionalTitoTicket,
                                      AccountPromotion AccountPromotion,
                                      JunketsBusinessLogic JunketInfo,
                                      out RechargeOutputParameters OutputParameters)
    {
      return DB_Recharge(OperationId, InputParameters, RedeemableAmountToAdd, Promotions, AccountMovements, CashierMovements, MultiPromos.AccountBalance.Zero, OpCode, SqlTrx, ExchangeResult,
                         PromotionalTitoTicket, AccountPromotion, JunketInfo, out OutputParameters);
    }

    public static Boolean DB_Recharge(Int64 OperationId,
                                      RechargeInputParameters InputParameters,
                                      Currency RedeemableAmountToAdd,
                                      DataTable Promotions,
                                      AccountMovementsTable AccountMovements,
                                      CashierMovementsTable CashierMovements,
                                      MultiPromos.AccountBalance LastBalance,
                                      OperationCode OpCode,
                                      SqlTransaction SqlTrx,
                                      CurrencyExchangeResult ExchangeResult,
                                      String PromotionalTitoTicket,
                                      AccountPromotion AccountPromotion,
                                      JunketsBusinessLogic JunketInfo,
                                      out RechargeOutputParameters OutputParameters)
    {

      #region " Variables "

      Currency _cash_in;
      Currency _cash_in_split1;
      Currency _cash_in_split2;
      Currency _prize;
      Currency _nr2;
      Decimal _dec_nr2;
      Decimal _nr2_pct;
      Currency _redeemable;
      Currency _devolution;
      Currency _total_to_pay;
      Currency _card_deposit_to_be_paid;
      Currency _balance_increment;
      Currency _initial_balance;
      StringBuilder _sb;
      TYPE_SPLITS _splits;
      AccountPromotion _aux_promotion;
      Int64 _play_session_id;
      Boolean _card_paid_on_this_transaction;
      CASHIER_MOVEMENT _cashier_mov_split1;
      CASHIER_MOVEMENT _cashier_mov_split2;
      CashierSessionInfo _session_info;
      int _int_value;
      PrizeComputationMode _prize_mode;
      MultiPromos.AccountBalance _in_session_balance;
      MultiPromos.AccountBalance _to_add;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _fin_balance;
      Decimal _ini_points;
      Decimal _fin_points;
      ArrayList _voucher_promo_list;

      ENUM_ANTI_MONEY_LAUNDERING_LEVEL _anti_ml_level;
      Boolean _account_already_registered;
      Boolean _threshold_crossed;
      Boolean _is_promogame;
      String _gp_prefix;
      Currency _cash_in_tax;
      Currency _cash_in_tax_split1;
      Currency _cash_in_tax_split2;
      Currency _tax_provisions;
      Currency _tax_custody;
      CurrencyExchangeType _payment_type;
      Boolean _allow_anonymous_recharge;
      CashierMovementsTable _cashier_movements;
      Boolean _is_acceptor;
      Currency _daily_cash_in;
      Currency _max_allowed;

      Currency _base_tax_deposit;
      Currency _tax_deposit;
      Currency _base_tax_split2;
      Currency _tax_split2;

      RECHARGE_ERROR_CODE _error_code;
      DateTime _next_gaming_day;

      String _national_currency;
      CASHIER_MOVEMENT _card_pay_type_cashier_mov;
      MovementType _card_pay_type_account_mov;
      Boolean _card_to_company_b;
      Boolean _printVoucher;
      String _text;
      Boolean _is_enabled_cash_desk_02;
      Int64 _play_session_mahine_id;

      Boolean _add_promotion_to_account_balance;
      PromoGame _game;
      Int64 _promo_game_id;

      _add_promotion_to_account_balance = false;
      _play_session_mahine_id = 0;
      _promo_game_id = 0;
      MovementType _mtype;
      String _xml;

      #endregion

      // RCI & DRV 08-JUL-2014: For now, don't insert special movements for Integrated chip operations (Palacio Shortcut).
      //                        Insert them if the AC_INITIAL_CASH_IN_FOR_CHIPS is enabled in the future.
      //
      //Boolean _integrated_chip_operation;

      // TODO: Falta poner el campo IsVirtualCard en el InputParameters.
      //_integrated_chip_operation = Chips.IsIntegratedChipAndCreditOperations(InputParameters.IsVirtualCard);

      if (ExchangeResult != null)
      {
        _payment_type = ExchangeResult.InType;
      }
      else
      {
        _payment_type = CurrencyExchangeType.CURRENCY;
      }

      OutputParameters = new RechargeOutputParameters(OperationId);

      _prize = 0;
      _card_paid_on_this_transaction = false;
      _session_info = CashierMovements.CashierSessionInfo;
      _next_gaming_day = _session_info.GamingDay.AddDays(1);

      try
      {
        _is_enabled_cash_desk_02 = CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws((Int16)CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02);

        if (Misc.IsGamingDayEnabled() && WGDB.Now >= _next_gaming_day && !(_session_info.UserType == GU_USER_TYPE.SYS_ACCEPTOR))
        {
          OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_RECHARGE_OUT_GAMING_DAY;

          return false;
        }

        // Lock ACCOUNTS
        if (!MultiPromos.Trx_UpdateAccountBalance(InputParameters.AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _ini_points, out _play_session_id, SqlTrx))
        {
          return false;
        }

        // SGB & RCI 22-SEP-2014: When a note acceptor recharge is done, Cage must be enabled.
        Cashier.GetMobileBankUserType(_session_info.UserType, out _is_acceptor);
        if (_is_acceptor && !Cage.IsCageEnabled())
        {
          OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_NOTE_ACCEPTOR_WITH_CAGE_DISABLED;

          return false;
        }

        #region " Check GeneralParams MaxAllowedCashIn & MaxAllowedDailyCashIn"

        // RCI 02-OCT-2014: Only check if it's a real Mobile Bank
        if (_session_info.UserType == GU_USER_TYPE.USER && _session_info.IsMobileBank)
        {
          // SGB & RCI 22-SEP-2014: Check if exceeds the allowable limit amount.
          _max_allowed = GeneralParam.GetCurrency("Cashier", "MaxAllowedCashIn");
          if (_max_allowed > 0 && RedeemableAmountToAdd > _max_allowed)
          {
            OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_RECHARGE_LIMIT_EXCEEDED;

            return false;
          }

          _max_allowed = GeneralParam.GetCurrency("Cashier", "MaxAllowedDailyCashIn");
          if (_max_allowed > 0)
          {
            // With MB the recharge operation is already added in the function, unlike in recharges from the cashier.
            _daily_cash_in = Accounts.SumTodayCashIn(InputParameters.AccountId, SqlTrx);
            if (_daily_cash_in > _max_allowed)
            {
              OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_DAILY_RECHARGE_LIMIT_EXCEEDED;

              return false;
            }
          }
        }

        #endregion

        #region " AccountRechargedAllowed when Account is anonymous "

        // Account is anonymous
        if (InputParameters.AccountLevel == 0)
        {
          // JML 20-NOV-2014 Check limits for unpersonal cards
          if (!Accounts.AccountRechargeAllowed(InputParameters.AccountId,
                                               InputParameters.ExternalTrackData,
                                               InputParameters.AccountLevel,
                                               RedeemableAmountToAdd,
                                               SqlTrx,
                                          out _allow_anonymous_recharge,
                                          out _error_code))
          {
            return false;
          }

          if (!_allow_anonymous_recharge && !(OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE))
          {

            #region " switch (_error_code) "

            switch (_error_code)
            {
              case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED:
              case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED:
                _text = "Exceeded maximum recharge.";
                break;

              case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED:
              case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED:
                _text = "Exceeded maximum daily recharge.";
                break;

              case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT:
                _text = "Minimun amount recharge not reached.";

                break;
              default:
                _text = "";
                break;
            }
            #endregion

            Log.Error(String.Format("DB_Recharge: Account {0}. Recharge Warning: Anonymous account can do more recharges. {1}", InputParameters.AccountId, _text));
            OutputParameters.ErrorCode = _error_code;

            return false;
          }
        }

        #endregion

        #region " Check Anti MoneyLaundering "

        if (_payment_type == CurrencyExchangeType.CURRENCY)
        {
          // RCI 27-JUL-2013: Anti money laundering check
          if (!Accounts.CheckAntiMoneyLaunderingFilters(InputParameters.AccountId, ENUM_CREDITS_DIRECTION.Recharge, RedeemableAmountToAdd, SqlTrx,
                                                        out _anti_ml_level, out _threshold_crossed, out _account_already_registered))
          {
            return false;
          }

          #region " Switch AntiMoney Laundering Level "

          switch (_anti_ml_level)
          {
            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None:
              break;

            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.IdentificationWarning:
              if (!_account_already_registered && _threshold_crossed)
              {
                if (_session_info.UserType == GU_USER_TYPE.USER && _session_info.IsMobileBank)
                {
                  if (GeneralParam.GetBoolean("AntiMoneyLaundering", "Recharge.Identification.DontAllowMB"))
                  {
                    Log.Error(String.Format("DB_Recharge: Account {0}. Identification Warning: Don't allow MB recharge.", InputParameters.AccountId));
                    OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_AML_RECHARGE_NOT_AUTHORIZED;

                    return false;
                  }
                }
              }
              break;

            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.ReportWarning:
              if (_threshold_crossed)
              {
                if (_session_info.UserType == GU_USER_TYPE.USER && _session_info.IsMobileBank)
                {
                  if (GeneralParam.GetBoolean("AntiMoneyLaundering", "Recharge.Report.DontAllowMB"))
                  {
                    Log.Error(String.Format("DB_Recharge: Account {0}. Report Warning: Don't allow MB recharge.", InputParameters.AccountId));
                    OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_AML_RECHARGE_NOT_AUTHORIZED;

                    return false;
                  }
                }
              }
              break;

            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification:
            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Report:
              _gp_prefix = _anti_ml_level == ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification ? "Identification" : "Report";

              if ((!_account_already_registered) || (_threshold_crossed && GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled")))
              {
                Log.Error(String.Format("DB_Recharge: Account {0}. {1}: Must identify in CashDesk.", InputParameters.AccountId, _gp_prefix));
                OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_AML_RECHARGE_NOT_AUTHORIZED;

                return false;
              }
              break;
            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.MaxAllowed:
              _gp_prefix = "Maximum allowed";

              if (_session_info.IsMobileBank || !_account_already_registered)
              {
                Log.Error(_session_info.IsMobileBank ?
                          String.Format("DB_Recharge: Account {0}. {1}: MB recharge not authorized.", InputParameters.AccountId, _gp_prefix) :
                          String.Format("DB_Recharge: Account {0}. {1}: Must identify in CashDesk.", InputParameters.AccountId, _gp_prefix));
                OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_AML_RECHARGE_NOT_AUTHORIZED;

                return false;
              }

              break;

            default:
              Log.Error(String.Format("DB_Recharge: Account {0}. Anti MoneyLaundering level unknown: {1}.", InputParameters.AccountId, _anti_ml_level));

              return false;
          }

          #endregion

        }

        // END Anti money laundering check
        #endregion


        _initial_balance = _ini_balance.TotalBalance;

        _in_session_balance = MultiPromos.AccountBalance.Zero;

        #region " Multipromos Methods 'Trx_UpdatePlayedAndWon || Trx_PromoMarkAsExhausted' "

        if (_play_session_id != 0)
        {
          if (!MultiPromos.Trx_UpdatePlayedAndWon(_play_session_id, out _in_session_balance, SqlTrx))
          {
            return false;
          }
        }
        else
        {
          if (!MultiPromos.Trx_PromoMarkAsExhausted(InputParameters.AccountId, SqlTrx))
          {
            return false;
          }
        }
        #endregion

        #region " MultiPromos Method Trx_PromoRecomputeWithholdOnRecharge, only when money is added"
        // Only recompute withhold if redeemable money is added.
        if (RedeemableAmountToAdd > 0)
        {
          if (!MultiPromos.Trx_PromoRecomputeWithholdOnRecharge(InputParameters.AccountId, SqlTrx))
          {
            return false;
          }
        }

        #endregion

        #region " set variables values "

        _card_deposit_to_be_paid = 0;
        _total_to_pay = RedeemableAmountToAdd;
        _cash_in = _total_to_pay;
        _redeemable = _cash_in;
        _devolution = _cash_in;

        _base_tax_deposit = 0;
        _tax_deposit = 0;
        _base_tax_split2 = 0;
        _tax_split2 = 0;

        _cash_in_split1 = 0;
        _cash_in_split2 = 0;

        _nr2 = 0;
        _nr2_pct = 0;

        _cash_in_tax = 0;
        _cash_in_tax_split1 = 0;
        _cash_in_tax_split2 = 0;

        // DHA 01-MAR-2016: Winpot - Provisioning Tax
        _tax_provisions = 0;

        _tax_custody = 0;

        // DHA 30-JUN-2015
        _national_currency = CurrencyExchange.GetNationalCurrency();
        _card_to_company_b = Misc.IsCardPlayerToCompanyB();
        #endregion

        if (!Split.ReadSplitParameters(out _splits))
        {
          return false;
        }

        // DRV 28-MAY-2014: If is chips operations, the voucher title is not set by general param
        if (InputParameters.IsChipsOperation)
        {
          _splits.company_a.cash_in_voucher_title = Resource.String("STR_VOUCHER_CHIPS_SALE_TITLE");
        }

        _sb = new StringBuilder();

        #region " Calculate values CashIn in splits, redeemable, devolution, ... "

        if (RedeemableAmountToAdd > 0)
        {
          // Calculate cash in splits
          if (OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE)
          {

            _cash_in_split1 = Math.Round(_cash_in * _splits.company_a.chips_sale_pct / 100, 2, MidpointRounding.AwayFromZero);
            _cash_in_split2 = _cash_in - _cash_in_split1;
          }
          else
          {
            _cash_in_split1 = Math.Round(_cash_in * _splits.company_a.cashin_pct / 100, 2, MidpointRounding.AwayFromZero);
            _cash_in_split2 = _cash_in - _cash_in_split1;
          }

          _redeemable = _cash_in_split1 + _cash_in_split2;
          _devolution = _cash_in_split1 + _cash_in_split2;

          // AJQ 14-SEP-2012, Always create the "PrizeCoupon" promotion when A/B exists.
          if (_cash_in_split2 > 0)
          {
            _redeemable = _cash_in_split1;

            //[XGJ][FJC][SJA] 23-MAR-2017 Fixed Bug 26082:Sorteo de M�quina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado.
            //if (_splits.prize_coupon && !_is_enabled_cash_desk_02)
            if (_splits.prize_coupon)
            {
              _devolution = _cash_in_split1;
            }
          }

          if (!InputParameters.IsChipsOperation)
          {
            // RCI 31-DEC-2013: CashIn Tax
            if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled"))
            {
              _cash_in_tax_split1 = CalculateCashInTax(_cash_in_split1);

              _cash_in_split1 -= _cash_in_tax_split1;
              _redeemable -= _cash_in_tax_split1;
              _devolution -= _cash_in_tax_split1;

              if (_cash_in_split2 > 0 && GeneralParam.GetBoolean("Cashier", "CashInTax.AlsoApplyToSplitB"))
              {
                _cash_in_tax_split2 = CalculateCashInTax(_cash_in_split2);

                _cash_in_split2 -= _cash_in_tax_split2;
                if (!_splits.prize_coupon)
                {
                  _devolution -= _cash_in_tax_split2;
                }
              }
            }
            _cash_in_tax = _cash_in_tax_split1 + _cash_in_tax_split2;
          }
          else
          {
            if (GeneralParam.GetBoolean("GamingTables", "CashInTax.Enabled"))
            {
              _cash_in_tax_split1 = CalculateCashInTaxGamingTables(_cash_in_split1);

              _cash_in_split1 -= _cash_in_tax_split1;
              _redeemable -= _cash_in_tax_split1;
              _devolution -= _cash_in_tax_split1;

              if (_cash_in_split2 > 0 && GeneralParam.GetBoolean("GamingTables", "CashInTax.AlsoApplyToSplitB"))
              {
                _cash_in_tax_split2 = CalculateCashInTaxGamingTables(_cash_in_split2);

                _cash_in_split2 -= _cash_in_tax_split2;
                if (!_splits.prize_coupon)
                {
                  _devolution -= _cash_in_tax_split2;
                }
              }
            }
            _cash_in_tax = _cash_in_tax_split1 + _cash_in_tax_split2;

          }

          switch (Misc.ApplyCustodyConfigurationCalculateRatio(OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE))
          {
            case Misc.TaxCustodyMode.TaxOnSplit1:
              _tax_custody = Accounts.CalculateTaxCustody(_cash_in_split1, OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE);
              _cash_in_split1 -= _tax_custody;
              _redeemable -= _tax_custody;
              _devolution -= _tax_custody;
              break;

            case Misc.TaxCustodyMode.TaxOnDeposit:
              Decimal _cash_in_spit1_temp;
              _cash_in_spit1_temp = _cash_in_split1;
              if (OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE)
              {
                _cash_in_split1 = Math.Round((_cash_in_split1 / (1 + Misc.GamingTablesTaxCustodyPct() / 100)), 2);
              }
              else
              {
                _cash_in_split1 = Math.Round((_cash_in_split1 / (1 + Misc.TaxCustodyPct() / 100)), 2);
              }
              _tax_custody = Accounts.CalculateTaxCustody(_cash_in_split1, OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE);
              _cash_in_split1 = _cash_in_spit1_temp - _tax_custody; // ensure we have the whole initial cashin_split1
              _redeemable -= _tax_custody;
              _devolution -= _tax_custody;
              break;

            case Misc.TaxCustodyMode.None:
            default:

              break;
          }

          // DHA 01-MAR-2016: Winpot - Provisioning Tax
          if (Misc.IsTaxProvisionsEnabled())
          {
            _tax_provisions = CalculateTaxProvisions(_cash_in_split1);

            _cash_in_split1 -= _tax_provisions;
            _redeemable -= _tax_provisions;
            _devolution -= _tax_provisions;
          }

          if (!InputParameters.IsChipsOperation)
          {
            if (!Cashier.GetNR2(InputParameters.AccountId, RedeemableAmountToAdd, out _dec_nr2, out _nr2_pct))
            {
              Log.Error("DB_Recharge: GetNR2 failed. AccountId: " + InputParameters.AccountId.ToString() + "AmountToAdd: " + RedeemableAmountToAdd.ToString());

              return false;
            }
            _nr2 = _dec_nr2;
          }

          if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier", "PrizeComputationMode"), out _int_value))
          {
            _int_value = 0;
          }
          _prize_mode = (PrizeComputationMode)_int_value;

          if (_prize_mode != PrizeComputationMode.KeepCountingInitialCashIn && !InputParameters.IsChipsOperation)
          {
            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNTS ");
            _sb.AppendLine("   SET   AC_INITIAL_CASH_IN = CASE WHEN AC_RE_BALANCE + @pInSessionReBalance < AC_INITIAL_CASH_IN * @pRatio ");
            _sb.AppendLine("                                   THEN AC_RE_BALANCE + @pInSessionReBalance ");
            _sb.AppendLine("                                   ELSE AC_INITIAL_CASH_IN END ");
            _sb.AppendLine(" WHERE   AC_ACCOUNT_ID      = @pAccountId");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InputParameters.AccountId;
              _sql_cmd.Parameters.Add("@pInSessionReBalance", SqlDbType.Money).Value = _in_session_balance.Redeemable;
              _sql_cmd.Parameters.Add("@pRatio", SqlDbType.Decimal).Value = _splits.prize_coupon ? 1.0m : _splits.company_a.cashin_pct * 0.01m;

              if (_sql_cmd.ExecuteNonQuery() != 1)
              {
                Log.Error("DB_Recharge: Recompute InitialCashIn failed. AccountId: " + InputParameters.AccountId.ToString());

                return false;
              }
            }
          }

          if (!InputParameters.IsChipsOperation || FeatureChips.CashierChipsSaleMode(false))
          {
            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNTS ");
            _sb.AppendLine("   SET   AC_INITIAL_CASH_IN = AC_INITIAL_CASH_IN + @pDevolution ");
            _sb.AppendLine(" WHERE   AC_ACCOUNT_ID      = @pAccountId");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pDevolution", SqlDbType.Money).Value = _devolution.SqlMoney;
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InputParameters.AccountId;

              if (_sql_cmd.ExecuteNonQuery() != 1)
              {
                Log.Error("DB_Recharge. Update Initial CashIn. AccountId: " + InputParameters.AccountId.ToString());

                return false;
              }
            }
          }
        }

        #endregion


        // DHA 30-JUN-2015: set cashier movement and account movement for card casino payment method
        _card_pay_type_cashier_mov = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN : CASHIER_MOVEMENT.CARD_DEPOSIT_IN;
        _card_pay_type_account_mov = MovementType.DepositIn;

        #region " Set card_pay_type_cashier_mov & card_pay_type_account_mov "

        if (ExchangeResult != null && ExchangeResult.InType == CurrencyExchangeType.CURRENCY)
        {
          if (ExchangeResult.InCurrencyCode != _national_currency)
          {
            _card_pay_type_cashier_mov = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE : CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE;
            _card_pay_type_account_mov = MovementType.CardDepositInCurrencyExchange;
          }
        }
        else if (ExchangeResult != null && ExchangeResult.InType == CurrencyExchangeType.CHECK)
        {
          _card_pay_type_cashier_mov = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK : CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK;
          _card_pay_type_account_mov = MovementType.CardDepositInCheck;
        }
        else if (ExchangeResult != null && ExchangeResult.InType == CurrencyExchangeType.CARD)
        {
          _card_pay_type_cashier_mov = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC : CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC;
          _card_pay_type_account_mov = MovementType.CardDepositInCardGeneric;

          if (ExchangeResult.SubType == CurrencyExchangeSubType.CREDIT_CARD)
          {
            _card_pay_type_cashier_mov = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT : CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT;
            _card_pay_type_account_mov = MovementType.CardDepositInCardCredit;
          }
          else if (ExchangeResult.SubType == CurrencyExchangeSubType.DEBIT_CARD)
          {
            _card_pay_type_cashier_mov = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT : CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT;
            _card_pay_type_account_mov = MovementType.CardDepositInCardDebit;
          }
        }
        #endregion

        if (_session_info.UserType == GU_USER_TYPE.USER && !_session_info.IsMobileBank)
        {

          #region " Update Card Deposit "

          // 0. Update Card Deposit
          if (!InputParameters.CardPaid && !InputParameters.ChipSaleRegister)  // RCI 24-SEP-2010
          {
            _card_deposit_to_be_paid = InputParameters.CardPrice;

            _total_to_pay = _cash_in + _card_deposit_to_be_paid;

            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNTS ");
            _sb.AppendLine("   SET   AC_DEPOSIT    = @pDeposit ");
            _sb.AppendLine("       , AC_CARD_PAID  = 1 ");
            _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");
            _sb.AppendLine("   AND   AC_CARD_PAID  = 0 ");

            using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_command.Parameters.Add("@pDeposit", SqlDbType.Money).Value = _card_deposit_to_be_paid.SqlMoney;
              _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InputParameters.AccountId;

              if (_sql_command.ExecuteNonQuery() != 1)
              {
                Log.Error("DB_Recharge. Card Deposit not updated. AccountId: " + InputParameters.AccountId.ToString());

                return false;
              }
            }
            _card_paid_on_this_transaction = true;

            // DHA 11-NOV-2013 Re-order 'Card Deposit In' movement to first position
            if (_total_to_pay > 0)
            {
              if (_card_pay_type_cashier_mov != CASHIER_MOVEMENT.CARD_DEPOSIT_IN &&
                  _card_pay_type_cashier_mov != CASHIER_MOVEMENT.CARD_REPLACEMENT)
              {
                CalculateSplit2Tax(_card_deposit_to_be_paid, out _base_tax_deposit, out _tax_deposit);
              }
              else
              {
                _tax_deposit = -1;
              }

              AccountMovements.Add(OperationId, InputParameters.AccountId, _card_pay_type_account_mov, _initial_balance, 0, _card_deposit_to_be_paid, _initial_balance);
            }

            // DHA 7-JUL-2015: change the bank transaction recharge + card creation
            if (_card_paid_on_this_transaction && ExchangeResult != null && (ExchangeResult.InType == CurrencyExchangeType.CARD || ExchangeResult.InType == CurrencyExchangeType.CHECK))
            {
              ExchangeResult.BankTransactionData.TransactionType = TransactionType.RECHARGE_CARD_CREATION;
            }
          }

          #endregion

          #region  " UpdateSalesLimit && UpdateSessionTotalSold "

          // JCM 31-JUL-2012 
          //  Verify if Cashier User Limit Sales is reached by this sale
          if (_total_to_pay > 0)
          {
            if (!Cashier.UpdateSalesLimit(_session_info.UserId, _session_info.CashierSessionId, _total_to_pay, SqlTrx,
                                          out OutputParameters.SalesLimitReached, out OutputParameters.RemainingSalesLimit))
            {
              return false;
            }

            if (OutputParameters.SalesLimitReached)
            {
              // 12-NOV-2013 JML    Fixed Bug WIG-403: Recharge failed to reach the sales limit.
              return false;
            }

            if (!Cashier.UpdateSessionTotalSold(_session_info.CashierSessionId, _session_info.UserId, Cashier.SalesLimitType.Cashier, _total_to_pay, SqlTrx))
            {
              OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_RECHARGE_WITH_CASH_CLOSED;
              return false;
            }
          }
          #endregion

        } // CashierSessionInfo.UserType == GU_USER_TYPE.USER

        #region " AccountMovements Add "

        if (!InputParameters.OnlyPromotion)
        {
          // Add redeemable amount to the account.
          if (_redeemable > 0)
          {
            _to_add = new MultiPromos.AccountBalance(_redeemable, 0, 0, _tax_custody);

            if (!MultiPromos.Trx_UpdateAccountBalance(InputParameters.AccountId, _to_add, out _fin_balance, SqlTrx))
            {
              return false;
            }
            _mtype = MovementType.CashIn;

            if (OpCode != OperationCode.CHIPS_SWAP)
            {
              AccountMovements.Add(OperationId, InputParameters.AccountId, _mtype, _initial_balance, 0, _redeemable, _initial_balance + _redeemable);
            }

            // RCI & DRV 08-JUL-2014: For now, don't insert special movements for Integrated chip operations (Palacio Shortcut).
            //                        Insert them if the AC_INITIAL_CASH_IN_FOR_CHIPS is enabled in the future.
            //
            //if (InputParameters.IsChipsOperation && _integrated_chip_operation)
            //{
            //  AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.ChipsSaleWithCashIn, _initial_balance + _redeemable, 0, _redeemable, _initial_balance + _redeemable);
            //}

            if (_payment_type != CurrencyExchangeType.CURRENCY)
            {
              AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.HIDDEN_RechargeNotDoneWithCash, _initial_balance + _redeemable, 0, _redeemable, _initial_balance + _redeemable);
            }

            if (OpCode != OperationCode.CHIPS_SWAP)
            {
              if (_cash_in_tax > 0)
              {
                AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.CashInTax,
                                    _initial_balance + _redeemable, _cash_in_tax, _cash_in_tax, _initial_balance + _redeemable);
              }

              if (_tax_custody > 0)
              {
                AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.TaxCustody,
                                   _initial_balance + _redeemable, _tax_custody, _tax_custody, _initial_balance + _redeemable);
              }

              if (_tax_provisions > 0)
              {
                AccountMovements.Add(OperationId, InputParameters.AccountId, MovementType.TaxProvisions,
                                    _initial_balance + _redeemable, _tax_provisions, _tax_provisions, _initial_balance + _redeemable);
              }
            }
          }
        }
        #endregion

        _balance_increment = 0;

        #region " CashierMovements Add "

        if (_total_to_pay > 0 && !InputParameters.ChipSaleRegister)
        {
          // Redeemable amount
          if (_cash_in > 0)
          {

            #region " Switch _session_info.UserType "

            switch (_session_info.UserType)
            {
              case GU_USER_TYPE.USER:
              case GU_USER_TYPE.SYS_SYSTEM:
              case GU_USER_TYPE.SYS_REDEMPTION:
                if (_session_info.IsMobileBank)
                {
                  _cashier_mov_split1 = CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1;
                  _cashier_mov_split2 = CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2;
                }
                else
                {
                  _cashier_mov_split1 = CASHIER_MOVEMENT.CASH_IN_SPLIT1;
                  _cashier_mov_split2 = CASHIER_MOVEMENT.CASH_IN_SPLIT2;
                }
                break;

              case GU_USER_TYPE.SYS_ACCEPTOR:
              case GU_USER_TYPE.SYS_PROMOBOX:
              case GU_USER_TYPE.SYS_TITO:
                _cashier_mov_split1 = CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1;
                _cashier_mov_split2 = CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2;
                break;

              case GU_USER_TYPE.SUPERUSER:
              case GU_USER_TYPE.NOT_ASSIGNED:
              default:
                Log.Warning("DB_Recharge: User type unknown: " + _session_info.UserType.ToString() + " OperationId: " + OperationId.ToString() +
                            ", AccountId: " + InputParameters.AccountId.ToString());

                return false;
            }
            #endregion

            // RCI 03-SEP-2013: If NoMoney (AsCashIn), change the original cashier movements.
            if (InputParameters.NoMoney)
            {
              _cashier_mov_split1 = CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1;
              _cashier_mov_split2 = CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2;
              _prize = _cash_in;
            }

            #region "AddPromotionToAccount, AddExchange "

            // The CASH_IN cashier movement only has to be added if the Recharge is done in a physical cashier.
            if (_session_info.UserType == GU_USER_TYPE.USER && !_session_info.IsMobileBank)
            {
              if (ExchangeResult != null)
              {
                // If there is a defined NR2 promotion for the currency exchange, insert the promotion to the account.
                if (ExchangeResult.ArePromotionsEnabled)
                {
                  if (!Promotion.ReadPromotionData(SqlTrx, Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE, out _aux_promotion))
                  {
                    return false;
                  }
                  // Insert the Exchange promotion in DataTable promotions.
                  if (!AccountPromotion.AddPromotionToAccount(OperationId, InputParameters.AccountId, InputParameters.AccountLevel, RedeemableAmountToAdd,
                                                                 0, true, ExchangeResult.NR2Amount, _aux_promotion, Promotions))
                  {
                    return false;
                  }
                }
                CashierMovements.AddExchange(OperationId, InputParameters.AccountId, InputParameters.ExternalTrackData, ExchangeResult);
              }

              if (OpCode != OperationCode.CHIPS_SWAP)
              {
                CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CASH_IN, _cash_in, InputParameters.AccountId, InputParameters.ExternalTrackData);
                _balance_increment += _cash_in;
              }
            }

            #endregion

            #region " CashierMovements Add when !InputParameters.OnlyPromotion"

            if (!InputParameters.OnlyPromotion)
            {
              if (_prize > 0)
              {
                Decimal _max_prize;

                _max_prize = GeneralParam.GetDecimal("Cashier", "PointsToRedeemable.MaxPrize", 10000);
                if (_prize > _max_prize && _max_prize > 0)
                {
                  Log.Error(String.Format("PointsToRedeemable Prize {0} is greater than MaxPrize {1} ", _prize.ToString(), _max_prize.ToString()));

                  OutputParameters.ErrorCode = RECHARGE_ERROR_CODE.ERROR_PRIZE_TOO_BIG;

                  return false;
                }

                CashierMovements.Add(OperationId, CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE, _prize, InputParameters.AccountId, InputParameters.ExternalTrackData);
              }

              if (OpCode != OperationCode.CHIPS_SWAP)
              {
                if (_cash_in_split1 > 0)
                {
                  // RCI & DRV 08-JUL-2014: For now, don't insert special movements for Integrated chip operations (Palacio Shortcut).
                  //                        Insert them if the AC_INITIAL_CASH_IN_FOR_CHIPS is enabled in the future.
                  //
                  //if (InputParameters.IsChipsOperation && _integrated_chip_operation)
                  //{
                  //  CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CHIPS_SALE_WITH_CASH_IN, _cash_in, InputParameters.AccountId, InputParameters.ExternalTrackData);
                  //}

                  CashierMovements.Add(OperationId, _cashier_mov_split1, _cash_in_split1, InputParameters.AccountId, InputParameters.ExternalTrackData);

                  if (_cash_in_tax_split1 > 0)
                  {
                    CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT1, _cash_in_tax_split1, InputParameters.AccountId, InputParameters.ExternalTrackData);
                  }

                  if (_tax_custody > 0)
                  {
                    CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_CUSTODY, _tax_custody, InputParameters.AccountId, InputParameters.ExternalTrackData);
                  }

                  if (_tax_provisions > 0)
                  {
                    CashierMovements.Add(OperationId, CASHIER_MOVEMENT.TAX_PROVISIONS, _tax_provisions, InputParameters.AccountId, InputParameters.ExternalTrackData);
                  }
                }

                if (_cash_in_split2 > 0)
                {
                  CalculateSplit2Tax(_cash_in_split2, out _base_tax_split2, out _tax_split2);
                  CashierMovements.Add(OperationId, _cashier_mov_split2, _cash_in_split2, InputParameters.AccountId, InputParameters.ExternalTrackData, "", "", 0, 0, 0, _tax_split2);

                  if (_cash_in_tax_split2 > 0)
                  {
                    CashierMovements.Add(OperationId, CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT2, _cash_in_tax_split2, InputParameters.AccountId, InputParameters.ExternalTrackData);
                  }
                }
              }
            }
            #endregion
          }

          // DHA 30-29-2015: Insert casino card pay metod 
          if (_card_paid_on_this_transaction)
          {
            CashierMovements.Add(OperationId, _card_pay_type_cashier_mov, _card_deposit_to_be_paid, InputParameters.AccountId, InputParameters.ExternalTrackData, "", "", 0, 0, 0, _tax_deposit);
          }

        } // if _total_to_pay

        #endregion

        #region "RedeemableAmountToAdd > 0"

        if (RedeemableAmountToAdd > 0)
        {

          if (Misc.IsTerminalDrawEnabled() && !InputParameters.IsChipsOperation)
          {
            if (!WSI.Common.TerminalDraw.TerminalDraw.GenerateTerminalDrawRecharge(OperationId,
                                                                              InputParameters.AccountId,
                                                                              _cash_in_split1, _cash_in_split2,
                                                                              out OutputParameters.ErrorCode,
                                                                                   out OutputParameters.ErrorMessage, SqlTrx))
            {
              return false;
            }
          }
          else
          {
            if (!CashDeskDrawBusinessLogic.ConditionallyCashDeskDraw(OperationId,
                                                                     InputParameters,
                                                                     RedeemableAmountToAdd,
                                                                     Promotions,
                                                                     AccountMovements,
                                                                     CashierMovements,
                                                                     SqlTrx,
                                                                     _cash_in_split1,
                                                                     _cash_in_split2,
                                                                     _tax_provisions,
                                                                     _nr2,
                                                                       AccountPromotion,
                                                                     out  OutputParameters,
                                                                     out _play_session_mahine_id))
            {
              return false;
            }
          }
        }

        #endregion

        // If promogameId is different than 1 the promotionstatus it's different than redemeed
        _is_promogame = (AccountPromotion != null && AccountPromotion.PromoGameId > 1);

        if (!AccountPromotion.InsertAccountPromotions(Promotions, false, _is_promogame, SqlTrx))
        {
          return false;
        }

        #region " TerminalDraw.GenerateTerminalDrawRecharge "

        // If PromoGameId isn't Bienvenida draw (1) generate a new terminal draw recharge with promo_game_Id
        // and if promotion is related with promogame
        if (Misc.IsTerminalDrawEnabled() && _is_promogame)
        {
          _game = new PromoGame(AccountPromotion.PromoGameId);
          _xml = AccountPromotion.PyramidalDist;
          _add_promotion_to_account_balance = true;

          if (!TerminalDraw.TerminalDraw.GenerateTerminalDrawRecharge(OperationId,
                                                                      InputParameters.AccountId,
                                                                      _cash_in_split1, _cash_in_split2,
                                                                      _xml,
                                                                      AccountPromotion.PromoGameId,
                                                                      AccountPromotion.min_cash_in_reward,
                                                                      _game.Type,
                                                                      out OutputParameters.ErrorCode,
                                                                      out OutputParameters.ErrorMessage, SqlTrx))
          {
            return false;
          }
        }

        #endregion

        if (ExchangeResult != null)
        {
          ExchangeResult.AccountPromotionId = GetAccountPromotionId(Promotions, "ACP_PROMO_TYPE = " + (Int32)Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE);
        }

        OutputParameters.CoverCouponAccountPromotionId = GetAccountPromotionId(Promotions, "ACP_PROMO_TYPE = " + (Int32)Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON);
        OutputParameters.AccountPromotionId = GetAccountPromotionId(Promotions, "ACP_PROMO_TYPE <> " + (Int32)Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE +
                                                                           " AND ACP_PROMO_TYPE <> " + (Int32)Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON);
        #region " MultiPromos Method Trx_AccountPromotionCost "

        // DDM 18-SEP-2012: Important! Must go before activate promotions!!
        if (_cash_in_split1 > 0)
        {
          // JML 28-Jan-2014: Reset cost promotions when recharging in the following cases: extra ball or the amount NR less than GP
          Decimal _min_amount_to_calculate_cost;
          Boolean _reset_cost_on_extra_ball;

          _min_amount_to_calculate_cost = 0m;
          _reset_cost_on_extra_ball = false;

          // JML 9-FEeb-2015: Tito mode: use default values.
          if (!TITO.Utils.IsTitoMode())
          {
            // JML 9-FEeb-2015: Cashless: use parameter values.
            _min_amount_to_calculate_cost = GeneralParam.GetDecimal("Cashier", "Promotion.NR.MinAmountToCalculateCost", 0m);
            _reset_cost_on_extra_ball = GeneralParam.GetBoolean("Cashier", "Promotion.NR.ResetCostOnExtraBall", false);
          }

          if ((_ini_balance.PromoNotRedeemable < _min_amount_to_calculate_cost && _play_session_id == 0) || (_reset_cost_on_extra_ball && _play_session_id != 0))
          {
            if (!MultiPromos.Trx_AccountPromotionCost(InputParameters.AccountId, ACCOUNTS_PROMO_COST.RESET_ON_RECHARGE, 0, SqlTrx))
            {
              return false;
            }
          }
          else
          {
            if (!MultiPromos.Trx_AccountPromotionCost(InputParameters.AccountId, ACCOUNTS_PROMO_COST.UPDATE_ON_RECHARGE, _cash_in_split1, SqlTrx))
            {
              return false;
            }
          }
        }

        #endregion

        // DRV 01-AUG-2014: When ChipSaleRegister, don't insert cashier movements.
        _cashier_movements = null;
        if (!InputParameters.ChipSaleRegister)
        {
          _cashier_movements = CashierMovements;
        }

        if (!AccountPromotion.Trx_ActivatePromotionsOnAccount(Promotions, AccountMovements, _cashier_movements, _add_promotion_to_account_balance, SqlTrx, InputParameters.CashDeskDraw, _is_promogame)) // LTC 13-OCT-2016
        {
          return false;
        }

        #region " CashDeskDrawBusinessLogic Methods GenerateMovementsPrizeModeCashPrize, GetOutCashPrizeMode "

        //RGR 13-OCT-2016
        if (OutputParameters.CashDeskDrawResult != null && InputParameters.ParticipateInCashDeskDraw != ParticipateInCashDeskDraw.No)
        {
          OutputParameters.CashDeskDrawResult.IsCashPrize = false;
          if (CashDeskDrawBusinessLogic.IsCashPrizeCashDeskDraw(InputParameters.CashDeskDraw))
          {
            //RGR 13-OCT-2016
            switch (OutputParameters.CashDeskDrawResult.m_prize_category.m_prize_type)
            {
              case PrizeType.Redeemable:
                if (!CashDeskDrawBusinessLogic.GenerateMovementsPrizeModeCashPrize(InputParameters.AccountId, InputParameters, OutputParameters, AccountMovements, CashierMovements, SqlTrx))
                {
                  return false;
                }
                break;
              case PrizeType.NoRedeemable:
                OutputParameters.CashDeskDrawResult.CashPrizeMode = CashDeskDrawBusinessLogic.GetOutCashPrizeMode(OutputParameters);
                break;
            }
            OutputParameters.CashDeskDrawResult.IsCashPrize = true;
          }
        }

        #endregion

        if (!MultiPromos.Trx_UpdateAccountBalance(InputParameters.AccountId, MultiPromos.AccountBalance.Zero, 0, out _fin_balance, out _fin_points, SqlTrx))
        {
          return false;
        }

        _to_add = _fin_balance - _ini_balance + LastBalance;

        #region " Operations.Trx_SetOperationBalance - MultiPromos.Trx_SetActivity "

        if ((OpCode == OperationCode.PROMOTION_WITH_TAXES || OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE) ||
            (OpCode != OperationCode.PROMOTION_WITH_TAXES && OpCode != OperationCode.PRIZE_PAYOUT && OpCode != OperationCode.PRIZE_PAYOUT_AND_RECHARGE))
        {

          if (!Operations.Trx_SetOperationBalance(OperationId, _to_add, SqlTrx))
          {
            return false;
          }

        }

        if (!MultiPromos.Trx_SetActivity(InputParameters.AccountId, SqlTrx))
        {
          return false;
        }
        #endregion

        OutputParameters.InitialBalance = _ini_balance;
        OutputParameters.FinalBalance = _fin_balance;
        OutputParameters.CoverCouponAmount = _nr2;

        #region " DB_InsertBankTransaction "

        //JBC 22/09/2014
        if (ExchangeResult != null && ExchangeResult.BankTransactionData.DocumentNumber != null)
        {
          if (!DB_InsertBankTransaction(OperationId, ExchangeResult, InputParameters.ExternalTrackData, SqlTrx))
          {

            return false;
          }
        }

        if (!InputParameters.GenerateVouchers)
        {
          return true;
        }

        #endregion

        #region " OutputParameters.VoucherList "

        OutputParameters.VoucherList = null;

        if (ExchangeResult != null)
        {
          OutputParameters.VoucherList = VoucherBuilder.CurrencyExchange(InputParameters.VoucherAccountInfo, InputParameters.AccountId, _splits, ExchangeResult,
                                                                         ExchangeResult.InType,
                                                                         CurrencyExchange.GetNationalCurrency(), OperationId, PrintMode.Print, SqlTrx);

          if (ExchangeResult.InType == CurrencyExchangeType.CREDITLINE)
          {
            VoucherBuilder.GetVoucherCreditLineGetMarker(OutputParameters.VoucherList, InputParameters.VoucherAccountInfo, ExchangeResult.InAmount + ExchangeResult.CardPrice, InputParameters.CreditLineData, _splits, OperationId, PrintMode.Print, SqlTrx);
          }

        }
        // AVZ SDS LA
        _printVoucher = true;
        if (RedeemableAmountToAdd == 0 && (OpCode == OperationCode.PRIZE_PAYOUT || OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE) && InputParameters.CardPaid)
        {
          _printVoucher = false;
        }

        #endregion

        #region " Set Voucher CashIn Values "

        if (!InputParameters.OnlyPromotion && _printVoucher && OpCode != OperationCode.CHIPS_SWAP)
        {

          ArrayList _cash_in_vouchers;

          VoucherCashIn.InputDetails _details = new VoucherCashIn.InputDetails();
          _details.InitialBalance = new MultiPromos.PromoBalance(_ini_balance, _ini_points);
          _details.FinalBalance = new MultiPromos.PromoBalance(_fin_balance, _fin_points);
          _details.CashIn1 = _cash_in_split1 - SubtractParticipationInDraw(OutputParameters.CashDeskDrawResult, InputParameters.CashDeskDraw);
          _details.CashIn2 = _cash_in_split2;
          _details.CardPrice = _card_deposit_to_be_paid;
          _details.CashInTaxSplit1 = _cash_in_tax_split1;
          _details.CashInTaxSplit2 = _cash_in_tax_split2;
          _details.CashInTaxCustody = _tax_custody; // LTC 14-ABR-2016
          _details.TotalBaseTaxSplit2 = _base_tax_deposit + _base_tax_split2;
          _details.TotalTaxSplit2 = _tax_deposit + _tax_split2;
          _details.IsIntegratedChipsSale = InputParameters.IsChipsOperation;

          if (ExchangeResult != null)
          {
            _details.ExchangeCommission = ExchangeResult.Comission;
            _details.PaymentType = ExchangeResult.InType;
            _details.WasForeingCurrency = ExchangeResult.WasForeingCurrency;
            _details.CardNumber = ExchangeResult.BankTransactionData.DocumentNumber;
            _details.CardHolder = ExchangeResult.BankTransactionData.HolderName;
            if (Misc.GetVoucherMode() == 1 && !String.IsNullOrEmpty(_details.CardNumber))
            {
              _details.IsPayingWithCreditCard = true;
            }

            // EOR 07-SEP-2016
            if (Misc.IsVoucherModeTV())
            {
              _details.CardAmount = ExchangeResult.CardPrice;
              _details.CardCommission = ExchangeResult.Comission;
            }
          }

          if (AccountPromotion != null)
          {
            _promo_game_id = AccountPromotion.PromoGameId;
          }

          // LTC 22-SEP-2016
          _cash_in_vouchers = VoucherBuilder.CashIn(InputParameters.VoucherAccountInfo,
                                                    InputParameters.AccountId,
                                                    _splits,
                                                    _details,
                                                    OutputParameters.CashDeskDrawResult,
                                                    OperationId,
                                                    OpCode,
                                                    PrintMode.Print,
                                                    InputParameters.CashDeskDraw,
                                                    SqlTrx,
                                                    JunketInfo,
                                                    _promo_game_id);

          if (OutputParameters.VoucherList == null)
          {
            OutputParameters.VoucherList = _cash_in_vouchers;
          }
          else
          {
            foreach (Voucher _voucher in _cash_in_vouchers)
            {
              OutputParameters.VoucherList.Add(_voucher);
            }
          }
        }

        #endregion

        #region " Generate Voucher PromoLost "

        if (Promotions != null && Promotions.Rows.Count > 0)
        {
          if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier.Voucher", "VoucherOnPromotionAwarded"), out _int_value))
          {
            _int_value = 0;
          }

          if (_int_value > 0)
          {
            if (!VoucherBuilder.PromoLost(InputParameters.VoucherAccountInfo,
                                                     InputParameters.AccountId,
                                                     _splits,
                                                     (_int_value == 1),
                                                     Promotions,
                                                     true,
                                                     OperationId,
                                                     PrintMode.Print,
                                                     OpCode,
                                                     PromotionalTitoTicket,
                                                     SqlTrx,
                                                     out _voucher_promo_list))
            {
              return false;
            }

            if (OutputParameters.VoucherList == null)
            {
              OutputParameters.VoucherList = _voucher_promo_list;
            }
            else
            {
              foreach (Voucher _voucher in _voucher_promo_list)
              {
                OutputParameters.VoucherList.Add(_voucher);
              }
            }
          }
        }

        #endregion

        #region " Generate Voucher TaxCustodyVoucher "

        // LTC 14-ABR-2016
        if (Misc.IsTaxCustodyEnabled() &&
           _tax_custody > 0 &&
          Misc.GetTaxCustodyVoucher() == Misc.TaxCustodyVoucher.CustodyVoucher) // EOR 02-ABR-2016
        {
          VoucherTaxCustody _tax_custody_voucher;

          _tax_custody_voucher = new VoucherTaxCustody(InputParameters.VoucherAccountInfo, _splits, _tax_custody, PrintMode.Print, SqlTrx);

          _tax_custody_voucher.Save(OperationId, SqlTrx);

          if (OutputParameters.VoucherList == null)
          {
            OutputParameters.VoucherList = new ArrayList();
            OutputParameters.VoucherList.Add(_tax_custody_voucher);
          }
          else
          {
            OutputParameters.VoucherList.Add(_tax_custody_voucher);
          }
        }

        #endregion

        #region " Generate Voucher tax_provisions "

        if (Misc.IsTaxProvisionsEnabled() && _tax_provisions > 0)
        {
          VoucherTaxProvisions _tax_provisions_voucher;

          _tax_provisions_voucher = new VoucherTaxProvisions(InputParameters.VoucherAccountInfo, _splits, _tax_provisions, PrintMode.Print, SqlTrx);

          _tax_provisions_voucher.Save(OperationId, SqlTrx);

          if (OutputParameters.VoucherList == null)
          {
            OutputParameters.VoucherList = new ArrayList();
            OutputParameters.VoucherList.Add(_tax_provisions_voucher);
          }
          else
          {
            OutputParameters.VoucherList.Add(_tax_provisions_voucher);
          }
        }

        #endregion

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    public static decimal SubtractParticipationInDraw(Result result, CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION draw)
    {
      decimal _subtract;

      _subtract = 0;
      if (result != null)
      {
        if (CashDeskDrawBusinessLogic.IsCashPrizeCashDeskDraw(draw))
        {
          _subtract = result.m_participation_amount;
        }
      }
      return _subtract;
    }

    private static Int64 GetAccountPromotionId(DataTable Promotions, String Filter)
    {
      DataRow[] _aux_rows;

      _aux_rows = Promotions.Select(Filter);
      if (_aux_rows.Length > 0)
      {

        return (Int64)_aux_rows[0]["ACP_UNIQUE_ID"];
      }

      return 0;

    } // GetAccountPromotionId


    //------------------------------------------------------------------------------
    // PURPOSE: Insert BankTransactionData into DB (table bank_transactions)
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - Int64 OperationId
    //          - BankTransactionData BankData
    //          - SqlTransaction Trx
    //
    //
    // RETURNS: 
    //      - Boolean
    //          true all works / false error
    // 
    public static Boolean DB_InsertBankTransaction(Int64 OperationId, CurrencyExchangeResult ExchangeResult,
                                                   String ExternalTrackData, SqlTransaction Trx)
    {
      StringBuilder _sb;
      BankTransactionData _bank_data;
      CashierSessionInfo _cashier_session;
      String _track_data;
      Int32 _card_type;

      try
      {
        _cashier_session = CommonCashierInformation.CashierSessionInfo();

        _bank_data = ExchangeResult.BankTransactionData;

        if (!CardNumber.TrackDataToInternal(ExternalTrackData, out _track_data, out _card_type))
        {
          _track_data = ExternalTrackData;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO      BANK_TRANSACTIONS           ");
        _sb.AppendLine("                 (                           ");
        _sb.AppendLine("                 BT_OPERATION_ID             ");
        _sb.AppendLine("             ,   BT_TYPE                     ");
        _sb.AppendLine("             ,   BT_DOCUMENT_NUMBER          ");
        _sb.AppendLine("             ,   BT_BANK_NAME                ");
        _sb.AppendLine("             ,   BT_HOLDER_NAME              ");
        _sb.AppendLine("             ,   BT_BANK_COUNTRY             ");
        _sb.AppendLine("             ,   BT_CARD_EXPIRATION_DATE     ");
        _sb.AppendLine("             ,   BT_CARD_TRACK_DATA          ");
        _sb.AppendLine("             ,   BT_AMOUNT                   ");
        _sb.AppendLine("             ,   BT_ISO_CODE                 ");
        _sb.AppendLine("             ,   BT_USER_ID                  ");
        _sb.AppendLine("             ,   BT_CASHIER_ID               ");
        _sb.AppendLine("             ,   BT_USER_NAME                ");
        _sb.AppendLine("             ,   BT_CASHIER_NAME             ");
        _sb.AppendLine("             ,   BT_TRANSACTION_TYPE         ");
        _sb.AppendLine("             ,   BT_EDITED                   ");
        _sb.AppendLine("             ,   BT_CHECK_ROUTING_NUMBER     ");
        _sb.AppendLine("             ,   BT_CHECK_ACCOUNT_NUMBER     ");
        _sb.AppendLine("             ,   BT_CHECK_DATE               ");
        _sb.AppendLine("             ,   BT_COMMENTS                 ");
        _sb.AppendLine("             ,   BT_ACCOUNT_TRACK_DATA       ");
        _sb.AppendLine("             ,   BT_ACCOUNT_HOLDER_NAME      ");
        _sb.AppendLine("                 )                           ");
        _sb.AppendLine("    VALUES                                   ");
        _sb.AppendLine("                 (                           ");
        _sb.AppendLine("                 @pOperationId               ");
        _sb.AppendLine("             ,   @pType                      ");
        _sb.AppendLine("             ,   @pDocumentNumber            ");
        _sb.AppendLine("             ,   @pBankName                  ");
        _sb.AppendLine("             ,   @pHolderName                ");
        _sb.AppendLine("             ,   @pBankCountry               ");
        _sb.AppendLine("             ,   @pCardExpirationDate        ");
        _sb.AppendLine("             ,   @pCardTrackData             ");
        _sb.AppendLine("             ,   @pAmount                    ");
        _sb.AppendLine("             ,   @pIsoCode                   ");
        _sb.AppendLine("             ,   @pUserId                    ");
        _sb.AppendLine("             ,   @pCashierId                 ");
        _sb.AppendLine("             ,   @pUserName                  ");
        _sb.AppendLine("             ,   @pCashierName               ");
        _sb.AppendLine("             ,   @pTransactionType           ");
        _sb.AppendLine("             ,   @pEdited                    ");
        _sb.AppendLine("             ,   @pCheckRoutingNumber        ");
        _sb.AppendLine("             ,   @pCheckAccountNumber        ");
        _sb.AppendLine("             ,   @pCheckDate                 ");
        _sb.AppendLine("             ,   @pComments                  ");
        _sb.AppendLine("             ,   @pAccountTrackdata          ");
        _sb.AppendLine("             ,   @pAccountHolderName         ");
        _sb.AppendLine("                 )                           ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Int32)_bank_data.Type;
          _cmd.Parameters.Add("@pDocumentNumber", SqlDbType.NVarChar).Value = _bank_data.DocumentNumber;

          // DHA 25-SEP-2015: add card price only when is CARD
          if (_bank_data.Type == CurrencyExchangeSubType.BANK_CARD
            || _bank_data.Type == CurrencyExchangeSubType.CREDIT_CARD
            || _bank_data.Type == CurrencyExchangeSubType.DEBIT_CARD)
          {
            _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = ExchangeResult.GrossAmount + ExchangeResult.CardPrice;
          }
          else
          {
            _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = ExchangeResult.GrossAmount;
          }

          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = ExchangeResult.InCurrencyCode;
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = _cashier_session.AuthorizedByUserId;
          _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = _cashier_session.TerminalId;
          _cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar).Value = _cashier_session.AuthorizedByUserName;
          _cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar).Value = _cashier_session.TerminalName;
          _cmd.Parameters.Add("@pEdited", SqlDbType.Bit).Value = _bank_data.CardEdited;
          _cmd.Parameters.Add("@pTransactionType", SqlDbType.Int).Value = _bank_data.TransactionType;

          if (!String.IsNullOrEmpty(_track_data))
          {
            _cmd.Parameters.Add("@pAccountTrackdata", SqlDbType.NVarChar).Value = _track_data;
          }
          else
          {
            _cmd.Parameters.Add("@pAccountTrackdata", SqlDbType.NVarChar).Value = DBNull.Value;
          }

          if (!String.IsNullOrEmpty(ExchangeResult.BankTransactionData.AccountHolderName))
          {
            _cmd.Parameters.Add("@pAccountHolderName", SqlDbType.NVarChar).Value = ExchangeResult.BankTransactionData.AccountHolderName;//_cashier_session.UserId;
          }
          else
          {
            _cmd.Parameters.Add("@pAccountHolderName", SqlDbType.NVarChar).Value = DBNull.Value;//_cashier_session.UserId;
          }

          if (!String.IsNullOrEmpty(_bank_data.BankName))
          {
            _cmd.Parameters.Add("@pBankName", SqlDbType.NVarChar).Value = _bank_data.BankName;
          }
          else
          {
            _cmd.Parameters.Add("@pBankName", SqlDbType.NVarChar).Value = DBNull.Value;
          }

          if (!String.IsNullOrEmpty(_bank_data.HolderName))
          {
            _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).Value = _bank_data.HolderName;
          }
          else
          {
            _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).Value = DBNull.Value;
          }

          _cmd.Parameters.Add("@pBankCountry", SqlDbType.Int).Value = _bank_data.BankCountry;

          if (!String.IsNullOrEmpty(_bank_data.ExpirationDate))
          {
            _cmd.Parameters.Add("@pCardExpirationDate", SqlDbType.NVarChar).Value = _bank_data.ExpirationDate;
          }
          else
          {
            _cmd.Parameters.Add("@pCardExpirationDate", SqlDbType.NVarChar).Value = DBNull.Value;
          }

          if (!String.IsNullOrEmpty(_bank_data.TrackData))
          {
            _cmd.Parameters.Add("@pCardTrackData", SqlDbType.VarChar).Value = _bank_data.TrackData;
          }
          else
          {
            _cmd.Parameters.Add("@pCardTrackData", SqlDbType.VarChar).Value = DBNull.Value;
          }

          if (!String.IsNullOrEmpty(_bank_data.RoutingNumber))
          {
            _cmd.Parameters.Add("@pCheckRoutingNumber", SqlDbType.NVarChar).Value = _bank_data.RoutingNumber;
          }
          else
          {
            _cmd.Parameters.Add("@pCheckRoutingNumber", SqlDbType.NVarChar).Value = DBNull.Value;
          }

          if (!String.IsNullOrEmpty(_bank_data.AccountNumber))
          {
            _cmd.Parameters.Add("@pCheckAccountNumber", SqlDbType.NVarChar).Value = _bank_data.AccountNumber;
          }
          else
          {
            _cmd.Parameters.Add("@pCheckAccountNumber", SqlDbType.NVarChar).Value = DBNull.Value;
          }

          if (_bank_data.CheckDate > DateTime.MinValue)
          {
            _cmd.Parameters.Add("@pCheckDate", SqlDbType.DateTime).Value = _bank_data.CheckDate;
          }
          else
          {
            _cmd.Parameters.Add("@pCheckDate", SqlDbType.DateTime).Value = DBNull.Value;
          }


          if (!String.IsNullOrEmpty(_bank_data.Comments))
          {
            _cmd.Parameters.Add("@pComments", SqlDbType.NVarChar).Value = _bank_data.Comments;
          }
          else
          {
            _cmd.Parameters.Add("@pComments", SqlDbType.NVarChar).Value = DBNull.Value;
          }


          if (_cmd.ExecuteNonQuery() != 1)
          {
            //Log.Error("DB_InsertBankTransaction. Card Transaction. AccountId: " + InputParameters.AccountId.ToString());

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate CashIn Tax. Needed in VoucherCashIn and DB_Recharge.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - Currency CashIn
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Currency
    // 
    public static Currency CalculateCashInTax(Currency CashIn)
    {
      Decimal _base_pct;
      Decimal _tax_pct;

      _base_pct = Math.Max(0, GeneralParam.GetDecimal("Cashier", "CashInTax.BasePct"));
      _tax_pct = Math.Max(0, GeneralParam.GetDecimal("Cashier", "CashInTax.Pct"));

      return Math.Round(CashIn * _base_pct * _tax_pct / 10000, 2, MidpointRounding.AwayFromZero);

    } // CalculateCashInTax

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate CashIn Tax for GamingTables. Needed in VoucherCashIn and DB_Recharge.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - Currency CashIn
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Currency
    // 
    public static Currency CalculateCashInTaxGamingTables(Currency CashIn)
    {
      Decimal _base_pct;
      Decimal _tax_pct;

      _base_pct = Math.Max(0, GeneralParam.GetDecimal("GamingTables", "CashInTax.BasePct"));
      _tax_pct = Math.Max(0, GeneralParam.GetDecimal("GamingTables", "CashInTax.Pct"));

      return Math.Round(CashIn * _base_pct * _tax_pct / 10000, 2, MidpointRounding.AwayFromZero);

    } // CalculateCashInTaxGamingTables

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate Provisionig Tax. Needed in VoucherCashIn and DB_Recharge.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - Currency CashIn
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Currency
    // 
    public static Currency CalculateTaxProvisions(Currency CashIn)
    {
      Decimal _tax_pct;

      _tax_pct = Math.Max(0, Misc.TaxProvisionsPct());

      return Math.Round(CashIn * _tax_pct / 100, 2, MidpointRounding.AwayFromZero);

    } // CalculateProvisiongTax

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate Custody Tax. Needed in VoucherCashIn and DB_Recharge.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - Currency CashIn
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Currency
    // 
    public static Currency CalculateTaxCustody(Currency CashIn, Boolean ApplyGamingTableTaxCustody)
    {
      Decimal _tax_pct;
      if (ApplyGamingTableTaxCustody)
      {
        _tax_pct = Math.Max(0, Misc.GamingTablesTaxCustodyPct());
      }
      else
      {
        _tax_pct = Math.Max(0, Misc.TaxCustodyPct());
      }

      return Math.Round(CashIn * _tax_pct / 100, 2, MidpointRounding.AwayFromZero);

    } // CalculateTaxCustody

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate Split2 Tax. Needed in VoucherCashIn and DB_Recharge.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - Currency Gross
    //          - Boolean IsDeposit
    //
    //      - OUTPUT:
    //          - Currency BaseTax
    //          - Currency Tax
    //
    // RETURNS: 
    // 
    public static void CalculateSplit2Tax(Currency Gross,
                                          out Currency BaseTax,
                                          out Currency Tax)
    {
      Decimal _tax_percent;

      BaseTax = 0;
      Tax = 0;

      _tax_percent = GeneralParam.GetDecimal("Cashier", "Split.B.Tax.Pct", 0);

      if (_tax_percent > 0)
      {
        BaseTax = Math.Round(Gross / (1 + _tax_percent / 100), 2, MidpointRounding.AwayFromZero);
        Tax = Math.Round(BaseTax * _tax_percent / 100, 2, MidpointRounding.AwayFromZero);
        BaseTax = Gross - Tax;
      }

      return;
    } // CalculateSplit2Tax

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate Split2 Tax. Needed in VoucherCashIn and DB_Recharge.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - Currency CardPrice
    //          - Currency CashInSplit2
    //
    //      - OUTPUT:
    //          - Currency TotalBaseTax
    //          - Currency TotalTax
    //
    // RETURNS: 
    // 
    public static void CalculateSplit2Tax(Currency CardPrice,
                                          Currency CashInSplit2,
                                          out Currency TotalBaseTax,
                                          out Currency TotalTax)
    {

      Currency _base_tax;
      Currency _tax;

      TotalBaseTax = 0;
      TotalTax = 0;

      CalculateSplit2Tax(CardPrice, out _base_tax, out _tax);

      TotalBaseTax += _base_tax;
      TotalTax += _tax;

      CalculateSplit2Tax(CashInSplit2, out _base_tax, out _tax);

      TotalBaseTax += _base_tax;
      TotalTax += _tax;

      return;
    } // CalculateSplit2Tax

    //------------------------------------------------------------------------------
    // PURPOSE: if card deposit is not already paid, calculate the amount to be paid.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - CardData
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Currency: The amount that user have to paid for the card.
    // 
    public static Currency CardDepositToPay(CardData CardData)
    {
      CardSettings CardSetting = new CardSettings();
      Currency _card_deposit_to_pay;

      _card_deposit_to_pay = 0;

      if (!CardData.CardPaid)
      {
        // ACC 16-FEB-2010 Future DepositAnonymous and DepositNonAnonymous Flag in general parameters
        CardData.DB_CardPriceSettings(ref CardSetting);

        if (CardData.PlayerTracking.HolderName == "")
        {
          // Anonym Card
          if (!CardData.IsVirtualCard)
          {
            _card_deposit_to_pay = CardSetting.AnonymCardPrice;
          }
        }
        else
        {
          // Personal Card
          _card_deposit_to_pay = CardSetting.PersonalCardPrice;
        }
      }

      return _card_deposit_to_pay;
    } // CardDepositToPay

    //------------------------------------------------------------------------------
    // PURPOSE: Not redeemable credit control.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - AccountId
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - GiftMsg
    //
    // RETURNS: 
    //      - Boolean: Is true if the redeemable can be applied.
    // 
    public static Boolean CheckAddNotRedeemable(Int64 AccountId, out Gift.GIFT_MSG GiftMsg)
    {
      Decimal _nr_balance;
      Decimal _nr_manual;
      Decimal _nr_gifts;
      Int32 _int_value;
      Boolean _gp_only_nr_is_zero;
      Boolean _gp_only_manual_nr_is_zero;

      _nr_balance = 0;
      _nr_manual = 0;
      _nr_gifts = 0;
      GiftMsg = Gift.GIFT_MSG.UNKNOWN;

      //  -Read General Params: OnlyWhenTotalNotRedeemableBalanceIsZero
      if (!Int32.TryParse(GeneralParam.Value("ManualPromotions", "OnlyWhenTotalNotRedeemableBalanceIsZero"), out _int_value))
      {
        _int_value = 0;
      }
      _gp_only_nr_is_zero = (_int_value == 1);

      //  -Read General Params: OnlyWhenManuallyAddedNotRedeemableBalanceIsZero           
      //                        Manual (Cashier & Kiosk)
      if (!Int32.TryParse(GeneralParam.Value("ManualPromotions", "OnlyWhenManuallyAddedNotRedeemableBalanceIsZero"), out _int_value))
      {
        _int_value = 1;
      }
      _gp_only_manual_nr_is_zero = (_int_value == 1);

      // Only need the NotRedeemableParts if any of the GP is true.
      if (_gp_only_nr_is_zero || _gp_only_manual_nr_is_zero)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Get NR , NR Manual, NR Gift Balance of current User Account
          if (!CardData.NotRedeemableParts(AccountId, out _nr_balance, out _nr_manual, out _nr_gifts, _db_trx.SqlTransaction))
          {
            //todo error
            GiftMsg = Gift.GIFT_MSG.CARD_ERROR;

            return false;
          }
        }

        //   -Only When Total Not Redeemable Balance Is Zero 
        if (_gp_only_nr_is_zero && _nr_balance != 0)
        {
          // The account has already non-redeemable credit
          GiftMsg = Gift.GIFT_MSG.CARD_HAS_NON_REDEEMABLE;

          return false;
        }

        //   -Only When Manually Added Not Redeemable Balance Is Zero
        if (_gp_only_manual_nr_is_zero && (_nr_gifts != 0 || _nr_manual != 0))
        {
          // The account has already non-redeemable credit
          GiftMsg = Gift.GIFT_MSG.CARD_HAS_NON_REDEEMABLE;

          return false;
        }
      }

      return true;
    }//CheckAddNotRedeemable


    //------------------------------------------------------------------------------
    // PURPOSE : It calls the DoRecharge method. After it, updates the status with ERROR or OK.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int64 RechargeId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean ProcessRecharge(CashierSessionInfo SessionInfo, Int64 AccountId, Currency AmountToAdd,
                                          Boolean ExternalLoyaltyProgram, Promotion.PROMOTION_TYPE PromotionType, String ElpDetails, SqlTransaction Trx,
                                          out ArrayList VoucherList, out Int64 OperationId, out Boolean PromoAsCashIn, out DataTable DtAccountPromotion)
    {
      AccountMovementsTable _ac_mov_table;
      CashierMovementsTable _cm_mov_table;
      AccountPromotion _promotion;
      Int64 _operation_id;
      CardData _card;
      Currency _amount_redeemable;
      Currency _amount_non_redeemable;
      Currency _money_as_cashin;
      OperationCode _operation_code;
      Boolean _is_tito_mode;
      String _error_msg;

      RechargeInputParameters _input_params;
      RechargeOutputParameters _output_params;

      _input_params = new RechargeInputParameters();

      _money_as_cashin = 0;

      _is_tito_mode = TITO.Utils.IsTitoMode();

      PromoAsCashIn = false;
      DtAccountPromotion = AccountPromotion.CreateAccountPromotionTable();

      _amount_redeemable = 0;
      _amount_non_redeemable = AmountToAdd;
      if (PromotionType == Promotion.PROMOTION_TYPE.EXTERNAL_REDEEM)
      {
        _amount_redeemable = AmountToAdd;
        _amount_non_redeemable = 0;

        if (GeneralParam.GetBoolean("Gifts.RedeemableCredits", "AsCashIn"))
        {
          _input_params.GenerateVouchers = !_is_tito_mode;
          _input_params.NoMoney = true;
          _money_as_cashin = AmountToAdd;
          PromoAsCashIn = true;
        }
      }

      VoucherList = new ArrayList();
      OperationId = 0;

      _card = new CardData();
      if (!CardData.DB_CardGetAllData(AccountId, _card, Trx))
      {
        return false;
      }

      _ac_mov_table = new AccountMovementsTable(SessionInfo);
      _cm_mov_table = new CashierMovementsTable(SessionInfo);

      if (_input_params.NoMoney)
      {
        _operation_code = OperationCode.GIFT_REDEEMABLE_AS_CASHIN;

        _promotion = new AccountPromotion();
        _promotion.id = 0;
      }
      else
      {
        _operation_code = OperationCode.PROMOTION;

        // Read the EXTERNAL promotion, used for WSP Recharge of NR amounts.
        if (!Promotion.ReadPromotionData(Trx, PromotionType, out _promotion))
        {
          return false;
        }
      }

      // Insert a PROMOTION operation.
      if (!Operations.DB_InsertOperation(_operation_code, _card.AccountId, SessionInfo.CashierSessionId, 0,
                                         _promotion.id, _amount_redeemable, _amount_non_redeemable, 0, 0, string.Empty,
                                         out _operation_id,
                                         out _error_msg,
                                         Trx))
      {
        return false;
      }

      if (_input_params.NoMoney)
      {
        // Do nothing
      }
      else
      {
        if (!AccountPromotion.AddPromotionToAccount(_operation_id, _card.AccountId, _card.PlayerTracking.CardLevel, 0, 0,
                                                  true, AmountToAdd, _promotion, DtAccountPromotion))
        {
          return false;
        }

        // Add External Loyalty Program Code
        if (ExternalLoyaltyProgram)
        {
          DtAccountPromotion.Rows[0]["ACP_DETAILS"] = ElpDetails;
        }
      }

      _input_params.AccountLevel = _card.PlayerTracking.CardLevel;
      _input_params.AccountId = _card.AccountId;
      _input_params.CardPaid = true;
      _input_params.CardPrice = 0;
      _input_params.ExternalTrackData = _card.TrackData;
      _input_params.VoucherAccountInfo = _card.VoucherAccountInfo();

      // Do the real recharge.
      if (!Accounts.DB_Recharge(_operation_id, _input_params, _money_as_cashin, DtAccountPromotion, _ac_mov_table, _cm_mov_table, Trx, out _output_params))
      {
        return false;
      }

      // Save accounts and cashier movements.
      if (!_ac_mov_table.Save(Trx))
      {
        return false;
      }

      if (!_cm_mov_table.Save(Trx))
      {
        return false;
      }

      // Output voucherList to print.
      VoucherList = _output_params.VoucherList;
      OperationId = _operation_id;

      return true;
    }
    public static Boolean ProcessRecharge(CashierSessionInfo SessionInfo, Int64 AccountId, Currency AmountToAdd,
                                          Boolean ExternalLoyaltyProgram, Promotion.PROMOTION_TYPE PromotionType, String ElpDetails, SqlTransaction Trx,
                                          out ArrayList VoucherList, out Int64 OperationId)
    {
      Boolean _as_cash_in;
      DataTable _dt_account_promo;

      return ProcessRecharge(SessionInfo, AccountId, AmountToAdd, ExternalLoyaltyProgram, PromotionType, ElpDetails, Trx,
                             out  VoucherList, out  OperationId, out _as_cash_in, out _dt_account_promo);
    } // ProcessRecharge

    public static OperationCode EvalOperationCode(Int64 promotion_id, SqlTransaction SqlTrx)
    {
      bool _general_tax_enabled = GeneralParam.GetBoolean("Cashier", "Promotions.RE.Tax.Enabled", true);

      if (_general_tax_enabled)
      {
        Promotion.TYPE_PROMOTION_DATA _promo_data = new Promotion.TYPE_PROMOTION_DATA();
        Promotion.ReadPromotionData(SqlTrx, promotion_id, _promo_data);

        if (_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE && _promo_data.apply_tax)
          return OperationCode.PROMOTION_WITH_TAXES;
      }

      return OperationCode.PROMOTION;
    } // EvalOperationCode

    /// <summary>
    /// Common routine of Recharge.
    /// </summary>
    /// <param name="OperationData"></param>
    /// <param name="Card"></param>
    /// <param name="ExchangeResult"></param>
    /// <param name="ParticipateCashDraw"></param>
    /// <param name="CashierSessionInformation"></param>
    /// <param name="DbTrx"></param>
    /// <param name="OperationId"></param>
    /// <param name="JunketInfo"></param>
    /// <param name="OutputParameter"></param>
    /// <param name="WarningAmountToPayZero">Indicates if amount = 0. In this case, Recharge is not executed </param>
    /// <param name="ErrorSalesLimitReachedText"></param>
    /// <param name="ErrorText"></param>
    /// <returns></returns>
    public static bool CommonRecharge(Object OperationData
                                     , CardData Card
                                     , CurrencyExchangeResult ExchangeResult
                                     , ParticipateInCashDeskDraw ParticipateCashDraw
                                     , CashierSessionInfo CashierSessionInformation
                                     , DB_TRX DbTrx
                                     , Int64 OperationId
                                     , JunketsBusinessLogic JunketInfo
                                     , out RechargeOutputParameters OutputParameter
                                     , out bool WarningAmountToPayZero
                                     , out string ErrorSalesLimitReachedText
                                     , out string ErrorText)
    {
      CashierOperations.TYPE_CARD_CASH_IN _data;
      OperationCode _operation_code;
      Currency _min_amount_recharge;

      OutputParameter = new RechargeOutputParameters();
      ErrorSalesLimitReachedText = "";
      ErrorText = "";
      WarningAmountToPayZero = false;

      // AJQ 24-SEP-2010, Promotions
      _data = (CashierOperations.TYPE_CARD_CASH_IN)OperationData;

      // Check if the amount to pay is greater than 0
      if (_data.amount_to_pay + _data.reward == 0)
      {
        WarningAmountToPayZero = true;

        return false;
      }

      _operation_code = GetOperationCode(_data, DbTrx.SqlTransaction);

      // DLL & RCI 28-OCT-2013: Moved CheckAntiMoneyLaundering() to frm_amount_input - btn_ok_Click() to add a permission to exceed report limit.
      if (!DB_CardCreditAdd(Card
                           , _data.amount_to_pay
                           , _data.promotion_id
                           , _data.reward
                           , _data.won_lock
                           , _data.spent_used
                           , ExchangeResult
                           , ParticipateCashDraw
                           , _operation_code
                           , OperationId
                           , CashierSessionInformation
                           , DbTrx.SqlTransaction
                           , JunketInfo
                           , out OutputParameter))
      {
        Log.Error(string.Format("btn_add_Click: DB_AddCreditToCard. Error adding credit to card. AccountId: {0}. Amount: {1}", Card.AccountId, _data.amount_to_pay.ToString()));


        String _holder_name;
        ErrorSalesLimitReachedText = "";
        ErrorText = "";

        // JML 12-NOV-2013 This error is move because now the function return false, when the credit allowed is reached
        if (OutputParameter.SalesLimitReached)
        {
          ErrorSalesLimitReachedText = Resource.String("STR_FRM_CASHIER_LIMIT_SALES_REACHED") + OutputParameter.RemainingSalesLimit;
          ErrorSalesLimitReachedText = ErrorSalesLimitReachedText.Replace("\\r\\n", "\r\n");
        }

        // JML 02-DIC-2014 Added errors to msgbox for failed recharges
        if (Card.PlayerTracking.CardLevel != 0)
        {
          _holder_name = Card.PlayerTracking.HolderName;
        }
        else
        {
          _holder_name = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ACCOUNT_MODE_ANONYMOUS");
        }

        switch (OutputParameter.ErrorCode)
        {
          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED:
            ErrorText = Resource.String("STR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED_ERROR", Card.AccountId, _holder_name, _data.amount_to_pay.ToString());

            break;
          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED:
            ErrorText = Resource.String("STR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED_ERROR", Card.AccountId, _holder_name, _data.amount_to_pay.ToString());

            break;
          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED:
            ErrorText = Resource.String("STR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED_ERROR", Card.AccountId, _holder_name, _data.amount_to_pay.ToString());

            break;
          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED:
            ErrorText = Resource.String("STR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED_ERROR", Card.AccountId, _holder_name, _data.amount_to_pay.ToString());

            break;
          case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT:
            _min_amount_recharge = GeneralParam.GetInt32("Account", "Level00.MinAllowedCashIn.Amount", 0, 0, Int32.MaxValue);
            ErrorText = Resource.String("STR_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT_ERROR", _min_amount_recharge.ToString(), Card.AccountId, _holder_name, _data.amount_to_pay.ToString());

            break;
          case RECHARGE_ERROR_CODE.ERROR_RECHARGE_WITH_WITHOLDING:
            _min_amount_recharge = GeneralParam.GetInt32("Account", "Level00.MinAllowedCashIn.Amount", 0, 0, Int32.MaxValue);
            ErrorText = Resource.String("STR_ACCOUNT_RECHARGE_WITH_PROMO_TAX_WITHOLDING_ERROR");

            break;
          default:
            // In case of exped's external provider, the generic errors are controlled at ExternalPaymentAndSaleValidationCommon
            if (ExternalPaymentAndSaleValidationCommon.ExpedMode == EnumExternalPaymentAndSaleValidationMode.MODE_EXPED)
            {
              ErrorText = OutputParameter.ErrorMessage;
            }
            else
            {
              if (!String.IsNullOrEmpty(OutputParameter.ErrorMessage))
              {
                ErrorText = OutputParameter.ErrorMessage + "\\r\\n" + "\\r\\n";
              }
              ErrorText += Resource.String("STR_UC_CARD_RECHARGE_ERROR", Card.AccountId, _holder_name, _data.amount_to_pay.ToString());
            }

            break;
        }

        ErrorText = ErrorText.Replace("\\r\\n", "\r\n");

        return false;
      }

      return true;
    } // CommonRecharge

    public static OperationCode GetOperationCode(CashierOperations.TYPE_CARD_CASH_IN Data, SqlTransaction SqlTrx)
    {
      if (Data.reward <= 0)
        return OperationCode.CASH_IN;

      bool _general_tax_enabled = GeneralParam.GetBoolean("Cashier", "Promotions.RE.Tax.Enabled", true);

      if (_general_tax_enabled)
      {
        Promotion.TYPE_PROMOTION_DATA _promo_data = new Promotion.TYPE_PROMOTION_DATA();
        Promotion.ReadPromotionData(SqlTrx, Data.promotion_id, _promo_data);

        if (_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE && _promo_data.apply_tax)
          return OperationCode.PROMOTION_WITH_TAXES;
      }

      return OperationCode.PROMOTION;
    } // OperationCode

    public static Boolean DB_CardCreditAdd(CardData Account,
                                           Currency RedeemableAmountToAdd,
                                           Int64 PromotionId,
                                           Currency NotRedeemableAmount,
                                           Currency NotRedeemableWonLock,
                                           Currency SpentUsed,
                                           CurrencyExchangeResult ExchangeResult,
                                           ParticipateInCashDeskDraw ParticipateInCashDraw,
                                           OperationCode OpCode,
                                           WithholdData WithholdData,
                                           CashierSessionInfo CashierSessionInformation,
                                           SqlTransaction SqlTrx,
                                           out RechargeOutputParameters OutputParams,
                                           ExternalValidationInputParameters ExpedInputParams = null,
                                           ExternalValidationOutputParameters ExpedOutputParams = null)
    {
      return DB_CardCreditAdd(Account, RedeemableAmountToAdd, PromotionId, NotRedeemableAmount, NotRedeemableWonLock,
                              SpentUsed, ExchangeResult, ParticipateInCashDraw, OpCode, false, 0, WithholdData, CashierSessionInformation, ExpedInputParams, ExpedOutputParams, SqlTrx, null, out OutputParams);
    }

    public static Boolean DB_CardCreditAdd(CardData Account,
                                       Currency RedeemableAmountToAdd,
                                       Int64 PromotionId,
                                       Currency NotRedeemableAmount,
                                       Currency NotRedeemableWonLock,
                                       Currency SpentUsed,
                                       CurrencyExchangeResult ExchangeResult,
                                       ParticipateInCashDeskDraw ParticipateInCashDraw,
                                       OperationCode OpCode,
                                       CashierSessionInfo CashierSessionInformation,
                                       SqlTransaction SqlTrx,
                                       out RechargeOutputParameters OutputParams)
    {
      return DB_CardCreditAdd(Account, RedeemableAmountToAdd, PromotionId, NotRedeemableAmount, NotRedeemableWonLock,
                              SpentUsed, ExchangeResult, ParticipateInCashDraw, OpCode, false, 0, null, CashierSessionInformation, SqlTrx, out OutputParams);
    }

    public static Boolean DB_CardCreditAdd(CardData Account,
                                           Currency RedeemableAmountToAdd,
                                           Int64 PromotionId,
                                           Currency NotRedeemableAmount,
                                           Currency NotRedeemableWonLock,
                                           Currency SpentUsed,
                                           CurrencyExchangeResult ExchangeResult,
                                           ParticipateInCashDeskDraw ParticipateInCashDraw,
                                           OperationCode OpCode,
                                           Boolean IsChipsSaleAtTableRegister,
                                           CashierSessionInfo CashierSessionInformation,
                                           SqlTransaction SqlTrx,
                                           out RechargeOutputParameters OutputParams)
    {
      // RCI 27-OCT-2015: Fixed Bug TFS-5618: Chips sale register broken. Must supply IsChipsSaleAtTableRegister parameter
      return DB_CardCreditAdd(Account, RedeemableAmountToAdd, PromotionId, NotRedeemableAmount, NotRedeemableWonLock,
                              SpentUsed, ExchangeResult, ParticipateInCashDraw, OpCode, IsChipsSaleAtTableRegister, 0, null, CashierSessionInformation, SqlTrx, out OutputParams);
    }

    public static Boolean DB_CardCreditAdd(CardData Account,
                                           Currency RedeemableAmountToAdd,
                                           Int64 PromotionId,
                                           Currency NotRedeemableAmount,
                                           Currency NotRedeemableWonLock,
                                           Currency SpentUsed,
                                           CurrencyExchangeResult ExchangeResult,
                                           ParticipateInCashDeskDraw ParticipateInCashDraw,
                                           OperationCode OpCode,
                                           Int64 OperationId,
                                           CashierSessionInfo CashierSessionInformation,
                                           SqlTransaction SqlTrx,
                                           out RechargeOutputParameters OutputParams)
    {
      return DB_CardCreditAdd(Account, RedeemableAmountToAdd, PromotionId, NotRedeemableAmount, NotRedeemableWonLock,
                              SpentUsed, ExchangeResult, ParticipateInCashDraw, OpCode, false, OperationId, null, CashierSessionInformation, SqlTrx, out OutputParams);
    }

    public static Boolean DB_CardCreditAdd(CardData Account,
                                       Currency RedeemableAmountToAdd,
                                       Int64 PromotionId,
                                       Currency NotRedeemableAmount,
                                       Currency NotRedeemableWonLock,
                                       Currency SpentUsed,
                                       CurrencyExchangeResult ExchangeResult,
                                       ParticipateInCashDeskDraw ParticipateInCashDraw,
                                       OperationCode OpCode,
                                       Boolean IsChipsSaleAtTableRegister,
                                       Int64 OperationId,
                                       WithholdData WithholdData,
                                       CashierSessionInfo CashierSessionInformation,
                                       SqlTransaction SqlTrx,
                                       out RechargeOutputParameters OutputParams)
    {
      return DB_CardCreditAdd(Account, RedeemableAmountToAdd, PromotionId, NotRedeemableAmount, NotRedeemableWonLock, SpentUsed,
                                        ExchangeResult, ParticipateInCashDraw, OpCode, IsChipsSaleAtTableRegister, OperationId, WithholdData, CashierSessionInformation,
                                        null, null, SqlTrx, null, out OutputParams);
    }

    public static Boolean DB_CardCreditAdd(CardData Account,
                                          Currency RedeemableAmountToAdd,
                                          Int64 PromotionId,
                                          Currency NotRedeemableAmount,
                                          Currency NotRedeemableWonLock,
                                          Currency SpentUsed,
                                          CurrencyExchangeResult ExchangeResult,
                                          ParticipateInCashDeskDraw ParticipateInCashDraw,
                                          OperationCode OpCode,
                                          Int64 OperationId,
                                          CashierSessionInfo CashierSessionInformation,
                                          SqlTransaction SqlTrx,
                                          JunketsBusinessLogic JunketInfo,
                                          out RechargeOutputParameters OutputParams)
    {
      return DB_CardCreditAdd(Account, RedeemableAmountToAdd, PromotionId, NotRedeemableAmount, NotRedeemableWonLock, SpentUsed,
                                        ExchangeResult, ParticipateInCashDraw, OpCode, false, OperationId, null, CashierSessionInformation,
                                        null, null, SqlTrx, JunketInfo, out OutputParams);
    }

    public static Boolean DB_CardCreditAdd(CardData Account,
                                      Currency RedeemableAmountToAdd,
                                      Int64 PromotionId,
                                      Currency NotRedeemableAmount,
                                      Currency NotRedeemableWonLock,
                                      Currency SpentUsed,
                                      CurrencyExchangeResult ExchangeResult,
                                      ParticipateInCashDeskDraw ParticipateInCashDraw,
                                      OperationCode OpCode,
                                      Boolean IsChipsSaleAtTableRegister,
                                      Int64 OperationId,
                                      WithholdData WithholdData,
                                      CashierSessionInfo CashierSessionInformation,
                                      SqlTransaction SqlTrx,
                                      JunketsBusinessLogic JunketInfo,
                                      out RechargeOutputParameters OutputParams)
    {
      return DB_CardCreditAdd(Account, RedeemableAmountToAdd, PromotionId, NotRedeemableAmount, NotRedeemableWonLock, SpentUsed,
                                        ExchangeResult, ParticipateInCashDraw, OpCode, IsChipsSaleAtTableRegister, OperationId, WithholdData, CashierSessionInformation,
                                        null, null, SqlTrx, JunketInfo, out OutputParams);
    }

    public static Boolean DB_CardCreditAdd(CardData Account,
                                       Currency RedeemableAmountToAdd,
                                       Int64 PromotionId,
                                       Currency NotRedeemableAmount,
                                       Currency NotRedeemableWonLock,
                                       Currency SpentUsed,
                                       CurrencyExchangeResult ExchangeResult,
                                       ParticipateInCashDeskDraw ParticipateInCashDraw,
                                       OperationCode OpCode,
                                       Boolean IsChipsSaleAtTableRegister,
                                       Int64 OperationId,
                                       WithholdData WithholdData,
                                       CashierSessionInfo CashierSessionInformation,
                                       ExternalValidationInputParameters ExpedInputParams,
                                       ExternalValidationOutputParameters ExpedOutputParams,
                                       SqlTransaction SqlTrx,
                                       JunketsBusinessLogic JunketInfo,
                                       out RechargeOutputParameters OutputParams)
    {
      Currency _nr2;
      Decimal _dec_nr2;
      Decimal _nr2_pct;
      CashierSessionInfo _cashier_session_info;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      RechargeInputParameters _input_params;
      Boolean _is_ok;
      Int64 _comission_to_b;
      long _sessionID;
      String _error_msg;

      Currency _investments_made_with_card_alarm_amount;
      String _alarm_description;
      Currency _card_amount;

      OutputParams = new RechargeOutputParameters();

      _comission_to_b = -1;

      try
      {
        if (PromotionId == 0 && (NotRedeemableAmount > 0 || NotRedeemableWonLock > 0))
        {
          return false;
        }

        if (NotRedeemableAmount <= 0)
        {
          PromotionId = 0;
        }

        _nr2 = 0;
        if (OpCode != OperationCode.CHIPS_SALE_WITH_RECHARGE && OpCode != OperationCode.CHIPS_SWAP)
        {
          if (!WSI.Common.Cashier.GetNR2(Account.AccountId, RedeemableAmountToAdd, out _dec_nr2, out _nr2_pct))
          {
            Log.Error("DB_CardCreditAdd: GetNR2 failed. AccountId: " + Account.AccountId + "AmountToAdd: " + RedeemableAmountToAdd.ToString());

            return false;
          }
          _nr2 = _dec_nr2;
        }

        if (Misc.IsCommissionToCompanyBEnabled() && ExchangeResult != null)
        {
          if (ExchangeResult.InType == CurrencyExchangeType.CARD)
          {
            _comission_to_b = (Int64)ExchangeResult.Comission * 100;
          }
        }

        _sessionID = CashierSessionInformation.CashierSessionId; //Cashier.SessionId;
        _cashier_session_info = CashierSessionInformation;// Cashier.CashierSessionInfo();

        if (OperationId == 0)
        {
          if (!Operations.DB_InsertOperation(OpCode,
                                             Account.AccountId,
                                             _sessionID,//Cashier.SessionId, 
                                             0,
                                             RedeemableAmountToAdd,
                                             PromotionId,
                                             NotRedeemableAmount,
                                             NotRedeemableWonLock,
                                             _nr2,
                                             _comission_to_b,                      // Operation Data
                                             IsChipsSaleAtTableRegister,
                                             0,
                                             string.Empty,
                                             String.Empty, //EOR 24-FEB-2016 ADD COMMENTS HANDPAY DEFAULT VALUE
                                             out OperationId,
                                             out _error_msg,
                                             SqlTrx,
                                             ExpedInputParams,
                                             ExpedOutputParams,
                                             Account.PlayerTracking.HolderName))
          {
            OutputParams.ErrorMessage = _error_msg;
            return false;
          }
        }

        _account_movements = new AccountMovementsTable(_cashier_session_info);
        _cashier_movements = new CashierMovementsTable(_cashier_session_info);

        _input_params = new RechargeInputParameters();
        _input_params.AccountId = Account.AccountId;
        _input_params.AccountLevel = Account.PlayerTracking.CardLevel;
        _input_params.CardPaid = Account.CardPaid;
        _input_params.CardPrice = Accounts.CardDepositToPay(Account);
        _input_params.ExternalTrackData = Account.TrackData;
        _input_params.VoucherAccountInfo = Account.VoucherAccountInfo();
        _input_params.ParticipateInCashDeskDraw = ParticipateInCashDraw;
        _input_params.IsChipsOperation = (OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE || OpCode == OperationCode.CHIPS_SWAP);
        _input_params.ChipSaleRegister = IsChipsSaleAtTableRegister;
        _input_params.WithholdData = WithholdData;

        // LTC 22-SEP-2016
        if (OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE)
          _input_params.CashDeskDraw = CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_01;
        else
          _input_params.CashDeskDraw = CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00;

        //JBC 01/10/2014: We need to save Holder Name into bank transaction database.
        if (ExchangeResult != null && ExchangeResult.BankTransactionData != null)
        {
          ExchangeResult.BankTransactionData.AccountHolderName = Account.PlayerTracking.HolderName;
        }

        //DPC 21-MAR-2017: Set credit line data for create voucher
        _input_params.CreditLineData = Account.CreditLineData;

        // JML 12-NOV-2013: The next three variables must be updated regardless of the result of the function.
        _is_ok = Accounts.DB_CardCreditAdd(_input_params, OperationId, _account_movements, _cashier_movements, RedeemableAmountToAdd,
                                           PromotionId, NotRedeemableAmount, NotRedeemableWonLock, SpentUsed, ExchangeResult,
                                           ParticipateInCashDraw, OpCode, SqlTrx, JunketInfo, out OutputParams);

        OutputParams.CashDeskDraw = _input_params.CashDeskDraw;

        if (_is_ok
            && ExchangeResult != null
            && (ExchangeResult.InType == CurrencyExchangeType.CARD || ExchangeResult.InType == CurrencyExchangeType.CARD2GO)
            && (OpCode == OperationCode.CHIPS_SALE || OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE)
            )
        {
          // Alarm: Alarm for Investments made with credit / debit card equal or over $XXXX.
          _investments_made_with_card_alarm_amount = GamingTableAlarms.GetInvestmentsMadeWithCardEqualOrOver();

          if (_investments_made_with_card_alarm_amount != 0)
          {
            _card_amount = ExchangeResult.GrossAmount;
            if (_card_amount >= _investments_made_with_card_alarm_amount)
            {
              // Insert Alarm
              _alarm_description = Resource.String("STR_ALARM_GAMBLING_TABLES_INVESTMENTS_MADE_WITH_CARD_EQUAL_OR_OVER", _investments_made_with_card_alarm_amount, _card_amount, Account.AccountId);

              Alarm.Register(AlarmSourceCode.Cashier,
                             CashierSessionInformation.UserId,
                             CashierSessionInformation.UserName + "@" + CashierSessionInformation.TerminalName,
                             (Int32)AlarmCode.User_GamnigTables_InvestmentsWithCreditDebitCard_EqualOrOver,
                             _alarm_description,
                             AlarmSeverity.Warning,
                             WGDB.Now, SqlTrx);
            }
          }
          // End Alarm: Alarm for Investments made with credit / debit card equal or over $XXXX.
        }

        return _is_ok;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_CardCreditAdd

  } // Accounts

} // WSI.Common