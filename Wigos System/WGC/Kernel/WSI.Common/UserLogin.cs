//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   UserLogin.cs
// DESCRIPTION:   Common login (WigosGUI and Cashier), check login
//                status.
// AUTHOR:        Raul Ruiz Barea
// CREATION DATE: 13-AUG-201
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 13-AUG-2012  RRB    Initial version
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

namespace WSI.Common
{
  public class UserLogin
  {
    #region ENUMS

    public enum LoginStatus
    {
      ERROR = 0,
      OK = 1,
      USER_NOT_EXISTS = 2,
      WRONG_PASSWORD = 3,
      WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED = 4,
      WRONG_PASSWORD_USER_IS_BLOCKED = 5,
      WRONG_PASSWORD_SUPER_USER = 6,
      USER_BLOCKED = 7,
      ILLEGAL_ACCESS = 8,
      SESION_OPENED = 9,
      NOT_PERMISSIONS = 10,
      OK_PASSWORD_EXPIRED = 11,
      OK_PASSWORD_CHANGE_REQ = 12
    } // LoginStatus

    public enum CommonForm
    {
      FORM_LOGIN_GUI = 10001,
      FORM_LOGIN_MOBI_BANK = 684,
      FORM_LOGIN_CASHIER = 0
    } // CommonForm

    public enum ChangePasswordStatus
    {
      ERROR = 0,
      OK = 1,
      WRONG_PASSWORD = 3,
      DB_ERROR = 4,
      PASSWORD_ERROR = 5
    } //ChangePasswordStatus

    public enum PasswordChange
    {
      PWD_NO_CHANGE = 0,
      PWD_CHANGE_REQUIRED = 1,
      PWD_CHANGE_REQUESTED = 2
    } //PasswordChange

    #endregion

    #region Constants

    public const Int32 MAX_FAILURES = 3;
    public const Int32 MIN_PASSWORD_LENGTH = 1;
    public const Int32 MAX_PASSWORD_LENGTH = 15;
    public const Int32 MIN_USERNAME_LENGTH = 1;
    public const Int32 MAX_USERNAME_LENGTH = 10;

    #endregion

    public class UserInfo
    {
      public Int32 user_id;
      public Int32 block_reason;
      public DateTime not_valid_before;
      public DateTime not_valid_after;
      public DateTime last_changed;
      public DateTime password_exp;
      public DateTime current_date;
      public Int32 password_exp_days;
      public Int32 login_failures;
      public Int32 profile_id;
      public Boolean psw_chg_req;
      public String user_name;
      public String full_name;
    }; // UserInfo

    // PURPOSE: Get information of the user from db
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserName:
    //           - UserInfo:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->User found False->User not found.
    public static Boolean GetUserInfoInDb(String UserName,
                                            UserInfo UserInfo,
                                            SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GU_USER_ID                    ");
        _sb.AppendLine("        , GU_BLOCK_REASON               ");
        _sb.AppendLine("        , ISNULL(GU_LOGIN_FAILURES, 0)  ");
        _sb.AppendLine("        , GU_NOT_VALID_BEFORE           ");
        _sb.AppendLine("        , GU_NOT_VALID_AFTER            ");
        _sb.AppendLine("        , GU_LAST_CHANGED               ");
        _sb.AppendLine("        , GU_PASSWORD_EXP               ");
        _sb.AppendLine("        , GU_PROFILE_ID                 ");
        _sb.AppendLine("        , GU_PWD_CHG_REQ                ");
        _sb.AppendLine("        , GU_USERNAME                   ");
        _sb.AppendLine("        , GU_FULL_NAME                  ");
        _sb.AppendLine("   FROM   GUI_USERS                     ");
        _sb.AppendLine("  WHERE   GU_USERNAME = @pUserName      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar, 50).Value = UserName;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return false;
            }

            UserInfo.user_id = _sql_reader.GetInt32(0);
            UserInfo.block_reason = _sql_reader.GetInt32(1);
            UserInfo.login_failures = _sql_reader.GetInt32(2);
            UserInfo.not_valid_before = _sql_reader.GetDateTime(3);
            UserInfo.not_valid_after = _sql_reader.IsDBNull(4) ? WGDB.Now.AddDays(1) : _sql_reader.GetDateTime(4);
            UserInfo.last_changed = _sql_reader.IsDBNull(5) ? WGDB.Now : _sql_reader.GetDateTime(5);

            if (_sql_reader.IsDBNull(6))
            {
              UserInfo.password_exp = WGDB.Now;
              UserInfo.password_exp_days = 0;
            }
            else
            {
              UserInfo.password_exp = _sql_reader.GetDateTime(6);
              UserInfo.password_exp_days = UserInfo.password_exp.Subtract(UserInfo.not_valid_before).Days;
            }

            UserInfo.profile_id = _sql_reader.GetInt32(7);
            UserInfo.current_date = WGDB.Now;
            UserInfo.psw_chg_req = _sql_reader.GetBoolean(8);
            UserInfo.user_name = _sql_reader.GetString(9);
            UserInfo.full_name = _sql_reader.IsDBNull(10) ? String.Empty : _sql_reader.GetString(10);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // GetUserInfoInDb

    // PURPOSE: Check if the password is correct
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountPwd:
    //           - Password:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->Password OK False->Wrong pwd.
    public static Boolean CheckPassword(String Password,
                                        String AccountPwd)
    {
      return (AccountPwd.ToUpper() == Password.ToUpper());
    } // CheckPassword

    // PURPOSE: Disable user and reset login failures
    //          when login_failures > MAX_FAILURES
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - None
    public static void DisableUser(Int32 UserId,
                                   SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   GUI_USERS ");
        _sb.AppendLine("    SET   GU_BLOCK_REASON   = GU_BLOCK_REASON | " + (Int32)GUI_USER_BLOCK_REASON.WRONG_PASSWORD);
        _sb.AppendLine("        , GU_LOGIN_FAILURES = 0 ");
        _sb.AppendLine("  WHERE   GU_USER_ID = @pUserId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          _sql_cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // DisableUser

    // PURPOSE: Reset login failures
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - None
    public static void ResetLoginFailures(Int32 UserId,
                                          SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   GUI_USERS ");
        _sb.AppendLine("    SET   GU_LOGIN_FAILURES = 0 ");
        _sb.AppendLine("  WHERE   GU_USER_ID        = @pUserId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          _sql_cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // ResetLoginFailures

    // PURPOSE: Increase login failures
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - None
    public static void IncreaseLoginFailures(Int32 UserId,
                                             SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   GUI_USERS ");
        _sb.AppendLine("    SET   GU_LOGIN_FAILURES = ISNULL(GU_LOGIN_FAILURES, 0) + 1 ");
        _sb.AppendLine("  WHERE   GU_USER_ID        = @pUserId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          _sql_cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // IncreaseLoginFailures

    // PURPOSE: Check about illegal access
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserInfo:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->OK False->Illegal access.
    public static Boolean CheckIllegalAccess(UserInfo UserInfo)
    {
      TimeSpan _diff_time;
      Double _total_days;

      _diff_time = WGDB.Now.Subtract(UserInfo.not_valid_before);
      _total_days = _diff_time.TotalDays;
      if (_total_days < 0.0)
      {
        return false;
      }

      _diff_time = UserInfo.not_valid_after.Subtract(WGDB.Now);
      _total_days = _diff_time.TotalDays;
      if (_total_days < 0.0)
      {
        return false;
      }

      _diff_time = WGDB.Now.Subtract(UserInfo.last_changed);
      _total_days = _diff_time.TotalDays;
      if (UserInfo.password_exp_days > 0 && _total_days >= UserInfo.password_exp_days)
      {
        return false;
      }

      return true;
    } // CheckIllegalAccess

    // PURPOSE: Check session status, login_mode = REGULAR
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->OK False->Session opened.
    public static Boolean CheckSession(Int32 UserId,
                                       ENUM_GUI GuiId)
    {
      Users.USER_SESSION_STATE _session_state;
      String _message;
      String _title;

      _message = "";
      _title = "";

      if (GuiId == ENUM_GUI.CASHIER)
      {
        _session_state = Users.GetStateUserSession("CASHIER", UserId, Environment.MachineName, out _message, out _title);
      }
      else if (GuiId == ENUM_GUI.WIGOS_GUI || GuiId == ENUM_GUI.MOBIBANK)
      {
        _session_state = Users.GetStateUserSession("WigosGUI", UserId, Environment.MachineName, out _message, out _title);
      }
      else
      {
        _session_state = Users.USER_SESSION_STATE.OPENED;
      }

      if (_session_state == Users.USER_SESSION_STATE.OPENED)
      {
        if (UserId != 0)
        {
          return false;
        }
      }
      //else if (_session_state == Users.USER_SESSION_STATE.EXPIRED || _session_state == Users.USER_SESSION_STATE.UNEXPECTED)
      //{
      //frm_message.Show(_message, _title,
      //                 MessageBoxButtons.OK,
      //                 MessageBoxIcon.Warning,
      //                 this.ParentForm);
      //}

      return true;
    } // CheckSession

    // PURPOSE: Check permissions, login_mode = REGULAR
    //
    //  PARAMS:
    //     - INPUT:
    //           - UserId:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->OK False->Don't have perms.
    public static Boolean CheckPermissions(Int32 UserId,
                                             Int32 ProfileId,
                                             ENUM_GUI GuiId,
                                             SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;
        Object _obj;
        Boolean _read_perm;

        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   GPF_READ_PERM ");
        //_sb.AppendLine("        , GPF_WRITE_PERM ");
        //_sb.AppendLine("        , GPF_DELETE_PERM ");
        //_sb.AppendLine("        , GPF_EXECUTE_PERM ");
        _sb.AppendLine("   FROM   GUI_PROFILE_FORMS ");
        _sb.AppendLine("  WHERE   GPF_PROFILE_ID = @pProfileId ");
        _sb.AppendLine("    AND   GPF_GUI_ID     = @pGuiId ");
        _sb.AppendLine("    AND   GPF_FORM_ID    = @pFormId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProfileId", SqlDbType.Int).Value = ProfileId;

          if (GuiId != ENUM_GUI.MOBIBANK)
          _sql_cmd.Parameters.Add("@pGuiId", SqlDbType.Int).Value = GuiId;

          if (GuiId == ENUM_GUI.CASHIER)
          {
            _sql_cmd.Parameters.Add("@pFormId", SqlDbType.Int).Value = CommonForm.FORM_LOGIN_CASHIER;
          }
          else if (GuiId == ENUM_GUI.WIGOS_GUI)
          {
            _sql_cmd.Parameters.Add("@pFormId", SqlDbType.Int).Value = CommonForm.FORM_LOGIN_GUI;
          }
          else if (GuiId == ENUM_GUI.MOBIBANK)
          {
            _sql_cmd.Parameters.Add("@pGuiId", SqlDbType.Int).Value = ENUM_GUI.WIGOS_GUI;
            _sql_cmd.Parameters.Add("@pFormId", SqlDbType.Int).Value = CommonForm.FORM_LOGIN_MOBI_BANK;
          }
          else
          {
            if (UserId != 0)
            {
              return false;
            }
          }

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj == null)
          {
            if (UserId != 0)
            {
              return false;
            }
          }
          else
          {
            _read_perm = (Boolean)_obj;
            if (!_read_perm)
            {
              if (UserId != 0)
              {
                return false;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // CheckPermission

    // PURPOSE: Check if the number of days for password expiration
    //          has been reached.
    //          
    //    - INPUT: UserInfo
    //              
    //    - OUTPUT: None
    //              
    // RETURNS:
    //              TRUE: password has expired
    //              FALSE: password has NOT expired
    // NOTES
    //     We obtain the current date from the server in the format
    //     'month  dd, yyyy' because it is independent of the local computer
    //     format that could not be understanded (dd/mm/yyyy) or (mm/dd/yyyy)
    //     The visualbasic understands english and spanish name of the month.
    //
    public static Boolean CheckPasswordExpiration(UserInfo UserInfo)
    {
      System.TimeSpan date_diff;
      Int32 num_of_days;
      DateTime center_date_only;

      if (UserInfo.user_id != 0)
      {
        //Subtracts today minus last changed and get the number of days
        center_date_only = UserInfo.current_date.Date;
        date_diff = center_date_only.Subtract(UserInfo.last_changed);
        num_of_days = date_diff.Days;

        // check if limit has been reached
        if ((UserInfo.password_exp_days > 0) && (num_of_days >= UserInfo.password_exp_days))
        {
          //ask the user to change the password
          return true;
        }
      }
      else
      {
        // password has NOT expired
        return false;
      }

      // password has NOT expired
      return false;
    } //CheckPasswordExpiration

    // PURPOSE: Gets and checks the parameters entered by the user in the Login Form. 
    //	      Check if the user exists, check if password is OK, check if the user is blocked.
    //	      Check illegal access, sessions, permissions and finally if pwd not expired.
    //
    //  PARAMS:
    //      - INPUT:
    //          - UserName
    //          - Password
    //          - GuiId
    //
    //      - OUTPUT:
    //          - AccountId
    //
    // RETURNS:
    //      - LoginStatus
    // 
    //   NOTES:
    //     
    public static LoginStatus LoginUser(String UserName,
                                         String Password,
                                         ENUM_GUI GuiId,
                                         out Int32 AccountId)
    {
      UserInfo _user_info;
      LoginStatus _status;
      Int32 _nls_id;
      Int32 _audit_code;
      String _description;
      AlarmCode _alarm_code;

      _user_info = new UserInfo();
      _status = LoginStatus.ERROR;
      _alarm_code = AlarmCode.Unknown;
      _description = "";

      _audit_code = 4;
      _nls_id = 5000;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!GetUserInfoInDb(UserName, _user_info, _db_trx.SqlTransaction))
          {
            _status = LoginStatus.USER_NOT_EXISTS;
          }
          else
          {
            PasswordPolicy _pol;
            _pol = new PasswordPolicy();

            if (!_pol.VerifyCurrentPassword(_user_info.user_id, Password, _db_trx.SqlTransaction))
            {
              if (_user_info.user_id == 0)
              {
                _status = LoginStatus.WRONG_PASSWORD_SUPER_USER;
              }
              else
              {
                if (_user_info.block_reason == 0)
                {
                  _user_info.login_failures = _user_info.login_failures + 1;
                  if ((_user_info.login_failures >= MAX_FAILURES) && (_user_info.login_failures % MAX_FAILURES == 0))
                  {
                    DisableUser(_user_info.user_id, _db_trx.SqlTransaction);
                    _status = LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED;
                  }
                  else
                  {
                    IncreaseLoginFailures(_user_info.user_id, _db_trx.SqlTransaction);
                    _status = LoginStatus.WRONG_PASSWORD;
                  }
                }
                else
                {
                  _status = LoginStatus.WRONG_PASSWORD_USER_IS_BLOCKED;
                }
              }
            }
            else if (_user_info.block_reason > 0)
            {
              _status = LoginStatus.USER_BLOCKED;
            }
            else if (!CheckIllegalAccess(_user_info))
            {
              _status = LoginStatus.ILLEGAL_ACCESS;
            }
            else if (!CheckSession(_user_info.user_id, GuiId))
            {
              _status = LoginStatus.SESION_OPENED;
            }
            else if (!CheckPermissions(_user_info.user_id, _user_info.profile_id, GuiId, _db_trx.SqlTransaction))
            {
              _status = LoginStatus.NOT_PERMISSIONS;
            }
            else
            {
              ResetLoginFailures(_user_info.user_id, _db_trx.SqlTransaction);

              if (_user_info.psw_chg_req)
              {
                _status = LoginStatus.OK_PASSWORD_CHANGE_REQ;
              }
              else
              {
                if (!CheckPasswordExpiration(_user_info))
                {
                  _status = LoginStatus.OK;
                }
                else
                {
                  _status = LoginStatus.OK_PASSWORD_EXPIRED;
                }
              }
            }
          }

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        AccountId = _user_info.user_id;

        switch (_status)
        {
          case LoginStatus.ERROR:
            break;
          case LoginStatus.OK:
            break;
          case LoginStatus.USER_NOT_EXISTS:
            AccountId = -1;
            break;
          case LoginStatus.WRONG_PASSWORD:
            break;
          case LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED:
            _nls_id += 106;
            _alarm_code = AlarmCode.User_MaxLoginAttemps;
            break;
          case LoginStatus.WRONG_PASSWORD_USER_IS_BLOCKED:
            break;
          case LoginStatus.WRONG_PASSWORD_SUPER_USER:
            break;
          case LoginStatus.USER_BLOCKED:
            break;
          case LoginStatus.ILLEGAL_ACCESS:
            break;
          case LoginStatus.SESION_OPENED:
            break;
          case LoginStatus.NOT_PERMISSIONS:
            break;
          case LoginStatus.OK_PASSWORD_EXPIRED:
            break;
          case LoginStatus.OK_PASSWORD_CHANGE_REQ:
            break;
          default:
            break;
        }
      }

      if (GuiId != ENUM_GUI.MOBIBANK)
      {
        // insert audit
        Auditor.Audit(GuiId,                     // GuiId
                       AccountId,                 // AccountId
                       UserName,                  // UserName
                       Environment.MachineName,   // MachineName
                       _audit_code,               // AuditCode
                       _nls_id,                   // NlsId (GUI)
                       "",                        // NlsParam01
                       "",                        // NlsParam02
                       "",                        // NlsParam03
                       "",                        // NlsParam04
                       "");                      // NlsParam05

      }


      // insert alarm
      if (_status == LoginStatus.WRONG_PASSWORD_SUPER_USER
          || _status == LoginStatus.NOT_PERMISSIONS
          || _status == LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED
          || _status == LoginStatus.ILLEGAL_ACCESS
          || _status == LoginStatus.SESION_OPENED
          || _status == LoginStatus.NOT_PERMISSIONS)
      {
        Alarm.Register(AlarmSourceCode.User,                      // SourceCode
                        (Int64)GuiId,                              // SourceId
                        UserName + "@" + Environment.MachineName,  // SourceName
                        _alarm_code,                               // Code
                        _description);                            // Description
      }

      return _status;
    } // LoginUser

    /* Cashier Change Password routines */

    // PURPOSE: Update user's password.
    //   
    //    - INPUT:
    //       - UserId: Internal user identifier.
    //       - Password: Password to be checked.
    //       - SqlTrans: Database context.
    //
    //    - OUTPUT:
    //
    // RETURNS:
    //   - DB_ERROR
    //   - OK
    //
    // NOTES:
    public static ChangePasswordStatus ChangeUserPassword(Int32 UserId,
                                                          String Password,
                                                          SqlTransaction Trx)
    {
      PasswordPolicy _pwd;

      _pwd = new PasswordPolicy();

      if (_pwd.ChangeUserPassword(UserId, Password, Trx))
      {
        return ChangePasswordStatus.OK;
      }

      return ChangePasswordStatus.DB_ERROR;
    } // ChangeUserPassword

    // PURPOSE: Check if the user exists, check if password is OK, check if the user is blocked.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - GuiId
    //
    // RETURNS:
    //      - IsValidUser
    // 
    //   NOTES:
    //     
    public static LoginStatus IsValidUser(int AccountId,
                                          ENUM_GUI GuiId)
    {
      UserInfo _user_info;
      LoginStatus _status;
      Int32 _nls_id;
      Int32 _audit_code;
      String _description;
      AlarmCode _alarm_code;

      _user_info = new UserInfo();
      _status = LoginStatus.ERROR;
      _alarm_code = AlarmCode.Unknown;
      _description = "";

      _audit_code = 4;
      _nls_id = 5000;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!GetUserIfExistInDb(AccountId, _user_info, _db_trx.SqlTransaction))
          {
            _status = LoginStatus.USER_NOT_EXISTS;
          }
          else
          {
            if (_user_info.block_reason > 0)
            {
              _status = LoginStatus.USER_BLOCKED;
            }
            else if (!CheckIllegalAccess(_user_info))
            {
              _status = LoginStatus.ILLEGAL_ACCESS;
            }
            //else if (!CheckSession(_user_info.user_id, GuiId))
            //{
            //  _status = LoginStatus.SESION_OPENED;
            //}
            else if (!CheckPermissions(_user_info.user_id, _user_info.profile_id, GuiId, _db_trx.SqlTransaction))
            {
              _status = LoginStatus.NOT_PERMISSIONS;
            }
            else
            {
              if (_user_info.psw_chg_req)
              {
                _status = LoginStatus.OK_PASSWORD_CHANGE_REQ;
              }
              else
              {
                if (!CheckPasswordExpiration(_user_info))
                {
                  _status = LoginStatus.OK;
                }
                else
                {
                  _status = LoginStatus.OK_PASSWORD_EXPIRED;
                }
              }
            }
          }

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        //AccountId = _user_info.user_id;

        switch (_status)
        {
          case LoginStatus.ERROR:
            break;
          case LoginStatus.OK:
            break;
          case LoginStatus.USER_NOT_EXISTS:
            AccountId = -1;
            break;
          case LoginStatus.WRONG_PASSWORD:
            break;
          case LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED:
            _nls_id += 106;
            _alarm_code = AlarmCode.User_MaxLoginAttemps;
            break;
          case LoginStatus.WRONG_PASSWORD_USER_IS_BLOCKED:
            break;
          case LoginStatus.WRONG_PASSWORD_SUPER_USER:
            break;
          case LoginStatus.USER_BLOCKED:
            break;
          case LoginStatus.ILLEGAL_ACCESS:
            break;
          case LoginStatus.SESION_OPENED:
            break;
          case LoginStatus.NOT_PERMISSIONS:
            break;
          case LoginStatus.OK_PASSWORD_EXPIRED:
            break;
          case LoginStatus.OK_PASSWORD_CHANGE_REQ:
            break;
          default:
            break;
        }
      }

      // insert audit
      Auditor.Audit(GuiId,                     // GuiId
                     AccountId,                 // AccountId
                     _user_info.user_name,                  // UserName
                     Environment.MachineName,   // MachineName
                     _audit_code,               // AuditCode
                     _nls_id,                   // NlsId (GUI)
                     "",                        // NlsParam01
                     "",                        // NlsParam02
                     "",                        // NlsParam03
                     "",                        // NlsParam04
                     "");                      // NlsParam05

      // insert alarm
      if (_status == LoginStatus.WRONG_PASSWORD_SUPER_USER
          || _status == LoginStatus.NOT_PERMISSIONS
          || _status == LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED
          || _status == LoginStatus.ILLEGAL_ACCESS
          || _status == LoginStatus.SESION_OPENED
          || _status == LoginStatus.NOT_PERMISSIONS)
      {
        Alarm.Register(AlarmSourceCode.User,                      // SourceCode
                        (Int64)GuiId,                              // SourceId
                        _user_info.user_name + "@" + Environment.MachineName,  // SourceName
                        _alarm_code,                               // Code
                        _description);                            // Description
      }

      return _status;
    } // IsValidUser

    // PURPOSE: Get information of the user from db if exist
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - UserInfo:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->User found False->User not found.
    public static Boolean GetUserIfExistInDb(int AccountId,
                                             UserInfo UserInfo,
                                             SqlTransaction SqlTrx)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GU_USER_ID                    ");
        _sb.AppendLine("        , GU_BLOCK_REASON               ");
        _sb.AppendLine("        , ISNULL(GU_LOGIN_FAILURES, 0)  ");
        _sb.AppendLine("        , GU_NOT_VALID_BEFORE           ");
        _sb.AppendLine("        , GU_NOT_VALID_AFTER            ");
        _sb.AppendLine("        , GU_LAST_CHANGED               ");
        _sb.AppendLine("        , GU_PASSWORD_EXP               ");
        _sb.AppendLine("        , GU_PROFILE_ID                 ");
        _sb.AppendLine("        , GU_PWD_CHG_REQ                ");
        _sb.AppendLine("        , GU_USERNAME                   ");
        _sb.AppendLine("        , GU_FULL_NAME                  ");
        _sb.AppendLine("   FROM   GUI_USERS                     ");
        _sb.AppendLine("  WHERE   GU_USER_ID = @pAccountId      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.Int).Value = AccountId;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return false;
            }

            UserInfo.user_id = _sql_reader.GetInt32(0);
            UserInfo.block_reason = _sql_reader.GetInt32(1);
            UserInfo.login_failures = _sql_reader.GetInt32(2);
            UserInfo.not_valid_before = _sql_reader.GetDateTime(3);
            UserInfo.not_valid_after = _sql_reader.IsDBNull(4) ? WGDB.Now.AddDays(1) : _sql_reader.GetDateTime(4);
            UserInfo.last_changed = _sql_reader.IsDBNull(5) ? WGDB.Now : _sql_reader.GetDateTime(5);

            if (_sql_reader.IsDBNull(6))
            {
              UserInfo.password_exp = WGDB.Now;
              UserInfo.password_exp_days = 0;
            }
            else
            {
              UserInfo.password_exp = _sql_reader.GetDateTime(6);
              UserInfo.password_exp_days = UserInfo.password_exp.Subtract(UserInfo.not_valid_before).Days;
            }

            UserInfo.profile_id = _sql_reader.GetInt32(7);
            UserInfo.current_date = WGDB.Now;
            UserInfo.psw_chg_req = _sql_reader.GetBoolean(8);
            UserInfo.user_name = _sql_reader.GetString(9);
            UserInfo.full_name = _sql_reader.IsDBNull(10) ? String.Empty : _sql_reader.GetString(10);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      return true;

  } // UserLogin

    // PURPOSE: Get information of the user from db if exist
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - UserInfo:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: True->User found False->User not found.
    public static Boolean GetUserIfExistInDbByMobileBankId(long BankId,
                                             UserInfo UserInfo)
    {
      try
      {
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   GU_USER_ID, GU_USERNAME, GU_FULL_NAME, MB_ACCOUNT_TYPE, MB_HOLDER_NAME                    ");
        _sb.AppendLine("   FROM   GUI_USERS                     ");
        _sb.AppendLine(" INNER JOIN   MOBILE_BANKS MB ON  MB_USER_ID  =  GU_USER_ID ");
        _sb.AppendLine("  WHERE   MB.MB_ACCOUNT_ID = @pBankId      ");


        using (DB_TRX _db_trx = new DB_TRX())
        {

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pBankId", SqlDbType.BigInt).Value = BankId;

            using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
            {
              if (!_sql_reader.Read())
              {
                return false;
              }

              UserInfo.user_id = _sql_reader.GetInt32(0);
              UserInfo.user_name = _sql_reader.GetString(1);
              UserInfo.full_name = _sql_reader.IsDBNull(4) ? String.Empty : _sql_reader.GetString(4);

            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      return true;

    } // UserLogin


  }
}
    