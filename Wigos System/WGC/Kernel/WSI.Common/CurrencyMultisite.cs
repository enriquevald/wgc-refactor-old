//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CurrencyMultisite.cs
// 
//   DESCRIPTION: Class to manage currencies in multisite
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 23-APR-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2015 FOS    First release.
// 07-MAY-2015 FOS    Insert thread to calculate currencies average
// 24-JAN-2017 RAB    Bug 15736: MultiSite GUI - Play sessions: Results aren't displayed if the date range is from one day.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

namespace WSI.Common
{
  public class CurrencyMultisite
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Has Multicurrency active
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsEnabledMultiCurrency()
    {
      return GeneralParam.GetBoolean("MultiSite.Multicurrency", "Enabled", false);
    } // IsEnabledMultiCurrency

    //------------------------------------------------------------------------------
    // PURPOSE : Is center whit Multicurrency active
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsCenterAndMultiCurrency()
    {
      return GeneralParam.GetBoolean("MultiSite", "IsCenter", false) && IsEnabledMultiCurrency();
    } // IsCenterAndMultiCurrency

    // PURPOSE: Get main currency MultiSite-MultiCurrency 
    //
    // PARAMS:
    //     - INPUT:
    //        - None
    //     - OUTPUT:
    //        - None
    //
    // RETURNS:
    //     - String with the ISO code of the main currency
    //
    public static String GetMainCurrency()
    {
      return GeneralParam.GetString("MultiSite.Multicurrency", "MainCurrency", string.Empty);
    } // GetMainCurrency

    // PURPOSE: Get MultiSite-Display Mode currencies
    //
    // PARAMS:
    //     - INPUT:
    //        - None
    //     - OUTPUT:
    //        - None
    //
    // RETURNS:
    //     - Mode to display currencies
    //
    public static Boolean GetDisplayMode()
    {
      return GeneralParam.GetBoolean("MultiSite.Multicurrency", "DisplayMode", false);
    } // GetDisplayMode

    // PURPOSE: Get Sites currencies average
    //
    // PARAMS:
    //     - INPUT:
    //
    //     - OUTPUT:
    //        - DtSitesCurrencies
    //
    // RETURNS:
    //     - Boolean
    //
    public static Boolean GetAllFilteredSitesCurrencies(DateTime DateFrom, DateTime DateTo, out DataTable DtSitesCurrencies)
    {
      StringBuilder _sb;
      DtSitesCurrencies = new DataTable();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   ER_CHANGE ");
        _sb.AppendLine("           ,ER_ISO_CODE ");
        _sb.AppendLine("           ,ER_WORKING_DAY ");
        _sb.AppendLine("     FROM   SITE_CURRENCIES ");
        _sb.AppendLine("LEFT JOIN   EXCHANGE_RATES  ");
        _sb.AppendLine("       ON   SC_ISO_CODE = ER_ISO_CODE");

        if (DateFrom.Date.Equals(DateTo.Date))
        {
          _sb.AppendLine("  WHERE   ER_WORKING_DAY = @pDateFrom ");
        }
        else
        {
          _sb.AppendLine("  WHERE   ER_WORKING_DAY >= @pDateFrom ");
          _sb.AppendLine("    AND   ER_WORKING_DAY < @pDateTo ");
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (DateFrom.Date.Equals(DateTo.Date))
            {
              //RAB 24-JAN-2017: It adds .date to dates in the same range of day
              _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom.Date;
            }
            else
            {
              _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom;
              _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateTo;
            }

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(DtSitesCurrencies);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GetAllFilteredSitesCurrencies 

    // PURPOSE: Get Sites currencies enabled in Multisite
    //
    // PARAMS:
    //     - INPUT:
    //
    //     - OUTPUT:
    //        - DtSitesCurrencies
    //
    // RETURNS:
    //     - Boolean
    //
    public static Boolean GetSiteCurrencies(out DataTable DtSiteCurrency)
    {
      StringBuilder _sb;
      DtSiteCurrency = new DataTable();

      try
      {
        _sb = new StringBuilder();


        _sb.AppendLine("   SELECT DISTINCT    SC_ISO_CODE ");
        _sb.AppendLine("                     ,SC_SITE_ID ");
        _sb.AppendLine("                     ,0 ");
        _sb.AppendLine("              FROM    SITE_CURRENCIES ");
        _sb.AppendLine("             WHERE    SC_TYPE = 1");
        _sb.AppendLine("          ORDER BY    SC_SITE_ID ");
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(DtSiteCurrency);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GetSiteCurrencies 
  }

  public static class CurrencyMultisiteThread
  {
    private static Thread m_thread;
    // Queue for threads requests
    private static WaitableQueue m_queue_currency_multisite;
    // Date ranges
    private static DateTime m_date_from;
    private static DateTime m_date_to;

    private static Boolean m_init = Init();

    public const Int32 READ_TIME = 5 * 1000;
    public delegate Boolean CurrencyMultiSiteChangedHandler(DataTable dt_currencies);     // create delegate


    // PURPOSE: Init new thread to calculate Average currency
    //
    // PARAMS:
    //     - INPUT:
    //        - FromDate -> DateTime 
    //        - ToDate -> DateTime 
    //     - OUTPUT:
    //        - None
    //
    // RETURNS:
    //     - None
    //
    private static Boolean Init()
    {
      //if (!Misc.IsCenterAndMultiCurrency())
      //{
      //  return false;
      //}

      m_queue_currency_multisite = new WaitableQueue();

      m_date_from = DateTime.MinValue;
      m_date_to = DateTime.MaxValue;

      m_thread = new Thread(MultiCurrency_ExchangeRatesThread);
      m_thread.Name = "MultiCurrency_ExchangeRatesThread";
      m_thread.Start();

      return true;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Process enqueue currency multisite
    //
    //  PARAMS:
    //     - INPUT:
    //        - DateTime: DateFrom
    //        - DateTime: DateTo
    //        - Object: Control/UserControl
    //        - CurrencyMultiSiteChangedHandler: Event
    //
    //     - OUTPUT:
    //
    // RETURNS:
    private static void Currency_Multisite_Enqueue(DateTime DateFrom, DateTime DateTo, Object Control, CurrencyMultiSiteChangedHandler Event_RedrawCurrenciesRate)
    {
      CurrencyMultisiteThreadData _currency_multisite_data;
      WaitableQueue _queue;

      // Create request structure data
      _currency_multisite_data = new CurrencyMultisiteThreadData(DateFrom, DateTo, Control, Event_RedrawCurrenciesRate);

      _queue = GetQueue();

      // Enqueue request
      _queue.Enqueue(_currency_multisite_data);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get queue currency multisite
    //
    //  PARAMS:
    //     - INPUT:
    //
    //     - OUTPUT:
    //        - WaitableQueue: Queue
    //
    // RETURNS:
    private static WaitableQueue GetQueue()
    {
      return m_queue_currency_multisite;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Enqueue new peticion
    //
    //  PARAMS:
    //     - INPUT:
    //        - DateTime: DateFrom
    //        - DateTime: DateTo
    //        - Object: Control/UserControl
    //        - CurrencyMultiSiteChangedHandler: Event
    //
    //     - OUTPUT:
    //
    // RETURNS:
    public static void RefreshData(DateTime DateFrom, DateTime DateTo, Object Control, CurrencyMultiSiteChangedHandler Event_RedrawCurrenciesRate)
    {
      Currency_Multisite_Enqueue(DateFrom, DateTo, Control, Event_RedrawCurrenciesRate);
    }

    // PURPOSE: Multi currency exchange Thread
    //
    // PARAMS:
    //     - INPUT:
    //        - None
    //     - OUTPUT:
    //        - None
    //
    // RETURNS:
    //
    public static void MultiCurrency_ExchangeRatesThread()
    {
      Object _item;
      CurrencyMultisiteThreadData _data;
      int _tick0;
      int _timeout;
      DataTable _dt_exchange_rate_currency;

      _dt_exchange_rate_currency = null;
      _tick0 = 0;

      try
      {
        _tick0 = Misc.GetTickCount();
        _timeout = Timeout.Infinite;
        
        // Dequeue request
        while (m_queue_currency_multisite.Dequeue(out _item, _timeout))
        {
          _data = (CurrencyMultisiteThreadData)_item;
          _dt_exchange_rate_currency = null;

          if (_data.m_date_from == DateTime.MinValue || _data.m_date_to == DateTime.MaxValue || _data.m_date_to == DateTime.MinValue)
          {
            continue;
          }

          // Get currencies rates
          CurrencyMultisite.GetAllFilteredSitesCurrencies(_data.m_date_from, _data.m_date_to, out _dt_exchange_rate_currency);

          if (_data.m_event_redraw_currencies_rate != null)
          {
            // Invoke delegate from control
            ((System.Windows.Forms.UserControl)_data.m_control).BeginInvoke(_data.m_event_redraw_currencies_rate, _dt_exchange_rate_currency);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    // PURPOSE: Multi currency exchange Thread Data
    //
    // PARAMS:
    //     - INPUT:
    //        - None
    //     - OUTPUT:
    //        - None
    //
    // RETURNS:
    //
    public class CurrencyMultisiteThreadData
    {
      public DateTime m_date_from;
      public DateTime m_date_to;
      public CurrencyMultiSiteChangedHandler m_event_redraw_currencies_rate;
      public Object m_control;

      //------------------------------------------------------------------------------
      // PURPOSE: Create instance for currency multisite thread data
      //
      //  PARAMS:
      //     - INPUT:
      //        - DateTime: DateFrom
      //        - DateTime: DateTo
      //        - Object: Control/UserControl
      //        - CurrencyMultiSiteChangedHandler: Event
      //
      //     - OUTPUT:
      //
      // RETURNS:
      public CurrencyMultisiteThreadData(DateTime DateFrom, DateTime DateTo, Object Control, CurrencyMultiSiteChangedHandler Event_RedrawCurrenciesRate)
      {
        this.m_date_from = DateFrom;
        this.m_date_to = DateTo;
        this.m_control = Control;
        this.m_event_redraw_currencies_rate = Event_RedrawCurrenciesRate;
      }
    }
  }

}
