using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  class StatisticsItem
  {
    internal const int MAX_ITEMS = 4;

    internal String m_name;
    internal Int64 m_num_samples;
    internal Int64[] m_sum = new Int64[MAX_ITEMS];
    internal Int64[] m_sum2 = new Int64[MAX_ITEMS];
    internal Int64[] m_min = new Int64[MAX_ITEMS];
    internal Int64[] m_max = new Int64[MAX_ITEMS];

    internal StatisticsItem(String Name)
    {
      m_name = Name;
    }
  }

  public class Statistics
  {
    public Object m_lock_root = new object();

    private int m_tick0 = Misc.GetTickCount();
    private Dictionary<String, StatisticsItem> m_dictionary = new Dictionary<String, StatisticsItem>();

    public Int64 ElapsedTicks
    {
      get
      {
        return Misc.GetElapsedTicks(m_tick0);
      }
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Save the SiteTable to DB.
    //
    //  PARAMS :
    //      - INPUT : 
    //        - String Source
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void Save(String Source)
    {
      Dictionary<String, StatisticsItem> _aux;
      Int64 _elapsed;
      DataTable _table;
      StringBuilder _sb;

      lock (m_lock_root)
      {
        _elapsed = Misc.GetElapsedTicks(m_tick0);
        _aux = m_dictionary;

        m_tick0 = Misc.GetTickCount();
        m_dictionary = new Dictionary<String, StatisticsItem>();
      }

      // Table is created
      _table = new DataTable();
      _table.Columns.Add("sta_datetime", Type.GetType("System.DateTime")).AllowDBNull = false;
      _table.Columns.Add("sta_source1", Type.GetType("System.String")).AllowDBNull = false;
      _table.Columns.Add("sta_source2", Type.GetType("System.String")).AllowDBNull = false;
      _table.Columns.Add("sta_source3", Type.GetType("System.String")).AllowDBNull = false;
      _table.Columns.Add("sta_interval", Type.GetType("System.Int32")).AllowDBNull = false;
      _table.Columns.Add("sta_num_samples", Type.GetType("System.Int32")).AllowDBNull = false;
      _table.Columns.Add("sta_sum_0", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_sum_1", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_sum_2", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_sum_3", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_sum2_0", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_sum2_1", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_sum2_2", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_sum2_3", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_min_0", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_min_1", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_min_2", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_min_3", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_max_0", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_max_1", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_max_2", Type.GetType("System.Int64")).AllowDBNull = false;
      _table.Columns.Add("sta_max_3", Type.GetType("System.Int64")).AllowDBNull = false;

      foreach (StatisticsItem _item in _aux.Values)
      {
        DataRow _row;
        _row = _table.NewRow();

        _row["STA_DATETIME"] = WGDB.Now;
        _row["STA_SOURCE1"] = Source;
        _row["STA_SOURCE2"] = Environment.MachineName;
        _row["STA_SOURCE3"] = _item.m_name;
        _row["STA_INTERVAL"] = _elapsed;
        _row["STA_NUM_SAMPLES"] = _item.m_num_samples;
        _row["STA_SUM_0"] = _item.m_sum[0];
        _row["STA_SUM_1"] = _item.m_sum[1];
        _row["STA_SUM_2"] = _item.m_sum[2];
        _row["STA_SUM_3"] = _item.m_sum[3];
        _row["STA_SUM2_0"] = _item.m_sum2[0];
        _row["STA_SUM2_1"] = _item.m_sum2[1];
        _row["STA_SUM2_2"] = _item.m_sum2[2];
        _row["STA_SUM2_3"] = _item.m_sum2[3];
        _row["STA_MIN_0"] = _item.m_min[0];
        _row["STA_MIN_1"] = _item.m_min[1];
        _row["STA_MIN_2"] = _item.m_min[2];
        _row["STA_MIN_3"] = _item.m_min[3];
        _row["STA_MAX_0"] = _item.m_max[0];
        _row["STA_MAX_1"] = _item.m_max[1];
        _row["STA_MAX_2"] = _item.m_max[2];
        _row["STA_MAX_3"] = _item.m_max[3];

        _table.Rows.Add(_row);
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();

          _sb.AppendLine("   INSERT INTO STATS");
          _sb.AppendLine("           ( STA_DATETIME, STA_SOURCE1, STA_SOURCE2, STA_SOURCE3");
          _sb.AppendLine("           , STA_INTERVAL");
          _sb.AppendLine("           , STA_NUM_SAMPLES");
          _sb.AppendLine("           , STA_SUM_0,  STA_SUM_1,  STA_SUM_2,  STA_SUM_3");
          _sb.AppendLine("           , STA_SUM2_0, STA_SUM2_1, STA_SUM2_2, STA_SUM2_3");
          _sb.AppendLine("           , STA_MIN_0,  STA_MIN_1,  STA_MIN_2,  STA_MIN_3");
          _sb.AppendLine("           , STA_MAX_0,  STA_MAX_1,  STA_MAX_2,  STA_MAX_3)");
          _sb.AppendLine("   VALUES");
          _sb.AppendLine("           ( @p_datetime, @p_source1, @p_source2, @p_source3");
          _sb.AppendLine("           , @p_interval");
          _sb.AppendLine("           , @p_num_samples");
          _sb.AppendLine("           , @p_sum_0,  @p_sum_1,  @p_sum_2,  @p_sum_3");
          _sb.AppendLine("           , @p_sum2_0, @p_sum2_1, @p_sum2_2, @p_sum2_3");
          _sb.AppendLine("           , @p_min_0,  @p_min_1,  @p_min_2,  @p_min_3");
          _sb.AppendLine("           , @p_max_0,  @p_max_1,  @p_max_2,  @p_max_3 )");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Batch Insert
            _sql_cmd.Parameters.Add("@p_datetime", SqlDbType.DateTime).SourceColumn = "STA_DATETIME";
            _sql_cmd.Parameters.Add("@p_source1", SqlDbType.NVarChar, 50).SourceColumn = "STA_SOURCE1";
            _sql_cmd.Parameters.Add("@p_source2", SqlDbType.NVarChar, 50).SourceColumn = "STA_SOURCE2";
            _sql_cmd.Parameters.Add("@p_source3", SqlDbType.NVarChar, 50).SourceColumn = "STA_SOURCE3";
            _sql_cmd.Parameters.Add("@p_interval", SqlDbType.Int).SourceColumn = "STA_INTERVAL";
            _sql_cmd.Parameters.Add("@p_num_samples", SqlDbType.Int).SourceColumn = "STA_NUM_SAMPLES";
            _sql_cmd.Parameters.Add("@p_sum_0", SqlDbType.BigInt).SourceColumn = "STA_SUM_0";
            _sql_cmd.Parameters.Add("@p_sum_1", SqlDbType.BigInt).SourceColumn = "STA_SUM_1";
            _sql_cmd.Parameters.Add("@p_sum_2", SqlDbType.BigInt).SourceColumn = "STA_SUM_2";
            _sql_cmd.Parameters.Add("@p_sum_3", SqlDbType.BigInt).SourceColumn = "STA_SUM_3";
            _sql_cmd.Parameters.Add("@p_sum2_0", SqlDbType.BigInt).SourceColumn = "STA_SUM2_0";
            _sql_cmd.Parameters.Add("@p_sum2_1", SqlDbType.BigInt).SourceColumn = "STA_SUM2_1";
            _sql_cmd.Parameters.Add("@p_sum2_2", SqlDbType.BigInt).SourceColumn = "STA_SUM2_2";
            _sql_cmd.Parameters.Add("@p_sum2_3", SqlDbType.BigInt).SourceColumn = "STA_SUM2_3";
            _sql_cmd.Parameters.Add("@p_min_0", SqlDbType.BigInt).SourceColumn = "STA_MIN_0";
            _sql_cmd.Parameters.Add("@p_min_1", SqlDbType.BigInt).SourceColumn = "STA_MIN_1";
            _sql_cmd.Parameters.Add("@p_min_2", SqlDbType.BigInt).SourceColumn = "STA_MIN_2";
            _sql_cmd.Parameters.Add("@p_min_3", SqlDbType.BigInt).SourceColumn = "STA_MIN_3";
            _sql_cmd.Parameters.Add("@p_max_0", SqlDbType.BigInt).SourceColumn = "STA_MAX_0";
            _sql_cmd.Parameters.Add("@p_max_1", SqlDbType.BigInt).SourceColumn = "STA_MAX_1";
            _sql_cmd.Parameters.Add("@p_max_2", SqlDbType.BigInt).SourceColumn = "STA_MAX_2";
            _sql_cmd.Parameters.Add("@p_max_3", SqlDbType.BigInt).SourceColumn = "STA_MAX_3";

            using (SqlDataAdapter _da = new SqlDataAdapter())
            {
              _da.InsertCommand = _sql_cmd;
              _da.UpdateBatchSize = 500;
              _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

              if (_da.Update(_table) != _table.Rows.Count)
              {
                // Not all data has been inserted
                Log.Message("StatisticsItem.Save Not all data has been inserted");
              }

              _db_trx.Commit();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    } // Save

    public void AddSample(String Name, Int64 NumTasks, Int64 TimeQueue, Int64 TimeProcess)
    {
      StatisticsItem _item;
      Int64[] _value;

      lock (m_lock_root)
      {
        if (m_dictionary.ContainsKey(Name))
        {
          _item = m_dictionary[Name];
        }
        else
        {
          _item = new StatisticsItem(Name);
          m_dictionary.Add(Name, _item);
        }

        _value = new Int64[StatisticsItem.MAX_ITEMS];
        _value[0] = NumTasks;
        _value[1] = TimeQueue;
        _value[2] = TimeProcess;
        _value[3] = TimeQueue + TimeProcess;

        lock (_item)
        {
          _item.m_num_samples++;

          for (int _i = 0; _i < StatisticsItem.MAX_ITEMS; _i++)
          {
            _item.m_sum[_i] += _value[_i];
            _item.m_sum2[_i] += (_value[_i] * _value[_i]);

            if (_item.m_num_samples == 1)
            {
              _item.m_min[_i] = _value[_i];
              _item.m_max[_i] = _value[_i];
            }
            else
            {
              _item.m_min[_i] = Math.Min(_item.m_min[_i], _value[_i]);
              _item.m_max[_i] = Math.Max(_item.m_max[_i], _value[_i]);
            }
          }
        }
      }
    } // AddSample

  }
}
