﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Cashamount.cs
// 
//   DESCRIPTION: CashAmount class, calculate total cash amount.
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 24-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-NOV-2015 FOS    First release
// 01-DEC-2015 FOS    Fixed Bug 7205: Protected taxWaiverMode and renamed function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015 FOS    Fixed Bug 7208: Incorrect Tax Value on Handpays
// 16-DEC-2015 FOS    Fixed Bug 7361: It doesn't make a credits withdrawal after a recharge operation
// 18-DEC-2015 FOS    Fixed Bug 6769: It doesn't delete taxes when TITO tickets are paid
// 19-MAY-2016 SDS    Fixed Bug 13001: Al realizar un pago de premio teniendo créditos no redimibles los elimina
// 25-MAY-2016 ETP    Fixed bug 13631: added some logs.
// 31-MAY-2016 EOR    Fixed Bug 12945: Custoudy Wrong Calculate for redeem without prize
// 18-JUL-2016         Fixed Bug 14810: Reintegro Total en una cuenta - Mantiene los créditos no redimibles
// 10-OCT-2016 LTC    Bug 18984:Cash desk draw: Service charge wrong formual on redeem
// 08-FEB-2017 JML    BUG 25253: Errors on Custody in case Type Machines (1)
// 09-mar-2017 FGB    PBI 25269: Third TAX - Cash session summary
// 28-MAR-2016 FJC    Bug 26250:Al hacer una reintegro sin juego no se muestra la información correcta en el ticket de pago - Aguascalientes
// 20-APR-2017 JML    Fixed Bug 26807: Custody - Incorrect custody amount to be refunded
// 09-MAY-2017 FJC    Fixed Bug 27235:Recarga banco móvil - Perdida de saldo (Concepto premio)
// 29-JUN-2017 JML    Fixed Bug 28465:WIGOS-3241 Tax custody is not returned after sale chips with recharge playing on gaming tables draw
// 05-JUL-2017 RGR    Fixed Bug 28571:WIGOS-2994 Custody Ticket
// 31-JUL-2017 DHA    WIGOS-3691 [Ticket #5397] Problemas con boton Pago de premios en efectivo
// 18-DEC-2017 DHA    Bug 31103:WIGOS-5431 [Ticket #9121] Unbalance in cashier by cents - Repay v03.006.0023
// 01-JAN-2018  DHA    Bug 31392:WIGOS-7261 Cashier - Prize payment operation doesn't work. Warning mesage "No es posible realizar la operación" is displayed.
// 01-JAN-2018  DHA    Bug 31393:WIGOS-7793 Service charge: wrong calculation with Terminal draw
// 11-JUN-2018 AGS    Bug 32984:WIGOS-12537 [Ticket #14786] Ticket cobrados Mesas
// -----------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WSI.Common
{
  public class CashAmount
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Calculates the amounts related to the card redeem:
    //          - Devolutions.
    //          - Won amount.
    //          - Tax amount.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CashMode : Type of Redeem
    //            - PARTIAL_REDEEM
    //            -  TOTAL_REDEEM
    //          - ToCash : Amount to cash.
    //          - DoCalculateSplits : Flag to indicate if read and calculate splits.
    //
    //      - OUTPUT:
    //          - CashRedeemAB: Total Cash info.
    //          - CashCompanyA: Cash info for Company A.
    //          - CashCompanyB: Cash info for Company B.
    //          - Splits: Split parameters read from general params (if DoCalculateSplits is true).
    //
    // RETURNS:
    //      - true: Redeem is possible from the amount to cash provided.
    //      - false: Redeem is not possible from the amount to cash provided.
    // 
    //   NOTES:
    //    
    public bool CalculateCashAmount(CASH_MODE CashMode, TYPE_SPLITS Splits,
                                     MultiPromos.PromoBalance IniBalance,
                                     Currency AmountToCash,
                                     Boolean IsApplyTaxes,
                                     CardData CardData,
                                     out MultiPromos.PromoBalance FinBalance,
                                     out TYPE_CASH_REDEEM CashRedeemAB,
                                     out TYPE_CASH_REDEEM CashCompanyA,
                                     out TYPE_CASH_REDEEM CashCompanyB)
    {
      return CalculateCashAmount(CashMode, Splits, IniBalance, AmountToCash, false, IsApplyTaxes, false, CardData, out FinBalance, out CashRedeemAB, out CashCompanyA, out CashCompanyB);
    }

    public bool CalculateCashAmountWithRedeemableTaxes(CASH_MODE CashMode, TYPE_SPLITS Splits,
                                     MultiPromos.PromoBalance IniBalance,
                                     Currency AmountToCash,
                                     Boolean IsChipsPurchaseWithCashOut,
                                     Boolean IsEnabledParam,
                                     CardData CardData,
                                     OperationCode OpCode,
                                     out MultiPromos.PromoBalance FinBalance,
                                     out TYPE_CASH_REDEEM CashRedeemAB,
                                     out TYPE_CASH_REDEEM CashCompanyA,
                                     out TYPE_CASH_REDEEM CashCompanyB)
    {
      // Conditionally disable Partial Redeem
      if (CashMode == CASH_MODE.PARTIAL_REDEEM && !(OpCode == OperationCode.PRIZE_PAYOUT_AND_RECHARGE || OpCode == OperationCode.PRIZE_PAYOUT))
      {
        if (GeneralParam.GetBoolean("Cashier", "PartialRedeem.Disabled"))
        {
          FinBalance = MultiPromos.PromoBalance.Zero;
          CashRedeemAB = new TYPE_CASH_REDEEM();
          CashCompanyA = new TYPE_CASH_REDEEM();
          CashCompanyB = new TYPE_CASH_REDEEM();

          return false;
        }
      }

      //Set Redeemable promotion taxes

      DevolutionPrizeParts _parts = new DevolutionPrizeParts(IsChipsPurchaseWithCashOut, OpCode);
 
      return CalculateCashAmountWork(CashMode,
                                     Splits,
                                     IniBalance,
                                     AmountToCash,
                                     IsChipsPurchaseWithCashOut,
                                     true,
                                     IsEnabledParam,
                                     false,
                                     CardData,
                                     _parts,
                                     true,
                                     out FinBalance,
                                     out CashRedeemAB,
                                     out CashCompanyA,
                                     out CashCompanyB);
    }

    public bool CalculateCashAmount(CASH_MODE CashMode, TYPE_SPLITS Splits,
                                     MultiPromos.PromoBalance IniBalance,
                                     Currency AmountToCash,
                                     Boolean IsChipsPurchaseWithCashOut,
                                     Boolean IsApplyTaxes,
                                     Boolean IsEnabledParam,
                                     CardData CardData,
                                     out MultiPromos.PromoBalance FinBalance,
                                     out TYPE_CASH_REDEEM CashRedeemAB,
                                     out TYPE_CASH_REDEEM CashCompanyA,
                                     out TYPE_CASH_REDEEM CashCompanyB)
    {
      Boolean _keepNotRedeemable = false;
      // AJQ 21-AUG-2012, Conditionally disable Partial Redeem
      if (CashMode == CASH_MODE.PARTIAL_REDEEM)
      {
        if (GeneralParam.GetBoolean("Cashier", "PartialRedeem.Disabled"))
        {
          FinBalance = MultiPromos.PromoBalance.Zero;
          CashRedeemAB = new TYPE_CASH_REDEEM();
          CashCompanyA = new TYPE_CASH_REDEEM();
          CashCompanyB = new TYPE_CASH_REDEEM();

          return false;
        }
      }
     
      //SDS 19-05-2016 Bug 13001
      if (IsApplyTaxes)
        _keepNotRedeemable = true;

      return CalculateCashAmountWork(CashMode,
                                     Splits,
                                     IniBalance,
                                     AmountToCash,
                                     IsChipsPurchaseWithCashOut,
                                     IsApplyTaxes,
                                     IsEnabledParam,
                                     false,
                                     CardData,
                                     null,
                                     _keepNotRedeemable,
                                     out FinBalance,
                                     out CashRedeemAB,
                                     out CashCompanyA,
                                     out CashCompanyB);
    } // CalculateCashAmount

    public void CalculateCashAmount_GetErrorMessage(Decimal TotalRedeemable, Currency VoucherAmount, CardData CardData, out String StrWarning)
    {
      CalculateCashAmount_GetErrorMessage(TotalRedeemable, VoucherAmount, false, CardData, out StrWarning);
    }

    public void CalculateCashAmount_GetErrorMessage(Decimal TotalRedeemable, Currency VoucherAmount, Boolean IsChipsPurchaseWithCashOut, CardData CardData, out String StrWarning)
    {
      DevolutionPrizeParts _parts_warning;

      _parts_warning = new DevolutionPrizeParts(IsChipsPurchaseWithCashOut);
      StrWarning = Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR");

      _parts_warning.Compute(TotalRedeemable, CardData.MaxDevolution, true, true); 
      
      if (VoucherAmount > _parts_warning.TotalPayable)
      {
        StrWarning = Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_PAYABLE");
      }
      else if (_parts_warning.DecimalRoundingEnabled && _parts_warning.RoundToCents > 1)
      {
        Int64 _cents;

        _cents = (Int64)(VoucherAmount * 100);
        if (_cents % _parts_warning.RoundToCents != 0)
        {
          StrWarning = Resource.String("STR_FRM_DECIMAL_REEDEM_ERROR");
        }
      }
    }

    public bool CalculateCashAmountWork(CASH_MODE CashMode, TYPE_SPLITS Splits,
                                     MultiPromos.PromoBalance IniBalance,
                                     Currency AmountToPay,
                                     CardData CardData,
                                     out MultiPromos.PromoBalance FinBalance,
                                     out TYPE_CASH_REDEEM CashRedeemAB,
                                     out TYPE_CASH_REDEEM CashCompanyA,
                                     out TYPE_CASH_REDEEM CashCompanyB)
    {
      return CalculateCashAmountWork(CashMode,
                                     Splits,
                                     IniBalance,
                                     AmountToPay,
                                     false,
                                     false,
                                     false,
                                     false,
                                     CardData,
                                     null,
                                     false,
                                     out FinBalance,
                                     out CashRedeemAB,
                                     out CashCompanyA,
                                     out CashCompanyB);
    }

    public bool CalculateCashAmountTicketPayment(CASH_MODE CashMode,
                                     TYPE_SPLITS Splits,
                                     MultiPromos.PromoBalance IniBalance,
                                     Currency AmountToPay,
                                     Boolean IsChipsPurchaseWithCashOut,
                                     Boolean IsApplyTaxes,
                                     Boolean IsParamEnabled,
                                     Boolean IsTicketPayment,
                                     CardData CardData,
                                     out MultiPromos.PromoBalance FinBalance,
                                     out TYPE_CASH_REDEEM CashRedeemAB,
                                     out TYPE_CASH_REDEEM CashCompanyA,
                                     out TYPE_CASH_REDEEM CashCompanyB)
    {
      return CalculateCashAmountWork(CashMode, Splits, IniBalance, AmountToPay, IsChipsPurchaseWithCashOut, IsApplyTaxes, IsParamEnabled, IsTicketPayment, CardData, null, false, out FinBalance, out CashRedeemAB, out CashCompanyA, out CashCompanyB);
    }

    public bool CalculateCashAmountWork(CASH_MODE CashMode,
                                        TYPE_SPLITS Splits,
                                     MultiPromos.PromoBalance IniBalance,
                                     Currency AmountToPay,
                                     Boolean IsChipsPurchaseWithCashOut,
                                     Boolean IsApplyTaxes,
                                     Boolean IsParamEnabled,
                                     Boolean IsTicketPayment,
                                     CardData CardData,
                                     DevolutionPrizeParts Parts,
                                     Boolean KeepNotRedeemable,
                                     out MultiPromos.PromoBalance FinBalance,
                                     out TYPE_CASH_REDEEM CashRedeemAB,
                                     out TYPE_CASH_REDEEM CashCompanyA,
                                     out TYPE_CASH_REDEEM CashCompanyB)
    {
      Currency _to_substract;
      DevolutionPrizeParts _parts;
      Currency _max_devolution;
      MultiPromos.AccountBalance _balance;
      MultiPromos.AccountBalance _balance_after_cancel;
      MultiPromos.PromoBalance _lost_balance;

      Decimal _amount_to_redeem;
      Decimal _cancelled_recharge;
      Decimal _cancellable_recharge;
      Decimal _max_dev_after_cancel;
      Decimal _devolution_custody;    //EOR 27-APR-2016

      Currency _a_part;
      Currency _b_part;
      Currency _b_part_after_cancel;
      CASH_MODE _cash_mode;
      DataTable _lost_promos;
      Int32 _when_redeem_cancel_all_promos;
      Currency _max_redeemable;
      Currency _max_payable;
      Currency _amount_to_pay;
      //Boolean _is_promo_with_taxes;  
      Boolean _pending_terminal_draw_recharges;  

      CashRedeemAB = new TYPE_CASH_REDEEM();
      CashCompanyA = new TYPE_CASH_REDEEM();
      CashCompanyB = new TYPE_CASH_REDEEM();

      if (Parts == null)
      {
        _parts = new DevolutionPrizeParts(IsChipsPurchaseWithCashOut);
        //_is_promo_with_taxes = false;
      }
      else
      {
        _parts = Parts;
        //_is_promo_with_taxes = true;
      }

      // Calculation steps:
      // 1. Calculate Redeemable amount.
      // 2. Calculate Devolution: Minimum between the initial Cash In and the Redeemable.
      // 3. Calculate Won amount: Redeemable - Devolution
      // 4. Calculate taxes: Federal, State and Total.
      // 5. Calculate maximum amount that can be paid: Devolution + Won - Taxes: Federal + State
      // 6. In case of Total redeem return values.
      // 7. In case of Partial Redeem calculate if input amount to cash is enough.
      // 8. Calculate amount to be taken from every part.

      // 1. Calculate Redeemable amount.
      // 2. Calculate Devolution: Minimum between the initial Cash In and the Redeemable.      
      // 3. Calculate Won amount: Redeemable - Devolution

      _cash_mode = CashMode;
      _cancelled_recharge = 0;
      _a_part = 0;
      _b_part = 0;
      _b_part_after_cancel = 0;
      _devolution_custody = 0;
      _pending_terminal_draw_recharges = Misc.Common_CheckPendingTerminalDrawsRecharges(CardData.AccountId);

      //LTC 11-OCT-2016
      if (Tax.ServiceChageOnlyCancellable == true)
      {
          _parts.DevolutionOnlyCancellable = CardData.Cancellable.TotalNotPlayedRecharge;          
      }
      
      if (IsChipsPurchaseWithCashOut)
      {
        _max_devolution = CardData.MaxDevolutionForChips;
        CardData.AccountBalance = new MultiPromos.AccountBalance(AmountToPay, 0, 0);
        CardData.CurrentBalance = AmountToPay;
      }
      else
      {
        _max_devolution = CardData.MaxDevolution;
      }
      
      _balance = new MultiPromos.AccountBalance(IniBalance.Balance);
      _balance_after_cancel = new MultiPromos.AccountBalance(IniBalance.Balance);
      _lost_balance = MultiPromos.PromoBalance.Zero;
      FinBalance = MultiPromos.PromoBalance.Zero;

      //----------- 1.
      _cancellable_recharge = 0;
      _max_dev_after_cancel = 0;

      // 09-MAY-2017 FJC Fixed Bug 27235:Recarga banco móvil - Perdida de saldo (Concepto premio)
      //  Add check with _pending_terminal_draw_recharges variable
      if (  CardData.Cancellable.TotalBalance.Redeemable > 0
         || _pending_terminal_draw_recharges)
      {
        _a_part = CardData.Cancellable.TotalBalance.Redeemable;
        if (GeneralParam.GetBoolean("Cashier", "CashInTax.AlsoApplyToSplitB"))
        {
          _b_part = Math.Round(_a_part * Splits.company_b.cashin_pct / Splits.company_a.cashin_pct, 2, MidpointRounding.AwayFromZero);  // Prize Coupon (Split B)
        }
        else
        {                    
          _b_part = Math.Round((_a_part + CardData.Cancellable.TotalCashInTaxAmount) * Splits.company_b.cashin_pct / Splits.company_a.cashin_pct, 2, MidpointRounding.AwayFromZero);  // Prize Coupon (Split B)
        }

        // If the _b_part is all the PromoRedeemable part, then the cancellation is possible.
        // In other words, if there is any redeemable promotion, cancellation is not possible.
        if (  (Math.Abs(_b_part - CardData.Cancellable.TotalBalance.PromoRedeemable) <= 0.00m) 
           || (_pending_terminal_draw_recharges))
        {
          _b_part = CardData.Cancellable.TotalBalance.PromoRedeemable;

          // EOR 27-APR-2016 Calculate Custody
          _parts.MaxDevolutionCustody = CardData.Cancellable.TotalBalance.CustodyProvision;
          _devolution_custody = _parts.CalculateDevolutionCustody(IniBalance.Balance.Redeemable, _max_devolution, IsChipsPurchaseWithCashOut);

          // The RedeemablePromotion comes from Split B
          _cancellable_recharge = _a_part + _b_part + _devolution_custody;
          if (!_parts.PrizeCouponEnabled && !Misc.IsTerminalDrawEnabled())
          {
            _max_dev_after_cancel = Math.Max(0, _max_devolution - _cancellable_recharge);
          }
          else
          {
            _max_dev_after_cancel = Math.Max(0, _max_devolution - _a_part);
          }

          _balance_after_cancel = new MultiPromos.AccountBalance(_balance);

          // There is a cancellation
          _balance_after_cancel.Redeemable -= _a_part;     // Se cancela el redimible que entro en A

          // JBP 07-03-2017: Only need withdraw real promotions redeemable amount
          _b_part_after_cancel = _b_part;

          if (!IsChipsPurchaseWithCashOut && _pending_terminal_draw_recharges)
          {
            _b_part_after_cancel = _b_part - Misc.Common_GetPendingAmountTerminalDrawsRecharges(CardData.AccountId);
          }

          _balance_after_cancel.PromoRedeemable -= _b_part_after_cancel;

          _balance_after_cancel.PromoNotRedeemable -= 0;

          if (_balance_after_cancel.Redeemable < 0              // 0
              || _balance_after_cancel.PromoRedeemable < 0      // -20
              || _balance_after_cancel.PromoNotRedeemable < 0)  // 0
          {
            // There is no cancellation
            _cancellable_recharge = 0;
            _devolution_custody = 0; //EOR 28-APR-2016
          }
        }
      }

      //----------- 2
      if (_cancellable_recharge > 0)
      {
        if (CashMode == CASH_MODE.TOTAL_REDEEM ||
           (CashMode == CASH_MODE.PARTIAL_REDEEM && AmountToPay >= _cancellable_recharge))
        {
          // Hi ha cancel
          _balance = new MultiPromos.AccountBalance(_balance_after_cancel);
          _max_devolution = _max_dev_after_cancel;
        }
        else
        {
          _devolution_custody = 0; //EOR 27-APR-2016
          _cancellable_recharge = 0;
        }
      }

      if (_cancellable_recharge <= 0)
      {
        _a_part = 0;
        _b_part = 0;
      }
      else
      {
        _cancelled_recharge = _cancellable_recharge;
      }

      if (IsChipsPurchaseWithCashOut && Misc.IsTaxCustodyEnabled())
      {
        CardData _car_data;
        _car_data = new CardData();

        CardData.DB_CardGetAllData(CardData.AccountId, _car_data);
        _balance.CustodyProvision = _car_data.AccountBalance.CustodyProvision;
      }

      _parts.MaxDevolutionCustody = _balance.CustodyProvision;
      
      if (Tax.EnableTitoTaxWaiver(IsParamEnabled))
      {
        _parts.Compute(_balance.TotalRedeemable, _max_devolution, IsApplyTaxes, IsParamEnabled, IsChipsPurchaseWithCashOut);
      }
      else
      {
        _parts.Compute(_balance.TotalRedeemable, _max_devolution, true, IsParamEnabled, IsChipsPurchaseWithCashOut);
      }

      _max_redeemable = _parts.Devolution + _parts.Prize;
      _max_payable = _parts.TotalPayable;

      _amount_to_pay = 0;

      // _max_payable = _max_payable + Cancellation

      if (CashMode == CASH_MODE.TOTAL_REDEEM)
      {
        _amount_to_redeem = _max_redeemable;
        _amount_to_pay = _max_payable;
        // Return the deposit: only when user select "TOTAL_REDEEM"
        // DRV 09-MAR-2015: Card deposit it's only redeemed with EGM TOTAL_REDEEM
        if (!IsChipsPurchaseWithCashOut)
        {
          //DCS 07-JUL-2015: Add card player refundable to Company B
          if (Misc.IsCardPlayerToCompanyB())
          {
            CashCompanyB.card_deposit = CardData.CardDepositToRedeem(CardData, out CashCompanyB.is_deposit);
            CashCompanyA.is_deposit = false;
          }
          else
          {
            CashCompanyA.card_deposit = CardData.CardDepositToRedeem(CardData, out CashCompanyA.is_deposit);
            CashCompanyB.is_deposit = false;
          }
        }
      }
      else
      {
        _amount_to_pay = Math.Max(0, ((Decimal)AmountToPay) - _cancellable_recharge);
        _amount_to_redeem = _max_redeemable;

        if (IsChipsPurchaseWithCashOut && _amount_to_pay > _max_payable)
        {
          _amount_to_pay = _max_payable;
        }
      }

      if (_amount_to_pay < 0 || _amount_to_pay > _max_payable && !_parts.IsPromotionWithTaxes)
      {
        Log.Warning(String.Format("CalculateCashAmountWork: Incorrect amount to pay: Amount to pay = {0} / Max payable = {1}", _amount_to_pay, _max_payable));
        return false;
      }

      // Check "cents'
      long _cents_to_pay;

      _cents_to_pay = (long)(_amount_to_pay * 100.00m);
      if (_cents_to_pay % _parts.RoundToCents != 0)
      {
        Log.Error(String.Format("CalculateCashAmountWork: Incorrect cents to pay: Cents to pay = {0} / RoundToCents = {1}", _cents_to_pay, _parts.RoundToCents));
        return false;
      }

      int _num_one_cents = 0;

      if (_max_payable <= 0)
      {
        _amount_to_redeem = _max_redeemable;
      }
      else
      {
        if (_parts.IsPromotionWithTaxes)
        {
          _amount_to_redeem = _amount_to_pay;
        }
        else
        {
          _amount_to_redeem = Math.Round(_max_redeemable * _amount_to_pay / _max_payable, 2, MidpointRounding.AwayFromZero);
        }

        _amount_to_redeem = Math.Min(_amount_to_redeem, _max_redeemable);
      }

      Decimal _delta;
      Int64 _max_cents;

      if (Tax.EnableTitoTaxWaiver(IsParamEnabled))
      {
        _parts.Compute(_amount_to_redeem, _max_devolution, IsApplyTaxes, IsParamEnabled, IsChipsPurchaseWithCashOut);
      }
      else
      {
        _parts.Compute(_amount_to_redeem, _max_devolution, true, IsParamEnabled, IsChipsPurchaseWithCashOut);
        if (_parts.IsPromotionWithTaxes)
        {
          _amount_to_pay = _parts.TotalPayable;
        }
      }

      _delta = Math.Round(Math.Abs(_parts.TotalPayable - _amount_to_pay), 2, MidpointRounding.AwayFromZero);
      _delta = Math.Max(0.01m, _delta);
      _max_cents = Math.Max(100, (Int64)(_amount_to_redeem * 100));

      while (_num_one_cents < _max_cents && !IsChipsPurchaseWithCashOut)
      {
        if (Tax.EnableTitoTaxWaiver(IsParamEnabled))
        {
          _parts.Compute(_amount_to_redeem, _max_devolution, IsApplyTaxes, IsParamEnabled, IsChipsPurchaseWithCashOut);
        }
        else
        {
          _parts.Compute(_amount_to_redeem, _max_devolution, true, IsParamEnabled, IsChipsPurchaseWithCashOut);
        }

        if (_parts.TotalPayable == _amount_to_pay)
        {
          // AmountToRedeem found!

          if (_amount_to_pay < _max_payable)
          {
            // Partial Redeem
            while (_amount_to_redeem > 0)
            {
              // Seek for the minimum amount to redeem that pays the same amount
              if (Tax.EnableTitoTaxWaiver(IsParamEnabled))
              {
                _parts.Compute(_amount_to_redeem - 0.01m, _max_devolution, IsApplyTaxes, IsParamEnabled, IsChipsPurchaseWithCashOut);
              }
              else
              {
                _parts.Compute(_amount_to_redeem - 0.01m, _max_devolution, true, IsParamEnabled, IsChipsPurchaseWithCashOut);
              }

              if (_parts.TotalPayable == _amount_to_pay)
              {
                // Found an amount that pays the same
                _amount_to_redeem = _amount_to_redeem - 0.01m;
              }
              else
              {
                if (Tax.EnableTitoTaxWaiver(IsParamEnabled))
                {
                  _parts.Compute(_amount_to_redeem, _max_devolution, IsApplyTaxes, IsParamEnabled, IsChipsPurchaseWithCashOut);
                }
                else
                {
                  // The previous test didn't pay the same amount, stop seeking
                  _parts.Compute(_amount_to_redeem, _max_devolution, true, IsParamEnabled, IsChipsPurchaseWithCashOut);
                }

                break;
              }
            }

            //
            // At this point the amount to redeem is the minimum that pays the desired amount
            //

            if (_parts.RoundToCents > 1 && _parts.DecimalRounding != 0)
            {
              //
              // Seek for the minimum decimal rounding that pays the same amount
              //
              Decimal _min_rounding_redeem;
              Decimal _min_rounding;

              _min_rounding_redeem = _amount_to_redeem;
              _min_rounding = Math.Abs(_parts.DecimalRounding);

              for (Decimal _redeem = _amount_to_redeem + 0.01m; _redeem <= _max_redeemable; _redeem += 0.01m)
              {
                _parts.Compute(_redeem, _max_devolution, true, IsParamEnabled, IsChipsPurchaseWithCashOut);
                if (_parts.TotalPayable != _amount_to_pay)
                {
                  // The previous test didn't pay the same amount, stop seeking
                  break;
                }

                if (Math.Abs(_parts.DecimalRounding) < _min_rounding)
                {
                  // Found an amount that pays the same and minimizes the rounding, save it!
                  _min_rounding_redeem = _redeem;
                  _min_rounding = Math.Abs(_parts.DecimalRounding);
                  if (_min_rounding == 0)
                  {
                    break;
                  }
                }
              }

              // Set the amount to redeem that minimizes the rounding
              _amount_to_redeem = _min_rounding_redeem;
              if (Tax.EnableTitoTaxWaiver(IsParamEnabled))
              {
                _parts.Compute(_amount_to_redeem, _max_devolution, IsApplyTaxes, IsParamEnabled, IsChipsPurchaseWithCashOut);
              }
              else
              {
                _parts.Compute(_amount_to_redeem, _max_devolution, true, IsParamEnabled, IsChipsPurchaseWithCashOut);
              }
            }
          }

          break;
        }

        _delta = Math.Round(_delta / 2, 2, MidpointRounding.AwayFromZero);
        _delta = Math.Max(0.01m, _delta);

        if (_delta == 0.01m)
        {
          _num_one_cents++;
        }

        if (_parts.TotalPayable > _amount_to_pay)
        {
          _amount_to_redeem -= _delta;
        }
        else
        {
          _amount_to_redeem += _delta;
        }

        _amount_to_redeem = Math.Max(0, Math.Min(_amount_to_redeem, _max_redeemable));
        _amount_to_redeem = Math.Round(_amount_to_redeem, 2, MidpointRounding.AwayFromZero);
      }

      if (_amount_to_pay == _max_payable && !IsChipsPurchaseWithCashOut)
      {
        // Efective Cash Mode
        _cash_mode = CASH_MODE.TOTAL_REDEEM;
        _amount_to_redeem = _max_redeemable;
      }

      if (Tax.EnableTitoTaxWaiver(IsParamEnabled))
      {
        if (IsTicketPayment)
        {
          _max_devolution = 0;
        }

        _parts.Compute(_amount_to_redeem, _max_devolution, IsApplyTaxes, IsParamEnabled, IsChipsPurchaseWithCashOut);
      }
      else
      {
        _parts.Compute(_amount_to_redeem, _max_devolution, true, IsParamEnabled, IsChipsPurchaseWithCashOut);

        if (_parts.TotalPayable != _amount_to_pay)
        {
          Log.Error(String.Format("CalculateCashAmountWork: Diference in BD: TotalPayable = {0} / Amount_to_pay = {1}", _parts.TotalPayable, _amount_to_pay));
          return false;
        }
      }

      // At this point 'balance' contains the remaining balance after any possible cancellation

      _to_substract = _amount_to_redeem;
      if (_to_substract < 0)
      {
        Log.Warning("CalculateCashAmountWork: Nothing to substract");
        return false;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        Int32 _flag_op_id;
        Boolean _cancel_all_promos;

        // Cancel all NR promotions related with CashIn
        _cancel_all_promos = false;
        _flag_op_id = 0;

        if (_cancelled_recharge > 0)
        {
          // Cancel Only NR promotions related with the operation
          _flag_op_id = 1;

          if (_cash_mode == CASH_MODE.TOTAL_REDEEM)
          {
            // Cancel all promos related with CashIn + Operation
            _flag_op_id = -1;
          }
        }

        // DCS 07-JUL-2015: Add card player refundable to Company B
        if (CashCompanyA.is_deposit || CashCompanyB.is_deposit)
        {
          // Anonymous card "leaving" the Casino: The account should be left to ZERO
          _cancel_all_promos = true;
        }

        switch (CashMode)
        {
          case CASH_MODE.PARTIAL_REDEEM:
            if (!Int32.TryParse(GeneralParam.Value("Cashier.PartialRedeem", "CancelAllPromotions"), out _when_redeem_cancel_all_promos))
            {
              _when_redeem_cancel_all_promos = 1;
            }
            break;

          case CASH_MODE.TOTAL_REDEEM:
          default:
            if (!Int32.TryParse(GeneralParam.Value("Cashier.TotalRedeem", "CancelAllPromotions"), out _when_redeem_cancel_all_promos))
            {
              _when_redeem_cancel_all_promos = 1;
            }
            break;
        }

        // Also, can cancel all promotions when General Params "Cashier.XXX_Redeem.CancelAllPromotions" is on.
        _cancel_all_promos = (_cancel_all_promos || (_when_redeem_cancel_all_promos != 0)) && !IsChipsPurchaseWithCashOut;

        if (!CardData.GetPromotionsToBeCancelledDueToRedeem(CardData.AccountId,
                                                   _flag_op_id, CardData.Cancellable.Operations,
                                                   _cancel_all_promos, false,
                                                   out _lost_balance,
                                                   out _lost_promos, _db_trx.SqlTransaction))
        {
          return false;
        }

        if (_cancellable_recharge > 0)
        {
          // QMP & RCI 10-APR-2013
          MultiPromos.AccountBalance _prize_coupon;
          AccountPromotion _aux_promotion;
          DataRow _row;

          _prize_coupon = new MultiPromos.AccountBalance(0, _b_part, 0);
          if (!Promotion.ReadPromotionData(_db_trx.SqlTransaction, Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON, out _aux_promotion))
          {
            return false;
          }
          _row = _lost_promos.NewRow();
          _row["ACP_UNIQUE_ID"] = -1;    // NOT USED
          _row["ACP_ACCOUNT_ID"] = CardData.AccountId;
          _row["ACP_BALANCE"] = _prize_coupon.PromoRedeemable;
          _row["ACP_CREDIT_TYPE"] = _aux_promotion.credit_type;
          _row["ACP_PROMO_NAME"] = _aux_promotion.name;
          _row["ACP_PROMO_TYPE"] = _aux_promotion.type;
          _row["ACP_PROMO_ID"] = _aux_promotion.id;
          _row["ACP_ACTIVATION"] = WGDB.Now; // NOT USED
          _lost_promos.Rows.Add(_row);
          _row.AcceptChanges();

          _lost_balance.Balance += _prize_coupon;
        }
      }

      if (Tax.EnableTitoTaxWaiver(IsParamEnabled))
      {
        _parts.Compute(_amount_to_redeem, _max_devolution, IsApplyTaxes, IsParamEnabled, IsChipsPurchaseWithCashOut);
      }
      else
      {
        _parts.Compute(_amount_to_redeem, _max_devolution, true, IsParamEnabled, IsChipsPurchaseWithCashOut);
      }

      // Redeemable part
      _amount_to_redeem = Math.Max(0, Math.Min(_balance.PromoRedeemable, _to_substract));
      FinBalance.Balance.PromoRedeemable = _balance.PromoRedeemable - _amount_to_redeem;
      _to_substract = _to_substract - _amount_to_redeem;
      _amount_to_redeem = Math.Max(0, Math.Min(_balance.Redeemable, _to_substract));
      FinBalance.Balance.Redeemable = _balance.Redeemable - _amount_to_redeem;
      _to_substract = _to_substract - _amount_to_redeem;
      if (_to_substract != 0)
      {
        return false;
      }

      // Not Redeemable part
      //SDS 19-05-2016 Bug 13001
      //SDS 18-07-2016 Bug 14810
      if (KeepNotRedeemable && _parts.IsPromotionWithTaxes)
      {
        _lost_balance.Balance.PromoNotRedeemable = Math.Max(_balance.PromoNotRedeemable, _lost_balance.Balance.PromoNotRedeemable);
        FinBalance.Balance.PromoNotRedeemable = _balance.PromoNotRedeemable;
      }
      else
      { 
       _lost_balance.Balance.PromoNotRedeemable = Math.Max(0, Math.Min(_balance.PromoNotRedeemable, _lost_balance.Balance.PromoNotRedeemable));
       FinBalance.Balance.PromoNotRedeemable = _balance.PromoNotRedeemable - _lost_balance.Balance.PromoNotRedeemable;
      }

      _lost_balance.Balance.PromoRedeemable = Math.Max(_b_part, Math.Min(_balance.PromoRedeemable, _lost_balance.Balance.PromoRedeemable));
     
      FinBalance.Balance.PromoRedeemable = _b_part + FinBalance.Balance.PromoRedeemable - _lost_balance.Balance.PromoRedeemable;

      // Points part
      _lost_balance.Points = Math.Max(0, Math.Min(IniBalance.Points, _lost_balance.Points));
      FinBalance.Points = IniBalance.Points - _lost_balance.Points;
      CashCompanyA.total_cash_in = _max_devolution;
      // It's only a cancellation when the cancelled amount matches the cancellable amount. 
      CashCompanyA.cancellation = _a_part; 
      CashCompanyA.devolution = Math.Round(_parts.Devolution * Splits.company_a.devolution_pct / 100, 2, MidpointRounding.AwayFromZero);

      // Prize & Taxes
      CashCompanyA.prize = _parts.Prize;
      if (!Tax.EnableTitoTaxWaiver(IsParamEnabled) || IsApplyTaxes)
      {
        CashCompanyA.tax1_name = _parts.Tax1.TaxName;
        CashCompanyA.tax2_name = _parts.Tax2.TaxName;
        CashCompanyA.tax3_name = _parts.Tax3.TaxName;

        CashCompanyA.tax1_pct = _parts.Tax1.TaxPct;
        CashCompanyA.tax2_pct = _parts.Tax2.TaxPct;
        CashCompanyA.tax3_pct = _parts.Tax3.TaxPct;

        if (IsApplyTaxes)
        {
          CashCompanyA.tax1 = _parts.Tax1.TaxAmount;
          CashCompanyA.tax2 = _parts.Tax2.TaxAmount;
          CashCompanyA.tax3 = _parts.Tax3.TaxAmount;
        }
        else
        {
          CashCompanyA.tax1 = 0;
          CashCompanyA.tax2 = 0;
          CashCompanyA.tax3 = 0;
        }

        CashCompanyA.tax_return_enabled = (_parts.TaxReturnMode != TaxReturnMode.None);
        CashCompanyA.tax_return = _parts.TaxReturn;
      }
      else
      {
        CashCompanyA.tax1_name = _parts.Tax1.TaxName;
        CashCompanyA.tax2_name = _parts.Tax2.TaxName;
        CashCompanyA.tax3_name = _parts.Tax3.TaxName;

        CashCompanyA.tax1_pct = 0;
        CashCompanyA.tax2_pct = 0;
        CashCompanyA.tax3_pct = 0;

        CashCompanyA.tax1 = 0;
        CashCompanyA.tax2 = 0;
        CashCompanyA.tax3 = 0;

        CashCompanyA.tax_return_enabled = false;
        CashCompanyA.tax_return = 0;
      }

      CashCompanyA.decimal_rounding_enabled = _parts.DecimalRoundingEnabled;
      CashCompanyA.decimal_rounding = _parts.DecimalRounding;

      //EOR 20-APR-2016
      if (_devolution_custody > 0)
      {
        CashCompanyA.DevolutionCustody = _devolution_custody;
      }
      else
      {
        CashCompanyA.DevolutionCustody = _parts.DevolutionCustody;
      }
      
      //CashCompanyA.DevolutionTerminalDraw = CardData.AccountBalance.TerminalDrawPendingDraw;

      // NR Lost
      CashCompanyA.lost_balance = _lost_balance;
      CashCompanyA.lost_promotions = _lost_promos;
      CashCompanyA.cancelled_promo_redeemable = _b_part;
      CashCompanyB.total_cash_in = 0; //  Math.Round(CashCompanyA.total_cash_in * Splits.company_b.cashin_pct / Splits.company_a.cashin_pct, 2, MidpointRounding.AwayFromZero);
      CashCompanyB.cancellation = _b_part; 
      CashCompanyB.devolution = _parts.Devolution - CashCompanyA.devolution;
      CashCompanyB.cancellable_operations = CardData.Cancellable.Operations;
      // DHA 08-OCT-2014: if a recharge is going to refund without plays
      CashCompanyB.not_played_recharge = CardData.Cancellable.TotalNotPlayedRecharge;
      CashCompanyB.service_charge = _parts.ServiceCharge;
      CashRedeemAB = CashCompanyA;
      CashRedeemAB.is_deposit = CashCompanyA.is_deposit || CashCompanyB.is_deposit;
      CashRedeemAB.card_deposit = CashCompanyA.is_deposit ? CashCompanyA.card_deposit : CashCompanyB.card_deposit;
      CashRedeemAB.total_cash_in = CashCompanyA.total_cash_in + CashCompanyB.total_cash_in;
      CashRedeemAB.cancellation = CashCompanyA.cancellation + CashCompanyB.cancellation;
      CashRedeemAB.devolution = CashCompanyA.devolution + CashCompanyB.devolution;
      CashRedeemAB.service_charge = CashCompanyB.service_charge;

      return true;
    } // CalculateCashAmountWork

  }
}
